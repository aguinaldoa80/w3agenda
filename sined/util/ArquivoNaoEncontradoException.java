package br.com.linkcom.sined.util;

public class ArquivoNaoEncontradoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArquivoNaoEncontradoException() {
		super();
	}

	public ArquivoNaoEncontradoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArquivoNaoEncontradoException(String message) {
		super(message);
	}

	public ArquivoNaoEncontradoException(Throwable cause) {
		super(cause);
	}

}
