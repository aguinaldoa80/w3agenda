package br.com.linkcom.sined.util;


public interface ValidacaoCampoObrigatorio {
	
	public String getDisplayNameCampo();
	public String getDisplayNameLista();
	public void setDisplayNameCampo(String string);
	public void setDisplayNameLista(String string);
	
	public String getPrefixo();
	public String getCampo();
	public String getCondicao();
	public Boolean getPelomenosumregistro();

}
