package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

import br.com.linkcom.neo.util.Util;

/**
 * Mapeamento para a function clientepessoa, visando a otimiza��o das buscas dos autocompletes no Postgres.
 * @author igorcosta
 *
 */
public class OtimizacaoDocumentoListagemFunction implements SQLFunction {

   @SuppressWarnings({ "rawtypes" })
   public String render(List args, SessionFactoryImplementor factory) {
      if (args.size() != 27) {
         throw new IllegalArgumentException("Voc� precisa fornecer vinte e seis argumentos: " +
         									"campo, " +
         									"cddocumentoclasse, " +
         									"nome_pessoa, " +
         									"cpf_pessoa, " +
         									"cnpj_pessoa, " +
         									"whereIn_documentoacao, " +
         									"whereIn_papel, " +
         									"whereIn_empresa, " +
         									"descricaonumero, " +         									
         									"cddocumento, " +
         									"cddocumentotipo, " +
         									"tipopagamento, " +
         									"dtemissao1, " +
         									"dtemissao2, " +
         									"dtvencimento1, " +
         									"dtvencimento2, " +
         									"dtcompetencia1, " +
         									"dtcompetencia2, " +
         									"valorminimo, " +
         									"valormaximo, " +
         									"cdconta, " +
         									"cdvinculoProvisionado, " +
         									"cddocumentoacaoprotesto, " + 
         									"cdcontagerencial, " + 
         									"cdcentrocusto, " +
         									"cdusuario, " +
         									"whereIn_empresa_permissao "
         									);
      }
      
      String campo = this.getArg(args, 0, false);
      String cddocumentoclasse = this.getArg(args, 1, false);
      String nomePessoa = this.getArg(args, 2, true);
      String cpfPessoa = this.getArg(args, 3, false);
      String cnpjPessoa = this.getArg(args, 4, false);
      String whereInAcao = this.getArg(args, 5, false);
      String whereInPapel = this.getArg(args, 6, false);
      String whereInEmpresa = this.getArg(args, 7, false);
      String descricaonumero = this.getArg(args, 8, true);      
      String cddocumento = this.getArg(args, 9, false);
      String cddocumentotipo = this.getArg(args, 10, false);
      String tipopagamento = this.getArg(args, 11, false);
      String dtemissao1 = this.getArg(args, 12, false);
      String dtemissao2 = this.getArg(args, 13, false);
      String dtvencimento1 = this.getArg(args, 14, false);
      String dtvencimento2 = this.getArg(args, 15, false);
      String dtcompetencia1 = this.getArg(args, 16, false);
      String dtcompetencia2 = this.getArg(args, 17, false);
      String valorminimo = this.getArg(args, 18, false);
      String valormaximo = this.getArg(args, 19, false);
      String cdconta = this.getArg(args, 20, false);
      String cdvinculoProvisionado = this.getArg(args, 21, false);
      String cddocumentoacaoprotesto = this.getArg(args, 22, false);
      String cdcontagerencial = this.getArg(args, 23, false);
      String cdcentrocusto = this.getArg(args, 24, false);
      String cdusuario = this.getArg(args, 25, false);
      String whereInEmpresaPermissao = this.getArg(args, 26, false);
      
      
      return "(" + campo + " = any(fcdocumentoarray(" + 
      												cddocumentoclasse + ", " +
      												nomePessoa+ ", " +
      												cpfPessoa+ ", " +
      												cnpjPessoa+ ", " +
      												whereInAcao + ", " +
      												whereInPapel + ", " +
      												whereInEmpresa + ", "+ 
      												descricaonumero + ", " +       												
      												cddocumento + ", " +
      												cddocumentotipo + ", " +
      												tipopagamento + ", " +
      												dtemissao1 + ", " +
      												dtemissao2 + ", " +
      												dtvencimento1 + ", " +
      												dtvencimento2 + ", " +
      												dtcompetencia1 + ", " +
      												dtcompetencia2 + ", " +
      												valorminimo + ", " +
      												valormaximo + ", " +
      												cdconta + ", " +
      												cdvinculoProvisionado + ", " +
      												cddocumentoacaoprotesto + ", " +
      												cdcontagerencial + ", " +
      												cdcentrocusto + ", " +
      												cdusuario + ", " +
      												whereInEmpresaPermissao + "" +
													")))"; 
   }

   @SuppressWarnings("rawtypes")
   private String getArg(List args, int pos, boolean like) {
		Object obj = args.get(pos);
		return obj != null ? (like ? Util.strings.tiraAcento(obj.toString().toUpperCase()) : obj.toString()) : "null";
	}

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}