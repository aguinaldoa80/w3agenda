package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

/**
 * Mapeamento para a function permissao_clienteempresa.
 * @author Luiz
 *
 */
public class PermissaoClienteEmpresaFunction implements SQLFunction {

   @SuppressWarnings("unchecked")
   public String render(List args, SessionFactoryImplementor factory) {
      if (args.size() != 2) {
         throw new IllegalArgumentException("Voc� precisa passar dois argumentos(cdcliente, whereInCdempresa)");
      }
      
      String cdcliente = (String) args.get(0);
      String whereInCdempresa = (String) args.get(1);
      return " permissao_clienteempresa(" + cdcliente + "," + whereInCdempresa + ") "; 
   }

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}