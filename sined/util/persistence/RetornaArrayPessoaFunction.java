package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

/**
 * Mapeamento para a function clientepessoa, visando a otimiza��o das buscas dos autocompletes no Postgres.
 * @author igorcosta
 *
 */
public class RetornaArrayPessoaFunction implements SQLFunction {

   @SuppressWarnings("unchecked")
   public String render(List args, SessionFactoryImplementor factory) {
      if (args.size() != 2) {
         throw new IllegalArgumentException("Voc� precisa passar apenas um argumento(nomePessoa)");
      }
      
      String nomePessoa = (String) args.get(0);
      String campo = (String) args.get(1);
      return "(" + campo + " = any(retornaArrayPessoa("+ nomePessoa+ ") ) )"; 
   }

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}