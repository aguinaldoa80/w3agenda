package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

import br.com.linkcom.sined.util.SinedUtil;

/**
 * Mapeamento para a function clientepessoa, visando a otimização das buscas dos autocompletes no Postgres.
 * @author igorcosta
 *
 */
public class SubqueryDocumentoListagemFornecedorCliente implements SQLFunction {

   @SuppressWarnings("unchecked")
   public String render(List args, SessionFactoryImplementor factory) {
      String param = (String) args.get(0);
      param = param.substring(1, param.length()-1);
      
      String field = (String) args.get(1);
      String funcaoTiraacento = SinedUtil.getFuncaoTiraacento();
      return "(" + field +  " in (SELECT f.cdpessoa FROM fornecedor f "+
      		 "WHERE UPPER(" + funcaoTiraacento + "(f.razaosocial)) LIKE '%" + param + "%') " +
	 		 " or " + field + " in (SELECT c.cdpessoa FROM Cliente c " +
	 		 "WHERE UPPER(" + funcaoTiraacento + "(c.razaosocial)) LIKE '%" + param + "%'))"; 
   }

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}