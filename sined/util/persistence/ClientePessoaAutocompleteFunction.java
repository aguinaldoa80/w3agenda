package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

/**
 * Mapeamento para a function clientepessoa, visando a otimiza��o das buscas dos autocompletes no Postgres.
 * @author igorcosta
 *
 */
public class ClientePessoaAutocompleteFunction implements SQLFunction {

   @SuppressWarnings("unchecked")
   public String render(List args, SessionFactoryImplementor factory) {
      if (args.size() != 3) {
         throw new IllegalArgumentException("Voc� precisa passar pelo menos tr�s argumentos(tipoBusca, q, colunaWhere)");
      }
      
      String tipoBusca = (String) args.get(0);
      String q = (String) args.get(1);
      String colunaWhere = (String) args.get(2);
      return "exists( select cp.cdpessoa from clientepessoa("+ tipoBusca + ", "+ q + ") cp where cp.cdpessoa="+ colunaWhere +  " )"; 
   }

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}