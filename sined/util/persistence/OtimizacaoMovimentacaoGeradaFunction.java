package br.com.linkcom.sined.util.persistence;


import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.Mapping;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

/**
 * Mapeamento para a function fcmovimentacaoarray, visando a otimiza��o da consulta do metodo findMovimentacaoGerada.
 * @author Rafael Salvio
 *
 */
public class OtimizacaoMovimentacaoGeradaFunction implements SQLFunction {

   @SuppressWarnings("unchecked")
   public String render(List args, SessionFactoryImplementor factory) {
      if (args.size() != 2) {
         throw new IllegalArgumentException("Voc� precisa fornecer dois argumentos: campo, cddocumento");
      }
      String campo = args.get(0) != null ? (String) args.get(0) : "null";
      String cddocumento = args.get(1) != null ? (String) args.get(1) : "null";
    
      return "(" + campo + " = any(fcmovimentacaoarray(" + cddocumento +")))"; 
   }

   @Override
   public Type getReturnType(Type columnType, Mapping mapping) throws QueryException {
      return new BooleanType();
   }

   @Override
   public boolean hasArguments() {
      return true;
   }

   @Override
   public boolean hasParenthesesIfNoArguments() {
      return false;
   }
}