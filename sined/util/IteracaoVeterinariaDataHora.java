package br.com.linkcom.sined.util;

import java.sql.Date;

import br.com.linkcom.neo.types.Hora;

public class IteracaoVeterinariaDataHora {
	
	private Date data;
	private Hora hora;
	
	public IteracaoVeterinariaDataHora(Date data, Hora hora){
		this.data = data;
		this.hora = hora;
	}

	public Date getData() {
		return data;
	}

	public Hora getHora() {
		return hora;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IteracaoVeterinariaDataHora){
			IteracaoVeterinariaDataHora iteracaoVeterinariaDataHora = (IteracaoVeterinariaDataHora) obj; 
			return SinedDateUtils.diferencaDias(iteracaoVeterinariaDataHora.getData(), data)==0 && iteracaoVeterinariaDataHora.getHora().equals(hora);
		}else {
			return super.equals(obj);
		}
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}