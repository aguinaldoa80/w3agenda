package br.com.linkcom.sined.util;


public class BarCodeValidation {
	
	private static int minValidLength = 8;
	private static int maxValidLength = 14;
	
	private static boolean onlyNumbers(String value){
		String numeros = "0123456789";
		for (int i = 0; i < value.length(); i++) {
			String caractere = value.substring(i, i + 1);
			if (numeros.indexOf(caractere) == -1)
				return false;
		}
		return true;
	}
	
	public static boolean valid(String value){
		try {
			if(value == null || value.trim().equals("")) 
				return false;
			
			switch (value.length()) {
				case 8:
				case 12:
				case 13:
				case 14:
					break;
				default:
					return false;
			}
			
			if(value.length() < minValidLength || value.length() > maxValidLength) 
				return false;
			
			if(!onlyNumbers(value))
				return false;
			
			String barcode = value.substring(0, value.length() - 1);
			int checksum = Integer.parseInt(value.substring(value.length() - 1));
			int calcSum = 0;
			int calcChecksum = 0;
			
			char[] barcodeCharArray = barcode.toCharArray();
			for (int i = 0; i < barcodeCharArray.length; i++) {
				int number = Integer.parseInt(String.valueOf(barcodeCharArray[i]));
				int index = i;
				if(value.length() % 2 == 0){
					index += 1;
				}
				if(index % 2 == 0){
					calcSum += number;
				} else {
					calcSum += number * 3;
				}
			}
			
			calcSum %= 10;
			calcChecksum = (calcSum == 0) ? 0 : (10 - calcSum);
			
			return calcChecksum == checksum;
		} catch (Exception e) {
			return false;
		}
	}
	
	
}
