package br.com.linkcom.sined.util;

import javax.servlet.http.HttpServletRequest;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.exception.NotInNeoContextException;
import br.com.linkcom.sined.geral.bean.Envioemail;



public class EmailUtil {
	
	public static final String EMAIL_YAHOO = "sindis.linkcom@yahoo.com.br";
	public static final String EMAIL_HOTMAIL = "sindis.linkcom@hotmail.com";
	public static final String EMAIL_GMAIL = "sindis.linkcom@gmail.com";
	
	/**
	 * Chave gerada para ir no link do download do boleto gerado no mailing.
	 * 
	 * @param id
	 * @author Thiago Clemente
	 * 
	 */
	public static String getKeyMailingBoleto(Integer id){
		
		Long hash = (long) (((id * 13 / 11) * 33) / 21) - id;
		Long dv = hash % 17;
		String key = hash.toString() + dv.toString();
		
		return key;
	}
	
	/**
	 * M�todo que retorna uma imagem em HTML para confirma��o de leitura de e-mail.
	 * 
	 * @param urlImagem
	 * @param cdenvioemail
	 * @param email
	 * @author Thiago Clemente
	 * 
	 */
	public static String getHtmlConfirmacaoEmail(Envioemail envioemail, String email){
		try{
			HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
			String url = servletRequest.getRequestURL().toString();
			String[] urlDividida = url.split("/");
			StringBuilder urlImagem = new StringBuilder();
			urlImagem.append("http://");
			urlImagem.append(urlDividida[2]);
			urlImagem.append("/");
			urlImagem.append(urlDividida[3]);
			urlImagem.append("/pub/ConfirmacaoEmail");
			
			return montarImagemLink(envioemail, email, urlImagem.toString());
		}catch (NotInNeoContextException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getHtmlConfirmacaoEmailWithoutContext(Envioemail envioemail, String email, String dominio){
		try{
			String url = "http://" + dominio + "/w3erp/pub/ConfirmacaoEmail"; 
			if(SinedUtil.isAmbienteDesenvolvimento()){
				url = "http://linkcom.local:8080/w3erp/pub/ConfirmacaoEmail"; 
			}
			
			return montarImagemLink(envioemail, email, url);
		}catch (NotInNeoContextException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private static String montarImagemLink(Envioemail envioemail, String email, String url) {
		StringBuilder html = new StringBuilder();
		html.append("<br>");
		html.append("<img src=\"");
		html.append(url);
		html.append("?ACAO=confirmacao&cdenvioemail=");
		html.append(envioemail.getCdenvioemail());
		html.append("&email=");
		html.append(email);
		html.append("\" ");
		html.append("width=\"0\" ");
		html.append("height=\"0\" ");
		html.append("border=\"0\">");
		
		return html.toString();
	}
	
}