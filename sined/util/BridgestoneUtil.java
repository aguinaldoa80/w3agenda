package br.com.linkcom.sined.util;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;

public class BridgestoneUtil {
	
	/**
	* Retorna o caminho do diret�rio onde os arquivos para integra��o bridgestone ser�o gerados
	*
	* @param nomeCliente
	* @return
	*/
	public static String getDirBridgestone(String nomeCliente){
		String SEPARATOR = System.getProperty("file.separator");
		return ArquivoDAO.getInstance().getPathDir() + SEPARATOR + Neo.getApplicationName() + SEPARATOR + "bridgestone" + SEPARATOR + nomeCliente ;
	}

}
