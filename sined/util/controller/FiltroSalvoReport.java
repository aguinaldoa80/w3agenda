package br.com.linkcom.sined.util.controller;

import java.util.HashMap;
import java.util.Map;

import br.com.linkcom.sined.util.tag.PeriodoTagEnum;

public class FiltroSalvoReport implements IFiltroSalvo {

	private boolean exibirFiltroExpandido;
	private Map<String, PeriodoTagEnum> mapPeriodo = new HashMap<String, PeriodoTagEnum>();
	private String hashFiltro;
	
	@Override
	public boolean isExibirFiltroExpandido() {
		return exibirFiltroExpandido;
	}
	
	@Override
	public Map<String, PeriodoTagEnum> getMapPeriodo() {
		return mapPeriodo;
	}
	
	@Override
	public String getHashFiltro() {
		return hashFiltro;
	}
	
	@Override
	public void setExibirFiltroExpandido(boolean exibirFiltroExpandido) {
		this.exibirFiltroExpandido = exibirFiltroExpandido;
	}
	
	@Override
	public void setMapPeriodo(Map<String, PeriodoTagEnum> mapPeriodo) {
		this.mapPeriodo = mapPeriodo;
	}
	
	@Override
	public void setHashFiltro(String hashFiltro) {
		this.hashFiltro = hashFiltro;
	}

}
