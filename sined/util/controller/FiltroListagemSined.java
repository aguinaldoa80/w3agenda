package br.com.linkcom.sined.util.controller;

import java.util.HashMap;
import java.util.Map;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.tag.PeriodoTagEnum;

public class FiltroListagemSined extends FiltroListagem implements IFiltroSalvo {
	
	/**Indica se o filtro utilizado pelo usu�rio � simples ou avan�ado*/
	protected boolean filterSimple = true;
	private Boolean limparFiltro = Boolean.FALSE;
	private Boolean paginacaoSimples = false;
	
	//salvar filtro
	private boolean exibirFiltroExpandido;
	private Map<String, PeriodoTagEnum> mapPeriodo = new HashMap<String, PeriodoTagEnum>();
	private String hashFiltro;
	private Boolean podeVisualizarReportEmTela = false;
	
	public boolean isFilterSimple() {
		return filterSimple;
	}
	public Boolean getLimparFiltro() {
		return limparFiltro;
	}
	@MaxLength(9)
	@Override
	public int getPageSize() {
		return super.getPageSize();
	}
	public Boolean getPaginacaoSimples() {
		return paginacaoSimples;
	}
	
	public void setPaginacaoSimples(Boolean paginacaoSimples) {
		this.paginacaoSimples = paginacaoSimples;
	}
	public void setFilterSimple(boolean filterSimple) {
		this.filterSimple = filterSimple;
	}
	public void setLimparFiltro(Boolean limparFiltro) {
		this.limparFiltro = limparFiltro;
	}
	
	
	
	@Override
	public boolean isExibirFiltroExpandido() {
		return exibirFiltroExpandido;
	}
	
	@Override
	public void setExibirFiltroExpandido(boolean exibirFiltroExpandido) {
		this.exibirFiltroExpandido = exibirFiltroExpandido;
	}

	@Override
	public Map<String, PeriodoTagEnum> getMapPeriodo() {
		return mapPeriodo;
	}
	
	@Override
	public String getHashFiltro() {
		return hashFiltro;
	}
	
	@Override
	public void setMapPeriodo(Map<String, PeriodoTagEnum> mapPeriodo) {
		this.mapPeriodo = mapPeriodo;
	}
	
	@Override
	public void setHashFiltro(String hashFiltro) {
		this.hashFiltro = hashFiltro;
	}

	public Boolean getPodeVisualizarReportEmTela() {
		return podeVisualizarReportEmTela;
	}
	
	public void setPodeVisualizarReportEmTela(Boolean podeVisualizarReportEmTela) {
		this.podeVisualizarReportEmTela = podeVisualizarReportEmTela;
	}
	

}
