package br.com.linkcom.sined.util.controller;

import java.lang.reflect.Method;

import javax.persistence.Id;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;


public class SimpleCrudController<FORMBEAN, BEAN> extends br.com.linkcom.neo.controller.crud.SimpleCrudController<FORMBEAN, BEAN>{
	@Override
	protected void excluir(WebRequestContext request, BEAN bean) throws Exception {
		
		String itens = request.getParameter("itenstodelete");
		if(itens != null && !itens.equals("")){
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			Method[] methods = reflectionCache.getMethods(beanClass);
			Method methodId = null;
			for (Method method : methods) {
				if(reflectionCache.isAnnotationPresent(method, Id.class)){
					methodId = method;
					break;
				}
			}
			
			String propertyFromGetter = Util.beans.getPropertyFromGetter(methodId.getName());
			methodId = Util.beans.getSetterMethod(beanClass, propertyFromGetter);
			String[] codes = itens.split(",");
						
				for (String code : codes) {
					if(!"".equals(code) && code != null){
						BEAN obj = beanClass.newInstance();
						methodId.invoke(obj, Integer.parseInt(code));
						genericService.delete(obj);
					}
				}
			
		} else {
			super.excluir(request, bean);
		}
	}	
	protected void validateBean(FORMBEAN bean, BindException errors) {
		
	}
}
