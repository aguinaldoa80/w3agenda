package br.com.linkcom.sined.util.controller;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.ReportController;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Filtro;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SalvarFiltroUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.ReportFileServlet;

public abstract class SinedReport<FILTRO> extends ReportController<FILTRO> {
	protected ArquivoService arquivoService;
	protected EmpresaService empresaService;
	protected CentrocustoService centrocustoService;
	protected ProjetoService projetoService;
	protected ClienteService clienteService;
	protected FornecedorService fornecedorService;
	protected ParametrogeralService parametrogeralService;

	public ArquivoService getArquivoService() {
		return arquivoService;
	}
	public EmpresaService getEmpresaService() {
		return empresaService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public CentrocustoService getCentrocustoService() {
		return centrocustoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public ProjetoService getProjetoService() {
		return projetoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public ClienteService getClienteService() {
		return clienteService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public FornecedorService getFornecedorService() {
		return fornecedorService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public abstract IReport createReportSined(WebRequestContext request, FILTRO filtro) throws Exception;
	
	/**
	 * M�todo para adicionar o nome do arquivo do relat�rio que ser� gerado.
	 * 
	 * Obs: N�o precisa colocar a data formatada, somente o nome do relat�rio.
	 * 
	 * Se este m�todo retornar string vazia ou null, o nome do relat�rio ser� o nome da classe
	 * menos a palavra report.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public abstract String getNomeArquivo();

	@Override
	public IReport createReport(WebRequestContext request, FILTRO command) throws Exception {
		Report report = (Report) createReportSined(request, command);
		this.configurarParametros(request, command, report);
		return report;
	}

	@Override
	public Resource generateResource(WebRequestContext request, FILTRO filtro) throws Exception {
		IReport report = null;
		desbloquearTela(request);
		try {			
			report = createReport(request, filtro);
		} catch (SinedException e) {
			request.addError(e.getMessage());	
		}
		
		if (report != null){
	        String name = getReportName(report);
	        byte[] bytes = getReportBytes(report);
	        return getPdfResource(name, bytes);
		} else
			return null;		
	}

	protected void configurarParametros(WebRequestContext request, FILTRO command, Report report) {
		Empresa empresa = this.getEmpresa(command);

		if (report != null){
			empresaService.setInformacoesPropriedadeRural(empresa);
			
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
			report.addParameter("EMPRESA", empresa == null ? null : (empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa() != null ? empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa() : empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa()));
			report.addParameter("TITULO", this.getTitulo(command));
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			this.adicionaParametrosFiltro(report, command);
		}
	}

	

	/**
	 * Tenta encontrar um atributo "empresa" no filtro. Se for encontrado, pega o valor
	 * deste atributo. Caso n�o encontre, � feito um load na empresa marcada como
	 * principal no cadastro de empresas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadComArquivo(Empresa)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	protected Empresa getEmpresa(FILTRO filtro) {
		Empresa empresa = null;

		try {
			empresa = (Empresa) filtro.getClass().getDeclaredMethod("getEmpresa").invoke(filtro);
			empresa = empresaService.loadComArquivo(empresa);
		} catch (Exception e) {
			empresa = empresaService.loadPrincipal();
		}

		return empresa;
	}

	/**
	 * Este m�todo abstrato deve ser sobrescrito para informar o nome do relat�rio.
	 * 
	 * @param filtro 
	 * 			pode servir de suporte caso o relat�rio varie de t�tulo de acordo com
	 * 			algum par�metro do filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public abstract String getTitulo(FILTRO filtro);

	/**
	 * M�todo que tenta encontrar no filtro campos que devem ser mostrados no cabe�alho de relat�rios.
	 * Caso encontre, j� adiciona automaticamente o par�metro. Se n�o for encontrado o m�todo, abafa a
	 * exce��o e passa direto.
	 * <p>
	 * Caso seja sobrescrito, tomar o devido cuidado quanto � chamada ao m�todo da superclasse: se
	 * o m�todo da superclasse for chamado e em seguida um dos par�metros for repassado (por exemplo,
	 * voc� seta o par�metro 'centroCusto' em seu m�todo), este par�metro ser� sobrescrito com o valor
	 * que voc� colocou.
	 * </p>
	 * 
	 * <p>
	 * Se em seu relat�rio n�o for necess�rio verificar estes par�metros, � recomend�vel que voc�
	 * o sobrescreva, sem chamada � superclasse, para evitar processamento desnecess�rio.
	 * </p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.CentrocustoService#getNomeCentroCustos(List)
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#getNomeProjetos(List)
	 * @see br.com.linkcom.sined.geral.service.ClienteService#getNomeClientes(List)
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#getNomeFornecedores(List)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#getNomeEmpresas(List)
	 * @param report
	 * @param filtro
	 * @author Hugo Ferreira
	 */
	@SuppressWarnings("unchecked")
	protected void adicionaParametrosFiltro(Report report, FILTRO filtro) {
		try {
			report.addParameter("centroCusto", centrocustoService.getNomeCentroCustos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaCentrocusto").invoke(filtro)));
		} catch (Exception e) {}

		try {
			report.addParameter("projeto", projetoService.getNomeProjetos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaProjeto").invoke(filtro)));
		} catch (Exception e) {}
		try {
			report.addParameter("cliente", clienteService.getNomeClientes((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaCliente").invoke(filtro)));
		} catch (Exception e) {}

		try {
			report.addParameter("fornecedor", fornecedorService.getNomeFornecedores((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaFornecedor").invoke(filtro)));
		} catch (Exception e) {}

		try {
			report.addParameter("empresaFiltro", empresaService.getNomeEmpresas((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaEmpresa").invoke(filtro)));
		} catch (Exception e) {}

	}
	
	@Override
	protected String getReportName(IReport report) {
		String nomeArquivo = getNomeArquivo();
		if (nomeArquivo == null || "".equals(nomeArquivo)) {
			nomeArquivo = this.getClass().getSimpleName().toLowerCase().replace("report", "");
		}
		return nomeArquivo + "_" + SinedUtil.datePatternForReport() + ".pdf";
	}
	
	public boolean isRelatorioExterno(){
		return false;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, FILTRO filtro) throws Exception {
		if(request.getSession().getAttribute(Parametrogeral.GERAR_RELATORIO_PDF) == null){
			String gerarRelatorioPdf = parametrogeralService.buscaValorPorNome(Parametrogeral.GERAR_RELATORIO_PDF);
			if(gerarRelatorioPdf == null || "true".equalsIgnoreCase(gerarRelatorioPdf.trim()))
				request.getSession().setAttribute(Parametrogeral.GERAR_RELATORIO_PDF, true);
			else
				request.getSession().setAttribute(Parametrogeral.GERAR_RELATORIO_PDF, false);
		}
		
		if((Boolean)request.getSession().getAttribute(Parametrogeral.GERAR_RELATORIO_PDF) || isRelatorioExterno()){
			ModelAndView modelAndView = super.doGerar(request, filtro);
			if(modelAndView == null){
				request.getSession().removeAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase());
			}
			
			return modelAndView;			
		}else{
			Resource recurso = generateResource(request, filtro);
			
			if(recurso==null){
				return goToAction(FILTRO);				
			} else if(recurso.getContentType().equals("application/pdf")) {
				String key = new BigInteger(130, new SecureRandom()).toString(32);
				String nameResource = recurso.getFileName();
				if(nameResource.indexOf(".") != -1){
					nameResource = nameResource.substring(0, nameResource.indexOf("."));
				}
				nameResource += "_";
				ReportFileServlet.add(nameResource + key, recurso);
				
				
				request.setAttribute("titulo", getTitulo(filtro));
				request.setAttribute("nomearquivodownloadw3erp", nameResource);
				request.setAttribute("reportFileURL", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+key+"."+recurso.getContentType().split("/")[1]);
				request.setAttribute("reportFileURLIframe", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+nameResource+key+"."+recurso.getContentType().split("/")[1]);
				request.setAttribute("isHttps", SinedUtil.getUrlWithContext().indexOf("https") != -1);
				
				String modulo = (String) NeoWeb.getRequestContext().getAttribute("NEO_MODULO");
				if(modulo != null && modulo.equals("pub")){
					return new ModelAndView("direct:relatorio/relatorioPreview");
				} else {
					return new ModelAndView("relatorio/relatorioPreview");
				}
			} else {
				ModelAndView modelAndView = super.doGerar(request, filtro);
				if(modelAndView == null){
					request.getSession().removeAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase());
				}
				
				return modelAndView;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ModelAndView doFiltro(WebRequestContext request, FILTRO filtro) throws ResourceGenerationException {
		if(request.getSession().getAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase()) == null){
			request.getSession().setAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase(), Boolean.TRUE);
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class))
					try{
						methodSetEmpresa.invoke(filtro, empresa);
					}catch(Exception e){
						throw new ResourceGenerationException("setEmpresa", e);
					}
			}
		}
		if(request.getSession().getAttribute("android").toString().equals("true")){
			request.setAttribute("isIncludeAndroidHack", true);
		}
		
		if(isSalvarFiltro()){
			if("true".equals(request.getParameter(MultiActionController.CLEAR_FILTER))){
				try {
					filtro = (FILTRO) filtro.getClass().newInstance();
					SalvarFiltroUtil.removeSalvarFiltroSessao(request);
					SalvarFiltroUtil.removeFiltroSalvoSessao(request, this.getSessionCommandName(this.getClass()));
				} catch (Exception e) {
					throw new SinedException("N�o foi poss�vel instanciar um objeto da classe " + filtro.getClass());
				}
				
			}
			SalvarFiltroUtil.configureRequestForListagem(request, this.getSessionCommandName(this.getClass()));
		}
		
		return super.doFiltro(request, filtro);
	}
	
	/**
	 * Desbloqueia a tela do relat�rio, setando o cookie 'blockbutton' para false
	 * @author igorcosta
	 * @param request
	 */
	protected void desbloquearTela(WebRequestContext request) {
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
	}
	
	
	
	/**
	 * FILTRO SALVO
	 */
	
	@Override
	protected ServletRequestDataBinder bind(WebRequestContext request, Object command, boolean validate) throws Exception {
		String acao = request.getParameter(CrudControllerSined.ACTION_PARAMETER);
		boolean isFiltro = StringUtils.isEmpty(acao) || acao.equalsIgnoreCase("filtro");
		String sessionCommandName = this.getSessionCommandName(this.getClass());

		/**
		 * Se est� usando filtro salvo, n�o deve fazer o binding normal e sim pelo filtro salvo.
		 */
		ServletRequestDataBinder binder;
		if(!BooleanUtils.toBoolean(request.getParameter("usarFiltroSalvo"))){
			binder = super.bind(request, command, validate);
		}else{
			binder = createBinder(request.getServletRequest(), command, getCommandName(command));
		}
		
//		ServletRequestDataBinder binder = super.bind(request, command, validate);
		if(isFiltro){
//			command = command.getClass().newInstance();
			SalvarFiltroUtil.bindingFiltroSalvo(request, binder, command, sessionCommandName);
		}
		return binder;
	}
	
	/**
     * Usa Salvar filtro
     */
	public boolean isSalvarFiltro(){
		return false;
	}
	
	
	/**
	 * M�todo utilizado no momento de salvamento do filtro.
	 * 
	 * @param request
	 * @param filtro
	 * @author igorsilveriocosta
	 */
	public void setaFiltroSessao(WebRequestContext request, FILTRO filtro){
		SalvarFiltroUtil.setSalvarFiltroSessao(request, filtro);
	}
	
	/**
	 * M�todo utilizado no momento de cancelamento do salvamento do filtro.
	 * 
	 * @param request
	 * @param filtro
	 */
	public void removeFiltroSessao(WebRequestContext request, FILTRO filtro){
		SalvarFiltroUtil.removeSalvarFiltroSessao(request);
	}

	/**
	 * M�todo que invoca a modal de salvamento/edi��o do filtro.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 */
	public ModelAndView doModalSalvarFiltro(WebRequestContext request, Filtro filtroBean){
		return SalvarFiltroUtil.doModalSalvarFiltro(request, filtroBean);
	}
	
	/**
	 * M�todo que invoca a modal de salvamento/edi��o de um filtro novo.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 */
	public ModelAndView doModalSalvarFiltroNovo(WebRequestContext request, Filtro filtroBean){
		return SalvarFiltroUtil.doModalSalvarFiltro(request, filtroBean);
	}
	
	/**
	 * M�todo que salva o filtro e retorna � p�gina de listagem.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView doSalvarFiltro(WebRequestContext request, Filtro filtroBean) throws Exception{
		Filtro filtroBeanSessao = SalvarFiltroUtil.doSalvarFiltro(request, this.getSessionCommandName(this.getClass()), filtroBean);
		
		//Fazendo o binding do Filtro
		FILTRO filtro = (FILTRO) filtroBeanSessao.getClasseFiltro();
		bind(request, filtro, true);
		request.addMessage("Filtro \""+filtroBean.getNome()+"\" salvo com sucesso.");

		return sendRedirectToAction(null);
	}	
	
	/**
	 * M�todo que remove o filtro selecionado.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 * @throws Exception
	 */
	public ModelAndView doExcluirFiltro(WebRequestContext request, FILTRO filtro) throws CrudException{
		return SalvarFiltroUtil.doExcluirFiltro(request, this.getSessionCommandName(this.getClass()));
	}
	
}