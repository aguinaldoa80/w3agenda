package br.com.linkcom.sined.util.controller;

import java.util.Map;

import br.com.linkcom.sined.util.tag.PeriodoTagEnum;

public interface IFiltroSalvo {

	public abstract boolean isExibirFiltroExpandido();

	public abstract Map<String, PeriodoTagEnum> getMapPeriodo();

	public abstract String getHashFiltro();
	
	public abstract void setExibirFiltroExpandido(boolean exibirFiltroExpandido);

	public abstract void setMapPeriodo(Map<String, PeriodoTagEnum> mapPeriodo);

	public abstract void setHashFiltro(String hashFiltro);

}