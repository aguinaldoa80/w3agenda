package br.com.linkcom.sined.util.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.usermodel.Font; // ???

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.util.SinedException;

public class SinedExcel extends HSSFWorkbook {

	public static HSSFCellStyle STYLE_HEADER_RELATORIO;
	public static HSSFCellStyle STYLE_HEADER_DATAGRID_BLUE_CENTER;
	public static HSSFCellStyle STYLE_HEADER_DATAGRID_BLUE_LEFT;
	public static HSSFCellStyle STYLE_HEADER_DATAGRID_GREY_CENTER;
	public static HSSFCellStyle STYLE_HEADER_DATAGRID_GREY_LEFT;
	public static HSSFCellStyle STYLE_DETALHE_LEFT_WHITE;
	public static HSSFCellStyle STYLE_DETALHE_LEFT_GREY;
	public static HSSFCellStyle STYLE_DETALHE_RIGHT_GREY;
	public static HSSFCellStyle STYLE_DETALHE_RIGHT_GREY_PERCENT;
	public static HSSFCellStyle STYLE_DETALHE_RIGHT_GREY_MONEY;
	public static HSSFCellStyle STYLE_DETALHE_RIGHT_YELLOW_MONEY;
	public static HSSFCellStyle STYLE_DETALHE_CENTER_GREY;
	public static HSSFCellStyle STYLE_DETALHE_CENTER_WHITE;
	public static HSSFCellStyle STYLE_DETALHE_RIGHT_WHITE;
	public static HSSFCellStyle STYLE_DETALHE_MONEY;
	public static HSSFCellStyle STYLE_DETALHE_PERCENT;
	public static HSSFCellStyle STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT;
	public static HSSFCellStyle STYLE_TOTAL_RIGHT_WITHOUT_BORDERLEFT;
	public static HSSFCellStyle STYLE_TOTAL_RIGHT;
	public static HSSFCellStyle STYLE_TOTAL_CENTER;
	public static HSSFCellStyle STYLE_TOTAL_LEFT;
	public static HSSFCellStyle STYLE_TOTAL_MONEY;
	public static HSSFCellStyle STYLE_TOTAL_MONEY_FONT12;
	public static HSSFCellStyle STYLE_TOTAL_MONEY_FONT_RED;
	public static HSSFCellStyle STYLE_TOTAL_MONEY_GREY;
	public static HSSFCellStyle STYLE_TOTAL_PERCENT;
	public static HSSFCellStyle STYLE_FILTRO_BORDER;
	public static HSSFCellStyle STYLE_FILTRO;
	public static HSSFCellStyle STYLE_RESUMO;
	public static HSSFCellStyle STYLE_CENTER;
	public static HSSFCellStyle STYLE_LEFT;
	public static HSSFCellStyle STYLE_LEFT_TOP;
	public static HSSFCellStyle STYLE_HORICENTER_VERTCENTER;
	public static HSSFCellStyle STYLE_TOTAL_MONEY_WHITE;
	public static HSSFCellStyle STYLE_HEADER_RELATORIO_WHITE;

	private HSSFDataFormat DATA_FORMAT_MONEY;
	private HSSFDataFormat DATA_FORMAT_PERCENT;
	
	public static short FORMAT_MONEY;
	public static short FORMAT_PERCENT;

	private HSSFFont FONT_HEADER_RELATORIO;
	private HSSFFont FONT_HEADER_DATAGRID;
	private HSSFFont FONT_DETALHE;
	private HSSFFont FONT_DETALHE_BOLD;
	private HSSFFont FONT_TOTAL;
	private HSSFFont FONT_FILTRO;
	private HSSFFont FONT_RED;
	
	private boolean init = false;
	
	public SinedExcel(){
		if (!init) {
			init();
			init = true;
		}
	}
	
	/**
	 * Inicializa a biblioteca do Sined para Excel.
	 *
	 * @author Rodrigo Freitas
	 */
	private void init(){
		this.initializeCustomColors();
		this.initializeFormatter();
		this.initializeFonts();
		this.initializeStyles();
	}
	
	/**
	 * Inicializa os formatadores.
	 *
	 * @author Rodrigo Freitas
	 */
	private void initializeFormatter() {
		FORMAT_MONEY = generateDataFormatMoney();
		FORMAT_PERCENT = generateDataFormatPercent();
	}

	/**
	 * Inicializa as fontes.
	 *
	 * @author Rodrigo Freitas
	 */
	private void initializeFonts(){
		FONT_HEADER_RELATORIO = generateFontBoldArial(17);
		FONT_HEADER_DATAGRID = generateFontBoldArial(12);
		FONT_DETALHE = generateFontDetalhe();
		FONT_TOTAL = generateFontBoldArial(10);
		FONT_FILTRO = generateFontBoldArial(12);
		FONT_DETALHE_BOLD = generateFontDetalheBold();
		FONT_RED = generateFontBoldArialRed(10);
	}
	
	/**
	 * Gera uma fonte em negrito de tamanho vari�vel, do tipo 'ARIAL'.
	 *
	 * @param tamanho
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFFont generateFontBoldArial(int tamanho) {
		HSSFFont font = this.createFont();
		font.setFontHeightInPoints((short) tamanho);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName("Arial");
		
		return font;
	}
	
	/**
	 * Gera uma fonte em negrito de tamanho vari�vel, do tipo 'ARIAL' de cor vermelha.
	 *
	 * @param tamanho
	 * @return
	 * @author Eduardo Marun
	 */
	private HSSFFont generateFontBoldArialRed(int tamanho) {
		HSSFFont font = this.createFont();
		font.setFontHeightInPoints((short) tamanho);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName("Arial");
		font.setColor(Font.COLOR_RED);
		return font;
	}
	

	/**
	 * Gera uma fonte 'ARIAL' de tamanho 10.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFFont generateFontDetalhe() {
		HSSFFont font = this.createFont();
		font.setFontHeightInPoints((short) 10);
		font.setFontName("Arial");
		
		return font;
	}
	
	/**
	 * Gera uma fonte 'ARIAL' de tamanho 10 e negrito.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFFont generateFontDetalheBold() {
		HSSFFont font = this.createFont();
		font.setFontHeightInPoints((short) 10);
		font.setFontName("Arial");
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		
		return font;
	}
	
	/**
	 * Inicializa os estilos.
	 *
	 * @author Rodrigo Freitas
	 */
	private void initializeStyles(){
		STYLE_HEADER_RELATORIO = generateStyleHeader(FONT_HEADER_RELATORIO, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
		STYLE_HEADER_RELATORIO_WHITE = generateStyleHeader(FONT_HEADER_RELATORIO, HSSFColor.WHITE.index, HSSFCellStyle.ALIGN_CENTER);
		STYLE_HEADER_DATAGRID_BLUE_CENTER = generateStyleHeader(FONT_HEADER_DATAGRID, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
		STYLE_HEADER_DATAGRID_BLUE_LEFT = generateStyleHeader(FONT_HEADER_DATAGRID, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_LEFT);
		STYLE_HEADER_DATAGRID_GREY_CENTER = generateStyleHeader(FONT_HEADER_DATAGRID, HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
		STYLE_HEADER_DATAGRID_GREY_LEFT = generateStyleHeader(FONT_HEADER_DATAGRID, HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_LEFT);
		STYLE_DETALHE_LEFT_WHITE = generateStyleDetalhe(HSSFCellStyle.ALIGN_LEFT, HSSFColor.WHITE.index, false);
		STYLE_DETALHE_LEFT_GREY = generateStyleDetalhe(HSSFCellStyle.ALIGN_LEFT, HSSFColor.GREY_25_PERCENT.index, true);
		STYLE_DETALHE_CENTER_WHITE = generateStyleDetalhe(HSSFCellStyle.ALIGN_CENTER, HSSFColor.WHITE.index, false);
		STYLE_DETALHE_CENTER_GREY = generateStyleDetalhe(HSSFCellStyle.ALIGN_CENTER, HSSFColor.GREY_25_PERCENT.index, true);
		STYLE_DETALHE_RIGHT_GREY_PERCENT = generateStyleDetalhePercent(HSSFCellStyle.ALIGN_RIGHT, HSSFColor.GREY_25_PERCENT.index, true);
		STYLE_DETALHE_RIGHT_GREY_MONEY = generateStyleDetalheMoney(HSSFCellStyle.ALIGN_RIGHT, HSSFColor.GREY_25_PERCENT.index, true);
		STYLE_DETALHE_RIGHT_YELLOW_MONEY = generateStyleDetalheMoney(HSSFCellStyle.ALIGN_RIGHT, HSSFColor.YELLOW.index, true);
		STYLE_DETALHE_RIGHT_GREY = generateStyleDetalhe(HSSFCellStyle.ALIGN_RIGHT, HSSFColor.GREY_25_PERCENT.index, true);
		STYLE_DETALHE_RIGHT_WHITE = generateStyleDetalhe(HSSFCellStyle.ALIGN_RIGHT, HSSFColor.WHITE.index, false);
		STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT = generateStyleTotal(HSSFCellStyle.ALIGN_LEFT, 1, 0);
		STYLE_TOTAL_RIGHT_WITHOUT_BORDERLEFT = generateStyleTotal(HSSFCellStyle.ALIGN_RIGHT, 0, 1);
		STYLE_TOTAL_LEFT = generateStyleTotal(HSSFCellStyle.ALIGN_LEFT, 1, 1);
		STYLE_TOTAL_CENTER = generateStyleTotal(HSSFCellStyle.ALIGN_CENTER, 1, 1);
		STYLE_TOTAL_RIGHT = generateStyleTotal(HSSFCellStyle.ALIGN_RIGHT, 1, 1);
		STYLE_FILTRO = generateStyleFiltro(false);
		STYLE_FILTRO_BORDER = generateStyleFiltro(true);
		STYLE_RESUMO = generateStyleResumo();
		STYLE_DETALHE_MONEY = generateStyleDetalheMoney();
		STYLE_DETALHE_PERCENT = generateStyleDetalhePercent();
		STYLE_TOTAL_MONEY = generateStyleTotalMoney();
		STYLE_TOTAL_MONEY_FONT12 = generateStyleTotalMoneyFont12();
		STYLE_TOTAL_MONEY_FONT_RED = generateStyleTotalMoneyRedFont();
		STYLE_TOTAL_PERCENT = generateStyleTotalPercent();
		STYLE_CENTER = generateStyle(HSSFCellStyle.ALIGN_CENTER, 1, 1);
		STYLE_LEFT = generateStyle(HSSFCellStyle.ALIGN_LEFT, 1, 1);
		STYLE_LEFT_TOP = generateStyle(HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.VERTICAL_TOP, 1, 1);
		STYLE_HORICENTER_VERTCENTER = generateStyle(HSSFCellStyle.ALIGN_CENTER, HSSFCellStyle.VERTICAL_CENTER, 1, 1);
		STYLE_TOTAL_MONEY_WHITE = generateStyleTotalMoneyWhite();
		STYLE_TOTAL_MONEY_GREY = generateStyleTotalMoneyGrey();
	}

	/**
	 * Gera um estilo de c�lula para dados do filtro.
	 * 
	 * Align: Left
	 * ForegroundColor: White
	 * Fonte: <code>FONT_FILTRO</code>
	 * VerticalAlign: Center
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleFiltro(Boolean border) {
		HSSFCellStyle style = this.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		style.setFillForegroundColor(HSSFColor.WHITE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setFont(FONT_FILTRO);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		if(border){
			style.setBorderTop((short) 1);
			style.setBorderBottom((short) 1);
			style.setBorderRight((short) 1);
			style.setBorderLeft((short) 1);
		}
		
		return style;
	}
	
	/**
	 * Gera um estilo de c�lula para dados do resumo do relat�rio.
	 * 
	 * Align: Left
	 * ForegroundColor: White
	 * Fonte: <code>FONT_DETALHE</code>
	 * VerticalAlign: Center
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleResumo() {
		HSSFCellStyle style = this.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		style.setFillForegroundColor(HSSFColor.WHITE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setFont(FONT_DETALHE);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		return style;
	}

	/**
	 * Gera o estilo de c�lula para dados totalizados.
	 *
	 * ForegroundColor: Blue
	 * Fonte: <code>FONT_TOTAL</code>
	 *
	 * @param align
	 * @param borderLeft
	 * @param borderRight
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleTotal(short align, int borderLeft, int borderRight) {
		HSSFCellStyle style = this.createCellStyle();
		style.setAlignment(align);
		style.setBorderTop((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderRight((short) borderRight);
		style.setBorderLeft((short) borderLeft);
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setFont(FONT_TOTAL);
		
		return style;
	}

	/**
	 * Gera o estilo de c�lula para dados simples.
	 *
	 * @param align
	 * @param borderLeft
	 * @param borderRight
	 * @return
	 * @author Tom�s Rabelo
	 */
	private HSSFCellStyle generateStyle(short align, int borderLeft, int borderRight) {
		HSSFCellStyle style = this.createCellStyle();
		style.setAlignment(align);
		style.setBorderTop((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderRight((short) borderRight);
		style.setBorderLeft((short) borderLeft);
		style.setFont(FONT_DETALHE);
		
		return style;
	}
	
	/**
	 * Gera o estilo de c�lula para dados simples.
	 *
	 * @param align
	 * @param borderLeft
	 * @param borderRight
	 * @return
	 * @author Tom�s Rabelo
	 */
	private HSSFCellStyle generateStyle(short align, short valign, int borderLeft, int borderRight) {
		HSSFCellStyle style = this.createCellStyle();
		style.setAlignment(align);
		style.setVerticalAlignment(valign);
		style.setBorderTop((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderRight((short) borderRight);
		style.setBorderLeft((short) borderLeft);
		style.setFont(FONT_DETALHE);
		
		return style;
	}
	
	/**
	 * Gera o estilo de c�lula para o totalizador do tipo money.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleTotalMoney() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 0);
		styleFooterRight.setFillForegroundColor(HSSFColor.BLUE.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_TOTAL);
		styleFooterRight.setDataFormat(FORMAT_MONEY);
		
		return styleFooterRight;
	}
	
	/**
	 * Gera o estilo de c�lula para o totalizador do tipo money.
	 *
	 * @return
	 * @since 19/09/2016
	 * @author C�sar Augusto
	 */
	private HSSFCellStyle generateStyleTotalMoneyFont12() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 0);
		styleFooterRight.setFillForegroundColor(HSSFColor.BLUE.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_FILTRO);
		styleFooterRight.setDataFormat(FORMAT_MONEY);
		
		return styleFooterRight;
	}
	
	/**
	 * Gera o estilo de c�lula para o totalizador do tipo money com a fonte vermelha.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleTotalMoneyRedFont() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 0);
		styleFooterRight.setFillForegroundColor(HSSFColor.BLUE.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_RED);
		styleFooterRight.setDataFormat(FORMAT_MONEY);
		
		return styleFooterRight;
	}
	
	/**
	 * Gera o estilo de c�lula para o totalizador do tipo money com cor de fundo branca.
	 *
	 * @return
	 * @author Jo�o Vitor
	 */
	private HSSFCellStyle generateStyleTotalMoneyWhite() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 1);
		styleFooterRight.setFillForegroundColor(HSSFColor.WHITE.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_TOTAL);
		styleFooterRight.setDataFormat(FORMAT_MONEY);
		
		return styleFooterRight;
	}
	
	private HSSFCellStyle generateStyleTotalMoneyGrey() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 1);
		styleFooterRight.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_TOTAL);
		styleFooterRight.setDataFormat(FORMAT_MONEY);
		
		return styleFooterRight;
	}
	
	/**
	 * Gera o estilo de c�lula para o totalizador do tipo percentual.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleTotalPercent() {
		HSSFCellStyle styleFooterRight = this.createCellStyle();
		styleFooterRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleFooterRight.setBorderTop((short) 1);
		styleFooterRight.setBorderBottom((short) 1);
		styleFooterRight.setBorderRight((short) 1);
		styleFooterRight.setBorderLeft((short) 0);
		styleFooterRight.setFillForegroundColor(HSSFColor.BLUE.index);
		styleFooterRight.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleFooterRight.setFont(FONT_TOTAL);
		styleFooterRight.setDataFormat(FORMAT_PERCENT);
		
		return styleFooterRight;
	}

	/**
	 * Gera o estilo de c�lula para o detalhe do datagrid.
	 * 
	 * ForegroundColor: White
	 * Font: <code>FONT_DETALHE</code>
	 *
	 * @param alignLeft
	 * @return
	 * @author Rodrigo Freitas
	 * @param color 
	 * @param negrito 
	 */
	private HSSFCellStyle generateStyleDetalhe(short alignLeft, short color, boolean negrito) {
		HSSFCellStyle styleCenter = this.createCellStyle();
		styleCenter.setAlignment(alignLeft);
		styleCenter.setBorderTop((short) 1);
		styleCenter.setBorderBottom((short) 1);
		styleCenter.setBorderLeft((short) 1);
		styleCenter.setBorderRight((short) 1);
		styleCenter.setFillForegroundColor(color);
		styleCenter.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleCenter.setFont(negrito ? FONT_DETALHE_BOLD : FONT_DETALHE);
		styleCenter.setWrapText(true);

		return styleCenter;
	}
	
	/**
	 * Gera o estilo para detalhe do tipo money.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleDetalheMoney() {
		HSSFCellStyle styleCenter = this.createCellStyle();
		styleCenter.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleCenter.setBorderTop((short) 1);
		styleCenter.setBorderBottom((short) 1);
		styleCenter.setBorderLeft((short) 1);
		styleCenter.setBorderRight((short) 1);
		styleCenter.setFillForegroundColor(HSSFColor.WHITE.index);
		styleCenter.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleCenter.setFont(FONT_DETALHE);
		styleCenter.setDataFormat(FORMAT_MONEY);
		
		return styleCenter;
	}
	
	/**
	 * Gera o estilo para detalhe do tipo percentual.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private HSSFCellStyle generateStyleDetalhePercent() {
		HSSFCellStyle styleCenter = this.createCellStyle();
		styleCenter.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		styleCenter.setBorderTop((short) 1);
		styleCenter.setBorderBottom((short) 1);
		styleCenter.setBorderLeft((short) 1);
		styleCenter.setBorderRight((short) 1);
		styleCenter.setFillForegroundColor(HSSFColor.WHITE.index);
		styleCenter.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleCenter.setFont(FONT_DETALHE);
		styleCenter.setDataFormat(FORMAT_PERCENT);
		
		return styleCenter;
	}

	private HSSFCellStyle generateStyleDetalhePercent(short align, short color, boolean negrito) {
		HSSFCellStyle styleCenter = this.createCellStyle();
		styleCenter.setAlignment(align);
		styleCenter.setBorderTop((short) 1);
		styleCenter.setBorderBottom((short) 1);
		styleCenter.setBorderLeft((short) 1);
		styleCenter.setBorderRight((short) 1);
		styleCenter.setFillForegroundColor(color);
		styleCenter.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleCenter.setFont(negrito ? FONT_DETALHE_BOLD : FONT_DETALHE);
		styleCenter.setDataFormat(FORMAT_PERCENT);
		styleCenter.setWrapText(true);
		
		return styleCenter;
	}

	private HSSFCellStyle generateStyleDetalheMoney(short align, short color, boolean negrito) {
		HSSFCellStyle styleCenter = this.createCellStyle();
		styleCenter.setAlignment(align);
		styleCenter.setBorderTop((short) 1);
		styleCenter.setBorderBottom((short) 1);
		styleCenter.setBorderLeft((short) 1);
		styleCenter.setBorderRight((short) 1);
		styleCenter.setFillForegroundColor(color);
		styleCenter.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleCenter.setFont(negrito ? FONT_DETALHE_BOLD : FONT_DETALHE);	
		styleCenter.setDataFormat(FORMAT_MONEY);
		styleCenter.setWrapText(true);
		
		return styleCenter;
	}

	/**
	 * Gera o estilo para o cabe�alho.
	 *
	 * @param font
	 * @return
	 * @author Rodrigo Freitas
	 * @param s 
	 * @param t 
	 */
	private HSSFCellStyle generateStyleHeader(HSSFFont font, short color, short align) {
		HSSFCellStyle styleTop = this.createCellStyle();
		styleTop.setAlignment(align);
		styleTop.setBorderTop((short) 1);
		styleTop.setBorderBottom((short) 1);
		styleTop.setBorderLeft((short) 1);
		styleTop.setBorderRight((short) 1);
		styleTop.setFillForegroundColor(color);
		styleTop.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleTop.setFont(font);
		styleTop.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		styleTop.setWrapText(true);
		
		return styleTop;
	}

	/**
	 * Inicializa as cores da palheta da planilha.
	 *
	 * @author Rodrigo Freitas
	 */
	private void initializeCustomColors(){
//		SUBSTITUI O AZUL POR AZUL CLARO
		this.getCustomPalette().setColorAtIndex(HSSFColor.BLUE.index, (byte) 197, (byte) 217, (byte) 241);
//		SUBSTITUI O CINZA POR CINZA MAIS CLARO
		this.getCustomPalette().setColorAtIndex(HSSFColor.GREY_25_PERCENT.index, (byte) 215, (byte) 215, (byte) 215);
//		SUBSTITUI O AMARELO POR AMARELO MAIS CLARO
		this.getCustomPalette().setColorAtIndex(HSSFColor.YELLOW.index, (byte) 242, (byte) 240, (byte) 136);
	}
	
	/**
	 * Adiciona cabe�alho padr�o do Sined � planilha.
	 * Obs: se utilizar este m�todo come�ar a criar linhas na planilha a partir da fileira 4.
	 * 
	 * @param sheetName
	 * @param headerTitle
	 * @author Tom�s Rabelo
	 */
	public void addSinedHeader(String sheetName, String headerTitle) {
		HSSFSheet planilha = this.getSheet(sheetName);
		if(planilha == null)
			throw new SinedException("A planilha "+sheetName+" n�o existe no workbook.");

		HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) 3));
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue(headerTitle);
	}

	/**
	 * Gera o formatador para percentual.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private short generateDataFormatPercent() {
		DATA_FORMAT_PERCENT = this.createDataFormat();
		return DATA_FORMAT_PERCENT.getFormat("0.00%");
	}
	
	/**
	 * Gera o formatador para money.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private short generateDataFormatMoney() {
		DATA_FORMAT_MONEY = this.createDataFormat();
		return DATA_FORMAT_MONEY.getFormat("R$ #,##0.00");
	}

	
	/**
	 * M�todo que retorna Resource. Este resource � para arquivos Excel 97-2003
	 * 
	 * @param name
	 * @return
	 * @throws IOException
	 * @author Tom�s Rabelo
	 */
	public Resource getWorkBookResource(String name) throws IOException{
		ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
		this.write(byteArray);
		
		Resource resource = new Resource();
        resource.setContentType("application/msexcel");
        resource.setFileName(name);
        resource.setContents(byteArray.toByteArray());
		return resource;
	}
	
	/**
	 * Passa-se o �ndice da coluna e retorna o algarismo correspondente 
	 * ex: 0 -> A, 26 -> AA
	 * 
	 * @param i
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String getAlgarismoColuna(int i) {
//		int length = i/26.0 >= 1 ? i/26+1 : 1;
//		int[] caracteres = new int[length];
//		
//		String algarismos = "";
//		for (int j = 0; j < caracteres.length; j++) {
//			int valorChar = 0;
//			if(i >= 26){
//				valorChar = 0;
//				i -= 26;
//			}
//			else{
//				valorChar = i;
//				i -= valorChar;
//			}
//			
//			Character a = new Character((char) (valorChar+65));
//			algarismos += a.charValue();
//		}
//		
//		return algarismos;
		int multiplicador = new Double(i/26).intValue(); 
		int length = multiplicador+1;
		Character[] caracteres = new Character[length];
		String algarismos = "";
		
		for (int j = 0; j < length; j++) {
			int valorChar = 0;
			if(i < 26){
				valorChar = i;
			}else {
				valorChar = i/26-1;
			}
			i = i-(multiplicador*26);
			
			Character a = new Character((char) (valorChar+65));
			caracteres[j] = a;
			
			if(i < 0) break;
		}
		
		for(Character c : caracteres){
			if(c != null && !"".equals(c))
				algarismos += c;
		}
		
		return algarismos;
	}

	public static void createHeaderCell(HSSFRow row, int i, String value) {
		HSSFCell cell = row.createCell((short) (i));
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue(value);
	}
	
}