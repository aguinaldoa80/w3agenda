package br.com.linkcom.sined.util.controller;

import java.lang.reflect.Method;
import java.util.List;

import javax.persistence.Id;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Filtro;
import br.com.linkcom.sined.geral.service.CampoobrigatorioService;
import br.com.linkcom.sined.util.SalvarFiltroUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.ValidacaoCampoObrigatorio;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;
import br.com.linkcom.sined.util.tag.TagFunctions;

public class CrudControllerSined<FILTRO extends FiltroListagem, FORMBEAN, BEAN> extends br.com.linkcom.neo.controller.crud.CrudController<FILTRO, FORMBEAN, BEAN>{

	@Override
	protected ServletRequestDataBinder bind(WebRequestContext request, Object command, boolean validate) throws Exception {
		
		String acao = request.getParameter(CrudControllerSined.ACTION_PARAMETER);
		boolean isListagem = StringUtils.isEmpty(acao) || acao.equalsIgnoreCase(CrudControllerSined.LISTAGEM);
		String sessionCommandName = this.getSessionCommandName(this.getClass());
		
		/**
		 * Se est� usando filtro salvo, n�o deve fazer o binding normal e sim pelo filtro salvo.
		 */
		ServletRequestDataBinder binder;
		if(!BooleanUtils.toBoolean(request.getParameter("usarFiltroSalvo"))){
			binder = super.bind(request, command, validate);
		}else{
			binder = createBinder(request.getServletRequest(), command, getCommandName(command));
		}
		
		if(isListagem){
			SalvarFiltroUtil.bindingFiltroSalvo(request, binder, command, sessionCommandName);
		}
		return binder;
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,FORMBEAN form) {
		if (request.getBindException().hasErrors()) request.setAttribute("ACAO", "salvar");
		return new ModelAndView("crud/"+getBeanName()+"Entrada");
	}
	
	@Override
	protected void setInfoForTemplate(WebRequestContext request) {
		super.setInfoForTemplate(request);
		request.setAttribute("TEMPLATE_requestQuery", request.getRequestQuery());
	}

	@Override
	protected void excluir(WebRequestContext request, BEAN bean) throws Exception {
		String itens = request.getParameter("itenstodelete");
		String telaDescricao;
		if (beanClass.getAnnotation(DisplayName.class)!=null){
			DisplayName displayName = beanClass.getAnnotation(DisplayName.class);
			telaDescricao = displayName.value();
		}else {
			telaDescricao = TagFunctions.getTelaDescricao();
		}
		
		if(itens != null && !itens.equals("")){
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			Method[] methods = reflectionCache.getMethods(beanClass);
			Method methodId = null;
			for (Method method : methods) {
				if(reflectionCache.isAnnotationPresent(method, Id.class)){
					methodId = method;
					break;
				}
			}

			String propertyFromGetter = Util.beans.getPropertyFromGetter(methodId.getName());
			methodId = Util.beans.getSetterMethod(beanClass, propertyFromGetter);
			String[] codes = itens.split(",");
			for (String code : codes) {
				if(!"".equals(code) && code != null){
					BEAN obj = beanClass.newInstance();
					try{
						methodId.invoke(obj, Integer.parseInt(code));
					} catch (NumberFormatException e) {
						methodId.invoke(obj, code);
					}
					if(validaExcluirEmMassa(request, obj))
						try {
							genericService.delete(obj);
						} catch (DataAccessException da) {
							throw new SinedException(telaDescricao + " n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
						}
				}
			}

		} else {
			try {
				super.excluir(request, bean);
			} catch (DataAccessException da) {
				throw new SinedException(telaDescricao + " n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
			}	
		}
		if(!"true".equals(request.getParameter("fromInsertOne"))){
			request.clearMessages();
			request.addMessage("Registro(s) exclu�do(s) com sucesso.");
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, FORMBEAN form) throws CrudException {
		if(request.getAttribute(CONSULTAR) == null){
			request.setAttribute(CONSULTAR, false);
		}
		if("true".equals(request.getParameter("forcarConsulta"))){
			request.setAttribute(CONSULTAR, true);
		}
		try {
			BEAN bean = formToBean(form);
			bean = carregar(request, bean);

			if(bean == null){
				request.addError("Registro n�o encontrado.");
				return sendRedirectToAction("listagem");
			}
			
			form = beanToForm(bean);
		} catch (Exception e) {
			throw new CrudException(EDITAR, e);
		}
		
		return getEditarModelAndView(request, form);
	}

	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, BEAN bean) {
		if("true".equals(request.getParameter("fromInsertOne"))){
			try {
				bean = super.carregar(request, bean);
				//teste para ver se o problema de readOnly n�o repita
				if(bean == null){
					bean = super.carregar(request, bean);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			String forCombo = StringUtils.isBlank(request.getParameter("insertFromSelect")) ? "true" : "false";
			boolean includeDescription = Boolean.valueOf(request.getParameter("includeDescriptionFromInsertOne") != null ? request.getParameter("includeDescriptionFromInsertOne") : "false"); 
			Object id = Util.strings.toStringIdStyled(bean, includeDescription);
			String description = Util.strings.toStringDescription(bean);
			NeoWeb.getRequestContext().clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent()
			.println("<html>" +
					"<script language=\"JavaScript\" src=\""+request.getServletRequest().getContextPath()+"/resource/js/util.js\"></script>" +
					"<script language=\"JavaScript\">selecionar('" + id + "', '" + addEscape(description) + "'," + forCombo + ");</script>" +
					"</html>");
			
			if("true".equalsIgnoreCase(request.getParameter("closeOnSave"))){
				View.getCurrent().println("<html><script type='text/javascript'>window.close();</script></html>");
			} 
			
			return null;
		} else {
			BeanDescriptor<BEAN> beanDescriptor = Neo.getApplicationContext().getBeanDescriptor(bean);
			String idProperty = beanDescriptor.getIdPropertyName();
			Object id = beanDescriptor.getId();
			
			if(idProperty != null && !idProperty.equals("") && id != null){
				String requestQuery = request.getRequestQuery();
				String query = "?" + ACTION_PARAMETER + "=consultar&" + idProperty + "=" + id;
				return new ModelAndView("redirect:" + requestQuery + query);
			}
			
			request.setAttribute(CONSULTAR, true);
			return continueOnAction("consultar", bean);
		}
	}
	
	protected String addEscape(String value){
		StringBuilder newStr = new StringBuilder();
		for (int i = 0; i < value.length(); i++) {
			switch (value.charAt(i)) {
			case '\'':
				newStr.append("\\'");
				break;
			case '\\':
				newStr.append("\\\\");
				break;

			default:
				newStr.append(value.charAt(i));
				break;
			}
		}
		
		return newStr.toString();
	}

	@Override
	protected void salvar(WebRequestContext request, BEAN bean) throws Exception {
		genericService.saveOrUpdate(bean);
		
		if(bean != null){
			BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, bean.getClass());
			Object id = Util.beans.getGetterMethod(bean.getClass(), beanDescriptor.getIdPropertyName()).invoke(bean);
			
			if(id != null){
				request.addMessage("Registro " + id + " salvo com sucesso.");
			} else request.addMessage("Registro salvo com sucesso.");
		} else {
			request.addMessage("Registro salvo com sucesso.");
		}
	}

	public Boolean validaExcluirEmMassa(WebRequestContext request, BEAN bean) {
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ModelAndView doListagem(WebRequestContext request, FILTRO filtro)throws CrudException {
		/**
		 * A verifica��o abaixo � feita para retornar o filtro para a ordena��o padr�o quando o filtro
		 * for reiniciado (bot�o 'Reiniciar'). O rein�cio do filtro � identificado pelo atributo "clearFilter" enviado
		 * com o valor "true" para o controller.
		 * A ordena��o padr�o � obtida atrav�s da annotation @DefaultOrderBy do DAO do controller.
		 * 
		 * @author Fl�vio Tavares
		 */
		if("true".equals(request.getParameter(MultiActionController.CLEAR_FILTER))){
			try {
				filtro = (FILTRO) filtro.getClass().newInstance();
				SalvarFiltroUtil.removeSalvarFiltroSessao(request);
				SalvarFiltroUtil.removeFiltroSalvoSessao(request, this.getSessionCommandName(this.getClass()));
			} catch (Exception e) {
				throw new SinedException("N�o foi poss�vel instanciar um objeto da classe " + filtro.getClass());
			}
			
			ReflectionCache cache = ReflectionCacheFactory.getReflectionCache();
			GenericDAO<BEAN> genericDAO = getGenericService().getGenericDAO();
			
			String orderBy = null;
			if(cache.isAnnotationPresent(genericDAO.getClass(), DefaultOrderBy.class)){
				DefaultOrderBy defaultOrderBy = cache.getAnnotation(genericDAO.getClass(), DefaultOrderBy.class);
				orderBy = defaultOrderBy.value();
			}
			filtro.setOrderBy(orderBy);
		}
		if(!filtro.isNotFirstTime()){
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null && empresa.getCdpessoa() != null){
				try {
					Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
					if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class) ){
						methodSetEmpresa.invoke(filtro, empresa);
					}
				} catch (Exception e) {
					throw new CrudException("methodSetEmpresa", e);
				}
			}
		}
		
		//FIXME refatorar e remover para o Util
		if(isSalvarFiltro()){
			SalvarFiltroUtil.configureRequestForListagem(request, this.getSessionCommandName(this.getClass()));
		}
		//limita a quantidade m�xima de registros por p�gina no W3 a no m�ximo 999 registros, para evitar (novos) travamentos.
		if(isPageSizeLimited() && filtro.getPageSize()>999){
			filtro.setPageSize(999);
		}
		
		setAtributosPaginacaoSimples(request, filtro);
		request.getSession().setAttribute(this.getSessionCommandName(filtro.getClass()), filtro);
		return super.doListagem(request, filtro);
	}

	
	
	
	/**
     * Usa Salvar filtro
     */
	public boolean isSalvarFiltro(){
		return false;
	}
	
	/**
     * Limita a quantidade de registros retornados
     */
	protected boolean isPageSizeLimited(){
		return true;
	}

	@Override
	protected BEAN criar(WebRequestContext request, BEAN form) throws Exception {
		if (Boolean.valueOf(request.getParameter("naoCriarInstancia"))) {
			return form;
		}
		
		BEAN bean =  super.criar(request, form);
		Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
		if(empresa != null){
			try {
				Method methodSetEmpresa = Util.beans.getSetterMethod(bean.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class) ){
					methodSetEmpresa.invoke(bean, empresa);
				}
			} catch (Exception e) {
				throw new CrudException("methodSetEmpresa", e);
			}
		}
		return bean;
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @auhtor Thiago Clemente
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void calcularTotalRegistrosPaginacaoAJax(WebRequestContext request, FILTRO filtro){
		
		QueryBuilder queryBuilder = genericService.getGenericDAO().newQueryBuilderWithFrom(genericService.getGenericDAO().getClass());
		genericService.getGenericDAO().updateListagemQuery(queryBuilder, filtro);
		
		QueryBuilderSined<Long> countQueryBuilder = new QueryBuilderSined<Long>(queryBuilder.getHibernateTemplate());
	       
        String idPropertyName = queryBuilder.getHibernateTemplate().getSessionFactory().getClassMetadata(queryBuilder.getFrom().getFromClass()).getIdentifierPropertyName();
        countQueryBuilder.select("count(distinct "+queryBuilder.getAlias()+"."+idPropertyName+")");
		
        QueryBuilder.From from = queryBuilder.getFrom();
        countQueryBuilder.from(from);
        
        List<QueryBuilder.Join> joins = queryBuilder.getJoins();
        for (QueryBuilder.Join join: joins) {
			countQueryBuilder.join(join.getJoinMode(), false, join.getPath());
		}
        
        QueryBuilder.Where where = queryBuilder.getWhere();
		countQueryBuilder.where(where);
		countQueryBuilder.setUseTranslator(false);
		countQueryBuilder.setUseReadOnly(true);
		
		updateQueryCountTotalregistrosPaginacaoAjax(countQueryBuilder, filtro);
		
		Long totalRegistrosPaginacao = countQueryBuilder.unique();
		
		View view = View.getCurrent();
		view.println("var totalRegistrosPaginacao = " + totalRegistrosPaginacao + ";");
	}
	
	
	public void updateQueryCountTotalregistrosPaginacaoAjax(QueryBuilderSined<Long> countQueryBuilder, FILTRO filtro) {
		countQueryBuilder.setUseWhereFornecedorEmpresa(Boolean.TRUE);
		countQueryBuilder.setUseWhereClienteEmpresa(Boolean.TRUE);
	}

	public boolean isPaginacaoSimples(){
		return true;
	}
	
	/**
	 * 
	 * @param request
	 * @auhtor Thiago Clemente
	 * 
	 */
	protected void setAtributosPaginacaoSimples(WebRequestContext request, FILTRO filtro){
		if (this.isPaginacaoSimples()){
			request.setAttribute("PAGINACAO_SIMPLES", Boolean.TRUE);
			if (filtro instanceof FiltroListagemSined){
				((FiltroListagemSined) filtro).setPaginacaoSimples(Boolean.TRUE);
			}
		}else {
			request.setAttribute("PAGINACAO_SIMPLES", Boolean.FALSE);
			if (filtro instanceof FiltroListagemSined){
				((FiltroListagemSined) filtro).setPaginacaoSimples(Boolean.FALSE);
			}
		}
	}
	
	/**
	 * M�todo utilizado no momento de salvamento do filtro.
	 * 
	 * @param request
	 * @param filtro
	 * @author igorsilveriocosta
	 */
	public void setaFiltroSessao(WebRequestContext request, FILTRO filtro){
		SalvarFiltroUtil.setSalvarFiltroSessao(request, filtro);
	}
	
	/**
	 * M�todo utilizado no momento de cancelamento do salvamento do filtro.
	 * 
	 * @param request
	 * @param filtro
	 */
	public void removeFiltroSessao(WebRequestContext request, FILTRO filtro){
		SalvarFiltroUtil.removeSalvarFiltroSessao(request);
	}

	/**
	 * M�todo que invoca a modal de salvamento/edi��o do filtro.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 */
	public ModelAndView doModalSalvarFiltro(WebRequestContext request, Filtro filtroBean){
		return SalvarFiltroUtil.doModalSalvarFiltro(request, filtroBean);
	}
	
	/**
	 * M�todo que invoca a modal de salvamento/edi��o de um filtro novo.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 */
	public ModelAndView doModalSalvarFiltroNovo(WebRequestContext request, Filtro filtroBean){
		return SalvarFiltroUtil.doModalSalvarFiltro(request, filtroBean);
	}
	
	/**
	 * M�todo que salva o filtro e retorna � p�gina de listagem.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView doSalvarFiltro(WebRequestContext request, Filtro filtroBean) throws Exception{
		Filtro filtroBeanSessao = SalvarFiltroUtil.doSalvarFiltro(request, this.getSessionCommandName(this.getClass()), filtroBean);
		
		//Fazendo o binding do Filtro
		FILTRO filtro = (FILTRO) filtroBeanSessao.getClasseFiltro();
		bind(request, filtro, true);
		request.addMessage("Filtro \""+filtroBean.getNome()+"\" salvo com sucesso.");

		return sendRedirectToAction(CrudControllerSined.LISTAGEM);
	}	
	
	/**
	 * M�todo que remove o filtro selecionado.
	 * 
	 * @param request
	 * @param filtroBean
	 * @return
	 * @throws Exception
	 */
	public ModelAndView doExcluirFiltro(WebRequestContext request, FILTRO filtro) throws CrudException{
		return SalvarFiltroUtil.doExcluirFiltro(request, this.getSessionCommandName(this.getClass()));
	}
	
	public ModelAndView doEntrada(WebRequestContext request, FORMBEAN form) throws CrudException {
		WebRequestContext webRequestContext = NeoWeb.getRequestContext();
		String path = webRequestContext.getRequestQuery();
		path = path.substring(path.indexOf("/", 1), path.length());
		
		List<ValidacaoCampoObrigatorio> listaValidacaoCampoObrigatorio = CampoobrigatorioService.getInstance().validaListaCampoObrigatorio(getCamposObrigatoriosAdicionais(), path, form, null);
		
		String campoObrigatorioJsonInit = View.convertToJson(listaValidacaoCampoObrigatorio).toString();
		request.setAttribute("CampoObrigatorioJsonInit", campoObrigatorioJsonInit);
		
		return super.doEntrada(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		super.validate(obj, errors, acao);
		if(obj != null){
			if(entradaCommandClass.isAssignableFrom(obj.getClass())){
				validateBeanCamposObrigatorios((FORMBEAN)obj, errors);
			}
		}
	}
	
	protected List<ValidacaoCampoObrigatorio> getCamposObrigatoriosAdicionais() {
		return null;
	}
	
	protected void validateBeanCamposObrigatorios(FORMBEAN form, BindException errors){
		WebRequestContext webRequestContext = NeoWeb.getRequestContext();
		String path = webRequestContext.getRequestQuery();
		path = path.substring(path.indexOf("/", 1), path.length());
		
		List<ValidacaoCampoObrigatorio> listaValidacaoCampoObrigatorio = CampoobrigatorioService.getInstance().validaListaCampoObrigatorio(getCamposObrigatoriosAdicionais(), path, form, errors);
		for (ValidacaoCampoObrigatorio validacaoCampoObrigatorio : listaValidacaoCampoObrigatorio) {
			try {
				if(StringUtils.isNotBlank(validacaoCampoObrigatorio.getPrefixo())){
					Method metodo = beanClass.getMethod("get" + Util.strings.captalize(validacaoCampoObrigatorio.getPrefixo()));
					Object lista = metodo.invoke(formToBean(form));
					
					if(lista != null && lista instanceof List<?>){
						if(validacaoCampoObrigatorio.getPelomenosumregistro() != null && validacaoCampoObrigatorio.getPelomenosumregistro() && ((List<?>) lista).size() == 0){
							errors.reject("001", "A lista " + validacaoCampoObrigatorio.getDisplayNameLista() + " � obrigat�rio pelo menos 1 item.");
						}
						
						for(Object object : (List<?>) lista){
							validateCampoObrigatorio(object, validacaoCampoObrigatorio, errors);
						}
					} else if(validacaoCampoObrigatorio.getPelomenosumregistro() != null && validacaoCampoObrigatorio.getPelomenosumregistro()){
						errors.reject("001", "A lista " + validacaoCampoObrigatorio.getDisplayNameLista() + " � obrigat�rio pelo menos 1 item.");
					}
				} else {
					validateCampoObrigatorio(form, validacaoCampoObrigatorio, errors);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void validateCampoObrigatorio(Object form, ValidacaoCampoObrigatorio validacaoCampoObrigatorio, BindException errors) throws Exception{
		Method metodo = form.getClass().getMethod("get" + Util.strings.captalize(validacaoCampoObrigatorio.getCampo()));
		Object objeto = metodo.invoke(form);
				
		boolean showError = false;	
		Class<?> classeRetorno = metodo.getReturnType();
		if (String.class.equals(classeRetorno) && (objeto==null || objeto.equals(""))){
			showError = true;						
		} else if (objeto==null){
			showError = true;
		}					
		if (showError){
			errors.reject("001", "O campo " + validacaoCampoObrigatorio.getDisplayNameCampo() + " � obrigat�rio.");
		}
	}
	
	public void ajaxVerifyAttributeSession(WebRequestContext request) {
		boolean attributeInvalid = false;
		try {
			attributeInvalid = request.getSession().getAttribute(request.getParameter("nameAttribute")) == null;
		} catch (Exception e) {}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var attributeInvalid = " + attributeInvalid + ";");
	}
	
}
