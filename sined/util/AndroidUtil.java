package br.com.linkcom.sined.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;

public class AndroidUtil {
	
	private static final String QUEBRA_LINHA = "\n\r";
	
	/**
	* M�todo que retorna uma String com a lista de URLs dos arquivos offlines separadas por uma quebra de linha
	*
	* @return
	* @since 30/09/2014
	* @author Luiz Fernando
	*/
	public static String getAndroidFilesString(){
		StringBuilder sb = new StringBuilder();
		List<File> sub = getAndroidFiles();  
	    
	    for (File arq : sub) {
    		sb.append("ArquivoAndroid?f=").append(arq.getName()).append(QUEBRA_LINHA);
		}
         return sb.toString();  
	}

	/**
	 * R
	 * @throws SinedException 
	 * @return
	 */
	/**
	* M�todo que retorna a lista de arquivos para o android disponiv�is para um cliente
	*
	* @return
	* @since 30/09/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public static List<File> getAndroidFiles() {
		List<File> files = new ArrayList<File>();
		String realPath = getDirAndroidJSON(SinedUtil.getStringConexaoBanco());
		File[] sub = new File(realPath).listFiles();  
		if(sub != null){
		    for (File fileDir: sub) {  
		    	if(!fileDir.getName().startsWith(".") || !fileDir.getName().contains("Store"))
		    		files.add (fileDir); //   
	        }
		}
		
		Collections.sort(files,  new Comparator() {  
			public int compare(Object o1, Object o2) {  
				File p1 = (File) o1;  
				File p2 = (File) o2;  
				return p1.getName().compareTo(p2.getName());  
			}  
		});
		return files;
	}
		
	/**
	* M�todo que retorna o caminho do diret�rio onde os arquivos JSON do android ser�o gerados
	*
	* @param nomeCliente
	* @return
	* @since 30/09/2014
	* @author Luiz Fernando
	*/
	public static String getDirAndroidJSON(String nomeCliente){
		String SEPARATOR = System.getProperty("file.separator");
		return ArquivoDAO.getInstance().getPathDir() + SEPARATOR + Neo.getApplicationName() + SEPARATOR + "android" + SEPARATOR + nomeCliente ;
	}

}
