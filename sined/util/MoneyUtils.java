package br.com.linkcom.sined.util;

import br.com.linkcom.neo.types.Money;

public class MoneyUtils {

	public static Money zeroWhenNull(Money valor){
		return valor == null? new Money(0): valor;
	}
	
	public static Double moneyToDouble(Money money){
		if(money == null){
			return 0d;
		}
		return money.doubleValue();
	}
	
	public static boolean isMaiorQueZero(Money valor){
		return valor != null && valor.getValue().doubleValue() > 0;
	}
}