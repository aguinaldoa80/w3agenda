package br.com.linkcom.sined.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.apache.velocity.tools.generic.AlternatorTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.LoopTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.apache.velocity.tools.generic.RenderTool;
import org.apache.velocity.tools.generic.ResourceTool;
import org.apache.velocity.tools.generic.SortTool;
import org.apache.velocity.tools.generic.ValueParser;

import br.com.linkcom.neo.types.ListSet;



public class VelocityUtil {

	private static boolean hasStarted = false;	
	private final static String getRegexpPreencheLinha(){
		return "(.*)(#preenche_linha)(\\d)\\((.*)\\)";
	}
	private final static Pattern getPatternPreencheLinha() {
		return Pattern.compile(getRegexpPreencheLinha(), Pattern.CASE_INSENSITIVE);
	}
	private static Map<Integer, String> mapaMacroPreencheLinha = new HashMap<Integer, String>();
	
	private static final String getMacroPreencheLinha(Integer key){
		if(mapaMacroPreencheLinha == null){
			mapaMacroPreencheLinha = new HashMap<Integer, String>();
		}
		
		if(mapaMacroPreencheLinha.containsKey(key)){
			return mapaMacroPreencheLinha.get(key);
		} else{
			return setMacroPreencheLinha(key);
		}
	}
	
	/**
	 * M�todo que gera macros de preenchimento de linhas para uma quantidade pr�-definida de campos.
	 * @param numCampos
	 * @return
	 * @author Rafael Salvio
	 */
	private static final String setMacroPreencheLinha(Integer numCampos){
		StringBuilder sb = new StringBuilder();
		sb.append("#macro ( preenche_linha"+numCampos)	
				.append(" $espacamento");
		for(int i=1; i<=numCampos; i++){
			sb.append(" $label"+i+" $param"+i+" $tamanho"+i);
		}
		sb.append(" )\n")
			.append("#set($branco = \" \")\n");
		if(numCampos.equals(1)){
			sb.append("#set($totalLacos = $param1.length() / $tamanho1)\n");
		}else{
			for(int i=1; i<=numCampos; i++){
				sb.append("#set($lacos"+i+" = $param"+i+".length() / $tamanho"+i+")\n");
			}
			for(int i=1; i<numCampos; i++){
				sb.append("#");
				if(i > 1){
					sb.append("else");
				}
				sb.append("if(true");
				for(int j=i+1; j<=numCampos; j++){
					sb.append(" && $lacos"+i+" >= $lacos"+j);
				}
				sb.append(")\n")
					.append("#set($totalLacos = $lacos"+i+")\n");
			}
			sb.append("#else\n")
				.append("#set($totalLacos = $lacos"+numCampos+")\n")
				.append("#end\n");
		}
		sb.append("#if($totalLacos > 0)\n")
			.append("#foreach($i in [0..$totalLacos] )\n");
		for(int i=1; i<=numCampos; i++){
			sb.append("#set($limiteInf"+i+" = ($i)*($tamanho"+i+"))\n")
				.append("#set($limiteSup"+i+" = ($i+1)*($tamanho"+i+"))\n");			
		}
		for(int i=1; i<=numCampos; i++){
			sb.append("#imprime_campo($limiteInf"+i+" $limiteSup"+i+" $label"+i+" $param"+i+" $tamanho"+i);
			if(i < numCampos){
				sb.append(" $espacamento)");
			} else{
				sb.append(" 0)\n\n");
			}
		}
		sb.append("#end\n")
			.append("#else\n");
		for(int i=1; i<=numCampos; i++){
			sb.append("$label"+i+" $param"+i);
			if(i < numCampos){
				sb.append("#if($espacamento > 0)#foreach($i in [1..$espacamento])$branco#end#end");
			} else{
				sb.append("\n");
			}
		}
		sb.append("#end\n")
			.append("#end\n");
		
		mapaMacroPreencheLinha.put(numCampos, sb.toString());
		return sb.toString();
	}
	
	public static String MACRO_IMPRIME_CAMPO =
	"#macro ( imprime_campo $limiteInf $limiteSup $label $param $tamanho $espacamento )\n" +
	"#set($branco = \" \")\n" +
	"#if($limiteInf > $param.length())\n" +
	"#set($espaco = $label.length()+1 + $tamanho + $espacamento)\n" +
	"#foreach($i in [1..$espaco])\n" +
	"$branco#end\n" +
	"#else\n" +
	"#if($limiteSup > $param.length())\n" +
	"#set($espaco = $espacamento + $limiteSup - $param.length() )\n" +
	"#set($limiteSup = $param.length())\n" +
	"#else\n" +
	"#set($espaco = $espacamento)\n" +
	"#end\n" +
	"#if($limiteInf == 0)\n" +
	"$label #else\n" +
	"#foreach($i in [0..$label.length()])\n" +
	"$branco#end\n" +
	"#end\n" +
	"$param.substring($limiteInf, $limiteSup)#if($espaco > 0)#foreach($j in [1..$espaco])$branco#end#end\n" +
	"#end\n" +
	"#end\n";
	
	public static String MACRO_STRINGCHEIA_DIREITA = 
			"#macro( stringCheiaDireita $param $tamanho )\n" +
			"#set($tamanhoStr = $param.length())\n" +
			"#if($tamanhoStr > $math.sub($tamanho, 1))\n" +
			"$param.substring(0, $tamanho)" +
			"#else\n" +
			"#set($tamanho = $tamanho - $tamanhoStr - 1)\n" +
			"#set($branco = \" \")\n" +
			"$param#foreach($i in [0..$tamanho] )$branco#end\n" +
			"#end\n" +
			"#end\n"; 
	public static String MACRO_STRINGCHEIA_ESQUERDA =
			"#macro( stringCheiaEsquerda $param $tamanho )\n" +
			"#set($tamanhoStr = $param.length())\n" +
			"#if($tamanhoStr > $math.sub($tamanho, 1))\n" +
			"$param.substring(0, $tamanho)" +
			"#else\n" +
			"#set($tamanho = $tamanho - $tamanhoStr - 1)\n" +
			"#set($branco = \" \")\n" +
			"#foreach($i in [0..$tamanho] )$branco#end$param" +
			"#end\n" +
			"#end\n";
		
	/**
	 * Inicia Velocity
	 * @throws Exception
	 */
	public static void init() throws Exception{
		if(!hasStarted){
			Velocity.setProperty("velocimacro.permissions.allow.inline.to.replace.global", true);
			Velocity.setProperty("velocimacro.library.autoreload", true);
			Velocity.setProperty("file.resource.loader.cache", false );
			Velocity.setProperty("velocity.engine.resource.manager.cache.enabled", false );
			
			Velocity.init();
			
			hasStarted = true;
		}
	}
	
	/**
	 * Retorna um contexto do Veocity devidamente configurado. 
	 * Por padr�o, valores nulos n�o ser�o exibidos.
	 * @return
	 */
	public static VelocityContext getNewVelocityContext(){
		try{
			init();
		}catch(Exception e){ 
			e.printStackTrace();
			throw new SinedException("Erro ao iniciar o parser de teplates do W3erp");
		}
		VelocityContext context = new VelocityContext();
		EventCartridge eventCartridge = new EventCartridge();
		eventCartridge.addEventHandler(new ReferenceInsertionEventHandler() {
		    public Object referenceInsert(String string, Object object) {
		       if (object == null) {
		          return "";
		       }
		       return object;
		    }
		});
		
		eventCartridge.attachToContext(context);
		
		context.put("math", new MathTool());
		context.put("number", new NumberTool());
		context.put("sort", new SortTool());
		context.put("date", new MathTool());
		context.put("number", new NumberTool());
		context.put("render", new RenderTool());
		context.put("escape", new EscapeTool());
		context.put("resource", new ResourceTool());
		context.put("alternator", new AlternatorTool());
		context.put("list", new LoopTool());
		context.put("valueParser", new ValueParser());
		context.put("r", "\r");
		return context;
	}
	/**
	 * Retorna a Teplate do Velocity a partir de uma String
	 * @param templateName
	 * @param templateContent
	 * @return
	 * @throws Throwable 
	 */
	public static Template getTemplateByString(String templateName, String templateContent) throws Throwable{
		RuntimeServices runtimeServices = RuntimeSingleton.getRuntimeServices();            
        StringReader reader = new StringReader(templateContent);
        SimpleNode node = runtimeServices.parse(reader, templateName);
        Template template = new Template();
        template.setRuntimeServices(runtimeServices);
        template.setData(node);
        template.initDocument();
        return template;
	}
	
	/**
	 * Renderiza um template Velocity
	 * @param template 
	 * @param map
	 * @return
	 * @throws ResourceNotFoundException
	 * @throws ParseErrorException
	 * @throws MethodInvocationException
	 * @throws Exception
	 */
	public static String renderTemplate(String templateString, Map<String, Object> map ) throws ResourceNotFoundException, ParseErrorException, MethodInvocationException, Exception{
		init();
		VelocityContext context = getNewVelocityContext();
		for (String key : map.keySet()) {
			context.put(key, map.get(key));
		}
		
		StringWriter writer = new StringWriter();
		Velocity.evaluate( context, writer, "log tag name", processaTemplate(templateString));
        return writer.toString();
	}
	
	/**
	 * Este m�todo retorna um template compil�vel com todas as macros necess�rias embutidas.
	 * Para tal ele percorre o template original em busca de macros personalizadas e adiciona as macros fixas.
	 * @param templateOriginal
	 * @return
	 * @author Rafael Salvio
	 */
	public static String processaTemplate(String templateOriginal){
		Set<String> listaMacros = new ListSet<String>(String.class);
		Scanner sc = new Scanner(templateOriginal);
		while (sc.hasNextLine()){
			String linhaAtual = sc.nextLine();
			Matcher matcher = getPatternPreencheLinha().matcher(linhaAtual);
			if(matcher.matches()){
				String numCampos = matcher.group(3);
				if(numCampos != null && !numCampos.trim().isEmpty() && Integer.parseInt(numCampos) > 0){
					if(listaMacros.isEmpty()){
						listaMacros.add(MACRO_IMPRIME_CAMPO);						
					}
					listaMacros.add(getMacroPreencheLinha(Integer.parseInt(numCampos)));
				}
			}
		}  
		listaMacros.add(MACRO_STRINGCHEIA_ESQUERDA);
		listaMacros.add(MACRO_STRINGCHEIA_DIREITA);
		
		return addMacros(listaMacros, templateOriginal);
	}
	
	/**
	 * M�todo que adiciona as macros ao template para compila��o.  
	 * @param templateString
	 * @return
	 * @author Rafael Salvio
	 */
	private static String addMacros(Set<String> listaMacros, String templateOriginal){
		if(listaMacros != null && !listaMacros.isEmpty()){
			StringBuilder templateFinal = new StringBuilder();
			for(String macro : listaMacros){
				templateFinal.append(macro);
			}
			templateFinal.append(templateOriginal);
			return templateFinal.toString();
		} else{
			return templateOriginal;
		}
	}
}
