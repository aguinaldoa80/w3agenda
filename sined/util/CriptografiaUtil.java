package br.com.linkcom.sined.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.Key;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;

import org.apache.commons.codec.binary.Base64;

public class CriptografiaUtil {
	
	private static final String ALIAS = "linkcom";
	private static final String PASS = "gre4POL2";
	private static final String ALGORITMO = "MD5withRSA";
	private static final String HEXDIGITS = "0123456789abcdef";
	
	private static File getFileCertificate() throws IOException{
		URL urlCadeiaCertificados = new URL("http", "utilpub.linkcom.com.br", "/w3erp/w3erp.jks");
        InputStream is = urlCadeiaCertificados.openConnection().getInputStream();
		
		File f = new File(System.getProperty("user.home") + File.separator + "w3erp.jks");
	    OutputStream out = new FileOutputStream(f);
	    byte buf[] = new byte[1024];
	    int len;
	    while((len=is.read(buf))>0) out.write(buf,0,len);
	    
	    out.close();
	    is.close();
	    
	    return f;
	}
	
	private static PrivateKey getPrivateKeyFromFile() throws Exception {
		File cert = getFileCertificate();
		
		KeyStore ks = KeyStore.getInstance ( "JKS" );
		char[] pwd = PASS.toCharArray();
		InputStream is = new FileInputStream( cert );
		ks.load( is, pwd );
		is.close();
		Key key = ks.getKey( ALIAS, pwd );
		if( key instanceof PrivateKey ) {
			return (PrivateKey) key;
		}
		return null;
	}

	private static PublicKey getPublicKeyFromFile() throws Exception {
		File cert = getFileCertificate();
		
		KeyStore ks = KeyStore.getInstance ( "JKS" );
		char[] pwd = PASS.toCharArray();
		InputStream is = new FileInputStream( cert );
		ks.load( is, pwd );
		
		@SuppressWarnings("unused")
		Key key = ks.getKey( ALIAS, pwd );
		
		Certificate c = ks.getCertificate( ALIAS );
		PublicKey p = c.getPublicKey();
		return p;
	}
	
	public static String createSignature(byte[] buffer) throws Exception {
		PrivateKey key = getPrivateKeyFromFile();
		
		Signature sig = Signature.getInstance(ALGORITMO);
		sig.initSign(key);
		sig.update(buffer, 0, buffer.length);
		return byteArrayToHexString(sig.sign());
	}

	public static boolean verifySignature(byte[] buffer, String signedStr) throws Exception {
		PublicKey key = getPublicKeyFromFile();
		byte[] signed = hexStringToByteArray(signedStr);
		
		Signature sig = Signature.getInstance(ALGORITMO);
		sig.initVerify(key);
		sig.update(buffer, 0, buffer.length);
		return sig.verify( signed );
	}
	
	private static String byteArrayToHexString(byte[] b) {
		StringBuffer buf = new StringBuffer();
    
		for (int i = 0; i < b.length; i++) {
			int j = ((int) b[i]) & 0xFF; 
			buf.append(HEXDIGITS.charAt(j / 16)); 
			buf.append(HEXDIGITS.charAt(j % 16)); 
		}
	    
		return buf.toString();
	}
  
	private static byte[] hexStringToByteArray(String hexa) throws IllegalArgumentException {
		if (hexa.length() % 2 != 0) {
			throw new IllegalArgumentException("String hexa inv�lida");  
		}
      
		byte[] b = new byte[hexa.length() / 2];
      
		for (int i = 0; i < hexa.length(); i+=2) {
			b[i / 2] = (byte) ((HEXDIGITS.indexOf(hexa.charAt(i)) << 4) | (HEXDIGITS.indexOf(hexa.charAt(i + 1))));          
		}
		return b;
	}

	public static String getHashCsrt(String csrt, String chaveAcesso) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCsrt = csrt + chaveAcesso;
		String result = null;
		
		MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
        byte[] hash = msdDigest.digest(hashCsrt.getBytes());
        
        result = new String(new Base64().encode(hash));
        
        return result;
	}
}
