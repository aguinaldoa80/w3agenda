package br.com.linkcom.sined.util.contacontabil;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Tipooperacao;

public class ContaContabilTreeFiltro {

	private String propriedade;
	private String tipooperacao;
	private Tipooperacao tipooperacao_obj;
	private String q;
	private String whereInCdcontagerencial;
	private String whereInNaturezaContagerencial;
	private String whereNotInNaturezaContagerencial;
	private String nomeCampoEmpresa;
	private String cdEmpresa;
	private String selectPai;
	private Boolean selectPai_Bool;
	
	public String getQ() {
		return q;
	}
	
	public Tipooperacao getTipooperacao_obj() {
		return tipooperacao_obj;
	}
	
	public String getTipooperacao() {
		return tipooperacao;
	}
	
	public String getPropriedade() {
		return propriedade;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
	public void setTipooperacao(String tipooperacao) {
		this.tipooperacao = tipooperacao;
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("C")) setTipooperacao_obj(Tipooperacao.TIPO_CREDITO);
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("D")) setTipooperacao_obj(Tipooperacao.TIPO_DEBITO);
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("CO")) setTipooperacao_obj(Tipooperacao.TIPO_CONTABIL);
	}
	
	public void setTipooperacao_obj(Tipooperacao tipooperacao_obj) {
		this.tipooperacao_obj = tipooperacao_obj;
	}
	
	public void setQ(String q) {
		this.q = q;
	}

	public String getWhereInCdcontagerencial() {
		return whereInCdcontagerencial;
	}
	
	public void setWhereInCdcontagerencial(String whereInCdcontagerencial) {
		this.whereInCdcontagerencial = whereInCdcontagerencial;
	}
	
	public String getWhereInNaturezaContagerencial() {
		return whereInNaturezaContagerencial;
	}

	public void setWhereInNaturezaContagerencial(
			String whereInNaturezaContagerencial) {
		this.whereInNaturezaContagerencial = whereInNaturezaContagerencial;
	}
	
	public String getWhereNotInNaturezaContagerencial() {
		return whereNotInNaturezaContagerencial;
	}
	
	public void setWhereNotInNaturezaContagerencial(String whereNotInNaturezaContagerencial) {
		this.whereNotInNaturezaContagerencial = whereNotInNaturezaContagerencial;
	}

	public String getNomeCampoEmpresa() {
		return nomeCampoEmpresa;
	}

	public void setNomeCampoEmpresa(String nomeCampoEmpresa) {
		this.nomeCampoEmpresa = nomeCampoEmpresa;
	}
	
	public String getCdEmpresa() {
		return cdEmpresa;
	}
	
	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
		
	public String getSelectPai() {
		return selectPai;
	}

	public Boolean getSelectPai_Bool() {
		return selectPai_Bool;
	}

	public void setSelectPai(String selectPai) {
		this.selectPai = selectPai;
		setSelectPai_Bool(StringUtils.equalsIgnoreCase(selectPai, "true"));
	}

	public void setSelectPai_Bool(Boolean selectPai_Bool) {
		this.selectPai_Bool = selectPai_Bool;
	}
}