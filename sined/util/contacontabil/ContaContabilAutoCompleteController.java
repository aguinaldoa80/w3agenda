package br.com.linkcom.sined.util.contacontabil;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.service.ContacontabilService;

@Bean
@Controller(path="/financeiro/process/ContaContabilAutoComplete")
public class ContaContabilAutoCompleteController extends MultiActionController {
	
	private ContacontabilService contacontabilService;
	
	public void setContagerencialService(
			ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}

	@DefaultAction
	public void view(WebRequestContext request, ContaContabilTreeFiltro filtro){
		String whereInNaturezaContagerencial = Util.strings.emptyIfNull(filtro.getWhereInNaturezaContagerencial());
		whereInNaturezaContagerencial = whereInNaturezaContagerencial.replace("undefined", "");
		whereInNaturezaContagerencial = whereInNaturezaContagerencial.replace("null", "");

		String whereInCdcontagerencial = Util.strings.emptyIfNull(filtro.getWhereInCdcontagerencial());
		whereInCdcontagerencial = whereInCdcontagerencial.replace("undefined", "");
		whereInCdcontagerencial = whereInCdcontagerencial.replace("null", "");
		
		String nomeCampoEmpresa = Util.strings.emptyIfNull(filtro.getNomeCampoEmpresa());
		nomeCampoEmpresa = nomeCampoEmpresa.replace("undefined", "");
		nomeCampoEmpresa = nomeCampoEmpresa.replace("null", "");
				
		String whereInEmpresa = Util.strings.emptyIfNull(request.getParameter("empresa"));
		whereInEmpresa = whereInEmpresa.replace("undefined", "");
		whereInEmpresa = whereInEmpresa.replace("null", "");
		
		List<ContaContabil> find = contacontabilService.findAutocomplete(filtro.getQ(), filtro.getTipooperacao_obj(), whereInNaturezaContagerencial, whereInCdcontagerencial, nomeCampoEmpresa, whereInEmpresa, filtro.getSelectPai_Bool());
		
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(find));
	}

	private String criarEstrutura(List<ContaContabil> find) {
		StringBuilder builder = new StringBuilder();
		if(find != null && find.size() > 0){
			for (ContaContabil cc : find) {
				cc.setAdicionarCodigoalternativo(Boolean.TRUE);
				builder.append(cc.getDescricaoInputPersonalizado() + "|" + cc.getCdcontacontabil() + "\n");
			}
		}
		return builder.toString();
	}

}
