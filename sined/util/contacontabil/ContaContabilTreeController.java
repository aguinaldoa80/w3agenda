package br.com.linkcom.sined.util.contacontabil;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.service.ContacontabilService;

@Bean
@Controller(path="/financeiro/process/ContaContabilTreeView")
public class ContaContabilTreeController extends MultiActionController {
	
private ContacontabilService contacontabilService;
	
	public void setContagerencialService(
			ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}

	@DefaultAction
	public ModelAndView view(WebRequestContext request, ContaContabilTreeFiltro filtro){
		
		String whereInCdcontagerencial = null;
		String whereInNaturezaContagerencial = null;
		String whereNotInNaturezaContagerencial = null;
		Integer cdEmpresa = null;
		
		try {
			cdEmpresa = Integer.parseInt(request.getParameter("empresa"));
		} catch (Exception e) {
			
		}
		
		if(filtro.getWhereInCdcontagerencial() != null && !"".equals(filtro.getWhereInCdcontagerencial()) && !"undefined".equals(filtro.getWhereInCdcontagerencial())){
			whereInCdcontagerencial = filtro.getWhereInCdcontagerencial();
		}
		
		if(filtro.getWhereInNaturezaContagerencial() != null && !"".equals(filtro.getWhereInNaturezaContagerencial()) && !"undefined".equals(filtro.getWhereInNaturezaContagerencial())){
			whereInNaturezaContagerencial = filtro.getWhereInNaturezaContagerencial();
		}
		
		if(filtro.getWhereNotInNaturezaContagerencial() != null && !"".equals(filtro.getWhereNotInNaturezaContagerencial()) && !"undefined".equals(filtro.getWhereNotInNaturezaContagerencial())){
			whereNotInNaturezaContagerencial = filtro.getWhereNotInNaturezaContagerencial();
		}

		
		List<ContaContabil> listaContagerecial = contacontabilService.findTreeView(filtro.getTipooperacao_obj(), whereInCdcontagerencial, whereInNaturezaContagerencial, whereNotInNaturezaContagerencial, cdEmpresa);
		
		String codigo_treeview = this.criaEstrutura(listaContagerecial, filtro.getSelectPai_Bool());
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		return new ModelAndView("direct:/process/contaContabilTreeView");
	}

	private String criaEstrutura(List<ContaContabil> listaContaContabil, Boolean selectPai) {
		if(listaContaContabil != null && listaContaContabil.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (ContaContabil cc : listaContaContabil) {
				if(cc.getFilhos() != null && cc.getFilhos().size() > 0){
					if(selectPai){
						sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
						sb.append(cc.getCdcontacontabil());
						sb.append(", '");
						sb.append(cc.getVcontacontabil().getIdentificador());
						sb.append(" - ");
						sb.append(cc.getNome() != null ? cc.getNome().replace("'", "\\'") : "");
						sb.append("')\">");
					} else {
						sb.append("<li><span>");
					}
					sb.append(cc.getVcontacontabil().getIdentificador());
					sb.append(" - ");
					sb.append(cc.getNome());
					sb.append("</span><ul>");
					sb.append(this.criaEstrutura(cc.getFilhos(), selectPai));
					sb.append("</ul></li>");
				} else {
					sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
					sb.append(cc.getCdcontacontabil());
					sb.append(", '");
					sb.append(cc.getVcontacontabil().getIdentificador());
					sb.append(" - ");
					sb.append(cc.getNome() != null ? cc.getNome().replace("'", "\\'") : "");
					sb.append("')\">");
					sb.append(cc.getVcontacontabil().getIdentificador());
					sb.append(" - ");
					sb.append(cc.getNome());
					sb.append("</span></li>");
				}
			}
			return sb.toString();
		} else return "";
	}
	
}