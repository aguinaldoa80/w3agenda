package br.com.linkcom.sined.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.GrafanaCliente;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.GrafanaDashboard;

public class GrafanaUtil {

	private final static String GRAFANA_SESSION = "GRAFANA_SESSION"; 
	
	public static GrafanaCliente getGrafanaCliente() {
		Object obj = NeoWeb.getRequestContext().getSession().getAttribute(GRAFANA_SESSION);
		
		if (obj == null) {
			GrafanaCliente grafanaCliente = null;
			try {
				grafanaCliente = getGrafanaClienteInW3controle();
			} catch (Exception e) {
				e.printStackTrace();
				
				try {
					String nomeMaquina = "N�o encontrado.";
					try {  
						InetAddress localaddr = InetAddress.getLocalHost();  
						nomeMaquina = localaddr.getHostName();  
					} catch (UnknownHostException e3) {}  
					
					
					String url = "N�o encontrado";
					try {  
						url = SinedUtil.getUrlWithContext();
					} catch (Exception e3) {}
					
					
					StringWriter stringWriter = new StringWriter();
					e.printStackTrace(new PrintWriter(stringWriter));
					PedidovendaService.getInstance().enviarEmailErro("[W3ERP] Erro ao consultar o Grafana Cliente.","Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ", URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
				} catch (Exception e2) {
					e.printStackTrace();
				}
			}
			
			NeoWeb.getRequestContext().getSession().setAttribute(GRAFANA_SESSION, grafanaCliente);
			return grafanaCliente;
		} else {
			return (GrafanaCliente) obj;
		}
	}

	private static GrafanaCliente getGrafanaClienteInW3controle() throws NamingException, IOException, JDOMException {
		String nomeBanco = ParametrogeralService.getInstance().getNomeBancoDeDados();
		
		URL url = montaURLRequisicao();
		String data = nomeBanco != null ? ("nomeBanco=" + nomeBanco) : "";
		
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(data);
		wr.flush();
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		String xml = "";
		
		while ((line = rd.readLine()) != null) {
			xml += line;
		}
		
		wr.close();
		rd.close();
		
		return verificaXmlDashboard(xml);
	}
	
	@SuppressWarnings("unchecked")
	private static GrafanaCliente verificaXmlDashboard(String xml) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(Util.strings.tiraAcento(xml).getBytes());
		
		if (rootElement != null && rootElement.getContent().size() > 0) {
			GrafanaCliente grafanaCliente = new GrafanaCliente();
			
			Element grafanaClienteElement = SinedUtil.getChildElement("grafanaCliente", rootElement.getContent());
			
			if (grafanaClienteElement != null) {
				Element nomeBancoElement = SinedUtil.getChildElement("nomeBanco", grafanaClienteElement.getContent());
				grafanaCliente.setNomeBanco(SinedUtil.getElementString(nomeBancoElement));
				
				List<GrafanaDashboard> listaDashboard = new ArrayList<GrafanaDashboard>();
				
				List<Element> listGrafanaDashboardElement = SinedUtil.getListChildElement("grafanaDashboard", grafanaClienteElement.getContent());
				for (Element grafanaDashboardElement : listGrafanaDashboardElement) {
					Element uidDashboardElement = SinedUtil.getChildElement("uidDashboard", grafanaDashboardElement.getContent());
					Element nomeDashboardElement = SinedUtil.getChildElement("nomeDashboard", grafanaDashboardElement.getContent());
					
					GrafanaDashboard grafanaDashboard = new GrafanaDashboard();
					grafanaDashboard.setUidDashboard(SinedUtil.getElementString(uidDashboardElement));
					grafanaDashboard.setNomeDashboard(SinedUtil.getElementString(nomeDashboardElement));
					listaDashboard.add(grafanaDashboard);
				}
				
				grafanaCliente.setListaDashboard(listaDashboard);
			}
			
			return grafanaCliente;
		}
		
		return new GrafanaCliente();
	}
	
	private static URL montaURLRequisicao() throws MalformedURLException, NamingException{
		String urlString = "linkcom.w3erp.com.br";
		
		if(SinedUtil.isAmbienteDesenvolvimento()){
			//urlString = InitialContext.doLookup("W3CONTROLE_URL");
			urlString = "localhost:8080";
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o dos dashboards.");
			}
		}
		
		return new URL("http://" + urlString + "/w3controle/pub/process/GrafanaCliente");
	}
}
