package br.com.linkcom.sined.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetiporesponsavel;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.NovidadeVersao;
import br.com.linkcom.sined.geral.bean.NovidadeVersaoFuncionalidade;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.CategoriaClienteLogin;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.AtividadetiporesponsavelService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EmailCobrancaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.RequisicaohistoricoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobrancaDocumento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarOSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericRespostaInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.util.bean.NovidadeVersaoRetorno;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.text.SimpleDateFormat;

public class ControleAcessoUtil {

	public static ControleAcessoUtil util = new ControleAcessoUtil();
	
	private static final String PENDENCIA_FINANCEIRA_ACESSO = "PENDENCIA_FINANCEIRA_ACESSO"; 
	private static final String PENDENCIA_FINANCEIRA_EMAIL = "PENDENCIA_FINANCEIRA_EMAIL"; 
	private static final String PENDENCIA_FINANCEIRA_TIPO = "PENDENCIA_FINANCEIRA_TIPO";
	private static final String CATEGORIA_CLIENTE = "CATEGORIA_CLIENTE";
	private static final String LOGIN_AREA_CLIENTE = "LOGIN_AREA_CLIENTE"; 
	private static final String SENHA_AREA_CLIENTE = "SENHA_AREA_CLIENTE"; 
	public static final String CNPJLINKCOM = "01342793000190";
	
	public void limparCache(WebRequestContext request){
		request.getSession().setAttribute(PENDENCIA_FINANCEIRA_TIPO, null);
		request.getSession().setAttribute(PENDENCIA_FINANCEIRA_ACESSO, null);
		request.getSession().setAttribute(PENDENCIA_FINANCEIRA_EMAIL, null);
		request.getSession().setAttribute(CATEGORIA_CLIENTE, null);
		request.getSession().setAttribute(LOGIN_AREA_CLIENTE, null);
		request.getSession().setAttribute(SENHA_AREA_CLIENTE, null);
	}
	
	public CategoriaClienteLogin getCategoriaCliente(){
		Object categoriaClienteLogin = NeoWeb.getRequestContext().getSession().getAttribute(CATEGORIA_CLIENTE);
		if(categoriaClienteLogin != null && categoriaClienteLogin instanceof CategoriaClienteLogin){
			return (CategoriaClienteLogin) categoriaClienteLogin;
		}
		return null;
	}
	
	public void doVerificaPendenciafinanceira(HttpServletRequest request) {
		
		try{
			if(request.getSession().getAttribute(PENDENCIA_FINANCEIRA_ACESSO) == null) {
				Empresa empresa = EmpresaService.getInstance().getEmpresaPrincipalForPendenciafinanceira(new Cnpj(CNPJLINKCOM));
				PendenciaFinanceiraRetorno retorno = null;
				if(empresa != null && empresa.getCpfOuCnpj() != null && !"".equals(empresa.getCpfOuCnpj())){
					retorno = enviaRequisicaoPendenciafinanceira(empresa);
				}
				String pendenciaTipoName = retorno != null && retorno.getPendenciaTipo() != null ? retorno.getPendenciaTipo().name() : null;
				String emailPendencia = retorno != null ? retorno.getEmail() : null;
				CategoriaClienteLogin categoriaClienteLogin = retorno != null && retorno.getCategoriaClienteLogin() != null ? retorno.getCategoriaClienteLogin() : CategoriaClienteLogin.SERVICOS;
				String loginAreaCliente = retorno != null ? retorno.getLoginAreaCliente() : null;
				String senhaAreaCliente = retorno != null ? retorno.getSenhaAreaCliente() : null;
				
				request.getSession().setAttribute(PENDENCIA_FINANCEIRA_TIPO, pendenciaTipoName);
				request.getSession().setAttribute(PENDENCIA_FINANCEIRA_EMAIL, emailPendencia);
				request.getSession().setAttribute(PENDENCIA_FINANCEIRA_ACESSO, Boolean.TRUE);
				request.getSession().setAttribute(CATEGORIA_CLIENTE, categoriaClienteLogin);
				request.getSession().setAttribute(LOGIN_AREA_CLIENTE, loginAreaCliente);
				request.getSession().setAttribute(SENHA_AREA_CLIENTE, senhaAreaCliente);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private String montaDadosBasicosRequisicao(Cpf cpf, Cnpj cnpj, String acao) throws UnsupportedEncodingException{
		String cpfStr = cpf != null ? cpf.getValue().toString() : null;
		String cnpjStr = cnpj != null ? cnpj.getValue().toString() : null;
		String assinatura = "12345";
		
		// Adicionando os par�metros
		String data = URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode(acao, "LATIN1");
		if(cnpj != null){
			data += "&" + URLEncoder.encode("cnpj", "LATIN1") + "=" + URLEncoder.encode(cnpjStr, "LATIN1");			
		}else if(cpf != null){
			data += "&" + URLEncoder.encode("cpf", "LATIN1") + "=" + URLEncoder.encode(cpfStr, "LATIN1");			
		}
		
		data += "&" + URLEncoder.encode("hash", "LATIN1") + "=" + assinatura;
		
		return data;
	}
	
	private URL montaURLRequisicao() throws MalformedURLException, NamingException{
		String urlString = "linkcom.w3erp.com.br";
		
		if(SinedUtil.isAmbienteDesenvolvimento()){
			urlString = InitialContext.doLookup("PENDENCIA_FINANCEIRA_URL");
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o de pend�ncia financeira.");
			}
		}
		
		return new URL("http://" + urlString + "/w3erp/pub/process/IntegracaoWebservice");
	}
	
	private void enviaRequisicaoCriarOSInadimplencia(Cpf cpf, Cnpj cnpj, String cdprojeto, Money valor){
		try{
			URL url = montaURLRequisicao();
			String data = montaDadosBasicosRequisicao(cpf, cnpj, "criaOSInadimplencia");
			data += "&" + URLEncoder.encode("cdprojeto", "LATIN1") + "=" + URLEncoder.encode(cdprojeto, "LATIN1")
					+ "&" + URLEncoder.encode("valor", "LATIN1") + "=" + URLEncoder.encode(valor.toString(), "LATIN1");
			
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); 
			wr.flush(); 
			
			conn.getInputStream();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private PendenciaFinanceiraRetorno enviaRequisicaoPendenciafinanceira(Empresa empresa) {
		try{
			URL url = montaURLRequisicao();
			String data = montaDadosBasicosRequisicao(empresa.getCpf(), empresa.getCnpj(), "verificaPendenciaFinanceira");
			
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); 
			wr.flush(); 
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line; 
			String xml = ""; 
			
			while ((line = rd.readLine()) != null) { xml += line; } 
			
			wr.close(); 
			rd.close(); 
			
			return verificaXmlPendenciaFinanceira(xml);
			
		} catch (Exception e) {
//			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	private PendenciaFinanceiraRetorno verificaXmlPendenciaFinanceira(String xml) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		String strNovo = new String(new StringUtils().tiraAcento(xml).getBytes(),"UTF-8");
		SAXBuilder sb = new SAXBuilder();
		Document d = sb.build(new ByteArrayInputStream(strNovo.getBytes()));
		
		Element status = SinedUtil.getChildElement("status", d.getRootElement().getContent());
		Element pendencia = SinedUtil.getChildElement("Pendencia", d.getRootElement().getContent());
		Element pendenciaTipo = SinedUtil.getChildElement("PendenciaTipo", d.getRootElement().getContent());
		Element email = SinedUtil.getChildElement("Email", d.getRootElement().getContent());
		Element categoria = SinedUtil.getChildElement("Categoria", d.getRootElement().getContent());
		Element login = SinedUtil.getChildElement("Login", d.getRootElement().getContent());
		Element senha = SinedUtil.getChildElement("Senha", d.getRootElement().getContent());
		
		PendenciaFinanceiraRetorno retorno = new PendenciaFinanceiraRetorno();
		
		if(email != null){
			retorno.setEmail(email.getText());
		}
		if(login != null && senha != null){
			retorno.setLoginAreaCliente(login.getText());
			retorno.setSenhaAreaCliente(senha.getText());
		}
		
		if(status != null){
			String statusStr = status.getText();
			if("1".equals(statusStr) && pendencia != null){
				String pendenciaStr = pendencia.getText();
				boolean pendenciaBool = "TRUE".equals(pendenciaStr.toUpperCase());
				
				if(pendenciaBool){
					String pendenciaTipoStr = pendenciaTipo.getText();
					retorno.setPendenciaTipo(PendenciaTipo.valueOf(pendenciaTipoStr));
				}				
			}
		}
		
		try{
			if(categoria != null){
				String categoriaStr = categoria.getText();
				if(categoriaStr != null && !categoriaStr.equals("")){
					Integer categoriaOrdinal = Integer.parseInt(categoriaStr);
					retorno.setCategoriaClienteLogin(CategoriaClienteLogin.values()[categoriaOrdinal]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	public void webServicePendenciaFinanceira(VerificaPendenciaFinanceira bean, GenericRespostaInformacaoBean respostaInformacaoBean) {
		if((bean.getCnpj() == null || bean.getCnpj().equals("")) && 
				(bean.getCpf() == null || bean.getCpf().equals(""))){
			throw new SinedException("Favor enviar o CNPJ ou CPF do cliente.");
		}		
		
		Cliente cliente = ClienteService.getInstance().carregarCreditolimitecompraByCnpj(bean);
		if(cliente != null){
			Money creditolimitecompra = cliente.getCreditolimitecompra() != null ? new Money(cliente.getCreditolimitecompra()) : null;
			List<String> listaEmail = this.preencheEmailCliente(cliente);
			CategoriaClienteLogin categoriaClienteLogin = this.getCategoriaLogin(cliente);
			
			Contagerencial contagerencial_mensalidade = new Contagerencial(ParametrogeralService.getInstance().getInteger(Parametrogeral.CONTA_GERENCIAL_MENSALIDADE));
			Projeto projeto_w3erp = new Projeto(ParametrogeralService.getInstance().getInteger(Parametrogeral.PROJETO_W3ERP));
			Projeto projeto_sindis = new Projeto(ParametrogeralService.getInstance().getInteger(Parametrogeral.PROJETO_SINDIS));
			Projeto projeto_supergov = new Projeto(ParametrogeralService.getInstance().getInteger(Parametrogeral.PROJETO_SUPERGOV));
			
			List<Documento> listaDocumento = ContareceberService.getInstance().getPendenciaFinanceiraByCpfCnpj(bean.getCnpj(), bean.getCpf(), contagerencial_mensalidade, projeto_w3erp, projeto_sindis, projeto_supergov);
			
			Integer dias_suspensao = ParametrogeralService.getInstance().getInteger(Parametrogeral.DIAS_PARA_SUSPENSAO);
			boolean existeContratoAtivo = ContratoService.getInstance().existeContratoAtivo(cliente, contagerencial_mensalidade, projeto_w3erp, projeto_sindis, projeto_supergov);
			
			PendenciaFinanceiraRetorno retorno;
			if(existeContratoAtivo){
				retorno = this.getPendenciaFinanceira(creditolimitecompra, listaEmail, listaDocumento, true, null, dias_suspensao, cliente);
			}else{
				retorno = this.getPendenciaFinanceiraSemContratoAtivo(cliente, categoriaClienteLogin, listaEmail);
			}
			respostaInformacaoBean.addCampo("Pendencia", retorno.getPendencia());
			if(retorno.getEmail() != null) respostaInformacaoBean.addCampo("Email", retorno.getEmail());
			if(retorno.getPendenciaTipo() != null) respostaInformacaoBean.addCampo("PendenciaTipo", retorno.getPendenciaTipo().name());
			if(categoriaClienteLogin != null) respostaInformacaoBean.addCampo("Categoria", categoriaClienteLogin.ordinal());
			if(retorno.getLoginAreaCliente() != null) respostaInformacaoBean.addCampo("Login", retorno.getLoginAreaCliente());
			if(retorno.getSenhaAreaCliente() != null) respostaInformacaoBean.addCampo("Senha", retorno.getSenhaAreaCliente());
		} else {
			respostaInformacaoBean.addCampo("Pendencia", Boolean.TRUE);
			respostaInformacaoBean.addCampo("PendenciaTipo", PendenciaTipo.SEM_CONTRATO_ATIVO.name());
		}
	}
	
	private PendenciaFinanceiraRetorno getPendenciaFinanceiraSemContratoAtivo(Cliente cliente, CategoriaClienteLogin categoriaClienteLogin, List<String> listaEmail) {
		PendenciaFinanceiraRetorno retorno = new PendenciaFinanceiraRetorno();
		retorno.setPendenciaTipo(PendenciaTipo.SEM_CONTRATO_ATIVO);
		retorno.setCategoriaClienteLogin(categoriaClienteLogin);
		
		retorno.setPendencia(Boolean.TRUE);
		retorno.setEmail(CollectionsUtil.concatenate(listaEmail, ";"));
		if(cliente != null){
			retorno.setLoginAreaCliente(cliente.getLogin());
			retorno.setSenhaAreaCliente(cliente.getSenha());
		}
		return retorno;
	}

	public void webServiceCriarRequisicao(CriarOSBean bean, GenericRespostaInformacaoBean respostaInformacaoBean) {
		Cliente cliente = null;
		if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
			cliente = ClienteService.getInstance().findByCnpj(new Cnpj(bean.getCnpj()));
		} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
			cliente = ClienteService.getInstance().findByCpf(new Cpf(bean.getCpf()));
		} else{			
			throw new SinedException("Favor enviar o CNPJ ou CPF do cliente.");
		}
		
		if(cliente != null){
			String cdatividadetipoString = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.TIPO_ATIVIDADE_NEGOCIACAO);
			Date dtAtual = SinedDateUtils.currentDate();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
			Atividadetipo atividadetipo = null;
			if(org.apache.commons.lang.StringUtils.isNotEmpty(cdatividadetipoString)){
				atividadetipo = new Atividadetipo();
				try {
					atividadetipo.setCdatividadetipo(Integer.parseInt(cdatividadetipoString));
				} catch (NumberFormatException e) {
					throw new SinedException("O par�metro TIPO_ATIVIDADE_NEGOCIACAO possui um tipo de atividade inv�lido.");
				}
				
				atividadetipo = AtividadetipoService.getInstance().load(atividadetipo);
			}
			Projeto projeto = ProjetoService.getInstance().load(new Projeto(bean.getCdprojeto()));
			if(projeto == null || projeto.getColaborador() == null){
				throw new SinedException("Favor enviar um projeto v�lido.");
			}
			
			Colaborador colaborador = projeto.getColaborador();
			if(atividadetipo != null){
				Atividadetiporesponsavel atividadetiporesponsavel = AtividadetiporesponsavelService.getInstance().findByAtividadetipoAndProjeto(atividadetipo, projeto);
				if(atividadetiporesponsavel != null){
					colaborador = atividadetiporesponsavel.getColaborador();
				}
			}
			
			String descricaoOs = "Licen�a expirada - Pend�ncia financeira: Licen�a de software referente a " + sdf.format(dtAtual) + " no valor de R$" + bean.getValor() + ".";
			
			Requisicao requisicao = new Requisicao();
			requisicao.setDtrequisicao(dtAtual);
			requisicao.setAtividadetipo(atividadetipo);
			requisicao.setColaboradorresponsavel(colaborador);
			requisicao.setCliente(cliente);
			requisicao.setProjeto(projeto);
			requisicao.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.EMESPERA));
			requisicao.setRequisicaoprioridade(new Requisicaoprioridade(Requisicaoprioridade.URGENTE));
			requisicao.setDescricao(descricaoOs);
			RequisicaoService.getInstance().saveOrUpdate(requisicao);
			
			Requisicaohistorico requisicaohistorico = new Requisicaohistorico();
			requisicaohistorico.setRequisicao(requisicao);
			requisicaohistorico.setObservacao("(Criado) "+descricaoOs);
			RequisicaohistoricoService.getInstance().saveOrUpdate(requisicaohistorico);
			
			respostaInformacaoBean.addCampo("cdrequisicao", requisicao.getCdrequisicao());
		}
	}
	
	private CategoriaClienteLogin getCategoriaLogin(Cliente cliente) {
		if(cliente.getListaPessoacategoria() != null && cliente.getListaPessoacategoria().size() > 0){
			for (Pessoacategoria pessoacategoria : cliente.getListaPessoacategoria()) {
				if(pessoacategoria != null && 
						pessoacategoria.getCategoria() != null && 
						pessoacategoria.getCategoria().getCategoriaClienteLogin() != null) {
					return pessoacategoria.getCategoria().getCategoriaClienteLogin();
				}
			}
		}
		return null;
	}

	private List<String> preencheEmailCliente(Cliente cliente) {
		List<PessoaContato> listaPessoaContato = cliente.getListaContato();
		List<String> listaEmail = new ArrayList<String>();
		
		if(cliente.getEmail() != null && !cliente.getEmail().equals("")){
			listaEmail.add(cliente.getEmail());
		}
		
		if(listaPessoaContato != null && listaPessoaContato.size() > 0){
			for (PessoaContato pessoaContato : listaPessoaContato) {
				Contato contato = pessoaContato.getContato();
				
				boolean receberboleto = (contato.getReceberboleto() != null && contato.getReceberboleto());
				boolean responsavel = (contato.getResponsavelos() != null && contato.getResponsavelos());
				
				if((receberboleto || responsavel) && contato.getEmailcontato() != null && !contato.getEmailcontato().equals("")){
					listaEmail.add(contato.getEmailcontato());
				}
			}
		}
		return listaEmail;
	}

	private PendenciaFinanceiraRetorno getPendenciaFinanceira(Money creditolimitecompra, List<String> listaEmail, List<Documento> pendenciasDocumentos, boolean mensagem, Calendario calendario, 
			Integer dias_suspensao, Cliente cliente) {
		
		PendenciaFinanceiraRetorno retorno = new PendenciaFinanceiraRetorno();
		
		if(creditolimitecompra != null && creditolimitecompra.getValue().doubleValue() > 0){
			boolean passou = false;
			for (Documento documento : pendenciasDocumentos) {
				if(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null){
					creditolimitecompra = creditolimitecompra.subtract(documento.getAux_documento().getValoratual());
					if(creditolimitecompra.getValue().doubleValue() < 0){
						PendenciaTipo pendenciaTipo = this.calculaPendenciaTipo(dias_suspensao, calendario, documento.getDtvencimento(), mensagem);
						
						passou = true;
						
						if(pendenciaTipo != null){
							retorno.setPendencia(Boolean.TRUE);
							retorno.setEmail(CollectionsUtil.concatenate(listaEmail, ";"));
							retorno.setPendenciaTipo(pendenciaTipo);
							
							if(cliente != null){
								retorno.setLoginAreaCliente(cliente.getLogin());
								retorno.setSenhaAreaCliente(cliente.getSenha());
							}
							
							break;
						} else {
							retorno.setPendencia(Boolean.FALSE);
						}
					}
				}
			}
			
			if(!passou){
				retorno.setPendencia(Boolean.FALSE);
			}
		} else {
			boolean pendencia = pendenciasDocumentos != null && pendenciasDocumentos.size() > 0;
			if(pendencia){
				PendenciaTipo pendenciaTipo = this.calculaPendenciaTipo(dias_suspensao, calendario, pendenciasDocumentos.get(0).getDtvencimento(), mensagem);
				if(pendenciaTipo != null){
					retorno.setPendencia(Boolean.TRUE);
					retorno.setEmail(CollectionsUtil.concatenate(listaEmail, ";"));
					retorno.setPendenciaTipo(pendenciaTipo);
					
					if(cliente != null){
						retorno.setLoginAreaCliente(cliente.getLogin());
						retorno.setSenhaAreaCliente(cliente.getSenha());
					}
					
				} else {
					retorno.setPendencia(Boolean.FALSE);
				}
			}
		}
		
		return retorno;
	}
	
	private Money getValorPendenciaFinanceira(List<Documento> pendenciasDocumentos) {
		Money valorTotal = new Money(0.);
		if(pendenciasDocumentos != null && pendenciasDocumentos.size() > 0){
			for(Documento doc : pendenciasDocumentos){
				valorTotal = valorTotal.add(doc.getAux_documento().getValoratual());
			}
		}
		
		return valorTotal;
	}
	
	private PendenciaTipo calculaPendenciaTipo(Integer dias_suspensao, Calendario calendario, Date dtvencimento, boolean mensagem) {
		Date current_date = SinedDateUtils.currentDate();
		if(mensagem){
			current_date = SinedDateUtils.ultimoDiaUtil(current_date);
		}
		
		Integer diferencaDiasCorridos = SinedDateUtils.diferencaDias(current_date, dtvencimento);
		Integer diferencaDiasUteis = SinedDateUtils.diferencaDiasUteis(dtvencimento, current_date);
		
		PendenciaTipo pendenciaTipo = null;
		
		if(mensagem){
			if(diferencaDiasCorridos >= dias_suspensao){
				pendenciaTipo = PendenciaTipo.BLOQUEIO_GERAL;
			} else if(diferencaDiasCorridos >= (dias_suspensao / 2)){
				pendenciaTipo = PendenciaTipo.AVISO_BLOQUEIO;
			} else if(diferencaDiasUteis >= 3){
				pendenciaTipo = PendenciaTipo.AVISO_PENDENCIA;
			}
		} else {
			if(diferencaDiasCorridos.equals(dias_suspensao)){
				pendenciaTipo = PendenciaTipo.BLOQUEIO_GERAL;
			} else if(diferencaDiasCorridos.equals(dias_suspensao - 3)){
				pendenciaTipo = PendenciaTipo.AVISO_BLOQUEIO;
			} else if(diferencaDiasCorridos.equals(dias_suspensao / 2)){
				pendenciaTipo = PendenciaTipo.AVISO_PENDENCIA;
			} else if(diferencaDiasUteis.equals(3)){
				pendenciaTipo = PendenciaTipo.AVISO_PENDENCIA;
			}
		}
		return pendenciaTipo;
	}

	public Boolean verificaEmailUsuarioPendencia() {
		Object emailPendenciaObj = NeoWeb.getRequestContext().getSession().getAttribute(PENDENCIA_FINANCEIRA_EMAIL);
		String emailPendenciaStr = emailPendenciaObj != null ? emailPendenciaObj.toString() : "";
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		if(emailPendenciaStr != null && !emailPendenciaStr.equals("") && usuarioLogado != null && usuarioLogado.getEmail() != null && !usuarioLogado.getEmail().equals("")){
			String[] emails = emailPendenciaStr.split(";");
			if(emails != null && emails.length > 0){
				for (int i = 0; i < emails.length; i++) {
					if(emails[i] != null && emails[i].equals(usuarioLogado.getEmail())){
						return true;
					}
				}
			}
		}
		
		return false;
	}

	public void envioEmailCobranca() {
		Projeto projeto_w3erp = new Projeto(ParametrogeralService.getInstance().getInteger(Parametrogeral.PROJETO_W3ERP));
		Projeto projeto_sindis = new Projeto(ParametrogeralService.getInstance().getInteger(Parametrogeral.PROJETO_SINDIS));
		
		SituacaoContrato[] situacoes = new SituacaoContrato[]{
			SituacaoContrato.NORMAL, SituacaoContrato.A_CONSOLIDAR, SituacaoContrato.ATENCAO, SituacaoContrato.ATRASADO
		};
		
		Contagerencial contagerencial_mensalidade = new Contagerencial(ParametrogeralService.getInstance().getInteger(Parametrogeral.CONTA_GERENCIAL_MENSALIDADE));
		List<Cliente> listaClienteSindis = ClienteService.getInstance().findForEnvioEmailCobranca(contagerencial_mensalidade, projeto_sindis, situacoes);
		List<Cliente> listaClienteW3erp = ClienteService.getInstance().findForEnvioEmailCobranca(contagerencial_mensalidade, projeto_w3erp, situacoes);
		
		this.enviaEmailProjeto(contagerencial_mensalidade, listaClienteW3erp, "W3ERP", projeto_w3erp, projeto_sindis);
		this.enviaEmailProjeto(contagerencial_mensalidade, listaClienteSindis, "SINDIS", projeto_w3erp, projeto_sindis);
	}

	private void enviaEmailProjeto(Contagerencial contagerencial_mensalidade, List<Cliente> listaCliente, String nomeSistema, Projeto projetoW3erp, Projeto projetoSindis) {
		Integer dias_suspensao = ParametrogeralService.getInstance().getInteger(Parametrogeral.DIAS_PARA_SUSPENSAO);
//		Calendario calendario = CalendarioService.getInstance().getCalendarioGeral(false);
		
		Map<Integer, PendenciaFinanceiraRetorno> mapEnvioEmail = new HashMap<Integer, PendenciaFinanceiraRetorno>();
		Map<Integer, String> mapNomeCliente = new HashMap<Integer, String>();
		
		for (Cliente cliente : listaCliente) {
			Money creditolimitecompra = cliente.getCreditolimitecompra() != null ? new Money(cliente.getCreditolimitecompra()) : null;
			List<String> listaEmail = this.preencheEmailCliente(cliente);
			
			List<Documento> listaDocumento = ContareceberService.getInstance().getPendenciaFinanceiraByCpfCnpj(
					cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null, 
					cliente.getCpf() != null ? cliente.getCpf().getValue() : null, 
					contagerencial_mensalidade, projetoW3erp, projetoSindis, null);
			
			PendenciaFinanceiraRetorno retorno = this.getPendenciaFinanceira(creditolimitecompra, listaEmail, listaDocumento, false, null, dias_suspensao, null);
			if(retorno.getPendencia() != null && retorno.getPendencia()){
				mapEnvioEmail.put(cliente.getCdpessoa(), retorno);
				mapNomeCliente.put(cliente.getCdpessoa(), cliente.getNome());
				//Criar OS em caso de pend�ncia do tipo BloqueioGeral
				if(PendenciaTipo.BLOQUEIO_GERAL.equals(retorno.getPendenciaTipo())){
					try{
						if(listaDocumento != null && !listaDocumento.isEmpty() && listaDocumento.get(0).getRateio() != null
							&& listaDocumento.get(0).getRateio().getListaRateioitem() != null && !listaDocumento.get(0).getRateio().getListaRateioitem().isEmpty()){
							Projeto projetoAtual = null;
							for(Rateioitem rateioitem : listaDocumento.get(0).getRateio().getListaRateioitem()){
								if(rateioitem != null && (projetoSindis.equals(rateioitem.getProjeto()) || projetoW3erp.equals(rateioitem.getProjeto()))){
									projetoAtual = rateioitem.getProjeto();
									break;
								}
							}
							if(projetoAtual != null && projetoAtual.getCdprojeto() != null){
								enviaRequisicaoCriarOSInadimplencia(cliente.getCpf(), cliente.getCnpj(), projetoAtual.getCdprojeto().toString(), getValorPendenciaFinanceira(listaDocumento));
							}
						}
					}catch (Exception e) {}
				}
			}
		}
		
		Set<Entry<Integer, PendenciaFinanceiraRetorno>> entrySetMapCliente = mapEnvioEmail.entrySet();
		for (Entry<Integer, PendenciaFinanceiraRetorno> entry : entrySetMapCliente) {
			PendenciaFinanceiraRetorno valor = entry.getValue();
			
			PendenciaTipo pendenciaTipo = valor.getPendenciaTipo();
			if(pendenciaTipo != null){
				this.envioEmailPendenciaFinanceira(valor, mapNomeCliente.get(entry.getKey()), nomeSistema);
			}
		}
	}
	
	private void envioEmailPendenciaFinanceira(PendenciaFinanceiraRetorno pendenciaFinanceira, String nomeCliente, String nomeSistema) {
		try{
			TemplateManager templateEmail = null;
			
			boolean gerencia = false;
			String assunto = "ATEN��O: Pend�ncia Financeira na Linkcom";
			
			switch (pendenciaFinanceira.getPendenciaTipo()) {
				case AVISO_PENDENCIA:
					templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioEmailAvisoPendencia.tpl");
					break;
				case AVISO_BLOQUEIO:
					templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioEmailAvisoBloqueio.tpl");
					
					Date data = SinedDateUtils.currentDate();
					data = SinedDateUtils.incrementDate(data, 3, Calendar.DAY_OF_MONTH);
					templateEmail.assign("tresdiasapos", SinedDateUtils.toString(data));
					
					break;
				case BLOQUEIO_GERAL:
					templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioEmailBloqueioGeral.tpl");
					templateEmail.assign("sistema", nomeSistema);
					templateEmail.assign("cliente", nomeCliente);
					
					assunto = nomeSistema + " BLOQUEADO";
					gerencia = true;
					break;
			}
			
			String emailsDestinatarios = pendenciaFinanceira.getEmail();
			
			if(emailsDestinatarios != null && !emailsDestinatarios.equals("")){
				Empresa empresa = EmpresaService.getInstance().loadPrincipal();
				
				String corpoEmail = templateEmail.getTemplate();
				String remetente = "cobranca@linkcom.com.br";
				
				if(empresa != null && empresa.getEmail() != null && !empresa.getEmail().equals("")){
					remetente = empresa.getEmail();
				}
				
				List<String> listBCC = new ArrayList<String>();
				if(gerencia){
					if(SinedUtil.isAmbienteDesenvolvimento()){
						String emailgerente = InitialContext.doLookup("PENDENCIA_FINANCEIRA_EMAILGERENTE");
						if(emailgerente != null && !emailgerente.trim().equals("")){
							listBCC.add(emailgerente);
						}
					} else {
						listBCC.add("rodrigo.freitas@linkcom.com.br");
						listBCC.add("gerentes@linkcom.com.br");
						listBCC.add("luiz.silva@linkcom.com.br");
						listBCC.add("financeiro@linkcom.com.br");
					}
				}
				
				Envioemail envioemail = EnvioemailService.getInstance().registrarEnvioWithoutLog(remetente, assunto, corpoEmail, Envioemailtipo.COBRANCA_PENDENCIA_FINANCEIRA);
				
				List<String> enviados = new ArrayList<String>();
				String[] destinatarios = emailsDestinatarios.split(";");
				
				for (int i = 0; i < destinatarios.length; i++) {
					String destinatario = destinatarios[i];
					
					if(!enviados.contains(destinatario)){
						try{
							Envioemailitem envioemailitem = new Envioemailitem();
							envioemailitem.setEnvioemail(envioemail);
							envioemailitem.setEmail(destinatario);
							EnvioemailitemService.getInstance().saveOrUpdateWithoutLog(envioemailitem);
							
							EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
								.setEmailItemId(envioemailitem.getCdenvioemailitem())
								.setCliente("linkcom.w3erp.com.br")
								.setTo(destinatario)
								.setListBCC(listBCC)
								.setSubject(assunto)
								.setFrom(remetente)
								.addHtmlText(corpoEmail + EmailUtil.getHtmlConfirmacaoEmailWithoutContext(envioemail, destinatario, "linkcom.w3erp.com.br"));
							
							email.sendMessage();
							
							enviados.add(destinatario);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void envioEmailBoletoAutomaticoAgendado(String jobName, Boolean isJob) {
		String urlWithContext = "";
		
		if (!jobName.contains("localhost")) {
			if (isJob != null && isJob) {
				jobName = jobName.replace("Job_Email_Envio_Boleto_Agendado_", "").replace("_w3erp_PostgreSQLDS", "");
				urlWithContext = jobName + "/w3erp";
			} else {
				urlWithContext = jobName;
			}
		} else {
			urlWithContext = "localhost:8080/w3erp";
		}
		
		List<Documentoenvioboleto> listaDocumentoenvioboleto = DocumentoenvioboletoService.getInstance().findForEnvioautomatico();
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<String> listaMensagemErro = new ArrayList<String>();
		MergeReport mergeReportBoleto = null;
		
		for(Documentoenvioboleto bean: listaDocumentoenvioboleto){
			listaDocumento.addAll(ContareceberService.getInstance().findForEnvioautomaticoAgendadoBoleto(bean));
		}
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			String whereInDocumento = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
			listaDocumento.removeAll(listaDocumento);
			listaDocumento.addAll(DocumentoService.getInstance().findForBoleto(whereInDocumento));
			Map<Pessoa, List<Documento>> mapaDocs = new HashMap<Pessoa, List<Documento>>();
			for(Documento documento: listaDocumento){
				if(!mapaDocs.containsKey(documento.getPessoa())){
					mapaDocs.put(documento.getPessoa(), new ArrayList<Documento>());
				}
				mapaDocs.get(documento.getPessoa()).add(documento);
			}

			Set<Entry<Pessoa, List<Documento>>> entrySet = mapaDocs.entrySet();
			for(Entry<Pessoa, List<Documento>> entry: entrySet){
				mergeReportBoleto = null;
				Pessoa cliente = entry.getKey();
				String boletosStr = "";
				StringBuilder listaContasVencer = new StringBuilder();
				listaContasVencer.append("<table border=\"1\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse;\">");
				listaContasVencer.append("<tr><td style=\"font-weight: bold;\">Descri��o</td><td style=\"font-weight: bold;\">Vencimento</td><td style=\"font-weight: bold;\">Valor</td><td style=\"font-weight: bold;\">Link</td></tr>");
				List<Documento> listaDocsEnviados = new ArrayList<Documento>();
				for(Documento doc: entry.getValue()){
					boletosStr = "";
					listaContasVencer.append("<tr><td>"+doc.getDescricao()+"</td><td>"+SinedDateUtils.toString(doc.getDtvencimento())+"</td><td>"+doc.getValor()+"</td><td>");
					
					if(doc.getConta() != null && doc.getDocumentotipo() != null &&
						Boolean.TRUE.equals(doc.getDocumentotipo().getBoleto())){
						try {
							if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
								StringBuilder url = new StringBuilder();
								url.append(urlWithContext).append("/pub/process/BoletoPub?cddocumento=").append(doc.getCddocumento())
									.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(doc.getCddocumento().toString()))));
								
								boletosStr += "<a href=\"" + url + "\">Clique aqui</a><br>";
							} else if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
								Report report = new Report("/financeiro/relBoleto");
								report.setDataSource(new BoletoBean[]{ContareceberService.getInstance().boletoDocumento(doc, false)});
								if(mergeReportBoleto == null){
									mergeReportBoleto = new MergeReport("boleto.pdf");
								}
								mergeReportBoleto.addReport(report);
							} else {
								StringBuilder url = new StringBuilder();
								url.append(urlWithContext).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(doc.getCddocumento())
									.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(doc.getCddocumento().toString()))));
								
								boletosStr += "<a href=\"" + url + "\">Clique aqui</a><br>";
							}
							
							listaDocsEnviados.add(doc);
						} catch (Exception e) {
							e.printStackTrace();
							listaMensagemErro.add("N�o foi poss�vel gerar boleto do documento " + doc.getNumeroCddocumento());
						}
					}
					
					listaContasVencer.append(boletosStr).append("</td></tr>");
				}
				
				if(org.apache.commons.lang.StringUtils.isEmpty(boletosStr) && mergeReportBoleto == null) {
					continue;
				}
				
				listaContasVencer.append("</table>");
				Documento doc = entry.getValue().get(0);
				String nomeResponsavel = doc.getEmpresa().getResponsavel() != null? doc.getEmpresa().getResponsavel().getNome(): "";
				String nomeEmpresa = doc.getEmpresa().getNome();
				String emailEmpresa = doc.getEmpresa().getEmail();
				String telefonesEmpresa = doc.getEmpresa().getTelefones();
				
				TemplateManager template = new TemplateManager("/WEB-INF/template/templateEnvioEmailBoletoAutomaticoAgendado.tpl")
						.assign("nomeCliente", cliente.getNome())
						.assign("listaContasVencer", listaContasVencer.toString())
						.assign("nomeResponsavel", nomeResponsavel)
						.assign("nomeEmpresa", nomeEmpresa)
						.assign("emailEmpresa", emailEmpresa)
						.assign("telefonesEmpresa", telefonesEmpresa);
				List<Resource> anexos = new ArrayList<Resource>();
				
				if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
					try {
						anexos.add(mergeReportBoleto.generateResource());
					} catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro ao anexar boleto no e-mail do cliente " + entry.getKey().getCdpessoa() + ": " + e.getMessage());
						continue;
					}
				}
				
				List<String> listBCC = new ArrayList<String>();
				listBCC.add(cliente.getEmail());
				List<Contato> listaContato = ContatoService.getInstance().findByPessoa(cliente, true);
				for(Contato contato: listaContato){
					if(org.apache.commons.lang.StringUtils.isNotBlank(contato.getEmailcontato())){
						listBCC.add(contato.getEmailcontato());
					}
				}
				
				for (String email : listBCC) {
					EmailManager emailmanager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
					try{
						emailmanager
								.setFrom(emailEmpresa)
								.setSubject("Envio de boleto")
								.setTo(email);
					} catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro ao criar e-mail autom�tico de envio de boleto agendado: " + e.getMessage());
						continue;
					}
					
					//REGISTRO DE E-MAIL DE COBRAN�A
					EmailCobranca emailCobranca = new EmailCobranca(email, cliente.getCdpessoa(), Boolean.TRUE);
					
					try {
						List<EmailCobrancaDocumento> listaEmailCobrancaDocumento = new ArrayList<EmailCobrancaDocumento>();
						for (Documento documento : entry.getValue()) {
							listaEmailCobrancaDocumento.add(new EmailCobrancaDocumento(documento));
						}
						emailCobranca.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
						
						EmailCobrancaService.getInstance().saveOrUpdate(emailCobranca);
					} catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro ao registrar e-mail de cobran�a do cliente " + entry.getKey().getCdpessoa() + ": " + e.getMessage());
						continue;
					}
					
					if(emailCobranca != null){
						emailmanager.setEmailId(emailCobranca.getCdemailcobranca());
					}
					
	//				ENVIO DE E-MAIL
					try{
						emailmanager.addHtmlText(template.getTemplate());
						for (Resource anexo : anexos) {
							emailmanager.attachFileUsingByteArray(anexo.getContents(), anexo.getFileName(), anexo.getContentType(), anexo.getFileName());
						}
						
						emailmanager.sendMessage();
						
						EmailCobrancaService.getInstance().updateEmailCobranca(emailCobranca);
					} catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro no envio de e-mail do cliente " + entry.getKey().getCdpessoa() + ": " + e.getMessage());
						continue;
					}
				}
				
				for(Documento docEnviado: listaDocsEnviados){
					Documentohistorico historico = DocumentohistoricoService.getInstance().geraHistoricoDocumento(docEnviado);
					historico.setDocumentoacao(Documentoacao.COBRANCA_DEBITO);
					historico.setObservacao("Cobran�a autom�tica de boleto conforme agendamento.");
					DocumentohistoricoService.getInstance().saveOrUpdate(historico);
				}
			}
		}
		DocumentoenvioboletoService.getInstance().concluiAgendamentosVencidos();
	}

	public NovidadeVersaoRetorno doNovidadeVersao(HttpServletRequest request, Boolean consultaMenu, Boolean novidadePeriodoAtual, NovidadeVersao novidadeVersao) {
		try{
			NovidadeVersaoRetorno retorno = enviaRequisicaoNovidadeVersao(consultaMenu, novidadePeriodoAtual, novidadeVersao);
			return retorno;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public NovidadeVersaoRetorno enviaRequisicaoNovidadeVersao (Boolean consultaMenu, Boolean novidadePeriodoAtual, NovidadeVersao novidadeVersao) {
		try{
			URL url = montaURLRequisicao();
			String data = montaDadosBasicosRequisicaoNovidadeVersao(consultaMenu, novidadePeriodoAtual, novidadeVersao, "retornaNovidadeVersao");		
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); 
			wr.flush(); 
			
			conn.getInputStream();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line; 
			String xml = ""; 
			
			while ((line = rd.readLine()) != null) { xml += line; } 
			wr.close(); 
			rd.close(); 
			
			return verificaXmlNovidadeVersao(xml);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	public List<NovidadeVersao> doVersoesComNovidade(HttpServletRequest request) {
		try{
			List<NovidadeVersao> retorno = enviaRequisicaoListaTodasVersoes(false);
			return retorno;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<NovidadeVersao> enviaRequisicaoListaTodasVersoes (Boolean consultaMenu) {	
		try{
			URL url = montaURLRequisicao();
			String data = montaDadosBasicosRequisicaoNovidadeVersao(consultaMenu, false, null, "retornaVersoesComNovidade");		
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); 
			wr.flush(); 
			
			conn.getInputStream();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line; 
			String xml = ""; 
			
			while ((line = rd.readLine()) != null) { xml += line; } 
			wr.close(); 
			rd.close(); 
			
			return verificaXmlVersoesComNovidade(xml);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}

	private String montaDadosBasicosRequisicaoNovidadeVersao(Boolean consultaMenu, Boolean novidadePeriodoAtual, NovidadeVersao novidadeVersao, String acao) throws UnsupportedEncodingException{
		// Adicionando o par�metro de A��o para o IWS
		String assinatura = "12345";
		String projeto = "W3ERP";
		
		String data = URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode(acao, "LATIN1");
		data += "&" + URLEncoder.encode("projeto", "LATIN1") + "=" + projeto;
		if(novidadeVersao != null && novidadeVersao.getCdnovidadeversao() != null){
			data += "&" + URLEncoder.encode("cdnovidadeVersao", "LATIN1") + "=" + novidadeVersao.getCdnovidadeversao();			
		}
		data += "&" + URLEncoder.encode("consultaMenu", "LATIN1") + "=" + consultaMenu;
		data += "&" + URLEncoder.encode("novidadePeriodoAtual", "LATIN1") + "=" + novidadePeriodoAtual;
		data += "&" + URLEncoder.encode("hash", "LATIN1") + "=" + assinatura;
		return data;
	}
	
	
	@SuppressWarnings("unchecked")
	private NovidadeVersaoRetorno verificaXmlNovidadeVersao(String xml) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		
		String strNovo = new String(new StringUtils().toString(xml).getBytes(),"UTF-8");
		SAXBuilder sb = new SAXBuilder();
		Document doc = sb.build(new ByteArrayInputStream(strNovo.getBytes()));	
		
		Element descricao = SinedUtil.getChildElement("descricao", doc.getRootElement().getContent());
		Element dtFim = SinedUtil.getChildElement("dtfim", doc.getRootElement().getContent());
		Element versao = SinedUtil.getChildElement("versao", doc.getRootElement().getContent());
		Element ultimaVersao = SinedUtil.getChildElement("ultimaVersao", doc.getRootElement().getContent());
		List<Element> listaFuncionalidades = SinedUtil.getListChildElement("funcionalidades", doc.getRootElement().getContent());
		
		NovidadeVersaoRetorno retorno = new NovidadeVersaoRetorno();
		List<NovidadeVersaoFuncionalidade> lista = new ArrayList<NovidadeVersaoFuncionalidade>();
		
		for (Element fn : listaFuncionalidades) {
			Segmento segmento = new Segmento();
			NovidadeVersaoFuncionalidade novidadeVersaoFuncionalidade = new NovidadeVersaoFuncionalidade();
			
			Element titulo = SinedUtil.getChildElement("titulo", fn.getContent());
			Element funcionalidade = SinedUtil.getChildElement("funcionalidade", fn.getContent());
			Element modulo = SinedUtil.getChildElement("modulo", fn.getContent());
			Element segmentoElement = SinedUtil.getChildElement("segmento", fn.getContent());
			Element urlBaseConhecimento = SinedUtil.getChildElement("urlBaseConhecimento", fn.getContent());
			Element urlVideo = SinedUtil.getChildElement("urlVideo", fn.getContent());
			
			novidadeVersaoFuncionalidade.setTitulo(titulo.getText());
			novidadeVersaoFuncionalidade.setFuncionalidade(funcionalidade.getText());
			novidadeVersaoFuncionalidade.setUrlBaseConhecimento(urlBaseConhecimento.getText());
			novidadeVersaoFuncionalidade.setUrlVideo(urlVideo.getText());
	
			segmento.setNome(segmentoElement.getText());
			
			novidadeVersaoFuncionalidade.setModulo(modulo.getText());
			novidadeVersaoFuncionalidade.setSegmento(segmento);
			
			lista.add(novidadeVersaoFuncionalidade);

		}

		if(lista.size() > 0){
			retorno.setListaNovidadeVersaoFuncionalidade(lista);	
		}
		retorno.setDescricao(descricao != null ? descricao.getText() : null);
		retorno.setVersao(versao != null ? versao.getText() : null);
		retorno.setDtFim(dtFim != null ? dtFim.getText(): null);
		retorno.setUltimaVersao(ultimaVersao != null && "true".equals(ultimaVersao.getText()) ? true : false);
		
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	private List<NovidadeVersao> verificaXmlVersoesComNovidade(String xml) throws JDOMException, IOException {
		String strNovo = new String(new StringUtils().toString(xml).getBytes(),"UTF-8");
		SAXBuilder sb = new SAXBuilder();
		Document doc = sb.build(new ByteArrayInputStream(strNovo.getBytes()));
		
		List<NovidadeVersao> listaNovidadeVersao = new ArrayList<NovidadeVersao>();
		
		List<Element> listaVersoes = SinedUtil.getListChildElement("listaVersoes", doc.getRootElement().getContent());
		for (Element element : listaVersoes) {
			
			NovidadeVersao novidadeVersao = new NovidadeVersao();
			
			Element cdnovidadeversao = SinedUtil.getChildElement("cdnovidadeversao", element.getContent());
			Element versao = SinedUtil.getChildElement("versao", element.getContent());
			
			novidadeVersao.setCdnovidadeversao(Integer.parseInt(cdnovidadeversao.getText()));
			novidadeVersao.setVersao(versao.getText());
			
			listaNovidadeVersao.add(novidadeVersao);
		}
		
		return listaNovidadeVersao;
		
	}

	enum PendenciaTipo {
		AVISO_PENDENCIA, AVISO_BLOQUEIO, BLOQUEIO_GERAL, SEM_CONTRATO_ATIVO
	}
	
	class PendenciaFinanceiraRetorno {
		
		private Boolean pendencia;
		private String email;
		private PendenciaTipo pendenciaTipo;
		private CategoriaClienteLogin categoriaClienteLogin;
		private String loginAreaCliente;
		private String senhaAreaCliente;
		
		public Boolean getPendencia() {
			return pendencia;
		}
		public String getEmail() {
			return email;
		}
		public PendenciaTipo getPendenciaTipo() {
			return pendenciaTipo;
		}
		public CategoriaClienteLogin getCategoriaClienteLogin() {
			return categoriaClienteLogin;
		}
		public String getLoginAreaCliente() {
			return loginAreaCliente;
		}
		public String getSenhaAreaCliente() {
			return senhaAreaCliente;
		}
		public void setCategoriaClienteLogin(
				CategoriaClienteLogin categoriaClienteLogin) {
			this.categoriaClienteLogin = categoriaClienteLogin;
		}
		public void setPendencia(Boolean pendencia) {
			this.pendencia = pendencia;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public void setPendenciaTipo(PendenciaTipo pendenciaTipo) {
			this.pendenciaTipo = pendenciaTipo;
		}
		public void setLoginAreaCliente(String loginAreaCliente) {
			this.loginAreaCliente = loginAreaCliente;
		}
		public void setSenhaAreaCliente(String senhaAreaCliente) {
			this.senhaAreaCliente = senhaAreaCliente;
		}
	}
		
	public NotaErroCorrecaoRetorno enviaRequisicaoNotaErroCorrecao (Notaerrocorrecao notaerrocorrecao) {	
		try{
			URL url = montaURLRequisicao();
			String data = montaDadosBasicosRequisicao(null, null, "verificaNotaErroCorrecao");					
			if(notaerrocorrecao.getCodigo() != null){
				data += "&" + URLEncoder.encode("codigo", "LATIN1") + "=" + URLEncoder.encode(notaerrocorrecao.getCodigo(), "LATIN1");
			}
			URLConnection conn = url.openConnection(); 
			conn.setDoOutput(true); 
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
			wr.write(data); 
			wr.flush(); 
			
			conn.getInputStream();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			String line; 
			String xml = ""; 
			
			while ((line = rd.readLine()) != null) { xml += line; } 
			
			wr.close(); 
			rd.close(); 
			
			return verificaXmlNotaErroCorrecao(xml);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
		
	public Notaerrocorrecao doVerificaNotaErroCorrecao(HttpServletRequest request, Notaerrocorrecao notaerrocorrecao) {
		try{
			NotaErroCorrecaoRetorno retorno = new NotaErroCorrecaoRetorno();
			
			retorno = enviaRequisicaoNotaErroCorrecao(notaerrocorrecao);

			String mensagem = retorno != null && retorno.getMensagem() != null ? retorno.getMensagem() : null;
			String url = retorno != null && retorno.getUrl() != null ? retorno.getUrl() : null;
			String correcao = retorno != null && retorno.getCorrecao() != null ? retorno.getCorrecao() : null;
			
			notaerrocorrecao.setMensagem(mensagem);
			notaerrocorrecao.setUrl(url);
			notaerrocorrecao.setCorrecao(correcao);
			
			return notaerrocorrecao;
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private NotaErroCorrecaoRetorno verificaXmlNotaErroCorrecao(String xml) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		
		String strNovo = new String(new StringUtils().toString(xml).getBytes(),"UTF-8");
		SAXBuilder sb = new SAXBuilder();
		Document doc = sb.build(new ByteArrayInputStream(strNovo.getBytes()));
		
		Element mensagem = SinedUtil.getChildElement("mensagem", doc.getRootElement().getContent());
		Element url = SinedUtil.getChildElement("url", doc.getRootElement().getContent());
		Element correcao = SinedUtil.getChildElement("correcao", doc.getRootElement().getContent());
		
		NotaErroCorrecaoRetorno retorno = new NotaErroCorrecaoRetorno();
		
		if(mensagem != null){
			retorno.setMensagem(mensagem.getText());	
		}
		if(url != null) {
			retorno.setUrl(url.getText());
		}
		if(correcao != null) {
			retorno.setCorrecao(correcao.getText());
		}
		return retorno;
	}
		
	class NotaErroCorrecaoRetorno {
		
		private String mensagem;
		private String url;
		private String correcao;
		
		public String getCorrecao() {
			return correcao;
		}
		public void setCorrecao(String correcao) {
			this.correcao = correcao;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getMensagem() {
			return mensagem;
		}
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		
	}
	
}
