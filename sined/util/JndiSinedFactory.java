package br.com.linkcom.sined.util;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.jndi.JndiTemplate;

import br.com.linkcom.neo.exception.NotInNeoContextException;

/**
 * Classe para gerenciar a conex�o com o banco de acordo url.
 * 
 * @since 09/04/2008
 * @author Fl�vio Tavares
 *
 */
public class JndiSinedFactory implements FactoryBean {

	JndiTemplate jndiTemplate = new JndiTemplate();
	
	private DataSource getCurrentDataSource() {
		try {
			boolean forceWrite = SinedUtil.isForceWriteByHourAndDayWeek();
			return (DataSource) jndiTemplate.lookup(getJndiName(forceWrite), getObjectType());
		} catch (NameNotFoundException e) {
			try {
				return (DataSource) jndiTemplate.lookup(getJndiName(true), getObjectType());
			} catch (NameNotFoundException e2) {
				throw new DatasourceException("[W3ERP][ERRO] DataSource n�o encontrado para o dominio: ", e2);
			} catch (NamingException e2) {
				throw new DatasourceException("Erro ao conseguir conex�o com o banco de dados!",e2);
			}
		} catch (NamingException e) {
			try{
				return (DataSource) jndiTemplate.lookup(getJndiName(true), getObjectType());
			} catch (NameNotFoundException e2) {
				throw new DatasourceException("[W3ERP][ERRO] DataSource n�o encontrado para o dominio: ", e2);
			} catch (NamingException e2) {
				throw new DatasourceException("Erro ao conseguir conex�o com o banco de dados!",e2);
			}
		}
	}
	
	private String getJndiName(boolean forceWriter) {
		String threadName = new String(Thread.currentThread().getName());
		boolean reader = false;
		if(threadName.endsWith("-reader")){
			reader = true;
			SinedUtil.desmarkAsReader();
			threadName = threadName.replaceAll("-reader", "");
		}
		
		if(forceWriter) reader = false;
//		System.out.println("USE " + (reader ? " READER" : (forceWriter ? " FORCE " : "") + " WRITER ################"));
		
		String dataSouce = SinedUtil.getJndiByThreadName(threadName);
		if(!StringUtils.isBlank(dataSouce)){
			return "java:/"+ dataSouce + (reader ? "_Reader" : "");
		}
		
		try {
			return "java:/"+ SinedUtil.getStringConexaoBanco() + (reader ? "_Reader" : "");
		} catch (NotInNeoContextException e) {
			return "java:/w3erp_PostgreSQLDS";
		}
	}

	public Object getObject() throws Exception {
		DataSource source = new DataSource(){

			public Connection getConnection() throws SQLException {
				return getCurrentDataSource().getConnection();
			}

			public Connection getConnection(String username, String password) throws SQLException {
				return getCurrentDataSource().getConnection(username, password);
			}

			public PrintWriter getLogWriter() throws SQLException {
				return getCurrentDataSource().getLogWriter();
			}

			public int getLoginTimeout() throws SQLException {
				return getCurrentDataSource().getLoginTimeout();
			}

			public void setLogWriter(PrintWriter out) throws SQLException {
				getCurrentDataSource().setLogWriter(out);
				
			}

			public void setLoginTimeout(int seconds) throws SQLException {
				getCurrentDataSource().setLoginTimeout(seconds);
				
			}

			public boolean isWrapperFor(Class<?> iface) throws SQLException {
				return getCurrentDataSource().isWrapperFor(iface);
			}

			public <T> T unwrap(Class<T> iface) throws SQLException {
				return getCurrentDataSource().unwrap(iface);
			}
			
		};
		return source;
	}

	public Class<?> getObjectType() {
		return DataSource.class;
	}

	public boolean isSingleton() {
		return true;
	}

	
}
