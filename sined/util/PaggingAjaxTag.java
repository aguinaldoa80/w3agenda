/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util;

import br.com.linkcom.neo.view.BaseTag;

public class PaggingAjaxTag extends BaseTag {

	protected Integer currentPage;
	protected Integer totalLinksPaginacao = 9;
	protected Integer totalNumberOfPages;
	protected String selectedClass;
	protected String unselectedClass;	
	protected String funcaoJs;	
	
	@Override
	protected void doComponent() throws Exception {
		
		int start = Math.max(1, currentPage + 1 - 4) - 1;
		int fim = Math.min(totalLinksPaginacao - (currentPage - start) + currentPage, totalNumberOfPages);
		boolean fim3pontos = fim < totalNumberOfPages;
		boolean start3pontos = start != 0;
		
		if (start3pontos){
			getOut().print("...&nbsp;");
		}
		
		for (int i=start; i<fim; i++){
			if (i==currentPage){
				String cs = selectedClass != null? " class=\"" + selectedClass + "\"" : "";
				getOut().print("<span" + cs + "style=\"color:#000000\">" + (i+1) + "</span> ");
			}else {
				String cs = unselectedClass !=null ? " class=\"" + unselectedClass + "\"" : "";
				getOut().print("<a href=\"javascript:" + funcaoJs + "(" + i + ")" + "\" " + cs + ">" + (i+1) + "</a> ");
			}
		}
		
		if (fim3pontos){
			getOut().print("...&nbsp;");
		}
		
		getOut().print("<span id=\"spanPaginacao\" style=\"vertical-align:top\"></span> ");
	}
	
	public Integer getCurrentPage() {
		return currentPage;
	}
	public String getSelectedClass() {
		return selectedClass;
	}
	public Integer getTotalNumberOfPages() {
		return totalNumberOfPages;
	}
	public String getUnselectedClass() {
		return unselectedClass;
	}
	public Integer getTotalLinksPaginacao() {
		return totalLinksPaginacao;
	}
	public String getFuncaoJs() {
		return funcaoJs;
	}
	
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public void setSelectedClass(String selectedClass) {
		this.selectedClass = selectedClass;
	}
	public void setTotalNumberOfPages(Integer totalNumberOfPages) {
		this.totalNumberOfPages = totalNumberOfPages;
	}
	public void setUnselectedClass(String unselectedClass) {
		this.unselectedClass = unselectedClass;
	}
	public void setTotalLinksPaginacao(Integer totalLinksPaginacao) {
		this.totalLinksPaginacao = totalLinksPaginacao;
	}
	public void setFuncaoJs(String funcaoJs) {
		this.funcaoJs = funcaoJs;
	}	
}