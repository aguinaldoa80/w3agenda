package br.com.linkcom.sined.util.bean;


public class LocalarmazenagemBean {

	protected int id;
	protected boolean permitirestoquenegativo;

    public LocalarmazenagemBean() {}
    
    public LocalarmazenagemBean(int id) {
        super();
        this.id = id;
        this.permitirestoquenegativo = false;
    }
    
    public LocalarmazenagemBean(int id, boolean permitirestoquenegativo) {
        super();
        this.id = id;
        this.permitirestoquenegativo = permitirestoquenegativo;
    }

    public int getId() {
		return id;
	}

	public boolean isPermitirestoquenegativo() {
		return permitirestoquenegativo;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPermitirestoquenegativo(boolean permitirestoquenegativo) {
		this.permitirestoquenegativo = permitirestoquenegativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalarmazenagemBean other = (LocalarmazenagemBean) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
