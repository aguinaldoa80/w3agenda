/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util.bean;

import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public class GenericBean {

	protected Object id;
	protected Object value;

    public GenericBean() {}
    
    public GenericBean(Object id, Object value) {
        super();
        this.id = id;
        this.value = value;
    }
    
    /*public <T> GenericBean(Object id){
    	
    }*/


    @Id
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	@DescriptionProperty
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}

    @Override
    public boolean equals(Object obj) {
        if (obj ==  null || !( obj instanceof GenericBean) || ((GenericBean) obj).getId()==null || getId()==null){
            return false;
        }else{
            return ((GenericBean)obj).getId().equals(getId());
        }
    }
	
}
