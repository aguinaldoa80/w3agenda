package br.com.linkcom.sined.util.bean;



public class Apontamentotarefaatraso {
	
	private Integer cdtarefa;
	private Integer cdpessoa;
	private String pessoaNome;
	private String email;	
	private String responsavelNome; 
	private String responsavelEmail;
	private Integer cdprojeto;
	
	
	
	public Apontamentotarefaatraso(Integer cdtarefa, Integer cdpessoa,
			String pessoaNome, String email, String responsavelNome, String responsavelEmail, Integer cdprojeto) {
		super();
		this.cdtarefa = cdtarefa;
		this.cdpessoa = cdpessoa;
		this.pessoaNome = pessoaNome;
		this.email = email;
		this.responsavelNome = responsavelNome;
		this.responsavelEmail = responsavelEmail;
		this.cdprojeto = cdprojeto;
	}
	
	
	public Integer getCdtarefa() {
		return cdtarefa;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getPessoaNome() {
		return pessoaNome;
	}
	public String getEmail() {
		return email;
	}
	public String getResponsavelNome() {
		return responsavelNome;
	}
	public String getResponsavelEmail() {
		return responsavelEmail;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}

	public void setCdtarefa(Integer cdtarefa) {
		this.cdtarefa = cdtarefa;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setPessoaNome(String pessoaNome) {
		this.pessoaNome = pessoaNome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setResponsavelNome(String responsavelNome) {
		this.responsavelNome = responsavelNome;
	}
	public void setResponsavelEmail(String responsavelEmail) {
		this.responsavelEmail = responsavelEmail;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}



}
