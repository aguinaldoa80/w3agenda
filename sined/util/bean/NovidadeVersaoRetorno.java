package br.com.linkcom.sined.util.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.NovidadeVersaoFuncionalidade;

public class NovidadeVersaoRetorno {
	
	protected String descricao;
	protected String dtFim;
	protected String versao;
	private List<NovidadeVersaoFuncionalidade> listaNovidadeVersaoFuncionalidade;
	private Boolean existeNovidade;
	private Boolean existeNovidadePeriodo;
	private Boolean ultimaVersao;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDtFim() {
		return dtFim;
	}
	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}
	public String getVersao() {
		return versao;
	}
	public void setVersao(String versao) {
		this.versao = versao;
	}
	public List<NovidadeVersaoFuncionalidade> getListaNovidadeVersaoFuncionalidade() {
		return listaNovidadeVersaoFuncionalidade;
	}
	public void setListaNovidadeVersaoFuncionalidade(
			List<NovidadeVersaoFuncionalidade> listaNovidadeVersaoFuncionalidade) {
		this.listaNovidadeVersaoFuncionalidade = listaNovidadeVersaoFuncionalidade;
	}
	public Boolean getExisteNovidade() {
		return existeNovidade;
	}
	public Boolean getExisteNovidadePeriodo() {
		return existeNovidadePeriodo;
	}
	public void setExisteNovidade(Boolean existeNovidade) {
		this.existeNovidade = existeNovidade;
	}
	public void setExisteNovidadePeriodo(Boolean existeNovidadePeriodo) {
		this.existeNovidadePeriodo = existeNovidadePeriodo;
	}
	public Boolean getUltimaVersao() {
		return ultimaVersao;
	}
	public void setUltimaVersao(Boolean ultimaVersao) {
		this.ultimaVersao = ultimaVersao;
	}
}
