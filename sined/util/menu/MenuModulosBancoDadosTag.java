package br.com.linkcom.sined.util.menu;

import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.view.BaseTag;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.geral.service.DashboardespecificoService;
import br.com.linkcom.sined.geral.service.MenumoduloService;
import br.com.linkcom.sined.util.SinedUtil;

public class MenuModulosBancoDadosTag extends BaseTag {
	
	public static String MODULOS_CACHE = MenuModulosBancoDadosTag.class.getName() + "_cache";
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doComponent() throws Exception {
		List<Menumodulo> listaModulos = (List<Menumodulo>)getRequest().getSession().getAttribute(MODULOS_CACHE);
		if(listaModulos == null){
			listaModulos = MenumoduloService.getInstance().findForTag(SinedUtil.getContexto());
			getRequest().getSession().setAttribute(MODULOS_CACHE, listaModulos);
		}

		pushAttribute("listaModulos", listaModulos);
		includeJspTemplate();
		popAttribute("listaModulos");
	}

}
