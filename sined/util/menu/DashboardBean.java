package br.com.linkcom.sined.util.menu;

public class DashboardBean {

	private Integer cddashboard;
	private String nome;
	private String url;
	private Integer id;
	private String usuariosistema;
	private String senhasistema;
	private Boolean geral = Boolean.FALSE;
	
	public String getNome() {
		return nome;
	}
	public String getUrl() {
		return url;
	}
	public Integer getId() {
		return id;
	}
	public String getUsuariosistema() {
		return usuariosistema;
	}
	public String getSenhasistema() {
		return senhasistema;
	}
	public Boolean getGeral() {
		return geral;
	}
	public Integer getCddashboard() {
		return cddashboard;
	}
	public void setCddashboard(Integer cddashboard) {
		this.cddashboard = cddashboard;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setUsuariosistema(String usuariosistema) {
		this.usuariosistema = usuariosistema;
	}
	public void setSenhasistema(String senhasistema) {
		this.senhasistema = senhasistema;
	}
	public void setGeral(Boolean geral) {
		this.geral = geral;
	}
	
}
