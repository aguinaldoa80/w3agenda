<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<script>
	function carregaMenuModulo(element, cdmenumodulo) {
		if(navigator.onLine){
			$('.headerbar .menu').html("<table cellspacing=\"0\" cellpadding=\"0\"><tr><td><img height=\"27\" src=\"${ctx}/imagens/loading.gif\"/></td><td style=\"vertical-align: middle;\">Carregando...</td></tr></table>");
		}else {
			$('.headerbar .menu').html("<table cellspacing=\"0\" cellpadding=\"0\"><tr><td><img height=\"12\" src=\"${ctx}/imagens/icon_warning.gif\"/></td><td style=\"vertical-align: middle;\">&nbsp;&nbsp;N�o foi poss�vel carregar o menu. O dispositivo est� offline.</td></tr></table>");
		}
		
		// Remove a marca��o de "selecionado" de todas as abas dos m�dulos
		$("#tabs6 ul li").each(function() {
			$(this).removeClass("current");
			$(this).find('.labelmenumoduloclass').css('border-bottom', '0px');
			var img = $(this).find('.menumoduloclass');
			img.attr('src', img.attr('text'));
		});
	
		// Adiciona a marca��o de "selecionado" na aba em quest�o
		$(element).parent().addClass("current");
		$(element).find('.labelmenumoduloclass').css('border-bottom', 'solid 1px #4e5767');
		var img = $(element).find('.menumoduloclass');
		img.attr('src', $(img).attr('text').replace('.png', '_over.png', 'g'));
		
		$.post(ctx + "/sistema/process/GerenciaMenu", {'ACAO':'ajaxLoadMenu', 'cdmenumodulo': cdmenumodulo}, 
			function(data){
				($('.headerbar .menu').html(data));
			}		
		);
		
		$('.lineMenuBottomClass').hide();
	}
	
	$(document).ready(function(){
		$('.linkmenumoduloclass').each(function(){
			var img = $(this).find('.menumoduloclass')
			var label = $(this).find('.labelmenumoduloclass')
			this.addEventListener('mouseout', function(event){
				if(img.closest('li').hasClass('current')!=true){
					img.attr('src', img.attr('text'));
					label.css('border-bottom', '0px');
				}
			});
			this.addEventListener('mouseover', function(event){
				img.attr('src', img.attr('text').replace('.png', '_over.png', 'g'));
				label.css('border-bottom', 'solid 1px  #4e5767');
			});
		});
	});
</script>

<ul>
	<c:forEach items="${listaModulos}" var="modulo">
		<c:if test="${s:getmodulopermissao(modulo.urlprefixoPermissao) || (modulo.dashboard != null && modulo.dashboard)}">
			<c:choose>
				<c:when test="${!empty modulo.eventoclick}">
					<li>
						<div onclick="${modulo.eventoclick}" class="linkmenumoduloclass">
							<div class="imgmenumoduloclass">
								<img src="${ctx}/imagens/${modulo.urlprefixo}.png" text="${ctx}/imagens/${modulo.urlprefixo}.png" class="menumoduloclass"/>
							</div>
							<div class="labelmenumoduloclass">
								<span>${modulo.nome}</span>
							</div>
						</div>
					</li>
				</c:when>
				<c:otherwise>
					<li class="${NEO_MODULO == modulo.urlprefixo ? 'current' : ''}">
						<div onclick="javascript:carregaMenuModulo(this,'${modulo.cdmenumodulo}')" class="linkmenumoduloclass">
							<div class="imgmenumoduloclass">
								<c:if test="${NEO_MODULO == modulo.urlprefixo}">
									<img src="${ctx}/imagens/${modulo.urlprefixo}_over.png" text="${ctx}/imagens/${modulo.urlprefixo}.png" class="menumoduloclass"/>
								</c:if>
								<c:if test="${NEO_MODULO != modulo.urlprefixo}">
									<img src="${ctx}/imagens/${modulo.urlprefixo}.png" text="${ctx}/imagens/${modulo.urlprefixo}.png" class="menumoduloclass"/>
								</c:if>
							</div>
    						<div class="labelmenumoduloclass">
								<span>${modulo.nome}</span>
                             </div>
						</div>
					</li>
				</c:otherwise>
			</c:choose>
		</c:if>
	</c:forEach>
</ul>
