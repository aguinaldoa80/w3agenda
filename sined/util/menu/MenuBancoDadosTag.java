package br.com.linkcom.sined.util.menu;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import br.com.linkcom.neo.view.BaseTag;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.geral.service.MenumoduloService;
import br.com.linkcom.sined.util.SinedUtil;


public class MenuBancoDadosTag extends BaseTag {
	
	private String urlprefixo;
	
	public String getUrlprefixo() {
		return urlprefixo;
	}
	
	public void setUrlprefixo(String urlprefixo) {
		this.urlprefixo = urlprefixo;
	}
	
	@Override
	public void doComponent() throws JspException, IOException {
		Menumodulo menumodulo = null;
		
		if(urlprefixo != null){
			menumodulo = MenumoduloService.getInstance().findByPrefixo(urlprefixo, SinedUtil.getContexto());
		}
		if(menumodulo != null){
			StringBuilder sbMenu = new MenuBancoDadosUtil().makeMenu(menumodulo);
			getOut().print(sbMenu.toString());
		}
	}

}
