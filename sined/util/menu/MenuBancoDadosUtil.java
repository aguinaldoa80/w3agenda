package br.com.linkcom.sined.util.menu;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.AuthorizationManager;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.menu.Menu;
import br.com.linkcom.neo.view.menu.MenuBuilder;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.geral.service.DashboardespecificoService;
import br.com.linkcom.sined.geral.service.MenuService;
import br.com.linkcom.sined.geral.service.NovidadeVersaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.GrafanaCliente;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.GrafanaDashboard;
import br.com.linkcom.sined.util.GrafanaUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.NovidadeVersaoRetorno;

public class MenuBancoDadosUtil {
	
	public static final String MENU_CACHE_MAP = MenuBancoDadosTag.class.getName()+"_cache";
	
	@SuppressWarnings("unchecked")
	public StringBuilder makeMenu(Menumodulo menumodulo) {
		String menuCode = null;
		
		Map<Menumodulo, String> menuCacheMap = (Map<Menumodulo, String>)NeoWeb.getRequestContext().getServletRequest().getSession().getAttribute(MENU_CACHE_MAP);
		if(menuCacheMap == null){
			menuCacheMap = new HashMap<Menumodulo, String>();
			NeoWeb.getRequestContext().getServletRequest().getSession().setAttribute(MENU_CACHE_MAP, menuCacheMap);
		}
		String cachedCode = menuCacheMap.get(menumodulo);
		
		if(cachedCode == null){
			Menu menu = null;
			String contextPath = NeoWeb.getRequestContext().getServletRequest().getContextPath();
			if(menumodulo.getDashboard() != null && menumodulo.getDashboard()){
				menu = new Menu();
				
				Boolean exibirDashboardsAntigos = ParametrogeralService.getInstance().getBoolean("EXIBIR_DASHBOARDS_ANTIGOS");
				
				NovidadeVersaoRetorno novidadeVersaoRetorno = NovidadeVersaoService.getInstance().existeNovidadeVersao();
				
				if(novidadeVersaoRetorno != null && novidadeVersaoRetorno.getExisteNovidade() != null){
					NeoWeb.getRequestContext().getServletRequest().getSession().setAttribute(SinedUtil.EXISTE_NOVIDADE_VERSAO, novidadeVersaoRetorno.getExisteNovidade());					
				}
				if(novidadeVersaoRetorno != null && novidadeVersaoRetorno.getExisteNovidadePeriodo() != null){
					NeoWeb.getRequestContext().getServletRequest().getSession().setAttribute(SinedUtil.EXISTE_NOVIDADE_VERSAO_PERIODO, novidadeVersaoRetorno.getExisteNovidadePeriodo());					
				}
				
				List<GrafanaDashboard> listaDashboardGrafana = null;
				GrafanaCliente grafanaCliente = null;
				if(SinedUtil.isUserHasAction("ACESSO_DASHBOARD_GRAFANA")){
					grafanaCliente = GrafanaUtil.getGrafanaCliente();
					if(grafanaCliente != null){
						listaDashboardGrafana = grafanaCliente.getListaDashboard();
					}
				}
				boolean hasDashboardGrafana = listaDashboardGrafana != null && listaDashboardGrafana.size() > 0;
				
				menu.addMenu(new Menu("Tela inicial", contextPath + "/adm/index?isDashboardAntigo=true"));
				
				if (exibirDashboardsAntigos) {
					List<DashboardBean> listaDashboard = DashboardespecificoService.getInstance().findAllDashboard(!hasDashboardGrafana);
					
					if(listaDashboard != null && listaDashboard.size() > 0){
						for (DashboardBean dashboardBean : listaDashboard) {
							menu.addMenu(new Menu(dashboardBean.getNome(), contextPath + "/adm/index?cdDashboard=" + dashboardBean.getCddashboard() + "&geralDashboard=" + dashboardBean.getGeral()));
						}
					}
				}
				
				if(hasDashboardGrafana){
					Collections.sort(listaDashboardGrafana, new Comparator<GrafanaDashboard>() {
						@Override
						public int compare(GrafanaDashboard o1, GrafanaDashboard o2) {
							return o1.getNomeDashboard().compareTo(o2.getNomeDashboard());
						}
					});
					
					for (GrafanaDashboard grafanaDashboard : listaDashboardGrafana) {
						menu.addMenu(new Menu(Util.strings.captalize(grafanaDashboard.getNomeDashboard()), contextPath + "/adm/index?nomeBanco=" + grafanaCliente.getNomeBanco() + "&uidDashboard=" + grafanaDashboard.getUidDashboard()));
					}
				}

				//GrafanaClienteService.getInstance().findGrafanaCliente();
				
				if(novidadeVersaoRetorno != null && Boolean.TRUE.equals(novidadeVersaoRetorno.getExisteNovidade())){
					menu.addMenu(new Menu("Novidades da vers�o", contextPath + "/adm/NovidadeVersao"));
				}
			} else {
				List<br.com.linkcom.sined.geral.bean.Menu> listaMenu = MenuService.getInstance().findByModulo(menumodulo);
				MenuBancoDadosParser parser = new MenuBancoDadosParser(contextPath);
				menu = parser.parse(listaMenu);
				this.verificarAutorizacao(menu, Neo.getApplicationContext().getAuthorizationManager(), Neo.getRequestContext().getUser());
			}
			
			this.removeUnnecessarySeparator(menu);
			this.removeEmptyMenus(menu);
			this.removeOnlySeparator(menu);
			
			MenuBuilder menuBuilder = new MenuBuilder();
			menuCode = menuBuilder.build(menu);
			
			menuCacheMap.put(menumodulo, menuCode);
		} else {
			menuCode = cachedCode;
		}
		
		String menuId = this.generateUniqueId(NeoWeb.getRequestContext().getServletRequest());
		String divId = this.generateUniqueId(NeoWeb.getRequestContext().getServletRequest());
		menuCode = "var "+menuId+" = \n"+menuCode+";";
		String drawCode = "cmDraw ('"+divId+"', "+menuId+", 'hbr', cmThemeOffice, 'ThemeOffice');";

		StringBuilder sbMenu = new StringBuilder();
		
		sbMenu
			.append("<span class=\"menuClass\" id=\""+divId+"\">")
			.append("</span>")
			.append("<script language=\"JavaScript\">")
			.append(menuCode)
			.append(drawCode)
			.append("</script>");
		
		return sbMenu;
	}
	
	private void removeUnnecessarySeparator(Menu menu) {
		if(menu.getSubmenus().size() > 0){
			for (int i = 0; i < menu.getSubmenus().size(); i++) {
				Menu submenu = menu.getSubmenus().get(i);
				removeUnnecessarySeparator(submenu);
				
				if(submenu.getTitle() != null && submenu.getTitle().matches("--(-)+")){
					boolean removeMenu = false;
					try {
						Menu submenu_ant = menu.getSubmenus().get(i-1);
					
						if(submenu_ant.getTitle().matches("--(-)+")){
							removeMenu = true;
						}
						
						Menu submenu_post = menu.getSubmenus().get(i+1);
						
						if(submenu_post.getTitle().matches("--(-)+")){
							removeMenu = true;
						}
					} catch (IndexOutOfBoundsException e) {
						removeMenu = true;
					}
					
					if(removeMenu){
						Menu parent2 = submenu.getParent();
						if(parent2 != null){
							parent2.getSubmenus().remove(submenu);	
						}
					}
				}
				
				if(!menu.getSubmenus().contains(submenu)){
					i--;
				}
			}
		}
	}

	private void removeOnlySeparator(Menu menu) {
		boolean onlySeparator = true;
		
		if(menu.getSubmenus().size() > 0){
			for (int i = 0; i < menu.getSubmenus().size(); i++) {
				Menu submenu = menu.getSubmenus().get(i);
				removeOnlySeparator(submenu);
				
				if(!menu.getSubmenus().contains(submenu)){
					i--;
				} else if(submenu.getTitle() != null && !submenu.getTitle().matches("--(-)+")){
					onlySeparator = false;
				}
			}
			
			if(onlySeparator){
				Menu parent2 = menu.getParent();
				if(parent2 != null){
					parent2.getSubmenus().remove(menu);	
				}
			}
		}
	}

	private String generateUniqueId(HttpServletRequest request) {
		Integer idsequence = (Integer) request.getAttribute("IDSEQUENCE");
		if(idsequence == null){
			idsequence = 0;
		}
		request.setAttribute("IDSEQUENCE", idsequence + 1);
		return ("" + ((char)(Math.random()*20+65))) + idsequence;
	}

	private void removeEmptyMenus(Menu menu) {
		if(menu.getSubmenus().size() > 0){
			for (int i = 0; i < menu.getSubmenus().size(); i++) {
				Menu submenu = menu.getSubmenus().get(i);
				removeEmptyMenus(submenu);
				if(!menu.getSubmenus().contains(submenu)){
					i--;
				}
			}
		}
		if(menu.getSubmenus().size() == 0 && 
				Util.strings.isEmpty(menu.getUrl()) && 
				(menu.getTitle() != null && !menu.getTitle().matches("--(-)+") )){
			Menu parent2 = menu.getParent();
			if(parent2 != null){
				parent2.getSubmenus().remove(menu);	
			}
		}
	}

	private void verificarAutorizacao(Menu menu, AuthorizationManager authorizationManager, User user) {
		int length = NeoWeb.getRequestContext().getServletRequest().getContextPath().length();
		for (Iterator<Menu> iter = menu.getSubmenus().iterator(); iter.hasNext();) {
			Menu submenu = iter.next();
			
			String url = submenu.getUrl();
			if (!StringUtils.isEmpty(url)) {
				if(url.contains("?")){
					url = url.substring(0, url.indexOf('?'));
				}
				if (!authorizationManager.isAuthorized(url.substring(length), null,user)) {
					iter.remove();
					continue;
				}
			}
			verificarAutorizacao(submenu, authorizationManager, user);
		}
	}

}
