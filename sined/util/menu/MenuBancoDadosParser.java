/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util.menu;

import java.util.List;

import br.com.linkcom.neo.view.menu.Menu;
import br.com.linkcom.sined.util.SinedUtil;

public class MenuBancoDadosParser {
	
	private String urlPrefix;
	
	public MenuBancoDadosParser(String urlPrefix) {
		this.urlPrefix = urlPrefix;
	}

	public String getUrlPrefix() {
		return urlPrefix;
	}

	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix;
	}

	public Menu parse(List<br.com.linkcom.sined.geral.bean.Menu> listaMenu) {
		Menu basemenu = new Menu();
		
		if(listaMenu == null || listaMenu.size() == 0){
			return basemenu;
		}
		
		for (br.com.linkcom.sined.geral.bean.Menu menuIt : listaMenu) {
			if(menuIt.getMenupai() == null){
				Menu menu = this.fillMenuAttributes(menuIt);
				basemenu.addMenu(menu);
				this.createMenu(menu, menuIt, listaMenu);
			}
		}
		
		List<Menu> submenus = basemenu.getSubmenus();
		// os primeiros menus nao tevem ter espašadores
		for (Menu menu2 : submenus) {
			if(menu2.getIcon().equals("&nbsp;&nbsp;&nbsp;&nbsp;")){
				menu2.setIcon("");
			}
		}
		
		return basemenu;
	}

	private void createMenu(Menu basemenu, br.com.linkcom.sined.geral.bean.Menu menuPai, List<br.com.linkcom.sined.geral.bean.Menu> listaMenu) {
		for (br.com.linkcom.sined.geral.bean.Menu menuIt : listaMenu) {
			if(menuIt.getMenupai() != null && menuIt.getMenupai().equals(menuPai)){
				Menu menu = this.fillMenuAttributes(menuIt);
				basemenu.addMenu(menu);
				this.createMenu(menu, menuIt, listaMenu);
			}
		}
	}

	private Menu fillMenuAttributes(br.com.linkcom.sined.geral.bean.Menu menuIt) {
		Menu menu = new Menu();
		
		String nome = menuIt.getNome();
		String url = menuIt.getUrl();
		String contexto = menuIt.getContexto();
		
		if(contexto == null || contexto.equals("")){
			contexto = urlPrefix;
		}
		if(contexto != null && url != null && !url.equals("") && !url.startsWith("javascript:")) {
			url = "/" + contexto + url;
		}
		
		menu.setTitle(nome);
		menu.setUrl(SinedUtil.escapeSingleQuotes(url));
		menu.setIcon("");
		menu.setTarget("");
		
		return menu;
	}

}