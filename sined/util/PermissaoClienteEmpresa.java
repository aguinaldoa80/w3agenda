package br.com.linkcom.sined.util;

public interface PermissaoClienteEmpresa {

	/**
	 * M�todo onde tem que se retornar a string com a query
	 * que limita o bean aos clientes vinculados a empresa do usu�rio.
	 * 
	 * LINGUAGEM: HQL
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String subQueryClienteEmpresa();
	
}
