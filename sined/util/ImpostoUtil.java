package br.com.linkcom.sined.util;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.PopupImpostoVendaInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;

public class ImpostoUtil {
	public static void copyDadosImposto(PopupImpostoVendaInterface origem, PopupImpostoVendaInterface destino){
		destino.setGrupotributacao(origem.getGrupotributacao());
		destino.setOutrasdespesas(origem.getOutrasdespesas());
		destino.setTipotributacaoicms(origem.getTipotributacaoicms());
		destino.setTipocobrancaicms(origem.getTipocobrancaicms());
		destino.setValorbcicms(origem.getValorbcicms());
		destino.setValoricms(origem.getValoricms());
		destino.setIcms(origem.getIcms());
		
		destino.setAbaterdesoneracaoicms(origem.getAbaterdesoneracaoicms());
		destino.setValordesoneracaoicms(origem.getValordesoneracaoicms());
		destino.setPercentualdesoneracaoicms(origem.getPercentualdesoneracaoicms());
		
		destino.setValorbcicmsst(origem.getValorbcicmsst());
		destino.setValoricmsst(origem.getValoricmsst());
		destino.setIcmsst(origem.getIcmsst());
		
		destino.setValorbcfcp(origem.getValorbcfcp());
		destino.setValorfcp(origem.getValorfcp());
		destino.setFcp(origem.getFcp());
		
		destino.setValorbcfcpst(origem.getValorbcfcpst());
		destino.setValorfcpst(origem.getValorfcpst());
		destino.setFcpst(origem.getFcpst());
		
		
		destino.setDadosicmspartilha(origem.getDadosicmspartilha());
		destino.setValorbcdestinatario(origem.getValorbcdestinatario());
		destino.setValorbcfcpdestinatario(origem.getValorbcfcpdestinatario());
		destino.setFcpdestinatario(origem.getFcpdestinatario());
		destino.setIcmsdestinatario(origem.getIcmsdestinatario());
		destino.setIcmsinterestadual(origem.getIcmsinterestadual());
		destino.setIcmsinterestadualpartilha(origem.getIcmsinterestadualpartilha());
		destino.setValorfcpdestinatario(origem.getValorfcpdestinatario());
		destino.setValoricmsdestinatario(origem.getValoricmsdestinatario());
		destino.setValoricmsremetente(origem.getValoricmsremetente());
		destino.setDifal(origem.getDifal());
		destino.setValordifal(origem.getValordifal());
		
		destino.setReducaobcicmsst(origem.getReducaobcicmsst());
		destino.setMargemvaloradicionalicmsst(origem.getMargemvaloradicionalicmsst());
		
		destino.setIpi(origem.getIpi());
		destino.setValoripi(origem.getValoripi());
		destino.setTipocobrancaipi(origem.getTipocobrancaipi());
		
		destino.setValorSeguro(origem.getValorSeguro());
		destino.setOutrasdespesas(origem.getOutrasdespesas());
		
        destino.setNcmcapitulo(origem.getNcmcapitulo());
        destino.setNcmcompleto(origem.getNcmcompleto());
        destino.setCfop(origem.getCfop());
	}
	
	public static Localdestinonfe getLocalDestino(Municipio municipioGerador, Endereco enderecoCliente){
		if(municipioGerador != null && municipioGerador.getUf() != null && enderecoCliente != null){
			Endereco enderecoNota = EnderecoService.getInstance().loadEndereco(enderecoCliente);
			if(enderecoNota != null){
				if(enderecoNota.getPais() != null && !Pais.BRASIL.equals(enderecoNota.getPais())){
					return Localdestinonfe.EXTERIOR;
				}else if(enderecoNota.getMunicipio() != null && enderecoNota.getMunicipio().getUf() != null && 
						enderecoNota.getMunicipio().getUf().getSigla() != null &&
						municipioGerador.getUf() != null &&
						!enderecoNota.getMunicipio().getUf().getSigla().equals(municipioGerador.getUf().getSigla())){
					return Localdestinonfe.INTERESTADUAL;
				}else {
					return Localdestinonfe.INTERNA;
				}
			}
		}
		return null;
	}
	
	public static Operacaonfe getOperacaonfe(Cliente cliente, Naturezaoperacao naturezaOperacao){
		Cliente clienteAux = null;
		if(Util.objects.isPersistent(cliente)){
			clienteAux = ClienteService.getInstance().load(cliente, "cliente.operacaonfe");
		}
		if(clienteAux !=null && clienteAux.getOperacaonfe() != null){
			return clienteAux.getOperacaonfe();
		}else if(Util.objects.isPersistent(naturezaOperacao)){
			Naturezaoperacao natureza = NaturezaoperacaoService.getInstance().load(naturezaOperacao, "naturezaoperacao.operacaonfe");
			if(natureza != null){
				return natureza.getOperacaonfe();
			}
		}
		return null;
	}
}
