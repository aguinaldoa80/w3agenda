package br.com.linkcom.sined.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.util.bean.LocalarmazenagemBean;

public class LocalarmazenagemBeanCacheUtil {

	private static Map<String, List<LocalarmazenagemBean>> mapCacheLocalarmazenagemBean = new HashMap<String, List<LocalarmazenagemBean>>();
	
	public static void remove(int cdlocalarmazenagem){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return;
			url = SinedUtil.getUrlWithoutContext();
			
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());	  		
			return ;
		}
		remove(cdlocalarmazenagem, url);
	}
	
	private static void remove(int cdlocalarmazenagem, String url){
		if(url == null) return;
		
		synchronized (mapCacheLocalarmazenagemBean) {
			List<LocalarmazenagemBean> lista = mapCacheLocalarmazenagemBean.get(url);
			if(lista != null && lista.size() > 0){
				lista.remove(new LocalarmazenagemBean(cdlocalarmazenagem));
			}
		}
	}
	
	public  static void put(int cdlocalarmazenagem, boolean permitirestoquenegativo){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return;
			url = SinedUtil.getUrlWithoutContext();
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());	  		
			return ;
		}
		put(cdlocalarmazenagem, permitirestoquenegativo, url);
	}
	
	private static void put(int cdlocalarmazenagem, boolean permitirestoquenegativo, String url){
		if(url == null) return;
		
		synchronized (mapCacheLocalarmazenagemBean) {
			List<LocalarmazenagemBean> lista = mapCacheLocalarmazenagemBean.get(url);
			if(lista == null){
				lista = new ArrayList<LocalarmazenagemBean>();
			}
			lista.add(new LocalarmazenagemBean(cdlocalarmazenagem, permitirestoquenegativo));
			
			mapCacheLocalarmazenagemBean.put(url, lista);
		}
	}
	
	public static Boolean get(int cdlocalarmazenagem){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return null;
			url = SinedUtil.getUrlWithoutContext();
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());  
			return null;
		}
		return get(cdlocalarmazenagem, url);
	}
	
	private static Boolean get(int cdlocalarmazenagem, String url){
		if(url == null) return null;
		
		synchronized (mapCacheLocalarmazenagemBean) {
			List<LocalarmazenagemBean> lista = mapCacheLocalarmazenagemBean.get(url);
			if(lista == null){
				return null;
			}
			for(LocalarmazenagemBean bean : lista){
				if(cdlocalarmazenagem == bean.getId())
					return bean.isPermitirestoquenegativo();
			}
			return null;
		}
	}
	
	public static void limparCache(){
		synchronized (mapCacheLocalarmazenagemBean) {
			mapCacheLocalarmazenagemBean = new HashMap<String, List<LocalarmazenagemBean>>();
		}
	}
	
}
