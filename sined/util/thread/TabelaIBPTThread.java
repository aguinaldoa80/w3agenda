package br.com.linkcom.sined.util.thread;

import org.springframework.jdbc.core.JdbcTemplate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.AvisousuarioService;
import br.com.linkcom.sined.geral.service.MaterialimpostoService;
import br.com.linkcom.sined.util.EmailManager;

public class TabelaIBPTThread extends Thread {

	private String nomeBanco;
	private JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null){
			jdbcTemplate = Neo.getObject(JdbcTemplate.class);
		}
		return jdbcTemplate;
	}
	
	public TabelaIBPTThread(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	@Override
	public void run() {
		try {
			String nomeThread = "Thread_Tabela_IBPT_ECF_" + nomeBanco;
			Thread.currentThread().setName(nomeThread);
			
			MaterialimpostoService.getInstance().atualizarImpostoIBPTMaterial();
		} catch (Exception e) {
			throw new RuntimeException("Erro ao atualizar imposto IBPT dos materiais. ", e);
		}
	}
}
