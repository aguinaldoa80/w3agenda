package br.com.linkcom.sined.util.thread;

import br.com.linkcom.sined.util.EmailManager;

/**
 * Thread para envio de emails com o EmailManager
 * @author igorcosta
 *
 */
public class EmailManagerThread extends Thread {

	private EmailManager emailManager;
	private String cliente;
	
	public EmailManagerThread(EmailManager emailManager, String cliente) {
		this.emailManager = emailManager;
	}

	@Override
	public void run() {
		try{
			String nomeThread = "Thread_Email_" + cliente;
			Thread.currentThread().setName(nomeThread);
			
			if (emailManager != null) {
				emailManager.sendMessage();
			} else {
				throw new Exception("EmailManager est� vazio.");
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Erro ao enviar email agendado via Thread. ", e);
		}
	}
	
}
