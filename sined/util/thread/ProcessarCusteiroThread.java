package br.com.linkcom.sined.util.thread;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.jdbc.core.JdbcTemplate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.CusteioHistorico;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.CusteioTipoDocumento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.MovimentacaoEstoqueHistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.SituacaoCusteio;
import br.com.linkcom.sined.geral.bean.TipoDocumentoCusteioEnum;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.service.CusteioHistoricoService;
import br.com.linkcom.sined.geral.service.CusteioProcessadoService;
import br.com.linkcom.sined.geral.service.CusteioTipoDocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoEstoqueHistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.CusteioFiltroAndBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

public class ProcessarCusteiroThread extends Thread {

	private HashMap<Integer, Integer> mapCdCentroCusto = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> mapCdProjeto = new HashMap<Integer, Integer>();
	private CusteioFiltroAndBean filtro;
	private JdbcTemplate jdbcTemplate;
	private HashMap <Integer,Projeto> mapProjeto;
	private HashMap <Integer,Centrocusto> mapCentroCusto;
	private String nomeBanco;
	private Usuario usuario;

	private static final String OBS = "Rateio alterado atrav�s do processo de custeio.";
	
	public JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null){
			jdbcTemplate = Neo.getObject(JdbcTemplate.class);
		}
		return jdbcTemplate;
	}

	

	public ProcessarCusteiroThread(HashMap<Integer, Integer> mapCdCentroCusto,HashMap<Integer, Integer> mapCdProjeto,CusteioFiltroAndBean filtro, HashMap<Integer, Projeto> mapProjeto, HashMap<Integer, Centrocusto> mapCentroCusto,String nomeBanco,Usuario usuario) {
		super();
		this.mapCdCentroCusto = mapCdCentroCusto;
		this.mapCdProjeto = mapCdProjeto;
		this.filtro = filtro;
		this.mapProjeto = mapProjeto;
		this.mapCentroCusto = mapCentroCusto;
		this.nomeBanco = nomeBanco;
		this.usuario = usuario;
	}



	@Override
	public void run() {
		Thread.currentThread().setName("Thread_Rateio_Custeio_" + nomeBanco );
		 HashMap<Integer, Integer> mapDocAndRateio = new HashMap<Integer, Integer>();
		 HashMap<Integer, Integer> mapMovAndRateio = new HashMap<Integer, Integer>();
		 HashMap<Integer, Integer> mapEstoqueAndRateio = new HashMap<Integer, Integer>();
		 List<CusteioTipoDocumento> listaTipoCusteio = new ArrayList<CusteioTipoDocumento>(); 
		try{  
			 for(Entry<Integer, Centrocusto> entry : mapCentroCusto.entrySet()){
					Centrocusto centroCustro = entry.getValue();
					List<Documento> listaDoc = DocumentoService.getInstance().buscarPorCentroCusto(false, centroCustro,filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),filtro.getEmpresa());
					if(SinedUtil.isListNotEmpty(listaDoc)){
						for (Documento documento : listaDoc) {
							CusteioTipoDocumento tipoDocumento = new CusteioTipoDocumento();
							if(Documentoclasse.CD_PAGAR.equals(documento.getDocumentotipo())){
								tipoDocumento.setTipoDocumentoCusteioEnum(TipoDocumentoCusteioEnum.PAGAR);
							}else{
								tipoDocumento.setTipoDocumentoCusteioEnum(TipoDocumentoCusteioEnum.RECEBER);
							}
							Integer cdRateio = documento.getRateio().getCdrateio();
							tipoDocumento.setCodObjetoOrigem(documento.getCddocumento());
							tipoDocumento.setCodRateioOrigem(documento.getRateio());
							mapDocAndRateio.put(documento.getCddocumento(), cdRateio);
							Rateio novoRateio = atulizaRateio(cdRateio,centroCustro,mapProjeto);
							tipoDocumento.setCodRateioProcessado(novoRateio);
							DocumentoService.getInstance().updateCdRateio(documento.getCddocumento(),novoRateio.getCdrateio());
							historicoPorTipo(TipoDocumentoCusteioEnum.PAGAR,documento.getCddocumento(), cdRateio);
							listaTipoCusteio.add(tipoDocumento);
						}
					}
					List<Movimentacao> listaMov = MovimentacaoService.getInstance().buscarPorCentroCusto(false, centroCustro,filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),filtro.getEmpresa());
					if(SinedUtil.isListNotEmpty(listaMov)){
						for (Movimentacao mov : listaMov) {
							CusteioTipoDocumento tipoDocumento = new CusteioTipoDocumento();
							tipoDocumento.setTipoDocumentoCusteioEnum(TipoDocumentoCusteioEnum.FINANCEIRA);
							Integer cdRateio = mov.getRateio().getCdrateio();
							tipoDocumento.setCodObjetoOrigem(mov.getCdmovimentacao());
							tipoDocumento.setCodRateioOrigem(mov.getRateio());
							mapMovAndRateio.put(mov.getCdmovimentacao(), cdRateio);
							Rateio novoRateio = atulizaRateio(cdRateio,centroCustro,mapProjeto);
							tipoDocumento.setCodRateioProcessado(novoRateio);
							MovimentacaoService.getInstance().updateCdRateio(mov.getCdmovimentacao(),novoRateio);
							historicoPorTipo(TipoDocumentoCusteioEnum.FINANCEIRA,mov.getCdmovimentacao(), cdRateio);
							listaTipoCusteio.add(tipoDocumento);
						}
					}
					List<Movimentacaoestoque> listaEstoque = MovimentacaoestoqueService.getInstance().buscarPorCentroCusto(false, centroCustro,filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),filtro.getEmpresa());
					if(SinedUtil.isListNotEmpty(listaEstoque)){
						for (Movimentacaoestoque estoque : listaEstoque) {
							CusteioTipoDocumento tipoDocumento = new CusteioTipoDocumento();
							tipoDocumento.setTipoDocumentoCusteioEnum(TipoDocumentoCusteioEnum.ESTOQUE);
							Integer cdRateio = estoque.getRateio().getCdrateio();
							tipoDocumento.setCodObjetoOrigem(estoque.getCdmovimentacaoestoque());
							tipoDocumento.setCodRateioOrigem(estoque.getRateio());
							mapEstoqueAndRateio.put(estoque.getCdmovimentacaoestoque(), cdRateio);
							Rateio novoRateio = atulizaRateio(cdRateio,centroCustro,mapProjeto);
							tipoDocumento.setCodRateioProcessado(novoRateio);
							MovimentacaoestoqueService.getInstance().updateCdRateio(estoque.getCdmovimentacaoestoque(),novoRateio.getCdrateio());
							historicoPorTipo(TipoDocumentoCusteioEnum.ESTOQUE,estoque.getCdmovimentacaoestoque(), cdRateio);
							listaTipoCusteio.add(tipoDocumento);
						}
					}
			 }
			 gravarClasseEHistorico(true,listaTipoCusteio,null);
		}catch (Exception e) {
			e.printStackTrace();
			gravarClasseEHistorico(false,listaTipoCusteio,e.getMessage().substring(0, 500));
		}
	}
	
	private void historicoPorTipo(TipoDocumentoCusteioEnum tipo,Integer idTipo, Integer cdRateio){
		if(TipoDocumentoCusteioEnum.ESTOQUE.equals(tipo)){
			MovimentacaoEstoqueHistorico historicoEstoque = new MovimentacaoEstoqueHistorico();
			historicoEstoque.setMovimentacaoEstoque(new Movimentacaoestoque(idTipo));
			historicoEstoque.setDtaltera(SinedDateUtils.currentTimestamp());
			historicoEstoque.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			historicoEstoque.setObservacao(OBS + SinedUtil.criarLinkVisualizacaoRateio(cdRateio));
			historicoEstoque.setMovimentacaoEstoqueAcao(MovimentacaoEstoqueAcao.CUSTEIO);
			MovimentacaoEstoqueHistoricoService.getInstance().saveOrUpdate(historicoEstoque);
			
		}else if(TipoDocumentoCusteioEnum.FINANCEIRA.equals(tipo)){
			Movimentacaohistorico historicoFinanceiro  = new Movimentacaohistorico();
			historicoFinanceiro.setDtaltera(SinedDateUtils.currentTimestamp());
			historicoFinanceiro.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			historicoFinanceiro.setObservacao(OBS + SinedUtil.criarLinkVisualizacaoRateio(cdRateio));
			historicoFinanceiro.setMovimentacaoacao(Movimentacaoacao.CUSTEIO);
			historicoFinanceiro.setMovimentacao(new Movimentacao(idTipo));
			MovimentacaohistoricoService.getInstance().saveOrUpdate(historicoFinanceiro);
		}else{
			Documentohistorico historicoDocumento = new Documentohistorico();
			historicoDocumento.setDtaltera(SinedDateUtils.currentTimestamp());
			historicoDocumento.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			historicoDocumento.setObservacao(OBS + SinedUtil.criarLinkVisualizacaoRateio(cdRateio));
			historicoDocumento.setDocumentoacao(Documentoacao.CUSTEIO);
			historicoDocumento.setDocumento(new Documento(idTipo));
			DocumentohistoricoService.getInstance().saveOrUpdate(historicoDocumento);
		}
	}
	
	private void gravarClasseEHistorico(boolean isSucesso, List<CusteioTipoDocumento> listaTipoCusteio, String msgErro){
			CusteioProcessadoService service = CusteioProcessadoService.getInstance();
			CusteioTipoDocumentoService tipoDocumentoService = CusteioTipoDocumentoService.getInstance();
			CusteioProcessado custeio = criarCusteioProcessado(isSucesso);
			service.saveOrUpdate(custeio);
			if(SinedUtil.isListNotEmpty(listaTipoCusteio)){
				for (CusteioTipoDocumento tipoDocumento : listaTipoCusteio) {
					tipoDocumento.setCusteioProcessado(custeio);
					tipoDocumentoService.saveOrUpdate(tipoDocumento);
				}
			}
			criarHistorico(isSucesso,msgErro,custeio);
			
	}
	private void criarHistorico(boolean isSucesso, String msgErro, CusteioProcessado custeio){
		CusteioHistorico historico = new CusteioHistorico();
		if(!isSucesso){
			historico.setObservacao(msgErro);
		}else{
			historico.setObservacao("Custeio realizado com sucesso");
		}
		historico.setCdusuarioaltera(usuario.getCdpessoa());
		historico.setDtaltera(SinedDateUtils.currentTimestamp());
		historico.setSituacaoCusteio(SituacaoCusteio.SUCESSO);
		historico.setCusteioProcessado(custeio);
		CusteioHistoricoService.getInstance().saveOrUpdate(historico);
//		
//		AvisoService.getInstance().adi
	}
	
	private Rateio atulizaRateio(Integer cdRateio, Centrocusto centroCustro, HashMap<Integer, Projeto> mapProjeto){
		RateioService rateioService = RateioService.getInstance();
		Rateio rateio = criarRateio(cdRateio);
		List<Rateioitem> listaNovosItens  = new ArrayList<Rateioitem>();
		for (Rateioitem item : rateio.getListaRateioitem()) {
			if(item.getCentrocusto().equals(centroCustro) && item.getProjeto() ==null){
				listaNovosItens.addAll(recalcularItemRateio(item,mapProjeto,centroCustro));
			}else{
				listaNovosItens.add(item);
			}
		}
		rateio.setListaRateioitem(listaNovosItens);
		rateioService.saveOrUpdate(rateio);
		return rateio;
	}
	
	private List<Rateioitem> recalcularItemRateio(Rateioitem itemAtual,HashMap<Integer, Projeto> mapProjeto,Centrocusto centroCusto){
		List<Rateioitem> listaItemProcessado = new ArrayList<Rateioitem>();
		filtro.getArquivo();
		Double referenciaPorcentagem = itemAtual.getPercentual();
		String strArquivo = new String(filtro.getArquivo().getContent());
		String[] listaLinhas = strArquivo.split("\\r?\\n");
		Integer nLinha = mapCdCentroCusto.get(centroCusto.getCdcentrocusto());
		String[] celulasDaLinha = listaLinhas[nLinha].split(";");
		int auxNumeroCelula = 1;
		for (String celula : celulasDaLinha) {
			if(auxNumeroCelula > 2 && referenciaPorcentagem > 0){
				Double porcentagem = Double.parseDouble(celula);
				if(porcentagem == 0){
					continue;
				}
				porcentagem = porcentagem * (referenciaPorcentagem / 100);
				Double valorTotal = itemAtual.getValor().getValue().doubleValue();
				Double valorFinal = valorTotal * (Double.parseDouble(celula)/100);
				Rateioitem novoItem = new Rateioitem();
				novoItem.setCentrocusto(centroCusto);
				novoItem.setPercentual(porcentagem);
				novoItem.setContagerencial(itemAtual.getContagerencial());
				novoItem.setValor(new Money(valorFinal));
				novoItem.setProjeto(retornarProjeCelula(auxNumeroCelula,mapProjeto));
				listaItemProcessado.add(novoItem);
			}
			auxNumeroCelula +=1;
		}
		
		
		for (Iterator<Rateioitem> iterator = listaItemProcessado.iterator(); iterator.hasNext();) {
			Rateioitem it = iterator.next();
			if(it.getProjeto() == null){
				iterator.remove();
				break;
			}
		}
		return listaItemProcessado;
	}
	
	private Projeto retornarProjeCelula(Integer nColuna, HashMap<Integer, Projeto> mapProjeto){
		for(Entry<Integer, Integer> entry :	mapCdProjeto.entrySet()){
			if(entry.getValue() == nColuna){
				return mapProjeto.get(entry.getKey());
			}
		}
		return null;
	}
	
	private Rateio criarRateio(Integer cdRateio){
		RateioService rateioService = RateioService.getInstance();
		Rateio novoRateio = rateioService.findRateio(new Rateio(cdRateio));
		List<Rateioitem> listaItem = novoRateio.getListaRateioitem();
		List<Rateioitem> novaLista = new ArrayList<Rateioitem>();
		novoRateio.setCdrateio(null);
		for (Rateioitem item : listaItem) {
			item.setCdrateioitem(null);
			novaLista.add(item);
		}
		novoRateio.setListaRateioitem(novaLista);
		return novoRateio;
	}

	
	private CusteioProcessado criarCusteioProcessado(boolean isSucesso){
		CusteioProcessado custeio = new CusteioProcessado();
		custeio.setArquivo(filtro.getArquivo());
		custeio.setDtInicio(filtro.getDtPeriodoInicio());
		custeio.setDtFim(filtro.getDtPeriodoFim());
		custeio.setDtaltera(SinedDateUtils.currentTimestamp());
		custeio.setEmpresa(filtro.getEmpresa());
		if(isSucesso){
			custeio.setSituacaoCusteio(SituacaoCusteio.SUCESSO);
		}else{
			custeio.setSituacaoCusteio(SituacaoCusteio.ERRO);
		}
		custeio.setDtConclusao(SinedDateUtils.currentDate());
		custeio.setUsuario(usuario);
		custeio.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		return custeio;
	}
	
}
















