package br.com.linkcom.sined.util.thread;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.springframework.jdbc.core.JdbcTemplate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.CorrigirPercentualRateioProcess;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.controller.SinedExcel;

public class CorrigirPercentualRateioThread extends Thread {

	private Boolean salvarAlteracoes;
	private String nome_arquivo;
	private JdbcTemplate jdbcTemplate;
	
	
	public JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null){
			jdbcTemplate = Neo.getObject(JdbcTemplate.class);
		}
		return jdbcTemplate;
	}

	public CorrigirPercentualRateioThread(Boolean salvarAlteracoes, String nome_arquivo) {
		this.salvarAlteracoes = salvarAlteracoes;
		this.nome_arquivo = nome_arquivo;
	}

	@Override
	public void run() {
		try{
			Thread.currentThread().setName("Thread_Corrigir_Rateio_linkcom.w3erp.com.br_w3erp_PostgreSQLDS");
			List<String> listaBancos = getListaBancos();
			if(listaBancos != null && !listaBancos.isEmpty()){
				Map<String, Exception> mapaFalhas = new HashMap<String, Exception>();
				SinedExcel wb = new SinedExcel();
				HSSFSheet sheet = wb.createSheet("Relatorio");
				sheet.setVerticallyCenter(true);
				sheet.setHorizontallyCenter(true);
				sheet.setDefaultColumnWidth((short) 30);
				sheet.setDefaultRowHeight((short) (10*35));
				Short linha = 0;
				Integer estilo = 0;
				for(String nome_banco : listaBancos){
					try{
						String nomeThread = "Thread_Corrigir_Rateio_" + nome_banco + ".w3erp.com.br_w3erp_PostgreSQLDS";
						Thread.currentThread().setName(nomeThread);
						CorrigirPercentualRateioProcess.Corrigir(salvarAlteracoes, sheet, linha, estilo, nome_banco);
					}catch (Exception e) {
						e.printStackTrace();
						mapaFalhas.put(nome_banco, e);
					}
				}
				sheet = wb.createSheet("Falha Banco");
				int i = 0;
				HSSFCellStyle style;
				HSSFRow row = sheet.createRow(i++);
				HSSFCell cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
				cell.setCellValue("Banco");
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
				cell.setCellValue("StackTrace");
				if(mapaFalhas != null && !mapaFalhas.isEmpty()){
					for(Entry<String, Exception> entry : mapaFalhas.entrySet()){
						if(i%2 != 0){
							style = SinedExcel.STYLE_DETALHE_LEFT_GREY;
						} else{
							style = SinedExcel.STYLE_DETALHE_LEFT_WHITE;
						}
						row = sheet.createRow(i++);
						cell = row.createCell((short) 0);
						cell.setCellStyle(style);
						cell.setCellValue(entry.getKey());
						cell = row.createCell((short) 1);
						cell.setCellStyle(style);
						StringBuilder st = new StringBuilder();
						if(entry.getValue().getStackTrace() != null && entry.getValue().getStackTrace().length > 0){
							for(int j = 0; j < entry.getValue().getStackTrace().length; j++){
								st.append(entry.getValue().getStackTrace()[j]).append("\n");
							}
						}
						cell.setCellValue(st.toString());
						row.setHeight( (short) 800);
					}
				}
				String mensagem;
				if(salvarAlteracoes != null && salvarAlteracoes){
					mensagem = "O processo foi executado com sucesso e as devidas correcoes foram aplicadas aos bancos!<br/>";
				}else{
					mensagem = "O processo foi executado com sucesso mas as correcoes n�o foram aplicadas aos bancos!<br/>";
				}
				mensagem += "Segue em anexo a planilha com o relatorio final do processo.";
				
				enviarEmail(getListaTo(), mensagem, wb);
			}
		}catch (Exception e) {
			StringBuilder mensagem = new StringBuilder();
			if(e.getMessage() != null){
				mensagem.append("<b>Mensagem do erro:<b/><br/><br/>")
						.append(e.getMessage());
			}
			if(e.getStackTrace() != null && e.getStackTrace().length > 0){
				mensagem.append("<br/><br/><b>Stacktrace:<b/><br/><br/>");
				for(int i = 0; i < e.getStackTrace().length; i++){
					mensagem.append(e.getStackTrace()[i]).append("<br/>");
				}
			}
			e.printStackTrace();
			enviarEmail(getListaTo(), mensagem.toString(), null);
		}
	}
	
	private List<String> getListaTo(){
		List<String> listaTo = new ListSet<String>(String.class);
		listaTo.add("rodrigo.freitas@linkcom.com.br");
		listaTo.add("rafael.salvio@linkcom.com.br");
		
		return listaTo;
	}
	
	private void enviarEmail(List<String> listaTo, String mensagem, SinedExcel wb){
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		try {
			email.setFrom("admin@linkcom.com.br")
				.setSubject("Relatorio de correcao de rateios")
				.setListTo(listaTo)
				.addHtmlText(mensagem);
			
			if(wb != null && wb.getWorkBookResource(nome_arquivo) != null){
				email.attachFileUsingByteArray(wb.getWorkBookResource(nome_arquivo).getContents(), nome_arquivo, "application/msexcel", "1");
			}
			
			email.sendMessage();
		} catch (Exception e) {
//			System.out.println("Erro ao enviar email com o relatorio de correcao de rateios:");
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<String> getListaBancos(){
		String sql = "SELECT trim(leading '_' from trim(leading 'w3erp' from pg_database.datname)) as coluna " +
						"FROM pg_database " +
						"WHERE pg_database.datname like 'w3erp_%' " +
						"ORDER BY coluna";
		List<String> listaBanco = getJdbcTemplate().queryForList(sql, String.class);
	
		return listaBanco;
	}
	
}
