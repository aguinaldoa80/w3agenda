package br.com.linkcom.sined.util.thread;

import br.com.linkcom.sined.geral.service.ArquivotefService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.IntegracaoTefCallbackBean;

public class IntegracaoTEFCallbackThread extends Thread {
	
	private IntegracaoTefCallbackBean bean;
	private String cliente;
	
	public IntegracaoTEFCallbackThread(IntegracaoTefCallbackBean bean, String cliente) {
		super();
		this.cliente = cliente;
		this.bean = bean;
	}

	@Override
	public void run() {
		Thread.currentThread().setName("Thread_IntegracaoTEFCallback_" + cliente);
		try {
			ArquivotefService.getInstance().processCallback(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
