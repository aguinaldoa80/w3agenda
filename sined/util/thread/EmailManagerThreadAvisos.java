package br.com.linkcom.sined.util.thread;

import org.springframework.jdbc.core.JdbcTemplate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.AvisousuarioService;
import br.com.linkcom.sined.util.EmailManager;

public class EmailManagerThreadAvisos extends Thread {

	private EmailManager emailManager;
	private Integer cdavisousuario;
	private Integer cdusuario;
	private String nomeBanco;
	private JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null){
			jdbcTemplate = Neo.getObject(JdbcTemplate.class);
		}
		return jdbcTemplate;
	}
	
	public EmailManagerThreadAvisos(EmailManager emailManager, String nomeBanco) {
		
	}
	
	public EmailManagerThreadAvisos(EmailManager emailManager, Integer cdavisousuario, Integer cdusuario, String nomeBanco) {
		this.emailManager = emailManager;
		this.cdavisousuario = cdavisousuario;
		this.cdusuario = cdusuario;
		this.nomeBanco = nomeBanco;
	}

	@Override
	public void run() {
		try {
			String nomeThread = "Thread_Email_Aviso_" + nomeBanco;
			Thread.currentThread().setName(nomeThread);
			
			emailManager.sendMessage();
			
			if(cdavisousuario != null){
				Avisousuario avisousuario = new Avisousuario(cdavisousuario);
				avisousuario.setUsuario(new Usuario(cdusuario));
				
				AvisousuarioService.getInstance().updateAvisoComoEnviadoEmail(avisousuario);
			}
		} catch (Exception e) {
			if(cdavisousuario != null){
				AvisousuarioService.getInstance().updatePreparadoEnvioEmail(new Avisousuario(cdavisousuario), Boolean.FALSE);
			}
			throw new RuntimeException("Erro ao enviar email agendado via Thread. ", e);
		}
	}
}
