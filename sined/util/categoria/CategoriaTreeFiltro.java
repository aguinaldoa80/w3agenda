package br.com.linkcom.sined.util.categoria;


public class CategoriaTreeFiltro {

	private String propriedade;
	private String tipopessoa;
	private String q;

	public String getQ() {
		return q;
	}
	
	public String getTipopessoa() {
		return tipopessoa;
	}
	
	public String getPropriedade() {
		return propriedade;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
	public void setTipopessoa(String tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	public void setQ(String q) {
		this.q = q;
	}
	
	public Boolean isCliente(){
		if (getTipopessoa() != null){
			if (getTipopessoa().equals("C")){
				return true;
			}else{
				return false;
			}
		}else
			return null;
	}

	public Boolean isFornecedor(){
		if (getTipopessoa() != null){
			if (getTipopessoa().equals("F")){
				return true;
			}else{
				return false;
			}
		}else
			return null;
	}

}
