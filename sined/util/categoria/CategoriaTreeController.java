package br.com.linkcom.sined.util.categoria;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.service.CategoriaService;

@Bean
@Controller(path="/crm/process/CategoriaTreeView")
public class CategoriaTreeController extends MultiActionController {
	
	private CategoriaService categoriaService;
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@DefaultAction
	public ModelAndView view(WebRequestContext request, CategoriaTreeFiltro filtro){
		List<Categoria> listaCategoria = categoriaService.findTreeView(filtro.isCliente(), filtro.isFornecedor());
		String codigo_treeview = this.criaEstrutura(listaCategoria);
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		return new ModelAndView("direct:/process/categoriaTreeView");
	}

	private String criaEstrutura(List<Categoria> listaCategoria) {
		if(listaCategoria != null && listaCategoria.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (Categoria c : listaCategoria) {
				if(c.getFilhos() != null && c.getFilhos().size() > 0){
					sb.append("<li><span>");
					sb.append(c.getVcategoria().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("</span><ul>");
					sb.append(this.criaEstrutura(c.getFilhos()));
					sb.append("</ul></li>");
				} else {
					sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
					sb.append(c.getCdcategoria());
					sb.append(", '");
					sb.append(c.getVcategoria().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("')\">");
					sb.append(c.getVcategoria().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("</span></li>");
				}
			}
			return sb.toString();
		} else return "";
	}
}
