package br.com.linkcom.sined.util.categoria;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.service.CategoriaService;

@Bean
@Controller(path="/crm/process/CategoriaAutoComplete")
public class CategoriaAutoCompleteController extends MultiActionController {
	
	private CategoriaService categoriaService;
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@DefaultAction
	public void view(WebRequestContext request, CategoriaTreeFiltro filtro){
		List<Categoria> find = categoriaService.findAutocomplete(filtro.getQ(), filtro.isCliente(), filtro.isFornecedor());
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(find));
	}

	private String criarEstrutura(List<Categoria> find) {
		StringBuilder builder = new StringBuilder();
		if(find != null && find.size() > 0){
			for (Categoria cg : find) {
				builder.append(cg.getDescricaoInputPersonalizado() + "|" + cg.getCdcategoria() + "\n");
			}
		}
		return builder.toString();
	}

}
