package br.com.linkcom.sined.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Configuracaoecomerro;
import br.com.linkcom.sined.geral.bean.Ecommaterial;
import br.com.linkcom.sined.geral.bean.Ecommaterialestoque;
import br.com.linkcom.sined.geral.service.ConfiguracaoecomerroService;

import com.google.gson.Gson;

@Bean
public class IntegracaoEcomUtil {

	public static IntegracaoEcomUtil util = new IntegracaoEcomUtil();
	public static Gson gson = new Gson();
	
	public void addErro(Exception e, String descricao) {
		addErro(null, e, descricao);
	}
	
	@SuppressWarnings("unchecked")
	public void addErro(Configuracaoecom configuracaoecom, Exception e, String descricao) {
			
		String nomeMaquina = "N�o encontrado.";
		String subdominioCliente = "N�o encontrado";
		String threadNome = "N�o encontrado";
		String parametros = "N�o foi possivel recuperar os par�metros.";
		try {  
		    InetAddress localaddr = InetAddress.getLocalHost();  
		    nomeMaquina = localaddr.getHostName();  
		} catch (UnknownHostException e3) {}
		
		try{subdominioCliente = SinedUtil.getSubdominioCliente();}catch(Exception ex){}
		try{threadNome = Thread.currentThread().getName();}catch(Exception ex){}
		try{
			Map<String, String[]> mapParams = (Map<String, String[]>) NeoWeb.getRequestContext().getServletRequest().getParameterMap();
			parametros = SalvarFiltroUtil.mapToString(mapParams);
		}catch(Exception ex){}
		
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		
		String mensagem = descricao + " (M�quina: " + nomeMaquina + ", Cliente: " + subdominioCliente + ", Thread: " + threadNome + ") Veja a pilha de execu��o para mais detalhes:\n\n" + stringWriter.toString();
		mensagem += "\n\nPar�metros enviados: " + parametros;
		
		Configuracaoecomerro configuracaoecomerro = ConfiguracaoecomerroService.getInstance().getErroByConfiguracaoecom(configuracaoecom, mensagem);
		
		if(configuracaoecomerro == null){
			Timestamp currentTimestamp = SinedDateUtils.currentTimestamp();
			
			configuracaoecomerro = new Configuracaoecomerro();
			configuracaoecomerro.setConfiguracaoecom(configuracaoecom);
			configuracaoecomerro.setDtinicioerro(currentTimestamp);
			configuracaoecomerro.setDtultimoerro(currentTimestamp);
			configuracaoecomerro.setContador(1);
			configuracaoecomerro.setMensagem(mensagem);
			
			ConfiguracaoecomerroService.getInstance().saveOrUpdate(configuracaoecomerro);
		} else {
			ConfiguracaoecomerroService.getInstance().addContadorErro(configuracaoecomerro);
		}
	}

	private String execute(String urlStr, String data) throws IOException {
        URL url = new URL("http://" + urlStr);  

        URLConnection conn = url.openConnection(); 
        conn.setDoOutput(true);
        
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), encoder);
        
        if(data != null){
        	data = encodeStringLatin1(data);
        	System.out.println("## INTEGRACAO ECOM (ENVIO): " + data);
        	wr.write(data);
        }
        
        wr.flush(); 

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
        String line; 
        String xml = ""; 

        while ((line = rd.readLine()) != null) { xml += line; } 

        wr.close(); 
        rd.close();
        
        System.out.println("## INTEGRACAO ECOM (RETORNO): " + xml);
        
        return xml;
    }
	
	public static String encodeStringLatin1(String texto) {
		if (texto != null && !texto.isEmpty()) {
			CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
			if (encoder.canEncode(texto)) {
				return texto;
			} else {
				try {
					texto = new String(texto.getBytes("UTF-8"));
				} catch (Exception e) {
					texto = encoderPersonalizado(texto);
				}
				return texto;
			}
		} else
			return null;
	}
	
	public static String encoderPersonalizado(String value) {
		StringBuilder valueSB = new StringBuilder();

		for (char c : value.toCharArray()) {
			char[] aux = { c };
			try {
				valueSB.append(new String(new String(aux).getBytes("UTF-8")));
			} catch (UnsupportedEncodingException e) {
				valueSB.append("?");
			}
		}

		return valueSB.toString();
	}

    
    private String getUrlIntegracaoEcom(Configuracaoecom configuracaoecom){
		if(configuracaoecom != null && configuracaoecom.getUrl() != null){
			return configuracaoecom.getUrl();
		} else {
			return "xxx.plus-w3erp.izap.com.br/w3.php";
		}
    }
    
    public void enviarEstoqueEcom(Ecommaterialestoque ecommaterialestoque, Configuracaoecom configuracaoecom) throws UnsupportedEncodingException, IOException {
    	Map<String, String> mapParam = new HashMap<String, String>();
    	
    	mapParam.put("event", "atualizarEstoqueProduto");
    	mapParam.put("id", ecommaterialestoque.getIdentificador());
    	mapParam.put("inventory_quantity", ecommaterialestoque.getQtde().toString());
    	
    	this.execute(this.getUrlIntegracaoEcom(configuracaoecom), gson.toJson(mapParam));
    }

    @SuppressWarnings("unchecked")
	public String enviarMaterialEcom(Ecommaterial ecommaterial, Configuracaoecom configuracaoecom) throws UnsupportedEncodingException, IOException {
    	Double qtde = 0d;
    	if(ecommaterial.getEcommaterialestoque() != null && ecommaterial.getEcommaterialestoque().getQtde() != null){
    		qtde = ecommaterial.getEcommaterialestoque().getQtde();
    	}
    	
    	Map<String, String> mapParam = new HashMap<String, String>();
    	
    	if(ecommaterial.getIdentificador() != null){
    		mapParam.put("event", "atualizarProduto");
	    	mapParam.put("id", ecommaterial.getIdentificador());
    	} else {
	    	mapParam.put("event", "criarProduto");
    	}
    	
    	mapParam.put("name", ecommaterial.getNome());
    	mapParam.put("inventory_quantity", qtde.toString());
    	
    	if(configuracaoecom.getEnviaridentificador() != null && configuracaoecom.getEnviaridentificador() && !StringUtils.isBlank(ecommaterial.getIdentificadormaterial())){
    		mapParam.put("sku", ecommaterial.getIdentificadormaterial());
    	} else if(ecommaterial.getMaterial() != null && ecommaterial.getMaterial().getCdmaterial() != null){
	    	mapParam.put("sku", ecommaterial.getMaterial().getCdmaterial().toString());
    	}
    	
    	if(ecommaterial.getValorvenda() != null)
    		mapParam.put("price", ecommaterial.getValorvenda().toString());
    	
    	if(ecommaterial.getQtdmin() != null)
    		mapParam.put("inventory_minimum", ecommaterial.getQtdmin().toString());

    	if(ecommaterial.getPesobruto() != null)
    		mapParam.put("weight", ecommaterial.getPesobruto().toString());
    	
    	if(ecommaterial.getComprimento() != null)
    		mapParam.put("length", ecommaterial.getComprimento().toString());

    	if(ecommaterial.getLargura() != null)
    		mapParam.put("width", ecommaterial.getLargura().toString());

    	if(ecommaterial.getAltura() != null)
    		mapParam.put("height", ecommaterial.getAltura().toString());
    	
    	if(ecommaterial.getAtivo() != null)
    		mapParam.put("status", ecommaterial.getAtivo().toString());
    	
        String json = this.execute(this.getUrlIntegracaoEcom(configuracaoecom), gson.toJson(mapParam));
        
        Map<String,Object> map = new HashMap<String,Object>();
        map = gson.fromJson(json, map.getClass());
        
        if(map == null || map.get("id") == null) return null;
        
        Object obj = map.get("id");
        if(obj == null) return null;
        
        if(obj instanceof Double){
        	return SinedUtil.descriptionDecimal((Double)obj).replaceAll("\\.", "");
        } else return obj.toString();
    }
	
	@SuppressWarnings("unchecked")
	public boolean consultarCancelamentoPedido(String identificadorPedidovenda, Configuracaoecom configuracaoecom) throws UnsupportedEncodingException, IOException{
		Map<String, String> mapParam = new HashMap<String, String>();
    	
    	mapParam.put("event", "consultarCancelamentoPedido");
    	mapParam.put("id", identificadorPedidovenda);
    	
		String json = this.execute(this.getUrlIntegracaoEcom(configuracaoecom), gson.toJson(mapParam));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map = gson.fromJson(json, map.getClass());
		
		String resultado = map.get("resultado").toString();
		return resultado != null && resultado.trim().toUpperCase().equals("TRUE");
	}

	public void cancelarPedido(String identificador, Configuracaoecom configuracaoecom) throws UnsupportedEncodingException, IOException {
		Map<String, String> mapParam = new HashMap<String, String>();
    	
    	mapParam.put("event", "cancelarPedido");
    	mapParam.put("id", identificador);
    	
		this.execute(this.getUrlIntegracaoEcom(configuracaoecom), gson.toJson(mapParam));
	}
}
