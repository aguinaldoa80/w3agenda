package br.com.linkcom.sined.util.funcao;

import net.sourceforge.jeval.function.Function;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;

public abstract class Funcao implements Function {

	protected Formularhvariavel formularhvariavel;
	
	public Formularhvariavel getFormularhvariavel() {
		return formularhvariavel;
	}
	public void setFormularhvariavel(Formularhvariavel formularhvariavel) {
		this.formularhvariavel = formularhvariavel;
	}
	public Funcao(Formularhvariavel formularhvariavel){
		this.formularhvariavel = formularhvariavel;
	}
}
