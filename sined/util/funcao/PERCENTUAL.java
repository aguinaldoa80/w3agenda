package br.com.linkcom.sined.util.funcao;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.bean.Formularhvariavelintervalo;
import br.com.linkcom.sined.geral.service.FormularhvariavelintervaloService;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.FunctionConstants;
import net.sourceforge.jeval.function.FunctionException;
import net.sourceforge.jeval.function.FunctionResult;

public class PERCENTUAL extends Funcao{

	private String nome;
	
	public PERCENTUAL(Formularhvariavel formularhvariavel, String nome) {
		super(formularhvariavel);
		this.nome = nome;
	}

	public FunctionResult execute(Evaluator evaluator, String arguments)throws FunctionException {
		Double result = new Double(0);
		try {
			validateArguments(arguments);
			String stringValue = new Evaluator().evaluate(arguments, true,false);
			Double argumentOne = Double.parseDouble(stringValue);									
			List<Formularhvariavelintervalo> listaFrhvi = FormularhvariavelintervaloService.getInstance().carregaIntervalos(formularhvariavel);			
			if(listaFrhvi==null || listaFrhvi.isEmpty()){
				result = (argumentOne * formularhvariavel.getValor())/100d;
			}else{
				Formularhvariavelintervalo frhvAux = new Formularhvariavelintervalo();
				for (int i=0;i<listaFrhvi.size();i++){
					Formularhvariavelintervalo formularhvariavelintervalo = listaFrhvi.get(i);
					if(i==listaFrhvi.size()){
						result = (argumentOne * formularhvariavelintervalo.getAliquota())/100d;
						break;
					} else if (argumentOne<formularhvariavelintervalo.getResultadoate() && frhvAux.getResultadoate()!=null && argumentOne>frhvAux.getResultadoate()){
						result = (argumentOne * formularhvariavelintervalo.getAliquota())/100d;
						break;
					} else if (argumentOne<formularhvariavelintervalo.getResultadoate()){
						result = argumentOne * 0d;
						break;
					}
					
					frhvAux = formularhvariavelintervalo;
				}
			}			
		} catch (EvaluationException ee) {
			throw new FunctionException("Express�o inv�lida nos argumentos passados a fun��o "+getName(), ee);
		} catch (Exception e) {
			e.printStackTrace();
			throw new FunctionException("Um argumento � requerido.", e);
		}

		return new FunctionResult(result.toString(),FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
	}

	/**
	 * Met�do criado para validar os valores passados as express�es
	 * @param arguments
	 * @throws EvaluationException
	 */
	private void validateArguments(String arguments) throws EvaluationException {
		if(!NumberUtils.isNumber(arguments))
			throw new EvaluationException("Erro: "+ arguments + " n�o � um numero valido.");		
	}

	public String getName() {
		return nome;
	}	
}