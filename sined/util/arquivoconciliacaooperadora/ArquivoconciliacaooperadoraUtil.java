package br.com.linkcom.sined.util.arquivoconciliacaooperadora;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
public class ArquivoconciliacaooperadoraUtil {

	public static ArquivoconciliacaooperadoraUtil util = new ArquivoconciliacaooperadoraUtil();
	
	public String printRegistro(List<?> listaRegistro) {
		StringBuilder sb = new StringBuilder();
		for (Object obj : listaRegistro) {
			sb.append(obj.toString()).append("\n");
		}
		return sb.toString();
	}
	
	public Properties getPropertiesDefaultValues(String banco_cliente, String type) {
		banco_cliente = banco_cliente.replaceAll("_w3erp_PostgreSQLDS", "");
		
		Properties properties = new Properties();
		InputStream inputstream = null;
		try{
			inputstream = ArquivoconciliacaooperadoraUtil.class.getResourceAsStream("padroes/" + banco_cliente + "_" + type + ".properties");
			properties.load(inputstream);
		} catch (Exception e) {
			try{
				inputstream = ArquivoconciliacaooperadoraUtil.class.getResourceAsStream("padroes/default_" + type + ".properties");
				properties.load(inputstream);
			}catch (Exception e2) {}
		} finally {
			if(inputstream != null){
				try {
					inputstream.close();
				} catch (IOException e) {}
			}
		}
		return properties;
	}

	public String maxPrintString(String str, int tamanho){
		if(str == null) return "";
		if(str.length() > tamanho) return str.substring(0, tamanho);
		else return str;
	}
	
	public String printDouble(Double dbl, int tamanhoNaoDecimal, int tamanhoDecimal){
		if(dbl == null) return "";
		
		String valorStr = dbl.toString();
		String[] valorArray = valorStr.split("\\.");
		return valorArray[0] + (valorArray.length > 1 ? this.printStringNumericRight(valorArray[1], tamanhoDecimal) : this.printStringNumericRight("0", tamanhoDecimal));
	}
	
	public String printStringNumeric(String s, int tamanho){
		return br.com.linkcom.neo.util.StringUtils.stringCheia(s, "0", tamanho, false);
	}
	
	public String printStringNumericRight(String s, int tamanho){
		return br.com.linkcom.neo.util.StringUtils.stringCheia(s, "0", tamanho, true);
	}

	public void stringToFile(String path, String nome, String conteudo) throws IOException {
		long tmp2 = System.currentTimeMillis();
		System.out.println("### Job Arquivo Conciliacao Operadora Inicio (Gravacao) " + tmp2);
		
		File dir = new File(path);
		dir.mkdirs();
		
		File file = new File(path + java.io.File.separator + nome);
        if(!file.exists()){
            boolean createNewFile = file.createNewFile();
            if(!createNewFile) throw new SinedException("Arquivo " + (path + nome) + " n�o criado.");
        }
        
        PrintWriter out = new PrintWriter(new FileWriter(file));
        out.print(conteudo);
        out.close();
        
        System.out.println("### Job Arquivo Conciliacao Operadora Fim (Gravacao) " + tmp2);
	}
	
	public String fileToString(File file) {
		long tmp2 = System.currentTimeMillis();
		System.out.println("### Job Arquivo Conciliacao Operadora Inicio (Carregamento) " + tmp2);
		
		BufferedReader reader = null;
		String retorno = "";
		try{
	        if(file.exists()){
	            StringBuilder fileData = new StringBuilder();
	            reader = new BufferedReader(new FileReader(file));
	            char[] buf = new char[1024];
	            int numRead = 0;
	            while ((numRead = reader.read(buf)) != -1) {
	                String readData = String.valueOf(buf, 0, numRead);
	                fileData.append(readData);
	                buf = new char[1024];
	            }
	            retorno = fileData.toString();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {}
			}
		}
		
		System.out.println("### Job Arquivo Conciliacao Operadora Fim (Carregamento) " + tmp2);
        return retorno;
	}
	
	public String printStringFile(String str){
		if(str == null || str.trim().equals("")){
			return " ";
		} else return str;
	}
	
	public String saveDir(String banco_cliente, boolean processado){
		banco_cliente = banco_cliente.replaceAll("_w3erp_PostgreSQLDS", "");
		
		String saveDir = ArquivoDAO.getInstance().getPathDir() +
							java.io.File.separator +
							"w3erp" +
							java.io.File.separator +
							"arquivoconciliacaooperadora" +
							java.io.File.separator +
							banco_cliente +
							java.io.File.separator +
							(processado ? "processado" : "nao_processado");
		
		return saveDir;
	}
	
	public void enviarEmailErro(Exception e, String descricao) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			String nomeMaquina = "N�o encontrado.";
			try {  
			    InetAddress localaddr = InetAddress.getLocalHost();  
			    nomeMaquina = localaddr.getHostName();  
			} catch (UnknownHostException e3) {}  
			
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			
			String mensagem = descricao + "<BR>M�quina: " + nomeMaquina + "<BR>Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
			
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro no arquivo de concilia��o da operadora de cart�o de cr�dito.")
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception ex) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.", ex);
			}
		}
	}
}
