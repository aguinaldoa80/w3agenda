package br.com.linkcom.sined.util;

public class SinedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int NO_UPDATE = 1;
	
	private int errorCode = 0;

	public SinedException() {
		super();
	}

	public SinedException(String message, Throwable cause) {
		super(message, cause);
	}

	public SinedException(String message) {
		super(message);
	}

	public SinedException(Throwable cause) {
		super(cause);
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public SinedException setErrorCode(int errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
}
