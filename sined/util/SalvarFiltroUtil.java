package br.com.linkcom.sined.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Filtro;
import br.com.linkcom.sined.geral.bean.Filtropapel;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.bean.enumeration.FiltroTipoEnum;
import br.com.linkcom.sined.geral.service.FiltroService;
import br.com.linkcom.sined.geral.service.PapelService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.util.tag.PeriodoTagEnum;

@Bean
public class SalvarFiltroUtil {

	private static final String MAP_PERIODO_PERIODO_PREFIX = "mapPeriodo['";
	private static final String PERIODO_TAG_PARAMETER_SUFFIX = "PeriodoTag']";
	private static final String NULL = "<null>";
	private static final String CHARSET = "ISO-8859-1";
	//String separadora  
	private static final String SEPARATOR = ".a.a.a.";
	// flag que indica se o filtro deve salvo como um novo filtro ou n�o
	private static final String SALVAR_NOVO = "salvarNovo";
	// flag que controla se o filtro salvo deve ser ou n�o utilizado no momento
	// do binding
	private static final String USAR_FILTRO_SALVO = "usarFiltroSalvo";
	private static final String __SALVAR_FILTRO__ = "__salvarFiltro__";
	private static final String LISTA_PAPEL_COMPLETA = "listaPapelCompleta";
	public static final String __FILTRO_SALVO__ = "__filtroSalvo__";
	public static final String __FILTRO_SELECIONADO__ = "__filtroSelecionado__";

	/**
	 * Retorna o ID de uma string no formato nomedopacote.classe[id=x]
	 * 
	 * @param valueString
	 * @return
	 */
	public static Integer getIdFromValueString(String valueString) {
		return valueString.equals(NULL) ? null : Integer.parseInt(valueString.substring(valueString.lastIndexOf("=") + 1, valueString.lastIndexOf("]")));
	}
	
	public static boolean hasFiltroSalvo(WebRequestContext request, String commandName){
		Integer cdfiltro;
		if (StringUtils.isEmpty(request.getServletRequest().getParameter(__FILTRO_SALVO__))) {
			cdfiltro = getFiltroSalvoSessao(request, commandName);
		} else {
			cdfiltro = getIdFromValueString(request.getServletRequest().getParameter(__FILTRO_SALVO__));
		}
		
		if(cdfiltro!=null){
			return (BooleanUtils.toBoolean(request.getParameter(USAR_FILTRO_SALVO)) || request.getServletRequest().getSession().getAttribute(__SALVAR_FILTRO__)!=null);
		}
		return false;
		
	}

	public static void bindingFiltroSalvo(WebRequestContext request, ServletRequestDataBinder binder, Object command, String commandName) {
		Integer cdfiltro;

		// Se n�o receber o filtro como parametro, verifica se existe um filtro
		// a ser carregado na sessao
		if (StringUtils.isEmpty(request.getServletRequest().getParameter(__FILTRO_SALVO__))) {
			cdfiltro = getFiltroSalvoSessao(request, commandName);

		} else {
			cdfiltro = getIdFromValueString(request.getServletRequest().getParameter(__FILTRO_SALVO__));
		}

		if (cdfiltro != null) {
			Filtro filtro = new Filtro();
			filtro.setCdfiltro(cdfiltro);
			filtro = FiltroService.getInstance().load(filtro);
			if(filtro != null){
				Map<String, String[]> mapParams = stringToMap(filtro.getParametros());
				request.setAttribute(__FILTRO_SELECIONADO__, filtro);
	
				// se a flag usarFiltroSalvo nao for true, devo fazer binding usando
				// os campos do filtro e n�o o filtro selecionado
				
				if (BooleanUtils.toBoolean(request.getParameter(USAR_FILTRO_SALVO)) || SalvarFiltroUtil.removeSalvarFiltroSessao(request)) {
					MutablePropertyValues pvs = new MutablePropertyValues();
					List<String> excludedParams = new ArrayList<String>();
					for (String param : mapParams.keySet()) {
						if(!excludedParams.contains(param)){
							String[] valueArray = mapParams.get(param);
							if(valueArray.length==1)
								pvs.addPropertyValue(param, valueArray[0]);
							else pvs.addPropertyValue(param, valueArray);
							excludedParams.addAll( bindingPeriodoTag(request, pvs, param, valueArray[0]) );
						}
					}
	//				try {
	//					command = command.getClass().newInstance();
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
					binder.bind(pvs);
	
					setFiltroSalvoSessao(request, commandName, cdfiltro);
				}
			}
		}
	}

	/**
	 * 
	 * @param request
	 * @param pvs
	 * @param param
	 * @param periodoValue
	 * @return Lista com os nomes dos param�tros os quais n�o devem mais ser feito o binding
	 */
	public static List<String> bindingPeriodoTag(WebRequestContext request, MutablePropertyValues pvs, String param, String periodoValue) {
		List<String> excludedParams = new ArrayList<String>();
		if(param.endsWith(PERIODO_TAG_PARAMETER_SUFFIX)){
			String propertyFromName = param.substring(MAP_PERIODO_PERIODO_PREFIX.length(), param.indexOf("_"));
			String propertyToName = param.substring( param.indexOf("_")+1, param.lastIndexOf("_"));
			
			PeriodoTagEnum periodo = periodoValue==null || periodoValue.equalsIgnoreCase(NULL) ? null :  PeriodoTagEnum.valueOf( periodoValue );
			request.setAttribute(param, periodo);
			String[] propertiesValues = periodo==null ? null : periodo.getPropertiesValues();
			String propertyFromValue = propertiesValues==null ? null : propertiesValues[0];
			String propertyToValue = propertiesValues==null ? null : propertiesValues[1];
			
			if(propertyFromValue!=null){
				pvs.addPropertyValue(propertyFromName, propertyFromValue);
				excludedParams.add(propertyFromName);
			}
			if(propertyToValue!=null){
				pvs.addPropertyValue(propertyToName, propertyToValue);
				excludedParams.add(propertyToName);
			}
		}
		return excludedParams;
	}

	public static String mapToString(Map<String, String[]> map) {
		StringBuilder stringBuilder = new StringBuilder();

		for (String key : map.keySet()) {
			if (stringBuilder.length() > 0) {
				stringBuilder.append("&");
			}
			String value = StringUtils.join(map.get(key), SEPARATOR);
			try {
				stringBuilder.append((key != null ? URLEncoder.encode(key, CHARSET) : ""));
				stringBuilder.append("=");
				stringBuilder.append(value != null ? URLEncoder.encode(value, CHARSET) : "");
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException("Este met�do requer suporte a codifica��o " + CHARSET, e);
			}
		}
		return stringBuilder.toString();
	}

	public static Map<String, String[]> stringToMap(String input) {
		Map<String, String[]> map = new HashMap<String, String[]>();

		String[] nameValuePairs = input.split("&");
		for (String nameValuePair : nameValuePairs) {
			String[] nameValue = nameValuePair.split("=");
			try {
				if(nameValue.length==2){
					String[] value = nameValue[1].split(SEPARATOR);
					for (int i=0; i<value.length;i++) {
						value[i] = URLDecoder.decode(value[i], CHARSET);
					}
					
					map.put(URLDecoder.decode(nameValue[0], CHARSET), value);
				}else{
					map.put(URLDecoder.decode(nameValue[0], CHARSET), new String[]{NULL});
				}
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException("Este met�do requer suporte a codifica��o " + CHARSET, e);
			}
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	private static Filtro bindingSalvarFiltro(WebRequestContext request, Object filtro) {
		Filtro filtroBean = new Filtro();
		HttpServletRequest req = request.getServletRequest();

		Map<String, String[]> mapParams = (Map<String, String[]>) request.getServletRequest().getParameterMap();
		String parametros = mapToString(mapParams);
		String filtroSalvo = req.getParameter(__FILTRO_SALVO__);
		if (StringUtils.isNotEmpty(filtroSalvo)) {
			filtroBean.setCdfiltro(getIdFromValueString(filtroSalvo));
		}

		String path = SinedUtil.pathTela();
		Tela tela = TelaService.getInstance().findByPath(path).get(0);

		filtroBean.setParametros(parametros);
		filtroBean.setTela(tela);
		filtroBean.setClasseFiltro(filtro);

		return filtroBean;
	}

	public static void setSalvarFiltroSessao(WebRequestContext request, Object filtro) {
		request.getServletRequest().getSession().setAttribute(__SALVAR_FILTRO__, bindingSalvarFiltro(request, filtro));
	}

	public static void setFiltroSalvoSessao(WebRequestContext request, String commandName, Integer cdfiltro) {
		request.getServletRequest().getSession().setAttribute(__FILTRO_SALVO__ + commandName, cdfiltro);
	}

	public static boolean removeSalvarFiltroSessao(WebRequestContext request) {
		boolean existeFiltroNaSessao = false;
		if(request.getServletRequest().getSession().getAttribute(__SALVAR_FILTRO__)!=null){
			existeFiltroNaSessao=true;
		}
		request.getServletRequest().getSession().removeAttribute(__SALVAR_FILTRO__);
		return existeFiltroNaSessao;
	}

	public static void removeFiltroSalvoSessao(WebRequestContext request, String commandName) {
		request.getServletRequest().getSession().removeAttribute(__FILTRO_SALVO__ + commandName);
	}

	public static Filtro getSalvarFiltroSessao(WebRequestContext request) {
		return (Filtro) request.getSession().getAttribute(__SALVAR_FILTRO__);
	}

	public static Integer getFiltroSalvoSessao(WebRequestContext request, String commandName) {
		Object cdfiltro = request.getSession().getAttribute(__FILTRO_SALVO__ + commandName);
		if (cdfiltro != null)
			return (Integer) cdfiltro;
		return null;
	}

	public static ModelAndView doModalSalvarFiltro(WebRequestContext request, Filtro filtroBean) {
		List<Papel> listaPapelCompleta = new ArrayList<Papel>();
		if(SinedUtil.isUsuarioLogadoAdministrador()){
			listaPapelCompleta = PapelService.getInstance().findAll();
		}else{
			for (Usuariopapel usuariopapel : SinedUtil.getUsuarioLogado().getListaUsuariopapel()) {
				listaPapelCompleta.add(usuariopapel.getPapel());
			}
		}
		
		List<Papel> listaPapel = new ArrayList<Papel>();
		if (filtroBean != null && filtroBean.getCdfiltro() != null) {
			filtroBean = FiltroService.getInstance().loadForEntrada(filtroBean);
			for (Filtropapel fup : filtroBean.getListaFiltropapel()) {
				listaPapel.add(fup.getPapel());
			}
		}

		filtroBean.setListaPapel(listaPapel);
		request.setAttribute(LISTA_PAPEL_COMPLETA, listaPapelCompleta);
		if (BooleanUtils.toBoolean(request.getParameter(SALVAR_NOVO))) {
			filtroBean.setCdfiltro(null);
			filtroBean.setNome("");
		}
		return new ModelAndView("direct:../popupSalvarFiltro", "filtro", filtroBean);
	}

	public static Filtro doSalvarFiltro(WebRequestContext request, String commandName, Filtro filtroBean) {
		Filtro filtroBeanSessao = SalvarFiltroUtil.getSalvarFiltroSessao(request);

		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		filtroBean.setParametros(filtroBeanSessao.getParametros());
		filtroBean.setTela(filtroBeanSessao.getTela());
		filtroBean.setUsuario(usuarioLogado);

		if(filtroBean.getTipo().equals(FiltroTipoEnum.COMPARTILHADO) && filtroBean.getListaPapel() != null){
			List<Filtropapel> listaFiltropapel = new ArrayList<Filtropapel>();
			for (Papel p : filtroBean.getListaPapel()) {
				Filtropapel fp = new Filtropapel();
				fp.setFiltro(filtroBean);
				fp.setPapel(p);
				listaFiltropapel.add(fp);
			}
			filtroBean.setListaFiltropapel(listaFiltropapel);
		}
		FiltroService.getInstance().saveOrUpdate(filtroBean);
		setFiltroSalvoSessao(request, commandName, filtroBean.getCdfiltro());

		return filtroBeanSessao;
	}

	/**
	 * Configura o request com todos os dados necess�rios para a exibi��o dos filtros salvos
	 * @param request
	 * @param commandName Nome da casse command(geralmente a classe filtro do controller)
	 */
	public static void configureRequestForListagem(WebRequestContext request, String commandName) {
		boolean salvarFiltro;
		if(request.getSession().getAttribute(Parametrogeral.SALVAR_FILTRO) == null){
			salvarFiltro = ParametrogeralService.getInstance().getBoolean(Parametrogeral.SALVAR_FILTRO);
			request.getSession().setAttribute(Parametrogeral.SALVAR_FILTRO, salvarFiltro);
		} else{				
			salvarFiltro = (Boolean) request.getSession().getAttribute(Parametrogeral.SALVAR_FILTRO);
		}
		request.setAttribute(Parametrogeral.SALVAR_FILTRO, salvarFiltro);//importante para ativar o periodoTag
		if(salvarFiltro){
			List<Filtro> listaFiltro = FiltroService.getInstance().findForSinedCrudListagem(request);
			if(listaFiltro != null){
				Boolean existeFiltroSelecionadoValido = Boolean.FALSE;
				
				Integer cdfiltro = SalvarFiltroUtil.getFiltroSalvoSessao(request, commandName);
				if(cdfiltro != null){
					existeFiltroSelecionadoValido = listaFiltro.contains(new Filtro(cdfiltro));
				}
				request.setAttribute("exibirFiltroSalvo", true);
				request.setAttribute("existeFiltroSelecionadoValido", existeFiltroSelecionadoValido);
				
				Integer cdusuario = SinedUtil.getUsuarioLogado().getCdpessoa();
				
				List<Filtro> listaFiltrosIndividuais = new ArrayList<Filtro>();
				List<Filtro> listaFiltrosCompartilhados = new ArrayList<Filtro>();
				for (Filtro f : listaFiltro) {
					if(f.getUsuario().getCdpessoa().equals(cdusuario))
						listaFiltrosIndividuais.add(f);
					else listaFiltrosCompartilhados.add(f);
				}
				
				request.setAttribute("podeSalvarOuExcluirFiltro", listaFiltrosIndividuais.contains(new Filtro(cdfiltro)));
				request.setAttribute("listaFiltrosIndividuais", listaFiltrosIndividuais);
				request.setAttribute("listaFiltrosCompartilhados", listaFiltrosCompartilhados);				
			}
		}
	}
	
	public static JsonModelAndView doExcluirFiltro(WebRequestContext request, String commandName){
		Filtro filtroBean = FiltroService.getInstance().loadByCdfiltro(request);
		FiltroService.getInstance().delete(filtroBean);
		String nome = filtroBean.getNome();
		request.addMessage("Filtro \""+nome+ "\" exclu�do com sucesso.");
		SalvarFiltroUtil.removeFiltroSalvoSessao(request, commandName);
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("success", true);
		return json;
	}
	
}