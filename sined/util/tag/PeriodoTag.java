package br.com.linkcom.sined.util.tag;

import br.com.linkcom.neo.view.template.TemplateTag;

/**
 * 
 * @author Fl�vio Tavares
 *
 */
public class PeriodoTag extends TemplateTag{
	
	private String topLabel;
	private Boolean showTopLabel = true;
	private String middleLabel;
	private String propertyFrom;
	private String propertyTo;
	private String style;
	private String stylePeriodo;
	private String styleClass;
	private String onKeyPress;
	private boolean typeDate = true;
	
	public String getTopLabel() {
		return topLabel;
	}
	public Boolean getShowTopLabel() {
		return showTopLabel;
	}
	public String getMiddleLabel() {
		if(middleLabel == null) return "at�";
		return middleLabel;
	}
	public String getPropertyFrom() {
		return propertyFrom;
	}
	public String getPropertyTo() {
		return propertyTo;
	}
	public String getStyle() {
		return style;
	}
	public String getStylePeriodo() {
		return stylePeriodo;
	}
	public String getOnKeyPress() {
		return onKeyPress;
	}
	public boolean isTypeDate() {
		return typeDate;
	}
	public String getStyleClass() {
		return styleClass;
	}
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
	public void setTypeDate(boolean typeDate) {
		this.typeDate = typeDate;
	}
	public void setOnKeyPress(String onKeyPress) {
		this.onKeyPress = onKeyPress;
	}
	public void setTopLabel(String topLabel) {
		this.topLabel = topLabel;
	}
	public void setShowTopLabel(Boolean showTopLabel) {
		this.showTopLabel = showTopLabel;
	}
	public void setMiddleLabel(String middleLabel) {
		this.middleLabel = middleLabel;
	}
	public void setPropertyFrom(String propertyFrom) {
		this.propertyFrom = propertyFrom;
	}
	public void setPropertyTo(String propertyTo) {
		this.propertyTo = propertyTo;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public void setStylePeriodo(String stylePeriodo) {
		this.stylePeriodo = stylePeriodo;
	}
	
	
	@Override
	protected void doComponent() throws Exception {
		pushAttribute("topLabel",getTopLabel());
		pushAttribute("showTopLabel",getShowTopLabel());
		pushAttribute("middleLabel",getMiddleLabel());
		pushAttribute("propertyFrom", getPropertyFrom());
		pushAttribute("propertyTo", getPropertyTo());
		pushAttribute("style", getStyle());
		pushAttribute("stylePeriodo", getStylePeriodo());
		pushAttribute("keyPress", getOnKeyPress());
		pushAttribute("periodoEnumId", generateUniqueId());
		pushAttribute("typeDate", isTypeDate());
		pushAttribute("styleClass", getStyleClass());
		includeJspTemplate();
		popAttribute("topLabel");
		popAttribute("middleLabel");
		popAttribute("propertyFrom");
		popAttribute("propertyTo");
		popAttribute("style");
		popAttribute("stylePeriodo");
		popAttribute("periodoEnumId");
		popAttribute("typeDate");
		popAttribute("styleClass");
	}
}
