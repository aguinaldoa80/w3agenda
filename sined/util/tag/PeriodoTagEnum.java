package br.com.linkcom.sined.util.tag;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.linkcom.sined.util.DateUtils;

public enum PeriodoTagEnum {
	
	ONTEM("Ontem"),
	HOJE("Hoje"),
	AMANHA("Amanh�"),
	
	SEMANA_CORRENTE("Semana corrente"), 
	MES_CORRENTE("M�s corrente"), 
	ANO_CORRENTE("Ano corrente"), 
	
	A_PARTIR_DESSE_MES("A partir desse m�s"),
	A_PARTIR_DESSE_ANO("A partir desse ano"),
	
	ATE_O_FINAL_MES("At� o final do m�s"),
	ATE_O_FINAL_ANO("At� o final do ano"),
	
	MES_PASSADO("M�s passado");
	
	
	private String nome;
		
	private PeriodoTagEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}

	public String[] getPropertiesValues() {
		Date propertyFrom = null;
		Date propertyTo = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		switch (this) {
		case ONTEM:
			 propertyFrom = DateUtils.ontem();
			 propertyTo = DateUtils.ontem();
			break;

		case HOJE:
			 propertyFrom = DateUtils.hoje();
			 propertyTo = DateUtils.hoje();
			break;

		case AMANHA:
			 propertyFrom = DateUtils.amanha();
			 propertyTo = DateUtils.amanha();
			break;

		case SEMANA_CORRENTE:
			 propertyFrom = DateUtils.primeiroDiaSemana();
			 propertyTo = DateUtils.ultimoDiaSemana();
			break;

		case ANO_CORRENTE:
			propertyFrom = DateUtils.primeiroDiaAno();
			propertyTo = DateUtils.ultimoDiaAno();
			break;

		case A_PARTIR_DESSE_MES:
			 propertyFrom = DateUtils.primeiroDiaMes();
			 propertyTo = null;
			break;

		case A_PARTIR_DESSE_ANO:
			 propertyFrom = DateUtils.primeiroDiaAno();
			 propertyTo = null;
			break;
		
		case ATE_O_FINAL_MES:
			 propertyFrom = DateUtils.hoje();
			 propertyTo = DateUtils.ultimoDiaMes();
			break;
		case ATE_O_FINAL_ANO:
			 propertyFrom = DateUtils.hoje();
			 propertyTo = DateUtils.ultimoDiaAno();
			break;
		case MES_PASSADO:
			 propertyFrom = DateUtils.primeiroDiaMesPassado();
			 propertyTo = DateUtils.ultimoDiaMesPassado();
			break;	
		case MES_CORRENTE:
			 propertyFrom = DateUtils.primeiroDiaMes();
			 propertyTo = DateUtils.ultimoDiaMes();
			break;
		default:
			break;
		}
		
		return new String[]{ 
			(propertyFrom==null ? "<null>" : sdf.format(propertyFrom)), 
			(propertyTo==null ? "<null>" : sdf.format(propertyTo))
		};
	}
	
	
}
