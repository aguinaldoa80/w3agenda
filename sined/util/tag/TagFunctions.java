package br.com.linkcom.sined.util.tag;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClassificacaoService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.LotematerialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.LicenseManager;
import br.com.linkcom.sined.util.SinedUtil;


public class TagFunctions {
	
	public static String truncate(String texto,Integer size) {
		if(texto != null && texto.length() >= size)
			texto = texto.substring(0, size)+" ...";
		
		return texto;
	}

	public static String getTelaDescricao() {
		return TelaService.getInstance().getTelaDescriptionByUrl(getPartialURL());
	}
	
	public static String getPartialURL(){
		return Util.web.getFirstUrl();
	}
	
	public static String getURL(){
		return SinedUtil.getUrlWithContext();
	}
	
	public static String getURLWithoutContext(){
		return SinedUtil.getUrlWithoutContext();
	}
	
	
	public static String getTotalPage(FiltroListagem filtro){
		if((filtro.getCurrentPage() + 1) == filtro.getNumberOfPages())
			return String.valueOf(filtro.getNumberOfResults());
		else
			return String.valueOf((filtro.getCurrentPage() + 1) * filtro.getPageSize());
	}
	
	public static String getTotalPagePaginacaoSimples(FiltroListagem filtro, List<?> lista){
		if((filtro.getCurrentPage() + 1) == filtro.getNumberOfPages())
			return String.valueOf((filtro.getCurrentPage()) * filtro.getPageSize() + lista.size());
		else
			return String.valueOf((filtro.getCurrentPage() + 1) * filtro.getPageSize());
	}
	
	/**
	 * @author Jo�o Paulo Zica
	 */
	public static Boolean getModuloPermissao(String path){
		return SinedUtil.getPermissaoModulo(path);
	}
	
	public static String findUserByCd(Integer code){
		Pessoa pessoa = PessoaService.getInstance().findPessoaByCodigo(code);
		if(pessoa != null){
			return pessoa.getNome();
		}
		return null;
	}
	
	public static String formataTimestamp(Timestamp timestamp){
		return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(timestamp);
	}
	
	public static String formataData(Date data){
		return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
	}
	
	public static String formataDataWithoutTime(Date data){
		if(data == null) return "";
		return new SimpleDateFormat("dd/MM/yyyy").format(data);
	}
	
	public static String getContextPath(){
		return NeoWeb.getRequestContext().getServletRequest().getContextPath();
	}
	
	public static String getBgColor(){
		String contextPath = NeoWeb.getRequestContext().getServletRequest().getContextPath();
		
		if(contextPath.contains("teste") && !contextPath.contains("cleber"))
			return "red";
		
		return "";
	}
	
	public static boolean isCleberApplication(){
		return Neo.getApplicationName().contains("cleber");	
	}
	
	public static Boolean needAprovacao(){
		return new SinedUtil().needAprovacao();		
	}
	
	public static String statusLicenca(){
		return LicenseManager.statusLicenca();
	}
	
	public static Boolean isBriefcase(){
		return SinedUtil.isBriefcase();		
	}
	
	public static Contagerencial carregaContagerencial(Contagerencial contagerencial){
		if(contagerencial != null && contagerencial.getCdcontagerencial() != null){
 			try{
 				if(contagerencial.getNome() == null || contagerencial.getVcontagerencial() == null || contagerencial.getVcontagerencial().getIdentificador() == null){
 					return ContagerencialService.getInstance().loadWithIdentificador(contagerencial);
 				}
			}catch (Exception e) {
				return ContagerencialService.getInstance().loadWithIdentificador(contagerencial);
			}
		} 
		return contagerencial;
	}
	
	public static ContaContabil carregaContacontabil(ContaContabil contacontabil){
		if(contacontabil != null && contacontabil.getCdcontacontabil() != null){
			try{
				if(contacontabil.getNome() == null || contacontabil.getVcontacontabil() == null || contacontabil.getVcontacontabil().getIdentificador() == null){
					return ContacontabilService.getInstance().loadWithIdentificador(contacontabil);
				}
			}catch (Exception e) {
				return ContacontabilService.getInstance().loadWithIdentificador(contacontabil);
			}
		} 
		return contacontabil;
	}
	
	public static Categoria carregaCategoria(Categoria categoria){
		if(categoria != null && categoria.getCdcategoria() != null){
			if(categoria.getNome() == null || categoria.getVcategoria() == null || categoria.getVcategoria().getIdentificador() == null){
				return CategoriaService.getInstance().loadWithIdentificador(categoria);
			} 
		} 
		return categoria;
	}
	
	public static Classificacao carregaClassificacao(Classificacao classificacao){
		if(classificacao != null && classificacao.getCdclassificacao() != null){
			if(classificacao.getNome() == null || classificacao.getVclassificacao() == null || classificacao.getVclassificacao().getIdentificador() == null){
				return ClassificacaoService.getInstance().loadWithIdentificador(classificacao);
			} 
		} 
		return classificacao;
	}
	
	public static Materialcategoria carregaMaterialcategoria(Materialcategoria materialcategoria){
		if(materialcategoria != null && materialcategoria.getCdmaterialcategoria() != null){
			return MaterialcategoriaService.getInstance().loadForEntrada(materialcategoria);
		} 
		return materialcategoria;
	}
	
	public static String descriptionContagerencial(Contagerencial contagerencial){
		if(contagerencial != null && contagerencial.getCdcontagerencial() != null){
			if(contagerencial.getNome() == null || contagerencial.getVcontagerencial() == null || contagerencial.getVcontagerencial().getIdentificador() == null){
				contagerencial = ContagerencialService.getInstance().loadWithIdentificador(contagerencial);
			} 
			
			return contagerencial.getDescricaoInputPersonalizado();
		} 
		return "";
	}
	
	public static String descriptionContacontabil(ContaContabil contacontabil){
		if(contacontabil != null && contacontabil.getCdcontacontabil() != null){
			if(contacontabil.getNome() == null || contacontabil.getVcontacontabil() == null || contacontabil.getVcontacontabil().getIdentificador() == null){
				contacontabil = ContacontabilService.getInstance().loadWithIdentificador(contacontabil);
			} 
			
			return contacontabil.getDescricaoInputPersonalizado();
		} 
		return "";
	}
	
	public static String descriptionCategoria(Categoria categoria){
		if(categoria != null && categoria.getCdcategoria() != null){
			if(categoria.getNome() == null || categoria.getVcategoria() == null || categoria.getVcategoria().getIdentificador() == null){
				categoria = CategoriaService.getInstance().loadWithIdentificador(categoria);
			} 
			
			return categoria.getDescricaoInputPersonalizado();
		} 
		return "";
	}
	
	public static String descriptionClassificacao(Classificacao classificacao){
		if(classificacao != null && classificacao.getCdclassificacao() != null){
			if(classificacao.getNome() == null || classificacao.getVclassificacao() == null || classificacao.getVclassificacao().getIdentificador() == null){
				classificacao = ClassificacaoService.getInstance().loadWithIdentificador(classificacao);
			} 
			
			return classificacao.getDescricaoInputPersonalizado();
		} 
		return "";
	}
	
	public static String descriptionMaterialcategoria(Materialcategoria materialcategoria){
		if (materialcategoria!=null && materialcategoria.getCdmaterialcategoria()!=null){
			return MaterialcategoriaService.getInstance().loadForEntrada(materialcategoria).getDescricaoAutocompleteTreeView();
		} 
		return "";
	}
	
	public static String descriptionLoteestoque(Loteestoque loteestoque, String cdmaterial){
		if(loteestoque != null && loteestoque.getCdloteestoque() != null){
			if(loteestoque.getNumero() == null || loteestoque.getValidade() == null){
				Material material = null;
				try { material = new Material(Integer.parseInt(cdmaterial)); } catch (Exception e) {}
				
				loteestoque = LoteestoqueService.getInstance().loadWithNumeroValidade(loteestoque, material);
			}
			return loteestoque.getNumerovalidade();
		} 
		return "";
	}
	
	public static Boolean isSpedFiscal(){
		return SinedUtil.isSpedFiscal();
	}
	
	public static Integer intervaloNotificacao(){
		return SinedUtil.intervaloNotificacao();
	}
	
	public static Boolean isAtalhoCadastrado(String path){
		return SinedUtil.isAtalhoCadastrado(path);
	}
	
	public static Boolean isEmpresaRequired(){
		return SinedUtil.isEmpresaRequired();
	}
	
	/**
	* M�todo que busca a ordem das colunas caso tenha sido definida
	*
	* @param exibir
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> campolistagemOrdem(){
		return SinedUtil.findCampolistagemOrdem();
	}
	
	/**
	* M�todo que busca as colunas a serem exibidas caso tenha sido definida
	*
	* @param exibir
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> campolistagemNaoexibir(){
		return SinedUtil.findCampolistagemNaoexibir();
	}
	
	/**
	* M�todo que busca as colunas fixas da listagem caso tenha sido definida
	*
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> campolistagemColunafixa(){
		return SinedUtil.findCamposlistagemColunaFixa();
	}
	
	/**
	 * Verifica se existe campos da listagem cadastrados para a tela
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public static Boolean isCampolistagemCadastrado(){
		return SinedUtil.isCampolistagemCadastrado();
	}
	
	/**
	 * Verifica se o usu�rio logado � administrador do sistema.
	 *
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public static Boolean isUsuarioAdministrador(){
		return SinedUtil.isUsuarioLogadoAdministrador();
	}
	
	/**
	 * Verifica se o sistema est� em ambiente de desenvolvimento.
	 *
	 * @return
	 * @since 28/06/2012
	 * @author Rodrigo Freitas
	 */
	public static Boolean isAmbienteDesenvolvimento(){
		return SinedUtil.isAmbienteDesenvolvimento();
	}
	
	public static Boolean isRecapagem(){
		return SinedUtil.isRecapagem();
	}
	
	public static Boolean isSegmentoOtr(){
		return SinedUtil.isSegmentoOtr();
	}
	
	public static Boolean isRateioMovimentacaoEstoque(){
		return SinedUtil.isRateioMovimentacaoEstoque();
	}
	
	public static Boolean exibirObsRateioConta(){
		return SinedUtil.exibirObsRateioConta();
	}
	
	public static String descriptionDecimal(Double dbl){
		return SinedUtil.descriptionDecimal(dbl);
	}
	
	public static String formatMoney(Money m){
		return SinedUtil.formatMoney(m);
	}
	
	public static String descriptionMoney(Double dbl){
		return SinedUtil.descriptionDecimal(dbl, true);
	}
	
	public static String descriptionMoneyRound(Double dbl){
		return SinedUtil.descriptionDecimal(SinedUtil.round(dbl, 4), true);
	}
	
	public static Boolean isUserHasAction(String acao){
		return SinedUtil.isUserHasAction(acao);
	}
	
	public static Boolean verificaEmailPendencia(){
		return ControleAcessoUtil.util.verificaEmailUsuarioPendencia();
	}
	/**
	 * Metodo que verifica se um objeto existe na lista
	 * @param list
	 * @param object
	 * @return
	 * @author Luiz Rom�rio Filho
	 * @since 02/10/2013
	 */
	public static Boolean contains(List<Object> list, Object object){
		 return list.contains(object);
	}
	
	public static String getParametrogeral(String nome){
		return ParametrogeralService.getInstance().getValorPorNome(nome);
	}
	
	public static boolean getPermissaoEmpresa(Integer id){
		return id == null || id == 0 || new SinedUtil().getUsuarioPermissaoEmpresaId(id);
	}
	
	public static boolean getPermissaoFornecedor(Integer id){
		return id == null || id == 0 || new SinedUtil().getUsuarioPermissaoFornecedorId(id);
	}
	
	
	public static String getDescriptionLoteestoqueWithValidade(Loteestoque loteestoque, Material material){
		StringBuilder s = new StringBuilder();
		if(loteestoque != null && loteestoque.getCdloteestoque() != null){
			if(loteestoque.getNumero() != null){
				s.append(loteestoque.getNumero());
			}
			
			try {
				if(!Hibernate.isInitialized(loteestoque.getListaLotematerial()) || (loteestoque.getListaLotematerial() == null || loteestoque.getListaLotematerial().isEmpty())){
					loteestoque.setListaLotematerial(LotematerialService.getInstance().findAllByLoteestoque(loteestoque));
				}
				if(material != null && loteestoque.getListaLotematerial() != null && !loteestoque.getListaLotematerial().isEmpty()){
					for(Lotematerial lotematerial : loteestoque.getListaLotematerial()){
						if(material != null && material.equals(lotematerial.getMaterial()) && lotematerial.getValidade() != null){
							s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(lotematerial.getValidade()));
						}
					}
				}	
			} catch (Exception e) {}
		}
		return s.toString();
	}
	
}
