<%@page import="br.com.linkcom.sined.util.tag.PeriodoTagEnum"%>
<%@page import="br.com.linkcom.sined.util.tag.PeriodoTag"%>
<%@page import="br.com.linkcom.sined.util.controller.IFiltroSalvo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${!empty propertyFrom && !empty propertyTo}">

<c:set var="instanceOfIFiltroSalvo" value="<%= new Boolean(request.getAttribute("filtro") != null && request.getAttribute("filtro") instanceof IFiltroSalvo) %>" />
<c:if test="${instanceOfIFiltroSalvo}">
	<c:set var="periodoName" value="${propertyFrom}_${propertyTo}_PeriodoTag" ></c:set>
	<c:set var="periodoValue" value="${filtro.mapPeriodo[periodoName]}"></c:set>
</c:if>

<n:panel>
	<c:if test="${showTopLabel}">
		${topLabel}<BR>
	</c:if>
	<t:propertyConfig showLabel="false" renderAs="single">
		<span id="datas_${periodoEnumId}" style="${periodoValue != null ? 'display: none' : ''}">
			<t:property name="${propertyFrom}" style="${style}" class="${styleClass}" onKeyUp="${keyPress}"/> 
			${middleLabel}
			<t:property name="${propertyTo}"  style="${style}" class="${styleClass}" onKeyUp="${keyPress}"/>
		</span>
		
		<c:if test="${instanceOfIFiltroSalvo && typeDate && requestScope.SALVAR_FILTRO==true}">
			<span id="periodo_${periodoEnumId}" style="${periodoValue == null ? 'display: none' : ''}">
				<n:input style="${stylePeriodo}" name="mapPeriodo['${propertyFrom}_${propertyTo}_PeriodoTag']" itens="<%=PeriodoTagEnum.values() %>" type="<%=PeriodoTag.class %>"  blankLabel="Selecione..." id="${periodoEnumId}" value="${periodoValue}" />
			</span>
		
			<span id="spanPeriodo_${periodoEnumId}" style="${periodoValue != null ? 'display: none' : ''}">
				<i class="fas fa-share-square" id="imgPeriodo_${periodoEnumId}" title="DEFINIR PER�ODO"></i>
			</span>
			<span id="spanDatas_${periodoEnumId}" style="${periodoValue == null ? 'display: none' : ''}">
				<i class="fas fa-trash-alt" id="imgDatas_${periodoEnumId}" title="LIMPAR PER�ODO"></i>
			</span>
		</c:if>
	</t:propertyConfig>
</n:panel>

<script type="text/javascript">
$(document).ready(function(){
	$('#imgPeriodo_${periodoEnumId}').click(function(){
		var $propertyFrom = $("input[name=${propertyFrom}]");
		var $propertyTo = $("input[name=${propertyTo}]");
		
		$("#spanDatas_${periodoEnumId}").show();
		$("#spanPeriodo_${periodoEnumId}").hide();
		
		$propertyFrom.val("01/01/1970");
		$propertyTo.val("01/01/1970");
		
		$("#${periodoEnumId}").val("<null>");
		
		$('#datas_${periodoEnumId}').hide();
		$('#periodo_${periodoEnumId}').show();
	});
	
	$('#imgDatas_${periodoEnumId}').click(function(){
		var $propertyFrom = $("input[name=${propertyFrom}]");
		var $propertyTo = $("input[name=${propertyTo}]");
	
		$("#spanPeriodo_${periodoEnumId}").show();
		$("#spanDatas_${periodoEnumId}").hide();
		
		$propertyFrom.val("");
		$propertyTo.val("");
		
		$("#${periodoEnumId}").val("<null>");
		
		$('#datas_${periodoEnumId}').show();
		$('#periodo_${periodoEnumId}').hide();
	});
});
</script>	

</c:if>