<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>
<%@ taglib prefix="f" tagdir="/WEB-INF/tags"%>

<style>

#boxes #dialog {
	position:absolute;
	z-index:9999;
	display:none;
	background-color:#fff; 
	padding:20px;
	color:#000;
}

#boxes #mask {
	position:absolute;
	z-index:9998;  
	display:none;
	background-color:#000; 
	opacity: 0.7;
}
  
</style>

<div id="boxes">
	<div id="dialog">
		<center>
			<b><u>Recalcular rateio</u></b><br/>
			<div style="text-align:left">
				<input name="btnPorValorPercentualRecalcularValores" type="radio" value="1" checked="checked" /><u>Por Valor</u>&nbsp;(Mant�m percentual e altera valor)<br/>
				<input name="btnPorValorPercentualRecalcularValores" type="radio" value="2" /><u>Por Percentual</u>&nbsp;(Mant�m valor e altera percentual)<br/><br/>
			</div>
			<button type="button" onClick="" class="btnApp" id="btnOkRecalcularValores">Ok</button>
			<button type="button" onClick="" class="btnApp" id="btnCancelarRecalcularValores">Cancelar</button>
		</center>
	</div>
	<div id="mask"></div>
</div>

<!-- DETALHE RATEIO -->
<n:panel title="${label}" colspan="${colspan}" onSelectTab="listenerTab${nameFunctions}();">
	<t:propertyConfig mode="input" renderAs="double">
		<n:doBody/>
	</t:propertyConfig>
	
	<input type="hidden" name="recalcularRateioPorValor" value="false"/>
	<input type="hidden" name="recalcularRateioPorPercentual" value="false"/>
	<t:property name="${name}.cdrateio" type="hidden" write="false"/>
	<t:detalhe onNewLine="${onNewLineDetalhe}" showBotaoNovaLinha="${mostrarNovaLinha}" showDeleteLineWarning="${warningDeleteLine}" showBotaoRemover="${mostrarNovaLinha}" name="${name}.listaRateioitem" id="listaRateioItem_${nameFunctions}"  beforeDeleteLine="triggerExcluir${nameFunctions}" >
		<n:column header="Conta gerencial">
			<n:body>
				<f:contagerencial styleClass="required" name="contagerencial" tipooperacao="${tipoOperacaoRateio.sigla}" whereInNaturezaContagerencial="${whereInNaturezaContagerencial}" whereInCdcontagerencial="${whereInCdcontagerencial}" onchange="${nomeFuncaoVerificaLimiteContagerencial};" width="250" renderAs="single"/>
			</n:body>
		</n:column>		
	  	<t:property name="centrocusto" style="width:180px;" itens="listaCentrocusto_${nameFunctions}" onChange="carregaListaProjeto${nameFunctions}(this);${nomeFuncaoVerificaLimiteContagerencial};" insertPath="/sistema/crud/Centrocusto"/>
		<t:property name="projeto" onChange="carregaCentrocusto${nameFunctions}(this);" style="width:160px;" itens="listaProjeto_${nameFunctions}" insertPath="/projeto/crud/Projeto"/>
  		<c:if test="${!empty exibirObservacao && exibirObservacao}">
			<n:column header="Observa��o">
	  			<t:property name="observacao" style="width: 158px;background-color:white" rows="1"/>
			</n:column>
  		</c:if>
		<n:column header="Valor" style="text-align: left;">
			<t:property name="valor" onChange="javascript:calcPercent${nameFunctions}(this,$s.getRowIndex(this))${nomeFuncaoVerificaLimiteContagerencial}" style="width:100px;" onlyPositiveNumbers="true"/>
		</n:column>
		<n:column header="Percentual">
			<t:property name="percentual" id="percentual"  onChange="javascript:calcValor${nameFunctions}(this,$s.getRowIndex(this));${nomeFuncaoVerificaLimiteContagerencial}" style="width:100px;" onlyPositiveNumbers="true" onKeyPress="return verifica_numeros${nameFunctions}(this,event);"/>
			<t:property name="cdrateioitem" type="hidden" write="false" />
			<t:property name="taxabaixaacrescimo" type="hidden" write="false" />
		</n:column>
	</t:detalhe>
	<br>
	<n:panelGrid columns="8" style="padding-left: 70%;" columnStyleClasses="propertyColumn propertyColumn">
		<t:propertyConfig showLabel="true" mode="input" renderAs="doubleline">
			<t:property name="${name}.valortotaux" type="money" labelStyleClass="labelColumn" class="disabled" onfocus="$(this).blur();"/>
		</t:propertyConfig>
		<t:propertyConfig showLabel="true" mode="input" renderAs="doubleline">
			<t:property name="${name}.valoraux" labelStyleClass="labelColumn" class="disabled" onfocus="$(this).blur();"/>
		</t:propertyConfig>
	</n:panelGrid>
</n:panel>

<script>
function carregaCentrocusto${nameFunctions}(campo){
	var projeto = campo.value;
	if(projeto != "" && projeto != "<null>"){
		var lista = campo.name.split("[")[0];
		var index = $s.getRowIndex(campo);
		
		var params = {
			projeto: projeto
		}
		
		jQuery.post("${app}/sistema/process/AjaxRateio?ACAO=carregaCentrocusto",params,function(data){
			eval(data);
			if(centrocusto != null){
				form[lista + '[' + index + '].centrocusto'].value = centrocusto;
			}
		});
	}
}

function carregaListaProjeto${nameFunctions}(campo){
	var centrocusto = campo.value;
	var lista = campo.name.split("[")[0];
	var index = $s.getRowIndex(campo);
	
	var params = {
		centrocusto: centrocusto
	}
	
	$.getJSON("${app}/sistema/process/AjaxRateio?ACAO=carregaListaProjeto",params,function(json){
		
		var valueProjetoAnterior = $("[name='rateio.listaRateioitem[" + index + "].projeto']").val();
		$("[name='rateio.listaRateioitem[" + index + "].projeto']").empty();
		$("[name='rateio.listaRateioitem[" + index + "].projeto']").addOption("<null>", "");
		
		var opcoesProjeto = json.opcoesProjeto;
		
		opcoesProjeto.forEach(function (projeto, n){
			$("[name='rateio.listaRateioitem[" + index + "].projeto']").append(
				new Option(projeto.nome,"br.com.linkcom.sined.geral.bean.Projeto[cdprojeto="+projeto.cdprojeto+"]") );
		});
		
		if(typeof valueProjetoAnterior != 'undefined' && valueProjetoAnterior != '<null>' && 
				valueProjetoAnterior != ''){
			$("[name='rateio.listaRateioitem[" + index + "].projeto']").val(valueProjetoAnterior);
		}
	});
}


function listenerTab${nameFunctions}(){
	var valor = getPropertyValor${nameFunctions}();
	valor = $s.toFloat(valor);
	
	if((valor > 0 && valor != "") || (valor == 0 && ${aceitaValorZerado})){
		enableAll${nameFunctions}();
		atualizaValorTotal${nameFunctions}();
		atualizaValorRestante${nameFunctions}();
		recalcularValores${nameFunctions}();
	}else{
		//disableAll${nameFunctions}();
		alert("Valor a ser rateado n�o foi informado.");
	}

}

function verifica_numeros${nameFunctions}(campo,event){
	var text = campo.value;
	var tecla = $s.getTecla(event);
	
	var insertComma = (tecla == 44 && text.length > 0 && text.indexOf(",") == -1);
	
	if (tecla == 0 || tecla == 8 || tecla >= 48 && tecla <= 57 || insertComma) { 
		return true;
	}
	else return false;
}

var atualizaValores${nameFunctions} = true;
function enableAll${nameFunctions}(){
	atualizaValores${nameFunctions} = true;
	$("input[name^=${name}.]").removeAttr("readOnly");
	$("button[id*=${name}.]").removeAttr("disabled");
	// $("select[name^=${name}.]").removeAttr("disabled");
}
function disableAll${nameFunctions}(){
	atualizaValores${nameFunctions} = false;
	$("input[name^=${name}.]").attr("readOnly","readOnly");
	$("button[id*=${name}.]").attr("disabled","disabled");
	// $("select[name^=${name}.]").attr("disabled","disabled");
}


/**
*	Fun��o disparada ao excluir linha do detalhe rateio.
*	Soma o valor atual do registro ao valor restante do rateio. 
*/
function triggerExcluir${nameFunctions}(index, id){
	indice = index.substring(index.lastIndexOf("=")+1,index.lastIndexOf("]"));
	var valor = $("input[name^=${name}\\.listaRateioitem\\["+(indice - 1)+"\\]\\.valor]").val()
	valor = valor.replace(".","");
	valor = $s.toFloat(valor);
	if(isNaN(valor)) return;
	somaValorRestante${nameFunctions}(valor);
}

function calcValor${nameFunctions}(field, index){
	if(!atualizaValores${nameFunctions}) return;
	var percent = $s.toFloat(field.value);
	var valor = (percent * getValorTotal${nameFunctions}()) / 100;
	setValorOnIndex${nameFunctions}(valor.toFixed(2),index);
	atualizaValorRestante${nameFunctions}();
}

function calcPercent${nameFunctions}(field, index){
	if(!atualizaValores${nameFunctions}) return;
	if(getPropertyValor${nameFunctions}() == 0) return;
	var valor = $s.toFloat(field.value);
	var percent = (valor * 100) / getValorTotal${nameFunctions}();
	setPercentualOnIndex${nameFunctions}(percent, index);
	atualizaValorRestante${nameFunctions}();
}


// FUN��ES DE MANIPULA��O DOS CAMPOS

function setPercentualOnIndex${nameFunctions}(percent, index){
	var input = "input[name^=${name}\\.listaRateioitem\\["+index+"\\]\\.percentual]";
	if(isNaN(percent) || !isFinite(percent)){
		percent = "";
	}
	$(input).val($s.toDecimal(percent));	
}
function setValorOnIndex${nameFunctions}(valor, index){
	var input = "input[name^=${name}\\.listaRateioitem\\["+index+"\\]\\.valor]";
	if(isNaN(valor)){
		valor = "";
	}
	$(input).val(valor.replace(".",","));	
}
function getValorTotal${nameFunctions}(){
	try{
		var valor = form['${propertyValor}'].value;
	}catch(e){
		alert("N�o foi poss�vel obter o valor total com o campo ${propertyValor}.");
	}
	if(valor == 0) valor = form['${name}.valoraux'].value;
	valor = $s.toFloat(valor);
	return valor.toFixed(2);
}
function getValorRestante${nameFunctions}(){
	var rest = form['${name}.valoraux'].value;
	rest = $s.toFloat(rest);
	return parseFloat(rest.toFixed(2));
}
function getSomaValoresRestante${nameFunctions}(){
	var restTot = 0.0;
	$("input[name^=${name}\\.listaRateioitem][name$=\\.valor]").each(function(){
		valor = $s.toFloat(this.value);
		if(isNaN(valor)){
			valor = 0;
		} 
		restTot = restTot + valor;	
	});	
	return restTot;
}
function setValorRestante${nameFunctions}(rest){
	rest = new String(rest);
	//rest = rest.replace(".",",");
	$(form['${name}.valoraux']).val(rest);
}
function somaValorRestante${nameFunctions}(valor){
	var soma = getValorRestante${nameFunctions}() + $s.toFloat(valor);
	setValorRestante${nameFunctions}(soma.toFixed(2));
}
function atualizaValorRestante${nameFunctions}(){
	var atual = getValorTotal${nameFunctions}() - getSomaValoresRestante${nameFunctions}();
	setValorRestante${nameFunctions}($s.toMoney(atual));
}
function atualizaValorTotal${nameFunctions}(){
	form['${name}.valortotaux'].value = getPropertyValor${nameFunctions}();
}


var confirmar${nameFunctions} = ${confirmar};
var valor${nameFunctions} = "";
var firstTime${nameFunctions} = true;

$(document).ready(function(){
	changePropertyValor${nameFunctions}();
	
	var cont = countItensRateio${nameFunctions}();
	if(cont == 0){
		//$("button[id=${name}.listaRateioitem_btnNewline]").click();
	}
	
	
	if(${!mostrarNovaLinha}){
		$("button[id=${name}\\.listaRateioitem_btnNewline]").hide();
	}
	
	valor${nameFunctions} = getPropertyValor${nameFunctions}();
	firstTime${nameFunctions} = ($("input[name='${propertyValor}']").val() == ""); 
	
	// EVENTO DE MUDAN�A DA PROPRIEDADE VALOR DO MASTER.
	$("input[name='${propertyValor}']").change(function(){
		changePropertyValor${nameFunctions}();
	});
	
	$('#btnOkRecalcularValores').click(function (e) {
		$('#btnCancelarRecalcularValores').click();
		var porValor = $("input[name=btnPorValorPercentualRecalcularValores]:checked").val()=="1" ? "true" : "false";
		var porPercentual = $("input[name=btnPorValorPercentualRecalcularValores]:checked").val()=="2" ? "true" : "false";
		$(form['recalcularRateioPorValor']).val(porValor);
		$(form['recalcularRateioPorPercentual']).val(porPercentual);
		processarValores${nameFunctions}(true);
	});	
	
	$('#btnCancelarRecalcularValores').click(function (e) {
		$("#dialog").hide();
		$("#mask").hide();
	});	
});

function changePropertyValor${nameFunctions}(event){
	
	//var conf;
	
	if (firstTime${nameFunctions}){
		//conf = true;
		processarValores${nameFunctions}(true);
	}else{
		if (confirmar${nameFunctions} && existeItemRateioInserido${nameFunctions}()){
			//Corre��o para OK. Falta confirmar onde acontece o erro.
			if ($("#dialog").css("display") == "block") return;
			
			conf = confirm("Se o valor for alterado, os itens do rateio ser�o atualizados.\nDeseja continuar?");
			if (conf){
				modalRecalcularValor();
				return;
			}else {
				processarValores${nameFunctions}(false);
			}
		}else {
			//conf = true;
			processarValores${nameFunctions}(true);
		}
	}
	
//	if(conf){
//		atualizaValorTotal${nameFunctions}();
//		if(getPropertyValor${nameFunctions}() != 0)	recalcularValores${nameFunctions}();
//		valor${nameFunctions} = getPropertyValor${nameFunctions}();
//		firstTime${nameFunctions} = false;		
//		
//	}else{
//		setPropertyValor${nameFunctions}(valor${nameFunctions});
//	}
//	if('${nomeFuncaoVerificaLimiteContagerencial}' != '')
//			${nomeFuncaoVerificaLimiteContagerencial};
}

function processarValores${nameFunctions}(conf){
	if (conf){
		atualizaValorTotal${nameFunctions}();
		if(getPropertyValor${nameFunctions}() != 0)	recalcularValores${nameFunctions}();
		valor${nameFunctions} = getPropertyValor${nameFunctions}();
		firstTime${nameFunctions} = false;		
	} else {
		setPropertyValor${nameFunctions}(valor${nameFunctions});
	}	
	if('${nomeFuncaoVerificaLimiteContagerencial}' != '')
		${nomeFuncaoVerificaLimiteContagerencial};	
	if('${nomeFuncaoMontaApropriacoes}' != '')
		${nomeFuncaoMontaApropriacoes};	
}

function getPropertyValor${nameFunctions}(){
	var valor = $("input[name='${propertyValor}']").val();
	if(valor == "") return 0;
	return valor;
}

function setPropertyValor${nameFunctions}(valor){
	$("input[name='${propertyValor}']").val(valor);
}

/**
*	M�todo utilizado para recalcular os valores dos itens dos rateios
*	com base nos percentuais dos mesmos e no valor atual da conta.
*
*	@see calculaValor(percent)
*/
function recalcularValores${nameFunctions}(){
	var valor = 0;
	var vritem = 0;
	var valorMestre = $s.toFloat(getPropertyValor${nameFunctions}());
	if((${alteraValor} || valorMestre == 0 || $(form['recalcularRateioPorValor']).val()=='true') && $(form['recalcularRateioPorPercentual']).val()!='true'){
		$("input[name^=${name}\\.listaRateioitem][name$=percentual]").each(function(){
			var index = $s.getRowIndex(this.name);
			vritem = calculaValor${nameFunctions}(this.value);
			try{
				if(vritem != null){
					$("input[name^=${name}\\.listaRateioitem\\["+index+"\\]\\.valor]").val(vritem);
				}
			}catch(e){}
			i++;
		});
	}else {
		$("input[name^=${name}\\.listaRateioitem][name$=valor]").each(function(){
			var index = $s.getRowIndex(this.name);
			valor = calculaPercent${nameFunctions}(this.value);
			try{
				if(valor != null){
					$("input[name^=${name}\\.listaRateioitem\\["+index+"\\]\\.percentual]").val($s.toDecimal(valor));
				}
			}catch(e){}
			i++;
		});
	}
	atualizaValorRestante${nameFunctions}();
}
/**
*	Calcula e monta o percentual do rateio no campo.
*/
function calculaPercent${nameFunctions}(valor){
	var percent = null;
	if(valor != ""){
		var valor = $s.toFloat(valor);
		percent = (valor * 100) / getValorTotal${nameFunctions}();
	}
	return percent;
}

/**
*	Calcula e monta o valor do item de rateio no campo.
*/
function calculaValor${nameFunctions}(percent){
	var valor = null;
	if(percent != ""){
		var percent =  $s.toFloat(percent);
		 valor = (percent * getValorTotal${nameFunctions}()) / 100;
	}
	return $s.toMoney(valor);
}

/**
*	Verifica se existe algum item de rateio inserido na lista
*/
function existeItemRateioInserido${nameFunctions}(){
	var cont = countItensRateio${nameFunctions}();
	return cont != 0;			
}
function countItensRateio${nameFunctions}(){
	var cont = 0;
	$("input[name^=${name}\\.listaRateioitem]").each(function(){
		cont++;
	});
	return cont;
}

function modalRecalcularValor(){
	var dialog = $("#dialog");
	var mask = $("#mask");
	
	var heightTela = $(window).height();
	var widthTela = $(window).width();
	var height = 80;
	var width = 450;
	var top = $(window).height()/2 - height/2;
	var left = $(window).width()/2 - width/2;
	
	$('#dialog').css({'display':'block', 'top':top, 'left':left, 'height':height, 'width':width});
	$('#mask').css({'top':0, 'left':0, 'height':heightTela, 'width':widthTela});
	$('#mask').show();
	
	//setTimeout(ajusteLayoutClickFocus,1);
}

function ajusteLayoutClickFocus(){
	var elem = $('input[name=btnPorValorPercentualRecalcularValores][checked=checked]');
	if(typeof elem != 'undefined' && elem.length > 0){ 
		$(elem[0]).focus();
		$(elem[0]).click();
		$(elem[0]).blur();
	}
}

</script>