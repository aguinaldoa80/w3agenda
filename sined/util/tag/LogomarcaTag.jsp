<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<div class="logo" style="padding-left: 5px; padding-top: 2px;">
	<c:if test="${putLink}">
		<a href="${ctx}" style="border: 0px; text-decoration: none;">
	</c:if>
		<c:if test="${!empty cdlogomarca && showlogo}">
			<img src="${ctx}/DOWNLOADFILE/${cdlogomarca}" class="imagelogo" border="0"/>&nbsp;&nbsp;&nbsp;
		</c:if>
		<img src="${urlImagem}" border="0" style="width: ${widthLogo}px" class="imglogow3"/>
	<c:if test="${putLink}">
		</a>
	</c:if>
</div>

<style>

	.imglogow3 {
		transition: margin 500ms, width 500ms, height 500ms, transform 250ms;
    	transform-origin: left center;	
	}

	.imglogow3:hover {
		transform: scale(1.15);
	}
</style>
