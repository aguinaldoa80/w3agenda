package br.com.linkcom.sined.util.tag;

import br.com.linkcom.neo.view.template.TemplateTag;
import br.com.linkcom.sined.util.SinedUtil;

public class LogomarcaTag extends TemplateTag{

	protected boolean showLogo = true;
	protected boolean putLink = true;
	protected String widthLogo = "90";
	
	@Override
	protected void doComponent() throws Exception {	
		try {
			String imagemStr = SinedUtil.getLogoEmpresaCabecalho(getRequest());
			
			pushAttribute("putLink", this.putLink);
			pushAttribute("urlImagem", imagemStr);
			pushAttribute("showlogo", this.showLogo);
			pushAttribute("widthLogo", this.widthLogo);
			includeJspTemplate();
			popAttribute("showlogo");
			popAttribute("urlImagem");
			popAttribute("putLink");
			popAttribute("widthLogo");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isShowLogo() {
		return showLogo;
	}

	public void setShowLogo(boolean showLogo) {
		this.showLogo = showLogo;
	}

	public boolean isPutLink() {
		return putLink;
	}

	public void setPutLink(boolean putLink) {
		this.putLink = putLink;
	}

	public String getWidthLogo() {
		return widthLogo;
	}

	public void setWidthLogo(String widthLogo) {
		this.widthLogo = widthLogo;
	}

}
