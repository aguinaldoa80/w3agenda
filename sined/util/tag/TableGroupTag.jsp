<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="t" uri="template"%>

<c:set var="panelWidth" value="${n:default('100%',TableGroupTag.panelgridWidth)}"/>
<c:if test="${TableGroupTag.filtroAvancado}">
	<c:set var="filtroAvancado" value="ADVANCED"/>
</c:if>


<n:panel colspan="${TableGroupTag.panelColspan}" class="${filtroAvancado}">
	<n:panelGrid columns="${TableGroupTag.columns}" columnStyles="padding-right:5px;" border="0" cellspacing="0" cellpadding="0" width="${panelWidth}">
 		<t:propertyConfig renderAs="doubleline">
 			<n:doBody />
 		</t:propertyConfig>
 	</n:panelGrid>
 </n:panel>

 					