package br.com.linkcom.sined.util.tag;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.template.TemplateTag;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.util.SinedUtil;

/**
 * 
 * @author Fl�vio Tavares
 * @since 20/05/2008
 *
 */
public class RateioTag extends TemplateTag{

	protected String name;
	protected Rateio bean;
	protected List<Contagerencial> listaContagerencial;
	protected List<Projeto> listaProjeto;
	protected List<Centrocusto> listaCentrocusto;
	protected List<Rateioitem> listaRateioitem;
	protected Money valor;
	protected Tipooperacao tipoOperacao;
	
	protected String label;
	protected Integer colspan;
	protected String propertyValor;
	protected Boolean confirmar;
	protected Boolean mostrarNovaLinha = true;
	protected Boolean warningDeleteLine = true;
	protected Boolean alteraValor = false;
	private String uniqueId;
	private String nameFunctions;
	protected Boolean aceitaValorZerado = false;
	private String nomeFuncaoVerificaLimiteContagerencial;
	private String nomeFuncaoMontaApropriacoes;
	protected String whereInNaturezaContagerencial;
	protected String whereInCdcontagerencial;
	protected String onNewLineDetalhe;
	protected Boolean exibirObservacao = false;
		
	@Override
	protected void doComponent() throws Exception {
		pushAttribute("name", getName());
		pushAttribute("tipoOperacaoRateio", getTipoOperacao());
		pushAttribute("nameFunctions", getNameFunctions());
		pushAttribute("listaContagerencial_" + getNameFunctions(), getListaContagerencial());
		pushAttribute("listaProjeto_" + getNameFunctions(), getListaProjeto());
		pushAttribute("listaCentrocusto_" + getNameFunctions(), getListaCentrocusto());
		pushAttribute("label", getLabel());
		pushAttribute("colspan", getColspan());
		pushAttribute("propertyValor", getPropertyValor());
		pushAttribute("confirmar", !Util.booleans.isFalse(getConfirmar()));
		pushAttribute("mostrarNovaLinha", getMostrarNovaLinha());
		pushAttribute("warningDeleteLine", getWarningDeleteLine());
		pushAttribute("alteraValor", getAlteraValor());
		pushAttribute("exibirObservacao", getExibirObservacao());
		pushAttribute("aceitaValorZerado", getAceitaValorZerado());
		pushAttribute("nomeFuncaoVerificaLimiteContagerencial", getNomeFuncaoVerificaLimiteContagerencial());
		pushAttribute("nomeFuncaoMontaApropriacoes", getNomeFuncaoMontaApropriacoes());
		pushAttribute("whereInNaturezaContagerencial", getWhereInNaturezaContagerencial());
		pushAttribute("whereInCdcontagerencial", getWhereInCdcontagerencial());
		pushAttribute("onNewLineDetalhe", getOnNewLineDetalhe());
		includeJspTemplate();
		popAttribute("name");
	}
	
	public String getName() {
		return name;
	}
	public Rateio getBean() {
		return bean;
	}
	public List<Contagerencial> getListaContagerencial() {
		if(this.listaContagerencial == null && this.tipoOperacao != null){
			if(whereInNaturezaContagerencial != null && !"".equals(whereInNaturezaContagerencial)){
				listaContagerencial = ContagerencialService.getInstance().findAnaliticasByNatureza(this.tipoOperacao, whereInNaturezaContagerencial);
			}else {
				listaContagerencial = ContagerencialService.getInstance().findAnaliticas(this.tipoOperacao);
			}
		}else if(whereInNaturezaContagerencial != null && !"".equals(whereInNaturezaContagerencial)){
			listaContagerencial = ContagerencialService.getInstance().findByNatureza(whereInNaturezaContagerencial);
		}
		return listaContagerencial;
	}
	public List<Projeto> getListaProjeto() {
		if(SinedUtil.isListEmpty(listaProjeto)){
			listaProjeto = ProjetoService.getInstance().findAll();
		}
		return listaProjeto;
	}
	@SuppressWarnings("unchecked")
	public List<Centrocusto> getListaCentrocusto() {
		if(SinedUtil.isListEmpty(listaCentrocusto)){
			List<Centrocusto> listaCentrocustoEscolhido = null;
			if(listaRateioitem != null && listaRateioitem.size() > 0){
				try{
					listaCentrocustoEscolhido = (List<Centrocusto>) CollectionsUtil.getListProperty(listaRateioitem, "centrocusto");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			listaCentrocusto = CentrocustoService.getInstance().findAtivos(listaCentrocustoEscolhido);
		}
		return listaCentrocusto;
	}
	public List<Rateioitem> getListaRateioitem() {
		return listaRateioitem;
	}
	public Money getValor() {
		return valor;
	}
	public Tipooperacao getTipoOperacao() {
		return tipoOperacao;
	}
	public Integer getColspan() {
		return colspan;
	}
	public String getPropertyValor() {
		return propertyValor;
	}
	public Boolean getConfirmar() {
		return confirmar;
	}
	public String getUniqueId() {
		if(StringUtils.isBlank(uniqueId)){
			uniqueId = generateUniqueId();
		}
		return uniqueId;
	}
	public String getNameFunctions() {
		if (StringUtils.isBlank(nameFunctions)) {
			nameFunctions = getName() + getUniqueId();
		}
		return nameFunctions;
	}
	public Boolean getMostrarNovaLinha() {
		return mostrarNovaLinha;
	}
	
	public Boolean getWarningDeleteLine() {
		return warningDeleteLine;
	}
	public Boolean getAlteraValor() {
		return alteraValor;
	}
	
	public Boolean getAceitaValorZerado() {
		return aceitaValorZerado;
	}
	
	public String getNomeFuncaoVerificaLimiteContagerencial() {
		return nomeFuncaoVerificaLimiteContagerencial;
	}

	public void setAceitaValorZerado(Boolean aceitaValorZerado) {
		this.aceitaValorZerado = aceitaValorZerado;
	}

	public void setWarningDeleteLine(Boolean warningDeleteLine) {
		this.warningDeleteLine = warningDeleteLine;
	}

	public void setMostrarNovaLinha(Boolean mostrarNovaLinha) {
		this.mostrarNovaLinha = mostrarNovaLinha;
	}
	public void setNameFunctions(String nameFunctions) {
		this.nameFunctions = nameFunctions;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public void setConfirmar(Boolean confirmar) {
		this.confirmar = confirmar;
	}
	public void setPropertyValor(String propertyValue) {
		this.propertyValor = propertyValue;
	}
	public void setColspan(Integer colspan) {
		this.colspan = colspan;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBean(Rateio bean) {
		this.bean = bean;
	}
	public void setListaContagerencial(List<Contagerencial> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
	public void setListaProjeto(List<Projeto> listaProjeto) {
		this.listaProjeto = listaProjeto;
	}
	public void setListaCentrocusto(List<Centrocusto> listaCentrocusto) {
		this.listaCentrocusto = listaCentrocusto;
	}
	public void setListaRateioitem(List<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setTipoOperacao(Tipooperacao tipooperacao) {
		this.tipoOperacao = tipooperacao;
	}
	public String getLabel() {
		if(StringUtils.isBlank(label)){
			label = "Rateio";
		}
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setAlteraValor(Boolean alteraValor) {
		this.alteraValor = alteraValor;
	}
	public void setNomeFuncaoVerificaLimiteContagerencial(
			String nomeFuncaoVerificaLimiteContagerencial) {
		this.nomeFuncaoVerificaLimiteContagerencial = nomeFuncaoVerificaLimiteContagerencial;
	}

	public String getWhereInNaturezaContagerencial() {
		return whereInNaturezaContagerencial;
	}
	public void setWhereInNaturezaContagerencial(
			String whereInNaturezaContagerencial) {
		this.whereInNaturezaContagerencial = whereInNaturezaContagerencial;
	}

	public String getWhereInCdcontagerencial() {
		return whereInCdcontagerencial;
	}

	public void setWhereInCdcontagerencial(String whereInCdcontagerencial) {
		this.whereInCdcontagerencial = whereInCdcontagerencial;
	}
	
	public String getOnNewLineDetalhe() {
		if(onNewLineDetalhe == null){
			return "";
		}
		return onNewLineDetalhe;
	}

	public void setOnNewLineDetalhe(String onNewLineDetalhe) {
		this.onNewLineDetalhe = onNewLineDetalhe;
	}

	public Boolean getExibirObservacao() {
		return exibirObservacao;
	}

	public void setExibirObservacao(Boolean exibirObservacao) {
		this.exibirObservacao = exibirObservacao;
	}

	public String getNomeFuncaoMontaApropriacoes() {
		return nomeFuncaoMontaApropriacoes;
	}

	public void setNomeFuncaoMontaApropriacoes(
			String nomeFuncaoMontaApropriacoes) {
		this.nomeFuncaoMontaApropriacoes = nomeFuncaoMontaApropriacoes;
	}
}