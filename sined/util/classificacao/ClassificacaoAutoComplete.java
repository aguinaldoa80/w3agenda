package br.com.linkcom.sined.util.classificacao;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.service.ClassificacaoService;

@Bean
@Controller(path="/sistema/process/ClassificacaoAutoComplete")
public class ClassificacaoAutoComplete extends MultiActionController{

	private ClassificacaoService classificacaoService;
	
	public void setClassificacaoService(ClassificacaoService classificacaoService) {this.classificacaoService = classificacaoService;}
	
	@DefaultAction
	public void view(WebRequestContext request, ClassificacaoTreeFiltro filtro){
		List<Classificacao> find = classificacaoService.findAutocomplete(filtro.getQ());
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(find));
	}

	private String criarEstrutura(List<Classificacao> find) {
		StringBuilder builder = new StringBuilder();
		if(find != null && find.size() > 0){
			for (Classificacao c : find) {
				builder.append(c.getDescricaoInputPersonalizado() + "|" + c.getCdclassificacao() + "\n");
			}
		}
		return builder.toString();
	}
	
}
