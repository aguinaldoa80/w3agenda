package br.com.linkcom.sined.util.classificacao;

public class ClassificacaoTreeFiltro {

	private String propriedade;
	private String q;

	public String getQ() {
		return q;
	}
	
	public String getPropriedade() {
		return propriedade;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
	public void setQ(String q) {
		this.q = q;
	}
	
}
