package br.com.linkcom.sined.util.classificacao;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.service.ClassificacaoService;

@Bean
@Controller(path="/sistema/process/ClassificacaoTreeView")
public class ClassificacaoTreeController extends MultiActionController{

	private ClassificacaoService classificacaoService;
	
	public void setClassificacaoService(ClassificacaoService classificacaoService) {this.classificacaoService = classificacaoService;}
	
	@DefaultAction
	public ModelAndView view(WebRequestContext request, ClassificacaoTreeFiltro filtro){
		List<Classificacao> listaClassificacao = classificacaoService.findTreeView();
		String codigo_treeview = this.criaEstrutura(listaClassificacao);
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		return new ModelAndView("direct:/process/classificacaoTreeView");
	}

	private String criaEstrutura(List<Classificacao> listaClassificacao) {
		if(listaClassificacao != null && listaClassificacao.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (Classificacao c : listaClassificacao) {
				if(c.getFilhos() != null && c.getFilhos().size() > 0){
					sb.append("<li><span>");
					sb.append(c.getVclassificacao().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("</span><ul>");
					sb.append(this.criaEstrutura(c.getFilhos()));
					sb.append("</ul></li>");
				} else {
					sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
					sb.append(c.getCdclassificacao());
					sb.append(", '");
					sb.append(c.getVclassificacao().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("')\">");
					sb.append(c.getVclassificacao().getIdentificador());
					sb.append(" - ");
					sb.append(c.getNome());
					sb.append("</span></li>");
				}
			}
			return sb.toString();
		} else return "";
	}
	
}
