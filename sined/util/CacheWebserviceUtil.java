package br.com.linkcom.sined.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovenda;

public class CacheWebserviceUtil {

	private static Map<String, List<Emporiumpedidovenda>> mapCacheEmporiumpedidovenda = new HashMap<String, List<Emporiumpedidovenda>>();
	private static Map<String, List<Arquivoemporium>> mapCacheArquivoemporiumCliente = new HashMap<String, List<Arquivoemporium>>();
	private static Map<String, Map<Integer, String>> mapCacheConsultaPagamento = new HashMap<String, Map<Integer,String>>();
	
	public static void putConsultaPagamento(Integer cddocumento, String statuspagamento){
		if(!NeoWeb.isInRequestContext()) return;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return;
		
		synchronized (mapCacheConsultaPagamento) {
			Map<Integer, String> map = mapCacheConsultaPagamento.get(url);
			if (map == null) {
				map = new HashMap<Integer, String>();
			}
			map.put(cddocumento, statuspagamento);

			mapCacheConsultaPagamento.put(url, map);
		}
	}
	
	public static String getConsultaPagamento(Integer cddocumento){
		if(!NeoWeb.isInRequestContext()) return null;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return null;
		
		synchronized (mapCacheConsultaPagamento) {
			Map<Integer, String> map = mapCacheConsultaPagamento.get(url);
			if(map == null){
				return null;
			}
			return map.get(cddocumento);
		}
	}
	
	public static void removeEmporiumpedidovenda(){
		if(!NeoWeb.isInRequestContext()) return;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return;
		
		synchronized (mapCacheEmporiumpedidovenda) {
			mapCacheEmporiumpedidovenda.remove(url);
		}
	}
	
	public static void putEmporiumpedidovenda(List<Emporiumpedidovenda> lista){
		if(!NeoWeb.isInRequestContext()) return;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return;
		
		synchronized (mapCacheEmporiumpedidovenda) {
			mapCacheEmporiumpedidovenda.put(url, lista);
		}
	}
	
	public static List<Emporiumpedidovenda> getEmporiumpedidovenda(){
		if(!NeoWeb.isInRequestContext()) return null;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return null;
		
		synchronized (mapCacheEmporiumpedidovenda) {
			return mapCacheEmporiumpedidovenda.get(url);
		}
	}
	
	public static void removeArquivoemporiumCliente(){
		if(!NeoWeb.isInRequestContext()) return;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return;
		
		synchronized (mapCacheArquivoemporiumCliente) {
			mapCacheArquivoemporiumCliente.remove(url);
		}
	}
	
	public static void putArquivoemporiumCliente(List<Arquivoemporium> lista){
		if(!NeoWeb.isInRequestContext()) return;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return;
		
		synchronized (mapCacheArquivoemporiumCliente) {
			mapCacheArquivoemporiumCliente.put(url, lista);
		}
	}
	
	public static List<Arquivoemporium> getArquivoemporiumCliente(){
		if(!NeoWeb.isInRequestContext()) return null;
		String url = SinedUtil.getUrlWithoutContext();
		if(url == null) return null;
		
		synchronized (mapCacheArquivoemporiumCliente) {
			return mapCacheArquivoemporiumCliente.get(url);
		}
	}
	
	public static void limparCache(){
		synchronized (mapCacheConsultaPagamento) {
			mapCacheConsultaPagamento = new HashMap<String, Map<Integer,String>>();
		}
		synchronized (mapCacheEmporiumpedidovenda) {
			mapCacheEmporiumpedidovenda = new HashMap<String, List<Emporiumpedidovenda>>();
		}
		synchronized (mapCacheArquivoemporiumCliente) {
			mapCacheArquivoemporiumCliente = new HashMap<String, List<Arquivoemporium>>();
		}
	}
	
}
