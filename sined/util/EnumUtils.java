package br.com.linkcom.sined.util;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.TranslateEnumBean;

public class EnumUtils {

	public static <E extends Enum<E>> List<TranslateEnumBean> translateEnumToBean(Class<E> en){
		List<TranslateEnumBean> lista = new ArrayList<TranslateEnumBean>();
		for(Enum<?> en1: en.getEnumConstants()){
			lista.add(new TranslateEnumBean(en1.name(), en1.ordinal(), en1.toString()));
		}
		return lista;
	}
	
	public static TranslateEnumBean translateEnum(Enum<?> en1){
		if(en1 == null){
			return null;
		}
		return new TranslateEnumBean(en1.name(), en1.ordinal(), en1.toString());
	}
}
