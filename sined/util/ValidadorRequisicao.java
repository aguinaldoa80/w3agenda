package br.com.linkcom.sined.util;


public class ValidadorRequisicao {

	public static int gerarCodigo(String login, String senha){
		int codigo = 0;
		if(login == null){
			login = "___";
		}
		if(senha == null){
			senha = "_____";
		}
		codigo += login.length() * 7 - senha.length() * 2;
		return codigo;
	}
}