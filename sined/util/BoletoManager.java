package br.com.linkcom.sined.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

/**
 * Classe respons�vel por processar o templates e substituir vari�veis atribu�das.
 * Default prefix: {
 * Degault sufix: }
 * 
 * ex: new TemplateManager("/WEB-INF/template/teste.tpl")
 *     .assign("key","teste valor")
 *     .assign("key002","teste Valor 2")
 *     .getTemplate();
 *     
 * Devolve uma String com o resultado da substitui��o das chaves.
 * 
 * @author Pedro Gon�alves
 */
public class BoletoManager {
	private String texto;
	private String prefix = "{";
	private String sufix = "}";
	private HashMap<String, Object> mapaKeys = new HashMap<String, Object>();
	
	/**
	 * Construtor default da classe
	 * @param template - ex: /web-inf/template/teste.tpl
	 */
	public BoletoManager(String texto) {
		this.texto = texto;
	}
	
	/**
	 * Construtor usado para alterar o sufixo e o prefixo default
	 * 
	 * @param template
	 * @param prefix - default "{"
	 * @param sufix - default "}"
	 */
	public BoletoManager(String texto,String prefix,String sufix) {
		this.texto = texto;
		this.prefix = prefix;
		this.sufix = sufix;
	}
	
	/**
	 * m�todo utilizado para setar o prefixo
	 * 
	 * @param prefix
	 * @return
	 */
	public BoletoManager setPrefix(String prefix) {
		this.prefix = prefix;
		return this;
	}
	
	/**
	 * m�todo utilizado para setar o sufixo
	 * 
	 * @param sufix
	 * @return
	 */
	public BoletoManager setSufix(String sufix) {
		this.sufix = sufix;
		return this;
	}
	
	/**
	 * Adiciona uma vari�vel para ser localizada e processada no template
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public BoletoManager assign(String key, String value) {
		mapaKeys.put(prefix+key+sufix, value);
		return this;
	}
	
	/**
	 * Processa as vari�veis e retorna uma String com o conte�do do template j� modificado.
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getTemplate() throws IOException{
		processaTemplate();
		return texto;
	}
	
	private void processaTemplate() {
		Set<String> keys = mapaKeys.keySet();
		String value = "";
		for (String key : keys) {
			value = mapaKeys.get(key) != null ? mapaKeys.get(key).toString() : "";
			texto = texto.replace(key, value);
		}
	}
}
