package br.com.linkcom.sined.util;

public interface PermissaoFornecedorEmpresa {

	/**
	 * M�todo onde tem que se retornar a string com a query
	 * que limita o bean aos fornecedores vinculados a empresa do usu�rio.
	 * 
	 * LINGUAGEM: HQL
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public String subQueryFornecedorEmpresa();
	
}
