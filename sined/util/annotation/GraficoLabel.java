package br.com.linkcom.sined.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Andrey Leonardo
 * Annotation criada para gera��o do gr�fico pizza
 * no process An�lise de Custo no tipo sint�tico.
 * @since 24/06/2015	
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface GraficoLabel {
	String value();
}