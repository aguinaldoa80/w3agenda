package br.com.linkcom.sined.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation que indica que a propriedade ser� comparada com ela mesma, vinda de outra instancia do objeto que ela pertence.
 * Deve ser colocado no metodo get da propriedade
 * @author Daniel
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CompararCampo {
}
