package br.com.linkcom.sined.util.emporium;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Emporiumidentificacaocliente;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Identificacaocliente;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmporiumidentificacaoclienteService;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.registros.RegistroCliente;
import br.com.linkcom.sined.util.emporium.registros.RegistroEndereco;
import br.com.linkcom.sined.util.emporium.registros.RegistroIdentificacaoCliente;

public class IntegracaoEmporiumImportacaoClientesUtil {

	public static IntegracaoEmporiumImportacaoClientesUtil util = new IntegracaoEmporiumImportacaoClientesUtil();
	
	public void executarJobIntegracao(String banco_cliente) throws IOException {
		this.criarArquivoImportacao(banco_cliente, null);
	}
	
	public Arquivoemporium criarArquivoImportacao(String banco_cliente, Integer id_cliente) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		ArquivoImportacaoEmporium bean = this.processaRegistrosW3(banco_cliente, id_cliente);
		
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroCliente));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroIdentificacaoCliente));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroEndereco));
		
		String strFile = sb.toString();
		strFile = strFile.replaceAll("\\|\\|", "| |");
		strFile = strFile.replaceAll("\\|\\|", "| |");
		
		if(strFile != null && strFile.trim().length() > 0){
			String nomeArquivo = "arquivo_importacaoclientes_" + SinedUtil.datePatternForEmporium();
			String pathArquivo = IntegracaoEmporiumUtil.util.saveDir(banco_cliente, false);
			
			IntegracaoEmporiumUtil.util.stringToFile(pathArquivo, nomeArquivo + ".txt", strFile);
			
			Resource resource = new Resource("text/plain", nomeArquivo + ".txt", strFile.getBytes());
			
			Arquivo arquivo = new Arquivo();
			
			arquivo.setNome(resource.getFileName());
			arquivo.setContent(resource.getContents());
			arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
			arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
			arquivo.setTipoconteudo(resource.getContentType());
			
			Arquivoemporium arquivoemporium = new Arquivoemporium();
			arquivoemporium.setArquivo(arquivo);
			arquivoemporium.setArquivoemporiumtipo(Arquivoemporiumtipo.IMPORTACAO_CLIENTES);
			
			ArquivoDAO.getInstance().saveFile(arquivoemporium, "arquivo");
			ArquivoemporiumService.getInstance().saveOrUpdate(arquivoemporium);
			
			CacheWebserviceUtil.removeArquivoemporiumCliente();
			
			// DELETA TABELAS AUXILIARES DE EXCLUS�O
			ArquivoemporiumService.getInstance().deleteTabelasAuxiliares();
			
			return arquivoemporium;
		}
		
		return null;
	}
	
	private ArquivoImportacaoEmporium processaRegistrosW3(String banco_cliente, Integer id_cliente) {
		ArquivoImportacaoEmporium bean = new ArquivoImportacaoEmporium();
		
		List<Cliente> listaCliente = ClienteService.getInstance().findForIntegracaoEmporium(id_cliente);
		
		List<Emporiumidentificacaocliente> listaEmporiumidentificaocliente = EmporiumidentificacaoclienteService.getInstance().findAll();
		String code_codigointerno = this.getCode_type(listaEmporiumidentificaocliente, Identificacaocliente.CODIGO_INTERNO);
		
		for (Cliente cliente : listaCliente) {
			String cdcliente = cliente.getCdpessoa().toString();	
			String nome = cliente.getNome();
			String razaosocial = cliente.getRazaosocial();
			String email = cliente.getEmail();
			String tipopessoa = "";
			String cpf = null;
			String cnpj = null;
			String rg = null;
			String ie = null;
			if(cliente.getTipopessoa() != null){
				if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
					tipopessoa = "1";
					cpf = cliente.getCpf() != null ? cliente.getCpf().getValue() : null;
					rg = cliente.getRg() != null && !cliente.getRg().equals("") ? cliente.getRg() : null;
				} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
					tipopessoa = "2";
					cnpj = cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null;
					ie = cliente.getInscricaoestadual() != null && !cliente.getInscricaoestadual().equals("") ? cliente.getInscricaoestadual() : null;
				}
			}
			
			Set<Telefone> listaTelefone = cliente.getListaTelefone();
			String telefoneprincipal = this.getTelefone(listaTelefone, Telefonetipo.PRINCIPAL);
			String telefonecelular = this.getTelefone(listaTelefone, Telefonetipo.CELULAR);
			
			RegistroCliente registroCliente = new RegistroCliente();
			IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "Cliente", registroCliente);
			registroCliente.setCode_type(code_codigointerno);
			registroCliente.setCode(cdcliente);
			registroCliente.setCustomer_type(tipopessoa);
			registroCliente.setCustomer_name(IntegracaoEmporiumUtil.util.maxPrintString(nome, 60));
			registroCliente.setCustomer_name_alt(IntegracaoEmporiumUtil.util.maxPrintString(razaosocial, 60));
			registroCliente.setCustomer_email(IntegracaoEmporiumUtil.util.maxPrintString(email, 50));
			registroCliente.setCustomer_phone1(IntegracaoEmporiumUtil.util.maxPrintString(telefoneprincipal, 15));
			registroCliente.setCustomer_phone2(IntegracaoEmporiumUtil.util.maxPrintString(telefonecelular, 15));
			
			bean.listaRegistroCliente.add(registroCliente);
			
			List<Identificacao> listaIdentificacao = new ArrayList<Identificacao>();
//			listaIdentificacao.add(new Identificacao(Identificacaocliente.CODIGO_INTERNO, cdcliente));
			if(cpf != null)	listaIdentificacao.add(new Identificacao(Identificacaocliente.CPF, cpf));
			if(cnpj != null) listaIdentificacao.add(new Identificacao(Identificacaocliente.CNPJ, cnpj));
			if(rg != null) listaIdentificacao.add(new Identificacao(Identificacaocliente.RG, rg));
			if(ie != null) listaIdentificacao.add(new Identificacao(Identificacaocliente.IE, ie));
			
			for (Identificacao identificacao : listaIdentificacao) {
				String sku_type = this.getCode_type(listaEmporiumidentificaocliente, identificacao.identificacaocliente);
				
				RegistroIdentificacaoCliente registroIdentificacaoCliente = new RegistroIdentificacaoCliente();
				IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "IdentificacaoCliente", registroIdentificacaoCliente);
				registroIdentificacaoCliente.setCode_type(code_codigointerno);
				registroIdentificacaoCliente.setCode(cdcliente);
				registroIdentificacaoCliente.setSku(identificacao.codigo);
				registroIdentificacaoCliente.setSku_type(sku_type);
				
				bean.listaRegistroIdentificacaoCliente.add(registroIdentificacaoCliente);
			}
			
			Endereco endereco = cliente.getEndereco();
			if(endereco != null){
				RegistroEndereco registroEndereco = new RegistroEndereco();
				IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "Endereco", registroEndereco);
				registroEndereco.setCode_type(code_codigointerno);
				registroEndereco.setCode(cdcliente);
				registroEndereco.setAddress_type("4");
				registroEndereco.setAddress(IntegracaoEmporiumUtil.util.maxPrintString(endereco.getLogradouro(), 60));
				registroEndereco.setComplement(IntegracaoEmporiumUtil.util.maxPrintString(endereco.getComplemento(), 20));
				registroEndereco.setNeig(IntegracaoEmporiumUtil.util.maxPrintString(endereco.getBairro(), 20));
				registroEndereco.setNumber(IntegracaoEmporiumUtil.util.maxPrintString(endereco.getNumero(), 20));
				registroEndereco.setReference(IntegracaoEmporiumUtil.util.maxPrintString(endereco.getPontoreferencia(), 20));
				registroEndereco.setZip(endereco.getCep() != null ? endereco.getCep().getValue() : "");
				registroEndereco.setCity(endereco.getMunicipio() != null ? IntegracaoEmporiumUtil.util.maxPrintString(endereco.getMunicipio().getNome(), 30) : "");
				registroEndereco.setState(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : "");
				
				bean.listaRegistroEndereco.add(registroEndereco);
			}
		}
		
		return bean;
	}
	
	private String getTelefone(Set<Telefone> listaTelefone, Integer tipotelefone) {
		if(listaTelefone != null && listaTelefone.size() > 0){
			for (Telefone telefone : listaTelefone) {
				if(telefone.getTelefonetipo() != null && 
						telefone.getTelefonetipo().equals(new Telefonetipo(tipotelefone)) &&
						telefone.getTelefone() != null){
					return StringUtils.soNumero(telefone.getTelefone());
				}
			}
		}
		return "";
	}

	private String getCode_type(List<Emporiumidentificacaocliente> listaEmporiumidentificaocliente, Identificacaocliente identificacaocliente) {
		if(listaEmporiumidentificaocliente != null && listaEmporiumidentificaocliente.size() > 0){
			for (Emporiumidentificacaocliente emporiumidentificaocliente : listaEmporiumidentificaocliente) {
				if(emporiumidentificaocliente.getIdentificacaocliente() != null && 
						emporiumidentificaocliente.getIdentificacaocliente().equals(identificacaocliente) &&
						emporiumidentificaocliente.getChaveemporium() != null){
					return emporiumidentificaocliente.getChaveemporium().toString();
				}
			}
		}
		return "";
	}
	
	private class Identificacao {
		private Identificacaocliente identificacaocliente;
		private String codigo;
		
		public Identificacao(Identificacaocliente identificacaocliente, String codigo) {
			this.identificacaocliente = identificacaocliente;
			this.codigo = codigo;
		}
	}

	private class ArquivoImportacaoEmporium {
		private List<RegistroCliente> listaRegistroCliente = new ArrayList<RegistroCliente>();
		private List<RegistroIdentificacaoCliente> listaRegistroIdentificacaoCliente = new ArrayList<RegistroIdentificacaoCliente>();
		private List<RegistroEndereco> listaRegistroEndereco = new ArrayList<RegistroEndereco>();
	}

}
