package br.com.linkcom.sined.util.emporium;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.EmporiumreducaozService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

import com.ibm.icu.util.Calendar;

public class IntegracaoEmporiumExportacaoMovimentosUtil {

	public static IntegracaoEmporiumExportacaoMovimentosUtil util = new IntegracaoEmporiumExportacaoMovimentosUtil();
	public static final String EXPORT_FOLDER = "/var/emporium/w3erp/exportacao";
	
	public void executarJobIntegracao(String banco_cliente) throws IOException {
		Date data = SinedDateUtils.currentDate();
		data = SinedDateUtils.addDiasData(data, -1);
		
		int dia = SinedDateUtils.getDateProperty(data, Calendar.DAY_OF_MONTH);
		
		Date dataMesAnterior = SinedDateUtils.addMesData(data, -1);
		
		Date dataInicioMesAnterior = SinedDateUtils.firstDateOfMonth(dataMesAnterior);
		Date dataFimMesAnterior = SinedDateUtils.lastDateOfMonth(dataMesAnterior);
		
		List<Empresa> listaEmpresa = EmpresaService.getInstance().findForIntegracaoEmporium();
		for (Empresa empresa : listaEmpresa) {
			this.criarArquivoExportacaoMovimentos(banco_cliente, data, empresa);
			this.criarArquivoExportacaoClientes(banco_cliente, empresa);
			
			if(dia >= 5 && dia <= 10 && !EmporiumreducaozService.getInstance().haveReducaozEmpresa(empresa, dataInicioMesAnterior, dataFimMesAnterior)){
				while(SinedDateUtils.beforeOrEqualIgnoreHour(dataInicioMesAnterior, dataFimMesAnterior)){
					this.criarArquivoExportacaoFiscaisGerais(banco_cliente, dataInicioMesAnterior, empresa);
					this.criarArquivoExportacaoFiscaisTributacao(banco_cliente, dataInicioMesAnterior, empresa);
					
					dataInicioMesAnterior = SinedDateUtils.addDiasData(dataInicioMesAnterior, 1);
				}
			}
		}
	}
	
	public Arquivoemporium criarArquivoExportacaoMovimentosCupom(String banco_cliente, Date datamovimento, Empresa empresa, String pdv, String cupom) throws IOException {
		StringBuilder sb = new StringBuilder();
		String nomeArquivo = "arquivo_exportacaomovimentoscupom_" + SinedUtil.datePatternForEmporium();
		
		sb.append("4|91|")
			.append(empresa.getIntegracaopdv())
			.append("|")
			.append(SinedDateUtils.toString(datamovimento, "yyyyMMdd")).append("|")
			.append(pdv).append("|")
			.append(cupom).append("|")
			.append(EXPORT_FOLDER + "/resultado_" + nomeArquivo + ".txt\n");
		
		return criarArquivoExportacao(banco_cliente, sb.toString(), nomeArquivo, Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO, datamovimento);
	}
	
	public Arquivoemporium criarArquivoExportacaoMovimentos(String banco_cliente, Date datamovimento, Empresa empresa) throws IOException {
		StringBuilder sb = new StringBuilder();
		String nomeArquivo = "arquivo_exportacaomovimentos_" + SinedUtil.datePatternForEmporium();
		
		sb.append("4|89|")
			.append(empresa.getIntegracaopdv())
			.append("|")
			.append(SinedDateUtils.toString(datamovimento, "yyyyMMdd"))
			.append("|" + EXPORT_FOLDER + "/resultado_" + nomeArquivo + ".txt\n");
		
		return criarArquivoExportacao(banco_cliente, sb.toString(), nomeArquivo, Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO, datamovimento);
	}
	
	public Arquivoemporium criarArquivoExportacaoFiscaisGerais(String banco_cliente, Date datamovimento, Empresa empresa) throws IOException {
		StringBuilder sb = new StringBuilder();
		String nomeArquivo = "arquivo_exportacaofiscaisgerais_" + SinedUtil.datePatternForEmporium();
		
		sb.append("4|84|")
			.append(empresa.getIntegracaopdv())
			.append("|")
		.append(SinedDateUtils.toString(datamovimento, "yyyyMMdd"))
		.append("|" + EXPORT_FOLDER + "/resultado_" + nomeArquivo + ".txt\n");
		
		return criarArquivoExportacao(banco_cliente, sb.toString(), nomeArquivo, Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISGERAIS, datamovimento);
	}
	
	public Arquivoemporium criarArquivoExportacaoFiscaisTributacao(String banco_cliente, Date datamovimento, Empresa empresa) throws IOException {
		StringBuilder sb = new StringBuilder();
		String nomeArquivo = "arquivo_exportacaofiscaistributacao_" + SinedUtil.datePatternForEmporium();
		
		sb.append("4|85|")
			.append(empresa.getIntegracaopdv())
			.append("|")
		.append(SinedDateUtils.toString(datamovimento, "yyyyMMdd"))
		.append("|" + EXPORT_FOLDER + "/resultado_" + nomeArquivo + ".txt\n");
		
		return criarArquivoExportacao(banco_cliente, sb.toString(), nomeArquivo, Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISTRIBUTACAO, datamovimento);
	}
	
	public Arquivoemporium criarArquivoExportacaoClientes(String banco_cliente, Empresa empresa) throws IOException {
		Date data = SinedDateUtils.currentDate();
		
		StringBuilder sb = new StringBuilder();
		String nomeArquivo = "arquivo_exportacaoclientes_" + SinedUtil.datePatternForEmporium();
		
		sb.append("4|92|")
			.append(empresa.getIntegracaopdv())
			.append("|")
			.append(SinedDateUtils.toString(data, "yyyyMMdd"))
			.append("|" + EXPORT_FOLDER + "/resultado_" + nomeArquivo + ".txt\n");
		
		return IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacao(banco_cliente, sb.toString(), nomeArquivo, Arquivoemporiumtipo.EXPORTACAO_CLIENTES, data);
	}
	
	private Arquivoemporium criarArquivoExportacao(String banco_cliente, String arquivoStr, String nomeArquivo, Arquivoemporiumtipo arquivoemporiumtipo, Date datamovimento) throws IOException {
		String pathArquivo = IntegracaoEmporiumUtil.util.saveDir(banco_cliente, false);
		
		IntegracaoEmporiumUtil.util.stringToFile(pathArquivo, nomeArquivo+ ".txt", arquivoStr);
		
		Resource resource = new Resource("text/plain", nomeArquivo+ ".txt", arquivoStr.getBytes());
		
		Arquivo arquivo = new Arquivo();
		
		arquivo.setNome(resource.getFileName());
		arquivo.setContent(resource.getContents());
		arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
		arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
		arquivo.setTipoconteudo(resource.getContentType());
		
		Arquivoemporium arquivoemporium = new Arquivoemporium();
		arquivoemporium.setArquivo(arquivo);
		arquivoemporium.setDtmovimento(datamovimento);
		arquivoemporium.setArquivoemporiumtipo(arquivoemporiumtipo);
		
		ArquivoDAO.getInstance().saveFile(arquivoemporium, "arquivo");
		ArquivoemporiumService.getInstance().saveOrUpdate(arquivoemporium);
		
		return arquivoemporium;
	}
	
}
