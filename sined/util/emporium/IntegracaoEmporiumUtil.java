package br.com.linkcom.sined.util.emporium;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtils;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
public class IntegracaoEmporiumUtil {

	public static IntegracaoEmporiumUtil util = new IntegracaoEmporiumUtil();
	
	public String printRegistro(List<?> listaRegistro) {
		StringBuilder sb = new StringBuilder();
		for (Object obj : listaRegistro) {
			sb.append(obj.toString()).append("\n");
		}
		return sb.toString();
	}

	public void setDefaultValues(String banco_cliente, String type, Object object) {
		Properties properties = this.getPropertiesDefaultValues(banco_cliente,type);
		this.populateDefaultValues(object, properties);
	}

	public void populateDefaultValues(Object object, Properties properties) {
		try {
			BeanUtils.populate(object, properties);
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	public Empresa getEmpresaEmporium(String loja, List<Empresa> listaEmpresa){
		if(listaEmpresa != null && listaEmpresa.size() > 0){
			for (Empresa empresa : listaEmpresa) {
				if(empresa != null && 
						empresa.getIntegracaopdv() != null &&
						empresa.getIntegracaopdv().equals(loja)){
					return empresa;
				}
			}
		}
		return null;
	}
	
	public Colaborador getColaboradorEmporium(String vendedor, List<Colaborador> listaColaborador) {
		if(listaColaborador != null && listaColaborador.size() > 0){
			for (Colaborador colaborador : listaColaborador) {
				if(colaborador != null && 
						colaborador.getCodigoemporium() != null &&
						colaborador.getCodigoemporium().equals(vendedor)){
					return colaborador;
				}
			}
		}
		return null;
	}
	
	public Tributacaoecf getTributacaoecfEmporium(String legenda, List<Tributacaoecf> listaTributacaoecf){
		if(listaTributacaoecf != null && listaTributacaoecf.size() > 0){
			for (Tributacaoecf tributacaoecf : listaTributacaoecf) {
				if(tributacaoecf != null &&
						tributacaoecf.getCodigoecf() != null &&
						tributacaoecf.getCodigoecf().equals(legenda)){
					return tributacaoecf;
				}
			}
		}
		return null;
	}
	
	public Emporiumpdv getPDVEmporium(String loja, String pdv, List<Emporiumpdv> listaPdv){
		if(listaPdv != null && listaPdv.size() > 0){
			for (Emporiumpdv emporiumpdv : listaPdv) {
				boolean pdvEquals = emporiumpdv != null &&
						emporiumpdv.getCodigoemporium() != null &&
						emporiumpdv.getCodigoemporium().equals(pdv);
				
				boolean lojaEquals = loja == null || (loja != null && emporiumpdv != null &&
						(emporiumpdv.getEmpresa() == null ||
						(emporiumpdv.getEmpresa() != null &&
						emporiumpdv.getEmpresa().getIntegracaopdv() != null &&
						emporiumpdv.getEmpresa().getIntegracaopdv().equals(loja))));
				
				if(lojaEquals && pdvEquals){
					return new Emporiumpdv(emporiumpdv);
				}
			}
		}
		return null;
	}

	public Properties getPropertiesDefaultValues(String banco_cliente, String type) {
		banco_cliente = banco_cliente.replaceAll("_w3erp_PostgreSQLDS", "");
		
		Properties properties = new Properties();
		InputStream inputstream = null;
		try{
			inputstream = IntegracaoEmporiumUtil.class.getResourceAsStream("padroes/" + banco_cliente + "_" + type + ".properties");
			properties.load(inputstream);
		} catch (Exception e) {
			try{
				inputstream = IntegracaoEmporiumUtil.class.getResourceAsStream("padroes/default_" + type + ".properties");
				properties.load(inputstream);
			}catch (Exception e2) {}
		} finally {
			if(inputstream != null){
				try {
					inputstream.close();
				} catch (IOException e) {}
			}
		}
		return properties;
	}

	public String maxPrintString(String str, int tamanho){
		if(str == null) return "";
		if(str.length() > tamanho) return str.substring(0, tamanho);
		else return str;
	}
	
	public String printDouble(Double dbl, int tamanhoNaoDecimal, int tamanhoDecimal){
		if(dbl == null) return "";
		
		String valorStr = dbl.toString();
		String[] valorArray = valorStr.split("\\.");
		return valorArray[0] + (valorArray.length > 1 ? this.printStringNumericRight(valorArray[1], tamanhoDecimal) : this.printStringNumericRight("0", tamanhoDecimal));
	}
	
	public String printStringNumeric(String s, int tamanho){
		return br.com.linkcom.neo.util.StringUtils.stringCheia(s, "0", tamanho, false);
	}
	
	public String printStringNumericRight(String s, int tamanho){
		return br.com.linkcom.neo.util.StringUtils.stringCheia(s, "0", tamanho, true);
	}

	public void stringToFile(String path, String nome, String conteudo) throws IOException {
		long tmp2 = System.currentTimeMillis();
		System.out.println("### Job Emporium Inicio (Gravacao) " + tmp2);
		
		File dir = new File(path);
		dir.mkdirs();
		
		File file = new File(path + java.io.File.separator + nome);
        if(!file.exists()){
            boolean createNewFile = file.createNewFile();
            if(!createNewFile) throw new SinedException("Arquivo " + (path + nome) + " n�o criado.");
        }
        
        PrintWriter out = new PrintWriter(new FileWriter(file));
        out.print(conteudo);
        out.close();
        
        System.out.println("### Job Emporium Fim (Gravacao) " + tmp2);
	}
	
	public String fileToString(File file) {
		long tmp2 = System.currentTimeMillis();
		System.out.println("### Job Emporium Inicio (Carregamento) " + tmp2);
		
		BufferedReader reader = null;
		String retorno = "";
		try{
	        if(file.exists()){
	            StringBuilder fileData = new StringBuilder();
	            reader = new BufferedReader(new FileReader(file));
	            char[] buf = new char[1024];
	            int numRead = 0;
	            while ((numRead = reader.read(buf)) != -1) {
	                String readData = String.valueOf(buf, 0, numRead);
	                fileData.append(readData);
	                buf = new char[1024];
	            }
	            retorno = fileData.toString();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {}
			}
		}
		
		System.out.println("### Job Emporium Fim (Carregamento) " + tmp2);
        return retorno;
	}
	
	public String printStringFile(String str){
		if(str == null || str.trim().equals("")){
			return " ";
		} else return str;
	}
	
	public String saveDir(String banco_cliente, boolean resultado){
		banco_cliente = banco_cliente.replaceAll("_w3erp_PostgreSQLDS", "");
		
		String saveDir = ArquivoDAO.getInstance().getPathDir() +
							java.io.File.separator +
							"emporium" +
							java.io.File.separator +
							banco_cliente +
							java.io.File.separator +
							(resultado ? "retorno" : "envio");
		
		return saveDir;
	}
	
	public void enviarEmailErro(Exception e, String descricao) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			String nomeMaquina = "N�o encontrado.";
			try {  
			    InetAddress localaddr = InetAddress.getLocalHost();  
			    nomeMaquina = localaddr.getHostName();  
			} catch (UnknownHostException e3) {}  
			
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			
			String mensagem = descricao + "<BR>M�quina: " + nomeMaquina + "<BR>Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
			
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro na integra��o com o ECF.")
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception ex) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.", ex);
			}
		}
	}
	
	public boolean haveIntegracaoEcf(){
		return ParametrogeralService.getInstance().getBoolean(Parametrogeral.INTEGRACAO_ECF);
	}
	
	public boolean haveIpEmporium(){
		String ip = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.EMPORIUM_IP_SERVIDOR);
		return ip != null && !ip.equals("");
	}

	public void fechaPopUpExecutaEmporium(WebRequestContext request) {
		try{
			String ip = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMPORIUM_IP_SERVIDOR);
			
			String ajax = "";
			if(ip != null && !ip.equals("")){
				ajax = "window.open('http://" + ip + "/w3erp.php', '" + SinedUtil.datePatternForEmporium() + "', 'channelmode=no,directories=no,fullscreen=no,height=1,left=0,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no,top=0,width=1');";
			}
			
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>function init() {" + ajax +
										"parent.location = parent.location;" +
										"parent.$.akModalRemove(true);}" +
										"init();" +
									"</script>");
		} catch (Exception e) {
			e.printStackTrace();
			SinedUtil.fechaPopUp(request);
		}
	}
	
}
