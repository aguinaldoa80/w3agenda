package br.com.linkcom.sined.util.emporium.bean;

public class Emporiumcliente {
	
	private Integer codigo;
	private String cpfcnpj;
	private String nome;
	private String email;
	private String ie;
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	public String getNome() {
		return nome;
	}
	public String getEmail() {
		return email;
	}
	public String getIe() {
		return ie;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}
	
}
