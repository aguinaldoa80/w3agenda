package br.com.linkcom.sined.util.emporium.registros;

public class RegistroDepartamento {

	private String status = "0";
	private String record = "02";
	private String id = "";
	private String parent_departament = "";
	private String allow_plu = "";
	private String name = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getId() {
		return id;
	}
	public String getParent_departament() {
		return parent_departament;
	}
	public String getAllow_plu() {
		return allow_plu;
	}
	public String getName() {
		return name;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setParent_departament(String parentDepartament) {
		parent_departament = parentDepartament;
	}
	public void setAllow_plu(String allowPlu) {
		allow_plu = allowPlu;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}


	@Override
	public String toString() {
		return status + "|" +
				record + "|" +
				id + "|" +
				parent_departament + "|" +
				allow_plu + "|" +
				name + "|";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroDepartamento other = (RegistroDepartamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
