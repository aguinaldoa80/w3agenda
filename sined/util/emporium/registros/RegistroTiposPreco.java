package br.com.linkcom.sined.util.emporium.registros;

public class RegistroTiposPreco {

	private String status = "0";
	private String record = "86";
	
	private String price_key = "";
	private String name = "";
	private String adm_price = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getPrice_key() {
		return price_key;
	}
	public String getName() {
		return name;
	}
	public String getAdm_price() {
		return adm_price;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public void setPrice_key(String priceKey) {
		price_key = priceKey;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAdm_price(String admPrice) {
		adm_price = admPrice;
	}

	@Override
	public String toString() {
		return status + "|" +
				record + "|" +
				price_key + "|" +
				name + "|" +
				adm_price + "|";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((price_key == null) ? 0 : price_key.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroTiposPreco other = (RegistroTiposPreco) obj;
		if (price_key == null) {
			if (other.price_key != null)
				return false;
		} else if (!price_key.equals(other.price_key))
			return false;
		return true;
	}
	
}
