package br.com.linkcom.sined.util.emporium.registros;

public class RegistroPLU_KIT {

	private String status = "0";
	private String record = "14";
	private String id_main = "";
	private String id = "";
	private String quantity = "";
	private String type_price = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getId_main() {
		return id_main;
	}
	public String getId() {
		return id;
	}
	public String getQuantity() {
		return quantity;
	}
	public String getType_price() {
		return type_price;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public void setId_main(String idMain) {
		id_main = idMain;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public void setType_price(String typePrice) {
		type_price = typePrice;
	}
	
	@Override
	public String toString() {
		return status + "|" +
				record + "|" +
				id_main + "|" + 
				id + "|" + 
				quantity + "|" + 
				type_price + "|";
	}
	
}
