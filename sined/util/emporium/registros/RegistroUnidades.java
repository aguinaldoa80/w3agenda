package br.com.linkcom.sined.util.emporium.registros;

public class RegistroUnidades {

	private String status = "0";
	private String record = "81";
	
	private String short_name = "";
	private String long_name = "";
	private String max_decimais = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getShort_name() {
		return short_name;
	}
	public String getLong_name() {
		return long_name;
	}
	public String getMax_decimais() {
		return max_decimais;
	}
	public void setShort_name(String shortName) {
		short_name = shortName;
	}
	public void setLong_name(String longName) {
		long_name = longName;
	}
	public void setMax_decimais(String maxDecimais) {
		max_decimais = maxDecimais;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}


	@Override
	public String toString() {
		return status + "|" +
				record + "|" +
				short_name + "|" +
				long_name + "|" +
				max_decimais + "|";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((short_name == null) ? 0 : short_name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroUnidades other = (RegistroUnidades) obj;
		if (short_name == null) {
			if (other.short_name != null)
				return false;
		} else if (!short_name.equals(other.short_name))
			return false;
		return true;
	}
	
}
