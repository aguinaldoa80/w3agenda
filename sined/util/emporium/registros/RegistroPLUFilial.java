package br.com.linkcom.sined.util.emporium.registros;

public class RegistroPLUFilial {

	private String status = "0";
	private String record = "11";
	private String store_key = "";
	private String id = "";
	private String desired_margin_perc = "";
	private String quantity_in_stock = "";
	private String out_of_stock_day = "";
	private String last_entered_day = "";
	private String last_sale_day = "";
	private String cost = "";
	private String start_cost = "";
	private String pos_id = "";
	private String nf_id = "";
	private String minimal_stock = "";
	private String not_for_sale = "";
	private String order_type = "";
	private String split_interest_perc = "";
	private String delayed_pay_percent = "";
	private String min_splits = "";
	private String max_splits = "";
	private String operation_type = "";
	private String tax_percent = "";
	private String tax_extra_percent = "";
	private String invoice_tax_percent = "";
	private String invoice_tax_extra_percent = "";
	private String qty_from_amount = "";
	private String wholesale_quantity = "";
	private String flag_stock = "";
	private String adder = "";
	private String total_tax = "";
	private String total_tax_01 = "";
	private String total_tax_02 = "";
	

	public String getStatus() {
		return status;
	}

	public String getRecord() {
		return record;
	}

	public String getStore_key() {
		return store_key;
	}

	public String getId() {
		return id;
	}

	public String getDesired_margin_perc() {
		return desired_margin_perc;
	}

	public String getQuantity_in_stock() {
		return quantity_in_stock;
	}

	public String getOut_of_stock_day() {
		return out_of_stock_day;
	}

	public String getLast_entered_day() {
		return last_entered_day;
	}

	public String getLast_sale_day() {
		return last_sale_day;
	}

	public String getCost() {
		return cost;
	}

	public String getStart_cost() {
		return start_cost;
	}

	public String getPos_id() {
		return pos_id;
	}

	public String getNf_id() {
		return nf_id;
	}

	public String getMinimal_stock() {
		return minimal_stock;
	}

	public String getNot_for_sale() {
		return not_for_sale;
	}

	public String getOrder_type() {
		return order_type;
	}

	public String getSplit_interest_perc() {
		return split_interest_perc;
	}

	public String getDelayed_pay_percent() {
		return delayed_pay_percent;
	}

	public String getMin_splits() {
		return min_splits;
	}

	public String getMax_splits() {
		return max_splits;
	}

	public String getOperation_type() {
		return operation_type;
	}

	public String getTax_percent() {
		return tax_percent;
	}

	public String getTax_extra_percent() {
		return tax_extra_percent;
	}

	public String getInvoice_tax_percent() {
		return invoice_tax_percent;
	}

	public String getInvoice_tax_extra_percent() {
		return invoice_tax_extra_percent;
	}

	public String getQty_from_amount() {
		return qty_from_amount;
	}

	public String getWholesale_quantity() {
		return wholesale_quantity;
	}

	public String getFlag_stock() {
		return flag_stock;
	}

	public String getAdder() {
		return adder;
	}

	public String getTotal_tax() {
		return total_tax;
	}

	public String getTotal_tax_01() {
		return total_tax_01;
	}

	public String getTotal_tax_02() {
		return total_tax_02;
	}

	public void setTotal_tax_01(String total_tax_01) {
		this.total_tax_01 = total_tax_01;
	}

	public void setTotal_tax_02(String total_tax_02) {
		this.total_tax_02 = total_tax_02;
	}

	public void setFlag_stock(String flagStock) {
		flag_stock = flagStock;
	}

	public void setAdder(String adder) {
		this.adder = adder;
	}

	public void setTotal_tax(String totalTax) {
		total_tax = totalTax;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public void setStore_key(String storeKey) {
		store_key = storeKey;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDesired_margin_perc(String desiredMarginPerc) {
		desired_margin_perc = desiredMarginPerc;
	}

	public void setQuantity_in_stock(String quantityInStock) {
		quantity_in_stock = quantityInStock;
	}

	public void setOut_of_stock_day(String outOfStockDay) {
		out_of_stock_day = outOfStockDay;
	}

	public void setLast_entered_day(String lastEnteredDay) {
		last_entered_day = lastEnteredDay;
	}

	public void setLast_sale_day(String lastSaleDay) {
		last_sale_day = lastSaleDay;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public void setStart_cost(String startCost) {
		start_cost = startCost;
	}

	public void setPos_id(String posId) {
		pos_id = posId;
	}

	public void setNf_id(String nfId) {
		nf_id = nfId;
	}

	public void setMinimal_stock(String minimalStock) {
		minimal_stock = minimalStock;
	}

	public void setNot_for_sale(String notForSale) {
		not_for_sale = notForSale;
	}

	public void setOrder_type(String orderType) {
		order_type = orderType;
	}

	public void setSplit_interest_perc(String splitInterestPerc) {
		split_interest_perc = splitInterestPerc;
	}

	public void setDelayed_pay_percent(String delayedPayPercent) {
		delayed_pay_percent = delayedPayPercent;
	}

	public void setMin_splits(String minSplits) {
		min_splits = minSplits;
	}

	public void setMax_splits(String maxSplits) {
		max_splits = maxSplits;
	}

	public void setOperation_type(String operationType) {
		operation_type = operationType;
	}

	public void setTax_percent(String taxPercent) {
		tax_percent = taxPercent;
	}

	public void setTax_extra_percent(String taxExtraPercent) {
		tax_extra_percent = taxExtraPercent;
	}

	public void setInvoice_tax_percent(String invoiceTaxPercent) {
		invoice_tax_percent = invoiceTaxPercent;
	}

	public void setInvoice_tax_extra_percent(String invoiceTaxExtraPercent) {
		invoice_tax_extra_percent = invoiceTaxExtraPercent;
	}

	public void setQty_from_amount(String qtyFromAmount) {
		qty_from_amount = qtyFromAmount;
	}

	public void setWholesale_quantity(String wholesaleQuantity) {
		wholesale_quantity = wholesaleQuantity;
	}

	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.store_key + "|" +
				this.id + "|" +
				this.desired_margin_perc + "|" +
				this.quantity_in_stock + "|" +
				this.out_of_stock_day + "|" +
				this.last_entered_day + "|" +
				this.last_sale_day + "|" +
				this.cost + "|" +
				this.start_cost + "|" +
				this.pos_id + "|" +
				this.nf_id + "|" +
				this.minimal_stock + "|" +
				this.not_for_sale + "|" +
				this.order_type + "|" +
				this.split_interest_perc + "|" +
				this.delayed_pay_percent + "|" +
				this.min_splits + "|" +
				this.max_splits + "|" +
				this.operation_type + "|" +
				this.tax_percent + "|" +
				this.tax_extra_percent + "|" +
				this.invoice_tax_percent + "|" +
				this.invoice_tax_extra_percent + "|" +
				this.qty_from_amount + "|" +
				this.wholesale_quantity + "|" +
				this.flag_stock + "|" +
				this.adder + "|" +
				this.total_tax + "|" +
				this.total_tax_01 + "|" +
				this.total_tax_02 + "|"
				;
	}
	
}
