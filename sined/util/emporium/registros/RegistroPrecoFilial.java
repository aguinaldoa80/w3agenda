package br.com.linkcom.sined.util.emporium.registros;

public class RegistroPrecoFilial {

	private String status = "0";
	private String record = "12";
	private String store_key = "";
	private String id = "";
	private String price = "";
	private String start_price = "";
	private String time_price = "";
	private String type_price = "";
	private String promotion = "";
	private String code_promotion = "";
	private String points = "";
	private String quantity = "";
	private String rate = "";
	private String sequence = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getStore_key() {
		return store_key;
	}
	public String getId() {
		return id;
	}
	public String getPrice() {
		return price;
	}
	public String getStart_price() {
		return start_price;
	}
	public String getTime_price() {
		return time_price;
	}
	public String getType_price() {
		return type_price;
	}
	public String getPromotion() {
		return promotion;
	}
	public String getCode_promotion() {
		return code_promotion;
	}
	public String getPoints() {
		return points;
	}
	public String getQuantity() {
		return quantity;
	}
	public String getRate() {
		return rate;
	}
	public String getSequence() {
		return sequence;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public void setStore_key(String storeKey) {
		store_key = storeKey;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public void setStart_price(String startPrice) {
		start_price = startPrice;
	}
	public void setTime_price(String timePrice) {
		time_price = timePrice;
	}
	public void setType_price(String typePrice) {
		type_price = typePrice;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	public void setCode_promotion(String codePromotion) {
		code_promotion = codePromotion;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.store_key + "|" +
				this.id + "|" +
				this.price + "|" +
				this.start_price + "|" +
				this.time_price + "|" +
				this.type_price + "|" +
				this.promotion + "|" +
				this.code_promotion + "|" +
				this.points + "|" /*+
				this.quantity + "|" +
				this.rate + "|" +
				this.sequence + "|"*/;
	}
	
}
