package br.com.linkcom.sined.util.emporium.registros;

public class RegistroCliente {

	private String status = "0";
	private String record = "20";
	private String code_type = "";
	private String code = "";
	private String customer_type = "";
	private String customer_name = "";
	private String customer_name_alt = "";
	private String customer_email = "";
	private String customer_phone1 = "";
	private String customer_phone2 = "";
	private String customer_date_inc = "";
	private String customer_job_id = "";
	private String customer_job_name = "";
	private String customer_job_phone = "";
	private String customer_title = "";
	private String customer_revenue = "";
	private String customer_job_date = "";
	private String customer_job_type = "";
	private String customer_job_name2 = "";
	private String customer_job_phone2 = "";
	private String customer_job_in = "";
	private String customer_job_out = "";
	private String customer_birthday = "";
	private String store_key = "";
	private String password = "";
	private String crypt_password = "";

	public String getStatus() {
		return status;
	}

	public String getRecord() {
		return record;
	}

	public String getCode_type() {
		return code_type;
	}

	public String getCode() {
		return code;
	}

	public String getCustomer_type() {
		return customer_type;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public String getCustomer_name_alt() {
		return customer_name_alt;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public String getCustomer_phone1() {
		return customer_phone1;
	}

	public String getCustomer_phone2() {
		return customer_phone2;
	}

	public String getCustomer_date_inc() {
		return customer_date_inc;
	}

	public String getCustomer_job_id() {
		return customer_job_id;
	}

	public String getCustomer_job_name() {
		return customer_job_name;
	}

	public String getCustomer_job_phone() {
		return customer_job_phone;
	}

	public String getCustomer_title() {
		return customer_title;
	}

	public String getCustomer_revenue() {
		return customer_revenue;
	}

	public String getCustomer_job_date() {
		return customer_job_date;
	}

	public String getCustomer_job_type() {
		return customer_job_type;
	}

	public String getCustomer_job_name2() {
		return customer_job_name2;
	}

	public String getCustomer_job_phone2() {
		return customer_job_phone2;
	}

	public String getCustomer_job_in() {
		return customer_job_in;
	}

	public String getCustomer_job_out() {
		return customer_job_out;
	}

	public String getCustomer_birthday() {
		return customer_birthday;
	}

	public String getStore_key() {
		return store_key;
	}

	public String getPassword() {
		return password;
	}

	public String getCrypt_password() {
		return crypt_password;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public void setCode_type(String codeType) {
		code_type = codeType;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomer_type(String customerType) {
		customer_type = customerType;
	}

	public void setCustomer_name(String customerName) {
		customer_name = customerName;
	}

	public void setCustomer_name_alt(String customerNameAlt) {
		customer_name_alt = customerNameAlt;
	}

	public void setCustomer_email(String customerEmail) {
		customer_email = customerEmail;
	}

	public void setCustomer_phone1(String customerPhone1) {
		customer_phone1 = customerPhone1;
	}

	public void setCustomer_phone2(String customerPhone2) {
		customer_phone2 = customerPhone2;
	}

	public void setCustomer_date_inc(String customerDateInc) {
		customer_date_inc = customerDateInc;
	}

	public void setCustomer_job_id(String customerJobId) {
		customer_job_id = customerJobId;
	}

	public void setCustomer_job_name(String customerJobName) {
		customer_job_name = customerJobName;
	}

	public void setCustomer_job_phone(String customerJobPhone) {
		customer_job_phone = customerJobPhone;
	}

	public void setCustomer_title(String customerTitle) {
		customer_title = customerTitle;
	}

	public void setCustomer_revenue(String customerRevenue) {
		customer_revenue = customerRevenue;
	}

	public void setCustomer_job_date(String customerJobDate) {
		customer_job_date = customerJobDate;
	}

	public void setCustomer_job_type(String customerJobType) {
		customer_job_type = customerJobType;
	}

	public void setCustomer_job_name2(String customerJobName2) {
		customer_job_name2 = customerJobName2;
	}

	public void setCustomer_job_phone2(String customerJobPhone2) {
		customer_job_phone2 = customerJobPhone2;
	}

	public void setCustomer_job_in(String customerJobIn) {
		customer_job_in = customerJobIn;
	}

	public void setCustomer_job_out(String customerJobOut) {
		customer_job_out = customerJobOut;
	}

	public void setCustomer_birthday(String customerBirthday) {
		customer_birthday = customerBirthday;
	}

	public void setStore_key(String storeKey) {
		store_key = storeKey;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setCrypt_password(String cryptPassword) {
		crypt_password = cryptPassword;
	}

	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.code_type + "|" +
				this.code + "|" +
				this.customer_type + "|" +
				this.customer_name + "|" +
				this.customer_name_alt + "|" +
				this.customer_email + "|" +
				this.customer_phone1 + "|" +
				this.customer_phone2 + "|" +
				this.customer_date_inc + "|" +
				this.customer_job_id + "|" +
				this.customer_job_name + "|" +
				this.customer_job_phone + "|" +
				this.customer_title + "|" +
				this.customer_revenue + "|" +
				this.customer_job_date + "|" +
				this.customer_job_type + "|" +
				this.customer_job_name2 + "|" +
				this.customer_job_phone2 + "|" +
				this.customer_job_in + "|" +
				this.customer_job_out + "|" +
				this.customer_birthday + "|" +
				this.store_key + "|" +
				this.password + "|" +
				this.crypt_password + "|";
	}
	
}
