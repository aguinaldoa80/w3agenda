package br.com.linkcom.sined.util.emporium.registros;

public class RegistroSKU {

	private String status = "0";
	private String record = "01";
	private String sku_id = "";
	private String plu_id = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getSku_id() {
		return sku_id;
	}
	public String getPlu_id() {
		return plu_id;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public void setSku_id(String skuId) {
		sku_id = skuId;
	}
	public void setPlu_id(String pluId) {
		plu_id = pluId;
	}
	
	@Override
	public String toString() {
		return status + "|" +
				record + "|" +
				sku_id + "|" +
				plu_id + "|";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((plu_id == null) ? 0 : plu_id.hashCode());
		result = prime * result + ((sku_id == null) ? 0 : sku_id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroSKU other = (RegistroSKU) obj;
		if (plu_id == null) {
			if (other.plu_id != null)
				return false;
		} else if (!plu_id.equals(other.plu_id))
			return false;
		if (sku_id == null) {
			if (other.sku_id != null)
				return false;
		} else if (!sku_id.equals(other.sku_id))
			return false;
		return true;
	}
	
}
