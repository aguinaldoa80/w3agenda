package br.com.linkcom.sined.util.emporium.registros;

public class RegistroPLU {

	private String status = "0";
	private String record = "10";
	private String id = "";
	private String base_plu_key = "";
	private String link_plu_key = "";
	private String plu_types = "";
	private String maker_key = "";
	private String short_description = "";
	private String long_description = "";
	private String commercial_description = "";
	private String image_file = "";
	private String pos_id = "";
	private String cost_decimals = "";
	private String unit_key = "";
	private String price_decimals = "";
	private String adder = "";
	private String departament_key = "";
	private String suggested_prompt = "";
	private String data_entry_key = "";
	private String tare_key = "";
	private String is_completion = "";
	private String has_deposit = "";
	private String has_plu_link = "";
	private String price_required = "";
	private String quantity_disallowed = "";
	private String quantity_required = "";
	private String decimal_disallowed = "";
	private String decimal_required = "";
	private String id_required = "";
	private String repeat_disallowed = "";
	private String scale = "";
	private String tracking_total = "";
	private String deposit = "";
	private String non_merchandise = "";
	private String return_disallowed = "";
	private String refund_disallowed = "";
	private String markdown_disallowed = "";
	private String rebate = "";
	private String not_for_sale = "";
	private String negative = "";
	private String clerk_required = "";
	private String kit = "";
	private String for_scale = "";
	private String delivery = "";
	private String authrizer_required = "";
	private String qty_from_amount = "";
	private String print_etq = "";
	private String completion = "";
	private String is_plu_base = "";
	private String store_adm_price = "";
	private String reverse_kit = "";
	private String verify_stock = "";
	private String valid_days = "";
	private String ncmsh = "";
	private String pis_cofins = "";
	private String quantity = "";
	private String quantity_min = "";
	private String quantity_max = "";
	private String classe = "";
	private String merchandise_origin = "";
	private String merchandise_group = "";
	private String pacote = "";
	private String wholesale_quantity = "";
	private String delivery_type = "";
	private String message_subtotal = "";
	private String standard_unit = "";
	private String quantity_standard = "";
	private String is_produced = "";
	private String is_input = "";
	private String is_scanned = "";
	private String gloss_weight = "";
	private String net_weight = "";
	private String total_tax = "";
	private String sefaz_id = "";
	private String anp_code = "";
	private String pis_pos_id = "";
	private String cofins_pos_id = "";
	private String self_checkout_weight = "";
	private String not_check_weight = "";
	private String total_tax_01 = "";
	private String total_tax_02 = "";
	
	public String getStatus() {
		return status;
	}
	public String getRecord() {
		return record;
	}
	public String getId() {
		return id;
	}
	public String getBase_plu_key() {
		return base_plu_key;
	}
	public String getLink_plu_key() {
		return link_plu_key;
	}
	public String getPlu_types() {
		return plu_types;
	}
	public String getMaker_key() {
		return maker_key;
	}
	public String getShort_description() {
		return short_description;
	}
	public String getLong_description() {
		return long_description;
	}
	public String getCommercial_description() {
		return commercial_description;
	}
	public String getImage_file() {
		return image_file;
	}
	public String getPos_id() {
		return pos_id;
	}
	public String getCost_decimals() {
		return cost_decimals;
	}
	public String getUnit_key() {
		return unit_key;
	}
	public String getPrice_decimals() {
		return price_decimals;
	}
	public String getAdder() {
		return adder;
	}
	public String getDepartament_key() {
		return departament_key;
	}
	public String getSuggested_prompt() {
		return suggested_prompt;
	}
	public String getData_entry_key() {
		return data_entry_key;
	}
	public String getTare_key() {
		return tare_key;
	}
	public String getIs_completion() {
		return is_completion;
	}
	public String getHas_deposit() {
		return has_deposit;
	}
	public String getHas_plu_link() {
		return has_plu_link;
	}
	public String getPrice_required() {
		return price_required;
	}
	public String getQuantity_disallowed() {
		return quantity_disallowed;
	}
	public String getQuantity_required() {
		return quantity_required;
	}
	public String getDecimal_disallowed() {
		return decimal_disallowed;
	}
	public String getDecimal_required() {
		return decimal_required;
	}
	public String getId_required() {
		return id_required;
	}
	public String getRepeat_disallowed() {
		return repeat_disallowed;
	}
	public String getScale() {
		return scale;
	}
	public String getTracking_total() {
		return tracking_total;
	}
	public String getDeposit() {
		return deposit;
	}
	public String getNon_merchandise() {
		return non_merchandise;
	}
	public String getReturn_disallowed() {
		return return_disallowed;
	}
	public String getRefund_disallowed() {
		return refund_disallowed;
	}
	public String getMarkdown_disallowed() {
		return markdown_disallowed;
	}
	public String getRebate() {
		return rebate;
	}
	public String getNot_for_sale() {
		return not_for_sale;
	}
	public String getNegative() {
		return negative;
	}
	public String getClerk_required() {
		return clerk_required;
	}
	public String getKit() {
		return kit;
	}
	public String getFor_scale() {
		return for_scale;
	}
	public String getDelivery() {
		return delivery;
	}
	public String getAuthrizer_required() {
		return authrizer_required;
	}
	public String getQty_from_amount() {
		return qty_from_amount;
	}
	public String getPrint_etq() {
		return print_etq;
	}
	public String getCompletion() {
		return completion;
	}
	public String getIs_plu_base() {
		return is_plu_base;
	}
	public String getStore_adm_price() {
		return store_adm_price;
	}
	public String getReverse_kit() {
		return reverse_kit;
	}
	public String getVerify_stock() {
		return verify_stock;
	}
	public String getValid_days() {
		return valid_days;
	}
	public String getNcmsh() {
		return ncmsh;
	}
	public String getPis_cofins() {
		return pis_cofins;
	}
	public String getQuantity() {
		return quantity;
	}
	public String getQuantity_min() {
		return quantity_min;
	}
	public String getQuantity_max() {
		return quantity_max;
	}
	public String getClasse() {
		return classe;
	}
	public String getMerchandise_origin() {
		return merchandise_origin;
	}
	public String getMerchandise_group() {
		return merchandise_group;
	}
	public String getPacote() {
		return pacote;
	}
	public String getWholesale_quantity() {
		return wholesale_quantity;
	}
	public String getDelivery_type() {
		return delivery_type;
	}
	public String getMessage_subtotal() {
		return message_subtotal;
	}
	public String getStandard_unit() {
		return standard_unit;
	}
	public String getQuantity_standard() {
		return quantity_standard;
	}
	public String getIs_produced() {
		return is_produced;
	}
	public String getIs_input() {
		return is_input;
	}
	public String getIs_scanned() {
		return is_scanned;
	}
	public String getGloss_weight() {
		return gloss_weight;
	}
	public String getNet_weight() {
		return net_weight;
	}
	public String getTotal_tax() {
		return total_tax;
	}
	public String getSefaz_id() {
		return sefaz_id;
	}
	public String getAnp_code() {
		return anp_code;
	}
	public String getPis_pos_id() {
		return pis_pos_id;
	}
	public String getCofins_pos_id() {
		return cofins_pos_id;
	}
	public String getSelf_checkout_weight() {
		return self_checkout_weight;
	}
	public String getNot_check_weight() {
		return not_check_weight;
	}
	public String getTotal_tax_01() {
		return total_tax_01;
	}
	public String getTotal_tax_02() {
		return total_tax_02;
	}
	public void setSefaz_id(String sefaz_id) {
		this.sefaz_id = sefaz_id;
	}
	public void setAnp_code(String anp_code) {
		this.anp_code = anp_code;
	}
	public void setPis_pos_id(String pis_pos_id) {
		this.pis_pos_id = pis_pos_id;
	}
	public void setCofins_pos_id(String cofins_pos_id) {
		this.cofins_pos_id = cofins_pos_id;
	}
	public void setSelf_checkout_weight(String self_checkout_weight) {
		this.self_checkout_weight = self_checkout_weight;
	}
	public void setNot_check_weight(String not_check_weight) {
		this.not_check_weight = not_check_weight;
	}
	public void setTotal_tax_01(String total_tax_01) {
		this.total_tax_01 = total_tax_01;
	}
	public void setTotal_tax_02(String total_tax_02) {
		this.total_tax_02 = total_tax_02;
	}
	public void setIs_scanned(String isScanned) {
		is_scanned = isScanned;
	}
	public void setGloss_weight(String glossWeight) {
		gloss_weight = glossWeight;
	}
	public void setNet_weight(String netWeight) {
		net_weight = netWeight;
	}
	public void setTotal_tax(String totalTax) {
		total_tax = totalTax;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setBase_plu_key(String basePluKey) {
		base_plu_key = basePluKey;
	}
	public void setLink_plu_key(String linkPluKey) {
		link_plu_key = linkPluKey;
	}
	public void setPlu_types(String pluTypes) {
		plu_types = pluTypes;
	}
	public void setMaker_key(String makerKey) {
		maker_key = makerKey;
	}
	public void setShort_description(String shortDescription) {
		short_description = shortDescription;
	}
	public void setLong_description(String longDescription) {
		long_description = longDescription;
	}
	public void setCommercial_description(String commercialDescription) {
		commercial_description = commercialDescription;
	}
	public void setImage_file(String imageFile) {
		image_file = imageFile;
	}
	public void setPos_id(String posId) {
		pos_id = posId;
	}
	public void setCost_decimals(String costDecimals) {
		cost_decimals = costDecimals;
	}
	public void setUnit_key(String unitKey) {
		unit_key = unitKey;
	}
	public void setPrice_decimals(String priceDecimals) {
		price_decimals = priceDecimals;
	}
	public void setAdder(String adder) {
		this.adder = adder;
	}
	public void setDepartament_key(String departamentKey) {
		departament_key = departamentKey;
	}
	public void setSuggested_prompt(String suggestedPrompt) {
		suggested_prompt = suggestedPrompt;
	}
	public void setData_entry_key(String dataEntryKey) {
		data_entry_key = dataEntryKey;
	}
	public void setTare_key(String tareKey) {
		tare_key = tareKey;
	}
	public void setIs_completion(String isCompletion) {
		is_completion = isCompletion;
	}
	public void setHas_deposit(String hasDeposit) {
		has_deposit = hasDeposit;
	}
	public void setHas_plu_link(String hasPluLink) {
		has_plu_link = hasPluLink;
	}
	public void setPrice_required(String priceRequired) {
		price_required = priceRequired;
	}
	public void setQuantity_disallowed(String quantityDisallowed) {
		quantity_disallowed = quantityDisallowed;
	}
	public void setQuantity_required(String quantityRequired) {
		quantity_required = quantityRequired;
	}
	public void setDecimal_disallowed(String decimalDisallowed) {
		decimal_disallowed = decimalDisallowed;
	}
	public void setDecimal_required(String decimalRequired) {
		decimal_required = decimalRequired;
	}
	public void setId_required(String idRequired) {
		id_required = idRequired;
	}
	public void setRepeat_disallowed(String repeatDisallowed) {
		repeat_disallowed = repeatDisallowed;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public void setTracking_total(String trackingTotal) {
		tracking_total = trackingTotal;
	}
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
	public void setNon_merchandise(String nonMerchandise) {
		non_merchandise = nonMerchandise;
	}
	public void setReturn_disallowed(String returnDisallowed) {
		return_disallowed = returnDisallowed;
	}
	public void setRefund_disallowed(String refundDisallowed) {
		refund_disallowed = refundDisallowed;
	}
	public void setMarkdown_disallowed(String markdownDisallowed) {
		markdown_disallowed = markdownDisallowed;
	}
	public void setRebate(String rebate) {
		this.rebate = rebate;
	}
	public void setNot_for_sale(String notForSale) {
		not_for_sale = notForSale;
	}
	public void setNegative(String negative) {
		this.negative = negative;
	}
	public void setClerk_required(String clerkRequired) {
		clerk_required = clerkRequired;
	}
	public void setKit(String kit) {
		this.kit = kit;
	}
	public void setFor_scale(String forScale) {
		for_scale = forScale;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	public void setAuthrizer_required(String authrizerRequired) {
		authrizer_required = authrizerRequired;
	}
	public void setQty_from_amount(String qtyFromAmount) {
		qty_from_amount = qtyFromAmount;
	}
	public void setPrint_etq(String printEtq) {
		print_etq = printEtq;
	}
	public void setCompletion(String completion) {
		this.completion = completion;
	}
	public void setIs_plu_base(String isPluBase) {
		is_plu_base = isPluBase;
	}
	public void setStore_adm_price(String storeAdmPrice) {
		store_adm_price = storeAdmPrice;
	}
	public void setReverse_kit(String reverseKit) {
		reverse_kit = reverseKit;
	}
	public void setVerify_stock(String verifyStock) {
		verify_stock = verifyStock;
	}
	public void setValid_days(String validDays) {
		valid_days = validDays;
	}
	public void setNcmsh(String ncmsh) {
		this.ncmsh = ncmsh;
	}
	public void setPis_cofins(String pisCofins) {
		pis_cofins = pisCofins;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public void setQuantity_min(String quantityMin) {
		quantity_min = quantityMin;
	}
	public void setQuantity_max(String quantityMax) {
		quantity_max = quantityMax;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public void setMerchandise_origin(String merchandiseOrigin) {
		merchandise_origin = merchandiseOrigin;
	}
	public void setMerchandise_group(String merchandiseGroup) {
		merchandise_group = merchandiseGroup;
	}
	public void setPacote(String pacote) {
		this.pacote = pacote;
	}
	public void setWholesale_quantity(String wholesaleQuantity) {
		wholesale_quantity = wholesaleQuantity;
	}
	public void setDelivery_type(String deliveryType) {
		delivery_type = deliveryType;
	}
	public void setMessage_subtotal(String messageSubtotal) {
		message_subtotal = messageSubtotal;
	}
	public void setStandard_unit(String standardUnit) {
		standard_unit = standardUnit;
	}
	public void setQuantity_standard(String quantityStandard) {
		quantity_standard = quantityStandard;
	}
	public void setIs_produced(String isProduced) {
		is_produced = isProduced;
	}
	public void setIs_input(String isInput) {
		is_input = isInput;
	}

	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.id + "|" +
				this.base_plu_key + "|" +
				this.link_plu_key + "|" +
				this.plu_types + "|" +
				this.maker_key + "|" +
				this.short_description + "|" +
				this.long_description + "|" +
				this.commercial_description + "|" +
				this.image_file + "|" +
				this.pos_id + "|" +
				this.cost_decimals + "|" +
				this.unit_key + "|" +
				this.price_decimals + "|" +
				this.adder + "|" +
				this.departament_key + "|" +
				this.suggested_prompt + "|" +
				this.data_entry_key + "|" +
				this.tare_key + "|" +
				this.is_completion + "|" +
				this.has_deposit + "|" +
				this.has_plu_link + "|" +
				this.price_required + "|" +
				this.quantity_disallowed + "|" +
				this.quantity_required + "|" +
				this.decimal_disallowed + "|" +
				this.decimal_required + "|" +
				this.id_required + "|" +
				this.repeat_disallowed + "|" +
				this.scale + "|" +
				this.tracking_total + "|" +
				this.deposit + "|" +
				this.non_merchandise + "|" +
				this.return_disallowed + "|" +
				this.refund_disallowed + "|" +
				this.markdown_disallowed + "|" +
				this.rebate + "|" +
				this.not_for_sale + "|" +
				this.negative + "|" +
				this.clerk_required + "|" +
				this.kit + "|" +
				this.for_scale + "|" +
				this.delivery + "|" +
				this.authrizer_required + "|" +
				this.qty_from_amount + "|" +
				this.print_etq + "|" +
				this.completion + "|" +
				this.is_plu_base + "|" +
				this.store_adm_price + "|" +
				this.reverse_kit + "|" +
				this.verify_stock + "|" +
				this.valid_days + "|" +
				this.ncmsh + "|" +
				this.pis_cofins + "|" +
				this.quantity + "|" +
				this.quantity_min + "|" +
				this.quantity_max + "|" +
				this.classe + "|" +
				this.merchandise_origin + "|" +
				this.merchandise_group + "|" +
				this.pacote + "|" +
				this.wholesale_quantity + "|" +
				this.delivery_type + "|" +
				this.message_subtotal + "|" +
				this.standard_unit + "|" +
				this.quantity_standard + "|" +
				this.is_produced + "|" +
				this.is_input + "|" +
				this.is_scanned + "|" +
				this.gloss_weight + "|" +
				this.net_weight + "|" +
				this.total_tax + "|" +
				this.sefaz_id + "|" +
				this.anp_code + "|" +
				this.pis_pos_id + "|" +
				this.cofins_pos_id + "|" +
				this.self_checkout_weight + "|" +
				this.not_check_weight + "|" +
				this.total_tax_01 + "|" +
				this.total_tax_02 + "|";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroPLU other = (RegistroPLU) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
