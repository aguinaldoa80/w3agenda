package br.com.linkcom.sined.util.emporium.registros;

public class RegistroIdentificacaoCliente {

	private String status = "0";
	private String record = "22";
	private String code_type = "";
	private String code = "";
	private String sku_type = "";
	private String sku = "";
	private String sku_status = "";
	private String amount_left = "";
	private String limit = "";
	private String points = "";
	private String password = "";
	private String crypt_password = "";

	public String getStatus() {
		return status;
	}

	public String getRecord() {
		return record;
	}

	public String getCode_type() {
		return code_type;
	}

	public String getCode() {
		return code;
	}

	public String getSku_type() {
		return sku_type;
	}

	public String getSku() {
		return sku;
	}

	public String getSku_status() {
		return sku_status;
	}

	public String getAmount_left() {
		return amount_left;
	}

	public String getLimit() {
		return limit;
	}

	public String getPoints() {
		return points;
	}

	public String getPassword() {
		return password;
	}

	public String getCrypt_password() {
		return crypt_password;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public void setCode_type(String codeType) {
		code_type = codeType;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setSku_type(String skuType) {
		sku_type = skuType;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public void setSku_status(String skuStatus) {
		sku_status = skuStatus;
	}

	public void setAmount_left(String amountLeft) {
		amount_left = amountLeft;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setCrypt_password(String cryptPassword) {
		crypt_password = cryptPassword;
	}

	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.code_type + "|" +
				this.code + "|" +
				this.sku_type + "|" +
				this.sku + "|" +
				this.sku_status + "|" +
				this.amount_left + "|" +
				this.limit + "|" +
				this.points + "|" +
				this.password + "|" +
				this.crypt_password + "|";
	}
	
}
