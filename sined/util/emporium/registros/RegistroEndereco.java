package br.com.linkcom.sined.util.emporium.registros;

public class RegistroEndereco {

	private String status = "0";
	private String record = "21";
	private String code_type = "";
	private String code = "";
	private String address_type = "";
	private String address = "";
	private String number = "";
	private String complement = "";
	private String neig = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String reference = "";
	private String phone_area_code = "";
	private String phone_number = "";
	private String address_time = "";

	public String getStatus() {
		return status;
	}

	public String getRecord() {
		return record;
	}

	public String getCode_type() {
		return code_type;
	}

	public String getCode() {
		return code;
	}

	public String getAddress_type() {
		return address_type;
	}

	public String getAddress() {
		return address;
	}

	public String getNumber() {
		return number;
	}

	public String getComplement() {
		return complement;
	}

	public String getNeig() {
		return neig;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public String getReference() {
		return reference;
	}

	public String getPhone_area_code() {
		return phone_area_code;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public String getAddress_time() {
		return address_time;
	}

	public void setAddress_type(String addressType) {
		address_type = addressType;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public void setNeig(String neig) {
		this.neig = neig;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setPhone_area_code(String phoneAreaCode) {
		phone_area_code = phoneAreaCode;
	}

	public void setPhone_number(String phoneNumber) {
		phone_number = phoneNumber;
	}

	public void setAddress_time(String addressTime) {
		address_time = addressTime;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public void setCode_type(String codeType) {
		code_type = codeType;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return this.status + "|" +
				this.record + "|" +
				this.code_type + "|" +
				this.code + "|" +
				this.address_type + "|" +
				this.address + "|" +
				this.number + "|" +
				this.complement + "|" +
				this.neig + "|" +
				this.city + "|" +
				this.state + "|" +
				this.zip + "|" +
				this.reference + "|" +
				this.phone_area_code + "|" +
				this.phone_number + "|" +
				this.address_time + "|";
	}
	
}
