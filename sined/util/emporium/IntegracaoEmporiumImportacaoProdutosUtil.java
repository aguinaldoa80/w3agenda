package br.com.linkcom.sined.util.emporium;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialimposto;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.registros.RegistroDepartamento;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLUFilial;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU_KIT;
import br.com.linkcom.sined.util.emporium.registros.RegistroPrecoFilial;
import br.com.linkcom.sined.util.emporium.registros.RegistroSKU;
import br.com.linkcom.sined.util.emporium.registros.RegistroTiposPreco;
import br.com.linkcom.sined.util.emporium.registros.RegistroUnidades;

public class IntegracaoEmporiumImportacaoProdutosUtil {

	public static IntegracaoEmporiumImportacaoProdutosUtil util = new IntegracaoEmporiumImportacaoProdutosUtil();
	
	public void executarJobIntegracao(String banco_cliente) throws IOException {
		this.criarArquivoImportacao(banco_cliente, null, false);
	}
	
	public Arquivoemporium criarArquivoImportacao(String banco_cliente, Integer cdmaterial, boolean substituicao) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		ArquivoImportacaoEmporium bean = this.processaRegistrosW3(banco_cliente, cdmaterial);
		
		if(substituicao){
			sb.append("9|02|\n"); // DEPARTAMENTO
			sb.append("9|81|\n"); // UNIDADES
			sb.append("9|86|\n"); // TIPOS PRE�OS
			sb.append("9|10|\n"); // PLU
			sb.append("9|11|\n"); // PLU/FILIAL
			sb.append("9|12|\n"); // PRE�O/FILIAL
			sb.append("9|01|\n"); // SKU
			sb.append("9|14|\n"); // PLU/KIT
		}
		
//		## TABELAS AUXILIARES ##
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroDepartamento));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroUnidades));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroTiposPreco));
		
//		## TABELAS DE EXCLUS�ES ##
		if(!substituicao){
			sb.append(IntegracaoEmporiumUtil.util.printRegistro(ArquivoemporiumService.getInstance().getRegistrosExclusaoTabelasAuxiliares(banco_cliente)));
			sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroExclusaoTiposPreco));
			sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroExclusaoPLU));
		}
		
//		## TABELAS PLU ##
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroPLU));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroPLUFilial));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroPrecoFilial));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroSKU));
		sb.append(IntegracaoEmporiumUtil.util.printRegistro(bean.listaRegistroPLU_KIT));
		
		String strFile = sb.toString();
		strFile = strFile.replaceAll("\\|\\|", "| |");
		strFile = strFile.replaceAll("\\|\\|", "| |");
		
		if(strFile != null && strFile.trim().length() > 0){
			String nomeArquivo = "arquivo_importacaoprodutos_" + SinedUtil.datePatternForEmporium();
			String pathArquivo = IntegracaoEmporiumUtil.util.saveDir(banco_cliente, false);
			
			IntegracaoEmporiumUtil.util.stringToFile(pathArquivo, nomeArquivo + ".txt", strFile);
			
			Resource resource = new Resource("text/plain", nomeArquivo + ".txt", strFile.getBytes());
			
			Arquivo arquivo = new Arquivo();
			
			arquivo.setNome(resource.getFileName());
			arquivo.setContent(resource.getContents());
			arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
			arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
			arquivo.setTipoconteudo(resource.getContentType());
			
			Arquivoemporium arquivoemporium = new Arquivoemporium();
			arquivoemporium.setArquivo(arquivo);
			arquivoemporium.setArquivoemporiumtipo(Arquivoemporiumtipo.IMPORTACAO_PRODUTOS);
			
			ArquivoDAO.getInstance().saveFile(arquivoemporium, "arquivo");
			ArquivoemporiumService.getInstance().saveOrUpdate(arquivoemporium);
			
			// DELETA TABELAS AUXILIARES DE EXCLUS�O
			ArquivoemporiumService.getInstance().deleteTabelasAuxiliares();
			
			return arquivoemporium;
		}
		
		return null;
	}
	
	private ArquivoImportacaoEmporium processaRegistrosW3(String banco_cliente, Integer id_material) {
		ArquivoImportacaoEmporium bean = new ArquivoImportacaoEmporium();
		
		List<Empresa> listaEmpresa = EmpresaService.getInstance().findForIntegracaoEmporium();
		List<Produto> listaProduto = ProdutoService.getInstance().findForIntegracaoEmporium(id_material);
		
		for (Produto produto : listaProduto) {
			String nomeReduzido = produto.getNomereduzido() != null && !produto.getNomereduzido().trim().equals("") ? produto.getNomereduzido().trim() : produto.getNome();
			String cdmaterial = produto.getCdmaterial().toString();
			Unidademedida unidademedida = produto.getUnidademedida();
			Materialgrupo materialgrupo = produto.getMaterialgrupo();
			Tributacaoecf tributacaoecf = produto.getTributacaoecf();
			String tributacaoStr = tributacaoecf != null && tributacaoecf.getCodigoecf() != null ? tributacaoecf.getCodigoecf() : "";
			Set<Materialrelacionado> listaMaterialrelacionado = produto.getListaMaterialrelacionado();
			boolean vendapromocional = produto.getVendapromocional() != null && produto.getVendapromocional() && listaMaterialrelacionado != null && listaMaterialrelacionado.size() > 0;
			
			Set<Materialimposto> listaMaterialimposto = produto.getListaMaterialimposto();
			Double total_tax = null;
			Double total_tax_01 = null;
			Double total_tax_02 = null;
			
			if(listaMaterialimposto != null && listaMaterialimposto.size() > 0){
				for (Materialimposto materialimposto : listaMaterialimposto) {
					total_tax = materialimposto.getNacionalfederal();
					total_tax_01 = materialimposto.getEstadual();
					total_tax_02 = materialimposto.getMunicipal();
					break;
				}
			} else {
				total_tax = produto.getPercentualimpostoecf() != null && produto.getPercentualimpostoecf() > 0 ? produto.getPercentualimpostoecf() : null;
			}
			
			
//			### PRODUTO ###
			RegistroPLU registroPLU = new RegistroPLU();
			registroPLU.setId(cdmaterial);
			IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "PLU", registroPLU);
			registroPLU.setShort_description(IntegracaoEmporiumUtil.util.maxPrintString(nomeReduzido, 22)); // Nome reduzido
			registroPLU.setLong_description(IntegracaoEmporiumUtil.util.maxPrintString(produto.getNome(), 50)); // Nome
			registroPLU.setCommercial_description(IntegracaoEmporiumUtil.util.maxPrintString(produto.getNome(), 50)); // Nome
			registroPLU.setNcmsh(produto.getNcmcompleto() != null ? IntegracaoEmporiumUtil.util.maxPrintString(produto.getNcmcompleto(), 12) : ""); // NCM
			registroPLU.setUnit_key(unidademedida.getSimbolo()); // Unidade de medida
			registroPLU.setDepartament_key(materialgrupo.getCdmaterialgrupo().toString()); // Grupo de material
			registroPLU.setKit(vendapromocional ? "1" : "0"); // Promocional
			registroPLU.setPos_id(tributacaoStr); // Grupo de tributa��o
			registroPLU.setTotal_tax(IntegracaoEmporiumUtil.util.printDouble(total_tax, 7, 3));
			registroPLU.setTotal_tax_01(IntegracaoEmporiumUtil.util.printDouble(total_tax_01, 7, 3));
			registroPLU.setTotal_tax_02(IntegracaoEmporiumUtil.util.printDouble(total_tax_02, 7, 3));
			
			if(produto.getAtivo() == null || !produto.getAtivo()){
				if(vendapromocional){
					RegistroTiposPreco registroTiposPreco = new RegistroTiposPreco();
					registroTiposPreco.setStatus("1");
					registroTiposPreco.setPrice_key(cdmaterial);
					registroTiposPreco.setName("LKKIT_" + cdmaterial);
					
					bean.listaRegistroExclusaoTiposPreco.add(registroTiposPreco);
				}
				
				registroPLU.setStatus("1");
				bean.listaRegistroExclusaoPLU.add(registroPLU);
				
				continue;
			}
			
			bean.listaRegistroPLU.add(registroPLU);
			
//			### PRODUTO POR FILIAL ###
			
			for (Empresa empresa : listaEmpresa) {
				RegistroPLUFilial registroPLUFilial = new RegistroPLUFilial();
				IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "PLUFilial", registroPLUFilial);
				registroPLUFilial.setStore_key(empresa.getIntegracaopdv());
				registroPLUFilial.setId(cdmaterial);
				registroPLUFilial.setPos_id(tributacaoStr); // Grupo de tributa��o
				registroPLUFilial.setNf_id(tributacaoStr); // Grupo de tributa��o
				
				boolean achou = false;
				if(listaMaterialimposto != null && listaMaterialimposto.size() > 0){
					for (Materialimposto materialimposto : listaMaterialimposto) {
						if(materialimposto.getEmpresa() != null && materialimposto.getEmpresa().equals(empresa)){
							registroPLUFilial.setTotal_tax(IntegracaoEmporiumUtil.util.printDouble(materialimposto.getNacionalfederal(), 7, 3));
							registroPLUFilial.setTotal_tax_01(IntegracaoEmporiumUtil.util.printDouble(materialimposto.getEstadual(), 7, 3));
							registroPLUFilial.setTotal_tax_02(IntegracaoEmporiumUtil.util.printDouble(materialimposto.getMunicipal(), 7, 3));
							achou = true;
							break;
						}
					}
				}
				
				if(!achou){
					registroPLUFilial.setTotal_tax(IntegracaoEmporiumUtil.util.printDouble(total_tax, 7, 3));
					registroPLUFilial.setTotal_tax_01(IntegracaoEmporiumUtil.util.printDouble(total_tax_01, 7, 3));
					registroPLUFilial.setTotal_tax_02(IntegracaoEmporiumUtil.util.printDouble(total_tax_02, 7, 3));
				}
				
				bean.listaRegistroPLUFilial.add(registroPLUFilial);
			}
			
//			### C�DIGO DE BARRAS ###
			if(produto.getCodigobarras() != null && !produto.getCodigobarras().equals("")){
				if(vendapromocional){
					registroPLU.setLong_description(IntegracaoEmporiumUtil.util.maxPrintString(produto.getCodigobarras() + " " + registroPLU.getLong_description(), 50));
					registroPLU.setCommercial_description(IntegracaoEmporiumUtil.util.maxPrintString(produto.getCodigobarras() + " " + registroPLU.getCommercial_description(), 50));
				} else {
					RegistroSKU registroSKU = new RegistroSKU();
					IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "SKU", registroSKU);
					registroSKU.setSku_id(produto.getCodigobarras());
					registroSKU.setPlu_id(cdmaterial);
					
					bean.listaRegistroSKU.add(registroSKU);
				}
			}
			
//			### GRUPO DE MATERIAL ###
			RegistroDepartamento registroDepartamento = new RegistroDepartamento();
			IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "Departamento", registroDepartamento);
			registroDepartamento.setId(materialgrupo.getCdmaterialgrupo().toString());
			registroDepartamento.setName(materialgrupo.getNome());
			
			if(!bean.listaRegistroDepartamento.contains(registroDepartamento)){
				bean.listaRegistroDepartamento.add(registroDepartamento);
			}
			
//			### UNIDADE DE MEDIDA ###
			RegistroUnidades registroUnidades = new RegistroUnidades();
			IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "Unidades", registroUnidades);
			registroUnidades.setShort_name(unidademedida.getSimbolo()); // Simbolo
			registroUnidades.setLong_name(unidademedida.getNome()); // Nome
			
			if(!bean.listaRegistroUnidades.contains(registroUnidades)){
				bean.listaRegistroUnidades.add(registroUnidades);
			}
			
//			### VALOR DE VENDA PADR�O ###
			String valorvenda = IntegracaoEmporiumUtil.util.printDouble(produto.getValorvenda(), 15, 3);
			String start_price = SinedDateUtils.toString(SinedDateUtils.currentDate(), "ddMMyyyy"); 
			String time_price = SinedDateUtils.toString(SinedDateUtils.currentDate(), "HHmmss");
			
			for (Empresa empresa : listaEmpresa) {
				RegistroPrecoFilial registroPrecoFilial = new RegistroPrecoFilial();
				IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "PrecoFilial", registroPrecoFilial);
				registroPrecoFilial.setStore_key(empresa.getIntegracaopdv());
				registroPrecoFilial.setId(cdmaterial); // Id do produto
				registroPrecoFilial.setPrice(valorvenda); // Valor de venda
				registroPrecoFilial.setStart_price(start_price); // O pre�o come�a a valer quando cria o arquivo de importa��o
				registroPrecoFilial.setTime_price(time_price); // O pre�o come�a a valer quando cria o arquivo de importa��o
				registroPrecoFilial.setType_price("1"); // Valor padr�o de venda
				
				if(vendapromocional){
					registroPrecoFilial.setType_price(cdmaterial); // Pre�o principal do produto
				}
				
				bean.listaRegistroPrecoFilial.add(registroPrecoFilial);
			}
			
//			### VENDA PROMOCIONAL ###
			if(vendapromocional){
				RegistroTiposPreco registroTiposPreco = new RegistroTiposPreco();
				IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "TiposPreco", registroTiposPreco);
				registroTiposPreco.setPrice_key(cdmaterial);
				registroTiposPreco.setName("LKKIT_" + cdmaterial);
				
				bean.listaRegistroTiposPreco.add(registroTiposPreco);
				
				for (Materialrelacionado materialrelacionado : listaMaterialrelacionado) {
					Material materialpromocao = materialrelacionado.getMaterialpromocao();
					Double quantidade = materialrelacionado.getQuantidade() != null ? materialrelacionado.getQuantidade().doubleValue() : 1;
					
					for (Empresa empresa : listaEmpresa) {
						RegistroPrecoFilial registroPrecoFilial_Kit = new RegistroPrecoFilial();
						IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "PrecoFilial", registroPrecoFilial_Kit);
						registroPrecoFilial_Kit.setStore_key(empresa.getIntegracaopdv());
						registroPrecoFilial_Kit.setId(materialpromocao.getCdmaterial().toString()); // Id do produto
						registroPrecoFilial_Kit.setPrice(IntegracaoEmporiumUtil.util.printDouble(materialrelacionado.getValorvenda().getValue().doubleValue(), 15, 3)); // Valor de venda
						registroPrecoFilial_Kit.setStart_price(start_price); // O pre�o come�a a valer quando cria o arquivo de importa��o
						registroPrecoFilial_Kit.setTime_price(time_price); // O pre�o come�a a valer quando cria o arquivo de importa��o
						registroPrecoFilial_Kit.setType_price(cdmaterial); // Valor padr�o de venda
						
						bean.listaRegistroPrecoFilial.add(registroPrecoFilial_Kit);
					}
					
					RegistroPLU_KIT registroPLUKIT = new RegistroPLU_KIT();
					IntegracaoEmporiumUtil.util.setDefaultValues(banco_cliente, "PLU_KIT", registroPLUKIT);
					registroPLUKIT.setId_main(cdmaterial); // C�digo do kit
					registroPLUKIT.setId(materialpromocao.getCdmaterial().toString()); // C�digo do produto que comp�e
					registroPLUKIT.setQuantity(IntegracaoEmporiumUtil.util.printDouble(quantidade, 9, 3));	// Quantidade do kit
					registroPLUKIT.setType_price(cdmaterial); // Tipo de pre�o
					
					bean.listaRegistroPLU_KIT.add(registroPLUKIT);
				}
			}
			
		}
		
		return bean;
	}
	
	private class ArquivoImportacaoEmporium {
//		## TABELAS DE EXCLUS�ES ##
		List<RegistroTiposPreco> listaRegistroExclusaoTiposPreco = new ArrayList<RegistroTiposPreco>();
		List<RegistroPLU> listaRegistroExclusaoPLU = new ArrayList<RegistroPLU>();
		
//		## TABELAS AUXILIARES ##
		List<RegistroDepartamento> listaRegistroDepartamento = new ArrayList<RegistroDepartamento>();
		List<RegistroUnidades> listaRegistroUnidades = new ArrayList<RegistroUnidades>();
		List<RegistroTiposPreco> listaRegistroTiposPreco = new ArrayList<RegistroTiposPreco>();
		
//		## TABELAS PLU ##
		List<RegistroPLU> listaRegistroPLU = new ArrayList<RegistroPLU>();
		List<RegistroPLUFilial> listaRegistroPLUFilial = new ArrayList<RegistroPLUFilial>();
		List<RegistroPrecoFilial> listaRegistroPrecoFilial = new ArrayList<RegistroPrecoFilial>();
		List<RegistroSKU> listaRegistroSKU = new ArrayList<RegistroSKU>();
		List<RegistroPLU_KIT> listaRegistroPLU_KIT = new ArrayList<RegistroPLU_KIT>();
	}

}
