package br.com.linkcom.sined.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.web.WebRequestContext;


public class ArquivoImportacao {
	
	private final DateFormat FORMAT = new SimpleDateFormat("ddMMyyyy");
	
	private String regA;
	private String regB;
	private String regC;
	
	// REGISTRO A
	private Integer numero;
	private Date data_de_emissao;
	private String nome_cliente;
	private String cpf_cliente;
	private String cnpj_cliente;
	private String im_cliente;
	private String email_cliente;
	private String telefone_cliente;
	private String lograouro_cliente;
	private Integer numero_cliente;
	private String complemento_cliente;
	private String bairro_cliente;
	private String cod_ibge_cliente;
	private String cep_cliente;
	
	// REGISTRO B
	private String discriminacao_servico;
	
	// REGISTRO C
	private Integer natureza_operacao;
	private Integer regime_especial;
	private String item_lista_de_servico;
	private String codigo_cnae;
	private String codigo_tributacao;
	private Double valor_servico;
	private Double aliquota_iss;
	private Boolean incide_iss;
	private Double aliquota_pis;
	private Boolean incide_pis;
	private Double aliquota_cofins;
	private Boolean incide_cofins;
	private Double aliquota_inss;
	private Boolean incide_inss;
	private Double aliquota_ir;
	private Boolean incide_ir;
	private Double aliquota_csll;
	private Boolean incide_csll;
	private Double descontocondicionado;
	private Double descontoincondicionado;
	private Double deducao;
	private Double outrasretencoes;
	
	public ArquivoImportacao(String regA, String regB, String regC, WebRequestContext request) {
		this.regA = regA;
		this.regB = regB;
		this.regC = regC;
		
		this.validar();
		this.processar();
		this.validarObrigatoriedade(request);
	}
	
	private void validarObrigatoriedade(WebRequestContext request) {
		List<String> listaMsg = new ArrayList<String>();
		
		if(numero == null)
			listaMsg.add("O campo n�mero da nota fiscal � obrigat�rio.");
		if(data_de_emissao == null)
			listaMsg.add("O campo data de emiss�o da nota fiscal � obrigat�rio.");
//		if(nome_cliente == null)
//			listaMsg.add("O campo nome do cliente � obrigat�rio.");
//		if(cpf_cliente == null && cnpj_cliente == null)
//			listaMsg.add("� obrigat�rio o preenchimento do cpf ou cnpj do cliente.");
//		if(numero_cliente == null)
//			listaMsg.add("O campo n�mero do cliente � obrigat�rio.");
//		if(bairro_cliente == null)
//			listaMsg.add("O campo bairro do cliente � obrigat�rio.");
//		if(cod_ibge_cliente == null)
//			listaMsg.add("O campo c�digo do ibge do cliente � obrigat�rio.");
//		if(cep_cliente == null)
//			listaMsg.add("O campo cep do cliente � obrigat�rio.");
		if(discriminacao_servico == null)
			listaMsg.add("O campo discrimina��o do servi�o � obrigat�rio.");
		if(natureza_operacao == null)
			listaMsg.add("O campo natureza da opera��o � obrigat�rio.");
		if(item_lista_de_servico == null)
			listaMsg.add("O campo item da lista de servi�o � obrigat�rio.");
		if(valor_servico == null)
			listaMsg.add("O campo valor do servi�o � obrigat�rio.");
				
		
		if(listaMsg.size() > 0){
			for (String string : listaMsg) {
				request.addError(string);
			}
			throw new SinedException("Existem campos obrigat�rios n�o preenchidos no arquivo especificado.");
		}
	}

	private void validar(){
		if(!regA.startsWith("A")){
			throw new SinedException("Registro A inv�lido.");
		}
		if(!regB.startsWith("B")){
			throw new SinedException("Registro B inv�lido.");
		}
		if(!regC.startsWith("C")){
			throw new SinedException("Registro C inv�lido.");
		}
	}
	
	private void processar(){
		
		// REGISTRO A
		numero = this.parseInt(regA.substring(1, 16));
		data_de_emissao = this.parseDate(regA.substring(16, 24));
		nome_cliente = this.parseString(regA.substring(24, 139));
		cpf_cliente = this.parseString(regA.substring(139, 150));
		cnpj_cliente = this.parseString(regA.substring(150, 164));
		im_cliente = this.parseString(regA.substring(164, 179));
		email_cliente = this.parseString(regA.substring(179, 259));
		telefone_cliente = this.parseString(regA.substring(259, 270));
		lograouro_cliente = this.parseString(regA.substring(270, 395));
		numero_cliente = this.parseInt(regA.substring(395, 405));
		complemento_cliente = this.parseString(regA.substring(405, 465));
		bairro_cliente = this.parseString(regA.substring(465, 525));
		cod_ibge_cliente = this.parseString(regA.substring(525, 532));
		cep_cliente = this.parseString(regA.substring(532, 540));
		
		// REGISTRO B
		discriminacao_servico = this.parseString(regB.substring(1, 2001));
		
		// REGISTRO C
		natureza_operacao = this.parseInt(regC.substring(1, 2));
		regime_especial = this.parseInt(regC.substring(2, 3));
		item_lista_de_servico = this.parseString(regC.substring(3, 8));
		codigo_cnae = this.parseString(regC.substring(8, 15));
		codigo_tributacao = this.parseString(regC.substring(15, 35));
		valor_servico = this.parseDouble(regC.substring(35, 52), 2);
		aliquota_iss = this.parseDouble(regC.substring(52, 61), 4);
		incide_iss = regC.substring(61, 62).equals("1");
		aliquota_pis = this.parseDouble(regC.substring(62, 71), 4);
		incide_pis = regC.substring(71, 72).equals("1");
		aliquota_cofins = this.parseDouble(regC.substring(72, 81), 4);
		incide_cofins = regC.substring(81, 82).equals("1");
		aliquota_inss = this.parseDouble(regC.substring(82, 91), 4);
		incide_inss = regC.substring(91, 92).equals("1");
		aliquota_ir = this.parseDouble(regC.substring(92, 101), 4);
		incide_ir = regC.substring(101, 102).equals("1");
		aliquota_csll = this.parseDouble(regC.substring(102, 111), 4);
		incide_csll = regC.substring(111, 112).equals("1");
		descontocondicionado = this.parseDouble(regC.substring(112, 129), 2);
		descontoincondicionado = this.parseDouble(regC.substring(129, 146), 2);
		deducao = this.parseDouble(regC.substring(146, 163), 2);
		outrasretencoes = this.parseDouble(regC.substring(163, 180), 2);
	}
	
//	private String retiraZeroEsquerda(String s) {
//		if(s != null){
//			while(s.charAt(0) == '0') s = s.substring(1, s.length());
//		}
//		return s;
//		
//	}

	private Date parseDate(String s){
		if(s == null || s.trim().equals("")) return null;
		
		try {
			return new Date(FORMAT.parse(s).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String parseString(String s){
		if(s == null || s.trim().equals("")) return null;
		
		s = s.trim();
		return s;
	}
	
	private Integer parseInt(String s){
		if(s == null || s.trim().equals("")) return null;
		
		s = s.trim();
		return Integer.parseInt(s);
	}
	
	
	private Double parseDouble(String s, int numDecimais){
		if(s == null || s.trim().equals("")) return null;
		
		s = s.trim();
		return Double.parseDouble(s.substring(0, s.length()-numDecimais) + "." + s.substring(s.length()-numDecimais, s.length()));
	}

	public Integer getNumero() {
		return numero;
	}

	public Date getData_de_emissao() {
		return data_de_emissao;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public String getCpf_cliente() {
		return cpf_cliente;
	}

	public String getCnpj_cliente() {
		return cnpj_cliente;
	}

	public String getIm_cliente() {
		return im_cliente;
	}

	public String getEmail_cliente() {
		return email_cliente;
	}

	public String getTelefone_cliente() {
		return telefone_cliente;
	}

	public String getLograouro_cliente() {
		return lograouro_cliente;
	}

	public Integer getNumero_cliente() {
		return numero_cliente;
	}

	public String getComplemento_cliente() {
		return complemento_cliente;
	}

	public String getBairro_cliente() {
		return bairro_cliente;
	}

	public String getCod_ibge_cliente() {
		return cod_ibge_cliente;
	}

	public String getCep_cliente() {
		return cep_cliente;
	}

	public String getDiscriminacao_servico() {
		return discriminacao_servico;
	}

	public Integer getNatureza_operacao() {
		return natureza_operacao;
	}

	public Integer getRegime_especial() {
		return regime_especial;
	}

	public String getItem_lista_de_servico() {
		return item_lista_de_servico;
	}

	public String getCodigo_cnae() {
		return codigo_cnae;
	}

	public String getCodigo_tributacao() {
		return codigo_tributacao;
	}

	public Double getValor_servico() {
		return valor_servico;
	}

	public Double getAliquota_iss() {
		return aliquota_iss;
	}

	public Boolean getIncide_iss() {
		return incide_iss;
	}

	public Double getAliquota_pis() {
		return aliquota_pis;
	}

	public Boolean getIncide_pis() {
		return incide_pis;
	}

	public Double getAliquota_cofins() {
		return aliquota_cofins;
	}

	public Boolean getIncide_cofins() {
		return incide_cofins;
	}

	public Double getAliquota_inss() {
		return aliquota_inss;
	}

	public Boolean getIncide_inss() {
		return incide_inss;
	}

	public Double getAliquota_ir() {
		return aliquota_ir;
	}

	public Boolean getIncide_ir() {
		return incide_ir;
	}

	public Double getAliquota_csll() {
		return aliquota_csll;
	}

	public Boolean getIncide_csll() {
		return incide_csll;
	}

	public Double getDescontocondicionado() {
		return descontocondicionado;
	}

	public Double getDescontoincondicionado() {
		return descontoincondicionado;
	}

	public Double getDeducao() {
		return deducao;
	}

	public Double getOutrasretencoes() {
		return outrasretencoes;
	}
	
}
