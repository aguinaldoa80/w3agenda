package br.com.linkcom.sined.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.SituacaoOportunidade;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AcompanhamentometaAnaliticoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AcompanhamentometaAnaliticoItensBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EvolucaoFaturamentoBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoProjeto;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.Histograma;
import br.com.linkcom.sined.util.bean.GenericBean;


public class Grafico {
	/**
	 * Cria um grafico de acompanhamento mostrando a evolu��o de um projeto.
	 * @param listaGraficoPlanejamento
	 * @return Image
	 * @author Ramon Brazil
	 */
	public static Image geraGraficoAcompanhamentoProjeto(List<AcompanhamentoProjeto> listaGraficoAcompanhamento) throws IOException{
		
		final TimeSeries sPlanejamento = new TimeSeries("Planejamento",Day.class);
    	final TimeSeries sAndamento = new TimeSeries("Andamento",Day.class);
    	final TimeSeries sCusto = new TimeSeries("Custo",Day.class);

    	Calendar data = Calendar.getInstance();
    	
        for (AcompanhamentoProjeto graficoAcompanhamento: listaGraficoAcompanhamento) {
        	data.setTime(graficoAcompanhamento.getData());
        	sPlanejamento.add(new Day(data.get(Calendar.DAY_OF_MONTH),data.get(Calendar.MONTH)+1, data.get(Calendar.YEAR)),graficoAcompanhamento.getPlanejamento());
			sAndamento.add(new Day(data.get(Calendar.DAY_OF_MONTH),data.get(Calendar.MONTH)+1, data.get(Calendar.YEAR)),graficoAcompanhamento.getAndamento());
			sCusto.add(new Day(data.get(Calendar.DAY_OF_MONTH),data.get(Calendar.MONTH)+1, data.get(Calendar.YEAR)),graficoAcompanhamento.getCusto());
		}
  
        final TimeSeriesCollection tsCollection = new TimeSeriesCollection();
        tsCollection.addSeries(sPlanejamento);
        tsCollection.addSeries(sAndamento);
        tsCollection.addSeries(sCusto);
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "",
            "", 
            "", 
            tsCollection,
            true,
            true,
            false
        );
        chart.setBackgroundPaint(Color.white);
        
        //Legenda  
        LegendTitle legend = chart.getLegend();
        legend.getContentXOffset();
        legend.setPosition(RectangleEdge.BOTTOM);        
        
        //�rea de plotagem do gr�fico
        final XYPlot plot = chart.getXYPlot();
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0)); 
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.black);  
        
        //Eixo X - Datas
        DateAxis dateAxis = (DateAxis) plot.getDomainAxis();
        //Formata a data 
        dateAxis.setDateFormatOverride(new SimpleDateFormat("dd/MM")); 
        //coloca os itens do eixo X na vertical
        dateAxis.setVerticalTickLabels(true);

        //Eixo Y - Percentuais
        final NumberAxis rangeAxisX = (NumberAxis) plot.getRangeAxis();
        //rangeAxisX.setRange(0.0, 100.0);
        rangeAxisX.setLabel("Percentual (%)");
        rangeAxisX.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        //Renderizador das Linhas
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesStroke(0,new BasicStroke(2.0f));
        renderer.setSeriesStroke(1,new BasicStroke(2.0f));
        renderer.setSeriesStroke(2,new BasicStroke(2.0f));
        plot.setRenderer(renderer);        
       
    	//Cria a imagem do gr�fico
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();		
		ChartUtilities.writeChartAsPNG(byteArrayOutputStream, chart,714,430);
		byte[] bytes = byteArrayOutputStream.toByteArray();
    	
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		BufferedImage image = ImageIO.read(byteArrayInputStream);     
		return image;
     }
	
	public static Image geraGraficoEvolucaoFaturamento(List<EvolucaoFaturamentoBean> listaEvolucao) throws IOException{
		
		final TimeSeries sFaturamento = new TimeSeries("Faturamento",Month.class);

    	Calendar data = Calendar.getInstance();
    	
        for (EvolucaoFaturamentoBean bean: listaEvolucao) {
        	data.setTime(bean.getData());
        	sFaturamento.add(new Month(data.get(Calendar.MONTH)+1, data.get(Calendar.YEAR)), bean.getValor());
		}
  
        final TimeSeriesCollection tsCollection = new TimeSeriesCollection();
        tsCollection.addSeries(sFaturamento);
        
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "",
            "", 
            "", 
            tsCollection,
            true,
            true,
            false
        );
        chart.setBackgroundPaint(Color.white);
        
        //Legenda  
        LegendTitle legend = chart.getLegend();
        legend.getContentXOffset();
        legend.setPosition(RectangleEdge.BOTTOM);        
        
        //�rea de plotagem do gr�fico
        final XYPlot plot = chart.getXYPlot();
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0)); 
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.black);  
        
        //Eixo X - Datas
        DateAxis dateAxis = (DateAxis) plot.getDomainAxis();
        //Formata a data 
        dateAxis.setDateFormatOverride(new SimpleDateFormat("MM/yyyy")); 
        //coloca os itens do eixo X na vertical
        dateAxis.setVerticalTickLabels(true);
        dateAxis.setTickUnit(new DateTickUnit(DateTickUnit.MONTH, 1));
        
        //Eixo Y - Percentuais
        final NumberAxis rangeAxisX = (NumberAxis) plot.getRangeAxis();
        //rangeAxisX.setRange(0.0, 100.0);
        rangeAxisX.setLabel("Valor (R$)");
        rangeAxisX.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        //Renderizador das Linhas
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesStroke(0,new BasicStroke(2.0f));
        plot.setRenderer(renderer);        
       
    	//Cria a imagem do gr�fico
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();		
		ChartUtilities.writeChartAsPNG(byteArrayOutputStream, chart,714,430);
		byte[] bytes = byteArrayOutputStream.toByteArray();
    	
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		BufferedImage image = ImageIO.read(byteArrayInputStream);     
		return image;
     }
	
	/**
	 * Cria um grafico de acompanhamento mostrando a utiliza��o de Material.
	 * @param listaHistograma
	 * @return Image
	 * @author Ramon Brazil
	 */
	public static Image gerahistograma(List<Histograma> listaHistograma) throws IOException{
		
		String series = "Planejado";
    	String series3 = "Realizado";
    	final  DefaultCategoryDataset data = new DefaultCategoryDataset();   	
        for (Histograma histograma: listaHistograma) {

        	data.addValue(histograma.getPlanejado(), series, histograma.getDia());
        	data.addValue(histograma.getRealizado(), series3, histograma.getDia());
		}
        final JFreeChart chart = ChartFactory.createBarChart(
            "",
            "", 
            "", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );
        chart.setBackgroundPaint(Color.white); 
        final CategoryPlot plot = chart.getCategoryPlot();
        CategoryAxis axis = plot.getDomainAxis();
        axis.setLowerMargin(0.02); 
        axis.setCategoryMargin(0.15); 
        axis.setUpperMargin(0.02); 
        axis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        
        //coloca a legenda � direita do grafico.  
        LegendTitle legend = chart.getLegend();
        legend.getContentXOffset();
        legend.setPosition(RectangleEdge.RIGHT);
        
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0)); 
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.black);        
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0.01);
        
        GradientPaint gp1 = new GradientPaint(
        0.0f, 0.0f, new Color(32,140,0),
        0.0f, 0.0f, new Color(32,140,0)
        );
        GradientPaint gp0 = new GradientPaint(
        0.0f, 0.0f, new Color(183,173,0),
        0.0f, 0.0f, new Color(183,173,0)
        );
        renderer.setSeriesPaint(0, gp0);
        renderer.setSeriesPaint(1, gp1);
            
    	
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();		
		ChartUtilities.writeChartAsPNG(byteArrayOutputStream, chart,535,320);
		byte[] bytes = byteArrayOutputStream.toByteArray();
    	
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		BufferedImage image = ImageIO.read(byteArrayInputStream);     
		return image;
     }
	
	public static Image geraGraficoAcompanhamentometa(AcompanhamentometaAnaliticoBean bean) throws IOException{
		List<AcompanhamentometaAnaliticoItensBean> lista = bean.getListaItens();
		if(lista == null) lista = new ArrayList<AcompanhamentometaAnaliticoItensBean>();
		
        DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        for(AcompanhamentometaAnaliticoItensBean item : lista){
            defaultCategoryDataset.addValue(item.getVendadiaria().getValue().doubleValue(), item.getData(), format.format(item.getData()));
        }

        JFreeChart chart = ChartFactory.createBarChart3D("", "", "", defaultCategoryDataset, PlotOrientation.VERTICAL, false, false, false);
        chart.setBorderVisible(true);
        chart.setBorderPaint(Color.black);
        
        CategoryPlot plot = chart.getCategoryPlot();
        if(bean.getMetadiaria() != null){
	        plot.addRangeMarker(new ValueMarker(bean.getMetadiaria().getValue().doubleValue()));
        }
        
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0.01);
        
        CategoryAxis axis = plot.getDomainAxis(); 
        axis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        axis.setTickLabelFont(new Font(axis.getTickLabelFont().getName(), axis.getTickLabelFont().getStyle(), 12));
        
//      NumberAxis numberAxis = (NumberAxis)plot.getRangeAxis(); 
//      numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits()); 
//      numberAxis.setTickUnit(new NumberTickUnit(1000));
        
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();		
		ChartUtilities.writeChartAsPNG(byteArrayOutputStream, chart,1024,450);
		byte[] bytes = byteArrayOutputStream.toByteArray();
    	
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		BufferedImage image = ImageIO.read(byteArrayInputStream);     
		return image;
     }
	
	public static Arquivo geraHistogramaOportunidadesMes(List<SituacaoOportunidade> listaSituacaoOportunidade)throws IOException{
		
    	if (listaSituacaoOportunidade != null && !listaSituacaoOportunidade.isEmpty()){
			int qtdMeses = listaSituacaoOportunidade.get(0).getListaMes().size();
	    	final  DefaultCategoryDataset data = new DefaultCategoryDataset();
	    	for(int i = 0; i < qtdMeses; i++){
	    		
	    		for (SituacaoOportunidade situacaoOportunidade : listaSituacaoOportunidade) {
	    			String aux[] = situacaoOportunidade.getListaMes().get(i).getMesAnoCorrente().split("/");
	    			data.addValue(situacaoOportunidade.getListaMes().get(i).getTotal(), situacaoOportunidade.getDescricao(), aux[0].substring(0, 3)+"/"+aux[1]);
				}
	    	}
		    	
	        final JFreeChart chart = ChartFactory.createBarChart(
	                "",
	                "", 
	                "", 
	                data,
	                PlotOrientation.VERTICAL,
	                true,
	                true,
	                false
	        );
	    	
	        chart.setBackgroundPaint(Color.white); 
	        final CategoryPlot plot = chart.getCategoryPlot();
	        CategoryAxis axis = plot.getDomainAxis();
	        axis.setLowerMargin(0.02); 
	        axis.setCategoryMargin(0.15); 
	        axis.setUpperMargin(0.02); 
	    
	        //coloca a legenda � direita do grafico.  
	        LegendTitle legend = chart.getLegend();
	        legend.getContentXOffset();
	        legend.setPosition(RectangleEdge.RIGHT);
	        
	        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0)); 
	        plot.setDomainGridlinePaint(Color.white);
	        plot.setRangeGridlinePaint(Color.black);        
	        BarRenderer renderer = (BarRenderer) plot.getRenderer();
	        renderer.setDrawBarOutline(false);
	        renderer.setItemMargin(0.01);
	        
	        GradientPaint gp1 = new GradientPaint(
	        0.0f, 0.0f, new Color(32,140,0),
	        0.0f, 0.0f, new Color(32,140,0)
	        );
	        GradientPaint gp0 = new GradientPaint(
	        0.0f, 0.0f, new Color(183,173,0),
	        0.0f, 0.0f, new Color(183,173,0)
	        );
	        renderer.setSeriesPaint(0, gp0);
	        renderer.setSeriesPaint(1, gp1);
	            
	    	
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();		
			
			ChartUtilities.writeChartAsPNG(byteArrayOutputStream, chart,1335,320);
			byte[] bytes = byteArrayOutputStream.toByteArray();
			return new Arquivo(bytes, "graficoOportunidade.jpeg", "image/jpeg");

    	}
    	else{
    		return null;
    	}
	}
	
	
	/**
	 * @param listaPercentual
	 * @return
	 * @throws Exception
	 * @author Andrey Leonardo
	 * @since 24/06/2015
	 */
	public static Arquivo gerarPieChartAnaliseCustoSintetica(List<GenericBean> listaPercentual) throws Exception{
		if(listaPercentual != null){
			final  DefaultPieDataset data = new DefaultPieDataset();
			for(GenericBean percentual : listaPercentual){
				data.setValue((String)percentual.getId(), (Double)percentual.getValue());
			}
			final JFreeChart pieChart = ChartFactory.createPieChart(
					"",
					data,
					true,					
					true,
					false
					);
			
			// Ajuste de background
			pieChart.setBackgroundPaint(Color.WHITE);			
						
	        PiePlot plot = (PiePlot) pieChart.getPlot();
	        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
	        plot.setNoDataMessage("N�o h� dados suficientes dispon�veis.");
	        plot.setCircular(false);
	        plot.setLabelGap(0.02);
	        PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator(
	                "{0}: {2}", new DecimalFormat("0.00"), new DecimalFormat("0.00%"));
	        plot.setLabelGenerator(gen);
	        
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();	
			ChartUtilities.writeChartAsPNG(byteArrayOutputStream, pieChart,741,400);
			byte[] bytes = byteArrayOutputStream.toByteArray();
			return new Arquivo(bytes, "pieChartAnaliseCusto.jpeg", "image/jpeg");
		}
		else{
			return null;
		}
	}
}

