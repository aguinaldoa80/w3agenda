package br.com.linkcom.sined.util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.sined.util.controller.SinedExcel;


public class SinedExcelUtil {
	
	public static void criarTituloExcel(HSSFRow row, int i, String value) {
		HSSFCell cell = row.createCell((short) (i));
		cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cell.setCellValue(value);
	}
	
	public static HSSFSheet iniciarPlanilha(SinedExcel excel,short defautColumn) {
		HSSFSheet planilha = excel.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) defautColumn);
		return planilha;
	}

	
	
	public static void mescarCelula(HSSFSheet planilha,int nLinha,int celulas) {
		planilha.addMergedRegion(new Region(nLinha, (short) 0, nLinha + 1, (short) celulas));
	}

	public static void justarTanhoColunas(HSSFSheet planilha,int nColulas, short tamanho) {
		int i =0;
		while(i < nColulas){
			planilha.setColumnWidth((short) i, tamanho);
			i++;
		}
	}
	
}