package br.com.linkcom.sined.util;

public class DatasourceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public static final int NO_UPDATE = 1;
	
	private int errorCode = 0;

	public DatasourceException() {
		super();
	}

	public DatasourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DatasourceException(String message) {
		super(message);
	}

	public DatasourceException(Throwable cause) {
		super(cause);
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public DatasourceException setErrorCode(int errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
}
