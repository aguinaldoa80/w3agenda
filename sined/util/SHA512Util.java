package br.com.linkcom.sined.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public class SHA512Util {

	public static String encrypt(String value) throws NoSuchAlgorithmException{
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
		byte[] hash = messageDigest.digest(value.getBytes());
		return new String(new Base64().encode(hash));
	}
}
