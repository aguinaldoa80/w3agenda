package br.com.linkcom.sined.util.listener;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.naming.NamingException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.sql.DataSource;

import org.springframework.jndi.JndiTemplate;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Sessao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.SinedUtil;
 
 
public class SessaoListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent evt) {
	}

	public void sessionDestroyed(HttpSessionEvent evt) {
		HttpSession session = evt.getSession();
		Statement st = null;
		Connection db = null;
		try {
			Object strConexao = session.getAttribute("STRING_CONEXAO_BANCO");
			Object cdpedidovendasincronizacaowms = session.getAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS");
			if(strConexao != null){
				JndiTemplate jndiTemplate = new JndiTemplate();
				DataSource dataSource = (DataSource)jndiTemplate.lookup((String) strConexao, DataSource.class);
				Class.forName("org.postgresql.Driver");
				db = dataSource.getConnection(); 
				
				if(cdpedidovendasincronizacaowms != null){
					try{
						session.removeAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS");
						Integer cdpedidovendasincronizacao = (Integer) cdpedidovendasincronizacaowms;
						String sqlpedidovendasincronizacao = this.makeUpdatePedidovendasincronizacao(cdpedidovendasincronizacao);
						
						if(sqlpedidovendasincronizacao != null && !"".equals(sqlpedidovendasincronizacao)){
							st = db.createStatement();
							st.execute(sqlpedidovendasincronizacao);
							st.close();
						}
					} catch (Exception e) {
					}
				}
				Usuario usuario = (Usuario)session.getAttribute("USER");
				if(usuario != null && usuario.getCdpessoa() != null){
					Sessao sessao = new Sessao();
					sessao.setDatahora(new Timestamp(System.currentTimeMillis()));
					sessao.setEntrada(false);
					try{
						sessao.setIp(NeoWeb.getRequestContext().getServletRequest().getRemoteAddr());
					} catch (Exception e) {
					}
					sessao.setSid(session.getId());
					sessao.setUsuario(usuario);
					
					
					String sql = this.makeInsert(sessao);
					
					st = db.createStatement();
					st.execute(sql);
					
					String urlLogando = "";
					try{
						urlLogando = "(" + SinedUtil.getUrlWithoutContext() + ") ";
					} catch (Exception e) {
					}
					
					System.out.println(urlLogando + usuario.getCdpessoa() + " - " + usuario.getNome() + " deslogando...");
				}
								
			}
			
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(st != null){
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
			if(db != null){
				try {
					db.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	private String makeInsert(Sessao sessao) {
		String sql = 
			"INSERT INTO SESSAO (CDSESSAO, CDUSUARIO, DATAHORA, ENTRADA, SID, IP) " +
			"VALUES (nextval('SQ_SESSAO'), " + sessao.getUsuario().getCdpessoa() + ", '" + sessao.getDatahora().toString() + "', false, '" + sessao.getSid() + "', "+(sessao.getIp() != null ? "'" + sessao.getIp() + "'" : "null")+")";
		
		
		return sql;
	}
	
	private String makeUpdatePedidovendasincronizacao(Integer cdpedidovendasincronizacao) {
		String sql = "";
		
		if(cdpedidovendasincronizacao != null){
			sql = "UPDATE PEDIDOVENDASINCRONIZACAO SET DESMARCARITENS = TRUE WHERE CDPEDIDOVENDASINCRONIZACAO = " + cdpedidovendasincronizacao + ";";
			sql +=  " INSERT INTO PEDIDOVENDASINCRONIZACAOHISTORICO (CDPEDIDOVENDASINCRONIZACAOHISTORICO, CDPEDIDOVENDASINCRONIZACAO, OBSERVACAO, DTALTERA) " +
					" VALUES (nextval('sq_pedidovendasincronizacaohistorico'), " + cdpedidovendasincronizacao + ", 'Update executado para desmarcaritens do pedido, reenviando-o. (Sess�o expirada) ', '" + new Timestamp(System.currentTimeMillis()) + "') ;";
		}		
		return sql;
	}
	

}
