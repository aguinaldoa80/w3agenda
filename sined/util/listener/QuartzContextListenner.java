package br.com.linkcom.sined.util.listener;

import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.jndi.JndiTemplate;

import br.com.linkcom.neo.core.config.AuthenticationConfig;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.job.ArquivoConciliacaoOperadoraJob;
import br.com.linkcom.sined.util.job.ArquivoIntegracaoBridgestoneJob;
import br.com.linkcom.sined.util.job.ArvoreAcessoIntegracaoJob;
import br.com.linkcom.sined.util.job.AtualizacaoAgendamentoservicoJob;
import br.com.linkcom.sined.util.job.AvisoJob;
import br.com.linkcom.sined.util.job.ConsultaEventosCorreiosJob;
import br.com.linkcom.sined.util.job.EnviaBoletoAutomaticoAgendadoJob;
import br.com.linkcom.sined.util.job.EnviaEmailCobrancaJob;
import br.com.linkcom.sined.util.job.EnviaEmailProximaVacinacaoJob;
import br.com.linkcom.sined.util.job.ExportadorArquivoGerencialJob;
import br.com.linkcom.sined.util.job.GenerateJSONEntitiesAndroidJob;
import br.com.linkcom.sined.util.job.GenerateJSONEntitiesPedidoVendaJob;
import br.com.linkcom.sined.util.job.GenerateJSONEntitiesW3ProducaoJob;
import br.com.linkcom.sined.util.job.IntegracaoEcomErroJob;
import br.com.linkcom.sined.util.job.IntegracaoEcomJob;
import br.com.linkcom.sined.util.job.IntegracaoEmporiumCriacaoJob;
import br.com.linkcom.sined.util.job.IntegracaoEmporiumCriacaoNotaJob;
import br.com.linkcom.sined.util.job.IntegracaoEmporiumResultadoJob;
import br.com.linkcom.sined.util.job.IntegracaoFlexDevelopersJob;
import br.com.linkcom.sined.util.job.IntegracaoUsuarioWebService;
import br.com.linkcom.sined.util.job.IteracaoVeterinariaJob;
import br.com.linkcom.sined.util.job.LimpezaCacheWebserviceJob;
import br.com.linkcom.sined.util.job.RemoveReportFilesJob;
import br.com.linkcom.sined.util.job.ResetCacheusuarioofflineJob;
import br.com.linkcom.sined.util.job.SincronizadorEntregaSincronizacaoJob;
import br.com.linkcom.sined.util.job.SincronizadorPedidoVendaSincronizacaoJob;
import br.com.linkcom.sined.util.job.ThreadGC;

public class QuartzContextListenner implements ServletContextListener {

	private Scheduler scheduler;
	private JndiTemplate jndiTemplate;

	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public void contextInitialized(ServletContextEvent arg0) {
		try{
			Locale.setDefault(new Locale("pt", "BR", ""));
			
			// SETANDO O PATH DO LOGIN
			AuthenticationConfig authenticationConfig = Neo.getObject(AuthenticationConfig.class);
			authenticationConfig.setLoginPage("/" + NeoWeb.getApplicationName() + "/pub/Login");
			boolean executeQuartz = isQuartz(); 
			if(executeQuartz){
				scheduler = StdSchedulerFactory.getDefaultScheduler();
				jndiTemplate = new JndiTemplate();
				this.scheduleConsultaEventosCorreios();
				this.scheduleAndroid();
				this.scheduleIntegracaoEcom();
				this.scheduleIntegracaoEmporium();
				this.scheduleArquivoGerencial();
				this.schedulePedidoVenda();			
				this.schedulePedidovendaIntegracaoWms();
				this.scheduleEntregaIntegracaoWms();
				this.scheduleAtualizacaoAgendamentoservico();
				this.scheduleEnvioEmailCobranca();
				this.scheduleRemoveReportFiles();
				this.scheduleLimpezaCacheWebservice();
				this.scheduleArquivoConciliacaoOperadora();
				this.scheduleArvoreAcesso();
				this.scheduleW3Producao();
				this.scheduleFlexDevelopers();
				this.scheduleBridgestone();
				this.scheduleIntegracaoDashboard();
				this.scheduleEnviaEmailVetProxVacina();
				this.scheduleIteracaoVeterinaria();
				this.gc();
				this.scheduleAviso();
				this.scheduleEnvioBoletoAutomaticoAgendado();
				scheduler.start();
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	private void scheduleAviso() {
		try {
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/aviso.properties");
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
				String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTrigger = new CronTrigger("Trigger_Aviso_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
					cronTrigger.setStartTime(new Date());
					
					JobDetail jobAviso = new JobDetail("Job_Aviso_" + banco, Scheduler.DEFAULT_GROUP, AvisoJob.class);
					jobAviso.getJobDataMap().put("nome_banco", banco);
					jobAviso.getJobDataMap().put(banco+"_start_thread", true);
					
//					if(SinedUtil.isAmbienteDesenvolvimento()){
//						SimpleTrigger simpleTrigger = new SimpleTrigger("Trigger_Aviso_Imediato" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
//						JobDetail imediateJob = new JobDetail("Job_Aviso_Imediato_" + banco, Scheduler.DEFAULT_GROUP, AvisoJob.class);
//						imediateJob.getJobDataMap().put("nome_banco", banco);
//						imediateJob.getJobDataMap().put(banco+"_start_thread", true);
//						
//						scheduler.scheduleJob(imediateJob, simpleTrigger);
//					}
					scheduler.scheduleJob(jobAviso, cronTrigger);
				}
			}
		} catch (Exception e) {
			System.out.println("Erro nos avisos.");
			e.printStackTrace();
		}
	}

	private void scheduleIntegracaoDashboard() {
		try {
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/dashboard_integracao.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String minutos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTrigger = new CronTrigger("Trigger_Dashboard_" + banco, Scheduler.DEFAULT_GROUP, "0  0/" + minutos + " * * * ?");
					cronTrigger.setStartTime(new Date());
					
					JobDetail jobIntegracaoUsuarioWebService = new JobDetail("Job_Dashboard_"+ banco, Scheduler.DEFAULT_GROUP, IntegracaoUsuarioWebService.class);
					scheduler.scheduleJob(jobIntegracaoUsuarioWebService, cronTrigger);			
				
				}
			}

			
		} catch(Exception e){
			System.out.println("Erro no agendamento das rotinas.");
			e.printStackTrace();
		}  
	}

	private boolean haveDatasource(String banco){
		try{
			jndiTemplate.lookup("java:/" + banco, DataSource.class);
			return true;
		} catch (Exception e) {
//			e.printStackTrace();
			return false;
		}
	}
	
	private void scheduleLimpezaCacheWebservice(){
		try{
			CronTrigger cronTrigger1 = new CronTrigger("trigger_limpeza_cache_webservice_1", Scheduler.DEFAULT_GROUP, "0 0 12 * * ?");
			cronTrigger1.setStartTime(new Date());
			
			CronTrigger cronTrigger2 = new CronTrigger("trigger_limpeza_cache_webservice_2", Scheduler.DEFAULT_GROUP, "0 0 19 * * ?");
			cronTrigger2.setStartTime(new Date());
			
			JobDetail job1 = new JobDetail("Job_Limpeza_Cache_1", Scheduler.DEFAULT_GROUP, LimpezaCacheWebserviceJob.class);
			JobDetail job2 = new JobDetail("Job_Limpeza_Cache_2", Scheduler.DEFAULT_GROUP, LimpezaCacheWebserviceJob.class);
			
			scheduler.scheduleJob(job1, cronTrigger1);
			scheduler.scheduleJob(job2, cronTrigger2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void scheduleIntegracaoEcom() {
		try{
//			if(!SinedUtil.isAmbienteDesenvolvimento()){
				InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/integracao_ecom.properties");
		
				Properties properties = new Properties();
				properties.load(inputstream);
			
				for (Object key : properties.keySet()){
					String banco = (String) key;
					String minutos = properties.getProperty(banco);
					
					if(this.haveDatasource(banco)){
						SimpleTrigger trigger_erro = new SimpleTrigger("trigger_integracaoecom_erro_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, SimpleTrigger.REPEAT_INDEFINITELY, 1000 * 60 * 5/* * 60*/);
						
						JobDetail job_erro = new JobDetail("Job_Integracao_Ecom_Erro_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEcomErroJob.class);
						
						CronTrigger cronTrigger = new CronTrigger("trigger_integracaoecom_" + banco, Scheduler.DEFAULT_GROUP, "0  0/" + minutos + " * * * ?");
						cronTrigger.setStartTime(new Date());
						
						JobDetail job = new JobDetail("Job_Integracao_Ecom_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEcomJob.class);
						
						scheduler.scheduleJob(job, cronTrigger);
						scheduler.scheduleJob(job_erro, trigger_erro);
					}
				}
//			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	private void scheduleRemoveReportFiles() {
		try{		
			CronTrigger cronTrigger = new CronTrigger("trigger_remove_report_files", Scheduler.DEFAULT_GROUP, "0 0 0 * * ?");
			cronTrigger.setStartTime(new Date());
			
			JobDetail job = new JobDetail("Job_Remove_Report_Files", Scheduler.DEFAULT_GROUP, RemoveReportFilesJob.class);
			
			scheduler.scheduleJob(job, cronTrigger);
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	private void scheduleEnvioEmailCobranca() {
		try{		
			
			String horario = "9:00";
			if(SinedUtil.isAmbienteDesenvolvimento()){
				horario = InitialContext.doLookup("PENDENCIA_FINANCEIRA_HORARIOEMAIL");
				if(horario == null || horario.trim().equals("")){
					return;
				}
			} 
			
			String hora = StringUtils.substringBefore(horario, ":");
			String minuto = StringUtils.substringAfter(horario, ":");
			
			CronTrigger cronTrigger = new CronTrigger("trigger_cobranca_email", Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
			cronTrigger.setStartTime(new Date());
			
			JobDetail job = new JobDetail("Job_Email_Cobranca", Scheduler.DEFAULT_GROUP, EnviaEmailCobrancaJob.class);
			
			scheduler.scheduleJob(job, cronTrigger);
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}

	private void scheduleAtualizacaoAgendamentoservico() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/agendamentoservico_atualizacao.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String minutos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTrigger = new CronTrigger("trigger_agendamentoservico_" + banco, Scheduler.DEFAULT_GROUP, "0 0/" + minutos + " * * * ?");
					cronTrigger.setStartTime(new Date());
					
					JobDetail jobExportaArquivo = new JobDetail("Job_Atualizacao_Agendamentoservico_" + banco, Scheduler.DEFAULT_GROUP, AtualizacaoAgendamentoservicoJob.class);
					jobExportaArquivo.getJobDataMap().put("nome_banco", banco);
					
					scheduler.scheduleJob(jobExportaArquivo, cronTrigger);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	private void scheduleIntegracaoEmporium() {
		try{
//			if(!SinedUtil.isAmbienteDesenvolvimento()){
				InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/integracao_emporium.properties");
		
				Properties properties = new Properties();
				properties.load(inputstream);
				
				for (Object key : properties.keySet()){
					String banco = (String) key;
					String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
					String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
					
					if(this.haveDatasource(banco)){
						Date dataAtual = SinedDateUtils.currentDate();
						
//						## CRIA��O DOS ARQUIVOS ##
						CronTrigger cronTrigger1 = new CronTrigger("trigger_emporium1_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
						cronTrigger1.setStartTime(dataAtual);
						
						JobDetail jobIntegracaoEmporium1 = new JobDetail("Job_Integracao_Emporium1_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEmporiumCriacaoJob.class);
						jobIntegracaoEmporium1.getJobDataMap().put("job_name", "Job_Integracao_Emporium1_" + banco);
						
//						## BUSCA DOS ARQUIVOS PENDENTES E PROCESSAMENTO ##
						CronTrigger cronTrigger2 = new CronTrigger("trigger_emporium2_" + banco, Scheduler.DEFAULT_GROUP, "0 0/1 * * * ?");
						cronTrigger2.setStartTime(SinedDateUtils.addMinutesData(dataAtual, 1));
						
						JobDetail jobIntegracaoEmporium2 = new JobDetail("Job_Integracao_Emporium2_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEmporiumResultadoJob.class);
						jobIntegracaoEmporium2.getJobDataMap().put("job_name", "Job_Integracao_Emporium2_" + banco);
						
//						## CRIA NOTAS COM BASE NOS XML'S DOS CUPONS ##
						CronTrigger cronTrigger3 = new CronTrigger("trigger_emporium3_" + banco, Scheduler.DEFAULT_GROUP, "0/2 * * * * ?");
						cronTrigger3.setStartTime(SinedDateUtils.addMinutesData(dataAtual, 1));
						
						JobDetail jobIntegracaoEmporium3 = new JobDetail("Job_Integracao_Emporium3_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEmporiumCriacaoNotaJob.class);
						jobIntegracaoEmporium3.getJobDataMap().put("job_name", "Job_Integracao_Emporium3_" + banco);
						
//						## AGENDAMENTO ##
						scheduler.scheduleJob(jobIntegracaoEmporium1, cronTrigger1);
						scheduler.scheduleJob(jobIntegracaoEmporium2, cronTrigger2);
						scheduler.scheduleJob(jobIntegracaoEmporium3, cronTrigger3);
					}
				}
//			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	private void scheduleArquivoGerencial() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/exportacao_gerencial.properties");
	
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
				String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTrigger1 = new CronTrigger("trigger_banco_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
					cronTrigger1.setStartTime(new Date());
					
					JobDetail jobExportaArquivo = new JobDetail("Job_Exportacao_Gerencial_" + banco, Scheduler.DEFAULT_GROUP, ExportadorArquivoGerencialJob.class);
					jobExportaArquivo.getJobDataMap().put("nome_banco", banco);
					
					scheduler.scheduleJob(jobExportaArquivo, cronTrigger1);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
 
	private void schedulePedidoVenda() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/pedido_venda_offline.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String minutos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTriggerPV = new CronTrigger("trigger_pedidovenda_" + banco, Scheduler.DEFAULT_GROUP, "0 0/" + minutos + " * * * ?");
					cronTriggerPV.setStartTime(new Date());
					
					CronTrigger cronTriggerResetCache = new CronTrigger("trigger_Reset_Cacheusuariooffline_" + banco, Scheduler.DEFAULT_GROUP, "0 0 0 * * ?");
					cronTriggerPV.setStartTime(new Date());
					
					JobDetail jobGenerateJSONPedidoVenda = new JobDetail("Job_Pedido_Venda_" + banco, Scheduler.DEFAULT_GROUP, GenerateJSONEntitiesPedidoVendaJob.class);
					jobGenerateJSONPedidoVenda.getJobDataMap().put("nome_banco", banco);
					jobGenerateJSONPedidoVenda.getJobDataMap().put(banco+"_start_thread", true);
	
					JobDetail jobGenerateJSONResetCache = new JobDetail("Job_Reset_Cacheusuariooffline_" + banco, Scheduler.DEFAULT_GROUP, ResetCacheusuarioofflineJob.class);
					jobGenerateJSONResetCache.getJobDataMap().put("nome_banco", banco);
					
					scheduler.scheduleJob(jobGenerateJSONPedidoVenda, cronTriggerPV);
					scheduler.scheduleJob(jobGenerateJSONResetCache, cronTriggerResetCache);
				}
			}
		} catch(Exception e){
//			e.printStackTrace();
		}  
	}  

	private void schedulePedidovendaIntegracaoWms(){
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/pedido_venda_sincronizacao.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String segundos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTriggerPV = new CronTrigger("trigger_pedidovendasincronizacao_" + banco, Scheduler.DEFAULT_GROUP, "0/" + segundos + " * * * * ?");
					cronTriggerPV.setStartTime(new Date());
					
					JobDetail jobSincronizadorPedidoVendaSincronizacao = new JobDetail("Job_Sincronizacao_Pedido_Venda_" + banco, Scheduler.DEFAULT_GROUP, SincronizadorPedidoVendaSincronizacaoJob.class);
					jobSincronizadorPedidoVendaSincronizacao.getJobDataMap().put("nome_banco", banco);
					
					scheduler.scheduleJob(jobSincronizadorPedidoVendaSincronizacao, cronTriggerPV);
				}
			}
		} catch(Exception e){
//			e.printStackTrace();
		}  
	}
	
	private void scheduleEntregaIntegracaoWms(){
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/entrega_sincronizacao.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String segundos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTriggerPV = new CronTrigger("trigger_entregasincronizacao_" + banco, Scheduler.DEFAULT_GROUP, "0/" + segundos + " * * * * ?");
					cronTriggerPV.setStartTime(new Date());
					
					JobDetail jobSincronizadorEntregaSincronizacao = new JobDetail("Job_Sincronizacao_Entrega_" + banco, Scheduler.DEFAULT_GROUP, SincronizadorEntregaSincronizacaoJob.class);
					jobSincronizadorEntregaSincronizacao.getJobDataMap().put("nome_banco", banco);
					
					scheduler.scheduleJob(jobSincronizadorEntregaSincronizacao, cronTriggerPV);
				}
			}
		} catch(Exception e){
//			e.printStackTrace();
		}  
	}
	
	private void scheduleAndroid() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/android.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
				String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
				
				if(this.haveDatasource(banco)){
					CronTrigger scheduleTrigger = new CronTrigger("trigger_android_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
					scheduleTrigger.setStartTime(new Date());
					
					JobDetail scheduleJob = new JobDetail("Job_Android_" + banco, Scheduler.DEFAULT_GROUP, GenerateJSONEntitiesAndroidJob.class);
					scheduleJob.getJobDataMap().put("nome_banco", banco);
					scheduleJob.getJobDataMap().put(banco+"_start_thread", true);
					
					SimpleTrigger simpleTrigger = new SimpleTrigger("trigger_android_imediato_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
					
					JobDetail imediateJob = new JobDetail("Job_Android_Imediato_" + banco, Scheduler.DEFAULT_GROUP, GenerateJSONEntitiesAndroidJob.class);
					imediateJob.getJobDataMap().put("nome_banco", banco);
					imediateJob.getJobDataMap().put(banco+"_start_thread", true);
					
					scheduler.scheduleJob(scheduleJob, scheduleTrigger);
					scheduler.scheduleJob(imediateJob, simpleTrigger);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}  
	} 
	
	
	private void scheduleArquivoConciliacaoOperadora() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/arquivoconciliacaooperadora.properties");
	
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String segundos = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					Date dataAtual = SinedDateUtils.currentDate();
					
					SimpleTrigger simpleTrigger = new SimpleTrigger("trigger_arquivo_conciliacao_operadora_" + banco, Scheduler.DEFAULT_GROUP, SimpleTrigger.REPEAT_INDEFINITELY, 1000*Long.parseLong(segundos));
//					simpleTrigger.setStartTime(SinedDateUtils.addMinutesData(dataAtual, 1));
					simpleTrigger.setStartTime(dataAtual);
	
					JobDetail jobArquivoConciliacaoOperadora = new JobDetail("Job_Arquivo_Conciliacao_Operadora_" + banco, Scheduler.DEFAULT_GROUP, ArquivoConciliacaoOperadoraJob.class);
					jobArquivoConciliacaoOperadora.getJobDataMap().put("job_name", "Job_Arquivo_Conciliacao_Operadora_" + banco);
					jobArquivoConciliacaoOperadora.getJobDataMap().put("nome_banco", banco);

					scheduler.scheduleJob(jobArquivoConciliacaoOperadora, simpleTrigger);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void scheduleArvoreAcesso() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/arvoreacesso_integracao.properties");
	
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				
				if(this.haveDatasource(banco)){
					Date dataAtual = SinedDateUtils.currentDate();
					
					CronTrigger cronTrigger = new CronTrigger("trigger_arvoreacessointegracao_" + banco, Scheduler.DEFAULT_GROUP, "1 * * * * ?");
					cronTrigger.setStartTime(SinedDateUtils.addMinutesData(dataAtual, 1));
	
					JobDetail jobAA = new JobDetail("Job_ArvoreAcesso_" + banco, Scheduler.DEFAULT_GROUP, ArvoreAcessoIntegracaoJob.class);
					jobAA.getJobDataMap().put("job_name", "Job_ArvoreAcesso_" + banco);
					jobAA.getJobDataMap().put("nome_banco", banco);

					scheduler.scheduleJob(jobAA, cronTrigger);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void scheduleW3Producao(){
		try {
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/w3producao.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
				String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
				
				if(this.haveDatasource(banco)){
					CronTrigger scheduleTrigger = new CronTrigger("trigger_w3producao_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
					scheduleTrigger.setStartTime(new Date());
					
					JobDetail scheduleJob = new JobDetail("Job_W3Producao_" + banco, Scheduler.DEFAULT_GROUP, GenerateJSONEntitiesW3ProducaoJob.class);
					scheduleJob.getJobDataMap().put("nome_banco", banco);
					scheduleJob.getJobDataMap().put(banco+"_start_thread", true);
					
					SimpleTrigger simpleTrigger = new SimpleTrigger("trigger_w3producao_imediato_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
					
					JobDetail imediateJob = new JobDetail("Job_W3Producao_Imediato_" + banco, Scheduler.DEFAULT_GROUP, GenerateJSONEntitiesW3ProducaoJob.class);
					imediateJob.getJobDataMap().put("nome_banco", banco);
					imediateJob.getJobDataMap().put(banco+"_start_thread", true);
					
					scheduler.scheduleJob(scheduleJob, scheduleTrigger);
					scheduler.scheduleJob(imediateJob, simpleTrigger);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void scheduleFlexDevelopers(){
		try {
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/flex_developers.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String minuto = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger scheduleTrigger = new CronTrigger("trigger_flexdevelopers_" + banco, Scheduler.DEFAULT_GROUP, "0 0/"+minuto+" * 1/1 * ? *");
					scheduleTrigger.setStartTime(new Date());
					
					JobDetail scheduleJob = new JobDetail("Job_FlexDevelopers_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoFlexDevelopersJob.class);
					scheduleJob.getJobDataMap().put("nome_banco", banco);
					scheduleJob.getJobDataMap().put(banco+"_start_thread", true);
					
					SimpleTrigger simpleTrigger = new SimpleTrigger("trigger_flexdevelopers_imediato_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
					
					JobDetail imediateJob = new JobDetail("Job_FlexDevelopers_Imediato_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoFlexDevelopersJob.class);
					imediateJob.getJobDataMap().put("nome_banco", banco);
					imediateJob.getJobDataMap().put(banco+"_start_thread", true);
					
					scheduler.scheduleJob(scheduleJob, scheduleTrigger);
					scheduler.scheduleJob(imediateJob, simpleTrigger);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void scheduleBridgestone() {
		try{
			InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/integracao_bridgestone.properties");
	
			Properties properties = new Properties();
			properties.load(inputstream);
			
			for (Object key : properties.keySet()){
				String banco = (String) key;
				String min = properties.getProperty(banco);
				
				if(this.haveDatasource(banco)){
					CronTrigger cronTrigger = new CronTrigger("trigger_bridgestone_" + banco, Scheduler.DEFAULT_GROUP, "0 0/"+min+" * 1/1 * ? *");
					cronTrigger.setStartTime(new Date());
	
					JobDetail jobBridgestone = new JobDetail("Job_Bridgestone_" + banco, Scheduler.DEFAULT_GROUP, ArquivoIntegracaoBridgestoneJob.class);
					jobBridgestone.getJobDataMap().put("job_name", "Job_Bridgestone_" + banco);
					jobBridgestone.getJobDataMap().put("nome_banco", banco);

					scheduler.scheduleJob(jobBridgestone, cronTrigger);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void scheduleEnviaEmailVetProxVacina (){
		try {
			if(!SinedUtil.isAmbienteDesenvolvimento()){
				InputStream inputStream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/enviaemail_proximavacina_veterinaria.properties");
				
				Properties properties = new Properties();
				properties.load(inputStream);
				
				for(Object key : properties.keySet()){
					String banco = (String) key;
					String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
					String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
					
					if(this.haveDatasource(banco)){
						Date dataAtual = SinedDateUtils.currentDate();
						
						CronTrigger cronTrigger = new CronTrigger("trigger_vetproxvacina_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
						cronTrigger.setStartTime(dataAtual);
						
						JobDetail jobDetailVetProxVacina = new JobDetail("Job_vetproxvacina_" +	banco, Scheduler.DEFAULT_GROUP, EnviaEmailProximaVacinacaoJob.class);
						jobDetailVetProxVacina.getJobDataMap().put("job_name", "Job_vetproxvacina_" +	banco);
						
//						SimpleTrigger cronTrigger = new SimpleTrigger("trigger_vetproxvacina_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
						
						scheduler.scheduleJob(jobDetailVetProxVacina,cronTrigger);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void scheduleIteracaoVeterinaria(){
		try {
			InputStream inputStream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/iteracao_veterinaria.properties");
			
			Properties properties = new Properties();
			properties.load(inputStream);
			
			for (Object key: properties.keySet()){
				String banco = (String) key;
				
				if (this.haveDatasource(banco)){
					//Cada hora
					final String cronExpression = "0 0 0/1 * * ?";
					
					//Cada minuto
//					final String cronExpression = "0 0/1 * * * ?";
					
					CronTrigger cronTrigger = new CronTrigger("trigger_iteracao_veterinaria_" + banco, Scheduler.DEFAULT_GROUP, cronExpression);
					cronTrigger.setStartTime(SinedDateUtils.currentDate());
					
					JobDetail jobDetail = new JobDetail("Job_iteracao_veterinaria_" + banco, Scheduler.DEFAULT_GROUP, IteracaoVeterinariaJob.class);
					
					scheduler.scheduleJob(jobDetail, cronTrigger);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void gc(){
		new ThreadGC().start();
	}
	
	private void scheduleEnvioBoletoAutomaticoAgendado() {		
		try {
			boolean executeQuartz = isQuartz(); 
			if(executeQuartz){
				InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/envioBoletoAgendado.properties");
				Properties properties = new Properties();
				properties.load(inputstream);
				
				for (Object key : properties.keySet()){
					String banco = (String) key;
					String hora = StringUtils.substringBefore(properties.getProperty(banco), ":");
					String minuto = StringUtils.substringAfter(properties.getProperty(banco), ":");
					
					if(this.haveDatasource(banco)){
						CronTrigger cronTrigger = new CronTrigger("Trigger_Email_Envio_Boleto_Agendado_" + banco, Scheduler.DEFAULT_GROUP, "0 " + minuto + " " + hora + " * * ?");
						cronTrigger.setStartTime(new Date());
						
						JobDetail jobAviso = new JobDetail("Job_Email_Envio_Boleto_Agendado_" + banco, Scheduler.DEFAULT_GROUP, EnviaBoletoAutomaticoAgendadoJob.class);
						jobAviso.getJobDataMap().put("nome_banco", banco);
						jobAviso.getJobDataMap().put(banco+"_start_thread", true);
						
						if(SinedUtil.isAmbienteDesenvolvimento()){
							SimpleTrigger simpleTrigger = new SimpleTrigger("Trigger_Email_Envio_Boleto_Agendado_Imediato" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, 0, 0L);
							JobDetail imediateJob = new JobDetail("Job_Email_Envio_Boleto_Agendado_Imediato_" + banco, Scheduler.DEFAULT_GROUP, EnviaBoletoAutomaticoAgendadoJob.class);
							imediateJob.getJobDataMap().put("nome_banco", banco);
							imediateJob.getJobDataMap().put(banco+"_start_thread", true);
							
							scheduler.scheduleJob(imediateJob, simpleTrigger);
						}
						
						scheduler.scheduleJob(jobAviso, cronTrigger);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Erro nos avisos.");
			e.printStackTrace();
		}
	}
	
	private void scheduleConsultaEventosCorreios() {
		try {
			InputStream inputStream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/consulta_eventos_correios.properties");
			
			Properties properties = new Properties();
			properties.load(inputStream);
			
			for (Object key: properties.keySet()){
				String banco = (String) key;
				
				if (this.haveDatasource(banco)){
					//Cada hora
					final String cronExpression = "0 0 10,14,18 * * ?";
					//final String cronExpression = "0 * * * * ?";
					
					//Cada minuto
//					final String cronExpression = "0 0/1 * * * ?";
					
					CronTrigger cronTrigger = new CronTrigger("trigger_consulta_eventos_correios_" + banco, Scheduler.DEFAULT_GROUP, cronExpression);
					cronTrigger.setStartTime(SinedDateUtils.currentDate());
					
					JobDetail jobDetail = new JobDetail("Job_consulta_eventos_correios_" + banco, Scheduler.DEFAULT_GROUP, ConsultaEventosCorreiosJob.class);
					jobDetail.getJobDataMap().put("nome_banco", banco);
					jobDetail.getJobDataMap().put(banco+"_start_thread", true);
					
					scheduler.scheduleJob(jobDetail, cronTrigger);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private boolean isQuartz(){
		try{
			String isQuartz = InitialContext.doLookup("EXECUTAR_QUARTZ");					
			if(isQuartz.equalsIgnoreCase("false")){
				return false;
			}else {
				return true;
			}
		}catch (Exception e) {
			return  true;
		}
	}
}