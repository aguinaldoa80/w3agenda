package br.com.linkcom.sined.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Licenca;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LicencaService;

public class LicenseManager {
			
	/**
	 * Converte o inteiro para um hexadecimal de dois d�gitos completado com zero � esquerda
	 * @param i
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static String hex(Integer i){
		String hexString = Integer.toHexString(i).toUpperCase();
		while (hexString.length() < 2)
			hexString = "0" + hexString;
		return  hexString;
	}

	/**
	 * Criptografa uma string
	 * @param string
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static String cript(String string){
	    Random random = new Random();
	    Integer constanteAsc;
	    String chave = "155";
	    Integer chavePos = 0;
	    Integer chaveLen = chave.length();

	    Integer offset = random.nextInt(256);
	    String destino = hex(offset);
	    for (int i = 0; i < string.length(); i++) {

	      constanteAsc = ((int)(string.charAt(i)) + offset) % 255;
	      if (chavePos < chaveLen)
	        ++chavePos;
	      else
	        chavePos = 1;

	      constanteAsc = constanteAsc ^ (int)(chave.charAt(chavePos-1));
	      destino += hex(constanteAsc);
	      offset = constanteAsc;
	    }

	    return destino;
	}

	/**
	 * Descriptografa uma string
	 * @param string
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static String decript(String string){
	  String s = string.substring(0, 2);
	  Integer offset = Integer.parseInt(s, 16);
	  Integer constantePos = 2;
	  Integer constanteAsc;

	  String chave = "155";
	  Integer chavePos = 0;
	  Integer chaveLen = chave.length();
	  Integer tmpConstanteAsc;
	  String destino = "";

	  do{
		  s = string.substring(constantePos, constantePos + 2);
		  constanteAsc = Integer.parseInt(s, 16);
		  if (chavePos < chaveLen)
			  ++chavePos;
		  else
			  chavePos = 1;
		  tmpConstanteAsc = constanteAsc ^ (int)(chave.charAt(chavePos - 1));
		  if (tmpConstanteAsc <= offset)
			  tmpConstanteAsc = 255 + tmpConstanteAsc - offset;
		  else
			  tmpConstanteAsc = tmpConstanteAsc - offset;
		  destino += (char)(tmpConstanteAsc.intValue());
		  offset = constanteAsc;
		  constantePos = constantePos + 2;
	  } while (constantePos < string.length());
	  return destino;
	}
	
	/**
	 * Retorna uma lista das telas que est�o licenciadas no sistema.
	 * @return
	 * @author Thiago Gon�alves
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getLicencas(){
		
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("W3ERP_LICENCAS");
		if (attribute != null && attribute instanceof List)
			return (List<String>)attribute;
			
		List<Licenca> licencas = LicencaService.getInstance().findAll();
		if (licencas == null || licencas.isEmpty())
			return null;
		
		List<String> lista = new ArrayList<String>();
		for (int i = 0; i < licencas.size(); i++) { 
			Licenca licenca = licencas.get(i);
			lista.add(licenca.getItemdescript());
		}
		NeoWeb.getRequestContext().getSession().setAttribute("W3ERP_LICENCAS", lista);			
		return lista;
	}	
		
	/**
	 * Retorna uma lista com os dados contidos no arquivo
	 * @param arquivo
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static List<String> dadosArquivoLicenca(Arquivo arquivo){

		if (arquivo == null)
			return null;
		
		List<String> lista = new ArrayList<String>();
		ByteArrayInputStream arrayInputStream = null;
		InputStreamReader streamReader = null;
		BufferedReader reader = null; 
		try {
			arrayInputStream = new ByteArrayInputStream(arquivo.getContent());
			streamReader = new InputStreamReader(arrayInputStream);
			reader = new BufferedReader(streamReader); 
			String line = null;		
			while ( (line = reader.readLine()) != null) {
				lista.add(line);
			}						
			if (reader != null)
				reader.close();
			if (streamReader != null)
				streamReader.close();
			if (arrayInputStream != null)
				arrayInputStream.close();			
			
			return lista;		
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Verifica se a lista dos dados de licen�a passados s�o v�lidos
	 * @param lista
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static Boolean validaLicenca(List<String> lista){
		if (lista == null || lista.isEmpty())
			return false;
		
		Long codigo = 0L;
		for (int i = 0; i < lista.size() - 1; i++) {
			String linha = decript(lista.get(i));
			for (int j = 0; j < linha.length(); j++) 
				codigo += linha.codePointAt(j);			
		}
		
		String ultimalinha = decript(lista.get(lista.size() - 1));
				
		return ultimalinha.equals(codigo.toString());
	}
	
	/**
	 * Retorna um Object[] contendo a validade e o nome da empresa do arquivo de licen�as
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static Object[] dadosLicenca(){
		List<String> arquivoLicenca = getLicencas();
		if (arquivoLicenca == null || arquivoLicenca.isEmpty())
			return null;
				 
		try {
			String line = arquivoLicenca.get(0);
			if (line != null){ 
				String[] split = line.split(";");
				if (split.length == 4){
					String empresa = split[2];
					String validade = split[3];
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
					java.util.Date data = dateFormat.parse(validade);
					return new Object[]{empresa, data};
				}
			} 					
		} catch (Exception e) {
		}
		return null;
	}
	
	/**
	 * Verifica se o m�dulo est� licenciado
	 * @param modulo
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static Boolean moduloLicenciado(String modulo){
		return telaLicenciada(modulo);
	}	
	
	/**
	 * Verifica se a tela est� licenciada
	 * @param path
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static Boolean telaLicenciada(String path){
		Boolean verificarLicenca = !SinedUtil.isAmbienteDesenvolvimento();
		
		if (!verificarLicenca)
			return true;
		else {			
			if (path.equals("/sistema/") || 
					path.equals("/sistema/process/arquivolicenca") || 
					path.equals("/sistema/crud/Empresa"))
				return true;
			
			List<String> licencas = getLicencas();
			if (licencas == null || licencas.isEmpty())
				return false;
			Boolean achou = false;
			
			try {
				String empresa = "";
				Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("W3ERP_EMPRESA");
				if (attribute != null && !"".equals(attribute)){
					empresa = (String)attribute;
				} else {				
					empresa = EmpresaService.getInstance().cnpjEmpresaPrincipal();			
					NeoWeb.getRequestContext().getSession().setAttribute("W3ERP_EMPRESA", empresa);
				}
				
				String empresacpf = "";
				Object attribute2 = NeoWeb.getRequestContext().getSession().getAttribute("W3ERP_EMPRESA_CPF");
				if (attribute2 != null && !"".equals(attribute2)){
					empresacpf = (String)attribute2;
				} else {				
					empresacpf = EmpresaService.getInstance().cpfEmpresaPrincipal();			
					NeoWeb.getRequestContext().getSession().setAttribute("W3ERP_EMPRESA_CPF", empresacpf);
				}
				
				for (String line : licencas) {
					if (line.indexOf(path) > 0){
						try{
							String[] split = line.split(";");
							String emp = split[2];
							String val = split[3];
							java.util.Date validade = new SimpleDateFormat("yyyyMMdd").parse(val);
							if ((StringUtils.soNumero(emp).equalsIgnoreCase(StringUtils.soNumero(empresa)) || 
								StringUtils.soNumero(emp).equalsIgnoreCase(StringUtils.soNumero(empresacpf))) && 
								validade.after(new java.util.Date()))
								achou = true;
						} catch (Exception e) {
						}
						break;
					}
				}				
			} catch (Exception e) {			
			}
			return achou; 
		}
	}
	
	public static String statusLicenca(){
		String msg = "";
		Object[] dadosLicenca = dadosLicenca();
		if (dadosLicenca != null){
			String empresa = (String)dadosLicenca[0];
			String cnpjprincipal = EmpresaService.getInstance().cnpjEmpresaPrincipal();
			String cpfprincipal = EmpresaService.getInstance().cpfEmpresaPrincipal();
			
			if (!StringUtils.soNumero(empresa).equalsIgnoreCase(StringUtils.soNumero(cnpjprincipal)) &&
					!StringUtils.soNumero(empresa).equalsIgnoreCase(StringUtils.soNumero(cpfprincipal))){
				msg = "ATEN��O!!! Empresa principal diferente da licenciada.";
				return msg;
			}
			
			java.util.Date data = (java.util.Date)dadosLicenca[1];
			
			Long dias = SinedDateUtils.calculaDiferencaDias(new java.sql.Date(new java.util.Date().getTime()),
															new java.sql.Date(data.getTime()));
			
			if (dias < 0)
				msg = "ATEN��O!!! Licen�a expirada.";
			else if (dias < 10L)
				msg = "ATEN��O!!! Licen�a v�lida at� " + (new SimpleDateFormat("dd/MM/yy").format(data)) + "."; 
		} else
			msg = "ATEN��O!!! Arquivo de licen�as inexistente.";
		return msg;
	}
}

