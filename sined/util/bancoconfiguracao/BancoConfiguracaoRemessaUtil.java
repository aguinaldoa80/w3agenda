package br.com.linkcom.sined.util.bancoconfiguracao;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoCampo;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.BancoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.ContaBancariaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.DocumentoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.GeralVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.MovimentacaoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.PessoaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.RemessaVO;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoPreencherEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.utils.StringUtils;

public class BancoConfiguracaoRemessaUtil {

	private ScriptEngine engine;
	private List<String> listaErro;
		
	public String gerarDadosArquivoRemessaConfiguravel(List<BancoConfiguracaoRemessaSegmento> segmentos, RemessaVO remessaVO){
		listaErro = new ArrayList<String>();		
		StringBuffer remessa = new StringBuffer();
		
		setEngine(remessaVO);	
		inicializaGeralVO(segmentos, remessaVO);
		setEngine(remessaVO);		
				
		Iterator<BancoConfiguracaoRemessaSegmento> it = segmentos.iterator();		
		while (it.hasNext()){
			BancoConfiguracaoRemessaSegmento segmento = it.next();			
			
			//Verifica a condi��o para gera��o do segmento
			if (segmento.getCondicao() != null && !"".equals(segmento.getCondicao())){
				Object resultado = eval(segmento.getCondicao());
				if (Boolean.class.equals(resultado.getClass())){
					if (!Boolean.TRUE.equals(((Boolean) resultado)))
						continue;
				}
			}
			
			if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento())){
				
				List<BancoConfiguracaoSegmento> listaDetalhe = new ArrayList<BancoConfiguracaoSegmento>();
				listaDetalhe.add(segmento.getBancoConfiguracaoSegmento());
				
				//Percorre os pr�ximos registros para encontrar mais detalhes
				while (it.hasNext()){
					segmento = it.next();
					
					//Verifica a condi��o para gera��o do segmento
					if (segmento.getCondicao() != null && !"".equals(segmento.getCondicao())){
						Object resultado = eval(segmento.getCondicao());
						if (Boolean.class.equals(resultado.getClass())){
							if (!Boolean.TRUE.equals(((Boolean) resultado)))
								continue;
						}
					}
					
					if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento())){
						listaDetalhe.add(segmento.getBancoConfiguracaoSegmento());
					} else
						break;
				}		
				
				if (listaDetalhe.size() > 0){					
					if (remessaVO != null && remessaVO.getListaDocumentoVO() != null && remessaVO.getListaDocumentoVO().size() > 0){			
						for (DocumentoVO documentoVO : remessaVO.getListaDocumentoVO()){
							setEngine(documentoVO);
							for (BancoConfiguracaoSegmento bcs : listaDetalhe){
								preencheSegmento(remessa, bcs);
								atualizaSequencial(remessaVO.getGeralVO(), bcs.getTipoSegmento());
							}
						}			
					}else if (remessaVO != null && remessaVO.getListaMovimentacaoVO() != null && remessaVO.getListaMovimentacaoVO().size() > 0){			
						for (MovimentacaoVO movimentacaoVO: remessaVO.getListaMovimentacaoVO()){
							setEngine(movimentacaoVO);
							for (BancoConfiguracaoSegmento bcs : listaDetalhe){
								preencheSegmento(remessa, bcs);
								atualizaSequencial(remessaVO.getGeralVO(), bcs.getTipoSegmento());
							}
						}			
					} else {
						this.addErro("Nenhum documento encontrado com o filtro informado.");
					}
				} else {
					this.addErro("Deve existir pelo menos um segmento de detalhe no arquivo de remessa.");
				}
			}
			
			if (BancoConfiguracaoTipoSegmentoEnum.HEADER.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento()) ||
					BancoConfiguracaoTipoSegmentoEnum.TRAILER.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento())){ 
				preencheSegmento(remessa, segmento.getBancoConfiguracaoSegmento());
				atualizaSequencial(remessaVO.getGeralVO(), segmento.getBancoConfiguracaoSegmento().getTipoSegmento());
			}
		}
		
		remessaVO.setListaErro(listaErro);		
		
		//Remove a �ltima quebra de linha
		//Tem Banco que requer uma quebra na �ltima linha e outros n�o. Ent�o por padr�o, a �ltima linha n�o vai com \r\n.
		//Caso o Banco requerer quebra na �ltima linha, adicionar no Segmento o '\r\n' manualmente. Exemplo: geral.sequencial + '\r\n'
		if (remessa.length() > 1 && remessa.substring(remessa.length()-2).equals("\r\n")){
			if(!(remessa.substring(remessa.length()-3, remessa.length()-2).charAt(0) == "\r".charAt(0))){
				return remessa.substring(0, remessa.length()-2);
			}
		}

		return remessa.toString();
	}
	
	public void preencheSegmento(StringBuffer remessa, BancoConfiguracaoSegmento segmento) {		
		for (BancoConfiguracaoSegmentoCampo bcsc : segmento.getListaBancoConfiguracaoSegmentoCampo()){
			if (bcsc.getCampo() != null){
				String valor = null;
				
				Object resultado = !"\\n".equals(bcsc.getCampo()) ? eval(bcsc.getCampo()) : "\r\n\r\n\r\n";
				if (resultado != null){
					if (Money.class.equals(resultado.getClass()))
						valor = new Long(((Money) resultado).toLong()).toString();
					else if (String.class.equals(resultado.getClass())){
						valor = (String) resultado;
						if (valor.contains("null"))
							valor = valor.replace("null", "");
					} else if (Integer.class.equals(resultado.getClass()))
						valor = ((Integer) resultado).toString();
					else if (Long.class.equals(resultado.getClass()))
						valor = ((Long) resultado).toString();						
					else if (Cnpj.class.equals(resultado.getClass()))
						valor = ((Cnpj) resultado).getValue();
					else if (Cpf.class.equals(resultado.getClass()))
						valor = ((Cpf) resultado).getValue();
					else if (Date.class.equals(resultado.getClass())){
						String formato = (bcsc.getFormatoData() != null && !"".equals(bcsc.getFormatoData()) ? bcsc.getFormatoData() : "ddMMyy");
						SimpleDateFormat sdf = new SimpleDateFormat(formato);
						valor = sdf.format(((Date) resultado));						
					} else if (Time.class.equals(resultado.getClass())){
						String formato = (bcsc.getFormatoData() != null && !"".equals(bcsc.getFormatoData()) ? bcsc.getFormatoData() : "HHmmss");
						SimpleDateFormat sdf = new SimpleDateFormat(formato);
						valor = sdf.format(((Time) resultado));	
					} else
						valor = resultado.toString();						
				}			
				
				if (valor != null && !"".equals(valor)){
					remessa.append(StringUtils.stringCheia(valor, (bcsc.isCompletarZero() ? "0" : " "), bcsc.getTamanho(), bcsc.isCompletarDireita()));
				} else if (bcsc.isObrigatorio()){
					this.addErro("O campo " + (bcsc.getDescricao() != null ? bcsc.getDescricao() : bcsc.getCampo()) + " � obrigat�rio.");
				} else {
					remessa.append(StringUtils.stringCheia("", (BancoConfiguracaoTipoPreencherEnum.ZERO.equals(bcsc.getPreencherCom()) ? "0" : " "), bcsc.getTamanho(), true));
				}				
			} else {
				this.addErro("O campo do segmento banc�rio n�o pode ser nulo.");
			}
		}		
		remessa.append("\r\n");
	}
	
	public ScriptEngine getEngine() {
		return engine;
	}
	
	public void setEngine(RemessaVO remessaVO) {
		if (this.engine == null){
			ScriptEngineManager manager = new ScriptEngineManager();
			this.engine = manager.getEngineByName("JavaScript");
		}		
		if (remessaVO != null){	
			if (remessaVO.getGeralVO() != null){
				GeralVO geralVO = remessaVO.getGeralVO();
				this.engine.put("geral", geralVO);
			}
			if (remessaVO.getBancoVO() != null){
				BancoVO bancoVO = remessaVO.getBancoVO();
				this.engine.put("banco", bancoVO);
			}			
			if (remessaVO.getContaBancariaVO() != null){
				ContaBancariaVO contaBancariaVO = remessaVO.getContaBancariaVO();
				this.engine.put("conta", contaBancariaVO);
			}			
			if (remessaVO.getEmpresaVO() != null){
				EmpresaVO empresaVO = remessaVO.getEmpresaVO();
				this.engine.put("empresa", empresaVO);
				this.engine.put("empresadoc", empresaVO);
			}
			
			EmpresaVO empresatitularVO = new EmpresaVO();
			if (remessaVO.getEmpresatitularVO() != null){
				empresatitularVO = remessaVO.getEmpresatitularVO();
			}
			this.engine.put("empresatitular", empresatitularVO);
		}
	}
	
	public void setEngine(DocumentoVO documentoVO) {
		if (this.engine == null){
			ScriptEngineManager manager = new ScriptEngineManager();
			this.engine = manager.getEngineByName("JavaScript");
		}		
		if (documentoVO != null){
			this.engine.put("documento", documentoVO);
		}		
		if (documentoVO.getPessoaVO() != null){
			PessoaVO pessoaVO = documentoVO.getPessoaVO();
			this.engine.put("pessoa", pessoaVO);
		}
		if (documentoVO.getEmpresaDOC() != null){
			EmpresaVO empresaVO = documentoVO.getEmpresaDOC();
			this.engine.put("empresadoc", empresaVO);
		}
		EmpresaVO empresatitularVO = new EmpresaVO();
		if (documentoVO.getEmpresaTITULAR() != null){
			empresatitularVO = documentoVO.getEmpresaTITULAR();
		}
		this.engine.put("empresatitular", empresatitularVO);
	}
	
	public void setEngine(GeralVO geralVO) {
		if (this.engine == null){
			ScriptEngineManager manager = new ScriptEngineManager();
			this.engine = manager.getEngineByName("JavaScript");
		}
		if (geralVO != null)
			this.engine.put("geral", geralVO);
	}

	public void setEngine(MovimentacaoVO movimentacaoVO) {
		if (this.engine == null){
			ScriptEngineManager manager = new ScriptEngineManager();
			this.engine = manager.getEngineByName("JavaScript");
		}		
		
		this.engine.put("movimentacao", movimentacaoVO);
	}
	
	public void addErro(String erro){
		if (!listaErro.contains(erro))
			listaErro.add(erro);
	}
	
	public void inicializaGeralVO(List<BancoConfiguracaoRemessaSegmento> segmentos, RemessaVO remessaVO){
		GeralVO geralVO = remessaVO.getGeralVO();
		
		Integer totalHeader = 0;
		Integer totalDetalhe = 0;
		Integer totalTrailer = 0;
		
		for (BancoConfiguracaoRemessaSegmento segmento : segmentos){
			if (segmento.getCondicao() != null && !"".equals(segmento.getCondicao())){
				Object resultado = eval(segmento.getCondicao());
				if (Boolean.class.equals(resultado.getClass())){
					if (!Boolean.TRUE.equals(((Boolean) resultado)))
						continue;
				}
			}
			
			if (BancoConfiguracaoTipoSegmentoEnum.HEADER.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento()))
				totalHeader++;
			else if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento()))
				totalDetalhe++;
			else if (BancoConfiguracaoTipoSegmentoEnum.TRAILER.equals(segmento.getBancoConfiguracaoSegmento().getTipoSegmento()))
				totalTrailer++;			
		}
		
		Money valorTotal = new Money();
		Money valoratual = new Money();
		Money valoroutrasentidades = new Money();
		Money atualizacaomonetaria = new Money();
		
		Boolean sacadoravalista = false;
		
		if (remessaVO.getListaDocumentoVO()!=null && !remessaVO.getListaDocumentoVO().isEmpty()){
			for (DocumentoVO documentoVO : remessaVO.getListaDocumentoVO()){
				if (documentoVO.getValor() != null)
					valorTotal = valorTotal.add(documentoVO.getValor());
				
				if (documentoVO.getValoratual() != null)
					valoratual = valoratual.add(documentoVO.getValoratual());
				
				if (documentoVO.getValoroutrasentidades() != null)
					valoroutrasentidades = valoroutrasentidades.add(documentoVO.getValoroutrasentidades());
				
				if (documentoVO.getAtualizacaomonetaria() != null)
					atualizacaomonetaria = atualizacaomonetaria.add(documentoVO.getAtualizacaomonetaria());
				
				if(documentoVO.getSacadoravalista() != null && documentoVO.getSacadoravalista()){
					sacadoravalista = true;
				}
			}
		}else if (remessaVO.getListaMovimentacaoVO()!=null && !remessaVO.getListaMovimentacaoVO().isEmpty()){
			for (MovimentacaoVO movimentacaoVO: remessaVO.getListaMovimentacaoVO()){
				if (movimentacaoVO.getValor() != null){
					valorTotal = valorTotal.add(movimentacaoVO.getValor());
				}
			}
		}
		
		int sizeLista = 0;
		
		if (remessaVO.getListaDocumentoVO()!=null && remessaVO.getListaDocumentoVO().size()>0){
			sizeLista = remessaVO.getListaDocumentoVO().size();
		}else if (remessaVO.getListaMovimentacaoVO()!=null && remessaVO.getListaMovimentacaoVO().size()>0){
			sizeLista = remessaVO.getListaMovimentacaoVO().size();
		}
		
		Integer totalLinhas = (totalDetalhe * sizeLista); // total geral sem contar com Header e Trailer.
		Integer totalRegistro = totalHeader + totalLinhas + totalTrailer; //total de todas as linhas do arquivo
		Integer totalLinhasLote = totalRegistro - 2; //total de todas as linhas do arquivo desconsiderando o header inicial e trailer final
		Integer quantidadeDocumento = sizeLista;
		
		geralVO.setSequencial(1);
		geralVO.setSequencialdetalhe(1);
		geralVO.setTotallinha(totalLinhas);
		geralVO.setTotallinhalote(totalLinhasLote);
		geralVO.setTotalregistro(totalRegistro);
		geralVO.setValortotal(valorTotal);
		geralVO.setValoroutrasentidades(valoroutrasentidades);
		geralVO.setValoratual(valoratual);
		geralVO.setAtualizacaomonetaria(atualizacaomonetaria);
		geralVO.setQuantidadedocumento(quantidadeDocumento);
		geralVO.setSacadoravalista(sacadoravalista);
		
		remessaVO.setGeralVO(geralVO);
	}
	
	public void atualizaSequencial(GeralVO geralVO, BancoConfiguracaoTipoSegmentoEnum tipo){
		Integer sequencial = 0;
		
		if (geralVO.getSequencial() != null)
			sequencial = geralVO.getSequencial(); 
		geralVO.setSequencial(++sequencial);
		
		if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(tipo)){
			Integer sequencialDetalhe = 0;
			if (geralVO.getSequencialdetalhe() != null)
				sequencialDetalhe = geralVO.getSequencialdetalhe(); 
			geralVO.setSequencialdetalhe(++sequencialDetalhe);
		}
		
		setEngine(geralVO);
	}
	
	private Object eval(String expressao){
		Object resultado = null;
		try {
			resultado = getEngine().eval(expressao);
			if (resultado != null && SinedException.class.equals(resultado.getClass()))
				throw (SinedException) resultado;
		} catch (ScriptException e) {
			this.addErro("Express�o inv�lida: '" + expressao + "'.");
		} catch (SinedException e) {
			this.addErro(e.getMessage());
		}	
		return resultado;
	}
	
}
