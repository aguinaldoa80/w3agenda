package br.com.linkcom.sined.util.suprimento;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;

@Bean
@Controller(path="/suprimento/process/MaterialCategoriaAutoComplete")
public class MaterialCategoriaAutoCompleteController extends MultiActionController {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}

	@DefaultAction
	public void view(WebRequestContext request, MaterialCategoriaTreeFiltro filtro){
		List<Materialcategoria> listaMaterialcategoria = materialcategoriaService.findAutocompleteTreeView(filtro);
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(listaMaterialcategoria));
	}

	private String criarEstrutura(List<Materialcategoria> listaMaterialcategoria) {
		StringBuilder sb = new StringBuilder();
		
		for (Materialcategoria materialcategoria : listaMaterialcategoria) {
			sb.append(materialcategoria.getDescricaoAutocompleteTreeView() + "|" + materialcategoria.getCdmaterialcategoria() + "|" + materialcategoria.getIdentificador() + "\n");
		}
		
		return sb.toString();
	}
}