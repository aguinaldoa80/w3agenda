package br.com.linkcom.sined.util.suprimento;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;

@Bean
@Controller(path="/suprimento/process/MaterialCategoriaTreeView")
public class MaterialCategoriaTreeController extends MultiActionController {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@DefaultAction
	public ModelAndView view(WebRequestContext request, MaterialCategoriaTreeFiltro filtro){
		String codigo_treeview = criaEstrutura(materialcategoriaService.findForTreeView(filtro), filtro.getUltimoNivel());
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		request.setAttribute("ultimoNivel", filtro.getUltimoNivel());
		return new ModelAndView("direct:/process/materialCategoriaTreeView");
	}
	
	private String criaEstrutura(List<Materialcategoria> listaMaterialcategoria, Boolean ultimoNivel) {
		if (Boolean.TRUE.equals(ultimoNivel)){
			return criaEstruturaUltimoNivel(listaMaterialcategoria);
		}else {
			return criaEstruturaTodosNiveis(listaMaterialcategoria);
		}		
	}
	
	private String criaEstruturaUltimoNivel(List<Materialcategoria> listaMaterialcategoria) {
		StringBuilder sb = new StringBuilder();
		
		for (Materialcategoria materialcategoria: listaMaterialcategoria) {
			if (materialcategoria.getFilhos()!=null && !materialcategoria.getFilhos().isEmpty()){
				sb.append("<li><span>");
				if (materialcategoria.getVmaterialcategoria().getIdentificador() != null){
					sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
					sb.append(" - ");					
				}
				sb.append(materialcategoria.getDescricao());
				sb.append("</span><ul>");
				sb.append(this.criaEstruturaUltimoNivel(materialcategoria.getFilhos()));
				sb.append("</ul></li>");
			}else {
				sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
				sb.append(materialcategoria.getCdmaterialcategoria());
				sb.append(", '");
				if(materialcategoria.getVmaterialcategoria().getIdentificador() != null){
					sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
					sb.append(" - ");					
				}
				sb.append(materialcategoria.getDescricao()!=null ? materialcategoria.getDescricao().replace("'", "\\'") : "");
				sb.append("', '");
				sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
				sb.append("')\">");
				if(materialcategoria.getVmaterialcategoria().getIdentificador() != null){
					sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
					sb.append(" - ");					
				}
				sb.append(materialcategoria.getDescricao());
				sb.append("</span></li>");
			}
		}
		
		return sb.toString();
	}
	
	private String criaEstruturaTodosNiveis(List<Materialcategoria> listaMaterialcategoria) {
		StringBuilder sb = new StringBuilder();
		
		for (Materialcategoria materialcategoria: listaMaterialcategoria) {
			sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
			sb.append(materialcategoria.getCdmaterialcategoria());
			sb.append(", '");
			sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
			sb.append(" - ");
			sb.append(materialcategoria.getDescricao()!=null ? materialcategoria.getDescricao().replace("'", "\\'") : "");
			sb.append("', '");
			sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
			sb.append("')\">");
			sb.append(materialcategoria.getVmaterialcategoria().getIdentificador());
			sb.append(" - ");
			sb.append(materialcategoria.getDescricao());
			sb.append("</span>");
			
			if (materialcategoria.getFilhos()!=null && !materialcategoria.getFilhos().isEmpty()){
				sb.append("<ul>");
				sb.append(this.criaEstruturaTodosNiveis(materialcategoria.getFilhos()));
				sb.append("</ul>");
			}
			
			sb.append("</li>");
		}
		
		return sb.toString();
	}
}