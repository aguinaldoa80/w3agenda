package br.com.linkcom.sined.util.suprimento;


public class MaterialCategoriaTreeFiltro {

	private String propriedade;
	private String q;
	private String whereAtivo = "true";
	private String whereUltimoNivel = "false";
	private Boolean ativo;
	private Boolean ultimoNivel;
	
	public String getPropriedade() {
		return propriedade;
	}
	public String getQ() {
		return q;
	}
	public String getWhereAtivo() {
		return whereAtivo;
	}
	public String getWhereUltimoNivel() {
		return whereUltimoNivel;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Boolean getUltimoNivel() {
		return ultimoNivel;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public void setWhereAtivo(String whereAtivo) {
		this.whereAtivo = whereAtivo;
		if ("true".equalsIgnoreCase(whereAtivo)){
			setAtivo(Boolean.TRUE);
		}else if ("false".equalsIgnoreCase(whereAtivo)){
			setAtivo(Boolean.FALSE);
		}
	}
	public void setWhereUltimoNivel(String whereUltimoNivel) {
		this.whereUltimoNivel = whereUltimoNivel;
		if ("true".equalsIgnoreCase(whereUltimoNivel)){
			setUltimoNivel(Boolean.TRUE);
		}else if ("false".equalsIgnoreCase(whereUltimoNivel)){
			setUltimoNivel(Boolean.FALSE);
		}
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setUltimoNivel(Boolean ultimoNivel) {
		this.ultimoNivel = ultimoNivel;
	}
}