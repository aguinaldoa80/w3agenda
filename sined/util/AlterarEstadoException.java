package br.com.linkcom.sined.util;

public class AlterarEstadoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private int errorCode = 0;

	public AlterarEstadoException() {
		super();
	}

	public AlterarEstadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlterarEstadoException(String message) {
		super(message);
	}

	public AlterarEstadoException(Throwable cause) {
		super(cause);
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public RuntimeException setErrorCode(int errorCode) {
		this.errorCode = errorCode;
		return this;
	}
}
