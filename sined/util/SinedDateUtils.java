package br.com.linkcom.sined.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.geral.service.EmpresaService;

public class SinedDateUtils {
	
	private static DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	private static DateFormat formatPostgre = new SimpleDateFormat("yyyy-MM-dd");
	public static DateFormat formatPostgreHora = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static DateFormat formatExtenso = new SimpleDateFormat("d' de 'MMMM' de 'yyyy");
	public static final Pattern PADRAO_DATA_DDMMYYYY = Pattern.compile("\\d\\d[/-]\\d\\d[/-]\\d\\d\\d\\d"), 
								PADRAO_DATA_YYYYMMDD = Pattern.compile("\\d\\d\\d\\d[/-]\\d\\d[/-]\\d\\d");	
	
	/**
	 * M�todo para transformar da string "dd/MM/yyyy" para java.sql.Date.
	 *
	 * @param strDate
	 * @return
	 * @throws ParseException
	 * @author Rodrigo Freitas
	 */
	public static Date stringToDate(String strDate) throws ParseException{
		return stringToDate(strDate, "dd/MM/yyyy");
	}
	
	public static Date stringToDate(String strDate, String pattern) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat(pattern);  
		Date data = new Date(format.parse(strDate).getTime()); 
		return data;
	}
	
	public static Timestamp stringToTimestamp(String strDate, String pattern) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat(pattern);  
		Timestamp data = new Timestamp(format.parse(strDate).getTime()); 
		return data;
	}
	
	
	/**
	 * Retorna a data passada por par�metro por extenso.
	 *
	 * @param data
	 * @return
	 * @since 12/06/2012
	 * @author Rodrigo Freitas
	 */
	public static String dataExtenso(Date data){
		if(data == null) return "";
		return formatExtenso.format(data);
	}
	
	public static String doubleFormatado(Double valor){
		if(valor != null){
			return new Money(valor).toString();
		}
		return "";
	}
	
	/**
	 * M�todo para obter a diferen�a, em dias, entre duas datas. 
	 *  
	 * @param de
	 * @param ate
	 * @return dias de atraso
	 * @author Andr� Brunelli
	 * @author Fl�vio Tavares
	 */
	public static Long calculaDiferencaDias(Date de, Date ate){		
		long dif = ate.getTime() - de.getTime();
		dif /= 1000 * 60 * 60 * 24;
		return dif;
	}
	
	/**
	 * M�todo para retornar o dia da semana atrav�s de uma data fornecida.
	 * @param date
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String getDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			return "Domingo";
		}else if (cal.get(Calendar.DAY_OF_WEEK) == 2) {
			return "Segunda-Feira";
		}else if (cal.get(Calendar.DAY_OF_WEEK) == 3) {
			return "Ter�a-Feira";
		}else if (cal.get(Calendar.DAY_OF_WEEK) == 4) {
			return "Quarta-Feira";
		}else if (cal.get(Calendar.DAY_OF_WEEK) == 5) {
			return "Quinta-Feira";
		}else if (cal.get(Calendar.DAY_OF_WEEK) == 6) {
			return "Sexta-Feira";
		}else{
			return "S�bado";
		}
	}

	/**
	 * <p>Converte uma data do tipo <code>java.sql.Date</code> para <code>java.util.Calendar</code>,
	 * desprezando as horas. Pode retornar null.
	 * 
	 * @param date
	 * @return Uma data do tipo java.util.Calendar, ou null, se o par�metro <code>date</code> for null.
	 * @author Hugo Ferreira
	 */
	public static Calendar javaSqlDateToCalendar(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		
		return calendar;
	}
	
	public static Date timestampToDate(Timestamp timestamp) {
		if(timestamp == null) return null;		
		return new Date(timestamp.getTime());
	}
	
	public static Timestamp dateToTimestamp(Date data) {
		if(data == null) return null;		
		return new Timestamp(data.getTime());
	}
	
	public static Timestamp dateToTimestampInicioDia(java.util.Date data) {
		if(data == null) return null;
		return dateToTimestampInicioDia(new Date(data.getTime()));
	}
	
	public static Timestamp dateToTimestampInicioDia(Date data) {
		if(data == null) return null;		
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	public static Timestamp dateToTimestampFinalDia(java.util.Date data) {
		if(data == null) return null;
		return dateToTimestampFinalDia(new Date(data.getTime()));
	}
	
	public static Timestamp dateToTimestampFinalDia(Date data) {
		if(data == null) return null;	
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	
	/**
	 * M�todo para obter a primeira data do m�s atual.
	 * 
	 * @return java.sql.Date
	 * @author Flavio
	 */
	public static java.sql.Date firstDateOfMonth(){
		Calendar firstData = Calendar.getInstance();
		firstData.set(Calendar.DAY_OF_MONTH, firstData.getActualMinimum(Calendar.DAY_OF_MONTH));
		java.sql.Date data = new java.sql.Date(firstData.getTimeInMillis());
		return data;
	}
	
	public static java.sql.Date firstDateOfMonth(Date date){
		Calendar firstData = SinedDateUtils.dateToCalendar(date);
		firstData.set(Calendar.DAY_OF_MONTH, firstData.getActualMinimum(Calendar.DAY_OF_MONTH));
		java.sql.Date data = new java.sql.Date(firstData.getTimeInMillis());
		return data;
	}
	
	/**
	 * M�todo para obter a data atual.
	 * 
	 * @return java.sql.Date
	 * @author Fl�vio Tavares
	 */
	public static java.sql.Date currentDate(){
		return new java.sql.Date(System.currentTimeMillis());
	}
	
	public static java.sql.Timestamp currentTimestamp(){
		return new java.sql.Timestamp(System.currentTimeMillis());
	}
	
	/**
	 * M�todo para obter a �ltima data do m�s atual.
	 * 
	 * @return java.sql.Date
	 * @author Flavio
	 */
	public static java.sql.Date lastDateOfMonth(){
		Calendar lastData = new GregorianCalendar();
		lastData.set(Calendar.DAY_OF_MONTH, lastData.getActualMaximum(Calendar.DAY_OF_MONTH));
		java.sql.Date data = new java.sql.Date(lastData.getTimeInMillis());
		return data;
	}
	
	public static java.sql.Date lastDateOfMonth(Date date){
		Calendar lastData = SinedDateUtils.dateToCalendar(date);
		lastData.set(Calendar.DAY_OF_MONTH, lastData.getActualMaximum(Calendar.DAY_OF_MONTH));
		java.sql.Date data = new java.sql.Date(lastData.getTimeInMillis());
		return data;
	}
	
	public static Date incrementaDia(Date data, Integer dia, Calendario calendario){
		if(data == null || dia < 0) return null;
		long millisecondsPerDay = 1000 * 60 * 60 * 24;
		while(dia > 0){
			data = new Date(data.getTime() + (millisecondsPerDay));
			if(calendario == null){
				if(diaValidoWithoutCalendar(data, false)){
					dia--;
				}
			} else {
				if(diaValidoCalendar(data, calendario)){
					dia--;
				}
			}
		}
		
		return data;
	}
	
	public static Integer diferencaDias(Date dtinicio, Date dtfim, Calendario calendario, Boolean finalDeSemanaValido) {
		if(dtfim == null || dtinicio == null) return -1;
		int dias = 0;
		Date data = new Date(dtinicio.getTime());
		
		while(dtfim.getTime() > data.getTime()){
			data = incrementDate(data, 1, Calendar.DAY_OF_MONTH);
			if(calendario == null){
				if(diaValidoWithoutCalendar(data, finalDeSemanaValido)){
					dias++;
				}
			} else {
				if(diaValidoCalendar(data, calendario)){
					dias++;
				}
			}
		}
		
		return dias;		
	}
	
	public static Integer diferencaDias(Date dtinicio, Date dtfim, Calendario calendario) {
		return diferencaDias(dtinicio, dtfim, calendario, false);
	}
	
	public static boolean diaValidoCalendar(Date data, Calendario calendario) {
		List<Calendario> listaCalendario = new ArrayList<Calendario>();
		listaCalendario.add(calendario);
		return diaValidoCalendar(data, listaCalendario);
	}
	
	public static boolean diaValidoCalendar(Date data, List<Calendario> listaCalendario) {
		if(data == null) return false;
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(data.getTime());
		
		List<Calendarioitem> lista = new ArrayList<Calendarioitem>();
		for (Calendario c : listaCalendario) {
			lista.addAll(c.getListaCalendarioitem());
		}
		
		if(verificaContains(cal, lista)) return false;
		
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			for (Calendario c : listaCalendario) {
				if(c.getDomingoutil() != null && !c.getDomingoutil()){
					return false;
				}
			}
		}
		
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
			for (Calendario c : listaCalendario) {
				if(c.getSabadoutil() != null && !c.getSabadoutil()){
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static boolean verificaContains(Calendar data, List<Calendarioitem> lista){
		if(lista != null && lista.size() > 0){
			Calendar cal = Calendar.getInstance();
			for (Calendarioitem item : lista) {
				if(item.getDtferiado() != null){
					cal.setTimeInMillis(item.getDtferiado().getTime());
					if(cal.get(Calendar.DAY_OF_MONTH) == data.get(Calendar.DAY_OF_MONTH) &&
							cal.get(Calendar.MONTH) == data.get(Calendar.MONTH) &&
							cal.get(Calendar.YEAR) == data.get(Calendar.YEAR)){
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean diaValidoWithoutCalendar(Date data, Boolean finalDeSemanaValido) {
		if(data == null) return false;
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(data.getTime());
		
		if(finalDeSemanaValido == null || !finalDeSemanaValido){
			if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) return false;
			if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) return false;
		}
		
		return true;
	}


	/**
	 * M�todo para incrementar datas.
	 * 
	 * @see #incrementDate(Calendar, int, int)
	 * @param data - Data a ser incrementada.
	 * @param num - N�mero de vezes que um campo ser� incrementado.
	 * @param field - Campo a ser incrementado. (Constantes de java.util.Calendar)
	 * @return java.sql.Date
	 * @author Fl�vio Tavares
	 */
	public static java.sql.Date incrementDate(Date data, int num, int field){
		return incrementDate(javaSqlDateToCalendar(data), num, field);
	}
	
	public static Timestamp incrementTimestamp(Timestamp timestamp, int num, int field){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timestamp.getTime());
		
		calendar.add(field, num);
		
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	/**
	 * M�todo para incrementar datas.
	 * 
	 * @param data - Data a ser incrementada.
	 * @param num - N�mero de vezes que um campo ser� incrementado.
	 * @param field - Campo a ser incrementado. (Constantes de java.util.Calendar)
	 * @return java.sql.Date
	 * @author Fl�vio Tavares
	 */
	public static java.sql.Date incrementDate(Calendar data, int num, int field){
		data.add(field, num);
		return new Date(data.getTimeInMillis());
	}

	
	
	/**
	 * M�todo utilizado para incrementar uma data de acordo com a frequ�ncia.
	 * 
	 * @see br.com.linkcom.sined.geral.bean.Frequencia
	 * @see br.com.linkcom.sined.util.SinedDateUtils#incrementDate(Date, int, int)
	 * @param data
	 * @param freq
	 * @author Fl�vio Tavares
	 */
	public static Date incrementDateFrequencia(Date data, Frequencia frequencia, Integer parcela){
		Date dateIncrement = null;
		switch(frequencia.getCdfrequencia()){
		case 1: dateIncrement = data;
			break;
		case 2: //DI�RIA
			dateIncrement = SinedDateUtils.incrementDate(data, 1*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 3: //SEMANAL
			dateIncrement = SinedDateUtils.incrementDate(data, 7*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 4: //QUINZENAL
			dateIncrement = SinedDateUtils.incrementDate(data, 15*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 5: //MENSAL
			dateIncrement = SinedDateUtils.incrementDate(data, 1*parcela, Calendar.MONTH);
			break;
		case 6: //ANUAL
			dateIncrement = SinedDateUtils.incrementDate(data, 1*parcela, Calendar.YEAR);
			break;
		case 7: //SEMESTRAL
			dateIncrement = SinedDateUtils.incrementDate(data, 6*parcela, Calendar.MONTH);
			break;
		case 9: //TRIMESTRAL
			dateIncrement = SinedDateUtils.incrementDate(data, 3*parcela, Calendar.MONTH);
			break;
		}
		
		return dateIncrement;
	}
	
	public static Timestamp incrementTimestampFrequencia(Timestamp timestamp, Frequencia frequencia, Integer parcela){
		Timestamp timestampIncrement = null;
		switch(frequencia.getCdfrequencia()){
		case 1: timestampIncrement = timestamp;
		break;
		case 2: //DI�RIA
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 1*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 3: //SEMANAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 7*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 4: //QUINZENAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 15*parcela, Calendar.DAY_OF_MONTH);
			break;
		case 5: //MENSAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 1*parcela, Calendar.MONTH);
			break;
		case 6: //ANUAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 1*parcela, Calendar.YEAR);
			break;
		case 7: //SEMESTRAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 6*parcela, Calendar.MONTH);
			break;
		case 9: //TRIMESTRAL
			timestampIncrement = SinedDateUtils.incrementTimestamp(timestamp, 3*parcela, Calendar.MONTH);
			break;
		}
		
		return timestampIncrement;
	}
	
	/**
	 * M�todo para obter uma data remota, com data de 01/01/0001
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static java.sql.Date remoteDate(){
		return new Date(-62167381200000L);
	}
	
	/**
	 * <p>Passa uma data para String no formato dd/MM/yyyy.</p>
	 * 
	 * @param date
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String toString(java.util.Date date) {
		return format.format(date);
	}
	
	/**
	 * <p>Passa uma data para String no formato yyyy-MM-dd.</p>
	 * 
	 * @param date
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String toStringPostgre(java.util.Date date) {
		return formatPostgre.format(date);
	}
	
	public static String toStringHoraPostgre(Hora hora) {
		return formatPostgreHora.format(hora);
	}
	
	/**
	 * <p>Passa uma data para String no formato informado.</p>
	 * 
	 * @param date
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static String toString(java.sql.Date date, String format) {
		return new java.text.SimpleDateFormat(format).format(date);
	}
	
	public static String toString(java.sql.Timestamp date, String format) {
		return new java.text.SimpleDateFormat(format).format(date);
	}
	
	/**
	 * <p>Passa uma data para String no formato dd/MM/yyyy.</p>
	 * 
	 * @param date
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String toString(java.util.Calendar calendar) {
		if(calendar == null) return "";
		return toString(new java.util.Date(calendar.getTimeInMillis()));
	}
	
	/**
	 * <p>Verifica se duas datas s�o iguais, desconsiderando as horas.</p>
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 * @author Hugo Ferreira
	 */
	public static boolean equalsIgnoreHour(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null && d2 == null) {
			return true;
		} else if ((d1 != null && d2 == null) || (d1 == null && d2 != null)) {
			return false;
		}
		return toString(d1).equals(toString(d2));
	}
	
	public static boolean equalsIgnoreHour(java.util.Date d1, java.util.Date d2) {
		if (d1 == null && d2 == null) {
			return true;
		} else if ((d1 != null && d2 == null) || (d1 == null && d2 != null)) {
			return false;
		}
		return toString(d1).equals(toString(d2));
	}
	
	/**
	 * Verifica se d1 � uma data anterior � d2, sem considerar as horas.
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean beforeIgnoreHour(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(d1);
		Calendar c2 = javaSqlDateToCalendar(d2);
		
		return c1.before(c2);
	}
	
	public static boolean beforeIgnoreHour(java.util.Date d1, java.util.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(new Date(d1.getTime()));
		Calendar c2 = javaSqlDateToCalendar(new Date(d2.getTime()));
		
		return c1.before(c2);
	}
	
	/**
	 * Verifica se d1 � igual ou anterior � d2, sem considerar as horas.
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean beforeOrEqualIgnoreHour(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(d1);
		Calendar c2 = javaSqlDateToCalendar(d2);
		
		return c1.getTimeInMillis() <= c2.getTimeInMillis();
	}
	
	public static boolean beforeOrEqualIgnoreHour(java.util.Date d1, java.util.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(new Date(d1.getTime()));
		Calendar c2 = javaSqlDateToCalendar(new Date(d2.getTime()));
		
		return c1.getTimeInMillis() <= c2.getTimeInMillis();
	}
	
	/**
	 * Verifica se d1 � uma data posterior � d2, sem considerar as horas.
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean afterIgnoreHour(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(d1);
		Calendar c2 = javaSqlDateToCalendar(d2);
		
		return c1.after(c2);
	}
	
	public static boolean afterIgnoreHour(java.util.Date d1, java.util.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(new Date(d1.getTime()));
		Calendar c2 = javaSqlDateToCalendar(new Date(d2.getTime()));
		
		return c1.after(c2);
	}

	/**
	 * Verifica se d1 � igual ou posterior � d2, sem considerar as horas.
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean afterOrEqualsIgnoreHour(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null || d2 == null) {
			throw new NullPointerException("As datas n�o podem ser nulas");
		}
		
		Calendar c1 = javaSqlDateToCalendar(d1);
		Calendar c2 = javaSqlDateToCalendar(d2);
		
		return c1.getTimeInMillis() >= c2.getTimeInMillis();
	}
	
	/**
	 * Retorna data atual com 00:00:00. para fazer compara��es no mesmo dia.
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date currentDateToBeginOfDay(){
		GregorianCalendar dataAtual = new GregorianCalendar();
		return new Date(new GregorianCalendar(dataAtual.get(Calendar.YEAR), dataAtual.get(Calendar.MONTH), dataAtual.get(Calendar.DAY_OF_MONTH), 0, 0, 0).getTimeInMillis());
	}
	
	/**
	 * M�todo para alterar propriedades de uma data, como dia, m�s ou ano...
	 * Semelhante ao m�todo {@link java.util.Calendar#set(int, int)}
	 * 
	 * @param data - Data com a qual ser� criada uma nova data modificada.
	 * @param value - Valor para o qual o campo ser� modificado.
	 * @param field - Campo que ser� modificado. Constantes de {@link java.util.Calendar}. Ex.: java.util.Calendar#DAY_OF_MONTH
	 * @return new java.sql.Date
	 * @author Fl�vio Tavares
	 */
	public static Date setDateProperty(Date data, int value, int field){
		Calendar ajuste = Calendar.getInstance();
		ajuste.setTime(data);
		ajuste.set(field, value);
		return new Date(ajuste.getTimeInMillis());
	}
	
	/**
	 * M�todo para obter uma propriedade de uma data.
	 * 
	 * @param data - Data com a qual ser� criada uma nova data modificada.
	 * @param field - Campo que ser� modificado. Constantes de {@link java.util.Calendar}. Ex.: java.util.Calendar#DAY_OF_MONTH
	 * @return 
	 * @author Fl�vio Tavares
	 */
	public static int getDateProperty(java.util.Date data, int field){
		Calendar ajuste = Calendar.getInstance();
		ajuste.setTime(data);
		return ajuste.get(field);
	}
	
	public static int getTimestampProperty(Timestamp timestamp, int field){
		Calendar ajuste = Calendar.getInstance();
		ajuste.setTime(timestamp);
		return ajuste.get(field);
	}
	
	/**
	 * M�todo que retorna a descri��o do m�s atrav�s de uma data fornecida.
	 * 
	 * @param data
	 * @author Thiago Clemente
	 * 
	 */
	public static String getDescricaoMes(Date data) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		
		String mes = "";
		
		switch (calendar.get(Calendar.MONTH)) {
			case 0:
				mes = "Janeiro";
				break;
			case 1:
				mes = "Fevereiro";
				break;	
			case 2:
				mes = "Mar�o";
				break;
			case 3:
				mes = "Abril";
				break;
			case 4:
				mes = "Maio";
				break;
			case 5:
				mes = "Junho";
				break;
			case 6:
				mes = "Julho";
				break;
			case 7:
				mes = "Agosto";
				break;
			case 8:
				mes = "Setembro";
				break;
			case 9:
				mes = "Outubro";
				break;
			case 10:
				mes = "Novembro";
				break;
			case 11:
				mes = "Dezembro";
				break;	
			default:
				break;
		}
				
		return mes;		
	}
	
	/**
	 * Calcula a diferen�a em horas das 
	 *
	 * @param inicioStr
	 * @param fimStr
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static Double calculaDiferencaHoras(String inicioStr, String fimStr) {
		String[] inicio = inicioStr.split(":");
		String[] fim = fimStr.split(":");
		
		Double inicioHora = Double.parseDouble(inicio[0]);
		Double fimHora = Double.parseDouble(fim[0]);
		
		Double inicioMin = Double.parseDouble(inicio[1]);
		Double fimMin = Double.parseDouble(fim[1]);
		
		inicioHora = (inicioMin / 60) + inicioHora;
		fimHora = (fimMin / 60) + fimHora;
		
		inicioHora = Math.round(inicioHora*100.0)/100.0;
		fimHora = Math.round(fimHora*100.0)/100.0;
		
		return (fimHora - inicioHora);
	}
	
	/**
	 * Converte strings de datas no formato yyyy-mm-dd ou dd-mm-yyyy em objetos do tipo java.sql.Date.
	 * O "-" pode ser substituido por "/". 
	 * @author Rodrigo Alvarenga 
	 * @param data Data nos formatos descritos acima
	 * @return java.sql.Date ou null se o formato da data for inv�lido
	 */
	public static java.sql.Date stringToSqlDate(String data) {
		if (data == null) {
			return null;
		}

		data = data.trim();
		
		if (PADRAO_DATA_YYYYMMDD.matcher(data).matches()) {
			data = data.replace('/', '-');
		}
		else if (PADRAO_DATA_DDMMYYYY.matcher(data).matches()) {
			String dt[] = data.split("[/-]");
			data = dt[2]+"-"+dt[1]+"-"+dt[0];
		}
		else
			return null;

		return java.sql.Date.valueOf(data);
	}
	
	public static int diferencaDias(java.util.Date data1, java.util.Date data2) {
		double diferenca = diferencaHoras(data1, data2) / 24.0;
		return SinedUtil.round(diferenca, 0).intValue();
	}

	public static int diferencaMeses(java.util.Date data1, java.util.Date data2) {
		double diferenca = diferencaHoras(data1, data2) / 24.0 / 30.0;
		return SinedUtil.round(diferenca, 0).intValue();
	}
	
	public static int diferencaSemanas(java.util.Date data1, java.util.Date data2) {
		double diferenca = diferencaHoras(data1, data2) / 24.0 / 7.0;
		return SinedUtil.round(diferenca, 0).intValue();
	}	
	
	/**
	 * Retorna a o resultado de (data1 - data2), em dias inteiros.
	 * 
	 * @author Bruno Eust�quio
	 * @param data1
	 * @param data2
	 * @return diferen�a em dias dias
	 */
	public static int diferencaHoras(java.util.Date data1, java.util.Date data2) {
		
		Calendar calendar1 = dateToCalendar(data1);
		calendar1.set(Calendar.HOUR_OF_DAY, 12);
		calendar1.set(Calendar.MINUTE, 0);
		calendar1.set(Calendar.SECOND, 0);
		calendar1.set(Calendar.MILLISECOND, 0);

		Calendar calendar2 = dateToCalendar(data2);
		calendar2.set(Calendar.HOUR_OF_DAY, 12);
		calendar2.set(Calendar.MINUTE, 0);
		calendar2.set(Calendar.SECOND, 0);
		calendar2.set(Calendar.MILLISECOND, 0);
		
		double diferenca = ((calendar1.getTimeInMillis() - calendar2.getTimeInMillis()) / 1000.0 / 60.0 / 60.0);
		return (int) SinedUtil.round(diferenca, 0).intValue();
	}
	
	/**
	 * Converte um java.sql.Date para Calendar
	 * 
	 * @author Bruno Eust�quio
	 * @param java.sql.Date
	 * @return Calendar
	 */
	public static Calendar dateToCalendar(java.util.Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * M�todo que adiciona dias a uma determinada Data
	 * 
	 * @param dtReferencia
	 * @param dias
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date addDiasData(Date dtReferencia, int dias) {
		if(dtReferencia == null){
			return null;
		}
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtReferencia.getTime());
		dtLimiteAux.add(Calendar.DAY_OF_MONTH, dias);
		
		return new Date(dtLimiteAux.getTimeInMillis());
	}
	
	public static Date addMinutesData(java.util.Date dtReferencia, int minutos) {
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtReferencia.getTime());
		dtLimiteAux.add(Calendar.MINUTE, minutos);
		
		return new Date(dtLimiteAux.getTimeInMillis());
	}
	
	/**
	 * M�todo que adiciona semanas a uma determinada Data
	 *
	 * @param dtReferencia
	 * @param semanas
	 * @return
	 * @since 24/05/2012
	 * @author Rodrigo Freitas
	 */
	public static Date addSemanasData(Date dtReferencia, int semanas) {
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtReferencia.getTime());
		dtLimiteAux.add(Calendar.WEEK_OF_YEAR, semanas);
		
		return new Date(dtLimiteAux.getTimeInMillis());
	}
	
	/**
	 * M�todo que adiciona dias a uma determinada Data
	 * N�o considera S�bados e Domingos.
	 * 
	 * @param dtReferencia
	 * @param dias
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date addDiasUteisData(Date dtReferencia, int dias) {
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtReferencia.getTime());
		
		for (int i = 0; i < dias; i++) {
			dtLimiteAux.add(Calendar.DAY_OF_MONTH, 1);
			while(dtLimiteAux.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || 
					dtLimiteAux.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
				dtLimiteAux.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		
		return new Date(dtLimiteAux.getTimeInMillis());
	}

	/**
	 * M�todo que adiciona m�s a uma determinada Data
	 * 
	 * @param dtReferencia
	 * @param meses
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date addMesData(Date dtReferencia, int meses) {
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtReferencia.getTime());
		dtLimiteAux.add(Calendar.MONTH, meses);
		
		return new Date(dtLimiteAux.getTimeInMillis());
	}
	
	/**
	 * Retorna uma nova data no final do dia
	 * 
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date dataToEndOfDay(Date data) {
		if(data != null){
			Calendar dtAux = Calendar.getInstance();
			dtAux.setTime(data);
			dtAux.set(Calendar.HOUR_OF_DAY, 23);
			dtAux.set(Calendar.MINUTE, 59);
			dtAux.set(Calendar.SECOND, 59);
			dtAux.set(Calendar.MILLISECOND, 999);
			
			return new Date(dtAux.getTimeInMillis());
		}else{
			return null;
		}
	}
	
	/**
	 * Retorna uma nova data no come�o do dia
	 * 
	 * @param java.util.data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static java.util.Date dataToEndOfDay(java.util.Date data) {
		if(data != null){
			return dataToEndOfDay(new java.sql.Date(data.getTime()));
		}else{
			return null;
		}
		
	}

	/**
	 * Retorna uma nova data no come�o do dia
	 * 
	 * @param java.sql.data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date dateToBeginOfDay(Date data) {
		if(data != null){
			Calendar dtAux = Calendar.getInstance();
			dtAux.setTime(data);
			dtAux.set(Calendar.HOUR_OF_DAY, 0);
			dtAux.set(Calendar.MINUTE, 0);
			dtAux.set(Calendar.SECOND, 0);
			dtAux.set(Calendar.MILLISECOND, 0);
			
			return new Date(dtAux.getTimeInMillis());
		}else{
			return null;
		}
		
	}

	/**
	 * Retorna uma nova data no come�o do dia
	 * 
	 * @param java.util.data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static java.util.Date dateToBeginOfDay(java.util.Date data) {
		if(data != null){
			return dateToBeginOfDay(new java.sql.Date(data.getTime()));
		}else{
			return null;
		}
		
	}

	/**
	 * Verifica se o dia � um domingo
	 * 
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Boolean isDaySunday(Date data) {
		Calendar dtAux = Calendar.getInstance();
		dtAux.setTime(data);
		return dtAux.get(Calendar.DAY_OF_WEEK) == 1;
	}

	/**
	 * Retorna dia da semana da data
	 * 
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String stringDayOfWeek(Date data) {
		Calendar dtAux = Calendar.getInstance();
		dtAux.setTime(data);
		switch (dtAux.get(Calendar.DAY_OF_WEEK)) {
			case 1:
				return "Dom.";
			case 2:
				return "Seg.";
			case 3:
				return "Ter.";
			case 4:
				return "Qua.";
			case 5:
				return "Qui.";
			case 6:
				return "Sex.";
			case 7:
				return "S�b.";
			default:
				return "Inv�lido";
			}
	}
	
	/**
	 * M�todo que retorna a qtde de meses entre datas.
	 * A data de inicio considerando o primeiro dia do m�s.
	 *
	 * @param dtInicio
	 * @param dtFim
	 * @return
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public static Integer mesesEntreInicio(Date dtInicio, Date dtFim) {
		Date inicio = firstDateOfMonth(dtInicio);
		
		Calendar dtInicioAux = Calendar.getInstance();
		Calendar dtFimAux = Calendar.getInstance();
		
		dtInicioAux.setTimeInMillis(inicio.getTime());
		dtFimAux.setTimeInMillis(dtFim.getTime());
		
		return mesesEntre(dtInicioAux, dtFimAux);
	}
	
	/**
	 * M�todo que retorna a qtde de meses entre datas.
	 * A data inicio considerando o primeiro dia do m�s.
	 * A data final considerando o �ltimo dia do m�s.
	 *
	 * @param dtInicio
	 * @param dtFim
	 * @return
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public static Integer mesesEntreInicioFim(Date dtInicio, Date dtFim) {
		Date inicio = firstDateOfMonth(dtInicio);
		Date fim = lastDateOfMonth(dtFim);
		
		Calendar dtInicioAux = Calendar.getInstance();
		Calendar dtFimAux = Calendar.getInstance();
		
		dtInicioAux.setTimeInMillis(inicio.getTime());
		dtFimAux.setTimeInMillis(fim.getTime());
		
		return mesesEntre(dtInicioAux, dtFimAux);
	}
	
	public static Integer mesesEntre(Date dtInicio, Date dtFim) {
		Calendar dtInicioAux = Calendar.getInstance();
		Calendar dtFimAux = Calendar.getInstance();
		
		dtInicioAux.setTimeInMillis(dtInicio.getTime());
		dtFimAux.setTimeInMillis(dtFim.getTime());
		
		return mesesEntre(dtInicioAux, dtFimAux);
	}
	
	/**
	 * Retorna a quantidade de meses entre duas datas (por exemplo: o per�odo entre 01/2007 e 03/2007 corresponde a 2 meses)
	 * @param dtInicio
	 * @param dtFim
	 * @return
	 * @author Thiago Gon�alves
	 */
	public static Integer mesesEntre(Calendar dtInicio, Calendar dtFim) {
		if (dtInicio == null || dtFim == null)
			return null;
		
		int diaIni = dtInicio.get(Calendar.DAY_OF_MONTH);
		int mesIni = dtInicio.get(Calendar.MONTH) + 1;
		int anoIni = dtInicio.get(Calendar.YEAR);

		int diaFim = dtFim.get(Calendar.DAY_OF_MONTH);
		int mesFim = dtFim.get(Calendar.MONTH) + 1;
		int anoFim = dtFim.get(Calendar.YEAR);

		int anos = 0;
		int meses = 0;

		if ( (anoFim < anoIni) || ((anoFim == anoIni) && (mesFim < mesIni)) )
		    return 0;

		anos = anoFim - anoIni;

		if (mesFim < mesIni){
			meses = (12 - mesIni) + mesFim;
			if (anos > 0)
				--anos;
		} else
			meses = mesFim - mesIni;

		if (diaFim < diaIni){
			if (meses == 0){
				if (anos > 0){
					--anos;
					meses = 11;
				}
			} else
				--meses;
		}

		meses += anos * 12;
		return meses;
	}

	public static Date setaHoraData(Date date, Hora hora) {
		Calendar dataPrincipal = Calendar.getInstance();
		dataPrincipal.setTimeInMillis(date.getTime());
		
		Calendar dataHora = Calendar.getInstance();
		dataHora.setTimeInMillis(hora.getTime());
		
		dataPrincipal.set(Calendar.HOUR_OF_DAY, dataHora.get(Calendar.HOUR_OF_DAY));
		dataPrincipal.set(Calendar.MINUTE, dataHora.get(Calendar.MINUTE));
		
		return new Date(dataPrincipal.getTimeInMillis());
	}
	
	/**
	* M�todo que busca o dia util anterior 
	*
	* @param data
	* @param empresa
	* @return
	* @since 13/03/2018
	* @author Fabricio Berg
	*/
	public static Date getAnteriorDataUtil(Date date, Empresa empresa) {
		Municipio municipio = null;
		Uf uf = null;
		
		if(empresa != null){
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		List<Calendario> listaCalendario = CalendarioService.getInstance().findCalendarioGeral(uf, municipio);
		
		
		while (!SinedDateUtils.diaValidoCalendar(date, listaCalendario)) {
			date = SinedDateUtils.addDiasData(date, -1);
		}
		
		return date;
	}
	
	/**
	* M�todo que busca a proximo dia util
	*
	* @param data
	* @param empresa
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public static Date getProximaDataUtil(Date data, Empresa empresa) {
		return getProximaDataUtil(data, empresa, false, null);
	}
	
	public static Date getProximaDataUtil(Date data, Empresa empresa, HashMap<String, List<Calendario>> mapCalendario) {
		return getProximaDataUtil(data, empresa, false, mapCalendario);
	}
	
	public static Date getProximaDataUtil(Date data, Empresa empresa, Boolean ultimaDiaUtilMes){
		return getProximaDataUtil(data, empresa, ultimaDiaUtilMes, null);
	}
	
	/**
	* M�todo que busca o proximo dia util considearando o calendario da empresa, caso exista
	*
	* @param data
	* @param empresa
	* @param ultimaDiaUtilMes
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public static Date getProximaDataUtil(Date data, Empresa empresa, Boolean ultimaDiaUtilMes, HashMap<String, List<Calendario>> mapCalendario) {
		if(empresa == null){
			empresa = EmpresaService.getInstance().loadPrincipal();
		}
		
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = EmpresaService.getInstance().loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		return getProximaDataUtil(data, uf, municipio, ultimaDiaUtilMes, mapCalendario);
	}
	
	public static Date getProximaDataUtilProximoMes(Date data, Empresa empresa, Boolean ultimaDiaUtilMes, Boolean considerarProximoMes) {
		if(empresa == null){
			empresa = EmpresaService.getInstance().loadPrincipal();
		}
		
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = EmpresaService.getInstance().loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		return getProximaDataUtil(data, uf, municipio, ultimaDiaUtilMes, considerarProximoMes, null);
	}
	
	/**
	* M�todo que busca o proximo dia util
	*
	* @param data
	* @param uf
	* @param municipio
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public static Date getProximaDataUtil(Date data, Uf uf, Municipio municipio) {
		return getProximaDataUtil(data, uf, municipio, false);
	}
	
	public static Date getProximaDataUtil(Date data, Uf uf, Municipio municipio, Boolean ultimaDiaUtilMes) {
		return getProximaDataUtil(data, uf, municipio, ultimaDiaUtilMes, Boolean.FALSE, null);
	}
	
	public static Date getProximaDataUtil(Date data, Uf uf, Municipio municipio, Boolean ultimaDiaUtilMes, HashMap<String, List<Calendario>> mapCalendario) {
		return getProximaDataUtil(data, uf, municipio, ultimaDiaUtilMes, Boolean.FALSE, mapCalendario);
	}

	/**
	 * M�todo que busca a pr�xima data �til caso seja feriado ou domingo
	 * Caso ultimaDiaUtilMes seja true e a data passada por par�metro seja o �ltimo dia e seja feriado ou domingo, 
	 * o sistema ir� retornar o �ltimo dia �til do m�s
	 * 
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Date getProximaDataUtil(Date data, Uf uf, Municipio municipio, Boolean ultimaDiaUtilMes, Boolean considerarProximoMes, HashMap<String, List<Calendario>> mapCalendario) {
		if(data != null){
			List<Calendario> listaCalendario = new ArrayList<Calendario>();
			if(mapCalendario != null){
				String key = (uf != null && uf.getCduf() != null ? uf.getCduf().toString() : "") + "-" + (municipio != null && municipio.getCdmunicipio() != null ? municipio.getCdmunicipio().toString() : "");
				if(mapCalendario.containsKey(key)){
					listaCalendario = mapCalendario.get(key);
				}else {
					listaCalendario = CalendarioService.getInstance().getCalendarioGeral(uf, municipio);
					mapCalendario.put(key, listaCalendario);
				}
			}else {
				listaCalendario = CalendarioService.getInstance().getCalendarioGeral(uf, municipio);
			}
			
			Calendar dataAux = Calendar.getInstance();
			dataAux.setTimeInMillis(data.getTime());
			
			boolean subtrair = false;
			Date dataUltimaDiaMes = null;
			if(ultimaDiaUtilMes != null && ultimaDiaUtilMes){
				dataUltimaDiaMes = lastDateOfMonth(new Date(dataAux.getTimeInMillis()));
			}
			if(Boolean.TRUE.equals(considerarProximoMes)){
				dataAux.add(Calendar.MONTH, 1);
			}
			if(listaCalendario != null && listaCalendario.size() > 0){
				
				while(true){
					if(diaValidoCalendar(new Date(dataAux.getTimeInMillis()), listaCalendario))
						return new Date(dataAux.getTimeInMillis());
					if(ultimaDiaUtilMes != null && ultimaDiaUtilMes && !subtrair && dataUltimaDiaMes != null && 
							dataUltimaDiaMes.equals(new Date(dataAux.getTimeInMillis()))){
						subtrair = true; 
					}
					dataAux.add(Calendar.DAY_OF_MONTH, subtrair ? -1 : 1);
				}
			} else {
				while(true){
					if(dataAux.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)
						return new Date(dataAux.getTimeInMillis());
					if(ultimaDiaUtilMes != null && ultimaDiaUtilMes && !subtrair && dataUltimaDiaMes != null && 
							dataUltimaDiaMes.equals(new Date(dataAux.getTimeInMillis()))){
						subtrair = true; 
					}
					dataAux.add(Calendar.DAY_OF_MONTH, subtrair ? -1 : 1);
				}
			}
		}
		return data;
	}
	
	public static double hoursToDouble(String tmpHours) throws NumberFormatException {
		Double result = 0d;
		tmpHours = tmpHours.trim();

		try {
			result = new Double(tmpHours);
		} catch (NumberFormatException nfe) {
			if (tmpHours.contains(":")) {
				int hours = 0;
				int minutes = 0;
				int locationOfColon = tmpHours.indexOf(":");
				try {
					hours = new Integer(tmpHours.substring(0, locationOfColon));
					minutes = new Integer(tmpHours
							.substring(locationOfColon + 1));
				} catch (NumberFormatException nfe2) {
					throw nfe2;
				}
				if (minutes > 0) {
					result = new Double(minutes) / 60.0;
				}
				result += hours;
			}
		}
		return result;
	}
	
	public static String doubleToHours(Double horas){
		String qtdeStr = horas.toString();
		String[] hours = qtdeStr.split("\\.");
		
		Integer hora = Integer.parseInt(hours[0]);
		String horaStr = hora.toString();
		if(hora < 10) horaStr = "0" + hora.toString();
		
		Double min = horas - Double.parseDouble(hora.toString());
		min = min * 60;
		long minInt = Math.round(min);
		String minStr = minInt + "";
		if(minInt < 10)  minStr = "0" + minInt;
		
		return horaStr + ":" + minStr;
	}
	
	/**
	 * A partir das datas do filtro cria uma lista de datas do primeiro dia do m�s.
	 *
	 * @param dtref1
	 * @param dtref2
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static List<Date> criaListaData(Date dtref1, Date dtref2) {
		List<Date> lista = new ArrayList<Date>();
		
		Date data = new Date(dtref1.getTime());	

		while(SinedDateUtils.beforeIgnoreHour(data, dtref2)){
			lista.add(data);
			data = SinedDateUtils.incrementDate(data, 1, Calendar.MONTH);
		}
		
		return lista;
	}
	
	/**
	 * A partir das datas do filtro cria uma lista de datas do primeiro dia do m�s.
	 *
	 * @param dtref1
	 * @param dtref2
	 * @return
	 * @author Luiz Fernando
	 */
	public static List<Date> criaListaDataBeforeOrEqual(Date dtref1, Date dtref2) {
		dtref1 = firstDateOfMonth(dtref1);
		dtref2 = lastDateOfMonth(dtref2);
		
		List<Date> lista = new ArrayList<Date>();
		
		Date data = new Date(dtref1.getTime());	

		while(SinedDateUtils.beforeOrEqualIgnoreHour(data, dtref2)){
			lista.add(data);
			data = SinedDateUtils.incrementDate(data, 1, Calendar.MONTH);
		}
		
		return lista;
	}
	
	/**
	 * transforma um double (horas) em uma string no formato 'HH:MM'
	 *
	 * @param d
	 * @return
	 * @author Rafael Salvio
	 */
	public static String doubleToFormatoHora(Double d) {
		String horas, minutos;
		String[] valor = d.toString().split("\\.");
		if(valor.length > 1){
			horas = (valor[0].length() == 1 ? "0" : "") + valor[0];
			Long temp = Math.round((Double.parseDouble("0."+valor[1]))*60.0);
			minutos = (temp.toString().length() == 1 ? "0":"")+ temp.toString();
		}
		else{
			horas = (valor[0].length() == 1 ? "0" : "") + valor[0];
			minutos = "00";
		}
		
		return (horas+":"+minutos);
	}
	
	/**
	 * M�todo que verifica igualdade de m�s e ano
	 *
	 * @param d1
	 * @param d2
	 * @return
	 * @author Luiz Fernando
	 */
	public static boolean equalsMesAno(java.sql.Date d1, java.sql.Date d2) {
		if (d1 == null && d2 == null) {
			return true;
		} else if ((d1 != null && d2 == null) || (d1 == null && d2 != null)) {
			return false;
		}
		return new SimpleDateFormat("MM/yyyy").format(d1).equals(new SimpleDateFormat("MM/yyyy").format(d2));
	}
	
	/**
	 * M�todo que retorna a quantidade de dias �teis de um per�odo 
	 *
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public static Integer getQtdeDiasUteis(Date dtinicio, Date dtfim) {
		Integer diasuteis = 0;
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtinicio.getTime());
		int dias = SinedDateUtils.diferencaDias(dtfim, dtinicio);
		
		for (int i = 0; i <= dias; i++) {
			dtLimiteAux.add(Calendar.DAY_OF_MONTH, 1);
			if(dtLimiteAux.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
					dtLimiteAux.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
				diasuteis++;
			}
		}
		
		return diasuteis;
	}
	
	/**
	*
	*<p>M�todo respons�vel em receber uma data do tipo java.util.Date e converter para<br>
	*java.sql.Date
	*
	* @param date java.util.Date
	* @return java.sql.Date
	*
	* @author Biharck Ara�jo
	* @since Aug 27, 2008
	*/
	public static java.sql.Date castUtilDateForSqlDate(java.util.Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long timeMilles = calendar.getTimeInMillis();
		java.sql.Date dateReturn = new java.sql.Date(timeMilles);
		return dateReturn;
	}
	
	public static java.util.Date castSqlDateForUtilDate(java.sql.Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long timeMilles = calendar.getTimeInMillis();
		java.util.Date dateReturn = new java.util.Date(timeMilles);
		return dateReturn;
	}

	public static java.sql.Date ultimoDiaUtil(java.sql.Date data){
		Calendar calendar = Calendar.getInstance();  
		calendar.setTime(data);
		
		while(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ||
				calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
			calendar.add(Calendar.DAY_OF_MONTH, -1);
		}
		
		return new Date(calendar.getTimeInMillis());
	}
	
	/**
	 * Diferenca de dias uteis entre duas datas, descontando os fins de semana
	 * @param dataInicial
	 * @param dataFinal
	 * @author igorcosta
	 * @return
	 */
	public static int diferencaDiasUteis( Date dataInicial, Date dataFinal ){  
	      
		int qtDiasUteis = 0;  
		int qtDiasEntreDatas = 0;  
		
		qtDiasEntreDatas = ( int ) ( ( dataFinal.getTime() - dataInicial.getTime() )/( 24*60*60*1000 ) );  
		qtDiasEntreDatas = qtDiasEntreDatas > 0 ? qtDiasEntreDatas : 0 ;
		
		Calendar calendar = Calendar.getInstance();  
		calendar.setTime(dataInicial);  
		for( int i = 0; i <= qtDiasEntreDatas; i++ ) {  
		    if( !( calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ) && !( calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ) ) {  
		        qtDiasUteis++;  
		    }  
		    calendar.add( Calendar.DATE, 1 );  
		}  
		  
		return qtDiasUteis;  
	}
	
	/**
	 * Verifica se a data passada � um s�bado ou um domingo.
	 */
	public static boolean isFinalSemana(Date data) {
		Calendar dtAux = Calendar.getInstance();
		dtAux.setTime(data);
		int dia = dtAux.get(Calendar.DAY_OF_WEEK);
		return (dia == Calendar.SATURDAY || dia == Calendar.SUNDAY);
	}
	
	public static List<Date> getListDateByPeriodo(Date data1, Date data2){
		if(data1 == null || data2 == null) return new ArrayList<Date>();
		if(SinedDateUtils.afterIgnoreHour(data1, data2)) return new ArrayList<Date>();
		
		List<Date> listDate = new ArrayList<Date>();
		
		Date data_aux = new Date(data1.getTime());
		
		do {
			listDate.add(data_aux);
			data_aux = SinedDateUtils.incrementDate(data_aux, 1, Calendar.DAY_OF_MONTH);
		} while(SinedDateUtils.beforeOrEqualIgnoreHour(data_aux, data2));
		
		return listDate;
	}

	/**
	 * Retorna o ano de uma data
	 * @param data
	 * @return
	 */
	public static int getAno(Date data) {
		if(data==null)
			throw new RuntimeException("Data n�o pode ser nula.");
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal.get(Calendar.YEAR);
	}
	
	public static int getDia(Date data) {
		if(data==null)
			throw new RuntimeException("Data n�o pode ser nula.");
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	public static int getMes(Date data) {
		if(data==null)
			throw new RuntimeException("Data n�o pode ser nula.");
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal.get(Calendar.MONTH);
	}
	
	public static Hora getHoraAtual(){
		return getHora(Calendar.getInstance());
	}
	
	public static Hora getHora(Calendar calendar){
		return new Hora(new DecimalFormat("00").format(calendar.get(Calendar.HOUR_OF_DAY)) + ":" + new DecimalFormat("00").format(calendar.get(Calendar.MINUTE)));
	}
	
	public static void addCalendar(Calendar calendar, Frequencia frequencia, int multiplicador){
		int field;
		int amount;
		
		if (frequencia.getCdfrequencia().intValue()==Frequencia.HORA.intValue()){
			field = Calendar.HOUR_OF_DAY;
			amount = multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.DIARIA.intValue()){
			field = Calendar.DAY_OF_MONTH;
			amount = multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.SEMANAL.intValue()){
			field = Calendar.DAY_OF_MONTH;
			amount = 7 * multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.QUINZENAL.intValue()){
			field = Calendar.DAY_OF_MONTH;
			amount = 15 * multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.MENSAL.intValue()){
			field = Calendar.MONTH;
			amount = multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.TRIMESTRAL.intValue()){
			field = Calendar.MONTH;
			amount = 3 * multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.SEMESTRE.intValue()){
			field = Calendar.MONTH;
			amount = 6 * multiplicador;
		}else if (frequencia.getCdfrequencia().intValue()==Frequencia.ANUAL.intValue()){
			field = Calendar.YEAR;
			amount = multiplicador;
		}else {
			return;
		}
		
		calendar.add(field, amount);
	}
	
	public static String getPeriodoDeAte(Date dtInicio, Date dtFim){
		String periodo = "";
		
		if(dtInicio != null){
			periodo = toString(dtInicio);
		}
		if(dtFim != null){
			periodo += " at� " + toString(dtFim);
		}
		return periodo;
	}
	
	public static String getMesAno(Date data){
		SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
		String mesAno = sdf.format(data);

		return mesAno;
	}

	public static Date getDataVencimentoValida(Date dtproximo, Date dtemissao) {
		if(dtproximo != null && dtemissao != null && afterIgnoreHour(dtemissao, dtproximo)){
			return dtemissao;
		}
		return dtproximo;
	}
	
	public static Timestamp subtractMinutes(Timestamp data, Integer minutos){
		return new Timestamp(data.getTime() - (minutos * 60000));
	}
}