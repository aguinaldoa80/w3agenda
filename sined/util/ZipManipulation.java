package br.com.linkcom.sined.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import br.com.linkcom.sined.geral.bean.Arquivo;


/**
 * Classe respons�vel por manipular arquivos ZIP
 *
 * @author Pedro Gon�alves
 */
public class ZipManipulation {
	
	protected Arquivo arquivo;
	protected HashMap<String, byte[]> entries = new HashMap<String, byte[]>();

	/**
	 * Inicializa o zip carregando o input stream e as entradas.
	 * @param arquivo
	 */
	public ZipManipulation(Arquivo arquivo) {
//		System.out.println(arquivo.getTipoconteudo());
//		if(!arquivo.getTipoconteudo().contains("zip"))
//			throw new SinedException("O arquivo n�o � do tipo ZIP");
		this.arquivo = arquivo;
	}

	/**
	 * cria um zipinput stream a partir dos bytes dos arquivos
	 */
	private ZipInputStream createZin(){
		ByteArrayInputStream buffer = new ByteArrayInputStream(arquivo.getContent());
		return new ZipInputStream(buffer);
	}

	/**
	 * l� todas as entradas do zip
	 */
	public void readEntries() {
		
		
		try {
			ZipInputStream zin = createZin();
			ZipEntry entry;
			
			
			
			while ((entry = zin.getNextEntry()) != null) {
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				int data;
				while ((data = zin.read()) != -1) {
					output.write(data);
				}
				entries.put(entry.getName(), output.toByteArray());
				output.close();
			}
			zin.close();
		} catch (IOException ioe) {
			System.err.println("Erro ao descompactar:" + ioe.getMessage());
			ioe.printStackTrace();
		}
		
//		try {
//			ZipInputStream zin = createZin();
//			ZipEntry entry;
//			while ((entry = zin.getNextEntry()) != null) {
//				ByteBuffer bbuf = ByteBuffer.allocate(Integer.parseInt(""+entry.getSize()));
//				byte data[] = new byte[1];
//				while (zin.read(data, 0, 1) != -1) {
//					bbuf.put(data);
//				}
//				entries.put(entry.getName(),bbuf);
//			}
//			zin.close();
//		} catch (IOException ioe) {
//			System.err.println("Erro ao descompactar:" + ioe.getMessage());
//			ioe.printStackTrace();
//		}
	}

	/**
	 * pega o total de entradas que tem em um zip
	 * @return
	 */
	public Integer getTotalEntries() {
		return entries.size();
	}

	/**
	 * pega todas as entredas de um arquivo zip
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> getEntriesNames(){
		return new ArrayList(entries.keySet());
	}

	/**
	 * l� os bytes da entrada especificada
	 *
	 * @param entryKey nome da entrada
	 * @return
	 * @throws IOException
	 * @throws IdeagriException quando o arquivo a ser extra�do os bytes n�o existe na pilha
	 */
	public byte[] getBytesFromEntry(String entryKey) throws IOException,SinedException{
		if(entries.containsKey(entryKey)){
			return entries.get(entryKey);
		}
		throw new SinedException("A entry pedida n�o existe na pilha");
	}

	/**
	 * Salva a entry no diret�rio
	 *
	 * @param name nome do arquivo na pilha
	 * @param filename nome do arquivo a ser salvo. Incluindo o diret�rio. ex: c:/temp
	 * @throws Exception
	 */
	public void saveEntryToDirectory(String name, String filename) throws Exception{
		if(entries.containsKey(name)){
			java.io.File file = new java.io.File(filename);
			if(!file.exists())
				file.createNewFile();

			FileOutputStream fileOutputStream = null;
			try {
				fileOutputStream = new FileOutputStream(filename);
				BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
				bufferedOutputStream.write(getBytesFromEntry(name));
				bufferedOutputStream.flush();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				try {
					fileOutputStream.close();
				}
				catch (Exception e) {
				}
			}
		} else {
			throw new SinedException("A entry pedida n�o existe na pilha");
		}
	}

	/**
	 * Retorna a quantidade de arquivos contidos no zip (usado para evitar consumo de memoria do metodo readEntries)
	 * @return
	 */
	public Integer getTotalFiles(){
		Integer files = 0;
		try {
			ZipInputStream zin = createZin();
			while (zin.getNextEntry() != null)
				++files;
			zin.close();
		} catch (Exception e) {
			throw new SinedException("Erro ao processar arquivo Zip.");
		}
		return files;
	}

	/**
	 * Retorna o nome do unico arquivo contido no zip (usado para evitar consumo de memoria do metodo readEntries)
	 * @return
	 */
	public String getUniqueFileName() {
		String name = null;
		try{
			ZipInputStream zin = createZin();
			ZipEntry nextEntry = zin.getNextEntry();
			if (nextEntry != null)
				name = nextEntry.getName();
			zin.close();
		} catch (Exception e) {
			throw new SinedException("Erro ao processar arquivo Zip.");
		}
		return name;
	}

	/**
	 * Extrai o �nico arquivo para o caminho especificado (usado para evitar consumo de memoria do metodo readEntries)
	 * @param filePath
	 */
	public void extractUniqueFile(String filePath){
		try {
			ZipInputStream zin = createZin();
	        zin.getNextEntry();

	        // Open the output file
	        OutputStream out = new FileOutputStream(filePath);

	        // Transfer bytes from the ZIP file to the output file
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = zin.read(buf)) > 0) {
	            out.write(buf, 0, len);
	        }

	        // Close the streams
	        out.close();
	        zin.close();
	    } catch (IOException e) {
	    	throw new SinedException("Erro ao processar arquivo Zip.");
	    }
	}
}
