
/**
 * Integracao_Inscricoes_Processo_SeletivoCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package br.com.linkcom.sined.util.flexdevelopers;

    /**
     *  Integracao_Inscricoes_Processo_SeletivoCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class Integracao_Inscricoes_Processo_SeletivoCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public Integracao_Inscricoes_Processo_SeletivoCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public Integracao_Inscricoes_Processo_SeletivoCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for importa_Inscricao method
            * override this method for handling normal response from importa_Inscricao operation
            */
           public void receiveResultimporta_Inscricao(
                    br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from importa_Inscricao operation
           */
            public void receiveErrorimporta_Inscricao(java.lang.Exception e) {
            }
                


    }
    