
/**
 * Integracao_Inscricoes_Processo_SeletivoStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */
        package br.com.linkcom.sined.util.flexdevelopers;

        

        /*
        *  Integracao_Inscricoes_Processo_SeletivoStub java implementation
        */

        
        public class Integracao_Inscricoes_Processo_SeletivoStub extends org.apache.axis2.client.Stub
        {
        protected org.apache.axis2.description.AxisOperation[] _operations;

        //hashmaps to keep the fault mapping
        private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
        private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
        private java.util.HashMap faultMessageMap = new java.util.HashMap();

        private static int counter = 0;

        private static synchronized java.lang.String getUniqueSuffix(){
            // reset the counter if it is greater than 99999
            if (counter > 99999){
                counter = 0;
            }
            counter = counter + 1; 
            return java.lang.Long.toString(System.currentTimeMillis()) + "_" + counter;
        }

    
    private void populateAxisService() throws org.apache.axis2.AxisFault {

     //creating the Service with a unique name
     _service = new org.apache.axis2.description.AxisService("Integracao_Inscricoes_Processo_Seletivo" + getUniqueSuffix());
     addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO", "importa_Inscricao"));
	    _service.addOperation(__operation);
	    

	    
	    
            _operations[0]=__operation;
            
        
        }

    //populates the faults
    private void populateFaults(){
         


    }

    /**
      *Constructor that takes in a configContext
      */

    public Integracao_Inscricoes_Processo_SeletivoStub(org.apache.axis2.context.ConfigurationContext configurationContext,
       java.lang.String targetEndpoint)
       throws org.apache.axis2.AxisFault {
         this(configurationContext,targetEndpoint,false);
   }


   /**
     * Constructor that takes in a configContext  and useseperate listner
     */
   public Integracao_Inscricoes_Processo_SeletivoStub(org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
         //To populate AxisService
         populateAxisService();
         populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,_service);
        
	
        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
        
            //Set the soap version
            _serviceClient.getOptions().setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
        
    
    }

    /**
     * Default Constructor
     */
    public Integracao_Inscricoes_Processo_SeletivoStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
        
                    this(configurationContext,"http://www.flexnuvem.com.br/Rehagro/Integracao_Inscricoes_Processo_Seletivo.wso" );
                
    }

    /**
     * Default Constructor
     */
    public Integracao_Inscricoes_Processo_SeletivoStub() throws org.apache.axis2.AxisFault {
        
                    this("http://www.flexnuvem.com.br/Rehagro/Integracao_Inscricoes_Processo_Seletivo.wso" );
                
    }

    /**
     * Constructor taking the target endpoint
     */
    public Integracao_Inscricoes_Processo_SeletivoStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(null,targetEndpoint);
    }



        
                    /**
                     * Auto generated method signature
                     * Função para importação de inscrições do Processo Seletivo. Para cada inscrição, retorna o nome do processo, número de inscrição e a mensagem de erro. Mensagem de erro vazia significa gravado com sucesso
                     * @see br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_Seletivo#importa_Inscricao
                     * @param importa_Inscricao0
                    
                     */

                    

                            public  br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse importa_Inscricao(

                            br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao importa_Inscricao0)
                        

                    throws java.rmi.RemoteException
                    
                    {
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
              _operationClient.getOptions().setAction("https://server4.flexnuvem.com.br/REHAGRO/Integracao_Inscricoes_Processo_SeletivoSoapType/Importa_InscricaoRequest");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    importa_Inscricao0,
                                                    optimizeContent(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                    "importa_Inscricao")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * Função para importação de inscrições do Processo Seletivo. Para cada inscrição, retorna o nome do processo, número de inscrição e a mensagem de erro. Mensagem de erro vazia significa gravado com sucesso
                * @see br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_Seletivo#startimporta_Inscricao
                    * @param importa_Inscricao0
                
                */
                public  void startimporta_Inscricao(

                 br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao importa_Inscricao0,

                  final br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
             _operationClient.getOptions().setAction("https://server4.flexnuvem.com.br/REHAGRO/Integracao_Inscricoes_Processo_SeletivoSoapType/Importa_InscricaoRequest");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    importa_Inscricao0,
                                                    optimizeContent(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                    "importa_Inscricao")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultimporta_Inscricao(
                                        (br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorimporta_Inscricao(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
					
										            callback.receiveErrorimporta_Inscricao(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorimporta_Inscricao(f);
                                            }
									    } else {
										    callback.receiveErrorimporta_Inscricao(f);
									    }
									} else {
									    callback.receiveErrorimporta_Inscricao(f);
									}
								} else {
								    callback.receiveErrorimporta_Inscricao(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorimporta_Inscricao(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[0].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[0].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                


       /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
       private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
       return returnMap;
    }

    
    
    private javax.xml.namespace.QName[] opNameArray = null;
    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;   
            }
        }
        return false;
    }
     //http://www.flexnuvem.com.br/Rehagro/Integracao_Inscricoes_Processo_Seletivo.wso
        public static class ArrayOfInscricaoProcessoSeletivo_Retorno
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ArrayOfInscricaoProcessoSeletivo_Retorno
                Namespace URI = https://server4.flexnuvem.com.br/REHAGRO
                Namespace Prefix = 
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for InscricaoProcessoSeletivo_Retorno
                        * This was an Array!
                        */

                        
                                    protected InscricaoProcessoSeletivo_Retorno[] localInscricaoProcessoSeletivo_Retorno ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInscricaoProcessoSeletivo_RetornoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return InscricaoProcessoSeletivo_Retorno[]
                           */
                           public  InscricaoProcessoSeletivo_Retorno[] getInscricaoProcessoSeletivo_Retorno(){
                               return localInscricaoProcessoSeletivo_Retorno;
                           }

                           
                        


                               
                              /**
                               * validate the array for InscricaoProcessoSeletivo_Retorno
                               */
                              protected void validateInscricaoProcessoSeletivo_Retorno(InscricaoProcessoSeletivo_Retorno[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InscricaoProcessoSeletivo_Retorno
                              */
                              public void setInscricaoProcessoSeletivo_Retorno(InscricaoProcessoSeletivo_Retorno[] param){
                              
                                   validateInscricaoProcessoSeletivo_Retorno(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localInscricaoProcessoSeletivo_RetornoTracker = true;
                                          } else {
                                             localInscricaoProcessoSeletivo_RetornoTracker = true;
                                                 
                                          }
                                      
                                      this.localInscricaoProcessoSeletivo_Retorno=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param InscricaoProcessoSeletivo_Retorno
                             */
                             public void addInscricaoProcessoSeletivo_Retorno(InscricaoProcessoSeletivo_Retorno param){
                                   if (localInscricaoProcessoSeletivo_Retorno == null){
                                   localInscricaoProcessoSeletivo_Retorno = new InscricaoProcessoSeletivo_Retorno[]{};
                                   }

                            
                                 //update the setting tracker
                                localInscricaoProcessoSeletivo_RetornoTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInscricaoProcessoSeletivo_Retorno);
                               list.add(param);
                               this.localInscricaoProcessoSeletivo_Retorno =
                             (InscricaoProcessoSeletivo_Retorno[])list.toArray(
                            new InscricaoProcessoSeletivo_Retorno[list.size()]);

                             }
                             

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       ArrayOfInscricaoProcessoSeletivo_Retorno.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ArrayOfInscricaoProcessoSeletivo_Retorno",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ArrayOfInscricaoProcessoSeletivo_Retorno",
                           xmlWriter);
                   }

               
                   }
                if (localInscricaoProcessoSeletivo_RetornoTracker){
                                       if (localInscricaoProcessoSeletivo_Retorno!=null){
                                            for (int i = 0;i < localInscricaoProcessoSeletivo_Retorno.length;i++){
                                                if (localInscricaoProcessoSeletivo_Retorno[i] != null){
                                                 localInscricaoProcessoSeletivo_Retorno[i].serialize(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo_Retorno"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                            // write null attribute
                                                            java.lang.String namespace2 = "https://server4.flexnuvem.com.br/REHAGRO";
                                                            if (! namespace2.equals("")) {
                                                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                                if (prefix2 == null) {
                                                                    prefix2 = generatePrefix(namespace2);

                                                                    xmlWriter.writeStartElement(prefix2,"InscricaoProcessoSeletivo_Retorno", namespace2);
                                                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                                                    xmlWriter.setPrefix(prefix2, namespace2);

                                                                } else {
                                                                    xmlWriter.writeStartElement(namespace2,"InscricaoProcessoSeletivo_Retorno");
                                                                }

                                                            } else {
                                                                xmlWriter.writeStartElement("InscricaoProcessoSeletivo_Retorno");
                                                            }

                                                           // write the nil attribute
                                                           writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                           xmlWriter.writeEndElement();
                                                    
                                                }

                                            }
                                     } else {
                                        
                                                // write null attribute
                                                java.lang.String namespace2 = "https://server4.flexnuvem.com.br/REHAGRO";
                                                if (! namespace2.equals("")) {
                                                    java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                    if (prefix2 == null) {
                                                        prefix2 = generatePrefix(namespace2);

                                                        xmlWriter.writeStartElement(prefix2,"InscricaoProcessoSeletivo_Retorno", namespace2);
                                                        xmlWriter.writeNamespace(prefix2, namespace2);
                                                        xmlWriter.setPrefix(prefix2, namespace2);

                                                    } else {
                                                        xmlWriter.writeStartElement(namespace2,"InscricaoProcessoSeletivo_Retorno");
                                                    }

                                                } else {
                                                    xmlWriter.writeStartElement("InscricaoProcessoSeletivo_Retorno");
                                                }

                                               // write the nil attribute
                                               writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                               xmlWriter.writeEndElement();
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localInscricaoProcessoSeletivo_RetornoTracker){
                             if (localInscricaoProcessoSeletivo_Retorno!=null) {
                                 for (int i = 0;i < localInscricaoProcessoSeletivo_Retorno.length;i++){

                                    if (localInscricaoProcessoSeletivo_Retorno[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo_Retorno"));
                                         elementList.add(localInscricaoProcessoSeletivo_Retorno[i]);
                                    } else {
                                        
                                                elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo_Retorno"));
                                                elementList.add(null);
                                            
                                    }

                                 }
                             } else {
                                 
                                        elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo_Retorno"));
                                        elementList.add(localInscricaoProcessoSeletivo_Retorno);
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ArrayOfInscricaoProcessoSeletivo_Retorno parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ArrayOfInscricaoProcessoSeletivo_Retorno object =
                new ArrayOfInscricaoProcessoSeletivo_Retorno();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ArrayOfInscricaoProcessoSeletivo_Retorno".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ArrayOfInscricaoProcessoSeletivo_Retorno)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo_Retorno").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    
                                                          nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                          if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                              list1.add(null);
                                                              reader.next();
                                                          } else {
                                                        list1.add(InscricaoProcessoSeletivo_Retorno.Factory.parse(reader));
                                                                }
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo_Retorno").equals(reader.getName())){
                                                                    
                                                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                                          list1.add(null);
                                                                          reader.next();
                                                                      } else {
                                                                    list1.add(InscricaoProcessoSeletivo_Retorno.Factory.parse(reader));
                                                                        }
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInscricaoProcessoSeletivo_Retorno((InscricaoProcessoSeletivo_Retorno[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                InscricaoProcessoSeletivo_Retorno.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class ArrayOfInscricaoProcessoSeletivo
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ArrayOfInscricaoProcessoSeletivo
                Namespace URI = https://server4.flexnuvem.com.br/REHAGRO
                Namespace Prefix = 
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for InscricaoProcessoSeletivo
                        * This was an Array!
                        */

                        
                                    protected InscricaoProcessoSeletivo[] localInscricaoProcessoSeletivo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInscricaoProcessoSeletivoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return InscricaoProcessoSeletivo[]
                           */
                           public  InscricaoProcessoSeletivo[] getInscricaoProcessoSeletivo(){
                               return localInscricaoProcessoSeletivo;
                           }

                           
                        


                               
                              /**
                               * validate the array for InscricaoProcessoSeletivo
                               */
                              protected void validateInscricaoProcessoSeletivo(InscricaoProcessoSeletivo[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InscricaoProcessoSeletivo
                              */
                              public void setInscricaoProcessoSeletivo(InscricaoProcessoSeletivo[] param){
                              
                                   validateInscricaoProcessoSeletivo(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localInscricaoProcessoSeletivoTracker = true;
                                          } else {
                                             localInscricaoProcessoSeletivoTracker = true;
                                                 
                                          }
                                      
                                      this.localInscricaoProcessoSeletivo=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param InscricaoProcessoSeletivo
                             */
                             public void addInscricaoProcessoSeletivo(InscricaoProcessoSeletivo param){
                                   if (localInscricaoProcessoSeletivo == null){
                                   localInscricaoProcessoSeletivo = new InscricaoProcessoSeletivo[]{};
                                   }

                            
                                 //update the setting tracker
                                localInscricaoProcessoSeletivoTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInscricaoProcessoSeletivo);
                               list.add(param);
                               this.localInscricaoProcessoSeletivo =
                             (InscricaoProcessoSeletivo[])list.toArray(
                            new InscricaoProcessoSeletivo[list.size()]);

                             }
                             

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       ArrayOfInscricaoProcessoSeletivo.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ArrayOfInscricaoProcessoSeletivo",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ArrayOfInscricaoProcessoSeletivo",
                           xmlWriter);
                   }

               
                   }
                if (localInscricaoProcessoSeletivoTracker){
                                       if (localInscricaoProcessoSeletivo!=null){
                                            for (int i = 0;i < localInscricaoProcessoSeletivo.length;i++){
                                                if (localInscricaoProcessoSeletivo[i] != null){
                                                 localInscricaoProcessoSeletivo[i].serialize(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                            // write null attribute
                                                            java.lang.String namespace2 = "https://server4.flexnuvem.com.br/REHAGRO";
                                                            if (! namespace2.equals("")) {
                                                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                                if (prefix2 == null) {
                                                                    prefix2 = generatePrefix(namespace2);

                                                                    xmlWriter.writeStartElement(prefix2,"InscricaoProcessoSeletivo", namespace2);
                                                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                                                    xmlWriter.setPrefix(prefix2, namespace2);

                                                                } else {
                                                                    xmlWriter.writeStartElement(namespace2,"InscricaoProcessoSeletivo");
                                                                }

                                                            } else {
                                                                xmlWriter.writeStartElement("InscricaoProcessoSeletivo");
                                                            }

                                                           // write the nil attribute
                                                           writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                           xmlWriter.writeEndElement();
                                                    
                                                }

                                            }
                                     } else {
                                        
                                                // write null attribute
                                                java.lang.String namespace2 = "https://server4.flexnuvem.com.br/REHAGRO";
                                                if (! namespace2.equals("")) {
                                                    java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                    if (prefix2 == null) {
                                                        prefix2 = generatePrefix(namespace2);

                                                        xmlWriter.writeStartElement(prefix2,"InscricaoProcessoSeletivo", namespace2);
                                                        xmlWriter.writeNamespace(prefix2, namespace2);
                                                        xmlWriter.setPrefix(prefix2, namespace2);

                                                    } else {
                                                        xmlWriter.writeStartElement(namespace2,"InscricaoProcessoSeletivo");
                                                    }

                                                } else {
                                                    xmlWriter.writeStartElement("InscricaoProcessoSeletivo");
                                                }

                                               // write the nil attribute
                                               writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                               xmlWriter.writeEndElement();
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localInscricaoProcessoSeletivoTracker){
                             if (localInscricaoProcessoSeletivo!=null) {
                                 for (int i = 0;i < localInscricaoProcessoSeletivo.length;i++){

                                    if (localInscricaoProcessoSeletivo[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo"));
                                         elementList.add(localInscricaoProcessoSeletivo[i]);
                                    } else {
                                        
                                                elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo"));
                                                elementList.add(null);
                                            
                                    }

                                 }
                             } else {
                                 
                                        elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                          "InscricaoProcessoSeletivo"));
                                        elementList.add(localInscricaoProcessoSeletivo);
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ArrayOfInscricaoProcessoSeletivo parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ArrayOfInscricaoProcessoSeletivo object =
                new ArrayOfInscricaoProcessoSeletivo();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ArrayOfInscricaoProcessoSeletivo".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ArrayOfInscricaoProcessoSeletivo)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    
                                                          nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                          if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                              list1.add(null);
                                                              reader.next();
                                                          } else {
                                                        list1.add(InscricaoProcessoSeletivo.Factory.parse(reader));
                                                                }
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricaoProcessoSeletivo").equals(reader.getName())){
                                                                    
                                                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                                          list1.add(null);
                                                                          reader.next();
                                                                      } else {
                                                                    list1.add(InscricaoProcessoSeletivo.Factory.parse(reader));
                                                                        }
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInscricaoProcessoSeletivo((InscricaoProcessoSeletivo[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                InscricaoProcessoSeletivo.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class Importa_InscricaoResponse
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "https://server4.flexnuvem.com.br/REHAGRO",
                "Importa_InscricaoResponse",
                "");

            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Importa_InscricaoResult
                        */

                        
                                    protected ArrayOfInscricaoProcessoSeletivo_Retorno localImporta_InscricaoResult ;
                                

                           /**
                           * Auto generated getter method
                           * @return ArrayOfInscricaoProcessoSeletivo_Retorno
                           */
                           public  ArrayOfInscricaoProcessoSeletivo_Retorno getImporta_InscricaoResult(){
                               return localImporta_InscricaoResult;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Importa_InscricaoResult
                               */
                               public void setImporta_InscricaoResult(ArrayOfInscricaoProcessoSeletivo_Retorno param){
                            
                                            this.localImporta_InscricaoResult=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
                org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       Importa_InscricaoResponse.this.serialize(MY_QNAME,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               MY_QNAME,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Importa_InscricaoResponse",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Importa_InscricaoResponse",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localImporta_InscricaoResult==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Importa_InscricaoResult cannot be null!!");
                                            }
                                           localImporta_InscricaoResult.serialize(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Importa_InscricaoResult"),
                                               factory,xmlWriter);
                                        
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Importa_InscricaoResult"));
                            
                            
                                    if (localImporta_InscricaoResult==null){
                                         throw new org.apache.axis2.databinding.ADBException("Importa_InscricaoResult cannot be null!!");
                                    }
                                    elementList.add(localImporta_InscricaoResult);
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Importa_InscricaoResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Importa_InscricaoResponse object =
                new Importa_InscricaoResponse();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Importa_InscricaoResponse".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Importa_InscricaoResponse)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Importa_InscricaoResult").equals(reader.getName())){
                                
                                                object.setImporta_InscricaoResult(ArrayOfInscricaoProcessoSeletivo_Retorno.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class Importa_Inscricao
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "https://server4.flexnuvem.com.br/REHAGRO",
                "Importa_Inscricao",
                "");

            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for SUsuario
                        */

                        
                                    protected java.lang.String localSUsuario ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSUsuario(){
                               return localSUsuario;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SUsuario
                               */
                               public void setSUsuario(java.lang.String param){
                            
                                            this.localSUsuario=param;
                                    

                               }
                            

                        /**
                        * field for SSenha
                        */

                        
                                    protected java.lang.String localSSenha ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSSenha(){
                               return localSSenha;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SSenha
                               */
                               public void setSSenha(java.lang.String param){
                            
                                            this.localSSenha=param;
                                    

                               }
                            

                        /**
                        * field for InscricoesProcessoSeletivo
                        */

                        
                                    protected ArrayOfInscricaoProcessoSeletivo localInscricoesProcessoSeletivo ;
                                

                           /**
                           * Auto generated getter method
                           * @return ArrayOfInscricaoProcessoSeletivo
                           */
                           public  ArrayOfInscricaoProcessoSeletivo getInscricoesProcessoSeletivo(){
                               return localInscricoesProcessoSeletivo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InscricoesProcessoSeletivo
                               */
                               public void setInscricoesProcessoSeletivo(ArrayOfInscricaoProcessoSeletivo param){
                            
                                            this.localInscricoesProcessoSeletivo=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
                org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       Importa_Inscricao.this.serialize(MY_QNAME,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               MY_QNAME,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Importa_Inscricao",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Importa_Inscricao",
                           xmlWriter);
                   }

               
                   }
               
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"sUsuario", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"sUsuario");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("sUsuario");
                                    }
                                

                                          if (localSUsuario==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("sUsuario cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSUsuario);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"sSenha", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"sSenha");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("sSenha");
                                    }
                                

                                          if (localSSenha==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("sSenha cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSSenha);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                            if (localInscricoesProcessoSeletivo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("InscricoesProcessoSeletivo cannot be null!!");
                                            }
                                           localInscricoesProcessoSeletivo.serialize(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricoesProcessoSeletivo"),
                                               factory,xmlWriter);
                                        
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "sUsuario"));
                                 
                                        if (localSUsuario != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSUsuario));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("sUsuario cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "sSenha"));
                                 
                                        if (localSSenha != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSSenha));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("sSenha cannot be null!!");
                                        }
                                    
                            elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "InscricoesProcessoSeletivo"));
                            
                            
                                    if (localInscricoesProcessoSeletivo==null){
                                         throw new org.apache.axis2.databinding.ADBException("InscricoesProcessoSeletivo cannot be null!!");
                                    }
                                    elementList.add(localInscricoesProcessoSeletivo);
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Importa_Inscricao parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Importa_Inscricao object =
                new Importa_Inscricao();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Importa_Inscricao".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Importa_Inscricao)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","sUsuario").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSUsuario(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","sSenha").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSSenha(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","InscricoesProcessoSeletivo").equals(reader.getName())){
                                
                                                object.setInscricoesProcessoSeletivo(ArrayOfInscricaoProcessoSeletivo.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class InscricaoProcessoSeletivo
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = InscricaoProcessoSeletivo
                Namespace URI = https://server4.flexnuvem.com.br/REHAGRO
                Namespace Prefix = 
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Nome_do_Processo
                        */

                        
                                    protected java.lang.String localNome_do_Processo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNome_do_Processo(){
                               return localNome_do_Processo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nome_do_Processo
                               */
                               public void setNome_do_Processo(java.lang.String param){
                            
                                            this.localNome_do_Processo=param;
                                    

                               }
                            

                        /**
                        * field for Ano_Processo
                        */

                        
                                    protected int localAno_Processo ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getAno_Processo(){
                               return localAno_Processo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Ano_Processo
                               */
                               public void setAno_Processo(int param){
                            
                                            this.localAno_Processo=param;
                                    

                               }
                            

                        /**
                        * field for Numero_Processo
                        */

                        
                                    protected int localNumero_Processo ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getNumero_Processo(){
                               return localNumero_Processo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Numero_Processo
                               */
                               public void setNumero_Processo(int param){
                            
                                            this.localNumero_Processo=param;
                                    

                               }
                            

                        /**
                        * field for Numero_Inscricao
                        */

                        
                                    protected int localNumero_Inscricao ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getNumero_Inscricao(){
                               return localNumero_Inscricao;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Numero_Inscricao
                               */
                               public void setNumero_Inscricao(int param){
                            
                                            this.localNumero_Inscricao=param;
                                    

                               }
                            

                        /**
                        * field for Data_da_Prova
                        */

                        
                                    protected int localData_da_Prova ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getData_da_Prova(){
                               return localData_da_Prova;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Data_da_Prova
                               */
                               public void setData_da_Prova(int param){
                            
                                            this.localData_da_Prova=param;
                                    

                               }
                            

                        /**
                        * field for Nome_do_Aluno
                        */

                        
                                    protected java.lang.String localNome_do_Aluno ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNome_do_Aluno(){
                               return localNome_do_Aluno;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nome_do_Aluno
                               */
                               public void setNome_do_Aluno(java.lang.String param){
                            
                                            this.localNome_do_Aluno=param;
                                    

                               }
                            

                        /**
                        * field for Sexo
                        */

                        
                                    protected java.lang.String localSexo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSexo(){
                               return localSexo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sexo
                               */
                               public void setSexo(java.lang.String param){
                            
                                            this.localSexo=param;
                                    

                               }
                            

                        /**
                        * field for Data_de_Nascimento
                        */

                        
                                    protected int localData_de_Nascimento ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getData_de_Nascimento(){
                               return localData_de_Nascimento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Data_de_Nascimento
                               */
                               public void setData_de_Nascimento(int param){
                            
                                            this.localData_de_Nascimento=param;
                                    

                               }
                            

                        /**
                        * field for Nome_do_Pai
                        */

                        
                                    protected java.lang.String localNome_do_Pai ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNome_do_Pai(){
                               return localNome_do_Pai;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nome_do_Pai
                               */
                               public void setNome_do_Pai(java.lang.String param){
                            
                                            this.localNome_do_Pai=param;
                                    

                               }
                            

                        /**
                        * field for Nome_da_Mae
                        */

                        
                                    protected java.lang.String localNome_da_Mae ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNome_da_Mae(){
                               return localNome_da_Mae;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nome_da_Mae
                               */
                               public void setNome_da_Mae(java.lang.String param){
                            
                                            this.localNome_da_Mae=param;
                                    

                               }
                            

                        /**
                        * field for Estado_Civil
                        */

                        
                                    protected int localEstado_Civil ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getEstado_Civil(){
                               return localEstado_Civil;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Estado_Civil
                               */
                               public void setEstado_Civil(int param){
                            
                                            this.localEstado_Civil=param;
                                    

                               }
                            

                        /**
                        * field for Raca
                        */

                        
                                    protected java.lang.String localRaca ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRaca(){
                               return localRaca;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Raca
                               */
                               public void setRaca(java.lang.String param){
                            
                                            this.localRaca=param;
                                    

                               }
                            

                        /**
                        * field for CPF
                        */

                        
                                    protected java.lang.String localCPF ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCPF(){
                               return localCPF;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CPF
                               */
                               public void setCPF(java.lang.String param){
                            
                                            this.localCPF=param;
                                    

                               }
                            

                        /**
                        * field for RG
                        */

                        
                                    protected java.lang.String localRG ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRG(){
                               return localRG;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RG
                               */
                               public void setRG(java.lang.String param){
                            
                                            this.localRG=param;
                                    

                               }
                            

                        /**
                        * field for RG_SSP
                        */

                        
                                    protected java.lang.String localRG_SSP ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRG_SSP(){
                               return localRG_SSP;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RG_SSP
                               */
                               public void setRG_SSP(java.lang.String param){
                            
                                            this.localRG_SSP=param;
                                    

                               }
                            

                        /**
                        * field for Titulo_de_Eleitor
                        */

                        
                                    protected java.lang.String localTitulo_de_Eleitor ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitulo_de_Eleitor(){
                               return localTitulo_de_Eleitor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Titulo_de_Eleitor
                               */
                               public void setTitulo_de_Eleitor(java.lang.String param){
                            
                                            this.localTitulo_de_Eleitor=param;
                                    

                               }
                            

                        /**
                        * field for Zona_do_Titulo_de_Eleitor
                        */

                        
                                    protected java.lang.String localZona_do_Titulo_de_Eleitor ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getZona_do_Titulo_de_Eleitor(){
                               return localZona_do_Titulo_de_Eleitor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zona_do_Titulo_de_Eleitor
                               */
                               public void setZona_do_Titulo_de_Eleitor(java.lang.String param){
                            
                                            this.localZona_do_Titulo_de_Eleitor=param;
                                    

                               }
                            

                        /**
                        * field for Expedicao_do_Titulo_de_Eleitor
                        */

                        
                                    protected java.lang.String localExpedicao_do_Titulo_de_Eleitor ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExpedicao_do_Titulo_de_Eleitor(){
                               return localExpedicao_do_Titulo_de_Eleitor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Expedicao_do_Titulo_de_Eleitor
                               */
                               public void setExpedicao_do_Titulo_de_Eleitor(java.lang.String param){
                            
                                            this.localExpedicao_do_Titulo_de_Eleitor=param;
                                    

                               }
                            

                        /**
                        * field for Certificado_Militar
                        */

                        
                                    protected java.lang.String localCertificado_Militar ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCertificado_Militar(){
                               return localCertificado_Militar;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Certificado_Militar
                               */
                               public void setCertificado_Militar(java.lang.String param){
                            
                                            this.localCertificado_Militar=param;
                                    

                               }
                            

                        /**
                        * field for Expedicao_Certificado_Militar
                        */

                        
                                    protected java.lang.String localExpedicao_Certificado_Militar ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExpedicao_Certificado_Militar(){
                               return localExpedicao_Certificado_Militar;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Expedicao_Certificado_Militar
                               */
                               public void setExpedicao_Certificado_Militar(java.lang.String param){
                            
                                            this.localExpedicao_Certificado_Militar=param;
                                    

                               }
                            

                        /**
                        * field for Cidade_de_Nascimento
                        */

                        
                                    protected java.lang.String localCidade_de_Nascimento ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCidade_de_Nascimento(){
                               return localCidade_de_Nascimento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cidade_de_Nascimento
                               */
                               public void setCidade_de_Nascimento(java.lang.String param){
                            
                                            this.localCidade_de_Nascimento=param;
                                    

                               }
                            

                        /**
                        * field for Sigla_Estado_de_Nascmento
                        */

                        
                                    protected java.lang.String localSigla_Estado_de_Nascmento ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSigla_Estado_de_Nascmento(){
                               return localSigla_Estado_de_Nascmento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sigla_Estado_de_Nascmento
                               */
                               public void setSigla_Estado_de_Nascmento(java.lang.String param){
                            
                                            this.localSigla_Estado_de_Nascmento=param;
                                    

                               }
                            

                        /**
                        * field for Nacionalidade
                        */

                        
                                    protected java.lang.String localNacionalidade ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNacionalidade(){
                               return localNacionalidade;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nacionalidade
                               */
                               public void setNacionalidade(java.lang.String param){
                            
                                            this.localNacionalidade=param;
                                    

                               }
                            

                        /**
                        * field for Deficiencia_Fisica
                        */

                        
                                    protected int localDeficiencia_Fisica ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDeficiencia_Fisica(){
                               return localDeficiencia_Fisica;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Deficiencia_Fisica
                               */
                               public void setDeficiencia_Fisica(int param){
                            
                                            this.localDeficiencia_Fisica=param;
                                    

                               }
                            

                        /**
                        * field for Deficiencia_Auditiva
                        */

                        
                                    protected int localDeficiencia_Auditiva ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDeficiencia_Auditiva(){
                               return localDeficiencia_Auditiva;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Deficiencia_Auditiva
                               */
                               public void setDeficiencia_Auditiva(int param){
                            
                                            this.localDeficiencia_Auditiva=param;
                                    

                               }
                            

                        /**
                        * field for Deficiencia_Visual
                        */

                        
                                    protected int localDeficiencia_Visual ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDeficiencia_Visual(){
                               return localDeficiencia_Visual;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Deficiencia_Visual
                               */
                               public void setDeficiencia_Visual(int param){
                            
                                            this.localDeficiencia_Visual=param;
                                    

                               }
                            

                        /**
                        * field for CEP
                        */

                        
                                    protected java.lang.String localCEP ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCEP(){
                               return localCEP;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CEP
                               */
                               public void setCEP(java.lang.String param){
                            
                                            this.localCEP=param;
                                    

                               }
                            

                        /**
                        * field for Tipo_Logradouro
                        */

                        
                                    protected java.lang.String localTipo_Logradouro ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTipo_Logradouro(){
                               return localTipo_Logradouro;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tipo_Logradouro
                               */
                               public void setTipo_Logradouro(java.lang.String param){
                            
                                            this.localTipo_Logradouro=param;
                                    

                               }
                            

                        /**
                        * field for Endereco
                        */

                        
                                    protected java.lang.String localEndereco ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEndereco(){
                               return localEndereco;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Endereco
                               */
                               public void setEndereco(java.lang.String param){
                            
                                            this.localEndereco=param;
                                    

                               }
                            

                        /**
                        * field for Bairro
                        */

                        
                                    protected java.lang.String localBairro ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBairro(){
                               return localBairro;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bairro
                               */
                               public void setBairro(java.lang.String param){
                            
                                            this.localBairro=param;
                                    

                               }
                            

                        /**
                        * field for Cidade
                        */

                        
                                    protected java.lang.String localCidade ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCidade(){
                               return localCidade;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cidade
                               */
                               public void setCidade(java.lang.String param){
                            
                                            this.localCidade=param;
                                    

                               }
                            

                        /**
                        * field for Sigla_Estado
                        */

                        
                                    protected java.lang.String localSigla_Estado ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSigla_Estado(){
                               return localSigla_Estado;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sigla_Estado
                               */
                               public void setSigla_Estado(java.lang.String param){
                            
                                            this.localSigla_Estado=param;
                                    

                               }
                            

                        /**
                        * field for Pais
                        */

                        
                                    protected java.lang.String localPais ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPais(){
                               return localPais;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Pais
                               */
                               public void setPais(java.lang.String param){
                            
                                            this.localPais=param;
                                    

                               }
                            

                        /**
                        * field for Numero
                        */

                        
                                    protected java.lang.String localNumero ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNumero(){
                               return localNumero;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Numero
                               */
                               public void setNumero(java.lang.String param){
                            
                                            this.localNumero=param;
                                    

                               }
                            

                        /**
                        * field for Complemento
                        */

                        
                                    protected java.lang.String localComplemento ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getComplemento(){
                               return localComplemento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Complemento
                               */
                               public void setComplemento(java.lang.String param){
                            
                                            this.localComplemento=param;
                                    

                               }
                            

                        /**
                        * field for DDD_Telefone
                        */

                        
                                    protected java.lang.String localDDD_Telefone ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDDD_Telefone(){
                               return localDDD_Telefone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DDD_Telefone
                               */
                               public void setDDD_Telefone(java.lang.String param){
                            
                                            this.localDDD_Telefone=param;
                                    

                               }
                            

                        /**
                        * field for Telefone
                        */

                        
                                    protected java.lang.String localTelefone ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTelefone(){
                               return localTelefone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Telefone
                               */
                               public void setTelefone(java.lang.String param){
                            
                                            this.localTelefone=param;
                                    

                               }
                            

                        /**
                        * field for DDD_Celular
                        */

                        
                                    protected java.lang.String localDDD_Celular ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDDD_Celular(){
                               return localDDD_Celular;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DDD_Celular
                               */
                               public void setDDD_Celular(java.lang.String param){
                            
                                            this.localDDD_Celular=param;
                                    

                               }
                            

                        /**
                        * field for Celular
                        */

                        
                                    protected java.lang.String localCelular ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCelular(){
                               return localCelular;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Celular
                               */
                               public void setCelular(java.lang.String param){
                            
                                            this.localCelular=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for Caixa_Postal
                        */

                        
                                    protected java.lang.String localCaixa_Postal ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCaixa_Postal(){
                               return localCaixa_Postal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Caixa_Postal
                               */
                               public void setCaixa_Postal(java.lang.String param){
                            
                                            this.localCaixa_Postal=param;
                                    

                               }
                            

                        /**
                        * field for Usar_Endereco_Comercial
                        */

                        
                                    protected int localUsar_Endereco_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getUsar_Endereco_Comercial(){
                               return localUsar_Endereco_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Usar_Endereco_Comercial
                               */
                               public void setUsar_Endereco_Comercial(int param){
                            
                                            this.localUsar_Endereco_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for CEP_Comercial
                        */

                        
                                    protected java.lang.String localCEP_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCEP_Comercial(){
                               return localCEP_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CEP_Comercial
                               */
                               public void setCEP_Comercial(java.lang.String param){
                            
                                            this.localCEP_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Tipo_Logradouro_Comercial
                        */

                        
                                    protected java.lang.String localTipo_Logradouro_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTipo_Logradouro_Comercial(){
                               return localTipo_Logradouro_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tipo_Logradouro_Comercial
                               */
                               public void setTipo_Logradouro_Comercial(java.lang.String param){
                            
                                            this.localTipo_Logradouro_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Endereco_Comercial
                        */

                        
                                    protected java.lang.String localEndereco_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEndereco_Comercial(){
                               return localEndereco_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Endereco_Comercial
                               */
                               public void setEndereco_Comercial(java.lang.String param){
                            
                                            this.localEndereco_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Bairro_Comercial
                        */

                        
                                    protected java.lang.String localBairro_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBairro_Comercial(){
                               return localBairro_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bairro_Comercial
                               */
                               public void setBairro_Comercial(java.lang.String param){
                            
                                            this.localBairro_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Cidade_Comercial
                        */

                        
                                    protected java.lang.String localCidade_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCidade_Comercial(){
                               return localCidade_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cidade_Comercial
                               */
                               public void setCidade_Comercial(java.lang.String param){
                            
                                            this.localCidade_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Sigla_Estado_Comercial
                        */

                        
                                    protected java.lang.String localSigla_Estado_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSigla_Estado_Comercial(){
                               return localSigla_Estado_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sigla_Estado_Comercial
                               */
                               public void setSigla_Estado_Comercial(java.lang.String param){
                            
                                            this.localSigla_Estado_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Pais_Comercial
                        */

                        
                                    protected java.lang.String localPais_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPais_Comercial(){
                               return localPais_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Pais_Comercial
                               */
                               public void setPais_Comercial(java.lang.String param){
                            
                                            this.localPais_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Numero_Comercial
                        */

                        
                                    protected java.lang.String localNumero_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNumero_Comercial(){
                               return localNumero_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Numero_Comercial
                               */
                               public void setNumero_Comercial(java.lang.String param){
                            
                                            this.localNumero_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Complemento_Comercial
                        */

                        
                                    protected java.lang.String localComplemento_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getComplemento_Comercial(){
                               return localComplemento_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Complemento_Comercial
                               */
                               public void setComplemento_Comercial(java.lang.String param){
                            
                                            this.localComplemento_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for DDD_Telefone_Comercial
                        */

                        
                                    protected java.lang.String localDDD_Telefone_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDDD_Telefone_Comercial(){
                               return localDDD_Telefone_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DDD_Telefone_Comercial
                               */
                               public void setDDD_Telefone_Comercial(java.lang.String param){
                            
                                            this.localDDD_Telefone_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Telefone_Comercial
                        */

                        
                                    protected java.lang.String localTelefone_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTelefone_Comercial(){
                               return localTelefone_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Telefone_Comercial
                               */
                               public void setTelefone_Comercial(java.lang.String param){
                            
                                            this.localTelefone_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Email_Comercial
                        */

                        
                                    protected java.lang.String localEmail_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail_Comercial(){
                               return localEmail_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email_Comercial
                               */
                               public void setEmail_Comercial(java.lang.String param){
                            
                                            this.localEmail_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Caixa_postal_Comercial
                        */

                        
                                    protected java.lang.String localCaixa_postal_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCaixa_postal_Comercial(){
                               return localCaixa_postal_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Caixa_postal_Comercial
                               */
                               public void setCaixa_postal_Comercial(java.lang.String param){
                            
                                            this.localCaixa_postal_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Empresa
                        */

                        
                                    protected java.lang.String localEmpresa ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmpresa(){
                               return localEmpresa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Empresa
                               */
                               public void setEmpresa(java.lang.String param){
                            
                                            this.localEmpresa=param;
                                    

                               }
                            

                        /**
                        * field for Contato_Comercial
                        */

                        
                                    protected java.lang.String localContato_Comercial ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getContato_Comercial(){
                               return localContato_Comercial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contato_Comercial
                               */
                               public void setContato_Comercial(java.lang.String param){
                            
                                            this.localContato_Comercial=param;
                                    

                               }
                            

                        /**
                        * field for Escola_Ensino_Medio
                        */

                        
                                    protected java.lang.String localEscola_Ensino_Medio ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEscola_Ensino_Medio(){
                               return localEscola_Ensino_Medio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Escola_Ensino_Medio
                               */
                               public void setEscola_Ensino_Medio(java.lang.String param){
                            
                                            this.localEscola_Ensino_Medio=param;
                                    

                               }
                            

                        /**
                        * field for Cidade_Escola_Ensino_Medio
                        */

                        
                                    protected java.lang.String localCidade_Escola_Ensino_Medio ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCidade_Escola_Ensino_Medio(){
                               return localCidade_Escola_Ensino_Medio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cidade_Escola_Ensino_Medio
                               */
                               public void setCidade_Escola_Ensino_Medio(java.lang.String param){
                            
                                            this.localCidade_Escola_Ensino_Medio=param;
                                    

                               }
                            

                        /**
                        * field for Sigla_EstadoEscola_Ensino_Medio
                        */

                        
                                    protected java.lang.String localSigla_EstadoEscola_Ensino_Medio ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSigla_EstadoEscola_Ensino_Medio(){
                               return localSigla_EstadoEscola_Ensino_Medio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sigla_EstadoEscola_Ensino_Medio
                               */
                               public void setSigla_EstadoEscola_Ensino_Medio(java.lang.String param){
                            
                                            this.localSigla_EstadoEscola_Ensino_Medio=param;
                                    

                               }
                            

                        /**
                        * field for Ano_conclusao_Ensino_Medio
                        */

                        
                                    protected int localAno_conclusao_Ensino_Medio ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getAno_conclusao_Ensino_Medio(){
                               return localAno_conclusao_Ensino_Medio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Ano_conclusao_Ensino_Medio
                               */
                               public void setAno_conclusao_Ensino_Medio(int param){
                            
                                            this.localAno_conclusao_Ensino_Medio=param;
                                    

                               }
                            

                        /**
                        * field for Faculdade
                        */

                        
                                    protected java.lang.String localFaculdade ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFaculdade(){
                               return localFaculdade;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Faculdade
                               */
                               public void setFaculdade(java.lang.String param){
                            
                                            this.localFaculdade=param;
                                    

                               }
                            

                        /**
                        * field for Cidade_Faculdade
                        */

                        
                                    protected java.lang.String localCidade_Faculdade ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCidade_Faculdade(){
                               return localCidade_Faculdade;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cidade_Faculdade
                               */
                               public void setCidade_Faculdade(java.lang.String param){
                            
                                            this.localCidade_Faculdade=param;
                                    

                               }
                            

                        /**
                        * field for Sigla_Estado_Faculdade
                        */

                        
                                    protected java.lang.String localSigla_Estado_Faculdade ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSigla_Estado_Faculdade(){
                               return localSigla_Estado_Faculdade;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sigla_Estado_Faculdade
                               */
                               public void setSigla_Estado_Faculdade(java.lang.String param){
                            
                                            this.localSigla_Estado_Faculdade=param;
                                    

                               }
                            

                        /**
                        * field for Titulo_Obitido
                        */

                        
                                    protected java.lang.String localTitulo_Obitido ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitulo_Obitido(){
                               return localTitulo_Obitido;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Titulo_Obitido
                               */
                               public void setTitulo_Obitido(java.lang.String param){
                            
                                            this.localTitulo_Obitido=param;
                                    

                               }
                            

                        /**
                        * field for Total_pontos_Processo_Seletivo
                        */

                        
                                    protected java.math.BigDecimal localTotal_pontos_Processo_Seletivo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getTotal_pontos_Processo_Seletivo(){
                               return localTotal_pontos_Processo_Seletivo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Total_pontos_Processo_Seletivo
                               */
                               public void setTotal_pontos_Processo_Seletivo(java.math.BigDecimal param){
                            
                                            this.localTotal_pontos_Processo_Seletivo=param;
                                    

                               }
                            

                        /**
                        * field for Classificacao_Processo_Seletivo
                        */

                        
                                    protected int localClassificacao_Processo_Seletivo ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getClassificacao_Processo_Seletivo(){
                               return localClassificacao_Processo_Seletivo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Classificacao_Processo_Seletivo
                               */
                               public void setClassificacao_Processo_Seletivo(int param){
                            
                                            this.localClassificacao_Processo_Seletivo=param;
                                    

                               }
                            

                        /**
                        * field for Classificacao_no_Curso
                        */

                        
                                    protected java.math.BigDecimal localClassificacao_no_Curso ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getClassificacao_no_Curso(){
                               return localClassificacao_no_Curso;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Classificacao_no_Curso
                               */
                               public void setClassificacao_no_Curso(java.math.BigDecimal param){
                            
                                            this.localClassificacao_no_Curso=param;
                                    

                               }
                            

                        /**
                        * field for Curso
                        */

                        
                                    protected java.lang.String localCurso ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCurso(){
                               return localCurso;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Curso
                               */
                               public void setCurso(java.lang.String param){
                            
                                            this.localCurso=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       InscricaoProcessoSeletivo.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":InscricaoProcessoSeletivo",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "InscricaoProcessoSeletivo",
                           xmlWriter);
                   }

               
                   }
               
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nome_do_Processo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nome_do_Processo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nome_do_Processo");
                                    }
                                

                                          if (localNome_do_Processo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nome_do_Processo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNome_do_Processo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Ano_Processo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Ano_Processo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Ano_Processo");
                                    }
                                
                                               if (localAno_Processo==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Ano_Processo cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAno_Processo));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Numero_Processo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Numero_Processo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Numero_Processo");
                                    }
                                
                                               if (localNumero_Processo==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Numero_Processo cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Processo));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Numero_Inscricao", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Numero_Inscricao");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Numero_Inscricao");
                                    }
                                
                                               if (localNumero_Inscricao==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Numero_Inscricao cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Inscricao));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Data_da_Prova", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Data_da_Prova");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Data_da_Prova");
                                    }
                                
                                               if (localData_da_Prova==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Data_da_Prova cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localData_da_Prova));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nome_do_Aluno", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nome_do_Aluno");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nome_do_Aluno");
                                    }
                                

                                          if (localNome_do_Aluno==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nome_do_Aluno cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNome_do_Aluno);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sexo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sexo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sexo");
                                    }
                                

                                          if (localSexo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sexo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSexo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Data_de_Nascimento", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Data_de_Nascimento");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Data_de_Nascimento");
                                    }
                                
                                               if (localData_de_Nascimento==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Data_de_Nascimento cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localData_de_Nascimento));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nome_do_Pai", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nome_do_Pai");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nome_do_Pai");
                                    }
                                

                                          if (localNome_do_Pai==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nome_do_Pai cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNome_do_Pai);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nome_da_Mae", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nome_da_Mae");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nome_da_Mae");
                                    }
                                

                                          if (localNome_da_Mae==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nome_da_Mae cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNome_da_Mae);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Estado_Civil", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Estado_Civil");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Estado_Civil");
                                    }
                                
                                               if (localEstado_Civil==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Estado_Civil cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstado_Civil));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Raca", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Raca");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Raca");
                                    }
                                

                                          if (localRaca==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Raca cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRaca);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CPF", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CPF");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CPF");
                                    }
                                

                                          if (localCPF==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CPF cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCPF);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"RG", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"RG");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("RG");
                                    }
                                

                                          if (localRG==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("RG cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRG);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"RG_SSP", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"RG_SSP");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("RG_SSP");
                                    }
                                

                                          if (localRG_SSP==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("RG_SSP cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRG_SSP);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Titulo_de_Eleitor", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Titulo_de_Eleitor");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Titulo_de_Eleitor");
                                    }
                                

                                          if (localTitulo_de_Eleitor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Titulo_de_Eleitor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitulo_de_Eleitor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Zona_do_Titulo_de_Eleitor", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Zona_do_Titulo_de_Eleitor");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Zona_do_Titulo_de_Eleitor");
                                    }
                                

                                          if (localZona_do_Titulo_de_Eleitor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Zona_do_Titulo_de_Eleitor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localZona_do_Titulo_de_Eleitor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Expedicao_do_Titulo_de_Eleitor", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Expedicao_do_Titulo_de_Eleitor");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Expedicao_do_Titulo_de_Eleitor");
                                    }
                                

                                          if (localExpedicao_do_Titulo_de_Eleitor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Expedicao_do_Titulo_de_Eleitor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localExpedicao_do_Titulo_de_Eleitor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Certificado_Militar", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Certificado_Militar");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Certificado_Militar");
                                    }
                                

                                          if (localCertificado_Militar==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Certificado_Militar cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCertificado_Militar);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Expedicao_Certificado_Militar", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Expedicao_Certificado_Militar");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Expedicao_Certificado_Militar");
                                    }
                                

                                          if (localExpedicao_Certificado_Militar==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Expedicao_Certificado_Militar cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localExpedicao_Certificado_Militar);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Cidade_de_Nascimento", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Cidade_de_Nascimento");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Cidade_de_Nascimento");
                                    }
                                

                                          if (localCidade_de_Nascimento==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Cidade_de_Nascimento cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCidade_de_Nascimento);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sigla_Estado_de_Nascmento", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sigla_Estado_de_Nascmento");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sigla_Estado_de_Nascmento");
                                    }
                                

                                          if (localSigla_Estado_de_Nascmento==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_de_Nascmento cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSigla_Estado_de_Nascmento);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nacionalidade", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nacionalidade");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nacionalidade");
                                    }
                                

                                          if (localNacionalidade==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nacionalidade cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNacionalidade);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Deficiencia_Fisica", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Deficiencia_Fisica");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Deficiencia_Fisica");
                                    }
                                
                                               if (localDeficiencia_Fisica==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Deficiencia_Fisica cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Fisica));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Deficiencia_Auditiva", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Deficiencia_Auditiva");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Deficiencia_Auditiva");
                                    }
                                
                                               if (localDeficiencia_Auditiva==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Deficiencia_Auditiva cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Auditiva));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Deficiencia_Visual", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Deficiencia_Visual");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Deficiencia_Visual");
                                    }
                                
                                               if (localDeficiencia_Visual==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Deficiencia_Visual cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Visual));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CEP", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CEP");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CEP");
                                    }
                                

                                          if (localCEP==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CEP cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCEP);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Tipo_Logradouro", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Tipo_Logradouro");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Tipo_Logradouro");
                                    }
                                

                                          if (localTipo_Logradouro==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Tipo_Logradouro cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTipo_Logradouro);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Endereco", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Endereco");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Endereco");
                                    }
                                

                                          if (localEndereco==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Endereco cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEndereco);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Bairro", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Bairro");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Bairro");
                                    }
                                

                                          if (localBairro==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Bairro cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBairro);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Cidade", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Cidade");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Cidade");
                                    }
                                

                                          if (localCidade==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Cidade cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCidade);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sigla_Estado", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sigla_Estado");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sigla_Estado");
                                    }
                                

                                          if (localSigla_Estado==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sigla_Estado cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSigla_Estado);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Pais", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Pais");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Pais");
                                    }
                                

                                          if (localPais==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Pais cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPais);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Numero", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Numero");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Numero");
                                    }
                                

                                          if (localNumero==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Numero cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNumero);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Complemento", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Complemento");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Complemento");
                                    }
                                

                                          if (localComplemento==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Complemento cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localComplemento);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DDD_Telefone", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DDD_Telefone");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DDD_Telefone");
                                    }
                                

                                          if (localDDD_Telefone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("DDD_Telefone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDDD_Telefone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Telefone", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Telefone");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Telefone");
                                    }
                                

                                          if (localTelefone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Telefone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTelefone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DDD_Celular", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DDD_Celular");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DDD_Celular");
                                    }
                                

                                          if (localDDD_Celular==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("DDD_Celular cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDDD_Celular);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Celular", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Celular");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Celular");
                                    }
                                

                                          if (localCelular==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Celular cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCelular);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Email", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Email");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Email");
                                    }
                                

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Caixa_Postal", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Caixa_Postal");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Caixa_Postal");
                                    }
                                

                                          if (localCaixa_Postal==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Caixa_Postal cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCaixa_Postal);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Usar_Endereco_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Usar_Endereco_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Usar_Endereco_Comercial");
                                    }
                                
                                               if (localUsar_Endereco_Comercial==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Usar_Endereco_Comercial cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsar_Endereco_Comercial));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CEP_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CEP_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CEP_Comercial");
                                    }
                                

                                          if (localCEP_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CEP_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCEP_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Tipo_Logradouro_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Tipo_Logradouro_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Tipo_Logradouro_Comercial");
                                    }
                                

                                          if (localTipo_Logradouro_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Tipo_Logradouro_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTipo_Logradouro_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Endereco_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Endereco_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Endereco_Comercial");
                                    }
                                

                                          if (localEndereco_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Endereco_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEndereco_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Bairro_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Bairro_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Bairro_Comercial");
                                    }
                                

                                          if (localBairro_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Bairro_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBairro_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Cidade_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Cidade_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Cidade_Comercial");
                                    }
                                

                                          if (localCidade_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Cidade_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCidade_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sigla_Estado_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sigla_Estado_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sigla_Estado_Comercial");
                                    }
                                

                                          if (localSigla_Estado_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSigla_Estado_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Pais_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Pais_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Pais_Comercial");
                                    }
                                

                                          if (localPais_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Pais_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPais_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Numero_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Numero_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Numero_Comercial");
                                    }
                                

                                          if (localNumero_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Numero_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNumero_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Complemento_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Complemento_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Complemento_Comercial");
                                    }
                                

                                          if (localComplemento_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Complemento_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localComplemento_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DDD_Telefone_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DDD_Telefone_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DDD_Telefone_Comercial");
                                    }
                                

                                          if (localDDD_Telefone_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("DDD_Telefone_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDDD_Telefone_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Telefone_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Telefone_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Telefone_Comercial");
                                    }
                                

                                          if (localTelefone_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Telefone_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTelefone_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Email_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Email_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Email_Comercial");
                                    }
                                

                                          if (localEmail_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Email_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Caixa_postal_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Caixa_postal_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Caixa_postal_Comercial");
                                    }
                                

                                          if (localCaixa_postal_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Caixa_postal_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCaixa_postal_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Empresa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Empresa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Empresa");
                                    }
                                

                                          if (localEmpresa==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Empresa cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmpresa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Contato_Comercial", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Contato_Comercial");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Contato_Comercial");
                                    }
                                

                                          if (localContato_Comercial==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Contato_Comercial cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localContato_Comercial);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Escola_Ensino_Medio", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Escola_Ensino_Medio");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Escola_Ensino_Medio");
                                    }
                                

                                          if (localEscola_Ensino_Medio==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Escola_Ensino_Medio cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEscola_Ensino_Medio);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Cidade_Escola_Ensino_Medio", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Cidade_Escola_Ensino_Medio");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Cidade_Escola_Ensino_Medio");
                                    }
                                

                                          if (localCidade_Escola_Ensino_Medio==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Cidade_Escola_Ensino_Medio cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCidade_Escola_Ensino_Medio);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sigla_EstadoEscola_Ensino_Medio", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sigla_EstadoEscola_Ensino_Medio");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sigla_EstadoEscola_Ensino_Medio");
                                    }
                                

                                          if (localSigla_EstadoEscola_Ensino_Medio==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sigla_EstadoEscola_Ensino_Medio cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSigla_EstadoEscola_Ensino_Medio);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Ano_conclusao_Ensino_Medio", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Ano_conclusao_Ensino_Medio");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Ano_conclusao_Ensino_Medio");
                                    }
                                
                                               if (localAno_conclusao_Ensino_Medio==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Ano_conclusao_Ensino_Medio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAno_conclusao_Ensino_Medio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Faculdade", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Faculdade");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Faculdade");
                                    }
                                

                                          if (localFaculdade==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Faculdade cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFaculdade);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Cidade_Faculdade", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Cidade_Faculdade");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Cidade_Faculdade");
                                    }
                                

                                          if (localCidade_Faculdade==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Cidade_Faculdade cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCidade_Faculdade);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Sigla_Estado_Faculdade", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Sigla_Estado_Faculdade");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Sigla_Estado_Faculdade");
                                    }
                                

                                          if (localSigla_Estado_Faculdade==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_Faculdade cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSigla_Estado_Faculdade);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Titulo_Obitido", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Titulo_Obitido");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Titulo_Obitido");
                                    }
                                

                                          if (localTitulo_Obitido==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Titulo_Obitido cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitulo_Obitido);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Total_pontos_Processo_Seletivo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Total_pontos_Processo_Seletivo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Total_pontos_Processo_Seletivo");
                                    }
                                

                                          if (localTotal_pontos_Processo_Seletivo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Total_pontos_Processo_Seletivo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal_pontos_Processo_Seletivo));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Classificacao_Processo_Seletivo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Classificacao_Processo_Seletivo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Classificacao_Processo_Seletivo");
                                    }
                                
                                               if (localClassificacao_Processo_Seletivo==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Classificacao_Processo_Seletivo cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClassificacao_Processo_Seletivo));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Classificacao_no_Curso", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Classificacao_no_Curso");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Classificacao_no_Curso");
                                    }
                                

                                          if (localClassificacao_no_Curso==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Classificacao_no_Curso cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClassificacao_no_Curso));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Curso", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Curso");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Curso");
                                    }
                                

                                          if (localCurso==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Curso cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCurso);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nome_do_Processo"));
                                 
                                        if (localNome_do_Processo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNome_do_Processo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nome_do_Processo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Ano_Processo"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAno_Processo));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Numero_Processo"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Processo));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Numero_Inscricao"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Inscricao));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Data_da_Prova"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localData_da_Prova));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nome_do_Aluno"));
                                 
                                        if (localNome_do_Aluno != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNome_do_Aluno));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nome_do_Aluno cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sexo"));
                                 
                                        if (localSexo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSexo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sexo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Data_de_Nascimento"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localData_de_Nascimento));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nome_do_Pai"));
                                 
                                        if (localNome_do_Pai != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNome_do_Pai));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nome_do_Pai cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nome_da_Mae"));
                                 
                                        if (localNome_da_Mae != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNome_da_Mae));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nome_da_Mae cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Estado_Civil"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstado_Civil));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Raca"));
                                 
                                        if (localRaca != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRaca));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Raca cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "CPF"));
                                 
                                        if (localCPF != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCPF));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CPF cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "RG"));
                                 
                                        if (localRG != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRG));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("RG cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "RG_SSP"));
                                 
                                        if (localRG_SSP != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRG_SSP));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("RG_SSP cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Titulo_de_Eleitor"));
                                 
                                        if (localTitulo_de_Eleitor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitulo_de_Eleitor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Titulo_de_Eleitor cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Zona_do_Titulo_de_Eleitor"));
                                 
                                        if (localZona_do_Titulo_de_Eleitor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZona_do_Titulo_de_Eleitor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Zona_do_Titulo_de_Eleitor cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Expedicao_do_Titulo_de_Eleitor"));
                                 
                                        if (localExpedicao_do_Titulo_de_Eleitor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpedicao_do_Titulo_de_Eleitor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Expedicao_do_Titulo_de_Eleitor cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Certificado_Militar"));
                                 
                                        if (localCertificado_Militar != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCertificado_Militar));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Certificado_Militar cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Expedicao_Certificado_Militar"));
                                 
                                        if (localExpedicao_Certificado_Militar != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpedicao_Certificado_Militar));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Expedicao_Certificado_Militar cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Cidade_de_Nascimento"));
                                 
                                        if (localCidade_de_Nascimento != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCidade_de_Nascimento));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Cidade_de_Nascimento cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sigla_Estado_de_Nascmento"));
                                 
                                        if (localSigla_Estado_de_Nascmento != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSigla_Estado_de_Nascmento));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_de_Nascmento cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nacionalidade"));
                                 
                                        if (localNacionalidade != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNacionalidade));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nacionalidade cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Deficiencia_Fisica"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Fisica));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Deficiencia_Auditiva"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Auditiva));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Deficiencia_Visual"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeficiencia_Visual));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "CEP"));
                                 
                                        if (localCEP != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCEP));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CEP cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Tipo_Logradouro"));
                                 
                                        if (localTipo_Logradouro != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTipo_Logradouro));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Tipo_Logradouro cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Endereco"));
                                 
                                        if (localEndereco != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndereco));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Endereco cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Bairro"));
                                 
                                        if (localBairro != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBairro));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Bairro cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Cidade"));
                                 
                                        if (localCidade != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCidade));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Cidade cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sigla_Estado"));
                                 
                                        if (localSigla_Estado != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSigla_Estado));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sigla_Estado cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Pais"));
                                 
                                        if (localPais != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPais));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Pais cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Numero"));
                                 
                                        if (localNumero != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Numero cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Complemento"));
                                 
                                        if (localComplemento != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComplemento));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Complemento cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "DDD_Telefone"));
                                 
                                        if (localDDD_Telefone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDDD_Telefone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("DDD_Telefone cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Telefone"));
                                 
                                        if (localTelefone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTelefone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Telefone cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "DDD_Celular"));
                                 
                                        if (localDDD_Celular != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDDD_Celular));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("DDD_Celular cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Celular"));
                                 
                                        if (localCelular != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCelular));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Celular cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Email cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Caixa_Postal"));
                                 
                                        if (localCaixa_Postal != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCaixa_Postal));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Caixa_Postal cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Usar_Endereco_Comercial"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsar_Endereco_Comercial));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "CEP_Comercial"));
                                 
                                        if (localCEP_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCEP_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CEP_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Tipo_Logradouro_Comercial"));
                                 
                                        if (localTipo_Logradouro_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTipo_Logradouro_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Tipo_Logradouro_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Endereco_Comercial"));
                                 
                                        if (localEndereco_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndereco_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Endereco_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Bairro_Comercial"));
                                 
                                        if (localBairro_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBairro_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Bairro_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Cidade_Comercial"));
                                 
                                        if (localCidade_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCidade_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Cidade_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sigla_Estado_Comercial"));
                                 
                                        if (localSigla_Estado_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSigla_Estado_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Pais_Comercial"));
                                 
                                        if (localPais_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPais_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Pais_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Numero_Comercial"));
                                 
                                        if (localNumero_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Numero_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Complemento_Comercial"));
                                 
                                        if (localComplemento_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComplemento_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Complemento_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "DDD_Telefone_Comercial"));
                                 
                                        if (localDDD_Telefone_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDDD_Telefone_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("DDD_Telefone_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Telefone_Comercial"));
                                 
                                        if (localTelefone_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTelefone_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Telefone_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Email_Comercial"));
                                 
                                        if (localEmail_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Email_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Caixa_postal_Comercial"));
                                 
                                        if (localCaixa_postal_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCaixa_postal_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Caixa_postal_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Empresa"));
                                 
                                        if (localEmpresa != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmpresa));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Empresa cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Contato_Comercial"));
                                 
                                        if (localContato_Comercial != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContato_Comercial));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Contato_Comercial cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Escola_Ensino_Medio"));
                                 
                                        if (localEscola_Ensino_Medio != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEscola_Ensino_Medio));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Escola_Ensino_Medio cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Cidade_Escola_Ensino_Medio"));
                                 
                                        if (localCidade_Escola_Ensino_Medio != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCidade_Escola_Ensino_Medio));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Cidade_Escola_Ensino_Medio cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sigla_EstadoEscola_Ensino_Medio"));
                                 
                                        if (localSigla_EstadoEscola_Ensino_Medio != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSigla_EstadoEscola_Ensino_Medio));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sigla_EstadoEscola_Ensino_Medio cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Ano_conclusao_Ensino_Medio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAno_conclusao_Ensino_Medio));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Faculdade"));
                                 
                                        if (localFaculdade != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFaculdade));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Faculdade cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Cidade_Faculdade"));
                                 
                                        if (localCidade_Faculdade != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCidade_Faculdade));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Cidade_Faculdade cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Sigla_Estado_Faculdade"));
                                 
                                        if (localSigla_Estado_Faculdade != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSigla_Estado_Faculdade));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Sigla_Estado_Faculdade cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Titulo_Obitido"));
                                 
                                        if (localTitulo_Obitido != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitulo_Obitido));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Titulo_Obitido cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Total_pontos_Processo_Seletivo"));
                                 
                                        if (localTotal_pontos_Processo_Seletivo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal_pontos_Processo_Seletivo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Total_pontos_Processo_Seletivo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Classificacao_Processo_Seletivo"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClassificacao_Processo_Seletivo));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Classificacao_no_Curso"));
                                 
                                        if (localClassificacao_no_Curso != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClassificacao_no_Curso));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Classificacao_no_Curso cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Curso"));
                                 
                                        if (localCurso != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurso));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Curso cannot be null!!");
                                        }
                                    

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static InscricaoProcessoSeletivo parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            InscricaoProcessoSeletivo object =
                new InscricaoProcessoSeletivo();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"InscricaoProcessoSeletivo".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (InscricaoProcessoSeletivo)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nome_do_Processo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNome_do_Processo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Ano_Processo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAno_Processo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Numero_Processo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumero_Processo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Numero_Inscricao").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumero_Inscricao(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Data_da_Prova").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setData_da_Prova(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nome_do_Aluno").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNome_do_Aluno(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sexo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSexo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Data_de_Nascimento").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setData_de_Nascimento(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nome_do_Pai").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNome_do_Pai(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nome_da_Mae").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNome_da_Mae(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Estado_Civil").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstado_Civil(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Raca").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRaca(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","CPF").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCPF(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","RG").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRG(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","RG_SSP").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRG_SSP(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Titulo_de_Eleitor").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitulo_de_Eleitor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Zona_do_Titulo_de_Eleitor").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZona_do_Titulo_de_Eleitor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Expedicao_do_Titulo_de_Eleitor").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpedicao_do_Titulo_de_Eleitor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Certificado_Militar").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCertificado_Militar(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Expedicao_Certificado_Militar").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpedicao_Certificado_Militar(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Cidade_de_Nascimento").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCidade_de_Nascimento(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sigla_Estado_de_Nascmento").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSigla_Estado_de_Nascmento(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nacionalidade").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNacionalidade(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Deficiencia_Fisica").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeficiencia_Fisica(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Deficiencia_Auditiva").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeficiencia_Auditiva(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Deficiencia_Visual").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeficiencia_Visual(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","CEP").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCEP(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Tipo_Logradouro").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTipo_Logradouro(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Endereco").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndereco(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Bairro").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBairro(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Cidade").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCidade(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sigla_Estado").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSigla_Estado(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Pais").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPais(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Numero").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumero(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Complemento").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setComplemento(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","DDD_Telefone").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDDD_Telefone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Telefone").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTelefone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","DDD_Celular").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDDD_Celular(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Celular").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCelular(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Email").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Caixa_Postal").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCaixa_Postal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Usar_Endereco_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUsar_Endereco_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","CEP_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCEP_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Tipo_Logradouro_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTipo_Logradouro_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Endereco_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndereco_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Bairro_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBairro_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Cidade_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCidade_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sigla_Estado_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSigla_Estado_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Pais_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPais_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Numero_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumero_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Complemento_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setComplemento_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","DDD_Telefone_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDDD_Telefone_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Telefone_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTelefone_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Email_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Caixa_postal_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCaixa_postal_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Empresa").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmpresa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Contato_Comercial").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setContato_Comercial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Escola_Ensino_Medio").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEscola_Ensino_Medio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Cidade_Escola_Ensino_Medio").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCidade_Escola_Ensino_Medio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sigla_EstadoEscola_Ensino_Medio").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSigla_EstadoEscola_Ensino_Medio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Ano_conclusao_Ensino_Medio").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAno_conclusao_Ensino_Medio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Faculdade").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFaculdade(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Cidade_Faculdade").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCidade_Faculdade(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Sigla_Estado_Faculdade").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSigla_Estado_Faculdade(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Titulo_Obitido").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitulo_Obitido(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Total_pontos_Processo_Seletivo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotal_pontos_Processo_Seletivo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Classificacao_Processo_Seletivo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setClassificacao_Processo_Seletivo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Classificacao_no_Curso").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setClassificacao_no_Curso(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Curso").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCurso(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "https://server4.flexnuvem.com.br/REHAGRO".equals(namespaceURI) &&
                  "ArrayOfInscricaoProcessoSeletivo_Retorno".equals(typeName)){
                   
                            return  ArrayOfInscricaoProcessoSeletivo_Retorno.Factory.parse(reader);
                        

                  }

              
                  if (
                  "https://server4.flexnuvem.com.br/REHAGRO".equals(namespaceURI) &&
                  "ArrayOfInscricaoProcessoSeletivo".equals(typeName)){
                   
                            return  ArrayOfInscricaoProcessoSeletivo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "https://server4.flexnuvem.com.br/REHAGRO".equals(namespaceURI) &&
                  "InscricaoProcessoSeletivo".equals(typeName)){
                   
                            return  InscricaoProcessoSeletivo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "https://server4.flexnuvem.com.br/REHAGRO".equals(namespaceURI) &&
                  "InscricaoProcessoSeletivo_Retorno".equals(typeName)){
                   
                            return  InscricaoProcessoSeletivo_Retorno.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    
        public static class InscricaoProcessoSeletivo_Retorno
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = InscricaoProcessoSeletivo_Retorno
                Namespace URI = https://server4.flexnuvem.com.br/REHAGRO
                Namespace Prefix = 
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("https://server4.flexnuvem.com.br/REHAGRO")){
                return "";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Nome_do_Processo
                        */

                        
                                    protected java.lang.String localNome_do_Processo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNome_do_Processo(){
                               return localNome_do_Processo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nome_do_Processo
                               */
                               public void setNome_do_Processo(java.lang.String param){
                            
                                            this.localNome_do_Processo=param;
                                    

                               }
                            

                        /**
                        * field for Numero_Inscricao
                        */

                        
                                    protected int localNumero_Inscricao ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getNumero_Inscricao(){
                               return localNumero_Inscricao;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Numero_Inscricao
                               */
                               public void setNumero_Inscricao(int param){
                            
                                            this.localNumero_Inscricao=param;
                                    

                               }
                            

                        /**
                        * field for Erro
                        */

                        
                                    protected java.lang.String localErro ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getErro(){
                               return localErro;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Erro
                               */
                               public void setErro(java.lang.String param){
                            
                                            this.localErro=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       InscricaoProcessoSeletivo_Retorno.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"https://server4.flexnuvem.com.br/REHAGRO");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":InscricaoProcessoSeletivo_Retorno",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "InscricaoProcessoSeletivo_Retorno",
                           xmlWriter);
                   }

               
                   }
               
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Nome_do_Processo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Nome_do_Processo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Nome_do_Processo");
                                    }
                                

                                          if (localNome_do_Processo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Nome_do_Processo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNome_do_Processo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Numero_Inscricao", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Numero_Inscricao");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Numero_Inscricao");
                                    }
                                
                                               if (localNumero_Inscricao==java.lang.Integer.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Numero_Inscricao cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Inscricao));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "https://server4.flexnuvem.com.br/REHAGRO";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Erro", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Erro");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Erro");
                                    }
                                

                                          if (localErro==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("Erro cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localErro);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Nome_do_Processo"));
                                 
                                        if (localNome_do_Processo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNome_do_Processo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Nome_do_Processo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Numero_Inscricao"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumero_Inscricao));
                            
                                      elementList.add(new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO",
                                                                      "Erro"));
                                 
                                        if (localErro != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localErro));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("Erro cannot be null!!");
                                        }
                                    

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static InscricaoProcessoSeletivo_Retorno parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            InscricaoProcessoSeletivo_Retorno object =
                new InscricaoProcessoSeletivo_Retorno();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"InscricaoProcessoSeletivo_Retorno".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (InscricaoProcessoSeletivo_Retorno)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Nome_do_Processo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNome_do_Processo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Numero_Inscricao").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumero_Inscricao(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("https://server4.flexnuvem.com.br/REHAGRO","Erro").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setErro(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
            private  org.apache.axiom.om.OMElement  toOM(br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                                    
                                        private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao param, boolean optimizeContent)
                                        throws org.apache.axis2.AxisFault{

                                             
                                                    try{

                                                            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                                                            emptyEnvelope.getBody().addChild(param.getOMElement(br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao.MY_QNAME,factory));
                                                            return emptyEnvelope;
                                                        } catch(org.apache.axis2.databinding.ADBException e){
                                                            throw org.apache.axis2.AxisFault.makeFault(e);
                                                        }
                                                

                                        }
                                
                             
                             /* methods to provide back word compatibility */

                             


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao.class.equals(type)){
                
                           return br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse.class.equals(type)){
                
                           return br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    
   }
   