package br.com.linkcom.sined.util;

public interface PermissaoProjeto {

	/**
	 * M�todo onde tem que se retornar a string com a query
	 * que limita o bean aos projetos do usu�rio.
	 * 
	 * LINGUAGEM: HQL
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String subQueryProjeto();
	
	/**
	 * M�todo que indica se na subquery est� sendo usado function para otimiza��o
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/11/2015
	 */
	public boolean useFunctionProjeto();
	
}
