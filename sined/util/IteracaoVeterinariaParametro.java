package br.com.linkcom.sined.util;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Frequencia;

public class IteracaoVeterinariaParametro {
	
	private Frequencia intervalo;
	private Integer acada;
	private Date dtinicio;
	private Hora hrinicio;
	private Boolean prazotermino;
	private Integer quantidadeiteracao;
	private List<IteracaoVeterinariaDataHora> listaDataHoraAplicacao;
	
	public IteracaoVeterinariaParametro(Frequencia intervalo, Integer acada, Date dtinicio, Hora hrinicio, Boolean prazotermino, Integer quantidadeiteracao, List<IteracaoVeterinariaDataHora> listaDataHoraAplicacao){
		this.intervalo = intervalo;
		this.acada = acada;
		this.dtinicio = dtinicio;
		this.hrinicio = hrinicio;
		this.prazotermino = prazotermino;
		this.quantidadeiteracao = quantidadeiteracao;
		this.listaDataHoraAplicacao = listaDataHoraAplicacao;
		
	}

	public Frequencia getIntervalo() {
		return intervalo;
	}
	public Integer getAcada() {
		return acada;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Hora getHrinicio() {
		return hrinicio;
	}
	public Boolean getPrazotermino() {
		return prazotermino;
	}
	public Integer getQuantidadeiteracao() {
		return quantidadeiteracao;
	}
	public List<IteracaoVeterinariaDataHora> getListaDataHoraAplicacao() {
		return listaDataHoraAplicacao;
	}
}