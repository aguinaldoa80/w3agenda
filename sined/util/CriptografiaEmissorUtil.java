package br.com.linkcom.sined.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CriptografiaEmissorUtil {

	public static String criptografa(String mensagem) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, md.digest(mensagem.getBytes()));
        String s = hash.toString(16);
        if (s.length() % 2 != 0) {
            s = "0" + s;
        }
        return s;
    }
    
    public static boolean verificaCriptografia(String mensagem, String hash) throws NoSuchAlgorithmException {
        String hash2 = CriptografiaEmissorUtil.criptografa(mensagem);
        return hash2.equals(hash);
    }
    
    public static void main(String[] args) throws NoSuchAlgorithmException {
		System.out.println(criptografa("consultaDados"));
	}
    
}
