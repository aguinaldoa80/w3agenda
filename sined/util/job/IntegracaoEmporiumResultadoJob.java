package br.com.linkcom.sined.util.job;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

public class IntegracaoEmporiumResultadoJob implements StatefulJob {

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		String nome_banco = Thread.currentThread().getName().substring(25);
		
		List<Arquivoemporium> listaArquivoemporiumProcessamento = new ArrayList<Arquivoemporium>();
		List<Arquivoemporium> listaArquivoemporium = ArquivoemporiumService.getInstance().getResultadosFaltantes();
		for (Arquivoemporium arquivoemporium : listaArquivoemporium) {
			final String nome_arquivo = arquivoemporium.getArquivo().getNome().replaceAll("\\.txt", "");
			
			long tmp = System.currentTimeMillis();
			System.out.println("### Job Emporium Inicio (Leitura) " + tmp);
			
			String pathDir = IntegracaoEmporiumUtil.util.saveDir(nome_banco, true) + java.io.File.separator;
			File dir = new File(pathDir);
			dir.mkdirs();
			
			FilenameFilter filter = new FilenameFilter() {
	            public boolean accept(File dir, String name) {
	                return name.contains(nome_arquivo);
	            }
	        };
	        
	        File[] files = dir.listFiles(filter);
	        System.out.println("### Job Emporium Fim (Leitura) " + tmp);
	        if(files.length > 0){
				File file = files[0];
				
				try{
					String nomeArquivo = file.getName();
					String contentStr = IntegracaoEmporiumUtil.util.fileToString(file);
					
					Resource resource = new Resource("text/plain", nomeArquivo, contentStr.getBytes());
					
					Arquivo arquivo = new Arquivo();
					arquivo.setNome(resource.getFileName());
					arquivo.setContent(resource.getContents());
					arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
					arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
					arquivo.setTipoconteudo(resource.getContentType());
					
					arquivoemporium.setArquivoresultado(arquivo);
					
					ArquivoDAO.getInstance().saveFile(arquivoemporium, "arquivoresultado");
					ArquivoemporiumService.getInstance().updateArquivoresultado(arquivoemporium);
					
					arquivoemporium = ArquivoemporiumService.getInstance().loadForEmporium(arquivoemporium);
					ArquivoemporiumService.getInstance().processaExportacao(arquivoemporium);
					
					if(arquivoemporium.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO)){
						listaArquivoemporiumProcessamento.add(arquivoemporium);
					}
				} catch (Exception e) {
					e.printStackTrace();
					IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro no Job para salvar o arquivo de retorno.");
				}
			}
		}
		
		for (Arquivoemporium arquivoemporium : listaArquivoemporiumProcessamento) {
			try{
				EmporiumvendaService.getInstance().processaVendasPendentes(arquivoemporium, true, null, null);
			} catch (Exception e) {
				e.printStackTrace();
				IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro no Job para processar as vendas pendentes.");
			}
		}
	}

}
