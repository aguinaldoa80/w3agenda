package br.com.linkcom.sined.util.job;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Configuracaoecomerro;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ConfiguracaoecomerroService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;

public class IntegracaoEcomErroJob implements StatefulJob {
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		List<String> listTo = new ArrayList<String>();
		listTo.add("rodrigo.freitas@linkcom.com.br");
		listTo.add("luiz.silva@linkcom.com.br");
		
		List<Configuracaoecomerro> listaErro = ConfiguracaoecomerroService.getInstance().findAll();
		List<String> mensagens = new ArrayList<String>();
		for (Configuracaoecomerro configuracaoecomerro : listaErro) {
			mensagens.add(configuracaoecomerro.getMensagem());
			ConfiguracaoecomerroService.getInstance().delete(configuracaoecomerro);
		}
		
		if(mensagens != null && mensagens.size() > 0){
			StringBuilder erros = new StringBuilder();
			int i = 0;
			for (String erro : mensagens) {
				erros.append(erro).append("\n\n----------------------------\n\n");
				i++;
				if(i >= 50) break;
			}
			
			try {
				new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
					.setFrom("integracaoecommerce@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro na integração com o E-commerce. " + SinedDateUtils.toString(SinedDateUtils.currentTimestamp(), "dd/MM/yyyy HH:mm"))
					.addHtmlText("Existe(m) " + mensagens.size() + " erro(s) na integração do job " + jobName + ". Em anexo o log dos 50 primeiros erros.")
					.attachFileUsingByteArray(erros.toString().getBytes(), "log.log", "text/plain", "1")
					.sendMessage();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
