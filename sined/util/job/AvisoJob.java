package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.service.AvisoService;

public class AvisoJob implements StatefulJob {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		String jobName = arg0.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		try{
			AvisoService.getInstance().criarAvisosControladosPorDia();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
