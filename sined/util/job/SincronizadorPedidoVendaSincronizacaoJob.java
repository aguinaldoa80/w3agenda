package br.com.linkcom.sined.util.job;

import java.util.Calendar;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.service.PedidovendasincronizacaoService;
import br.com.linkcom.sined.util.SinedUtil;

public class SincronizadorPedidoVendaSincronizacaoJob implements StatefulJob {
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		int hora = c.get(Calendar.HOUR_OF_DAY);
		// Altera��o feita a pedido do guilherme para que n�o rode sincroniza�ao com o 
		// WMS enquanto os servidores estiverem fazendo backup
		if(hora != 2 && hora!=3 ){
//			System.out.println("W3ERP (Integra��o Pedido de Venda WMS)- Iniciando tarefa agendada pelo Quartz: " + jobName);		
			try {			
				PedidovendasincronizacaoService.getInstance().sincronizarPedidovenda();
			} catch (Exception e) {
				if(!SinedUtil.isAmbienteDesenvolvimento()){
					e.printStackTrace();
					SinedUtil.enviaEmailErroSincronizacaoWMS("pedido de venda", 0, e);
				}
			}
//			System.out.println("W3ERP (Integra��o Pedido de Venda WMS)- Fim tarefa agendada pelo Quartz: " + jobName);
		}
	}
}
