package br.com.linkcom.sined.util.job;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadora;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraStatusPagamento;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraTipoTransacao;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivoconciliacaooperadoraService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.TaxaparcelaService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoOperadoraBean;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.arquivoconciliacaooperadora.ArquivoconciliacaooperadoraUtil;

public class ArquivoConciliacaoOperadoraJob implements StatefulJob {

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
	
		ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService = Neo.getObject(ArquivoconciliacaooperadoraService.class);
		
		String nome_banco = jobContext.getJobDetail().getJobDataMap().get("nome_banco").toString();
	
		long tmp = System.currentTimeMillis();
		System.out.println("### Job Arquivo Conciliacao Operadora Inicio (Leitura) " + tmp);
		
		String pathSrcDir = ArquivoconciliacaooperadoraUtil.util.saveDir(nome_banco, false) + java.io.File.separator;
		File srcDir = new File(pathSrcDir);
		srcDir.mkdirs();
        
		
		String pathDestDir = ArquivoconciliacaooperadoraUtil.util.saveDir(nome_banco, true) + java.io.File.separator;
		File destDir = new File(pathDestDir);
		destDir.mkdirs();
		
        File[] files = srcDir.listFiles();
        
        System.out.println("### Job Arquivo Conciliacao Operadora Fim (Leitura) " + tmp);
        
        
        for (File file : files) {
			try{
				ArquivoConciliacaoOperadoraBean arquivoConciliacaoOperadoraBean = new ArquivoConciliacaoOperadoraBean();
			
				String nomeArquivo = file.getName();
				String contentStr = ArquivoconciliacaooperadoraUtil.util.fileToString(file);
				
				Resource resource = new Resource("text/plain", nomeArquivo, contentStr.getBytes());
				
				Arquivo arquivo = new Arquivo();
				arquivo.setNome(resource.getFileName());
				arquivo.setContent(resource.getContents());
				arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
				arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
				arquivo.setTipoconteudo(resource.getContentType());
				
				arquivoConciliacaoOperadoraBean.setArquivo(arquivo);
				
				Integer opcaoExtrato = Integer.parseInt(arquivoConciliacaoOperadoraBean.getArquivoTextoHeaderOpcaoExtrato());
				
				ArquivoService.getInstance().saveOrUpdate(arquivo);

				if (opcaoExtrato < 4 ) {
					List<Arquivoconciliacaooperadora> lista = this.processaArquivoVenda(arquivoConciliacaoOperadoraBean);
					for (Arquivoconciliacaooperadora arquivoconciliacaooperadora : lista) {
						if(arquivoconciliacaooperadora.getNsudoc() == null || !arquivoconciliacaooperadoraService.haveArquivoconciliacaooperadoraByNsudoc(arquivoconciliacaooperadora))
							arquivoconciliacaooperadoraService.saveOrUpdate(arquivoconciliacaooperadora);
					}
				} else {
					List<Arquivoconciliacaooperadora> lista = this.processaArquivoPagamento(arquivoConciliacaoOperadoraBean);
					for (Arquivoconciliacaooperadora arquivoconciliacaooperadora : lista) {
						atualizaStatusPagamento(arquivoconciliacaooperadora);
					}
				}
				
				try{
					FileUtils.moveFileToDirectory(file, destDir, false);
				} catch (Exception e) {
					file.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
				ArquivoconciliacaooperadoraUtil.util.enviarEmailErro(e, "Erro no Job para salvar o arquivo de conciliação da operadora.");
			}
		}
	}

	public List<Arquivoconciliacaooperadora> processaArquivoVenda(ArquivoConciliacaoOperadoraBean arquivo){
		TaxaparcelaService taxaparcelaService = Neo.getObject(TaxaparcelaService.class);
		ContareceberService contareceberService = Neo.getObject(ContareceberService.class);
		EmpresaService empresaService = EmpresaService.getInstance();
		String[] linhas = arquivo.getArquivoTexto().split("\\r?\\n");
		String linhaRegistro1 = "";
		Arquivoconciliacaooperadora arquivoConciliacaoOperadora;
		List<Arquivoconciliacaooperadora> listaArquivoconciliacaooperadora = new ArrayList<Arquivoconciliacaooperadora>();
		Empresa empresa = empresaService.loadPrincipal(); 
		for (String linha : linhas) {
			arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();

			if (linha.startsWith("0")) {
				Integer numeroEstabelecimento = Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getInicio(), EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getFim()));
				if (numeroEstabelecimento != null) {
					if (empresa != null && empresa.getEstabelecimento() == null ) {
						empresa.setEstabelecimento(numeroEstabelecimento);
						empresaService.saveOrUpdateNumeroEstabelecimento(empresa);
					}
				}
			} else if (linha.startsWith("1")) {
				linhaRegistro1 = linha;
			} else if(linha.startsWith("2")) {
				arquivoConciliacaoOperadora.setEstabelecimento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getInicio(),EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getFim())));
				arquivoConciliacaoOperadora.setNumeroro(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.NUMERO_RO.getInicio(),EnumArquivoConciliacaoOperadora.NUMERO_RO.getFim()));
				arquivoConciliacaoOperadora.setDataapresentacao(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_APRESENTACAO.getInicio(),EnumArquivoConciliacaoOperadora.DATA_APRESENTACAO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setTipotransacao(EnumArquivoConciliacaoOperadoraTipoTransacao.getEnumTipoTransacao(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.TIPO_TRANSACAO.getInicio(), EnumArquivoConciliacaoOperadora.TIPO_TRANSACAO.getFim()))));
				arquivoConciliacaoOperadora.setDataprevistapagamento(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_PREVISTA_PAGAMENTO.getInicio(), EnumArquivoConciliacaoOperadora.DATA_PREVISTA_PAGAMENTO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setDataenviobanco(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_ENVIO_BANCO.getInicio(), EnumArquivoConciliacaoOperadora.DATA_ENVIO_BANCO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setValorbruto(Double.parseDouble(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.VALOR_BRUTO.getInicio(), EnumArquivoConciliacaoOperadora.VALOR_BRUTO.getFim()))/100);
				arquivoConciliacaoOperadora.setValorliquido(Double.parseDouble(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.VALOR_LIQUIDO.getInicio(),EnumArquivoConciliacaoOperadora.VALOR_LIQUIDO.getFim()))/100);
				arquivoConciliacaoOperadora.setStatuspagamento(EnumArquivoConciliacaoOperadoraStatusPagamento.getEnumArquivoConciliacaoOperadoraStatusPagamento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getInicio(),EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getFim()))));
				arquivoConciliacaoOperadora.setDatavenda(DateUtils.stringToDate(linha.substring(EnumArquivoConciliacaoOperadora.DATA_VENDA.getInicio(),EnumArquivoConciliacaoOperadora.DATA_VENDA.getFim()),"yyyyMMdd"));
				arquivoConciliacaoOperadora.setValorvenda(Double.parseDouble(linha.substring(EnumArquivoConciliacaoOperadora.VALOR_VENDA.getInicio(), EnumArquivoConciliacaoOperadora.VALOR_VENDA.getFim()))/100);
				arquivoConciliacaoOperadora.setParcela(Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.PARCELA.getInicio(), EnumArquivoConciliacaoOperadora.PARCELA.getFim())));
				arquivoConciliacaoOperadora.setTotalparcela(Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.TOTAL_PARCELAS.getInicio(), EnumArquivoConciliacaoOperadora.TOTAL_PARCELAS.getFim())));
				arquivoConciliacaoOperadora.setCodigoautoricacao(linha.substring(EnumArquivoConciliacaoOperadora.CODIGO_AUTORIZACAO.getInicio(),EnumArquivoConciliacaoOperadora.CODIGO_AUTORIZACAO.getFim()));
				arquivoConciliacaoOperadora.setTid(linha.substring(EnumArquivoConciliacaoOperadora.TID.getInicio(),EnumArquivoConciliacaoOperadora.TID.getFim()));
				arquivoConciliacaoOperadora.setNsudoc(linha.substring(EnumArquivoConciliacaoOperadora.NSU_DOC.getInicio(),EnumArquivoConciliacaoOperadora.NSU_DOC.getFim()));
				arquivoConciliacaoOperadora.setMotivorejeicao(linha.substring(EnumArquivoConciliacaoOperadora.MOTIVO_REJEICAO.getInicio(),EnumArquivoConciliacaoOperadora.MOTIVO_REJEICAO.getFim()));
				arquivoConciliacaoOperadora.setTaxaparcela(taxaparcelaService.findTaxaParcelaByParcela(arquivoConciliacaoOperadora.getTotalparcela()));
				arquivoConciliacaoOperadora.setEmpresaestabelecimento(empresa);
				arquivoConciliacaoOperadora.setArquivo(arquivo.getArquivo());
				if (StringUtils.isNotBlank(arquivoConciliacaoOperadora.getTid())) {
					Documento documentoAssociacao = contareceberService.findForAssociacaoArquivoConciliacaoOperadora(arquivoConciliacaoOperadora.getTid());
					if (documentoAssociacao != null && documentoAssociacao.getCddocumento() != null) {
						arquivoConciliacaoOperadora.setDocumento(documentoAssociacao);
					}
				}
				if(arquivoConciliacaoOperadora.getTipotransacao() != null && arquivoConciliacaoOperadora.getTipotransacao().equals(EnumArquivoConciliacaoOperadoraTipoTransacao.VENDA)){
					listaArquivoconciliacaooperadora.add(arquivoConciliacaoOperadora);
				}
			} 
		}
		return listaArquivoconciliacaooperadora;
	}

	public List<Arquivoconciliacaooperadora> processaArquivoPagamento(ArquivoConciliacaoOperadoraBean arquivo){
		String[] linhas = arquivo.getArquivoTexto().split("\\r?\\n");
		String linhaRegistro1 = "";
		Arquivoconciliacaooperadora arquivoConciliacaoOperadora;
		List<Arquivoconciliacaooperadora> listaArquivoconciliacaooperadora = new ArrayList<Arquivoconciliacaooperadora>();
		
		for (String linha : linhas) {
			arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();
			
			if (linha.startsWith("1")) {
				linhaRegistro1 = linha;
			} else if (linha.startsWith("2")) {
				arquivoConciliacaoOperadora.setNumeroro(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.NUMERO_RO.getInicio(),EnumArquivoConciliacaoOperadora.NUMERO_RO.getFim()));
				arquivoConciliacaoOperadora.setStatuspagamento(EnumArquivoConciliacaoOperadoraStatusPagamento.getEnumArquivoConciliacaoOperadoraStatusPagamento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getInicio(),EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getFim()))));
				arquivoConciliacaoOperadora.setDatavenda(DateUtils.stringToDate(linha.substring(EnumArquivoConciliacaoOperadora.DATA_VENDA.getInicio(),EnumArquivoConciliacaoOperadora.DATA_VENDA.getFim()),"yyyyMMdd"));
				arquivoConciliacaoOperadora.setTid(linha.substring(EnumArquivoConciliacaoOperadora.TID.getInicio(),EnumArquivoConciliacaoOperadora.TID.getFim()));
				arquivoConciliacaoOperadora.setNsudoc(linha.substring(EnumArquivoConciliacaoOperadora.NSU_DOC.getInicio(),EnumArquivoConciliacaoOperadora.NSU_DOC.getFim()));
				arquivoConciliacaoOperadora.setArquivo(arquivo.getArquivo());
				listaArquivoconciliacaooperadora.add(arquivoConciliacaoOperadora);
			} 
		}
		return listaArquivoconciliacaooperadora;
	}
	
	public void atualizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadoraPagamento){
		ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService = Neo.getObject(ArquivoconciliacaooperadoraService.class);
		Arquivoconciliacaooperadora arquivoconciliacaooperadoraVenda = arquivoconciliacaooperadoraService.findForAtulizaStatusPagamento(arquivoconciliacaooperadoraPagamento);
		if (arquivoconciliacaooperadoraVenda != null) {
			arquivoconciliacaooperadoraService.atualizaStatusPagamento(arquivoconciliacaooperadoraVenda, arquivoconciliacaooperadoraPagamento);
			if (arquivoconciliacaooperadoraVenda.getDocumento() != null && arquivoconciliacaooperadoraVenda.getDocumento().getCddocumento() != null) {
				arquivoconciliacaooperadoraService.conciliar(arquivoconciliacaooperadoraVenda.getCdarquivoconciliacaooperadora().toString());
			}
		}
	}
}

