package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.util.CacheWebserviceUtil;

public class LimpezaCacheWebserviceJob implements StatefulJob {
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		try{
			CacheWebserviceUtil.limparCache();
		} catch (Exception e) {}
	}
	
}
