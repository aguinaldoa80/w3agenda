package br.com.linkcom.sined.util.job;

import java.sql.Timestamp;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Ecommaterial;
import br.com.linkcom.sined.geral.bean.Ecommaterialestoque;
import br.com.linkcom.sined.geral.service.EcommaterialService;
import br.com.linkcom.sined.geral.service.EcommaterialestoqueService;
import br.com.linkcom.sined.geral.service.EcommaterialidentificadorService;
import br.com.linkcom.sined.util.IntegracaoEcomUtil;
import br.com.linkcom.sined.util.SinedDateUtils;

public class IntegracaoEcomJob implements StatefulJob {
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		Timestamp currentTimestamp = SinedDateUtils.currentTimestamp();
		
		if(EcommaterialService.getInstance().haveDadosSincronizacao()){
			while(true){
				Ecommaterial ecommaterial = EcommaterialService.getInstance().primeiroFilaSincronizacao(currentTimestamp);
				if(ecommaterial == null) break;
				if(ecommaterial.getConfiguracaoecom() == null) continue;
				try{
					EcommaterialService.getInstance().updateSincronizado(ecommaterial);
					ecommaterial = EcommaterialService.getInstance().loadEcommaterial(ecommaterial);
					
					String identificador = IntegracaoEcomUtil.util.enviarMaterialEcom(ecommaterial, ecommaterial.getConfiguracaoecom());
					
					if(identificador != null){
						EcommaterialService.getInstance().updateIdentificador(ecommaterial, identificador, ecommaterial.getConfiguracaoecom());
						EcommaterialestoqueService.getInstance().updateIdentificador(ecommaterial.getMaterial(), identificador, ecommaterial.getConfiguracaoecom());
						EcommaterialidentificadorService.getInstance().saveOrUpdateIdentificador(ecommaterial.getMaterial(), identificador, ecommaterial.getConfiguracaoecom());
					}
				} catch (Exception e) {
					EcommaterialService.getInstance().updateFinalFila(ecommaterial);
					
					String descricaoErro = "Ocorreu um erro ao sincronizar o material: " + 
							(ecommaterial != null && ecommaterial.getMaterial() != null ? ecommaterial.getMaterial().getCdmaterial() : "N�o encontrado");
					
					e.printStackTrace();
					IntegracaoEcomUtil.util.addErro(e, descricaoErro);
				}
			}
			
			while(true){
				Ecommaterialestoque ecommaterialestoque = EcommaterialestoqueService.getInstance().primeiroFilaSincronizacao(currentTimestamp);
				if(ecommaterialestoque == null) break;
				
				try{
					if(ecommaterialestoque.getIdentificador() != null) {
						EcommaterialestoqueService.getInstance().updateSincronizado(ecommaterialestoque);
						ecommaterialestoque = EcommaterialestoqueService.getInstance().loadEcommaterialestoque(ecommaterialestoque);
						
						IntegracaoEcomUtil.util.enviarEstoqueEcom(ecommaterialestoque, ecommaterialestoque.getConfiguracaoecom());
					}
				} catch (Exception e) {
					EcommaterialestoqueService.getInstance().updateFinalFila(ecommaterialestoque);
					
					String descricaoErro = "Ocorreu um erro ao sincronizar o estoque do material: " + 
							(ecommaterialestoque != null && ecommaterialestoque.getMaterial() != null ? ecommaterialestoque.getMaterial().getCdmaterial() : "N�o encontrado");
					
					e.printStackTrace();
					IntegracaoEcomUtil.util.addErro(e, descricaoErro);
				}
			}
			
		}
	}
	
}
