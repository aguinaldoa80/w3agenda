package br.com.linkcom.sined.util.job;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.ContratosincronizacaoAA;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.ContratosincronizacaoAAService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.rest.arvoreacesso.ContratoBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTClient;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTModel;

public class ArvoreAcessoIntegracaoJob implements StatefulJob {

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		ClienteService clienteService = ClienteService.getInstance();
		ContratosincronizacaoAAService contratoServiceSincronizacaoAAService = Neo.getObject(ContratosincronizacaoAAService.class);
		
		
//		String nome_banco = jobContext.getJobDetail().getJobDataMap().get("nome_banco").toString();
	
		long tmp = System.currentTimeMillis();
		System.out.println("### In�cio do Job de Integra��o da ArvoreAcesso " + tmp);
		
		List<ContratosincronizacaoAA> lista = contratoServiceSincronizacaoAAService.findNaoSincronizados();
		
		for(ContratosincronizacaoAA it : lista){
			EmpresaArvoreAcessoRESTModel model = null;
			Contratohistorico contratohistorico = null;
			List<ContratoBean> listaContratoBeanAux = new ArrayList<ContratoBean>();
			try{
				System.out.println("#### Job de Integra��o da ArvoreAcesso ::: Tentando sincronizar cdcliente=" + it.getCliente().getCdpessoa());
				Cliente cliente = clienteService.loadForSincronizacaoAA(it.getCliente());

				if(StringUtils.isBlank(cliente.getRazaosocial())){
					cliente.setRazaosocial(cliente.getNome());
				}
				
				//valida envio
				validaDadosObrigatorios(cliente);
				
				EmpresaArvoreAcessoRESTClient clientWS = new EmpresaArvoreAcessoRESTClient();
				
				EmpresaArvoreAcessoRESTBean bean = new EmpresaArvoreAcessoRESTBean(cliente);
				listaContratoBeanAux.addAll(bean.getLista_contrato());
				model = clientWS.enviaEmpresa(bean);
				if(model!=null && !model.isErro() && model.getId()!=null){
					ContratosincronizacaoAAService.getInstance().updateSincronizacao(it.getCliente().getCdpessoa(), model.getId());
					
					if(listaContratoBeanAux != null && !listaContratoBeanAux.isEmpty()){
						Timestamp hora = SinedDateUtils.currentTimestamp();
						for(ContratoBean item : listaContratoBeanAux){
							contratohistorico = new Contratohistorico();
							contratohistorico.setContrato(new Contrato(item.getId_contrato()));
							contratohistorico.setAcao(Contratoacao.ALTERADO);
							contratohistorico.setDthistorico(hora);
							contratohistorico.setObservacao("Contrato sincronizado com a �rvore de Acesso.");
							
							ContratohistoricoService.getInstance().saveOrUpdate(contratohistorico);
						}
					}
				}else{
					throw new RuntimeException("Servidor n�o retornou dados da requisi��o.");
				}
			}catch(Exception e){
				System.out.println("ERRO ao sincronizar com a ArvoreAcesso o registro com id="+it.getCliente().getCdpessoa());
				e.printStackTrace();
				String mensagemErro = (model==null || StringUtils.isBlank(model.getMensagem())) ? e.getMessage() : model.getMensagem();
				
				ContratosincronizacaoAA aa = ContratosincronizacaoAAService.getInstance().findByCdCliente(it.getCliente().getCdpessoa());
				
				ContratosincronizacaoAAService.getInstance().updateError(it.getCliente().getCdpessoa(), mensagemErro);
				
				if(mensagemErro != null && aa != null && !mensagemErro.equals(aa.getMensagem())){
					if(listaContratoBeanAux != null && !listaContratoBeanAux.isEmpty()){
						for(ContratoBean item : listaContratoBeanAux){
							contratohistorico = new Contratohistorico();
							contratohistorico.setContrato(new Contrato(item.getId_contrato()));
							contratohistorico.setAcao(Contratoacao.ALTERADO);
							contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
							contratohistorico.setObservacao("ERRO ao sincronizar com a ArvoreAcesso: " + mensagemErro);
							
							ContratohistoricoService.getInstance().saveOrUpdate(contratohistorico);
						}
					} else {
						List<Contrato> listaContrato = ContratoService.getInstance().findByContrato(it.getCliente());
						if(listaContrato != null && !listaContrato.isEmpty()){
							for(Contrato item : listaContrato){
								contratohistorico = new Contratohistorico();
								contratohistorico.setContrato(item);
								contratohistorico.setAcao(Contratoacao.ALTERADO);
								contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
								contratohistorico.setObservacao("ERRO ao sincronizar com a ArvoreAcesso: " + mensagemErro);
								
								ContratohistoricoService.getInstance().saveOrUpdate(contratohistorico);
							}
						}
					}
				}
				
			}
			System.out.println("#### Job de Integra��o da ArvoreAcesso ::: Fim da tentativa de sincroniza��o do cdcliente=" + it.getCliente().getCdpessoa());
		}
		
        System.out.println("### Fim do Job de Integra��o com a ArvoreAcesso " + tmp);
        
        
		
	}

	/**
	 * Valida dados obrigat�rios antes do envio
	 * @param cliente
	 */
	private void validaDadosObrigatorios(Cliente cliente) {
		String msg = "";
		ContatoService contatoService = ContatoService.getInstance();
		
		if(StringUtils.isBlank(cliente.getRazaosocial())){
			msg += "N�o possui razao social.\n";
		}
		
		for(PessoaContato pessoaContato : cliente.getListaContato()) {
			Contato contato = contatoService.getContatoByCdPessoaLigacao(pessoaContato.getPessoa().getCdpessoa());
			
			if(contato.getContatotipo()==null || StringUtils.isBlank(contato.getContatotipo().getNome()) ){
				msg += "Contato n�o possui contatotipo definido.\n";
			}
		}
		if(StringUtils.isNotBlank(msg)){
			throw new RuntimeException(msg);
		}
	}

}

