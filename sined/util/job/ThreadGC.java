package br.com.linkcom.sined.util.job;

import javax.naming.InitialContext;

import org.apache.commons.lang.math.NumberUtils;

public class ThreadGC extends Thread {

	@Override
	public void run() {
		try {
			while (true){
				String minutos = InitialContext.doLookup("MINUTOS_GC");
				if (minutos==null || minutos.trim().equals("") || !NumberUtils.isNumber(minutos) || new Integer(minutos)<=0){
					break;
				}
				
				sleep(new Integer(minutos) * 1000 * 60);
				
				System.out.println(">>>>>>>>>>>>> System.gc()");
				System.gc();
			}			
		} catch (Exception e) {
		}
	}
}