package br.com.linkcom.sined.util.job;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedException;

public class ExportadorArquivoGerencialJob implements StatefulJob{

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
//		System.out.println("Iniciando tarefa agendada pelo Quartz: " + jobName);
		
		Parametrogeral parametrogeral = ParametrogeralService.getInstance().findForArquivoGerencial("LocalArquivosGerenciais");
		
		if (parametrogeral != null && !StringUtils.isEmpty(parametrogeral.getValor())){
		
			String localArquivos = parametrogeral.getValor();
			if (!localArquivos.endsWith("/") && !localArquivos.endsWith("\\"))
				localArquivos = localArquivos + "/";
			
			try{
				exportarContaGerenciais(localArquivos);
				exportarCentroCusto(localArquivos);
				exportarFornecedor(localArquivos); 
				exportarCliente(localArquivos); 
				exportarProjeto(localArquivos); 
				exportarContaPagar(localArquivos);
				exportarContaReceber(localArquivos); 
			}catch (Exception e) {
				e.printStackTrace();
				StringWriter stringWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter(stringWriter);
				e.printStackTrace(printWriter);
				enviarEmailErro("Ocorreu um erro ao gerar os arquivos gerenciais. " +
						"Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
			}
		}else{
			enviarEmailErro("N�o foi poss�vel exportar os arquivos gerencias para o Banco de Dados " + jobName.substring(25) +
					", pois o par�metro LocalArquivosGerenciais n�o foi definido.");
		}
		
//		System.out.println("Tarefa do Quartz conclu�da: " + jobName);
	}

	private void enviarEmailErro(String mensagem) {
		Parametrogeral pg = ParametrogeralService.getInstance().findForArquivoGerencial("EMAIL_ADMIN");
		String[] emails = pg.getValor().split(";");
		List<String> listTo = new ArrayList<String>();
		String string;
		
		for (int i = 0; i < emails.length; i++) {
			string = emails[i];
			if(string != null && !string.trim().equals("")){
				listTo.add(string.trim());
			}
		}

		try {
			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			
			email
				.setFrom("w3erp@w3erp.com.br")
				.setListTo(listTo)
				.setSubject("[W3ERP] Erro ao exportar arquivos gerenciais.")
				.addHtmlText(mensagem)
				.sendMessage();
		} catch (Exception e) {
			throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
		}
	}

	private void exportarContaReceber(String localArquivos) throws IOException {

		//Contas a Receber: C�digo do Centro de Custo, Data da emiss�o, Data do Vencimento, Data do Pagamento, Valor, C�digo da Conta Gerencial, Situa��o, C�digo do Projeto.
		Iterator<Object[]> iterator = ContapagarService.getInstance().findForArquivoGerencial(Documentoclasse.OBJ_RECEBER);
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "conta_receber.csv")));
		
		try{
			writer.println("C�digo do Centro de Custo;Data da emiss�o;Data do Vencimento;Data do Pagamento;Valor;Descricao;C�digo da Conta Gerencial;Situa��o;C�digo do Projeto;C�digo da Pessoa;");
			while(iterator.hasNext()){
				
				Object[] documento = iterator.next();

				writer.print(documento[0]);//C�digo do Centro de Custo
				writer.print(";");
				writer.print(documento[1]);//Data da emiss�o
				writer.print(";");
				writer.print(documento[2]);//Data do Vencimento
				writer.print(";");
				if (documento[3] != null)
					writer.print(documento[3]);//Data do Pagamento
				writer.print(";");
				
				if(documento[4] instanceof Long){
					writer.print(new Money((Long) documento[4], true).toString());
				} else {
					writer.print(documento[4]); //Valor
				}
				
				writer.print(";");
				writer.print(documento[5]);//Descricao
				writer.print(";");
				writer.print(documento[6]);//C�digo da Conta Gerencial
				writer.print(";");
				writer.print(documento[7]);//Situa��o
				writer.print(";");
				if (documento[8] != null)
					writer.print(documento[8]);//C�digo do Projeto
				writer.print(";");
				if (documento[9] != null)
					writer.print(documento[9]); //C�digo da pessoa
				writer.println();
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarContaPagar(String localArquivos) throws IOException {

		//Contas a Pagar: C�digo do Centro de Custo, Data da emiss�o, Data do Vencimento, Data do Pagamento, Valor, C�digo da Conta Gerencial, Situa��o, C�digo do Projeto.
		Iterator<Object[]> iterator = ContapagarService.getInstance().findForArquivoGerencial(Documentoclasse.OBJ_PAGAR);
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "conta_pagar.csv")));
		
		try{
			writer.println("C�digo do Centro de Custo;Data da emiss�o;Data do Vencimento;Data do Pagamento;Valor;Descri��o;C�digo da Conta Gerencial;Situa��o;C�digo do Projeto;C�digo da Pessoa;");
			while(iterator.hasNext()){
				
				Object[] documento = iterator.next();

				writer.print(documento[0]);//C�digo do Centro de Custo
				writer.print(";");
				writer.print(documento[1]);//Data da emiss�o
				writer.print(";");
				writer.print(documento[2]);//Data do Vencimento
				writer.print(";");
				if (documento[3] != null)
					writer.print(documento[3]);//Data do Pagamento
				writer.print(";");
				
				if(documento[4] instanceof Long){
					writer.print(new Money((Long) documento[4], true).toString());
				} else {
					writer.print(documento[4]); //Valor
				}
				
				writer.print(";");
				writer.print(documento[5]);//Descri��o
				writer.print(";");
				writer.print(documento[6]);//C�digo da Conta Gerencial
				writer.print(";");
				writer.print(documento[7]);//Situa��o
				writer.print(";");
				if (documento[8] != null)
					writer.print(documento[8]);//C�digo do Projeto
				writer.print(";");
				if (documento[9] != null)
					writer.print(documento[9]); //C�digo da pessoa
				writer.println();
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarProjeto(String localArquivos) throws IOException {
		
		//Projeto: C�digo do Projeto, Descri��o
		Iterator<Projeto> iterator = ProjetoService.getInstance().findForArquivoGerencial();
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "projeto.csv")));
		
		try{
			writer.println("C�digo;Descri��o");
			while(iterator.hasNext()){
				Projeto projeto = iterator.next();
				writer.print(projeto.getCdprojeto());
				writer.print(";");
				writer.println(projeto.getNome());
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarCliente(String localArquivos) throws IOException {
		
		//Cliente: C�digo de Cliente, Descri��o, Munic�pio e Estado.
		Iterator<Object[]> iterator = ClienteService.getInstance().findForArquivoGerencial();
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "cliente.csv")));
		
		Integer ultimoID = null;
		
		try{
			writer.println("C�digo;Descri��o;Munic�pio;Estado");
			while(iterator.hasNext()){
				Object[] cliente = iterator.next();
				
				//se o fornecedor tiver mais de um endere�o ele ser� retornado mais de uma vez
				if (ultimoID != null && ultimoID.equals(cliente[0]))
					continue;
				
				writer.print(cliente[0]);
				writer.print(";");
				writer.print(cliente[1]);
				writer.print(";");
				writer.print(cliente[2]);
				writer.print(";");
				writer.println(cliente[3]);
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarFornecedor(String localArquivos) throws IOException {
		
		//Fornecedor: C�digo do Fornecedor, Descri��o, Munic�pio e Estado.
		Iterator<Object[]> iterator = FornecedorService.getInstance().findForArquivoGerencial();
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "fornecedor.csv")));
		
		Integer ultimoID = null;
		
		try{
			writer.println("C�digo;Descri��o;Munic�pio;Estado");
			while(iterator.hasNext()){
				Object[] fornecedor = iterator.next();
				
				//se o fornecedor tiver mais de um endere�o ele ser� retornado mais de uma vez
				if (ultimoID != null && ultimoID.equals(fornecedor[0]))
					continue;
				
				writer.print(fornecedor[0]);
				writer.print(";");
				writer.print(fornecedor[1]);
				writer.print(";");
				writer.print(fornecedor[2]);
				writer.print(";");
				writer.println(fornecedor[3]);
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarCentroCusto(String localArquivos) throws IOException {
		
		//Centro de Custo: C�digo do CC, Descri��o. 
		Iterator<Centrocusto> iterator = CentrocustoService.getInstance().findForArquivoGerencial();
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "centro_custo.csv")));
		
		try{
			writer.println("C�digo;Descri��o");
			while(iterator.hasNext()){
				Centrocusto centrocusto = iterator.next();
				writer.print(centrocusto.getCdcentrocusto());
				writer.print(";");
				writer.println(centrocusto.getNome());
			}
			
		}finally{
			writer.close();
		}
	}

	private void exportarContaGerenciais(String localArquivos) throws IOException {

		//Contas Gerenciais: C�digo da Conta, Descri��o. 
		Iterator<Object[]> iterator = ContagerencialService.getInstance().findForArquivoGerencial();
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(localArquivos + "conta_gerencial.csv")));
		
		try{
			writer.println("C�digo;Identificador;Descri��o");
			while(iterator.hasNext()){
				Object[] contagerencial = iterator.next();
				writer.print(contagerencial[0]);
				writer.print(";");
				writer.print(contagerencial[2]);
				writer.print(";");
				writer.println(contagerencial[1]);
			}
			
		}finally{
			writer.close();
		}
	}

}
