package br.com.linkcom.sined.util.job;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NameNotFoundException;

import net.sf.json.JSON;

import org.apache.commons.io.FileUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MaterialvendaService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoitemService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.OfflineUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.offline.MaterialtabelaprecoOfflineJSON;
import br.com.linkcom.sined.util.offline.MaterialtabelaprecoitemOfflineJSON;

public class GenerateJSONEntitiesPedidoVendaJob implements StatefulJob{
	
	private static final String SEPARATOR = System.getProperty("file.separator");
	private String path;
	private String forcarCahceStart;
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
//		System.out.println("Iniciando tarefa agendada pelo Quartz: " + jobName);
		
		String nome_banco = jobContext.getJobDetail().getJobDataMap().get("nome_banco").toString();
		path = OfflineUtil.getDirOfflineJSON(nome_banco);
		forcarCahceStart = jobContext.getJobDetail().getJobDataMap().get(nome_banco+"_start_thread").toString();
		
		try {
			//verificando se houve alguma atualiza�ao nas entidades cacheadas
			Long ultimaAtualizacaoEntidadeOffline = SinedUtil.getUltimaAtualizacaoEntidadeOffline();
			Long ultimoCacheEntidadeOffline = ParametrogeralService.getInstance().getLong(Parametrogeral.OFFLINE_DT_MODIFICACAO_ENTIDADES) ;
			
			if(ultimaAtualizacaoEntidadeOffline>ultimoCacheEntidadeOffline || "true".equals(forcarCahceStart)){
				if("true".equals(forcarCahceStart))
					jobContext.getJobDetail().getJobDataMap().put(nome_banco+"_start_thread", "false");
				
				exportarPedidoVendaTipo();
				exportarColaborador();
				exportarEmpresa();
				exportarLocalarmazenagem();
				exportarProjeto();
				exportarClienteWithEndereco();
				exportarPrazoPagamento();
				exportarPrazoPagamentoItem();
				exportarIndiceCorrecao();
				exportarConta();
				exportarDocumentoTipo();
				exportarMaterial(); //completo
				exportarMaterialrelacionado();
				exportarMaterialvenda();
				exportarUnidadeMedida();
				exportarUf();
				exportarMunicipio();
				exportarMaterialtabelaprecoAndMaterialtabelaprecoitem();
				
				//atualizar o parametro
				ParametrogeralService.getInstance().updateValorPorNome(Parametrogeral.OFFLINE_DT_MODIFICACAO_ENTIDADES, ultimaAtualizacaoEntidadeOffline+"");
			}
		} catch (Exception e) {
			if(e.getCause() instanceof NameNotFoundException){
//				e.printStackTrace();
				try{
					jobContext.getScheduler().deleteJob(jobName, Scheduler.DEFAULT_GROUP);
				} catch (Exception e2) {}
			} else {
				String nomeMaquina = "N�o encontrado.";
				try {  
		            InetAddress localaddr = InetAddress.getLocalHost();  
		            nomeMaquina = localaddr.getHostName();  
		        } catch (UnknownHostException e3) {}  
				
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				enviarEmailErro("Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
			}
		}
		
//		System.out.println("Tarefa do Quartz conclu�da: " + jobName);
	}

	private void exportarUnidadeMedida() {
		UnidademedidaService unidademedidaService = Neo.getObject(UnidademedidaService.class);
		saveToFile(View.convertToJson(unidademedidaService.findForPVOffline()), "unidademedida");
	}

	private void exportarUf() {
		UfService ufService = Neo.getObject(UfService.class);
		saveToFile(View.convertToJson(ufService.findForPVOffline()), "uf");
	}

	private void exportarMaterialtabelaprecoAndMaterialtabelaprecoitem() {
		MaterialtabelaprecoService materialtabelaprecoService = Neo.getObject(MaterialtabelaprecoService.class);
		List<MaterialtabelaprecoOfflineJSON> lista = materialtabelaprecoService.findForPVOffline();
		List<MaterialtabelaprecoitemOfflineJSON> listaItens = new ArrayList<MaterialtabelaprecoitemOfflineJSON>();
		
		if(lista != null && !lista.isEmpty()){
			for(MaterialtabelaprecoOfflineJSON materialtabelaprecoOfflineJSON : lista){
				if(materialtabelaprecoOfflineJSON.getListaMaterialtabelaprecoitem() != null && 
						!materialtabelaprecoOfflineJSON.getListaMaterialtabelaprecoitem().isEmpty()){
					listaItens.addAll(materialtabelaprecoOfflineJSON.getListaMaterialtabelaprecoitem());
				}
			}
		}
		saveToFile(View.convertToJson(lista), "materialtabelapreco");
		saveToFile(View.convertToJson(listaItens), "materialtabelaprecoitem");
	}
	
	private void exportarMunicipio() {
		MunicipioService municipioService = Neo.getObject(MunicipioService.class);
		saveToFile(View.convertToJson(municipioService.findForPVOffline()), "municipio");
	}
	
	private void exportarMaterial() {
		MaterialService materialService = Neo.getObject(MaterialService.class);
		saveToFile(View.convertToJson(materialService.findForPVOffline()), "material");
		
	}
	
	private void exportarMaterialrelacionado() {
		MaterialrelacionadoService materialrelacionadoService = Neo.getObject(MaterialrelacionadoService.class);
		saveToFile(View.convertToJson(materialrelacionadoService.findForPVOffline()), "materialrelacionado");
		
	}
	
	private void exportarMaterialvenda() {
		MaterialvendaService materialvendaService = Neo.getObject(MaterialvendaService.class);
		saveToFile(View.convertToJson(materialvendaService.findForPVOffline()), "materialvenda");
		
	}

	private void exportarDocumentoTipo() {
		DocumentotipoService documentotipoService = Neo.getObject(DocumentotipoService.class);
		saveToFile(View.convertToJson(documentotipoService.findForPVOffline()), "documentotipo");
	}

	private void exportarConta() {
		ContaService contaService = Neo.getObject(ContaService.class);
		saveToFile(View.convertToJson(contaService.findForPVOffline()), "conta");
	}

	private void exportarIndiceCorrecao() {
		IndicecorrecaoService indicecorrecaoService = Neo.getObject(IndicecorrecaoService.class);
		saveToFile(View.convertToJson(indicecorrecaoService.findForPVOffline()), "indicecorrecao");
		
	}

	private void exportarPrazoPagamento() {
		PrazopagamentoService prazopagamentoService = Neo.getObject(PrazopagamentoService.class);
		saveToFile(View.convertToJson(prazopagamentoService.findForPVOffline()), "prazopagamento");
	}

	private void exportarPrazoPagamentoItem() {
		PrazopagamentoitemService prazopagamentoitemService = Neo.getObject(PrazopagamentoitemService.class);
		saveToFile(View.convertToJson(prazopagamentoitemService.findForPVOffline()), "prazopagamentoitem");
	}
	
	private void exportarClienteWithEndereco() {
		ClienteService clienteService = Neo.getObject(ClienteService.class);
		saveToFile(View.convertToJson(clienteService.findForPVOffline()), "cliente");
	}

	private void exportarEmpresa() {
		EmpresaService empresaService = Neo.getObject(EmpresaService.class);
		saveToFile(View.convertToJson(empresaService.findForPVOffline()), "empresa");
	}
	
	private void exportarLocalarmazenagem() {
		LocalarmazenagemService localarmazenagemService = Neo.getObject(LocalarmazenagemService.class);
		saveToFile(View.convertToJson(localarmazenagemService.findForPVOffline()), "localarmazenagem");
	}
	
	private void exportarProjeto() {
		ProjetoService projetoService = Neo.getObject(ProjetoService.class);
		saveToFile(View.convertToJson(projetoService.findForPVOffline()), "projeto");
	}

	private void exportarColaborador() {
		ColaboradorService colaboradorService = Neo.getObject(ColaboradorService.class);
		saveToFile(View.convertToJson(colaboradorService.findForPVOffline()), "colaborador");
		
	}

	private void exportarPedidoVendaTipo() {
		PedidovendatipoService pedidovendatipoService = PedidovendatipoService.getInstance();
		saveToFile(View.convertToJson(pedidovendatipoService.findForPVOffline()), "pedidovendatipo");
	}

	/**
	 * Salva os arquivos jsons na pasta de arquivos offline
	 * @param json
	 * @throws IOException 
	 */
	private void saveToFile(JSON json, String filename) {
		try {
			
//			File fileNew = new File(PATH + SEPARATOR + filename + "_temp.json");
//			System.out.println("TESTE:: " + PATH + SEPARATOR + filename + "_temp.json");
			File fileNew = new File(path + SEPARATOR + filename + "_temp.json");
			String jsonStr = "PVOffline.entities." + filename + " = " + json.toString();
			FileOutputStream fileOS; 
			
			if(!fileNew.exists()){
				fileNew.getParentFile().mkdirs();
				fileNew.createNewFile();
			}
			fileOS = FileUtils.openOutputStream(fileNew);
////			
//			fileOS = new FileOutputStream(fileNew, false);
			FileChannel outChannel = fileOS.getChannel();
			ByteBuffer buf = ByteBuffer.allocate(jsonStr.getBytes("iso-8859-1").length);
			
			byte[] bytes = jsonStr.getBytes("iso-8859-1");
			buf.put(bytes);
			buf.flip();
	
			outChannel.write(buf);
			
			fileOS.close();
			
			File fileOld = new File(path + SEPARATOR + filename + ".json");
			fileOld.delete();
			
			fileNew.renameTo(fileOld);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void enviarEmailErro(String mensagem) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro ao gerar arquivosde PVOffline.")
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
}
