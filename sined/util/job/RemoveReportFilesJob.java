package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.util.neo.ReportFileServlet;

public class RemoveReportFilesJob implements StatefulJob{
	
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		try{
			ReportFileServlet.removeReportFiles();
		}catch(Exception e){
//			System.out.println("Erro ao remover os arquivos de relatórios.");
			e.printStackTrace();
		}
	}
}
