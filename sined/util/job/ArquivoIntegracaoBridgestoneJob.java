package br.com.linkcom.sined.util.job;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoIntegracaoBridgestone;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Atividadetipoitem;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Requisicaonota;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.service.ArquivoIntegracaoBridgestoneService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.util.BridgestoneUtil;

import com.ibm.icu.text.SimpleDateFormat;

public class ArquivoIntegracaoBridgestoneJob implements StatefulJob {

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		String nomeCliente = jobContext.getJobDetail().getJobDataMap().getString("nome_banco");

		try{
			ArquivoIntegracaoBridgestoneService integracaoService = ArquivoIntegracaoBridgestoneService.getInstance();
			
			List<ArquivoIntegracaoBridgestone> arquivosNaoProcessados =  integracaoService.findForArquivoIntegracaoBridgestoneJob();
			
			for(ArquivoIntegracaoBridgestone aib : arquivosNaoProcessados){
				List<String> listaRegs = new ArrayList<String>();
				listaRegs.addAll(makeRegs(aib));
				Arquivo arquivo = criaArquivo(listaRegs, "OS"+leftPad(aib.getCdarquivointegracaobridgestone().toString(), 10, '0'), nomeCliente);
				
				aib.setArquivo(arquivo);
				aib.setProcessada(true);
				integracaoService.saveOrUpdate(aib);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private List<String> makeRegs(ArquivoIntegracaoBridgestone aib){
		List<String> listaReg = new ArrayList<String>();
		
		if(aib.getNota().getListaRequisicaonota()!=null && !aib.getNota().getListaRequisicaonota().isEmpty()){ //originário de os
			listaReg.addAll(makeREG01OS(aib));
			
			for(Requisicaonota rn : aib.getNota().getListaRequisicaonota()){
				List<Material> listaMaterial = new ArrayList<Material>();
				for(Materialrequisicao mr : rn.getRequisicao().getListaMateriaisrequisicao()){
					listaMaterial.add(mr.getMaterial());
				}
				listaReg.addAll(makeREG02(listaMaterial, aib.getNota().getEmpresa().getIntegracaobts(), rn.getRequisicao().getCdrequisicao(), "00"));
				listaReg.addAll(makeREG03(listaMaterial, aib.getNota().getEmpresa().getIntegracaobts(), rn.getRequisicao().getCdrequisicao(), "00"));
			}
		}else if(aib.getNota().getListaNotavenda()!=null && !aib.getNota().getListaNotavenda().isEmpty()){ //originário de venda
			listaReg.addAll(makeREG01Venda(aib));
			for(NotaVenda nv : aib.getNota().getListaNotavenda()){
				List<Material> listaMaterial = new ArrayList<Material>();
				for(Vendamaterial vm : nv.getVenda().getListavendamaterial()){
					listaMaterial.add(vm.getMaterial());
				}
				listaReg.addAll(makeREG02(listaMaterial, aib.getNota().getEmpresa().getIntegracaobts(), nv.getVenda().getCdvenda(), "99"));
				listaReg.addAll(makeREG03(listaMaterial, aib.getNota().getEmpresa().getIntegracaobts(), nv.getVenda().getCdvenda(), "99"));
			}
		}
		
		return listaReg;
	}
	
	private List<String> makeREG01OS(ArquivoIntegracaoBridgestone aib){
		List<String> listaReg01OS = new ArrayList<String>();
		
		StringBuilder reg01 = new StringBuilder();
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yy");
		SimpleDateFormat hora = new SimpleDateFormat("hh:mm:ss");
		
		String placa="", modelo="", marca="", anoFabricacao="";
		
		for(Requisicaonota requisicaonota : aib.getNota().getListaRequisicaonota()){
			reg01.append("REG01")
			.append(aib.getNota().getEmpresa()!=null ? leftPad(aib.getNota().getEmpresa().getIntegracaobts(), 6, '0') : leftPad("", 6, '0'))
			.append(leftPad("00"+requisicaonota.getRequisicao().getCdrequisicao(), 30, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? data.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? hora.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '))
			.append(aib.getNotaFiscalServico()!=null && aib.getNotaFiscalServico().getIndicadortipopagamento()!=null 
					? leftPad(aib.getNotaFiscalServico().getIndicadortipopagamento().toString(), 50, ' ') : leftPad("", 50, ' '));
			
			if(requisicaonota.getRequisicao().getAtividadetipo()!=null){
				for(Atividadetipoitem ati : requisicaonota.getRequisicao().getAtividadetipo().getListaAtividadetipoitem()){
					switch(ati.getOrdem()){
						case 1: placa = ati.getNome(); break;
						case 2: marca = ati.getNome(); break;
						case 3: modelo = ati.getNome(); break;
						case 4: anoFabricacao = ati.getNome(); break;
					}
				}
			}
			
			reg01.append(leftPad(placa, 7, ' '))
			.append(leftPad(marca, 50, ' '))
			.append(leftPad(modelo, 50, ' '))
			.append(leftPad(anoFabricacao, 4, ' '))
			.append(aib.getNota().getCliente().getCpf()!=null ? leftPad(aib.getNota().getCliente().getCpf().toString(), 20, ' ') : leftPad("", 20, ' '))
			.append(aib.getNota().getCliente().getCnpj()!=null ? leftPad(aib.getNota().getCliente().getCnpj().toString(), 20, ' ') : leftPad("", 20, ' '))
			.append(leftPad(aib.getNota().getCliente().getNome(), 60, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getNome(), 40, ' ') : leftPad("", 40, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getUf().getSigla(), 2, ' ') : leftPad("", 2, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getNome(), 30, ' ') : leftPad("", 30, ' '))
			.append("0000000000")
			.append(aib.getNota().getTelefoneCliente()!=null ? leftPad(aib.getNota().getTelefoneCliente(), 20, ' ') : leftPad("", 20, ' '))
			.append(leftPad(aib.getNota().getNumero(), 9, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? data.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '))
			.append(leftPad("", 44, ' '))
			.append(leftPad(aib.getNota().getCliente().getEmail(), 100, ' '));
			
			listaReg01OS.add(reg01.toString());
		}
		
		return listaReg01OS;
	}
	
	private List<String> makeREG01Venda(ArquivoIntegracaoBridgestone aib){
		List<String> listaReg01Venda = new ArrayList<String>();
		
		StringBuilder reg01 = new StringBuilder();
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yy");
		SimpleDateFormat hora = new SimpleDateFormat("hh:mm:ss");
		
		String placa="", modelo="", marca="", anoFabricacao="";
		
		for(NotaVenda notaVenda : aib.getNota().getListaNotavenda()){
			reg01.append("REG01")
			.append(aib.getNota().getEmpresa()!=null ? leftPad(aib.getNota().getEmpresa().getIntegracaobts(), 6, '0') : leftPad("", 6, '0'))
			.append(leftPad("99"+notaVenda.getVenda().getCdvenda(), 30, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? data.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? hora.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '))
			.append(notaVenda.getVenda().getPrazopagamento()!=null ? leftPad(notaVenda.getVenda().getPrazopagamento().getNome(), 50, ' ') : leftPad("", 50, ' '));
			
			if(notaVenda.getVenda().getPedidovendatipo()!=null){
				for(Campoextrapedidovendatipo cepvt : notaVenda.getVenda().getPedidovendatipo().getListaCampoextrapedidovenda()){
					switch(cepvt.getOrdem()){
						case 1: placa = cepvt.getNome(); break;
						case 2: marca = cepvt.getNome(); break;
						case 3: modelo = cepvt.getNome(); break;
						case 4: anoFabricacao = cepvt.getNome(); break;
					}
				}
			}
			
			reg01.append(leftPad(placa, 7, ' '))
			.append(leftPad(marca, 50, ' '))
			.append(leftPad(modelo, 50, ' '))
			.append(leftPad(anoFabricacao, 4, ' '))
			.append(aib.getNota().getCliente().getCpf()!=null ? leftPad(aib.getNota().getCliente().getCpf().toString(), 20, ' ') : leftPad("", 20, ' '))
			.append(aib.getNota().getCliente().getCnpj()!=null ? leftPad(aib.getNota().getCliente().getCnpj().toString(), 20, ' ') : leftPad("", 20, ' '))
			.append(leftPad(aib.getNota().getCliente().getNome(), 60, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getNome(), 40, ' ') : leftPad("", 40, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getUf().getSigla(), 2, ' ') : leftPad("", 2, ' '))
			.append(aib.getNota().getEnderecoCliente()!=null ? leftPad(aib.getNota().getEnderecoCliente().getMunicipio().getNome(), 30, ' ') : leftPad("", 30, ' '))
			.append("0000000000")
			.append(aib.getNota().getTelefoneCliente()!=null ? leftPad(aib.getNota().getTelefoneCliente(), 20, ' ') : leftPad("", 20, ' '))
			.append(leftPad(aib.getNota().getNumero(), 9, ' '))
			.append(aib.getNota().getDtEmissao()!=null ? data.format(aib.getNota().getDtEmissao()) : leftPad("", 8, ' '));
			
			List<Arquivonfnota> listaArquivonfNota =  new ArrayList<Arquivonfnota>();
			if(aib.getNota().getListaArquivonfnota()!=null && !aib.getNota().getListaArquivonfnota().isEmpty()){
				listaArquivonfNota.addAll(aib.getNota().getListaArquivonfnota());
				Collections.sort(listaArquivonfNota, new Comparator<Arquivonfnota>() {
					@Override
					public int compare(Arquivonfnota o1, Arquivonfnota o2) {
						return o1.getDtprocessamento().compareTo(o2.getDtprocessamento());
					}
				});
			}
			reg01.append(!listaArquivonfNota.isEmpty() ? leftPad(aib.getNota().getListaArquivonfnota().get(0).getChaveacesso(), 44, ' ') : leftPad("", 44, ' '))
			.append(leftPad(aib.getNota().getCliente().getEmail(), 100, ' '));
			
			listaReg01Venda.add(reg01.toString());
		}
		
		return listaReg01Venda;
	}
	
	private List<String> makeREG02(List<Material> listaMaterial, String integracaoBts, Integer numeroVendaOS, String origem){
		List<String> listaReg02 = new ArrayList<String>();
		StringBuilder reg02 = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#.00000"); 
		
		agrupaMaterial(listaMaterial);
		
		for(Material m : listaMaterial){
			if(m.getProduto()!=null && m.getProduto()){
				reg02.append("REG02")
				.append(leftPad(integracaoBts, 6, '0'))
				.append(leftPad(origem+numeroVendaOS, 30, ' '))
				.append(leftPad(m.getCdmaterial().toString(), 50, ' '))
				.append(leftPad(m.getNome(), 100, ' '))
				.append(leftPad(m.getQtdMaterialNota()+"00000", 14, '0'))
				.append(m.getValorvenda()!=null ? leftPad(df.format(m.getValorvenda()).replace(".", ""), 14, ' ') : leftPad("", 14, ' '))
				.append(leftPad(m.getMaterialgrupo().getCdmaterialgrupo().toString(), 20, ' '));
				
				List<Materialfornecedor> listaFornecedor = new ArrayList<Materialfornecedor>(m.getListaFornecedor());
				reg02.append(!listaFornecedor.isEmpty() ? leftPad(listaFornecedor.get(0).getFornecedor().getNome(), 50, ' ') : leftPad("", 50, ' ') );
				
				listaReg02.add(reg02.toString());
			}
		}
		
		return listaReg02;
		
	}
	
	private List<String> makeREG03(List<Material> listaMaterial, String integracaoBts, Integer numeroVendaOS, String origem){
		List<String> listaReg03 = new ArrayList<String>();
		StringBuilder reg03 = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#.00"); 
		
		agrupaMaterial(listaMaterial);
		
		for(Material m : listaMaterial){
			if(m.getServico()!=null && m.getServico()){
				reg03.append("REG03")
				.append(leftPad(integracaoBts, 6, '0'))
				.append(leftPad(origem+numeroVendaOS, 30, ' '))
				.append(leftPad(m.getCdmaterial().toString(), 50, ' '))
				.append(leftPad(m.getNome(), 100, ' '))
				.append(m.getValorvenda()!=null ? leftPad(df.format(m.getValorvenda()).replace(".", ""), 14, ' ') : leftPad("", 14, ' '))
				.append(leftPad(m.getQtdMaterialNota()+"00", 14, '0'))
				.append(leftPad(m.getMaterialgrupo().getCdmaterialgrupo().toString(), 20, ' '));
				
				listaReg03.add(reg03.toString());
			}
		}
		
		return listaReg03;
		
	}
	

	private void agrupaMaterial(List<Material> lista){
		List<Material> listaMaterial = new ArrayList<Material>(lista);
		List<Material> listaAux = new ArrayList<Material>();
		
		for(int i=0; i<listaMaterial.size(); i++){
			for(int k = i+1; k<listaMaterial.size(); k++){
				
				if(listaMaterial.get(i).getCdmaterial() == listaMaterial.get(k).getCdmaterial() 
						&& listaMaterial.get(i).getNome() == listaMaterial.get(k).getNome()){
					
					listaMaterial.get(i).setQtdMaterialNota(listaMaterial.get(i).getQtdMaterialNota()+1);
					listaAux.add(listaMaterial.get(k));
				}
			}
		}
		
		listaMaterial.removeAll(listaAux);
		
	}
	
	private String leftPad(String str, int size, char padChar){
		if(str==null) str = "";
		String rs = StringUtils.leftPad(str, size, padChar);
		if(rs.length() > size)
			rs = rs.substring(0, size-1);
		return rs;
	}
	
	private Arquivo criaArquivo(List<String> linhas, String nome, String nomeCliente) throws IOException{
		String path = BridgestoneUtil.getDirBridgestone(nomeCliente);
		
		File file = new File(path+"\\"+nome+".txt");
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(String linha : linhas){
			if(linhas.indexOf(linha)!=0)bw.newLine();
			bw.write(linha);
		}
		bw.close();
		
		byte[] bFile = new byte[(int) file.length()];
		FileInputStream fileInputStream=null;
		
	    fileInputStream = new FileInputStream(file);
	    fileInputStream.read(bFile);
	    fileInputStream.close();
				
	    Arquivo arquivo = new Arquivo(bFile, file.getName(), "text/plain");
	    
	    ArquivoService.getInstance().saveOrUpdate(arquivo);
	    
	    return arquivo;
		
	}

}
