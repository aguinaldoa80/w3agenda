package br.com.linkcom.sined.util.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.naming.NameNotFoundException;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.StatefulJob;

import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.VendaSincronizacaoFlex;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendaSincronizacaoFlexService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub;
import br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.ArrayOfInscricaoProcessoSeletivo;
import br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.InscricaoProcessoSeletivo;
import br.com.linkcom.sined.util.flexdevelopers.Integracao_Inscricoes_Processo_SeletivoStub.InscricaoProcessoSeletivo_Retorno;

public class IntegracaoFlexDevelopersJob implements StatefulJob {

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = null;
		String nomeBanco = null;
		
		jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome("FLEX_DEVELOPERS_SERVIDOR"))){
			try{
				System.setProperty("javax.net.ssl.trustStoreType", "JKS");
		        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		        path = path.substring(0, path.indexOf("br/")) + "flexnuvem.keystore";
		        System.setProperty("javax.net.ssl.trustStore", path); 
		        		
				nomeBanco = jobContext.getJobDetail().getJobDataMap().get("nome_banco").toString();
				
				Integracao_Inscricoes_Processo_SeletivoStub stub = new Integracao_Inscricoes_Processo_SeletivoStub(ParametrogeralService.getInstance().getValorPorNome("FLEX_DEVELOPERS_URL"));
				Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao request = new Integracao_Inscricoes_Processo_SeletivoStub.Importa_Inscricao();
				request.setSUsuario(ParametrogeralService.getInstance().getValorPorNome("FLEX_DEVELOPERS_USUARIO"));
				request.setSSenha(ParametrogeralService.getInstance().getValorPorNome("FLEX_DEVELOPERS_SENHA"));
				
				List<VendaSincronizacaoFlex> lista = VendaSincronizacaoFlexService.getInstance().findForIntegracaoFlex();
				if(lista != null && !lista.isEmpty()){
					for(VendaSincronizacaoFlex vendaFlex : lista){
						ArrayOfInscricaoProcessoSeletivo arrayInscricoes = new Integracao_Inscricoes_Processo_SeletivoStub.ArrayOfInscricaoProcessoSeletivo();

						Venda venda = VendaService.getInstance().findForVendaSincronizacaoFlex(vendaFlex);
						Projeto projeto = venda.getProjeto();
						Cliente cliente = venda.getCliente();
						
						if(venda != null && projeto != null && cliente != null){
							InscricaoProcessoSeletivo inscricao = new InscricaoProcessoSeletivo();
							
							inscricao.setNome_do_Processo(projeto.getNome());
							inscricao.setAno_Processo(projeto.getDtprojeto() != null ? SinedDateUtils.getAno(projeto.getDtprojeto()) : 0);
							inscricao.setNumero_Processo(projeto.getCdprojeto()); 
							inscricao.setNumero_Inscricao(venda.getCdvenda()); 
							inscricao.setData_da_Prova(0);
							inscricao.setNome_do_Aluno(cliente.getNome()); 
							inscricao.setSexo(cliente.getSexo() != null ? formataSexo(cliente.getSexo()) : " ");
							inscricao.setData_de_Nascimento(cliente.getDtnascimento() != null ? formataData(cliente.getDtnascimento()) : 0);
							inscricao.setNome_do_Pai(cliente.getPai() != null ? cliente.getPai() : " ");
							inscricao.setNome_da_Mae(cliente.getMae() != null ? cliente.getMae() : " ");
							inscricao.setEstado_Civil(cliente.getEstadocivil() != null ? formataEstadoCivil(cliente.getEstadocivil()) : 0);
							inscricao.setRaca(" ");
							inscricao.setCPF(cliente.getCpf() != null ? formataCPF(cliente.getCpf()) : " "); 
							inscricao.setRG(cliente.getRg() != null ? cliente.getRg() : " ");
							inscricao.setRG_SSP(cliente.getOrgaoemissorrg() != null ? cliente.getOrgaoemissorrg() : " ");
							inscricao.setTitulo_de_Eleitor(" ");
							inscricao.setZona_do_Titulo_de_Eleitor(" ");
							inscricao.setExpedicao_do_Titulo_de_Eleitor(" ");
							inscricao.setCertificado_Militar(" ");
							inscricao.setExpedicao_Certificado_Militar(" ");
							inscricao.setCidade_de_Nascimento(cliente.getMunicipionaturalidade()!=null ? cliente.getMunicipionaturalidade().getNome() : " ");
							inscricao.setSigla_Estado_de_Nascmento(cliente.getMunicipionaturalidade()!=null && cliente.getMunicipionaturalidade().getUf() != null ? cliente.getMunicipionaturalidade().getUf().getSigla() : " " );
							inscricao.setNacionalidade(cliente.getNacionalidade()!=null ? cliente.getNacionalidade().getDescricao() : " ");
							inscricao.setDeficiencia_Fisica(0);
							inscricao.setDeficiencia_Auditiva(0);
							inscricao.setDeficiencia_Visual(0);
							inscricao.setCEP(cliente.getEnderecoCep());
							
							Endereco endereco = venda.getEndereco();
							inscricao.setTipo_Logradouro(" ");
							inscricao.setEndereco(endereco != null && endereco.getLogradouro() != null ? endereco.getLogradouro() : " ");
							inscricao.setBairro(endereco != null && endereco.getBairro() != null ? endereco.getBairro() : " ");
							inscricao.setCidade(endereco != null && endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : " ");
							inscricao.setSigla_Estado(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : " ");
							inscricao.setPais(endereco != null && endereco.getPais() != null ? endereco.getPais().getNome() : " ");
							inscricao.setNumero(endereco != null && endereco.getNumero() != null ? endereco.getNumero() : " ");
							inscricao.setComplemento(endereco != null && endereco.getComplemento() != null ? endereco.getComplemento() : " ");	
							inscricao.setCaixa_Postal(endereco != null && endereco.getCaixapostal() != null ? venda.getEndereco().getCaixapostal() : " ");
							
							inscricao.setDDD_Telefone(dddTelefone(cliente.getTelefoneprincipal()));
							inscricao.setDDD_Celular(dddTelefone(cliente.getTelefonecelular()));
							inscricao.setTelefone(telefoneSemDDD(cliente.getTelefoneprincipal()));
							inscricao.setCelular(telefoneSemDDD(cliente.getTelefonecelular()));
							inscricao.setEmail(cliente.getEmail() != null ? cliente.getEmail() : " ");
							
							inscricao.setUsar_Endereco_Comercial(0);
							inscricao.setCEP_Comercial(" ");
							inscricao.setTipo_Logradouro_Comercial(" ");
							inscricao.setEndereco_Comercial(" ");
							inscricao.setBairro_Comercial(" ");
							inscricao.setCidade_Comercial(" ");
							inscricao.setSigla_Estado_Comercial(" ");
							inscricao.setPais_Comercial(" ");
							inscricao.setNumero_Comercial(" ");
							inscricao.setComplemento_Comercial(" ");
							inscricao.setDDD_Telefone_Comercial(" ");
							inscricao.setTelefone_Comercial(" ");
							inscricao.setEmail_Comercial(" ");
							inscricao.setCaixa_postal_Comercial(" ");
							inscricao.setEmpresa(" ");
							inscricao.setContato_Comercial(" ");
							inscricao.setEscola_Ensino_Medio(" ");
							inscricao.setCidade_Escola_Ensino_Medio(" ");
							inscricao.setSigla_EstadoEscola_Ensino_Medio(" ");
							inscricao.setAno_conclusao_Ensino_Medio(0);
							inscricao.setFaculdade(" ");
							inscricao.setCidade_Faculdade(" ");
							inscricao.setSigla_Estado_Faculdade(" ");
							inscricao.setTitulo_Obitido(" ");
							inscricao.setTotal_pontos_Processo_Seletivo(new BigDecimal(0));
							inscricao.setClassificacao_Processo_Seletivo(0);
							inscricao.setClassificacao_no_Curso(new BigDecimal(0));
							
							inscricao.setCurso(venda.getListavendamaterial() != null && venda.getListavendamaterial().get(0).getMaterial() != null && venda.getListavendamaterial().get(0).getMaterial().getIdentificacao() != null ? venda.getListavendamaterial().get(0).getMaterial().getIdentificacao() : ""); //material � transient
							arrayInscricoes.addInscricaoProcessoSeletivo(inscricao);
							
							request.setInscricoesProcessoSeletivo(arrayInscricoes);
							
							Thread.sleep(3000);  
							
							Integracao_Inscricoes_Processo_SeletivoStub.Importa_InscricaoResponse response = stub.importa_Inscricao(request);
							
							if(response.getImporta_InscricaoResult().getInscricaoProcessoSeletivo_Retorno().length>0){
								InscricaoProcessoSeletivo_Retorno retorno = response.getImporta_InscricaoResult().getInscricaoProcessoSeletivo_Retorno()[0];
								String msg = retorno.getErro();
								if(StringUtils.isEmpty(msg)){
									msg = "Integra��o enviada com sucesso.";
									VendaSincronizacaoFlexService.getInstance().atualizaDtSincronizacao(vendaFlex);
								}
								
								Vendahistorico vendaHistorico = new Vendahistorico();
								vendaHistorico.setVenda(venda);
								vendaHistorico.setAcao("Integra��o");
								vendaHistorico.setObservacao(msg);
								
								VendahistoricoService.getInstance().saveOrUpdate(vendaHistorico);
							}
						}else{
							Vendahistorico vendaHistorico = new Vendahistorico();
							vendaHistorico.setVenda(venda);
							vendaHistorico.setAcao("Integra��o");
							String msg = "Integra��o n�o realizada ";
							if(projeto == null){
								msg += "o Projeto n�o pode ser nulo ";
							}
							if(cliente == null){
								msg += "o Cliente n�o pode ser nulo";
							}
							vendaHistorico.setObservacao(msg);
							
							VendahistoricoService.getInstance().saveOrUpdate(vendaHistorico);
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();				
				if(e.getCause() instanceof NameNotFoundException){
					try{
						jobContext.getScheduler().deleteJob(jobName, Scheduler.DEFAULT_GROUP);
					} catch (Exception e2) {}
				} else {
					String nomeMaquina = "N�o encontrado.";
					try {  
			            InetAddress localaddr = InetAddress.getLocalHost();  
			            nomeMaquina = localaddr.getHostName();  
			        } catch (UnknownHostException e3) {}  
					
					StringWriter stringWriter = new StringWriter();
					e.printStackTrace(new PrintWriter(stringWriter));
					PneuService.getInstance().enviarEmailErro("[W3ERP] Erro ao fazer a integra��o","Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ", BD: " + nomeBanco + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
				}
			}
		}
	}

	protected String formataCPF(Cpf cpf) {
		return StringUtils.leftPad(cpf.getValue(), 11, '0');
	}

	protected int formataEstadoCivil(Estadocivil estadocivil) {
		if(Estadocivil.SOLTEIRO.getCdestadocivil().equals(estadocivil.getCdestadocivil()))
			return 1;
		else if(Estadocivil.CASADO.getCdestadocivil().equals(estadocivil.getCdestadocivil()))
			return 2;
		else if(Estadocivil.DESQUITADO.getCdestadocivil().equals(estadocivil.getCdestadocivil()))
			return 3;
		else if(Estadocivil.OUTROS.getCdestadocivil().equals(estadocivil.getCdestadocivil()))
			return 4;
		else if(Estadocivil.DIVORCIADO.getCdestadocivil().equals(estadocivil.getCdestadocivil()))
			return 5;
		return 0;
	}

	protected String formataSexo(Sexo sexo) {
		if(Sexo.FEMININO.equals(sexo.getCdsexo())){
			return "F";
		}else{
			return "M";
		}
	}

	protected int formataData(Date dtnascimento) {
		if(dtnascimento == null){
			return 0;			
		}else{
	    	Calendar cal = Calendar.getInstance();
	    	cal.setTime(dtnascimento);
	    	int dia = cal.get(Calendar.DAY_OF_MONTH);
	    	int mes = cal.get(Calendar.MONTH)+1;
	    	int ano = cal.get(Calendar.YEAR);
			
	    	String diaString = ""+dia;
	    	if(dia < 10){
	    		diaString = "0"+dia;
	    	}
	    	
	    	String mesString = ""+mes;
	    	if(mes < 10){
	    		mesString = "0"+mes;
	    	}
	    	
	    	String anoString = ""+ano;
	    	if(anoString.length() < 4){
	    		if(anoString.length() == 1){
	    			anoString = "000"+ano;
	    		}else if(anoString.length() == 2){
	    			anoString = "00"+ano;
	    		}else{
	    			anoString = "000"+ano;
	    		}
	    	}
	    	
	    	String data = ""+diaString+""+mesString+""+anoString;
	    	return Integer.parseInt(data);
		}
	}

	protected String telefoneSemDDD(String telefone){
		String numero = "";
		if(telefone != null && StringUtils.isNotBlank(telefone)){
			if(telefone.indexOf(")") != -1){
				numero = telefone.substring(4,telefone.length());
			}else{
				numero = telefone.substring(2,telefone.length());				
			}
		}
		return numero.trim();
	}
	
	protected String dddTelefone(String telefone){
		String ddd = "";
		if(telefone != null && StringUtils.isNotBlank(telefone)){
			if(telefone.indexOf(")") != -1){
				ddd = telefone.substring(1,3);	
			}else{
				ddd = telefone.substring(0,2);			
			}
		}
		return ddd.trim();
	}
}
