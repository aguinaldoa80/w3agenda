package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumExportacaoMovimentosUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumImportacaoClientesUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumImportacaoProdutosUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

public class IntegracaoEmporiumCriacaoJob implements StatefulJob {

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		String dataSource = Thread.currentThread().getName().substring(25);
		
		try {
			IntegracaoEmporiumImportacaoProdutosUtil.util.executarJobIntegracao(dataSource);
		} catch (Exception e) {
			e.printStackTrace();
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Ocorreu um erro ao criar arquivos de importação de produtos.");
		}
		
		try {
			IntegracaoEmporiumImportacaoClientesUtil.util.executarJobIntegracao(dataSource);
		} catch (Exception e) {
			e.printStackTrace();
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Ocorreu um erro ao criar arquivos de importação de clientes.");
		}
		
		try {
			IntegracaoEmporiumExportacaoMovimentosUtil.util.executarJobIntegracao(dataSource);
		} catch (Exception e) {
			e.printStackTrace();
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Ocorreu um erro ao criar arquivos de exportação de movimentos.");
		}
	}

}
