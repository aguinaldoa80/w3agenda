package br.com.linkcom.sined.util.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.util.SinedUtil;

public class IntegracaoEmporiumCriacaoNotaJob implements StatefulJob{

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		List<Emporiumvenda> listaEmporiumVenda = EmporiumvendaService.getInstance().findForCriarNotas();
		
//		String nome_banco = Thread.currentThread().getName().substring(25);
		
		if(SinedUtil.isListNotEmpty(listaEmporiumVenda)){
			NotafiscalprodutoService.getInstance().criasNotasByCupom(listaEmporiumVenda);
		}
		
	}

}
