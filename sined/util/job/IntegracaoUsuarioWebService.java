package br.com.linkcom.sined.util.job;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.UsuarioSincronizacaoDashboard;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaEnum;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.UsuarioSincronizacaoDashboardService;
import br.com.linkcom.sined.util.SHA512Util;
import br.com.linkcom.sined.util.rest.dashboard.UsuarioRESTBean;
import br.com.linkcom.sined.util.rest.dashboard.UsuarioRESTClient;
import br.com.linkcom.sined.util.rest.dashboard.UsuarioRESTModel;

public class IntegracaoUsuarioWebService implements StatefulJob {

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName); 
		
		
		System.out.println("Iniciando JOB de Integra��o de usu�rio com o Sistema Dashboard");
		sincronizarUsuarios();
		sincronizarUsuariosDelete();
	}

	private void sincronizarUsuariosDelete() {
		UsuarioRESTClient usuarioRESTClient = new UsuarioRESTClient();
		UsuarioRESTModel usuarioRESTModel = new UsuarioRESTModel(); 
		List<UsuarioSincronizacaoDashboard> listaUsuarioSincronizacao = UsuarioSincronizacaoDashboardService.getInstance().findForSincronizacaoDelete();
		
		for(UsuarioSincronizacaoDashboard usuarioSincronizacao : listaUsuarioSincronizacao){
			try {
				UsuarioRESTBean usuarioBean = new UsuarioRESTBean(usuarioSincronizacao.getIdusuario());
				usuarioBean.setSistemaOrigem("w3erp");		
				
				usuarioRESTModel = usuarioRESTClient.deleteUsuario(usuarioBean);
				System.out.println(usuarioRESTModel);
				if(usuarioRESTModel.isErro()){
					UsuarioSincronizacaoDashboardService.getInstance().updateError(usuarioBean.getIdExterno(), usuarioRESTModel.getCodigo_erro(), usuarioRESTModel.getMensagem());
				}else{
					UsuarioSincronizacaoDashboardService.getInstance().updateSincronizacao(usuarioBean.getIdExterno(),usuarioRESTModel.getCodigo_erro(), usuarioRESTModel.getMensagem());
				}
				
			}catch (Exception e) {
				System.out.println(usuarioSincronizacao.getIdusuario() + " :: " + usuarioRESTModel.getCodigo_erro() + " :: " + usuarioRESTModel.getMensagem());
				e.printStackTrace();
				try{
					UsuarioSincronizacaoDashboardService.getInstance().updateError(usuarioSincronizacao.getIdusuario(), 99, e.getMessage());
				}catch (Exception ex) {
					System.out.println(usuarioSincronizacao.getIdusuario() + " :: " + usuarioRESTModel.getCodigo_erro() + " :: " + usuarioRESTModel.getMensagem());
					System.out.println("Erro ao registrar erro de sincroniza��o do usuario:");
					ex.printStackTrace();
				}
			}	
		}
	}

	private void sincronizarUsuarios() {
		UsuarioRESTClient usuarioRESTClient = new UsuarioRESTClient();
		UsuarioRESTModel usuarioRESTModel = new UsuarioRESTModel(); 
		List<UsuarioSincronizacaoDashboard> listaUsuarioSincronizacao = UsuarioSincronizacaoDashboardService.getInstance().findForSincronizacao();
		
		for(UsuarioSincronizacaoDashboard usuarioSincronizacao : listaUsuarioSincronizacao){
			try {
				UsuarioRESTBean usuarioBean = new UsuarioRESTBean(usuarioSincronizacao.getIdusuario());
				Usuario usuario = UsuarioService.getInstance().findById(usuarioBean.getIdExterno());
					usuarioBean.setNome(usuario.getNome());
					usuarioBean.setCpf(usuario.getCpfOuCnpj());
					usuarioBean.setEmail(usuario.getEmail());
					usuarioBean.setIdExterno(usuario.getCdpessoa());
					usuarioBean.setLogin(usuario.getLogin());
					if(Tipopessoa.PESSOA_JURIDICA.equals(usuario.getTipopessoa())){
						usuarioBean.setTipoPessoa(TipoPessoaEnum.PESSOA_JURIDICA);						
					}else{
						usuarioBean.setTipoPessoa(TipoPessoaEnum.PESSOA_FISICA);
					}
					if(StringUtils.isBlank(usuarioSincronizacao.getToken())){
						String token =	SHA512Util.encrypt(usuario.getLogin() + "&&&" + usuario.getSenha());
						usuarioBean.setToken(token);						
					}else{
						usuarioBean.setToken(usuarioSincronizacao.getToken());
					}	
					usuarioBean.setSistemaOrigem("w3erp");
				
								
				usuarioRESTModel = usuarioRESTClient.saveUsuario(usuarioBean);
				if(usuarioRESTModel.isErro()){
					UsuarioSincronizacaoDashboardService.getInstance().updateError(usuarioBean.getIdExterno(), usuarioRESTModel.getCodigo_erro(), usuarioRESTModel.getMensagem());
				}else{
					UsuarioSincronizacaoDashboardService.getInstance().updateSincronizacao(usuarioBean.getIdExterno(), usuarioRESTModel.getToken(), usuarioRESTModel.getCdpessoa(),usuarioRESTModel.getCodigo_erro(), usuarioRESTModel.getMensagem());
				}
			}catch (Exception e) {
				System.out.println(usuarioSincronizacao.getIdusuario() + " :: " + usuarioRESTModel.getCodigo_erro() + " :: " + usuarioRESTModel.getMensagem());
				e.printStackTrace();
				try{
					UsuarioSincronizacaoDashboardService.getInstance().updateError(usuarioSincronizacao.getIdusuario(), 99, e.getMessage());
				}catch (Exception ex) {
					System.out.println(usuarioSincronizacao.getIdusuario() + " :: " + usuarioRESTModel.getCodigo_erro() + " :: " + usuarioRESTModel.getMensagem());
					System.out.println("Erro ao registrar erro de sincroniza��o do usuario:");
					ex.printStackTrace();
				}
			}
			
		}
	}
}
