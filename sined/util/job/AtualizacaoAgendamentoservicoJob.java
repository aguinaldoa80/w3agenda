package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.service.AgendamentoservicoService;

public class AtualizacaoAgendamentoservicoJob implements StatefulJob {

	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
//		System.out.println("Iniciando tarefa agendada pelo Quartz: " + jobName);
		try{
			AgendamentoservicoService.getInstance().updateAux();
		} catch (Exception e) {}
//		System.out.println("Tarefa do Quartz conclu�da: " + jobName);
	}

}
