package br.com.linkcom.sined.util.job;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.bean.Aplicacao;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariamaterial;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariarotina;
import br.com.linkcom.sined.geral.service.OrdemservicoveterinariaService;
import br.com.linkcom.sined.geral.service.OrdemservicoveterinariamaterialService;
import br.com.linkcom.sined.geral.service.OrdemservicoveterinariarotinaService;
import br.com.linkcom.sined.util.IteracaoVeterinariaDataHora;
import br.com.linkcom.sined.util.IteracaoVeterinariaParametro;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;

public class IteracaoVeterinariaJob implements StatefulJob {

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		System.out.println("*** JOB ITERACAO VETERINARIA INICIO *** " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Timestamp(System.currentTimeMillis())));
		
		Thread.currentThread().setName(jobContext.getJobDetail().getName());
		
		for (Ordemservicoveterinariamaterial ordemservicoveterinariamaterial: OrdemservicoveterinariamaterialService.getInstance().findForIteracao()){
			List<IteracaoVeterinariaDataHora> listaDataHoraAplicacao = new ArrayList<IteracaoVeterinariaDataHora>();
			if (ordemservicoveterinariamaterial.getListaAplicacao()!=null){
				for (Aplicacao aplicacao: ordemservicoveterinariamaterial.getListaAplicacao()){
					listaDataHoraAplicacao.add(new IteracaoVeterinariaDataHora(aplicacao.getDtprevisto(), aplicacao.getHrprevisto()));
				}
			}
			
			IteracaoVeterinariaParametro parametro = new IteracaoVeterinariaParametro(ordemservicoveterinariamaterial.getPeriodicidade(), ordemservicoveterinariamaterial.getFrequencia(), ordemservicoveterinariamaterial.getDtinicio(), ordemservicoveterinariamaterial.getHrinicio(), ordemservicoveterinariamaterial.getPrazotermino(), ordemservicoveterinariamaterial.getQuantidadeiteracao(), listaDataHoraAplicacao);
			IteracaoVeterinariaRetorno retorno = OrdemservicoveterinariaService.getInstance().processaIteracao(parametro);
			OrdemservicoveterinariamaterialService.getInstance().updateIteracao(ordemservicoveterinariamaterial, retorno);
		}		

		for (Ordemservicoveterinariarotina ordemservicoveterinariarotina: OrdemservicoveterinariarotinaService.getInstance().findForIteracao()){
			List<IteracaoVeterinariaDataHora> listaDataHoraAplicacao = new ArrayList<IteracaoVeterinariaDataHora>();
			if (ordemservicoveterinariarotina.getListaAplicacao()!=null){
				for (Aplicacao aplicacao: ordemservicoveterinariarotina.getListaAplicacao()){
					listaDataHoraAplicacao.add(new IteracaoVeterinariaDataHora(aplicacao.getDtprevisto(), aplicacao.getHrprevisto()));
				}
			}
			
			IteracaoVeterinariaParametro parametro = new IteracaoVeterinariaParametro(ordemservicoveterinariarotina.getFrequencia(), ordemservicoveterinariarotina.getAcada(), ordemservicoveterinariarotina.getDtinicio(), ordemservicoveterinariarotina.getHrinicio(), ordemservicoveterinariarotina.getPrazotermino(), ordemservicoveterinariarotina.getQuantidadeiteracao(), listaDataHoraAplicacao);
			IteracaoVeterinariaRetorno retorno = OrdemservicoveterinariaService.getInstance().processaIteracao(parametro);
			OrdemservicoveterinariarotinaService.getInstance().updateIteracao(ordemservicoveterinariarotina, retorno);
		}
		
		System.out.println("*** JOB ITERACAO VETERINARIA FIM    *** " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Timestamp(System.currentTimeMillis())));
	}
}
