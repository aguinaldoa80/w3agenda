package br.com.linkcom.sined.util.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.correios.webservice.resource.CorreiosUtils;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ExpedicaoService;

public class ConsultaEventosCorreiosJob implements StatefulJob{

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		System.out.println("Chamou");
		if("".equals("")){
			//return;
		}
		System.out.println("In�cio tarefa agendada pelo Quartz (boleto autom�tico): " + jobName);
		try{
			List<Empresa> listaEmpresa = EmpresaService.getInstance().findWithConfiguracaoCorreios();
			for(Empresa empresa: listaEmpresa){
				List<Expedicao> listaExpedicao = ExpedicaoService.getInstance().findForRastreamentoCorreios(empresa);
				CorreiosUtils.consulta(empresa, listaExpedicao, jobName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Fim da tarefa agendada pelo Quartz (Eventos dos correios): " + jobName);
	}
}
