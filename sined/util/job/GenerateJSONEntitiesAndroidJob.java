package br.com.linkcom.sined.util.job;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.naming.NameNotFoundException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.geral.service.PedidovendaService;

public class GenerateJSONEntitiesAndroidJob implements StatefulJob{
		
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = null;
		String nomeBanco = null;
		try{
			jobName = jobContext.getJobDetail().getName();
			Thread.currentThread().setName(jobName);
			
			nomeBanco = jobContext.getJobDetail().getJobDataMap().get("nome_banco").toString();
			PedidovendaService.getInstance().gerarArquivosAndroid(nomeBanco);
		}catch (Exception e) {
			e.printStackTrace();
			if(e.getCause() instanceof NameNotFoundException){
				try{
					jobContext.getScheduler().deleteJob(jobName, Scheduler.DEFAULT_GROUP);
				} catch (Exception e2) {}
			} else {
				String nomeMaquina = "N�o encontrado.";
				try {  
		            InetAddress localaddr = InetAddress.getLocalHost();  
		            nomeMaquina = localaddr.getHostName();  
		        } catch (UnknownHostException e3) {}  
				
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				PedidovendaService.getInstance().enviarEmailErro("[W3ERP] Erro ao gerar arquivos android.","Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ", BD: " + nomeBanco + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
			}
		}
		
	}
	
	
}
