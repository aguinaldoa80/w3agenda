package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.util.veterinaria.EnviaEmailProximaVacinacaoUtil;

public class EnviaEmailProximaVacinacaoJob implements StatefulJob {

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getJobDataMap().getString("job_name");
		Thread.currentThread().setName(jobName);
		
		try {
			EnviaEmailProximaVacinacaoUtil.util.executarJobEnviaEmail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
