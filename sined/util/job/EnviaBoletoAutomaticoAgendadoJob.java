package br.com.linkcom.sined.util.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import br.com.linkcom.sined.util.ControleAcessoUtil;

public class EnviaBoletoAutomaticoAgendadoJob implements StatefulJob{

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		String jobName = jobContext.getJobDetail().getName();
		Thread.currentThread().setName(jobName);
		
		System.out.println("Início tarefa agendada pelo Quartz (boleto automático): " + jobName);
        ControleAcessoUtil.util.envioEmailBoletoAutomaticoAgendado(jobName, Boolean.TRUE);
		System.out.println("Fim da tarefa agendada pelo Quartz (boleto automático): " + jobName);
	}
}
