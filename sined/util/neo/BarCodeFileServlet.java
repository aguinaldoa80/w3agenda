/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util.neo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import br.com.linkcom.lkbanco.boleto.BarCode2of5;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.FileDAO;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.util.ArquivoNaoEncontradoException;
import br.com.linkcom.sined.util.tag.TagFunctions;


public class BarCodeFileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String DOWNLOAD_BARCODE_PATH = "/BARCODE";
	
	public static final String DOWNLOAD_BARCODE_MAP = "NEO_DOWNLOAD_BARCODE_MAP";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String barcode;
		try {
			barcode = extractBarCode(request);
		}
		catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		// Obt�m o conte�do
		byte[] result = BarCode2of5.geraCodigobarrasJPEG(barcode);
		Resource resource = new Resource("image/jpeg", "barcode.jpeg", result);
        if(resource == null){
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
        }
        resource.setSize(result.length);
        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		if (resource.getSize()>=0) {
			response.setContentLength(resource.getSize());
		}

		response.getOutputStream().write(resource.getContents());
		response.flushBuffer();
	}

	private String extractBarCode(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		String[] campos = requestURI.split("/");
		return campos[campos.length-1];
	}

	public static void addCdfile(HttpSession session, Long cdfile) {
		getMap(session).put(cdfile, cdfile);
	}

	public static Long getCdfile(HttpSession session, Long cdfile) {
		return getMap(session).get(cdfile);
	}

	private static HashMap<Long, Long> getMap(HttpSession session) {
		@SuppressWarnings("unchecked")
		HashMap<Long, Long> map = (HashMap<Long, Long>) session.getAttribute(DOWNLOAD_BARCODE_MAP);
		
		if (map == null) {
			map = new HashMap<Long, Long>();
			session.setAttribute(DOWNLOAD_BARCODE_MAP, map);
		}

		return map;
	}

	@SuppressWarnings("unchecked")
	protected Resource getResource(HttpServletRequest request, Long cdfile){
		DefaultListableBeanFactory defaultListableBeanFactory = Neo.getApplicationContext().getConfig().getDefaultListableBeanFactory();
		Map beansOfType;
		do{
			beansOfType = defaultListableBeanFactory.getBeansOfType(FileDAO.class); 
			if(beansOfType.size() == 0){
				defaultListableBeanFactory = (DefaultListableBeanFactory)defaultListableBeanFactory.getParentBeanFactory();
			} else{
				break;
			}
		} while(defaultListableBeanFactory != null);
		
		
		if(beansOfType.size() == 1){
			FileDAO<?> fileDAO = (FileDAO<?>) beansOfType.values().iterator().next();
			Class<?>[] allClassesOfTypeFile = Neo.getApplicationContext().getClassManager().getAllClassesOfType(File.class);
			if(allClassesOfTypeFile.length == 1){
				File file;
				try {
					file = (File)allClassesOfTypeFile[0].newInstance();
					file.setCdfile(cdfile);
					try {
						file = fileDAO.loadWithContents(file);
					} catch (Exception e) {
						request.setAttribute("url", TagFunctions.getPartialURL());
						throw new ArquivoNaoEncontradoException("Arquivo n�o encontrado.");
					}
					Resource resource = new Resource(file.getContenttype(), file.getName(), file.getContent());
					Long size = file.getSize();
					if (size != null) {
						resource.setSize(size.intValue());
					}
					return resource;
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				}
			}
		}
		throw new RuntimeException("Estenda a classe DownloadFileServlet e sobrescreva o m�todo getResource");
	}
	
	protected long getLastModified(HttpServletRequest request, Long cdfile){
		return -1;
	}

}
