package br.com.linkcom.sined.util.neo;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.FileDAO;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.util.ArquivoNaoEncontradoException;
import br.com.linkcom.sined.util.tag.TagFunctions;

public class DownloadPublicoServlet extends HttpServlet{

	private static final long serialVersionUID = 4065090582597775302L;
	
	private static final List<String> urls =  new Vector<String>();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String contexto = request.getContextPath();
		String urlServidor = request.getRequestURL().toString();
		String servidor = StringUtils.substringBefore(urlServidor, contexto);
		String urlRequisitada = servidor + request.getRequestURI(); 

		if (!urls.contains(urlRequisitada)){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		//URL processada, removendo da lista de permiss�es
		urls.remove(urlRequisitada);
		
		Long cdfile = extractCdfile(request);
		
		if (cdfile == null){
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		// Obt�m o conte�do
		Resource resource = null;
		
		try{
			resource = getResource(request, cdfile);
	        if(resource == null){
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        	return;
	        }
		} catch (ArquivoNaoEncontradoException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}

        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		//response.setHeader("Last-Modified", );
		if (resource.getSize()>=0) {
			response.setContentLength(resource.getSize());
		}

		response.getOutputStream().write(resource.getContents());
		response.flushBuffer();
	}

	@Override
	protected long getLastModified(HttpServletRequest request) {
		return -1;
	}
	
	@SuppressWarnings("unchecked")
	protected Resource getResource(HttpServletRequest request, Long cdfile){
		DefaultListableBeanFactory defaultListableBeanFactory = Neo.getApplicationContext().getConfig().getDefaultListableBeanFactory();
		Map beansOfType;
		do{
			beansOfType = defaultListableBeanFactory.getBeansOfType(FileDAO.class); 
			if(beansOfType.size() == 0){
				defaultListableBeanFactory = (DefaultListableBeanFactory)defaultListableBeanFactory.getParentBeanFactory();
			} else{
				break;
			}
		} while(defaultListableBeanFactory != null);
		
		
		if(beansOfType.size() == 1){
			FileDAO<?> fileDAO = (FileDAO<?>) beansOfType.values().iterator().next();
			Class<?>[] allClassesOfTypeFile = Neo.getApplicationContext().getClassManager().getAllClassesOfType(File.class);
			if(allClassesOfTypeFile.length == 1){
				File file;
				try {
					file = (File)allClassesOfTypeFile[0].newInstance();
					file.setCdfile(cdfile);
					try {
						file = fileDAO.loadWithContents(file);
					} catch (Exception e) {
						request.setAttribute("url", TagFunctions.getPartialURL());
						throw new ArquivoNaoEncontradoException("Arquivo n�o encontrado.");
					}
					Resource resource = new Resource(file.getContenttype(), file.getName(), file.getContent());
					Long size = file.getSize();
					if (size != null) {
						resource.setSize(size.intValue());
					}
					return resource;
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				}
			}
		}
		throw new RuntimeException("Estenda a classe DownloadFileServlet e sobrescreva o m�todo getResource");
	}
	
	protected Long extractCdfile(HttpServletRequest request) {
		
		try{
			String requestURI = request.getRequestURI();
			Pattern pattern = Pattern.compile(".+?/([0-9]+)_[0-9]+");
			Matcher matcher = pattern.matcher(requestURI);
			if (matcher.find()) {
				return new Long(matcher.group(1));
			}
			else {
				return null;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void addUrl(String url) {

		urls.add(url);
	}

	
}
