package br.com.linkcom.sined.util.neo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.ArquivoNaoEncontradoException;
import br.com.linkcom.sined.util.SinedUtil;


public class ReportFileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public static final String REPORT_FILE_PATH = "/REPORT";
	public static final String SEPARATOR = System.getProperty("file.separator");
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key,contentType;
		Resource resource = null;
		
		try {
			key = extractKey(request);
			if(StringUtils.isNotEmpty(request.getParameter("nomearquivodownloadw3erp"))){
				key = request.getParameter("nomearquivodownloadw3erp") + key;
			}
		} catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		try {
			contentType = extractContentType(request);
		} catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
				
		try{
			resource = getResource(key, contentType);
	        if(resource == null){
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        	return;
	        }
		} catch (ArquivoNaoEncontradoException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}

        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		if (resource.getSize()>=0) {
			response.setContentLength(resource.getSize());
		}

		response.getOutputStream().write(resource.getContents());
		response.flushBuffer();
	}

	@Override
	protected long getLastModified(HttpServletRequest request) {
		return -1;
	}
	
	protected String extractKey(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		Pattern pattern = Pattern.compile(".+?REPORTFILE/([0-9A-Za-z-_]+)");
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			throw new Exception("URL inválida");
		}
	}
	protected String extractContentType(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		Pattern pattern = Pattern.compile(".+?REPORTFILE/([0-9A-Za-z-_]+)\\.(.+)");
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			return matcher.group(2);
		} else {
			throw new Exception("URL inválida");
		}
	}
	
	public static void add(String key, Resource report) {
		try {
			String path = getDirReportFile(SinedUtil.getClientNameForDir().split(":")[0]);
			String contentType =  report.getContentType().split("/")[1];
			File fileNew = new File(path + SEPARATOR + key + "." + contentType + "_temp");
			FileOutputStream fileOS; 
			
			if(!fileNew.exists()){
				fileNew.getParentFile().mkdirs();
				fileNew.createNewFile();
			}
			fileOS = FileUtils.openOutputStream(fileNew);
			FileChannel outChannel = fileOS.getChannel();
			ByteBuffer buf = ByteBuffer.allocate(report.getContents().length);
			
			byte[] bytes = report.getContents();
			buf.put(bytes);
			buf.flip();
	
			outChannel.write(buf);
			
			fileOS.close();
			
			File fileOld = new File(path + SEPARATOR + key + "." + contentType);
			fileOld.delete();
			
			fileNew.renameTo(fileOld);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Método a ser invocado para limpar todos os arquivos de relatório do sistema.
	 */
	public static void removeReportFiles(){
		try {
			String[] exec = new String[] { "rm", "-rf", getDirReportFile("") };
			Process process;
			process = Runtime.getRuntime().exec(exec);
			process.waitFor();
			
			if (process.exitValue() == 0) {
//				System.out.println("Arquivos de relatórios removidos com sucesso.");
			}else{
				final char[] buffer = new char[0x10000];
				StringBuilder out = new StringBuilder();
				Reader in = new InputStreamReader(process.getErrorStream(), "UTF-8");
				int read;
				do {
					read = in.read(buffer, 0, buffer.length);
					if (read>0) {
						out.append(buffer, 0, read);
					}
				} while (read>=0);
//				System.out.println(out.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	public static Resource getResource(String key, String contentType) {
		InputStream inputStream = null;
		try{	
			String path = getDirReportFile(SinedUtil.getClientNameForDir().split(":")[0]);
			
			File file = new File(path + SEPARATOR + key + "." + contentType);
			inputStream = new FileInputStream(file);
			BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
			byte[] bytes = new byte[(int) file.length()];
			bufferedInputStream.read(bytes);
			
			Resource report = new Resource("application/"+contentType, key+"."+contentType, bytes);
			report.setSize(bytes.length);
			return report;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
				try {
					inputStream.close();
				}catch (Exception e) {}
		}
		
		return null;
	}
	
	public static String getDirReportFile(String nomeCliente){
		return ArquivoDAO.getInstance().getPathDir() + SEPARATOR + Neo.getApplicationName() + SEPARATOR + "relatorio" + SEPARATOR + nomeCliente ;
	}
}
