package br.com.linkcom.sined.util.neo.persistence;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ListagemResultSined<E> extends ListagemResult<E>{

	public ListagemResultSined(QueryBuilder<E> queryBuilder, FiltroListagem filtroListagem) {
		super(queryBuilder, filtroListagem);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void init(QueryBuilder<E> queryBuilder, FiltroListagem filtroListagem, Boolean forceCountAllFields) {	
		if (filtroListagem instanceof FiltroListagemSined && (Boolean.TRUE.equals(((FiltroListagemSined) filtroListagem).getPaginacaoSimples()))){
			initPaginacaoSimples(queryBuilder, (FiltroListagemSined) filtroListagem);
		} else {
			QueryBuilderSined<Long> countQueryBuilder = new QueryBuilderSined<Long>(queryBuilder.getHibernateTemplate());
			       
	        if(forceCountAllFields != null && forceCountAllFields){
	        	countQueryBuilder.select("count(*)");
	        } else {
	        	String idPropertyName = queryBuilder.getHibernateTemplate().getSessionFactory().getClassMetadata(queryBuilder.getFrom().getFromClass()).getIdentifierPropertyName();
	        	countQueryBuilder.select("count(distinct "+queryBuilder.getAlias()+"."+idPropertyName+")");
	        }
	        
			
	        QueryBuilder.From from = queryBuilder.getFrom();
	        countQueryBuilder.from(from);
	        
	        List<QueryBuilder<E>.Join> joins = queryBuilder.getJoins();
	        for (QueryBuilder.Join join : joins) {
	        	//quando estiver contando nao precisa de fazer fetch do join
				countQueryBuilder.join(join.getJoinMode(), false, join.getPath());
			}
	        QueryBuilder.Where where = queryBuilder.getWhere();
			countQueryBuilder.where(where);
			countQueryBuilder.setUseTranslator(false);
			Long numeroResultados = countQueryBuilder.unique();
	                
			filtroListagem.setNumberOfResults(numeroResultados.intValue());
			init(queryBuilder, filtroListagem, numeroResultados);
		}
	}
	
	/**
	 * 
	 * @param queryBuilder
	 * @param filtroListagemSined
	 * @auhtor Thiago Clemente
	 * 
	 */
	private void initPaginacaoSimples(QueryBuilder<E> queryBuilder, FiltroListagemSined filtroListagemSined){
		
		//se estiver filtrando novamente voltar para a primeira p�gina
		if (filtroListagemSined.getEVENT().equals(FiltroListagem.FILTER)){
			filtroListagemSined.setCurrentPage(0);
		}
		
		int currentPage = filtroListagemSined.getCurrentPage();
		int pageSize = filtroListagemSined.getPageSize();		
		int maxResults = pageSize > 100 ? (pageSize+1) : (pageSize * 5 + 1);
		queryBuilder.setMaxResults(maxResults);
		queryBuilder.setFirstResult(currentPage * pageSize);
		
		if (!StringUtils.isEmpty(filtroListagemSined.getOrderBy())){
			queryBuilder.orderBy(filtroListagemSined.getOrderBy() + " " + (filtroListagemSined.isAsc() ? "ASC" : "DESC"));
		}
		
		lista = queryBuilder.list();
		
		int numeroResultados = lista.size();
		int numberOfPages = numeroResultados / pageSize + currentPage;
		
		if (numeroResultados % pageSize != 0){
			numberOfPages++;
		}
		
		filtroListagemSined.setNumberOfResults(numeroResultados);
		filtroListagemSined.setNumberOfPages(numberOfPages);
		
		int fromIndex = 0;
		int toIndex = pageSize > numeroResultados ? numeroResultados : pageSize;
		
		lista = lista.subList(fromIndex, toIndex);
	}

}
