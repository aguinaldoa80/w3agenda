package br.com.linkcom.sined.util.neo.persistence;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;

public class TipoAjusteType implements UserType {

	private static final int[] SQL_TYPES = { Types.INTEGER };
	private final HashMap<Integer, TipoAjuste> mapConstantes;

	public TipoAjusteType(){
		TipoAjuste[] enumConstants = TipoAjuste.class.getEnumConstants();
		mapConstantes = new HashMap<Integer, TipoAjuste>();
		
		for (TipoAjuste tp : enumConstants)
			mapConstantes.put(tp.getId(), tp);
	}
	
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner)
			throws HibernateException, SQLException {

		Integer valor = resultSet.getInt(names[0]);
		Object result = null;
		if (!resultSet.wasNull()) {
			result = mapConstantes.get(valor);
		}
		return result;
	}

	public void nullSafeSet(PreparedStatement preparedStatement, Object value,
			int index) throws HibernateException, SQLException {

		if (null == value) {
			preparedStatement.setNull(index, Types.VARCHAR);
		} else {
			preparedStatement.setInt(index, ((TipoAjuste) value).getId());
		}
	}

	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	public boolean isMutable() {
		return false;
	}

	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return cached;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return original;
	}

	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y)
			return true;
		if (null == x || null == y)
			return false;
		return x.equals(y);
	}

	public Class<?> returnedClass() {
		return TipoAjuste.class;
	}

}
