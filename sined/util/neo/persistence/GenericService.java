package br.com.linkcom.sined.util.neo.persistence;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.persistence.QueryBuilder.Where;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.BeansUtil;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.CompararCampo;
import br.com.linkcom.sined.util.bean.GenericBean;

public class GenericService<BEAN> extends br.com.linkcom.neo.service.GenericService<BEAN> {
	
	/**
	 * Executa um saveOrUpdate sem criar transa��o com o banco.
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	public void saveOrUpdateNoUseTransaction(BEAN bean) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
					"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}

		((GenericDAO<BEAN>)genericDAO).saveOrUpdateNoUseTransaction(bean);
	}
	
	public void saveOrUpdateNoUseTransactionWithoutLog(BEAN bean) {
		((GenericDAO<BEAN>)genericDAO).saveOrUpdateNoUseTransactionWithoutLog(bean);
	}
	
	public void saveOrUpdateWithoutLog(BEAN bean) {
		((GenericDAO<BEAN>)genericDAO).saveOrUpdateWithoutLog(bean);
	}
	
	/**
	 * M�todo para carregar determinados campos de um BEAN informados por par�metro.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public BEAN load(BEAN bean, String campos) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}

		return ((GenericDAO<BEAN>)genericDAO).load(bean, campos);
	}
	
	public BEAN loadWithoutWhereEmpresa(BEAN bean, String campos) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}

		return ((GenericDAO<BEAN>)genericDAO).loadWithoutWhereEmpresa(bean, campos);
	}
	
	public BEAN loadReader(BEAN bean, String campos) {
		SinedUtil.markAsReader();
		return this.load(bean, campos);
	}
	
	public BEAN loadReaderWithoutWhereEmpresa(BEAN bean, String campos) {
		SinedUtil.markAsReader();
		return this.loadWithoutWhereEmpresa(bean, campos);
	}
	
	public BEAN loadReaderWithoutWhereEmpresa(BEAN bean) {
		SinedUtil.markAsReader();
		return this.loadWithoutWhereEmpresa(bean);
	}
	
	public BEAN loadReader(BEAN bean) {
		SinedUtil.markAsReader();
		return this.load(bean);
	}
	
	/**
	 * M�todo para carregar colecao de um BEAN.
	 * 
	 * @param bean
	 * @param colecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public BEAN loadFetch(BEAN bean, String colecao) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}

		return ((GenericDAO<BEAN>)genericDAO).loadFetch(bean, colecao);
	}
	
	public BEAN loadWithoutWhereEmpresa(BEAN bean) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}
		
		return ((GenericDAO<BEAN>)genericDAO).loadWithoutWhereEmpresa(bean);
	}
	
	public BEAN loadForEntradaWithoutWhereEmpresa(BEAN bean) {
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}
		
		return ((GenericDAO<BEAN>)genericDAO).loadForEntradaWithoutWhereEmpresa(bean);
	}
	
	/**
	 * Carrega somente registros ativos.
	 * Se for informado algum VarArg, ser� carregado o registro referente a ele tamb�m,
	 * mesmo sendo inativo.
	 * 
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericDAO#findAtivos(String[], Object...)
	 * 
	 * @param extraFields
	 * 			caso seja necess�rio especificar algum campo para carregar al�m do c�digo e do
	 * 			DescriptionProperty, especificar neste par�metro.
	 * 
	 * @param bean
	 * 			especificar algum registro que deve ser carregado, mesmo se estiver inativo.
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<BEAN> findAtivos(BEAN[] bean, String[] extraFields){
		if (!(genericDAO instanceof GenericDAO<?>)) {
			throw new SinedException("Seu DAO n�o estende o GenericDAO da aplica��o. " +
				"Por favor, estenda o DAO de br.com.linkcom.sined.util.neo.persistence");
		}

		return ((GenericDAO<BEAN>)genericDAO).findAtivos(bean, extraFields);
	}
	
	/**
	 * Carrega somente registros ativos.
	 * 
	 * @see #findAtivos(String[], Object...)
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<BEAN> findAtivos(){
		return this.findAtivos(null, null);
	}
	
	/**
	 * Carrega somente registros ativos, mais o bean que estiver informado no
	 * par�metro da fun��o, mesmo se ele estiver inativo.
	 * 
	 * @see #findAtivos(Object)
	 * @param bean
	 * 			especificar algum registro que deve ser carregado, mesmo se estiver inativo.
	 * @return
	 * @author Hugo Ferreira
	 */
	@SuppressWarnings("unchecked")
	public List<BEAN> findAtivos(BEAN bean){
		if(bean != null){
			Object[] arr = new Object[]{bean};
			return this.findAtivos((BEAN[])arr, null);
		} else {
			return this.findAtivos();
		}
	}

	/**
	 * Carrega somente registros ativos.
	 * Pode ser informado uma lista de BEANS para serem carregados mesmo sendo inativos.
	 * 
	 * @see #findAtivos(String[], Object...)
	 
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	@SuppressWarnings("unchecked")
	public List<BEAN> findAtivos(List<BEAN> lista){
		BEAN[] array = lista != null ? (BEAN[]) lista.toArray(new Object[lista.size()]) : null;
		
		return this.findAtivos(array, null);
	}
	
	/**
	 * Carrega somente registros ativos.
	 * Pode ser informado uma lista de BEANS para serem carregados mesmo sendo inativos.
	 *
	 * @param array
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/11/2015
	 */
	public List<BEAN> findAtivos(BEAN... array){
		return this.findAtivos(array, null);
	}
	
	public String getCampoExtraAlterado(Pedidovendatipo beanAntigo, Pedidovendatipo beanCorrente) {
		
		if(beanAntigo != null && beanCorrente != null){
			StringBuilder camposExtra = new StringBuilder();
			
			for(Campoextrapedidovendatipo campoExtraAntigo: beanAntigo.getListaCampoextrapedidovenda()){
				Boolean isAlteracao = true;
				String colunaAntigo = campoExtraAntigo.getNome() + campoExtraAntigo.getOrdem() + campoExtraAntigo.getObrigatorio();
		
				for(Campoextrapedidovendatipo campoExtraCorrente: beanCorrente.getListaCampoextrapedidovenda()) {
					String colunaCorrente = campoExtraCorrente.getNome() + campoExtraCorrente.getOrdem() + campoExtraCorrente.getObrigatorio();
					
					if(colunaCorrente.equals(colunaAntigo)) {
						isAlteracao = false;
						break;
					}
				}	
				if(isAlteracao){
					String campoExtraObrigatorio = Boolean.TRUE.equals(campoExtraAntigo.getObrigatorio()) ? "Sim" : "N�o";
					camposExtra.append("Campo: "+campoExtraAntigo.getNome()+"  -   Ordem: "+campoExtraAntigo.getOrdem()+"  -   Obrigatorio: "+campoExtraObrigatorio+"<br>");
				}
			}
			if(camposExtra.length() > 0) return "<br>Campo Extra: <br>" + camposExtra.toString();	
		}
		return "";
	}
	
	
	public String getCamposAlterados(Object beanAntigo, Object beanCorrente) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		StringBuilder camposAlterados = new StringBuilder();
		String retorno = "Campos alterados: ";
		if(beanAntigo != null && beanCorrente != null){
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			Method[] methods = reflectionCache.getMethods(beanAntigo.getClass());
			
			for (Method method : methods) {
				if(reflectionCache.isAnnotationPresent(method, CompararCampo.class)){
					Object objAntigo = method.invoke(beanAntigo);
					Object objCorrente = method.invoke(beanCorrente);
					
					if((!(objCorrente instanceof Money) && (objAntigo != null && objCorrente != null) && (!objCorrente.equals(objAntigo)))
							|| ((objCorrente instanceof Money && objAntigo instanceof Money) && ((Money)objAntigo).compareTo((Money)objCorrente) != 0)){
						String valorAntigo = new BeansUtil().getObjectValueToString(objAntigo);
						if(valorAntigo.equals("true")){
							valorAntigo = "Sim";
						} 
						if (valorAntigo.equals("false")){
							valorAntigo = "N�o";
						}
						String valorCorrente = new BeansUtil().getObjectValueToString(objCorrente);
						if(valorCorrente.equals("true")){
							valorCorrente = "Sim";
						} 
						if (valorCorrente.equals("false")){
							valorCorrente = "N�o";
						}

						camposAlterados.append(method.getAnnotation(DisplayName.class).value() + " de: " + valorAntigo + " para: " + valorCorrente).append("<br>");
					}
				}
			}	
		}
		if(camposAlterados.length() > 1){
			return retorno.concat(camposAlterados.toString());
		} else {
			return "";
		}
		
	}
	
	
	public List<GenericBean> findAllForComboWS(){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findForCombo());
	}
	
	public List<GenericBean> findAllForComboWS(Where where){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findForCombo());
	}
}
