package br.com.linkcom.sined.util.neo.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


public class QueryBuilderSined<E> extends QueryBuilder<E> {

	
	protected boolean useWhereEmpresa = true;
	protected boolean useWhereProjeto = true;
	protected boolean useWhereClienteEmpresa = false;
	protected boolean useWhereFornecedorEmpresa = false;
	protected boolean useReadOnly = false;
	
	public boolean isUseWhereProjeto() {
		return useWhereProjeto;
	}
	
	public boolean isUseWhereEmpresa() {
		return useWhereEmpresa;
	}
	
	public boolean isUseWhereClienteEmpresa() {
		return useWhereClienteEmpresa;
	}

	public boolean isUseWhereFornecedorEmpresa() {
		return useWhereFornecedorEmpresa;
	}
	
	public boolean isUseReadOnly() {
		return useReadOnly;
	}
	
	public QueryBuilderSined<E> setUseReadOnly(boolean useReadOnly) {
		this.useReadOnly = useReadOnly;
		return this;
	}

	public QueryBuilderSined<E> setUseWhereProjeto(boolean useWhereProjeto) {
		this.useWhereProjeto = useWhereProjeto;
		return this;
	}
	
	public QueryBuilderSined<E> setUseWhereEmpresa(boolean useWhereEmpresa) {
		this.useWhereEmpresa = useWhereEmpresa;
		return this;
	} 
	
	public QueryBuilderSined<E> setUseWhereClienteEmpresa(boolean useWhereClienteEmpresa) {
		this.useWhereClienteEmpresa = useWhereClienteEmpresa;
		return this;
	} 
	
	public QueryBuilderSined<E> setUseWhereFornecedorEmpresa(boolean useWhereFornecedorEmpresa) {
		this.useWhereFornecedorEmpresa = useWhereFornecedorEmpresa;
		return this;
	}
	
	public QueryBuilderSined<E> removeUseWhere() {
		this.useWhereEmpresa = false;
		this.useWhereProjeto = false;
		this.useWhereClienteEmpresa = false;
		this.useWhereFornecedorEmpresa = false;
		return this;
	}
	
	public QueryBuilderSined(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	
	@Override
	public String getQuery() {
		Class<?> fromClass = getFrom().getFromClass();
		doJoinEmpresa(fromClass);
		doWhereProjeto(fromClass);
		doWhereClienteEmpresa(fromClass);
		doWhereFornecedorEmpresa(fromClass);
		
		if(getFrom() == null){
			throw new RuntimeException("N�o foi informada a cl�usula from da Query");
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(getSelect());
		stringBuilder.append(" ");
		stringBuilder.append(getFrom());
		stringBuilder.append(" ");
		for (Join join : getJoins()) {
			stringBuilder.append(join);
			stringBuilder.append(" ");
		}
		
		stringBuilder.append(getWhere());
		
		if(getGroupBy() != null){
			stringBuilder.append(" ");
			stringBuilder.append(getGroupBy());
		}
		
		if(getOrderby()!=null){
			stringBuilder.append(" ORDER BY ");
			stringBuilder.append(getOrderby());
		}
		String hibernateQuery = stringBuilder.toString();
        return hibernateQuery;
	}

	
	private void doWhereProjeto(Class<?> fromClass) {
		try {
			if(useWhereProjeto){
				Object newInstance = fromClass.newInstance();
				if(newInstance instanceof PermissaoProjeto){
					if(Neo.isInRequestContext() && NeoWeb.getUser() != null && NeoWeb.getUser() instanceof Usuario){
						Usuario usuario = (Usuario)NeoWeb.getUser();
						
						if(!usuario.getTodosprojetos()){				
							PermissaoProjeto permissaoProjeto = (PermissaoProjeto) newInstance;
							BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, fromClass);
							String campo = org.apache.commons.lang.StringUtils.uncapitalize(fromClass.getSimpleName()) + "." + beanDescriptor.getIdPropertyName();
							String whereInOrFunction = permissaoProjeto.subQueryProjeto();
							if(permissaoProjeto.useFunctionProjeto()){
								where(whereInOrFunction + "(" + campo + ", '" + SinedUtil.getListaProjeto() + "') = true");
							} else {
								where(campo + " in (" + whereInOrFunction + ")");
							}
						}
					}
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void doWhereClienteEmpresa(Class<?> fromClass) {
		try {
			Object newInstance = fromClass.newInstance();
			if(!(newInstance instanceof Parametrogeral)){
				if(useWhereClienteEmpresa && SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
					if(newInstance instanceof PermissaoClienteEmpresa){
						String listaEmpresa = new SinedUtil().getListaEmpresa();
							
						if(listaEmpresa != null){				
							PermissaoClienteEmpresa permissaoClienteEmpresa = (PermissaoClienteEmpresa) newInstance;
							BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, fromClass);
							String campo = org.apache.commons.lang.StringUtils.uncapitalize(fromClass.getSimpleName()) + "." + beanDescriptor.getIdPropertyName();
							where(campo + " in (" + permissaoClienteEmpresa.subQueryClienteEmpresa() + ")");
						}
					}
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void doWhereFornecedorEmpresa(Class<?> fromClass) {
		try {
			Object newInstance = fromClass.newInstance();
			if(useWhereFornecedorEmpresa && SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
				if(newInstance instanceof PermissaoFornecedorEmpresa){
					String listaEmpresa = new SinedUtil().getListaEmpresa();
						
					if(listaEmpresa != null){				
						PermissaoFornecedorEmpresa permissaoFornecedorEmpresa = (PermissaoFornecedorEmpresa) newInstance;
						BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, fromClass);
						String campo = org.apache.commons.lang.StringUtils.uncapitalize(fromClass.getSimpleName()) + "." + beanDescriptor.getIdPropertyName();
						where(campo + " in (" + permissaoFornecedorEmpresa.subQueryFornecedorEmpresa() + ")");
					}
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Realiza as altera��es necess�rias na Query, caso a annotation @JoinEmpresa esteja na Entity do from
	 * @author Rodrigo Freitas
	 * @author Igor Silv�rio 
	 * @param fromClass
	 */
	private void doJoinEmpresa(Class<?> fromClass) {
		StringBuilder[] whereInAlias = {new StringBuilder(), new StringBuilder()};
		if (useWhereEmpresa) {
			JoinEmpresa joinEmpresa = ReflectionCacheFactory.getReflectionCache().getAnnotation(fromClass, JoinEmpresa.class);
			boolean joinEmpresaFrom = true; 
			if(joinEmpresa != null){
				try {
					Object newInstance = fromClass.newInstance();
					if(newInstance instanceof Cliente){
						if(!useWhereClienteEmpresa || !SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
							joinEmpresaFrom = false;
						}
					}else if(newInstance instanceof Fornecedor){
						if(!useWhereFornecedorEmpresa || !SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
							joinEmpresaFrom = false;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (joinEmpresa != null && joinEmpresaFrom) {
				String listaEmpresa = new SinedUtil().getListaEmpresa();
				
				if (listaEmpresa != null && !listaEmpresa.equals("")) {
					String valueCoalesce = joinEmpresa.value();
					
					String[] joinsCoalesceArray = valueCoalesce.split(",");
					
					int indexWhereIn = 0;
					for(String value : joinsCoalesceArray){
						String valueJoinEmpresa = value;
						
						String[] joinsArray = value.split("[.]");
						
						if (joinsArray.length > 2) {
							List<String> listaJoins = new ArrayList<String>();
							
							String alias = null;
							 
							/**
							 * Definindo quais os joins que ter�o de ser feitos.
							 * Considerando uma annotation @JoinEmpresa("a.b.c.d"), ele dever� fazer os joins "a.b b", "b.c c"
							 */
							for (int i = 0; i < joinsArray.length-2; i++) {
								alias = joinsArray[i+1];
								listaJoins.add(joinsArray[i]+"."+joinsArray[i+1]+" "+alias);
							}
							
							/**
							 * definindo como deve ser o whereIn de join empresa.
							 * Considerando uma annotation @JoinEmpresa("a.b.c.d"), o whereIn ser� "c.d'
							 */
							for (int i = joinsArray.length-2; i < joinsArray.length; i++) {
								whereInAlias[indexWhereIn].append(joinsArray[i]);
								if(i<joinsArray.length-1)
									whereInAlias[indexWhereIn].append(".");
							}
								
							/**
							 * Removendo os joins que j� existem no QueryBuilder
							 */
							for (Join join: getJoins()) {
								String string = join.getPath();
								for (int i = 0; i < listaJoins.size(); i++) {
									if (string.equals(listaJoins.get(i))) {
										listaJoins.set(i, null);
									}
								}
							}
							
							for (String jj : listaJoins) {
								if (jj != null) {
									leftOuterJoin(jj, true);
								}
							}
						}
						
						//Se o whereIn n�o foi preenchido (@Joinempresa s� tem dois niveis), usar o valor de @JoinEmpresa no WhereIn
						if(whereInAlias[indexWhereIn].length()==0){
//							whereInAlias.append(joinEmpresa.value());
							whereInAlias[indexWhereIn].append(valueJoinEmpresa);
						}
						
						indexWhereIn++;
					}
					
					if(whereInAlias[1].length() > 0){
						openParentheses()
							.whereIn(" COALESCE(" + whereInAlias[0] + ".cdpessoa, " + whereInAlias[1] + ".cdpessoa) ", listaEmpresa)
							.or()
							.where(whereInAlias[1] + " is null")
						.closeParentheses();
					}else {
						openParentheses()
							.whereIn(whereInAlias[0] + ".cdpessoa", listaEmpresa)
							.or()
							.where(whereInAlias[0] + " is null")
						.closeParentheses();
					}
				}
			}
		}
	}
	
    /**
     * Cria uma cl�usulua where ... like ... ignorando caixa e acentos.
     * S� � necess�rio informar a expressao que deve ser usado o like, n�o utilizar '?'
     * Ex.:
     * whereLikeIgnoreAll("associado.nome", associado.getNome())
     * Isso ser� transformado em: UPPER(TIRAACENTO(associado.nome)) LIKE '%'||UPPER(TIRAACENTO(nome))||'%'
     * Se o parametro for null ou string vazia essa condi��o n�o ser� criada
     * @param whereClause
     * @param parameter
     * @return
     */
	//Alterado para verificar o caracter ? somente na clausula e n�o nos par�metros
	@Override
    public QueryBuilder<E> whereLikeIgnoreAll(String whereClause, String parameter) {
		if (parameter != null && !parameter.equals("")) {
			if(parameter.indexOf("'") != -1 && parameter.indexOf("''") == -1){
				parameter = parameter.replace("'", "''");
			}
			if (whereClause.indexOf('?') > 0) {
				throw new IllegalArgumentException("A cl�usula where do QueryBuilder n�o pode ter o caracter '?'. Deve ser passada apenas a express�o que se deseja fazer o like. Veja javadoc!");
			}
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			if (funcaoTiraacento != null) {
				where("UPPER(" + funcaoTiraacento + "(" + whereClause + ")) LIKE '%'||?||'%'", Util.strings.tiraAcento(parameter).toUpperCase());
			}
			else {
				where("UPPER(" + whereClause + ") LIKE '%'||?||'%'", Util.strings.tiraAcento(parameter).toUpperCase());
			}
		}
		return this;
	}

	@Override
	public List<E> list() {
		if(useReadOnly) SinedUtil.markAsReader();
		List<E> list = null;
		try {
			list = super.list();
			SinedUtil.desmarkAsReader();
		} catch (Exception e) {
			SinedUtil.desmarkAsReader();
			list = super.list();
		}
		return list;
	}

	@Override
	public Iterator<E> iterate() {
		if(useReadOnly) SinedUtil.markAsReader();
		Iterator<E> iterate = null;
		try {
			iterate = super.iterate();
			SinedUtil.desmarkAsReader();
		} catch (Exception e) {
			SinedUtil.desmarkAsReader();
			iterate = super.iterate();
		}
		return iterate;
	}

	@Override
	public E unique() {
		if(useReadOnly) SinedUtil.markAsReader();
		E unique = null;
		try {
			unique = super.unique();
			SinedUtil.desmarkAsReader();
		} catch (Exception e) {
			SinedUtil.desmarkAsReader();
			unique = super.unique();
		}
		return unique;
	}
	
	
	public QueryBuilderSined<E> from(Class clazz) {
		return (QueryBuilderSined<E>)super.from(clazz);
	}
}
