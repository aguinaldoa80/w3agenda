/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util.neo.persistence;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.AliasMap;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.QueryBuilderResultTranslatorImpl;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

public class GenericDAO<BEAN> extends br.com.linkcom.neo.persistence.GenericDAO<BEAN>{
	
	@Override
	public <E> QueryBuilder<E> newQueryBuilder(Class<E> clazz) {
		return new QueryBuilderSined<E>(getHibernateTemplate()).setUseReadOnly(true);
	}
	
	public <E> QueryBuilderSined<E> newQueryBuilderSined(Class<E> clazz) {
		return new QueryBuilderSined<E>(getHibernateTemplate()).setUseReadOnly(true);
	}
	
	protected QueryBuilderSined<BEAN> querySined() {
		return (QueryBuilderSined<BEAN>) new QueryBuilderSined<BEAN>(getHibernateTemplate()).setUseReadOnly(true).from(beanClass);
	}
	
	@Override
	protected QueryBuilder<BEAN> query() {
		return ((QueryBuilderSined<BEAN>) super.query()).setUseReadOnly(true);
	}
	
	@Override
	public <E> QueryBuilder<E> newQueryBuilderWithFrom(Class<E> clazz) {
		return new QueryBuilderSined<E>(getHibernateTemplate()).setUseReadOnly(true).from(beanClass);
	}
	
	public <E> QueryBuilderSined<E> newQueryBuilderSinedWithFrom(Class<E> clazz) {
		return new QueryBuilderSined<E>(getHibernateTemplate()).setUseReadOnly(true).from(beanClass);
	}
	
	@Override
	public void saveOrUpdate(BEAN bean) {
		if (bean instanceof Log){
			((Log) bean).setDtaltera(new Timestamp(System.currentTimeMillis()));
			if(Neo.isInRequestContext() && Neo.getUser() != null && Neo.getUser() instanceof Usuario){
				((Log) bean).setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			}
		}
		super.saveOrUpdate(bean);
	}
	
	public void saveOrUpdateWithoutLog(BEAN bean) {
		super.saveOrUpdate(bean);
	}

	/**
	 * M�todo para carregar determinados campos de um BEAN informados por par�metro.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public BEAN load(BEAN bean, String campos) {
		if(bean == null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		QueryBuilder<BEAN> query = querySined().setUseReadOnly(true);
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
				.entity(bean)
				.unique();
	}
	
	public BEAN loadWithoutWhereEmpresa(BEAN bean, String campos) {
		if(bean == null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		QueryBuilder<BEAN> query = querySined().setUseReadOnly(true).removeUseWhere();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
				.entity(bean)
				.unique();
	}
	
	/**
	 * M�todo para carregar colecao de um BEAN.
	 * 
	 * @param bean
	 * @param colecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public BEAN loadFetch(BEAN bean, String colecao) {
		if(bean == null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		QueryBuilder<BEAN> query = querySined().setUseReadOnly(true);
		
		if(colecao!=null && !colecao.equals("")){
			query.fetchCollection(colecao);
		}
		
		return query
				.entity(bean)
				.unique();
	}
	
	public BEAN loadWithoutWhereEmpresa(BEAN bean){
		if(bean == null){
			return null;
		}
		return querySined()
			.setUseReadOnly(true)
			.setUseWhereEmpresa(false)
			.setUseWhereClienteEmpresa(false)
			.entity(bean)
			.unique();
	}
	
	public BEAN loadForEntradaWithoutWhereEmpresa(BEAN bean){
		if(bean == null || Util.beans.getId(bean) == null){
			return null;
		}
		QueryBuilder<BEAN> query = querySined().setUseReadOnly(true).setUseWhereClienteEmpresa(false).setUseWhereEmpresa(false).entity(bean);
		updateEntradaQuery(query);
		return query.unique();
	}
	
	/**
	 * Executa um saveOrUpdate sem criar transa��o com o banco.
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	public void saveOrUpdateNoUseTransaction(BEAN bean) {
		if (bean instanceof Log){
			((Log) bean).setDtaltera(new Timestamp(System.currentTimeMillis()));
			if(Neo.isInRequestContext() && Neo.getUser() != null && Neo.getUser() instanceof Usuario){
				((Log) bean).setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			}
		}
		
		SaveOrUpdateStrategy save = save(bean);
		save.useTransaction(false);
		updateSaveOrUpdate(save);
		save.execute();
		getHibernateTemplate().flush();
	}
	
	public void saveOrUpdateNoUseTransactionWithoutLog(BEAN bean) {
		SaveOrUpdateStrategy save = save(bean);
		save.useTransaction(false);
		updateSaveOrUpdate(save);
		save.execute();
		getHibernateTemplate().flush();
	}
	
	
	@Override
	public ListagemResult<BEAN> findForListagem(FiltroListagem filtro) {
		QueryBuilderSined<BEAN> query = (QueryBuilderSined<BEAN>)query();
		query.orderBy(Util.strings.uncaptalize(beanClass.getSimpleName())+".id");
		query.setUseWhereClienteEmpresa(true);
		query.setUseWhereFornecedorEmpresa(true);
		query.setUseReadOnly(true);
		updateListagemQuery(query, filtro);
		QueryBuilderSined<BEAN> queryBuilder = query;
		return new ListagemResultSined<BEAN>(queryBuilder, filtro);
	}
	
	
	
	@Override
	public BEAN load(BEAN bean) {
		SinedUtil.markAsReader();
		return super.load(bean);
	}

	@Override
	public BEAN loadForEntrada(BEAN bean) {
		if(!SinedUtil.isFromInsertOne()) SinedUtil.markAsReader();
		return super.loadForEntrada(bean);
	}

	@Override
	public List<BEAN> findAll() {
		SinedUtil.markAsReader();
		return super.findAll();
	}
	
	@Override
	public List<BEAN> findAll(String orderBy) {
		SinedUtil.markAsReader();
		return super.findAll(orderBy);
	}
	
	@Override
	public List<BEAN> findBy(Object o, boolean forCombo, String... extraFields) {
		SinedUtil.markAsReader();
		return super.findBy(o, forCombo, extraFields);
	}
	
	@Override
	public ListagemResult<BEAN> findForExportacao(FiltroListagem filtro) {
		SinedUtil.markAsReader();
		return super.findForExportacao(filtro);
	}
	
	@Override
	public List<BEAN> findForCombo(String... extraFields) {
		SinedUtil.markAsReader();
		return super.findForCombo(extraFields);
	}
	
	@Override
	public void initQueryFindForCombo() {
//		if (translatorQueryFindForCombo == null) {
			//BeanDescriptor<BEAN> beanDescriptor = Neo.getApplicationContext().getBeanDescriptor(null, beanClass);
			//String descriptionPropertyName = beanDescriptor.getDescriptionPropertyName();
			//String idPropertyName = beanDescriptor.getIdPropertyName();

			String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
			String[] selectedProperties = getComboSelectedProperties(alias);
			String hbQueryFindForCombo = "select " + getComboSelect() + " " + "from " + beanClass.getName() + " " + alias;
			String whereProjeto = " where ";
			String whereClienteEmpresa = " where ";
			JoinEmpresa joinEmpresa = ReflectionCacheFactory.getReflectionCache().getAnnotation(beanClass, JoinEmpresa.class);
			if (joinEmpresa != null) {
				String listaEmpresa = new SinedUtil().getListaEmpresa();
				if (listaEmpresa != null && !listaEmpresa.equals("")) {
					
					String value = joinEmpresa.value();
					String[] joinsArray = value.split("[.]");
					
					if (joinsArray.length > 1) {
						for (int i = 0; i < joinsArray.length-1; i++) {
							hbQueryFindForCombo += " left outer join " + joinsArray[i]+"."+joinsArray[i+1]+" as "+joinsArray[i+1] + " ";
						}
					}

					hbQueryFindForCombo += " where ("+joinsArray[joinsArray.length-1]+".cdpessoa in ("+listaEmpresa+") or empresa is null )";
					whereProjeto = " and ";
					whereClienteEmpresa = " and ";
				}
			}
			
			try {
				if(NeoWeb.getUser() != null){
					Usuario usuario = (Usuario)NeoWeb.getUser();
					if(!usuario.getTodosprojetos()){
						Object newInstance = beanClass.newInstance();
						if(newInstance instanceof PermissaoProjeto){
							PermissaoProjeto permissaoProjeto = (PermissaoProjeto) newInstance;
							BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
							String campo = alias + "." + beanDescriptor.getIdPropertyName();
							String whereInOrFunction = permissaoProjeto.subQueryProjeto();
							if(permissaoProjeto.useFunctionProjeto()){
								hbQueryFindForCombo += whereProjeto + whereInOrFunction + "(" + campo + ", '" + SinedUtil.getListaProjeto() + "') = true";
							} else {
								hbQueryFindForCombo += whereProjeto + campo + " in (" + whereInOrFunction + ")";
							}
							whereClienteEmpresa = " and ";
						}
					}
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			try {
				Object newInstance = beanClass.newInstance();
				if(!(newInstance instanceof Parametrogeral)){
					if(NeoWeb.getUser() != null && SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
						String listaEmpresa = new SinedUtil().getListaEmpresa();
						if(listaEmpresa != null){
							if(newInstance instanceof PermissaoClienteEmpresa){
								PermissaoClienteEmpresa permissaoClienteEmpresa = (PermissaoClienteEmpresa) newInstance;
								BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
								String campo = alias + "." + beanDescriptor.getIdPropertyName();
								hbQueryFindForCombo += whereClienteEmpresa + campo + " in (" + permissaoClienteEmpresa.subQueryClienteEmpresa() + ")";
							}
						}
					}
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			hbQueryFindForCombo = getQueryFindForCombo(hbQueryFindForCombo);
			if (orderBy != null) {
				hbQueryFindForCombo += "  order by " + orderBy;
			}
			translatorQueryFindForCombo = new QueryBuilderResultTranslatorImpl();
			translatorQueryFindForCombo.init(selectedProperties, new AliasMap[] { new AliasMap(alias, null, beanClass) });
			NeoWeb.getRequestContext().setAttribute(NEO_COMBO_CACHE_QUERY,hbQueryFindForCombo);
//		}
	}
	
	/**
	 * Carrega somente registros ativos.
	 * Se for informado algum VarArg, ser� carregado o registro referente a ele tamb�m,
	 * mesmo sendo inativo.
	 * 
	 * @param bean
	 * @param extraFields
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<BEAN> findAtivos(BEAN[] bean, String[] extraFields){
		
		QueryBuilderSined<BEAN> query = querySined();
		query.setUseWhereClienteEmpresa(true);
		query.setUseWhereFornecedorEmpresa(true);
		query.setUseReadOnly(true);
		String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
		
		query
			.select(getComboSelect(extraFields))
			.openParentheses()
			.where(alias+".ativo = ?", Boolean.TRUE)
			.orderBy(orderBy);

		if (bean != null && bean.length > 0) {
			List<BEAN> list = new ArrayList<BEAN>();

			for (int i = 0; i < bean.length; i++) {
				if (bean[i] != null) list.add(bean[i]);
			}
			
			
			BeanDescriptor<BEAN> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
			String idPropertyName = beanDescriptor.getIdPropertyName();
			
			query.or();
			query.whereIn(alias + "." + idPropertyName, CollectionsUtil.listAndConcatenate(list, idPropertyName, ","));
		}
		query.closeParentheses();
		
		return query.list();
	}
	
	@Override
	public List<BEAN> findForAutocomplete(String q, String propertyMatch, String propertyLabel, Boolean matchOption) {
		String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
		BeanDescriptor<BEAN> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
		
		String[] labelPropertyName;
		String[] matchPropertyName;
		
		if(propertyMatch != null && !propertyMatch.equals("")){
			matchPropertyName = propertyMatch.split(",");
		} else {
			matchPropertyName = new String[]{alias + "." + beanDescriptor.getDescriptionPropertyName()};
		}
		
		if(propertyLabel != null && !propertyLabel.equals("")){
			labelPropertyName = propertyLabel.split(",");
		} else {
			labelPropertyName = new String[]{alias + "." + beanDescriptor.getDescriptionPropertyName()};
		}
		
		String idPropertyName = alias + "." + beanDescriptor.getIdPropertyName();
		
		QueryBuilderSined<BEAN> query = querySined();
		query.select(idPropertyName + "," + StringUtils.join(labelPropertyName, ","));
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		query.openParentheses();
		for (int i = 0; i < matchPropertyName.length; i++) {
			if(matchOption){
				query.whereLikeIgnoreAll(matchPropertyName[i], q);
				query.or();
			} else {
				if (funcaoTiraacento != null) {
					query.where("UPPER(" + funcaoTiraacento + "(" + matchPropertyName[i] + ")) LIKE ?||'%'", Util.strings.tiraAcento(q).toUpperCase());
				}
				else {
					query.where("UPPER(" + matchPropertyName[i] + ") LIKE ?||'%'", Util.strings.tiraAcento(q).toUpperCase());
				}
				query.or();
			}
		}		
		query.closeParentheses();
		
		query.setMaxResults(30);//Valor atual no arquivo neo.autocomplete.js
		query.setUseReadOnly(true);
		
		return query.orderBy(StringUtils.join(labelPropertyName, ",")).list();		
	}

}
