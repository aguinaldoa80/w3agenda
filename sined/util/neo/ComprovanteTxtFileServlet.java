package br.com.linkcom.sined.util.neo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.util.SinedUtil;


public class ComprovanteTxtFileServlet extends HttpServlet {

	private static final long serialVersionUID = -1993318160404049998L;
	
	public static final String COMPROVANTE_TXT_PATH = "/COMPROVANTETXT";
	public static final String COMPROVANTE_TXT_MAP = "COMPROVANTE_TXT_MAP";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key;
		try {
			key = extractKey(request);
		} catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		Object obj = request.getSession().getAttribute(COMPROVANTE_TXT_MAP);
		if(obj == null || !(obj instanceof Map<?, ?>)){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		String comprovante = get(request.getSession(), key);
		if(comprovante == null){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		remove(request.getSession(), key);
		
		Resource resource = new Resource("application/remote_printing", "comprovante_" + SinedUtil.datePatternForReport() + ".w3erp", comprovante.getBytes());

        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		response.setContentLength(comprovante.getBytes().length);
		response.getOutputStream().write(comprovante.getBytes());
		response.flushBuffer();
	}

	@Override
	protected long getLastModified(HttpServletRequest request) {
		return -1;
	}
	
	protected String extractKey(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		Pattern pattern = Pattern.compile(".+?COMPROVANTETXT/([0-9A-Za-z]+)");
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			throw new Exception("URL inv�lida");
		}
	}
	
	public static void add(HttpSession session, String key, String comprovante) {
		getMap(session).put(key, comprovante);
	}
	
	public static void remove(HttpSession session, String key) {
		getMap(session).remove(key);
	}

	public static String get(HttpSession session, String key) {
		return getMap(session).get(key);
	}

	@SuppressWarnings("unchecked")
	private static HashMap<String, String> getMap(HttpSession session) {
		HashMap<String, String> map = (HashMap<String, String>) session.getAttribute(COMPROVANTE_TXT_MAP);
		
		if (map == null) {
			map = new HashMap<String, String>();
			session.setAttribute(COMPROVANTE_TXT_MAP, map);
		}

		return map;
	}

}
