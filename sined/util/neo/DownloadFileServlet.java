/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.util.neo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.FileDAO;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.util.ArquivoNaoEncontradoException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.tag.TagFunctions;


public class DownloadFileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String DOWNLOAD_FILE_PATH = "/DOWNLOADFILE";
	
	public static final String DOWNLOAD_FILE_MAP = "NEO_DOWNLOAD_FILE_MAP";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Long cdfile;
		try {
			cdfile = extractCdfile(request);
		}
		catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		String param = request.getParameter("isNfeDesktop");
		String senha = request.getParameter("senhaNfeDesktop");
		boolean permitido = false;
		if(param != null && param.toUpperCase().equals("TRUE") && 
				senha != null && senha.toUpperCase().equals("LINKCOM")){
			permitido = true;
		}
		
		// Checa se h� permiss�o
		if (!permitido && DownloadFileServlet.getCdfile(request.getSession(), cdfile) == null) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		// Obt�m o conte�do
		Resource resource = null;
		
		try{
			resource = getResource(request, cdfile);
	        if(resource == null){
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        	return;
	        }
		} catch (ArquivoNaoEncontradoException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}

        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		//response.setHeader("Last-Modified", );
		if (resource.getSize()>=0) {
			response.setContentLength(resource.getSize());
		}

		response.getOutputStream().write(resource.getContents());
		response.flushBuffer();
	}

	@Override
	protected long getLastModified(HttpServletRequest request) {
		try {
			return getLastModified(request, extractCdfile(request));
		}
		catch (Exception e) {
			return 0;
		}
	}
	
	protected Long extractCdfile(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		Pattern pattern = Pattern.compile(".+?/([0-9]+)");
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			return new Long(matcher.group(1));
		}
		else {
			throw new Exception("URL inv�lida");
		}
	}

	public static void addCdfile(HttpSession session, Long cdfile) {
		getMap(session).put(cdfile, cdfile);
	}

	public static Long getCdfile(HttpSession session, Long cdfile) {
		return getMap(session).get(cdfile);
	}

	private static HashMap<Long, Long> getMap(HttpSession session) {
		@SuppressWarnings("unchecked")
		HashMap<Long, Long> map = (HashMap<Long, Long>) session.getAttribute(DOWNLOAD_FILE_MAP);
		
		if (map == null) {
			map = new HashMap<Long, Long>();
			session.setAttribute(DOWNLOAD_FILE_MAP, map);
		}

		return map;
	}

	@SuppressWarnings("unchecked")
	protected Resource getResource(HttpServletRequest request, Long cdfile){
		DefaultListableBeanFactory defaultListableBeanFactory = Neo.getApplicationContext().getConfig().getDefaultListableBeanFactory();
		Map beansOfType;
		do{
			beansOfType = defaultListableBeanFactory.getBeansOfType(FileDAO.class); 
			if(beansOfType.size() == 0){
				defaultListableBeanFactory = (DefaultListableBeanFactory)defaultListableBeanFactory.getParentBeanFactory();
			} else{
				break;
			}
		} while(defaultListableBeanFactory != null);
		
		
		if(beansOfType.size() == 1){
			FileDAO<?> fileDAO = (FileDAO<?>) beansOfType.values().iterator().next();
			Class<?>[] allClassesOfTypeFile = Neo.getApplicationContext().getClassManager().getAllClassesOfType(File.class);
			if(allClassesOfTypeFile.length == 1){
				File file;
				try {
					file = (File)allClassesOfTypeFile[0].newInstance();
					file.setCdfile(cdfile);
					try {
						SinedUtil.markAsReader();
						file = fileDAO.loadWithContents(file);
					} catch (Exception e) {
						request.setAttribute("url", TagFunctions.getPartialURL());
						throw new ArquivoNaoEncontradoException("Arquivo n�o encontrado.");
					}
					Resource resource = new Resource(file.getContenttype(), file.getName(), file.getContent());
					Long size = file.getSize();
					if (size != null) {
						resource.setSize(size.intValue());
					}
					return resource;
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				}
			}
		}
		throw new RuntimeException("Estenda a classe DownloadFileServlet e sobrescreva o m�todo getResource");
	}
	
	protected long getLastModified(HttpServletRequest request, Long cdfile){
		return -1;
	}

}
