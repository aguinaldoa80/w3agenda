package br.com.linkcom.sined.util.neo;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.FileDAO;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.geral.service.ImagensEcommerceService;
import br.com.linkcom.sined.util.ArquivoNaoEncontradoException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.tag.TagFunctions;

public class ImagensEcommerceServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public static final String DOWNLOAD_FILE_PATH = "/DOWNLOADFILE";
	
	public static final String DOWNLOAD_FILE_MAP = "NEO_DOWNLOAD_FILE_MAP";
	
	public static ImagensEcommerceService imagensEcommerceService;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Long cdfile;
		String hash = "";
		
		try {
			hash = extractHash(request);
			//busca o cdarquivo daquele hash
			cdfile = imagensEcommerceService.getInstance().loadCdFileByHash(hash);
			if(cdfile == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        	return;
			}
		}
		catch (Exception e) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}
		
		// Obt�m o conte�do
		Resource resource = null;
		
		try{
			resource = getResource(request, cdfile);
	        if(resource == null){
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND);
	        	return;
	        }
		} catch (ArquivoNaoEncontradoException e) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
        	return;
		}

        response.setContentType(resource.getContentType());
		response.setHeader("Content-Disposition","attachment; filename=\""+ resource.getFileName() + "\";");
		//response.setHeader("Last-Modified", );
		if (resource.getSize()>=0) {
			response.setContentLength(resource.getSize());
		}

		response.getOutputStream().write(resource.getContents());
		response.flushBuffer();
	}
	
	protected String extractHash(HttpServletRequest request) throws Exception {
		String requestURI = request.getRequestURI();
		String[] split = requestURI.split("/");
		System.out.println("requestURI: "+requestURI);
		if (split != null && split[split.length-2].length() == 32) {
			String hash = split[split.length-2];
			return hash;
		}else {
			throw new Exception("URL inv�lida");
		}
	}

	@SuppressWarnings("unchecked")
	protected Resource getResource(HttpServletRequest request, Long cdfile){
		DefaultListableBeanFactory defaultListableBeanFactory = Neo.getApplicationContext().getConfig().getDefaultListableBeanFactory();
		Map beansOfType;
		do{
			beansOfType = defaultListableBeanFactory.getBeansOfType(FileDAO.class); 
			if(beansOfType.size() == 0){
				defaultListableBeanFactory = (DefaultListableBeanFactory)defaultListableBeanFactory.getParentBeanFactory();
			} else{
				break;
			}
		} while(defaultListableBeanFactory != null);
		
		
		if(beansOfType.size() == 1){
			FileDAO<?> fileDAO = (FileDAO<?>) beansOfType.values().iterator().next();
			Class<?>[] allClassesOfTypeFile = Neo.getApplicationContext().getClassManager().getAllClassesOfType(File.class);
			if(allClassesOfTypeFile.length == 1){
				File file;
				try {
					file = (File)allClassesOfTypeFile[0].newInstance();
					file.setCdfile(cdfile);
					try {
						SinedUtil.markAsReader();
						file = fileDAO.loadWithContents(file);
					} catch (Exception e) {
						request.setAttribute("url", TagFunctions.getPartialURL());
						throw new ArquivoNaoEncontradoException("Arquivo n�o encontrado.");
					}
					Resource resource = new Resource(file.getContenttype(), file.getName(), file.getContent());
					Long size = file.getSize();
					if (size != null) {
						resource.setSize(size.intValue());
					}
					return resource;
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				}
			}
		}
		throw new RuntimeException("Estenda a classe DownloadFileServlet e sobrescreva o m�todo getResource");
	}
	
	protected long getLastModified(HttpServletRequest request, Long cdfile){
		return -1;
	}
	
}
