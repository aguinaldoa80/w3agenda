package br.com.linkcom.sined.util;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;

//TODO FAZER COM OUTROS CAMPOS DE PESQUISA TAMB�M: DESCRICAO

public class PesquisaCBO {
	
	private final static String URL_CODIGO = "http://www.mtecbo.gov.br/cbosite/pages/pesquisas/BuscaPorCodigo.jsf";
	
	private final static String GET_JSESSION_1 = "action=\"/cbosite/pages/pesquisas/BuscaPorCodigo.jsf;";
	private final static String GET_JSESSION_2 = "\"";
	
	private final static String GET_VIEWSTATE_1 = "<input type=\"hidden\" name=\"javax.faces.ViewState\" id=\"javax.faces.ViewState\" value=\"";
	private final static String GET_VIEWSTATE_2 = "\" />";
	
	private final static String PATTERN_CODIGO = "<td.*?>[\\r\\n\\s]*<p.*?>[\\r\\n\\s]*<a.*?>([0-9]{4}-[0-9]{2})</a>[\\r\\n\\s]*</p>[\\r\\n\\s]*</td>[\\r\\n\\s]*<td.*?>[\\r\\n\\s]*<p.*?>[\\r\\n\\s]*<span.*?>([^<]+)</span>[\\r\\n\\s]*</p>[\\r\\n\\s]*</td>";

	public static List<CBO> findByCodigo(String codigo){
		try{
			// PEGA O HTML DA P�GINA DE BUSCA
			String html1 = getHtml(URL_CODIGO, null);
			
			// PEGA ATRIBUTOS DA P�GINA PARA O FORMUL�RIO
			String jsession = between(html1, GET_JSESSION_1, GET_JSESSION_2);
			String viewState = between(html1, GET_VIEWSTATE_1, GET_VIEWSTATE_2); 
			
			// CRIA OS PAR�METROS
			Params params = new Params();
			params.addParam("j_id66:j_id74", codigo);
			params.addParam("j_id66:procurarButton", "Consultar");
			params.addParam("j_id66_SUBMIT", "1");
			params.addParam("javax.faces.ViewState", viewState);
			
			// PEGA RESULTADO
			String html2 = getHtml(URL_CODIGO + ";" + jsession, params.toString());
			
			Pattern p = Pattern.compile(PATTERN_CODIGO); 
			Matcher m = p.matcher(html2);
			
			List<CBO> listaCBO = new ArrayList<CBO>();
			
			while (m.find()) {
				listaCBO.add(new CBO(StringEscapeUtils.unescapeHtml(m.group(1)), StringEscapeUtils.unescapeHtml(m.group(2))));
			}
			
			return listaCBO;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Erro na busca do CBO.");
		}
	}
	
	private static String between(String string, String comeco, String fim){
		return string.substring(
				string.indexOf(comeco) + comeco.length(), 
				string.indexOf(fim, string.indexOf(comeco) + comeco.length()));
	}
	
	private static String getHtml(String urlstr, String data) throws Exception{
		// Enviando requisi��o
		URL url = new URL(urlstr);  
		HttpURLConnection conn = (HttpURLConnection) url.openConnection(); 
		conn.setRequestMethod("POST");
		conn.setDoOutput(true); 
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
		if(data != null) wr.write(data); 
		wr.flush(); 
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		String line; 
		String html = ""; 
		
		while ((line = rd.readLine()) != null) { html += line; } 
		
		wr.close(); 
		rd.close(); 
		
		return html;
	}
	
	public static class CBO {
		private String codigo;
		private String descricao;
		
		public CBO() {
		}
		
		public CBO(String codigo, String descricao) {
			this.codigo = codigo;
			this.descricao = descricao;
		}
		
		public String getCodigo() {
			return codigo;
		}
		public String getDescricao() {
			return descricao;
		}
		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		@Override
		public String toString() {
			return this.codigo + " - " + this.descricao;
		}
	}
	
	private static class Params {
		
		private Map<String, String> mapParams;
		
		public Params() {
			mapParams = new HashMap<String, String>();
		}
		
		public void addParam(String key, String value){
			mapParams.put(key, value);
		}
		
		@Override
		public String toString() {
			if(mapParams != null && mapParams.size() > 0){
				StringBuilder sb = new StringBuilder();
				try{
					Set<Entry<String, String>> params = mapParams.entrySet();
					for (Iterator<Entry<String, String>> iterator = params.iterator(); iterator.hasNext();) {
						Entry<String, String> entry = (Entry<String, String>) iterator.next();
						sb.append(URLEncoder.encode(entry.getKey(), "LATIN1"))
							.append("=")
							.append(URLEncoder.encode(entry.getValue(), "LATIN1"));
						if(iterator.hasNext()){
							sb.append("&");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return sb != null ? sb.toString() : "";
			} else return "";
		}
	}
}
