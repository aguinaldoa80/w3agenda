package br.com.linkcom.sined.util;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import br.com.linkcom.neo.exception.NeoException;

public class LdapGroupAuthenticator {
	
    public static boolean isSame(String target, String candidate) {
        if (target != null && target.equalsIgnoreCase(candidate)) {
            return true;
        }
        return false;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static boolean authenticate(String username, String password) {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://bd1.linkcom.com.br:389");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "uid=" + username + ",ou=Users,dc=linkcom,dc=com,dc=br");
        env.put(Context.SECURITY_CREDENTIALS, password);
        DirContext ctx = null;
        String defaultSearchBase = "dc=linkcom,dc=com,dc=br";
        String groupDistinguishedName = "cn=linkcom_w3erp_login,ou=Groups,dc=linkcom,dc=com,dc=br";

        try {
            ctx = new InitialDirContext(env);
            
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration results = ctx.search(defaultSearchBase, "(&(cn=*)(memberuid=" + username + "))", controls);
            
            while (results.hasMoreElements()) {
				SearchResult searchResult = (SearchResult) results.next();
				String nameGroup = searchResult.getNameInNamespace();
				if(isSame(groupDistinguishedName, nameGroup)){
					System.out.println(username + " is authorized.");
					return true;
				}
			}
           
            System.out.println(username + " is NOT authorized.");
            return false;
        } catch (AuthenticationException e) {
        	e.printStackTrace();
            System.out.println(username + " is NOT authenticated");
            return false;
        } catch (NamingException e) {
        	e.printStackTrace();
            throw new NeoException(e);
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                } catch (NamingException e) {
                    throw new NeoException(e);
                }
            }
        }
    }

}
