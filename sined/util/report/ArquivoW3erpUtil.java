package br.com.linkcom.sined.util.report;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.SinedUtil;

/**
 *  
 * @author Thiago Clemente
 *
 */
public class ArquivoW3erpUtil {
	
	public static final String QUEBRA_LINHA = "\r\n";
	public static final int RECIBO_TIPO_3_4_COLUNAS = 75; 
	public static final int[][] RECIBO_TIPO_3_4_qtdeLinhasIniciaisReciboPorVia = {{4, 4}, {7, 5}};
	public static final int[][] RECIBO_TIPO_3_4_qtdeLinhasReciboPorVia = {{11, 13}, {13, 12}};
	public static final int[] RECIBO_TIPO_3_4_qtdeLinhasPorRecibo = {24, 25};
	public static final int[] RECIBO_TIPO_3_4_qtdeLinhasAdicionaisPorRecibo = {0, 3};
	public static final int RECIBO_TIPO_3_4_maximoLinhasPorVia = 6;
	
	/**
	 * 
	 * @param stringBuilder
	 * @param total
	 * @author Thiago Clemente
	 * 
	 */
	public static void addQuebraLinha(StringBuilder stringBuilder, int total, String aux){
		for (int i=1; i<=total; i++){
			stringBuilder.append(QUEBRA_LINHA);
			stringBuilder.append(Util.strings.emptyIfNull(aux));
		}
	}
	
	/**
	 * 
	 * @param stringBuilder
	 * @param total
	 * @author Thiago Clemente
	 * 
	 */
	public static void addQuebraLinha(StringBuilder stringBuilder, int total){
		addQuebraLinha(stringBuilder, total, null);
	}
	
	/**
	 * 
	 * @param stringBuilder
	 * @param total
	 * @author Thiago Clemente
	 * 
	 */
	public static void addColunaVazia(StringBuilder stringBuilder, int total){
		for (int i=1; i<=total; i++){
			stringBuilder.append(" ");
		}
	}
	
	/**
	 * 
	 * @param stringBuilder
	 * @param texto
	 * @param tamanho
	 * @param colunaVazia
	 * @author Thiago Clemente
	 * 
	 */
	public static void addTextoQuebraLinha(StringBuilder stringBuilder, String texto, int tamanho, int colunaVazia){
		if (texto.length()>tamanho){
			stringBuilder.append(texto.substring(0, tamanho));
			stringBuilder.append(QUEBRA_LINHA);
			addColunaVazia(stringBuilder, colunaVazia);
			addTextoQuebraLinha(stringBuilder, texto.substring(tamanho), tamanho, colunaVazia);
		}else {
			stringBuilder.append(texto);
		}
	}
	
	public static StringBuilder getStringBuilderAddTexto(StringBuilder stringBuilder, String texto, int linha, int posicaoInicial){
		
		String[] linhasStr = stringBuilder.toString().split(QUEBRA_LINHA);
		StringBuilder stringBuilderNew = new StringBuilder();
		String stringAntes = null;
		String stringDepois = null;
		
		for (int i=0; i<linhasStr.length; i++){
			String linhaStr = linhasStr[i];
			if (i==linha){
				if (linhaStr.length() >= (posicaoInicial + texto.length())){
					stringAntes = linhaStr.substring(0, posicaoInicial);
					stringDepois = linhaStr.substring(posicaoInicial + texto.length());
					linhaStr = stringAntes + texto + stringDepois;
				}else {
					stringAntes = linhaStr;
					for (int j=0; j<posicaoInicial-1; j++){
						linhaStr += " ";
					}
					linhaStr += texto;
					linhaStr += stringAntes;
				}
			}
			stringBuilderNew.append(linhaStr);
			if (i<linhasStr.length-1){
				stringBuilderNew.append(QUEBRA_LINHA);
			}
		}
		
		return stringBuilderNew;
	}
	
	public static List<String> getLinhas(String texto, int colunas){
		
		List<String> linhas = new ArrayList<String>();
		String linha;
		int ultimaPos = 0;
		
		for (int i=0; i<texto.length(); i++){
			if (i==texto.length()-1 || (i+1)%colunas==0){
				linha = texto.substring(ultimaPos, i+1);
				linha = SinedUtil.formataTamanho(linha, colunas, ' ', true);
				linhas.add(linha);
				ultimaPos = i+1;
			}			
		}
		
		return linhas;
	}
	
	public static int getTotalLinhas(StringBuilder stringBuilder){
		return stringBuilder.toString().split("\r").length;
	}
	
	/**
	 * 
	 * @param stringBuilder
	 * @param texto
	 * @param colunasLateral
	 * @param direita
	 * @author Thiago Clemente
	 * 
	 */
	public static void adicionarLateralTextoFormatado(StringBuilder stringBuilder, String texto, int colunasLateral, boolean direita){
		if (direita){
			stringBuilder.append(SinedUtil.formataTamanho(texto, colunasLateral, ' ', true));
		}else {
			stringBuilder.append(SinedUtil.formataTamanho(texto, colunasLateral, ' ', false));
		}
	}
	
	/**
	 * 
	 * @param stringBuilderOriginal
	 * @param colunasTotal
	 * @param colunasLateralEsquerda
	 * @param colunasLateralDireita
	 * @param colunasCentro
	 * @param textoColunaLateralEsquerda
	 * @param textoColunaLateralDireita
	 * @author Thiago Clemente
	 * 
	 */
	public static StringBuilder getTextoFormatado(StringBuilder stringBuilderOriginal,
													int colunasTotal,
													int colunasLateralEsquerda,
													int colunasLateralDireita,
													int colunasCentro,
													String textoColunaLateralEsquerda,
													String textoColunaLateralDireita){
		
		StringBuilder stringBuilderRetorno = new StringBuilder();
		String[] linhasSplit = stringBuilderOriginal.toString().split(QUEBRA_LINHA);
		String linha1;
		String linha2;
		
		for (int i=0; i<linhasSplit.length; i++){
			linha1 = linhasSplit[i];
			for (int j=0; j<linha1.length(); j+=colunasCentro){
				int fim = (j + colunasCentro > linha1.length()) ? linha1.length() : j + colunasCentro;
				linha2 = linha1.substring(j, fim);
				adicionarLateralTextoFormatado(stringBuilderRetorno,textoColunaLateralEsquerda, colunasLateralEsquerda, false);
				stringBuilderRetorno.append(SinedUtil.formataTamanho(linha2, colunasCentro, ' ', true));
				adicionarLateralTextoFormatado(stringBuilderRetorno, textoColunaLateralDireita, colunasLateralDireita, true);
				stringBuilderRetorno.append(QUEBRA_LINHA);
			}
		}
		
		return stringBuilderRetorno;
	}
	
	public static StringBuilder getQuebraDePagina(StringBuilder stringBuilderOriginal,
													int linhasPagina,
													int linhasAdicional){
		
		StringBuilder stringBuilderRetorno = new StringBuilder();
		String[] linhasSplit = stringBuilderOriginal.toString().split(QUEBRA_LINHA);
		int linhasTotal = linhasPagina + linhasAdicional;
		int cont = 1;
		
		for (int i=0; i<linhasSplit.length; i++){
			stringBuilderRetorno.append(linhasSplit[i]);
			stringBuilderRetorno.append(QUEBRA_LINHA);
			if (i==linhasSplit.length-1){
				if (getTotalLinhas(stringBuilderRetorno)<=(linhasTotal)){
					addQuebraLinha(stringBuilderRetorno, linhasTotal - getTotalLinhas(stringBuilderRetorno), " ");
				}else {
					addQuebraLinha(stringBuilderRetorno, linhasTotal - ((getTotalLinhas(stringBuilderRetorno)) % linhasTotal), " ");
				}
				break;
			}else if (cont==linhasPagina){				
				addQuebraLinha(stringBuilderRetorno, linhasAdicional, " ");
				cont = 1;
				continue;
			}else {
				cont++;
			}
		}
		
		return stringBuilderRetorno;
	}
	
	public static void ajustarMaximoLinhas(StringBuilder stringBuilder, Integer maximoLinhas){
		Integer totalLinhas = getTotalLinhas(stringBuilder);
		if (totalLinhas>maximoLinhas){
			String[] linhasSplit = stringBuilder.toString().split(QUEBRA_LINHA);
			StringBuilder stringBuilderNovo = new StringBuilder();
			for (int i=0; i<maximoLinhas; i++){
				stringBuilderNovo.append(linhasSplit[i]);
				stringBuilderNovo.append(QUEBRA_LINHA);
			}
			stringBuilder = new StringBuilder();
			stringBuilder.append(stringBuilderNovo.toString());
		}
	}
	
	public static void formataTamanhoSemCaracteresEspeciais(StringBuilder sb, String valor, int tamanho, char complemento, boolean alinharComplementoDireita){
		sb.append(SinedUtil.formataTamanho(Util.strings.tiraAcento(valor), tamanho, complemento, alinharComplementoDireita));
	}
}