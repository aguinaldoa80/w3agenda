package br.com.linkcom.sined.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Classe com as utilidades de datas da biblioteca.
 * 
 * @author Marden Silva
 * @since 29/05/2008
 */
public class DateUtils {
	
	/**
	 * Transforma uma string do tipo 'dd/MM/yyyy' em um objeto java.util.Date.
	 * 
	 * @param string
	 * @return
	 * @author Marden Silva
	 */
	public static Date stringToDate(String strData){
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date data;
		try {
			data = new Date(format.parse(strData).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return data;
	}
	
	/**
	 * Transforma uma string em um objeto java.util.Date.
	 * 
	 * @param strData
	 * @param mask
	 * @return Date
	 * @author Jo�o Vitor
	 */
	public static Date stringToDate(String strData, String mask){
		
		SimpleDateFormat format = new SimpleDateFormat(mask);
		Date data;
		try {
			data = new Date(format.parse(strData).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return data;
	}
	
	private static Calendar getCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(hoje());
		return cal;
	}

	public static java.util.Date ontem() {
		Calendar cal = getCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}
	
	public static java.util.Date hoje() {
		return new java.util.Date(System.currentTimeMillis());
	}
	
	public static java.util.Date amanha() {
		Calendar cal = getCalendar();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public static java.util.Date primeiroDiaSemana() {
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
		return cal.getTime();
	}

	public static java.util.Date ultimoDiaSemana() {
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		return cal.getTime();
	}


	public static java.util.Date primeiroDiaMes() {
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public static java.util.Date ultimoDiaMes() {
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}
	
	public static java.util.Date primeiroDiaAno() {
		Calendar cal = getCalendar();
		cal.set(Calendar.DAY_OF_YEAR, 1);
		return cal.getTime();
	}

	public static java.util.Date ultimoDiaAno() {
		Calendar cal = getCalendar();
		cal.set(Calendar.MONTH, 11); 
		cal.set(Calendar.DAY_OF_MONTH, 31);
		return cal.getTime();
	}

	public static java.util.Date primeiroDiaMesPassado() {
		Calendar cal = getCalendar();
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); 
		cal.set(Calendar.DAY_OF_MONTH, 1); 
		return cal.getTime();
	}

	public static java.util.Date ultimoDiaMesPassado() {
		Calendar cal = getCalendar();
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); 
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH) );
		return cal.getTime();
	}
	
	public static Boolean fimDeSemana(java.util.Date data) {
		return isSabado(data) || isDomingo(data);
	}

	public static Boolean isSabado(java.util.Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		Integer day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
		return day_of_week == 7;
	}
	
	public static Boolean isDomingo(java.util.Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		Integer day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
		return day_of_week == 1;
	}
	
	public static String mesPorExtenso(Integer mes){
		switch (mes) {
		case 0:
			return "Janeiro";
		case 1:
			return "Fevereiro";
		case 2:
			return "Mar�o";
		case 3:
			return "Abril";
		case 4:
			return "Maio";
		case 5:
			return "Junho";
		case 6:
			return "Julho";
		case 7:
			return "Agosto";
		case 8:
			return "Setembro";
		case 9:
			return "Outubro";
		case 10:
			return "Novembro";
		case 11:
			return "Dezembro";
		default:
			return "M�s Inv�lido.";
		}
	}	
}
