package br.com.linkcom.sined.util.rest.android.contato;


public class ContatoRESTWSBean {

	private Long id;
	private Integer cdpessoa;
	private String nome;
	private String telefone;
	private String email;
	private Integer cdcontatotipo;
	    
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getEmail() {
		return email;
	}
	public Integer getCdcontatotipo() {
		return cdcontatotipo;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setCdcontatotipo(Integer cdcontatotipo) {
		this.cdcontatotipo = cdcontatotipo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
