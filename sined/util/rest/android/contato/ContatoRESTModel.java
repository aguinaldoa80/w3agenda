package br.com.linkcom.sined.util.rest.android.contato;

import br.com.linkcom.sined.geral.bean.Contato;

public class ContatoRESTModel {
	
	private Long id;
	private Integer cdpessoa;
	private Integer cdpessoaligacao;
    private String nome;
    private Integer cdcontatotipo;

    public ContatoRESTModel(){}
	
	public ContatoRESTModel(Contato bean){
		this.cdpessoa = bean.getCdpessoa();
		
		if (bean.getListaPessoa() != null && bean.getListaPessoa().size() > 0) {
			this.cdpessoaligacao = bean.getListaPessoa().get(0).getPessoa() != null ? bean.getListaPessoa().get(0).getPessoa().getCdpessoa() : null;
		}
		
		this.nome = bean.getNome();
		this.cdcontatotipo = bean.getContatotipo() != null ? bean.getContatotipo().getCdcontatotipo() : null;
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}

	public Integer getCdcontatotipo() {
		return cdcontatotipo;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdcontatotipo(Integer cdcontatotipo) {
		this.cdcontatotipo = cdcontatotipo;
	}

	public Integer getCdpessoaligacao() {
		return cdpessoaligacao;
	}

	public void setCdpessoaligacao(Integer cdpessoaligacao) {
		this.cdpessoaligacao = cdpessoaligacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
