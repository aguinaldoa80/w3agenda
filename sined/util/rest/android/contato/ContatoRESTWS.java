package br.com.linkcom.sined.util.rest.android.contato;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Contato")
public class ContatoRESTWS implements SimpleRESTWS<ContatoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ContatoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ContatoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ContatoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ContatoRESTModel> listaContato = ContatoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaContato);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ContatoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
