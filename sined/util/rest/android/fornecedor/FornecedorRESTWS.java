package br.com.linkcom.sined.util.rest.android.fornecedor;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Fornecedor")
public class FornecedorRESTWS implements SimpleRESTWS<FornecedorRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(FornecedorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(FornecedorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(FornecedorRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<FornecedorRESTModel> listaFornecedor = FornecedorService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaFornecedor);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(FornecedorRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
