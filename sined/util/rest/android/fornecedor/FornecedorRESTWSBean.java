package br.com.linkcom.sined.util.rest.android.fornecedor;


public class FornecedorRESTWSBean {

	private Integer cdpessoa;
	private String nome;
    private Boolean ativo;
    private Boolean transportador;
	
    public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Boolean getTransportador() {
		return transportador;
	}
	
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setTransportador(Boolean transportador) {
		this.transportador = transportador;
	}
}
