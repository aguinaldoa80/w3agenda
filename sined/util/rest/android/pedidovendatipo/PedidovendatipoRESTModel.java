package br.com.linkcom.sined.util.rest.android.pedidovendatipo;

import br.com.linkcom.sined.geral.bean.Pedidovendatipo;

public class PedidovendatipoRESTModel {

    private Integer cdpedidovendatipo;
    private String descricao;
    private Boolean bonificacao;
    private Boolean comodato;
    private Boolean obrigarProjeto;
    private Integer cdprojetoprincipal;

    public PedidovendatipoRESTModel(){}
    
    public PedidovendatipoRESTModel(Pedidovendatipo bean){
    	this.cdpedidovendatipo = bean.getCdpedidovendatipo();
    	this.descricao = bean.getDescricao();
    	this.bonificacao = bean.getBonificacao();
    	this.comodato = bean.getComodato();
    	this.obrigarProjeto = Boolean.TRUE.equals(bean.getObrigarProjeto());
    	this.cdprojetoprincipal = bean.getProjetoprincipal() != null ? bean.getProjetoprincipal().getCdprojeto() : null;
    	
    }

	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public Boolean getBonificacao() {
		return bonificacao;
	}
	
	public void setBonificacao(Boolean bonificacao) {
		this.bonificacao = bonificacao;
	}

	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getComodato() {
		return comodato;
	}

	public void setComodato(Boolean comodato) {
		this.comodato = comodato;
	}

	public Boolean getObrigarProjeto() {
		return obrigarProjeto;
	}

	public Integer getCdprojetoprincipal() {
		return cdprojetoprincipal;
	}

	public void setObrigarProjeto(Boolean obrigarProjeto) {
		this.obrigarProjeto = obrigarProjeto;
	}

	public void setCdprojetoprincipal(Integer cdprojetoprincipal) {
		this.cdprojetoprincipal = cdprojetoprincipal;
	}
}
