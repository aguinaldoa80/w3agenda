package br.com.linkcom.sined.util.rest.android.materialQtd;

public class MaterialQtdRESTWSBean {

	private Integer codMat;
	private Integer codEmp;
	private Integer codArm;
	private Integer codProj;

	public MaterialQtdRESTWSBean() {
		super();
	}
	public Integer getCodMat() {
		return codMat;
	}
	public void setCodMat(Integer codMat) {
		this.codMat = codMat;
	}
	public Integer getCodEmp() {
		return codEmp;
	}
	public void setCodEmp(Integer codEmp) {
		this.codEmp = codEmp;
	}
	public Integer getCodArm() {
		return codArm;
	}
	public void setCodArm(Integer codArm) {
		this.codArm = codArm;
	}
	public Integer getCodProj() {
		return codProj;
	}
	public void setCodProj(Integer codProj) {
		this.codProj = codProj;
	}

}
