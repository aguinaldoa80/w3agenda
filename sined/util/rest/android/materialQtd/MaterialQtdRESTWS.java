package br.com.linkcom.sined.util.rest.android.materialQtd;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Quantidade")
public class MaterialQtdRESTWS implements SimpleRESTWS<MaterialQtdRESTWSBean>, AuthenticatedRESTWS {

	private PedidovendaService pedidoVendaService;
	private MaterialService materialService;
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPedidoVendaService(PedidovendaService pedidoVendaService) {
		this.pedidoVendaService = pedidoVendaService;
	}
	
	@Override
	public ModelAndStatus delete(MaterialQtdRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialQtdRESTWSBean command) {
		Material mat = materialService.load(new Material(command.getCodMat()));
		
		Venda venda = new Venda();
		venda.setMaterial(mat);
		venda.setLocalarmazenagem(command.getCodArm() != null ? new Localarmazenagem(command.getCodArm()) : null);
		venda.setEmpresa(new Empresa(command.getCodEmp()));
		venda.setProjeto(command.getCodProj() != null ? new Projeto(command.getCodProj()) : null);
		
		return new ModelAndStatus(pedidoVendaService.buscarQuantidadeAPP(venda));
	}

	@Override
	public ModelAndStatus get(MaterialQtdRESTWSBean command) {
		return null;
	}
	
	@Override
	public ModelAndStatus put(MaterialQtdRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
