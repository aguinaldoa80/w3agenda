package br.com.linkcom.sined.util.rest.android.materialQtd;


public class MaterialQtdRESTModel {

	private Integer cdmaterial;
	private Double qtdEstoque;
	private Double qtdReservada;
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public Double getQtdEstoque() {
		return qtdEstoque;
	}
	public void setQtdEstoque(Double qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}
	public Double getQtdReservada() {
		return qtdReservada;
	}
	public void setQtdReservada(Double qtdReservada) {
		this.qtdReservada = qtdReservada;
	}
	
	
}
