package br.com.linkcom.sined.util.rest.android.documento;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Documento")
public class DocumentoRESTWS implements SimpleRESTWS<DocumentoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(DocumentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DocumentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(DocumentoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<DocumentoRESTModel> listaCliente = DocumentoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(DocumentoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
