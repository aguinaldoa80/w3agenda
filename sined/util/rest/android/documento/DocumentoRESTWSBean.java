package br.com.linkcom.sined.util.rest.android.documento;


public class DocumentoRESTWSBean {

	private Integer cddocumento;
	private String nome;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public String getNome() {
		return nome;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
