package br.com.linkcom.sined.util.rest.android.documento;


import br.com.linkcom.sined.geral.bean.Documento;


public class DocumentoRESTModel {
	
	private Integer cddocumento;
	private String descricao;
	private String numero;
	private Integer cdpessoa;
	private Long dtemissao;
	private Long dtvencimento;
	private Double valor;
	private Double valortotalpago;
	private Double valorrestante;
	private Integer cddocumentoacao;
	
    public DocumentoRESTModel(){}
	
	public DocumentoRESTModel(Documento bean){
		this.cddocumento = bean.getCddocumento();
		this.descricao = bean.getDescricao();
		this.numero = bean.getNumero();
		this.cdpessoa = bean.getPessoa() != null ? bean.getPessoa().getCdpessoa() : null;
		this.dtemissao = bean.getDtemissao() != null ? bean.getDtemissao().getTime() : null;
		this.dtvencimento = bean.getDtvencimento() != null ? bean.getDtvencimento().getTime() : null;
		this.valor = bean.getValor() != null ? bean.getValor().getValue().doubleValue() : null;
		this.valortotalpago = bean.getValortotalpago() != null ? bean.getValortotalpago().getValue().doubleValue() : null;
		this.valorrestante = bean.getValorRestante() != null ? bean.getValorRestante().getValue().doubleValue() : null;
		this.cddocumentoacao = bean.getDocumentoacao() != null ? bean.getDocumentoacao().getCddocumentoacao() : null;
	}

	public Integer getCddocumento() {
		return cddocumento;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getNumero() {
		return numero;
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public Long getDtemissao() {
		return dtemissao;
	}

	public Long getDtvencimento() {
		return dtvencimento;
	}

	public Double getValor() {
		return valor;
	}
	
	public Double getValortotalpago() {
		return valortotalpago;
	}

	public Double getValorrestante() {
		return valorrestante;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setDtemissao(Long dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setDtvencimento(Long dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void setValorrestante(Double valorrestante) {
		this.valorrestante = valorrestante;
	}
	
	public void setValortotalpago(Double valortotalpago) {
		this.valortotalpago = valortotalpago;
	}

	public Integer getCddocumentoacao() {
		return cddocumentoacao;
	}

	public void setCddocumentoacao(Integer cddocumentoacao) {
		this.cddocumentoacao = cddocumentoacao;
	}
	
}
