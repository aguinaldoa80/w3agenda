package br.com.linkcom.sined.util.rest.android.formapagamento;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Formapagamento")
public class FormapagamentoRESTWS implements SimpleRESTWS<FormapagamentoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(FormapagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(FormapagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(FormapagamentoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<FormapagamentoRESTModel> listaFormapagamento = FormapagamentoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaFormapagamento);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(FormapagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
