package br.com.linkcom.sined.util.rest.android.formapagamento;

import br.com.linkcom.sined.geral.bean.Formapagamento;

public class FormapagamentoRESTModel {

    private Integer cdformapagamento;
    private String nome;

    public FormapagamentoRESTModel(){}
    
    public FormapagamentoRESTModel(Formapagamento bean){
    	this.cdformapagamento = bean.getCdformapagamento();
    	this.nome = bean.getNome();
    }

    public Integer getCdformapagamento() {
        return cdformapagamento;
    }

    public void setCdformapagamento(Integer cdformapagamento) {
        this.cdformapagamento = cdformapagamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
