package br.com.linkcom.sined.util.rest.android.prazopagamento;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Prazopagamento")
public class PrazopagamentoRESTWS implements SimpleRESTWS<PrazopagamentoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PrazopagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PrazopagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PrazopagamentoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PrazopagamentoRESTModel> listaPrazopagamento = PrazopagamentoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaPrazopagamento);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PrazopagamentoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
