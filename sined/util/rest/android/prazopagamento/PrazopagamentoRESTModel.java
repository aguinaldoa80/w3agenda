package br.com.linkcom.sined.util.rest.android.prazopagamento;

import br.com.linkcom.sined.geral.bean.Prazopagamento;

public class PrazopagamentoRESTModel {

    private Integer cdprazopagamento;
    private String nome;
    private Double juros;
    private Boolean dataparceladiautil;
    private Integer tipoiniciodata;
    private Boolean parcelasiguaisjuros;
    private Integer diasparainiciar;
    private Boolean avista;
    private Boolean ativo;
    
    public PrazopagamentoRESTModel(){}
    
    public PrazopagamentoRESTModel(Prazopagamento bean){
    	this.cdprazopagamento = bean.getCdprazopagamento();
    	this.nome = bean.getNome();
    	this.juros = bean.getJuros();
    	this.dataparceladiautil = bean.getDataparceladiautil();
    	this.tipoiniciodata = bean.getTipoiniciodata() != null ? bean.getTipoiniciodata().ordinal() : null;
    	this.parcelasiguaisjuros = bean.getParcelasiguaisjuros();
    	this.diasparainiciar = bean.getDiasparainiciar();
    	this.avista = bean.getAvista();
    	this.ativo = bean.getAtivo();
    }
    
    public Integer getCdprazopagamento() {
        return cdprazopagamento;
    }

    public void setCdprazopagamento(Integer cdprazopagamento) {
        this.cdprazopagamento = cdprazopagamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Double getJuros() {
		return juros;
	}
    
    public void setJuros(Double juros) {
		this.juros = juros;
	}

	public Integer getTipoiniciodata() {
		return tipoiniciodata;
	}

	public Boolean getParcelasiguaisjuros() {
		return parcelasiguaisjuros;
	}

	public Integer getDiasparainiciar() {
		return diasparainiciar;
	}

	public void setTipoiniciodata(Integer tipoiniciodata) {
		this.tipoiniciodata = tipoiniciodata;
	}

	public void setParcelasiguaisjuros(Boolean parcelasiguaisjuros) {
		this.parcelasiguaisjuros = parcelasiguaisjuros;
	}

	public void setDiasparainiciar(Integer diasparainiciar) {
		this.diasparainiciar = diasparainiciar;
	}
	
	public Boolean getDataparceladiautil() {
		return dataparceladiautil;
	}
	
	public void setDataparceladiautil(Boolean dataparceladiautil) {
		this.dataparceladiautil = dataparceladiautil;
	}

	public Boolean getAvista() {
		return avista;
	}

	public void setAvista(Boolean avista) {
		this.avista = avista;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
    
}
