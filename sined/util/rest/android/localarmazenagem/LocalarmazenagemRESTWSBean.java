package br.com.linkcom.sined.util.rest.android.localarmazenagem;


public class LocalarmazenagemRESTWSBean {

	private Integer cdlocalarmazenagem;
	private String nome;
	
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public String getNome() {
		return nome;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
