package br.com.linkcom.sined.util.rest.android.localarmazenagem;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;

public class LocalarmazenagemRESTModel {
	
	private Integer cdlocalarmazenagem;
	private String nome;
	private Boolean principal;
	private Boolean ativo;
	
    public LocalarmazenagemRESTModel(){}
	
	public LocalarmazenagemRESTModel(Localarmazenagem bean){
		this.cdlocalarmazenagem = bean.getCdlocalarmazenagem();
		this.nome = bean.getNome();
		this.principal = bean.getPrincipal();
		this.ativo = bean.getAtivo();
	}

	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}

	public String getNome() {
		return nome;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
