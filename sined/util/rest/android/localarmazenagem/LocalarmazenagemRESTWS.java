package br.com.linkcom.sined.util.rest.android.localarmazenagem;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Localarmazenagem")
public class LocalarmazenagemRESTWS implements SimpleRESTWS<LocalarmazenagemRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(LocalarmazenagemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(LocalarmazenagemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(LocalarmazenagemRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<LocalarmazenagemRESTModel> listaCliente = LocalarmazenagemService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(LocalarmazenagemRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
