package br.com.linkcom.sined.util.rest.android.arquivojson;


public class ArquivojsonRESTWSBean {

	private String nomearquivo;
    private String caminho;

    public String getNomearquivo() {
        return nomearquivo;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setNomearquivo(String nomearquivo) {
        this.nomearquivo = nomearquivo;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
}
