package br.com.linkcom.sined.util.rest.android.arquivojson;

import java.io.File;

public class ArquivojsonRESTModel {
	
    private String nomearquivo;
    private String caminho;
    private Integer ordem;
    
    public ArquivojsonRESTModel(){}
    
    public ArquivojsonRESTModel(String ctx, File file){
    	this.nomearquivo = file.getName();
    	this.caminho = ctx + "/pub/Arquivojsonandroid?f=" + file.getName();
    	try {
    		this.ordem = Integer.parseInt(file.getName().substring(0, file.getName().indexOf("_")));
		} catch (Exception e) {
			this.ordem = Integer.MAX_VALUE;
		}
    }

    public String getNomearquivo() {
        return nomearquivo;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setNomearquivo(String nomearquivo) {
        this.nomearquivo = nomearquivo;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
    
    public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
}
