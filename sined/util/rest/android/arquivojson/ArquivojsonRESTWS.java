package br.com.linkcom.sined.util.rest.android.arquivojson;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.util.AndroidUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Arquivojson")
public class ArquivojsonRESTWS implements SimpleRESTWS<ArquivojsonRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ArquivojsonRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ArquivojsonRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ArquivojsonRESTWSBean command) {
		String ctx = SinedUtil.getUrlWithContext();
		List<ArquivojsonRESTModel> lista = new ArrayList<ArquivojsonRESTModel>();
		List<File> listFile = AndroidUtil.getAndroidFiles();
		if(SinedUtil.isListNotEmpty(listFile)){
			for(File file : listFile){
				lista.add(new ArquivojsonRESTModel(ctx, file));
			}
		}
		
		Collections.sort(lista,new Comparator<ArquivojsonRESTModel>(){
			public int compare(ArquivojsonRESTModel a, ArquivojsonRESTModel b) {
				return a.getOrdem().compareTo(b.getOrdem());
			}
		});
		
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		modelAndStatus.setModel(lista);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ArquivojsonRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
