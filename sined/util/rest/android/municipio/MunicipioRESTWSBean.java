package br.com.linkcom.sined.util.rest.android.municipio;


public class MunicipioRESTWSBean {

	private Integer cdmunicipio;
	private String nome;
	private Integer cduf;
	
	public Integer getCdmunicipio() {
		return cdmunicipio;
	}
	public String getNome() {
		return nome;
	}
	public Integer getCduf() {
		return cduf;
	}
	
	public void setCdmunicipio(Integer cdmunicipio) {
		this.cdmunicipio = cdmunicipio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}
}
