package br.com.linkcom.sined.util.rest.android.municipio;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Municipio")
public class MunicipioRESTWS implements SimpleRESTWS<MunicipioRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MunicipioRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MunicipioRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MunicipioRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MunicipioRESTModel> listaMunicipio = MunicipioService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMunicipio);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MunicipioRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
