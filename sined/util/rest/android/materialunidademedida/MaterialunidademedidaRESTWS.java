package br.com.linkcom.sined.util.rest.android.materialunidademedida;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialunidademedida")
public class MaterialunidademedidaRESTWS implements SimpleRESTWS<MaterialunidademedidaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialunidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialunidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialunidademedidaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialunidademedidaRESTModel> listaMaterialunidademedida = MaterialunidademedidaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialunidademedida);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialunidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
