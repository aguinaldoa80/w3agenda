package br.com.linkcom.sined.util.rest.android.materialunidademedida;

import br.com.linkcom.sined.geral.bean.Materialunidademedida;

public class MaterialunidademedidaRESTModel {

    private Integer cdmaterialunidademedida;
    private Integer cdunidademedida;
    private Integer cdmaterial;
    private Double fracao;
    private Double qtdereferencia;
    private Double valorunitario;
    private Double valormaximo;
    private Double valorminimo;
    private Boolean prioridadevenda;
    
    public MaterialunidademedidaRESTModel(){}
    
    public MaterialunidademedidaRESTModel(Materialunidademedida bean){
    	this.cdmaterialunidademedida = bean.getCdmaterialunidademedida();
    	this.cdunidademedida = bean.getUnidademedida() != null ? bean.getUnidademedida().getCdunidademedida() : null;
    	this.cdmaterial = bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null;
    	this.fracao = bean.getFracao();
    	this.qtdereferencia = bean.getQtdereferencia();
    	this.valorunitario = bean.getValorunitario();
    	this.valormaximo = bean.getValormaximo();
    	this.valorminimo = bean.getValorminimo();
    	this.prioridadevenda = bean.getPrioridadevenda();
    }

	public Integer getCdmaterialunidademedida() {
		return cdmaterialunidademedida;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Double getFracao() {
		return fracao;
	}

	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public Double getValorunitario() {
		return valorunitario;
	}

	public void setCdmaterialunidademedida(Integer cdmaterialunidademedida) {
		this.cdmaterialunidademedida = cdmaterialunidademedida;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public Double getValormaximo() {
		return valormaximo;
	}

	public Double getValorminimo() {
		return valorminimo;
	}

	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}

	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}

	public Boolean getPrioridadevenda() {
		return prioridadevenda;
	}

	public void setPrioridadevenda(Boolean prioridadevenda) {
		this.prioridadevenda = prioridadevenda;
	}
}
