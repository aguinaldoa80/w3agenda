package br.com.linkcom.sined.util.rest.android.vwultimascompras;


public class VwultimascomprasandroidRESTWSBean {

	private Integer id;
	private Integer cdpedidovenda;

	public Integer getId() {
		return id;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
}
