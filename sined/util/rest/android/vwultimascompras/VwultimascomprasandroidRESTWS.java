package br.com.linkcom.sined.util.rest.android.vwultimascompras;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.VwultimascomprasandroidService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Vwultimascomprasandroid")
public class VwultimascomprasandroidRESTWS implements SimpleRESTWS<VwultimascomprasandroidRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(VwultimascomprasandroidRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(VwultimascomprasandroidRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(VwultimascomprasandroidRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<VwultimascomprasandroidRESTModel> listaVwultimascomprasandroid = VwultimascomprasandroidService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaVwultimascomprasandroid);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(VwultimascomprasandroidRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
