package br.com.linkcom.sined.util.rest.android.vwultimascompras;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.view.Vwultimascomprasandroid;

public class VwultimascomprasandroidRESTModel {
	
	private String id;
    private Integer cdpedidovenda;
    private Date dtpedidovenda;
    private Integer cdcliente;
    private Integer cdmaterial;
    private Double quantidade;
    private Integer cdunidademedida;
    private Double preco;
    private Integer cdpedidovendatipo;
    private Integer pedidovendasituacao;
    private Long datapedidovenda;
    
    public VwultimascomprasandroidRESTModel(){}
	
	public VwultimascomprasandroidRESTModel(Vwultimascomprasandroid bean){
		this.id = bean.getId();
	    this.cdpedidovenda = bean.getCdpedidovenda();
	    this.datapedidovenda = bean.getDtpedidovenda() != null ? bean.getDtpedidovenda().getTime() : null;
	    this.cdcliente = bean.getCdcliente();
	    this.cdmaterial = bean.getCdmaterial();
	    this.quantidade = bean.getQuantidade();
	    this.cdunidademedida = bean.getCdunidademedida();
	    this.preco = bean.getPreco();
	    this.cdpedidovendatipo = bean.getCdpedidovendatipo();
	    this.pedidovendasituacao = bean.getPedidovendasituacao();
	}

	public String getId() {
		return id;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public Date getDtpedidovenda() {
		return dtpedidovenda;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public Double getPreco() {
		return preco;
	}
	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	public Long getDatapedidovenda() {
		return datapedidovenda;
	}
	public Integer getPedidovendasituacao() {
		return pedidovendasituacao;
	}

	public void setPedidovendasituacao(Integer pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setDtpedidovenda(Date dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
	public void setDatapedidovenda(Long datapedidovenda) {
		this.datapedidovenda = datapedidovenda;
	}
}
