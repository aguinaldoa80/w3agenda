package br.com.linkcom.sined.util.rest.android.login;

public class LoginRESTWSBean {

	private String login;
	private String hash;
	public String getLogin() {
		return login;
	}
	public String getHash() {
		return hash;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	
}
