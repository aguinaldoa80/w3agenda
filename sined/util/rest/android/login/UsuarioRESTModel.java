package br.com.linkcom.sined.util.rest.android.login;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.util.rest.android.usuarioempresa.UsuarioempresaRESTModel;

public class UsuarioRESTModel {
	
	private Integer cdpessoa;
	private String nome;
    private Boolean restricaoclientevendedor;
    private Boolean restricaovendavendedor;
	
	private List<UsuarioempresaRESTModel> listaUsuarioempressa;
	
	public UsuarioRESTModel(Usuario usuario){
		this.cdpessoa = usuario.getCdpessoa();
		this.nome = usuario.getNome();
		this.restricaoclientevendedor = usuario.getRestricaoclientevendedor() != null ? usuario.getRestricaoclientevendedor() : false;
		this.restricaovendavendedor = usuario.getRestricaovendavendedor() != null ? usuario.getRestricaovendavendedor() : false;
		
		if(usuario.getListaUsuarioempresa() != null && !usuario.getListaUsuarioempresa().isEmpty()){
			List<UsuarioempresaRESTModel> lista = new ArrayList<UsuarioempresaRESTModel>();
			for(Usuarioempresa usuarioempresa : usuario.getListaUsuarioempresa()){
				lista.add(new UsuarioempresaRESTModel(usuarioempresa));
			}
			this.listaUsuarioempressa = lista;
		}
	}
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getRestricaoclientevendedor() {
		return restricaoclientevendedor;
	}

	public Boolean getRestricaovendavendedor() {
		return restricaovendavendedor;
	}

	public void setRestricaoclientevendedor(Boolean restricaoclientevendedor) {
		this.restricaoclientevendedor = restricaoclientevendedor;
	}

	public void setRestricaovendavendedor(Boolean restricaovendavendedor) {
		this.restricaovendavendedor = restricaovendavendedor;
	}

	public List<UsuarioempresaRESTModel> getListaUsuarioempressa() {
		return listaUsuarioempressa;
	}

	public void setListaUsuarioempressa(
			List<UsuarioempresaRESTModel> listaUsuarioempressa) {
		this.listaUsuarioempressa = listaUsuarioempressa;
	}
}
