package br.com.linkcom.sined.util.rest.android.login;

import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SHA512Util;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.RESTException;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.annotations.URI;

@Bean
@URI("/android/Login")
public class LoginRESTWS implements SimpleRESTWS<LoginRESTWSBean> {

	
	private UsuarioService usuarioService;
	
	public void setUsu(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	
	@Override
	public ModelAndStatus post(LoginRESTWSBean command) {
		if(command==null)
			throw new RESTException("Dados informados s�o nulos ou inv�lidos.");
		
		if(StringUtils.isBlank(command.getLogin()))
			throw new RESTException("Login n�o informado.");
		
		if(StringUtils.isBlank(command.getHash()))
			throw new RESTException("Senha n�o informada.");
		 
		
		
		AuthorizationDAO authorizationDAO = Neo.getObject(AuthorizationDAO.class);
		Usuario user = (Usuario) authorizationDAO.findUserByLogin(command.getLogin());
		if(user==null){
			throw new RESTException("Login inv�lido.", "Login inv�lido.");
		}
		
		if(user.getBloqueado()){
			throw new RESTException("O usu�rio informado est� bloqueado.", "O usu�rio informado est� bloqueado.");
		}
		
		String hash;
		try {
			hash = SHA512Util.encrypt(user.getLogin()+"|#$"+user.getSenha());
		} catch (NoSuchAlgorithmException e) {
			throw new RESTException(e);			
		}
		 
		if (command.getHash().equals(hash)) {
			Usuario usuarioDB = usuarioService.carregaUsuarioWithEmpresa(user);
			return new ModelAndStatus(new UsuarioRESTModel(usuarioDB));
		}else{
			throw new RESTException("Usu�rio e/ou senha inv�lidos.", "Usu�rio e/ou senha inv�lidos.");
		}
			
		
	}

	@Override public ModelAndStatus get(LoginRESTWSBean command) {return null;}
	@Override public ModelAndStatus delete(LoginRESTWSBean command) {return null;}
	@Override public ModelAndStatus put(LoginRESTWSBean command) {return null;}
}
