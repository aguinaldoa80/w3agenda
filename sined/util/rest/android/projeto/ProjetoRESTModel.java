package br.com.linkcom.sined.util.rest.android.projeto;

import br.com.linkcom.sined.geral.bean.Projeto;

public class ProjetoRESTModel {

    private Integer cdprojeto;
    private String nome;
    private Integer situacao;

    public ProjetoRESTModel(){}
    
    public ProjetoRESTModel(Projeto bean){
    	this.cdprojeto = bean.getCdprojeto();
    	this.nome = bean.getNome();
    	this.situacao = bean.getSituacao() != null ? bean.getSituacao().ordinal() : null;
    }
    
    public Integer getCdprojeto() {
        return cdprojeto;
    }

    public void setCdprojeto(Integer cdprojeto) {
        this.cdprojeto = cdprojeto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
}
