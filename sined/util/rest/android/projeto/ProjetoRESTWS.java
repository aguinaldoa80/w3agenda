package br.com.linkcom.sined.util.rest.android.projeto;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Projeto")
public class ProjetoRESTWS implements SimpleRESTWS<ProjetoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ProjetoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ProjetoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ProjetoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ProjetoRESTModel> listaProjeto = ProjetoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaProjeto);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ProjetoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
