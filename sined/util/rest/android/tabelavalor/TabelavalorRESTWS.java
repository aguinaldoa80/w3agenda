package br.com.linkcom.sined.util.rest.android.tabelavalor;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.TabelavalorService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Tabelavalor")
public class TabelavalorRESTWS implements SimpleRESTWS<TabelavalorRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(TabelavalorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(TabelavalorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(TabelavalorRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<TabelavalorRESTModel> listaCliente = TabelavalorService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(TabelavalorRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
