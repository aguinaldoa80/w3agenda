package br.com.linkcom.sined.util.rest.android.tabelavalor;


public class TabelavalorRESTWSBean {

	protected Integer cdtabelavalor;
	protected String identificador;
	protected String descricao;	
	protected Double valor;

	public Integer getCdtabelavalor() {
		return cdtabelavalor;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getDescricao() {
		return descricao;
	}
	public Double getValor() {
		return valor;
	}
	
	public void setCdtabelavalor(Integer cdtabelavalor) {
		this.cdtabelavalor = cdtabelavalor;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
}
