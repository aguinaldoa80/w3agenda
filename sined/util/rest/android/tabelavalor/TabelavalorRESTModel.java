package br.com.linkcom.sined.util.rest.android.tabelavalor;

import br.com.linkcom.sined.geral.bean.Tabelavalor;

public class TabelavalorRESTModel {
	
	protected Integer cdtabelavalor;
	protected String identificador;
	protected String descricao;	
	protected Double valor;
	
    public TabelavalorRESTModel(){}
	
	public TabelavalorRESTModel(Tabelavalor bean){
		this.cdtabelavalor = bean.getCdtabelavalor();
		this.identificador = bean.getIdentificador();
		this.descricao = bean.getDescricao();
		this.valor = bean.getValor();
	}

	public Integer getCdtabelavalor() {
		return cdtabelavalor;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getDescricao() {
		return descricao;
	}
	public Double getValor() {
		return valor;
	}

	public void setCdtabelavalor(Integer cdtabelavalor) {
		this.cdtabelavalor = cdtabelavalor;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
}
