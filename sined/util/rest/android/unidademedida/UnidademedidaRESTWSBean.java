package br.com.linkcom.sined.util.rest.android.unidademedida;


public class UnidademedidaRESTWSBean {

	private Integer cdunidademedida;
	private String nome;
	
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome() {
		return nome;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
