package br.com.linkcom.sined.util.rest.android.unidademedida;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Unidademedida")
public class UnidademedidaRESTWS implements SimpleRESTWS<UnidademedidaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(UnidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(UnidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(UnidademedidaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<UnidademedidaRESTModel> listaUnidademedida = UnidademedidaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaUnidademedida);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(UnidademedidaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
