package br.com.linkcom.sined.util.rest.android.unidademedida;

import br.com.linkcom.sined.geral.bean.Unidademedida;

public class UnidademedidaRESTModel {
	
	private Integer cdunidademedida;
    private String nome;
    private String simbolo;
    private Boolean ativo;
    
    public UnidademedidaRESTModel(){}
    
    public UnidademedidaRESTModel(Unidademedida um){
		this.cdunidademedida = um.getCdunidademedida();
		this.nome = um.getNome();
		this.simbolo = um.getSimbolo();
		this.ativo = um.getAtivo();
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public String getNome() {
		return nome;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
