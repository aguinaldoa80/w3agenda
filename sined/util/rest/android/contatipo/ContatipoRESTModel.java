package br.com.linkcom.sined.util.rest.android.contatipo;

import br.com.linkcom.sined.geral.bean.Contatipo;

public class ContatipoRESTModel {
	
	private Integer cdcontatipo;
	private String nome;

	public ContatipoRESTModel(){}

    public ContatipoRESTModel(Contatipo bean) {
		setCdcontatipo(bean.getCdcontatipo());
		setNome(bean.getNome());
	}

	public Integer getCdcontatipo() {
        return cdcontatipo;
    }

    public void setCdcontatipo(Integer cdcontatipo) {
        this.cdcontatipo = cdcontatipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
