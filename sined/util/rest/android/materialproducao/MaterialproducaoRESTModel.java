package br.com.linkcom.sined.util.rest.android.materialproducao;

import br.com.linkcom.sined.geral.bean.Materialproducao;

public class MaterialproducaoRESTModel {
	
	private Integer cdmaterialproducao;
    protected Integer cdmaterialmestre;
    protected Integer cdmaterial;
    protected Boolean exibirvenda;
    
    public MaterialproducaoRESTModel(){}
    
    public MaterialproducaoRESTModel(Materialproducao m){
		this.cdmaterialproducao = m.getCdmaterialproducao();
		this.cdmaterialmestre = m.getMaterialmestre() != null ? m.getMaterialmestre().getCdmaterial() : null;
		this.cdmaterial = m.getMaterial() != null ? m.getMaterial().getCdmaterial() : null;
		this.exibirvenda = m.getExibirvenda() != null ? m.getExibirvenda() : false;
	}

	public Integer getCdmaterialproducao() {
		return cdmaterialproducao;
	}
	public Integer getCdmaterialmestre() {
		return cdmaterialmestre;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Boolean getExibirvenda() {
		return exibirvenda;
	}

	public void setCdmaterialproducao(Integer cdmaterialproducao) {
		this.cdmaterialproducao = cdmaterialproducao;
	}
	public void setCdmaterialmestre(Integer cdmaterialmestre) {
		this.cdmaterialmestre = cdmaterialmestre;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setExibirvenda(Boolean exibirvenda) {
		this.exibirvenda = exibirvenda;
	}
}
