package br.com.linkcom.sined.util.rest.android.materialproducao;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialproducao")
public class MaterialproducaoRESTWS implements SimpleRESTWS<MaterialproducaoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialproducaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialproducaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialproducaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialproducaoRESTModel> listaMaterialproducao = MaterialproducaoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialproducao);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialproducaoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
