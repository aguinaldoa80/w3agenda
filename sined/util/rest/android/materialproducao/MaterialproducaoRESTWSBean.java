package br.com.linkcom.sined.util.rest.android.materialproducao;


public class MaterialproducaoRESTWSBean {

	private Integer cdmaterialproducao;
	private Integer cdmaterial;
	
	public Integer getCdmaterialproducao() {
		return cdmaterialproducao;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterialproducao(Integer cdmaterialproducao) {
		this.cdmaterialproducao = cdmaterialproducao;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
}
