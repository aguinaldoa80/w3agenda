package br.com.linkcom.sined.util.rest.android.clientevendedor;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Clientevendedor")
public class ClientevendedorRESTWS implements SimpleRESTWS<ClientevendedorRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ClientevendedorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ClientevendedorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ClientevendedorRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ClientevendedorRESTModel> listaConta = ClientevendedorService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaConta);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ClientevendedorRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
