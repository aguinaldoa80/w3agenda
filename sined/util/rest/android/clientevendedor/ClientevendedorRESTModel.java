package br.com.linkcom.sined.util.rest.android.clientevendedor;

import br.com.linkcom.sined.geral.bean.Clientevendedor;

public class ClientevendedorRESTModel {

    private Integer cdclientevendedor;
	private Integer cdcolaborador;
	private Integer cdcliente;

    public ClientevendedorRESTModel(){}
    
    public ClientevendedorRESTModel(Clientevendedor bean){
    	this.cdclientevendedor = bean.getCdclientevendedor();
    	this.cdcolaborador = bean.getColaborador()==null?null: bean.getColaborador().getCdpessoa();
    	this.cdcliente = bean.getCliente()==null?null: bean.getCliente().getCdpessoa();
    }

	public Integer getCdclientevendedor() {
		return cdclientevendedor;
	}

	public Integer getCdcolaborador() {
		return cdcolaborador;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public void setCdclientevendedor(Integer cdclientevendedor) {
		this.cdclientevendedor = cdclientevendedor;
	}

	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

    
    
}
