package br.com.linkcom.sined.util.rest.android.conta;

import org.apache.commons.lang.BooleanUtils;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;

public class ContaRESTModel {

    private Integer cdconta;
    private String nome;
    private String agencia;
    private String dvagencia;
    private String dvnumero;
    private String numero;
    private Boolean gerarboleto;
    private Integer cdcontatipo;

    public ContaRESTModel(){}
    
    public ContaRESTModel(Conta bean){
    	this.cdconta = bean.getCdconta();
    	this.nome = bean.getNome();
    	this.agencia = bean.getAgencia();
    	this.dvagencia = bean.getDvagencia();
        this.dvnumero = bean.getDvnumero();
        this.numero = bean.getNumero();
        this.gerarboleto = Boolean.FALSE;
        if((bean.getListaContacarteira() == null) || (bean.getListaContacarteira().isEmpty())){
        	this.gerarboleto = Boolean.TRUE;
        }else{
        	for(Contacarteira contacarteira: bean.getListaContacarteira()){
        		if(!BooleanUtils.isTrue(contacarteira.getNaogerarboleto())){
        			this.gerarboleto = Boolean.TRUE;
        			break;
        		}
        	}
        }
        
        this.cdcontatipo = bean.getContatipo()!=null ? bean.getContatipo().getCdcontatipo() : null;
    }

	public Integer getCdconta() {
		return cdconta;
	}

	public String getNome() {
		return nome;
	}

	public String getAgencia() {
		return agencia;
	}

	public String getDvagencia() {
		return dvagencia;
	}

	public String getDvnumero() {
		return dvnumero;
	}

	public String getNumero() {
		return numero;
	}

	public Boolean getGerarboleto() {
		return gerarboleto;
	}
	
	public Integer getCdcontatipo() {
		return cdcontatipo;
	}
	
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}

	public void setDvnumero(String dvnumero) {
		this.dvnumero = dvnumero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setGerarboleto(Boolean gerarboleto) {
		this.gerarboleto = gerarboleto;
	}
	
	public void setCdcontatipo(Integer cdcontatipo) {
		this.cdcontatipo = cdcontatipo;
	}
}
