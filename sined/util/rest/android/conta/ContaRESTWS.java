package br.com.linkcom.sined.util.rest.android.conta;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Conta")
public class ContaRESTWS implements SimpleRESTWS<ContaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ContaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ContaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ContaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ContaRESTModel> listaConta = ContaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaConta);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ContaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
