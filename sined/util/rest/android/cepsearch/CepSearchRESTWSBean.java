package br.com.linkcom.sined.util.rest.android.cepsearch;

public class CepSearchRESTWSBean {

	private String cep;
	
	public String getCep() {
		return cep;
	}
	
	public void setCep(String cep) {
		this.cep = cep;
	}
}
