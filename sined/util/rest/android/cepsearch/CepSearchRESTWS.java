package br.com.linkcom.sined.util.rest.android.cepsearch;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.wscep.service.CepSearch;
import br.com.linkcom.sined.wscep.service.CepSearchServiceLocator;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.RESTException;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/CepSearch")
public class CepSearchRESTWS implements SimpleRESTWS<CepSearchRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(CepSearchRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(CepSearchRESTWSBean command) {
		if(command==null || StringUtils.isBlank(command.getCep()))
			throw new RESTException("CEP n�o informado");
		
		try{
			new Cep(command.getCep());
		}catch (IllegalArgumentException e) {
			throw new RESTException("CEP inv�lido");
		}
		
		
		String endereco;
		String logradouro    = "";
		String complemento   = "";
		String bairro        = "";
		String nomeMunicipio = "";
		String siglaUf       = "";
		String cdmunicipio   = "";
		String cduf          = "";		

		CepSearchServiceLocator loc = new CepSearchServiceLocator();
		CepSearch cepSearch;
		try {
			cepSearch = loc.getCepSearch();
			endereco = cepSearch.findAddressByCep(command.getCep().replaceAll("\\.", "").replaceAll("-", ""));
		} catch (Exception e) {
			throw new RESTException("Erro ao realizar a consulta no servi�o de CEP." ,e);
		}
		
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		
		if (endereco!=null){
			String[] campos = endereco.split("#",5);
			logradouro    = (campos.length > 0 ? campos[0] : "");
			complemento   = (campos.length > 1 ? campos[1] : "");
			bairro        = (campos.length > 2 ? campos[2] : "");
			nomeMunicipio = (campos.length > 3 ? campos[3] : "");
			siglaUf       = (campos.length > 4 ? campos[4] : "");
			cdmunicipio   = "";
			cduf          = "";

			if (!nomeMunicipio.equals("") && !siglaUf.equals("")) {
				Municipio municipio = MunicipioService.getInstance().getByNomeAndUf( nomeMunicipio, siglaUf);
				if (municipio != null) {
					cdmunicipio = municipio.getCdmunicipio().toString();
					cduf = municipio.getUf().getCduf().toString();
				}
			}
			
			EnderecoRESTModel model = new EnderecoRESTModel();
			model.setLogradouro(logradouro);
			model.setComplemento(complemento);
			model.setBairro(bairro);
			model.setCdmunicipio(!cdmunicipio.equals("") ? Integer.parseInt(cdmunicipio) : null);
			model.setCduf(!cduf.equals("") ? Integer.parseInt(cduf) : null);
			modelAndStatus.setModel(model);
		}else{
			throw new RESTException("CEP informado n�o encontrado.");
		}
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(CepSearchRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(CepSearchRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
