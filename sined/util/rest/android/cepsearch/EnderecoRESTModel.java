package br.com.linkcom.sined.util.rest.android.cepsearch;

public class EnderecoRESTModel {
	
	private String logradouro;
	private String complemento;
	private String bairro;
	private Integer cdmunicipio;
	private Integer cduf;
	
	public String getLogradouro() {
		return logradouro;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public Integer getCdmunicipio() {
		return cdmunicipio;
	}
	public Integer getCduf() {
		return cduf;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCdmunicipio(Integer cdmunicipio) {
		this.cdmunicipio = cdmunicipio;
	}
	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}
	
	
	
}
