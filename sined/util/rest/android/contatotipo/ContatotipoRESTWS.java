package br.com.linkcom.sined.util.rest.android.contatotipo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ContatotipoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Contatotipo")
public class ContatotipoRESTWS implements SimpleRESTWS<ContatotipoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ContatotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ContatotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ContatotipoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ContatotipoRESTModel> listaContatotipo = ContatotipoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaContatotipo);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ContatotipoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
