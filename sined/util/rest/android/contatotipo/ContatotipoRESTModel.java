package br.com.linkcom.sined.util.rest.android.contatotipo;

import br.com.linkcom.sined.geral.bean.Contatotipo;

public class ContatotipoRESTModel {
	
	private Integer cdcontatotipo;
    private String nome;

    public ContatotipoRESTModel(){}
	
	public ContatotipoRESTModel(Contatotipo bean){
		this.cdcontatotipo = bean.getCdcontatotipo();
		this.nome = bean.getNome();
	}

	public Integer getCdcontatotipo() {
		return cdcontatotipo;
	}

	public String getNome() {
		return nome;
	}

	public void setCdcontatotipo(Integer cdcontatotipo) {
		this.cdcontatotipo = cdcontatotipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
