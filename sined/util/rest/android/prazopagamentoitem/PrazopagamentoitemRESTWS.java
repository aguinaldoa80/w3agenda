package br.com.linkcom.sined.util.rest.android.prazopagamentoitem;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PrazopagamentoitemService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Prazopagamentoitem")
public class PrazopagamentoitemRESTWS implements SimpleRESTWS<PrazopagamentoitemRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PrazopagamentoitemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PrazopagamentoitemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PrazopagamentoitemRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PrazopagamentoitemRESTModel> listaPrazopagamentoitem = PrazopagamentoitemService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaPrazopagamentoitem);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PrazopagamentoitemRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
