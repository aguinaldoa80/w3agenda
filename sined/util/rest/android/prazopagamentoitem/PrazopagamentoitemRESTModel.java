package br.com.linkcom.sined.util.rest.android.prazopagamentoitem;

import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;

public class PrazopagamentoitemRESTModel {

    private Integer cdprazopagamentoitem;
    private Integer dias;
    private Integer meses;
    private Integer parcela;
    private Integer cdprazopagamento;

    public PrazopagamentoitemRESTModel(){}
    
    public PrazopagamentoitemRESTModel(Prazopagamentoitem bean){
    	this.cdprazopagamentoitem = bean.getCdprazopagamentoitem();
    	this.dias = bean.getDias();
    	this.meses = bean.getMeses();
    	this.parcela = bean.getParcela();
    	this.cdprazopagamento = bean.getPrazopagamento() != null ? bean.getPrazopagamento().getCdprazopagamento() : null; 
    }

	public Integer getCdprazopagamentoitem() {
		return cdprazopagamentoitem;
	}

	public Integer getDias() {
		return dias;
	}

	public Integer getMeses() {
		return meses;
	}

	public Integer getParcela() {
		return parcela;
	}

	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}

	public void setCdprazopagamentoitem(Integer cdprazopagamentoitem) {
		this.cdprazopagamentoitem = cdprazopagamentoitem;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
}
