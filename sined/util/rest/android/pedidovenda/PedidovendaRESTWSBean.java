package br.com.linkcom.sined.util.rest.android.pedidovenda;

import java.util.ArrayList;
import java.util.List;

public class PedidovendaRESTWSBean {
	
	private Integer cdpedidovenda;
    private Integer cdcliente;
    private Integer cdcontato;
    private Integer cdpedidovendatipo;
    private Integer cdprojeto;
    private String observacoes;
    private Integer cdresponsavelfrete;
    private Integer cdtransportadora;
    private Integer cdendereco;
    private String identificador;
    private java.util.Date dtpedidovenda;
    private java.util.Date dtprazoentrega;
    private Integer cdempresa;
    private Integer cdcolaborador;
    private Double valor = 0D;
    private Double percentualdesconto;
    private Double desconto;
    private Double valorusadovalecompra;
    private Double valorfrete = 0D;
    private Double valorfinal = 0D;
    private Double saldofinal = 0D;
	private List<PedidovendamaterialRESTWSBean> listaPedidovendamaterialRESTWSBean = new ArrayList<PedidovendamaterialRESTWSBean>();
    private Integer cdprazopagamento;
    private Integer cddocumentotipo;
    private Integer cdconta;
    private Integer cdindicecorrecao;
	private List<PedidovendapagamentoRESTWSBean> listaPedidovendapagamentoRESTWSBean = new ArrayList<PedidovendapagamentoRESTWSBean>();
    private Integer pedidovendasituacao;
    private Long cdusuarioaltera;
    private String protocolo;
    private Integer cdlocalarmazenagem;
    private Boolean emitirnotacoleta;
	
    public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdcontato() {
		return cdcontato;
	}
	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public Integer getCdresponsavelfrete() {
		return cdresponsavelfrete;
	}
	public Integer getCdtransportadora() {
		return cdtransportadora;
	}
	public Integer getCdendereco() {
		return cdendereco;
	}
	public String getIdentificador() {
		return identificador;
	}
	public java.util.Date getDtpedidovenda() {
		return dtpedidovenda;
	}
	public java.util.Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	public Double getValor() {
		return valor;
	}
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public Double getDesconto() {
		return desconto;
	}
	public Double getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public Double getValorfrete() {
		return valorfrete;
	}
	public Double getValorfinal() {
		return valorfinal;
	}
	public Double getSaldofinal() {
		return saldofinal;
	}
	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public Integer getCdindicecorrecao() {
		return cdindicecorrecao;
	}
	public Integer getPedidovendasituacao() {
		return pedidovendasituacao;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdcontato(Integer cdcontato) {
		this.cdcontato = cdcontato;
	}
	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setCdresponsavelfrete(Integer cdresponsavelfrete) {
		this.cdresponsavelfrete = cdresponsavelfrete;
	}
	public void setCdtransportadora(Integer cdtransportadora) {
		this.cdtransportadora = cdtransportadora;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDtpedidovenda(java.util.Date dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	public void setDtprazoentrega(java.util.Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setValorusadovalecompra(Double valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	public void setValorfrete(Double valorfrete) {
		this.valorfrete = valorfrete;
	}
	public void setValorfinal(Double valorfinal) {
		this.valorfinal = valorfinal;
	}
	public void setSaldofinal(Double saldofinal) {
		this.saldofinal = saldofinal;
	}
	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCdindicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}
	public void setPedidovendasituacao(Integer pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	public List<PedidovendamaterialRESTWSBean> getListaPedidovendamaterialRESTWSBean() {
		return listaPedidovendamaterialRESTWSBean;
	}
	public List<PedidovendapagamentoRESTWSBean> getListaPedidovendapagamentoRESTWSBean() {
		return listaPedidovendapagamentoRESTWSBean;
	}
	public void setListaPedidovendamaterialRESTWSBean(
			List<PedidovendamaterialRESTWSBean> listaPedidovendamaterialRESTWSBean) {
		this.listaPedidovendamaterialRESTWSBean = listaPedidovendamaterialRESTWSBean;
	}
	public void setListaPedidovendapagamentoRESTWSBean(
			List<PedidovendapagamentoRESTWSBean> listaPedidovendapagamentoRESTWSBean) {
		this.listaPedidovendapagamentoRESTWSBean = listaPedidovendapagamentoRESTWSBean;
	}
	public Long getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public String getProtocolo() {
		return protocolo;
	}
	public void setCdusuarioaltera(Long cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public Boolean getEmitirnotacoleta() {
		return emitirnotacoleta;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setEmitirnotacoleta(Boolean emitirnotacoleta) {
		this.emitirnotacoleta = emitirnotacoleta;
	}
}
