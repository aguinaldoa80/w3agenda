package br.com.linkcom.sined.util.rest.android.pedidovenda;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Pedidovenda")
public class PedidovendaRESTWS implements SimpleRESTWS<PedidovendaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PedidovendaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PedidovendaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		modelAndStatus.setModel(PedidovendaService .getInstance().salvarPedidovendaAndroid(command));
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(PedidovendaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PedidovendaRESTModel> listaPedidovenda = new ArrayList<PedidovendaRESTModel>();
		modelAndStatus.setModel(listaPedidovenda);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PedidovendaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
