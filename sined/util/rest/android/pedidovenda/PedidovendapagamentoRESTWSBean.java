package br.com.linkcom.sined.util.rest.android.pedidovenda;

import java.util.Date;

public class PedidovendapagamentoRESTWSBean {
	
	private Long idpedidovendapagamento;
    private Integer cdpedidovendapagamento;
    private Long idpedidovenda;
    private Integer cdpedidovenda;
    private Integer numeroparcela;
    private Integer cddocumentotipo;
    private Date dataparcela;
    private Double valororiginal;
    private String banco;
    private String agencia;
    private String conta;
    private String numero;

    public Long getIdpedidovendapagamento() {
        return idpedidovendapagamento;
    }

    public void setIdpedidovendapagamento(Long idpedidovendapagamento) {
        this.idpedidovendapagamento = idpedidovendapagamento;
    }

    public Integer getCdpedidovendapagamento() {
        return cdpedidovendapagamento;
    }

    public void setCdpedidovendapagamento(Integer cdpedidovendapagamento) {
        this.cdpedidovendapagamento = cdpedidovendapagamento;
    }

    public Long getIdpedidovenda() {
        return idpedidovenda;
    }

    public void setIdpedidovenda(Long idpedidovenda) {
        this.idpedidovenda = idpedidovenda;
    }

    public Integer getCdpedidovenda() {
        return cdpedidovenda;
    }

    public void setCdpedidovenda(Integer cdpedidovenda) {
        this.cdpedidovenda = cdpedidovenda;
    }

    public Integer getNumeroparcela() {
        return numeroparcela;
    }

    public void setNumeroparcela(Integer numeroparcela) {
        this.numeroparcela = numeroparcela;
    }

    public Integer getCddocumentotipo() {
        return cddocumentotipo;
    }

    public void setCddocumentotipo(Integer cddocumentotipo) {
        this.cddocumentotipo = cddocumentotipo;
    }

    public Date getDataparcela() {
        return dataparcela;
    }

    public void setDataparcela(Date dataparcela) {
        this.dataparcela = dataparcela;
    }

    public Double getValororiginal() {
        return valororiginal;
    }

    public void setValororiginal(Double valororiginal) {
        this.valororiginal = valororiginal;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
