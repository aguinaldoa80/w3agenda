package br.com.linkcom.sined.util.rest.android.pedidovenda;

import java.sql.Date;

import br.com.linkcom.sined.util.rest.android.pneu.PneuRESTWSBean;

public class PedidovendamaterialRESTWSBean {
	
	private Integer cdpedidovendamaterial;
    private Integer cdpedidovenda;
    private Integer cdmaterial;
    private Integer cdunidademedida;
    private Double quantidade = 0D;
    private Double preco = 0D;
    private Double desconto = 0D;
    private Double saldo = 0D;
    private Date dtprazoentrega;
    private String observacoes;
    private Double qtdereferencia;
    private Double fatorconversao;
    private Double valorcustomaterial;
    private Double valorvendamaterial;
    private Integer cdmaterialcoleta;
    private PneuRESTWSBean pneuRESTWSBean;
	
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}

	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Double getPreco() {
		return preco;
	}

	public Double getDesconto() {
		return desconto;
	}

	public Double getSaldo() {
		return saldo;
	}

	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public Double getFatorconversao() {
		return fatorconversao;
	}

	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}

	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}

	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}

	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}

	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	public PneuRESTWSBean getPneuRESTWSBean() {
		return pneuRESTWSBean;
	}

	public void setPneuRESTWSBean(PneuRESTWSBean pneuRESTWSBean) {
		this.pneuRESTWSBean = pneuRESTWSBean;
	}

	public Integer getCdmaterialcoleta() {
		return cdmaterialcoleta;
	}

	public void setCdmaterialcoleta(Integer cdmaterialcoleta) {
		this.cdmaterialcoleta = cdmaterialcoleta;
	}
}
