package br.com.linkcom.sined.util.rest.android.pedidovenda;

import br.com.linkcom.sined.geral.bean.Pedidovenda;

public class PedidovendaRESTModel {

    private Integer cdpedidovenda;
    private Integer cdcliente;
    private Double valor; 
    
    private String erroSincronizacao;

    public PedidovendaRESTModel(){}
    
    public PedidovendaRESTModel(Pedidovenda bean){
    	this.cdpedidovenda = bean.getCdpedidovenda();
    	this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
    	this.valor = bean.getValor();
    }

	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public Double getValor() {
		return valor;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getErroSincronizacao() {
		return erroSincronizacao;
	}

	public void setErroSincronizacao(String erroSincronizacao) {
		this.erroSincronizacao = erroSincronizacao;
	}
}
