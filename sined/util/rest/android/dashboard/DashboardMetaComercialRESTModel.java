package br.com.linkcom.sined.util.rest.android.dashboard;

public class DashboardMetaComercialRESTModel {
	
	private Double metaComercialTotal;
	private Double metaComercialAtingida;
	
	public Double getMetaComercialTotal() {
		return metaComercialTotal;
	}
	public Double getMetaComercialAtingida() {
		return metaComercialAtingida;
	}
	public void setMetaComercialTotal(Double metaComercialTotal) {
		this.metaComercialTotal = metaComercialTotal;
	}
	public void setMetaComercialAtingida(Double metaComercialAtingida) {
		this.metaComercialAtingida = metaComercialAtingida;
	}
	
}
