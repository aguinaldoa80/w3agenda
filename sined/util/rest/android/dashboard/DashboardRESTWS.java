package br.com.linkcom.sined.util.rest.android.dashboard;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.MetacomercialService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Dashboard")
public class DashboardRESTWS implements SimpleRESTWS<DashboardRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(DashboardRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DashboardRESTWSBean command) {
		DashboardRESTModel model = new DashboardRESTModel();
		
		DashboardMetaComercialRESTModel metaComercial = MetacomercialService.getInstance().getDashboardMetaComercial(command);
		
		model.setPedidosAguardandoAprovacao(PedidovendaService.getInstance().getPedidosAguardandoAprovacaoForAndroid(command.getCdusuario()));
		model.setPedidosPrevistos(PedidovendaService.getInstance().getPedidosPrevistosForAndroid(command.getCdusuario()));
		model.setPedidosConfirmados(PedidovendaService.getInstance().getPedidosConfirmadorForAndroid(command.getCdusuario()));
		model.setNovosClientes(ClienteService.getInstance().getNovosClientesForAndroid(command.getCdusuario()));
		model.setMetaComercialTotal(metaComercial != null ? metaComercial.getMetaComercialTotal() : null);
		model.setMetaComercialAtingida(metaComercial != null ? metaComercial.getMetaComercialAtingida() : null);
		model.setTicketMedio(VendaService.getInstance().getTicketMedioForAndroid(command.getCdusuario()));
		model.setEvolucaoVenda(VendaService.getInstance().getEvolucaoVendaForAndroid(command));
		
		return new ModelAndStatus(model);
	}

	@Override
	public ModelAndStatus get(DashboardRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(DashboardRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
