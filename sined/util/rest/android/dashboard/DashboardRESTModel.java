package br.com.linkcom.sined.util.rest.android.dashboard;

public class DashboardRESTModel {
	
	private Double metaComercialTotal;
	private Double metaComercialAtingida;
	private Integer pedidosAguardandoAprovacao;
	private Integer pedidosPrevistos;
	private Integer pedidosConfirmados;
	private Integer novosClientes;
	private Double ticketMedio;
	private DashboardEvolucaoVendaRESTModel[] evolucaoVenda;
	
	public Double getMetaComercialTotal() {
		return metaComercialTotal;
	}
	public Double getMetaComercialAtingida() {
		return metaComercialAtingida;
	}
	public Integer getPedidosAguardandoAprovacao() {
		return pedidosAguardandoAprovacao;
	}
	public Integer getPedidosPrevistos() {
		return pedidosPrevistos;
	}
	public Integer getPedidosConfirmados() {
		return pedidosConfirmados;
	}
	public Integer getNovosClientes() {
		return novosClientes;
	}
	public Double getTicketMedio() {
		return ticketMedio;
	}
	public DashboardEvolucaoVendaRESTModel[] getEvolucaoVenda() {
		return evolucaoVenda;
	}
	public void setMetaComercialTotal(Double metaComercialTotal) {
		this.metaComercialTotal = metaComercialTotal;
	}
	public void setMetaComercialAtingida(Double metaComercialAtingida) {
		this.metaComercialAtingida = metaComercialAtingida;
	}
	public void setPedidosAguardandoAprovacao(Integer pedidosAguardandoAprovacao) {
		this.pedidosAguardandoAprovacao = pedidosAguardandoAprovacao;
	}
	public void setPedidosPrevistos(Integer pedidosPrevistos) {
		this.pedidosPrevistos = pedidosPrevistos;
	}
	public void setPedidosConfirmados(Integer pedidosConfirmados) {
		this.pedidosConfirmados = pedidosConfirmados;
	}
	public void setNovosClientes(Integer novosClientes) {
		this.novosClientes = novosClientes;
	}
	public void setTicketMedio(Double ticketMedio) {
		this.ticketMedio = ticketMedio;
	}
	public void setEvolucaoVenda(DashboardEvolucaoVendaRESTModel[] evolucaoVenda) {
		this.evolucaoVenda = evolucaoVenda;
	}
	
}
