package br.com.linkcom.sined.util.rest.android.dashboard;

public class DashboardRESTWSBean {

	private Integer cdusuario;
	private Integer cdempresa;
	
	public Integer getCdusuario() {
		return cdusuario;
	}
	
	public Integer getCdempresa() {
		return cdempresa;
	}
	
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
	
}
