package br.com.linkcom.sined.util.rest.android.dashboard;

public class DashboardEvolucaoVendaRESTModel {
	
	private Long data;
	private Double valor;
	
	public Long getData() {
		return data;
	}
	public Double getValor() {
		return valor;
	}
	public void setData(Long data) {
		this.data = data;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
