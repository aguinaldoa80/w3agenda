package br.com.linkcom.sined.util.rest.android.materialtabelapreco;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialtabelapreco")
public class MaterialtabelaprecoRESTWS implements SimpleRESTWS<MaterialtabelaprecoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialtabelaprecoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialtabelaprecoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialtabelaprecoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialtabelaprecoRESTModel> listaMaterialtabelapreco = MaterialtabelaprecoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialtabelapreco);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialtabelaprecoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
