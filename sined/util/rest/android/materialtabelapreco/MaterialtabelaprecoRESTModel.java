package br.com.linkcom.sined.util.rest.android.materialtabelapreco;

import br.com.linkcom.sined.geral.bean.Materialtabelapreco;

public class MaterialtabelaprecoRESTModel {

    private Integer cdmaterialtabelapreco;
    private String nome;
    private Integer cdcategoria;
    private Integer cdfrequencia;
    private Integer cdmaterialgrupo;
    private Integer cdpedidovendatipo;
    private Integer cdprazopagamento;
    private Double taxaacrescimo;
    private Double descontopadrao;
    private Double acrescimopadrao;
    private Long datafim;
    private Long datainicio;
    private Boolean discriminarDescontoVenda;
    private String tipoCalculo;
    
    public MaterialtabelaprecoRESTModel(){}
    
    public MaterialtabelaprecoRESTModel(Materialtabelapreco bean){
    	this.cdmaterialtabelapreco = bean.getCdmaterialtabelapreco();
    	this.nome = bean.getNome();
    	this.cdcategoria = bean.getCategoria() != null ? bean.getCategoria().getCdcategoria() : null;
        this.cdfrequencia = bean.getFrequencia() != null ? bean.getFrequencia().getCdfrequencia() : null;
        this.cdmaterialgrupo = bean.getMaterialgrupo() != null ? bean.getMaterialgrupo().getCdmaterialgrupo() : null;
        this.cdpedidovendatipo = bean.getPedidovendatipo() != null ? bean.getPedidovendatipo().getCdpedidovendatipo() : null;
        this.cdprazopagamento = bean.getPrazopagamento() != null ? bean.getPrazopagamento().getCdprazopagamento() : null;
        this.taxaacrescimo = bean.getTaxaacrescimo() != null ? bean.getTaxaacrescimo().getValue().doubleValue() : null;
        this.acrescimopadrao = bean.getAcrescimopadrao();
        this.descontopadrao = bean.getDescontopadrao();
        this.datafim = bean.getDtfim() != null ? bean.getDtfim().getTime() : null;
        this.datainicio = bean.getDtinicio() != null ? bean.getDtinicio().getTime() : null;
        this.discriminarDescontoVenda = bean.getDiscriminardescontovenda();
        this.tipoCalculo = bean.getTabelaprecotipo() != null ? bean.getTabelaprecotipo().getDescricao() : null;
        
    }

	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}

	public String getNome() {
		return nome;
	}

	public Integer getCdcategoria() {
		return cdcategoria;
	}

	public Integer getCdfrequencia() {
		return cdfrequencia;
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}

	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}

	public Double getTaxaacrescimo() {
		return taxaacrescimo;
	}

	public Double getDescontopadrao() {
		return descontopadrao;
	}

	public Double getAcrescimopadrao() {
		return acrescimopadrao;
	}

	public Boolean getDiscriminarDescontoVenda() {
		return discriminarDescontoVenda;
	}
	
	public String getTipoCalculo() {
		return tipoCalculo;
	}
	
	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}
	
	public void setDiscriminarDescontoVenda(Boolean discriminarDescontoVenda) {
		this.discriminarDescontoVenda = discriminarDescontoVenda;
	}
	
	public void setDescontopadrao(Double descontopadrao) {
		this.descontopadrao = descontopadrao;
	}

	public void setAcrescimopadrao(Double acrescimopadrao) {
		this.acrescimopadrao = acrescimopadrao;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}

	public void setCdfrequencia(Integer cdfrequencia) {
		this.cdfrequencia = cdfrequencia;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}

	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}

	public void setTaxaacrescimo(Double taxaacrescimo) {
		this.taxaacrescimo = taxaacrescimo;
	}

	public Long getDatafim() {
		return datafim;
	}

	public Long getDatainicio() {
		return datainicio;
	}

	public void setDatafim(Long datafim) {
		this.datafim = datafim;
	}

	public void setDatainicio(Long datainicio) {
		this.datainicio = datainicio;
	}
}
