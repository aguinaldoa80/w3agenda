package br.com.linkcom.sined.util.rest.android.usuario;

import br.com.linkcom.sined.geral.bean.Usuario;

public class UsuarioRESTModel {
	
	private Integer cdusuario;
    private String login;
    private String nome;
    private boolean restricaoclientevendedor;
    private boolean restricaovendavendedor;
    
    public UsuarioRESTModel(){}
    
    public UsuarioRESTModel(Usuario bean){
		this.cdusuario = bean.getCdpessoa();
		this.login = bean.getLogin();
		this.nome = bean.getNome();
		this.restricaoclientevendedor = bean.getRestricaoclientevendedor() != null ? bean.getRestricaoclientevendedor() : false;
		this.restricaovendavendedor = bean.getRestricaovendavendedor() != null ? bean.getRestricaovendavendedor() : false;
	}

	public Integer getCdusuario() {
		return cdusuario;
	}

	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}

	public String getLogin() {
		return login;
	}

	public String getNome() {
		return nome;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isRestricaoclientevendedor() {
		return restricaoclientevendedor;
	}

	public boolean isRestricaovendavendedor() {
		return restricaovendavendedor;
	}

	public void setRestricaoclientevendedor(boolean restricaoclientevendedor) {
		this.restricaoclientevendedor = restricaoclientevendedor;
	}

	public void setRestricaovendavendedor(boolean restricaovendavendedor) {
		this.restricaovendavendedor = restricaovendavendedor;
	}
}
