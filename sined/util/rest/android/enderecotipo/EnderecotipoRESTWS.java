package br.com.linkcom.sined.util.rest.android.enderecotipo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.EnderecotipoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Enderecotipo")
public class EnderecotipoRESTWS implements SimpleRESTWS<EnderecotipoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(EnderecotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(EnderecotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(EnderecotipoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<EnderecotipoRESTModel> listaEndereco = EnderecotipoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaEndereco);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(EnderecotipoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
