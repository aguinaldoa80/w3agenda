package br.com.linkcom.sined.util.rest.android.enderecotipo;


public class EnderecotipoRESTWSBean {

	private Integer cdenderecotipo;
	private String nome;
	
	public Integer getCdenderecotipo() {
		return cdenderecotipo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdenderecotipo(Integer cdenderecotipo) {
		this.cdenderecotipo = cdenderecotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
