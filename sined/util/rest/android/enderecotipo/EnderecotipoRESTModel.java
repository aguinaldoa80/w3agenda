package br.com.linkcom.sined.util.rest.android.enderecotipo;

import br.com.linkcom.sined.geral.bean.Enderecotipo;

public class EnderecotipoRESTModel {
	
	private Integer cdenderecotipo;
    private String nome;
    
    public EnderecotipoRESTModel(){}
    
    public EnderecotipoRESTModel(Enderecotipo bean){
		this.cdenderecotipo = bean.getCdenderecotipo();
		this.nome = bean.getNome();
	}

	public Integer getCdenderecotipo() {
		return cdenderecotipo;
	}

	public String getNome() {
		return nome;
	}

	public void setCdenderecotipo(Integer cdenderecotipo) {
		this.cdenderecotipo = cdenderecotipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
