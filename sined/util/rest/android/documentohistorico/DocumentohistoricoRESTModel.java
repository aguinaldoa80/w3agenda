package br.com.linkcom.sined.util.rest.android.documentohistorico;

import br.com.linkcom.sined.geral.bean.Documentohistorico;


public class DocumentohistoricoRESTModel {
	
	private Integer cddocumentohistorico;
	private Integer cddocumento;
	private String observacao;
	private Long dtalteracao;
	private String nomeusuario;
	
    public DocumentohistoricoRESTModel(){}
	
	public DocumentohistoricoRESTModel(Documentohistorico bean){
		this.cddocumentohistorico = bean.getCddocumentohistorico();
		this.cddocumento = bean.getDocumento() != null ? bean.getDocumento().getCddocumento() : null;
		this.observacao = bean.getObservacao();
		this.dtalteracao = bean.getDtaltera() != null ? bean.getDtaltera().getTime() : null;
		this.nomeusuario = bean.getUsuarioaltera() != null ? bean.getUsuarioaltera().getNome() : null; 
	}

	public Integer getCddocumentohistorico() {
		return cddocumentohistorico;
	}

	public Integer getCddocumento() {
		return cddocumento;
	}

	public String getObservacao() {
		return observacao;
	}

	public Long getDtalteracao() {
		return dtalteracao;
	}

	public String getNomeusuario() {
		return nomeusuario;
	}

	public void setCddocumentohistorico(Integer cddocumentohistorico) {
		this.cddocumentohistorico = cddocumentohistorico;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDtalteracao(Long dtalteracao) {
		this.dtalteracao = dtalteracao;
	}

	public void setNomeusuario(String nomeusuario) {
		this.nomeusuario = nomeusuario;
	}
}
