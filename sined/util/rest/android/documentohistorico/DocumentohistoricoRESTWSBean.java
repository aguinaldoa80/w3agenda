package br.com.linkcom.sined.util.rest.android.documentohistorico;


public class DocumentohistoricoRESTWSBean {

	private Integer cddocumentohistorico;
	private String nome;
	
	public Integer getCddocumentohistorico() {
		return cddocumentohistorico;
	}
	public String getNome() {
		return nome;
	}
	public void setCddocumentohistorico(Integer cddocumentohistorico) {
		this.cddocumentohistorico = cddocumentohistorico;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
