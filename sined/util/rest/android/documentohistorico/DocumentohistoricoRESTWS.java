package br.com.linkcom.sined.util.rest.android.documentohistorico;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Documentohistorico")
public class DocumentohistoricoRESTWS implements SimpleRESTWS<DocumentohistoricoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(DocumentohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DocumentohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(DocumentohistoricoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<DocumentohistoricoRESTModel> listaCliente = DocumentohistoricoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(DocumentohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
