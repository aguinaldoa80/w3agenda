package br.com.linkcom.sined.util.rest.android.material;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialRESTModel {
	
	private Integer cdmaterial;
    private String nome;
    protected Integer cdunidademedida;
    protected String codigobarras;
    protected String identificacao;
    protected Integer cdmaterialgrupo;
    protected Double valorvenda;
    protected Double valorcusto;
    protected Double peso;
    protected Double pesobruto;
    protected Double valorvendamaximo;
    protected Double valorvendaminimo;
    protected Boolean vendapromocional;
    protected Boolean exibiritenskitvenda;
    protected Boolean servico;
    protected Boolean producao;
    protected Integer cdmaterialcoleta;
    protected Double altura;
    protected Double largura;
	protected Double comprimento;
    protected Boolean ativo;
    protected Integer tipoitemsped;
    protected Integer cdarquivo;
    
    public MaterialRESTModel(){}
    
    public MaterialRESTModel(Material m){
		this.cdmaterial = m.getCdmaterial();
		this.nome = m.getNome();
		this.codigobarras = m.getCodigobarras();
		this.identificacao = m.getIdentificacao();
		this.valorvenda = m.getValorvenda();
		this.valorcusto = m.getValorcusto();
		this.peso = m.getPeso();
		this.pesobruto = m.getPesobruto();
		this.valorvendamaximo = m.getValorvendamaximo();
		this.valorvendaminimo = m.getValorvendaminimo();
		this.exibiritenskitvenda = m.getExibiritenskitvenda();
		this.vendapromocional = m.getVendapromocional();
		this.servico = m.getServico();
		this.producao = m.getProducao();
		this.ativo = m.getAtivo();
		this.tipoitemsped = m.getTipoitemsped() != null ? m.getTipoitemsped().ordinal() : null;
		if(m.getUnidademedida() != null)
			this.cdunidademedida = m.getUnidademedida().getCdunidademedida();
		if(m.getMaterialgrupo() != null)
			this.cdmaterialgrupo = m.getMaterialgrupo().getCdmaterialgrupo();
		if(m.getMaterialcoleta() != null)
			this.cdmaterialcoleta = m.getMaterialcoleta().getCdmaterial();
		
		if(m.getMaterialproduto() != null){
			this.altura = m.getMaterialproduto().getAltura();
			this.largura = m.getMaterialproduto().getLargura();
			this.comprimento = m.getMaterialproduto().getComprimento();
		}
		if(m.getArquivo() != null && m.getArquivo().getCdarquivo() != null){
			this.cdarquivo = m.getArquivo().getCdarquivo().intValue();
		}
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public String getNome() {
		return nome;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public Double getValorvenda() {
		return valorvenda;
	}

	public Double getPeso() {
		return peso;
	}

	public Double getPesobruto() {
		return pesobruto;
	}

	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}

	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	
	public Boolean getExibiritenskitvenda() {
		return exibiritenskitvenda;
	}
	
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	
	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	
	public void setExibiritenskitvenda(Boolean exibiritenskitvenda) {
		this.exibiritenskitvenda = exibiritenskitvenda;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public Integer getCdarquivo() {
		return cdarquivo;
	}

	public void setCdarquivo(Integer cdarquivo) {
		this.cdarquivo = cdarquivo;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}

	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}

	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}

	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Double getValorcusto() {
		return valorcusto;
	}

	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}

	public Boolean getProducao() {
		return producao;
	}
	public Boolean getServico() {
		return servico;
	}

	public void setProducao(Boolean producao) {
		this.producao = producao;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}

	public Integer getCdmaterialcoleta() {
		return cdmaterialcoleta;
	}

	public void setCdmaterialcoleta(Integer cdmaterialcoleta) {
		this.cdmaterialcoleta = cdmaterialcoleta;
	}

	public Double getAltura() {
		return altura;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getComprimento() {
		return comprimento;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public Integer getTipoitemsped() {
		return tipoitemsped;
	}

	public void setTipoitemsped(Integer tipoitemsped) {
		this.tipoitemsped = tipoitemsped;
	}
}
