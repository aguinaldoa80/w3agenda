package br.com.linkcom.sined.util.rest.android.material;

import java.sql.Timestamp;


public class MaterialRESTWSBean {

	private Integer cdmaterial;
	private String nome;
	private Timestamp dtUltimaSincronizacao;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getNome() {
		return nome;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Timestamp getDtUltimaSincronizacao() {
		return dtUltimaSincronizacao;
	}
	public void setDtUltimaSincronizacao(Long dtUltimaSincronizacao) {
		this.dtUltimaSincronizacao = new Timestamp(dtUltimaSincronizacao);
	}
	
}
