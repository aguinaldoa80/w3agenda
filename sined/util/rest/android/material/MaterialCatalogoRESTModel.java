package br.com.linkcom.sined.util.rest.android.material;

import java.sql.Timestamp;
import java.util.List;

public class MaterialCatalogoRESTModel {

	private Timestamp dtUltimaSincronizacao;
	private List<MaterialRESTModel> listaMaterialRest;
	
	
	public MaterialCatalogoRESTModel() {

	}

	public MaterialCatalogoRESTModel(Timestamp dtUltimaSincronizacao,
			List<MaterialRESTModel> listaMaterialRest) {
	
		this.dtUltimaSincronizacao = dtUltimaSincronizacao;
		this.listaMaterialRest = listaMaterialRest;
	}
	
	public Timestamp getDtUltimaSincronizacao() {
		return dtUltimaSincronizacao;
	}
	public void setDtUltimaSincronizacao(Timestamp dtUltimaSincronizacao) {
		this.dtUltimaSincronizacao = dtUltimaSincronizacao;
	}
	public List<MaterialRESTModel> getListaMaterialRest() {
		return listaMaterialRest;
	}
	public void setListaMaterialRest(List<MaterialRESTModel> listaMaterialRest) {
		this.listaMaterialRest = listaMaterialRest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((dtUltimaSincronizacao == null) ? 0 : dtUltimaSincronizacao
						.hashCode());
		result = prime
				* result
				+ ((listaMaterialRest == null) ? 0 : listaMaterialRest
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaterialCatalogoRESTModel other = (MaterialCatalogoRESTModel) obj;
		if (dtUltimaSincronizacao == null) {
			if (other.dtUltimaSincronizacao != null)
				return false;
		} else if (!dtUltimaSincronizacao.equals(other.dtUltimaSincronizacao))
			return false;
		if (listaMaterialRest == null) {
			if (other.listaMaterialRest != null)
				return false;
		} else if (!listaMaterialRest.equals(other.listaMaterialRest))
			return false;
		return true;
	}
	
	
	
}
