package br.com.linkcom.sined.util.rest.android.material;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Material")
public class MaterialRESTWS implements SimpleRESTWS<MaterialRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialRESTWSBean command) {
		System.out.println("--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"-------------------------------------------- \n");
		System.out.println(command);
		System.out.println("--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"--------------------------------------------\n" +
				"-------------------------------------------- \n");
		System.out.println(command);
		
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialRESTModel> listaMaterial = null;

		if(command.getDtUltimaSincronizacao() != null){
			MaterialCatalogoRESTModel materialCatalogo = new MaterialCatalogoRESTModel();
			materialCatalogo.setDtUltimaSincronizacao(new Timestamp(System.currentTimeMillis()));
			materialCatalogo.setListaMaterialRest(MaterialService.getInstance().findForAndroid(null, true, command.getDtUltimaSincronizacao()));
			modelAndStatus.setModel(materialCatalogo);
		}else{
			listaMaterial = MaterialService.getInstance().findForAndroid();
			modelAndStatus.setModel(listaMaterial);
		}
		
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
