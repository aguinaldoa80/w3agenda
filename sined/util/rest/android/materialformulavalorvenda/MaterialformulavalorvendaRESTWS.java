package br.com.linkcom.sined.util.rest.android.materialformulavalorvenda;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialformulavalorvenda")
public class MaterialformulavalorvendaRESTWS implements SimpleRESTWS<MaterialformulavalorvendaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialformulavalorvendaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialformulavalorvendaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialformulavalorvendaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialformulavalorvendaRESTModel> listaMaterialformulavalorvenda = MaterialformulavalorvendaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialformulavalorvenda);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialformulavalorvendaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
