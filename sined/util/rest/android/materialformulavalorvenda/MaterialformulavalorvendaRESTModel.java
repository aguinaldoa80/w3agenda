package br.com.linkcom.sined.util.rest.android.materialformulavalorvenda;

import br.com.linkcom.sined.geral.bean.Materialformulavalorvenda;

public class MaterialformulavalorvendaRESTModel {

	protected Integer cdmaterialformulavalorvenda;
	protected Integer cdmaterial;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

    public MaterialformulavalorvendaRESTModel(){}
    
    public MaterialformulavalorvendaRESTModel(Materialformulavalorvenda bean){
    	this.cdmaterialformulavalorvenda = bean.getCdmaterialformulavalorvenda();
    	this.cdmaterial = bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null;
    	this.ordem = bean.getOrdem();
        this.identificador = bean.getIdentificador();
        this.formula = bean.getFormula();
    }

	public Integer getCdmaterialformulavalorvenda() {
		return cdmaterialformulavalorvenda;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getFormula() {
		return formula;
	}

	public void setCdmaterialformulavalorvenda(Integer cdmaterialformulavalorvenda) {
		this.cdmaterialformulavalorvenda = cdmaterialformulavalorvenda;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
}
