package br.com.linkcom.sined.util.rest.android.materialrelacionado;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialrelacionado")
public class MaterialrelacionadoRESTWS implements SimpleRESTWS<MaterialrelacionadoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialrelacionadoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialrelacionadoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialrelacionadoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialrelacionadoRESTModel> listaMaterialrelacionado = MaterialrelacionadoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialrelacionado);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialrelacionadoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
