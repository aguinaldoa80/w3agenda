package br.com.linkcom.sined.util.rest.android.materialrelacionado;

import br.com.linkcom.sined.geral.bean.Materialrelacionado;

public class MaterialrelacionadoRESTModel {

    private Integer cdmaterialrelacionado;
    private Integer cdmaterial;
    private Integer cdmaterialpromocao;
    private Integer ordemexibicao;
    private Double quantidade;
    private Double valorvenda;

    public MaterialrelacionadoRESTModel(){}
    
    public MaterialrelacionadoRESTModel(Materialrelacionado bean){
    	this.cdmaterialrelacionado = bean.getCdmaterialrelacionado();
    	this.cdmaterial = bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null;
    	this.cdmaterialpromocao = bean.getMaterialpromocao() != null ? bean.getMaterialpromocao().getCdmaterial() : null;
    	this.ordemexibicao = bean.getOrdemexibicao();
    	this.quantidade = bean.getQuantidade();
    	this.valorvenda = bean.getValorvenda() != null ? bean.getValorvenda().getValue().doubleValue() : null;
    }

	public Integer getCdmaterialrelacionado() {
		return cdmaterialrelacionado;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdmaterialpromocao() {
		return cdmaterialpromocao;
	}

	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Double getValorvenda() {
		return valorvenda;
	}

	public void setCdmaterialrelacionado(Integer cdmaterialrelacionado) {
		this.cdmaterialrelacionado = cdmaterialrelacionado;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdmaterialpromocao(Integer cdmaterialpromocao) {
		this.cdmaterialpromocao = cdmaterialpromocao;
	}

	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
}
