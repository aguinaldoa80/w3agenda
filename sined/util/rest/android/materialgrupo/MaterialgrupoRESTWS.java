package br.com.linkcom.sined.util.rest.android.materialgrupo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialgrupo")
public class MaterialgrupoRESTWS implements SimpleRESTWS<MaterialgrupoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialgrupoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialgrupoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialgrupoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialgrupoRESTModel> listaMaterialgrupo = MaterialgrupoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialgrupo);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialgrupoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
