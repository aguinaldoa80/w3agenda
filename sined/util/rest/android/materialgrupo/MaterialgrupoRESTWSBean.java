package br.com.linkcom.sined.util.rest.android.materialgrupo;


public class MaterialgrupoRESTWSBean {

	private Integer cdmaterialgrupo;
	private String nome;
	
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
