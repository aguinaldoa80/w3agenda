package br.com.linkcom.sined.util.rest.android.materialgrupo;

import br.com.linkcom.sined.geral.bean.Materialgrupo;

public class MaterialgrupoRESTModel {
	
	private Integer cdmaterialgrupo;
    private String nome;
    
    public MaterialgrupoRESTModel(){}
    
    public MaterialgrupoRESTModel(Materialgrupo um){
		this.cdmaterialgrupo = um.getCdmaterialgrupo();
		this.nome = um.getNome();
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public String getNome() {
		return nome;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
