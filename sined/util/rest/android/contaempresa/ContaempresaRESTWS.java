package br.com.linkcom.sined.util.rest.android.contaempresa;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ContaempresaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Contaempresa")
public class ContaempresaRESTWS implements SimpleRESTWS<ContaempresaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ContaempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ContaempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ContaempresaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ContaempresaRESTModel> listaCliente = ContaempresaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ContaempresaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
