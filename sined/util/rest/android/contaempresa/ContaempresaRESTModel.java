package br.com.linkcom.sined.util.rest.android.contaempresa;

import br.com.linkcom.sined.geral.bean.Contaempresa;

public class ContaempresaRESTModel {
	
	private Integer cdcontaempresa;
	private Integer cdconta;
	private Integer cdempresa;
	
    public ContaempresaRESTModel(){}
	
	public ContaempresaRESTModel(Contaempresa bean){
		this.cdcontaempresa = bean.getCdcontaempresa();
		this.cdconta = bean.getConta() != null ? bean.getConta().getCdconta() : null;
		this.cdempresa = bean.getEmpresa() != null ? bean.getEmpresa().getCdpessoa() : null;
	}

	public Integer getCdcontaempresa() {
		return cdcontaempresa;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}

	public void setCdcontaempresa(Integer cdcontaempresa) {
		this.cdcontaempresa = cdcontaempresa;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
}
