package br.com.linkcom.sined.util.rest.android.contaempresa;


public class ContaempresaRESTWSBean {

	private Integer cdcontaempresa;
	private Integer cdconta;
	private Integer cdempresa;
	
	public Integer getCdcontaempresa() {
		return cdcontaempresa;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	
	public void setCdcontaempresa(Integer cdcontaempresa) {
		this.cdcontaempresa = cdcontaempresa;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
}
