package br.com.linkcom.sined.util.rest.android.pneumarca;


public class PneumarcaRESTWSBean {

	private Integer cdpneumarca;
	private String nome;
	
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
