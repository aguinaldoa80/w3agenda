package br.com.linkcom.sined.util.rest.android.pneumarca;

import br.com.linkcom.sined.geral.bean.Pneumarca;

public class PneumarcaRESTModel {
	
	private Integer cdpneumarca;
	private String nome;
	
    public PneumarcaRESTModel(){}
	
	public PneumarcaRESTModel(Pneumarca bean){
		this.cdpneumarca = bean.getCdpneumarca();
		this.nome = bean.getNome();
	}
	
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
