package br.com.linkcom.sined.util.rest.android.colaborador;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Colaborador")
public class ColaboradorRESTWS implements SimpleRESTWS<ColaboradorRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ColaboradorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ColaboradorRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ColaboradorRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ColaboradorRESTModel> listaConta = ColaboradorService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaConta);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ColaboradorRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
