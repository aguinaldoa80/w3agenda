package br.com.linkcom.sined.util.rest.android.colaborador;

import br.com.linkcom.sined.geral.bean.Colaborador;

public class ColaboradorRESTModel {

    private Integer cdpessoa;
    private String nome;
    private String telefones;

    public ColaboradorRESTModel(){}
    
    public ColaboradorRESTModel(Colaborador bean){
    	this.cdpessoa = bean.getCdpessoa();
    	this.nome = bean.getNome();
    	this.telefones = bean.getTelefonesComTipoComQuebraLinha();
    }

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefones() {
		return telefones;
	}

	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
}
