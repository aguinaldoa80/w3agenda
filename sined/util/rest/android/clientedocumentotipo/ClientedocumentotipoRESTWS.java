package br.com.linkcom.sined.util.rest.android.clientedocumentotipo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClienteDocumentoTipoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Clientedocumentotipo")
public class ClientedocumentotipoRESTWS implements SimpleRESTWS<ClientedocumentotipoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ClientedocumentotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ClientedocumentotipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ClientedocumentotipoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ClientedocumentotipoRESTModel> listaCliente = ClienteDocumentoTipoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ClientedocumentotipoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
