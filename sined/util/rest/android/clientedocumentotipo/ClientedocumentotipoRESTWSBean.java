package br.com.linkcom.sined.util.rest.android.clientedocumentotipo;


public class ClientedocumentotipoRESTWSBean {

	private Integer cdclientedocumentotipo;
	private String nome;
	
	public Integer getCdclientedocumentotipo() {
		return cdclientedocumentotipo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdclientedocumentotipo(Integer cdclientedocumentotipo) {
		this.cdclientedocumentotipo = cdclientedocumentotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
