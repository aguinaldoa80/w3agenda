package br.com.linkcom.sined.util.rest.android.clientedocumentotipo;

import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;


public class ClientedocumentotipoRESTModel {
	
	private Integer cdclientedocumentotipo;
	private Integer cddocumentotipo;
	private Integer cdcliente;
	
    public ClientedocumentotipoRESTModel(){}
	
	public ClientedocumentotipoRESTModel(ClienteDocumentoTipo bean){
		this.cdclientedocumentotipo = bean.getCdclientedocumentotipo();
		this.cddocumentotipo = bean.getDocumentotipo() != null ? bean.getDocumentotipo().getCddocumentotipo() : null;
		this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
	}

	public Integer getCdclientedocumentotipo() {
		return cdclientedocumentotipo;
	}

	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public void setCdclientedocumentotipo(Integer cdclientedocumentotipo) {
		this.cdclientedocumentotipo = cdclientedocumentotipo;
	}

	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
}
