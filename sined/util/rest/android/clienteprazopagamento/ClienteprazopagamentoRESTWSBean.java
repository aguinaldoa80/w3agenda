package br.com.linkcom.sined.util.rest.android.clienteprazopagamento;


public class ClienteprazopagamentoRESTWSBean {

	private Integer cdclienteprazopagamento;
	private String nome;
	
	public Integer getCdclienteprazopagamento() {
		return cdclienteprazopagamento;
	}
	public String getNome() {
		return nome;
	}
	public void setCdclienteprazopagamento(Integer cdclienteprazopagamento) {
		this.cdclienteprazopagamento = cdclienteprazopagamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
