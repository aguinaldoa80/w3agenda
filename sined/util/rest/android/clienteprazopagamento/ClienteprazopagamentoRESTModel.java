package br.com.linkcom.sined.util.rest.android.clienteprazopagamento;

import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;


public class ClienteprazopagamentoRESTModel {
	
	private Integer cdclienteprazopagamento;
	private Integer cdcliente;
	private Integer cdprazopagamento;
	
    public ClienteprazopagamentoRESTModel(){}
	
	public ClienteprazopagamentoRESTModel(ClientePrazoPagamento bean){
		this.cdclienteprazopagamento = bean.getCdclienteprazopagamento();
		this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
		this.cdprazopagamento = bean.getPrazopagamento() != null ? bean.getPrazopagamento().getCdprazopagamento() : null;
	}

	public Integer getCdclienteprazopagamento() {
		return cdclienteprazopagamento;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}

	public void setCdclienteprazopagamento(Integer cdclienteprazopagamento) {
		this.cdclienteprazopagamento = cdclienteprazopagamento;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
}
