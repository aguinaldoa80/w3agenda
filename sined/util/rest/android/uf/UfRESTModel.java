package br.com.linkcom.sined.util.rest.android.uf;

import br.com.linkcom.sined.geral.bean.Uf;

public class UfRESTModel {
	
	private Integer cduf;
    private String nome;
    private String sigla;
    
    public UfRESTModel(){}
    
    public UfRESTModel(Uf um){
		this.cduf = um.getCduf();
		this.nome = um.getNome();
		this.sigla = um.getSigla();
	}

	public Integer getCduf() {
		return cduf;
	}

	public String getNome() {
		return nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
