package br.com.linkcom.sined.util.rest.android.uf;


public class UfRESTWSBean {

	private Integer cduf;
	private String nome;
	
	public Integer getCduf() {
		return cduf;
	}
	public String getNome() {
		return nome;
	}
	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
