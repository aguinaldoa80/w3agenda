package br.com.linkcom.sined.util.rest.android.uf;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Uf")
public class UfRESTWS implements SimpleRESTWS<UfRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(UfRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(UfRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(UfRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<UfRESTModel> listauf = UfService.getInstance().findForAndroid();
		modelAndStatus.setModel(listauf);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(UfRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
