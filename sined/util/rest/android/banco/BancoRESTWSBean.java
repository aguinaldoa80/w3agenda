package br.com.linkcom.sined.util.rest.android.banco;


public class BancoRESTWSBean {

	private Integer cdbanco;
	private String nome;
	
	public Integer getCdbanco() {
		return cdbanco;
	}
	public String getNome() {
		return nome;
	}
	public void setCdbanco(Integer cdbanco) {
		this.cdbanco = cdbanco;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
