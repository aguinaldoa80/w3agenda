package br.com.linkcom.sined.util.rest.android.banco;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Banco")
public class BancoRESTWS implements SimpleRESTWS<BancoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(BancoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(BancoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(BancoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<BancoRESTModel> listaCliente = BancoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(BancoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
