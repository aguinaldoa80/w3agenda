package br.com.linkcom.sined.util.rest.android.banco;

import br.com.linkcom.sined.geral.bean.Banco;

public class BancoRESTModel {
	
	private Integer cdbanco;
	private String nome;
	
    public BancoRESTModel(){}
	
	public BancoRESTModel(Banco bean){
		this.cdbanco = bean.getCdbanco();
		this.nome = bean.getNome();
	}
	
	public Integer getCdbanco() {
		return cdbanco;
	}
	public String getNome() {
		return nome;
	}
	public void setCdbanco(Integer cdbanco) {
		this.cdbanco = cdbanco;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
