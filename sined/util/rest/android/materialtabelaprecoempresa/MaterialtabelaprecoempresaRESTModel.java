package br.com.linkcom.sined.util.rest.android.materialtabelaprecoempresa;

import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;

public class MaterialtabelaprecoempresaRESTModel {

    private Integer cdmaterialtabelaprecoempresa;
    private Integer cdmaterialtabelapreco;
    private Integer cdempresa;

    public MaterialtabelaprecoempresaRESTModel(){}
    
    public MaterialtabelaprecoempresaRESTModel(Materialtabelaprecoempresa bean){
    	this.cdmaterialtabelaprecoempresa = bean.getCdmaterialtabelaprecoempresa();
    	this.cdmaterialtabelapreco = bean.getMaterialtabelapreco() != null ? 
    			bean.getMaterialtabelapreco().getCdmaterialtabelapreco() : null;
    	this.cdempresa = bean.getEmpresa() != null ? bean.getEmpresa().getCdpessoa() : null;
    }

	public Integer getCdmaterialtabelaprecoempresa() {
		return cdmaterialtabelaprecoempresa;
	}

	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}

	public Integer getCdempresa() {
		return cdempresa;
	}

	public void setCdmaterialtabelaprecoempresa(Integer cdmaterialtabelaprecoempresa) {
		this.cdmaterialtabelaprecoempresa = cdmaterialtabelaprecoempresa;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}

	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
}
