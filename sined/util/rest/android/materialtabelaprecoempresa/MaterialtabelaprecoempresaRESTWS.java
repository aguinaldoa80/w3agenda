package br.com.linkcom.sined.util.rest.android.materialtabelaprecoempresa;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoempresaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialtabelaprecoempresa")
public class MaterialtabelaprecoempresaRESTWS implements SimpleRESTWS<MaterialtabelaprecoempresaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialtabelaprecoempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialtabelaprecoempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialtabelaprecoempresaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialtabelaprecoempresaRESTModel> listaMaterialtabelapreco = MaterialtabelaprecoempresaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialtabelapreco);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialtabelaprecoempresaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
