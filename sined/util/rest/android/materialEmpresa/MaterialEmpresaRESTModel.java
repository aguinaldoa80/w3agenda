package br.com.linkcom.sined.util.rest.android.materialEmpresa;


public class MaterialEmpresaRESTModel {
	
	private Integer cdmaterialempresa;
	private Integer cdmaterial;
	private Integer cdempresa;
    
    public MaterialEmpresaRESTModel(){}

	public Integer getCdmaterialempresa() {
		return cdmaterialempresa;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdempresa() {
		return cdempresa;
	}

	public void setCdmaterialempresa(Integer cdmaterialempresa) {
		this.cdmaterialempresa = cdmaterialempresa;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}

}
