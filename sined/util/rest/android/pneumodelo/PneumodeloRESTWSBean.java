package br.com.linkcom.sined.util.rest.android.pneumodelo;


public class PneumodeloRESTWSBean {

	private Integer cdpneumodelo;
	private String nome;
	
	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
