package br.com.linkcom.sined.util.rest.android.pneumodelo;

import br.com.linkcom.sined.geral.bean.Pneumodelo;

public class PneumodeloRESTModel {
	
	private Integer cdpneumodelo;
	private String nome;
    private Integer cdpneumarca;
	
    public PneumodeloRESTModel(){}
	
	public PneumodeloRESTModel(Pneumodelo bean){
		this.cdpneumodelo = bean.getCdpneumodelo();
		this.nome = bean.getNome();
		this.cdpneumarca = bean.getPneumarca() != null ? bean.getPneumarca().getCdpneumarca() : null;
	}
	
	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}
	public String getNome() {
		return nome;
	}
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	
	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
}
