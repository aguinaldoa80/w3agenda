package br.com.linkcom.sined.util.rest.android.pneumodelo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PneumodeloService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Pneumodelo")
public class PneumodeloRESTWS implements SimpleRESTWS<PneumodeloRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PneumodeloRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PneumodeloRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PneumodeloRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PneumodeloRESTModel> listaCliente = PneumodeloService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PneumodeloRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
