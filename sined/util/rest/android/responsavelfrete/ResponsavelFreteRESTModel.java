package br.com.linkcom.sined.util.rest.android.responsavelfrete;

import br.com.linkcom.sined.geral.bean.ResponsavelFrete;

public class ResponsavelFreteRESTModel {
	
	private Integer cdResponsavelFrete;
    private String nome;
    private Integer cdnfe;

    public ResponsavelFreteRESTModel(){}
	
	public ResponsavelFreteRESTModel(ResponsavelFrete bean){
		this.cdResponsavelFrete = bean.getCdResponsavelFrete();
		this.nome = bean.getNome();
		this.cdnfe = bean.getCdnfe();
	}

	public Integer getCdResponsavelFrete() {
		return cdResponsavelFrete;
	}

	public String getNome() {
		return nome;
	}

	public Integer getCdnfe() {
		return cdnfe;
	}

	public void setCdResponsavelFrete(Integer cdResponsavelFrete) {
		this.cdResponsavelFrete = cdResponsavelFrete;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdnfe(Integer cdnfe) {
		this.cdnfe = cdnfe;
	}
}
