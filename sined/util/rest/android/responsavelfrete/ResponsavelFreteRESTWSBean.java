package br.com.linkcom.sined.util.rest.android.responsavelfrete;


public class ResponsavelFreteRESTWSBean {

	private Integer cdResponsavelFrete;

	public Integer getCdResponsavelFrete() {
		return cdResponsavelFrete;
	}

	public void setCdResponsavelFrete(Integer cdResponsavelFrete) {
		this.cdResponsavelFrete = cdResponsavelFrete;
	}
}
