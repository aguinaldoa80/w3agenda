package br.com.linkcom.sined.util.rest.android.responsavelfrete;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ResponsavelFreteService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/ResponsavelFrete")
public class ResponsavelFreteRESTWS implements SimpleRESTWS<ResponsavelFreteRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ResponsavelFreteRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ResponsavelFreteRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ResponsavelFreteRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ResponsavelFreteRESTModel> listaResponsavelFrete = ResponsavelFreteService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaResponsavelFrete);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ResponsavelFreteRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
