package br.com.linkcom.sined.util.rest.android.meiocontato;

import br.com.linkcom.sined.geral.bean.Meiocontato;


public class MeiocontatoRESTModel {
	
	private Integer cdmeiocontato;
	private String nome;
	
    public MeiocontatoRESTModel(){}
	
	public MeiocontatoRESTModel(Meiocontato bean){
		this.cdmeiocontato = bean.getCdmeiocontato();
		this.nome = bean.getNome();
	}
	
	public Integer getCdmeiocontato() {
		return cdmeiocontato;
	}
	public String getNome() {
		return nome;
	}
	public void setCdmeiocontato(Integer cdmeiocontato) {
		this.cdmeiocontato = cdmeiocontato;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
