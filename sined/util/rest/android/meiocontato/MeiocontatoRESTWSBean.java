package br.com.linkcom.sined.util.rest.android.meiocontato;


public class MeiocontatoRESTWSBean {

	private Integer cdmeiocontato;
	private String nome;
	
	public Integer getCdmeiocontato() {
		return cdmeiocontato;
	}
	public String getNome() {
		return nome;
	}
	public void setCdmeiocontato(Integer cdmeiocontato) {
		this.cdmeiocontato = cdmeiocontato;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
