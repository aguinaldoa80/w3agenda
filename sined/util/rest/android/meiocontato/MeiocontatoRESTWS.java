package br.com.linkcom.sined.util.rest.android.meiocontato;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/meiocontato")
public class MeiocontatoRESTWS implements SimpleRESTWS<MeiocontatoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MeiocontatoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MeiocontatoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MeiocontatoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MeiocontatoRESTModel> listaCliente = MeiocontatoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MeiocontatoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
