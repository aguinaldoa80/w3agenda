package br.com.linkcom.sined.util.rest.android.empresa;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Empresa")
public class EmpresaRESTWS implements SimpleRESTWS<EmpresaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(EmpresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(EmpresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(EmpresaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<EmpresaRESTModel> listaCliente = EmpresaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(EmpresaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
