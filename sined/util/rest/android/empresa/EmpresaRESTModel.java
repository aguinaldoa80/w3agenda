package br.com.linkcom.sined.util.rest.android.empresa;

import br.com.linkcom.sined.geral.bean.Empresa;

public class EmpresaRESTModel {
	
	private Integer cdpessoa;
    private String nome;
    private String nomefantasia;
    private String cnpj;
    private String termorecapagem;
    
    public EmpresaRESTModel(){}
	
	public EmpresaRESTModel(Empresa e){
		this.cdpessoa = e.getCdpessoa();
		this.nome = e.getNome();
		this.nomefantasia = e.getNomefantasia();
		this.cnpj = e.getCnpj() != null ? e.getCnpj().toString() : null;
		this.termorecapagem = e.getTermorecapagem();
		
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getTermorecapagem() {
		return termorecapagem;
	}
	public String getNomefantasia() {
		return nomefantasia;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setTermorecapagem(String termorecapagem) {
		this.termorecapagem = termorecapagem;
	}
}
