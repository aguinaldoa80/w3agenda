package br.com.linkcom.sined.util.rest.android.empresa;


public class EmpresaRESTWSBean {

	private Integer cdpessoa;
	private String nome;
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
