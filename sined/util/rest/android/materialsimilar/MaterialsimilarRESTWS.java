package br.com.linkcom.sined.util.rest.android.materialsimilar;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialsimilar")
public class MaterialsimilarRESTWS implements SimpleRESTWS<MaterialsimilarRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialsimilarRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialsimilarRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialsimilarRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialsimilarRESTModel> listaMaterialsimilar = MaterialsimilarService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialsimilar);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialsimilarRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
