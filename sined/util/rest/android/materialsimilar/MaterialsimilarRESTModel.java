package br.com.linkcom.sined.util.rest.android.materialsimilar;

import br.com.linkcom.sined.geral.bean.Materialsimilar;

public class MaterialsimilarRESTModel {
	
	private Integer cdmaterialsimilar;
    private Integer cdmaterial;
    private Integer cdmaterialsimilaritem;

    public MaterialsimilarRESTModel(){}
	
	public MaterialsimilarRESTModel(Materialsimilar bean){
		this.cdmaterialsimilar = bean.getCdmaterialsimilar();
		this.cdmaterial = bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null;
		this.cdmaterialsimilaritem = bean.getMaterialsimilaritem() != null ? 
				bean.getMaterialsimilaritem().getCdmaterial() : null;
	}

	public Integer getCdmaterialsimilar() {
		return cdmaterialsimilar;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdmaterialsimilaritem() {
		return cdmaterialsimilaritem;
	}

	public void setCdmaterialsimilar(Integer cdmaterialsimilar) {
		this.cdmaterialsimilar = cdmaterialsimilar;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdmaterialsimilaritem(Integer cdmaterialsimilaritem) {
		this.cdmaterialsimilaritem = cdmaterialsimilaritem;
	}
}
