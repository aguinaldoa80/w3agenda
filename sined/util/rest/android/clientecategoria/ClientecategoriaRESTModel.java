package br.com.linkcom.sined.util.rest.android.clientecategoria;

import br.com.linkcom.sined.geral.bean.Pessoacategoria;

public class ClientecategoriaRESTModel {

    private Integer cdpessoacategoria;
	private Integer cdcategoria;
	private Integer cdcliente;

    public ClientecategoriaRESTModel(){}
    
    public ClientecategoriaRESTModel(Pessoacategoria bean){
    	this.cdpessoacategoria = bean.getCdpessoacategoria();
    	this.cdcategoria = bean.getCategoria() == null ? null : bean.getCategoria().getCdcategoria();
    	this.cdcliente = bean.getPessoa() == null ? null : bean.getPessoa().getCdpessoa();
    }

	public Integer getCdpessoacategoria() {
		return cdpessoacategoria;
	}

	public Integer getCdcategoria() {
		return cdcategoria;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public void setCdpessoacategoria(Integer cdpessoacategoria) {
		this.cdpessoacategoria = cdpessoacategoria;
	}

	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

}
