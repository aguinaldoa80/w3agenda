package br.com.linkcom.sined.util.rest.android.clientecategoria;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PessoacategoriaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Clientecategoria")
public class ClientecategoriaRESTWS implements SimpleRESTWS<ClientecategoriaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ClientecategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ClientecategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ClientecategoriaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ClientecategoriaRESTModel> listaConta = PessoacategoriaService.getInstance().findClienteForAndroid();
		modelAndStatus.setModel(listaConta);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ClientecategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
