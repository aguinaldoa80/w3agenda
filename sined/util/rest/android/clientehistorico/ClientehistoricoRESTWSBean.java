package br.com.linkcom.sined.util.rest.android.clientehistorico;


public class ClientehistoricoRESTWSBean {

	private Long id;
	private Integer cdclientehistorico;
	private Integer cdcliente;
    private Integer cdmeiocontato;
    private Integer cdatividadetipo;
    private Integer cdsituacaohistorico;
    private String observacao;
    private Long dtalteracao;
    private Long cdusuarioaltera;
	
    public Long getId() {
		return id;
	}
	public Integer getCdclientehistorico() {
		return cdclientehistorico;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdmeiocontato() {
		return cdmeiocontato;
	}
	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}
	public Integer getCdsituacaohistorico() {
		return cdsituacaohistorico;
	}
	public String getObservacao() {
		return observacao;
	}
	public Long getDtalteracao() {
		return dtalteracao;
	}
	public Long getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setCdclientehistorico(Integer cdclientehistorico) {
		this.cdclientehistorico = cdclientehistorico;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdmeiocontato(Integer cdmeiocontato) {
		this.cdmeiocontato = cdmeiocontato;
	}
	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}
	public void setCdsituacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtalteracao(Long dtalteracao) {
		this.dtalteracao = dtalteracao;
	}
	public void setCdusuarioaltera(Long cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
