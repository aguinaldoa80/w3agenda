package br.com.linkcom.sined.util.rest.android.clientehistorico;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/HistoricoCliente")
public class ClientehistoricoRESTWS implements SimpleRESTWS<ClientehistoricoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ClientehistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ClientehistoricoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		modelAndStatus.setModel(ClientehistoricoService.getInstance().salvarClientehistoricoAndroid(command));
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(ClientehistoricoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ClientehistoricoRESTModel> listaCliente = ClientehistoricoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ClientehistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
