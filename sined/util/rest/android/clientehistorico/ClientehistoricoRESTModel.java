package br.com.linkcom.sined.util.rest.android.clientehistorico;

import br.com.linkcom.sined.geral.bean.Clientehistorico;

public class ClientehistoricoRESTModel {
	
	private Integer cdclientehistorico;
	private Integer cdcliente;
	private String observacao;
	private Integer cdcontato;
	private Integer cdmeiocontato;
	private Integer cdatividadetipo;
	private Integer situacao;
	private Integer cdsituacaohistorico;
	private Long dtalteracao;
	private String nomeusuario;
	
	private String erroSincronizacao;
	
    public ClientehistoricoRESTModel(){}
	
	public ClientehistoricoRESTModel(Clientehistorico bean){
		this.cdclientehistorico = bean.getCdclientehistorico();
		this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
		this.observacao = bean.getObservacao();
		this.cdcontato = bean.getContato() != null ? bean.getContato().getCdpessoa() : null;
		this.cdmeiocontato = bean.getMeiocontato() != null ? bean.getMeiocontato().getCdmeiocontato() : null;
		this.cdatividadetipo = bean.getAtividadetipo() != null ? bean.getAtividadetipo().getCdatividadetipo() : null;
		this.situacao = bean.getSituacao() != null ? bean.getSituacao().ordinal() : null;
		this.cdsituacaohistorico = bean.getSituacaohistorico() != null ? bean.getSituacaohistorico().getCdsituacaohistorico() : null;
		this.dtalteracao = bean.getDtaltera() != null ? bean.getDtaltera().getTime() : null;
		this.nomeusuario = bean.getUsuarioaltera() != null ? bean.getUsuarioaltera().getNome() : null;
	}

	public Integer getCdclientehistorico() {
		return cdclientehistorico;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public String getObservacao() {
		return observacao;
	}

	public Integer getCdcontato() {
		return cdcontato;
	}

	public Integer getCdmeiocontato() {
		return cdmeiocontato;
	}

	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public Integer getCdsituacaohistorico() {
		return cdsituacaohistorico;
	}
	
	public Long getDtalteracao() {
		return dtalteracao;
	}

	public String getNomeusuario() {
		return nomeusuario;
	}

	public void setCdclientehistorico(Integer cdclientehistorico) {
		this.cdclientehistorico = cdclientehistorico;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdcontato(Integer cdcontato) {
		this.cdcontato = cdcontato;
	}

	public void setCdmeiocontato(Integer cdmeiocontato) {
		this.cdmeiocontato = cdmeiocontato;
	}

	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public void setCdsituacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}

	public void setDtalteracao(Long dtalteracao) {
		this.dtalteracao = dtalteracao;
	}

	public void setNomeusuario(String nomeusuario) {
		this.nomeusuario = nomeusuario;
	}

	public String getErroSincronizacao() {
		return erroSincronizacao;
	}

	public void setErroSincronizacao(String erroSincronizacao) {
		this.erroSincronizacao = erroSincronizacao;
	}
}
