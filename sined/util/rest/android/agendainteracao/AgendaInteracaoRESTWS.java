package br.com.linkcom.sined.util.rest.android.agendainteracao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.dao.AgendainteracaoDAO;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.geral.service.AgendainteracaohistoricoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/AgendaInteracao")
public class AgendaInteracaoRESTWS implements SimpleRESTWS<AgendaInteracaoRESTWSBean>, AuthenticatedRESTWS {
	
	private AgendainteracaoService agendaInteracaoService;
	private AgendainteracaohistoricoService agendaInteracaoHistoricoService;
	
	public void setAgendaInteracaoHistoricoService(
			AgendainteracaohistoricoService agendaInteracaoHistoricoService) {
		this.agendaInteracaoHistoricoService = agendaInteracaoHistoricoService;
	}

	public void setAgendaInteracaoService(
			AgendainteracaoService agendaInteracaoService) {
		this.agendaInteracaoService = agendaInteracaoService;
	}
	
	

	@Override
	public ModelAndStatus delete(AgendaInteracaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(AgendaInteracaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<Agendainteracao> listaAgenda = agendaInteracaoService.findForAndroid("3", "", true);
		List<Agendainteracaohistorico> listaHistorico = agendaInteracaoHistoricoService.findForAndroidHistorico("3", "", true);
		//Lista de agenda a fazer.
		List<AgendaInteracaoRESTWSBean> agenda = new ArrayList<AgendaInteracaoRESTWSBean>();
		//Lista de agenda concluida concluida no app.
		List<HistoricoInteracaoRESTWSBean> realizado = new ArrayList<HistoricoInteracaoRESTWSBean>();
		for(Agendainteracao agendaAFazer : listaAgenda){
			AgendaInteracaoRESTWSBean ag = new AgendaInteracaoRESTWSBean();
			
			ag.setCdAgendaIntegracao(agendaAFazer.getCdagendainteracao());
			ag.setDtProximaInteracao(agendaAFazer.getDtproximainteracao());
			ag.setCdCliente(agendaAFazer.getCliente().getCdpessoa());
			ag.setNomeCliente(agendaAFazer.getCliente().getNome());
			ag.setEnderecosCliente(new ArrayList<String>());
			for(Endereco endereco : agendaAFazer.getCliente().getListaEndereco()){
				ag.getEnderecosCliente().add(endereco.getLogradouroCompletoComBairro());
			}
			ag.setTelefonesCliente(new ArrayList<String>());
			for(PessoaContato contato : agendaAFazer.getCliente().getListaContato()){
				ag.getTelefonesCliente().add(contato.getContato().getTelefones());
			}
			
			agenda.add(ag);
		}
		for(Agendainteracaohistorico historicoAgenda : listaHistorico){
			HistoricoInteracaoRESTWSBean ag = new HistoricoInteracaoRESTWSBean();
			ag.setCdAgendaInteracaoHistorico(historicoAgenda.getCdagendainteracaohistorico());
			ag.setDataInteracao(new java.sql.Date(historicoAgenda.getDtaltera().getTime()));
			ag.setCdCliente(historicoAgenda.getAgendainteracao().getCliente().getCdpessoa());
			ag.setNomeCliente(historicoAgenda.getAgendainteracao().getCliente().getNome());
			ag.setObservacao(historicoAgenda.getObservacao());
			ag.setEnderecosCliente(new ArrayList<String>());
			for(Endereco endereco : historicoAgenda.getAgendainteracao().getCliente().getListaEndereco()){
				ag.getEnderecosCliente().add(endereco.getLogradouroCompletoComBairro());
			}
			ag.setTelefonesCliente(new ArrayList<String>());
			for(PessoaContato contato : historicoAgenda.getAgendainteracao().getCliente().getListaContato()){
				ag.getTelefonesCliente().add(contato.getContato().getTelefones());
			}
			
			realizado.add(ag);
		}
		List<Object> obj = new ArrayList<Object>();
		obj.add(agenda);
		obj.add(realizado);
		modelAndStatus.setModel(obj);
		//modelAndStatus.setModel(realizado);
//		List<MaterialRESTModel> listaMaterial = null;
//
//		if(command.getDtUltimaSincronizacao() != null){
//			MaterialCatalogoRESTModel materialCatalogo = new MaterialCatalogoRESTModel();
//			materialCatalogo.setDtUltimaSincronizacao(new Timestamp(System.currentTimeMillis()));
//			materialCatalogo.setListaMaterialRest(MaterialService.getInstance().findForAndroid(null, true, command.getDtUltimaSincronizacao()));
//			modelAndStatus.setModel(materialCatalogo);
//		}else{
//			listaMaterial = MaterialService.getInstance().findForAndroid();
//			modelAndStatus.setModel(listaMaterial);
//		}
		
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(AgendaInteracaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<Agendainteracao> listaAgenda = agendaInteracaoService.findForAndroid("3", "", true);
		List<Agendainteracaohistorico> listaHistorico = agendaInteracaoHistoricoService.findForAndroidHistorico("3", "", true);
		//Lista de agenda a fazer.
		List<AgendaInteracaoRESTWSBean> agenda = new ArrayList<AgendaInteracaoRESTWSBean>();
		//Lista de agenda concluida concluida no app.
		List<HistoricoInteracaoRESTWSBean> realizado = new ArrayList<HistoricoInteracaoRESTWSBean>();
		for(Agendainteracao agendaAFazer : listaAgenda){
			AgendaInteracaoRESTWSBean ag = new AgendaInteracaoRESTWSBean();
			
			ag.setCdAgendaIntegracao(agendaAFazer.getCdagendainteracao());
			ag.setDtProximaInteracao(agendaAFazer.getDtproximainteracao());
			ag.setCdCliente(agendaAFazer.getCliente().getCdpessoa());
			ag.setNomeCliente(agendaAFazer.getCliente().getNome());
			ag.setEnderecosCliente(new ArrayList<String>());
			for(Endereco endereco : agendaAFazer.getCliente().getListaEndereco()){
				ag.getEnderecosCliente().add(endereco.getLogradouroCompletoComBairro());
			}
			ag.setTelefonesCliente(new ArrayList<String>());
			for(PessoaContato contato : agendaAFazer.getCliente().getListaContato()){
				ag.getTelefonesCliente().add(contato.getContato().getTelefones());
			}
			
			agenda.add(ag);
		}
		for(Agendainteracaohistorico historicoAgenda : listaHistorico){
			HistoricoInteracaoRESTWSBean ag = new HistoricoInteracaoRESTWSBean();
			ag.setCdAgendaInteracaoHistorico(historicoAgenda.getCdagendainteracaohistorico());
			ag.setDataInteracao(new java.sql.Date(historicoAgenda.getDtaltera().getTime()));
			ag.setCdCliente(historicoAgenda.getAgendainteracao().getCliente().getCdpessoa());
			ag.setNomeCliente(historicoAgenda.getAgendainteracao().getCliente().getNome());
			ag.setObservacao(historicoAgenda.getObservacao());
			ag.setEnderecosCliente(new ArrayList<String>());
			for(Endereco endereco : historicoAgenda.getAgendainteracao().getCliente().getListaEndereco()){
				ag.getEnderecosCliente().add(endereco.getLogradouroCompletoComBairro());
			}
			ag.setTelefonesCliente(new ArrayList<String>());
			for(PessoaContato contato : historicoAgenda.getAgendainteracao().getCliente().getListaContato()){
				ag.getTelefonesCliente().add(contato.getContato().getTelefones());
			}
			
			realizado.add(ag);
		}
		List<Object> obj = new ArrayList<Object>();
		obj.add(agenda);
		obj.add(realizado);
		modelAndStatus.setModel(obj);
		//modelAndStatus.setModel(realizado);
//		List<MaterialRESTModel> listaMaterial = null;
//
//		if(command.getDtUltimaSincronizacao() != null){
//			MaterialCatalogoRESTModel materialCatalogo = new MaterialCatalogoRESTModel();
//			materialCatalogo.setDtUltimaSincronizacao(new Timestamp(System.currentTimeMillis()));
//			materialCatalogo.setListaMaterialRest(MaterialService.getInstance().findForAndroid(null, true, command.getDtUltimaSincronizacao()));
//			modelAndStatus.setModel(materialCatalogo);
//		}else{
//			listaMaterial = MaterialService.getInstance().findForAndroid();
//			modelAndStatus.setModel(listaMaterial);
//		}
		
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(AgendaInteracaoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
