package br.com.linkcom.sined.util.rest.android.agendainteracao;

import java.sql.Date;
import java.util.List;

public class HistoricoInteracaoRESTWSBean {

	private Integer cdAgendaInteracaoHistorico;
	private Date dataInteracao;
	private String observacao;
	private Integer cdCliente;
	private String nomeCliente;
	private List<String> enderecosCliente;
	private List<String> telefonesCliente;
	public Integer getCdAgendaInteracaoHistorico() {
		return cdAgendaInteracaoHistorico;
	}
	public void setCdAgendaInteracaoHistorico(Integer cdAgendaInteracaoHistorico) {
		this.cdAgendaInteracaoHistorico = cdAgendaInteracaoHistorico;
	}
	public Date getDataInteracao() {
		return dataInteracao;
	}
	public void setDataInteracao(Date dataInteracao) {
		this.dataInteracao = dataInteracao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Integer getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public List<String> getEnderecosCliente() {
		return enderecosCliente;
	}
	public void setEnderecosCliente(List<String> enderecosCliente) {
		this.enderecosCliente = enderecosCliente;
	}
	public List<String> getTelefonesCliente() {
		return telefonesCliente;
	}
	public void setTelefonesCliente(List<String> telefonesCliente) {
		this.telefonesCliente = telefonesCliente;
	}
	
	
		
}
