package br.com.linkcom.sined.util.rest.android.agendainteracao;

import java.sql.Date;
import java.util.List;

public class AgendaInteracaoRESTWSBean {

	private Integer cdusuario;
	private Integer cdempresa;
	private Integer cdAgendaIntegracao;
	private Date dtProximaInteracao;
	private Integer cdCliente;
	private String nomeCliente;
	private List<String> enderecosCliente;
	private List<String> telefonesCliente;
	
	
	public Integer getCdusuario() {
		return cdusuario;
	}
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public Integer getCdAgendaIntegracao() {
		return cdAgendaIntegracao;
	}
	public void setCdAgendaIntegracao(Integer cdAgendaIntegracao) {
		this.cdAgendaIntegracao = cdAgendaIntegracao;
	}
	public Date getDtProximaInteracao() {
		return dtProximaInteracao;
	}
	public void setDtProximaInteracao(Date dtProximaInteracao) {
		this.dtProximaInteracao = dtProximaInteracao;
	}
	public Integer getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public List<String> getEnderecosCliente() {
		return enderecosCliente;
	}
	public void setEnderecosCliente(List<String> enderecosCliente) {
		this.enderecosCliente = enderecosCliente;
	}
	public List<String> getTelefonesCliente() {
		return telefonesCliente;
	}
	public void setTelefonesCliente(List<String> telefonesCliente) {
		this.telefonesCliente = telefonesCliente;
	}
	
}
