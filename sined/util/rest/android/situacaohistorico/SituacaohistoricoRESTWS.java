package br.com.linkcom.sined.util.rest.android.situacaohistorico;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.SituacaohistoricoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Situacaohistorico")
public class SituacaohistoricoRESTWS implements SimpleRESTWS<SituacaohistoricoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(SituacaohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(SituacaohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(SituacaohistoricoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<SituacaohistoricoRESTModel> listaCliente = SituacaohistoricoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(SituacaohistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
