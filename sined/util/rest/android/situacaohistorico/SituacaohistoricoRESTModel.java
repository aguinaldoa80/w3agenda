package br.com.linkcom.sined.util.rest.android.situacaohistorico;

import br.com.linkcom.sined.geral.bean.Situacaohistorico;

public class SituacaohistoricoRESTModel {
	
	private Integer cdsituacaohistorico;
	private String descricao;
	
    public SituacaohistoricoRESTModel(){}
	
	public SituacaohistoricoRESTModel(Situacaohistorico bean){
		this.cdsituacaohistorico = bean.getCdsituacaohistorico();
		this.descricao = bean.getDescricao();
	}
	
	public Integer getCdsituacaohistorico() {
		return cdsituacaohistorico;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdsituacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
