package br.com.linkcom.sined.util.rest.android.situacaohistorico;


public class SituacaohistoricoRESTWSBean {

	private Integer cdsituacaohistorico;
	private String nome;
	
	public Integer getCdsituacaohistorico() {
		return cdsituacaohistorico;
	}
	public String getNome() {
		return nome;
	}
	public void setCdsituacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
