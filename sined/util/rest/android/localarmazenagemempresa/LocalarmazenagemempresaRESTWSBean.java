package br.com.linkcom.sined.util.rest.android.localarmazenagemempresa;


public class LocalarmazenagemempresaRESTWSBean {

	private Integer cdlocalarmazenagemempresa;
	private Integer cdlocalarmazenagem;
	private Integer cdempresa;
	
	public Integer getCdlocalarmazenagemempresa() {
		return cdlocalarmazenagemempresa;
	}
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	
	public void setCdlocalarmazenagemempresa(Integer cdlocalarmazenagemempresa) {
		this.cdlocalarmazenagemempresa = cdlocalarmazenagemempresa;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
}
