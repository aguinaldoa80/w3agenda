package br.com.linkcom.sined.util.rest.android.localarmazenagemempresa;

import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;

public class LocalarmazenagemempresaRESTModel {
	
	private Integer cdlocalarmazenagemempresa;
	private Integer cdlocalarmazenagem;
	private Integer cdempresa;
	
    public LocalarmazenagemempresaRESTModel(){}
	
	public LocalarmazenagemempresaRESTModel(Localarmazenagemempresa bean){
		this.cdlocalarmazenagemempresa = bean.getCdlocalarmazenagemempresa();
		this.cdlocalarmazenagem = bean.getLocalarmazenagem() != null ? bean.getLocalarmazenagem().getCdlocalarmazenagem() : null;
		this.cdempresa = bean.getEmpresa() != null ? bean.getEmpresa().getCdpessoa() : null;
	}

	public Integer getCdlocalarmazenagemempresa() {
		return cdlocalarmazenagemempresa;
	}

	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}

	public Integer getCdempresa() {
		return cdempresa;
	}

	public void setCdlocalarmazenagemempresa(Integer cdlocalarmazenagemempresa) {
		this.cdlocalarmazenagemempresa = cdlocalarmazenagemempresa;
	}

	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}

	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}

	
}
