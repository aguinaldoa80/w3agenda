package br.com.linkcom.sined.util.rest.android.localarmazenagemempresa;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.LocalarmazenagemempresaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Localarmazenagemempresa")
public class LocalarmazenagemempresaRESTWS implements SimpleRESTWS<LocalarmazenagemempresaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(LocalarmazenagemempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(LocalarmazenagemempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(LocalarmazenagemempresaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<LocalarmazenagemempresaRESTModel> listaCliente = LocalarmazenagemempresaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(LocalarmazenagemempresaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
