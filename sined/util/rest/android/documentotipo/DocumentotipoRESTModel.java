package br.com.linkcom.sined.util.rest.android.documentotipo;

import br.com.linkcom.sined.geral.bean.Documentotipo;

public class DocumentotipoRESTModel {

    private Integer cddocumentotipo;
    private String nome;
    private Boolean boleto;
    
    public DocumentotipoRESTModel(){}
    
    public DocumentotipoRESTModel(Documentotipo bean){
    	this.cddocumentotipo = bean.getCddocumentotipo();
    	this.nome = bean.getNome();
    	this.boleto = bean.getBoleto();
    }

    public Integer getCddocumentotipo() {
        return cddocumentotipo;
    }

    public void setCddocumentotipo(Integer cddocumentotipo) {
        this.cddocumentotipo = cddocumentotipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Boolean getBoleto() {
        return boleto;
    }

    public void setBoleto(Boolean boleto) {
        this.boleto = boleto;
    }
}
