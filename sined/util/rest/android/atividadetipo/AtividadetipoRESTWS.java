package br.com.linkcom.sined.util.rest.android.atividadetipo;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Atividadetipo")
public class AtividadetipoRESTWS implements SimpleRESTWS<AtividadetipoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(AtividadetipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(AtividadetipoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(AtividadetipoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<AtividadetipoRESTModel> listaCliente = AtividadetipoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(AtividadetipoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
