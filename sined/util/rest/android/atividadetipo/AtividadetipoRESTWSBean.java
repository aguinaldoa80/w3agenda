package br.com.linkcom.sined.util.rest.android.atividadetipo;


public class AtividadetipoRESTWSBean {

	private Integer cdatividadetipo;
	private String nome;
	
	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
