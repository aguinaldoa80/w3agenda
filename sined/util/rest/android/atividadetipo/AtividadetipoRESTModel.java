package br.com.linkcom.sined.util.rest.android.atividadetipo;

import br.com.linkcom.sined.geral.bean.Atividadetipo;

public class AtividadetipoRESTModel {
	
	private Integer cdatividadetipo;
	private String nome;
	
    public AtividadetipoRESTModel(){}
	
	public AtividadetipoRESTModel(Atividadetipo bean){
		this.cdatividadetipo = bean.getCdatividadetipo();
		this.nome = bean.getNome();
	}
	
	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}
	public String getNome() {
		return nome;
	}
	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
