package br.com.linkcom.sined.util.rest.android.endereco;


public class EnderecoRESTWSBean {

	private Long id;
	private Integer cdendereco;
	private Integer cdmunicipio;
    private Integer cduf;
    private String logradouro;
    private String bairro;
    private String cep;
    private String numero;
    private String complemento;
    private Integer cdenderecotipo;
	
    public Integer getCdendereco() {
		return cdendereco;
	}
	public Integer getCdmunicipio() {
		return cdmunicipio;
	}
	public Integer getCduf() {
		return cduf;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public Integer getCdenderecotipo() {
		return cdenderecotipo;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setCdmunicipio(Integer cdmunicipio) {
		this.cdmunicipio = cdmunicipio;
	}
	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setCdenderecotipo(Integer cdenderecotipo) {
		this.cdenderecotipo = cdenderecotipo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
