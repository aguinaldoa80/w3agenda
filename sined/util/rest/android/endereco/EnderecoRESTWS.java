package br.com.linkcom.sined.util.rest.android.endereco;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Endereco")
public class EnderecoRESTWS implements SimpleRESTWS<EnderecoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(EnderecoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(EnderecoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(EnderecoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<EnderecoRESTModel> listaEndereco = EnderecoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaEndereco);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(EnderecoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
