package br.com.linkcom.sined.util.rest.android.pneumedida;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PneumedidaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Pneumedida")
public class PneumedidaRESTWS implements SimpleRESTWS<PneumedidaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PneumedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PneumedidaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PneumedidaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PneumedidaRESTModel> listaCliente = PneumedidaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PneumedidaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
