package br.com.linkcom.sined.util.rest.android.pneumedida;


public class PneumedidaRESTWSBean {

	private Integer cdpneumedida;
	private String nome;
	
	public Integer getCdpneumedida() {
		return cdpneumedida;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
