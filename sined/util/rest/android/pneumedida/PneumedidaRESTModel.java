package br.com.linkcom.sined.util.rest.android.pneumedida;

import br.com.linkcom.sined.geral.bean.Pneumedida;

public class PneumedidaRESTModel {
	
	private Integer cdpneumedida;
	private String nome;
	
    public PneumedidaRESTModel(){}
	
	public PneumedidaRESTModel(Pneumedida bean){
		this.cdpneumedida = bean.getCdpneumedida();
		this.nome = bean.getNome();
	}
	
	public Integer getCdpneumedida() {
		return cdpneumedida;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
