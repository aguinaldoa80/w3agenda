package br.com.linkcom.sined.util.rest.android.pneu;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Pneu")
public class PneuRESTWS implements SimpleRESTWS<PneuRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PneuRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PneuRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PneuRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<PneuRESTModel> listaCliente = PneuService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PneuRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
