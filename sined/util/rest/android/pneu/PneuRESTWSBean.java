package br.com.linkcom.sined.util.rest.android.pneu;


public class PneuRESTWSBean {

	private Long idpneu;
    private Integer cdpneu;
    private Long idpedidovendamaterial;
    private Integer cdpedidovendamaterial;
    private Integer cdpneumarca;
    private Integer cdpneumodelo;
    private Integer cdpneumedida;
    private String serie;
    private String dot;
    private Integer numeroreforma;
    private Integer cdmaterialbanda;

    public Long getIdpneu() {
        return idpneu;
    }

    public Integer getCdpneu() {
        return cdpneu;
    }

    public Long getIdpedidovendamaterial() {
        return idpedidovendamaterial;
    }

    public Integer getCdpedidovendamaterial() {
        return cdpedidovendamaterial;
    }

    public Integer getCdpneumarca() {
        return cdpneumarca;
    }

    public Integer getCdpneumodelo() {
        return cdpneumodelo;
    }

    public Integer getCdpneumedida() {
        return cdpneumedida;
    }

    public String getSerie() {
        return serie;
    }

    public String getDot() {
        return dot;
    }

    public Integer getNumeroreforma() {
        return numeroreforma;
    }

    public Integer getCdmaterialbanda() {
        return cdmaterialbanda;
    }

    public void setIdpneu(Long idpneu) {
        this.idpneu = idpneu;
    }

    public void setCdpneu(Integer cdpneu) {
        this.cdpneu = cdpneu;
    }

    public void setIdpedidovendamaterial(Long idpedidovendamaterial) {
        this.idpedidovendamaterial = idpedidovendamaterial;
    }

    public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
        this.cdpedidovendamaterial = cdpedidovendamaterial;
    }

    public void setCdpneumarca(Integer cdpneumarca) {
        this.cdpneumarca = cdpneumarca;
    }

    public void setCdpneumodelo(Integer cdpneumodelo) {
        this.cdpneumodelo = cdpneumodelo;
    }

    public void setCdpneumedida(Integer cdpneumedida) {
        this.cdpneumedida = cdpneumedida;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public void setDot(String dot) {
        this.dot = dot;
    }

    public void setNumeroreforma(Integer numeroreforma) {
        this.numeroreforma = numeroreforma;
    }

    public void setCdmaterialbanda(Integer cdmaterialbanda) {
        this.cdmaterialbanda = cdmaterialbanda;
    }
}
