package br.com.linkcom.sined.util.rest.android.pneu;

import br.com.linkcom.sined.geral.bean.Pneu;

public class PneuRESTModel {
	
	private Integer cdpneu;
	private Integer cdpneumarca;
	private Integer cdpneumedida;
	private Integer cdpneumodelo;
	private String serie;
	private String dot;
	private Integer cdmaterialbanda;
	private Integer numeroreforma;
	
    public PneuRESTModel(){}
	
	public PneuRESTModel(Pneu bean){
		this.cdpneu = bean.getCdpneu();
		this.cdpneumarca = bean.getPneumarca() != null ? bean.getPneumarca().getCdpneumarca() : null;
		this.cdpneumedida = bean.getPneumedida() != null ? bean.getPneumedida().getCdpneumedida() : null;
		this.cdpneumodelo = bean.getPneumodelo() != null ? bean.getPneumodelo().getCdpneumodelo() : null;
		this.serie = bean.getSerie();
		this.dot = bean.getDot();
		this.cdmaterialbanda = bean.getMaterialbanda() != null ? bean.getMaterialbanda().getCdmaterial() : null;
		this.numeroreforma = bean.getNumeroreforma() != null ? bean.getNumeroreforma().ordinal() : null;
	}

	public Integer getCdpneu() {
		return cdpneu;
	}

	public Integer getCdpneumarca() {
		return cdpneumarca;
	}

	public Integer getCdpneumedida() {
		return cdpneumedida;
	}

	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}

	public String getSerie() {
		return serie;
	}

	public String getDot() {
		return dot;
	}

	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}

	public Integer getNumeroreforma() {
		return numeroreforma;
	}

	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}

	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}

	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}

	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}

	public void setNumeroreforma(Integer numeroreforma) {
		this.numeroreforma = numeroreforma;
	}
}
