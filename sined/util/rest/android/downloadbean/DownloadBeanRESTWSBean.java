package br.com.linkcom.sined.util.rest.android.downloadbean;

import java.sql.Date;

import br.com.linkcom.sined.util.SinedDateUtils;


public class DownloadBeanRESTWSBean {

	private Long dataUltimaAtualizacao;
	private String whereInUsuarioAndroid;
	private String whereInPedidovendaAndroid;
	private Boolean considerarSomenteTabelapreco;
	private Boolean considerarSomenteTabelaclientevendedor;
	private Boolean considerarSomenteUltimascompras;

	public Long getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Long dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getWhereInUsuarioAndroid() {
		return whereInUsuarioAndroid;
	}

	public void setWhereInUsuarioAndroid(String whereInUsuarioAndroid) {
		this.whereInUsuarioAndroid = whereInUsuarioAndroid;
	}
	
	public String getWhereInPedidovendaAndroid() {
		return whereInPedidovendaAndroid;
	}
	
	public void setWhereInPedidovendaAndroid(String whereInPedidovendaAndroid) {
		this.whereInPedidovendaAndroid = whereInPedidovendaAndroid;
	}
	
	public Boolean getConsiderarSomenteTabelapreco() {
		return considerarSomenteTabelapreco;
	}

	public void setConsiderarSomenteTabelapreco(Boolean considerarSomenteTabelapreco) {
		this.considerarSomenteTabelapreco = considerarSomenteTabelapreco;
	}
	
	public Boolean getConsiderarSomenteTabelaclientevendedor() {
		return considerarSomenteTabelaclientevendedor;
	}

	public void setConsiderarSomenteTabelaclientevendedor(Boolean considerarSomenteTabelaclientevendedor) {
		this.considerarSomenteTabelaclientevendedor = considerarSomenteTabelaclientevendedor;
	}
	
	public Boolean getConsiderarSomenteUltimascompras() {
		return considerarSomenteUltimascompras;
	}

	public void setConsiderarSomenteUltimascompras(Boolean considerarSomenteUltimascompras) {
		this.considerarSomenteUltimascompras = considerarSomenteUltimascompras;
	}

	public Long getDataUltimaAtualizacaoMenosUmaHora(){
		if(dataUltimaAtualizacao != null){
			return SinedDateUtils.addMinutesData(new Date(dataUltimaAtualizacao), -60).getTime();
		}
		
		return dataUltimaAtualizacao;
	}
}
