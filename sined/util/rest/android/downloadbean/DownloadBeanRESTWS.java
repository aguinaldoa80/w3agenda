package br.com.linkcom.sined.util.rest.android.downloadbean;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/DownloadBean")
public class DownloadBeanRESTWS implements SimpleRESTWS<DownloadBeanRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(DownloadBeanRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DownloadBeanRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		DownloadBeanRESTModel model = UsuarioService.getInstance().getDownloadBeanForAndroid(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(DownloadBeanRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(DownloadBeanRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
