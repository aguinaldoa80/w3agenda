package br.com.linkcom.sined.util.rest.android.downloadbean;

import br.com.linkcom.sined.util.rest.android.atividadetipo.AtividadetipoRESTModel;
import br.com.linkcom.sined.util.rest.android.banco.BancoRESTModel;
import br.com.linkcom.sined.util.rest.android.categoria.CategoriaRESTModel;
import br.com.linkcom.sined.util.rest.android.cliente.ClienteRESTModel;
import br.com.linkcom.sined.util.rest.android.clientedocumentotipo.ClientedocumentotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.clientehistorico.ClientehistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.clienteprazopagamento.ClienteprazopagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.clientevendedor.ClientevendedorRESTModel;
import br.com.linkcom.sined.util.rest.android.colaborador.ColaboradorRESTModel;
import br.com.linkcom.sined.util.rest.android.conta.ContaRESTModel;
import br.com.linkcom.sined.util.rest.android.contaempresa.ContaempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.contatipo.ContatipoRESTModel;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTModel;
import br.com.linkcom.sined.util.rest.android.contatotipo.ContatotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.documento.DocumentoRESTModel;
import br.com.linkcom.sined.util.rest.android.documentohistorico.DocumentohistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.documentotipo.DocumentotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.empresa.EmpresaRESTModel;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTModel;
import br.com.linkcom.sined.util.rest.android.enderecotipo.EnderecotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.formapagamento.FormapagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.fornecedor.FornecedorRESTModel;
import br.com.linkcom.sined.util.rest.android.indicecorrecao.IndicecorrecaoRESTModel;
import br.com.linkcom.sined.util.rest.android.localarmazenagem.LocalarmazenagemRESTModel;
import br.com.linkcom.sined.util.rest.android.localarmazenagemempresa.LocalarmazenagemempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.material.MaterialRESTModel;
import br.com.linkcom.sined.util.rest.android.materialEmpresa.MaterialEmpresaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialformulavalorvenda.MaterialformulavalorvendaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialgrupo.MaterialgrupoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialproducao.MaterialproducaoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialrelacionado.MaterialrelacionadoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialsimilar.MaterialsimilarRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelapreco.MaterialtabelaprecoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecocliente.MaterialtabelaprecoclienteRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoempresa.MaterialtabelaprecoempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoitem.MaterialtabelaprecoitemRESTModel;
import br.com.linkcom.sined.util.rest.android.materialunidademedida.MaterialunidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.meiocontato.MeiocontatoRESTModel;
import br.com.linkcom.sined.util.rest.android.municipio.MunicipioRESTModel;
import br.com.linkcom.sined.util.rest.android.parametrogeral.ParametrogeralRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovendahistorico.PedidovendahistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovendatipo.PedidovendatipoRESTModel;
import br.com.linkcom.sined.util.rest.android.pneu.PneuRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumarca.PneumarcaRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumedida.PneumedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumodelo.PneumodeloRESTModel;
import br.com.linkcom.sined.util.rest.android.prazopagamento.PrazopagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.prazopagamentoitem.PrazopagamentoitemRESTModel;
import br.com.linkcom.sined.util.rest.android.projeto.ProjetoRESTModel;
import br.com.linkcom.sined.util.rest.android.responsavelfrete.ResponsavelFreteRESTModel;
import br.com.linkcom.sined.util.rest.android.situacaohistorico.SituacaohistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.tabelavalor.TabelavalorRESTModel;
import br.com.linkcom.sined.util.rest.android.uf.UfRESTModel;
import br.com.linkcom.sined.util.rest.android.unidademedida.UnidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.usuario.UsuarioRESTModel;
import br.com.linkcom.sined.util.rest.android.usuarioempresa.UsuarioempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.valecompra.ValecompraRESTModel;
import br.com.linkcom.sined.util.rest.android.vwultimascompras.VwultimascomprasandroidRESTModel;


public class DownloadBeanRESTModel {
	
	private Long dataUltimaSincronizacao;
	private Boolean haveDados;
	
	private CategoriaRESTModel[] categoriaRESTModelArray;
    private ClienteRESTModel[] clienteRESTModelArray;
    private ColaboradorRESTModel[] colaboradorRESTModelArray;
    private ContaRESTModel[] contaRESTModelArray;
    private ContatipoRESTModel[] contatipoRESTModelArray;
    private ContatoRESTModel[] contatoRESTModelArray;
    private ContatotipoRESTModel[] contatotipoRESTModelArray;
    private DocumentotipoRESTModel[] documentotipoRESTModelArray;
    private EmpresaRESTModel[] empresaRESTModelArray;
    private ContaempresaRESTModel[] contaempresaRESTModelArray;
    private EnderecoRESTModel[] enderecoRESTModelArray;
    private EnderecotipoRESTModel[] enderecotipoRESTModelArray;
    private FormapagamentoRESTModel[] formapagamentoRESTModelArray;
    private FornecedorRESTModel[] fornecedorRESTModelArray;
    private IndicecorrecaoRESTModel[] indicecorrecaoRESTModelArray;
    private MaterialRESTModel[] materialRESTModelArray;
    private MaterialgrupoRESTModel[] materialgrupoRESTModelArray;
    private MaterialrelacionadoRESTModel[] materialrelacionadoRESTModelArray;
    private MaterialsimilarRESTModel[] materialsimilarRESTModelArray;
    private MaterialtabelaprecoRESTModel[] materialtabelaprecoRESTModelArray;
    private MaterialtabelaprecoitemRESTModel[] materialtabelaprecoitemRESTModelArray;
    private MaterialtabelaprecoclienteRESTModel[] materialtabelaprecoclienteRESTModelArray;
    private MaterialtabelaprecoempresaRESTModel[] materialtabelaprecoempresaRESTModelArray;
    private MaterialunidademedidaRESTModel[] materialunidademedidaRESTModelArray;
    private MunicipioRESTModel[] municipioRESTModelArray;
    private ParametrogeralRESTModel[] parametrogeralRESTModelArray;
    private PedidovendatipoRESTModel[] pedidovendatipoRESTModelArray;
    private PrazopagamentoRESTModel[] prazopagamentoRESTModelArray;
    private PrazopagamentoitemRESTModel[] prazopagamentoitemRESTModelArray;
    private ProjetoRESTModel[] projetoRESTModelArray;
    private ResponsavelFreteRESTModel[] responsavelFreteRESTModelArray;
    private BancoRESTModel[] bancoRESTModelArray;
    private UfRESTModel[] ufRESTModelArray;
    private UnidademedidaRESTModel[] unidademedidaRESTModelArray;
    private ValecompraRESTModel[] valecompraRESTModelArray;
    private UsuarioempresaRESTModel[] usuarioempresaRESTModelArray;
    private ClientevendedorRESTModel[] clientevendedorRESTModelArray;
    private PedidovendahistoricoRESTModel[] pedidovendahistoricoRESTModelArray;
    private VwultimascomprasandroidRESTModel[] vwultimascomprasandroidRESTModelArray;
    private UsuarioRESTModel[] usuarioRESTModelArray;
    private AtividadetipoRESTModel[] atividadetipoRESTModelArray;
    private ClientedocumentotipoRESTModel[] clientedocumentotipoRESTModelArray;
    private ClientehistoricoRESTModel[] clientehistoricoRESTModelArray;
    private ClienteprazopagamentoRESTModel[] clienteprazopagamentoRESTModelArray;
    private DocumentoRESTModel[] documentoRESTModelArray;
    private DocumentohistoricoRESTModel[] documentohistoricoRESTModelArray;
    private LocalarmazenagemRESTModel[] localarmazenagemRESTModelArray;
    private MeiocontatoRESTModel[] meiocontatoRESTModelArray;
    private PneuRESTModel[] pneuRESTModelArray;
    private PneumarcaRESTModel[] pneumarcaRESTModelArray;
    private PneumedidaRESTModel[] pneumedidaRESTModelArray;
    private PneumodeloRESTModel[] pneumodeloRESTModelArray;
    private SituacaohistoricoRESTModel[] situacaohistoricoRESTModelArray;
    private LocalarmazenagemempresaRESTModel[] localarmazenagemempresaRESTModelArray;
    private MaterialproducaoRESTModel[] materialproducaoRESTModelArray;
    private MaterialformulavalorvendaRESTModel[] materialformulavalorvendaRESTModelArray;
    private TabelavalorRESTModel[] tabelavalorRESTModelArray;
    private MaterialEmpresaRESTModel[] materialEmpresaRESTModel;

    
    private String whereInExcluidoCategoria;
    private String whereInExcluidoCliente;
    private String whereInExcluidoColaborador;
    private String whereInExcluidoConta;
    private String whereInExcluidoContatipo;
    private String whereInExcluidoContato;
    private String whereInExcluidoContatotipo;
    private String whereInExcluidoDocumentotipo;
    private String whereInExcluidoEmpresa;
    private String whereInExcluidoContaempresa;
    private String whereInExcluidoEndereco;
    private String whereInExcluidoEnderecotipo;
    private String whereInExcluidoFormapagamento;
    private String whereInExcluidoFornecedor;
    private String whereInExcluidoIndicecorrecao;
    private String whereInExcluidoMaterial;
    private String whereInExcluidoMaterialgrupo;
    private String whereInExcluidoMaterialrelacionado;
    private String whereInExcluidoMaterialsimilar;
    private String whereInExcluidoMaterialtabelapreco;
    private String whereInExcluidoMaterialtabelaprecoitem;
    private String whereInExcluidoMaterialtabelaprecocliente;
    private String whereInExcluidoMaterialtabelaprecoempresa;
    private String whereInExcluidoMaterialunidademedida;
    private String whereInExcluidoMunicipio;
    private String whereInExcluidoPedidovendatipo;
    private String whereInExcluidoPrazopagamento;
    private String whereInExcluidoPrazopagamentoitem;
    private String whereInExcluidoProjeto;
    private String whereInExcluidoResponsavelFrete;
    private String whereInExcluidoBanco;
    private String whereInExcluidoUf;
    private String whereInExcluidoUnidademedida;
    private String whereInExcluidoValecompra;
    private String whereInExcluidoUsuarioempresa;
    private String whereInExcluidoPedidovendahistorico;
    private String whereInExcluidoClientevendedor;
    private String whereInExcluidoAtividadetipo;
    private String whereInExcluidoClientedocumentotipo;
    private String whereInExcluidoClientehistorico;
    private String whereInExcluidoClienteprazopagamento;
    private String whereInExcluidoDocumento;
    private String whereInExcluidoDocumentohistorico;
    private String whereInExcluidoLocalarmazenagem;
    private String whereInExcluidoMeiocontato;
    private String whereInExcluidoPneu;
    private String whereInExcluidoPneumarca;
    private String whereInExcluidoPneumedida;
    private String whereInExcluidoPneumodelo;
    private String whereInExcluidoSituacaohistorico;
    private String whereInExcluidoLocalarmazenagemempresa;
    private String whereInExcluidoMaterialproducao;
    private String whereInExcluidoMaterialformulavalorvenda;
    private String whereInExcluidoTabelavalor;
    private String whereInExcluidoMaterialEmpresa;
    
    public Long getDataUltimaSincronizacao() {
		return dataUltimaSincronizacao;
	}
	public void setDataUltimaSincronizacao(Long dataUltimaSincronizacao) {
		this.dataUltimaSincronizacao = dataUltimaSincronizacao;
	}
	
	public CategoriaRESTModel[] getCategoriaRESTModelArray() {
		return categoriaRESTModelArray;
	}
	public ClienteRESTModel[] getClienteRESTModelArray() {
		return clienteRESTModelArray;
	}
	public ColaboradorRESTModel[] getColaboradorRESTModelArray() {
		return colaboradorRESTModelArray;
	}
	public ContaRESTModel[] getContaRESTModelArray() {
		return contaRESTModelArray;
	}
	public ContatoRESTModel[] getContatoRESTModelArray() {
		return contatoRESTModelArray;
	}
	public ContatotipoRESTModel[] getContatotipoRESTModelArray() {
		return contatotipoRESTModelArray;
	}
	public DocumentotipoRESTModel[] getDocumentotipoRESTModelArray() {
		return documentotipoRESTModelArray;
	}
	public EmpresaRESTModel[] getEmpresaRESTModelArray() {
		return empresaRESTModelArray;
	}
	public ContaempresaRESTModel[] getContaempresaRESTModelArray() {
		return contaempresaRESTModelArray;
	}
	public EnderecoRESTModel[] getEnderecoRESTModelArray() {
		return enderecoRESTModelArray;
	}
	public EnderecotipoRESTModel[] getEnderecotipoRESTModelArray() {
		return enderecotipoRESTModelArray;
	}
	public FormapagamentoRESTModel[] getFormapagamentoRESTModelArray() {
		return formapagamentoRESTModelArray;
	}
	public FornecedorRESTModel[] getFornecedorRESTModelArray() {
		return fornecedorRESTModelArray;
	}
	public IndicecorrecaoRESTModel[] getIndicecorrecaoRESTModelArray() {
		return indicecorrecaoRESTModelArray;
	}
	public MaterialRESTModel[] getMaterialRESTModelArray() {
		return materialRESTModelArray;
	}
	public MaterialgrupoRESTModel[] getMaterialgrupoRESTModelArray() {
		return materialgrupoRESTModelArray;
	}
	public MaterialrelacionadoRESTModel[] getMaterialrelacionadoRESTModelArray() {
		return materialrelacionadoRESTModelArray;
	}
	public MaterialsimilarRESTModel[] getMaterialsimilarRESTModelArray() {
		return materialsimilarRESTModelArray;
	}
	public MaterialtabelaprecoRESTModel[] getMaterialtabelaprecoRESTModelArray() {
		return materialtabelaprecoRESTModelArray;
	}
	public MaterialtabelaprecoitemRESTModel[] getMaterialtabelaprecoitemRESTModelArray() {
		return materialtabelaprecoitemRESTModelArray;
	}
	public MaterialunidademedidaRESTModel[] getMaterialunidademedidaRESTModelArray() {
		return materialunidademedidaRESTModelArray;
	}
	public MunicipioRESTModel[] getMunicipioRESTModelArray() {
		return municipioRESTModelArray;
	}
	public ParametrogeralRESTModel[] getParametrogeralRESTModelArray() {
		return parametrogeralRESTModelArray;
	}
	public PedidovendatipoRESTModel[] getPedidovendatipoRESTModelArray() {
		return pedidovendatipoRESTModelArray;
	}
	public PrazopagamentoRESTModel[] getPrazopagamentoRESTModelArray() {
		return prazopagamentoRESTModelArray;
	}
	public PrazopagamentoitemRESTModel[] getPrazopagamentoitemRESTModelArray() {
		return prazopagamentoitemRESTModelArray;
	}
	public ProjetoRESTModel[] getProjetoRESTModelArray() {
		return projetoRESTModelArray;
	}
	public ResponsavelFreteRESTModel[] getResponsavelFreteRESTModelArray() {
		return responsavelFreteRESTModelArray;
	}
	public UfRESTModel[] getUfRESTModelArray() {
		return ufRESTModelArray;
	}
	public String getWhereInExcluidoMaterialEmpresa() {
		return whereInExcluidoMaterialEmpresa;
	}
	public UnidademedidaRESTModel[] getUnidademedidaRESTModelArray() {
		return unidademedidaRESTModelArray;
	}
	public ValecompraRESTModel[] getValecompraRESTModelArray() {
		return valecompraRESTModelArray;
	}
	public void setCategoriaRESTModelArray(
			CategoriaRESTModel[] categoriaRESTModelArray) {
		this.categoriaRESTModelArray = categoriaRESTModelArray;
	}
	public void setClienteRESTModelArray(ClienteRESTModel[] clienteRESTModelArray) {
		this.clienteRESTModelArray = clienteRESTModelArray;
	}
	public void setColaboradorRESTModelArray(
			ColaboradorRESTModel[] colaboradorRESTModelArray) {
		this.colaboradorRESTModelArray = colaboradorRESTModelArray;
	}
	public void setContaRESTModelArray(ContaRESTModel[] contaRESTModelArray) {
		this.contaRESTModelArray = contaRESTModelArray;
	}
	public void setContatoRESTModelArray(ContatoRESTModel[] contatoRESTModelArray) {
		this.contatoRESTModelArray = contatoRESTModelArray;
	}
	public void setContatotipoRESTModelArray(
			ContatotipoRESTModel[] contatotipoRESTModelArray) {
		this.contatotipoRESTModelArray = contatotipoRESTModelArray;
	}
	public void setDocumentotipoRESTModelArray(
			DocumentotipoRESTModel[] documentotipoRESTModelArray) {
		this.documentotipoRESTModelArray = documentotipoRESTModelArray;
	}
	public void setEmpresaRESTModelArray(EmpresaRESTModel[] empresaRESTModelArray) {
		this.empresaRESTModelArray = empresaRESTModelArray;
	}
	public void setContaempresaRESTModelArray(ContaempresaRESTModel[] contaempresaRESTModelArray) {
		this.contaempresaRESTModelArray = contaempresaRESTModelArray;
	}
	public void setEnderecoRESTModelArray(EnderecoRESTModel[] enderecoRESTModelArray) {
		this.enderecoRESTModelArray = enderecoRESTModelArray;
	}
	public void setEnderecotipoRESTModelArray(
			EnderecotipoRESTModel[] enderecotipoRESTModelArray) {
		this.enderecotipoRESTModelArray = enderecotipoRESTModelArray;
	}
	public void setFormapagamentoRESTModelArray(
			FormapagamentoRESTModel[] formapagamentoRESTModelArray) {
		this.formapagamentoRESTModelArray = formapagamentoRESTModelArray;
	}
	public void setFornecedorRESTModelArray(
			FornecedorRESTModel[] fornecedorRESTModelArray) {
		this.fornecedorRESTModelArray = fornecedorRESTModelArray;
	}
	public void setIndicecorrecaoRESTModelArray(
			IndicecorrecaoRESTModel[] indicecorrecaoRESTModelArray) {
		this.indicecorrecaoRESTModelArray = indicecorrecaoRESTModelArray;
	}
	public void setMaterialRESTModelArray(MaterialRESTModel[] materialRESTModelArray) {
		this.materialRESTModelArray = materialRESTModelArray;
	}
	public void setMaterialgrupoRESTModelArray(
			MaterialgrupoRESTModel[] materialgrupoRESTModelArray) {
		this.materialgrupoRESTModelArray = materialgrupoRESTModelArray;
	}
	public void setMaterialrelacionadoRESTModelArray(
			MaterialrelacionadoRESTModel[] materialrelacionadoRESTModelArray) {
		this.materialrelacionadoRESTModelArray = materialrelacionadoRESTModelArray;
	}
	public void setMaterialsimilarRESTModelArray(
			MaterialsimilarRESTModel[] materialsimilarRESTModelArray) {
		this.materialsimilarRESTModelArray = materialsimilarRESTModelArray;
	}
	public void setMaterialtabelaprecoRESTModelArray(
			MaterialtabelaprecoRESTModel[] materialtabelaprecoRESTModelArray) {
		this.materialtabelaprecoRESTModelArray = materialtabelaprecoRESTModelArray;
	}
	public void setMaterialtabelaprecoitemRESTModelArray(
			MaterialtabelaprecoitemRESTModel[] materialtabelaprecoitemRESTModelArray) {
		this.materialtabelaprecoitemRESTModelArray = materialtabelaprecoitemRESTModelArray;
	}
	public void setMaterialunidademedidaRESTModelArray(
			MaterialunidademedidaRESTModel[] materialunidademedidaRESTModelArray) {
		this.materialunidademedidaRESTModelArray = materialunidademedidaRESTModelArray;
	}
	public void setMunicipioRESTModelArray(
			MunicipioRESTModel[] municipioRESTModelArray) {
		this.municipioRESTModelArray = municipioRESTModelArray;
	}
	public void setParametrogeralRESTModelArray(
			ParametrogeralRESTModel[] parametrogeralRESTModelArray) {
		this.parametrogeralRESTModelArray = parametrogeralRESTModelArray;
	}
	public void setPedidovendatipoRESTModelArray(
			PedidovendatipoRESTModel[] pedidovendatipoRESTModelArray) {
		this.pedidovendatipoRESTModelArray = pedidovendatipoRESTModelArray;
	}
	public void setPrazopagamentoRESTModelArray(
			PrazopagamentoRESTModel[] prazopagamentoRESTModelArray) {
		this.prazopagamentoRESTModelArray = prazopagamentoRESTModelArray;
	}
	public void setPrazopagamentoitemRESTModelArray(
			PrazopagamentoitemRESTModel[] prazopagamentoitemRESTModelArray) {
		this.prazopagamentoitemRESTModelArray = prazopagamentoitemRESTModelArray;
	}
	public void setProjetoRESTModelArray(ProjetoRESTModel[] projetoRESTModelArray) {
		this.projetoRESTModelArray = projetoRESTModelArray;
	}
	public void setResponsavelFreteRESTModelArray(
			ResponsavelFreteRESTModel[] responsavelFreteRESTModelArray) {
		this.responsavelFreteRESTModelArray = responsavelFreteRESTModelArray;
	}
	public void setUfRESTModelArray(UfRESTModel[] ufRESTModelArray) {
		this.ufRESTModelArray = ufRESTModelArray;
	}
	public void setUnidademedidaRESTModelArray(
			UnidademedidaRESTModel[] unidademedidaRESTModelArray) {
		this.unidademedidaRESTModelArray = unidademedidaRESTModelArray;
	}
	public void setValecompraRESTModelArray(
			ValecompraRESTModel[] valecompraRESTModelArray) {
		this.valecompraRESTModelArray = valecompraRESTModelArray;
	}
	public MaterialtabelaprecoclienteRESTModel[] getMaterialtabelaprecoclienteRESTModelArray() {
		return materialtabelaprecoclienteRESTModelArray;
	}
	public BancoRESTModel[] getBancoRESTModelArray() {
		return bancoRESTModelArray;
	}
	public MaterialtabelaprecoempresaRESTModel[] getMaterialtabelaprecoempresaRESTModelArray() {
		return materialtabelaprecoempresaRESTModelArray;
	}
	public String getWhereInExcluidoMaterialtabelaprecoempresa() {
		return whereInExcluidoMaterialtabelaprecoempresa;
	}
	public void setMaterialtabelaprecoempresaRESTModelArray(
			MaterialtabelaprecoempresaRESTModel[] materialtabelaprecoempresaRESTModelArray) {
		this.materialtabelaprecoempresaRESTModelArray = materialtabelaprecoempresaRESTModelArray;
	}
	public void setWhereInExcluidoMaterialtabelaprecoempresa(
			String whereInExcluidoMaterialtabelaprecoempresa) {
		this.whereInExcluidoMaterialtabelaprecoempresa = whereInExcluidoMaterialtabelaprecoempresa;
	}
	public void setMaterialtabelaprecoclienteRESTModelArray(
			MaterialtabelaprecoclienteRESTModel[] materialtabelaprecoclienteRESTModelArray) {
		this.materialtabelaprecoclienteRESTModelArray = materialtabelaprecoclienteRESTModelArray;
	}
	public void setBancoRESTModelArray(BancoRESTModel[] bancoRESTModelArray) {
		this.bancoRESTModelArray = bancoRESTModelArray;
	}
	public String getWhereInExcluidoCategoria() {
		return whereInExcluidoCategoria;
	}
	public String getWhereInExcluidoCliente() {
		return whereInExcluidoCliente;
	}
	public String getWhereInExcluidoColaborador() {
		return whereInExcluidoColaborador;
	}
	public String getWhereInExcluidoConta() {
		return whereInExcluidoConta;
	}
	public String getWhereInExcluidoContato() {
		return whereInExcluidoContato;
	}
	public String getWhereInExcluidoContatotipo() {
		return whereInExcluidoContatotipo;
	}
	public String getWhereInExcluidoDocumentotipo() {
		return whereInExcluidoDocumentotipo;
	}
	public String getWhereInExcluidoEmpresa() {
		return whereInExcluidoEmpresa;
	}
	public String getWhereInExcluidoContaempresa() {
		return whereInExcluidoContaempresa;
	}
	public String getWhereInExcluidoEndereco() {
		return whereInExcluidoEndereco;
	}
	public String getWhereInExcluidoEnderecotipo() {
		return whereInExcluidoEnderecotipo;
	}
	public String getWhereInExcluidoFormapagamento() {
		return whereInExcluidoFormapagamento;
	}
	public String getWhereInExcluidoFornecedor() {
		return whereInExcluidoFornecedor;
	}
	public String getWhereInExcluidoIndicecorrecao() {
		return whereInExcluidoIndicecorrecao;
	}
	public String getWhereInExcluidoMaterial() {
		return whereInExcluidoMaterial;
	}
	public String getWhereInExcluidoMaterialgrupo() {
		return whereInExcluidoMaterialgrupo;
	}
	public String getWhereInExcluidoMaterialrelacionado() {
		return whereInExcluidoMaterialrelacionado;
	}
	public String getWhereInExcluidoMaterialsimilar() {
		return whereInExcluidoMaterialsimilar;
	}
	public String getWhereInExcluidoMaterialtabelapreco() {
		return whereInExcluidoMaterialtabelapreco;
	}
	public String getWhereInExcluidoMaterialtabelaprecoitem() {
		return whereInExcluidoMaterialtabelaprecoitem;
	}
	public String getWhereInExcluidoMaterialtabelaprecocliente() {
		return whereInExcluidoMaterialtabelaprecocliente;
	}
	public String getWhereInExcluidoMaterialunidademedida() {
		return whereInExcluidoMaterialunidademedida;
	}
	public String getWhereInExcluidoMunicipio() {
		return whereInExcluidoMunicipio;
	}
	public String getWhereInExcluidoPedidovendatipo() {
		return whereInExcluidoPedidovendatipo;
	}
	public String getWhereInExcluidoPrazopagamento() {
		return whereInExcluidoPrazopagamento;
	}
	public String getWhereInExcluidoPrazopagamentoitem() {
		return whereInExcluidoPrazopagamentoitem;
	}
	public String getWhereInExcluidoProjeto() {
		return whereInExcluidoProjeto;
	}
	public String getWhereInExcluidoResponsavelFrete() {
		return whereInExcluidoResponsavelFrete;
	}
	public String getWhereInExcluidoBanco() {
		return whereInExcluidoBanco;
	}
	public String getWhereInExcluidoUf() {
		return whereInExcluidoUf;
	}
	public String getWhereInExcluidoUnidademedida() {
		return whereInExcluidoUnidademedida;
	}
	public String getWhereInExcluidoValecompra() {
		return whereInExcluidoValecompra;
	}
	public void setWhereInExcluidoCategoria(String whereInExcluidoCategoria) {
		this.whereInExcluidoCategoria = whereInExcluidoCategoria;
	}
	public void setWhereInExcluidoCliente(String whereInExcluidoCliente) {
		this.whereInExcluidoCliente = whereInExcluidoCliente;
	}
	public void setWhereInExcluidoColaborador(String whereInExcluidoColaborador) {
		this.whereInExcluidoColaborador = whereInExcluidoColaborador;
	}
	public void setWhereInExcluidoConta(String whereInExcluidoConta) {
		this.whereInExcluidoConta = whereInExcluidoConta;
	}
	public void setWhereInExcluidoContato(String whereInExcluidoContato) {
		this.whereInExcluidoContato = whereInExcluidoContato;
	}
	public void setWhereInExcluidoContatotipo(String whereInExcluidoContatotipo) {
		this.whereInExcluidoContatotipo = whereInExcluidoContatotipo;
	}
	public void setWhereInExcluidoDocumentotipo(String whereInExcluidoDocumentotipo) {
		this.whereInExcluidoDocumentotipo = whereInExcluidoDocumentotipo;
	}
	public void setWhereInExcluidoEmpresa(String whereInExcluidoEmpresa) {
		this.whereInExcluidoEmpresa = whereInExcluidoEmpresa;
	}
	public void setWhereInExcluidoContaempresa(String whereInExcluidoContaempresa) {
		this.whereInExcluidoContaempresa = whereInExcluidoContaempresa;
	}
	public void setWhereInExcluidoEndereco(String whereInExcluidoEndereco) {
		this.whereInExcluidoEndereco = whereInExcluidoEndereco;
	}
	public void setWhereInExcluidoEnderecotipo(String whereInExcluidoEnderecotipo) {
		this.whereInExcluidoEnderecotipo = whereInExcluidoEnderecotipo;
	}
	public void setWhereInExcluidoFormapagamento(
			String whereInExcluidoFormapagamento) {
		this.whereInExcluidoFormapagamento = whereInExcluidoFormapagamento;
	}
	public void setWhereInExcluidoFornecedor(String whereInExcluidoFornecedor) {
		this.whereInExcluidoFornecedor = whereInExcluidoFornecedor;
	}
	public void setWhereInExcluidoIndicecorrecao(
			String whereInExcluidoIndicecorrecao) {
		this.whereInExcluidoIndicecorrecao = whereInExcluidoIndicecorrecao;
	}
	public void setWhereInExcluidoMaterial(String whereInExcluidoMaterial) {
		this.whereInExcluidoMaterial = whereInExcluidoMaterial;
	}
	public void setWhereInExcluidoMaterialgrupo(String whereInExcluidoMaterialgrupo) {
		this.whereInExcluidoMaterialgrupo = whereInExcluidoMaterialgrupo;
	}
	public void setWhereInExcluidoMaterialrelacionado(
			String whereInExcluidoMaterialrelacionado) {
		this.whereInExcluidoMaterialrelacionado = whereInExcluidoMaterialrelacionado;
	}
	public void setWhereInExcluidoMaterialsimilar(
			String whereInExcluidoMaterialsimilar) {
		this.whereInExcluidoMaterialsimilar = whereInExcluidoMaterialsimilar;
	}
	public void setWhereInExcluidoMaterialtabelapreco(
			String whereInExcluidoMaterialtabelapreco) {
		this.whereInExcluidoMaterialtabelapreco = whereInExcluidoMaterialtabelapreco;
	}
	public void setWhereInExcluidoMaterialtabelaprecoitem(
			String whereInExcluidoMaterialtabelaprecoitem) {
		this.whereInExcluidoMaterialtabelaprecoitem = whereInExcluidoMaterialtabelaprecoitem;
	}
	public void setWhereInExcluidoMaterialtabelaprecocliente(
			String whereInExcluidoMaterialtabelaprecocliente) {
		this.whereInExcluidoMaterialtabelaprecocliente = whereInExcluidoMaterialtabelaprecocliente;
	}
	public void setWhereInExcluidoMaterialunidademedida(
			String whereInExcluidoMaterialunidademedida) {
		this.whereInExcluidoMaterialunidademedida = whereInExcluidoMaterialunidademedida;
	}
	public void setWhereInExcluidoMunicipio(String whereInExcluidoMunicipio) {
		this.whereInExcluidoMunicipio = whereInExcluidoMunicipio;
	}
	public void setWhereInExcluidoPedidovendatipo(
			String whereInExcluidoPedidovendatipo) {
		this.whereInExcluidoPedidovendatipo = whereInExcluidoPedidovendatipo;
	}
	public void setWhereInExcluidoPrazopagamento(
			String whereInExcluidoPrazopagamento) {
		this.whereInExcluidoPrazopagamento = whereInExcluidoPrazopagamento;
	}
	public void setWhereInExcluidoPrazopagamentoitem(
			String whereInExcluidoPrazopagamentoitem) {
		this.whereInExcluidoPrazopagamentoitem = whereInExcluidoPrazopagamentoitem;
	}
	public void setWhereInExcluidoProjeto(String whereInExcluidoProjeto) {
		this.whereInExcluidoProjeto = whereInExcluidoProjeto;
	}
	public void setWhereInExcluidoResponsavelFrete(
			String whereInExcluidoResponsavelFrete) {
		this.whereInExcluidoResponsavelFrete = whereInExcluidoResponsavelFrete;
	}
	public void setWhereInExcluidoBanco(String whereInExcluidoBanco) {
		this.whereInExcluidoBanco = whereInExcluidoBanco;
	}
	public void setWhereInExcluidoUf(String whereInExcluidoUf) {
		this.whereInExcluidoUf = whereInExcluidoUf;
	}
	public void setWhereInExcluidoUnidademedida(String whereInExcluidoUnidademedida) {
		this.whereInExcluidoUnidademedida = whereInExcluidoUnidademedida;
	}
	public void setWhereInExcluidoValecompra(String whereInExcluidoValecompra) {
		this.whereInExcluidoValecompra = whereInExcluidoValecompra;
	}
	public UsuarioempresaRESTModel[] getUsuarioempresaRESTModelArray() {
		return usuarioempresaRESTModelArray;
	}
	public String getWhereInExcluidoUsuarioempresa() {
		return whereInExcluidoUsuarioempresa;
	}
	public void setUsuarioempresaRESTModelArray(
			UsuarioempresaRESTModel[] usuarioempresaRESTModelArray) {
		this.usuarioempresaRESTModelArray = usuarioempresaRESTModelArray;
	}
	public void setWhereInExcluidoUsuarioempresa(
			String whereInExcluidoUsuarioempresa) {
		this.whereInExcluidoUsuarioempresa = whereInExcluidoUsuarioempresa;
	}
	public ClientevendedorRESTModel[] getClientevendedorRESTModelArray() {
		return clientevendedorRESTModelArray;
	}
	public PedidovendahistoricoRESTModel[] getPedidovendahistoricoRESTModelArray() {
		return pedidovendahistoricoRESTModelArray;
	}
	public VwultimascomprasandroidRESTModel[] getVwultimascomprasandroidRESTModelArray() {
		return vwultimascomprasandroidRESTModelArray;
	}
	
	public String getWhereInExcluidoPedidovendahistorico() {
		return whereInExcluidoPedidovendahistorico;
	}
	public String getWhereInExcluidoClientevendedor() {
		return whereInExcluidoClientevendedor;
	}
	public void setClientevendedorRESTModelArray(
			ClientevendedorRESTModel[] clientevendedorRESTModelArray) {
		this.clientevendedorRESTModelArray = clientevendedorRESTModelArray;
	}
	public void setPedidovendahistoricoRESTModelArray(
			PedidovendahistoricoRESTModel[] pedidovendahistoricoRESTModelArray) {
		this.pedidovendahistoricoRESTModelArray = pedidovendahistoricoRESTModelArray;
	}
	public void setVwultimascomprasandroidRESTModelArray(
			VwultimascomprasandroidRESTModel[] vwultimascomprasandroidRESTModelArray) {
		this.vwultimascomprasandroidRESTModelArray = vwultimascomprasandroidRESTModelArray;
	}
	public void setWhereInExcluidoPedidovendahistorico(
			String whereInExcluidoPedidovendahistorico) {
		this.whereInExcluidoPedidovendahistorico = whereInExcluidoPedidovendahistorico;
	}
	public void setWhereInExcluidoClientevendedor(
			String whereInExcluidoClientevendedor) {
		this.whereInExcluidoClientevendedor = whereInExcluidoClientevendedor;
	}
	public ContatipoRESTModel[] getContatipoRESTModelArray() {
		return contatipoRESTModelArray;
	}
	public String getWhereInExcluidoContatipo() {
		return whereInExcluidoContatipo;
	}
	public void setContatipoRESTModelArray(
			ContatipoRESTModel[] contatipoRESTModelArray) {
		this.contatipoRESTModelArray = contatipoRESTModelArray;
	}
	public void setWhereInExcluidoContatipo(String whereInExcluidoContatipo) {
		this.whereInExcluidoContatipo = whereInExcluidoContatipo;
	}
	
	public UsuarioRESTModel[] getUsuarioRESTModelArray() {
		return usuarioRESTModelArray;
	}
	public void setUsuarioRESTModelArray(UsuarioRESTModel[] usuarioRESTModelArray) {
		this.usuarioRESTModelArray = usuarioRESTModelArray;
	}
	public AtividadetipoRESTModel[] getAtividadetipoRESTModelArray() {
		return atividadetipoRESTModelArray;
	}
	public ClientedocumentotipoRESTModel[] getClientedocumentotipoRESTModelArray() {
		return clientedocumentotipoRESTModelArray;
	}
	public ClientehistoricoRESTModel[] getClientehistoricoRESTModelArray() {
		return clientehistoricoRESTModelArray;
	}
	public ClienteprazopagamentoRESTModel[] getClienteprazopagamentoRESTModelArray() {
		return clienteprazopagamentoRESTModelArray;
	}
	public DocumentoRESTModel[] getDocumentoRESTModelArray() {
		return documentoRESTModelArray;
	}
	public DocumentohistoricoRESTModel[] getDocumentohistoricoRESTModelArray() {
		return documentohistoricoRESTModelArray;
	}
	public LocalarmazenagemRESTModel[] getLocalarmazenagemRESTModelArray() {
		return localarmazenagemRESTModelArray;
	}
	public MeiocontatoRESTModel[] getMeiocontatoRESTModelArray() {
		return meiocontatoRESTModelArray;
	}
	public PneuRESTModel[] getPneuRESTModelArray() {
		return pneuRESTModelArray;
	}
	public PneumarcaRESTModel[] getPneumarcaRESTModelArray() {
		return pneumarcaRESTModelArray;
	}
	public PneumedidaRESTModel[] getPneumedidaRESTModelArray() {
		return pneumedidaRESTModelArray;
	}
	public PneumodeloRESTModel[] getPneumodeloRESTModelArray() {
		return pneumodeloRESTModelArray;
	}
	public SituacaohistoricoRESTModel[] getSituacaohistoricoRESTModelArray() {
		return situacaohistoricoRESTModelArray;
	}
	public String getWhereInExcluidoAtividadetipo() {
		return whereInExcluidoAtividadetipo;
	}
	public String getWhereInExcluidoClientedocumentotipo() {
		return whereInExcluidoClientedocumentotipo;
	}
	public String getWhereInExcluidoClientehistorico() {
		return whereInExcluidoClientehistorico;
	}
	public String getWhereInExcluidoClienteprazopagamento() {
		return whereInExcluidoClienteprazopagamento;
	}
	public String getWhereInExcluidoDocumento() {
		return whereInExcluidoDocumento;
	}
	public String getWhereInExcluidoDocumentohistorico() {
		return whereInExcluidoDocumentohistorico;
	}
	public String getWhereInExcluidoLocalarmazenagem() {
		return whereInExcluidoLocalarmazenagem;
	}
	public String getWhereInExcluidoMeiocontato() {
		return whereInExcluidoMeiocontato;
	}
	public String getWhereInExcluidoPneu() {
		return whereInExcluidoPneu;
	}
	public String getWhereInExcluidoPneumarca() {
		return whereInExcluidoPneumarca;
	}
	public String getWhereInExcluidoPneumedida() {
		return whereInExcluidoPneumedida;
	}
	public String getWhereInExcluidoPneumodelo() {
		return whereInExcluidoPneumodelo;
	}
	public String getWhereInExcluidoSituacaohistorico() {
		return whereInExcluidoSituacaohistorico;
	}
	public MaterialEmpresaRESTModel[] getMaterialEmpresaRESTModel() {
		return materialEmpresaRESTModel;
	}
	public void setAtividadetipoRESTModelArray(
			AtividadetipoRESTModel[] atividadetipoRESTModelArray) {
		this.atividadetipoRESTModelArray = atividadetipoRESTModelArray;
	}
	public void setClientedocumentotipoRESTModelArray(
			ClientedocumentotipoRESTModel[] clientedocumentotipoRESTModelArray) {
		this.clientedocumentotipoRESTModelArray = clientedocumentotipoRESTModelArray;
	}
	public void setClientehistoricoRESTModelArray(
			ClientehistoricoRESTModel[] clientehistoricoRESTModelArray) {
		this.clientehistoricoRESTModelArray = clientehistoricoRESTModelArray;
	}
	public void setClienteprazopagamentoRESTModelArray(
			ClienteprazopagamentoRESTModel[] clienteprazopagamentoRESTModelArray) {
		this.clienteprazopagamentoRESTModelArray = clienteprazopagamentoRESTModelArray;
	}
	public void setDocumentoRESTModelArray(
			DocumentoRESTModel[] documentoRESTModelArray) {
		this.documentoRESTModelArray = documentoRESTModelArray;
	}
	public void setDocumentohistoricoRESTModelArray(
			DocumentohistoricoRESTModel[] documentohistoricoRESTModelArray) {
		this.documentohistoricoRESTModelArray = documentohistoricoRESTModelArray;
	}
	public void setLocalarmazenagemRESTModelArray(
			LocalarmazenagemRESTModel[] localarmazenagemRESTModelArray) {
		this.localarmazenagemRESTModelArray = localarmazenagemRESTModelArray;
	}
	public void setMeiocontatoRESTModelArray(
			MeiocontatoRESTModel[] meiocontatoRESTModelArray) {
		this.meiocontatoRESTModelArray = meiocontatoRESTModelArray;
	}
	public void setPneuRESTModelArray(PneuRESTModel[] pneuRESTModelArray) {
		this.pneuRESTModelArray = pneuRESTModelArray;
	}
	public void setPneumarcaRESTModelArray(
			PneumarcaRESTModel[] pneumarcaRESTModelArray) {
		this.pneumarcaRESTModelArray = pneumarcaRESTModelArray;
	}
	public void setPneumedidaRESTModelArray(
			PneumedidaRESTModel[] pneumedidaRESTModelArray) {
		this.pneumedidaRESTModelArray = pneumedidaRESTModelArray;
	}
	public void setPneumodeloRESTModelArray(
			PneumodeloRESTModel[] pneumodeloRESTModelArray) {
		this.pneumodeloRESTModelArray = pneumodeloRESTModelArray;
	}
	public void setSituacaohistoricoRESTModelArray(
			SituacaohistoricoRESTModel[] situacaohistoricoRESTModelArray) {
		this.situacaohistoricoRESTModelArray = situacaohistoricoRESTModelArray;
	}
	public void setWhereInExcluidoAtividadetipo(String whereInExcluidoAtividadetipo) {
		this.whereInExcluidoAtividadetipo = whereInExcluidoAtividadetipo;
	}
	public void setWhereInExcluidoClientedocumentotipo(
			String whereInExcluidoClientedocumentotipo) {
		this.whereInExcluidoClientedocumentotipo = whereInExcluidoClientedocumentotipo;
	}
	public void setWhereInExcluidoClientehistorico(
			String whereInExcluidoClientehistorico) {
		this.whereInExcluidoClientehistorico = whereInExcluidoClientehistorico;
	}
	public void setWhereInExcluidoClienteprazopagamento(
			String whereInExcluidoClienteprazopagamento) {
		this.whereInExcluidoClienteprazopagamento = whereInExcluidoClienteprazopagamento;
	}
	public void setWhereInExcluidoDocumento(String whereInExcluidoDocumento) {
		this.whereInExcluidoDocumento = whereInExcluidoDocumento;
	}
	public void setWhereInExcluidoDocumentohistorico(
			String whereInExcluidoDocumentohistorico) {
		this.whereInExcluidoDocumentohistorico = whereInExcluidoDocumentohistorico;
	}
	public void setWhereInExcluidoLocalarmazenagem(
			String whereInExcluidoLocalarmazenagem) {
		this.whereInExcluidoLocalarmazenagem = whereInExcluidoLocalarmazenagem;
	}
	public void setWhereInExcluidoMeiocontato(String whereInExcluidoMeiocontato) {
		this.whereInExcluidoMeiocontato = whereInExcluidoMeiocontato;
	}
	public void setWhereInExcluidoPneu(String whereInExcluidoPneu) {
		this.whereInExcluidoPneu = whereInExcluidoPneu;
	}
	public void setWhereInExcluidoPneumarca(String whereInExcluidoPneumarca) {
		this.whereInExcluidoPneumarca = whereInExcluidoPneumarca;
	}
	public void setWhereInExcluidoPneumedida(String whereInExcluidoPneumedida) {
		this.whereInExcluidoPneumedida = whereInExcluidoPneumedida;
	}
	public void setWhereInExcluidoPneumodelo(String whereInExcluidoPneumodelo) {
		this.whereInExcluidoPneumodelo = whereInExcluidoPneumodelo;
	}
	public void setWhereInExcluidoSituacaohistorico(
			String whereInExcluidoSituacaohistorico) {
		this.whereInExcluidoSituacaohistorico = whereInExcluidoSituacaohistorico;
	}
	public LocalarmazenagemempresaRESTModel[] getLocalarmazenagemempresaRESTModelArray() {
		return localarmazenagemempresaRESTModelArray;
	}
	public String getWhereInExcluidoLocalarmazenagemempresa() {
		return whereInExcluidoLocalarmazenagemempresa;
	}
	public void setLocalarmazenagemempresaRESTModelArray(
			LocalarmazenagemempresaRESTModel[] localarmazenagemempresaRESTModelArray) {
		this.localarmazenagemempresaRESTModelArray = localarmazenagemempresaRESTModelArray;
	}
	public void setWhereInExcluidoLocalarmazenagemempresa(
			String whereInExcluidoLocalarmazenagemempresa) {
		this.whereInExcluidoLocalarmazenagemempresa = whereInExcluidoLocalarmazenagemempresa;
	}
	
	public MaterialproducaoRESTModel[] getMaterialproducaoRESTModelArray() {
		return materialproducaoRESTModelArray;
	}
	public String getWhereInExcluidoMaterialproducao() {
		return whereInExcluidoMaterialproducao;
	}
	
	public void setMaterialproducaoRESTModelArray(MaterialproducaoRESTModel[] materialproducaoRESTModelArray) {
		this.materialproducaoRESTModelArray = materialproducaoRESTModelArray;
	}
	public void setWhereInExcluidoMaterialproducao(String whereInExcluidoMaterialproducao) {
		this.whereInExcluidoMaterialproducao = whereInExcluidoMaterialproducao;
	}
	public MaterialformulavalorvendaRESTModel[] getMaterialformulavalorvendaRESTModelArray() {
		return materialformulavalorvendaRESTModelArray;
	}
	public String getWhereInExcluidoMaterialformulavalorvenda() {
		return whereInExcluidoMaterialformulavalorvenda;
	}
	public void setMaterialformulavalorvendaRESTModelArray(
			MaterialformulavalorvendaRESTModel[] materialformulavalorvendaRESTModelArray) {
		this.materialformulavalorvendaRESTModelArray = materialformulavalorvendaRESTModelArray;
	}
	public void setWhereInExcluidoMaterialformulavalorvenda(
			String whereInExcluidoMaterialformulavalorvenda) {
		this.whereInExcluidoMaterialformulavalorvenda = whereInExcluidoMaterialformulavalorvenda;
	}
	
	public TabelavalorRESTModel[] getTabelavalorRESTModelArray() {
		return tabelavalorRESTModelArray;
	}
	public String getWhereInExcluidoTabelavalor() {
		return whereInExcluidoTabelavalor;
	}
	public void setTabelavalorRESTModelArray(
			TabelavalorRESTModel[] tabelavalorRESTModelArray) {
		this.tabelavalorRESTModelArray = tabelavalorRESTModelArray;
	}
	public void setWhereInExcluidoTabelavalor(String whereInExcluidoTabelavalor) {
		this.whereInExcluidoTabelavalor = whereInExcluidoTabelavalor;
	}

	public void setMaterialEmpresaRESTModel(
			MaterialEmpresaRESTModel[] materialEmpresaRESTModel) {
		this.materialEmpresaRESTModel = materialEmpresaRESTModel;
	}
	public void setWhereInExcluidoMaterialEmpresa(
			String whereInExcluidoMaterialEmpresa) {
		this.whereInExcluidoMaterialEmpresa = whereInExcluidoMaterialEmpresa;
	}
	
	public Boolean getHaveDados() {
		return haveDados;
	}
	public void setHaveDados(Boolean haveDados) {
		this.haveDados = haveDados;
	}
}
