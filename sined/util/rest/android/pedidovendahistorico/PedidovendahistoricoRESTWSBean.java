package br.com.linkcom.sined.util.rest.android.pedidovendahistorico;

public class PedidovendahistoricoRESTWSBean {
	private String whereInPedidovenda;

    public String getWhereInPedidovenda() {
        return whereInPedidovenda;
    }

    public void setWhereInPedidovenda(String whereInPedidovenda) {
        this.whereInPedidovenda = whereInPedidovenda;
    }

}
