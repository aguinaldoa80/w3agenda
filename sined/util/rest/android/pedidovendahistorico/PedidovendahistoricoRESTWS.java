package br.com.linkcom.sined.util.rest.android.pedidovendahistorico;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Colaborador")
public class PedidovendahistoricoRESTWS implements SimpleRESTWS<PedidovendahistoricoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(PedidovendahistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(PedidovendahistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(PedidovendahistoricoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus(); 
		List<PedidovendahistoricoRESTModel> listaConta = PedidovendahistoricoService.getInstance().findForAndroid(null, false, command.getWhereInPedidovenda());
		modelAndStatus.setModel(listaConta);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(PedidovendahistoricoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
