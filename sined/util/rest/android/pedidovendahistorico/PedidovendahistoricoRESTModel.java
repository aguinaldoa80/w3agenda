package br.com.linkcom.sined.util.rest.android.pedidovendahistorico;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;

public class PedidovendahistoricoRESTModel {

	private Integer cdpedidovendahistorico;
    private Integer cdpedidovenda;
    private String acao;
    private String observacao;
    private Timestamp dtalteracao;
    private String nomeUsuario;

    public PedidovendahistoricoRESTModel(){}
    
    public PedidovendahistoricoRESTModel(Pedidovendahistorico bean){
    	this.cdpedidovendahistorico = bean.getCdpedidovendahistorico();
    	this.cdpedidovenda = bean.getPedidovenda()==null ? null : bean.getPedidovenda().getCdpedidovenda();
    	this.acao = bean.getAcao();
    	this.observacao = bean.getObservacao();
    	this.dtalteracao = bean.getDtaltera();
    }
    
    public Integer getCdpedidovendahistorico() {
        return cdpedidovendahistorico;
    }

    public void setCdpedidovendahistorico(Integer cdpedidovendahistorico) {
        this.cdpedidovendahistorico = cdpedidovendahistorico;
    }

    public Integer getCdpedidovenda() {
        return cdpedidovenda;
    }

    public void setCdpedidovenda(Integer cdpedidovenda) {
        this.cdpedidovenda = cdpedidovenda;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Timestamp getDtalteracao() {
        return dtalteracao;
    }

    public void setDtalteracao(Timestamp dtalteracao) {
        this.dtalteracao = dtalteracao;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }
        
}
