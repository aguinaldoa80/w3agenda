package br.com.linkcom.sined.util.rest.android.materialtabelaprecocliente;

import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;

public class MaterialtabelaprecoclienteRESTModel {

    private Integer cdmaterialtabelaprecocliente;
    private Integer cdmaterialtabelapreco;
    private Integer cdcliente;

    public MaterialtabelaprecoclienteRESTModel(){}
    
    public MaterialtabelaprecoclienteRESTModel(Materialtabelaprecocliente bean){
    	this.cdmaterialtabelaprecocliente = bean.getCdmaterialtabelaprecocliente();
    	this.cdmaterialtabelapreco = bean.getMaterialtabelapreco() != null ? 
    			bean.getMaterialtabelapreco().getCdmaterialtabelapreco() : null;
    	this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
    }

	public Integer getCdmaterialtabelaprecocliente() {
		return cdmaterialtabelaprecocliente;
	}

	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public void setCdmaterialtabelaprecocliente(Integer cdmaterialtabelaprecocliente) {
		this.cdmaterialtabelaprecocliente = cdmaterialtabelaprecocliente;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
}
