package br.com.linkcom.sined.util.rest.android.cliente;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTModel;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTModel;

public class ClienteRESTModel {
	
	private Long id;
	private Integer cdpessoa;
    private String nome;
    private String razaosocial;
    private Integer tipopessoa;
    private String cpf;
    private String cnpj;
    private String inscricaoestadual;
    private String email;
    private String telefone;
    private String observacao;
    private Boolean ativo;
    private Double creditolimitecompra;
    private Double saldovalecompra;
    private Double saldoantecipacao;
    
    private List<EnderecoRESTModel> listaEnderecoRESTModel = new ArrayList<EnderecoRESTModel>();
    private List<ContatoRESTModel> listaContatoRESTModel = new ArrayList<ContatoRESTModel>();
    
    private String erroSincronizacao;

    public ClienteRESTModel(){}
	
	public ClienteRESTModel(Cliente c){
		this.cdpessoa = c.getCdpessoa();
		this.nome = c.getNome();
		this.razaosocial = c.getRazaosocial();
		this.tipopessoa = c.getTipopessoa() != null ? c.getTipopessoa().ordinal() : null;
		this.inscricaoestadual = c.getInscricaoestadual();
		this.email = c.getEmail();
		this.observacao = c.getObservacao();
		this.ativo = Boolean.TRUE.equals(c.getAtivo());
		this.creditolimitecompra = c.getCreditolimitecompra();
		this.saldovalecompra = c.getSaldoValecompra() != null ? c.getSaldoValecompra().getValue().doubleValue() : null;
		this.saldoantecipacao = c.getSaldoAntecipacao() != null ? c.getSaldoAntecipacao().getValue().doubleValue() : null;
		
		Telefone telefonePrincipal = c.getTelefone();
		this.telefone = telefonePrincipal != null ? telefonePrincipal.getTelefone() : "";
		
		if(c.getCpf() != null)
			this.cpf = c.getCpf().getValue();
		if(c.getCnpj() != null)
			this.cnpj = c.getCnpj().getValue();
		
	}
	
    public Integer getCdpessoa() {
        return cdpessoa;
    }

    public String getNome() {
        return nome;
    }

    public String getRazaosocial() {
        return razaosocial;
    }

    public Integer getTipopessoa() {
        return tipopessoa;
    }

    public String getCpf() {
        return cpf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getInscricaoestadual() {
        return inscricaoestadual;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getObservacao() {
        return observacao;
    }
    
    public Boolean getAtivo() {
		return ativo;
	}

    public void setCdpessoa(Integer cdpessoa) {
        this.cdpessoa = cdpessoa;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setRazaosocial(String razaosocial) {
        this.razaosocial = razaosocial;
    }

    public void setTipopessoa(Integer tipopessoa) {
        this.tipopessoa = tipopessoa;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setInscricaoestadual(String inscricaoestadual) {
        this.inscricaoestadual = inscricaoestadual;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getErroSincronizacao() {
		return erroSincronizacao;
	}

	public void setErroSincronizacao(String erroSincronizacao) {
		this.erroSincronizacao = erroSincronizacao;
	}

	public List<EnderecoRESTModel> getListaEnderecoRESTModel() {
		return listaEnderecoRESTModel;
	}

	public List<ContatoRESTModel> getListaContatoRESTModel() {
		return listaContatoRESTModel;
	}

	public void setListaEnderecoRESTModel(
			List<EnderecoRESTModel> listaEnderecoRESTModel) {
		this.listaEnderecoRESTModel = listaEnderecoRESTModel;
	}

	public void setListaContatoRESTModel(
			List<ContatoRESTModel> listaContatoRESTModel) {
		this.listaContatoRESTModel = listaContatoRESTModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCreditolimitecompra() {
		return creditolimitecompra;
	}
	
	public Double getSaldovalecompra() {
		return saldovalecompra;
	}

	public void setSaldovalecompra(Double saldovalecompra) {
		this.saldovalecompra = saldovalecompra;
	}

	public void setCreditolimitecompra(Double creditolimitecompra) {
		this.creditolimitecompra = creditolimitecompra;
	}

	public Double getSaldoantecipacao() {
		return saldoantecipacao;
	}

	public void setSaldoantecipacao(Double saldoantecipacao) {
		this.saldoantecipacao = saldoantecipacao;
	}
}
