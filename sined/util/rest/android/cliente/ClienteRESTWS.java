package br.com.linkcom.sined.util.rest.android.cliente;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Cliente")
public class ClienteRESTWS implements SimpleRESTWS<ClienteRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ClienteRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ClienteRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		modelAndStatus.setModel(ClienteService.getInstance().salvarClienteAndroid(command, SinedUtil.retornaUsuarioAndroid()));
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(ClienteRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ClienteRESTModel> listaCliente = ClienteService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ClienteRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
