package br.com.linkcom.sined.util.rest.android.cliente;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTWSBean;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTWSBean;


public class ClienteRESTWSBean {

	private Long id;
	private Integer cdpessoa;
    private String nome;
    private String nomeLike;
    private String razaosocial;
    private Integer tipopessoa;
    private String cpf;
    private String cnpj;
    private String inscricaoestadual;
    private String email;
    private String telefone;
    private String observacao;
    private Boolean ativo;
	
	private List<ContatoRESTWSBean> listaContatoRESTWSBean = new ArrayList<ContatoRESTWSBean>();
    private List<EnderecoRESTWSBean> listaEnderecoRESTWSBean = new ArrayList<EnderecoRESTWSBean>();
	
    public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public String getNomeLike() {
		return nomeLike;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public Integer getTipopessoa() {
		return tipopessoa;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public String getEmail() {
		return email;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getObservacao() {
		return observacao;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public List<ContatoRESTWSBean> getListaContatoRESTWSBean() {
		return listaContatoRESTWSBean;
	}
	public List<EnderecoRESTWSBean> getListaEnderecoRESTWSBean() {
		return listaEnderecoRESTWSBean;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNomeLike(String nomeLike) {
		this.nomeLike = nomeLike;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setTipopessoa(Integer tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaContatoRESTWSBean(
			List<ContatoRESTWSBean> listaContatoRESTWSBean) {
		this.listaContatoRESTWSBean = listaContatoRESTWSBean;
	}
	public void setListaEnderecoRESTWSBean(
			List<EnderecoRESTWSBean> listaEnderecoRESTWSBean) {
		this.listaEnderecoRESTWSBean = listaEnderecoRESTWSBean;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
