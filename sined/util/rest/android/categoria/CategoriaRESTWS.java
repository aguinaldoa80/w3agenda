package br.com.linkcom.sined.util.rest.android.categoria;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Categoria")
public class CategoriaRESTWS implements SimpleRESTWS<CategoriaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(CategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(CategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(CategoriaRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<CategoriaRESTModel> listaCliente = CategoriaService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaCliente);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(CategoriaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
