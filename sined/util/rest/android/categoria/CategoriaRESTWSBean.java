package br.com.linkcom.sined.util.rest.android.categoria;



public class CategoriaRESTWSBean {

	private Integer cdcategoria;
	private Integer item;
	private String nome;
	private boolean cliente;
	private boolean fornecedor;
	private Integer cdcategoriapai;
	
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	public Integer getItem() {
		return item;
	}
	public String getNome() {
		return nome;
	}
	public boolean isCliente() {
		return cliente;
	}
	public boolean isFornecedor() {
		return fornecedor;
	}
	public Integer getCdcategoriapai() {
		return cdcategoriapai;
	}
	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCliente(boolean cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(boolean fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCdcategoriapai(Integer cdcategoriapai) {
		this.cdcategoriapai = cdcategoriapai;
	}
}
