package br.com.linkcom.sined.util.rest.android.categoria;

import br.com.linkcom.sined.geral.bean.Categoria;

public class CategoriaRESTModel {
	
	private Integer cdcategoria;
	private String nome;
	
	public CategoriaRESTModel(){}
	
	public CategoriaRESTModel(Categoria bean){
		this.cdcategoria = bean.getCdcategoria();
		this.nome = bean.getNome();
	}
	
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	public String getNome() {
		return nome;
	}
	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
