package br.com.linkcom.sined.util.rest.android.parametrogeral;

import br.com.linkcom.sined.geral.bean.Parametrogeral;

public class ParametrogeralRESTModel {
	
    private String nome;
    private String valor;
    
    public ParametrogeralRESTModel(){}
    
    public ParametrogeralRESTModel(Parametrogeral parametrogeral){
		this.nome = parametrogeral.getNome();
		this.valor = parametrogeral.getValor();
	}

	public String getNome() {
		return nome;
	}
	
	public String getValor() {
		return valor;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
