package br.com.linkcom.sined.util.rest.android.parametrogeral;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Parametrogeral")
public class ParametrogeralRESTWS implements SimpleRESTWS<ParametrogeralRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ParametrogeralRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ParametrogeralRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ParametrogeralRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ParametrogeralRESTModel> listaMaterialgrupo = ParametrogeralService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialgrupo);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ParametrogeralRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
