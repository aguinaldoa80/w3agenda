package br.com.linkcom.sined.util.rest.android.materialtabelaprecoitem;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoitemService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Materialtabelaprecoitem")
public class MaterialtabelaprecoitemRESTWS implements SimpleRESTWS<MaterialtabelaprecoitemRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(MaterialtabelaprecoitemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MaterialtabelaprecoitemRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(MaterialtabelaprecoitemRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<MaterialtabelaprecoitemRESTModel> listaMaterialtabelaprecoitem = MaterialtabelaprecoitemService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaMaterialtabelaprecoitem);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(MaterialtabelaprecoitemRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
