package br.com.linkcom.sined.util.rest.android.materialtabelaprecoitem;

import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;

public class MaterialtabelaprecoitemRESTModel {

    private Integer cdmaterialtabelaprecoitem;
    private Integer cdmaterial;
    private Integer cdmaterialtabelapreco;
    private Integer cdunidademedida;
    private Double valor;
    private Double valorvendaminimo;
    private Double valorvendamaximo;
    private Double percentualDesconto;

    public MaterialtabelaprecoitemRESTModel(){}
    
    public MaterialtabelaprecoitemRESTModel(Materialtabelaprecoitem bean){
    	this.cdmaterialtabelaprecoitem = bean.getCdmaterialtabelaprecoitem();
    	this.cdmaterial = bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null;
    	this.cdmaterialtabelapreco = bean.getMaterialtabelapreco() != null ? bean.getMaterialtabelapreco().getCdmaterialtabelapreco() : null;
    	this.cdunidademedida = bean.getUnidademedida() != null ? bean.getUnidademedida().getCdunidademedida() : null;
    	this.valor = bean.getValor();
    	this.valorvendaminimo = bean.getValorvendaminimo();
    	this.valorvendamaximo = bean.getValorvendamaximo();
    	this.percentualDesconto = bean.getPercentualdesconto();
    }

	public Integer getCdmaterialtabelaprecoitem() {
		return cdmaterialtabelaprecoitem;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Double getValor() {
		return valor;
	}

	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}

	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}

	public Double getPercentualDesconto() {
		return percentualDesconto;
	}
	
	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}
	
	public void setCdmaterialtabelaprecoitem(Integer cdmaterialtabelaprecoitem) {
		this.cdmaterialtabelaprecoitem = cdmaterialtabelaprecoitem;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}

	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}
}
