package br.com.linkcom.sined.util.rest.android.indicecorrecao;

import br.com.linkcom.sined.geral.bean.Indicecorrecao;

public class IndicecorrecaoRESTModel {

    private Integer cdindicecorrecao;
    private String descricao;
    private String sigla;

    public IndicecorrecaoRESTModel(){}
    
    public IndicecorrecaoRESTModel(Indicecorrecao bean){
    	this.cdindicecorrecao = bean.getCdindicecorrecao();
    	this.descricao = bean.getDescricao();
    	this.sigla = bean.getSigla();
    }

	public Integer getCdindicecorrecao() {
		return cdindicecorrecao;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setCdindicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
