package br.com.linkcom.sined.util.rest.android.indicecorrecao;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Indicecorrecao")
public class IndicecorrecaoRESTWS implements SimpleRESTWS<IndicecorrecaoRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(IndicecorrecaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(IndicecorrecaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(IndicecorrecaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<IndicecorrecaoRESTModel> listaIndicecorrecao = IndicecorrecaoService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaIndicecorrecao);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(IndicecorrecaoRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
