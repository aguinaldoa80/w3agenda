package br.com.linkcom.sined.util.rest.android.valecompra;

import java.util.List;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Valecompra")
public class ValecompraRESTWS implements SimpleRESTWS<ValecompraRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(ValecompraRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ValecompraRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(ValecompraRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		List<ValecompraRESTModel> listaValecompra = ValecompraService.getInstance().findForAndroid();
		modelAndStatus.setModel(listaValecompra);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus put(ValecompraRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}
}
