package br.com.linkcom.sined.util.rest.android.valecompra;


public class ValecompraRESTWSBean {

	private Integer cdvalecompra;

	public Integer getCdvalecompra() {
		return cdvalecompra;
	}

	public void setCdvalecompra(Integer cdvalecompra) {
		this.cdvalecompra = cdvalecompra;
	}
}
