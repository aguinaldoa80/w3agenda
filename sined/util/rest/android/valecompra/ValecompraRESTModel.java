package br.com.linkcom.sined.util.rest.android.valecompra;

import br.com.linkcom.sined.geral.bean.Valecompra;

public class ValecompraRESTModel {
	
	private Integer cdvalecompra;
    private Integer cdcliente;
    private Integer cdtipooperacao;
    private Double valor;

    public ValecompraRESTModel(){}
	
	public ValecompraRESTModel(Valecompra bean){
		this.cdvalecompra = bean.getCdvalecompra();
		this.cdcliente = bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null;
		this.cdtipooperacao = bean.getTipooperacao() != null ? bean.getTipooperacao().getCdtipooperacao() : null;
		this.valor = bean.getValor() != null ? bean.getValor().getValue().doubleValue() : null;
	}

	public Integer getCdvalecompra() {
		return cdvalecompra;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public Integer getCdtipooperacao() {
		return cdtipooperacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setCdvalecompra(Integer cdvalecompra) {
		this.cdvalecompra = cdvalecompra;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setCdtipooperacao(Integer cdtipooperacao) {
		this.cdtipooperacao = cdtipooperacao;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
}
