package br.com.linkcom.sined.util.rest.android.usuarioempresa;

import br.com.linkcom.sined.geral.bean.Usuarioempresa;

public class UsuarioempresaRESTModel {
	
	private Integer cdusuarioempresa;
    private Integer cdusuario;
    private Integer cdempresa;
    private String nomeempresa;
    
    public UsuarioempresaRESTModel(){}
    
    public UsuarioempresaRESTModel(Usuarioempresa bean){
		this.cdusuarioempresa = bean.getCdusuarioempresa();
		this.cdusuario = bean.getUsuario() != null ? bean.getUsuario().getCdpessoa() : null;
		if(bean.getEmpresa() != null){
			this.cdempresa =  bean.getEmpresa().getCdpessoa();
			this.nomeempresa = bean.getEmpresa().getNome();
		}
	}

	public Integer getCdusuarioempresa() {
		return cdusuarioempresa;
	}

	public Integer getCdusuario() {
		return cdusuario;
	}

	public Integer getCdempresa() {
		return cdempresa;
	}

	public void setCdusuarioempresa(Integer cdusuarioempresa) {
		this.cdusuarioempresa = cdusuarioempresa;
	}

	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}

	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}

	public String getNomeempresa() {
		return nomeempresa;
	}

	public void setNomeempresa(String nomeempresa) {
		this.nomeempresa = nomeempresa;
	}
	
	
}
