package br.com.linkcom.sined.util.rest.android.usuarioempresa;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.AuthenticatedRESTWS;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/android/Usuarioempresa")
public class UsuarioempresaRESTWS implements SimpleRESTWS<UsuarioempresaRESTWSBean>, AuthenticatedRESTWS {

	@Override
	public ModelAndStatus delete(UsuarioempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(UsuarioempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus get(UsuarioempresaRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(UsuarioempresaRESTWSBean command) {
		return null;
	}

	@Override
	public User verifyCredentials(String token) throws NotAuthenticatedException {
		return SinedUtil.verificaUsuarioAndroid(token);
	}

}
