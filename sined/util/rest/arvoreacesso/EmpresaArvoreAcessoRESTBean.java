package br.com.linkcom.sined.util.rest.arvoreacesso;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteSegmento;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.util.rest.authentication.AuthenticateByKeyBean;


public class EmpresaArvoreAcessoRESTBean extends AuthenticateByKeyBean{
	private Integer id;
	private String nome;
	private String razao_social;
	private String cnpj;
	private String site;
	private String email;
	private String segmento;
	private List<TelefoneBean> lista_telefone = new ArrayList<TelefoneBean>();
	private List<EnderecoBean> lista_endereco = new ArrayList<EnderecoBean>();
	private List<ContatoBean> lista_contato = new ArrayList<ContatoBean>();
	private List<ContratoBean> lista_contrato = new ArrayList<ContratoBean>();

	public EmpresaArvoreAcessoRESTBean(Cliente cliente) {
		this.id = cliente.getCdpessoa();
		this.nome = cliente.getNome();
		this.razao_social = StringUtils.isNotBlank(cliente.getRazaosocial()) ? cliente.getRazaosocial() : cliente.getNome();
		this.cnpj = cliente.getCnpj()!=null ? cliente.getCnpj().getValue() : null;
		this.site = cliente.getSite();
		this.email = cliente.getEmail();
		
		if(cliente.getListaClienteSegmento()!=null){
			for(ClienteSegmento clienteSegmento : cliente.getListaClienteSegmento()){
				if(clienteSegmento!=null && clienteSegmento.isPrincipal() && clienteSegmento.getSegmento()!=null){
					this.segmento = clienteSegmento.getSegmento().getNome();
					break;
				}
			}
		} 
		
		for(Contrato contrato : cliente.getListaContrato()){
			br.com.linkcom.sined.geral.bean.Fornecedor representante = null;
			if(Vendedortipo.FORNECEDOR.equals(contrato.getVendedortipo()) && contrato.getVendedor()!=null && contrato.getVendedor().getCdpessoa()!=null){
				representante = FornecedorService.getInstance().loadRepresentanteForWSArvoreAcesso(contrato.getVendedor());
			}
			lista_contrato.add(new ContratoBean(contrato, representante));
		}
		
		for(Telefone telefone : cliente.getListaTelefone()){
			lista_telefone.add(new TelefoneBean(telefone));
		}
		
		for(Endereco endereco : cliente.getListaEndereco()){
			lista_endereco.add(new EnderecoBean(endereco));
		}
		
		for(PessoaContato pessoaContato: cliente.getListaContato()){
			Contato contato = pessoaContato.getContato();
			
			lista_contato.add(new ContatoBean(contato));
		}
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public String getCnpj() {
		return cnpj;
	}

	public String getSite() {
		return site;
	}

	public String getEmail() {
		return email;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<TelefoneBean> getLista_telefone() {
		return lista_telefone;
	}
	public void setLista_telefone(List<TelefoneBean> listaTelefone) {
		lista_telefone = listaTelefone;
	}
	
	public List<ContatoBean> getLista_contato() {
		return lista_contato;
	}
	public void setLista_contato(List<ContatoBean> listaContato) {
		lista_contato = listaContato;
	}
	
	public List<ContratoBean> getLista_contrato() {
		return lista_contrato;
	}
	public void setLista_contrato(List<ContratoBean> listaContrato) {
		lista_contrato = listaContrato;
	}
	public List<EnderecoBean> getLista_endereco() {
		return lista_endereco;
	}
	public void setLista_endereco(List<EnderecoBean> listaEndereco) {
		lista_endereco = listaEndereco;
	}
	
	public String getSegmento() {
		return segmento;
	}
	
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
}
