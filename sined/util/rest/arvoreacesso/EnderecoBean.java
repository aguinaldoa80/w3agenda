package br.com.linkcom.sined.util.rest.arvoreacesso;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;

public class EnderecoBean {
	protected Integer tipo_endereco;
	protected String cep;
	protected String logradouro;
	protected String numero;
	protected String complemento;
	protected String bairro;
	protected String uf;
	protected String municipio;
	protected String caixa_postal;
	
	public EnderecoBean(Endereco endereco) {
		Integer tipo = null;
		if (endereco.getEnderecotipo().equals(Enderecotipo.UNICO)) {
			tipo = 1;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)) {
			tipo = 2;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.COBRANCA)) {
			tipo = 3;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.ENTREGA)) {
			tipo = 4;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.CORRESPONDENCIA)) {
			tipo = 5;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.INSTALACAO)) {
			tipo = 6;
		} else if (endereco.getEnderecotipo().equals(Enderecotipo.OUTRO)) {
			tipo = 7;
		} else if (endereco.getEnderecotipo() == Enderecotipo.INATIVO) {
			tipo = 8;
		}
		
		this.tipo_endereco = tipo;
		this.cep = endereco.getCep() != null? endereco.getCep().getValue(): "";
		this.logradouro = Util.strings.emptyIfNull(endereco.getLogradouro());
		this.numero = Util.strings.emptyIfNull(endereco.getNumero());
		this.complemento = Util.strings.emptyIfNull(endereco.getComplemento());
		this.bairro = Util.strings.emptyIfNull(endereco.getBairro());
		
		this.uf = "";
		this.municipio = "";
		if(endereco.getMunicipio() != null){
			this.uf = endereco.getMunicipio().getUf() != null? Util.strings.emptyIfNull(endereco.getMunicipio().getUf().getSigla()): "";
			this.municipio = Util.strings.emptyIfNull(endereco.getMunicipio().getNome());			
		}
		
		this.caixa_postal = Util.strings.emptyIfNull(endereco.getCaixapostal());
	}
	public Integer getTipo_endereco() {
		return tipo_endereco;
	}
	public String getCep() {
		return cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getUf() {
		return uf;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getCaixa_postal() {
		return caixa_postal;
	}
	public void setTipo_endereco(Integer tipo_endereco) {
		this.tipo_endereco = tipo_endereco;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setCaixa_postal(String caixa_postal) {
		this.caixa_postal = caixa_postal;
	}
}
