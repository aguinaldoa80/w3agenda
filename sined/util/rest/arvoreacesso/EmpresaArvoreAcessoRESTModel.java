package br.com.linkcom.sined.util.rest.arvoreacesso;

public class EmpresaArvoreAcessoRESTModel {

	private Integer id;
	private boolean erro;
	private String codigo_erro;
	private String mensagem;
	public Integer getId() {
		return id;
	}
	public boolean isErro() {
		return erro;
	}
	public String getCodigo_erro() {
		return codigo_erro;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setErro(boolean erro) {
		this.erro = erro;
	}
	public void setCodigo_erro(String codigo_erro) {
		this.codigo_erro = codigo_erro;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
}
