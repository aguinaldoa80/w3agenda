package br.com.linkcom.sined.util.rest.arvoreacesso;

import br.com.linkcom.sined.geral.bean.Telefone;

public class TelefoneBean {
//	protected Integer tipo_telefone;
	protected String tipo;
	protected String telefone;
	
	public TelefoneBean(Telefone telefone) {
//		Integer tipo = null;
//		if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL)) {
//			tipo =  1;
//		} else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.FAX)) {
//			tipo =  2;
//		} else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR)) {
//			tipo =  3;
//		} else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.RESIDENCIAL)) {
//			tipo =  4;
//		} else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.COMERCIAL)) {
//			tipo =  5;
//		} else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.OUTROS)) {
//			tipo =  6;
//		}
//		this.tipo_telefone = tipo;
		this.tipo = telefone.getTelefonetipo().getNome();
		this.telefone = telefone.getTelefone();
	}
	public String getTelefone() {
		return telefone;
	}
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
