package br.com.linkcom.sined.util.rest.arvoreacesso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.util.rest.authentication.AuthenticateByKeyBean;
import br.com.linkcom.util.rest.client.GenericRESTClient;
import br.com.linkcom.util.rest.client.HttpMethodEnum;
import br.com.linkcom.util.rest.client.RESTClientException;

public class EmpresaArvoreAcessoRESTClient extends GenericRESTClient {

	public EmpresaArvoreAcessoRESTModel enviaEmpresa(EmpresaArvoreAcessoRESTBean bean) throws IOException, RESTClientException, URISyntaxException{
		return (EmpresaArvoreAcessoRESTModel) post(EmpresaArvoreAcessoRESTModel.class, bean);
	}
	
	private static final String URI = "/Empresa";
	@Override
	protected URL servidor() throws MalformedURLException {
		//URL no formato parecido com http://mapaavaliacoes.com.br/Arvoreacesso/rest/
		//return new URL("http://localhost:8080/arvoreacesso/rest/");
		String url = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO);
		return new URL(url);
	}

	@Override
	public int timeout() {
		return 30;
	}

	@Override
	public URI uri() throws URISyntaxException {
		return new URI(URI);
	}
	
	@Override
    protected void beforeSend(HttpMethodEnum method, Class<?> clazz, Object param) {
    	if(!(param instanceof AuthenticateByKeyBean)){
    		throw new RuntimeException("O Bean que � enviado pelo WS precisa extender a classe AuthenticateByKeyBean.");
    	}
    	((AuthenticateByKeyBean)param).setKey( ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.CHAVE_WS_ARVOREACESSO) );
    }
	
	@Override
	protected String charset() {
		return "ISO-8859-1";
	}

}
