package br.com.linkcom.sined.util.rest.arvoreacesso;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.util.SinedUtil;

public class ContatoBean {
	private String nome;
	private String tipo_contato;
	private String email;
	private String observacao;
	private String cargo;
	private List<TelefoneBean> lista_telefone = new ArrayList<TelefoneBean>();
	
	public ContatoBean(Contato contato) {
		this.nome = Util.strings.emptyIfNull(contato.getNome());
		this.tipo_contato = Util.strings.emptyIfNull(contato.getContatotipo() != null ? contato.getContatotipo().getNome() : null);
		this.email = Util.strings.emptyIfNull(contato.getEmail());
		this.observacao = Util.strings.emptyIfNull(contato.getObservacao());
		this.cargo = null;//N�o existe essa info no W3
		
		if(SinedUtil.isListNotEmpty(contato.getListaTelefone())){
			for(Telefone telefone : contato.getListaTelefone()){
				this.lista_telefone.add(new TelefoneBean(telefone));
			}
		}
	}
	public String getNome() {
		return nome;
	}
	public String getTipo_contato() {
		return tipo_contato;
	}
	public String getEmail() {
		return email;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getCargo() {
		return cargo;
	}
	public List<TelefoneBean> getLista_telefone() {
		return lista_telefone;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipo_contato(String tipo_contato) {
		this.tipo_contato = tipo_contato;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public void setLista_telefone(List<TelefoneBean> listaTelefone) {
		lista_telefone = listaTelefone;
	}
}
