package br.com.linkcom.sined.util.rest.arvoreacesso;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Telefone;

public class RepresentanteBean {
	protected String nome;
	protected String razao_social;
	protected String cnpj;
	protected String site;
	protected String email;
	protected List<TelefoneBean> lista_telefone = new ArrayList<TelefoneBean>();
	protected List<EnderecoBean> lista_endereco = new ArrayList<EnderecoBean>();
	protected List<ContatoBean> lista_contato = new ArrayList<ContatoBean>();
	
	public RepresentanteBean(Fornecedor fornecedor) {
		this.nome = fornecedor.getNome();
		this.razao_social = fornecedor.getRazaosocial();
		this.cnpj = fornecedor.getCnpj().getValue()+"";
		this.site = fornecedor.getSite();
		this.email = fornecedor.getEmail();
		
		for(Telefone telefone : fornecedor.getListaTelefone()){
			lista_telefone.add(new TelefoneBean(telefone));
		}
		
		for(Endereco endereco: fornecedor.getListaEndereco()){
			lista_endereco.add(new EnderecoBean(endereco));
		}
		
		for(PessoaContato pessoaContato : fornecedor.getListaContato()){
			lista_contato.add(new ContatoBean(pessoaContato.getContato()));
		}
	}
	public String getNome() {
		return nome;
	}
	public String getRazao_social() {
		return razao_social;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getSite() {
		return site;
	}
	public String getEmail() {
		return email;
	}
	public List<TelefoneBean> getLista_telefone() {
		return lista_telefone;
	}
	public List<EnderecoBean> getLista_endereco() {
		return lista_endereco;
	}
	public List<ContatoBean> getLista_contato() {
		return lista_contato;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setLista_telefone(List<TelefoneBean> listaTelefone) {
		lista_telefone = listaTelefone;
	}
	public void setLista_endereco(List<EnderecoBean> listaEndereco) {
		lista_endereco = listaEndereco;
	}
	public void setLista_contato(List<ContatoBean> listaContato) {
		lista_contato = listaContato;
	}
	
}
