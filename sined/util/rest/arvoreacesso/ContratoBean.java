package br.com.linkcom.sined.util.rest.arvoreacesso;

import java.text.SimpleDateFormat;

import org.apache.commons.lang.BooleanUtils;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Fornecedor;


public class ContratoBean {
	protected Integer id_contrato;
	protected String identificador;
	protected String descricao;
	protected String data_inicio;
	protected String data_fim;
	protected Boolean faturar_webservice;
	protected RepresentanteBean representante;
	
	public ContratoBean(Contrato contrato, Fornecedor representante) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.id_contrato = contrato.getCdcontrato();
		this.identificador = contrato.getIdentificador();
		this.descricao = contrato.getDescricao();
		this.data_inicio = sdf.format(contrato.getDtinicio());
		this.data_fim = contrato.getDtfim()!=null ? sdf.format(contrato.getDtfim()) : null;
		this.faturar_webservice = BooleanUtils.isTrue(contrato.getContratotipo()!=null ? contrato.getContratotipo().getFaturamentowebservice() : null);
		if(representante!=null){
			this.representante = new RepresentanteBean(representante);
		}
	}
	public Integer getId_contrato() {
		return id_contrato;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getData_inicio() {
		return data_inicio;
	}
	public String getData_fim() {
		return data_fim;
	}
	public Boolean getFaturar_webservice() {
		return faturar_webservice;
	}
	public RepresentanteBean getRepresentante() {
		return representante;
	}
	public void setId_contrato(Integer id_contrato) {
		this.id_contrato = id_contrato;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setData_inicio(String data_inicio) {
		this.data_inicio = data_inicio;
	}
	public void setData_fim(String data_fim) {
		this.data_fim = data_fim;
	}
	public void setFaturar_webservice(Boolean faturar_webservice) {
		this.faturar_webservice = faturar_webservice;
	}
	public void setRepresentante(RepresentanteBean representante) {
		this.representante = representante;
	}
}
