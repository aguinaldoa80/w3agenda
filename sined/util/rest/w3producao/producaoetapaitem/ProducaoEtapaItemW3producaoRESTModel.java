package br.com.linkcom.sined.util.rest.w3producao.producaoetapaitem;

import br.com.linkcom.sined.geral.bean.Producaoetapaitem;

public class ProducaoEtapaItemW3producaoRESTModel {
	
	private Integer cdproducaoetapaitem;
	private Integer cdproducaoetapa;
	private Integer cdproducaoetapanome;
	private Integer ordem;
	
	public ProducaoEtapaItemW3producaoRESTModel() {}
	
	public ProducaoEtapaItemW3producaoRESTModel(Producaoetapaitem producaoetapaitem){
		this.cdproducaoetapaitem = producaoetapaitem.getCdproducaoetapaitem();
		this.cdproducaoetapa = producaoetapaitem.getProducaoetapa()!=null ? producaoetapaitem.getProducaoetapa().getCdproducaoetapa() : null;
		this.cdproducaoetapanome = producaoetapaitem.getProducaoetapanome() != null ? producaoetapaitem.getProducaoetapanome().getCdproducaoetapanome() : null;
		this.ordem = producaoetapaitem.getOrdem();
	}
	
	public Integer getCdproducaoetapaitem() {
		return cdproducaoetapaitem;
	}
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	public Integer getCdproducaoetapanome() {
		return cdproducaoetapanome;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setCdproducaoetapaitem(Integer cdproducaoetapaitem) {
		this.cdproducaoetapaitem = cdproducaoetapaitem;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public void setCdproducaoetapanome(Integer cdproducaoetapanome) {
		this.cdproducaoetapanome = cdproducaoetapanome;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	

}
