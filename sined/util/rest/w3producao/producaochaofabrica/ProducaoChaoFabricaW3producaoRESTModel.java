package br.com.linkcom.sined.util.rest.w3producao.producaochaofabrica;

import java.sql.Date;


public class ProducaoChaoFabricaW3producaoRESTModel {
	
	private Integer cdproducaochaofabrica;
	private Integer cdpedidovenda;
	private Integer cdpedidovendamaterial;
	private Integer cdproducaoagenda;
	private Integer cdproducaoagendamaterial;
	private Integer cdcliente;
	private Integer cdmaterial;
	private Integer cdpneu;
	private Integer cdproducaoetapa; 
	private Integer cdmaterialgarantido;
	private Date dtprazoentrega;
	
	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}

	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}

	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}

	public Integer getCdproducaoagendamaterial() {
		return cdproducaoagendamaterial;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Integer getCdpneu() {
		return cdpneu;
	}
	
	public Integer getCdcliente() {
		return cdcliente;
	}
	
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	
	public Integer getCdmaterialgarantido() {
		return cdmaterialgarantido;
	}
	
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}

	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}

	public void setCdproducaoagendamaterial(Integer cdproducaoagendamaterial) {
		this.cdproducaoagendamaterial = cdproducaoagendamaterial;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}
	
	public void setCdmaterialgarantido(Integer cdmaterialgarantido) {
		this.cdmaterialgarantido = cdmaterialgarantido;
	}
	
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
}
