package br.com.linkcom.sined.util.rest.w3producao.downloadproducaobean;

import br.com.linkcom.sined.util.rest.w3producao.producaochaofabrica.ProducaoChaoFabricaW3producaoRESTModel;

public class DownloadProducaoW3producaoRESTModel {
	
	private ProducaoChaoFabricaW3producaoRESTModel[] producaoChaoFabricaRESTModelArray;

	public ProducaoChaoFabricaW3producaoRESTModel[] getProducaoChaoFabricaRESTModelArray() {
		return producaoChaoFabricaRESTModelArray;
	}

	public void setProducaoChaoFabricaRESTModelArray(
			ProducaoChaoFabricaW3producaoRESTModel[] producaoChaoFabricaRESTModelArray) {
		this.producaoChaoFabricaRESTModelArray = producaoChaoFabricaRESTModelArray;
	}
	
}
