package br.com.linkcom.sined.util.rest.w3producao.downloadproducaobean;

public class DownloadProducaoW3producaoRESTWSBean {
	
	private String whereInEmpresa;
	
	public String getWhereInEmpresa() {
		return whereInEmpresa;
	}
	
	public void setWhereInEmpresa(String whereInEmpresa) {
		this.whereInEmpresa = whereInEmpresa;
	}
	
}
