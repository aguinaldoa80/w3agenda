package br.com.linkcom.sined.util.rest.w3producao.downloadproducaobean;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/DownloadProducaoW3producao")
public class DownloadProducaoW3producaoRESTWS implements SimpleRESTWS<DownloadProducaoW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(DownloadProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(DownloadProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DownloadProducaoW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		DownloadProducaoW3producaoRESTModel model = ProducaochaofabricaService.getInstance().getDownloadProducaoForW3producao(command.getWhereInEmpresa());
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(DownloadProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
