package br.com.linkcom.sined.util.rest.w3producao.estoque;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.GerenciamentomaterialService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/EstoqueW3producao")
public class EstoqueW3producaoRESTWS implements SimpleRESTWS<EstoqueW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(EstoqueW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(EstoqueW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		EstoqueW3producaoRESTModel model = GerenciamentomaterialService.getInstance().getEstoqueForW3Producao(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(EstoqueW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(EstoqueW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}
}