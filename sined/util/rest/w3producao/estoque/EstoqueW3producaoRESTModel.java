package br.com.linkcom.sined.util.rest.w3producao.estoque;


public class EstoqueW3producaoRESTModel {
	
	private EstoqueMaterialW3producaoRESTModel[] estoqueMaterialRESTModelArray;
	
	public EstoqueMaterialW3producaoRESTModel[] getEstoqueMaterialRESTModelArray() {
		return estoqueMaterialRESTModelArray;
	}
	
	public void setEstoqueMaterialRESTModelArray(EstoqueMaterialW3producaoRESTModel[] estoqueMaterialRESTModelArray) {
		this.estoqueMaterialRESTModelArray = estoqueMaterialRESTModelArray;
	}	
}
