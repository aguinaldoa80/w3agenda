package br.com.linkcom.sined.util.rest.w3producao.estoque;


public class EstoqueMaterialW3producaoRESTModel {
	
	private Integer cdmaterial;
	private Double qtdedisponivel;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
}
