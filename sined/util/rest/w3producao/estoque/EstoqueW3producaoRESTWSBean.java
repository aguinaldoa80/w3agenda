package br.com.linkcom.sined.util.rest.w3producao.estoque;

public class EstoqueW3producaoRESTWSBean {

	private String whereInEmpresa;
	
	public String getWhereInEmpresa() {
		return whereInEmpresa;
	}
	
	public void setWhereInEmpresa(String whereInEmpresa) {
		this.whereInEmpresa = whereInEmpresa;
	}
}
