package br.com.linkcom.sined.util.rest.w3producao.pneumarca;

import br.com.linkcom.sined.geral.bean.Pneumarca;

public class PneuMarcaW3producaoRESTModel {
	
	private Integer cdpneumarca;
	private String nome;
	
	public PneuMarcaW3producaoRESTModel() {}

	public PneuMarcaW3producaoRESTModel(Pneumarca pneumarca){
		this.cdpneumarca = pneumarca.getCdpneumarca();
		this.nome = pneumarca.getNome();
	}
	
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
