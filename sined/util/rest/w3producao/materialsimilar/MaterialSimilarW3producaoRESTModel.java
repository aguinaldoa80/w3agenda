package br.com.linkcom.sined.util.rest.w3producao.materialsimilar;

import br.com.linkcom.sined.geral.bean.Materialsimilar;

public class MaterialSimilarW3producaoRESTModel {
	
	private Integer cdmaterialsimilar;
	private Integer cdmaterial;
	private Integer cdmaterialsimilaritem;
	
	public MaterialSimilarW3producaoRESTModel() {}
	
	public MaterialSimilarW3producaoRESTModel(Materialsimilar materialsimilar){
		this.cdmaterialsimilar = materialsimilar.getCdmaterialsimilar();
		this.cdmaterial = materialsimilar.getMaterial()!=null ? materialsimilar.getMaterial().getCdmaterial() : null;
		this.cdmaterialsimilaritem = materialsimilar.getMaterialsimilaritem()!=null ? materialsimilar.getMaterialsimilaritem().getCdmaterial() : null;
	}
	
	public Integer getCdmaterialsimilar() {
		return cdmaterialsimilar;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdmaterialsimilaritem() {
		return cdmaterialsimilaritem;
	}
	public void setCdmaterialsimilar(Integer cdmaterialsimilar) {
		this.cdmaterialsimilar = cdmaterialsimilar;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdmaterialsimilaritem(Integer cdmaterialsimilaritem) {
		this.cdmaterialsimilaritem = cdmaterialsimilaritem;
	}
	
	

}
