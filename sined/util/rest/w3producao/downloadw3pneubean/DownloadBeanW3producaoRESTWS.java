package br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/DownloadBeanW3producao")
public class DownloadBeanW3producaoRESTWS implements SimpleRESTWS<DownloadBeanW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(DownloadBeanW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(DownloadBeanW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		DownloadBeanW3producaoRESTModel model = UsuarioService.getInstance().getDownloadBeanForW3producao(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(DownloadBeanW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(DownloadBeanW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
