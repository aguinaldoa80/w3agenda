package br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean;

public class DownloadBeanW3producaoRESTWSBean {

	private Long dataUltimaAtualizacao;

	public Long getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Long dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

}
