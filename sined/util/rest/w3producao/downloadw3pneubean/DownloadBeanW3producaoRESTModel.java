package br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean;

import br.com.linkcom.sined.util.rest.w3producao.cliente.ClienteW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.clienteVendedor.ClienteVendedorW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.empresa.EmpresaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipo.GarantiaTipoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipopercentual.GarantiaTipoPercentualW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.material.MaterialW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialgrupo.MaterialGrupoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialpneumodelo.MaterialPneuModeloW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialproducao.MaterialProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialsimilar.MaterialSimilarW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialunidademedida.MaterialUnidademedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.motivodevolucao.MotivoDevolucaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.patrimonioitem.PatrimonioItemW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pedidovenda.PedidoVendaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneu.PneuW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumarca.PneuMarcaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumedida.PneuMedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumodelo.PneuModeloW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneuqualificacao.PneuQualificacaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapa.ProducaoEtapaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapaitem.ProducaoEtapaItemW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanome.ProducaoEtapaNomeW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanomecampo.ProducaoEtapaNomeCampoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.unidademedida.UnidadeMedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.usuario.UsuarioW3producaoRESTModel;

public class DownloadBeanW3producaoRESTModel {
	
	private Long dataUltimaSincronizacao;
	
	private UsuarioW3producaoRESTModel[] usuarioRESTModelArray;
    private EmpresaW3producaoRESTModel[] empresaRESTModelArray;
    private ClienteW3producaoRESTModel[] clienteRESTModelArray;
    private ClienteVendedorW3producaoRESTModel[] clienteVendedorRESTModelArray;
    private MaterialW3producaoRESTModel[] materialRESTModelArray;
    private MaterialSimilarW3producaoRESTModel[] materialSimilarRESTModelArray;
    private MaterialUnidademedidaW3producaoRESTModel[] materialUnidademedidaRESTModelArray;
    private MaterialProducaoW3producaoRESTModel[] materialProducaoRESTModelArray;
    private MaterialGrupoW3producaoRESTModel[] materialGrupoRESTModelArray;
    private PatrimonioItemW3producaoRESTModel[] patrimonioItemRESTModelArray;
    private UnidadeMedidaW3producaoRESTModel[] unidadeMedidaRESTModelArray;
    private ProducaoEtapaW3producaoRESTModel[] producaoEtapaRESTModelArray;
    private ProducaoEtapaItemW3producaoRESTModel[] producaoEtapaItemRESTModelArray;
    private ProducaoEtapaNomeW3producaoRESTModel[] producaoEtapaNomeRESTModelArray;
    private ProducaoEtapaNomeCampoW3producaoRESTModel[] producaoEtapaNomeCampoRESTModelArray;
    private MotivoDevolucaoW3producaoRESTModel[] motivoDevolucaoRESTModelArray;
    private PneuW3producaoRESTModel[] pneuRESTModelArray;
    private PneuMarcaW3producaoRESTModel[] pneuMarcaRESTModelArray;
    private PneuModeloW3producaoRESTModel[] pneuModeloRESTModelArray;
    private PneuMedidaW3producaoRESTModel[] pneuMedidaRESTModelArray;
    private PedidoVendaW3producaoRESTModel[] pedidoVendaRESTModelArray;
    private MaterialPneuModeloW3producaoRESTModel[] materialPneuModeloRESTModelArray;
    private PneuQualificacaoW3producaoRESTModel[] pneuQualificacaoRESTModelArray;
    private GarantiaTipoW3producaoRESTModel[] garantiaTipoRESTModelArray;
    private GarantiaTipoPercentualW3producaoRESTModel[] garantiaTipoPercentualRESTModelArray;

    private String whereInExcluidoUsuario;
    private String whereInExcluidoEmpresa;
    private String whereInExcluidoCliente;
    private String whereInExcluidoMaterial;
    private String whereInExcluidoMaterialSimilar;
    private String whereInExcluidoMaterialUnidademedida;
    private String whereInExcluidoMaterialProducao;
    private String whereInExcluidoMaterialGrupo;
    private String whereInExcluidoPatrimonioItem;
    private String whereInExcluidoUnidadeMedida;
    private String whereInExcluidoProducaoEtapa;
    private String whereInExcluidoProducaoEtapaItem;
    private String whereInExcluidoProducaoEtapaNome;
    private String whereInExcluidoProducaoEtapaNomeCampo;
    private String whereInExcluidoMotivoDevolucao;
    private String whereInExcluidoPneu;
    private String whereInExcluidoPneuMarca;
    private String whereInExcluidoPneuModelo;
    private String whereInExcluidoPneuMedida;
    private String whereInExcluidoPedidoVenda;
    private String whereInExcluidoMaterialPneuModelo;
    private String whereInExcluidoPneuQualificacao;
    private String whereInExcluidoGarantiaTipo;
    private String whereInExcluidoGarantiaTipoPercentual;
    
	
    public Long getDataUltimaSincronizacao() {
		return dataUltimaSincronizacao;
	}
	public void setDataUltimaSincronizacao(Long dataUltimaSincronizacao) {
		this.dataUltimaSincronizacao = dataUltimaSincronizacao;
	}
	public UsuarioW3producaoRESTModel[] getUsuarioRESTModelArray() {
		return usuarioRESTModelArray;
	}
	public EmpresaW3producaoRESTModel[] getEmpresaRESTModelArray() {
		return empresaRESTModelArray;
	}
	public ClienteW3producaoRESTModel[] getClienteRESTModelArray() {
		return clienteRESTModelArray;
	}
	public MaterialW3producaoRESTModel[] getMaterialRESTModelArray() {
		return materialRESTModelArray;
	}
	public MaterialSimilarW3producaoRESTModel[] getMaterialSimilarRESTModelArray() {
		return materialSimilarRESTModelArray;
	}
	public MaterialProducaoW3producaoRESTModel[] getMaterialProducaoRESTModelArray() {
		return materialProducaoRESTModelArray;
	}
	public MaterialGrupoW3producaoRESTModel[] getMaterialGrupoRESTModelArray() {
		return materialGrupoRESTModelArray;
	}
	public PatrimonioItemW3producaoRESTModel[] getPatrimonioItemRESTModelArray() {
		return patrimonioItemRESTModelArray;
	}
	public UnidadeMedidaW3producaoRESTModel[] getUnidadeMedidaRESTModelArray() {
		return unidadeMedidaRESTModelArray;
	}
	public ProducaoEtapaW3producaoRESTModel[] getProducaoEtapaRESTModelArray() {
		return producaoEtapaRESTModelArray;
	}
	public ProducaoEtapaItemW3producaoRESTModel[] getProducaoEtapaItemRESTModelArray() {
		return producaoEtapaItemRESTModelArray;
	}
	public MotivoDevolucaoW3producaoRESTModel[] getMotivoDevolucaoRESTModelArray() {
		return motivoDevolucaoRESTModelArray;
	}
	public PneuW3producaoRESTModel[] getPneuRESTModelArray() {
		return pneuRESTModelArray;
	}
	public PneuMarcaW3producaoRESTModel[] getPneuMarcaRESTModelArray() {
		return pneuMarcaRESTModelArray;
	}
	public PneuModeloW3producaoRESTModel[] getPneuModeloRESTModelArray() {
		return pneuModeloRESTModelArray;
	}
	public PneuMedidaW3producaoRESTModel[] getPneuMedidaRESTModelArray() {
		return pneuMedidaRESTModelArray;
	}
	public ProducaoEtapaNomeW3producaoRESTModel[] getProducaoEtapaNomeRESTModelArray() {
		return producaoEtapaNomeRESTModelArray;
	}
	public ProducaoEtapaNomeCampoW3producaoRESTModel[] getProducaoEtapaNomeCampoRESTModelArray() {
		return producaoEtapaNomeCampoRESTModelArray;
	}
	public String getWhereInExcluidoProducaoEtapaNome() {
		return whereInExcluidoProducaoEtapaNome;
	}
	public String getWhereInExcluidoProducaoEtapaNomeCampo() {
		return whereInExcluidoProducaoEtapaNomeCampo;
	}
	public MaterialUnidademedidaW3producaoRESTModel[] getMaterialUnidademedidaRESTModelArray() {
		return materialUnidademedidaRESTModelArray;
	}
	public String getWhereInExcluidoMaterialUnidademedida() {
		return whereInExcluidoMaterialUnidademedida;
	}
	public PedidoVendaW3producaoRESTModel[] getPedidoVendaRESTModelArray() {
		return pedidoVendaRESTModelArray;
	}
	public void setPedidoVendaRESTModelArray(
			PedidoVendaW3producaoRESTModel[] pedidoVendaRESTModelArray) {
		this.pedidoVendaRESTModelArray = pedidoVendaRESTModelArray;
	}
	public String getWhereInExcluidoPedidoVenda() {
		return whereInExcluidoPedidoVenda;
	}
	public MaterialPneuModeloW3producaoRESTModel[] getMaterialPneuModeloRESTModelArray() {
		return materialPneuModeloRESTModelArray;
	}
	public String getWhereInExcluidoMaterialPneuModelo() {
		return whereInExcluidoMaterialPneuModelo;
	}
	public void setMaterialPneuModeloRESTModelArray(
			MaterialPneuModeloW3producaoRESTModel[] materialPneuModeloRESTModelArray) {
		this.materialPneuModeloRESTModelArray = materialPneuModeloRESTModelArray;
	}
	public void setWhereInExcluidoMaterialPneuModelo(
			String whereInExcluidoMaterialPneuModelo) {
		this.whereInExcluidoMaterialPneuModelo = whereInExcluidoMaterialPneuModelo;
	}
	public void setWhereInExcluidoPedidoVenda(String whereInExcluidoPedidoVenda) {
		this.whereInExcluidoPedidoVenda = whereInExcluidoPedidoVenda;
	}
	public void setMaterialUnidademedidaRESTModelArray(
			MaterialUnidademedidaW3producaoRESTModel[] materialUnidademedidaRESTModelArray) {
		this.materialUnidademedidaRESTModelArray = materialUnidademedidaRESTModelArray;
	}
	public void setWhereInExcluidoMaterialUnidademedida(
			String whereInExcluidoMaterialUnidademedida) {
		this.whereInExcluidoMaterialUnidademedida = whereInExcluidoMaterialUnidademedida;
	}
	public void setProducaoEtapaNomeRESTModelArray(
			ProducaoEtapaNomeW3producaoRESTModel[] producaoEtapaNomeRESTModelArray) {
		this.producaoEtapaNomeRESTModelArray = producaoEtapaNomeRESTModelArray;
	}
	public void setProducaoEtapaNomeCampoRESTModelArray(
			ProducaoEtapaNomeCampoW3producaoRESTModel[] producaoEtapaNomeCampoRESTModelArray) {
		this.producaoEtapaNomeCampoRESTModelArray = producaoEtapaNomeCampoRESTModelArray;
	}
	public void setWhereInExcluidoProducaoEtapaNome(
			String whereInExcluidoProducaoEtapaNome) {
		this.whereInExcluidoProducaoEtapaNome = whereInExcluidoProducaoEtapaNome;
	}
	public void setWhereInExcluidoProducaoEtapaNomeCampo(
			String whereInExcluidoProducaoEtapaNomeCampo) {
		this.whereInExcluidoProducaoEtapaNomeCampo = whereInExcluidoProducaoEtapaNomeCampo;
	}
	public void setUsuarioRESTModelArray(UsuarioW3producaoRESTModel[] usuarioRESTModelArray) {
		this.usuarioRESTModelArray = usuarioRESTModelArray;
	}
	public void setEmpresaRESTModelArray(EmpresaW3producaoRESTModel[] empresaRESTModelArray) {
		this.empresaRESTModelArray = empresaRESTModelArray;
	}
	public void setClienteRESTModelArray(ClienteW3producaoRESTModel[] clienteRESTModelArray) {
		this.clienteRESTModelArray = clienteRESTModelArray;
	}
	public void setMaterialRESTModelArray(MaterialW3producaoRESTModel[] materialRESTModelArray) {
		this.materialRESTModelArray = materialRESTModelArray;
	}
	public void setMaterialSimilarRESTModelArray(
			MaterialSimilarW3producaoRESTModel[] materialSimilarRESTModelArray) {
		this.materialSimilarRESTModelArray = materialSimilarRESTModelArray;
	}
	public void setMaterialProducaoRESTModelArray(
			MaterialProducaoW3producaoRESTModel[] materialProducaoRESTModelArray) {
		this.materialProducaoRESTModelArray = materialProducaoRESTModelArray;
	}
	public void setMaterialGrupoRESTModelArray(
			MaterialGrupoW3producaoRESTModel[] materialGrupoRESTModelArray) {
		this.materialGrupoRESTModelArray = materialGrupoRESTModelArray;
	}
	public void setPatrimonioItemRESTModelArray(
			PatrimonioItemW3producaoRESTModel[] patrimonioItemRESTModelArray) {
		this.patrimonioItemRESTModelArray = patrimonioItemRESTModelArray;
	}
	public void setUnidadeMedidaRESTModelArray(
			UnidadeMedidaW3producaoRESTModel[] unidadeMedidaRESTModelArray) {
		this.unidadeMedidaRESTModelArray = unidadeMedidaRESTModelArray;
	}
	public void setProducaoEtapaRESTModelArray(
			ProducaoEtapaW3producaoRESTModel[] producaoEtapaRESTModelArray) {
		this.producaoEtapaRESTModelArray = producaoEtapaRESTModelArray;
	}
	public void setProducaoEtapaItemRESTModelArray(
			ProducaoEtapaItemW3producaoRESTModel[] producaoEtapaItemRESTModelArray) {
		this.producaoEtapaItemRESTModelArray = producaoEtapaItemRESTModelArray;
	}
	public void setMotivoDevolucaoRESTModelArray(
			MotivoDevolucaoW3producaoRESTModel[] motivoDevolucaoRESTModelArray) {
		this.motivoDevolucaoRESTModelArray = motivoDevolucaoRESTModelArray;
	}
	public void setPneuRESTModelArray(PneuW3producaoRESTModel[] pneuRESTModelArray) {
		this.pneuRESTModelArray = pneuRESTModelArray;
	}
	public void setPneuMarcaRESTModelArray(
			PneuMarcaW3producaoRESTModel[] pneuMarcaRESTModelArray) {
		this.pneuMarcaRESTModelArray = pneuMarcaRESTModelArray;
	}
	
	public ClienteVendedorW3producaoRESTModel[] getClienteVendedorRESTModelArray() {
		return clienteVendedorRESTModelArray;
	}
	
	public void setPneuModeloRESTModelArray(
			PneuModeloW3producaoRESTModel[] pneuModeloRESTModelArray) {
		this.pneuModeloRESTModelArray = pneuModeloRESTModelArray;
	}
	public void setPneuMedidaRESTModelArray(
			PneuMedidaW3producaoRESTModel[] pneuMedidaRESTModelArray) {
		this.pneuMedidaRESTModelArray = pneuMedidaRESTModelArray;
	}
	public PneuQualificacaoW3producaoRESTModel[] getPneuQualificacaoRESTModelArray() {
		return pneuQualificacaoRESTModelArray;
	}
	public GarantiaTipoW3producaoRESTModel[] getGarantiaTipoRESTModelArray() {
		return garantiaTipoRESTModelArray;
	}
	public GarantiaTipoPercentualW3producaoRESTModel[] getGarantiaTipoPercentualRESTModelArray() {
		return garantiaTipoPercentualRESTModelArray;
	}
	public void setPneuQualificacaoRESTModelArray(PneuQualificacaoW3producaoRESTModel[] pneuQualificacaoRESTModelArray) {
		this.pneuQualificacaoRESTModelArray = pneuQualificacaoRESTModelArray;
	}
	public void setGarantiaTipoRESTModelArray(GarantiaTipoW3producaoRESTModel[] garantiaTipoRESTModelArray) {
		this.garantiaTipoRESTModelArray = garantiaTipoRESTModelArray;
	}
	public void setGarantiaTipoPercentualRESTModelArray(GarantiaTipoPercentualW3producaoRESTModel[] garantiaTipoPercentualRESTModelArray) {
		this.garantiaTipoPercentualRESTModelArray = garantiaTipoPercentualRESTModelArray;
	}
	public String getWhereInExcluidoUsuario() {
		return whereInExcluidoUsuario;
	}
	public String getWhereInExcluidoEmpresa() {
		return whereInExcluidoEmpresa;
	}
	public String getWhereInExcluidoCliente() {
		return whereInExcluidoCliente;
	}
	public String getWhereInExcluidoMaterial() {
		return whereInExcluidoMaterial;
	}
	public String getWhereInExcluidoMaterialSimilar() {
		return whereInExcluidoMaterialSimilar;
	}
	public String getWhereInExcluidoMaterialProducao() {
		return whereInExcluidoMaterialProducao;
	}
	public String getWhereInExcluidoMaterialGrupo() {
		return whereInExcluidoMaterialGrupo;
	}
	public String getWhereInExcluidoPatrimonioItem() {
		return whereInExcluidoPatrimonioItem;
	}
	public String getWhereInExcluidoUnidadeMedida() {
		return whereInExcluidoUnidadeMedida;
	}
	public String getWhereInExcluidoProducaoEtapa() {
		return whereInExcluidoProducaoEtapa;
	}
	public String getWhereInExcluidoProducaoEtapaItem() {
		return whereInExcluidoProducaoEtapaItem;
	}
	public String getWhereInExcluidoMotivoDevolucao() {
		return whereInExcluidoMotivoDevolucao;
	}
	public String getWhereInExcluidoPneu() {
		return whereInExcluidoPneu;
	}
	public String getWhereInExcluidoPneuMarca() {
		return whereInExcluidoPneuMarca;
	}
	public String getWhereInExcluidoPneuModelo() {
		return whereInExcluidoPneuModelo;
	}
	public String getWhereInExcluidoPneuMedida() {
		return whereInExcluidoPneuMedida;
	}
	public String getWhereInExcluidoPneuQualificacao() {
		return whereInExcluidoPneuQualificacao;
	}
	public String getWhereInExcluidoGarantiaTipo() {
		return whereInExcluidoGarantiaTipo;
	}
	public String getWhereInExcluidoGarantiaTipoPercentual() {
		return whereInExcluidoGarantiaTipoPercentual;
	}
	public void setWhereInExcluidoUsuario(String whereInExcluidoUsuario) {
		this.whereInExcluidoUsuario = whereInExcluidoUsuario;
	}
	public void setWhereInExcluidoEmpresa(String whereInExcluidoEmpresa) {
		this.whereInExcluidoEmpresa = whereInExcluidoEmpresa;
	}
	public void setWhereInExcluidoCliente(String whereInExcluidoCliente) {
		this.whereInExcluidoCliente = whereInExcluidoCliente;
	}
	public void setWhereInExcluidoMaterial(String whereInExcluidoMaterial) {
		this.whereInExcluidoMaterial = whereInExcluidoMaterial;
	}
	public void setWhereInExcluidoMaterialSimilar(
			String whereInExcluidoMaterialSimilar) {
		this.whereInExcluidoMaterialSimilar = whereInExcluidoMaterialSimilar;
	}
	public void setWhereInExcluidoMaterialProducao(
			String whereInExcluidoMaterialProducao) {
		this.whereInExcluidoMaterialProducao = whereInExcluidoMaterialProducao;
	}
	public void setWhereInExcluidoMaterialGrupo(
			String whereInExcluidoMaterialGrupo) {
		this.whereInExcluidoMaterialGrupo = whereInExcluidoMaterialGrupo;
	}
	public void setWhereInExcluidoPatrimonioItem(
			String whereInExcluidoPatrimonioItem) {
		this.whereInExcluidoPatrimonioItem = whereInExcluidoPatrimonioItem;
	}
	public void setWhereInExcluidoUnidadeMedida(String whereInExcluidoUnidadeMedida) {
		this.whereInExcluidoUnidadeMedida = whereInExcluidoUnidadeMedida;
	}
	public void setWhereInExcluidoProducaoEtapa(String whereInExcluidoProducaoEtapa) {
		this.whereInExcluidoProducaoEtapa = whereInExcluidoProducaoEtapa;
	}
	public void setWhereInExcluidoProducaoEtapaItem(
			String whereInExcluidoProducaoEtapaItem) {
		this.whereInExcluidoProducaoEtapaItem = whereInExcluidoProducaoEtapaItem;
	}
	public void setWhereInExcluidoMotivoDevolucao(
			String whereInExcluidoMotivoDevolucao) {
		this.whereInExcluidoMotivoDevolucao = whereInExcluidoMotivoDevolucao;
	}
	public void setWhereInExcluidoPneu(String whereInExcluidoPneu) {
		this.whereInExcluidoPneu = whereInExcluidoPneu;
	}
	public void setWhereInExcluidoPneuMarca(String whereInExcluidoPneuMarca) {
		this.whereInExcluidoPneuMarca = whereInExcluidoPneuMarca;
	}
	public void setWhereInExcluidoPneuModelo(String whereInExcluidoPneuModelo) {
		this.whereInExcluidoPneuModelo = whereInExcluidoPneuModelo;
	}
	public void setWhereInExcluidoPneuMedida(String whereInExcluidoPneuMedida) {
		this.whereInExcluidoPneuMedida = whereInExcluidoPneuMedida;
	}
	public void setWhereInExcluidoPneuQualificacao(String whereInExcluidoPneuQualificacao) {
		this.whereInExcluidoPneuQualificacao = whereInExcluidoPneuQualificacao;
	}
	public void setWhereInExcluidoGarantiaTipo(String whereInExcluidoGarantiaTipo) {
		this.whereInExcluidoGarantiaTipo = whereInExcluidoGarantiaTipo;
	}
	public void setWhereInExcluidoGarantiaTipoPercentual(String whereInExcluidoGarantiaTipoPercentual) {
		this.whereInExcluidoGarantiaTipoPercentual = whereInExcluidoGarantiaTipoPercentual;
	}
	public void setClienteVendedorRESTModelArray(
			ClienteVendedorW3producaoRESTModel[] clienteVendedorRESTModelArray) {
		this.clienteVendedorRESTModelArray = clienteVendedorRESTModelArray;
	}
}