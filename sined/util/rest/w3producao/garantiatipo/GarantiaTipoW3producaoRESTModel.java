package br.com.linkcom.sined.util.rest.w3producao.garantiatipo;

import br.com.linkcom.sined.geral.bean.Garantiatipo;



public class GarantiaTipoW3producaoRESTModel {
	
	private Integer cdgarantiatipo;
	private String descricao;
	private Boolean geragarantia;
	private Boolean ativo;
	
	public GarantiaTipoW3producaoRESTModel() {}
	
	public GarantiaTipoW3producaoRESTModel(Garantiatipo garantiatipo){
		this.cdgarantiatipo = garantiatipo.getCdgarantiatipo();
		this.descricao = garantiatipo.getDescricao();
		this.geragarantia = garantiatipo.getGeragarantia();
		this.ativo = garantiatipo.getAtivo();
	}

	public Integer getCdgarantiatipo() {
		return cdgarantiatipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public Boolean getGeragarantia() {
		return geragarantia;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdgarantiatipo(Integer cdgarantiatipo) {
		this.cdgarantiatipo = cdgarantiatipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setGeragarantia(Boolean geragarantia) {
		this.geragarantia = geragarantia;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
