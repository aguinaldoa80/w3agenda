package br.com.linkcom.sined.util.rest.w3producao.pneu;

import br.com.linkcom.sined.geral.bean.Pneu;



public class PneuW3producaoRESTModel {
	
	private Integer cdpneu;
	private Integer cdpneumarca;
	private Integer cdpneumedida;
	private Integer cdpneumodelo;
	private String serie;
	private String dot;
	private Integer cdmaterialbanda;
	private Integer numero_reforma;
	private Integer cdpneuqualificacao;
	
	public PneuW3producaoRESTModel() {}
	
	public PneuW3producaoRESTModel(Pneu pneu){
		this.cdpneu = pneu.getCdpneu();
		this.cdpneumarca = pneu.getPneumarca()!=null ? pneu.getPneumarca().getCdpneumarca() : null;
		this.cdpneumedida = pneu.getPneumedida()!=null ? pneu.getPneumedida().getCdpneumedida() : null;
		this.cdpneumodelo = pneu.getPneumodelo()!=null ? pneu.getPneumodelo().getCdpneumodelo() : null;
		this.serie = pneu.getSerie();
		this.dot = pneu.getDot();
		this.cdmaterialbanda = pneu.getMaterialbanda()!=null ? pneu.getMaterialbanda().getCdmaterial() : null;
		this.numero_reforma = pneu.getNumeroreforma() != null ? pneu.getNumeroreforma().ordinal() : null; 
		this.cdpneuqualificacao = pneu.getPneuqualificacao() != null ? pneu.getPneuqualificacao().getCdpneuqualificacao() : null; 
	}
	
	
	public Integer getCdpneu() {
		return cdpneu;
	}
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	public Integer getCdpneumedida() {
		return cdpneumedida;
	}
	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}
	public String getSerie() {
		return serie;
	}
	public String getDot() {
		return dot;
	}
	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}
	public Integer getNumero_reforma() {
		return numero_reforma;
	}
	public Integer getCdpneuqualificacao() {
		return cdpneuqualificacao;
	}
	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}
	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}
	public void setNumero_reforma(Integer numeroReforma) {
		numero_reforma = numeroReforma;
	}
	public void setCdpneuqualificacao(Integer cdpneuqualificacao) {
		this.cdpneuqualificacao = cdpneuqualificacao;
	}
}
