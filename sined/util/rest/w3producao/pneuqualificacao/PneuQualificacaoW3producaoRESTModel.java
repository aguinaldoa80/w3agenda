package br.com.linkcom.sined.util.rest.w3producao.pneuqualificacao;

import br.com.linkcom.sined.geral.bean.Pneuqualificacao;



public class PneuQualificacaoW3producaoRESTModel {
	
	private Integer cdpneuqualificacao;
	private String nome;
	private Boolean garantia;
	private Boolean ativo;
	
	public PneuQualificacaoW3producaoRESTModel() {}
	
	public PneuQualificacaoW3producaoRESTModel(Pneuqualificacao pneuqualificacao){
		this.cdpneuqualificacao = pneuqualificacao.getCdpneuqualificacao();
		this.nome = pneuqualificacao.getNome();
		this.garantia = pneuqualificacao.getGarantia();
		this.ativo = pneuqualificacao.getAtivo();
	}

	public Integer getCdpneuqualificacao() {
		return cdpneuqualificacao;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getGarantia() {
		return garantia;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdpneuqualificacao(Integer cdpneuqualificacao) {
		this.cdpneuqualificacao = cdpneuqualificacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setGarantia(Boolean garantia) {
		this.garantia = garantia;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
}
