package br.com.linkcom.sined.util.rest.w3producao.materialpneumodelo;

import br.com.linkcom.sined.geral.bean.Materialpneumodelo;

public class MaterialPneuModeloW3producaoRESTModel {
	
	private Integer cdmaterialpneumodelo;
	private Integer cdmaterial;
	private Integer cdpneumodelo;
	private Integer cdpneumedida;
	
	public MaterialPneuModeloW3producaoRESTModel() {}
	
	public MaterialPneuModeloW3producaoRESTModel(Materialpneumodelo materialpneumodelo){
		this.cdmaterialpneumodelo = materialpneumodelo.getCdmaterialpneumodelo();
		this.cdmaterial = materialpneumodelo.getMaterial()!=null ? materialpneumodelo.getMaterial().getCdmaterial() : null;
		this.cdpneumodelo = materialpneumodelo.getPneumodelo()!=null ? materialpneumodelo.getPneumodelo().getCdpneumodelo() : null;
		this.cdpneumedida = materialpneumodelo.getPneumedida()!=null ? materialpneumodelo.getPneumedida().getCdpneumedida() : null;
	}

	public Integer getCdmaterialpneumodelo() {
		return cdmaterialpneumodelo;
	}

	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}

	public Integer getCdpneumedida() {
		return cdpneumedida;
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setCdmaterialpneumodelo(Integer cdmaterialpneumodelo) {
		this.cdmaterialpneumodelo = cdmaterialpneumodelo;
	}

	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}

	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}
	
	
}
