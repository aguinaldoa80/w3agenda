package br.com.linkcom.sined.util.rest.w3producao.garantiatipopercentual;

import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;



public class GarantiaTipoPercentualW3producaoRESTModel {
	
	private Integer cdgarantiatipopercentual;
	private Integer cdgarantiatipo;
	private Double percentual;
	
	public GarantiaTipoPercentualW3producaoRESTModel() {}
	
	public GarantiaTipoPercentualW3producaoRESTModel(Garantiatipopercentual garantiatipopercentual){
		this.cdgarantiatipopercentual = garantiatipopercentual.getCdgarantiatipopercentual();
		this.cdgarantiatipo = garantiatipopercentual.getGarantiatipo()!=null ? garantiatipopercentual.getGarantiatipo().getCdgarantiatipo() : null;
		this.percentual = garantiatipopercentual.getPercentual();
	}

	public Integer getCdgarantiatipopercentual() {
		return cdgarantiatipopercentual;
	}
	public Integer getCdgarantiatipo() {
		return cdgarantiatipo;
	}
	public Double getPercentual() {
		return percentual;
	}

	public void setCdgarantiatipopercentual(Integer cdgarantiatipopercentual) {
		this.cdgarantiatipopercentual = cdgarantiatipopercentual;
	}
	public void setCdgarantiatipo(Integer cdgarantiatipo) {
		this.cdgarantiatipo = cdgarantiatipo;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}	
}