package br.com.linkcom.sined.util.rest.w3producao.marcarproducao;

public class MarcarProducaoW3producaoRESTWSBean {
	
	private Integer cdproducaochaofabrica;

	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}

	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}
	
}
