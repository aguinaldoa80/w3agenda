package br.com.linkcom.sined.util.rest.w3producao.marcarproducao;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/MarcarProducaoW3producao")
public class MarcarProducaoW3producaoRESTWS implements SimpleRESTWS<MarcarProducaoW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(MarcarProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(MarcarProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(MarcarProducaoW3producaoRESTWSBean command) {
		ProducaochaofabricaService.getInstance().updateProducaoForW3producao(command);
		return new ModelAndStatus();
	}

	@Override
	public ModelAndStatus get(MarcarProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
