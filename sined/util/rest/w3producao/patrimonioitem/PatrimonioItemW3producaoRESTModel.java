package br.com.linkcom.sined.util.rest.w3producao.patrimonioitem;

import br.com.linkcom.sined.geral.bean.Patrimonioitem;

public class PatrimonioItemW3producaoRESTModel {
	
	private Integer cdpatrimonioitem;
	private Integer cdmaterial;
	private String plaqueta;
	private Boolean ativo;
	
	public PatrimonioItemW3producaoRESTModel() {}
	
	public PatrimonioItemW3producaoRESTModel(Patrimonioitem patrimonioitem){
		this.cdpatrimonioitem = patrimonioitem.getCdpatrimonioitem();
		this.cdmaterial = patrimonioitem.getBempatrimonio()!=null ? patrimonioitem.getBempatrimonio().getCdmaterial() : null;
		this.plaqueta = patrimonioitem.getPlaqueta();
		this.ativo = patrimonioitem.getAtivo();
	}
	
	
	public Integer getCdpatrimonioitem() {
		return cdpatrimonioitem;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getPlaqueta() {
		return plaqueta;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setCdpatrimonioitem(Integer cdpatrimonioitem) {
		this.cdpatrimonioitem = cdpatrimonioitem;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
	

}
