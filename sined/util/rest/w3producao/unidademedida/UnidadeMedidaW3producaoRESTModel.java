package br.com.linkcom.sined.util.rest.w3producao.unidademedida;

import br.com.linkcom.sined.geral.bean.Unidademedida;

public class UnidadeMedidaW3producaoRESTModel {
	
	private Integer cdunidademedida;
	private String nome;
	private String simbolo;
	private Boolean ativo;
	
	public UnidadeMedidaW3producaoRESTModel(){}
	
	public UnidadeMedidaW3producaoRESTModel(Unidademedida unidadeMedida){
		this.cdunidademedida = unidadeMedida.getCdunidademedida();
		this.nome = unidadeMedida.getNome();
		this.simbolo = unidadeMedida.getSimbolo();
		this.ativo = unidadeMedida.getAtivo();
	}
	
	
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome() {
		return nome;
	}
	public String getSimbolo() {
		return simbolo;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
