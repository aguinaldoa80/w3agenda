package br.com.linkcom.sined.util.rest.w3producao.pedidovenda;

import br.com.linkcom.sined.geral.bean.Pedidovenda;

public class PedidoVendaW3producaoRESTModel {

	
	private Integer cdpedidovenda;
	private String identificador;
	private String identificadorexterno;
	private Long dtpedidovenda;
	private Integer cdresponsavel;
	private String nomeresponsavel;
	
	public PedidoVendaW3producaoRESTModel() {}
	
	public PedidoVendaW3producaoRESTModel(Pedidovenda pedidovenda){
		this.cdpedidovenda = pedidovenda.getCdpedidovenda();
		this.identificador = pedidovenda.getIdentificador();
		this.identificadorexterno = pedidovenda.getIdentificacaoexterna();
		this.dtpedidovenda = pedidovenda.getDtpedidovenda().getTime();
		this.cdresponsavel = pedidovenda.getColaborador().getCdpessoa();
		this.nomeresponsavel = pedidovenda.getColaborador().getNome();
	}
	
	
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public String getIdentificador() {
		return identificador;
	}
	public Long getDtpedidovenda() {
		return dtpedidovenda;
	}
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}
	
	public Integer getCdresponsavel() {
		return cdresponsavel;
	}
	public String getNomeresponsavel() {
		return nomeresponsavel;
	}
	// fim get  - inicio set
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}
	public void setDtpedidovenda(Long dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	
	public void setCdresponsavel(Integer cdresponsavel) {
		this.cdresponsavel = cdresponsavel;
	}
	public void setNomeresponsavel(String nomeresponsavel) {
		this.nomeresponsavel = nomeresponsavel;
	}
}
