package br.com.linkcom.sined.util.rest.w3producao.pneumodelo;

import br.com.linkcom.sined.geral.bean.Pneumodelo;

public class PneuModeloW3producaoRESTModel {
	
	private Integer cdpneumodelo;
	private Integer cdpneumarca;
	private String nome;
	
	public PneuModeloW3producaoRESTModel() {}
	
	public PneuModeloW3producaoRESTModel(Pneumodelo pneumodelo){
		this.cdpneumodelo = pneumodelo.getCdpneumodelo();
		this.cdpneumarca = pneumodelo.getPneumarca()!=null ? pneumodelo.getPneumarca().getCdpneumarca() : null;
		this.nome = pneumodelo.getNome();
	}
	
	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}
	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
