package br.com.linkcom.sined.util.rest.w3producao.materialproducao;

import br.com.linkcom.sined.geral.bean.Materialproducao;


public class MaterialProducaoW3producaoRESTModel {
	
	private Integer cdmaterialproducao;
	private Integer ordem_exibicao;
	private Integer cdmaterialmestre;
	private Integer cdmaterial;
	private Double consumo;
	private Boolean exibir_venda;
	private Boolean acompanhabanda;
	private Integer cdproducaoetapanome;
	
	public MaterialProducaoW3producaoRESTModel() {}
	
	public MaterialProducaoW3producaoRESTModel(Materialproducao materialproducao){
		this.cdmaterialproducao = materialproducao.getCdmaterialproducao();
		this.ordem_exibicao = materialproducao.getOrdemexibicao();
		this.cdmaterialmestre = materialproducao.getMaterialmestre()!=null ? materialproducao.getMaterialmestre().getCdmaterial() : null;
		this.cdmaterial = materialproducao.getMaterial()!=null ? materialproducao.getMaterial().getCdmaterial() : null;
		this.consumo = materialproducao.getConsumo();
		this.exibir_venda = materialproducao.getExibirvenda();
		this.acompanhabanda = materialproducao.getAcompanhabanda();
		this.cdproducaoetapanome = materialproducao.getProducaoetapanome() != null ? materialproducao.getProducaoetapanome().getCdproducaoetapanome() : null;
	}
	
	public Integer getCdmaterialproducao() {
		return cdmaterialproducao;
	}
	public Integer getOrdem_exibicao() {
		return ordem_exibicao;
	}
	public Integer getCdmaterialmestre() {
		return cdmaterialmestre;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Double getConsumo() {
		return consumo;
	}
	public Boolean getExibir_venda() {
		return exibir_venda;
	}
	public Boolean getAcompanhabanda() {
		return acompanhabanda;
	}
	public Integer getCdproducaoetapanome() {
		return cdproducaoetapanome;
	}
	public void setCdproducaoetapanome(Integer cdproducaoetapanome) {
		this.cdproducaoetapanome = cdproducaoetapanome;
	}
	public void setCdmaterialproducao(Integer cdmaterialproducao) {
		this.cdmaterialproducao = cdmaterialproducao;
	}
	public void setOrdem_exibicao(Integer ordemExibicao) {
		ordem_exibicao = ordemExibicao;
	}
	public void setCdmaterialmestre(Integer cdmaterialmestre) {
		this.cdmaterialmestre = cdmaterialmestre;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}
	public void setExibir_venda(Boolean exibirVenda) {
		exibir_venda = exibirVenda;
	}
	public void setAcompanhabanda(Boolean acompanhabanda) {
		this.acompanhabanda = acompanhabanda;
	}
}