package br.com.linkcom.sined.util.rest.w3producao.producaoetapanomecampo;

import br.com.linkcom.sined.geral.bean.Producaoetapanomecampo;

public class ProducaoEtapaNomeCampoW3producaoRESTModel {
	
	private Integer cdproducaoetapanomecampo;
	private Integer cdproducaoetapanome;
	private String nome;
	private Integer tipo;
	private Boolean obrigatorio;
	
	public ProducaoEtapaNomeCampoW3producaoRESTModel() {}
	
	public ProducaoEtapaNomeCampoW3producaoRESTModel(Producaoetapanomecampo producaoetapanomecampo){
		this.cdproducaoetapanomecampo = producaoetapanomecampo.getCdproducaoetapanomecampo();
		this.cdproducaoetapanome = producaoetapanomecampo.getProducaoetapanome() != null ? producaoetapanomecampo.getProducaoetapanome().getCdproducaoetapanome() : null;
		this.nome = producaoetapanomecampo.getNome();
		this.tipo = producaoetapanomecampo.getTipo() != null ? producaoetapanomecampo.getTipo().ordinal() : null;
		this.obrigatorio = producaoetapanomecampo.getObrigatorio();
	}

	public Integer getCdproducaoetapanomecampo() {
		return cdproducaoetapanomecampo;
	}

	public Integer getCdproducaoetapanome() {
		return cdproducaoetapanome;
	}

	public String getNome() {
		return nome;
	}

	public Integer getTipo() {
		return tipo;
	}

	public Boolean getObrigatorio() {
		return obrigatorio;
	}

	public void setCdproducaoetapanomecampo(Integer cdproducaoetapanomecampo) {
		this.cdproducaoetapanomecampo = cdproducaoetapanomecampo;
	}

	public void setCdproducaoetapanome(Integer cdproducaoetapanome) {
		this.cdproducaoetapanome = cdproducaoetapanome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	

}
