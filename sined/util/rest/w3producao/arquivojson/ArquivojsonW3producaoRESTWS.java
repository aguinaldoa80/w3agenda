package br.com.linkcom.sined.util.rest.w3producao.arquivojson;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.W3ProducaoUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/ArquivojsonW3producao")
public class ArquivojsonW3producaoRESTWS implements SimpleRESTWS<ArquivojsonW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(ArquivojsonW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(ArquivojsonW3producaoRESTWSBean command) {
		String ctx = SinedUtil.getUrlWithContext();
		List<ArquivojsonW3producaoRESTModel> lista = new ArrayList<ArquivojsonW3producaoRESTModel>();
		List<File> listFile = W3ProducaoUtil.getW3ProducaoFiles();
		if(SinedUtil.isListNotEmpty(listFile)){
			for(File file : listFile){
				lista.add(new ArquivojsonW3producaoRESTModel(ctx, file));
			}
		}
		
		Collections.sort(lista,new Comparator<ArquivojsonW3producaoRESTModel>(){
			public int compare(ArquivojsonW3producaoRESTModel a, ArquivojsonW3producaoRESTModel b) {
				return a.getOrdem().compareTo(b.getOrdem());
			}
		});
		
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		modelAndStatus.setModel(lista);
		return modelAndStatus;
	}
	
	@Override
	public ModelAndStatus get(ArquivojsonW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(ArquivojsonW3producaoRESTWSBean command) {
		return null;
	}



	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
