package br.com.linkcom.sined.util.rest.w3producao.arquivojson;

import java.io.File;

public class ArquivojsonW3producaoRESTModel {
	
    private String nomearquivo;
    private String caminho;
    private Integer ordem;
    
    public ArquivojsonW3producaoRESTModel(){}
    
    public ArquivojsonW3producaoRESTModel(String ctx, File file){
    	this.nomearquivo = file.getName();
    	this.caminho = ctx + "/pub/Arquivojsonw3producao?f=" + file.getName();
    	try {
    		this.ordem = Integer.parseInt(file.getName().substring(0, file.getName().indexOf("_")));
		} catch (Exception e) {
			this.ordem = Integer.MAX_VALUE;
		}
    }

    public String getNomearquivo() {
        return nomearquivo;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setNomearquivo(String nomearquivo) {
        this.nomearquivo = nomearquivo;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
    
    public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
}
