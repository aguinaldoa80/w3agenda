package br.com.linkcom.sined.util.rest.w3producao.producaoetapa;

import br.com.linkcom.sined.geral.bean.Producaoetapa;

public class ProducaoEtapaW3producaoRESTModel {
	
	private Integer cdproducaoetapa;
	private String nome;
	private Integer baixarestoquemateriaprima;
	private Integer ordemPrioridade;
	

	public ProducaoEtapaW3producaoRESTModel(){}
	
	public ProducaoEtapaW3producaoRESTModel(Producaoetapa producaoetapa){
		this.cdproducaoetapa = producaoetapa.getCdproducaoetapa();
		this.nome = producaoetapa.getNome();
		this.baixarestoquemateriaprima = producaoetapa.getBaixarEstoqueMateriaprima() != null ? producaoetapa.getBaixarEstoqueMateriaprima().ordinal() : 0;
		this.ordemPrioridade = producaoetapa.getOrdemprioridade();
	}
	
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	public String getNome() {
		return nome;
	}
	public Integer getBaixarestoquemateriaprima() {
		return baixarestoquemateriaprima;
	}
	public void setBaixarestoquemateriaprima(Integer baixarestoquemateriaprima) {
		this.baixarestoquemateriaprima = baixarestoquemateriaprima;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getOrdemPrioridade() {
		return ordemPrioridade;
	}

	public void setOrdemPrioridade(Integer ordemPrioridade) {
		this.ordemPrioridade = ordemPrioridade;
	}

}
