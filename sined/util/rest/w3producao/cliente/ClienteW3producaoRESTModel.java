package br.com.linkcom.sined.util.rest.w3producao.cliente;

import br.com.linkcom.sined.geral.bean.Cliente;

public class ClienteW3producaoRESTModel {
	
	private Integer cdpessoa;
	private String nome;
	
	public ClienteW3producaoRESTModel() {}
	public ClienteW3producaoRESTModel(Cliente cliente){
		this.cdpessoa = cliente.getCdpessoa();
		this.nome = cliente.getNome();
	}
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
