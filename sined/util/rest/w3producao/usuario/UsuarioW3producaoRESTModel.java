package br.com.linkcom.sined.util.rest.w3producao.usuario;

import br.com.linkcom.sined.geral.bean.Usuario;

public class UsuarioW3producaoRESTModel {
	
	private Integer cdpessoa;
	private String nome;
	private String login;
	private String senha;
	private String senhaterminal;
	
	public UsuarioW3producaoRESTModel() {}
	public UsuarioW3producaoRESTModel(Usuario usuario) {
		this.cdpessoa = usuario.getCdpessoa();
		this.nome = usuario.getNome();
		this.login = usuario.getLogin();
		this.senha = usuario.getSenha();
		this.senhaterminal = usuario.getSenhaterminal() != null ? usuario.getSenhaterminal().toString() : null;
	}
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public String getLogin() {
		return login;
	}
	public String getSenha() {
		return senha;
	}
	public String getSenhaterminal() {
		return senhaterminal;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setSenhaterminal(String senhaterminal) {
		this.senhaterminal = senhaterminal;
	}
}
