package br.com.linkcom.sined.util.rest.w3producao.retorno;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/RetornoProducaoW3producao")
public class RetornoProducaoW3producaoRESTWS implements SimpleRESTWS<RetornoProducaoW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(RetornoProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(RetornoProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(RetornoProducaoW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		RetornoProducaoW3producaoRESTModel model = ProducaochaofabricaService.getInstance().retornoProducaoForW3producao(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(RetornoProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
