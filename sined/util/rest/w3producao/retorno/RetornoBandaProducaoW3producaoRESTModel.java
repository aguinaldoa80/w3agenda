package br.com.linkcom.sined.util.rest.w3producao.retorno;

public class RetornoBandaProducaoW3producaoRESTModel {

	private Boolean sincronizado;
	
	public Boolean getSincronizado() {
		return sincronizado;
	}
	
	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}
	
}
