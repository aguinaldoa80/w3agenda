package br.com.linkcom.sined.util.rest.w3producao.retorno;



public class RetornoServicoProducaoW3producaoRESTWSBean {

	private Integer cdproducaochaofabrica;
	private Integer cdmaterial;

	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}	
}
