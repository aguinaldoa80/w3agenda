package br.com.linkcom.sined.util.rest.w3producao.retorno;

import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoCampoadicionalW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoConstatacaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoEquipamentoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoEtapaW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoHistoricoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoItemadicionalW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoMateriaprimaW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoPneuW3producaoRESWSTBean;

public class RetornoProducaoW3producaoRESTWSBean {

	private Integer cdproducaochaofabrica;
	private Integer cdmotivodevolucao;
	private Integer cdmaterial;
	private String observacaorecusa;
	private Double residuobanda;
	private Integer cdgarantiatipo;
	private Integer cdgarantiatipopercentual;
	
	private RetornoProducaoPneuW3producaoRESWSTBean pneu;
	private RetornoProducaoEtapaW3producaoRESTWSBean[] etapas;
	private RetornoProducaoCampoadicionalW3producaoRESTWSBean[] camposadicionais;
	private RetornoProducaoEquipamentoW3producaoRESTWSBean[] equipamentos;
	private RetornoProducaoItemadicionalW3producaoRESTWSBean[] itensadicionais;
	private RetornoProducaoMateriaprimaW3producaoRESTWSBean[] materiasprimas;
	private RetornoProducaoHistoricoW3producaoRESTWSBean[] historicos;
	private RetornoProducaoConstatacaoW3producaoRESTWSBean[] constatacoes;
	private RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean[] motivosdevolucoes;
	
	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}

	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}

	public String getObservacaorecusa() {
		return observacaorecusa;
	}
	
	public Double getResiduobanda() {
		return residuobanda;
	}

	public Integer getCdgarantiatipo() {
		return cdgarantiatipo;
	}

	public Integer getCdgarantiatipopercentual() {
		return cdgarantiatipopercentual;
	}

	public RetornoProducaoEtapaW3producaoRESTWSBean[] getEtapas() {
		return etapas;
	}

	public RetornoProducaoCampoadicionalW3producaoRESTWSBean[] getCamposadicionais() {
		return camposadicionais;
	}

	public RetornoProducaoEquipamentoW3producaoRESTWSBean[] getEquipamentos() {
		return equipamentos;
	}

	public RetornoProducaoItemadicionalW3producaoRESTWSBean[] getItensadicionais() {
		return itensadicionais;
	}

	public RetornoProducaoMateriaprimaW3producaoRESTWSBean[] getMateriasprimas() {
		return materiasprimas;
	}

	public RetornoProducaoPneuW3producaoRESWSTBean getPneu() {
		return pneu;
	}

	public RetornoProducaoHistoricoW3producaoRESTWSBean[] getHistoricos() {
		return historicos;
	}
	
	public RetornoProducaoConstatacaoW3producaoRESTWSBean[] getConstatacoes() {
		return constatacoes;
	}
	
	public RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean[] getMotivosdevolucoes() {
		return motivosdevolucoes;
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setPneu(RetornoProducaoPneuW3producaoRESWSTBean pneu) {
		this.pneu = pneu;
	}

	public void setHistoricos(
			RetornoProducaoHistoricoW3producaoRESTWSBean[] historicos) {
		this.historicos = historicos;
	}
	
	public void setConstatacoes(RetornoProducaoConstatacaoW3producaoRESTWSBean[] constatacoes) {
		this.constatacoes = constatacoes;
	}
	
	public void setMotivosdevolucoes(RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean[] motivosdevolucoes) {
		this.motivosdevolucoes = motivosdevolucoes;
	}
	
	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}

	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}

	public void setObservacaorecusa(String observacaorecusa) {
		this.observacaorecusa = observacaorecusa;
	}

	public void setResiduobanda(Double residuobanda) {
		this.residuobanda = residuobanda;
	}

	public void setCdgarantiatipo(Integer cdgarantiatipo) {
		this.cdgarantiatipo = cdgarantiatipo;
	}

	public void setCdgarantiatipopercentual(Integer cdgarantiatipopercentual) {
		this.cdgarantiatipopercentual = cdgarantiatipopercentual;
	}

	public void setEtapas(RetornoProducaoEtapaW3producaoRESTWSBean[] etapas) {
		this.etapas = etapas;
	}

	public void setCamposadicionais(
			RetornoProducaoCampoadicionalW3producaoRESTWSBean[] camposadicionais) {
		this.camposadicionais = camposadicionais;
	}

	public void setEquipamentos(
			RetornoProducaoEquipamentoW3producaoRESTWSBean[] equipamentos) {
		this.equipamentos = equipamentos;
	}

	public void setItensadicionais(
			RetornoProducaoItemadicionalW3producaoRESTWSBean[] itensadicionais) {
		this.itensadicionais = itensadicionais;
	}

	public void setMateriasprimas(
			RetornoProducaoMateriaprimaW3producaoRESTWSBean[] materiasprimas) {
		this.materiasprimas = materiasprimas;
	}
	
}
