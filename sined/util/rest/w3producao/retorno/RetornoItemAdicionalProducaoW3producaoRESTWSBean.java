package br.com.linkcom.sined.util.rest.w3producao.retorno;


public class RetornoItemAdicionalProducaoW3producaoRESTWSBean {

	private Integer cdproducaoagenda;
	private Integer etapa_id;
	private Integer cdmaterial;
	private Integer cdpneu;
	private Double quantidade;
	
	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}

	public Integer getEtapa_id() {
		return etapa_id;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Double getQuantidade() {
		return quantidade;
	}
	
	public Integer getCdpneu() {
		return cdpneu;
	}
	
	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}

	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}

	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}	
}
