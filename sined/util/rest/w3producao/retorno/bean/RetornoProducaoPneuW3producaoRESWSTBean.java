package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoPneuW3producaoRESWSTBean {

	protected Integer cdpneu;
	protected Integer cdmaterialbanda;
	protected Integer cdpneuqualificacao;
	
	public Integer getCdpneu() {
		return cdpneu;
	}
	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}
	public Integer getCdpneuqualificacao() {
		return cdpneuqualificacao;
	}
	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}
	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}
	public void setCdpneuqualificacao(Integer cdpneuqualificacao) {
		this.cdpneuqualificacao = cdpneuqualificacao;
	}
}
