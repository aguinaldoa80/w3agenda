package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoEquipamentoW3producaoRESTWSBean {

	private Integer etapa_id;
	private Integer cdmaterial;
	private Integer cdpatrimonioitem;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdpatrimonioitem() {
		return cdpatrimonioitem;
	}
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdpatrimonioitem(Integer cdpatrimonioitem) {
		this.cdpatrimonioitem = cdpatrimonioitem;
	}
	
}
