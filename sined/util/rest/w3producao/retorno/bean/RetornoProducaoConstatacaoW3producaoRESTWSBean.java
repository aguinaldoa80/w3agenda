package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoConstatacaoW3producaoRESTWSBean {
	
	private Integer etapa_id;
	private Integer cdmotivodevolucao;
	private String observacao;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}
	public String getObservacao() {
		return observacao;
	}
	
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
