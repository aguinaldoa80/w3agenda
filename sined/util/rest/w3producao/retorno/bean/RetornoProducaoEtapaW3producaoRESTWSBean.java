package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoEtapaW3producaoRESTWSBean {

	private Integer etapa_id;
	private Integer cdproducaoetapa;
	private Integer cdproducaoetapaitem;
	private Integer etapaanterior_id;
	private String observacao;
	private Boolean concluida;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public String getObservacao() {
		return observacao;
	}
	public Integer getCdproducaoetapaitem() {
		return cdproducaoetapaitem;
	}
	public Integer getEtapaanterior_id() {
		return etapaanterior_id;
	}
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public void setCdproducaoetapaitem(Integer cdproducaoetapaitem) {
		this.cdproducaoetapaitem = cdproducaoetapaitem;
	}
	public void setEtapaanterior_id(Integer etapaanterior_id) {
		this.etapaanterior_id = etapaanterior_id;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Boolean getConcluida() {
		return concluida;
	}
	public void setConcluida(Boolean concluida) {
		this.concluida = concluida;
	}
	
}
