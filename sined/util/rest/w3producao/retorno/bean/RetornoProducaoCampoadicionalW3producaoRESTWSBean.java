package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoCampoadicionalW3producaoRESTWSBean {

	private Integer etapa_id;
	private Integer cdproducaoetapanomecampo;
	private Boolean valorboleano;
	private String valorcaracter;
	
	public Integer getCdproducaoetapanomecampo() {
		return cdproducaoetapanomecampo;
	}
	public Boolean getValorboleano() {
		return valorboleano;
	}
	public String getValorcaracter() {
		return valorcaracter;
	}
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public void setCdproducaoetapanomecampo(Integer cdproducaoetapanomecampo) {
		this.cdproducaoetapanomecampo = cdproducaoetapanomecampo;
	}
	public void setValorboleano(Boolean valorboleano) {
		this.valorboleano = valorboleano;
	}
	public void setValorcaracter(String valorcaracter) {
		this.valorcaracter = valorcaracter;
	}
	
}
