package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoMateriaprimaW3producaoRESTWSBean {

	private Integer etapa_id;
	private Integer cdmaterial;
	private Double quantidade;
	private Integer cdunidademedida;
	private Integer ordemexibicao;
	private Boolean banda;
	private Boolean acompanhabanda;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Boolean getAcompanhabanda() {
		return acompanhabanda;
	}
	public void setAcompanhabanda(Boolean acompanhabanda) {
		this.acompanhabanda = acompanhabanda;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public Boolean getBanda() {
		return banda;
	}
	public void setBanda(Boolean banda) {
		this.banda = banda;
	}
	
}
