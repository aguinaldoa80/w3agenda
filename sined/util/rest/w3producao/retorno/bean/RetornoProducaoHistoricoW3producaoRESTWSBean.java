package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;


public class RetornoProducaoHistoricoW3producaoRESTWSBean {
	
	private Integer etapa_id;
	private Integer acao_id;
	private Integer cdusuario;
	private Long datahora;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public Integer getAcao_id() {
		return acao_id;
	}
	public Integer getCdusuario() {
		return cdusuario;
	}
	public Long getDatahora() {
		return datahora;
	}
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public void setAcao_id(Integer acao_id) {
		this.acao_id = acao_id;
	}
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
	public void setDatahora(Long datahora) {
		this.datahora = datahora;
	}

}
