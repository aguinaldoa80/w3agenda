package br.com.linkcom.sined.util.rest.w3producao.retorno.bean;

public class RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean {
	
	private Integer etapa_id;
	private Integer cdmotivodevolucao;
	
	public Integer getEtapa_id() {
		return etapa_id;
	}
	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}
	
	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}
	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}
}
