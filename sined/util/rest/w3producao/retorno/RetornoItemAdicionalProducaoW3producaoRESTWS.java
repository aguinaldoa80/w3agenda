package br.com.linkcom.sined.util.rest.w3producao.retorno;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ProducaoagendaitemadicionalService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/RetornoItemAdicionalProducaoW3producao")
public class RetornoItemAdicionalProducaoW3producaoRESTWS implements SimpleRESTWS<RetornoItemAdicionalProducaoW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(RetornoItemAdicionalProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(RetornoItemAdicionalProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(RetornoItemAdicionalProducaoW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		RetornoItemAdicionalProducaoW3producaoRESTModel model = ProducaoagendaitemadicionalService.getInstance().retornoProducaoItemAdicionalForW3producao(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(RetornoItemAdicionalProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}
}