package br.com.linkcom.sined.util.rest.w3producao.retorno;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/RetornoBandaProducaoW3producao")
public class RetornoBandaProducaoW3producaoRESTWS implements SimpleRESTWS<RetornoBandaProducaoW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(RetornoBandaProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(RetornoBandaProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(RetornoBandaProducaoW3producaoRESTWSBean command) {
		ModelAndStatus modelAndStatus = new ModelAndStatus();
		RetornoBandaProducaoW3producaoRESTModel model = PneuService.getInstance().retornoBandaProducaoForW3producao(command);
		modelAndStatus.setModel(model);
		return modelAndStatus;
	}

	@Override
	public ModelAndStatus get(RetornoBandaProducaoW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}
}