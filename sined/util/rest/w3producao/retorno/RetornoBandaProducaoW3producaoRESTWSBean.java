package br.com.linkcom.sined.util.rest.w3producao.retorno;



public class RetornoBandaProducaoW3producaoRESTWSBean {

	private Integer cdproducaochaofabrica;
	private Integer cdpneu;
	private Integer cdmaterialbanda;
	private Double qtdeprevista;
	private Integer cdunidademedida;
	private Integer cdmaterialacompanhabanda;
	private Double qtdeprevistaacompanhabanda;
	private Integer cdunidademedidaacompanhabanda;
	
	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}
	
	public Integer getCdpneu() {
		return cdpneu;
	}

	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}
	
	public Double getQtdeprevista() {
		return qtdeprevista;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Integer getCdmaterialacompanhabanda() {
		return cdmaterialacompanhabanda;
	}

	public Double getQtdeprevistaacompanhabanda() {
		return qtdeprevistaacompanhabanda;
	}

	public Integer getCdunidademedidaacompanhabanda() {
		return cdunidademedidaacompanhabanda;
	}

	public void setCdmaterialacompanhabanda(Integer cdmaterialacompanhabanda) {
		this.cdmaterialacompanhabanda = cdmaterialacompanhabanda;
	}

	public void setQtdeprevistaacompanhabanda(Double qtdeprevistaacompanhabanda) {
		this.qtdeprevistaacompanhabanda = qtdeprevistaacompanhabanda;
	}

	public void setCdunidademedidaacompanhabanda(
			Integer cdunidademedidaacompanhabanda) {
		this.cdunidademedidaacompanhabanda = cdunidademedidaacompanhabanda;
	}

	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}
	
	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}

	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}
	
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
}