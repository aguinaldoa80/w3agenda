package br.com.linkcom.sined.util.rest.w3producao.material;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialW3producaoRESTModel {
	
	private Integer cdmaterial;
	private String nome;
	private Integer cdunidademedida;
	private Integer cdproducaoetapa;
	private Integer cdmaterialgrupo;
	private Integer cdmaterialacompanhabanda;
	private Boolean producao;
	private Boolean patrimonio;
	private Boolean ativo;
	private Double profundidadesulco;
	
	public MaterialW3producaoRESTModel() {}
	
	public MaterialW3producaoRESTModel(Material material){
		this.cdmaterial = material.getCdmaterial();
		this.nome = material.getNome();
		this.cdunidademedida = material.getUnidademedida()!= null ? material.getUnidademedida().getCdunidademedida() : null;
		this.cdproducaoetapa = material.getProducaoetapa()!=null ? material.getProducaoetapa().getCdproducaoetapa() : null;
		this.cdmaterialgrupo = material.getMaterialgrupo()!=null ? material.getMaterialgrupo().getCdmaterialgrupo() : null;
		this.cdmaterialacompanhabanda = material.getMaterialacompanhabanda()!=null ? material.getMaterialacompanhabanda().getCdmaterial(): null;
		this.producao = material.getProducao();
		this.patrimonio = material.getPatrimonio();
		this.ativo = material.getAtivo();
		this.profundidadesulco = material.getProfundidadesulco();
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getNome() {
		return nome;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	public Integer getCdmaterialacompanhabanda() {
		return cdmaterialacompanhabanda;
	}
	public Boolean getProducao() {
		return producao;
	}
	public Boolean getPatrimonio() {
		return patrimonio;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Double getProfundidadesulco() {
		return profundidadesulco;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	public void setCdmaterialacompanhabanda(Integer cdmaterialacompanhabanda) {
		this.cdmaterialacompanhabanda = cdmaterialacompanhabanda;
	}
	public void setProducao(Boolean producao) {
		this.producao = producao;
	}
	public void setPatrimonio(Boolean patrimonio) {
		this.patrimonio = patrimonio;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setProfundidadesulco(Double profundidadesulco) {
		this.profundidadesulco = profundidadesulco;
	}
}