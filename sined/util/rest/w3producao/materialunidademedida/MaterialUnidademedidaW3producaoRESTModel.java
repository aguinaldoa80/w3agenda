package br.com.linkcom.sined.util.rest.w3producao.materialunidademedida;

import br.com.linkcom.sined.geral.bean.Materialunidademedida;

public class MaterialUnidademedidaW3producaoRESTModel {
	
	private Integer cdmaterialunidademedida;
	private Integer cdmaterial;
	private Integer cdunidademedida;
	private Double fracao;
	private Double qtdereferencia;
	private Boolean prioridadeproducao;
	
	public MaterialUnidademedidaW3producaoRESTModel() {}
	
	public MaterialUnidademedidaW3producaoRESTModel(Materialunidademedida materialunidademedida){
		this.cdmaterialunidademedida = materialunidademedida.getCdmaterialunidademedida();
		this.cdmaterial = materialunidademedida.getMaterial()!=null ? materialunidademedida.getMaterial().getCdmaterial() : null;
		this.cdunidademedida = materialunidademedida.getUnidademedida()!=null ? materialunidademedida.getUnidademedida().getCdunidademedida() : null;
		this.fracao = materialunidademedida.getFracao();
		this.qtdereferencia = materialunidademedida.getQtdereferencia();
		this.prioridadeproducao = materialunidademedida.getPrioridadeproducao() != null && materialunidademedida.getPrioridadeproducao();
	}
	
	public Integer getCdmaterialunidademedida() {
		return cdmaterialunidademedida;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public Double getFracao() {
		return fracao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public Boolean getPrioridadeproducao() {
		return prioridadeproducao;
	}
	public void setCdmaterialunidademedida(Integer cdmaterialunidademedida) {
		this.cdmaterialunidademedida = cdmaterialunidademedida;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setPrioridadeproducao(Boolean prioridadeproducao) {
		this.prioridadeproducao = prioridadeproducao;
	}
	

}
