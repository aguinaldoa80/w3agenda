package br.com.linkcom.sined.util.rest.w3producao.clienteVendedor;


public class ClienteVendedorW3producaoRESTModel {
	private Integer cdColaborador;
	private Integer cdCliente;
	private String nomeColaborador;
	
	
	public Integer getCdColaborador() {
		return cdColaborador;
	}
	public Integer getCdCliente() {
		return cdCliente;
	}
	public String getNomeColaborador() {
		return nomeColaborador;
	}
	public void setCdColaborador(Integer cdColaborador) {
		this.cdColaborador = cdColaborador;
	}
	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}
	public void setNomeColaborador(String nomeColaborador) {
		this.nomeColaborador = nomeColaborador;
	}
}
