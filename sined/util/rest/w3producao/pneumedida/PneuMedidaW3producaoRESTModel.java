package br.com.linkcom.sined.util.rest.w3producao.pneumedida;

import br.com.linkcom.sined.geral.bean.Pneumedida;

public class PneuMedidaW3producaoRESTModel {
	
	private Integer cdpneumedida;
	private String nome;
	
	public PneuMedidaW3producaoRESTModel() {}
	
	public PneuMedidaW3producaoRESTModel(Pneumedida pneumedida){
		this.cdpneumedida = pneumedida.getCdpneumedida();
		this.nome = pneumedida.getNome();
	}
	
	
	public Integer getCdpneumedida() {
		return cdpneumedida;
	}
	public String getNome() {
		return nome;
	}
	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
