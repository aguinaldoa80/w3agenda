package br.com.linkcom.sined.util.rest.w3producao.producaoetapanome;

import br.com.linkcom.sined.geral.bean.Producaoetapanome;

public class ProducaoEtapaNomeW3producaoRESTModel {
	
	private Integer cdproducaoetapanome;
	private String nome;
	private Boolean permitirgerargarantia;
	private Boolean enviaritemadicionalaposconclusao;
	private Boolean enviarservicoaposconclusao;
	private Boolean enviarbandaaposconclusao;
	
	public ProducaoEtapaNomeW3producaoRESTModel() {}
	
	public ProducaoEtapaNomeW3producaoRESTModel(Producaoetapanome producaoetapanome){
		this.cdproducaoetapanome = producaoetapanome.getCdproducaoetapanome();
		this.nome = producaoetapanome.getNome();
		this.permitirgerargarantia = producaoetapanome.getPermitirgerargarantia();
		this.enviaritemadicionalaposconclusao = producaoetapanome.getEnviaritemadicionalaposconclusao();
		this.enviarservicoaposconclusao = producaoetapanome.getEnviarservicoaposconclusao();
		this.enviarbandaaposconclusao = producaoetapanome.getEnviarbandaaposconclusao();
	}

	public Integer getCdproducaoetapanome() {
		return cdproducaoetapanome;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getPermitirgerargarantia() {
		return permitirgerargarantia;
	}
	public Boolean getEnviaritemadicionalaposconclusao() {
		return enviaritemadicionalaposconclusao;
	}
	public Boolean getEnviarservicoaposconclusao() {
		return enviarservicoaposconclusao;
	}
	public Boolean getEnviarbandaaposconclusao() {
		return enviarbandaaposconclusao;
	}

	public void setCdproducaoetapanome(Integer cdproducaoetapanome) {
		this.cdproducaoetapanome = cdproducaoetapanome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setPermitirgerargarantia(Boolean permitirgerargarantia) {
		this.permitirgerargarantia = permitirgerargarantia;
	}
	public void setEnviaritemadicionalaposconclusao(Boolean enviaritemadicionalaposconclusao) {
		this.enviaritemadicionalaposconclusao = enviaritemadicionalaposconclusao;
	}
	public void setEnviarservicoaposconclusao(Boolean enviarservicoaposconclusao) {
		this.enviarservicoaposconclusao = enviarservicoaposconclusao;
	}
	public void setEnviarbandaaposconclusao(Boolean enviarbandaaposconclusao) {
		this.enviarbandaaposconclusao = enviarbandaaposconclusao;
	}	
}