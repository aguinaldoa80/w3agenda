package br.com.linkcom.sined.util.rest.w3producao.empresa;

import br.com.linkcom.sined.geral.bean.Empresa;

public class EmpresaW3producaoRESTModel {
	
	private Integer cdpessoa;
	private String nome;
	private String nome_fantasia;
	
	public EmpresaW3producaoRESTModel(){}
	public EmpresaW3producaoRESTModel(Empresa empresa){
		this.cdpessoa = empresa.getCdpessoa();
		this.nome = empresa.getNome();
		this.nome_fantasia = empresa.getNomefantasia();
	}
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public String getNome_fantasia() {
		return nome_fantasia;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNome_fantasia(String nomeFantasia) {
		nome_fantasia = nomeFantasia;
	}
}
