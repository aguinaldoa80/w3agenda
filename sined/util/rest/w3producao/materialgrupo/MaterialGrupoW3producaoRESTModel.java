package br.com.linkcom.sined.util.rest.w3producao.materialgrupo;

import br.com.linkcom.sined.geral.bean.Materialgrupo;

public class MaterialGrupoW3producaoRESTModel {
	
	private Integer cdmaterialgrupo;
	private String nome;
	
	public MaterialGrupoW3producaoRESTModel() {}
	
	public MaterialGrupoW3producaoRESTModel(Materialgrupo materialgrupo){
		this.cdmaterialgrupo = materialgrupo.getCdmaterialgrupo();
		this.nome = materialgrupo.getNome();
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public String getNome() {
		return nome;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
