package br.com.linkcom.sined.util.rest.w3producao.atualizartabela;

public class AtualizarTabelaW3producaoRESTWSBean {
	
	private Integer cdpneu;
	private Integer cdcliente;
	private Integer cdmaterial;
	private Integer cdmaterialgarantido;
	private Integer cdproducaoetapa;
	
	public Integer getCdpneu() {
		return cdpneu;
	}
	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public Integer getCdmaterialgarantido() {
		return cdmaterialgarantido;
	}
	public void setCdmaterialgarantido(Integer cdmaterialgarantido) {
		this.cdmaterialgarantido = cdmaterialgarantido;
	}	
}
