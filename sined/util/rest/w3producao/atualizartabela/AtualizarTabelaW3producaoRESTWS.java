package br.com.linkcom.sined.util.rest.w3producao.atualizartabela;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.ProducaoetapaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.ModelAndStatus;
import br.com.linkcom.util.rest.SimpleRESTWS;
import br.com.linkcom.util.rest.authentication.AuthenticateByKey;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

@Bean
@br.com.linkcom.util.rest.annotations.URI("/w3producao/AtualizarTabelaW3producao")
public class AtualizarTabelaW3producaoRESTWS implements SimpleRESTWS<AtualizarTabelaW3producaoRESTWSBean>, AuthenticateByKey {

	@Override
	public ModelAndStatus delete(AtualizarTabelaW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus put(AtualizarTabelaW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public ModelAndStatus post(AtualizarTabelaW3producaoRESTWSBean command) {
		if(command.getCdpneu() != null){
			PneuService.getInstance().atualizaW3producao(command.getCdpneu());
		} else if(command.getCdcliente() != null){
			ClienteService.getInstance().atualizaW3producao(command.getCdcliente());
		} else if(command.getCdmaterial() != null){
			MaterialService.getInstance().atualizaW3producao(command.getCdmaterial());
		} else if(command.getCdmaterialgarantido() != null){
			MaterialService.getInstance().atualizaW3producao(command.getCdmaterialgarantido());
		} else if(command.getCdproducaoetapa() != null){
			ProducaoetapaService.getInstance().atualizaW3producao(command.getCdproducaoetapa());
		}
		return new ModelAndStatus();
	}

	@Override
	public ModelAndStatus get(AtualizarTabelaW3producaoRESTWSBean command) {
		return null;
	}

	@Override
	public boolean verifyCredentials(String key) throws NotAuthenticatedException {
		return SinedUtil.validaChaveWSW3Producao(key);
	}

}
