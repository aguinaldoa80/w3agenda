package br.com.linkcom.sined.util.rest.w3producao.motivodevolucao;

import br.com.linkcom.sined.geral.bean.Motivodevolucao;

public class MotivoDevolucaoW3producaoRESTModel {
	
	private Integer cdmotivodevolucao;
	private String descricao;
	private Boolean constatacao;
	private Boolean motivodevolucao;
	
	public MotivoDevolucaoW3producaoRESTModel() {}
	
	public MotivoDevolucaoW3producaoRESTModel(Motivodevolucao motivodevolucao){
		this.cdmotivodevolucao = motivodevolucao.getCdmotivodevolucao();
		this.descricao = motivodevolucao.getDescricao();
		this.constatacao = motivodevolucao.getConstatacao();
		this.motivodevolucao = motivodevolucao.getMotivodevolucao();
	}	
	
	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}
	public String getDescricao() {
		return descricao;
	}
	public Boolean getConstatacao() {
		return constatacao;
	}
	public Boolean getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setConstatacao(Boolean constatacao) {
		this.constatacao = constatacao;
	}
	public void setMotivodevolucao(Boolean motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
}