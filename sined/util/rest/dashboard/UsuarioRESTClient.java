package br.com.linkcom.sined.util.rest.dashboard;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.util.rest.authentication.AuthenticateByKeyBean;
import br.com.linkcom.util.rest.client.GenericRESTClient;
import br.com.linkcom.util.rest.client.HttpMethodEnum;
import br.com.linkcom.util.rest.client.RESTClientException;

public class UsuarioRESTClient extends GenericRESTClient {
	
	private static final String URI = "/Usuario";

	public UsuarioRESTModel saveUsuario(UsuarioRESTBean usuarioBean) throws IOException, RESTClientException, URISyntaxException {
    	return (UsuarioRESTModel) post(UsuarioRESTModel.class, usuarioBean);
	}
	
	public UsuarioRESTModel deleteUsuario(UsuarioRESTBean usuarioBean) throws IOException, RESTClientException, URISyntaxException {
    	return (UsuarioRESTModel) delete(UsuarioRESTModel.class, usuarioBean);
	}

	@Override
	public URI uri() throws URISyntaxException {
		return new URI(URI);
	}

	@Override
	protected URL servidor() throws MalformedURLException {
		String url = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.URL_INTEGRACAO_DASHBOARD);
		return new URL(url);
	}

	@Override
	public int timeout() {
		return 30;
	}
	
	@Override
    protected void beforeSend(HttpMethodEnum method, Class<?> clazz, Object param) {
    	if(!(param instanceof AuthenticateByKeyBean)){
    		throw new RuntimeException("O Bean que � enviado pelo WS precisa extender a classe AuthenticateByKeyBean.");
    	}
    	System.out.println(method);
    }
		
}