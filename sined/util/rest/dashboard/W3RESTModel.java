package br.com.linkcom.sined.util.rest.dashboard;

public class W3RESTModel {
	
	private boolean erro = false;
	private Integer codigo_erro;
	private String mensagem;
	
	public boolean isErro() {
		return erro;
	}
	public Integer getCodigo_erro() {
		return codigo_erro;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setErro(boolean erro) {
		this.erro = erro;
	}
	public void setCodigo_erro(Integer codigoErro) {
		codigo_erro = codigoErro;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
}
