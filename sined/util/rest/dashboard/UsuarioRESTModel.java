package br.com.linkcom.sined.util.rest.dashboard;

public class UsuarioRESTModel extends W3RESTModel{
	
	private String token;
	private String cdpessoa;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCdpessoa() {
		return cdpessoa;
	}

	public void setCdpessoa(String cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	@Override
	public String toString() {
		return "RESTMODEL[token="+token+ ", codigo_erro="+getCodigo_erro()+ ", Mensagem="+getMensagem()+", erro="+isErro()+ "]";
	}
}