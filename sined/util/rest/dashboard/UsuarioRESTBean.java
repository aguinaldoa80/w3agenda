package br.com.linkcom.sined.util.rest.dashboard;

import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaEnum;
import br.com.linkcom.util.rest.authentication.AuthenticateByKeyBean;

public class UsuarioRESTBean extends AuthenticateByKeyBean {
	
	private String token;	
	private String cpf;	
	private String email;		
	
	//Required Fields
	private String nome;
	private String login;
	private Integer idExterno;
	private String sistemaOrigem;
	private TipoPessoaEnum tipoPessoa;	
	
	public UsuarioRESTBean(Integer id) {
		this.idExterno = id;
	}
	public String getToken() {
		return token;
	}
	public String getCpf() {
		return cpf;
	}
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	public String getLogin() {
		return login;
	}
	public Integer getIdExterno() {
		return idExterno;
	}
	public String getSistemaOrigem() {
		return sistemaOrigem;
	}
	public void setSistemaOrigem(String sistemaOrigem) {
		this.sistemaOrigem = sistemaOrigem;
	}
	public TipoPessoaEnum getTipoPessoa() {
		return tipoPessoa;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setIdExterno(Integer id) {
		this.idExterno = id;
	}
	public void setTipoPessoa(TipoPessoaEnum tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
}
