package br.com.linkcom.sined.util.loteestoque;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.service.LoteestoqueService;

@Bean
@Controller(path="/suprimento/process/LoteestoqueAutoComplete")
public class LoteestoqueAutoCompleteController extends MultiActionController {
	
	private LoteestoqueService loteestoqueService;
	
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}

	@DefaultAction
	public void view(WebRequestContext request, LoteestoqueTreeFiltro filtro){
		List<Loteestoque> find = new ArrayList<Loteestoque>();
		if(filtro.getQ() != null && filtro.getQ().length() > 2 && filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			find = loteestoqueService.findAutocompleteVinculoMaterialComponent(filtro.getQ(), filtro.getMaterial(), filtro.getEmpresa(), filtro.getLocalarmazenagem());
		}
		
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(find));
	}

	private String criarEstrutura(List<Loteestoque> find) {
		StringBuilder builder = new StringBuilder();
		if(find != null && find.size() > 0){
			for (Loteestoque le : find) {
				builder.append(le.getNumerovalidadeqtde() + "|" + le.getCdloteestoque() + "\n");
			}
		}
		return builder.toString();
	}

}
