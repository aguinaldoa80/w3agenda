package br.com.linkcom.sined.util.loteestoque;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.service.LoteestoqueService;

@Bean
@Controller(path="/suprimento/process/LoteestoqueTreeView")
public class LoteestoqueTreeController extends MultiActionController {
	
	private LoteestoqueService LoteestoqueService;
	
	public void setLoteestoqueService(LoteestoqueService LoteestoqueService) {
		this.LoteestoqueService = LoteestoqueService;
	}

	@DefaultAction
	public ModelAndView view(WebRequestContext request, LoteestoqueTreeFiltro filtro){
		List<Loteestoque> listaLoteestoque = LoteestoqueService.findTreeView(request, filtro.getMaterial(), filtro.getEmpresa(), filtro.getLocalarmazenagem());
		
		String codigo_treeview = this.criaEstrutura(listaLoteestoque);
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		return new ModelAndView("direct:/process/loteestoqueTreeView");
	}

	private String criaEstrutura(List<Loteestoque> lista) {
		if(lista != null && lista.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (Loteestoque le : lista) {
				sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
				sb.append(le.getCdloteestoque());
				sb.append(", '");
				sb.append(le.getNumerovalidade().replace("'", "\\'"));
				sb.append("')\">");
				sb.append(le.getNumerovalidadeqtde());
				sb.append("</span></li>");
			}
			return sb.toString();
		} else return "";
	}
}
