package br.com.linkcom.sined.util.loteestoque;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;


public class LoteestoqueTreeFiltro {

	private String propriedade;
	private String q;
	private String cdmaterial;
	private String cdempresa;
	private String cdlocalarmazenagem;
	private Material material;
	private Empresa empresa;
	private Localarmazenagem localarmazenagem;
	
	public String getPropriedade() {
		return propriedade;
	}
	public String getQ() {
		return q;
	}
	public String getCdmaterial() {
		return cdmaterial;
	}
	public String getCdempresa() {
		return cdempresa;
	}
	public String getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public Material getMaterial() {
		return material;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public void setCdmaterial(String cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdempresa(String cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdlocalarmazenagem(String cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
}