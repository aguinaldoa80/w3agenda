package br.com.linkcom.sined.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.controller.simple.bean.AvisoEquipeBean;
import br.com.linkcom.sined.geral.service.PedidovendaService;

public class W3ControleUtil {
	
	private final static String AVISO_EQUIPE_SESSION = "AVISO_EQUIPE_SESSION"; 
	
	@SuppressWarnings("unchecked")
	public static ArrayList<AvisoEquipeBean> getAvisoEquipe(){
		Object obj = NeoWeb.getRequestContext().getSession().getAttribute(AVISO_EQUIPE_SESSION);
		
		if (obj == null) {
			ArrayList<AvisoEquipeBean> listaAvisoEquipeBean = new ArrayList<AvisoEquipeBean>();
			try {
				listaAvisoEquipeBean.addAll(getAvisoEquipeW3Controle());
			} catch (Exception e) {
				e.printStackTrace();
				
				try {
					String nomeMaquina = "N�o encontrado.";
					try {  
						InetAddress localaddr = InetAddress.getLocalHost();  
						nomeMaquina = localaddr.getHostName();  
					} catch (UnknownHostException e3) {}  
					
					
					String url = "N�o encontrado";
					try {  
						url = SinedUtil.getUrlWithContext();
					} catch (Exception e3) {}
					
					
					StringWriter stringWriter = new StringWriter();
					e.printStackTrace(new PrintWriter(stringWriter));
					PedidovendaService.getInstance().enviarEmailErro("[W3ERP] Erro ao consultar o W3Controle para pegar Avisos de Equipe.","Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ", URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
				} catch (Exception e2) {
					e.printStackTrace();
				}
			}
			NeoWeb.getRequestContext().getSession().setAttribute(AVISO_EQUIPE_SESSION, listaAvisoEquipeBean);
			return listaAvisoEquipeBean;
		} else {
			return (ArrayList<AvisoEquipeBean>) obj; 
		}
	}

	private static ArrayList<AvisoEquipeBean> getAvisoEquipeW3Controle() throws NamingException, IOException, JDOMException {
		URL url = montaURLRequisicao();
		
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.flush();
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		String line;
		String xml = "";
		
		while ((line = rd.readLine()) != null) {
			xml += line;
		}
		
		wr.close();
		rd.close();
		
		return verificaXmlAvisoEquipe(xml);
	}
	
	@SuppressWarnings("unchecked")
	private static ArrayList<AvisoEquipeBean> verificaXmlAvisoEquipe(String xml) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
		
		if (rootElement != null && rootElement.getContent().size() > 0) {
			ArrayList<AvisoEquipeBean> listaAvisoEquipe = new ArrayList<AvisoEquipeBean>();
			List<Element> avisoEquipeElement = SinedUtil.getListChildElement("avisoEquipe", rootElement.getContent());
			
			if (avisoEquipeElement != null && avisoEquipeElement.size() > 0) {
				for (Element avisoEquipeElt : avisoEquipeElement) {
					Element tituloElement = SinedUtil.getChildElement("titulo", avisoEquipeElt.getContent());
					Element conteudoElement = SinedUtil.getChildElement("conteudo", avisoEquipeElt.getContent());
					Element dtInicioElement = SinedUtil.getChildElement("dtInicio", avisoEquipeElt.getContent());
					
					AvisoEquipeBean avisoEquipeBean = new AvisoEquipeBean();
					avisoEquipeBean.setTitulo(SinedUtil.getElementString(tituloElement));
					avisoEquipeBean.setConteudo(SinedUtil.getElementString(conteudoElement));
					avisoEquipeBean.setDtInicio(SinedUtil.getElementString(dtInicioElement));
					listaAvisoEquipe.add(avisoEquipeBean);
				}
			}
			
			return listaAvisoEquipe;
		}
		
		return new ArrayList<AvisoEquipeBean>();
	}
	
	private static URL montaURLRequisicao() throws MalformedURLException, NamingException {
		String urlString = "linkcom.w3erp.com.br";
		
		if(SinedUtil.isAmbienteDesenvolvimento()){
			urlString = InitialContext.doLookup("W3CONTROLE_URL");
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o dos dashboards.");
			}
		}
		
		return new URL("http://" + urlString + "/w3controle/pub/process/AvisoEquipe");
	}

}
