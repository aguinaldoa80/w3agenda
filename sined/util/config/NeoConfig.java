package br.com.linkcom.sined.util.config;

import br.com.linkcom.neo.core.config.DefaultConfig;

public class NeoConfig extends DefaultConfig {
	@Override
	public String getRequiredRenderType() {
		return "addclass";
	}
	
	@Override
	public String getInsertOnePathParameters() {
		return "showListagemLink=false&closeOnCancel=true";
	}
}
