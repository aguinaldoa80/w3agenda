package br.com.linkcom.sined.util;

import org.hibernate.Hibernate;
import org.hibernate.dialect.function.StandardSQLFunction;

import br.com.linkcom.neo.persistence.PostgresSqlDialectForFTS;
import br.com.linkcom.sined.util.persistence.ClientePessoaAutocompleteFunction;
import br.com.linkcom.sined.util.persistence.OtimizacaoDocumentoListagemFunction;
import br.com.linkcom.sined.util.persistence.OtimizacaoMovimentacaoGeradaFunction;
import br.com.linkcom.sined.util.persistence.OtimizacaoPedidoVendaProjetoFunction;
import br.com.linkcom.sined.util.persistence.OtimizacaoVendaProjetoFunction;
import br.com.linkcom.sined.util.persistence.PermissaoClienteEmpresaFunction;
import br.com.linkcom.sined.util.persistence.PermissaoFornecedorEmpresaFunction;
import br.com.linkcom.sined.util.persistence.RetornaArrayDocumentoNegociadoFunction;
import br.com.linkcom.sined.util.persistence.RetornaArrayPessoaFunction;
import br.com.linkcom.sined.util.persistence.SubqueryDocumentoListagemFornecedorCliente;


public class PostgresSqlDialect extends PostgresSqlDialectForFTS{

	public PostgresSqlDialect(){
		super();
		registerFunction("qtdereservada", new StandardSQLFunction("qtdereservada", Hibernate.DOUBLE));
		registerFunction("qtde_unidadeprincipal", new StandardSQLFunction("qtde_unidadeprincipal", Hibernate.DOUBLE));
		registerFunction("ultimocargo_colaborador", new StandardSQLFunction("ultimocargo_colaborador", Hibernate.INTEGER));
		registerFunction("ultima_localizacao", new StandardSQLFunction("ultima_localizacao", Hibernate.STRING));
		registerFunction("subquery_documento_listagem_fornecedor_cliente", new SubqueryDocumentoListagemFornecedorCliente());
		registerFunction("retorna_array_pessoa", new RetornaArrayPessoaFunction());
		registerFunction("retorna_array_documento_negociado", new RetornaArrayDocumentoNegociadoFunction());
		//autocompletes
		registerFunction("autocomplete_cliente_pessoa", new ClientePessoaAutocompleteFunction());
		registerFunction("otimizacao_documento_listagem", new OtimizacaoDocumentoListagemFunction());
		registerFunction("otimizacao_movimentacao_gerada", new OtimizacaoMovimentacaoGeradaFunction());
		registerFunction("otimizacao_venda_projeto", new OtimizacaoVendaProjetoFunction());
		registerFunction("otimizacao_pedidovenda_projeto", new OtimizacaoPedidoVendaProjetoFunction());
		//subquery permissao cliente/empresa
		registerFunction("permissao_clienteempresa", new PermissaoClienteEmpresaFunction());
		//subquery permissao fornecedor/empresa
		registerFunction("permissao_fornecedorempresa", new PermissaoFornecedorEmpresaFunction());
	}

}
