package br.com.linkcom.sined.util;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

import br.com.linkcom.sined.geral.bean.enumeration.Tiposalario;

public class TiposalarioType implements UserType {

	private static final int[] SQL_TYPES = { Types.INTEGER };
	private final HashMap<Integer, Tiposalario> mapConstantes;

	public TiposalarioType(){
		Tiposalario[] enumConstants = Tiposalario.class.getEnumConstants();
		mapConstantes = new HashMap<Integer, Tiposalario>();
		
		for (Tiposalario tp : enumConstants)
			mapConstantes.put(tp.getValue(), tp);
	}
	
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner)
			throws HibernateException, SQLException {

		Integer valor = resultSet.getInt(names[0]);
		Object result = null;
		if (!resultSet.wasNull()) {
			result = mapConstantes.get(valor);
		}
		return result;
	}

	public void nullSafeSet(PreparedStatement preparedStatement, Object value,
			int index) throws HibernateException, SQLException {

		if (null == value) {
			preparedStatement.setNull(index, Types.INTEGER);
		} else {
			preparedStatement.setInt(index, ((Tiposalario) value).getValue());
		}
	}

	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	public boolean isMutable() {
		return false;
	}

	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return cached;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return original;
	}

	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y)
			return true;
		if (null == x || null == y)
			return false;
		return x.equals(y);
	}

	public Class<?> returnedClass() {
		return Tiposalario.class;
	}

}
