package br.com.linkcom.sined.util;

import java.util.HashMap;
import java.util.Map;

import br.com.linkcom.neo.core.web.NeoWeb;

public class ParametroCacheUtil {

	private static Map<String, Map<String, String>> mapCacheParametro = new HashMap<String, Map<String,String>>();
	
	public static void remove(String nome){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return;
			url = SinedUtil.getUrlWithoutContext();
			
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());	  		
			return ;
		}
		remove(nome, url);
	}
	
	private static void remove(String nome, String url){
		if(url == null) return;
		
		synchronized (mapCacheParametro) {
			Map<String, String> map = mapCacheParametro.get(url);
			if(map != null){
				map.remove(nome);
			}
		}
	}
	
	public  static void put(String nome, String valor){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return;
			url = SinedUtil.getUrlWithoutContext();
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());	  		
			return ;
		}
		put(nome, valor, url);
	}
	
	private static void put(String nome, String valor, String url){
		if(url == null) return;
		
		synchronized (mapCacheParametro) {
			Map<String, String> map = mapCacheParametro.get(url);
			if(map == null){
				map = new HashMap<String, String>();
			}
			map.put(nome, valor);
			
			mapCacheParametro.put(url, map);
		}
	}
	
	
	
	public static String get(String nome){
		String url = null;
		try{
			if(!NeoWeb.isInRequestContext()) return null;
			url = SinedUtil.getUrlWithoutContext();
		}catch(Exception e){
//			System.out.println("Erro ParametroCacheUtil: " + e.getMessage());  
			return null;
		}
		return get(nome, url);
	}
	
	private static String get(String nome, String url){
		if(url == null) return null;
		
		synchronized (mapCacheParametro) {
			Map<String, String> map = mapCacheParametro.get(url);
			if(map == null){
				return null;
			}
			return map.get(nome);
		}
	}
	
	public static void limparCache(){
		synchronized (mapCacheParametro) {
			mapCacheParametro = new HashMap<String, Map<String ,String>>();
		}
	}
	
}
