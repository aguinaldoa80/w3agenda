package br.com.linkcom.sined.util.briefcase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
  
public class BriefCase {    
	private static StringBuffer retornodoComando;    
	private static StringBuffer errosdoComando; 
	     
	public static void executar(Integer servico, AcaoBriefCase acao, Integer Codigo) {    
		String Retorno;    
		retornodoComando = new StringBuffer();   
		errosdoComando = new StringBuffer();   

		try {    
			Process processo = Runtime.getRuntime().exec("/usr/local/sbin/BriefCase \""+servico+"\" \""+acao.getCdacao()+"\" \""+Codigo+"\" ");    
//			Process processo = Runtime.getRuntime().exec("C://teste.bat \""+servico+"\" \""+acao.getCdacao()+"\" \""+Codigo+"\" ");    

			InputStream StreamEntrada = processo.getInputStream();    
			InputStream StreamErro = processo.getErrorStream();    
			BufferedReader SaidadoProcesso;    

			SaidadoProcesso = new BufferedReader(new InputStreamReader(StreamEntrada));    
			while ( (Retorno = SaidadoProcesso.readLine()) != null ) {    
				retornodoComando.append(Retorno + "\r\n");   
			}    

			SaidadoProcesso = new BufferedReader(new InputStreamReader(StreamErro));    
			while ( (Retorno = SaidadoProcesso.readLine()) != null ) {    
				errosdoComando.append(Retorno + "\r\n");   
			}   

			processo.waitFor();    

			SaidadoProcesso.close();    
			StreamErro.close();    
			StreamEntrada.close();    
			processo.getOutputStream().close();    

		} catch ( IOException MensagemdeErro ) {    
			errosdoComando.append(MensagemdeErro.toString());   
		} catch ( InterruptedException MensagemdeErro ) {    
			errosdoComando.append(MensagemdeErro.toString());   
		} catch (Exception e) {
			e.printStackTrace();
		}
	}    

	public static String getRetorno() {    
		if ( retornodoComando.length() > 0 ) {   
			return retornodoComando.toString();   
		} else {   
			return null;   
		}   
	}    

	public static String getErro() {    
		if ( errosdoComando.length() > 0 ) {   
			return errosdoComando.toString();   
		} else {   
			return null;   
		}   
	}    
}