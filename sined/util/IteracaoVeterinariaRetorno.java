package br.com.linkcom.sined.util;

import java.sql.Date;
import java.sql.Timestamp;

import br.com.linkcom.neo.types.Hora;

public class IteracaoVeterinariaRetorno {
	
	private Date dtproximaiteracao;
	private Hora hrproximaiteracao;
	private Date dtfimprevisaoiteracao;
	private Hora hrfimprevisaoiteracao;
	private Timestamp dtfim;
	
	public IteracaoVeterinariaRetorno(){
	}
	
	public IteracaoVeterinariaRetorno(Date dtproximaiteracao, Hora hrproximaiteracao, Date dtfimprevisaoiteracao, Hora hrfimprevisaoiteracao, Timestamp dtfim){
		this.dtproximaiteracao = dtproximaiteracao;
		this.hrproximaiteracao = hrproximaiteracao;
		this.dtfimprevisaoiteracao = dtfimprevisaoiteracao;
		this.hrfimprevisaoiteracao = hrfimprevisaoiteracao;
		this.dtfim = dtfim;
	}

	public Date getDtproximaiteracao() {
		return dtproximaiteracao;
	}
	public Hora getHrproximaiteracao() {
		return hrproximaiteracao;
	}
	public Date getDtfimprevisaoiteracao() {
		return dtfimprevisaoiteracao;
	}
	public Hora getHrfimprevisaoiteracao() {
		return hrfimprevisaoiteracao;
	}
	public Timestamp getDtfim() {
		return dtfim;
	}

	public void setDtproximaiteracao(Date dtproximaiteracao) {
		this.dtproximaiteracao = dtproximaiteracao;
	}
	public void setHrproximaiteracao(Hora hrproximaiteracao) {
		this.hrproximaiteracao = hrproximaiteracao;
	}
	public void setDtfimprevisaoiteracao(Date dtfimprevisaoiteracao) {
		this.dtfimprevisaoiteracao = dtfimprevisaoiteracao;
	}
	public void setHrfimprevisaoiteracao(Hora hrfimprevisaoiteracao) {
		this.hrfimprevisaoiteracao = hrfimprevisaoiteracao;
	}
	public void setDtfim(Timestamp dtfim) {
		this.dtfim = dtfim;
	}	
}