package br.com.linkcom.sined.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;

public class OfflineUtil {
	private static final String QUEBRA_LINHA = "\n\r";
	
	/**
	 * Retorna uma String com a lista de URLs dos arquivos offlines separadas por uma quebra de linha
	 * @return
	 */
	public static String getOfflineFilesString(){
		StringBuilder sb = new StringBuilder();
		List<File> sub = getOfflineFiles();  
	    
	    for (File arq : sub) {
    		sb.append("ArquivoOffline?f=").append(arq.getName()).append(QUEBRA_LINHA);
		}
         return sb.toString();  
	}

	/**
	 * Retorna a lista de arquivos offline disponiv�is para um cliente
	 * @throws SinedException 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<File> getOfflineFiles() {
		List<File> files = new ArrayList<File>();
		String realPath = getDirOfflineJSON(SinedUtil.getStringConexaoBanco());
		File[] sub = new File(realPath).listFiles();  
		if(sub != null){
		    for (File fileDir: sub) {  
		    	if(!fileDir.getName().startsWith(".") || !fileDir.getName().contains("Store"))
		    		files.add (fileDir); //   
	        }
		}
		
		Collections.sort(files,  new Comparator() {  
			public int compare(Object o1, Object o2) {  
				File p1 = (File) o1;  
				File p2 = (File) o2;  
				return p1.getName().compareTo(p2.getName());  
			}  
		});
		return files;
	}
	
//	public static String getEntitiesJSONFilesString(ServletContext ctx) {
//	    StringBuilder sb = new StringBuilder();
//	    for (File arq : getEntitiesJSONFilesLista(ctx)) {
//			sb.append("../js/offline/").append(arq.getName()).append(QUEBRA_LINHA);
//		}
//	    return "";
//	}
//
//	public static List<String> getEntitiesJSONFilesLista(ServletContext ctx) {
//		List<File> files = new ArrayList<File>();
//		String realPath = ctx.getRealPath("js/offline/");
//		System.out.println("RealPath: "+realPath);
//        File[] sub = new File(realPath).listFiles();  
//	    for (File fileDir: sub) {  
//	    	files.add (fileDir); //   
//        }
//	    return files;
//	}

	/**
		 * Retorna o caminho do diret�rio onde os arquivos JSON da tela de pedido de venda offline ir�o ficar  
		 * @param nomeCliente Nome do cliente 
		 * @return
		 */
		public static String getDirOfflineJSON(String nomeCliente){
			String SEPARATOR = System.getProperty("file.separator");
			return ArquivoDAO.getInstance().getPathDir() + SEPARATOR + Neo.getApplicationName() + SEPARATOR + "offline" + SEPARATOR + nomeCliente ;
		}

}
