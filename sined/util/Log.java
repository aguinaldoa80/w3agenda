package br.com.linkcom.sined.util;

import java.sql.Timestamp;


public interface Log {

	void setCdusuarioaltera(Integer cdusuarioaltera);
	void setDtaltera(Timestamp dtaltera);
	public Integer getCdusuarioaltera();
	public Timestamp getDtaltera();

}
