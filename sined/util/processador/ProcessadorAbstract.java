package br.com.linkcom.sined.util.processador;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebApplicationContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAssociadaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;
import br.com.linkcom.sined.util.SinedDateUtils;
@SuppressWarnings("unchecked")
public abstract class ProcessadorAbstract<BEAN> {
	
	private static enum type{INICIOFIM, PROCESSAMENTO, GRUPO, CAMPO};
	protected static Map<String, Method> mapaSetter = null;
	protected static Map<String, Method> mapaConversao = null;
	
	private Scanner sc;
	protected String html;
	protected Class<BEAN> beanClass;

	public ProcessadorAbstract() {
		Class[] classes = Util.generics.getGenericTypes2(this.getClass());
		if(classes.length == 1 && !classes[0].equals(Object.class)){
			beanClass = classes[0];
			return;	
		}
		throw new RuntimeException("A classe "+this.getClass().getName()+" deve declarar um BEAN que ser� preenchido ap�s o processamento.");
	}

	protected Map<String, List<String>> getMapaGrupo(Pattern pattern){
		Map<String, List<String>> mapaGrupos = new HashMap<String, List<String>>();
		
		Matcher matcher = pattern.matcher(html);
		while(matcher.find()){
			if(!mapaGrupos.containsKey(matcher.group(1).trim())){
				mapaGrupos.put(matcher.group(1).trim(), new ListSet<String>(String.class));
			}
			mapaGrupos.get(matcher.group(1).trim()).add(matcher.group(2));
		}
		
		return mapaGrupos;
	}
	protected final Pattern getPattern(Pattern pattern, String regex){
		if(pattern == null){			
			pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		}
		return pattern;
	}
	
	protected final Map<String, Method> getMapaMetodos(Map<String, Method> mapa, Class<?> classe, String prefixo){
		if(mapa == null){
			mapa = new HashMap<String, Method>();
			Method[] metodos = classe.getDeclaredMethods();
			
			if(metodos != null && metodos.length > 0){
				for(Method metodo : metodos){
					if(metodo.getName().startsWith(prefixo)){
						mapa.put(metodo.getName().substring(prefixo.length()), metodo);
					}
				}
			}
		}
		return mapa;
	}
	
	protected Map<String, Method> getMapaConversao(){
		return getMapaMetodos(mapaConversao, this.getClass(), "set");
	}
	protected Map<String, Method> getMapaSetter(){
		return getMapaMetodos(mapaSetter, beanClass, "set");
	}
	
	protected Object extrairDados(Method metodoGetPattern, String conteudoBusca, String identificador) throws Exception{
		Matcher matcher = ((Pattern) metodoGetPattern.invoke(this)).matcher(conteudoBusca);
		Object dado = null;
		
		while(matcher.find()){
			if(matcher.group(1).trim().equalsIgnoreCase(identificador)){
				dado = matcher.group(2).trim();
				if(dado == null){
					dado = new String();
				}
			}
		}
		
		return dado;
	}
	
	public final Boolean isLeiauteNovo(String htmlCompleto) throws Exception{
		StringBuilder path = new StringBuilder();
		path.append(((WebApplicationContext)NeoWeb.getApplicationContext()).getServletContext().getRealPath(File.separator))
			.append("WEB-INF")
			.append(File.separator)
			.append("processador")
			.append(File.separator)
			.append(getNomeArquivoRegrasProcessamento());

		File arquivoRegras = new File(path.toString());
		
		if(arquivoRegras != null){
			sc = new Scanner(arquivoRegras);
			
			while(sc.hasNextLine()){
				String linha = sc.nextLine();
				
				//Descarte de linha de coment�rio
				if(linha.startsWith("#")){
					continue;
				}
				
				//Busca os delimitadores definidos no arquivo de regra para limitar o html lido.
				if(linha.startsWith(type.INICIOFIM.toString().concat(":"))){
					String[] iniciofim = linha.substring(type.INICIOFIM.toString().length()+1).split(";");
					if(iniciofim.length == 2){
						Integer inicio = htmlCompleto.indexOf(iniciofim[0]);
						Integer fim = htmlCompleto.indexOf(iniciofim[1]);
						if(inicio >= 0 && fim >= 0){
							html = htmlCompleto.substring(inicio, fim);
							return Boolean.TRUE;	
						} else{
							EntregaService.lancarErroConsultaNotaFiscalEletronica(htmlCompleto);
						}
					} else{
						throw new Exception("Arquivo de regras n�o possui delimitadores de Inicio/Fim");
					}
				//A linha de delimitadores deve ser a primeira (excluindo coment�rios). 
				// Caso encontre alguma linha cujo tipo seja PROCESSAMENTO,GRUPO ou CAMPO, lan�a exce��o.
				}else if(linha.startsWith(type.PROCESSAMENTO.toString().concat(":"))
						|| linha.startsWith(type.GRUPO.toString().concat(":"))
						|| linha.startsWith(type.CAMPO.toString().concat(":"))){
					throw new Exception("Arquivo de regras n�o possui delimitadores de Inicio/Fim");
				}
			}
		} else{
			throw new Exception("Arquivo de regras n�o encontrado.");
		}
		
		return Boolean.FALSE;
	}

	public final BEAN processarHTML() throws Exception{			
		Map<String, List<String>> mapaGrupos = null;
		Map<String, List<Object>> mapaCampos = new HashMap<String, List<Object>>();
		List<String> listaCampos = new ListSet<String>(String.class);


		String grupoAtual = null;
		String linha = "";
		
		while(sc.hasNextLine()){
			linha = sc.nextLine();
			
			//Descarte de linha de coment�rio
			if(linha.startsWith("#")){
				continue;
			}
			
			//Caso a linha corresponda a um processamento, deve-se identificar o processamento e invocar o seu respectivo m�todo.
			//O retorno do m�todo ser� um mapa de 'grupos' de elementos encontrados que correspondem ao tipo de processamento.
			if(linha.startsWith(type.PROCESSAMENTO.toString().concat(":"))){
				//Garante que os dados de grupos s�o reiniciados para novo processamento.
				mapaGrupos = null; grupoAtual = null;
				linha = linha.substring(type.PROCESSAMENTO.toString().length()+1);
				Method getPattern = null;
				String nomeMetodo = "getPattern" + Util.strings.captalize(linha.toLowerCase());
				try{
					getPattern = this.getClass().getDeclaredMethod(nomeMetodo);
				} catch (Exception e) {
					try{							
						getPattern = this.getClass().getSuperclass().getDeclaredMethod(nomeMetodo);
					} catch (Exception ee) {
						throw new Exception("Falha na identifica��o do processamento: " + Util.strings.captalize(linha.toLowerCase()));
					}
				}
				
				if(getPattern != null){
					try{							
						mapaGrupos = getMapaGrupo((Pattern) getPattern.invoke(this));
					} catch (Exception e) {
						throw new Exception("Falha no processamento: " + Util.strings.captalize(linha.toLowerCase()));
					}
				}
				
			//Caso a linha corresponda a um grupo, seta o nome do grupo na vari�vel 'grupoAtual' para posterior consulta de campos.
			} else if(linha.startsWith(type.GRUPO.toString().concat(":"))){
				String grupo = linha.substring(type.GRUPO.toString().length()+1);
				if(mapaGrupos != null && mapaGrupos.containsKey(grupo)){
					grupoAtual = grupo;
				} else{
					grupoAtual = null;//FIXME LAN�AR EXCE��O?
				}
				
			//Caso a linha corresponda a um campo, extrai as informa��es deste (nome_metodo_extracao;dominio;identificador_campo;nome_bean)
			// e adiciona o valor encontrado  ao mapa de campos com chave sendo o nome_bean;
			} else if(linha.startsWith(type.CAMPO.toString().concat(":"))){
				if(grupoAtual != null && !grupoAtual.isEmpty()){
					String[] campo = linha.substring(type.CAMPO.toString().length()+1).split(";");
					List<String> conteudoGrupoAtual = mapaGrupos.get(grupoAtual);
					String nomeMetodoGetPattern = "getPattern"+Util.strings.captalize(campo[0].toLowerCase());
					Method metodoGetPattern = null;
					try{								
						metodoGetPattern = this.getClass().getDeclaredMethod(nomeMetodoGetPattern);
					} catch (Exception e) {
						try{
							metodoGetPattern = this.getClass().getSuperclass().getDeclaredMethod(nomeMetodoGetPattern);
						} catch (Exception ee) {
							throw new Exception("Falha na identifica��o do modelo de extra��o: " + campo[0]);
						}
					}
					
					if(metodoGetPattern != null && conteudoGrupoAtual != null){
						if(!mapaCampos.containsKey(campo[2])){
							listaCampos.add(campo[2]);
							mapaCampos.put(campo[2], new ListSet<Object>(Object.class));
						}
						
						for(String html : conteudoGrupoAtual){							
							try {	
								Object dado = extrairDados(metodoGetPattern, html, campo[1]);
								if(dado != null){
									mapaCampos.get(campo[2]).add(dado);
								}else {
									mapaCampos.get(campo[2]).add(null);
								}
							} catch (Exception e) {
								throw new Exception("Falha na extra��o do campo: " + campo[2]);
							}
						}
					}
				}				
			}
		}
		
		return preencheBean(mapaCampos, listaCampos);
	}
	
	protected final BEAN preencheBean(Map<String, List<Object>> mapaCampos, List<String> listaCampos) throws Exception {
		BEAN bean = beanClass.newInstance();
		Map<String, Method> mapaSetter = getMapaSetter();
		Map<String, Method> mapaConversao = getMapaConversao();

		//Itera pelo mapaCampos para preencher todos os campos extraidos da nfe.
		if(mapaCampos != null && !mapaCampos.isEmpty() && mapaSetter != null && !mapaSetter.isEmpty()){
			for(String chave : listaCampos){
				List<Object> listaDados = mapaCampos.get(chave);
				Method set = mapaSetter.get(Util.strings.captalize(chave));		

				//Caso exista um setter personalizado, este ser� invocado e respons�vel pelo preenchimento do campo no bean.
				//O m�todo setter dever� conter a assinatura: BEAN 'setNomecampo'(BEAN bean, List<Object> campo); 
				if(listaDados != null && !listaDados.isEmpty()){
					if(mapaConversao.containsKey(Util.strings.captalize(chave))){
						Method conversor = mapaConversao.get(Util.strings.captalize(chave));
						if(conversor != null){
							conversor.invoke(this, bean, listaDados);
						} else{
							throw new Exception("N�o foi poss�vel localizar o m�todo de convers�o para o campo " + chave);
						}
					} else if(set != null){					
						preencheCampoPadrao(bean, chave, listaDados.get(0), set);
					} else{
						preencheCampoPersonalizado(bean, chave, listaDados);
					}
				}
			}
		}
		
		return bean;
	}
	
	private Boolean isEmpty(Object obj){
		return (obj == null || obj.toString().trim().equals(""));
	}
	
	protected void preencheCampoPadrao(Object bean, String chave, Object dado, Method set) throws Exception{
		//Verifica se existe valor para o campo assim como se existe um m�todo setter para este.
		if(set != null && dado != null){
			Object arg = null;
			String paramClassName = set.getParameterTypes()[0].getSimpleName();
			
			//Convers�o padr�o de tipos.
			//Deve ser implementado no processador filho um m�todo 'setNomecampo' para cada 'campo' de tipos n�o listados abaixo.
			//Desta forma o m�todo ser� adicionado ao 'mapaConvers�o' e o preenchimento do campo ser� personalizado. 
			try{
				if(paramClassName.equalsIgnoreCase("String")){
					arg = dado.toString().trim();
				} else if(paramClassName.equalsIgnoreCase("Boolean") && !isEmpty(dado)){
					arg = Boolean.parseBoolean(dado.toString().trim());
				} else if(paramClassName.equalsIgnoreCase("Integer") && !isEmpty(dado)){
					arg = Integer.parseInt(dado.toString().trim());
				} else if(paramClassName.equalsIgnoreCase("Double") && !isEmpty(dado)){
					arg = Double.parseDouble(dado.toString().trim().replace(".", "").replace(",", "."));
				}  else if(paramClassName.equalsIgnoreCase("Money") && !isEmpty(dado)){
					arg = new Money((Double.parseDouble(dado.toString().trim().replace(".", "").replace(",", "."))));
				} else if(paramClassName.equalsIgnoreCase("Date") && !isEmpty(dado)){
					arg = SinedDateUtils.stringToDate(dado.toString().trim(), "dd/MM/yyyy");
				} 
			} catch (Exception e) {
				throw new Exception("Falha na convers�o de campos: " + e.getMessage());
			}
			
			//Preenche o bean com a vari�vel 'arg' ('campo'convertido) atrav�s de seu respectivo setter.
			try{	
				if(arg != null){			
					set.invoke(bean, arg);
				}
			} catch (Exception e) {
				throw new Exception("Falha no preenchimento do campo " + chave);
			}
		}
	}
	
	protected void confereImportacaoXmlNfeItemBean(ImportacaoXmlNfeBean bean, Integer size){
		if(bean.getListaItens() == null){
			bean.setListaItens(new ArrayList<ImportacaoXmlNfeItemBean>(size));
			for(int i=0; i<size; i++){
				bean.getListaItens().add(new ImportacaoXmlNfeItemBean());
			}
		}
	}
	
	protected void confereImportacaoXmlNfeAssociadaBean(ImportacaoXmlNfeBean bean, Integer size){
		if(bean.getListaNfeAssociadas() == null){
			bean.setListaNfeAssociadas(new ArrayList<ImportacaoXmlNfeAssociadaBean>(size));
			for(int i=0; i<size; i++){
				bean.getListaNfeAssociadas().add(new ImportacaoXmlNfeAssociadaBean());
			}
		}
	}
	
	protected abstract void preencheCampoPersonalizado(BEAN bean, String chave, List<Object> listaDados) throws Exception;
	protected abstract String getNomeArquivoRegrasProcessamento();	
}
