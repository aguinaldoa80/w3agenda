package br.com.linkcom.sined.util.processador;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAssociadaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;

public class ProcessadorCte extends ProcessadorAbstract<ImportacaoXmlNfeBean> {
	//Exemplo de cte: 3514.0626.3412.2200.0242.5701.0000.1582.2810.0158.2283
	//SERVICES
	private static EmpresaService empresaService;
	private static PessoaService pessoaService;
	private static MaterialService materialService;
	private static CfopService cfopService;
	private static EntradafiscalService entradafiscalService;
	
	public EmpresaService getEmpresaService() {
		if(empresaService == null){
			empresaService = Neo.getObject(EmpresaService.class);
		}
		return empresaService;
	}
	
	public PessoaService getPessoaService() {
		if(pessoaService == null){
			pessoaService = Neo.getObject(PessoaService.class);
		}
		return pessoaService;
	}
	
	public MaterialService getMaterialService() {
		if(materialService == null){
			materialService = Neo.getObject(MaterialService.class);
		}
		return materialService;
	}
	
	public CfopService getCfopService() {
		if(cfopService == null){
			cfopService = Neo.getObject(CfopService.class);
		}
		return cfopService;
	}
	public static EntradafiscalService getEntradafiscalService() {
		if(entradafiscalService == null){
			entradafiscalService = Neo.getObject(EntradafiscalService.class);
		}
		return entradafiscalService;
	}
	
	//MAPAS
	private static Map<String, Method> mapaSetterItem;

	protected Map<String, Method> getMapaSetterItem(){
		return getMapaMetodos(mapaSetterItem, ImportacaoXmlNfeItemBean.class, "set");
	}
	
	
	//PATTERN E REGEX
	private static String regexBlocos = "(?s)<div class=\"header\"(?:\\s)*(?:|style=\"text-align: center;\")(?:\\s)*(?:>|xmlns:sig=\"http://www.w3.org/2000/09/xmldsig#\">)(.+?)(?:</div>)(?:.+?>)(.+?)(?:</table>)";
	private static Pattern patternBlocos;
	private static String regexTh = "<th(?:.*?)>(?!\\s)(.+)</th>";
	private static Pattern patternTh;
	private static String regexTd = "<td(?:.*?)>(?s)(.*?)</td>";
	private static Pattern patternTd;
	private static String regexCampo = "(.+)&\\|\\|\\|&(.+)";
	private static Pattern patternCampo;
	private static String separator = "&|||&";
	
	public Pattern getPatternTh() {
		return getPattern(patternTh, regexTh);
	}
	public Pattern getPatternTd() {
		return getPattern(patternTd, regexTd);
	}
	protected Pattern getPatternBlocos(){
		return getPattern(patternBlocos, regexBlocos);
	}
	protected Pattern getPatternCampo(){
		return getPattern(patternCampo, regexCampo);
	}
	

	//CONSTRUTOR
	public ProcessadorCte() {super();}


	//METODOS SOBRESCRITOS
	@Override
	protected String getNomeArquivoRegrasProcessamento() {
		return "arquivoRegrasCte";
	}
	
	@Override
	protected Map<String, List<String>> getMapaGrupo(Pattern pattern) {
		Map<String, List<String>> mapaGrupos = new HashMap<String, List<String>>();
		
		Matcher matcher = pattern.matcher(html);
		while(matcher.find()){
			if(!mapaGrupos.containsKey(matcher.group(1).trim())){
				mapaGrupos.put(matcher.group(1).trim(), new ListSet<String>(String.class));
			}
			String conteudoHtml = matcher.group(2).trim();
			if(conteudoHtml != null && !conteudoHtml.isEmpty()){
				List<String> labels = new ListSet<String>(String.class);
				List<String> campos = new ListSet<String>(String.class);
				
				Matcher matcherLabel = getPatternTh().matcher(conteudoHtml);
				while(matcherLabel.find()){
					String label = matcherLabel.group(1).trim();
					if(label != null && !label.replaceAll("\u00A0","").trim().isEmpty()){
						labels.add(label);
					}
				}
				
				if(!labels.isEmpty()){
					Integer count = 0;
					Matcher matcherCampos = getPatternTd().matcher(conteudoHtml);
					while(matcherCampos.find()){
						campos.add(labels.get(count++%labels.size())+separator+matcherCampos.group(1).trim());
					}
					mapaGrupos.get(matcher.group(1).trim()).addAll(campos);
				}				
			}
		}
		
		return mapaGrupos;
	}

	@Override
	protected void preencheCampoPersonalizado(ImportacaoXmlNfeBean bean, String chave, List<Object> listaDados) throws Exception {
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			int i=0;
			for(Object campo : listaDados){
				preencheCampoPadrao(bean.getListaItens().get(i), chave, campo, getMapaSetterItem().get(Util.strings.captalize(chave)));
				i++;
			}
		}
	}


	//SETTERS PERSONALIZADOS
	protected void setEmpresa(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			Cnpj cnpj = new Cnpj(listaDados.get(0).toString().trim());
			bean.setEmpresa(getEmpresaService().carregaEmpresaByCnpj(cnpj));
		}
	}
	
	protected void setRazaosocial(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null && (bean.getRazaosocial() == null || bean.getRazaosocial().isEmpty())){
			bean.setRazaosocial(listaDados.get(0).toString().trim());
		}
	}
	
	protected void setCnpj(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			Cnpj cnpj = new Cnpj(listaDados.get(0).toString().trim());
			bean.setCnpj(cnpj);
			if(bean.getRazaosocial() == null){
				bean.setRazaosocial(getPessoaService().buscarPessoa(bean.getCnpj().getValue()));
			}
		}
	}
	
	protected void setCsticms(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			int i=0;
			for(Object dado : listaDados){
				if(dado !=  null){
					String[] string = dado.toString().trim().split("-");
					bean.getListaItens().get(i).setCsticms(Tipocobrancaicms.getTipocobrancaicms(string[0].trim()));
				}
				i++;
			}
		}
	}
	
	protected void setIcms(ImportacaoXmlNfeBean bean, List<Object> listaDados) throws Exception{
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			int i=0;
			for(Object campo : listaDados){
				if(campo != null){
					campo = campo.toString().replaceAll("%", "");
				}
				ImportacaoXmlNfeItemBean item = bean.getListaItens().get(i);
				preencheCampoPadrao(item, "Aliquota ICMS", campo, getMapaSetterItem().get(Util.strings.captalize("icms")));
				i++;
			}
		}
	}
	
	protected void setValorunitario(ImportacaoXmlNfeBean bean, List<Object> listaDados) throws Exception{
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			Material material = getMaterialService().getMaterialForImportacaoCte();
			int i=0;
			for(Object campo : listaDados){
				ImportacaoXmlNfeItemBean item = bean.getListaItens().get(i);
				preencheCampoPadrao(item, "Valor Total Servi�o", campo, getMapaSetterItem().get(Util.strings.captalize("valorunitario")));
				preencheCampoPadrao(item, "Valor Total Servi�o", campo, getMapaSetterItem().get(Util.strings.captalize("valorproduto")));
				if(material != null){
					item.setMaterial(material);
					item.setCprod(material.getCdmaterial() != null ? material.getCdmaterial().toString() : null);
					item.setXprod(material.getNome());
					item.setUnidademedidacomercial(material.getUnidademedida() != null ? material.getUnidademedida().getSimbolo() : null);
					item.setQtde(1.);
					item.setSequencial(i);
				}
				i++;
			}
		}
	}
	
	protected void setCfop(ImportacaoXmlNfeBean bean, List<Object> listaDados) {
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			int i=0;
			for(Object campo : listaDados){
				if(campo != null && !campo.toString().isEmpty()){
					bean.getListaItens().get(i).setCfop(getCfopService().findByCodigo(campo.toString()));
				}
				i++;
			}
		}
	}
	
	protected void setChaveacessoassociada(ImportacaoXmlNfeBean bean, List<Object> listaDados) {
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeAssociadaBean(bean, listaDados.size());
			int i=0;
			for(Object campo : listaDados){
				if(campo != null && !campo.toString().isEmpty()){
					String chaveacesso = campo.toString().trim().replaceAll("\\.", "");
					ImportacaoXmlNfeAssociadaBean nfeAssociadaBean = bean.getListaNfeAssociadas().get(i);
					nfeAssociadaBean.setChaveacesso(chaveacesso);
					nfeAssociadaBean.setListaNfe(getEntradafiscalService().findByChaveacesso(chaveacesso));
				}
				i++;
			}
		}
	}
}
