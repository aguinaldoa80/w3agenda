package br.com.linkcom.sined.util.processador;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;

public class ProcessadorNfe extends ProcessadorAbstract<ImportacaoXmlNfeBean> {
	
	private String regexBlocos = "(?s)<fieldset><legend class=\"(?:titulo-aba|titulo-aba-interna)\">(.+?)(?=</legend>)</legend>(.+?)(?=</fieldset>)</fieldset></div>";
	private Pattern patternBlocos;
//	private String regexDetalheproduto = "(?s)<table class=\"(\\w*)\">(?:<tr>|<tr class=\"highlighted\">)(.*?)(?=<legend class=\"toggle\">PIS</legend>)" ;
//	private String regexDetalheproduto = "(?s)<fieldset><legend class=\"(?:titulo-aba|titulo-aba-interna)\">(.+?)(?=</legend>)</legend>(.+?)</div></div>";
	private String regexDetalheproduto = "(?s)</table><table class=\"(toggle box|toggable box)\"(?:[^>]*)>(.*?)(?=</table>(?:<table|</fieldset>|</div>))";
	private String regexDetalheprodutoIPI = "(?s)<legend>(Imposto Sobre Produtos Industrializados)(.+?)(?=</legend>)</legend>(.+?)";
	private String regexDetalheprodutoPIS = "(?s)<legend class=\"toggle\">(PIS)(?=</legend>)</legend>(.+?)</fieldset>";
	private String regexDetalheprodutoCOFINS = "(?s)<legend class=\"toggle\">(COFINS)(?=</legend>)</legend>(.+?)</fieldset>";
	private Pattern patternDetalheproduto;
	private Pattern patternDetalheprodutoipi;
	private Pattern patternDetalheprodutopis;
	private Pattern patternDetalheprodutocofins;
	private String regexTdclassspan = "(?s)<td class=\"([\\w-]*)\"><span(?:>| class=\"multiline\">)(.*?)</span></td>" ;
	private Pattern patternTdclassspan;
	private String regexLabelspan = "(?s)<label>(.*?)</label><span(?: class=\"(?:linha|multiline)\")?>(.*?)</span>" ;
	private Pattern patternLabelspan;
	private Map<String, Method> mapaSetterItem;
	
	@Override
	protected String getNomeArquivoRegrasProcessamento() {
		return "arquivoRegrasNfe";
	}
	
	@Override
	protected void preencheCampoPersonalizado(ImportacaoXmlNfeBean bean, String chave, List<Object> listaDados) throws Exception {
		if(listaDados != null && !listaDados.isEmpty()){
			confereImportacaoXmlNfeItemBean(bean, listaDados.size());
			int i=0;
			for(Object campo : listaDados){
				preencheCampoPadrao(bean.getListaItens().get(i), chave, campo, getMapaSetterItem().get(Util.strings.captalize(chave)));
				i++;
			}
		}
	}
	
	protected Pattern getPatternTdclassspan(){
		return getPattern(patternTdclassspan, regexTdclassspan);
	}
	
	protected Pattern getPatternLabelspan(){
		return getPattern(patternLabelspan, regexLabelspan);
	}
	
	protected Pattern getPatternBlocos(){
		return getPattern(patternBlocos, regexBlocos);
	}
	
	protected Pattern getPatternDetalheproduto(){
		return getPattern(patternDetalheproduto, regexDetalheproduto);
	}
	
	protected Pattern getPatternDetalheprodutoipi(){
		return getPattern(patternDetalheprodutoipi, regexDetalheprodutoIPI);
	}
	
	protected Pattern getPatternDetalheprodutopis(){
		return getPattern(patternDetalheprodutopis, regexDetalheprodutoPIS);
	}
	
	protected Pattern getPatternDetalheprodutocofins(){
		return getPattern(patternDetalheprodutocofins, regexDetalheprodutoCOFINS);
	}
	
	protected Map<String, Method> getMapaSetterItem(){
		return getMapaMetodos(mapaSetterItem, ImportacaoXmlNfeItemBean.class, "set");
	}
	
	protected void setIndpag(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			String[] string = listaDados.get(0).toString().trim().split(" - ");
			if(!"".equalsIgnoreCase(string[0].trim())){
				bean.setIndpag(Formapagamentonfe.values()[Integer.parseInt(string[0].trim())]);
			}
		}
	}
	
	protected void setEmpresa(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			Cnpj cnpj = new Cnpj(listaDados.get(0).toString().trim());
			bean.setEmpresa(EmpresaService.getInstance().carregaEmpresaByCnpj(cnpj));
		}
	}
	
	protected void setRazaosocial(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null && (bean.getRazaosocial() == null || bean.getRazaosocial().isEmpty())){
			bean.setRazaosocial(listaDados.get(0).toString().trim());
		}
	}
	
	protected void setCnpj(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			Cnpj cnpj = new Cnpj(listaDados.get(0).toString().trim());
			bean.setCnpj(cnpj);
			bean.setRazaosocial(PessoaService.getInstance().buscarPessoa(bean.getCnpj().getValue()));
		}
	}
	
	protected void setResponsavelFrete(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		if(listaDados.get(0) != null){
			String [] string = listaDados.get(0).toString().trim().replaceAll(" ", "").split("-"); 
			if(string.length > 0 && string[0] != null && !"".equals(string[0].trim())){
				ResponsavelFrete responsavelFrete = ResponsavelFrete.getBean(Integer.parseInt(string[0].trim()));
				bean.setResponsavelFrete(responsavelFrete);
			}
		}
	}
	
	protected void setCsticms(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		confereImportacaoXmlNfeItemBean(bean, listaDados.size());
		int i=0;
		for(Object dado : listaDados){
			if(dado !=  null){
				String[] string = dado.toString().trim().split("-");
				bean.getListaItens().get(i).setCsticms(Tipocobrancaicms.getTipocobrancaicms(string[0].trim()));
			}
			i++;
		}	
	}
	
	protected void setCstipi(ImportacaoXmlNfeBean bean, List<Object> listaDados){
		confereImportacaoXmlNfeItemBean(bean, listaDados.size());
		int i=0;
		for(Object dado : listaDados){
			if(dado !=  null){
				String[] string = dado.toString().trim().split("-");
				bean.getListaItens().get(i).setCstipi(Tipocobrancaipi.getTipocobrancaipi(string[0].trim()));
			}
			i++;
		}	
	}
}
