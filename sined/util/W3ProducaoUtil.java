package br.com.linkcom.sined.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;

public class W3ProducaoUtil {
	
	/**
	* M�todo que retorna o caminho do diret�rio onde os arquivos JSON do W3Producao ser�o gerados
	*
	* @param nomeCliente
	* @return
	*/
	public static String getDirW3ProducaoJSON(String nomeCliente){
		String SEPARATOR = System.getProperty("file.separator");
		return ArquivoDAO.getInstance().getPathDir() + SEPARATOR + Neo.getApplicationName() + SEPARATOR + "w3producao" + SEPARATOR + nomeCliente ;
	}
	
	/**
	 * M�todo que retorna a lista de arquivos, para o W3Producao, dispon�veis para um cliente
	 * 
	 * @return
	 */
	public static List<File> getW3ProducaoFiles() {
		List<File> files = new ArrayList<File>();
		String realPath = getDirW3ProducaoJSON(SinedUtil.getStringConexaoBanco());
		File[] sub = new File(realPath).listFiles();  
		if(sub != null){
		    for (File fileDir: sub) {  
		    	if(!fileDir.getName().startsWith(".") || !fileDir.getName().contains("Store"))
		    		files.add (fileDir); //   
	        }
		}
		
		Collections.sort(files,  new Comparator<File>() {  
			public int compare(File o1, File o2) {  
				return o1.getName().compareTo(o2.getName());  
			}  
		});
		return files;
	}

}
