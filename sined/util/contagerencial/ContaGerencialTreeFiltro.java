package br.com.linkcom.sined.util.contagerencial;

import br.com.linkcom.sined.geral.bean.Tipooperacao;

public class ContaGerencialTreeFiltro {

	private String propriedade;
	private String tipooperacao;
	private Tipooperacao tipooperacao_obj;
	private String q;
	private String whereInCdcontagerencial;
	private String whereInNaturezaContagerencial;
	private String whereNotInNaturezaContagerencial;
	
	public String getQ() {
		return q;
	}
	
	public Tipooperacao getTipooperacao_obj() {
		return tipooperacao_obj;
	}
	
	public String getTipooperacao() {
		return tipooperacao;
	}
	
	public String getPropriedade() {
		return propriedade;
	}
	
	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}
	
	public void setTipooperacao(String tipooperacao) {
		this.tipooperacao = tipooperacao;
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("C")) setTipooperacao_obj(Tipooperacao.TIPO_CREDITO);
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("D")) setTipooperacao_obj(Tipooperacao.TIPO_DEBITO);
		if(tipooperacao != null && tipooperacao.trim().toUpperCase().equals("CO")) setTipooperacao_obj(Tipooperacao.TIPO_CONTABIL);
	}
	
	public void setTipooperacao_obj(Tipooperacao tipooperacao_obj) {
		this.tipooperacao_obj = tipooperacao_obj;
	}
	
	public void setQ(String q) {
		this.q = q;
	}

	public String getWhereInCdcontagerencial() {
		return whereInCdcontagerencial;
	}
	
	public void setWhereInCdcontagerencial(String whereInCdcontagerencial) {
		this.whereInCdcontagerencial = whereInCdcontagerencial;
	}
	
	public String getWhereInNaturezaContagerencial() {
		return whereInNaturezaContagerencial;
	}

	public void setWhereInNaturezaContagerencial(
			String whereInNaturezaContagerencial) {
		this.whereInNaturezaContagerencial = whereInNaturezaContagerencial;
	}
	
	public String getWhereNotInNaturezaContagerencial() {
		return whereNotInNaturezaContagerencial;
	}
	
	public void setWhereNotInNaturezaContagerencial(String whereNotInNaturezaContagerencial) {
		this.whereNotInNaturezaContagerencial = whereNotInNaturezaContagerencial;
	}
}