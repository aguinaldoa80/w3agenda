package br.com.linkcom.sined.util.contagerencial;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.service.ContagerencialService;

@Bean
@Controller(path="/financeiro/process/ContaGerencialTreeView")
public class ContaGerencialTreeController extends MultiActionController {
	
	private ContagerencialService contagerencialService;
	
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	@DefaultAction
	public ModelAndView view(WebRequestContext request, ContaGerencialTreeFiltro filtro){
		
		String whereInCdcontagerencial = null;
		String whereInNaturezaContagerencial = null;
		String whereNotInNaturezaContagerencial = null;
		
		if(filtro.getWhereInCdcontagerencial() != null && !"".equals(filtro.getWhereInCdcontagerencial()) && !"undefined".equals(filtro.getWhereInCdcontagerencial())){
			whereInCdcontagerencial = filtro.getWhereInCdcontagerencial();
		}
		
		if(filtro.getWhereInNaturezaContagerencial() != null && !"".equals(filtro.getWhereInNaturezaContagerencial()) && !"undefined".equals(filtro.getWhereInNaturezaContagerencial())){
			whereInNaturezaContagerencial = filtro.getWhereInNaturezaContagerencial();
		}
		
		if(filtro.getWhereNotInNaturezaContagerencial() != null && !"".equals(filtro.getWhereNotInNaturezaContagerencial()) && !"undefined".equals(filtro.getWhereNotInNaturezaContagerencial())){
			whereNotInNaturezaContagerencial = filtro.getWhereNotInNaturezaContagerencial();
		}
		
		List<Contagerencial> listaContagerecial = contagerencialService.findTreeView(filtro.getTipooperacao_obj(), whereInCdcontagerencial, whereInNaturezaContagerencial, whereNotInNaturezaContagerencial);
		
		String codigo_treeview = this.criaEstrutura(listaContagerecial);
		request.setAttribute("codigo_treeview", codigo_treeview);
		request.setAttribute("propriedade", filtro.getPropriedade());
		return new ModelAndView("direct:/process/contaGerencialTreeView");
	}

	private String criaEstrutura(List<Contagerencial> listaContagerecial) {
		if(listaContagerecial != null && listaContagerecial.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (Contagerencial cg : listaContagerecial) {
				if(cg.getFilhos() != null && cg.getFilhos().size() > 0){
					sb.append("<li><span>");
					sb.append(cg.getVcontagerencial().getIdentificador());
					sb.append(" - ");
					sb.append(cg.getNome());
					sb.append("</span><ul>");
					sb.append(this.criaEstrutura(cg.getFilhos()));
					sb.append("</ul></li>");
				} else {
					sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
					sb.append(cg.getCdcontagerencial());
					sb.append(", '");
					sb.append(cg.getVcontagerencial().getIdentificador());
					sb.append(" - ");
					sb.append(cg.getNome() != null ? cg.getNome().replace("'", "\\'") : "");
					sb.append("')\">");
					sb.append(cg.getVcontagerencial().getIdentificador());
					sb.append(" - ");
					sb.append(cg.getNome());
					sb.append("</span></li>");
				}
			}
			return sb.toString();
		} else return "";
	}
	
	
//	UnidadeGerencialService unidadeGerencialService;
//	PlanoGestaoService planoGestaoService;
//	
//	public void setUnidadeGerencialService(UnidadeGerencialService unidadeGerencialService) {
//		this.unidadeGerencialService = unidadeGerencialService;
//	}
//	
//	public void setPlanoGestaoService(PlanoGestaoService planoGestaoService) {
//		this.planoGestaoService = planoGestaoService;
//	}
//	
//	@DefaultAction
//	public ModelAndView view(WebRequestContext request, ContaGerencialTreeFiltro filtro){
//		List<UnidadeGerencial> find = unidadeGerencialService.find(null, 5, filtro.getPlanoGestao());
//		//esse c�digo ser� utilizado se modificar o algoritmo (escolher/limpar) do botao do treeview
////		List<UnidadeGerencial> allParents = unidadeGerencialService.findAllParents(filtro.getUnidadeGerencial());
////		for (UnidadeGerencial unidadeGerencial : allParents) {
////			System.out.println(unidadeGerencial.getNome());
////		}
//		StringBuilder builder = new StringBuilder();
//		
//		UnidadeGerencial pai = null;
//		criarEstrutura(find, builder, pai, true);
//		builder.append("document.propriedade = '"+request.getParameter("propriedade")+"'");
//		request.setAttribute("codigo", builder.toString());
//		request.setAttribute("classUnidadeGerencial", UnidadeGerencial.class.getName());
//		request.setAttribute("descPlanoGestao", planoGestaoService.load(filtro.getPlanoGestao()).getAnoExercicio());
//		return new ModelAndView("unidadeGerencialTreeView");
//	}
//	/**
//	 * Carrega determinados itens via Ajax
//	 * @param request
//	 * @param filtro
//	 * @throws IOException
//	 */
//	public void load(WebRequestContext request, ContaGerencialTreeFiltro filtro) throws IOException{
//		List<UnidadeGerencial> unidadesGerenciais = filtro.getUnidadesGerenciais();
//		StringBuilder builder = new StringBuilder();
//		if (unidadesGerenciais != null) {
//			for (UnidadeGerencial unidadePai : unidadesGerenciais) {
//				List<UnidadeGerencial> find = unidadeGerencialService.find(unidadePai, 5, filtro.getPlanoGestao());
//				criarEstrutura(find, builder, unidadePai, false);
//			}
//		}
//		HttpServletResponse response = request.getServletResponse();
//		response.setCharacterEncoding("UTF-8");
//		PrintWriter out = response.getWriter();
//		out.println(builder.toString());
//	}
//
//	private void criarEstrutura(List<UnidadeGerencial> find, StringBuilder builder, UnidadeGerencial pai, boolean first) {
//		for (UnidadeGerencial unidade : find) {
//			
//			String sigla = SGIUtils.escape( unidade.getSigla() );
//			String nome = SGIUtils.escape( unidade.getNome() );
//			
//			builder.append("var node"+unidade.getId()+" = new Node('"+unidade.getId()+"');");
//			builder.append("node"+unidade.getId()+".info = '"+sigla+" "+nome+"'; ");
//			builder.append("node"+unidade.getId()+".label = '"+sigla+"'; ");
//			builder.append("var column1"+unidade.getId()+" = node"+unidade.getId()+".newColumn();");
//			builder.append("column1"+unidade.getId()+".innerHTML = '<span id =\\'td"+unidade.getId()+"\\' onclick=\\'changeSelectState(findNode("+unidade.getId()+"), event);\\' ondblclick=\\'select(findNode("+unidade.getId()+"), event);selecionar();\\'>"+sigla+" - "+nome+"</span>';");
//			//builder.append("column1"+unidade.getId()+".onselect = function(node){changeSelectState(node);};");
//			if(unidade.getNumeroFilhos() != null && unidade.getNumeroFilhos() > 0){
//				builder.append("node"+unidade.getId()+".hasChild = true;");
//			}
//			if(first){//first indica se t� rolando o jsp se n�o � ajax
//				if(pai == null){
//					builder.append("treeTable.addNode(node"+unidade.getId()+");");
//				} else {
//					builder.append("node"+pai.getId()+".addChild(node"+unidade.getId()+");");
//				}
//			} else {
//				builder.append("findNode("+pai.getId()+").addChild(node"+unidade.getId()+");");
//			}
//
//			if(unidade.getFilhos().size() > 0){
//				criarEstrutura(unidade.getFilhos(), builder, unidade, first);
//			}
//		}
//	}
}
