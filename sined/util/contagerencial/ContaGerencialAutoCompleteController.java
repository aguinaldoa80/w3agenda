package br.com.linkcom.sined.util.contagerencial;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.service.ContagerencialService;

@Bean
@Controller(path="/financeiro/process/ContaGerencialAutoComplete")
public class ContaGerencialAutoCompleteController extends MultiActionController {
	
	private ContagerencialService contagerencialService;
	
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}

	@DefaultAction
	public void view(WebRequestContext request, ContaGerencialTreeFiltro filtro){
		/*List<Contagerencial> find = null;
		if(filtro.getWhereInNaturezaContagerencial() != null && 
				!"".equals(filtro.getWhereInNaturezaContagerencial()) && 
				!"undefined".equals(filtro.getWhereInNaturezaContagerencial()) &&
				!"null".equals(filtro.getWhereInNaturezaContagerencial())){
			find = contagerencialService.findAutocompleteNatureza(filtro.getQ(), filtro.getTipooperacao_obj(), filtro.getWhereInNaturezaContagerencial());
		}else {
			find = contagerencialService.findAutocomplete(filtro.getQ(), filtro.getTipooperacao_obj());
		}*/
		
		String whereInNaturezaContagerencial = Util.strings.emptyIfNull(filtro.getWhereInNaturezaContagerencial());
		whereInNaturezaContagerencial = whereInNaturezaContagerencial.replace("undefined", "");
		whereInNaturezaContagerencial = whereInNaturezaContagerencial.replace("null", "");
		
		String whereInCdcontagerencial = Util.strings.emptyIfNull(filtro.getWhereInCdcontagerencial());
		whereInCdcontagerencial = whereInCdcontagerencial.replace("undefined", "");
		whereInCdcontagerencial = whereInCdcontagerencial.replace("null", "");
		
		List<Contagerencial> find = contagerencialService.findAutocomplete(filtro.getQ(), filtro.getTipooperacao_obj(), whereInNaturezaContagerencial, whereInCdcontagerencial);
		
		request.getServletResponse().setCharacterEncoding("UTF-8");
		View.getCurrent().eval(criarEstrutura(find));
	}

	private String criarEstrutura(List<Contagerencial> find) {
		StringBuilder builder = new StringBuilder();
		if(find != null && find.size() > 0){
			for (Contagerencial cg : find) {
				cg.setAdicionarCodigoalternativo(Boolean.TRUE);
				builder.append(cg.getDescricaoInputPersonalizado() + "|" + cg.getCdcontagerencial() + "\n");
			}
		}
		return builder.toString();
	}

}
