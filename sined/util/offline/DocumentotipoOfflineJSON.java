package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Documentotipo;

public class DocumentotipoOfflineJSON {
	
	private Integer cddocumentotipo;
	private String nome;
	private Boolean boleto;
	
	public DocumentotipoOfflineJSON(Documentotipo dt) {
		setCddocumentotipo(dt.getCddocumentotipo());
		setNome(dt.getNome());
		if(dt.getBoleto() != null){
			setBoleto(dt.getBoleto());
		}else {
			setBoleto(Boolean.FALSE);
		}
	}

	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getBoleto() {
		return boleto;
	}

	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBoleto(Boolean boleto) {
		this.boleto = boleto;
	}
}
