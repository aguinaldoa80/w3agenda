package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;

public class UnidadeMedidaOfflineJSON {
	private Integer cdunidademedida;
	private String nome;
	private boolean ativo;
	private List<UnidadeMedidaConversaoOfflineJSON> listaUnidademedidaconversao;
	
	public UnidadeMedidaOfflineJSON(Unidademedida unidademedida){
		setCdunidademedida(unidademedida.getCdunidademedida());
		setNome(unidademedida.getNome());
		setAtivo(unidademedida.getAtivo() != null ? unidademedida.getAtivo() : false);
		ArrayList<UnidadeMedidaConversaoOfflineJSON> lista = new ArrayList<UnidadeMedidaConversaoOfflineJSON>();
		if(unidademedida.getListaUnidademedidaconversao() != null && !unidademedida.getListaUnidademedidaconversao().isEmpty()){
			for (Unidademedidaconversao unidademedidaconversao : unidademedida.getListaUnidademedidaconversao()) {
				unidademedidaconversao.setUnidademedida(unidademedida);
				lista.add( new UnidadeMedidaConversaoOfflineJSON(unidademedidaconversao) );
			}
		}
		setListaUnidademedidaconversao(lista);
		
	}
	
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome() {
		return nome;
	}
	public List<UnidadeMedidaConversaoOfflineJSON> getListaUnidademedidaconversao() {
		return listaUnidademedidaconversao;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaUnidademedidaconversao(
			List<UnidadeMedidaConversaoOfflineJSON> listaUnidademedidaconversao) {
		this.listaUnidademedidaconversao = listaUnidademedidaconversao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
}
