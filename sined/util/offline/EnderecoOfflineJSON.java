package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Endereco;

public class EnderecoOfflineJSON {

	private Integer cdendereco;
	private String descricao;
	
	private Integer cdenderecoantigo;
	private Integer cdendereconovo;
	
	public EnderecoOfflineJSON(Endereco endereco){
		this.cdendereco = endereco.getCdendereco();
		this.descricao = endereco.getDescricaoCombo();
	}
	
	public EnderecoOfflineJSON(Integer cdenderecoantigo, Integer cdendereconovo){
		this.cdenderecoantigo = cdenderecoantigo;
		this.cdendereconovo = cdendereconovo;
	}
	
	public Integer getCdendereco() {
		return cdendereco;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCdenderecoantigo() {
		return cdenderecoantigo;
	}
	public Integer getCdendereconovo() {
		return cdendereconovo;
	}

	public void setCdenderecoantigo(Integer cdenderecoantigo) {
		this.cdenderecoantigo = cdenderecoantigo;
	}
	public void setCdendereconovo(Integer cdendereconovo) {
		this.cdendereconovo = cdendereconovo;
	}	
}
