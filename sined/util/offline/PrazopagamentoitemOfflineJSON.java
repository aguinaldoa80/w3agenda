package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;

public class PrazopagamentoitemOfflineJSON {
	
	private Integer cdprazopagamentoitem;
	protected Integer parcela;
	protected Integer dias;
	protected Integer meses;
	private Integer cdprazopagamento;
	
	public PrazopagamentoitemOfflineJSON(Prazopagamentoitem ppi) {
		setCdprazopagamento(ppi.getPrazopagamento().getCdprazopagamento());
		setParcela(ppi.getParcela());
		setDias(ppi.getDias());
		setMeses(ppi.getMeses());
		setCdprazopagamentoitem(ppi.getCdprazopagamentoitem());
	}

	public Integer getCdprazopagamentoitem() {
		return cdprazopagamentoitem;
	}

	public Integer getParcela() {
		return parcela;
	}

	public Integer getDias() {
		return dias;
	}

	public Integer getMeses() {
		return meses;
	}

	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}

	public void setCdprazopagamentoitem(Integer cdprazopagamentoitem) {
		this.cdprazopagamentoitem = cdprazopagamentoitem;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}

	
		
}
