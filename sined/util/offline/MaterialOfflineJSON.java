package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialempresa;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;

public class MaterialOfflineJSON {
	private Integer cdmaterial;
	private String identificacao;
	private String referencia;
	private String nome;
	private Integer cdmaterialgrupo;
	private String materialGrupoNome;
	private String classestring;
	private Integer cdunidademedida;
	private Double valorcusto;
	private Double valorvenda;
	private Double valorvendaminimo;
	private Double valorvendamaximo;
	private String codigoalternativo;
	private String descriptionAutocomplete;
	private Double quantidadeDisponivel;
	private boolean vendapromocional;
	private boolean naoexibirminmaxvenda;
	private boolean ativo;
	private List<EmpresaOfflineJSON> listaEmpresa; // listaEmpresamaterial
	private List<LocalarmazenagemOfflineJSON> listaLocalarmazenagem;
	private List<MaterialunidademedidaOfflineJSON> listaMaterialunidademedida;
	private boolean unidadesecundariaTabelapreco; 
	
	public MaterialOfflineJSON(Material m, Double qtdedisponivel) {
		setCdmaterial(m.getCdmaterial());
		setIdentificacao(m.getIdentificacao());
		setReferencia(m.getReferencia());
		setNome(m.getNome());
		setCdmaterialgrupo(m.getMaterialgrupo().getCdmaterialgrupo());
		setMaterialGrupoNome(m.getMaterialgrupo().getNome());
		setClassestring(m.getClassestring());
		setCdunidademedida(m.getUnidademedida().getCdunidademedida());
		setValorcusto(m.getValorcusto());
		setValorvenda(m.getValorvenda());
		setValorvendaminimo(m.getValorvendaminimo());
		setValorvendamaximo(m.getValorvendamaximo());
		setCodigoalternativo(m.getCodigoalternativo());
		setQuantidadeDisponivel(qtdedisponivel);
		setDescriptionAutocomplete(m.getDescriptionAutocompelteVenda());
		setVendapromocional(m.getVendapromocional() != null ? m.getVendapromocional() : false);
		setUnidadesecundariaTabelapreco(false);
		setAtivo(m.getAtivo() != null ? m.getAtivo() : false);
		setNaoexibirminmaxvenda(m.getNaoexibirminmaxvenda() != null ? m.getNaoexibirminmaxvenda() : false);
		
		List<EmpresaOfflineJSON> _listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for(Materialempresa me : m.getListaMaterialempresa()){
			_listaEmpresa.add( new EmpresaOfflineJSON(me.getEmpresa()) );
		}
		setListaEmpresa(_listaEmpresa);
		
		List<MaterialunidademedidaOfflineJSON> _listaMaterialunidademedida = new ArrayList<MaterialunidademedidaOfflineJSON>();
		if(m.getListaMaterialunidademedida() != null && !m.getListaMaterialunidademedida().isEmpty()){
			for(Materialunidademedida mum : m.getListaMaterialunidademedida()){
				_listaMaterialunidademedida.add( new MaterialunidademedidaOfflineJSON(mum) );
			}
		}
		setListaMaterialunidademedida(_listaMaterialunidademedida);
	}
	
	public MaterialOfflineJSON(Material m, List<Localarmazenagem> listaLocalarmazenagem) {
		setCdmaterial(m.getCdmaterial());
		setIdentificacao(m.getIdentificacao());
		setReferencia(m.getReferencia());
		setNome(m.getNome());
		setCdmaterialgrupo(m.getMaterialgrupo().getCdmaterialgrupo());
		setMaterialGrupoNome(m.getMaterialgrupo().getNome());
		setClassestring(m.getClassestring());
		setCdunidademedida(m.getUnidademedida().getCdunidademedida());
		setValorcusto(m.getValorcusto());
		setValorvenda(m.getValorvenda());
		setValorvendaminimo(m.getValorvendaminimo());
		setValorvendamaximo(m.getValorvendamaximo());
		setCodigoalternativo(m.getCodigoalternativo());
//		setQuantidadeDisponivel(qtdedisponivel);
		setDescriptionAutocomplete(m.getDescriptionAutocompelteVenda());
		setVendapromocional(m.getVendapromocional() != null ? m.getVendapromocional() : false);
		setAtivo(m.getAtivo() != null ? m.getAtivo() : false);
		setNaoexibirminmaxvenda(m.getNaoexibirminmaxvenda() != null ? m.getNaoexibirminmaxvenda() : false);
		
		List<EmpresaOfflineJSON> _listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for(Materialempresa me : m.getListaMaterialempresa()){
			_listaEmpresa.add( new EmpresaOfflineJSON(me.getEmpresa()) );
		}
		setListaEmpresa(_listaEmpresa);
		
		List<LocalarmazenagemOfflineJSON> _listaLocalarmazenagem = new ArrayList<LocalarmazenagemOfflineJSON>();
		for(Localarmazenagem la : listaLocalarmazenagem){
			_listaLocalarmazenagem.add(new LocalarmazenagemOfflineJSON(la));
		}
		setListaLocalarmazenagem(_listaLocalarmazenagem);
		
		List<MaterialunidademedidaOfflineJSON> _listaMaterialunidademedida = new ArrayList<MaterialunidademedidaOfflineJSON>();
		if(m.getListaMaterialunidademedida() != null && !m.getListaMaterialunidademedida().isEmpty()){
			for(Materialunidademedida mum : m.getListaMaterialunidademedida()){
				_listaMaterialunidademedida.add( new MaterialunidademedidaOfflineJSON(mum) );
			}
		}
		setListaMaterialunidademedida(_listaMaterialunidademedida);
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getReferencia() {
		return referencia;
	}

	public String getNome() {
		return nome;
	}

	public String getMaterialGrupoNome() {
		return materialGrupoNome;
	}

	public String getClassestring() {
		return classestring;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Double getValorcusto() {
		return valorcusto;
	}

	public Double getValorvenda() {
		return valorvenda;
	}

	public String getCodigoalternativo() {
		return codigoalternativo;
	}

	public String getDescriptionAutocomplete() {
		return descriptionAutocomplete;
	}

	public Double getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}

	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}

	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}

	public List<EmpresaOfflineJSON> getListaEmpresa() {
		return listaEmpresa;
	}
	
	public List<LocalarmazenagemOfflineJSON> getListaLocalarmazenagem() {
		return listaLocalarmazenagem;
	}

	public void setListaEmpresa(List<EmpresaOfflineJSON> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	
	public boolean isVendapromocional() {
		return vendapromocional;
	}

	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}

	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}

	public void setQuantidadeDisponivel(Double quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}

	public void setDescriptionAutocomplete(String descriptionAutocomplete) {
		this.descriptionAutocomplete = descriptionAutocomplete;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setMaterialGrupoNome(String materialGrupoNome) {
		this.materialGrupoNome = materialGrupoNome;
	}

	public void setClassestring(String classestring) {
		this.classestring = classestring;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}

	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}

	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}

	public void setCodigoalternativo(String codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}

	public void setListaLocalarmazenagem(List<LocalarmazenagemOfflineJSON> listaLocalarmazenagem) {
		this.listaLocalarmazenagem = listaLocalarmazenagem;
	}

	public void setVendapromocional(boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}

	public List<MaterialunidademedidaOfflineJSON> getListaMaterialunidademedida() {
		return listaMaterialunidademedida;
	}

	public void setListaMaterialunidademedida(List<MaterialunidademedidaOfflineJSON> listaMaterialunidademedida) {
		this.listaMaterialunidademedida = listaMaterialunidademedida;
	}

	public boolean isUnidadesecundariaTabelapreco() {
		return unidadesecundariaTabelapreco;
	}

	public void setUnidadesecundariaTabelapreco(boolean unidadesecundariaTabelapreco) {
		this.unidadesecundariaTabelapreco = unidadesecundariaTabelapreco;
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}

	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isNaoexibirminmaxvenda() {
		return naoexibirminmaxvenda;
	}

	public void setNaoexibirminmaxvenda(boolean naoexibirminmaxvenda) {
		this.naoexibirminmaxvenda = naoexibirminmaxvenda;
	}
}
