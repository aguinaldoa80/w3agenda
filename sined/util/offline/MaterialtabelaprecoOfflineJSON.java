package br.com.linkcom.sined.util.offline;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;

public class MaterialtabelaprecoOfflineJSON {
	
	private Integer cdmaterialtabelapreco;
	private String nome;
	private Date dtinicio;
	private Date dtfim;
	private Money taxaacrescimo;
	private Double descontopadrao;
	private Double acrescimopadrao;
	private Integer cdprazopagamento;
	private Integer cdpedidovendatipo;
	private Integer cdcategoria;
	private Integer cdmaterialgrupo;
	private List<ClienteOfflineJSON> listaCliente;
	private List<EmpresaOfflineJSON> listaEmpresa;
	private List<MaterialtabelaprecoitemOfflineJSON> listaMaterialtabelaprecoitem;
	protected Money taxapedidovenda;
	
	public MaterialtabelaprecoOfflineJSON(Materialtabelapreco bean) {
		setCdmaterialtabelapreco(bean.getCdmaterialtabelapreco());
		setNome(bean.getNome());
		setDtinicio(bean.getDtinicio());
		setDtfim(bean.getDtfim());
		setTaxaacrescimo(bean.getTaxaacrescimo());
		setDescontopadrao(bean.getDescontopadrao());
		setAcrescimopadrao(bean.getAcrescimopadrao());
		
		if(bean.getPrazopagamento() != null)
			setCdprazopagamento(bean.getPrazopagamento().getCdprazopagamento());
		if(bean.getPedidovendatipo() != null)
			setCdpedidovendatipo(bean.getPedidovendatipo().getCdpedidovendatipo());
		if(bean.getCategoria() != null)
			setCdcategoria(bean.getCategoria().getCdcategoria());
		if(bean.getMaterialgrupo() != null)
			setCdmaterialgrupo(bean.getMaterialgrupo().getCdmaterialgrupo());
		
		List<ClienteOfflineJSON> listaCliente = new ArrayList<ClienteOfflineJSON>();
		for (Materialtabelaprecocliente m : bean.getListaMaterialtabelaprecocliente()) {
			if(m.getCliente() != null)
				listaCliente.add(new ClienteOfflineJSON(m.getCliente()));
		}
		setListaCliente(listaCliente);
		
		List<EmpresaOfflineJSON> listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for (Materialtabelaprecoempresa m: bean.getListaMaterialtabelaprecoempresa()) {
			if(m.getEmpresa() != null)
				listaEmpresa.add(new EmpresaOfflineJSON(m.getEmpresa()));
		}
		setListaEmpresa(listaEmpresa);
		
		List<MaterialtabelaprecoitemOfflineJSON> listaItens = new ArrayList<MaterialtabelaprecoitemOfflineJSON>();
		for (Materialtabelaprecoitem m: bean.getListaMaterialtabelaprecoitem()) {
			if(m.getMaterial() != null)
				listaItens.add(new MaterialtabelaprecoitemOfflineJSON(m, bean));
		}
		setListaMaterialtabelaprecoitem(listaItens);
	}

	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public Date getDtfim() {
		return dtfim;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public Money getTaxaacrescimo() {
		return taxaacrescimo;
	}

	public void setTaxaacrescimo(Money taxaacrescimo) {
		this.taxaacrescimo = taxaacrescimo;
	}

	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}

	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}

	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}

	public List<ClienteOfflineJSON> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<ClienteOfflineJSON> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public List<EmpresaOfflineJSON> getListaEmpresa() {
		return listaEmpresa;
	}

	public void setListaEmpresa(List<EmpresaOfflineJSON> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}

	public List<MaterialtabelaprecoitemOfflineJSON> getListaMaterialtabelaprecoitem() {
		return listaMaterialtabelaprecoitem;
	}

	public void setListaMaterialtabelaprecoitem(
			List<MaterialtabelaprecoitemOfflineJSON> listaMaterialtabelaprecoitem) {
		this.listaMaterialtabelaprecoitem = listaMaterialtabelaprecoitem;
	}

	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}

	public Integer getCdcategoria() {
		return cdcategoria;
	}

	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}

	public Double getDescontopadrao() {
		return descontopadrao;
	}

	public Double getAcrescimopadrao() {
		return acrescimopadrao;
	}

	public void setDescontopadrao(Double descontopadrao) {
		this.descontopadrao = descontopadrao;
	}

	public void setAcrescimopadrao(Double acrescimopadrao) {
		this.acrescimopadrao = acrescimopadrao;
	}

	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}

	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
}
