package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Materialvenda;

public class MaterialvendaOfflineJSON {
	private Integer cdmaterial;
	private Integer cdmaterialmestre;
	
	private String nome_material;
	private String identificacao_material;
	private Double valor;
	private Double valorvendaminimo;
	private Double valorvendamaximo;
	private boolean vendapromocional;
	private Integer cdunidademedida;
	private String nome_unidademedida;
	private Double qtde;
	
	
	public MaterialvendaOfflineJSON(Materialvenda mv) {
		setCdmaterial(mv.getMaterialvendaitem().getCdmaterial());
		setCdmaterialmestre(mv.getMaterial().getCdmaterial());
		setNome_material(mv.getMaterialvendaitem().getNome());
		setQtde(1.0);
		if(mv.getMaterialvendaitem().getValorvenda() != null){
			setValor(mv.getMaterialvendaitem().getValorvenda());
		}
		if(mv.getMaterialvendaitem().getValorvendaminimo() != null){
			setValorvendaminimo(mv.getMaterialvendaitem().getValorvendaminimo());
		}
		if(mv.getMaterialvendaitem().getValorvendamaximo() != null){
			setValorvendamaximo(mv.getMaterialvendaitem().getValorvendamaximo());
		}
		setVendapromocional(mv.getMaterialvendaitem().getVendapromocional() == null ? false : mv.getMaterialvendaitem().getVendapromocional());
		
		if(mv.getMaterialvendaitem().getUnidademedida() != null){
			setCdunidademedida(mv.getMaterialvendaitem().getUnidademedida().getCdunidademedida());
			if(mv.getMaterialvendaitem().getUnidademedida().getNome() != null){
				setNome_unidademedida(mv.getMaterialvendaitem().getUnidademedida().getNome());
			}
		}
	}


	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdmaterialmestre() {
		return cdmaterialmestre;
	}
	public String getNome_material() {
		return nome_material;
	}
	public String getIdentificacao_material() {
		return identificacao_material;
	}
	public Double getValor() {
		return valor;
	}
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}
	public boolean isVendapromocional() {
		return vendapromocional;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome_unidademedida() {
		return nome_unidademedida;
	}
	public Double getQtde() {
		return qtde;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdmaterialmestre(Integer cdmaterialmestre) {
		this.cdmaterialmestre = cdmaterialmestre;
	}
	public void setNome_material(String nome_material) {
		this.nome_material = nome_material;
	}
	public void setIdentificacao_material(String identificacao_material) {
		this.identificacao_material = identificacao_material;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}
	public void setVendapromocional(boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome_unidademedida(String nome_unidademedida) {
		this.nome_unidademedida = nome_unidademedida;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}	
}
