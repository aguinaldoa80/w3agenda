package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuarioprojeto;

public class ProjetoOfflineJSON {
	
	private Integer cdprojeto;
	private String nome;
	private List<UsuarioprojetoOfflineJSON> listaUsuarioprojeto;
	
	public ProjetoOfflineJSON(Projeto p) {
		setCdprojeto(p.getCdprojeto());
		setNome(p.getNome());
		
		List<UsuarioprojetoOfflineJSON> lista = new ArrayList<UsuarioprojetoOfflineJSON>();
		if(p.getListaUsuarioprojeto() != null){
			for (Usuarioprojeto up : p.getListaUsuarioprojeto()) {
				lista.add(new UsuarioprojetoOfflineJSON(up));
			}
		}
		setListaUsuarioprojeto(lista);
	}

	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public String getNome() {
		return nome;
	}
	public List<UsuarioprojetoOfflineJSON> getListaUsuarioprojeto() {
		return listaUsuarioprojeto;
	}

	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaUsuarioprojeto(List<UsuarioprojetoOfflineJSON> listaUsuarioprojeto) {
		this.listaUsuarioprojeto = listaUsuarioprojeto;
	}
}
