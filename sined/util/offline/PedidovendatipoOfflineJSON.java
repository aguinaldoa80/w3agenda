package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Pedidovendatipo;

public class PedidovendatipoOfflineJSON {
	
	private Integer cdpedidovendatipo;
	private String descricao;
	private boolean bonificacao;
	private boolean obrigarPrazoEntrega;
	
	public PedidovendatipoOfflineJSON() {
	}
	
	public PedidovendatipoOfflineJSON(Pedidovendatipo pvt) {
		setCdpedidovendatipo(pvt.getCdpedidovendatipo());
		setDescricao(pvt.getDescricao());
		setBonificacao(pvt.getBonificacao() != null ? pvt.getBonificacao() : false);
		setObrigarPrazoEntrega(pvt.getObrigarPrazoEntrega() != null ? pvt.getObrigarPrazoEntrega() : false); 
	}
	
	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public boolean isBonificacao() {
		return bonificacao;
	}

	public void setBonificacao(boolean bonificacao) {
		this.bonificacao = bonificacao;
	}

	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isObrigarPrazoEntrega() {
		return obrigarPrazoEntrega;
	}

	public void setObrigarPrazoEntrega(boolean obrigarPrazoEntrega) {
		this.obrigarPrazoEntrega = obrigarPrazoEntrega;
	}
}
