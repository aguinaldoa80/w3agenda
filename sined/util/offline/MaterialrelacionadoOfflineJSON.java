package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Materialrelacionado;

public class MaterialrelacionadoOfflineJSON {
	private Integer cdmaterial;
	private Integer cdmaterialmestre;
	private Double valorvenda;
	private Double quantidade;
	
	public MaterialrelacionadoOfflineJSON(Materialrelacionado mr) {
		setCdmaterial(mr.getMaterialpromocao().getCdmaterial());
		setCdmaterialmestre(mr.getMaterial().getCdmaterial());
		if(mr.getValorvenda() != null){
			setValorvenda(mr.getValorvenda().getValue().doubleValue());
		}
		setQuantidade(mr.getQuantidade());
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdmaterialmestre() {
		return cdmaterialmestre;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public Double getQuantidade() {
		return quantidade;
	}


	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdmaterialmestre(Integer cdmaterialmestre) {
		this.cdmaterialmestre = cdmaterialmestre;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
}
