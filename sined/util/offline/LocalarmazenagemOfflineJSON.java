package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;

public class LocalarmazenagemOfflineJSON {
	
	private Integer cdlocalarmazenagem;
	private String nome;
	private Double qtde;
	
	private List<EmpresaOfflineJSON> listaEmpresa;
	
	public LocalarmazenagemOfflineJSON(Localarmazenagem la) {
		setCdlocalarmazenagem(la.getCdlocalarmazenagem());
		setNome(la.getNome());
		setQtde(la.getQtde() != null ? la.getQtde() : 0.0);
		
		List<EmpresaOfflineJSON> _listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for(Localarmazenagemempresa lae : la.getListaempresa()){
			_listaEmpresa.add( new EmpresaOfflineJSON(lae.getEmpresa()));
		}
		setListaEmpresa(_listaEmpresa);
	}

	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public String getNome() {
		return nome;
	}
	public List<EmpresaOfflineJSON> getListaEmpresa() {
		return listaEmpresa;
	}
	public Double getQtde() {
		return qtde;
	}
	
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaEmpresa(List<EmpresaOfflineJSON> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
