package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Usuarioprojeto;

public class UsuarioprojetoOfflineJSON {
	
	private Integer cdusuarioprojeto;
	private Integer cdusuario;
	
	public UsuarioprojetoOfflineJSON(Usuarioprojeto up) {
		setCdusuarioprojeto(up.getCdusuarioprojeto());
		setCdusuario(up.getUsuario().getCdpessoa());
	}

	public Integer getCdusuarioprojeto() {
		return cdusuarioprojeto;
	}
	public Integer getCdusuario() {
		return cdusuario;
	}

	public void setCdusuarioprojeto(Integer cdusuarioprojeto) {
		this.cdusuarioprojeto = cdusuarioprojeto;
	}
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
}
