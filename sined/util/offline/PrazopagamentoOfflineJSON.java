package br.com.linkcom.sined.util.offline;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Prazopagamento;

public class PrazopagamentoOfflineJSON {
	
	private Integer cdprazopagamento;
	private String nome;
	private Boolean ativo;
	private Double juros;
	private Boolean parcelasiguaisjuros;
	private Boolean prazomedio;
	protected Money valorminimo;
	protected Integer prazomediodias;
	
	
	public PrazopagamentoOfflineJSON(Prazopagamento pp) {
		setCdprazopagamento(pp.getCdprazopagamento());
		setNome(pp.getNome());
		setAtivo(pp.getAtivo());
		setJuros(pp.getJuros());
		setParcelasiguaisjuros(pp.getParcelasiguaisjuros());
		setPrazomedio(pp.getPrazomedio());
		setValorminimo(pp.getValorminimo());
		setPrazomediodias(pp.getPrazomediodias());
	}


	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}


	public String getNome() {
		return nome;
	}


	public Boolean getAtivo() {
		return ativo;
	}


	public Double getJuros() {
		return juros;
	}


	public Boolean getParcelasiguaisjuros() {
		return parcelasiguaisjuros;
	}


	public Boolean getPrazomedio() {
		return prazomedio;
	}


	public Money getValorminimo() {
		return valorminimo;
	}


	public Integer getPrazomediodias() {
		return prazomediodias;
	}


	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	public void setJuros(Double juros) {
		this.juros = juros;
	}


	public void setParcelasiguaisjuros(Boolean parcelasiguaisjuros) {
		this.parcelasiguaisjuros = parcelasiguaisjuros;
	}


	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}


	public void setValorminimo(Money valorminimo) {
		this.valorminimo = valorminimo;
	}


	public void setPrazomediodias(Integer prazomediodias) {
		this.prazomediodias = prazomediodias;
	}

	
	
}
