package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Indicecorrecao;

public class IndicecorrecaoOfflineJSON {
	
	private Integer cdindicecorrecao;
	private String sigla;
	
	public IndicecorrecaoOfflineJSON(Indicecorrecao ic) {
		setCdindicecorrecao(ic.getCdindicecorrecao());
		setSigla(ic.getSigla());
	}

	public Integer getCdindicecorrecao() {
		return cdindicecorrecao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setCdindicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	
}
