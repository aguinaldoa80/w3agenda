package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Empresa;

public class EmpresaOfflineJSON {
	
	private Integer cdpessoa;
	private String nome;
	
	public EmpresaOfflineJSON(Empresa e) {
		setCdpessoa(e.getCdpessoa());
		setNome(e.getNome());
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

			

}
