package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Colaborador;

public class ColaboradorOfflineJSON {
	
	private Integer cdpessoa;
	private String nome;
	
	public ColaboradorOfflineJSON(Colaborador c) {
		setCdpessoa(c.getCdpessoa());
		setNome(c.getNome());
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

			

}
