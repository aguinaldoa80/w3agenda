package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Uf;

public class UfOfflineJSON {
	
	protected Integer cduf;
	protected String nome;
	protected String sigla;
	
	public UfOfflineJSON(Uf uf) {
		setCduf(uf.getCduf());
		setNome(uf.getNome());
		setSigla(uf.getSigla());
	}

	public Integer getCduf() {
		return cduf;
	}

	public String getNome() {
		return nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	

}
