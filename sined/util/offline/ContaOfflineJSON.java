package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contaempresa;

public class ContaOfflineJSON {
	
	private Integer cdconta;
	private String nome;
	private List<EmpresaOfflineJSON> listaEmpresa;
	
	
	public ContaOfflineJSON(Conta c) {
		setCdconta(c.getCdconta());
		setNome(c.getNome());
		
		List<EmpresaOfflineJSON> _listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for(Contaempresa ce : c.getListaContaempresa()){
			_listaEmpresa.add( new EmpresaOfflineJSON(ce.getEmpresa()) );
		}
		setListaEmpresa(_listaEmpresa);
	}

	public Integer getCdconta() {
		return cdconta;
	}
	public String getNome() {
		return nome;
	}
	public List<EmpresaOfflineJSON> getListaEmpresa() {
		return listaEmpresa;
	}


	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaEmpresa(List<EmpresaOfflineJSON> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
}
