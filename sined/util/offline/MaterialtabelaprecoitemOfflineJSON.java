package br.com.linkcom.sined.util.offline;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;

public class MaterialtabelaprecoitemOfflineJSON {
	private Integer cdmaterialtabelaprecoitem;
	private Integer cdmaterial;
	private Integer cdunidademedida;
	private Double valor;
	private Double valorvendaminimo;
	private Double valorvendamaximo;
	
	private Integer cdmaterialtabelapreco;
	private Date dtinicio;
	private Date dtfim;
	protected Money taxaacrescimo;
	private Integer cdprazopagamento;
	private Integer cdpedidovendatipo;
	private List<ClienteOfflineJSON> listaCliente;
	private List<EmpresaOfflineJSON> listaEmpresa;
	
	public MaterialtabelaprecoitemOfflineJSON(Materialtabelaprecoitem bean, Materialtabelapreco materialtabelapreco) {
		setCdmaterialtabelaprecoitem(bean.getCdmaterialtabelaprecoitem());
		setCdmaterialtabelapreco(cdmaterialtabelapreco);
		if(bean.getMaterial() != null)
			setCdmaterial(bean.getMaterial().getCdmaterial());
		if(bean.getUnidademedida() != null)
			setCdunidademedida(bean.getUnidademedida().getCdunidademedida());
		
		setValor(bean.getValor());
		setValorvendaminimo(bean.getValorvendaminimo());
		setValorvendamaximo(bean.getValorvendamaximo());
		
		setCdmaterialtabelapreco(materialtabelapreco.getCdmaterialtabelapreco());
		setDtinicio(materialtabelapreco.getDtinicio());
		setDtfim(materialtabelapreco.getDtfim());
		setTaxaacrescimo(materialtabelapreco.getTaxaacrescimo());
		
		if(materialtabelapreco.getPrazopagamento() != null)
			setCdprazopagamento(materialtabelapreco.getPrazopagamento().getCdprazopagamento());
		if(materialtabelapreco.getPedidovendatipo() != null)
			setCdpedidovendatipo(materialtabelapreco.getPedidovendatipo().getCdpedidovendatipo());
		
		List<ClienteOfflineJSON> listaCliente = new ArrayList<ClienteOfflineJSON>();
		for (Materialtabelaprecocliente m : materialtabelapreco.getListaMaterialtabelaprecocliente()) {
			if(m.getCliente() != null)
				listaCliente.add(new ClienteOfflineJSON(m.getCliente()));
		}
		setListaCliente(listaCliente);
		
		List<EmpresaOfflineJSON> listaEmpresa = new ArrayList<EmpresaOfflineJSON>();
		for (Materialtabelaprecoempresa m: materialtabelapreco.getListaMaterialtabelaprecoempresa()) {
			if(m.getEmpresa() != null)
				listaEmpresa.add(new EmpresaOfflineJSON(m.getEmpresa()));
		}
		setListaEmpresa(listaEmpresa);
	}
	
	
	public Integer getCdmaterialtabelaprecoitem() {
		return cdmaterialtabelaprecoitem;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public Double getValor() {
		return valor;
	}
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}
	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Money getTaxaacrescimo() {
		return taxaacrescimo;
	}
	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}
	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	public List<ClienteOfflineJSON> getListaCliente() {
		return listaCliente;
	}
	public List<EmpresaOfflineJSON> getListaEmpresa() {
		return listaEmpresa;
	}

	
	public void setCdmaterialtabelaprecoitem(Integer cdmaterialtabelaprecoitem) {
		this.cdmaterialtabelaprecoitem = cdmaterialtabelaprecoitem;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}
	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setTaxaacrescimo(Money taxaacrescimo) {
		this.taxaacrescimo = taxaacrescimo;
	}
	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
	public void setListaCliente(List<ClienteOfflineJSON> listaCliente) {
		this.listaCliente = listaCliente;
	}
	public void setListaEmpresa(List<EmpresaOfflineJSON> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
}