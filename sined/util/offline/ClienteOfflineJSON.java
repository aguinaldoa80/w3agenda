package br.com.linkcom.sined.util.offline;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;

public class ClienteOfflineJSON {
	
	private Integer cdpessoa;
	private String nome;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String razaosocial;
	private List<EnderecoOfflineJSON> listaEndereco;
	private List<ColaboradorOfflineJSON> listaVendedor;//listaClienteVendedor
	private List<PessoacategoriaOfflineJSON> listaPessoacategoria;
	protected Money taxapedidovenda;
	
	private Integer cdpessoaantigo;
	private Integer cdpessoanovo;
	
	public ClienteOfflineJSON(Cliente c) {
		setCdpessoa(c.getCdpessoa());
		setNome(c.getNome());
		setCpf(c.getCpf());
		setCnpj(c.getCnpj());
		setRazaosocial(c.getRazaosocial());
		setTaxapedidovenda(c.getTaxapedidovenda());
		
		List<EnderecoOfflineJSON> lista = new ArrayList<EnderecoOfflineJSON>();
		if(c.getListaEndereco() != null && !c.getListaEndereco().isEmpty()){
			for (Endereco e : c.getListaEndereco()) {
				lista.add(new EnderecoOfflineJSON(e));
			}
		}
		setListaEndereco(lista);
		
		List<ColaboradorOfflineJSON> listaVendedor = new ArrayList<ColaboradorOfflineJSON>();
		if(c.getListaClientevendedor() != null && !c.getListaClientevendedor().isEmpty()){
			for (Clientevendedor cv: c.getListaClientevendedor()) {
				if(cv.getColaborador() != null)
					listaVendedor.add(new ColaboradorOfflineJSON(cv.getColaborador()));
			}
		}
		setListaVendedor(listaVendedor);
		
		List<PessoacategoriaOfflineJSON> listaPessoacategoria = new ArrayList<PessoacategoriaOfflineJSON>();
		if(c.getListaPessoacategoria() != null && !c.getListaPessoacategoria().isEmpty()){
			for (Pessoacategoria pc: c.getListaPessoacategoria()) {
				listaPessoacategoria.add(new PessoacategoriaOfflineJSON(pc));
			}
		}
		setListaPessoacategoria(listaPessoacategoria);
	}
	
	public ClienteOfflineJSON(Integer cdpessoanovo, Integer cdpessoaantigo){
		this.cdpessoanovo = cdpessoanovo;
		this.cdpessoaantigo = cdpessoaantigo;
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}
	
	public List<ColaboradorOfflineJSON> getListaVendedor() {
		return listaVendedor;
	}

	public Cpf getCpf() {
		return cpf;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}
	
	public String getRazaosocial() {
		return razaosocial;
	}
	
		public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setListaVendedor(List<ColaboradorOfflineJSON> listaVendedor) {
		this.listaVendedor = listaVendedor;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<EnderecoOfflineJSON> getListaEndereco() {
		return listaEndereco;
	}

	public void setListaEndereco(List<EnderecoOfflineJSON> listaEndereco) {
		this.listaEndereco = listaEndereco;
	}

	public List<PessoacategoriaOfflineJSON> getListaPessoacategoria() {
		return listaPessoacategoria;
	}

	public void setListaPessoacategoria(
			List<PessoacategoriaOfflineJSON> listaPessoacategoria) {
		this.listaPessoacategoria = listaPessoacategoria;
	}
	
	public Integer getCdpessoaantigo() {
		return cdpessoaantigo;
	}

	public Integer getCdpessoanovo() {
		return cdpessoanovo;
	}

	public void setCdpessoaantigo(Integer cdpessoaantigo) {
		this.cdpessoaantigo = cdpessoaantigo;
	}

	public void setCdpessoanovo(Integer cdpessoanovo) {
		this.cdpessoanovo = cdpessoanovo;
	}
}
