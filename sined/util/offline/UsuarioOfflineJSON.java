package br.com.linkcom.sined.util.offline;

import org.apache.commons.lang.StringUtils;

public class UsuarioOfflineJSON {
	private String login;
	private String hash;
	private String codigo;
	private Integer cdpessoa;
	private String nome;
	private boolean restricaoclientevendedor;
	private boolean todosprojetos;
	private boolean semprelogaroffline;
	
	public UsuarioOfflineJSON(){
		
	}
	
	public String getLogin() {
		return login;
	}
	public String getHash() {
		return hash;
	}
	
	public String getCodigo() {
		return StringUtils.leftPad(codigo, 6, "0");
	}
	
	public Integer getCdpessoa() {
		return cdpessoa;
	}

	public String getNome() {
		return nome;
	}

	public boolean isRestricaoclientevendedor() {
		return restricaoclientevendedor;
	}

	public void setRestricaoclientevendedor(Boolean restricaoclientevendedor) {
		this.restricaoclientevendedor = restricaoclientevendedor!=null ? restricaoclientevendedor : false;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}

	public boolean isTodosprojetos() {
		return todosprojetos;
	}

	public void setTodosprojetos(Boolean todosprojetos) {
		this.todosprojetos = todosprojetos != null ? todosprojetos : false;;
	}
	
	public boolean isSemprelogaroffline() {
		return semprelogaroffline;
	}

	public void setSemprelogaroffline(boolean semprelogaroffline) {
		this.semprelogaroffline = semprelogaroffline;
	}	
}
