package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;

public class UnidadeMedidaConversaoOfflineJSON {
	protected Integer cdunidademedidaconversao;
	protected Integer cdunidademedida;
	protected Integer cdunidademedidarelacionada;
	protected Double fracao;
	protected Double qtdereferencia;
	
	public UnidadeMedidaConversaoOfflineJSON(Unidademedidaconversao unidademedidaconversao){
		setCdunidademedidaconversao(unidademedidaconversao.getCdunidademedidaconversao());
		if(unidademedidaconversao.getUnidademedida() != null)
			setCdunidademedida(unidademedidaconversao.getUnidademedida().getCdunidademedida());
		setCdunidademedidarelacionada( unidademedidaconversao.getUnidademedidarelacionada().getCdunidademedida() );
		setFracao(unidademedidaconversao.getFracao());
		setQtdereferencia(unidademedidaconversao.getQtdereferencia());
	}
	
	public Integer getCdunidademedidaconversao() {
		return cdunidademedidaconversao;
	}
	
	public Integer getCdunidademedidarelacionada() {
		return cdunidademedidarelacionada;
	}
	public Double getFracao() {
		return fracao;
	}
	public void setCdunidademedidaconversao(Integer cdunidademedidaconversao) {
		this.cdunidademedidaconversao = cdunidademedidaconversao;
	}
	

	public void setCdunidademedidarelacionada(Integer cdunidademedidarelacionada) {
		this.cdunidademedidarelacionada = cdunidademedidarelacionada;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
}
