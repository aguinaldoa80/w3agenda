package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Pessoacategoria;

public class PessoacategoriaOfflineJSON {
	
	private Integer cdpessoa;
	private Integer cdcategoria;
	
	public PessoacategoriaOfflineJSON(Pessoacategoria pessoacategoria) {
		if(pessoacategoria.getCategoria() != null){
			setCdcategoria(pessoacategoria.getCategoria().getCdcategoria());
		}
		if(pessoacategoria.getPessoa() != null){
			setCdpessoa(pessoacategoria.getPessoa().getCdpessoa());
		}
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public Integer getCdcategoria() {
		return cdcategoria;
	}

	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}
}
