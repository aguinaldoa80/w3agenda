package br.com.linkcom.sined.util.offline;

import br.com.linkcom.sined.geral.bean.Materialunidademedida;

public class MaterialunidademedidaOfflineJSON {
	
	private Integer cdmaterialunidademedida;
	private Integer cdunidademedida;
	private Double fracao;
	private Double valorunitario;
	protected Boolean mostrarconversao;
	protected Boolean prioridadevenda;
	protected Double qtdereferencia;
	
	
	public MaterialunidademedidaOfflineJSON(Materialunidademedida mum) {
		setCdmaterialunidademedida(mum.getCdmaterialunidademedida());
		if(mum.getUnidademedida() != null && mum.getUnidademedida().getCdunidademedida() != null)
			setCdunidademedida(mum.getUnidademedida().getCdunidademedida());
		setFracao(mum.getFracao() != null ? mum.getFracao() : 1d);
		setValorunitario(mum.getValorunitario());
		setMostrarconversao(mum.getMostrarconversao() != null ? mum.getMostrarconversao() : false);
		setPrioridadevenda(mum.getPrioridadevenda() != null ? mum.getPrioridadevenda() : false);
		setQtdereferencia(mum.getQtdereferencia() != null ? mum.getQtdereferencia() : 1d);
	}


	public Integer getCdmaterialunidademedida() {
		return cdmaterialunidademedida;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public Double getFracao() {
		return fracao;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public Boolean getMostrarconversao() {
		return mostrarconversao;
	}
	public Boolean getPrioridadevenda() {
		return prioridadevenda;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setCdmaterialunidademedida(Integer cdmaterialunidademedida) {
		this.cdmaterialunidademedida = cdmaterialunidademedida;
	}


	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setMostrarconversao(Boolean mostrarconversao) {
		this.mostrarconversao = mostrarconversao;
	}
	public void setPrioridadevenda(Boolean prioridadevenda) {
		this.prioridadevenda = prioridadevenda;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
}
