package br.com.linkcom.sined.util;

import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.ByteArrayDataSource;

/**
 * Class to send email with html body and attachs<br>
 * <br>
 * how to use:<br>
 *      String fileAttachment = "path/of/file";<br>
 *		String text = "<simg src='cid:00001'> example to show an image in email body";<br>
 *		<br>
 *		Email email = new Email("from@from.com","to@to.com","subject");<br>
 *		email.addHtmlText(text)<br>
 *			 .attachFile(fileAttachment, "name/of/file", "00001")<br>
 *			 .sendMessage();<br>
 *  <br>
 *	@author Pedro Gon�alves
 */
public class EmailManager {

	protected String from;
	protected String to;
	protected String bcc;
	protected String cc;
	protected String subject;
	protected Multipart multipart;
	protected MimeMessage message;
	protected List<String> listTo;
	protected List<String> listBCC;
	protected List<String> listCC;
	protected Session session;
	private String cliente;
	private Integer emailId;
	private Integer emailItemId;
	private String jndiName;
	private boolean charset_ISO_8859_1 = false;	
	
	public EmailManager setCliente(String cliente) {
		this.cliente = cliente;
		return this;
	}
	
	public String getCliente() {
		return cliente;
	}
	public Integer getEmailId() {
		return emailId;
	}

	public EmailManager setEmailId(Integer emailId) {
		this.emailId = emailId;
		return this;
	}
	
	public Integer getEmailItemId() {
		return emailItemId;
	}
	
	public EmailManager setEmailItemId(Integer emailItemId) {
		this.emailItemId = emailItemId;
		return this;
	}

	public EmailManager setFrom(String from) {
		this.from = from;
		return this;
	}

	public EmailManager setTo(String to) {
		this.to = to;
		return this;
	}
	
	public EmailManager setCc(String cc) {
		this.cc = cc;
		return this;
	}
	
	public EmailManager setBcc(String bcc) {
		this.bcc = bcc;
		return this;
	}
	
	public EmailManager setListBCC(List<String> listBCC) {
		this.listBCC = listBCC;
		return this;
	}
	
	public EmailManager setListCC(List<String> listCC) {
		this.listCC = listCC;
		return this;
	}
	
	public EmailManager setSubject(String subject) {
		this.subject = subject;
		return this;
	}
	
	public String getFrom() {
		return from;
	}

	public MimeMessage getMessage() {
		return message;
	}

	public Multipart getMultipart() {
		return multipart;
	}

	public String getSubject() {
		return subject;
	}

	public String getTo() {
		return to;
	}
	
	public String getBcc() {
		return bcc;
	}

	public String getCc() {
		return cc;
	}

	public List<String> getListBCC() {
		return listBCC;
	}

	public List<String> getListCC() {
		return listCC;
	}

	public EmailManager setMessage(MimeMessage message) {
		this.message = message;
		return this;
	}
	
	public List<String> getListTo() {
		return listTo;
	}
	
	public EmailManager setListTo(List<String> listTo) {
		this.listTo = listTo;
		return this;
	}

	public EmailManager setMultipart(Multipart multipart) {
		this.multipart = multipart;
		return this;
	}
	
	public String getJndiName() {
		return jndiName;
	}
	
	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}
	
	public boolean isCharset_ISO_8859_1() {
		return charset_ISO_8859_1;
	}
	
	public void setCharset_ISO_8859_1(boolean charsetISO_8859_1) {
		charset_ISO_8859_1 = charsetISO_8859_1;
	}
	
	public EmailManager(String jndiName) {
		this.setJndiName(jndiName);
	}
	
	private void verify() {
		if (from == null) {throw new NullPointerException("from can�t be null");}
		else if (to == null && listTo == null) {throw new NullPointerException("to can�t be null");}
		else if (subject == null) {throw new NullPointerException("subject can�t be null");}
	}

	private EmailManager configure() throws Exception {
		verify();
		
		configureSession();
		message.setFrom(new InternetAddress(from));
		
		try {
			if(StringUtils.isBlank(cliente)){
				cliente = SinedUtil.getSubdominioCliente();
			}
		} catch(Exception e) {
			try {
				if(StringUtils.isBlank(cliente)){
					cliente = SinedUtil.getSubdominioClienteByThread();
				}
			} catch (Exception e2) {
				cliente = "Indefinido";
			}
		}
		if(emailId!=null || emailItemId!=null){
			message.setHeader("X-SMTPAPI", "{\"category\": \""+cliente+"\", \"unique_args\": {\"emailId\": \""+ emailId +"\",\"emailItemId\": \""+ emailItemId +"\"}}");
			message.setHeader("X-Mailin-custom", "emailId=" + emailId + "&emailItemId=" + emailItemId);
		}else{
			message.setHeader("X-SMTPAPI", "{\"category\": \""+cliente+"\"}");
		}
		message.setHeader("X-Mailin-Tag", cliente);
		
		if (listTo != null) {
			for (String element : listTo) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(element));
			}
		} else {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		}
		
		if (listBCC != null) {
			for (String element : listBCC) {
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(element));
			}
		} else if(bcc != null){
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
		}
		
		if (listCC != null) {
			for (String element : listCC) {
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(element));
			}
		} else if(cc != null){
			message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
		}
		
		message.setSubject(subject);
		return this;
	}

	public EmailManager attachFile(String file,String filename,String contentType, String id) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setHeader("Content-Type", contentType);
		messageBodyPart.setFileName(filename);
		messageBodyPart.setContentID("<"+id+">");
		addPart(messageBodyPart);
		return this;
	}
	
	public EmailManager attachFileUsingByteArray(byte[] file,String filename,String contentType, String id) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		DataSource source = new ByteArrayDataSource(file, contentType);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setHeader("Content-Type", contentType);
		messageBodyPart.setFileName(filename);
		messageBodyPart.setContentID("<"+id+">");
		addPart(messageBodyPart);
		return this;
	}
	
	public void addPart(MimeBodyPart part) throws Exception {
		if(multipart == null) multipart = new MimeMultipart();
		multipart.addBodyPart(part);
	}
	
	public EmailManager addHtmlText(String text) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text);
		messageBodyPart.setHeader("Content-Type", "text/html; charset=\"iso-8859-1\"");
		addPart(messageBodyPart);
		return this;
	}
	
	public void sendMessage() throws Exception{
		configure();
		
		message.setContent(multipart);
		Transport.send(message);
		this.setFrom(null);
		this.setTo(null);
		this.setListTo(null);
		this.setSubject(null);
	}

	private void configureSession() throws NamingException {
		session = (Session) InitialContext.doLookup(jndiName);
		message = new MimeMessage(session);
	}

}
