package br.com.linkcom.sined.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Hibernate;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.neo.authorization.AuthorizationManager;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.bean.PropertyDescriptor;
import br.com.linkcom.neo.controller.ExtendedBeanWrapper;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.standard.RequestContext;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.exception.NotInNeoContextException;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoImageResolver;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Acaopapel;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Campolistagem;
import br.com.linkcom.sined.geral.bean.Campolistagemusuario;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.LogoImagem;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCriacaoPedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoarredondamentoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocoluna;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.AcaoService;
import br.com.linkcom.sined.geral.service.AcaopapelService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CampolistagemService;
import br.com.linkcom.sined.geral.service.CampolistagemusuarioService;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MenuService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PermissaoService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.sistema.controller.process.AlteraEmpresaSelecionadaProcess;
import br.com.linkcom.sined.util.briefcase.BriefCase;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.DownloadPublicoServlet;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

public class SinedUtil {
	
	public static final String SINED_CACHE_MODULES = "sined_cache_menu"; 
	public static final String SINED_CACHE_ACAOPERMISSION = "sined_cache_acaopermissions"; 
	private static DecimalFormat format = new DecimalFormat("#,##0.00;(#,##0.00)");
	private static DecimalFormat formatVazio = new DecimalFormat("");
	public static final String REGEX_APENAS_LETRAS_SEFIP = "[^A-Za-z\\ ]";
	public static final String REGEX_APENAS_NUMEROS_SEFIP = "[^0-9\\ ]";
	public static final String REGEX_APENAS_LETRAS_NUMEROS_SEFIP = "[^0-9A-Za-z\\ ]";
	public static final String REGEX_WHEREIN = "[^0-9,\\ ]";
	public static final DateFormat FORMATADOR_DATA_HORA = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public static final DateFormat FORMATADOR_DATA = new SimpleDateFormat("yyyy-MM-dd");
	public static final DateFormat FORMATADOR_DATA_HORA_TIMEZONE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	public static final String QUEBRA_LINHA_RTF = "\\\\par ";
	public static final String EXISTE_NOVIDADE_VERSAO = "EXISTE_NOVIDADE_VERSAO"; 
	public static final String EXISTE_NOVIDADE_VERSAO_PERIODO = "EXISTE_NOVIDADE_VERSAO_PERIODO";
	public static final String STATUS_PAGO_ECOMPLETO = "2";
	public static final String STATUS_AGUARDANDO_PAGAMENTO_ECOMPLETO = "1";
	public static final String WHEREIN_PROXIMAS_SITUACOES_APOS_PAGO = "2,5,6,12,14,16,35,36,37,51";
	
	public static boolean isSpedFiscal(){
		return "TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SPEDFISCAL));
	}
	
	public static boolean exibirObsRateioConta(){
		return "TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.OBS_RATEIO_CONTA));
	}
	
	public static Integer intervaloNotificacao(){
		try {
			return Integer.parseInt(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.INTERVALO_NOTIFICACAO));
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static String descriptionDecimal(Double d, boolean money){
		if(d == null) return "";
		String format = formatVazio.format(d);
		if (format.startsWith(",")) {
			format = "0" + format;
		}
		if(format.contains(",") && format.split(",")[1].length() == 1){
			format = format + "0";
		}
		if(money && !format.contains(",")){
			format = format + ",00";
		}
		return format;
	}
	
	public static String descriptionDecimal(Double d){
		return SinedUtil.descriptionDecimal(d, false);
	}
	
	public static String joinArray(String[] array, String separator){
		String string = "";
		for (int i = 0; i < array.length; i++) {
			string += array[i] + separator;
		}
		return string.substring(0,string.length() - separator.length());
	}
	
	public static void quebraWhereIn(String clausula, String whereIn, QueryBuilder<?> query) {
		String[] ids = whereIn.split(",");
		List<String> listaIds = Arrays.asList(ids);
		
		query.openParentheses();
		query.where("1 = 2");
		for (int i = 0; i < listaIds.size(); i += 1000){
			int fim = (i + 1000 > listaIds.size()) ? listaIds.size() : i + 1000;
			query.or().where(clausula + " in (" + CollectionsUtil.concatenate(listaIds.subList(i, fim), ",") + ")");
		}
		query.closeParentheses();
	}
	
	public static String quebraWhereInSubQuery(String clausula, String whereIn) {
		String[] ids = whereIn.split(",");
		List<String> listaIds = Arrays.asList(ids);
		
		StringBuilder where = new StringBuilder("(1 = 2");
		for (int i = 0; i < listaIds.size(); i += 1000){
			int fim = (i + 1000 > listaIds.size()) ? listaIds.size() : i + 1000;
			where.append(" or ").append(clausula + " in (" + CollectionsUtil.concatenate(listaIds.subList(i, fim), ",") + ")");
		}
		where.append(")");
		return where.toString();
	}
	
	
	public static List<Element> getListChildElement(String nome, List<Element> content) {
		List<Element> lista = new ArrayList<Element>();
		Object object;
		Element element;
		if(content != null){
			for (int i = 0; i < content.size(); i++) {
				object = content.get(i);
				if(object instanceof Element){
					element = (Element)object;
					if(element.getName().equals(nome)){
						lista.add(element);
					}
				}
			}		
		}
		return lista;
	}
	
	public static Element getChildElement(String nome, List<Element> lista){
		Element child = null;
		Object object;
		Element element;
		if(lista != null){
			for (int i = 0; i < lista.size(); i++) {
				object = lista.get(i);
				if(object instanceof Element){
					element = (Element)object;
					if(element.getName().equals(nome)){
						child = element;
						break;
					}
				}
			}		
		}
		return child;
	}
	
	public static String retiraZeroDireita(Double numDouble){
		if(numDouble != null){
			String numStr = numDouble.toString();
			while(numStr.endsWith("0")){
				numStr = numStr.substring(0, numStr.length() - 1);
			}
			
			if(numStr.endsWith(".")){
				numStr = numStr.substring(0, numStr.length() - 1);
			}
			
			return numStr.replaceAll("\\.", ",");
		} else return null;
	}
	
	public static String retiraZeroEsquerda(String codigo){
		if(codigo != null){
			while(codigo.startsWith("0")){
				codigo = codigo.substring(1);
			}
			return codigo;
		} else return null;
	}
	
	
	/**
	 * M�todo para valida��o de e-mail
	 * 
	 * @param email
	 * @return 
	 * @author Jo�o Paulo Zica
	 */		
	public static Boolean verificaEmail(String email) {
		if (!email.trim().matches("[a-zA-Z_0-9\\.\\-]+?@[a-zA-Z_0-9\\.\\-]+?\\.[a-zA-Z_0-9\\.\\-]+")) {
			return false;
		}
		return true;
	}	
	
	public static String getClientNameForDir() {
		if(Thread.currentThread().getName().startsWith("Job_Integracao_Emporium1_") || 
				Thread.currentThread().getName().startsWith("Job_Integracao_Emporium2_")){
			String dataSource = Thread.currentThread().getName().substring(25);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			
			return "w3erp" + java.io.File.separator + dataSource;
		}else if(Thread.currentThread().getName().startsWith("Job_Bridgestone_")){
			String dataSource = Thread.currentThread().getName().substring(16);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			
			return "w3erp" + java.io.File.separator + dataSource;
		}else if(Thread.currentThread().getName().startsWith("Thread_IntegracaoTEFCallback_")){
			String dataSource = Thread.currentThread().getName().substring(29);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			
			return "w3erp" + java.io.File.separator + dataSource;
		}else if(Thread.currentThread().getName().startsWith("Thread_Rateio_Custeio_")){
			String dataSource = Thread.currentThread().getName().substring(22);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			
			return "w3erp" + java.io.File.separator + dataSource;
		} else if(Thread.currentThread().getName().startsWith("Job_Sincronizacao_Pedido_Venda_") 
				|| Thread.currentThread().getName().startsWith("Job_Sincronizacao_Entrega_")){
			String dataSource = Thread.currentThread().getName().substring(31);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			if(isAmbienteDesenvolvimento()){
				try{
					String dataSourceAux = InitialContext.doLookup("SAVE_DIR_INTEGRACAO_WMS_AMB_DES");
					if(dataSourceAux != null && !dataSourceAux.isEmpty()){
						dataSource = dataSourceAux;
					}
				} catch (Exception e) {	} 
			}
			return "w3erp" + java.io.File.separator + dataSource;
		} else if(Thread.currentThread().getName().startsWith("Job_Nfse_")){
			String dataSource = Thread.currentThread().getName().substring(9);
			dataSource = dataSource.replaceAll("_w3erp_PostgreSQLDS", "");
			
			return "w3erp" + java.io.File.separator + dataSource;
		} else {
			HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
			String url = servletRequest.getRequestURL().toString();
			
			String[] urlDividida = url.split("/");
			
			String servidor = urlDividida[2];
			String contexto = urlDividida[3];
					
			return contexto+java.io.File.separator+servidor;
		}
	}
	
	public static boolean isBriefcase(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("W3ERP_BRIEFCASE");
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome("W3ERP_BRIEFCASE"))){
					NeoWeb.getRequestContext().getSession().setAttribute("W3ERP_BRIEFCASE", Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute("W3ERP_BRIEFCASE", Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isBriefcase(HttpServletRequest request){
		Object attribute = request.getSession().getAttribute("W3ERP_BRIEFCASE");
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome("W3ERP_BRIEFCASE"))){
					request.getSession().setAttribute("W3ERP_BRIEFCASE", Boolean.TRUE);
					return true;
				} else {
					request.getSession().setAttribute("W3ERP_BRIEFCASE", Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isRecapagemChaodeFabrica(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA))){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA, Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isRateioMovimentacaoEstoque(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE))){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE, Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isRecapagemChaodeFabrica(HttpServletRequest request){
		Object attribute = request.getSession().getAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA))){
					request.getSession().setAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA, Boolean.TRUE);
					return true;
				} else {
					request.getSession().setAttribute(Parametrogeral.RECAPAGEM_CHAO_DE_FABRICA, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isListarRazaoSocianClienteExpedicao(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO))){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO, Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isRecapagem(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.RECAPAGEM);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RECAPAGEM, Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RECAPAGEM, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean isRecapagem(HttpServletRequest request){
		Object attribute = request.getSession().getAttribute(Parametrogeral.RECAPAGEM);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if("TRUE".equals(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
					request.getSession().setAttribute(Parametrogeral.RECAPAGEM, Boolean.TRUE);
					return true;
				} else {
					request.getSession().setAttribute(Parametrogeral.RECAPAGEM, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	public static boolean showNewLineButtonRateio(){
		
		String parameterName = "exibirNovaLinhaRateio";
		
		HttpSession session = NeoWeb.getRequestContext().getSession();
		
		Object attribute = session.getAttribute(parameterName);
		
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			
			if("true".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(parameterName))){
				session.setAttribute(parameterName, Boolean.TRUE);
				return true;
			} else {
				session.setAttribute(parameterName, Boolean.FALSE);
				return false;
			}
		}
	}
	
	private static String getNomeCompletoModulo(){
		try {
//			if(SinedUtil.getUsuarioLogado() != null){
				return " - M�dulo " + SinedUtil.getNomeModulo();
//			} else {
//				return "";
//			}
			
		} catch (NotInNeoContextException e) {
			return "";
		}
	}
	
	/**
	 * Retorna o t�tulo da tela.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static String getTituloTela(){
		if(isSined()){
			return "DALTEC SINED" + SinedUtil.getNomeCompletoModulo();
		}
		
		String titulo = MenuService.getInstance().getTituloTela(Util.web.getFirstUrl());
		
		if (titulo==null || titulo.trim().equals("")){
			return "W3ERP" + SinedUtil.getNomeCompletoModulo();
		}else {
			return titulo;
		}
	}
	
	public static String getTituloTela(HttpServletRequest request){
		if(isSined(request)){
			return "DALTEC SINED" + SinedUtil.getNomeCompletoModulo();
		}
		return "W3ERP" + SinedUtil.getNomeCompletoModulo();
	}

	/**
	 * M�todo para valida��o de campos onde s� s�o permitidas letras
	 * 
	 * @param string
	 * @return 
	 * @author Jo�o Paulo Zica
	 */		
	public static Boolean validaLetras(String string){
		if (!string.matches("(([a-zA-Z�-�\\s]).+)")) {
			return false;
		}
		return true;
	}
	
	/**
	 * M�todo que valida o c�digo de barras
	 *
	 * @param cean
	 * @return
	 * @author Luiz Fernando
	 * @author Rodrigo Freitas (Feita a nova valida��o em classe separada)
	 */
	public static Boolean validaCEAN(String cean){
		return BarCodeValidation.valid(cean);
	}
	
	
	/**
	 * M�todo para valida��o de campos string onde s�o permitidos n�meros
	 * 
	 * @param string
	 * @return 
	 * @author Jo�o Paulo Zica
	 */		
	public static Boolean validaDigitos(String string){
		if (!string.matches("(([0-9 -])+)")) {
			return false;
		}
		return true;
	}
	
	/**
	 * M�todo para valida��o de campos string onde s� s�o permitidos n�meros
	 * 
	 * @param string
	 * @return 
	 * @author Jo�o Paulo Zica
	 */		
	public static Boolean validaNumeros(String string){
		if (string == null || !string.matches("(([0-9])+)")) {
			return false;
		}
		return true;
	}
	
	/**
	 * M�todo para pegar o usu�rio logado no sistema
	 * 
	 * @return user
	 * @author Jo�o Paulo Zica
	 */
	public static Usuario getUsuarioLogado(){
		if(Neo.isInRequestContext()){
			User user = Neo.getRequestContext().getUser();
			if(user instanceof Usuario){
				return (Usuario) user;
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @return
	* @since 18/04/2016
	* @author Luiz Fernando
	*/
	public static Integer getCdUsuarioLogado(){
		Usuario usuario  = getUsuarioLogado();
		return usuario != null ? usuario.getCdpessoa() : null;
	}
	
	public static Colaborador getUsuarioComoColaborador(){
		User user = Neo.getRequestContext().getUser();
		if(user instanceof Usuario){
			Usuario usuario = (Usuario) user;
			SinedUtil.markAsReader();
			return SinedUtil.getUsuarioComoColaborador(usuario);
		} else {
			return null;
		}
	}
	
	public static Colaborador getUsuarioComoColaborador(Usuario usuario){
		return ColaboradorService.getInstance().loadWithoutPermissao(new Colaborador(usuario.getCdpessoa()), "colaborador.cdpessoa, colaborador.nome");
	}
		
	/**
	 * Verifica se o usu�rio logado � administrador do sistema
	 * 
	 * @throws SinedException - Se o usu�rio n�o estiver logado
	 * @return TRUE - Se o usu�rio tiver em um de seus pap�is o perfil de adminstrador.
	 * 		   FALSE - Se o usu�rio n�o tiver algum de seus pap�is o perfil de administrador. 
	 */
	public static Boolean isUsuarioLogadoAdministrador(){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if (usuarioLogado == null) 
			throw new SinedException("N�o foi encontrado usu�rio logado.");
		
		return SinedUtil.isUsuarioAdministrador(usuarioLogado);
	}
	
	public static Boolean isUsuarioAdministrador(Usuario usuario){
		if (usuario == null) 
			throw new SinedException("N�o foi encontrado usu�rio.");
		
		List<Usuariopapel> listaUsuariopapel = usuario.getListaUsuariopapel();
		boolean isadmin = false;
		if(listaUsuariopapel != null){
			for (Usuariopapel usuariopapel : listaUsuariopapel) {
				if(usuariopapel.getPapel().getAdministrador()){
					isadmin = true;
					break;
				}
			}
		}
		return isadmin;
	}
	
	public static Boolean isRestricaoEmpresaFornecedorUsuarioLogado(){
		try {
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			return usuarioLogado != null && usuarioLogado.getRestricaoempresafornecedor() != null && usuarioLogado.getRestricaoempresafornecedor();
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Boolean isRestricaoFornecedorProdutoUsuarioLogado(){
		try {
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			return usuarioLogado != null && usuarioLogado.getRestricaofornecedorproduto() != null && usuarioLogado.getRestricaofornecedorproduto();
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Boolean isRestricaoVendaVendedorUsuarioLogado(){
		try {
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			return usuarioLogado != null && usuarioLogado.getRestricaovendavendedor() != null && usuarioLogado.getRestricaovendavendedor();
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Boolean isRestricaoEmpresaClienteUsuarioLogado(){
		try {
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			return usuarioLogado != null && usuarioLogado.getRestricaoempresacliente() != null && usuarioLogado.getRestricaoempresacliente();
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * M�todo para trazer a descri��o completa do m�dulo a partir da URL
	 * 
	 * @author Pedro Gon�alves
	 * @return nome
	 */
	public static String getNomeModulo() {
		return getNomeModulo((String) NeoWeb.getRequestContext().getAttribute("NEO_MODULO"));
	}
	
	/**
	 * M�todo para trazer a descri��o completa do m�dulo a partir da URL
	 * 
	 * @author Mairon Cezar
	 * @return nome
	 */
	public static String getNomeModulo(String modulo) {
		if ("adm".equals(modulo)) modulo = "Administra��o";
		else if ("financeiro".equals(modulo)) modulo = "Financeiro";
		else if ("rh".equals(modulo)) modulo = "Recursos humanos";
		else if ("sistema".equals(modulo)) modulo = "Sistema";
		else if ("projeto".equals(modulo)) modulo = "Projeto";
		else if ("juridico".equals(modulo)) modulo = "Jur�dico";
		else if ("crm".equals(modulo)) modulo = "CRM";
		else if ("faturamento".equals(modulo)) modulo = "Faturamento";
		else if ("contabil".equals(modulo)) modulo = "Cont�bil";
		else if ("servicointerno".equals(modulo)) modulo = "Servi�os";
		else if ("almoxarifado".equals(modulo)) modulo = "Almoxarifado";
		else if ("suprimento".equals(modulo)) modulo = "Suprimento";
		else if ("fornecedor".equals(modulo)) modulo = "Cota��o de Fornecedor";
		else if ("veiculo".equals(modulo)) modulo = "Ve�culo";
		else if ("briefcase".equals(modulo)) modulo = "BriefCase";
		else if ("fiscal".equals(modulo)) modulo = "Fiscal";
		else if ("pub".equals(modulo)) modulo = "P�blico";
		else if ("producao".equals(modulo)) modulo = "Produ��o";
		else if ("geradorrelatorio".equals(modulo)) modulo = "Gerador de Relat�rios";
		
		return modulo;
	}
	
	/**
	 * Verifica se o usu�rio ter permiss�o de acesso ao menos uma tela do m�dulo.
	 * Os resultados s�o guardados em cache na sess�o.
	 * @see br.com.linkcom.sined.util.SinedUtil#SINED_CACHE_MODULES
	 * 
	 * @see br.com.linkcom.sined.util.SinedUtil.findPermission(String module)
	 * @author Pedro Gon�alves
	 */
	@SuppressWarnings("unchecked")
	public static boolean getPermissaoModulo(String modulo) {
		HttpSession session = NeoWeb.getRequestContext().getSession();
		Object attribute = session.getAttribute(SINED_CACHE_MODULES);
		if(attribute == null) {
			HashMap<String, Boolean> hashModulePermission = new HashMap<String, Boolean>();
			hashModulePermission.put(modulo, findPermission(modulo) > 0);
			session.setAttribute(SINED_CACHE_MODULES, hashModulePermission);
			return hashModulePermission.get(modulo);
		} else {
			HashMap<String, Boolean> hashModulePermission = (HashMap<String, Boolean>) attribute;
			if(hashModulePermission.containsKey(modulo)){
				return hashModulePermission.get(modulo);
			} else {
				hashModulePermission.put(modulo, findPermission(modulo) > 0);
				session.setAttribute(SINED_CACHE_MODULES, hashModulePermission);
				return hashModulePermission.get(modulo);
			}
		}
		
		//return false;
	}
	
	/**
	 * Verifica se o usu�rio logado tem permiss�o de acesso ao menos uma tela pertencente ao m�dulo. 
	 * @see br.com.linkcom.sined.geral.service.PermissaoService#findPermissaoModulo(List<Usuariopapel> listUsuariopapel, String path)
	 * 
	 * @param module
	 * @return
	 * @user Pedro Gon�alves
	 */
	private static Long findPermission(String module){
		if (!LicenseManager.moduloLicenciado(module))
			return 0L;
		
		Usuario usuario = (Usuario) NeoWeb.getUser();
		List<Usuariopapel> listUsuariopapel = usuario.getListaUsuariopapel();
		Long admin = Long.valueOf(0);
		for (Usuariopapel usuariopapel : listUsuariopapel) {
			if(usuariopapel.getPapel().getAdministrador()){
				admin = Long.valueOf(1);
			}
		}
		if (admin > 0) {
			return admin;
		} else {
			Long findPermissaoModulo = PermissaoService.getInstance().findPermissaoModulo(listUsuariopapel, module);
			return findPermissaoModulo;
		}
	}
	
	/**
	 * Verifica se o n�vel do usu�rio tem permiss�o de acesso a a��o
	 * 
	 * @author Pedro Gon�alves
	 * 			Rodrigo Freitas - Ajsutes para ignorar as permiss�es se o usu�rio � administrador.
	 * @param acao
	 * @return
	 */
	
	public static Boolean isUserHasAction(String acao){
		return isUserHasAction(getUsuarioLogado(), acao);
	
	}
	
	/**
	 * Verifica se o n�vel do usu�rio tem permiss�o de acesso a a��o
	 * 
	 * @author Pedro Gon�alves
	 * 			Rodrigo Freitas - Ajsutes para ignorar as permiss�es se o usu�rio � administrador.
	 * 			Igor - Adicionado o usu�rio como parametro do met�do
	 * @param usuario
	 * @param acao
	 * @return
	 */
	
	@SuppressWarnings("unchecked")
	public static Boolean isUserHasAction(Usuario usuario, String acao){
		acao = acao.toUpperCase();
		HttpSession session = NeoWeb.getRequestContext().getSession();
		Object attribute = session.getAttribute(SINED_CACHE_ACAOPERMISSION);
		if(attribute == null) {
			HashMap<String, Boolean> hashModulePermission = new HashMap<String, Boolean>();
			List<Usuariopapel> listaUsuariopapel = (usuario==null ? getUsuarioLogado() : usuario).getListaUsuariopapel();
			Boolean admin = false;
			for (Usuariopapel usuariopapel : listaUsuariopapel) {
				if (usuariopapel.getPapel().getAdministrador()) {
					admin = true;
				}
			}
			if (admin) {
				List<Acao> listaAllAcao = AcaoService.getInstance().findAll();
				for (Acao acao2 : listaAllAcao) {
					hashModulePermission.put(acao2.getKey(),true);
				}
				session.setAttribute(SINED_CACHE_ACAOPERMISSION, hashModulePermission);
			} else {
				List<Acaopapel> findAllPermissionsByPapel = AcaopapelService.getInstance().findAllPermissionsByPapel(listaUsuariopapel);
				for (Acaopapel acaopapel : findAllPermissionsByPapel) {
						hashModulePermission.put(acaopapel.getAcao().getKey(), acaopapel.getPermitido());
				}
				session.setAttribute(SINED_CACHE_ACAOPERMISSION, hashModulePermission);
			}

			if(hashModulePermission.containsKey(acao)){
				return hashModulePermission.get(acao);
			} else {
				return false;
			}
		} else {
			HashMap<String, Boolean> hashModulePermission = (HashMap<String, Boolean>) attribute;
			if(hashModulePermission.containsKey(acao)){
				return hashModulePermission.get(acao);
			} else {
				return false;
			}
		}
	}
	
	/**
	 * M�todo respons�vel por converter listas em vari�veis javascript.
	 * 
	 * @param lista
	 * @param var
	 * @param label
	 * @return
	 */
	public static String convertToJavaScript(List<?> lista, String var, String label) {
		StringBuilder javascript = new StringBuilder();		
		if(Util.strings.isNotEmpty(var)){
			javascript.append("var "+var+" = ");
		}
		javascript.append("[");
		for (Iterator<?> iter = lista.iterator(); iter.hasNext();) {
			Object element = iter.next();
			String description;
			if (Util.strings.isEmpty(label)) {
				description = Util.strings.toStringDescription(element);
			} else {
				PropertyDescriptor propertyDescriptor = Neo.getApplicationContext().getBeanDescriptor(element).getPropertyDescriptor(label);
				description = Util.strings.toStringDescription(propertyDescriptor.getValue());
			}
			description = escapeSingleQuotes(description);
			String id = Util.strings.toStringIdStyled(element);
			javascript.append("['"+id+"', '"+description+"']");
			if(iter.hasNext()){
				javascript.append(",");
			}
		}
		javascript.append("];");
		return javascript.toString();
	}
	
	/**
	 * Converte lista de Enum para lista de JavaScript
	 * 
	 * @param lista
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static String convertEnumToJavaScript(Collection<? extends Enum<?>> lista){
		StringBuilder js = new StringBuilder();
		if(SinedUtil.isListNotEmpty(lista)){
			js.append("[");
			
			for (Iterator<? extends Enum<?>> iter = lista.iterator(); iter.hasNext();) {
				Enum<? extends Enum<?>> enum1 = iter.next();
				String description = Util.strings.toStringDescription(enum1);
				String value = enum1.name();
				js.append("['").append(value).append("','").append(description).append("']");
				if(iter.hasNext()){
					js.append(",");
				}
			}
			js.append("]");
		}
		
		return js.toString();
	}
	
	public static String escapeSingleQuotes(String message) {
		if(message == null) return message;
		
		return message
				.replace((CharSequence)"'", "\\'")
				.replace((CharSequence)"\n", " ")
				.replace((CharSequence)"\r", " ");
	}
	
	public static String escapeJavascript(String message) {
		if(message == null) return message;
		
		return message
				.replace((CharSequence)"'", "\\'")
				.replace((CharSequence)"\r", " ")
				.replace((CharSequence)"\n", "' + '\\n")
				;
	}
	
	/**
	 * M�todo respons�vel pela valida��o de PIS/PASEP
	 * 
	 * @param doc
	 * @return
	 * @author Flavio Tavares
	 */
	public static Boolean verificaPisPasep(String doc){
	      doc = doc.replace(".", "").replace(".", "").replace("-", "");

	      if(doc.length() != 11)
	         return false;
	      if(doc.equals("00000000000"))
	         return false;

	      int intTotal = 0;
	      String strPeso = "3298765432";

	      for(int i = 0; i < 10; i++) {
	         int intResultado = Integer.parseInt(doc.substring(i, i+1)) * Integer.parseInt(strPeso.substring(i, i+1));      
	         intTotal += intResultado;
	      }

	      Integer intResto = intTotal % 11;
	      if(intResto != 0)
	         intResto = 11 - intResto;
	      if(intResto == 10 || intResto == 11)
	         intResto = 0;
	      if(!intResto.equals(Integer.parseInt(doc.substring(10, 11), 10)))
	         return false;

	      return true;
	}
	
	public static Double roundAliquotaNfse(Double value, int scale, boolean arredondar){
		if(value == null) return null;
		if(!arredondar) return value;
		
		BigDecimal decimal = new BigDecimal(value);
		decimal = decimal.setScale(scale, BigDecimal.ROUND_HALF_UP);
		
		return decimal.doubleValue();
	}
	
	/**
	 * M�todo para arredondar valores.
	 * 
	 * @param value
	 * @param scale - N�mero de casas decimais.
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static Double round(Double value, int scale){
		return round(value, scale, true);
	}
	
	public static Double round(Double value, int scale, boolean considerarEscalaDez){
		if(value == null) return null;
		
		BigDecimal decimal = new BigDecimal(value);
		
		if(considerarEscalaDez) {
			decimal = decimal.setScale(10, BigDecimal.ROUND_HALF_UP);
		}
		
		decimal = decimal.setScale(scale, BigDecimal.ROUND_HALF_UP);
		
		return decimal.doubleValue();
	}
	
	public static Double roundUp(Double value, int scale){
		if(value == null) return null;

	    long factor = (long) Math.pow(10, scale);
	    value = value * factor;
	    value = round(value, 5);
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	/**
	 * M�todo para arredondar valores - Bigdecimal
	 *
	 * @param value
	 * @param scale - N�mero de casas decimais
	 * @return
	 * @author Luiz Fernando
	 */
	public static BigDecimal round(BigDecimal value, int scale){
		if(value == null) return null;
				
		return value.setScale(scale, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * Cria um lista de uma propriedade distinta a partir de uma collection.
	 * 
	 * @param collection
	 * @param property
	 * @return
	 * @author Rodrigo Freitas
	 */
	private static List<?> getListDistinctProperty(Collection<?> collection, String property){
		List<Object> list = new ArrayList<Object>();
		Iterator<?> iter = collection.iterator();
		if(iter.hasNext()){
			Object next = iter.next();
			ExtendedBeanWrapper beanWrapper = new ExtendedBeanWrapper(next);
			
			Object propertyValue = beanWrapper.getPropertyValue(property);
			if (propertyValue != null && !list.contains(propertyValue)) {
				list.add(propertyValue);
			}
			

			while(iter.hasNext()){
				try {
					beanWrapper.setWrappedInstance(iter.next());
					propertyValue = beanWrapper.getPropertyValue(property);
					if (propertyValue != null && !list.contains(propertyValue)) {
						list.add(propertyValue);
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return list;
	}
	
	
	
	/**
	 * Ao passar a lista e o separador, � montada uma string com a propriedade e ela n�o se repete.
	 * 
	 * @see br.com.linkcom.sined.util.SinedUtil#getListDistinctProperty
	 * @see br.com.linkcom.neo.util.CollectionsUtil#concatenate
	 * @param collection
	 * @param property
	 * @param token
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static String listAndConcatenate(Collection<?> collection, String property, String token){
		return CollectionsUtil.concatenate(getListDistinctProperty(collection, property),token);
	}
	
	/**
	 * M�todo para concatenar os ID's de uma lista de Beans para ser usado com em where in.
	 * 
	 * @param list
	 * @return - ID's concatenados e separados por v�rgula ou vazio em caso de lista vazia ou nula.
	 * @author Fl�vio Tavares
	 */
	public static String listAndConcatenateIDs(Collection<?> list){
		if(isListNotEmpty(list)){
			Object object = list.iterator().next();
			String property = NeoWeb.getApplicationContext().getBeanDescriptor(object).getIdPropertyName();
			
			if(StringUtils.isNotBlank(property)){
				return CollectionsUtil.listAndConcatenate(list, property, ",");
			}
		}
		return "";
	}
	
	/**
	 * Ao passar a lista e o separador, � montada uma string com a propriedade e ela n�o se repete.
	 * 
	 * @see br.com.linkcom.sined.util.SinedUtil#getListDistinctProperty
	 * @see br.com.linkcom.neo.util.CollectionsUtil#concatenate
	 * @param collection
	 * @param property
	 * @param token
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public static String listAndConcatenateSort(Collection<?> collection, String property, String token){
		List<?> listDistinctProperty = getListDistinctProperty(collection, property);
		if(listDistinctProperty != null && listDistinctProperty.size() > 0){
			if(String.class.isInstance(listDistinctProperty.get(0))){
				Collections.sort((List<String>)listDistinctProperty, String.CASE_INSENSITIVE_ORDER);
			} else if(Integer.class.isInstance(listDistinctProperty.get(0))){
				Collections.sort((List<Integer>)listDistinctProperty);
			}
		}
		return CollectionsUtil.concatenate(listDistinctProperty,token);
	}
	
	
	/**
	 * <p>Obt�m a url completa com o contexto da aplica��o.</p>
	 * <p><b>Exemplo</b><pre>http://www.sined.com.br/sined</pre></p>
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static String getUrlWithContext(){
		String url = NeoWeb.getRequestContext().getServletRequest().getRequestURL().toString();
		String[] urlDividida = url.split("/");
		String servidor = urlDividida[2];
		
		String http = "https:".equals(urlDividida[0]) ? "https:" : "http:"; 
		return http + "//" + servidor + NeoWeb.getRequestContext().getServletRequest().getContextPath();
		
	}	
	
	public static String getUrlWithoutContext(){
		String url = NeoWeb.getRequestContext().getServletRequest().getRequestURL().toString();
		String[] urlDividida = url.split("/");
		String servidor = urlDividida[2];
		return servidor;
	}
	
	public static String getUrlWithoutContext(HttpServletRequest request){
		String url = request.getRequestURL().toString();
		String[] urlDividida = url.split("/");
		String servidor = urlDividida[2];
		return servidor;
	}

	
	public static String getListaProjeto() {
		String whereIn = null;
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaProjetoLogado");
		if(attribute != null){
			whereIn = (String)attribute;
		} else {
			Usuario usuario = (Usuario) NeoWeb.getUser();
			if(usuario != null){
				usuario = UsuarioService.getInstance().carregaUsuarioWithProjeto(usuario);
				
				whereIn = SinedUtil.listAndConcatenate(usuario.getListaUsuarioprojeto(), "projeto.cdprojeto", ",");
				NeoWeb.getRequestContext().getSession().setAttribute("listaProjetoLogado", whereIn);
			}
		}
		return whereIn;
	}
	
	public static void zeraListaProjeto(){
		NeoWeb.getRequestContext().getSession().removeAttribute("listaProjetoLogado");		
	}
	
	
	/**
	 * Carrega a lista de empresa do usu�rio logado.
	 * 
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#carregaUsuarioWithEmpresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String getListaEmpresa() {
		String listAndConcatenate = null;
		if(Neo.isInRequestContext()){
			User user = Neo.getUser();
			if(user instanceof Usuario){
				Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaEmpresaLogado");
				if (attribute != null) {
					listAndConcatenate = (String)attribute;
				} else {
					Usuario usuario = (Usuario) user;
					if(usuario!= null){
						usuario = UsuarioService.getInstance().carregaUsuarioWithEmpresa(usuario);
		
						listAndConcatenate = SinedUtil.listAndConcatenate(usuario.getListaUsuarioempresa(), "empresa.cdpessoa", ",");
						NeoWeb.getRequestContext().getSession().setAttribute("listaEmpresaLogado", listAndConcatenate);
					}
				}	
			}
		}
		return listAndConcatenate;
	}
	
	/**
	* M�todo que verifica se o usu�rio tem permiss�o em um determinada empresa
	*
	* @param empresa
	* @return
	* @since 15/01/2016
	* @author Luiz Fernando
	*/
	public boolean getUsuarioPermissaoEmpresa(Empresa empresa){
		if(empresa != null && empresa.getCdpessoa() != null){
			String whereInEmpresa = getListaEmpresa();
			if(StringUtils.isNotBlank(whereInEmpresa)){
				for(String cdpessoa : whereInEmpresa.split(",")){
					if(cdpessoa.equals(empresa.getCdpessoa().toString())) return true;
				}
			}
		}
		return false;
	}
	
	public boolean getUsuarioPermissaoEmpresaId(Integer cdpessoa){
		return cdpessoa == null || getUsuarioPermissaoEmpresa(new Empresa(cdpessoa));
	}
	
	public boolean getUsuarioPermissaoFornecedorId(Integer cdpessoa){
		return cdpessoa == null || getUsuarioPermissaoFornecedor(new Fornecedor(cdpessoa));
	}
	
	public boolean getUsuarioPermissaoFornecedor(Fornecedor fornecedor){
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			return FornecedorService.getInstance().isPermissaoFornecedor(fornecedor);
		}
		return false;
	}
	
	public static String getListaEmail(){
		
		Parametrogeral pg = ParametrogeralService.getInstance().findByNome("EMAIL_ADMIN");
		
		if(pg == null || pg.getValor() == null || pg.getValor().equals("")){
			throw new SinedException("E-mail do administrador n�o encontrado.");
		}
		
		String[] emails = pg.getValor().split(";");
		String string = "";
		
		for (int i = 0; i < emails.length; i++) {
			if(i==0) string = emails[i];
			else string += ", " + emails[i];
		}
		
		return string;
	}
	
	public static String getTelefoneAdmin(){
		
		Parametrogeral pg = ParametrogeralService.getInstance().findByNome("TELEFONE_ADMIN");
		
		if(pg == null || pg.getValor() == null || pg.getValor().equals("")){
			throw new SinedException("Telefone do administrador n�o encontrado.");
		}
		
		return pg.getValor();
	}
	
	/**
	 * M�todo utilizado para enviar um email aos respons�veis pelo Sined quando houver 
	 * problema de compatibilidade com o banco.
	 * 
	 * @author Fl�vio Tavares
	 */
	public static void enviaEmailAdmin(){
		String template = null;
		
		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
		String url = servletRequest.getRequestURL().toString();
		
		String[] urlDividida = url.split("/");
		
		String servidor = urlDividida[2];
		String contexto = urlDividida[3];
		
		urlDividida = servidor.split(".");
		if(urlDividida.length==0){
			urlDividida = servidor.split(":");
			if(urlDividida.length>0){				
				servidor = urlDividida[0];
			}
		}
		
		if("localhost".equals(servidor)){
			return;
		}
		
		try {
			template = new TemplateManager("/WEB-INF/template/templateErroVersao.tpl")
				.assign("app", contexto)
				.assign("ctx", servidor)
				.getTemplate();
		} catch (IOException e) {
			throw new SinedException("Erro ao obter template para e-mail.",e);
		}
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		
		
		Parametrogeral pg = ParametrogeralService.getInstance().findByNome("EMAIL_ADMIN");
		
		if(pg == null || pg.getValor() == null || pg.getValor().equals("")){
			throw new SinedException("E-mail do administrador n�o encontrado.");
		}
		
		String[] emails = pg.getValor().split(";");
		List<String> listTo = new ArrayList<String>();
		String string;
		
		for (int i = 0; i < emails.length; i++) {
			string = emails[i];
			if(string != null && !string.trim().equals("")){
				listTo.add(string.trim());
			}
		}
		try {
			email
				.setFrom("w3erp@w3erp.com.br")
				.setListTo(listTo)
				.setSubject("[W3ERP] Problema de compatibilidade de banco.")
				.addHtmlText(template)
				.sendMessage();
		} catch (Exception e) {
			throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
		}
	}
	
	public static void enviarEmailErro(String assunto,String mensagem, Exception e) {
		if(!SinedUtil.isAmbienteDesenvolvimento() && ParametrogeralService.getInstance().getBoolean(Parametrogeral.ENVIAR_EMAIL_ERRO_COMUNICACAO_EMISSOR)){
			List<String> listTo = new ArrayList<String>();
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				String nomeMaquina = "N�o encontrado.";
				try {  
		            InetAddress localaddr = InetAddress.getLocalHost();  
		            nomeMaquina = localaddr.getHostName();  
		        } catch (UnknownHostException e3) {}  
				

		        String url = "N�o encontrado";
				try {  
					url = SinedUtil.getUrlWithContext();
		        } catch (Exception e3) {}
		        
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject(assunto)
					.addHtmlText(nomeMaquina + " - " + url + " - " + mensagem)
					.sendMessage();
			} catch (Exception e1) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
	
	/**
	 * M�todo utilizado para enviar um email aos respons�veis pelo sistema quando houver erro de integra��o;
	 * 
	 * @author Rafael Salvio
	 */
	public static void enviaEmailErroSincronizacaoWMS(String tipoSincronizacao, Integer cdsincronizacao, Exception exception){
		String stacktrace = "";
		try{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			exception.printStackTrace(pw);
			stacktrace = sw.toString();
		} catch (Exception e) {
//			System.out.println("Erro ao obter o stacktrace do erro de sincroniza��o.");
			e.printStackTrace();
			return;
		}
		
		enviaEmailErroSincronizacaoWMS(tipoSincronizacao, cdsincronizacao, stacktrace);
	}
	
	/**
	 * M�todo utilizado para enviar um email aos respons�veis pelo sistema quando houver erro de integra��o;
	 * 
	 * @author Rafael Salvio
	 */
	public static void enviaEmailErroSincronizacaoWMS(String tipoSincronizacao, Integer cdsincronizacao, String stacktrace){
		String template = null;
		String cliente = "";
		if (Thread.currentThread().getName().startsWith("Job_Sincronizacao_Pedido_Venda_")){
			String dataSource = Thread.currentThread().getName().substring(31);
			if(dataSource != null){
				cliente = dataSource.split("\\.")[0];
			}
		} else if (Thread.currentThread().getName().startsWith("Job_Sincronizacao_Entrega_")){
			String dataSource = Thread.currentThread().getName().substring(26);
			if(dataSource != null){
				cliente = dataSource.split("\\.")[0];
			}
		} else{
//			System.out.println("Erro ao identificar a Thread de sincroniza��o.");
			return;
		}
		
		try {
			template = new TemplateManager("/WEB-INF/template/templateErroSincronizacao.tpl")
				.assign("cliente", cliente)
				.assign("tipoSincronizacao", tipoSincronizacao)
				.assign("cdSincronizacao", cdsincronizacao.toString())
				.assign("stacktrace", stacktrace)
				.getTemplate();
		} catch (IOException e) {
//			System.out.println("Erro ao obter template para e-mail.");
			e.printStackTrace();
			return;
		}
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		
		
		Parametrogeral pg = ParametrogeralService.getInstance().findByNome("EMAILS_ERRO_INTEGRACAO_WMS");
		
		if(pg == null || pg.getValor() == null || pg.getValor().equals("")){
			pg = new Parametrogeral();
			pg.setValor("rodrigo.freitas@linkcom.com.br;rafael.salvio@linkcom.com.br");
		}
		
		String[] emails = pg.getValor().split(";");
		List<String> listTo = new ArrayList<String>();
		String string;
		
		for (int i = 0; i < emails.length; i++) {
			string = emails[i];
			if(string != null && !string.trim().equals("")){
				listTo.add(string.trim());
			}
		}
		try {
			email
				.setFrom("w3erp@w3erp.com.br")
				.setListTo(listTo)
				.setSubject("[W3ERP] Problema de sincroniza��o de "+tipoSincronizacao+".")
				.addHtmlText(template)
				.sendMessage();
		} catch (Exception e) {
//			System.out.println("Erro ao enviar e-mail ao administrador.");
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Coloca aspas simples em cada valor de uma cl�usula "WhereIn", quando este � composto de
	 * valores alfanum�ricos separados por v�rgula.
	 * <p>
	 * Ex.: "Jo�o,Francisco,Joaquim" ----> "'Jo�o','Francisco','Joaquim'"
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String montaWhereInString(String whereIn) {
		if (whereIn == null || "".equals(whereIn))
			return null;
		
		StringBuffer buffer = new StringBuffer();
		for (String cpf : whereIn.split(",")) {
			buffer
				.append("'")
				.append(cpf)
				.append("',");
		}
		return buffer.deleteCharAt(buffer.length()-1).toString();
	}
	
	
	/**
	 * Coloca aspas simples em cada valor de uma clausula "WhereIn", quando este � composto
	 * de valores alfanumeriocs separados por qualquer caractere
	 * @param whereIn
	 * @author Dhiogo Morais
	 * @return
	 */
	
	public static String montaWhereInStringVersaoNova(String identificadorExterno, String caractere) {
		if (identificadorExterno == null || "".equals(identificadorExterno))
			return null;
		
		StringBuffer buffer = new StringBuffer();
		for (String nome : identificadorExterno.split(caractere)) {
			buffer
				.append("'")
				.append(nome)
				.append("',");
		}
		return buffer.deleteCharAt(buffer.length()-1).toString();
	}
	
	/**
	 * Transforma em inteiro cada valor de uma cl�usula "WhereIn", quando este � composto de
	 * valores alfanum�ricos separados por v�rgula.
	 * Feito para solucionar erro no parser de where in do hibernate. N�o permite 0 a esquerda em valores inteiros
	 * <p>
	 * 
	 * @param whereIn
	 * @return
	 * @author Marden Silva
	 */
	public static String montaWhereInInteger(String whereIn) {
		if (whereIn == null || "".equals(whereIn))
			return null;
		
		StringBuffer buffer = new StringBuffer();
		for (String cpf : whereIn.split(",")) {
			try{
				buffer
					.append(Integer.parseInt(cpf))
					.append(",");
			} catch (Exception e) {	}
		}
		return buffer.length() > 0 ? buffer.deleteCharAt(buffer.length()-1).toString() : null;
	}
	
	/**
	 * Recebe uma string de valores separados por v�rgula e, caso exista
	 * algum valor repetido, este � removido.
	 * 
	 * @param s
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String removeValoresDuplicados(String s) {
		List<String> listaString = new ArrayList<String>(Arrays.asList(s.split(",")));
		List<String> listaNaoDuplicada = new ArrayList<String>();
		String ultimo = null;
		
		//Ordena a lista para facilitar a localiza��o dos valores repetidos.
		Collections.sort(listaString);
		
		for (String elem : listaString) {
			if (!StringUtils.equals(ultimo, elem)) {
				listaNaoDuplicada.add(elem);
				ultimo = elem;
			}
		}
		
		return CollectionsUtil.concatenate(listaNaoDuplicada, ",");
	}
		
	/**
	 * M�todo para verificar se um determinado arquivo � uma imagem.
	 * 
	 * @param arquivo
	 * @return true - se o arquivo for imagem.<br>
	 * 			false - se o arquivo for null ou n�o for imagem.
	 * @author Fl�vio Tavares
	 */
	public static boolean isImage(Arquivo arquivo){
		if(arquivo == null) return false;
		try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(arquivo.getContent()));
			if (image == null) return false;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Retorna a descri��o do per�odo formatada de acordo com o padr�o, para
	 * ser colocado em relat�rios.
	 * 
	 * @param dtInicio
	 * @param dtFim
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String getDescricaoPeriodo(Date dtInicio, Date dtFim) {
		String periodo = null;
		
		if (dtInicio != null && dtFim != null) {
			periodo = SinedDateUtils.toString(dtInicio) + " a " + SinedDateUtils.toString(dtFim);
		} else if (dtInicio != null) {
			periodo = "A partir de " + SinedDateUtils.toString(dtInicio);
		} else if (dtFim != null) {
			periodo = "At� " + SinedDateUtils.toString(dtFim);
		}
		
		return periodo;
	}
	
	/**
	 * Retorna a descri��o do per�odo formatada de acordo com o padr�o, para
	 * ser colocado em relat�rios.
	 * 
	 * @param dtInicio
	 * @param dtFim
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static String getDescricaoPeriodoTimestamp(Timestamp dtInicio, Timestamp dtFim) {
		String periodo = null;
		
		if (dtInicio != null && dtFim != null) {
			periodo = SinedDateUtils.toString(dtInicio, "dd/MM/yyyy HH:mm") + " a " + SinedDateUtils.toString(dtFim, "dd/MM/yyyy HH:mm");
		} else if (dtInicio != null) {
			periodo = "A partir de " + SinedDateUtils.toString(dtInicio, "dd/MM/yyyy HH:mm");
		} else if (dtFim != null) {
			periodo = "At� " + SinedDateUtils.toString(dtFim, "dd/MM/yyyy HH:mm");
		}
		
		return periodo;
	}
	
	/**
	 * Pega a imagem que est� associada � empresa no cadastro de Empresa.
	 * Se n�o for encontrada uma imagem ou se uma empresa n�o for passada, retorna
	 * null.
	 * 
	 * @see br.com.linkcom.sined.sgeral.service.ArquivoService#loadWithContents(br.com.linkcom.neo.types.File)
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadAsImage(br.com.linkcom.neo.types.File)
	 * @param empresa
	 * @return a logomarca associada � empresa. Pode ser null.
	 * @author Hugo Ferreira
	 */
	public static Image getLogo(Empresa empresa) {
		Image imageLogo = null;

		try {
			if (empresa != null &&  empresa.getCdpessoa() != null && empresa.getLogomarca() != null 
					&& empresa.getLogomarca().getCdarquivo() != null) {
				Arquivo logomarca = empresa.getLogomarca();
				logomarca = ArquivoService.getInstance().loadWithContents(logomarca); //aqui ele da erro logomarca t� nula
				imageLogo = ArquivoService.getInstance().loadAsImage(logomarca);
			}
		} catch (NeoException e) {
//			System.out.println("Imagem para relat�rio n�o encontrada:");
			e.printStackTrace();
		}

		return imageLogo;
	}
	
	public static String getLogoURLForReport(Empresa empresa) {
		try {
			if (empresa != null &&  
					empresa.getCdpessoa() != null && 
					empresa.getLogomarca() != null && 
					empresa.getLogomarca().getCdarquivo() != null) {
				return getUrlWithContext() + "/DOWNLOADFILE/" + empresa.getLogomarca().getCdarquivo() + "?isNfeDesktop=true&senhaNfeDesktop=linkcom";
			}
		} catch (NeoException e) {
//			System.out.println("Imagem para relat�rio n�o encontrada:");
			e.printStackTrace();
		}

		return "";
	}
	
	/**
	* M�todo que adiciona o cdarquivo na sess�o e retorna a url do arquivo 
	*
	* @param cdarquivo
	* @return
	* @since 30/08/2016
	* @author Luiz Fernando
	*/
	public static String getImgURLForGeradorReport(String cdarquivo) {
		try { return getImgURLForGeradorReport(Long.parseLong(cdarquivo)); } catch (Exception e) { return ""; }
	}
	
	/**
	* M�todo que adiciona o cdarquivo na sess�o e retorna a url do arquivo
	*
	* @param cdarquivo
	* @return
	* @since 30/08/2016
	* @author Luiz Fernando
	*/
	public static String getImgURLForGeradorReport(Long cdarquivo) {
		try {
			if (cdarquivo != null) {
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), cdarquivo);
				return getUrlWithContext() + DownloadFileServlet.DOWNLOAD_FILE_PATH + "/" + cdarquivo;
			}
		} catch (NeoException e) {}

		return "";
	}
	
	/**
	 * M�todo para verificar se um lista est� vazia.
	 * <pre>
	 * 	list == null returns true
	 * 	list.size() == 0 returns true
	 * 	list.size() > 0 returns false
	 * </pre>
	 * 
	 * @see #isListNotEmpty(Collection)
	 * @param list
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static boolean isListEmpty(Collection<?> list){
		return !SinedUtil.isListNotEmpty(list);
	}
	
	/**
	 * M�todo para verificar se um lista <b>n�o</b> est� vazia.
	 * <pre>
	 * 	list == null returns false
	 * 	list.size() > 0 returns true
	 * 	list.size() == 0 returns false
	 * </pre>
	 * 
	 * @param list
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static boolean isListNotEmpty(Collection<?> list){
		return list != null && list.size() > 0;
	}
	
	public static boolean isListNotEmptyAndInitialized(Collection<?> list){
		return Hibernate.isInitialized(list) && list != null && list.size() > 0;
	}
	
	/**
	 * <p>M�todo para verificar se um campo <tt>Money</tt> � null ou 0.</p>
	 * <pre>
	 * 	SinedUtil.isBlank(null)	= true
	 * 	SinedUtil.isBlank(00,00)	= true
	 * 	SinedUtil.isBlank(12,34)	= false
	 * 	SinedUtil.isBlank(-12,34)	= false
	 * </pre>
	 * @param valor
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static boolean isBlank(Money valor){
		return valor == null || valor.compareTo(BigDecimal.ZERO) == 0;
	}
		
	public static double truncate(double value) {  
	    return Math.round(value * 100) / 100d;  
	}
	
	public static double truncateFloor(double value) {  
		return Math.floor(value * 100) / 100d;  
	}

	public static double truncateFormat(double value, int qtdeCasasDecimais){
		StringBuilder sb = new StringBuilder("#0.");
		for(int i = 0; i < qtdeCasasDecimais; i++) sb.append("#");
		
		DecimalFormat df = new DecimalFormat(sb.toString());
		df.setRoundingMode(RoundingMode.DOWN);
		
		String str = df.format(value);
		str = str.replaceAll(",", ".");
		return Double.valueOf(str);
	}
	
	public static double truncateFormat(double value){
		DecimalFormat df = new DecimalFormat("#0.00");
		df.setRoundingMode(RoundingMode.DOWN);
		
		String str = df.format(value);
		str = str.replaceAll(",", ".");
		return Double.valueOf(str);
	}
	
	public static double truncateFormatEmporium(BigDecimal value, DecimalFormat df){
		if(df == null) df = new DecimalFormat("#0.00");
		df.setRoundingMode(RoundingMode.DOWN);
		
		String str = df.format(value);
		str = str.replaceAll(",", ".");
		return Double.valueOf(str);
	}
	
	/**
	 * M�todo para verificar se uma determinada propriedade de uma classe � obrigat�rio, ou seja, se possui 
	 * a annotation Required.
	 * 
	 * @param clazz
	 * @param property
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static boolean isPropertyRequired(Class<?> clazz, String property){
		Method method = Util.beans.getGetterMethod(clazz, property);
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		return reflectionCache.isAnnotationPresent(method, Required.class);
	}
	
	/**
	 * Envia um script para o cliente.
	 * 
	 * @param script
	 * @author Fl�vio Tavares
	 */
	public static void printScript(String script){
		NeoWeb.getRequestContext().getServletResponse().setContentType("text/html");
		
		View.getCurrent()
			.println("<script>")
			.println(script)
			.println("</script>");
	}
	
	/**
	 * Faz um 'refresh' na p�gina atual, logo depois fecha a janela.
	 * 
	 * @see
	 * @param request
	 * @author Hugo Ferreira
	 */
	public static void recarregarPaginaWithClose(WebRequestContext request) {
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<html><script>window.opener.location.href = window.opener.location.href;window.close();</script></html>");
	}
	
	/**
	 * Faz um 'refresh' na p�gina atual.
	 * 
	 * @see
	 * @param request
	 * @author Hugo Ferreira
	 */
	public static void recarregarPaginaWithoutClose(WebRequestContext request) {
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.location.href = window.location.href;</script>");
	}
	
	/**
	 * Verifica se a string tem tamanho que o campo do relat�rio consiga
	 * suportar. Caso n�o tenha, insere quebras de linha.
	 * 
	 * @see
	 * @param linha
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String inserirQuebraLinha(String linha, int maxLength) {
		
		if (StringUtils.isBlank(linha)) {
			return linha;
		}
		
		if (linha.length() > maxLength) {
			String[] arrPalavras = linha.split(" ");
			StringBuilder linhaStBuilder = new StringBuilder();
			StringBuilder auxStBuilder = new StringBuilder();
			linha = "";
			
			for (String palavra : arrPalavras) {
				if (auxStBuilder.length() > maxLength) {
					auxStBuilder.append("\n");
					linhaStBuilder.append(auxStBuilder.toString());
					auxStBuilder.delete(0, auxStBuilder.length()); //limpa todo o stBuilder
				}
				auxStBuilder.append(palavra);
				auxStBuilder.append(" ");
			}
			
			linhaStBuilder.append(auxStBuilder.toString());
			linha = linhaStBuilder.toString();
			if (linha.endsWith("\n")) {
				linha = linha.substring(0, (linha.length()-1));
			}
		}
		
		return linha;
	}
	
	/**
	 * M�todo para pegar o whereIn que foi passado por par�metro.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static String getItensSelecionados(WebRequestContext request) {
		String selectedItens = request.getParameter("selectedItens");
		if (StringUtils.isBlank(selectedItens)) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return selectedItens;
	}
	
	/**
	 * M�todo para verificar se um objeto pode ser usado como filtro em query.<br>
	 * A verifica��o � feita da seguinte maneira:
	 * <pre>
	 * 	Se objeto != null e possui um Id != null 	= true
	 * 	Se objeto == null  							= false
	 * 	Se objeto != null e n�o possui um Id 		= false
	 * 	Se objeto != null e possui um Id null 		= false
	 * </pre>
	 * <b>Id: retorno de um m�todo anotado com a annotation javax.persistence.Id</b>
	 * 
	 * @param object
	 * @return true - Se o objeto pode ser usado como filtro.
	 * 			false - Caso contr�rio.
	 * @author Fl�vio Tavares
	 */
	public static boolean isObjectValid(Object object){
		if(object != null){
			return Util.beans.getId(object) != null;
		}
		return false;
	}
	
	public static void validaObjectWithExecption(Object object){
		if(!isObjectValid(object)) throw new SinedException("Par�metro inv�lido");
	}
	
	/**
	 * M�todo para formatar um objeto Money.
	 * N�meros negativos entre par�nteses.
	 * #,##0.00;(#,##0.00)
	 * 
	 * @param money
	 * @return
	 * @author Fl�vio Tavares
	 */
	public static String formatMoney(Money money){
		if(money == null) return "";
		return format.format(money.getValue());
	}
	
	/**
	 * M�todo que retornar string com o padr�o utilizado nos nomes dos relat�rios
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String datePatternForReport(){
		SimpleDateFormat simpleDateFormatTimestamp = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return simpleDateFormatTimestamp.format(new java.util.Date());
	}

	public static String datePatternForEmporium(){
		SimpleDateFormat simpleDateFormatTimestamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSSS");
		return simpleDateFormatTimestamp.format(new java.util.Date());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Set listToSet(List list, Class clazz) {
		if (list == null) {
			return new ListSet(clazz);
		}
		Set listSet = new ListSet(clazz);
		listSet.addAll(list);
		return listSet;
	}
	
	/**
	 * Fecha pop up's que utilizam o plugin do Jquery AkModal
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 */
	public static void fechaPopUp(WebRequestContext request) {
		if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>" +
				"parent.location.href.substring(0, (parent.location.href.indexOf('#')==-1 ? parent.location.href.length : parent.location.href.indexOf('#')));" +
				"var href = parent.location.href, indexOfHash = href.indexOf('#'); " +
				"parent.location.href = href.substring(0, (indexOfHash==-1 ? href.length : indexOfHash)); " +
				"parent.$.akModalRemove(true);" +
				"</script>");
	}
	
	/**
	 * Fecha pop up's que utilizam o plugin do Jquery AkModal sem recarregar a p�gina.
	 * 
	 * @param request
	 * @author Jo�o Vitor
	 */
	public static void fechaPopUpSemRecarregar(WebRequestContext request) {
		if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>" +
				"parent.$.akModalRemove(true);" +
				"</script>");
	}

	public static void redirecionamento(WebRequestContext request, String url){
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '" + request.getServletRequest().getContextPath() + url + "';</script>");
	}
	
	/**
	 * Preenche os campos de um Log.
	 * 
	 * @param log
	 * @author Fl�vio Tavares
	 */
	public static void updateLog(Log log){
		if(log != null){
			log.setCdusuarioaltera(getUsuarioLogado().getCdpessoa());
			log.setDtaltera(new Timestamp(System.currentTimeMillis()));
		}
	}
	
	/**
	 * Atualiza v�rias propriedades de uma lista.
	 * 
	 * @param list - Lista que ser� atualizada
	 * @param property - Propriedade a ser atualizada
	 * @param value - Novo valor da propriedade
	 * @author Fl�vio Tavares
	 */
	public static void setListPropertyValue(Collection<?> list, String property, Object value){
		if(property == null){
			throw new IllegalArgumentException("Par�metro property n�o deve ser nulo.");
		}
		
		if(isListNotEmpty(list)){
			for (Object object : list) {
				ExtendedBeanWrapper beanWrapper = new ExtendedBeanWrapper(object);
				beanWrapper.setPropertyValue(property, value);
			}
		}
		
	}
	
	
	/**
	 * M�todo que verifica se a ordem de compra precisa de aprova��o.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean needAprovacao(){
		Object param = NeoWeb.getRequestContext().getSession().getAttribute("NEED_APROVACAO_ORDEMCOMPRA");
		String valor;
		if(param == null){
			valor = ParametrogeralService.getInstance().getValorPorNome("Aprovar Ordem de Compra");
			NeoWeb.getRequestContext().getSession().setAttribute("NEED_APROVACAO_ORDEMCOMPRA", valor);
		} else {
			valor = param.toString();
		}
		return valor.trim().toUpperCase().equals("TRUE");
	}

	
	/**
	 * Muda a vers�o do Bean formato yyyyMMdd + contador
	 * 
	 * @param versao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Integer mudaVersaoBriefCase(Integer versao) {
		try{
			String version = versao + "";
			String versaoData = version.substring(0, 8);
			
			Integer count = Integer.valueOf(version.substring(8, version.length()));
			count++;
			
			if(count < 10){
				versaoData += "0" + count;
			} else {
				versaoData += count;
			}
			
			return Integer.valueOf(versaoData);
		} catch (Exception e) {
			return versao;
		}
	}

	/**
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static Integer criaVersaoBriefCase() {
		String versao = new SimpleDateFormat("yyyyMMdd").format(System.currentTimeMillis()) + "01";
		return Integer.valueOf(versao);
	}

	/**
	 * M�todo faz o disparo das a��es a serem tomadas no BriefCase
	 * 
	 * @param dominio
	 * @param acaoBriefCase
	 * @param codigo
	 * @author Tom�s Rabelo
	 */
	public static void executaBriefCase(Integer dominio, AcaoBriefCase acaoBriefCase, Integer codigo) {
		BriefCase.executar(dominio, acaoBriefCase, codigo);
		if(BriefCase.getErro() != null)
			NeoWeb.getRequestContext().addError(BriefCase.getErro());
	}

	/**
	 * M�todo que retorna logo do cabe�alho
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String getLogoEmpresaCabecalho(HttpServletRequest request) {
		
		String imagemStr = request.getContextPath() + LogoImagem.IMG_SINED;
		if(!isSined(request)){
			if(isBriefcase(request)){
				imagemStr = request.getContextPath() + LogoImagem.IMG_W3ERP_BRIEFCASE;
			} else {
				imagemStr = request.getContextPath() + LogoImagem.IMG_W3ERP;
			}
		}
		return imagemStr;
	}
	
	public static String getLogoEmpresaCabecalho() {
		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
		return getLogoEmpresaCabecalho(servletRequest);
	}
	
	public static boolean isSined() {
		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
		return isSined(servletRequest);
	}
	
	public static boolean isSined(HttpServletRequest request) {
		String url = request.getRequestURL().toString();
		
		String[] urlDividida = url.split("/");
		
		String servidor = urlDividida[2];
		
		return "www.sined.com.br".equals(servidor.toLowerCase());
	}
	
	
	/**
	 * 
	 *<p>M�todo respons�vel em receber uma string contendo o caminho de um dado e retornar<br>
	 *o id.
	 *
	 *
	 * @param values valores das requisi��es
	 * @return {@link Integer}id
	 * 
	 * @author Taidson
	 * @since 25/04/2010
     * Estrutura copia do Sindis
	 */

	public static Integer returnIdWithParameters(String ...values) {
		Pattern pattern = Pattern.compile("\\d");
		StringBuilder result = new StringBuilder();
		Matcher matcher = pattern.matcher(values[0]);
		while(matcher.find())
		result.append(matcher.group());
		return Integer.parseInt(result.toString());
	}

	/**
	 * Busca dentro da lista de telefones passadas por par�metro o(s) tipo(s) de telefone na ordem.
	 *
	 * @param listaTelefone
	 * @param tipos
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static Telefone getTelefoneTipo(Set<Telefone> listaTelefone, Telefonetipo... tipos) {
		for (int i = 0; i < tipos.length; i++) {
			for (Telefone tel : listaTelefone) {
				if(tel.getTelefonetipo() != null && 
						tel.getTelefonetipo().getCdtelefonetipo() != null &&
						tipos[i].getCdtelefonetipo() != null &&
						tipos[i].getCdtelefonetipo().equals(tel.getTelefonetipo().getCdtelefonetipo())){
					return tel;
				}
			}
		}
		return null;
	}
	
	/**
	 * Realiza o c�lculo do m�dulo 11
	 * 
	 * @param numero
	 * @param comportamento
	 * 	 
	 */
	public static long modulo11(String numero) {
		int indexador;
		double soma    = 0;
		double mult    = 0;
		double result  = 0;		
		int tamanho = numero.length();
		
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 8) + 2; //Sequ�ncia de 2 a 9
			try {
				soma = soma + (Long.parseLong(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = 11 - (soma % 11);
		
		return (long)result;
	}
	
	/**
	 * M�todo que retorna o conte�do de uma lista concatenado em uma string
	 * 
	 * @return
	 * @author Marden Silva
	 */
	public static String concatenate(Collection<?> collection, String token){
		StringBuilder builder = new StringBuilder();
		for (Iterator<?> iter = collection.iterator(); iter.hasNext();) {
			Object o = iter.next();
			builder.append(o);
			if(iter.hasNext()){
				builder.append(token);
			}
		}
		return builder.toString();
	}

	
	public static boolean haveGrupoMaterialUsuario() {
		List<Materialgrupo> listaMaterialgrupo = getListaUsuarioMaterialgrupo();
		return listaMaterialgrupo != null && listaMaterialgrupo.size() > 0;
	}

	@SuppressWarnings("unchecked")
	public static List<Materialgrupo> getListaUsuarioMaterialgrupo() {
		Object objParam = NeoWeb.getRequestContext().getSession().getAttribute("LISTA_MATERIALGRUPO_USUARIO");
		List<Materialgrupo> listaMaterialgrupo;
		if(objParam != null && objParam instanceof List<?>){
			listaMaterialgrupo = (List<Materialgrupo>)objParam;
		} else {
			listaMaterialgrupo = MaterialgrupoService.getInstance().findByUsuario(SinedUtil.getUsuarioLogado());
			NeoWeb.getRequestContext().getSession().setAttribute("LISTA_MATERIALGRUPO_USUARIO", listaMaterialgrupo);
		}
		return listaMaterialgrupo;
	}

	public static String getWhereInGrupoMaterialUsuario() {
		List<Materialgrupo> listaMaterialgrupo = getListaUsuarioMaterialgrupo();
		return CollectionsUtil.listAndConcatenate(listaMaterialgrupo, "cdmaterialgrupo", ",");
	}

	public static Double calculaPorcentagem(Money valor, Money valorTotal) {
		
		if(valor != null && valorTotal != null && valorTotal.getValue().doubleValue() > 0){
			double d1 = valor.getValue().doubleValue();
			double d2 = valorTotal.getValue().doubleValue();
			return (d1*100d)/d2;
		} else {
			return 0d;
		}
	}
	
	/**
	 * Coloca na sess�o, caso ainda n�o tenham sido colocados, todos os atalhos selecionados pelo usu�rio.
	 * 
	 * @param request
	 */
	public static void carregaAtalhosUsuario(WebRequestContext request) {
		if (request.getSession().getAttribute("LISTA_ATALHO") == null) {
			Usuario usuarioLogado = getUsuarioLogado();
			List<Tela> listaAtalho = TelaService.getInstance().findAtalhoByPessoa(usuarioLogado);
			List<Tela> listaAtalhoFiltrada = new ArrayList<Tela>();
			
			// S� adiciona na lista os atalhos das telas nas quais o usu�rio tem permiss�o de acesso.
			if (listaAtalho != null) {
				AuthorizationManager authorizationManager = Neo.getApplicationContext().getAuthorizationManager();
				for (Tela tela : listaAtalho) {
					if (authorizationManager.isAuthorized(tela.getPath(), null, usuarioLogado)) {
						listaAtalhoFiltrada.add(tela);
					}
				}
			}
			
			request.getSession().setAttribute("LISTA_ATALHO", listaAtalhoFiltrada);
		}		
	}	
	
	@SuppressWarnings("unchecked")
	public static boolean isAtalhoCadastrado(String path) {
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("LISTA_ATALHO");
		String context =  NeoWeb.getRequestContext().getServletRequest().getContextPath();
		path = path.replace(context, "");
		
		if (attribute != null) {
			List<Tela> listaAtalho = (List<Tela>) attribute;
			
			for (Tela tela : listaAtalho) {
				if (tela.getPath().equals(path)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo para buscar as colunas fixa. Se definido, retorna o array preenchido com a ordem sen�o retorna array vazio
	*
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> findCamposlistagemColunaFixa(){
		Tela tela = TelaService.getInstance().findColunaFixa(Util.web.getFirstUrl());
		ArrayList<Integer> ordem = new ArrayList<Integer>();
		
		if(tela != null && tela.getListaCampolistagem() != null && !tela.getListaCampolistagem().isEmpty()){						
			for(Campolistagem campolistagem : tela.getListaCampolistagem())									
				ordem.add(campolistagem.getOrdemjsp()-1);				
						
			return ordem;
		}		
		return ordem;
	}
	
	
	/**
	* M�todo que busca a ordem das colunas. Se definido, retorna o array com a ordem sen�o retorna array vazio
	*
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> findCampolistagemOrdem(){
					
		Tela tela = TelaService.getInstance().findForCampolistagemByPath(Util.web.getFirstUrl());
		ArrayList<Integer> ordem = new ArrayList<Integer>();
		
		if(tela != null && tela.getListaCampolistagem() != null && !tela.getListaCampolistagem().isEmpty()){
			Campolistagemusuario c = CampolistagemusuarioService.getInstance().findByTelaUsuario(tela, SinedUtil.getUsuarioLogado());
			if(c != null && c.getCamposordem() != null && !c.getCamposordem().equals("")){
				String[] ordemStr = c.getCamposordem().split(",");
				for(int i = 0; i < ordemStr.length; i++)						
					ordem.add(Integer.parseInt(ordemStr[i]));				
			}else{
				for(Campolistagem campolistagem : tela.getListaCampolistagem()){
					if(//campolistagem.getExibir() != null && campolistagem.getExibir() &&
							campolistagem.getOrdemjsp() != null)					
						ordem.add(campolistagem.getOrdemjsp()-1);					
				}
			}			
			return ordem;
		}		
		return ordem;
	}
	
	/**
	* M�todo que busca as colunas a serem exibidas. Se definido, retorna o array preenchido sen�o retorna array vazio
	*
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public static ArrayList<Integer> findCampolistagemNaoexibir(){
		
		Tela tela = TelaService.getInstance().findForCampolistagemByPath(Util.web.getFirstUrl(), Tipocoluna.COLUNA_MOVEL);
		ArrayList<Integer> ordem = new ArrayList<Integer>();
		ArrayList<Integer> ordemCorreta = new ArrayList<Integer>();
		ArrayList<Integer> aux_ordem = new ArrayList<Integer>();
		
		if(tela != null && tela.getListaCampolistagem() != null && !tela.getListaCampolistagem().isEmpty()){
			
			Campolistagemusuario c = CampolistagemusuarioService.getInstance().findByTelaUsuario(tela, SinedUtil.getUsuarioLogado());
			if(c != null && c.getCamposexibicao() != null && !c.getCamposexibicao().equals("")){
				String[] ordemStr = c.getCamposexibicao().split(",");
				for(int i = 0; i < ordemStr.length; i++){
					if(ordemStr[i].equals("false"))
						ordem.add(i);
				}
				
				aux_ordem = findCampolistagemOrdem();
				for(int i = 0; i < ordem.size(); i++){
					for(int j = 0; j < aux_ordem.size(); j++){
						if(ordem.get(i) == j){
							ordemCorreta.add(aux_ordem.get(j));
							break;
						}	
					}
				}
				return ordemCorreta;
				
			}else{
				for(Campolistagem campolistagem : tela.getListaCampolistagem()){				
					if((campolistagem.getExibir() == null || !campolistagem.getExibir()) && campolistagem.getOrdemexibicao() != null)					
						ordem.add(campolistagem.getOrdemjsp()-1);					
				}
			}
			return ordem;
		}		
		return ordem;
	}
	
	/**
	 * M�todo que verifica se existe campos da listagem cadastrados para a tela
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public static Boolean isCampolistagemCadastrado(){
		Tela tela = TelaService.getInstance().findTelaByPath(Util.web.getFirstUrl());
		
		if(tela != null && tela.getCdtela() != null)
			return CampolistagemService.getInstance().existeCamposCadastrados(tela);
		else
			return Boolean.FALSE;
	}
	
	/**
	* M�todo para saber a url da tela. Ex: '/modulo/crud/tela'
	*
	* @return 
	* @since Oct 26, 2011
	* @author Luiz Fernando F Silva
	*/
	public static String pathTela(){
		return Util.web.getFirstUrl();
	}
	
	/**
	* Retorna o Subdominio do cliente
	*
	*
	* @return
	* @since Nov 4, 2011
	* @author Luiz Fernando F Silva
	 */
	public static String getSubdominioCliente(){
		return getSubdominioCliente(NeoWeb.getRequestContext().getServletRequest());
	}
	
	/**
	* Retorna o Subdominio v�lido do cliente
	*
	*
	* @return
	* @since Mar 11, 2020
	* @author Diogo Souza
	 */
	public static String getSubdominioValidoCliente(){
		return getSubdominioValidoCliente(NeoWeb.getRequestContext().getServletRequest());
	}
	
	public static String getSubdominioClienteByThread(){
		String threadName = new String(Thread.currentThread().getName());
		String jndiByThreadName = getJndiByThreadName(threadName);
		return jndiByThreadName != null ? jndiByThreadName.split("_")[0] : null;
	}
	
	public static String getJndiByThreadName(String threadName) {
		if (threadName.startsWith("Job_Exportacao_Gerencial_")){
			return threadName.substring(25);
		} else if (threadName.startsWith("Job_Pedido_Venda_")){
			return threadName.substring(17);
		}else if (threadName.startsWith("Job_W3Producao_Imediato_")){
			return threadName.substring(24);
		}else if (threadName.startsWith("Job_W3Producao_")){
			return threadName.substring(15);
		}else if (threadName.startsWith("Job_Android_Imediato_")){
			return threadName.substring(21);
		}else if (threadName.startsWith("Job_Android_")){
			return threadName.substring(12);
		} else if (threadName.startsWith("Job_Sincronizacao_Pedido_Venda_")){
			return threadName.substring(31);
		} else if (threadName.startsWith("Job_Sincronizacao_Entrega_")){
			return threadName.substring(26);
		} else if (threadName.startsWith("Job_Reset_Cacheusuariooffline_")){
			return threadName.substring(30);
		} else if (threadName.startsWith("Job_Integracao_Ecom_Erro_")){
			return threadName.substring(25);
		} else if (threadName.startsWith("Job_Integracao_Ecom_")){
			return threadName.substring(20);
		} else if (threadName.startsWith("Job_Integracao_Emporium1_") || 
				threadName.startsWith("Job_Integracao_Emporium2_")){
			return threadName.substring(25);
		} else if (threadName.startsWith("Job_Atualizacao_Agendamentoservico_")){
			return threadName.substring(35);
		} else if (threadName.startsWith("Thread_Corrigir_Rateio_")){
			return threadName.substring(23);
		} else if (threadName.startsWith("Job_ArvoreAcesso_")){
			return threadName.substring(17);
		} else if(threadName.startsWith("Job_FlexDevelopers_Imediato_")){
			return threadName.substring(28);
		} else if(threadName.startsWith("Job_FlexDevelopers_")){
			return threadName.substring(19);
		} else if (threadName.startsWith("Job_Bridgestone_")){
			return threadName.substring(16);
		} else if (threadName.startsWith("Job_Dashboard_")){
			return threadName.substring(14);
		} else if (threadName.startsWith("Job_vetproxvacina_")){
			return threadName.substring(18);
		} else if (threadName.startsWith("Thread_Rateio_Custeio_")){
			return threadName.substring(22);
		} else if (threadName.equals("Job_Email_Cobranca")){
			if(SinedUtil.isAmbienteDesenvolvimento()){
				try {
					String dsDesenvolvimento = InitialContext.doLookup("PENDENCIA_FINANCEIRA_DATASOURCE");
					if(dsDesenvolvimento == null || dsDesenvolvimento.trim().equals("")){
						throw new DatasourceException("N�o foi encontrado o datasource para a pen�ncia financeira.");
					}
					return dsDesenvolvimento;
				} catch (NamingException e) {
					throw new DatasourceException("N�o foi encontrado o datasource para a pen�ncia financeira.");
				}
			} else {
				return "linkcom.w3erp.com.br_w3erp_PostgreSQLDS";
			}
		} else if(threadName.startsWith("Job_Arquivo_Conciliacao_Operadora_")){
			return threadName.substring(34);
		}else if (threadName.startsWith("Job_iteracao_veterinaria_")){
			return threadName.substring("Job_iteracao_veterinaria_".length());
		}/*else if (Thread.currentThread().getName().startsWith("Job_Aviso_Imediato_")){
			return Thread.currentThread().getName().substring("Job_Aviso_Imediato_".length());
		}*/else if (threadName.startsWith("Job_Aviso_")){
			return threadName.substring("Job_Aviso_".length());
		} else if (threadName.startsWith("Thread_IntegracaoTEFCallback_")){
			return threadName.substring(29);
		} else if (threadName.startsWith("Thread_Email_")){
			return threadName.substring(13);
		} else if (threadName.startsWith("Thread_Email_Aviso_")){
			return threadName.substring(19);
		}else if (Thread.currentThread().getName().startsWith("Job_Email_Envio_Boleto_Agendado_Imediato_")) {
			return Thread.currentThread().getName().substring("Job_Email_Envio_Boleto_Agendado_Imediato_".length());
		}else if (threadName.startsWith("Job_Email_Envio_Boleto_Agendado_")) {
			return threadName.substring("Job_Email_Envio_Boleto_Agendado_".length());
		}else if (Thread.currentThread().getName().startsWith("Thread_Tabela_IBPT_ECF_")){
			return threadName.substring(23);
		}else if (Thread.currentThread().getName().startsWith("Job_Nfse_")){
			return threadName.substring(9);
		}else if (Thread.currentThread().getName().startsWith("Job_consulta_eventos_correios_")){
			return threadName.substring("Job_consulta_eventos_correios_".length());
		}
		
		return null;
	}
	
	/**
	* Retorna o Subdominio do cliente
	*
	*
	* @return
	* @since Nov 4, 2011
	* @author Luiz Fernando F Silva
	 */
	public static String getSubdominioCliente(HttpServletRequest servletRequest){
		String url = servletRequest.getRequestURL().toString();
		
		String[] urlDividida = url.split("/");
		
		String servidor = urlDividida[2];
		
		urlDividida = servidor.split(".");
		if(urlDividida.length==0){
			urlDividida = servidor.split(":");
			if(urlDividida.length>0){				
				servidor = urlDividida[0];
			}
		}
		
		return servidor;
	}
	
	/**
	* Retorna Subdominio v�lido do cliente
	*
	*
	* @return
	* @since Mar 11, 2020
	* @author Diogo Souza
	 */
	public static String getSubdominioValidoCliente(HttpServletRequest servletRequest){
		String url = servletRequest.getRequestURL().toString();
		String[] urlDividida = url.split("/");
		String servidor = urlDividida[2];
		urlDividida = servidor.split("\\.");
	
		if(urlDividida.length>0){
			servidor = urlDividida[0];
		}
		return servidor;
	}


	
	/**
	 * 
	 * M�todo para concatenar uma lista e retornar uma String
	 *
	 *@author Thiago Augusto
	 *@date 27/02/2012
	 * @param collection
	 * @param property
	 * @param token
	 * @return
	 */
	public static String getListAndConcatenate(Collection<?> collection, String property1, String property2, String token){
		return getConcatenate(getListDistinctProperty(collection, property1, property2),token);
	}





	/**
	 * 
	 * Concatena todos os elementos de uma determinada collection e insere o token entre cada elemento
	 *
	 *@author Thiago Augusto
	 *@date 27/02/2012
	 * @param collection
	 * @param token
	 * @return
	 */
	public static String getConcatenate(Collection<?> collection, String token){
		StringBuilder builder = new StringBuilder();
		for (Iterator<?> iter = collection.iterator(); iter.hasNext();) {
			if(iter != null){
				Object o = iter.next();
				builder.append(o);
				if(iter.hasNext()){
					builder.append(token);
				}
			}
		}
		return builder.toString();
	}
	
	/**
	 * Cria um lista de uma propriedade distinta a partir de uma collection.
	 * 
	 * @param collection
	 * @param property
	 * @return
	 * @author Rodrigo Freitas
	 */
	private static List<?> getListDistinctProperty(Collection<?> collection, String property1, String property2){
		List<Object> list = new ArrayList<Object>();
		Iterator<?> iter = collection.iterator();
		if(iter.hasNext()){
			Object next = iter.next();
			ExtendedBeanWrapper beanWrapper = new ExtendedBeanWrapper(next);
			
			Object propertyValue1 = beanWrapper.getPropertyValue(property1);
			Object propertyValue2 = beanWrapper.getPropertyValue(property2);

			if (propertyValue1 != null && !list.contains(propertyValue1)) {
				list.add(propertyValue1 + " / " + propertyValue2);
			}

			while(iter.hasNext()){
				try {
					beanWrapper.setWrappedInstance(iter.next());
					propertyValue1 = beanWrapper.getPropertyValue(property1);
					propertyValue2 = beanWrapper.getPropertyValue(property2);
					if (propertyValue1 != null && !list.contains(propertyValue1)) {
						list.add(propertyValue1 + " / " + propertyValue2);
					} 
					
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return list;
	}
	
	public static String listAndConcatenate(Object[] array, String property, String token){
		List<Object> list = new ArrayList<Object>();
		if(array != null && array.length > 0){
			for(Object obj : array) list.add(obj);
		}
		return CollectionsUtil.concatenate(getListDistinctProperty(list, property),token);
	}
	
	/**
	 * 
	 * M�todo que carrega o usu�rio com o n�vel de permiss�o e retorna uma string com os ids de papel.
	 *
	 *@author Thiago Augusto
	 *@date 21/03/2012
	 * @return
	 */
	public static String getWhereInCdPapel(){
		return SinedUtil.getWhereInCdPapel(getUsuarioLogado());
	}
	
	public static String getWhereInCdPapel(Usuario bean){
		Usuario usuario = UsuarioService.getInstance().getUsuarioComPapel(bean);
		String whereIn = listAndConcatenate(usuario.getListaUsuariopapel(), "papel.cdpapel", ",");
		return whereIn;
	}
	
	/**
	 * 
	 * M�todo que retorna o valor do desconto � partir de uma porcentagem
	 *
	 * @name calculaValorPorcentagem
	 * @param valor
	 * @param porcentagem
	 * @return
	 * @return Money
	 * @author Thiago Augusto
	 * @date 10/04/2012
	 *
	 */
	public static String calculaValorPorcentagem(Money valor, Money porcentagem){
		Money retorno = new Money();
		retorno = valor.multiply(porcentagem);
		retorno = retorno.divide(new Money(100));
		return retorno.toString().replace(".", "").replace(",", "");
	}

	/**
	 * Retorna a string de conex�o com o banco;
	 *
	 * @return
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public static String getStringConexaoBanco() {
		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
		String url = servletRequest.getRequestURL().toString();
		
		String[] urlDividida = url.split("/");
		
		String servidor = urlDividida[2];
		String contexto = urlDividida[3];
		
		urlDividida = servidor.split(".");
		if(urlDividida.length==0){
			urlDividida = servidor.split(":");
			if(urlDividida.length>0){				
				servidor = urlDividida[0];
			}
		}
		
		NeoWeb.getRequestContext().getSession().setAttribute("STRING_CONEXAO_BANCO", "java:/"+servidor+"_"+contexto+"_PostgreSQLDS");
		return servidor+"_"+contexto+"_PostgreSQLDS";
	}
	
	public static String getStringConexaoBancoIntegracao() {
		try {
			return getStringConexaoBanco();
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * Retorna a ultima altera��o nas tabelas as quais s�o feitas cache offline.
	 * @author igorsilveriocosta
	 * @return
	 */
	public static Long getUltimaAtualizacaoEntidadeOffline() {
		StringBuilder sb = new StringBuilder()
		.append("select max(A.dtaltera) from (")
		.append(" select max(dtaltera) as dtaltera from pedidovendatipo UNION ")
		.append(" select max(dtaltera) as dtaltera from colaborador UNION ")
		.append(" select max(dtaltera) as dtaltera from empresa UNION ")
		.append(" select max(dtaltera) as dtaltera from cliente UNION ")
		.append(" select max(dtaltera) as dtaltera from endereco UNION ")
		.append(" select max(dtaltera) as dtaltera from prazopagamento UNION ")
		.append(" select max(dtaltera) as dtaltera from prazopagamentoitem UNION ")
		.append(" select max(dtaltera) as dtaltera from indicecorrecao UNION ")
		.append(" select max(dtaltera) as dtaltera from conta UNION ")
		.append(" select max(dtaltera) as dtaltera from documentotipo UNION ")
		.append(" select max(dtaltera) as dtaltera from material UNION ")
		.append(" select max(dtaltera) as dtaltera from unidademedida UNION ")
		.append(" select max(dtaltera) as dtaltera from uf UNION ")
		.append(" select max(dtaltera) as dtaltera from municipio ) as A");
		return ((Timestamp) Neo.getObject(GenericDAO.class).getJdbcTemplate().queryForObject(sb.toString(), Timestamp.class)).getTime();
	}

	/**
	 * M�todo que coloca os escapes para gera��o do CSV
	 *
	 * @param string
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public static String escapeCsv(String string) {
		if(string == null) return null;	
		return string.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll(";", " ");
	}
	
	/**
	 * Verifica se o sistema est� em ambiente de desenvolvimento.
	 *
	 * @return
	 * @since 28/06/2012
	 * @author Rodrigo Freitas
	 */
	public static boolean isAmbienteDesenvolvimento(){
		try{
			String ambiente = InitialContext.doLookup("AMBIENTE_DESENVOLVIMENTO_W3ERP");
			if(ambiente != null && "TRUE".equals(ambiente.toUpperCase())){
				return true;
			}
		} catch (Exception e) {}
		return false;
	}
	
	public static String getUrlIntegracaoWms(Empresa empresa){
		String urlParam = EmpresaService.getInstance().getUrlIntegracaoWms(empresa);
		if(SinedUtil.isAmbienteDesenvolvimento()){
			try{
				String urlCustomParam = InitialContext.doLookup("URL_INTEGRACAO_WMS_AMB_DES");
				if(urlCustomParam != null && !urlCustomParam.isEmpty()){
					return urlCustomParam;
				}
			} catch (Exception e) {
			} finally{
				urlParam = "wmsexemplo.wmsweb.com.br/wmsexemplo";
			}
		}
		return urlParam;
	}
	
	/**
	 * M�todo que monta o ID para o campo hidden do autocomplete. 
	 *
	 * @param value
	 * @param autocompleteLabelProperty
	 * @return
	 * @since 26/06/2012
	 * @author Rodrigo Freitas
	 */
	public static String getIdAutocomplete(Object value, String autocompleteLabelProperty) {
		try{
			Class<?> clazz = value.getClass();
	
			String id = clazz.getName();
			id += "[";
	
			BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, clazz);
			id += beanDescriptor.getIdPropertyName() + "=" + Util.beans.getGetterMethod(clazz, beanDescriptor.getIdPropertyName()).invoke(value);
	
			id += ",";
			
			if(autocompleteLabelProperty != null && !autocompleteLabelProperty.equals("")){
				String[] array = autocompleteLabelProperty.split(",");
				String campo;
				Object invoke;
				for (int i = 0; i < array.length; i++) {
					campo = array[i].substring(array[i].lastIndexOf(".")+1, array[i].length());
					invoke = Util.beans.getGetterMethod(clazz, campo).invoke(value);
					if(invoke != null){
						id += campo + "=";
						id += Util.strings.addScapesToDescription(invoke.toString());
						id += ",";
					}
				}
		
				id = id.substring(0, id.length()-1);
			} else {
				id += beanDescriptor.getDescriptionPropertyName() + "=" + Util.strings.addScapesToDescription(Util.beans.getGetterMethod(clazz, beanDescriptor.getDescriptionPropertyName()).invoke(value).toString());
			}
			
			id += "]";
			return id;
		} catch (Exception e) {
			return "<null>";
		}
	}
	
	/**
	 * Cria o XML de erro com a mensagem passada por par�metro.
	 *
	 * @param message
	 * @return
	 * @throws IOException
	 * @since 07/08/2012
	 * @author Rodrigo Freitas
	 */
	public static String createXmlErro(String message) throws IOException {
		Element resposta = new Element("Resposta");
		
		Element status = new Element("Status");
		status.setText("0");
		
		Element erro = new Element("Erro");
		erro.setText(message);
		
		resposta.addContent(status);
		resposta.addContent(erro);
		
		XMLOutputter xout = new XMLOutputter();  
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		xout.output(resposta, byteArrayOutputStream);
		
		return new String(byteArrayOutputStream.toByteArray());
	}
	
	/**
	 * Cria o XML de erro com o stacktrace junto.
	 *
	 * @param e
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 * @throws IOException 
	 */
	public static String createXmlErroWithStacktrace(Exception e) throws IOException {
		Element resposta = new Element("Resposta");
		
		Element status = new Element("Status");
		status.setText("0");
		
		Element erro = new Element("Erro");
		erro.setText(Util.strings.tiraAcento(e.getMessage()));
		
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		
		Element stacktrace = new Element("Stacktrace");
		stacktrace.setText(stringWriter.toString());
		
		resposta.addContent(status);
		resposta.addContent(erro);
		resposta.addContent(stacktrace);
		
		XMLOutputter xout = new XMLOutputter();  
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		xout.output(resposta, byteArrayOutputStream);
		
		return new String(byteArrayOutputStream.toByteArray());
	}
	
	/**
	 * Cria o XML de sucesso da comunica��o entre sistemas.
	 *
	 * @return
	 * @throws IOException
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	public static String createXmlSucesso() throws IOException {
		Element resposta = new Element("Resposta");
		
		Element status = new Element("Status");
		status.setText("1");
		resposta.addContent(status);
		
		XMLOutputter xout = new XMLOutputter();  
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		xout.output(resposta, byteArrayOutputStream);
		
		return new String(byteArrayOutputStream.toByteArray());
	}
	
	/**
	 * Cria a resposta com o XML
	 *
	 * @param resposta
	 * @return
	 * @throws IOException
	 * @since 07/08/2012
	 * @author Rodrigo Freitas
	 */
	public static String getRespostaXML(Element resposta) throws IOException {
		XMLOutputter xout = new XMLOutputter();  
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		
		Format newFormat = Format.getCompactFormat();
		newFormat.setEncoding("LATIN1");
		xout.setFormat(newFormat);
		
		xout.output(new Document(resposta), byteArrayOutputStream);
		
		return new String(byteArrayOutputStream.toByteArray());
	}
	
	/**
	 * 
	 * M�todo trunca sem arredondar
	 *
	 * @name roundFloor
	 * @param value
	 * @param scale
	 * @return
	 * @return BigDecimal
	 * @author Thiago Augusto
	 * @date 30/08/2012
	 *
	 */
	public static BigDecimal roundFloor(BigDecimal value, int scale){
		if(value == null) 
			throw new NullPointerException("O par�metro value n�o pode ser null.");
				
		return value.setScale(scale, BigDecimal.ROUND_FLOOR);
	}
	
	public static BigDecimal roundUp(BigDecimal value, int scale){
		if(value == null) 
			throw new NullPointerException("O par�metro value n�o pode ser null.");
				
		return value.setScale(scale, BigDecimal.ROUND_UP);
	}
	
	/**
	 * Cria o link para uma conta a receber.
	 *
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public static String makeLinkContareceber(Documento documento){
		return "<a href=\"" + 
					NeoWeb.getRequestContext().getServletRequest().getContextPath() + 
					"/financeiro/crud/Contareceber?ACAO=consultar&cddocumento=" + 
					documento.getCddocumento() + 
					"\">" + 
					documento.getCddocumento() + 
					"</a>";
	}
	
	/**
	 * Cria o link para uma venda.
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public static String makeLinkVenda(Venda venda){
		String link = "";
		
		if(venda != null && venda.getCdvenda() != null){
			try {
				link = "<a href=\"" + 
						NeoWeb.getRequestContext().getServletRequest().getContextPath() + 
						"/faturamento/crud/Venda?ACAO=consultar&cdvenda=" + 
						venda.getCdvenda() + 
						"\">" + 
						venda.getCdvenda() + 
						"</a>";
			} catch (Exception e) {
				return "<a href=\"javascript:visualizarVenda(" + venda.getCdvenda() + ");\">" + venda.getCdvenda() + "</a>";
			}
		}
		return link;
	}
	
	/**
	 * Cria o link para uma Nota.
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public static String makeLinkNota(Nota nota){
		String numeroNota = nota.getNumero();
		if(numeroNota == null){
			numeroNota = nota.getCdNota() + "";
		}
		
		String path = "NotaFiscalServico";
		if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
			path = "Notafiscalproduto";
		}
		
		return "<a href=\"" + 
					NeoWeb.getRequestContext().getServletRequest().getContextPath() + 
					"/faturamento/crud/" + path + "?ACAO=consultar&cdNota=" + 
					nota.getCdNota() + 
					"\">" + 
					numeroNota + 
					"</a>";
	}
	
	/**
	 * Retorna a format��o de acordo com a quantidade de casas decimais. (At� 4 casas decimais)
	 *
	 * @param valor
	 * @return
	 * @author Luiz Fernando
	 */
	public static String getFormatacaoCasasdecimais(Double valor) {
		String formatacao = "#,##0.00";
		if(valor != null){
			String str = valor.toString();
			String casasdecimais = str.substring(str.indexOf(".")+1);
			if(casasdecimais.length() == 3){
				formatacao = "#,###0.000";
			}else if(casasdecimais.length() > 3){
				formatacao = "#,####0.0000";
			}
		}
		return formatacao;
	}

	public static String formataTamanho(String valor, int tamanho, char complemento, boolean alinharComplementoDireita){		
		return formataTamanho(valor, tamanho, complemento, alinharComplementoDireita, null);
	}
	
	public static String formataTamanho(String valor, int tamanho, char complemento, boolean alinharComplementoDireita, String replaceAllRegexSefip){		
		
		if (valor==null){
			valor = "";
		}
		
		if (replaceAllRegexSefip!=null){
			valor = valor.replaceAll(replaceAllRegexSefip, "");
			valor = valor.replace("     ", " ");
			valor = valor.replace("    ", " ");
			valor = valor.replace("   ", " ");
			valor = valor.replace("  ", " ");
			valor = valor.trim();
		}
		
		if (tamanho>0 && valor.length()>=tamanho){
			return valor.substring(0,tamanho);
		}			
		
		while (valor.length()<tamanho){
			if (alinharComplementoDireita){
				valor += complemento;
			}else {
				valor = complemento+valor;
			}				
		}
		
		return valor;
	}	
	
	public static String imprimeListaRegistroDIRF(List<? extends Object> lista){
		if(lista != null && lista.size() > 0){
			StringBuilder sb = new StringBuilder();
			for (Object object : lista) {
				sb.append(object.toString());
			}
			return sb.toString();
		} else return "";
	}
	
	public static String imprimeRegistroDIRF(Object reg){
		return reg != null ? reg.toString() : "";
	}
	
	public static String imprimeLongDIRF(Long longObj){
		return longObj != null ? longObj.toString() : "";
	}
	
	public static String imprimeDataDIRF(Date data){
		return data != null ? SinedDateUtils.toString(data, "yyyyMMdd") : "";
	}
	
	public static String getElementString(Element element){
		if(element == null) return null;
		else {
			if(element.getText() != null && !element.getText().equals("")){
				try{
					return new String(element.getText().getBytes("LATIN1"));
				} catch (Exception e) {
					e.printStackTrace();
					return element.getText();
				}
			} else return null;
		}
	}
	
	public static Double getElementDouble(Element element){
		if(element == null) return null;
		else {
			String str = element.getText();
			if(str == null || str.trim().equals("")) return null;
			return Double.parseDouble(str);
		}
	}
	
	public static Integer getElementInteger(Element element){
		if(element == null) return null;
		else {
			String str = element.getText();
			if(str == null || str.trim().equals("")) return null;
			return Integer.parseInt(str);
		}
	}
	
	public static Timestamp getElementTimestamp(Element element){
		if(element == null) return null;
		else {
			try{
				String str = element.getText();
				if(str == null || str.trim().equals("")) return null;
				return new Timestamp(FORMATADOR_DATA_HORA.parse(str).getTime());
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	public static Date getElementDate(Element element){
		if(element == null) return null;
		else {
			try{
				String str = element.getText();
				if(str == null || str.trim().equals("")) return null;
				return new Date(FORMATADOR_DATA.parse(str).getTime());
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
		
	
	public static List<Integer> getListaByIds(String ids){
		
		List<Integer> lista = new ArrayList<Integer>();
		
		if (ids!=null){
			ids = ids.replace(" ", "");
			String[] idsSplit = ids.split("\\,");
			for (String id: idsSplit){
				if (NumberUtils.isNumber(id)){
					lista.add(Integer.valueOf(id));
				}
			}
		}
		
		return lista;
	}
	
	public static String getContexto() {
		return NeoWeb.getRequestContext().getServletRequest().getContextPath().replaceAll("/", "");
	}
	
	public static Double roundByUnidademedida(Double dbl, Unidademedida um){
		try {
			if(um != null && um.getCasasdecimaisestoque() != null){
				return SinedUtil.round(dbl, um.getCasasdecimaisestoque());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl;
	}
	
	public static Double roundByUnidademedidaRoundFloor(Double dbl, Unidademedida um){
		try {
			BigDecimal decimal = new BigDecimal(dbl);
			
			if(um != null && um.getCasasdecimaisestoque() != null){
				decimal = decimal.setScale(um.getCasasdecimaisestoque(), BigDecimal.ROUND_FLOOR);
				dbl = decimal.doubleValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl;
	}
	
	public static Integer getParametroCasasDecimaisArredondamento(){
		try {
			String param_decimais = null;
			if(NeoWeb.isInRequestContext()){
				Object paramObj = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
				if(paramObj != null){
					param_decimais = paramObj.toString();
				}
			} 
			if(param_decimais == null || param_decimais.equals("")){
				param_decimais = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			}
			
			if(param_decimais != null && !param_decimais.equals("")){
				if(NeoWeb.isInRequestContext()){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, param_decimais);
				}
				
				
				return Integer.parseInt(param_decimais);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Double roundByParametro(Double dbl){
		try {
			String param_decimais = null;
			if(NeoWeb.isInRequestContext()){
				Object paramObj = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
				if(paramObj != null){
					param_decimais = paramObj.toString();
				}
			} 
			if(param_decimais == null || param_decimais.equals("")){
				param_decimais = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			}
			
			if(param_decimais != null && !param_decimais.equals("")){
				if(NeoWeb.isInRequestContext()){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, param_decimais);
				}
				
				
				Integer numero_casas_decimais = Integer.parseInt(param_decimais);
				return SinedUtil.round(dbl, numero_casas_decimais);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl;
	}
	
	public static Double roundByParametro(Double dbl, Integer numero_casas_decimais){
		try {
			String param_decimais = null;
			if(NeoWeb.isInRequestContext()){
				Object paramObj = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
				if(paramObj != null){
					param_decimais = paramObj.toString();
				}
			} 
			if(param_decimais == null || param_decimais.equals("")){
				param_decimais = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			}
			
			if(param_decimais != null && !param_decimais.equals("")){
				if(NeoWeb.isInRequestContext()){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, param_decimais);
				}
				
				numero_casas_decimais = Integer.parseInt(param_decimais);
				return SinedUtil.round(dbl, numero_casas_decimais);
			}else {
				return SinedUtil.round(dbl, numero_casas_decimais);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl;
	}
	
	public static BigDecimal roundByParametro(BigDecimal dbl){
		try {
			String param_decimais = null;
			if(NeoWeb.isInRequestContext()){
				Object paramObj = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
				if(paramObj != null){
					param_decimais = paramObj.toString();
				}
			} 
			if(param_decimais == null || param_decimais.equals("")){
				param_decimais = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			}
			
			if(param_decimais != null && !param_decimais.equals("")){
				if(NeoWeb.isInRequestContext()){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, param_decimais);
				}
				
				
				Integer numero_casas_decimais = Integer.parseInt(param_decimais);
				return SinedUtil.round(dbl, numero_casas_decimais);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl;
	}
	
	public static Double roundByParametroRelatorio(Double dbl){
		return roundByParametro(dbl, 2);
	}
	
	public static Element getRootElementXML(byte[] content) throws JDOMException, IOException, UnsupportedEncodingException {
		SAXBuilder sb = new SAXBuilder();
		
		Document d = null;
		try{
			d = sb.build(new ByteArrayInputStream(content));
		} catch (Exception e) {
			String str = new String(content);
			d = sb.build(new ByteArrayInputStream(str.getBytes("UTF-8")));
		}
		Element rootElement = d.getRootElement();
		return rootElement;
	}
	
	public static TipoCriacaoPedidovenda getTipoCriacaoPedidovenda(Integer tipocriacao) {
		if(tipocriacao == null){
			tipocriacao = 1;
		}
		
		TipoCriacaoPedidovenda tipoCriacaoPedidovenda = TipoCriacaoPedidovenda.values()[tipocriacao - 1];
		return tipoCriacaoPedidovenda;
	}
	
	public static Boolean isRestricaoClienteVendedor(Usuario usuario){
		Boolean restricaoClienteVendedor = UsuarioService.getInstance().isRestricaoClienteVendedor(usuario);
		return restricaoClienteVendedor;
	}
	
	/**
	* M�todo que retorna um objeto Money com valor igual a zero se o valor passado for nulo
	*
	* @param valor
	* @author Thiago Clemente
	*
	*/
	public static Money zeroIfNull(Money valor){
		return valor!=null ? valor : new Money(0);
	}
	
	/**
	 * M�todo que retorna um objeto Double com valor igual a zero se o valor passado for nulo
	 *
	 * @param valor
	 * @author Thiago Clemente
	 *
	 */
	public static Double zeroIfNull(Double valor){
		return valor!=null ? valor : 0D;
	}
	
	public static String criarUrlsPublicas(String html) {
		
		if (html == null || html.isEmpty())
			return null;
		
		Pattern pattern = Pattern.compile("(/DOWNLOADFILE/[0-9]+)");
		Matcher matcher = pattern.matcher(html);
		StringBuffer sb = new StringBuffer(html.length());
		DateFormat df = new SimpleDateFormat("'_'yyyyMMddHHmmssSSS");
		
		String servidor;
		String contexto;
		RequestContext requestContext = Neo.getRequestContext();
		if (requestContext instanceof WebRequestContext){
			HttpServletRequest servletRequest = ((WebRequestContext) requestContext).getServletRequest();
			
			contexto = servletRequest.getContextPath();
			String urlServidor = servletRequest.getRequestURL().toString();
			servidor = StringUtils.substringBefore(urlServidor, contexto);
		}else{
			throw new SinedException("Incapaz de determinar o nome do servidor.");
		}
		
		while (matcher.find()) {
			String url = matcher.group(1);
			String novaUrl = url.replace("/DOWNLOADFILE/", "/DOWNLOADPUBFILE/").concat(df.format(new java.util.Date()));
			DownloadPublicoServlet.addUrl(servidor + contexto + novaUrl);
			
			matcher.appendReplacement(sb, novaUrl);
		}
		matcher.appendTail(sb);
		
		return sb.toString();
	}
	
	public static BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight, 
			int bufferedType, Object hint, boolean higherQuality, boolean keepProportion){
		BufferedImage ret = (BufferedImage)img;
		int w, h;
		if (higherQuality) {
			w = img.getWidth(null);
			h = img.getHeight(null);
		} else {
			w = targetWidth;
			h = targetHeight;
		}
		
		do {
			if (higherQuality && (w > targetWidth || h > targetHeight)) {
				w /= 2;
				h /= 2;
				if (!keepProportion) {
					if( w < targetWidth){
						w = targetWidth;
					}
					if (h < targetHeight) {
						h = targetHeight;
					}
				}
			}
		
			BufferedImage tmp = new BufferedImage(w, h, bufferedType);
			Graphics2D g2d = tmp.createGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			if(hint != null){
				g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			}
			g2d.drawImage(ret, 0, 0, w, h, null);
			g2d.dispose();
		
			ret = tmp;
		} while (w > targetWidth || h > targetHeight);
		
		return ret;
	}
	
	public static Image removerCanalAlpha(final BufferedImage im, final Color color){
		final ImageFilter filter = new RGBImageFilter(){
	        public final int filterRGB(final int x, final int y, final int rgb) {
        		if(rgb == 0x00000000){
        			return 0xFF000000 + color.getRGB();
        		} else{
        			return rgb;
        		}
	        }
		};

		final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
	    return Toolkit.getDefaultToolkit().createImage(ip);
    }
	
	public static Map<Integer, Integer> generateImagensByMaterialtipo(Map<Integer, List<Arquivo>> mapaArquivos, Integer defaultWidth, Integer defaultHeight) throws Exception{
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		ArquivoService arquivoService = ArquivoService.getInstance();
		ArquivoDAO arquivoDAO = ArquivoDAO.getInstance();
		
		if(mapaArquivos != null){
			for(Integer cdmaterialtipo : mapaArquivos.keySet()){
				List<Arquivo> listaArquivos = mapaArquivos.get(cdmaterialtipo);
				Integer maxColumns = listaArquivos.size() > 10 ? 10 : listaArquivos.size();
				Integer maxRows = listaArquivos.size()/maxColumns > 3 ? 3 : (int) Math.ceil(listaArquivos.size()/(double)maxColumns);
				Integer totalWidth = maxColumns*defaultWidth;
				Integer totalHeight = maxRows*defaultHeight;
				BufferedImage img = new BufferedImage(totalWidth, totalHeight, BufferedImage.TYPE_INT_ARGB);
				BufferedImage newImg;
				for(int row=0; row<maxRows; row++){
					for(int column=0; column<maxColumns; column++){
						if(listaArquivos.size() > (row*10)+column){
							Image imgOri = arquivoService.loadAsImage(listaArquivos.get((row*10)+column));
							newImg = new BufferedImage(imgOri.getWidth(null), imgOri.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		                    Graphics2D g2d = (Graphics2D)newImg.createGraphics();
		                    g2d.drawImage(imgOri, 0, 0, null);
		                    g2d.dispose();
							
							newImg = SinedUtil.getScaledInstance(newImg, 
									defaultWidth, defaultHeight, BufferedImage.TYPE_INT_ARGB, 
									RenderingHints.VALUE_INTERPOLATION_BICUBIC, Boolean.TRUE, Boolean.FALSE);
							
							g2d = img.createGraphics();
							g2d.drawImage(SinedUtil.removerCanalAlpha(newImg, Color.white), column*defaultWidth, row*defaultHeight, defaultWidth, defaultHeight, null);
						} else{
							break;
						}
					}
				}
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
			    ImageIO.write(img, "png", baos);
			    byte[] bytes= baos.toByteArray();
			    baos.close();
			    Arquivo arquivo = new Arquivo(bytes, "Legenda.png", "image/png");
			    
			    Materialtipo materialtipo = new Materialtipo();
			    materialtipo.setCdmaterialtipo(cdmaterialtipo);
			    materialtipo.setArquivoLegenda(arquivo);
			    
			    arquivoDAO.saveFile(materialtipo, "arquivoLegenda");
			    mapa.put(cdmaterialtipo, materialtipo.getArquivoLegenda().getCdarquivo().intValue());
			}
			MaterialtipoService.getInstance().bulkUpdateCdarquivolegenda(mapa);
		}
		return mapa;
	}

	public static String getFuncaoTiraacento() {
		return Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
	}
	
	public static String getNumeroNotaFormatado(Integer numeroNota){
		
		if (numeroNota == null)
			return null;
		else
			return getNumeroNotaFormatado(numeroNota.toString());
	}
		
	public static String getNumeroNotaFormatado(String numeroNota){
		
		if (numeroNota == null)
			return null;
		
		Integer digitos = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.DIGITOS_NUMERO_NOTAFISCAL);
		if (digitos != null){
			return StringUtils.leftPad(numeroNota, digitos, "0");
		} else {
			return numeroNota;
		}
	}

	/**
	* M�todo que seta o cookie de controle de duplo clique
	*
	* @param request
	* @since 25/06/2014
	* @author Luiz Fernando
	*/
	public static void setCookieBlockButton(WebRequestContext request) {
		if(request != null){
			request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		}
		
	}
	
	/**
	* M�todo que divide o whereIn de acordo com a quantidade passada por par�metro
	*
	* @param whereIn
	* @param qtde
	* @return
	* @since 06/08/2014
	* @author Luiz Fernando
	*/
	public static List<String> getListaWhereIn(String whereIn, Integer qtde) {
		List<String> listaWhereIn = new ArrayList<String>();
		if(StringUtils.isNotEmpty(whereIn)){
			String[] ids = whereIn.split(",");
			if(qtde == null) qtde = 30;
			
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < ids.length; i++) {
				if(i != 0 && i % qtde == 0){
					listaWhereIn.add(sb.toString());
					sb = new StringBuilder();
				}
				if(sb.length() > 0) sb.append(",");
				sb.append(ids[i]);
			}
			listaWhereIn.add(sb.toString());
		}
		return listaWhereIn;
	}
	

	public static User verificaUsuarioAndroid(String token) throws NotAuthenticatedException{
		NotAuthenticatedException tokenInvalido = new NotAuthenticatedException("Token inv�lido");
		//token = "1|g8mmVLeETdJVyrLqmNuxSqib8J159ytKDqPFRU7+9HMsbtAdpYts03a7KkNu46YUL5V83fSlNilPstYths2P9w==||";
		String tokens[] = token.split("\\|\\|\\|");

//        String[] tokens = token.split("\\|");
//        System.out.println(tokens.length);
//        System.out.println(tokens[0]);
		if(tokens==null || tokens.length!=2) 
			throw tokenInvalido;
		
		Integer cdpessoa = Integer.parseInt(tokens[0]);
		
		UsuarioService usuarioService = Neo.getObject(UsuarioService.class);
		if(StringUtils.isNotBlank(tokens[1])){
			Usuario usuarioLogado;
			try {
				usuarioLogado = usuarioService.loadUsuarioByHashAndroid(cdpessoa, tokens[1]);
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
			if(usuarioLogado!=null)
				return usuarioLogado;
			throw tokenInvalido;
		}else{
			throw new NotAuthenticatedException("Token n�o fornecido");
		}
		
	}
	
	public static Usuario retornaUsuarioAndroid() throws NotAuthenticatedException{
		try {
			String token = NeoWeb.getRequestContext().getServletRequest().getHeader("sec-token"); 
			
			NotAuthenticatedException tokenInvalido = new NotAuthenticatedException("Token inv�lido");
			String tokens[] = token.split("\\|\\|\\|");
			if(tokens==null || tokens.length!=2) 
				throw tokenInvalido;
			
			Integer cdpessoa = Integer.parseInt(tokens[0]);
			
			UsuarioService usuarioService = Neo.getObject(UsuarioService.class);
			if(StringUtils.isNotBlank(tokens[1])){
				Usuario usuarioLogado;
				try {
					usuarioLogado = usuarioService.loadUsuarioByHashAndroid(cdpessoa, tokens[1]);
				} catch (NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				}
				if(usuarioLogado!=null)
					return usuarioLogado;
				throw tokenInvalido;
			}else{
				throw new NotAuthenticatedException("Token n�o fornecido");
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	* M�todo que retorna o link para incluir no hist�rico
	*
	* @param whereIn
	* @param nameFunction
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public static String makeLinkHistorico(String whereIn, String nameFunction) {
		StringBuilder link = new StringBuilder();
		
		if(StringUtils.isNotEmpty(whereIn)){
			for(String id : whereIn.split(",")){
				link.append(makeLinkHistorico(id, id, nameFunction) + ", ");
			}
		}
		return link.length() > 0 ? link.substring(0, link.length()-2) : "";
	}
	
	/**
	* M�todo que retorna o link para incluir no hist�rico
	*
	* @param id
	* @param identificador
	* @param nameFunction
	* @return
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public static String makeLinkHistorico(String id, String identificador, String nameFunction) {
		return "<a href=\"javascript:"+nameFunction+"(" + id +")\">" + identificador + "</a>";
	}
	
	public static String makeLinkHtml(String whereIn, String url, String atributos) {
		StringBuilder link = new StringBuilder();
		
		if(StringUtils.isNotEmpty(whereIn)){
			for(String id : whereIn.split(",")){
				link.append("<a " + 
						(StringUtils.isNotBlank(atributos) ? "style=\"" + atributos + "\"": "") + 
						" href=\"" + 
						NeoWeb.getRequestContext().getServletRequest().getContextPath() + 
						url +
						id + 
						"\">" + 
						id + 
						"</a>, ");
			}
		}
		return link.length() > 0 ? link.substring(0, link.length()-2) : "";
	}
	
	/**
	* M�todo que retorna true caso o par�metro OBRIGAR_EMPRESA seja TRUE.
	*
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public static boolean isEmpresaRequired(){
		try {
			return "TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.OBRIGAR_EMPRESA));
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/**
	* M�todo que retorna o n�mero limitado a 10 caracteres
	*
	* @param numero
	* @return
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public static String getNumeroDocumentoRemessa(String numero) {
		if(StringUtils.isNotBlank(numero) && numero.length() > 10){
			return numero.substring(0, 10);
		}else {
			return numero;
		}
	}
	
	public static Double roundImpostoByParametro(Double dbl, String param_imposto){
		return roundImpostoByParametro(new BigDecimal(dbl), param_imposto);
	}
	
	public static Double roundImpostoByParametro(BigDecimal dbl, String param_imposto){
		try {
			String param_decimais = null;
			if(NeoWeb.isInRequestContext()){
				Object paramObj = NeoWeb.getRequestContext().getSession().getAttribute(param_imposto);
				if(paramObj != null){
					param_decimais = paramObj.toString();
				}
			} 
			if(param_decimais == null || param_decimais.equals("")){
				param_decimais = ParametrogeralService.getInstance().getValorPorNome(param_imposto);
			}
			
			if(param_decimais != null && !param_decimais.equals("")){
				if(NeoWeb.isInRequestContext()){
					NeoWeb.getRequestContext().getSession().setAttribute(param_imposto, param_decimais);
				}
				
				if(Tipoarredondamentoimposto.ROUND_FLOOR.ordinal() == new Integer(Integer.parseInt(param_decimais)).intValue()){
					return dbl.setScale(2, BigDecimal.ROUND_FLOOR).doubleValue();
				}else if(Tipoarredondamentoimposto.ROUND_HALF_UP.ordinal() == new Integer(Integer.parseInt(param_decimais)).intValue()){
					return dbl.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
				}else if(Tipoarredondamentoimposto.ROUND_HALF_DOWN.ordinal() == new Integer(Integer.parseInt(param_decimais)).intValue()){
					return dbl.setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
				}else if(Tipoarredondamentoimposto.ROUND_HALF_EVEN.ordinal() == new Integer(Integer.parseInt(param_decimais)).intValue()){
					return dbl.setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
				}else { 
					return dbl.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
				}
			}else {
				return SinedUtil.round(dbl.doubleValue(), 2);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dbl != null ? dbl.doubleValue() : null;
	}
	
	/**
	* M�todo que retornas as fun��es de javascript toMoney, toFloat, demaskvalue, formatamoney
	*
	* @return
	* @since 20/01/2016
	* @author Luiz Fernando
	*/
	public static String getFuncoesSinedJs() {
		String funcFormatamoney = "function formatamoney(c) { " +
		"var t = this; if(c == undefined) c = 2; " +
		"var p, d = (t=t.split(\".\"))[1].substr(0, c); " +
		"for(p = (t=t[0]).length; (p-=3) >= 1;) { " +
		"	t = t.substr(0,p) + \".\" + t.substr(p); " +
		"} " +
		"return t+\",\"+d+Array(c+1-d.length).join(0); " +
		"} " + 
		"String.prototype.formatCurrency=formatamoney; ";
		
		String funcDemaskvalue= " function demaskvalue(valor, currency){ " +
		"var val2 = ''; " +
		"var strCheck = '-0123456789'; " +
		"var len = valor.length; " +
		"if (len== 0){ " +
		"	return 0.00; " +
		"} " +
		"if (currency ==true){ " +
		"	for(var i = 0; i < len; i++) " +
		"		if ((valor.charAt(i) != '0') && (valor.charAt(i) != ',')) break; " +
		"	for(; i < len; i++){ " +
		"		if (strCheck.indexOf(valor.charAt(i))!=-1) val2+= valor.charAt(i); " +
		"	} " +
		"	if(val2.length==0) return \"0.00\"; " +
		"	if (val2.length==1)return \"0.0\" + val2; " +
		"	if (val2.length==2)return \"0.\" + val2; " +
		"	var parte1 = val2.substring(0,val2.length-2); " +
		"	var parte2 = val2.substring(val2.length-2); " +
		"	var returnvalue = parte1 + \".\" + parte2; " +
		"	return returnvalue; " +
		"} " +
		"else{ " +
		"	val3 =\"\"; " +
		"	for(var k=0; k < len; k++){ " +
		"		if (strCheck.indexOf(valor.charAt(k))!=-1) val3+= valor.charAt(k); " +
		"	} " +
		"	return val3; " +
		"} " +
		"} ";
		
		String funcToFloat = "function toFloat(valor){ " +
		"if(typeof valor == \"string\"){ " +
		"	if(valor == \"\") return 0.0; " +
		"	 " +
		"	valor = valor.replace(new RegExp('\\\\.', 'gi'),\"\"); " +
		"	valor = valor.replace(new RegExp(',', 'gi'),\".\"); " +
		"	 " +
		"	var floatNum = parseFloat(valor); " +
		"	return floatNum; " +
		"} " +
		"if(typeof valor == \"object\"){ " +
		"	var floatNum = valor.value; " +
		"	return parseFloat(value); " +
		"} " +
		"return parseFloat(valor); " +
		"} ";
		
		String funcToMoney = "function toMoney(valor){ " +
		"if(valor == null) return \"0,00\"; " +
		"valor = valor.toFixed(2); " +
		"valor = new String(valor); " +
		"valor = valor.replace(\".\", \",\"); " +
		"return demaskvalue(valor,true).formatCurrency();" +
		"} ";
		
		String funcGetValorArredondado = "function getValorArredondado(valor){ " +
		" return toFloat(+(Math.round(valor + \"e+2\") + \"e-2\"))" +
		"} ";
		
		return  (funcFormatamoney + funcDemaskvalue + funcToFloat + funcToMoney + funcGetValorArredondado).replace("\n", "");
	}
	
	/**
	* M�todo que retorna o cdpessoa da empresa selecionada no cabe�alho
	*
	* @return
	* @since 27/01/2016
	* @author Luiz Fernando
	*/
	public static Integer getIdEmpresaSelecionadaCabecalho() {
		try {
			HttpServletRequest request = NeoWeb.getRequestContext().getServletRequest();
			if(request != null && request.getSession() != null && request.getSession().getAttribute(AlteraEmpresaSelecionadaProcess.ATTR_EMPRESASELECIONADA) != null){
				return ((Empresa) request.getSession().getAttribute(AlteraEmpresaSelecionadaProcess.ATTR_EMPRESASELECIONADA)).getCdpessoa();
			}
		} catch (Exception e) {}
		
		return null;
	}
	
	public static boolean validaChaveWSW3Producao(String key) {
		if(StringUtils.isBlank(key)){
			throw new NotAuthenticatedException("Chave n�o informada");
		}
		String bdKey = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.W3PNEU_WS_KEY);
		if(StringUtils.isBlank(bdKey)){
			throw new NotAuthenticatedException("Chave n�o cadastrada no BD.");
		}
		
		return key.equals(bdKey);
	}
	
	public static void addContentXML(Element parent, String nome, java.util.Date valor) {
		if(valor != null){
			addContentXML(parent, nome, SinedDateUtils.toStringPostgre(valor));
		}
	}
	
	public static void addContentXML(Element parent, String nome, Date valor) {
		if(valor != null){
			addContentXML(parent, nome, SinedDateUtils.toStringPostgre(valor));
		}
	}
	
	public static void addContentXML(Element parent, String nome, Integer valor) {
		if(valor != null){
			addContentXML(parent, nome, valor.toString());
		}
	}
	
	public static void addContentXML(Element parent, String nome, String valor) {
		if(valor != null){
			Element codigoElt = new Element(nome);
			codigoElt.setText(valor);
			parent.addContent(codigoElt);
		}
	}
	
	public static String getIdEmpresaParametroRequisicaoAutocomplete() {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {}
		return cdempresa;
	}
	public static String getTipoOperacaoParametroRequisicaoAutocomplete() {
		String tipoOperacao = null;
		try {
			tipoOperacao = NeoWeb.getRequestContext().getParameter("tipoOperacao");
		} catch (Exception e) {}
		return tipoOperacao;
	}
	public static Integer getEmpresaParametroRequisicaoAutocomplete() {
		Integer empresa = null;
		try {
			empresa = Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa"));
		} catch (Exception e) {}
		return empresa;
	}
	public static Integer getNotaFiscalProdutoParametroRequisicaoAutocomplete() {
		Integer notaFiscalProduto = null;
		try {
			notaFiscalProduto = Integer.parseInt(NeoWeb.getRequestContext().getParameter("notaFiscalProduto"));
		} catch (Exception e) {}
		return notaFiscalProduto;
	}
	public static Integer getEntradaFiscalParametroRequisicaoAutocomplete() {
		Integer entradaFiscal = null;
		try {
			entradaFiscal = Integer.parseInt(NeoWeb.getRequestContext().getParameter("entradaFiscal"));
		} catch (Exception e) {}
		return entradaFiscal;
	}
	
	public static String getParametroRequisicaoAutocomplete(String parametro) {
		String whereIn = null;
		try {
			whereIn = NeoWeb.getRequestContext().getParameter(parametro);
		} catch (Exception e) {}
		return whereIn;
	}
	
	public static List<?> getListPaginacao(WebRequestContext request, FiltroListagemSined filtro, List<?> listaCompleta){
    	
    	int size = listaCompleta.size();
    	
    	Integer currentPage = 0;
    	String parameter = request.getParameter("currentPage");
        if (parameter!=null) {
        	currentPage = Integer.parseInt(parameter);
        }
        
        int i = size / filtro.getPageSize();
        if (size % filtro.getPageSize() != 0){
        	i++;
        }
        
        filtro.setPageSize(filtro.getPageSize());
        filtro.setNumberOfResults(size); 
        filtro.setNumberOfPages(i);
        filtro.setCurrentPage(currentPage);
        
        return listaCompleta.subList(currentPage * filtro.getPageSize(), currentPage * filtro.getPageSize() + filtro.getPageSize() > size ? size : currentPage * filtro.getPageSize() + filtro.getPageSize());
    }
	
	public static Money getSoma(List<Money> listaMoney){
		Money retorno = new Money();
		
		for (Money money: listaMoney){
			if (money!=null){
				retorno = retorno.add(money);
			}
		}
		
		return retorno;
	}
	
	public static String getNomeArquivoSemExtensao(String nome) {
		if (nome != null) {
			int posUltimoPonto = nome.lastIndexOf(".");
			
			if (posUltimoPonto >= 0) {
				return nome.substring(0, posUltimoPonto);
			} else {
				return nome;
			}
		} else {
			return "";
		}
	}
	
	public static String getNomeArquivoReduzido(String nome, int tamanhoMaximo){
		if (nome==null){
			return "";
		}
		
		if (nome.length()<=tamanhoMaximo){
			return nome;
		}else {
			int posUltimoPonto = nome.lastIndexOf(".");
			if (posUltimoPonto>=0){
				String extensao = nome.substring(posUltimoPonto, nome.length());
				int posFim = tamanhoMaximo<=posUltimoPonto ? (tamanhoMaximo - extensao.length()) : posUltimoPonto;
				return nome.substring(0, posFim-1) + "~" + extensao;			
			}else {
				return nome.substring(0, tamanhoMaximo);			
			}
		}		
	}
	
	public String getWhereClienteempresa(String aliasCliente, boolean addAnd, String whereInEmpresa){
		String sql = "";
		if(StringUtils.isBlank(aliasCliente)){
			throw new SinedException("Alias inv�lido para a condi��o de restri��o de cliente por empresa");
		}
		String whereInEmpresaUsuario = new SinedUtil().getListaEmpresa();
		if(StringUtils.isNotBlank(whereInEmpresaUsuario) || StringUtils.isNotBlank(whereInEmpresa)){
			sql += (addAnd ? " and " : "") + aliasCliente + ".cdpessoa in ( select cRestricaoEmpresa.cdpessoa from cliente cRestricaoEmpresa " +
							" left outer join Clienteempresa ceRestricaoEmpresa on ceRestricaoEmpresa.cdcliente = cRestricaoEmpresa.cdpessoa " +
							" where ceRestricaoEmpresa.cdempresa is null or ceRestricaoEmpresa.cdempresa in ("+ 
							(StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : whereInEmpresaUsuario) +
							")) ";
		}
		return sql;
	}
	
	/**
	* M�todo que cria a condi��o de restri��o de fornecedor por empresa
	*
	* @param aliasFornecedor
	* @param addAnd
	* @param whereInEmpresa
	* @return
	* @since 25/01/2017
	* @author Luiz Fernando
	*/
	public String getWhereFornecedorempresa(String aliasFornecedor, boolean addAnd, String whereInEmpresa){
		String sql = "";
		if(isRestricaoEmpresaFornecedorUsuarioLogado()){
			if(StringUtils.isBlank(aliasFornecedor)){
				throw new SinedException("Alias inv�lido para a condi��o de restri��o de fornecedor por empresa");
			}
			String whereInEmpresaUsuario = new SinedUtil().getListaEmpresa();
			if(StringUtils.isNotBlank(whereInEmpresaUsuario) || StringUtils.isNotBlank(whereInEmpresa)){
				sql += (addAnd ? " and " : "") + aliasFornecedor + ".cdpessoa in ( select fRestricaoEmpresa.cdpessoa from fornecedor fRestricaoEmpresa " +
								" left outer join Fornecedorempresa feRestricaoEmpresa on feRestricaoEmpresa.cdfornecedor = fRestricaoEmpresa.cdpessoa " +
								" where feRestricaoEmpresa.cdempresa is null or feRestricaoEmpresa.cdempresa in ("+ 
								(StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : whereInEmpresaUsuario) +
								")) ";
			}
		}
		return sql;
	}
	
	public String getWhereFuncaoRestricaoFornecedorempresa(String aliasFornecedor, boolean addAnd, String whereInEmpresa){
		String sql = "";
		if(isRestricaoEmpresaFornecedorUsuarioLogado()){
			if(StringUtils.isBlank(aliasFornecedor)){
				throw new SinedException("Alias inv�lido para a condi��o de restri��o de fornecedor por empresa");
			}
			String whereInEmpresaUsuario = new SinedUtil().getListaEmpresa();
			if(StringUtils.isNotBlank(whereInEmpresaUsuario) || StringUtils.isNotBlank(whereInEmpresa)){
				sql += (addAnd ? " and " : "")  + " true = permissao_fornecedorempresa(" + aliasFornecedor + ".cdpessoa, '" + 
													(StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : whereInEmpresaUsuario) + "')";
			}
		}
		return sql;
	}
	
	//true = permissao_fornecedorempresa(" + aliasFornecedor + ".cdpessoa, '" + new SinedUtil().getListaEmpresa() + "')
	
	public static String getTextoUTF8(String texto){
		if(StringUtils.isNotBlank(texto)){
			try {
				return new String(texto.getBytes(),"UTF-8");
			} catch (UnsupportedEncodingException e) {}
		}
		return texto;
	}
	
	public static String removeLinkHtml(String texto){
		return StringUtils.isNotBlank(texto) ? texto.replaceAll("<a ?href=[^,]*['\"]>", "").replaceAll("</a>", "") : texto;
	}
	
	public static String getFormataCasasdecimais(Double valor, Unidademedida unidademedida) {
		if(valor != null){
			String str = BigDecimal.valueOf(valor).toPlainString();
			String casasdecimais = str.substring(str.indexOf(".")+1);
			if(Long.parseLong(casasdecimais) > 0){
				return roundByUnidademedida(valor, unidademedida).toString();
			}else {
				return str;
			}
		}
		return "0";
	}
	
	/**
	* M�todo que verifica o valor do parametro SPED_MAPA_PRODUTO. 
	*
	* @return
	* @since 05/04/2017
	* @author Luiz Fernando
	*/
	public static boolean isSpedMapaProduto(){
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute(Parametrogeral.SPED_MAPA_PRODUTO);
		if(attribute != null){
			return (Boolean)attribute;
		} else {
			if(Neo.isInApplicationContext()){
				if(ParametrogeralService.getInstance().getBoolean(Parametrogeral.SPED_MAPA_PRODUTO)){
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.SPED_MAPA_PRODUTO, Boolean.TRUE);
					return true;
				} else {
					NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.SPED_MAPA_PRODUTO, Boolean.FALSE);
					return false;
				}
			} else return false;
		}
	}
	
	/**
	 * Transforma a quantidade de horas em Double para o formato HH:MM
	 *
	 * @param qtdehoras
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/04/2017
	 */
	public static String transformDecimalHourString(Double qtdehoras) {
		String qtdeStr = qtdehoras.toString();
		String[] hours = qtdeStr.split("\\.");
		
		Integer hora = Integer.parseInt(hours[0]);
		String horaStr = hora.toString();
		if(hora < 10) horaStr = "0" + hora.toString();
		
		Double min = qtdehoras - Double.parseDouble(hora.toString());
		min = min * 60;
		long minInt = Math.round(min);
		String minStr = minInt + "";
		if(minInt < 10)  minStr = "0" + minInt;
		
		return horaStr + ":" + minStr;
	}
	
	public static String convertImageToBase64(Image imagem){
		if(imagem == null){
			return "";
		}
		BufferedImage bi = new BufferedImage(imagem.getWidth(null),imagem.getHeight(null),BufferedImage.TYPE_INT_RGB);
	    Graphics bg = bi.getGraphics();
	    bg.drawImage(imagem, 0, 0, null);
	    bg.dispose();
	     
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();

	    try {
	    	ImageIO.write( bi, "jpg", baos );
			baos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    byte[] imageInByteArray = baos.toByteArray();
	    try {
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    String b64 = javax.xml.bind.DatatypeConverter.printBase64Binary(imageInByteArray);
	    return b64;
	}
	
	public static Integer getIdUnicoWhereIn(String whereIn){
		if(StringUtils.isNotBlank(whereIn)){
			String[] ids = whereIn.split(",");
			if(ids.length == 1){
				try {
					return Integer.parseInt(ids[0]);
				} catch (Exception e) {}
			}else if(ids.length > 1){
				String idUnico = null;
				for(String id : ids){
					if(idUnico == null){
						idUnico = id;
					}else if(!idUnico.equals(id)){
						idUnico = null;
						break;
					}
				}
				if(idUnico != null){
					try {
						return Integer.parseInt(idUnico);
					} catch (Exception e) {}
				}
			}
		}
		return null;
	}
	
	public static Integer casasDecimaisForArredondamentoDesconto(){
		Integer numCasasDecimais = null;
		boolean utilizarArredondamentoDesconto = ParametrogeralService.getInstance().getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
		if (utilizarArredondamentoDesconto) {
			String aux = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			if (aux != null && !aux.trim().isEmpty()) {
				try {
					numCasasDecimais = Integer.parseInt(aux);
				} catch(Exception e) {}
			}
		}
		return numCasasDecimais;
	}
	//alterar a imagem do boleto aqui
	public static Image getLogoLinkCom() {
		try {
			return NeoImageResolver.getInstance().getImage("/imagens/sys/w3erp_logo_relatorio_rodape_.png");
		} catch (IOException e) {
			return null;
		}
	}

	public static boolean exibirChatSuporte(){
		if(NeoWeb.isInRequestContext()){
			String nomeParametro = "ATIVAR_CHAT_SUPORTE";
			HttpSession session = NeoWeb.getRequestContext().getSession();
			Object valorParametro = session.getAttribute(nomeParametro);
			if(valorParametro == null){
				valorParametro = ParametrogeralService.getInstance().buscaValorPorNome(nomeParametro);
				session.setAttribute(nomeParametro, valorParametro != null ? valorParametro.toString() : "false");
			}
			return "true".equalsIgnoreCase(valorParametro.toString());
		}
		
		return false;
	}

	public static boolean permissaoAcaoTela(WebRequestContext request, String acao) throws IOException {
		AuthorizationManager authorizationManager = Neo.getApplicationContext().getAuthorizationManager();
		if(!authorizationManager.isAuthorized(pathTela(), acao, getUsuarioLogado())){
			HttpServletResponse response = request.getServletResponse();
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sem permiss�o para acessar conte�do");
			return false;
		}
		
		return true;
	}
	
	public static boolean existMessageError(Message[] message, String mensagem){
		if(mensagem != null && message != null && message.length > 0){
			for(Message m : message){
				if(m.getSource().equals(mensagem)) return true;
			}
		}
		return false;
	}
	
	public static List<String> getMessageError(Message[] message){
		List<String> lista = new ArrayList<String>();
		if(message != null && message.length > 0){
			for(Message m : message){
				if(MessageType.ERROR.equals(m.getType())){
					lista.add(m.getSource().toString());
				}
			}
		}
		return lista;
	}
	
	public static void markAsReader() {
		if(!Thread.currentThread().getName().contains("-reader"))
			Thread.currentThread().setName(Thread.currentThread().getName() + "-reader");
	}
	
	public static boolean isFromInsertOne(){
		try {
			return "true".equals(NeoWeb.getRequestContext().getParameter("fromInsertOne"));
		} catch (Exception e) {
			return false;
		}
	}
	
	public static void desmarkAsReader() {
		Thread.currentThread().setName(Thread.currentThread().getName().replaceAll("-reader", ""));
	}
	
	public static Double getQtdeEntradaSubtractSaida(Double qtdeEntrada, Double qtdeSaida) {
		if (qtdeEntrada == null && qtdeSaida == null) return 0.0;
		if (qtdeEntrada != null && qtdeSaida == null) return qtdeEntrada;
		if (qtdeEntrada == null && qtdeSaida != null) return qtdeSaida;

		return new BigDecimal(qtdeEntrada.toString()).subtract(
		new BigDecimal(qtdeSaida.toString())).doubleValue();
	}
	
	public static String impressaoDataMDFe(Timestamp dhEvento) {
		if(dhEvento == null) return "";
		String format = FORMATADOR_DATA_HORA_TIMEZONE.format(dhEvento);
		return format.substring(0, format.length()-2) + ":00";
	}
	
	public static boolean isForceWriteByHourAndDayWeek() {
		Timestamp currentTimestamp = SinedDateUtils.currentTimestamp();
		int dayWeek = SinedDateUtils.getTimestampProperty(currentTimestamp, Calendar.DAY_OF_WEEK);
		int hour = SinedDateUtils.getTimestampProperty(currentTimestamp, Calendar.HOUR_OF_DAY);
		
		if(dayWeek == Calendar.SATURDAY || dayWeek == Calendar.SUNDAY){
			return true;
		}
		if(hour < 7 || hour > 18){
			return true;
		}
		
		return false;
	}
	
	public static String getFileChecksum(File file) throws IOException, NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		
	    //Get file input stream for reading the file content
	    FileInputStream fis = new FileInputStream(file);
	     
	    //Create byte array to read data in chunks
	    byte[] byteArray = new byte[1024];
	    int bytesCount = 0;
	      
	    //Read file data and update in message digest
	    while ((bytesCount = fis.read(byteArray)) != -1) {
	        digest.update(byteArray, 0, bytesCount);
	    };
	     
	    //close the stream; We don't need it now.
	    fis.close();
	     
	    //Get the hash's bytes
	    byte[] bytes = digest.digest();
	     
	    //This bytes[] has bytes in decimal format;
	    //Convert it to hexadecimal format
	    StringBuilder sb = new StringBuilder();
	    for(int i=0; i< bytes.length ;i++) {
	        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	    }
	     
	    //return complete hash
	   return sb.toString();
	}
	
	public static boolean moneyIsMaiorQueZero(Money valor){
		return valor != null && valor.getValue().doubleValue() > 0.0;
	}
	
	public static String criarLinkVisualizacaoRateio(Integer cdRateio){
		if(cdRateio != null){
			return "<a href=\"javascript:abrirPopUpRateio(" + cdRateio + ")\"> Rateio anterior: " + cdRateio + "</a>";  
		}
		return "";
		
	}
	
	public static String booleanToSimNao(Boolean bool){
		return Boolean.TRUE.equals(bool)? "Sim": "N�o";
	}
	
	/**
	* M�todo que converte os valores Money para String, colocando os n�meros negativos entre par�nteses para a contabilidade
	*
	* @param valorMoney
	* @since 15/01/2020
	* @author Eduardo Marun
	*/
	public static String converteValorNegativoRelatorio (Money valorMoney){
		String valorString;
		
		if(valorMoney.toLong() < 0){
			valorString = valorMoney.toString().substring(1, valorMoney.toString().length());
			valorString = "(" + valorString + ")";
		}else {
			valorString = valorMoney.toString();
		}
		return valorString;
	}
	
	public static QueryBuilder setJoinsByComponentePneu(QueryBuilder query){
		query.leftOuterJoin("pneu.pneumarca pneumarca")
		     .leftOuterJoin("pneu.pneumodelo pneumodelo")
		     .leftOuterJoin("pneu.pneumedida pneumedida")
		     .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
		     .leftOuterJoin("pneu.pneuSegmento pneuSegmento")
		     .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
		     .leftOuterJoin("pneu.otrPneuTipo otrPneuTipo");
		String selectCampos = "pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, pneu.descricao, pneu.lonas, pneu.acompanhaRoda, " +
					"pneumarca.cdpneumarca, pneumarca.nome, " +
					"pneumedida.cdpneumedida, pneumedida.nome, " +
					"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
					"otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, " +
					"pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, " +
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, pneuSegmento.acompanhaRoda, " +
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.tipoPneuOtr, " +
					"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, pneuqualificacao.garantia, " +
			 		"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, pneumaterialbanda.profundidadesulco";
		if(query.getSelect() != null && StringUtils.isNotBlank(query.getSelect().getValue())){
			query.select(query.getSelect().getValue() +", "+selectCampos);
		}else{
			query.select(selectCampos);
		}
		return query;
	}

	public static String putBreakLineAtLimit(String text, int limit) {
		if(StringUtils.isBlank(text)) return text;
		if(text.length() <= limit) return text;
		
		StringBuilder strFinal = new StringBuilder();
		StringBuilder strLine = new StringBuilder();
		
		String[] arrayLineBreak = text.contains("\r\n")? text.split("\r\n"): text.split("\n");
		if(arrayLineBreak.length > 1){//Se j� possui quebra de linha no texto original, fax a chamada recursiva desse m�todo, por linha do texto original, pra n�o retirar as quebras originais
			for(String lineBreak: arrayLineBreak){
				strFinal.append(SinedUtil.putBreakLineAtLimit(lineBreak, limit));
				if(!(strFinal.toString().endsWith("\r\n ") || strFinal.toString().endsWith("\r\n"))){
					strFinal.append("\r\n");
				}
			}
			return strFinal.toString();
		}
		String[] arrayAtWhiteSpace = text.split("\\s+");
		for (String string : arrayAtWhiteSpace) {
			if(string.length() + strLine.length() > limit){
				strFinal.append(strLine.toString()).append("\r\n");
				strLine = new StringBuilder();
			}
			strLine.append(string).append(" ");
		}
		strFinal.append(strLine.toString());
		return strFinal.toString();
	}
	
	public static Localdestinonfe getLocaldestinonfeByCfop(Cfop cfop){
		Localdestinonfe localdestinonfe = null;
		if(Util.objects.isPersistent(cfop)){
			Cfop cfopAux = CfopService.getInstance().load(cfop, "cfop.cdcfop, cfop.cfopescopo");
			if(cfopAux != null){
				if(cfopAux.getCfopescopo().equals(Cfopescopo.ESTADUAL)){
					localdestinonfe = Localdestinonfe.INTERNA;
				} else if(cfopAux.getCfopescopo().equals(Cfopescopo.FORA_DO_ESTADO)){
					localdestinonfe = Localdestinonfe.INTERESTADUAL;
				} else if(cfopAux.getCfopescopo().equals(Cfopescopo.INTERNACIONAL)){
					localdestinonfe = Localdestinonfe.EXTERIOR;
				}
			}
		}
		return localdestinonfe;
	}
	
	public static boolean isSegmentoOtr(){
		return ParametrogeralService.getInstance().getBoolean(Parametrogeral.HABILITAR_CONFIGURACAO_OTR);
	}
	
	public static String complementoOtr(){
		return SinedUtil.isSegmentoOtr()? "Otr": "";
	}

	public static String removeQuebraLinha(String str) {
		if(StringUtils.isNotBlank(str)){
			String strFinal = str.replace("\n", "");
			strFinal = strFinal.replace("\r", "");
			return strFinal;
		}
		
		return str;
	}

	public static boolean isIntegracaoTrayCorp(){
		return ParametrogeralService.getInstance().getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && "TRAYCORP".equalsIgnoreCase(ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE));
	}
	
	public static boolean isIntegracaoEcompleto(){
		return ParametrogeralService.getInstance().getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && "ECOMPLETO".equalsIgnoreCase(ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE));
	}
}