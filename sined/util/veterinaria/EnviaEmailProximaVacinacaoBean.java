package br.com.linkcom.sined.util.veterinaria;

public class EnviaEmailProximaVacinacaoBean {
	
	private Integer cdanimal; 
	private String nomeAnimal; 
	private String nomeCliente; 
	private String vacinas; 
	private String proximaaplicacao;
	
	public Integer getCdanimal() {
		return cdanimal;
	}
	public String getNomeAnimal() {
		return nomeAnimal;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public String getVacinas() {
		return vacinas;
	}
	public String getProximaaplicacao() {
		return proximaaplicacao;
	}
	
	public void setCdanimal(Integer cdanimal) {
		this.cdanimal = cdanimal;
	}
	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public void setVacinas(String vacinas) {
		this.vacinas = vacinas;
	}
	public void setProximaaplicacao(String proximaaplicacao) {
		this.proximaaplicacao = proximaaplicacao;
	}
}