package br.com.linkcom.sined.util.veterinaria;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Animal;
import br.com.linkcom.sined.geral.bean.AnimalVacina;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.AnimalVacinaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;

public class EnviaEmailProximaVacinacaoUtil {
	
	public static EnviaEmailProximaVacinacaoUtil util = new EnviaEmailProximaVacinacaoUtil(); 
	
	public void executarJobEnviaEmail(){
		Integer dias = Integer.parseInt(ParametrogeralService.getInstance().getValorPorNome("DIAS_PARA_VACINACAO"));
		Date proximaVacina = SinedDateUtils.addDiasData(new Date(System.currentTimeMillis()), dias);
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		//Carregando a lista dos animais
		List<AnimalVacina> listaAnimalVacina = AnimalVacinaService.getInstance().findForDataVacina(proximaVacina);
		
		/*
		//Agrupando vacinas
		List<AnimalVacina> listaVacagrupadas = new ArrayList<AnimalVacina>();
		
		if(SinedUtil.isListNotEmpty(listaAnimalVacina)){
			for (AnimalVacina animalVacina : listaAnimalVacina) {
				boolean achou = false;
				for (AnimalVacina animalVacina2 : listaVacagrupadas) {
					if (animalVacina != null && 
							animalVacina.getAnimal() != null && 
							animalVacina.getAnimal().getCdanimal() != null &&
							animalVacina2 != null && 
							animalVacina2.getAnimal() != null && 
							animalVacina2.getAnimal().getCdanimal() != null &&
							animalVacina.getAnimal().getCdanimal().equals(animalVacina2.getAnimal().getCdanimal())) {
						
						achou = true;
					}
				}
				
				if(achou) continue;
				
				listaVacagrupadas.add(new AnimalVacina(
										animalVacina.getAnimal().getCdanimal(), 
										animalVacina.getProduto().getNome(), 
										animalVacina.getAnimal().getNome(), 
										animalVacina.getAnimal().getCliente().getNome(), 
										formatoData.format(animalVacina.getProximaaplicacao()), 
										animalVacina.getAnimal()));
			}
		}
		//Envio de E-mail
		if(SinedUtil.isListNotEmpty(listaVacagrupadas)){
			for (AnimalVacina animalVacina : listaVacagrupadas) {
				if(animalVacina != null && animalVacina.getAnimal() != null && animalVacina.getAnimal().getCliente() != null
						&& animalVacina.getAnimal().getAnimalSituacao() != null && !Boolean.TRUE.equals(animalVacina.getAnimal().getAnimalSituacao().getObito()))					
					enviaEmailCliente(animalVacina);
			}
		}*/
		
		Map<Animal, List<AnimalVacina>> mapa = new HashMap<Animal, List<AnimalVacina>>();
		
		for (AnimalVacina animalVacina: listaAnimalVacina) {
			List<AnimalVacina> listaAnimalVacinaAux = mapa.get(animalVacina.getAnimal());
			if (listaAnimalVacinaAux==null){
				listaAnimalVacinaAux = new ArrayList<AnimalVacina>();
			}
			listaAnimalVacinaAux.add(animalVacina);
			mapa.put(animalVacina.getAnimal(), listaAnimalVacinaAux);
		}
		
		for (Animal animal: mapa.keySet()){
			String nomeCliente = animal.getCliente().getNome();
			if (nomeCliente.indexOf(" ")>-1){
				nomeCliente = nomeCliente.substring(0, nomeCliente.indexOf(" ")); //Pegando nome at� o primeiro espa�o
			}
			
			EnviaEmailProximaVacinacaoBean bean = new EnviaEmailProximaVacinacaoBean();
			bean.setCdanimal(animal.getCdanimal());
			bean.setNomeAnimal(animal.getNome());
			bean.setNomeCliente(nomeCliente);
			bean.setVacinas(CollectionsUtil.listAndConcatenate(mapa.get(animal), "produto.nome", ", "));
			bean.setProximaaplicacao(formatoData.format(proximaVacina));
			
			enviaEmailCliente(bean, animal);
		}
	}
	
	public void enviaEmailCliente (EnviaEmailProximaVacinacaoBean bean, Animal animal){
		Empresa empresa = EmpresaService.getInstance().loadPrincipal();		
		
		String assunto = "Chegou a hora da vacina/verm�fugo do seu pet!";

		ReportTemplateBean templateEmail = ReportTemplateService.getInstance().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMAIL_VET_VACINACAO);
		if(templateEmail != null){
			ITemplateEngine engine = new GroovyTemplateEngine().build(templateEmail.getLeiaute());
			
			Map<String,Object> datasource = new HashMap<String, Object>();
			datasource.put("bean", bean);
			String corpoEmailHTML = engine.make(datasource);

			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			//Registrando envio de e-mail
			Envioemail emailSalvo = EnvioemailService.getInstance().registrarEnvio(empresa.getEmail(), assunto, corpoEmailHTML, Envioemailtipo.AVISO_DE_PROXIMA_VACINACAO, 
					animal.getCliente().getCliente(), animal.getCliente().getEmail(), animal.getCliente().getNome(), email);	
			
			try{
				String dominio = Thread.currentThread().getName().substring(18, Thread.currentThread().getName().indexOf("_w3erp_PostgreSQLDS"));
				
				email.setCharset_ISO_8859_1(true);
				email.setFrom(emailSalvo.getRemetente());
				email.setSubject(emailSalvo.getAssunto());
				email.setTo(emailSalvo.getEmaildestinatario());
				email.addHtmlText(emailSalvo.getMensagem() + EmailUtil.getHtmlConfirmacaoEmailWithoutContext(emailSalvo, emailSalvo.getEmaildestinatario(), dominio));
				
				email.sendMessage();

			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
