package br.com.linkcom.sined.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.bean.GenericBean;

public class ObjectUtils extends br.com.linkcom.neo.util.ObjectUtils{

	public static GenericBean translateEntityToGenericBean(Object object){
		return object != null && Hibernate.isInitialized(object)? new GenericBean(Util.strings.toStringIdStyled(object), Util.strings.toStringDescription(object)): null;
	}
	
	public static List<GenericBean> translateEntityListToGenericBeanList(List<?> lista){
		List<GenericBean> returnList = new ArrayList<GenericBean>();
		if(CollectionsUtil.isListNotEmpty(lista)){
			for(Object obj: lista){
				returnList.add(ObjectUtils.translateEntityToGenericBean(obj));
			}
		}
		return returnList;
	}
	
	public static <E extends Object> GenericBean translateEntityToGenericBean(E object, String descriptionName) throws NoSuchFieldException, IllegalAccessException{
		Field fld = object.getClass().getDeclaredField(descriptionName);
        //Object obj = fld.get(object);
		Object obj = null;
        Method getterMethod = Util.beans.getGetterMethod(object.getClass(), descriptionName);
		try {
			obj = getterMethod.invoke(object);
			//Object obj2 = getterMethod.invoke(o2);
		}catch(Exception e){
			e.printStackTrace();
		}
		//return object != null? new GenericBean(Util.strings.toStringIdStyled(object), (String)obj): null;
		return object != null? new GenericBean(Util.strings.toStringIdStyled(object), (String)obj): null;
	}
	
	public static List<GenericBean> translateEntityListToGenericBeanList(List<?> lista, String descriptionName){
		List<GenericBean> returnList = new ArrayList<GenericBean>();
		for(Object obj: lista){
			GenericBean bean = null;
			try {
				bean = ObjectUtils.translateEntityToGenericBean(obj, descriptionName);
				returnList.add(bean);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnList;
	}
}
