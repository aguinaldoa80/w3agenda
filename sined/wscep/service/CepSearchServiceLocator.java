/**
 * CepSearchServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.linkcom.sined.wscep.service;


@SuppressWarnings({"serial","unchecked"})
public class CepSearchServiceLocator extends org.apache.axis.client.Service implements br.com.linkcom.sined.wscep.service.CepSearchService {

    public CepSearchServiceLocator() {
    }


    public CepSearchServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CepSearchServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CepSearch
    private java.lang.String CepSearch_address = "http://utilpub.linkcom.com.br/WSCep/services/CepSearch";

    public java.lang.String getCepSearchAddress() {
        return CepSearch_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CepSearchWSDDServiceName = "CepSearch";

    public java.lang.String getCepSearchWSDDServiceName() {
        return CepSearchWSDDServiceName;
    }

    public void setCepSearchWSDDServiceName(java.lang.String name) {
        CepSearchWSDDServiceName = name;
    }

    public br.com.linkcom.sined.wscep.service.CepSearch getCepSearch() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CepSearch_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCepSearch(endpoint);
    }

    public br.com.linkcom.sined.wscep.service.CepSearch getCepSearch(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
        	br.com.linkcom.sined.wscep.service.CepSearchSoapBindingStub _stub = new br.com.linkcom.sined.wscep.service.CepSearchSoapBindingStub(portAddress, this);
            _stub.setPortName(getCepSearchWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCepSearchEndpointAddress(java.lang.String address) {
        CepSearch_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.linkcom.sined.wscep.service.CepSearch.class.isAssignableFrom(serviceEndpointInterface)) {
            	br.com.linkcom.sined.wscep.service.CepSearchSoapBindingStub _stub = new br.com.linkcom.sined.wscep.service.CepSearchSoapBindingStub(new java.net.URL(CepSearch_address), this);
                _stub.setPortName(getCepSearchWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CepSearch".equals(inputPortName)) {
            return getCepSearch();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.wscep.linkcom.com.br", "CepSearchService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.wscep.linkcom.com.br", "CepSearch"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CepSearch".equals(portName)) {
            setCepSearchEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

