/**
 * CepSearchService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.linkcom.sined.wscep.service;

public interface CepSearchService extends javax.xml.rpc.Service {
    public java.lang.String getCepSearchAddress();

    public br.com.linkcom.sined.wscep.service.CepSearch getCepSearch() throws javax.xml.rpc.ServiceException;

    public br.com.linkcom.sined.wscep.service.CepSearch getCepSearch(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
