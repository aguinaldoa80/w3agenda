package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamaterialinspecao;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.dao.EntregamaterialDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistroentradaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregamaterialService extends GenericService<Entregamaterial> {

	private EntregamaterialDAO entregamaterialDAO;
	private EntregamaterialinspecaoService entregamaterialinspecaoService;
	private EntradafiscalService entradafiscalService;
	
	public void setEntregamaterialDAO(EntregamaterialDAO entregamaterialDAO) {
		this.entregamaterialDAO = entregamaterialDAO;
	}
	public void setEntregamaterialinspecaoService(
			EntregamaterialinspecaoService entregamaterialinspecaoService) {
		this.entregamaterialinspecaoService = entregamaterialinspecaoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param ordemcompramaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Double getQtdEntregueDoMaterialNaOrdemCompra(Ordemcompramaterial ordemcompramaterial) {
		return entregamaterialDAO.getQtdEntregueDoMaterialNaOrdemCompra(ordemcompramaterial);
	}

	/**
	 * M�todo que salva a entrega de materia e os itens da inspe��o do determinado material.
	 * Obs: este save so � feito a 1� vez que a entrega � salva. Pois ele gera ou n�o uma lista com estes itens de 
	 * inspe��o. Os demais saves s�o feitos normais n�o utilizando este para n�o apagar os itens.
	 * 
	 * @param entregamaterial
	 * @author Tom�s Rabelo
	 */
	public void saveEntregaMaterialMaisListaInspecao(Entregamaterial entregamaterial) {
		saveOrUpdateNoUseTransaction(entregamaterial);
		if(entregamaterial.getListaInspecao() != null && entregamaterial.getListaInspecao().size() > 0)
			for (Entregamaterialinspecao entregamaterialinspecao : entregamaterial.getListaInspecao()) 
				entregamaterialinspecaoService.saveOrUpdateNoUseTransaction(entregamaterialinspecao);
	}
	

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#findByEntrega
	 * @param e
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregamaterial> findByEntrega(Entrega e) {
		return entregamaterialDAO.findByEntrega(e);
	}
	
	public Double getQtdeByEntregaMaterial(Entregamaterial entregamaterial){
		return entregamaterialDAO.getQtdeByEntregaMaterial(entregamaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#findForLancarEstoqueConverterUnidademedida(Entrega entrega)
	 *
	 * @param entrega
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregamaterial> findForLancarEstoqueConverterUnidademedida(Entrega entrega, Material material) {
		return entregamaterialDAO.findForLancarEstoqueConverterUnidademedida(entrega, material);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#atualizaQtdeutilizadaValorSaldoretido(Integer cdentregamaterial, Double qtdeutilizada, Money valorsaldoretido)
	 *
	 * @param cdentregamaterial
	 * @param qtdeutilizada
	 * @param valorsaldoretido
	 * @author Luiz Fernando
	 */
	public void atualizaQtdeutilizada(Integer cdentregamaterial, Double qtdeutilizada) {
		entregamaterialDAO.atualizaQtdeutilizada(cdentregamaterial, qtdeutilizada);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#findQtdeutilizada(Integer cdentregamaterial)
	 *
	 * @param cdentregamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregamaterial findQtdeutilizada(Integer cdentregamaterial) {
		return entregamaterialDAO.findQtdeutilizada(cdentregamaterial);
	}
	
	/**
	 * M�todo que retorna a descri��o resumida de um cfop
	 *
	 * @param listaEntregamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public String getDescricaoresuminaCfopNatop(Set<Entregamaterial> listaEntregamaterial) {
		String natop = null; 
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getCfop() != null && em.getCfop().getDescricaoresumida() != null){
					natop = em.getCfop().getDescricaoresumida();
					break;
				}
			}
		}
		return natop;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite)
	 *
	 * @param cdmaterial
	 * @param cdempresa
	 * @param limite
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregamaterial> findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite){
		return entregamaterialDAO.findUltimascomprasByMaterial(cdmaterial, cdempresa, limite);
	}
	
	/**
	 * M�todo que retorna o iten da entrega com o melhor pre�o (�ltimas compras)
	 *
	 * @param cdmaterial
	 * @param cdempresa
	 * @param limite
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregamaterial getUltimascomprasMelhorValorByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite){
		Entregamaterial entregamaterial = null;
		List<Entregamaterial> listaEntregamaterial = this.findUltimascomprasByMaterial(cdmaterial, null, 5);
		
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			Collections.sort(listaEntregamaterial, new Comparator<Entregamaterial>(){
				public int compare(Entregamaterial em1, Entregamaterial em2){
					return em1.getValorunitario().compareTo(em2.getValorunitario());
				}
			});
			entregamaterial = listaEntregamaterial.get(0);
		}
		return entregamaterial;
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Entregamaterial> findForPdfListagem(String whereIn){
		return entregamaterialDAO.findForPdfListagem(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		entregamaterialDAO.deleteAllFromEntregaNotInWhereIn(cdentrega, whereNotIn);
	}
	
	public Entregamaterial addInfMestregrade(Entregamaterial bean, Entregamaterial copia, Material material, Double qtde){
		if(copia == null) copia = new Entregamaterial();
		entradafiscalService.setIdentificadorinterno(copia, null);
		
		copia.setEntregadocumento(bean.getEntregadocumento());
		
		copia.setMaterial(material != null ? material : copia.getMaterial());
		copia.setQtde(qtde != null ? qtde : bean.getQtde());
		copia.setComprimento(bean.getComprimento());
		copia.setValorunitario(bean.getValorunitario());
		copia.setMaterialclasse(bean.getMaterialclasse());
		copia.setMaterialclassetrans(bean.getMaterialclassetrans());
		
		copia.setCprod(bean.getCprod());
		copia.setXprod(bean.getXprod());
		copia.setCfop(bean.getCfop());
		copia.setUnidademedidacomercial(bean.getUnidademedidacomercial());
		copia.setValorproduto(bean.getValorproduto());
		copia.setValordesconto(bean.getValordesconto());
		copia.setValorfrete(bean.getValorfrete());
		copia.setValorseguro(bean.getValorseguro());
		copia.setValoroutrasdespesas(bean.getValoroutrasdespesas());
		
		copia.setCsticms(bean.getCsticms());
		copia.setValorbcicms(bean.getValorbcicms());
		copia.setIcms(bean.getIcms());
		copia.setValoricms(bean.getValoricms());
		copia.setValorbcicmsst(bean.getValorbcicmsst());
		copia.setIcmsst(bean.getIcmsst());
		copia.setValoricmsst(bean.getValoricmsst());
		copia.setValormva(bean.getValormva());
		copia.setNaoconsideraricmsst(bean.getNaoconsideraricmsst());
		
		copia.setCstipi(bean.getCstipi());
		copia.setValorbcipi(bean.getValorbcipi());
		copia.setIpi(bean.getIpi());
		copia.setValoripi(bean.getValoripi());
		
		copia.setCstpis(bean.getCstpis());
		copia.setValorbcpis(bean.getValorbcpis());
		copia.setPis(bean.getPis());
		copia.setPisretido(bean.getPisretido());
		copia.setQtdebcpis(bean.getQtdebcpis());
		copia.setValorunidbcpis(bean.getValorunidbcpis());
		copia.setValorpis(bean.getValorpis());
		copia.setValorpisretido(bean.getValorpisretido());
		
		copia.setCstcofins(bean.getCstcofins());
		copia.setValorbccofins(bean.getValorbccofins());
		copia.setCofins(bean.getCofins());
		copia.setCofinsretido(bean.getCofinsretido());
		copia.setQtdebccofins(bean.getQtdebccofins());
		copia.setValorunidbccofins(bean.getValorunidbccofins());
		copia.setValorcofins(bean.getValorcofins());
		copia.setValorcofinsretido(bean.getValorcofinsretido());
		
		copia.setValorcsll(bean.getValorcsll());
		copia.setValorir(bean.getValorir());
		copia.setValorinss(bean.getValorinss());
		copia.setValorimpostoimportacao(bean.getValorimpostoimportacao());
		
		copia.setLocalarmazenagem(bean.getLocalarmazenagem());
		copia.setLoteestoque(bean.getLoteestoque());
		copia.setProjeto(bean.getProjeto());
		copia.setCentrocusto(bean.getCentrocusto());
		copia.setFaturamentocliente(bean.getFaturamentocliente());
		
		copia.setValorbciss(bean.getValorbciss());
		copia.setIss(bean.getIss());
		copia.setValoriss(bean.getValoriss());
		copia.setMunicipioiss(bean.getMunicipioiss());
		copia.setTipotributacaoiss(bean.getTipotributacaoiss());
		copia.setClistservico(bean.getClistservico());
		
		copia.setNaturezabcc(bean.getNaturezabcc());
		
		copia.setValorFiscalIcms(bean.getValorFiscalIcms());
		copia.setValorFiscalIpi(bean.getValorFiscalIpi());
		
		if(bean.getListaOrdemcompraentregamaterial() != null && !bean.getListaOrdemcompraentregamaterial().isEmpty()){
			Set<Ordemcompraentregamaterial> listaOCEM = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
			for(Ordemcompraentregamaterial ordemcompraentregamaterial : bean.getListaOrdemcompraentregamaterial()){
				Ordemcompraentregamaterial copiaOCEM = new Ordemcompraentregamaterial();
				copiaOCEM.setEntregamaterial(copia);
				copiaOCEM.setOrdemcompramaterial(ordemcompraentregamaterial.getOrdemcompramaterial());
				copiaOCEM.setQtde(copia.getQtde());
				listaOCEM.add(copiaOCEM);				
			}
			copia.setListaOrdemcompraentregamaterial(listaOCEM);
		}
		
		return copia;
	}
	
	public Entregamaterial carregaEntregamaterialWMS(Entregamaterial entregamaterial){
		return entregamaterialDAO.carregaEntregamaterialWMS(entregamaterial);
	}
	
	/**
	 * Busca todas as entradas para o relat�rio de livro de registro de entrada.
	 * 
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Entregamaterial> findForLivroEntrada(LivroregistroentradaFiltro filtro){
	
		return entregamaterialDAO.findForLivroEntrada(filtro);
	}
	
	public List<Entregamaterial> findForIndustrializacaoRetorno(String whereIn, String whereInVenda){
		return entregamaterialDAO.findForIndustrializacaoRetorno(whereIn, whereInVenda);
	}
	
	public List<Entregamaterial> findForCTRC(String whereInCdentregadocumento, String whereNotInCdentregamaterial){
		return entregamaterialDAO.findForCTRC(whereInCdentregadocumento, whereNotInCdentregamaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#findByFornecedorMaterial(Material material, Fornecedor fornecedor)
	*
	* @param material
	* @param fornecedor
	* @return
	* @since 08/01/2015
	* @author Luiz Fernando
	*/
	public List<Entregamaterial> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		return entregamaterialDAO.findByFornecedorMaterial(material, fornecedor);
	}
	
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Entregamaterial> findForSpedRegistroK250(SpedarquivoFiltro filtro) {
		return entregamaterialDAO.findForSpedRegistroK250(filtro);
	}
	
	/**
	 * @param numero
	 * @return
	 * @since 28/08/2015
	 * @author Andrey Leonardo
	 */
	public List<Entregamaterial> findForVincularNotaFiscalProdutoItem(Notafiscalprodutoitem notafiscalprodutoitem){
		return entregamaterialDAO.findForVincularNotaFiscalProdutoItem(notafiscalprodutoitem);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregamaterialDAO#getPrecoMaximoCompra(String cdmaterial, String cdempresa, Integer qtdeMeses)
	*
	* @param cdmaterial
	* @param cdempresa
	* @param qtdeMeses
	* @return
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public Double getPrecoMaximoCompra(String cdmaterial, String cdempresa, Integer qtdeMeses) {
		return entregamaterialDAO.getPrecoMaximoCompra(cdmaterial, cdempresa, qtdeMeses);
	}
	
	/**
	* M�todo que retorna a lista de lote do item da entrada fiscal de acordo com a lista passada por par�metro
	*
	* @param entregamaterial
	* @param lista
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public Set<Entregamateriallote> getListaEntregamateriallote(Entregamaterial entregamaterial, List<Entregamateriallote> lista) {
		Set<Entregamateriallote> listaEntregamateriallote =  new ListSet<Entregamateriallote>(Entregamateriallote.class);
		if(entregamaterial != null && entregamaterial.getCdentregamaterial() != null && SinedUtil.isListNotEmpty(lista)){
			for(Entregamateriallote entregamateriallote : lista){
				if(entregamateriallote.getEntregamaterial() != null && entregamateriallote.getEntregamaterial().getCdentregamaterial() != null && 
						entregamateriallote.getEntregamaterial().getCdentregamaterial().equals(entregamaterial.getCdentregamaterial())){
					listaEntregamateriallote.add(entregamateriallote);
				}
			}
		}
		return listaEntregamateriallote;
	}
	
	public Money getValorIR(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorir()!=null){
					total = total.add(entregamaterial.getValorir());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorINSS(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorinss()!=null){
					total = total.add(entregamaterial.getValorinss());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorCSLL(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorcsll()!=null){
					total = total.add(entregamaterial.getValorcsll());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorII(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValoriiItem()!=null){
					total = total.add(entregamaterial.getValoriiItem());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorISS(Set<Entregamaterial> listaEntregamaterial){
		return getValorISS(listaEntregamaterial, false);
	}
	
	public Money getValorISS(Set<Entregamaterial> listaEntregamaterial, boolean retido){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValoriss()!=null && (!retido || Tipotributacaoiss.RETIDA.equals(entregamaterial.getTipotributacaoiss()))){
					total = total.add(entregamaterial.getValoriss());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorICMS(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValoricms()!=null){
					total = total.add(entregamaterial.getValoricms());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorICMSST(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValoricmsst()!=null && !Boolean.TRUE.equals(entregamaterial.getNaoconsideraricmsst())){
					total = total.add(entregamaterial.getValoricmsst());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorIPI(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValoripi()!=null){
					total = total.add(entregamaterial.getValoripi());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorPIS(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorpis()!=null){
					total = total.add(entregamaterial.getValorpis());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorPISRetido(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorpisretido()!=null){
					total = total.add(entregamaterial.getValorpisretido());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorCOFINS(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorcofins()!=null){
					total = total.add(entregamaterial.getValorcofins());
				}
			}
		}
		
		return total;
	}
	
	public Money getValorCOFINSRetido(Set<Entregamaterial> listaEntregamaterial){
		Money total = new Money();
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if (entregamaterial.getValorcofinsretido()!=null){
					total = total.add(entregamaterial.getValorcofinsretido());
				}
			}
		}
		
		return total;
	}
	
	
	public Entregamaterial findByMaterialNotaFiscal (Entregadocumento entrada, Material material){
		return entregamaterialDAO.findByMaterialEntradaFiscal(entrada, material);
	}
}