package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Etnia;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EtniaService extends GenericService<Etnia> {	
	
	/* singleton */
	private static EtniaService instance;
	public static EtniaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EtniaService.class);
		}
		return instance;
	}
	
}
