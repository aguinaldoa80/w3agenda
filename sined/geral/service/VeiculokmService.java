package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ConsistenciaKmTO;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.VeiculokmDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AnaliticoSinteticoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.AnaliticoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculokmService extends GenericService<Veiculokm>{

	private VeiculokmDAO veiculokmDAO;
	private ArquivoDAO arquivoDAO;
	private VeiculoService veiculoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setVeiculokmDAO(VeiculokmDAO veiculokmDAO) {
		this.veiculokmDAO = veiculokmDAO;
	}
	
	/**
	 * M�todo coloca na listagem a KM atual do veiculo
	 * 
	 * @see #buscaMateriais(List)
	 * @see #getKmAtualDoVeiculo(Veiculo) 
	 * @param lista
	 * @author Tom�s Rabelo
	 */
	public void adicionaKmAtualRegistros(List<Veiculokm> lista) {
		if(lista != null && lista.size() > 0){
			for (Veiculokm veiculokm: lista) 
				veiculokm.setVeiculokmatual(getKmAtualDoVeiculo(veiculokm.getVeiculo()));
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculokm getKmAtualDoVeiculo(Veiculo veiculo) {
		return veiculokmDAO.getKmAtualDoVeiculo(veiculo);
	}
	
	/**
	 * M�todo para verificar se um arquivo de importa��o Memphis 
	 * est� no formato correto 
	 * 
	 * @author Rafael Odon
	 * @param arquivo
	 * @return Boolean
	 */
	public Boolean verificarFormatoArquivo(Arquivo arquivo){
		return false;
			//!arquivo.getNome().endsWith(".txt") ||
			//!Pattern.matches("(.{3}\\d{4};\\d{2}/\\d{2}/\\d{4};\\d+\r?\n)+[.\n]*",  new String(arquivo.getConteudo()));
	}
	
	/**
	 * Retorna a diferen�a de quilometragem da ultima para a atual
	 * 
	 * @author Rafael Odon
	 * @param kmNovo, kmAntigo
	 * @return Long
	 */
	public Integer calculaDiferenca(Veiculokm kmNovo, Veiculokm kmAntigo){
		//se houver ultima importa��o...
		if (kmAntigo != null)
			return kmNovo.getKmnovo() - kmAntigo.getKmnovo();
		else
			//sen�o, a diferen�a � a partir do zero
			return kmNovo.getKmnovo();
	}
	
	/**
	 * Recebe um arquivo de texto, coleta dados das suas linhas
	 * importando quilometragens dos ve�culos para o sistema.
	 * 
	 * @param request
	 * @param arquivo, list, list
	 * @throws FormatoDeArquivoInvalidoException
	 * @author Rafael Odon
	 * @return int (quantidade de importa��es realizadas)
	 * @see br.com.linkcom.w3auto.geral.service.KmService#verificarFormatoArquivo(Arquivo)
	 * @see br.com.linkcom.w3auto.geral.service.KmService#gerarKmDaLinha(String, Arquivo)
	 * @see br.com.linkcom.w3auto.geral.service.KmService#findByVeiculo(Veiculo)
	 */
	public int importarArquivo(Arquivo arquivo, List<ConsistenciaKmTO> consistencia, List<Veiculokm> kmAcima5000) throws Exception{
		
		String [] linhas;
		int cont=0;
		
		//verifica se o arquivo est� no formato desejado
		if (verificarFormatoArquivo(arquivo)){
			throw new SinedException("Os dados do arquivo n�o s�o v�lidos. A importa��o n�o foi realizada.");
		}else{			
						
			String texto = new String(arquivo.getContent());
			linhas =  texto.split("\n");
			
			
			//salva o arquivo enviado para refer�nci�-lo nas quilometragens
			arquivoDAO.saveOrUpdate(arquivo);
	            
			for(String linha:linhas){										
				try{
					Veiculokm kmNovo = gerarKmDaLinha(linha,arquivo);
					
					if(kmNovo != null){
						cont++;
						//se o ve�culo n�o existir...
						if(kmNovo.getVeiculo().getCdveiculo() == null){
							consistencia.add(new ConsistenciaKmTO(kmNovo.getVeiculo().getPlaca(),"Placa n�o cadastrada no sistema.",false)); 
						}
						//se o ve�culo existir:
						else{
							//encontra a quilometragem anterior se ela existir...
							Veiculokm kmAntigo = new Veiculokm();
							kmAntigo = findByVeiculo(kmNovo.getVeiculo());
							
							//checa se a data da quilometragem anterior � maior que a data atual						
							if(kmAntigo!=null){
								if(kmAntigo.getDtentrada().after(kmNovo.getDtentrada())){
									consistencia.add(new ConsistenciaKmTO(kmNovo.getVeiculo().getPlaca(),
										"A data da quilometragem importada est� menor que a �ltima data de quilometragem.",
										false));
								}else{
								
									Integer diferenca = calculaDiferenca(kmNovo, kmAntigo);								
									
									//checa se a quilometragem abaixou
									if (diferenca < 0){
										consistencia.add(new ConsistenciaKmTO(kmNovo.getVeiculo().getPlaca(),"Quilometragem fora do padr�o.",true));
									}
									//se for superior a 5000 km, adiciona a lista de destaque
									if (diferenca >= 5000){
										kmAcima5000.add(kmNovo);												
									}																													
									
									try{
										kmNovo.setTipokmajuste(Tipokmajuste.OUTRO);
										//persiste a nova quilometragem
										saveOrUpdate(kmNovo);
									}catch(DataIntegrityViolationException e){
										consistencia.add(new ConsistenciaKmTO(kmNovo.getVeiculo().getPlaca(),"A quilometragem j� existe no sistema.",false));				
									}
								}																					
							}else{
								//caso nao exista nenhuma Km para o veiculo, define Kmentrada
								Veiculo veiculo = veiculoService.load(kmNovo.getVeiculo());
								veiculo.setKminicial(kmNovo.getKmnovo());
								veiculoService.atualizaKmInicial(veiculo);							
								kmNovo.setTipokmajuste(Tipokmajuste.INCIAL);
								//persiste a nova quilometragem
								saveOrUpdate(kmNovo);
							}
						}	
					}
				}catch(ParseException e){	
					throw new SinedException("Erro tentando converter String para Date na importa��o de quilometragem. ("+linha+")");
				}catch(NoSuchElementException e){
					throw new SinedException("Erro tentando interpretar linha na importa��o de quilometragem. ("+linha+")");
				}catch(NumberFormatException e){
					throw new SinedException("Erro tentando converter String para Long na importa��o de quilometragem. ("+linha+")");
				}		
				
			}
		}
		
		return cont;
	}
	
	/**
	 * Recebe uma linha e o seu arquivo de origem
	 * e configura um novo Km retornando-o
	 * 
	 * @param request
	 * @param linha, arquivo
	 * @return Km
	 * @throws ParseException 
	 * @throws NumberFormatException
	 * @author Rafael Odon
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#findByPlaca(Veiculo)
	 */
	public Veiculokm gerarKmDaLinha(String linha, Arquivo arquivo) throws ParseException,NumberFormatException{
		
		//elimina o \r se restar algum ap�s o split('\n')... (compatibilidade quebra de linha Linux/Ms)
		linha = linha.trim();
		Veiculokm kmNovo = null;
		
		if (!linha.equals("")){
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");		
			StringTokenizer st = new StringTokenizer(linha,";");		
			
			//obtem os 3 tokens da linha
			String placa = st.nextToken();
			Timestamp data = new Timestamp(df.parse(st.nextToken()).getTime());
			Double kmatual = Double.parseDouble(st.nextToken());
					
			//trata a data para ser a maior do dia (23:59)				
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(data.getTime());
			calendar.set(Calendar.HOUR, 23);
			calendar.set(Calendar.MINUTE, 59);
			data.setTime(calendar.getTimeInMillis());
			
			//gera uma nova Km com os dados
			kmNovo = new Veiculokm();										
			kmNovo.setDtentrada(new Timestamp(data.getTime()));
			kmNovo.setKmnovo(kmatual.intValue());
			kmNovo.setArquivo(arquivo);
			
			//encontra o ve�culo pela placa da quilometragem
			Veiculo veiculo = new Veiculo();
			if(placa.length() > 3){
				placa = placa.substring(0, 3) + "-" + placa.substring(3, placa.length());
			}
			veiculo.setPlaca(placa);
			Veiculo veiculoFind = veiculoService.findByPlaca(veiculo);
			if(veiculoFind != null)
				kmNovo.setVeiculo(veiculoFind);
			else
				kmNovo.setVeiculo(veiculo);
		}
			
		return kmNovo;
	}
	
	/**
	 * M�todo encontrar a ultima quilometragem de um ve�culo
	 * 
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Km
	 * @see br.com.linkcom.w3auto.geral.dao.KmDAO#findByVeiculo(Veiculo)
	 */
	public Veiculokm findByVeiculo(Veiculo veiculo){
		return veiculokmDAO.findByVeiculo(veiculo);
	}	
	
	/**
	 * Metodo para montar o relat�rio Anal�tico
	 * @return report
	 * @param  filtro
	 * @author Ramon Brazil
	 * @see #findAnalitico(AnaliticoFiltro)
	 */
	public Report createReportAnalitico(AnaliticoFiltro filtro) {
		List<AnaliticoSinteticoReportBean> lista = new ArrayList<AnaliticoSinteticoReportBean>();
		List<Veiculokm>listaajuste = null; 
		listaajuste = this.findAnalitico(filtro); 
		AnaliticoSinteticoReportBean beanAnalitico = null;
		for (Veiculokm ajuste : listaajuste) {
			beanAnalitico = new AnaliticoSinteticoReportBean();
			if(ajuste.getVeiculo().getColaborador() != null){
				beanAnalitico.setCooperado(ajuste.getVeiculo().getColaborador().getNome());
			}
			if(ajuste.getVeiculo().getVeiculomodelo() != null){
				beanAnalitico.setModelo(ajuste.getVeiculo().getVeiculomodelo().getNome());
			}
			beanAnalitico.setPlaca(ajuste.getVeiculo().getPlaca());
			beanAnalitico.setPrefixo(ajuste.getVeiculo().getPrefixo() != null ? ajuste.getVeiculo().getPrefixo() : " ");
			beanAnalitico.setTipoajuste(ajuste.getTipokmajuste().getDescricao());
			beanAnalitico.setDtajuste(ajuste.getDtentrada());
			beanAnalitico.setKmanterior(this.getKmanterior(ajuste));
			beanAnalitico.setKmposterior(ajuste.getKmnovo());
			beanAnalitico.setKmatual((ajuste.getVeiculo().getVwveiculo().getKmatual())== null? 0:ajuste.getVeiculo().getVwveiculo().getKmatual());
			beanAnalitico.setKmentrada((ajuste.getVeiculo().getKminicial() == null? 0: ajuste.getVeiculo().getKminicial()));
			beanAnalitico.setKmreal(ajuste.getVeiculo().getVwveiculo().getKmreal());
			beanAnalitico.setKmrodado(ajuste.getVeiculo().getVwveiculo().getKmrodado());
			lista.add(beanAnalitico);
		}
		if (lista == null || lista.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report = new Report("veiculo/relAnalitico");
//			report.addParameter("DATAINICIO",filtro.getDtinicio());
//			report.addParameter("DATAFIM",filtro.getDtfim());
			
			report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
			report.addParameter("LISTA", lista);
			report.addSubReport("SUBANALITICO", new Report("veiculo/relAnaliticoSub"));	                     
			return report;
		}
	}
	
	private Integer getKmanterior(Veiculokm ajuste) {
		return veiculokmDAO.getKmanterior(ajuste);
	}
	
	/**
	 * Metodo para montar o relat�rio Sintet�co
	 * @return report
	 * @param  filtro
	 * @author Ramon Brazil
	 * @see #findAnalitico(AnaliticoFiltro)
	 */
	public Report createReportSintetico(AnaliticoFiltro filtro) {
		List<AnaliticoSinteticoReportBean> lista = new ArrayList<AnaliticoSinteticoReportBean>();
		List<Veiculokm>listaajuste = null;	
		listaajuste = this.findAnalitico(filtro); 				
		Integer veiculo = 0;
		AnaliticoSinteticoReportBean beanAnalitico = null;
		for (Veiculokm ajuste : listaajuste) {
			if(!veiculo.equals(ajuste.getVeiculo().getCdveiculo())){
				beanAnalitico = new AnaliticoSinteticoReportBean();
				if(ajuste.getVeiculo().getColaborador() != null){
					beanAnalitico.setCooperado(ajuste.getVeiculo().getColaborador().getNome());
				}
				if(ajuste.getVeiculo().getVeiculomodelo() != null){
					beanAnalitico.setModelo(ajuste.getVeiculo().getVeiculomodelo().getNome());
				}
				beanAnalitico.setPlaca(ajuste.getVeiculo().getPlaca());
				beanAnalitico.setPrefixo(ajuste.getVeiculo().getPrefixo());
				beanAnalitico.setKmatual((ajuste.getVeiculo().getVwveiculo().getKmatual()) == null? 0:ajuste.getVeiculo().getVwveiculo().getKmatual());
				beanAnalitico.setKmentrada((ajuste.getVeiculo().getKminicial() == null? 0: ajuste.getVeiculo().getKminicial()));
				beanAnalitico.setKmreal(ajuste.getVeiculo().getVwveiculo().getKmreal());
				beanAnalitico.setKmrodado(ajuste.getVeiculo().getVwveiculo().getKmrodado());
				lista.add(beanAnalitico);
				veiculo = ajuste.getVeiculo().getCdveiculo();
			}
		}
		if (lista == null || lista.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report = new Report("veiculo/relSintetico");
			report.addParameter("PERIODO",SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
			report.addParameter("LISTA", lista);
			report.addSubReport("SUBSINTETICO", new Report("veiculo/relSinteticoSub"));
			return report;
		}
	}
	
	/**
	 * Metodo de referencia ao DAO para buscar uma lista de ajustes
	 * 
	 * @param filtro
	 * @author Ramon Brazil
	 * @see br.com.linkcom.sined.geral.dao.VeiculokmDAO.findAnalitico
	 */
	public List<Veiculokm> findAnalitico(AnaliticoFiltro filtro){
		return veiculokmDAO.findAnalitico(filtro);
	}

	@Override
	protected ListagemResult<Veiculokm> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Veiculokm> listagemResult = veiculokmDAO.findForExportacao(filtro);
		List<Veiculokm> lista = listagemResult.list();
		
		adicionaKmAtualRegistros(lista);
		
		return listagemResult;
	}
}
