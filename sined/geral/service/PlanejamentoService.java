package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioitem;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Periodo;
import br.com.linkcom.sined.geral.bean.Periodocargo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.PlanejamentoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AcompanhamentoProjetoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRGFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRHFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.CalculoCustoProjetoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoProjeto;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.Histograma;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.Grafico;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PlanejamentoService extends GenericService<Planejamento>{

	private PlanejamentoDAO planejamentoDAO;
	private TarefarecursogeralService tarefarecursogeralService;
	private TarefarecursohumanoService tarefarecursohumanoService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private PlanejamentorecursohumanoService planejamentorecursohumanoService;
	private PlanejamentodespesaService planejamentodespesaService;
	private ValorreferenciaService valorreferenciaService;
	private ContagerencialService contagerencialService;
	private ProjetoService projetoService;
	private MaterialService materialService;
	private PeriodoService periodoService;
	private CargoService cargoService;  
	private TarefaService tarefaService;
	private ProducaodiariaService producaodiariaService;
	private MovimentacaoService movimentacaoService;
	private CalendarioService calendarioService;
	
	public void setPlanejamentodespesaService(
			PlanejamentodespesaService planejamentodespesaService) {
		this.planejamentodespesaService = planejamentodespesaService;
	}
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	public void setPeriodoService(PeriodoService periodoService) {
		this.periodoService = periodoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}	
	public void setValorreferenciaService(
			ValorreferenciaService valorreferenciaService) {
		this.valorreferenciaService = valorreferenciaService;
	}
	public void setPlanejamentoDAO(PlanejamentoDAO planejamentoDAO) {
		this.planejamentoDAO = planejamentoDAO;
	}
	public void setPlanejamentorecursogeralService(
			PlanejamentorecursogeralService planejamentorecursogeralService) {
		this.planejamentorecursogeralService = planejamentorecursogeralService;
	}
	public void setPlanejamentorecursohumanoService(
			PlanejamentorecursohumanoService planejamentorecursohumanoService) {
		this.planejamentorecursohumanoService = planejamentorecursohumanoService;
	}
	public void setTarefarecursogeralService(
			TarefarecursogeralService tarefarecursogeralService) {
		this.tarefarecursogeralService = tarefarecursogeralService;
	}
	public void setTarefarecursohumanoService(
			TarefarecursohumanoService tarefarecursohumanoService) {
		this.tarefarecursohumanoService = tarefarecursohumanoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {
		this.producaodiariaService = producaodiariaService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setCalendarioService(CalendarioService calendarioService) {
		this.calendarioService = calendarioService;
	}	
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#isOutroProjetoComSituacao
	 * @param planejamento
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isOutroProjetoComSituacao(Planejamento planejamento, List<Planejamentosituacao> lista){
		return planejamentoDAO.isOutroProjetoComSituacao(planejamento, lista);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#loadWithSituacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamento> loadWithSituacao(String whereIn) {
		return planejamentoDAO.loadWithSituacao(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#updateSituacao
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Planejamentosituacao situacao) {
		planejamentoDAO.updateSituacao(whereIn, situacao);
	}

	/**
	 * Cria o relat�rio de Or�amento de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.ValorreferenciaService#findByPlanejamento
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#preencheListaVRTarefaMat
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#preencheListaVRTarefaCar
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursogeralService#preencheListaVRPlanejamentoMat
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursohumanoService#preencheListaVRPlanejamentoCar
	 * @see br.com.linkcom.sined.geral.service.PlanejamentodespesaService#preencheListaVRPlanejamentoDespesa
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#agruparContasGerenciais
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#buscarArvoreContaGerencial
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#inserirFolhasNaListaCompleta
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#inserirFolhasNaListaCompleta
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#calcularValores
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#adicionaOutrosCargos
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#adicionaOutrosMateriais
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#totalizar
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#adicionaOutrasDespesas
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createReport(OrcamentoAnaliticoReportFiltro filtro) {		
		Report report = new Report("/projeto/orcamentoSintetico");
		List<AnaliseRecDespBean> listaFolhas =  new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaCompleta =  new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaOutrosMaterial = new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaOutrosCargo = new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaOutrosDespesa = new ArrayList<AnaliseRecDespBean>();
		Money somacargo = new Money();
		Money somamaterial = new Money();
		Money somadespesa = new Money();
		
		List<Valorreferencia> listaValorReferencia = valorreferenciaService.findByPlanejamento(filtro.getPlanejamento());
		
		somamaterial = tarefarecursogeralService.preencheListaVRTarefaMat(filtro, listaFolhas, listaOutrosMaterial, somamaterial, listaValorReferencia);
		
		somacargo = tarefarecursohumanoService.preencheListaVRTarefaCar(filtro, listaFolhas, listaOutrosCargo, somacargo, listaValorReferencia);
		
		somamaterial = planejamentorecursogeralService.preencheListaVRPlanejamentoMat(filtro, listaFolhas, listaOutrosMaterial, somamaterial, listaValorReferencia);
		
		somacargo = planejamentorecursohumanoService.preencheListaVRPlanejamentoCar(filtro, listaFolhas, listaOutrosCargo, somacargo, listaValorReferencia);
		
		somadespesa = planejamentodespesaService.preencheListaVRPlanejamentoDespesa(filtro, listaFolhas, listaOutrosDespesa, somadespesa);
		
		this.agruparContasGerenciais(listaFolhas);
		
		listaCompleta = contagerencialService.buscarArvoreContaGerencial(null);
		
		contagerencialService.inserirFolhasNaListaCompleta(listaCompleta, listaFolhas);
		contagerencialService.calcularValores(listaCompleta, false);
		
		if((listaOutrosCargo != null && listaOutrosCargo.size() > 0) ||
				(listaOutrosMaterial != null && listaOutrosMaterial.size() > 0) ||
				(listaOutrosDespesa != null && listaOutrosDespesa.size() > 0) ){
			AnaliseRecDespBean bean = new AnaliseRecDespBean();
			bean.setIdentificador("XX");
			bean.setNome("Outros");
			bean.setOperacao("D");
			bean.setValor(somacargo.add(somadespesa).add(somamaterial));
			listaCompleta.add(bean);
			
			this.adicionaOutrosCargos(listaCompleta, listaOutrosCargo, somacargo);
			this.adicionaOutrasDespesas(listaCompleta, listaOutrosDespesa, somadespesa);
			this.adicionaOutrosMateriais(listaCompleta, listaOutrosMaterial, somamaterial);
		}
		
		Map<String, Money> mpTotais = contagerencialService.totalizar(listaCompleta, false);
		Money totalCredito = mpTotais.get("totalCredito");
		Money totalDebito = mpTotais.get("totalDebito");
		
		report.setDataSource(listaCompleta);
		
		report.addParameter("totalCredito", totalCredito);
		report.addParameter("totalDebito", totalDebito);
		report.addParameter("saldo", totalCredito.subtract(totalDebito));
		
		report.addParameter("PROJETO", projetoService.load(filtro.getProjeto(), "projeto.nome").getNome());
		report.addParameter("PLANEJAMENTO", this.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao());

		return report;
	}
	
	
	/**
	 * Agrupa os itens do relat�rio de an�lise de receitas e despesas de acordo com o identificador e nome da conta gerencial.
	 *
	 * @param listaFolhas
	 * @author Rodrigo Freitas
	 */
	public void agruparContasGerenciais(List<AnaliseRecDespBean> listaFolhas) {
		AnaliseRecDespBean bean = null;
		AnaliseRecDespBean bean2 = null;
		for (int i = 0; i < listaFolhas.size(); i++) {
			bean = listaFolhas.get(i);
			for (int j = 0; j < listaFolhas.size(); j++) {
				if(i != j){
					bean2 = listaFolhas.get(j);
					if(bean.getIdentificador().equals(bean2.getIdentificador()) && bean.getNome().equals(bean2.getNome())){
						bean.setValor(bean.getValor().add(bean2.getValor()));
						listaFolhas.remove(j);
						j--;
					}
				}
			}
		}
	}

	/**
	 * Cria o relat�rio de acompanhamento de um determinado projeto.
	 * 
 	 * @see br.com.linkcom.sined.geral.service.ProjetoService#load(Projeto)
 	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#load(Planejamento)
 	 * @see br.com.linkcom.sined.geral.service.TarefaService#findTarefasFolha(Planejamento)
	 * @see br.com.linkcom.sined.geral.service.TarefaService#calculaPesoTarefa(List)
	 * @see br.com.linkcom.sined.geral.service.ProducaodiariaService#findByPlanejamento(Planejamento)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findByProjeto(Projeto)
	 * @see br.com.linkcom.sined.geral.service.CalendarioService#getByProjeto(Projeto)
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#montaListaAcompanhamentoProjeto(Planejamento, List, List, List, List, Boolean, Boolean)
	 * @see br.com.linkcom.sined.util.Grafico#geraGraficoAcompanhamentoProjeto(List) 
	 * 
	 * @param filtro
	 * @return report
	 * @throws SinedException - caso o custo do planejamento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public IReport createAcompanhamentoProjetoReport(AcompanhamentoProjetoFiltro filtro) throws Exception{		
		Report report = new Report("/projeto/acompanhamentoProjeto");

		Projeto projeto = projetoService.load(filtro.getProjeto());
		Planejamento planejamento = load(filtro.getPlanejamento());
		
		//Verifica se o custo do planejamento � nulo
		if (planejamento.getCusto() == null) {
			throw new SinedException("O custo do planejamento n�o pode ser nulo.");
		}
		
		//Busca as tarefas folha do projeto/planejamento selecionados no filtro
		List<Tarefa> listaTarefaFolha = tarefaService.findTarefasFolha(planejamento);
		
		//Calcula o peso de cada tarefa baseado na rela��o dura��o da tarefa / dura��o total das tarefas
		listaTarefaFolha = tarefaService.calculaPesoTarefa(listaTarefaFolha);
		
		//Busca todos os registros de produ��o do planejamento
		List<Producaodiaria> listaProducaoDiaria = producaodiariaService.findByPlanejamento(planejamento);
		
		//Busca todos os registros de movimenta��o relacionados ao projeto
		List<Movimentacao> listaMovimentacao = movimentacaoService.findByProjeto(projeto);		
		
		//Busca o calend�rio associado ao projeto, caso exista
		Calendario calendarioProjeto = calendarioService.getByProjeto(projeto);
		Boolean sabadoUtil = false;
		Boolean domingoUtil = false;
		List<Date> listaFeriado = new ArrayList<Date>();
		if (calendarioProjeto != null) {
			sabadoUtil  = calendarioProjeto.getSabadoutil() != null ? calendarioProjeto.getSabadoutil() : false;
			domingoUtil = calendarioProjeto.getDomingoutil() != null ? calendarioProjeto.getDomingoutil() : false;
			//Preenche a lista de feriados, caso exista
			if (calendarioProjeto.getListaCalendarioitem() != null) {
				for (Calendarioitem calendarioitem : calendarioProjeto.getListaCalendarioitem()) {
					listaFeriado.add(calendarioitem.getDtferiado());
				}
			}
		}
		
		List<AcompanhamentoProjeto> listaAcompanhamentoProjeto = AcompanhamentoProjetoService.montaListaAcompanhamentoProjeto(planejamento, listaTarefaFolha, listaProducaoDiaria, listaMovimentacao, listaFeriado, sabadoUtil, domingoUtil);
		
		report.addParameter("PROJETO",projeto.getNome());
		report.addParameter("PLANEJAMENTO",planejamento.getDescricao());
		report.addParameter("GRAFICO",Grafico.geraGraficoAcompanhamentoProjeto(listaAcompanhamentoProjeto));
		report.addParameter("FORMATADOR",new DecimalFormat("#,##0.00"));		
		report.setDataSource(listaAcompanhamentoProjeto);
		return report;
	}	
	
	/**
	 * Cria o relat�rio com um grafico mostrando o uso de um material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#load
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#load
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#findForHistogramaRG
	 * @param filtro
	 * @return report
	 * @author Ramon Brazil
	 */
	
	public IReport createHistogramaRGReport(HistogramaRGFiltro filtro) throws Exception{		
		Report report = new Report("/projeto/histogramaRG");
		List<Histograma> listaHistograma = this.findForHistogramaRG(filtro); 

		report.addParameter("PROJETO",filtro.getProjeto()!= null ? projetoService.load(filtro.getProjeto()).getNome():null);
		report.addParameter("PLANEJAMENTO",filtro.getPlanejamento()!=null ? this.load(filtro.getPlanejamento()).getDescricao():null);
		report.addParameter("MATERIAL",filtro.getMaterial()!= null ? materialService.load(filtro.getMaterial(),"material.nome").getNome():null);
		report.addParameter("DTINICIO", filtro.getDtinicio()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()): null);		
		report.addParameter("DTFIM", filtro.getDtfim()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()): null);		
		report.addParameter("GRAFICO",Grafico.gerahistograma(listaHistograma));
		report.setDataSource(listaHistograma);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#findForHistogramaRG
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Histograma> findForHistogramaRG(HistogramaRGFiltro filtro) {
		return planejamentoDAO.findForHistogramaRG(filtro);
	}
	
	/**
	 * Cria o relat�rio com um grafico mostrando a evolu��o de um projeto.
	 * 
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#load
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#load
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#findForHistogramaRH
	 * @param filtro
	 * @return report
	 * @author Ramon Brazil
	 */
	public IReport createHistogramaRHReport(HistogramaRHFiltro filtro) throws Exception{		
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		Report report = new Report("/projeto/histogramaRH");
		List<Histograma> listaHistograma = this.findForHistogramaRH(filtro);
			
		report.addParameter("PROJETO",filtro.getProjeto()!= null ? projetoService.load(filtro.getProjeto(),"projeto.nome").getNome():null);
		report.addParameter("PLANEJAMENTO",filtro.getPlanejamento()!=null ? this.load(filtro.getPlanejamento(),"planejamento.descricao").getDescricao():null);
		report.addParameter("DTINICIO", filtro.getDtinicio()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()): null);		
		report.addParameter("DTFIM", filtro.getDtfim()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()): null);		
		report.addParameter("GRAFICO",Grafico.gerahistograma(listaHistograma));
		if(filtro.getFuncao()== Boolean.FALSE){
			listaCargo.add(cargoService.load(filtro.getCargo(),"cargo.nome"));
			filtro.setListacargo(listaCargo);  
		}else{
			if(filtro.getListacargo()!=null){
				for (Cargo cargo :filtro.getListacargo()) {
					listaCargo.add(cargoService.load(cargo,"cargo.nome"));
					filtro.setListacargo(listaCargo); 
				}	
			}else{
				listaCargo = cargoService.findByPlanejamento(filtro.getPlanejamento());
				filtro.setListacargo(listaCargo); 
			}
		
		}
		report.addParameter("LISTACARGOS", filtro.getListacargo());
		report.addSubReport("SUBHISTOGRMARH", new Report("/projeto/SubHistogramaRH"));	    
		report.setDataSource(listaHistograma);
	return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#findForHistogramaRH
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Histograma> findForHistogramaRH(HistogramaRHFiltro filtro) {
		return planejamentoDAO.findForHistogramaRH(filtro);
	}
	
	public ModelAndView createHistogramaCSV(WebRequestContext request, FiltroListagemSined filtro, String tipo) throws Exception {
		
		StringBuilder csv = new StringBuilder();
		List<Histograma> listaHistograma = new ArrayList<Histograma>(); 
		
		if (tipo.equals("rh")){
			listaHistograma = this.findForHistogramaRH((HistogramaRHFiltro) filtro);
		}else {
			listaHistograma = this.findForHistogramaRG((HistogramaRGFiltro) filtro);
		}
		
		csv.append("Dia;Planejado;Realizado;");
		csv.append("\n");
		
		for (Histograma histograma: listaHistograma){
			csv.append(histograma.getDia()!=null ? histograma.getDia() : "0");
			csv.append(";");
			csv.append(histograma.getPlanejado()!=null ? histograma.getPlanejado() : "0");
			csv.append(";");
			csv.append(histograma.getRealizado()!=null ? histograma.getRealizado() : "0");
			csv.append(";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "histograma" + tipo + "_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return new ResourceModelAndView(resource);
	}
	
	/**
	 * Adiciona os outros materiais que n�o tem conta gerencial cadastrada.
	 *
	 * @param listaCompleta
	 * @param listaOutrosMaterial
	 * @param somamaterial
	 * @author Rodrigo Freitas
	 */
	private void adicionaOutrosMateriais(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaOutrosMaterial, Money somamaterial) {
		if(listaOutrosMaterial != null && listaOutrosMaterial.size() > 0){
			AnaliseRecDespBean bean = new AnaliseRecDespBean();
			bean.setIdentificador("XX.XX");
			bean.setNome("Outros Materiais");
			bean.setOperacao("D");
			bean.setValor(somamaterial);
			listaCompleta.add(bean);
			this.agruparContasGerenciais(listaOutrosMaterial);
			listaCompleta.addAll(listaOutrosMaterial);
		}
	}
	
	/**
	 * Adiciona os outras despesas que n�o tem conta gerencial cadastrada.
	 *
	 * @param listaCompleta
	 * @param listaOutrosDespesa
	 * @param somadespesa
	 * @author Rodrigo Freitas
	 */
	private void adicionaOutrasDespesas(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaOutrosDespesa, Money somadespesa) {
		if(listaOutrosDespesa != null && listaOutrosDespesa.size() > 0){
			AnaliseRecDespBean bean = new AnaliseRecDespBean();
			bean.setIdentificador("XX.XX");
			bean.setNome("Outras Despesas");
			bean.setOperacao("D");
			bean.setValor(somadespesa);
			listaCompleta.add(bean);
			this.agruparContasGerenciais(listaOutrosDespesa);
			listaCompleta.addAll(listaOutrosDespesa);
		}
		
	}
	
	/**
	 * Adiciona os outros cargos que n�o tem conta gerencial cadastrada.
	 *
	 * @param listaCompleta
	 * @param listaOutrosCargo
	 * @param somacargo
	 * @author Rodrigo Freitas
	 */
	private void adicionaOutrosCargos(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaOutrosCargo, Money somacargo) {
		if(listaOutrosCargo != null && listaOutrosCargo.size() > 0){
			AnaliseRecDespBean bean = new AnaliseRecDespBean();
			bean.setIdentificador("XX.XX");
			bean.setNome("Outros Cargos");
			bean.setOperacao("D");
			bean.setValor(somacargo);
			listaCompleta.add(bean);
			this.agruparContasGerenciais(listaOutrosCargo);
			listaCompleta.addAll(listaOutrosCargo);
		}
	}


	/**
	 * Retorna o c�digo e a descri��o dos planejamentos associados ao projeto para serem utilizados no Flex
	 * Se o projeto for nulo, n�o ser� retornado nenhum planejamento.
	 *  
	 * @see #planejamentoDAO.findByProjetoForFlex(projeto)
	 * @param projeto
	 * @return lista de planejamentos
	 * @author Rodrigo Alvarenga
	 */	
	public List<Planejamento> findByProjetoForFlex(Projeto projeto){
		return planejamentoDAO.findByProjetoForFlex(projeto);
	}
	

	/**
	 * Calcula o custo do projeto de acordo com a vers�o do planejamento e preenche o filtro para ser utilizado
	 * na tela do Flex
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#carregaListaCargoQtde
	 * @param filtro
	 * @return filtro preenchido
	 * @author Rodrigo Alvarenga
	 */	
	public CalculoCustoProjetoFiltro calculaCustoProjeto(CalculoCustoProjetoFiltro filtro){

		List<Periodo> listaCalculoCustoProjetoPeriodoFiltro = new ArrayList<Periodo>();
		List<Periodo> listaCalculoCustoProjetoPeriodoAlocacaoFiltro = this.carregaListaCargoQtde(filtro.getPlanejamento());
		List<Periodocargo> lista = null;
		Periodocargo alocacaoFiltro = null;
		
		filtro.setPlanejamento(load(filtro.getPlanejamento(),"planejamento.dtinicio, planejamento.dtfim, planejamento.tamanhotempo, planejamento.cdplanejamento, planejamento.descricao"));
		
		Integer numPeriodos = new Long(Math.round(new Double((SinedDateUtils.calculaDiferencaDias(filtro.getPlanejamento().getDtinicio(), filtro.getPlanejamento().getDtfim())/7)+0.5))).intValue();
		
		for (Periodo calculoCustoProjetoPeriodoFiltro : listaCalculoCustoProjetoPeriodoAlocacaoFiltro) {
			
			calculoCustoProjetoPeriodoFiltro.setListaCargo(distribuirRecursos(numPeriodos, calculoCustoProjetoPeriodoFiltro.getTotal()));
			calculoCustoProjetoPeriodoFiltro.setPlanejamento(filtro.getPlanejamento());
			
			lista = new ArrayList<Periodocargo>();
			
			for (int j = 0; j < calculoCustoProjetoPeriodoFiltro.getListaCargo().size(); j++) {
				alocacaoFiltro = new Periodocargo();
				alocacaoFiltro.setNumero(j+1);
				alocacaoFiltro.setaQtde(calculoCustoProjetoPeriodoFiltro.getListaCargo().get(j).intValue(), filtro.getPlanejamento().getTamanhotempo());
				lista.add(alocacaoFiltro);
			}
			calculoCustoProjetoPeriodoFiltro.setListaPeriodocargo(lista);
			
			listaCalculoCustoProjetoPeriodoFiltro.add(calculoCustoProjetoPeriodoFiltro);
		}
		filtro.setListaCalculoCustoProjetoPeriodoFiltro(listaCalculoCustoProjetoPeriodoFiltro);
		return filtro;
	}
	
	/**
	 * Distribui os recursos de acordo com o prazo e total de HH.
	 *
	 * @param prazo
	 * @param totalHH
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Double> distribuirRecursos(int prazo, double totalHH) {
		List<Double> listaDistribuicao = new ArrayList<Double>();
		
		Double p = (double) prazo+1;
		Double t = totalHH;		
		
		Double a = (6*t)/( -1*p*p*p + p );
		Double b = -a*p;
		
		for(double x=1; x<=prazo; x++){
			double y = Math.ceil(a*x*x+b*x);
			listaDistribuicao.add(y);			
		}
		
		return listaDistribuicao;
	}
	
	/**
	 * Salva o custo do projeto de acordo com a vers�o do planejamento
	 *  
	 * @param filtro
	 * @return 
	 * @author Rodrigo Alvarenga
	 */	
	public CalculoCustoProjetoFiltro salvaCustoProjeto(CalculoCustoProjetoFiltro filtro){
		periodoService.deleteWherePlanejamento(filtro.getPlanejamento());
		for (Periodo periodo : filtro.getListaCalculoCustoProjetoPeriodoFiltro()) {
			periodo.setCdperiodo(null);
			for (Periodocargo periodocargo : periodo.getListaPeriodocargo()) {
				periodocargo.setCdperiodocargo(null);
				periodocargo.setPeriodo(null);
			}
			periodoService.saveOrUpdate(periodo);
			Collections.sort(periodo.getListaPeriodocargo(),new Comparator<Periodocargo>(){
				public int compare(Periodocargo o1, Periodocargo o2) {
					return o1.getNumero().compareTo(o2.getNumero());
				}
			});
		}
		return filtro;
	}
	
	/**
	 * V� se o planejamento j� tem o c�lculo dos recursos, sen�o tiver chama o c�lculo.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#calculaCustoProjeto
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public CalculoCustoProjetoFiltro buscaPlanejamento(CalculoCustoProjetoFiltro filtro){
		List<Periodo> listaPeriodo = periodoService.findByPlanejamento(filtro.getPlanejamento());	
		if (listaPeriodo != null && listaPeriodo.size() > 0) {
			filtro.setListaCalculoCustoProjetoPeriodoFiltro(listaPeriodo);
			return filtro;
		} else {
			return this.calculaCustoProjeto(filtro);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#carregaListaCargoQtde
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Periodo> carregaListaCargoQtde(Planejamento planejamento){
		return planejamentoDAO.carregaListaCargoQtde(planejamento);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#getListaRecursosReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<RecursosReportBean> getListaRecursosReport(OrcamentoAnaliticoReportFiltro filtro){
		return planejamentoDAO.getListaRecursosReport(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#getListaRecursosReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createReportOrcamentoAnalitico(OrcamentoAnaliticoReportFiltro filtro) {
		Report report = new Report("/projeto/orcamentoAnalitico");	
		List<RecursosReportBean> lista = this.getListaRecursosReport(filtro);
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#findCargos
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cargo> findCargos(Planejamento planejamento) {
		return planejamentoDAO.findCargos(planejamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#findMateriais
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findMateriais(Planejamento planejamento) {
		return planejamentoDAO.findMateriais(planejamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#findDespesa 
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecimentotipo> findDespesa(Planejamento planejamento) {
		return planejamentoDAO.findDespesa(planejamento);
	}
	
	/**
	 * Retorna uma lista de planejamentos de um determinado projeto que est�o em uma determinada situa��o.
	 *
	 * @param projeto
	 * @param planejamentoSituacao
	 * @return lista de Planejamento
	 * @throws SinedException - quando um dos par�metros for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Planejamento> findByProjetoSituacao(Projeto projeto, Planejamentosituacao planejamentoSituacao) {
		return planejamentoDAO.findByProjetoSituacao(projeto, planejamentoSituacao);
	}
	
	/**
	 * Retorna uma lista de planejamentos autorizados de um determinado projeto.
	 *
	 * @param projeto
	 * @return lista de Planejamento
	 * @throws SinedException - quando um dos par�metros for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Planejamento> findAutorizadosByProjeto(Projeto projeto) {
		return planejamentoDAO.findByProjetoSituacao(projeto, Planejamentosituacao.AUTORIZADA);
	}
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#haveSolicitacaocompra
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveSolicitacaocompra(Planejamento planejamento) {
		return planejamentoDAO.haveSolicitacaocompra(planejamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#loadForCopiaProjeto(Planejamento)
	 * @param planejamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Planejamento loadForCopiaProjeto(Planejamento planejamento){
		return planejamentoDAO.loadForCopiaProjeto(planejamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentoDAO#materialProjeto
	 * 
	 * @param material
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean materialProjeto(Material material, Planejamento planejamento) {
		return planejamentoDAO.materialProjeto(material, planejamento);
	}
	
	public Planejamento loadWithOrcamento(Planejamento planejamento) {
		return planejamentoDAO.loadWithOrcamento(planejamento);
	}
	
	public Planejamento loadWithProjto(Planejamento planejamento) {
		return planejamentoDAO.loadWithProjto(planejamento);
	}
	
	public Solicitacaocompra gerarSolicitacaocompra(Solicitacaocompra solicitacaocompra, String whereIn) {
		
		List<Planejamentorecursogeral> listaRG = planejamentorecursogeralService.findByPlanejamento(whereIn);
		List<Solicitacaocompra> listaSolicitacao = new ArrayList<Solicitacaocompra>();
		Solicitacaocompra sol = null;
		Projeto projeto = null;
		
		for (Planejamentorecursogeral tg : listaRG) {
			sol = new Solicitacaocompra();
			
			sol.setMaterial(tg.getMaterial());
			sol.setQtde(new Double(tg.getQtde()));
			sol.setDtlimite(tg.getPlanejamento().getDtinicio());
			
			sol.setPlanejamento(tg.getPlanejamento());
			sol.setCdplanejamentorecursogeral(tg.getCdplanejamentorecursogeral());
			
			sol.setGerado(tg.getListaSolicitacaocompraorigem() != null && tg.getListaSolicitacaocompraorigem().size() > 0);
			if(projeto == null && tg.getPlanejamento() != null){
				projeto = tg.getPlanejamento().getProjeto();
			}
			listaSolicitacao.add(sol);
		}	
		
		solicitacaocompra.setSolicitacoes(listaSolicitacao);
		solicitacaocompra.setProjeto(projeto);
		return solicitacaocompra;
	}
	
	/**
	* M�todo que cria requisi��es de material de acordo com os recursos gerais do planejamento
	*
	* @param requisicaomaterial
	* @param whereIn
	* @return
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public Requisicaomaterial gerarRequisicaomaterial(Requisicaomaterial requisicaomaterial, String whereIn) {
		List<Planejamentorecursogeral> listaRG = planejamentorecursogeralService.findByPlanejamento(whereIn);
		List<Requisicaomaterial> lista = new ArrayList<Requisicaomaterial>();
		Requisicaomaterial req = null;
		Projeto projeto = null;
		
		for (Planejamentorecursogeral tg : listaRG) {
			req = new Requisicaomaterial();
			
			req.setMaterial(tg.getMaterial());
			req.setQtde(new Double(tg.getQtde()));
			req.setDtlimite(tg.getPlanejamento().getDtinicio());
			
			req.setPlanejamento(tg.getPlanejamento());
			req.setCdplanejamentorecursogeral(tg.getCdplanejamentorecursogeral());
			
			req.setGerado(SinedUtil.isListNotEmpty(tg.getListaRequisicaoorigem()));
			if(projeto == null && tg.getPlanejamento() != null){
				projeto = tg.getPlanejamento().getProjeto();
			}
			lista.add(req);
		}	
		
		requisicaomaterial.setRequisicoes(lista);
		requisicaomaterial.setProjeto(projeto);
		return requisicaomaterial;
	}
	
	/**
	* M�todo que cria ordem de compra de acordo com os recursos gerais do planejamento
	*
	* @param ordemcompra
	* @param whereIn
	* @return
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public Ordemcompra gerarOrdemcompra(Ordemcompra ordemcompra, String whereIn) {
		List<Planejamentorecursogeral> listaRG = planejamentorecursogeralService.findByPlanejamento(whereIn);
		List<Ordemcompramaterial> listaItens = new ArrayList<Ordemcompramaterial>();
		Ordemcompramaterial ocm = null;
		
		for (Planejamentorecursogeral tg : listaRG) {
			ocm = new Ordemcompramaterial();
			
			ocm.setMaterial(tg.getMaterial());
			ocm.setUnidademedida(tg.getMaterial().getUnidademedida());
			ocm.setQtdpedida(new Double(tg.getQtde() != null ? tg.getQtde() : 0d));
			ocm.setQtdefrequencia(1d);
			ocm.setFrequencia(new Frequencia(Frequencia.UNICA));
			ocm.setFrequenciatrans(new Frequencia(Frequencia.UNICA));
			ocm.setValorunitario(tg.getMaterial().getValorcusto());
			
			if(ocm.getQtdpedida() != null && ocm.getQtdefrequencia() != null && 
					ocm.getValorunitario() != null){
				ocm.setValor(ocm.getQtdpedida() * ocm.getQtdefrequencia() * ocm.getValorunitario());
			}
			
			ocm.setPlanejamento(tg.getPlanejamento());
			ocm.setPlanejamentorecursogeral(tg);
			
			ocm.setGerado(SinedUtil.isListNotEmpty(tg.getListaOrdemcompramaterial()));
			if(tg.getPlanejamento() != null){
				ocm.setProjeto(tg.getPlanejamento().getProjeto());
			}
			listaItens.add(ocm);
		}	
		
		ordemcompra.setListaMaterial(listaItens);
		ordemcompra.setWhereInPlanejamento(whereIn);
		return ordemcompra;
	}
	
	public List<Planejamento> findByProjeto(Projeto projeto){
		return planejamentoDAO.findByProjeto(projeto);
	}
}
