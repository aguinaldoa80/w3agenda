package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendavalorcampoextra;
import br.com.linkcom.sined.geral.dao.VendavalorcampoextraDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaValorCampoExtraWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendavalorcampoextraService extends GenericService<Vendavalorcampoextra> {
	
	private VendavalorcampoextraDAO vendavalorcampoextraDAO;
	
	public void setVendavalorcampoextraDAO(VendavalorcampoextraDAO vendavalorcampoextraDAO) {
		this.vendavalorcampoextraDAO = vendavalorcampoextraDAO;
	}

	/**
	 * Busca todos os campos extras de um determinado {@link Venda}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Vendavalorcampoextra> findByVenda(Venda bean) {

		return vendavalorcampoextraDAO.findByVenda(bean);
	}

	public List<VendaValorCampoExtraWSBean> toWSList(List<Vendavalorcampoextra> listaVendaValorCampoExtra){
		List<VendaValorCampoExtraWSBean> lista = new ArrayList<VendaValorCampoExtraWSBean>();
		if(SinedUtil.isListNotEmpty(listaVendaValorCampoExtra)){
			for(Vendavalorcampoextra vvce: listaVendaValorCampoExtra){
				VendaValorCampoExtraWSBean bean = new VendaValorCampoExtraWSBean();
				bean.setCampoextrapedidovendatipo(ObjectUtils.translateEntityToGenericBean(vvce.getCampoextrapedidovendatipo()));
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vvce.getVenda()));
				bean.setCdvendavalorcampoextra(vvce.getCdvendavalorcampoextra());
				bean.setValor(vvce.getValor());
				
				lista.add(bean);
			}
			
		}
		return lista;
	}
}
