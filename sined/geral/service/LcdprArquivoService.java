package br.com.linkcom.sined.geral.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ibm.icu.text.DecimalFormat;

import br.com.linkcom.lcdpr.registros.Lcdpr;
import br.com.linkcom.lcdpr.registros.Registro0000;
import br.com.linkcom.lcdpr.registros.Registro0010;
import br.com.linkcom.lcdpr.registros.Registro0030;
import br.com.linkcom.lcdpr.registros.Registro0040;
import br.com.linkcom.lcdpr.registros.Registro0045;
import br.com.linkcom.lcdpr.registros.Registro0050;
import br.com.linkcom.lcdpr.registros.Registro9999;
import br.com.linkcom.lcdpr.registros.RegistroQ100;
import br.com.linkcom.lcdpr.registros.RegistroQ200;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.LcdprArquivo;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.ParticipantePropriedadeRural;
import br.com.linkcom.sined.geral.bean.PropriedadeRural;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.LcdprArquivoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LcdprArquivoService extends GenericService<LcdprArquivo> {
	
	private LcdprArquivoDAO lcdprArquivoDAO;
	private EmpresaproprietarioService empresaproprietarioService;
	private PropriedadeRuralService propriedadeRuralService;
	private ContaService contaService;
	private MovimentacaoService movimentacaoService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	
	public void setEmpresaproprietarioService(EmpresaproprietarioService empresaproprietarioService) {this.empresaproprietarioService = empresaproprietarioService;}
	public void setPropriedadeRuralService(PropriedadeRuralService propriedadeRuralService) {this.propriedadeRuralService = propriedadeRuralService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setLcdprArquivoDAO(LcdprArquivoDAO lcdprArquivoDAO) {this.lcdprArquivoDAO = lcdprArquivoDAO;}
	
	public Resource geraArquivo(LcdprArquivoFiltro filtro, WebRequestContext request) throws SinedException{
		Lcdpr lcdpr = new Lcdpr();
			
		//come�a com 3 pois os registros 0000, 0010 e 0030 s�o fixos (1:1)
		int contadorLinhas = 3;
		
		List<Empresaproprietario> listaEmpresaproprietario = empresaproprietarioService.loadForLcdpr(filtro);
		Colaborador proprietarioRural = listaEmpresaproprietario.get(0).getColaborador();
		
		if(listaEmpresaproprietario.get(0).getColaborador().getColaboradorcontabilista() == null){
			throw new SinedException("O propriet�rio rural deve ter um contador cadastrado no registro de colaborador.");
		}
		
		lcdpr.setRegistro0000(this.createRegistro0000(filtro, listaEmpresaproprietario, proprietarioRural));
		
		contadorLinhas = this.calcularQuantidadeLinhas(lcdpr, contadorLinhas);		
		
		lcdpr.setRegistro9999(this.createRegistro9999(filtro, listaEmpresaproprietario.get(0).getColaborador(), contadorLinhas));
		
		String nome = "lcdpr_" + SinedUtil.datePatternForReport() + ".txt";
		byte[] bytes = null;
		//converte para o padrao UTF-8 exigido pela Receita
		try {
			bytes = lcdpr.toString().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			bytes = lcdpr.toString().getBytes();
			e.printStackTrace();
		}

		this.saveArquivoLcdpr(filtro, nome, bytes, proprietarioRural);
		
		return new Resource("text/plain", nome, bytes);
	}

	private Registro0000 createRegistro0000(LcdprArquivoFiltro filtro, List<Empresaproprietario> listaEmpresaproprietario, Colaborador proprietarioRural) {
		Registro0000 registro0000 = new Registro0000();
		List<PropriedadeRural> listaPropriedadeRural = propriedadeRuralService.loadForLcdpr(filtro);
		
		registro0000.setReg(Registro0000.REG);
		registro0000.setNome_esc("LCDPR");
		//CODIGO DA VERSAO
		registro0000.setCod_ver("0013");
		
		if(proprietarioRural.getCpf() != null){
			registro0000.setCpf(proprietarioRural.getCpf().getValue());			
		}
		if(filtro.getColaborador() != null && filtro.getColaborador().getNome() != null){
			registro0000.setNome(filtro.getColaborador().getNome());			
		}
		if(filtro.getIndicadorInicioDoPeriodo() != null){
			registro0000.setInd_sit_ini_per(filtro.getIndicadorInicioDoPeriodo().getId());			
		}
		if(filtro.getIndicadorSituacaoEspecial() != null){
			registro0000.setSit_especial(filtro.getIndicadorSituacaoEspecial().getId());
		}
		if(filtro.getIndicadorSituacaoEspecial() != null && filtro.getIndicadorSituacaoEspecial().getId() == 1 && filtro.getDtFalecimento() != null){
			registro0000.setDt_sit_esp(filtro.getDtFalecimento());
		}
		if(filtro.getDtinicio() != null){
			registro0000.setDt_ini(filtro.getDtinicio());			
		}
		if(filtro.getDtfim() != null){
			registro0000.setDt_fin(filtro.getDtfim());			
		}
		
		registro0000.setRegistro0010(this.createRegistro0010(filtro));
		registro0000.setRegistro0030(this.createRegistro0030(filtro, proprietarioRural));
		registro0000.setListaRegistro0040(this.createRegistro0040(filtro, listaPropriedadeRural));
		registro0000.setListaRegistro0050(this.createRegistro0050(filtro));
		
		HashMap<String, RegistroQ200> mapaRegistroQ200 = new HashMap<String, RegistroQ200>();
		registro0000.setListaRegistroQ100(this.createRegistroQ100(listaPropriedadeRural, filtro, mapaRegistroQ200));
		registro0000.setListaRegistroQ200(this.createRegistroQ200(mapaRegistroQ200));
		
		return registro0000;
	}

	private Registro0010 createRegistro0010(LcdprArquivoFiltro filtro) {
		Registro0010 registro0010 = new Registro0010();
		
		registro0010.setReg(Registro0010.REG);
		if(filtro.getFormaApuracao() != null){
			registro0010.setForma_apur(filtro.getFormaApuracao().getId());			
		}
		
		return registro0010;
	}
	
	private Registro0030 createRegistro0030(LcdprArquivoFiltro filtro, Colaborador proprietarioRural) {
		Registro0030 registro0030 = new Registro0030();
		
		registro0030.setReg(Registro0030.REG);
		if(proprietarioRural != null && proprietarioRural.getEndereco() != null){
			if(proprietarioRural.getEndereco().getLogradouro() != null){
				registro0030.setEndereco(proprietarioRural.getEndereco().getLogradouro());				
			}else {
				throw new SinedException("O propriet�rio rural deve ter um logradouro cadastrado em seu endere�o.");
			}
			
			if(proprietarioRural.getEndereco().getNumero() != null){
				registro0030.setNum(proprietarioRural.getEndereco().getNumero());				
			}else {
				throw new SinedException("O propriet�rio rural deve ter um n�mero cadastrado em seu endere�o.");
			}
			
			registro0030.setCompl(proprietarioRural.getEndereco().getComplemento());
			
			if(proprietarioRural.getEndereco().getBairro() != null){
				registro0030.setBairro(proprietarioRural.getEndereco().getBairro());								
			}else {
				throw new SinedException("O propriet�rio rural deve ter um bairro cadastrado em seu endere�o.");
			}
			
			if(proprietarioRural != null && proprietarioRural.getEndereco() != null &&
					proprietarioRural.getEndereco().getMunicipio() != null && proprietarioRural.getEndereco().getMunicipio().getUf() != null &&
					proprietarioRural.getEndereco().getMunicipio().getUf().getSigla() != null){
				registro0030.setUf(proprietarioRural.getEndereco().getMunicipio().getUf().getSigla());				
			}
			if(proprietarioRural != null && proprietarioRural.getEndereco() != null && 
					proprietarioRural.getEndereco().getMunicipio() != null && proprietarioRural.getEndereco().getMunicipio().getCdibge() != null){
				registro0030.setCod_mun(proprietarioRural.getEndereco().getMunicipio().getCdibge());				
			}
			if (proprietarioRural.getEndereco().getCep() != null){
				registro0030.setCep(proprietarioRural.getEndereco().getCep().getValue());				
			}else {
				throw new SinedException("O propriet�rio rural deve ter um CEP cadastrado em seu endere�o.");
			}
		}
		
		if(SinedUtil.isListNotEmpty(proprietarioRural.getListaTelefone())){
			for (Telefone telefone : proprietarioRural.getListaTelefone()) {
				if(telefone.getTelefonetipo().getCdtelefonetipo() == 1){
					String telefonePrincipal = telefone.getTelefone();
					telefonePrincipal = telefonePrincipal.replace(".", "");
					telefonePrincipal = telefonePrincipal.replace("-", "");
					telefonePrincipal = telefonePrincipal.replace("(", "");
					telefonePrincipal = telefonePrincipal.replace(")", "");
					telefonePrincipal = telefonePrincipal.replace(" ", "");
					registro0030.setNum_tel(telefonePrincipal);
				}
			}			
			if(registro0030.getNum_tel() == null){
				String primeiroTelefone = proprietarioRural.getListaTelefone().iterator().next().getTelefone();
				primeiroTelefone = primeiroTelefone.replace(".", "");
				primeiroTelefone = primeiroTelefone.replace("-", "");
				primeiroTelefone = primeiroTelefone.replace("(", "");
				primeiroTelefone = primeiroTelefone.replace(")", "");
				primeiroTelefone = primeiroTelefone.replace(" ", "");
				registro0030.setNum_tel(primeiroTelefone);
			}
		}
		
		if(proprietarioRural.getEmail() == null){
			throw new SinedException("O propriet�rio rural deve ter um email cadastrado.");
		}else{
			registro0030.setEmail(proprietarioRural.getEmail());
		}
		
		return registro0030;
	}
	
	private List<Registro0040> createRegistro0040(LcdprArquivoFiltro filtro, List<PropriedadeRural> listaPropriedadeRural) {
		List<Registro0040> listaRegistro0040 =  new ArrayList<Registro0040>();
		
		for (PropriedadeRural propriedadeRural : listaPropriedadeRural) {
			Registro0040 registro0040 = new Registro0040();
			
			registro0040.setReg(Registro0040.REG);
			if(propriedadeRural.getCodigoImovel() != null){
				registro0040.setCod_imovel(propriedadeRural.getCodigoImovel().toString());				
			}
			
			registro0040.setPais("BR");
			registro0040.setMoeda("BRL");
			
			if(propriedadeRural.getCafir() != null){
				registro0040.setCad_itr(propriedadeRural.getCafir());				
			}
			if(propriedadeRural.getCaepf() != null){
				registro0040.setCaepf(propriedadeRural.getCaepf());				
			}
			if(propriedadeRural.getEmpresa().getInscricaoestadual() != null){
				registro0040.setInscr_estadual(propriedadeRural.getEmpresa().getInscricaoestadual());				
			}
			if(propriedadeRural.getNome() != null){
				registro0040.setNome_imovel(propriedadeRural.getNome());				
			}
			
			if (SinedUtil.isListNotEmpty(propriedadeRural.getListaEndereco())) {
				Endereco endereco = propriedadeRural.getEnderecoWithPrioridade();

				if (endereco != null){
					registro0040.setEndereco(endereco.getLogradouro());
					registro0040.setNum(endereco.getNumero());
					registro0040.setCompl(endereco.getComplemento());
					registro0040.setBairro(endereco.getBairro());
					if (endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null) {
						registro0040.setUf(endereco.getMunicipio().getUf().getSigla());
					}
					if (endereco.getMunicipio() != null && endereco.getMunicipio().getCdibge() != null) {
						registro0040.setCod_mun(endereco.getMunicipio().getCdibge());
					}
					if(endereco.getCep() != null){
						registro0040.setCep(endereco.getCep().getValue());						
					}
				}
			}
			if(propriedadeRural.getTipoExploracao() != null){
				registro0040.setTipo_exploracao(propriedadeRural.getTipoExploracao().getId());				
			}
			
			if(SinedUtil.isListNotEmpty(propriedadeRural.getListaParticipantePropriedadeRural())){
				List<ParticipantePropriedadeRural> pariticpantes = propriedadeRural.getListaParticipantePropriedadeRural();
				for (ParticipantePropriedadeRural participante : pariticpantes) {
					if(participante.getColaborador().getCdpessoa().equals(filtro.getColaborador().getCdpessoa())){
						String percentual = new DecimalFormat("#0.00").format(participante.getPercentual());
						percentual = percentual.replace(",", "");
						registro0040.setParticipacao(percentual);
					}
				}
			}
			if(SinedUtil.isListNotEmpty(propriedadeRural.getListaParticipantePropriedadeRural())){
				registro0040.setListaRegistro0045(this.createRegistro0045(filtro, propriedadeRural));				
			}
			
			listaRegistro0040.add(registro0040);
		}
		return listaRegistro0040;
	}
	private List<Registro0045> createRegistro0045(LcdprArquivoFiltro filtro, PropriedadeRural propriedadeRural) {
		List<Registro0045> listaRegistro0045 =  new ArrayList<Registro0045>();
			
		for (ParticipantePropriedadeRural participante  : propriedadeRural.getListaParticipantePropriedadeRural()) {
			if(!participante.getColaborador().getCdpessoa().equals(filtro.getColaborador().getCdpessoa())){
				
				Registro0045 registro0045 = new Registro0045();
				
				registro0045.setReg(Registro0045.REG);
				
				if(propriedadeRural.getCodigoImovel() != null){
					registro0045.setCod_imovel(propriedadeRural.getCodigoImovel().toString());				
				}
				if(propriedadeRural.getTipoTerceiros() != null){
					registro0045.setTipo_contraparte(propriedadeRural.getTipoTerceiros().getId());
				}
				if(participante.getColaborador().getCpf() != null){
					registro0045.setId_contraparte(participante.getColaborador().getCpf().getValue());
				}
				if(participante.getColaborador().getNome() != null){
					registro0045.setNome_contraparte(participante.getColaborador().getNome());
				}
				if(participante.getPercentual() != null){
					String percentual = new DecimalFormat("#0.00").format(participante.getPercentual());
					percentual = percentual.replace(",", "");
					registro0045.setPerc_contraparte(percentual);
				}
				
				listaRegistro0045.add(registro0045);
			}
		}	

		return listaRegistro0045;
	}
	
	private List<Registro0050> createRegistro0050(LcdprArquivoFiltro filtro) {
		List<Registro0050> listaRegistro0050 = new ArrayList<Registro0050>();
		List<Conta> listaContasProdutor = contaService.loadForLcdpr(filtro);
		
		for (Conta conta : listaContasProdutor) {
			Registro0050 registro0050 = new Registro0050();
			
			registro0050.setReg(Registro0050.REG);
			
			if(conta.getCdconta().toString().length() == 1){
				registro0050.setCod_conta("00"+conta.getCdconta().toString());		
			}else if(conta.getCdconta().toString().length() == 2){
				registro0050.setCod_conta("0"+conta.getCdconta().toString());
			}else if(conta.getCdconta().toString().length() == 3){
				registro0050.setCod_conta(conta.getCdconta().toString());
			}/*else {
				throw new SinedException("Erro no valor cod_conta (Registro0050).");
			}*/
			
			registro0050.setPais_cta("BR");
			
			if(conta.getBanco() != null && conta.getBanco().getNumero() != null){
				registro0050.setBanco(conta.getBanco().getNumero().toString());
			}
			if(conta.getBanco() != null && conta.getBanco().getNome() != null){
				registro0050.setNome_banco(conta.getBanco().getNome());
			}
			if(conta.getAgencia() != null){
				registro0050.setAgencia(conta.getAgencia());
			}
			if(conta.getNumero() != null && conta.getDvnumero() != null){
				registro0050.setNum_conta(conta.getNumero() + conta.getDvnumero());
			}else if(conta.getNumero() != null){
				registro0050.setNum_conta(conta.getNumero());
			}
			
			listaRegistro0050.add(registro0050);
		}
		
		return listaRegistro0050;
	}
	
	private List<RegistroQ100> createRegistroQ100(List<PropriedadeRural> listaPropriedadeRural, LcdprArquivoFiltro filtro, HashMap<String, RegistroQ200> mapaRegistroQ200) {
		List<Movimentacao> listaMovimentacoes = movimentacaoService.loadForLcdpr(filtro);
		List<RegistroQ100> listaRegistroQ100 = new ArrayList<RegistroQ100>();
		
		HashMap<Integer, Integer> mapaPropriedadesPrincipaisEmpresas = new HashMap<Integer, Integer>();
		
		HashMap<Integer, Conta> mapaContas = new HashMap<Integer, Conta>();
		
		//busca o imovel marcado como principal (por empresa) para ser usado no campo cod_imovel em todas as movimentacoes daquela empresa
		for (PropriedadeRural propriedadeRural : listaPropriedadeRural) {
			if(mapaPropriedadesPrincipaisEmpresas.containsKey(propriedadeRural.getEmpresa().getCdpessoa())){
				continue;
			}else {
				if(propriedadeRural.getPrincipal() != null && propriedadeRural.getPrincipal()){
					mapaPropriedadesPrincipaisEmpresas.put(propriedadeRural.getEmpresa().getCdpessoa(), propriedadeRural.getCodigoImovel());
				}				
			}
		}
		
		for (Movimentacao movimentacao : listaMovimentacoes) {
			//caso haja mais que 1 documento de origem, deve ser criado um registro Q100 para cada
			if(SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigem())){
				if(movimentacao.getListaMovimentacaoorigem().size() > 1){
					//verifica se h� diferen�a de centavos entre o valor total dos documentos, e o valor proporcional de cada registro q100 criado
					Money valorTotalDocumentos = new Money();
					for (Movimentacaoorigem origem : movimentacao.getListaMovimentacaoorigem()) {						
						valorTotalDocumentos = valorTotalDocumentos.add(origem.getDocumento().getAux_documento().getValoratual());
					}
					
					Money valorTotalCalculado = new Money();
					for (Movimentacaoorigem movimentacaoOrigem : movimentacao.getListaMovimentacaoorigem()) {
						Money porcentagem = movimentacaoOrigem.getDocumento().getAux_documento().getValoratual().divide(valorTotalDocumentos);
						Money valor = movimentacao.getValor().multiply(porcentagem);
						movimentacaoOrigem.setValorQ100(valor);
						valorTotalCalculado = valorTotalCalculado.add(valor);
					}
					
					valorTotalCalculado = new Money().getValorArredondadoDuasCasasDecimais(valorTotalCalculado);
					Money diferenca = new Money();
					
					if(movimentacao.getValor().getValue().doubleValue() > valorTotalCalculado.getValue().doubleValue()){
						diferenca = movimentacao.getValor().subtract(valorTotalCalculado);
						Money valorUltimaParcela = movimentacao.getListaMovimentacaoorigem().get(movimentacao.getListaMovimentacaoorigem().size()-1).getDocumento().getAux_documento().getValoratual().add(diferenca);
						movimentacao.getListaMovimentacaoorigem().get(movimentacao.getListaMovimentacaoorigem().size()-1).setValorQ100(valorUltimaParcela);
					}else if (valorTotalCalculado.getValue().doubleValue() < movimentacao.getValor().getValue().doubleValue()){
						diferenca = valorTotalCalculado.subtract(movimentacao.getValor());
						Money valorUltimaParcela = movimentacao.getListaMovimentacaoorigem().get(movimentacao.getListaMovimentacaoorigem().size()-1).getDocumento().getAux_documento().getValoratual().subtract(diferenca);
						movimentacao.getListaMovimentacaoorigem().get(movimentacao.getListaMovimentacaoorigem().size()-1).setValorQ100(valorUltimaParcela);
					}
				}
				
				for (Movimentacaoorigem movimentacaoOrigem : movimentacao.getListaMovimentacaoorigem()) {					
					RegistroQ100 registroQ100 = preencheRegistroQ100(movimentacao, movimentacaoOrigem, mapaContas, mapaPropriedadesPrincipaisEmpresas, mapaRegistroQ200); 
					listaRegistroQ100.add(registroQ100);
				}
			}else {
				RegistroQ100 registroQ100 = preencheRegistroQ100(movimentacao, null, mapaContas, mapaPropriedadesPrincipaisEmpresas, mapaRegistroQ200); 
				listaRegistroQ100.add(registroQ100);
			}
		}
		
		return listaRegistroQ100;
	}
	
	private RegistroQ100 preencheRegistroQ100 (Movimentacao movimentacao, Movimentacaoorigem movimentacaoOrigem, HashMap<Integer, Conta> mapaContas, HashMap<Integer, Integer> mapaPropriedadesPrincipaisEmpresas, HashMap<String, RegistroQ200> mapaRegistroQ200){
		RegistroQ100 registroQ100 = new RegistroQ100();
		
		registroQ100.setReg(RegistroQ100.REG);
		if(movimentacao.getDtmovimentacao() != null){
			registroQ100.setData(movimentacao.getDtmovimentacao());				
		}
		
		registroQ100.setCod_imovel(mapaPropriedadesPrincipaisEmpresas.get(movimentacao.getEmpresa().getCdpessoa()).toString());				
		
		//codigo da conta
		//prenchendo com 000 se for pagamento em dinheiro e 999 se for cheque ou antecipacao. Se for outra coisa, pegando o cdconta
		if(movimentacao.getFormapagamento().getCdformapagamento().equals(1) || movimentacao.getFormapagamento().getCdformapagamento().equals(3)){
			registroQ100.setCod_conta("000");
		}else if(movimentacao.getFormapagamento().getCdformapagamento().equals(5) || movimentacao.getFormapagamento().getCdformapagamento().equals(6)){
			if(movimentacao.getConta().getCdconta().toString().length() == 1){
				registroQ100.setCod_conta("00" + movimentacao.getConta().getCdconta().toString());				
			}else if(movimentacao.getConta().getCdconta().toString().length() == 2){
				registroQ100.setCod_conta("0" + movimentacao.getConta().getCdconta().toString());				
			}else if(movimentacao.getConta().getCdconta().toString().length() == 3){
				registroQ100.setCod_conta(movimentacao.getConta().getCdconta().toString());				
			}/*else {
				throw new SinedException("Erro no valor cod_conta (RegistroQ100).");
			}*/
		}else {
			registroQ100.setCod_conta("999");
		}
		
		if(movimentacaoOrigem != null && movimentacaoOrigem.getDocumento() != null){
			if(movimentacaoOrigem.getDocumento().getNumero() != null){
				registroQ100.setNum_doc(movimentacaoOrigem.getDocumento().getNumero());						
			}else if(movimentacao.getChecknum() != null){
				registroQ100.setNum_doc(movimentacao.getChecknum());
			}			
		}else {
			if(movimentacao.getChecknum() != null){
				registroQ100.setNum_doc(movimentacao.getChecknum());
			}
		}
		
		//tipo_doc
		if(movimentacaoOrigem != null){
			if(movimentacaoOrigem.getDocumento() != null && movimentacaoOrigem.getDocumento().getDocumentotipo() != null &&
					movimentacaoOrigem.getDocumento().getDocumentotipo().getTipoDocumentoLCDPR() != null){
				registroQ100.setTipo_doc(movimentacaoOrigem.getDocumento().getDocumentotipo().getTipoDocumentoLCDPR().getId());
			}else {
				registroQ100.setTipo_doc(6);
			}
		}else {
			registroQ100.setTipo_doc(6);
		}
		
		if(movimentacao.getHistorico() != null){
			registroQ100.setHist(movimentacao.getHistorico());
		}
		
		//idparticipante TEM QUE SER VERIFICADO
		if(movimentacaoOrigem != null){
			if(movimentacaoOrigem.getDocumento() != null && movimentacaoOrigem.getDocumento().getPessoa() != null){
				if(movimentacaoOrigem.getDocumento().getPessoa().getCpf() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDocumento().getPessoa().getCpf().getValue());
				}else if(movimentacaoOrigem.getDocumento().getPessoa().getCnpj() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDocumento().getPessoa().getCnpj().getValue());
				}
			}else if(movimentacaoOrigem.getDespesaviagemacerto() != null && movimentacaoOrigem.getDespesaviagemacerto().getColaborador() != null){
				if(movimentacaoOrigem.getDespesaviagemacerto().getColaborador().getCpf() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDespesaviagemacerto().getColaborador().getCpf().getValue());
				}else if(movimentacaoOrigem.getDespesaviagemacerto().getColaborador().getCnpj() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDespesaviagemacerto().getColaborador().getCnpj().getValue());
				}	
			}else if(movimentacaoOrigem.getDespesaviagemadiantamento() != null && movimentacaoOrigem.getDespesaviagemadiantamento().getColaborador() != null){
				if(movimentacaoOrigem.getDespesaviagemadiantamento().getColaborador().getCpf() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDespesaviagemadiantamento().getColaborador().getCpf().getValue());
				}else if(movimentacaoOrigem.getDespesaviagemadiantamento().getColaborador().getCnpj() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getDespesaviagemadiantamento().getColaborador().getCnpj().getValue());
				}
			}else if(movimentacaoOrigem.getAgendamento() != null && movimentacaoOrigem.getAgendamento().getPessoa() != null){
				if(movimentacaoOrigem.getAgendamento().getPessoa().getCpf() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getAgendamento().getPessoa().getCpf().getValue());
				}else if(movimentacaoOrigem.getAgendamento().getPessoa().getCnpj() != null){
					registroQ100.setId_partic(movimentacaoOrigem.getAgendamento().getPessoa().getCnpj().getValue());
				}
			}	
		}else {
			//verificar AQUI!!! como pegar o id do participante se for movimentacao avulsa?
			registroQ100.setId_partic(null);
		}
		
		//tipo_lanc
		if(movimentacaoOrigem != null && movimentacaoOrigem.getDocumento() != null && movimentacaoOrigem.getDocumento().getDocumentotipo() != null &&
				Boolean.TRUE.equals(movimentacaoOrigem.getDocumento().getDocumentotipo().getAntecipacao())){
			registroQ100.setTipo_lanc(3);
		}else if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().getCdtipooperacao() != null){
			if(movimentacao.getTipooperacao().getCdtipooperacao().equals(1)){
				registroQ100.setTipo_lanc(2);
			}else if(movimentacao.getTipooperacao().getCdtipooperacao().equals(2)){
				registroQ100.setTipo_lanc(1);
			}					
		}
		
		//logica para pegar a porcentagem de cada documento em cada movimentacao
		if(movimentacaoOrigem != null && movimentacao.getListaMovimentacaoorigem().size() > 1){			
			if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().getCdtipooperacao() != null){
				if(movimentacao.getTipooperacao().getCdtipooperacao().equals(1)){
					registroQ100.setVl_entrada("0");
					
					String valorSaida = movimentacaoOrigem.getValorQ100().toString();
					valorSaida = valorSaida.replace(".", "");
					valorSaida = valorSaida.replace(",", "");
					registroQ100.setVl_saida(valorSaida);
				}else if(movimentacao.getTipooperacao().getCdtipooperacao().equals(2)){
					String valorEntrada = movimentacaoOrigem.getValorQ100().toString();
					valorEntrada = valorEntrada.replace(".", "");
					valorEntrada = valorEntrada.replace(",", "");
					registroQ100.setVl_entrada(valorEntrada);
					
					registroQ100.setVl_saida("0");
				}
			}	
		}else {
			if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().getCdtipooperacao() != null){
				if(movimentacao.getTipooperacao().getCdtipooperacao().equals(1)){
					registroQ100.setVl_entrada("0");
					
					String valorSaida = movimentacao.getValor().toString();
					valorSaida = valorSaida.replace(".", "");
					valorSaida = valorSaida.replaceAll(",", "");
					registroQ100.setVl_saida(valorSaida);
				}else if(movimentacao.getTipooperacao().getCdtipooperacao().equals(2)){
					String valorEntrada = movimentacao.getValor().toString();
					valorEntrada = valorEntrada.replace(".", "");
					valorEntrada = valorEntrada.replace(",", "");
					registroQ100.setVl_entrada(valorEntrada);
					
					registroQ100.setVl_saida("0");
				}
			}
		}
		
		//calcula saldo
		Conta conta = new Conta();
		Money saldo = new Money();
		if(!mapaContas.containsKey(movimentacao.getConta().getCdconta())){
			conta = movimentacao.getConta();
			saldo = contaService.calculaSaldoAtual(movimentacao.getConta(), SinedDateUtils.addDiasData(movimentacao.getDtmovimentacao(), -1));
		}else{
			conta = mapaContas.get(movimentacao.getConta().getCdconta());
			saldo = conta.getSaldoatual();
		}	
			
		if(movimentacaoOrigem != null && movimentacaoOrigem.getDocumento() != null){
			if(movimentacao.getTipooperacao().getCdtipooperacao().equals(1)){
				saldo = saldo.subtract(movimentacaoOrigem.getDocumento().getAux_documento().getValoratual());						
			}else if(movimentacao.getTipooperacao().getCdtipooperacao().equals(2)){
				saldo = saldo.add(movimentacaoOrigem.getDocumento().getAux_documento().getValoratual());
			}				
		}else {
			if(movimentacao.getTipooperacao().getCdtipooperacao().equals(1)){
				saldo = saldo.subtract(movimentacao.getValor());						
			}else if(movimentacao.getTipooperacao().getCdtipooperacao().equals(2)){
				saldo = saldo.add(movimentacao.getValor());
			}
		}
		String saldoString = saldo.toString();
		saldoString = saldoString.replace(",", "");
		saldoString = saldoString.replace(".", "");
		saldoString = saldoString.replace("-", "");
		registroQ100.setSld_fin(saldoString);
		if(saldo.getValue().doubleValue() >= 0){
			registroQ100.setNat_sld_fin("P");
		}else {
			registroQ100.setNat_sld_fin("N");
		}
		
		conta.setSaldoatual(saldo);
		mapaContas.put(conta.getCdconta(), conta);
		
		//preenche o registro Q200 (totalizador mensal)
		if(!mapaRegistroQ200.containsKey(SinedDateUtils.getMesAno(movimentacao.getDtmovimentacao()))){
			RegistroQ200 registroQ200 = new RegistroQ200();
			registroQ200.setReg(RegistroQ200.REG);
			String mesAno = SinedDateUtils.getMesAno(movimentacao.getDtmovimentacao());
			mesAno = mesAno.replace("/", "");
			registroQ200.setMes(mesAno);
			registroQ200.setVl_entrada(registroQ100.getVl_entrada());
			registroQ200.setVl_saida(registroQ100.getVl_saida());

			Money saldoFinal = new Money(registroQ200.getVl_entrada()).subtract(new Money(registroQ200.getVl_saida()));
			
			String saldoFinalString = saldoFinal.toString();
			saldoFinalString = saldoFinalString.replace(",", "");
			saldoFinalString = saldoFinalString.replace(".", "");
			saldoFinalString = saldoFinalString.replace("-", "");
			
			registroQ200.setSld_fin(saldoFinalString);
			if(saldoFinal.getValue().doubleValue() >= 0){
				registroQ200.setNat_sld_fin("P");
			}else {
				registroQ200.setNat_sld_fin("N");
			}
			
			mapaRegistroQ200.put(SinedDateUtils.getMesAno(movimentacao.getDtmovimentacao()), registroQ200);	
		}else {
			RegistroQ200 registroQ200 = mapaRegistroQ200.get(SinedDateUtils.getMesAno(movimentacao.getDtmovimentacao()));
				
			StringBuilder valorEntradaQ200 = new StringBuilder(registroQ200.getVl_entrada());
			Money valorEntradaQ200Money = new Money();
			if(!"0".equals(valorEntradaQ200.toString())){	
				valorEntradaQ200.insert(valorEntradaQ200.length()-2, ".");
				valorEntradaQ200Money = new Money(valorEntradaQ200.toString());	
				
			}
			StringBuilder valorEntradaQ100 = new StringBuilder(registroQ100.getVl_entrada());
			
			Money valorEntradaQ100Money = new Money();
			if(!"0".equals(valorEntradaQ100.toString())){
				valorEntradaQ100.insert(valorEntradaQ100.length()-2, ".");
				valorEntradaQ100Money = new Money(valorEntradaQ100.toString());					
			}
			
			valorEntradaQ200Money = valorEntradaQ200Money.add(valorEntradaQ100Money);
			
			String valorEntradaQ200String = valorEntradaQ200Money.toString();
			valorEntradaQ200String = valorEntradaQ200String.replace(",", "");
			valorEntradaQ200String = valorEntradaQ200String.replace(".", "");
			valorEntradaQ200String = valorEntradaQ200String.replace("-", "");
			
			registroQ200.setVl_entrada(valorEntradaQ200String);
			
			StringBuilder valorSaidaQ200 = new StringBuilder(registroQ200.getVl_saida());
			Money valorSaidaQ200Money = new Money();
			
			if(!"0".equals(valorSaidaQ200.toString())){
				valorSaidaQ200.insert(valorSaidaQ200.length()-2, ".");
				valorSaidaQ200Money = new Money(valorSaidaQ200.toString());	
			}
			
			StringBuilder valorSaidaQ100 = new StringBuilder(registroQ100.getVl_saida());
			Money valorSaidaQ100Money = new Money();
			
			if(!"0".equals(valorSaidaQ100.toString())){
				valorSaidaQ100.insert(valorSaidaQ100.length()-2, ".");
				valorSaidaQ100Money = new Money(valorSaidaQ100.toString());		
			}
			
			valorSaidaQ200Money = valorSaidaQ200Money.add(valorSaidaQ100Money);
			
			String valorSaidaQ200String = valorSaidaQ200Money.toString();
			valorSaidaQ200String = valorSaidaQ200String.replace(",", "");
			valorSaidaQ200String = valorSaidaQ200String.replace(".", "");
			valorSaidaQ200String = valorSaidaQ200String.replace("-", "");
			
			registroQ200.setVl_saida(valorSaidaQ200String);
			
			Money saldoFinal = valorEntradaQ200Money.subtract(valorSaidaQ200Money);
			String saldoFinalString = saldoFinal.toString();
			saldoFinalString = saldoFinalString.replace(",", "");
			saldoFinalString = saldoFinalString.replace(".", "");
			saldoFinalString = saldoFinalString.replace("-", "");
			
			registroQ200.setSld_fin(saldoFinalString);
			
			if(saldoFinal.getValue().doubleValue() >= 0){
				registroQ200.setNat_sld_fin("P");
			}else {
				registroQ200.setNat_sld_fin("N");
			}
			
			mapaRegistroQ200.put(SinedDateUtils.getMesAno(movimentacao.getDtmovimentacao()), registroQ200);
		}

		return registroQ100;
	}
	
	private List<RegistroQ200> createRegistroQ200(HashMap<String, RegistroQ200> mapaRegistroQ200) {
		List<RegistroQ200> listaRegistroQ200 = new ArrayList<RegistroQ200>(mapaRegistroQ200.values());
		
		return listaRegistroQ200;
	}
	
	private Registro9999 createRegistro9999(LcdprArquivoFiltro filtro, Colaborador colaborador, int contador) {
		Registro9999 registro9999 = new Registro9999();
		
		registro9999.setReg(Registro9999.REG);
		
		if(colaborador.getColaboradorcontabilista() != null){
			if(colaborador.getColaboradorcontabilista().getRazaosocial() != null){
				registro9999.setIdent_nom(colaborador.getColaboradorcontabilista().getRazaosocial());				
			}
			
			if(colaborador.getColaboradorcontabilista().getCnpj() != null){
				registro9999.setIdent_cpf_cnpj(colaborador.getColaboradorcontabilista().getCnpj().getValue());
			}else if(colaborador.getColaboradorcontabilista().getCpf() != null){
				registro9999.setIdent_cpf_cnpj(colaborador.getColaboradorcontabilista().getCpf().getValue());
			}
			
			if(colaborador.getCrccontabilista() != null){
				registro9999.setInd_crc(colaborador.getCrccontabilista());	
			}
						
			if(colaborador.getColaboradorcontabilista().getEmail() != null){
				registro9999.setEmail(colaborador.getColaboradorcontabilista().getEmail());
			}
			
			if(SinedUtil.isListNotEmpty(colaborador.getColaboradorcontabilista().getListaTelefone())){
				for (Telefone telefone : colaborador.getColaboradorcontabilista().getListaTelefone()) {
					if(telefone.getTelefonetipo().getCdtelefonetipo() == 1){
						String telefonePrincipal = telefone.getTelefone();
						telefonePrincipal = telefonePrincipal.replace(".", "");
						telefonePrincipal = telefonePrincipal.replace("-", "");
						telefonePrincipal = telefonePrincipal.replace("(", "");
						telefonePrincipal = telefonePrincipal.replace(")", "");
						telefonePrincipal = telefonePrincipal.replace(" ", "");
						registro9999.setFone(telefonePrincipal);
					}
				}
				if(registro9999.getFone() == null){
					String primeroTelefone = colaborador.getColaboradorcontabilista().getListaTelefone().iterator().next().getTelefone();
					primeroTelefone = primeroTelefone.replace(".", "");
					primeroTelefone = primeroTelefone.replace("-", "");
					primeroTelefone = primeroTelefone.replace("(", "");
					primeroTelefone = primeroTelefone.replace(")", "");
					primeroTelefone = primeroTelefone.replace(" ", "");
					registro9999.setFone(primeroTelefone);
				}
			}
		}
		contador += 1;
		
		registro9999.setQtd_lin(contador);
		
		return registro9999;
	}
	
	private int calcularQuantidadeLinhas(Lcdpr lcdpr, int contadorLinhas) {
		for (int i = 0; i < lcdpr.getRegistro0000().getListaRegistro0040().size(); i++) {
			contadorLinhas += 1;
			for (int j = 0; j < lcdpr.getRegistro0000().getListaRegistro0040().get(i).getListaRegistro0045().size(); j++) {
				contadorLinhas += 1;
			}
		}
		for (int i = 0; i < lcdpr.getRegistro0000().getListaRegistro0050().size(); i++) {
			contadorLinhas += 1;
		}
		for (int i = 0; i < lcdpr.getRegistro0000().getListaRegistroQ100().size(); i++) {
			contadorLinhas += 1;
		}
		for (int i = 0; i < lcdpr.getRegistro0000().getListaRegistroQ200().size(); i++) {
			contadorLinhas += 1;
		}
		
		return contadorLinhas;
	}
	
	private void saveArquivoLcdpr(LcdprArquivoFiltro filtro, String nome, byte[] bytes, Colaborador proprietarioRural) {
		Arquivo arquivo = new Arquivo(bytes, nome, "text/plain");
		
		LcdprArquivo lcdprArquivo = this.findLcdprArquivo(filtro);;
		
		if(lcdprArquivo == null){
			lcdprArquivo = new LcdprArquivo();
		}
		
		lcdprArquivo.setDtinicio(filtro.getDtinicio());
		lcdprArquivo.setDtfim(filtro.getDtfim());
		lcdprArquivo.setColaborador(proprietarioRural);
		lcdprArquivo.setArquivo(arquivo);
		
		arquivoDAO.saveFile(lcdprArquivo, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		this.saveOrUpdate(lcdprArquivo);
	}
	
	private LcdprArquivo findLcdprArquivo(LcdprArquivoFiltro filtro) {
		return lcdprArquivoDAO.findLcdprArquivo(filtro);
	}
	
}