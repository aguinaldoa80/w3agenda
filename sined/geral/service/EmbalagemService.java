package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Embalagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.dao.EmbalagemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmbalagemService extends GenericService<Embalagem>{
	
	private EmbalagemDAO embalagemDAO;
	
	public void setEmbalagemDAO(EmbalagemDAO embalagemDAO) {
		this.embalagemDAO = embalagemDAO;
	}

	public Embalagem findEmbalagemByMaterialCodigoBarras(Material material, String codigoBarras, Unidademedida unidademedidaCodigoBarras) {
		return embalagemDAO.findEmbalagemByMaterialCodigoBarras(material, codigoBarras, unidademedidaCodigoBarras);
	}

}
