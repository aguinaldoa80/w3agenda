package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.PlanejamentorecursogeralDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PlanejamentorecursogeralService extends GenericService<Planejamentorecursogeral>{

	private PlanejamentorecursogeralDAO planejamentorecursogeralDAO;
	
	public void setPlanejamentorecursogeralDAO(
			PlanejamentorecursogeralDAO planejamentorecursogeralDAO) {
		this.planejamentorecursogeralDAO = planejamentorecursogeralDAO;
	}
	
	public List<Planejamentorecursogeral> preencheListaPlanRecGeral(Collection<Tarefarecursogeral> lista){
		List<Planejamentorecursogeral> listaPlan = new ArrayList<Planejamentorecursogeral>();
		Planejamentorecursogeral pg = null;
		for (Tarefarecursogeral tg : lista) {
			pg = new Planejamentorecursogeral();
			pg.setCdplanejamentorecursogeral(tg.getCdtarefarecursogeral().equals(0) ? null : tg.getCdtarefarecursogeral());
			pg.setMaterial(tg.getMaterial());
			pg.setQtde(tg.getQtde());
			
			listaPlan.add(pg);
		}
		return listaPlan;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentorecursogeralDAO#findByPlanejamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findByPlanejamento(Planejamento planejamento) {
		return planejamentorecursogeralDAO.findByPlanejamento(planejamento);					
	}
	

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentorecursogeralDAO#findByPlanejamento
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findByPlanejamento(String whereIn) {
		return planejamentorecursogeralDAO.findByPlanejamento(whereIn);
	}
	
	/**
	 * Preenche a lista de valores de refer�ncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursogeralService#findByPlanejamento
	 * @param planejamento
	 * @param listaReferencia
	 * @param listaTodas
	 * @author Rodrigo Freitas
	 */
	public void valoresPlanejamentoGeral(Planejamento planejamento, List<Valorreferencia> listaReferencia, List<Valorreferencia> listaTodas) {
		Valorreferencia valorreferencia;
		List<Planejamentorecursogeral> listaPlanejamentoGeral = this.findByPlanejamento(planejamento);
		for (Planejamentorecursogeral planejamentorecursogeral : listaPlanejamentoGeral) {
			valorreferencia = new Valorreferencia();
			valorreferencia.setMaterial(planejamentorecursogeral.getMaterial());
			valorreferencia.setPlanejamento(planejamento);
			valorreferencia.setValor(new Money(planejamentorecursogeral.getMaterial().getValorvenda()));
			
			if (!listaTodas.contains(valorreferencia) && !listaReferencia.contains(valorreferencia)) {
				listaReferencia.add(valorreferencia);
			}
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentorecursogeralDAO#findForOrcamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findForOrcamento(Planejamento planejamento) {
		return planejamentorecursogeralDAO.findForOrcamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de refr�ncia a partir de uma lista de planejamentoRecursoGeral.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursogeralService#findForOrcamento
	 * @see br.com.linkcom.sined.geral.bean.Valorreferencia#getValor
	 * @param filtro
	 * @param listaFolhas
	 * @param listaOutrosMaterial
	 * @param somamaterial
	 * @param listaValorReferencia
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money preencheListaVRPlanejamentoMat(OrcamentoAnaliticoReportFiltro filtro, List<AnaliseRecDespBean> listaFolhas,
			List<AnaliseRecDespBean> listaOutrosMaterial, Money somamaterial, List<Valorreferencia> listaValorReferencia) {
		AnaliseRecDespBean bean;
		Money valor;
		List<Planejamentorecursogeral> listaPlanejamentoGeral = this.findForOrcamento(filtro.getPlanejamento());
		for (Planejamentorecursogeral planejamentorecursogeral : listaPlanejamentoGeral) {
			bean = new AnaliseRecDespBean();
			if (planejamentorecursogeral.getMaterial().getContagerencial() != null) {
				bean.setIdentificador(planejamentorecursogeral.getMaterial().getContagerencial().getVcontagerencial().getIdentificador());
				bean.setNome(planejamentorecursogeral.getMaterial().getContagerencial().getNome());
				bean.setOperacao(planejamentorecursogeral.getMaterial().getContagerencial().getTipooperacao().getSigla());
				valor = Valorreferencia.getValor(planejamentorecursogeral.getMaterial(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(planejamentorecursogeral.getQtde())) : new Money());
				
				listaFolhas.add(bean);
			} else {
				bean.setIdentificador("XX.XX.XX");
				bean.setNome(planejamentorecursogeral.getMaterial().getNome());
				bean.setOperacao("D");
				valor = Valorreferencia.getValor(planejamentorecursogeral.getMaterial(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(planejamentorecursogeral.getQtde())) :  new Money());
				somamaterial = somamaterial.add(valor != null ? valor.multiply(new Money(planejamentorecursogeral.getQtde())) :  new Money());
				
				listaOutrosMaterial.add(bean);
			}
		}
		return somamaterial;
	}

	/**
	* M�todo que busca os recursos gerais do planejamento
	*
	* @see br.com.linkcom.sined.geral.service.PlanejamentorecursogeralService#findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial)
	*
	* @param whereIn
	* @return
	* @since 15/03/2016
	* @author Luiz Fernando
	*/
	public List<Planejamentorecursogeral> findForBloquearQtdeCompra(Projeto projeto, String whereInMaterial) {
		if(projeto == null || projeto.getCdprojeto() == null)
			throw new SinedException("par�metro inv�lido.");
		
		return findForBloquearQtdeCompra(projeto.getCdprojeto().toString(), whereInMaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PlanejamentorecursogeralDAO#findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial)
	*
	* @param whereInProjeto
	* @param whereInMaterial
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	public List<Planejamentorecursogeral> findForBloquearQtdeCompra(String  whereInProjeto, String whereInMaterial) {
		return planejamentorecursogeralDAO.findForBloquearQtdeCompra(whereInProjeto, whereInMaterial);
	}
}
