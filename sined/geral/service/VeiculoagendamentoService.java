package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.dao.VeiculoagendamentoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculoagendamentoService extends GenericService<Veiculoordemservico>{
	
	private VeiculoagendamentoDAO veiculoagendamentoDAO;
			
	public void setVeiculoagendamentoDAO(
			VeiculoagendamentoDAO veiculoagendamentoDAO) {
		this.veiculoagendamentoDAO = veiculoagendamentoDAO;
	}
	
	@Override
	public void delete(Veiculoordemservico bean) {
		
		Veiculoordemservico fullBean = veiculoagendamentoDAO.loadForDelete(bean);
		Veiculoordemservico inspecao = veiculoagendamentoDAO.findInspecaoDoAgendamento(fullBean);
		if(inspecao != null)
			throw new SinedException("Agendamento n�o pode ser exclu�do pois sua inspe��o j� foi realizada.");
		else			
			veiculoagendamentoDAO.delete(bean);	
	}

	/**
	 * M�todo com refer�ncia DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeAgendamentoData(Veiculoordemservico bean) {
		return veiculoagendamentoDAO.existeAgendamentoData(bean);
	}	
}
