package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.ComissionamentoPorFaixasDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ComissionamentoPorFaixasFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentoPorFaixasReportBean;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentoPorFaixasTotalizadorReportBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class ComissionamentoPorFaixasService extends
		GenericService<Documentocomissao> {
	private ComissionamentoPorFaixasDAO comissionamentoPorFaixasDAO;
	private VendamaterialService vendamaterialService;

	public void setComissionamentoPorFaixasDAO(
			ComissionamentoPorFaixasDAO comissionamentoPorFaixasDAO) {
		this.comissionamentoPorFaixasDAO = comissionamentoPorFaixasDAO;
	}
	public void setVendamaterialService(
			VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}

	public List<Documentocomissao> findForSomaTotaisListagem(
			ComissionamentoPorFaixasFiltro filtro) {
		return comissionamentoPorFaixasDAO.findForSomaTotaisListagem(filtro);
	}

	public void preencheTotalizadores(ComissionamentoPorFaixasFiltro filtro) {
		List<Documentocomissao> lista = this.findForSomaTotaisListagem(filtro);

		if (SinedUtil.isListNotEmpty(lista)) {
			Money totalVendas = new Money(), totalComissao = new Money();
			List<Venda> vendasTotalizadas = new ArrayList<Venda>();
			Map<Venda,List<Vendamaterial> > mapa = new HashMap<Venda, List<Vendamaterial> >();
			for (Documentocomissao doc : lista) {
				if(mapa.containsValue(doc.getVenda().getListavendamaterial())){
					doc.getVenda().setListavendamaterial(mapa.get(doc.getVenda().getListavendamaterial()));
				}else{
					doc.getVenda().setListavendamaterial(vendamaterialService.findForTotalizacaoVenda(doc.getVenda()));
					mapa.put(doc.getVenda(), doc.getVenda().getListavendamaterial());
				}	
				if(!vendasTotalizadas.contains(doc.getVenda())){
					vendasTotalizadas.add(doc.getVenda());
					totalVendas = totalVendas.add(doc.getVenda().getTotalvenda());
				}
				totalComissao = totalComissao.add(doc.getValorcomissao());
			}

			filtro.setQuantidade(lista.size());
			filtro.setTotalVendas(totalVendas);
			filtro.setTotalComissao(totalComissao);
		}
	}

	public IReport gerarRelatorioComissionament(
			ComissionamentoPorFaixasFiltro filtro) {
		Report report = new Report("");
		report = new Report("/rh/comissionamentoPorFaixas");
		
		List<ComissionamentoPorFaixasReportBean> lista = new ArrayList<ComissionamentoPorFaixasReportBean>();
		
		
		List<Documentocomissao> listaComissao = comissionamentoPorFaixasDAO.findForPdf(filtro);
		String whereInDocumentoComissao = CollectionsUtil.listAndConcatenate(listaComissao, "cddocumentocomissao", ",");
		Map<Integer, Money> mapaTotalVendasPorColaborador = new HashMap<Integer, Money>();
		for(Documentocomissao bean: listaComissao){
			ComissionamentoPorFaixasReportBean reportBean = new ComissionamentoPorFaixasReportBean();
			reportBean.setComissionamento(bean.getComissionamento().getNome());
			if(bean.getVenda() != null){
				bean.getVenda().setListavendamaterial(vendamaterialService.findForTotalizacaoVenda(bean.getVenda()));
				reportBean.setVenda(bean.getVenda().getCdvenda().toString());
				reportBean.setDtVenda(bean.getVenda().getDtvenda());
				reportBean.setValorVenda(bean.getVenda().getTotalvenda());
				if(bean.getVenda().getColaborador() != null && bean.getVenda().getColaborador().getCdpessoa() != null){
					reportBean.setVendedor(bean.getVenda().getColaborador().getNome());
					if(!mapaTotalVendasPorColaborador.containsKey(bean.getVenda().getColaborador().getCdpessoa())){
						mapaTotalVendasPorColaborador.put(bean.getVenda().getColaborador().getCdpessoa(), new Money());
					}
					Money valor = mapaTotalVendasPorColaborador.get(bean.getVenda().getColaborador().getCdpessoa());
					mapaTotalVendasPorColaborador.put(bean.getVenda().getColaborador().getCdpessoa(), valor.add(reportBean.getValorVenda()));
				}
			}
			if(bean.getVendamaterial() != null){
				if(bean.getVendamaterial().getMaterial() != null){
					reportBean.setIdentificacaoProduto(bean.getVendamaterial().getMaterial().getIdentificacaoOuCdmaterial());
					reportBean.setMaterial(bean.getVendamaterial().getMaterial().getNome());
				}
				if(bean.getVendamaterial().getFaixaMarkupNome() != null){
					reportBean.setMarkup(bean.getVendamaterial().getFaixaMarkupNome().getNome());
				}
			}
			reportBean.setValorComissao(bean.getValorcomissao());
			reportBean.setPago(bean.getRepassado()? "Sim": "N�o");
			
			lista.add(reportBean);
		}
		
		List<ComissionamentoPorFaixasTotalizadorReportBean> listaTotalizacao = this.findTotalComissaoPorColaborador(whereInDocumentoComissao);
		for(ComissionamentoPorFaixasTotalizadorReportBean bean: listaTotalizacao){
			bean.setValorTotalVenda(mapaTotalVendasPorColaborador.get(bean.getCdPessoa()));
		}

		report.setDataSource(lista);
		Report subreport = new Report("/rh/comissionamentoPorFaixas_sub");
		report.addParameter("LISTATOTALIZACAO", listaTotalizacao);
		report.addSubReport("SUB_TOTALIZACAO_POR_VENDEDOR", subreport);
		return report;
	}
	
	public List<ComissionamentoPorFaixasTotalizadorReportBean> findTotalComissaoPorColaborador(String whereInDocumentoComissao){
		return comissionamentoPorFaixasDAO.findTotalComissaoPorColaborador(whereInDocumentoComissao);
	}
	
	@Override
	public Resource generateCSV(FiltroListagem filtro, String[] fields) {
		String cabecalho = "Venda;Data da venda;Identifica��o do Produto;Produto;Markup;Comissionamento;Valor da Venda;Valor da Comiss�o;Pago;\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
		
		filtro.setNumberOfPages(Integer.MAX_VALUE);
		ListagemResult<Documentocomissao> listagemResult = this.findForListagem(filtro);
		
		for(Documentocomissao doc : listagemResult.list()) {
			csv.append(doc.getVenda().getCdvenda() + ";")
				.append(pattern.format(doc.getVenda().getDtvenda()) + ";")
				.append(doc.getVenda().getColaborador().getNome() + ";")
				.append((doc.getVendamaterial() != null ? doc.getVendamaterial().getMaterial().getCdmaterial() : "") + ";")
				.append((doc.getVendamaterial() != null ? doc.getVendamaterial().getMaterial().getNome() : "") + ";")
				.append((doc.getVendamaterial() != null ? doc.getVendamaterial().getFaixaMarkupNome().getNome() : "") + ";")
				.append(doc.getComissionamento().getNome() + ";")
				.append(doc.getVenda().getTotalvenda() + ";")
				.append(doc.getValorcomissao() + ";")
				.append((doc.getRepassado() != null && doc.getRepassado() ? "Sim" : "N�o") + ";\n");
		}
		
		return new Resource("text/csv", "comissionamentoPorFaixas.csv", csv.toString().getBytes());
	}
}
