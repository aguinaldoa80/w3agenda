package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.SaldoIpi;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.geral.dao.SaldoIpiDAO;

public class SaldoIpiService extends GenericService<SaldoIpi> {

	private SaldoIpiDAO saldoIpiDAO;
	
	public void setSaldoIpiDAO(SaldoIpiDAO saldoIpiDAO) {
		this.saldoIpiDAO = saldoIpiDAO;
	}
	
	public Double getLastMesAnoSaldoIpiByEmpresa(Empresa empresa, CreditoDebitoEnum tipoUtilizacao) {
		SaldoIpi saldoIpi = saldoIpiDAO.getLastMesAnoSaldoIpiByEmpresa(empresa, tipoUtilizacao);
		
		if (saldoIpi != null) {
			return saldoIpi.getValor().getValue().doubleValue();
		} else {
			return 0d;
		}
	}

	public SaldoIpi getSaldoIpiByMesAnoEmpresaTipoUtilizacao(SaldoIpi bean) {
		return saldoIpiDAO.getSaldoIpiByMesAnoEmpresaTipoUtilizacao(bean);
	}
}