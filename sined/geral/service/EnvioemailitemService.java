package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.dao.EnvioemailitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EnvioemailitemService extends GenericService<Envioemailitem>{
		
	private EnvioemailitemDAO envioemailitemDAO;
	
	public void setEnvioemailitemDAO(EnvioemailitemDAO envioemailitemDAO) {
		this.envioemailitemDAO = envioemailitemDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @param envioemail
	 * @author Thiago Clemente
	 * 
	 */
	public List<Envioemailitem> find(Envioemail envioemail){
		return envioemailitemDAO.find(envioemail);
	}
	
	/**
	 * M�todo que atualiza os emails lidos
	 * 
	 * @param cdenvioemail
	 * @param email
	 * @author Thiago Clemente
	 * 
	 */
	public void confirmacaoEmail(Integer cdenvioemail, String email){
		envioemailitemDAO.confirmacaoEmail(cdenvioemail, email);
	}
	
	private static EnvioemailitemService instance;
	public static EnvioemailitemService getInstance(){
		if (instance==null){
			instance = Neo.getObject(EnvioemailitemService.class);
		}
		return instance;
	}	
}