package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.dao.TipotaxaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipotaxaService extends GenericService<Tipotaxa>{

	private TipotaxaDAO tipotaxaDAO;
	
	public void setTipotaxaDAO(TipotaxaDAO tipotaxaDAO) {
		this.tipotaxaDAO = tipotaxaDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 14/03/2012
	 * @param tipotaxa
	 * @return
	 */
	public Tipotaxa getTipoTaxa(Tipotaxa tipotaxa){
		return tipotaxaDAO.getTipoTaxa(tipotaxa);
	}
	
	public boolean isApos(Tipotaxa tipotaxa){
		return Tipotaxa.DESAGIO.equals(tipotaxa) || Tipotaxa.MULTA.equals(tipotaxa) || Tipotaxa.JUROS.equals(tipotaxa) || Tipotaxa.JUROS_MES.equals(tipotaxa);
	}
	
	public List<Tipotaxa> findTipotaxaContareceberForDocumentotipo(){
		List<Tipotaxa> lista = new ArrayList<Tipotaxa>();
		lista.add(Tipotaxa.TAXAMOVIMENTO);
		lista.add(Tipotaxa.TAXABAIXA_DESCONTO);
		return lista;
	}	
}
