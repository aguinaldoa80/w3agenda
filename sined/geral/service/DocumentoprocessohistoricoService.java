package br.com.linkcom.sined.geral.service;

import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoprocessohistorico;
import br.com.linkcom.sined.geral.dao.DocumentoprocessohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoprocessohistoricoService extends GenericService<Documentoprocessohistorico>{

	private DocumentoprocessohistoricoDAO documentoprocessohistoricoDAO;
	
	public void setDocumentoprocessohistoricoDAO(DocumentoprocessohistoricoDAO documentoprocessohistoricoDAO) {
		this.documentoprocessohistoricoDAO = documentoprocessohistoricoDAO;
	}

	public void deleteByIds(String cdsHistorico, String cdsArquivo) {
		documentoprocessohistoricoDAO.deleteByIds(cdsHistorico, cdsArquivo);
	}
	
	public List<Documentoprocessohistorico> removeHistoricoArquivo(List<Documentoprocessohistorico> listaHistorico){
		Iterator<Documentoprocessohistorico> listaInterator = listaHistorico.iterator();
		Boolean isRemove = false;
		String cdsArquivo = CollectionsUtil.listAndConcatenate(listaHistorico, "arquivo.cdarquivo", ",");
		String cdsHistorico = CollectionsUtil.listAndConcatenate(listaHistorico, "cddocumentoprocessohistorico", ",");
		
		for (Documentoprocessohistorico historico : listaHistorico) {	
			if(historico!=null && historico.getArquivo()!=null && historico.getArquivo().getCdarquivo()!=null){
				isRemove = true;
				while(listaInterator.hasNext()){
					if(listaInterator.next().getCddocumentoprocessohistorico().equals(historico.getCddocumentoprocessohistorico()))
						listaInterator.remove();											
				}
				if(listaHistorico.isEmpty())
					break;
			}
		}
		
		if(isRemove){
			this.deleteByIds(cdsHistorico,cdsArquivo);			
		}
		
		return listaHistorico;
	}
	
	public Boolean getMessenger (List<Documentoprocessohistorico> listaHistorico){
		Boolean isRemove = false;
		for (Documentoprocessohistorico historico : listaHistorico) {	
			if(historico.getArquivo()!=null && historico.getArquivo().getCdarquivo()!=null)
				isRemove = true;
		}
		return isRemove;		
	}
	
}