package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.dao.EscalahorarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EscalahorarioService extends GenericService<Escalahorario> {

	private EscalahorarioDAO escalahorarioDAO;
	
	public void setEscalahorarioDAO(EscalahorarioDAO escalahorarioDAO) {
		this.escalahorarioDAO = escalahorarioDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalahorario> findEscalaHorarioDoVeiculo(Veiculo veiculo){
		return escalahorarioDAO.findEscalaHorarioDoVeiculo(veiculo);
	}

	/**
	 * @param escala
	 * @return
	 */
	public List<Escalahorario> getHorariosDiferentes(Escala escala) {
		List<Escalahorario> horarios = new ArrayList<Escalahorario>();
		if(escala.getHorariotodosdias() != null && !escala.getHorariotodosdias()){
			for (Escalahorario escalahorario : escala.getListaescalahorario()) {
				boolean achou = false;
				for (Escalahorario escalahorarioDistintos : horarios) {
					if(escalahorario.getHorainicio().getTime() == escalahorarioDistintos.getHorainicio().getTime() &&
					   escalahorario.getHorafim().getTime() == escalahorarioDistintos.getHorafim().getTime()) {
						achou = true;
					}
				}
				if(!achou)
					horarios.add(escalahorario);
			}
		}else{
			return escala.getListaescalahorario();
		}
		return horarios;
	}

	/**
	 * Acha a escala de hor�rio correta. Este m�todo � usado nas telas FLEX que utilizam agenda.
	 * 
	 * @param data
	 * @param escala
	 * @param escalahorario
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escalahorario achaEscalaDeHorarioCorreta(Date data, Escala escala, Escalahorario escalahorario) {
		if(escala.getHorariotodosdias() != null && !escala.getHorariotodosdias()){
			Calendar dataAux = Calendar.getInstance();
			dataAux.setTimeInMillis(data.getTime());
			
			for (Escalahorario escalahorarioOriginal : escala.getListaescalahorario()) {
				if(escalahorarioOriginal.getDiasemana().ordinal()+1 == dataAux.get(Calendar.DAY_OF_WEEK) && 
				   escalahorarioOriginal.getHorainicio().getTime() == escalahorario.getHorainicio().getTime() && 
				   escalahorarioOriginal.getHorafim().getTime() == escalahorario.getHorafim().getTime())
					return escalahorarioOriginal;
			}
		}
		return escalahorario;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalahorario> findEscalaHorarioDoColaboradorNaData(Colaborador colaborador, Date data) {
		List<Escalahorario> escalas = this.findEscalaHorario(colaborador, data);
		this.limpaHorasConvertString(escalas);
		return escalas;
	}

	public List<Escalahorario> limpaHorasConvertString(List<Escalahorario> escalas) {
		if(escalas != null && !escalas.isEmpty()){
			for (Escalahorario escalahorario : escalas) {
				escalahorario.setHorafimstring(escalahorario.getHorafim().toString());
				escalahorario.setHorainiciostring(escalahorario.getHorainicio().toString());
				escalahorario.setHorafim(null);
				escalahorario.setHorainicio(null);
			}
		}
		
		return escalas;
	}

	public List<Escalahorario> findEscalaHorario(Colaborador colaborador, Date data) {
		return escalahorarioDAO.findEscalaHorarioDoColaboradorNaData(colaborador, data);
	}

}
