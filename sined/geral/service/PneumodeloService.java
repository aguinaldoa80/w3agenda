package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.dao.PneumodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.pneumodelo.PneumodeloRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumodelo.PneuModeloW3producaoRESTModel;

public class PneumodeloService extends GenericService<Pneumodelo> {

	private PneumodeloDAO pneumodeloDAO;
	private PneuSegmentoService pneuSegmentoService;

	public void setPneumodeloDAO(PneumodeloDAO pneumodeloDAO) {
		this.pneumodeloDAO = pneumodeloDAO;
	}
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {
		this.pneuSegmentoService = pneuSegmentoService;
	}


	/* singleton */
	private static PneumodeloService instance;
	public static PneumodeloService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneumodeloService.class);
		}
		return instance;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumodeloDAO#findAutocomplete(String q, String whereInPneumarca)
	*
	* @param q
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumodelo> findAutocomplete(String q){
		String cdpneumarca = null;
		boolean otr = false;
		String cdPneuSegmento = null;
		try {
			cdpneumarca = NeoWeb.getRequestContext().getParameter("cdpneumarca");
			cdPneuSegmento = NeoWeb.getRequestContext().getParameter("cdpneuSegmento");
			
			if(StringUtils.isNotBlank(cdPneuSegmento)){
				otr = pneuSegmentoService.isOtr(new PneuSegmento(Integer.parseInt(cdPneuSegmento)));
			}
		} catch (Exception e) {} 
		return pneumodeloDAO.findAutocomplete(q, cdpneumarca, otr);
	}
	
	public List<Pneumodelo> findAutocomplete(String q, Pneumarca pneuMarca, PneuSegmento pneuSegmento){
		return pneumodeloDAO.findAutocomplete(q, pneuMarca.getCdpneumarca().toString(), pneuSegmentoService.isOtr(pneuSegmento));
	}
	
	public List<PneuModeloW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de pneumodelo para a sincronizacao com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PneuModeloW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PneuModeloW3producaoRESTModel> lista = new ArrayList<PneuModeloW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumodelo pm : pneumodeloDAO.findForW3Producao(whereIn))
				lista.add(new PneuModeloW3producaoRESTModel(pm));
		}
		
		return lista;
	}
	
	/**
	 * Carrega pelo nome, se n�o existir insere o registro.
	 *
	 * @param nome
	 * @param pneumarca
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumodelo loadOrInsertByNomeMarca(String nome, Pneumarca pneumarca){
		Pneumodelo pneumodelo = this.loadByNomeMarca(nome, pneumarca);
		if(pneumodelo == null){
			pneumodelo = new Pneumodelo();
			pneumodelo.setNome(nome);
			pneumodelo.setPneumarca(pneumarca);
			this.saveOrUpdate(pneumodelo);
		}
		return pneumodelo;
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.PneumodeloService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumodeloRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumodeloDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumodeloRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PneumodeloRESTModel> lista = new ArrayList<PneumodeloRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumodelo bean : pneumodeloDAO.findForAndroid(whereIn))
				lista.add(new PneumodeloRESTModel(bean));
		}
		
		return lista;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumodelo loadByNomeMarca(String nome, Pneumarca pneumarca) {
		return pneumodeloDAO.loadByNomeMarca(nome, pneumarca);
	}
	
	public List<Pneumodelo> findByPneuSegmento(PneuSegmento pneuSegmento, Pneumarca pneuMarca) {
		if(pneuSegmento != null){
			pneuSegmento = pneuSegmentoService.loadReader(pneuSegmento, "pneuSegmento.cdPneuSegmento, pneuSegmento.otr");
		}
		return pneumodeloDAO.findByPneuSegmento(pneuSegmento != null && Boolean.TRUE.equals(pneuSegmento.getOtr()),pneuMarca);
	}
	
	public boolean isOtr(Pneumodelo pneumodelo){
		Pneumodelo bean = this.load(pneumodelo, "pneumodelo.cdpneumodelo, pneumodelo.otr");
		if(bean == null){
			return false;
		}
		return Boolean.TRUE.equals(bean.getOtr());
	}
}