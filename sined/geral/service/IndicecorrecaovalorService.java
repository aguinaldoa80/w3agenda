package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Indicecorrecaovalor;
import br.com.linkcom.sined.geral.dao.IndicecorrecaovalorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class IndicecorrecaovalorService extends GenericService<Indicecorrecao> {

	private IndicecorrecaovalorDAO indicecorrecaovalorDAO;
	
	public void setIndicecorrecaovalorDAO(
			IndicecorrecaovalorDAO indicecorrecaovalorDAO) {
		this.indicecorrecaovalorDAO = indicecorrecaovalorDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param indicecorrecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Indicecorrecaovalor> findByIndiceCorrecao(Indicecorrecao indicecorrecao) {
		return indicecorrecaovalorDAO.findByIndiceCorrecao(indicecorrecao);
	}

	
}
