package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.view.Vwrelatoriooverhead;
import br.com.linkcom.sined.geral.dao.VwrelatoriooverheadDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.RelacaoOverheadFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwrelatoriooverheadService extends GenericService<Vwrelatoriooverhead>{

	private VwrelatoriooverheadDAO vwrelatoriooverheadDAO;
	private CentrocustoService centrocustoService;
	
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setVwrelatoriooverheadDAO(
			VwrelatoriooverheadDAO vwrelatoriooverheadDAO) {
		this.vwrelatoriooverheadDAO = vwrelatoriooverheadDAO;
	}
	
	/**
	 * Cria o arquivo excel formatado da rela��o Overhead/Faturamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#criaMapCentroCusto
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#preencheMap
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#criaListaData
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#findFaturamentoLiquido
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#makeHeaderRelatorio
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#createHeaderDataGrid
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#getColunas
	 *
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoRelacaoOverheadExcelFormatado(RelacaoOverheadFiltro filtro) throws IOException {
		
		// CRIA UM MAP E COLOCA TODOS OS CENTRO DE CUSTOS NO MAP
		Map<Centrocusto, List<Vwrelatoriooverhead>> map = this.criaMapCentroCusto();
		
		// BUSCA OS VALORES NA VIEW VWRELATORIOOVERHEAD E PREENCHE CRIANDO UM MAP COM A KEY
		// O CENTRO DE CUSTO COM O VALOR UMA LISTA DE BEANS VWRELATORIOOVERHEAD
		this.preencheMap(filtro, map);
		
		// A PARTIR DO IN�CIO E FIM DO FILTRO CRIA UMA LISTA DE DATAS DESTE INTERVALO
		// SOMENTE DATAS DO PRIMEIRO DIA DO M�S
		List<Date> listaData = this.criaListaData(filtro.getDtref1(), filtro.getDtref2());
		
		// BUSCA A LISTA DO FATURAMENTO L�QUIDO
		List<Vwrelatoriooverhead> listaFaturamento = this.findFaturamentoLiquido(filtro);
		
		SinedExcel wb = new SinedExcel();

		HSSFRow row;
		HSSFCell cell;
		
		HSSFSheet p = wb.createSheet("Planilha");
        p.setDefaultColumnWidth((short) 15);
        p.setDisplayGridlines(false);
        
        // CRIANDO O CABE�ALHO DO RELAT�RIO
        this.makeHeaderRelatorio(map, p);
        
        // COLOCANDO O MAP EM UMA LISTA DE Entry<Centrocusto, List<Vwrelatoriooverhead>> E ORDENANDO PELO NOME DO CENTRO DE CUSTO
        // N�O PODE RETIRAR ESSA ORDENA��O POIS SE N�O D� PROBLEMA DE TROCAS DE VALORES !!!!!
        List<Entry<Centrocusto, List<Vwrelatoriooverhead>>> entrySet = new ArrayList<Entry<Centrocusto,List<Vwrelatoriooverhead>>>(map.entrySet());
        Collections.sort(entrySet, new Comparator<Entry<Centrocusto, List<Vwrelatoriooverhead>>>(){
			public int compare(Entry<Centrocusto, List<Vwrelatoriooverhead>> o1, Entry<Centrocusto, List<Vwrelatoriooverhead>> o2) {
				return o1.getKey().getNome().toUpperCase().compareTo(o2.getKey().getNome().toUpperCase());
			}
		});
        
        // CRIANDO O HEADER DA TABELA
        this.createHeaderDataGrid(p, entrySet);
        
        
        Money valor, fatliquido;
        List<Vwrelatoriooverhead> lista;
        int j;
        int i = 0;
        Format format = new SimpleDateFormat("MMM/yyyy");
        // UM LOOP NA LISTA DE DATA PARA O PREENCHIMENTO DE CADA M�S
        for (Date date : listaData) {
			
        	row = p.createRow((short) (5 + i));
    		row.setHeight((short) (12*35));
        	
    		// CRIA A COLUNA DE DESCRI��O DO M�S
            cell = row.createCell((short) 0);
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
            cell.setCellValue(format.format(date));
            
            
            j = 0;
            // LOOP NA LISTA DO MAP PARA O PREENCHIMENTO DOS VALORES DOS CENTRO DE CUSTOS
            for (Entry<Centrocusto, List<Vwrelatoriooverhead>> entry : entrySet) {
				          
            	// BUSCA O VALOR DO CENTRO DE CUSTO NAQUELE M�S
            	valor = new Money();
            	lista = entry.getValue();
            	if(lista != null && lista.size() > 0){
	            	for (Vwrelatoriooverhead vwrelatoriooverhead : lista) {
						if(SinedDateUtils.equalsIgnoreHour(vwrelatoriooverhead.getDtref(), date)){
							valor = vwrelatoriooverhead.getValor();
							break;
						}
					}	
            	}
            	
            	// CRIA A COLUNA DO VALOR
            	cell = row.createCell((short) (1+j));
                cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
                cell.setCellValue(valor.getValue().doubleValue());
                
                j++;
                
                // CRIA A COLUNA DO PERCENTUAL
                // SE O FATURAMENTO LIQUIDO VIR ZERO O PERCENTUAL � ZERO TAMB�M
                cell = row.createCell((short) (1+j));
                cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
                cell.setCellFormula("IF(" + SinedExcel.getAlgarismoColuna(((entrySet.size()*2) + 3)) + (5+i+1) + ">0;" + SinedExcel.getAlgarismoColuna(j) + (5+i+1) + "/" + SinedExcel.getAlgarismoColuna(((entrySet.size()*2) + 3)) + (5+i+1) + ";0)");
                
                j++;
			}
            
            // CRIA O TOTAL DO VALOR DAQUELE M�S
            cell = row.createCell((short) (1+j));
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
            cell.setCellFormula("SUM(" + this.getColunas(entrySet.size(), 5+i+1, 1) + ")");
        	
            // CRIA O TOTAL DE PERCENTUAL DAQUELE M�S
            cell = row.createCell((short) (1+j+1));
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
            cell.setCellFormula("SUM(" + this.getColunas(entrySet.size(), 5+i+1, 2) + ")");
            
            // SOMA OS FATURAMENTOS LIQUIDOS DE TODOS OS CENTRO DE CUSTO 
            fatliquido = new Money();
            if(listaFaturamento != null && listaFaturamento.size() > 0){
	            for (Vwrelatoriooverhead vw2 : listaFaturamento) {
	    			if(SinedDateUtils.equalsIgnoreHour(date, vw2.getDtref())){
	    				fatliquido = fatliquido.add(vw2.getValor());
	    			}
	    		}
            }
            
            // CRIA A COLUNA DO FATURAMENTO LIQUIDO
            cell = row.createCell((short) (1+j+2));
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
            cell.setCellValue(fatliquido.getValue().doubleValue());
            
        	i++;
		}
        
        // CRIA A LINHA OCULTA QUE SOMA OS VALORES DOS CENTRO DE CUSTO
        row = p.createRow((short) (5 + i));
		row.setHeight((short) (0));
    	
		int k = 0;
		for (int l = 0; l < entrySet.size()+1; l++, k+=2) {
			cell = row.createCell((short) (1+k));
            cell.setCellFormula("SUM(" + SinedExcel.getAlgarismoColuna(1+k) + "6:" + SinedExcel.getAlgarismoColuna(1+k) + (listaData.size()+5) + ")");
		}
        
		
		// CRIA A LINHA DA M�DIA PONDERADA
		row = p.createRow((short) (5 + i + 1));
		row.setHeight((short) (12*35));
		
		p.addMergedRegion(new Region((5 + i + 1), (short) 0, (5 + i + 1), (short) 1));
		
		cell = row.createCell((short) (0));
        cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
        cell.setCellValue("M�dia ponderada");
        
        cell = row.createCell((short) (1));
        cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
        
        k = 0;
        for (int l = 0; l < entrySet.size()+1; l++, k+=2) {
        	
        	cell = row.createCell((short) (2+k));
            cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
            cell.setCellFormula(SinedExcel.getAlgarismoColuna(1+k) + (5 + i + 1) + "/" + SinedExcel.getAlgarismoColuna(entrySet.size()*2 + 3) + (5 + i + 3));
            
            // N�O PODE CRIAR A C�LULA NA �LTIMA INTERA��O DO LOOP
            if(l+1 < entrySet.size()+1){
            	cell = row.createCell((short) (3+k));
            	cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
            }
        }
        
        // CRIA A �LTIMA COLUNA COM A M�DIA SIMPLES DE TODOS OS FATURAMENTOS LIQUIDOS DURANTE O PER�ODO
        cell = row.createCell((short) (k+1));
        cell.setCellFormula("AVERAGE(" + SinedExcel.getAlgarismoColuna(k+1) + "6:" + SinedExcel.getAlgarismoColuna(k+1) + (listaData.size()+5) + ")");
        cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
        
        // CRIA A LINHA COM O TOTAL DE FATURAMENTO L�QUIDO
        row = p.createRow((short) (5 + i + 2));
		row.setHeight((short) (12*35));
		
		p.addMergedRegion(new Region((5 + i + 2), (short) 0, (5 + i + 2), (short) (entrySet.size()*2 + 2)));
        
		// LOOP PARA SETAR O STYLE DAS LINHAS MESCLADAS
        for (int w = 0; w <= entrySet.size()*2 + 2; w++) {
        	cell = row.createCell((short) (w));
            cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
            cell.setCellValue("Total l�quido faturado");
		}
        
        // CRIA A COLUNA COM A SOMA DOS FATURAMENTOS LIQUIDOS
        cell = row.createCell((short) (k+1));
        cell.setCellFormula("SUM(" + SinedExcel.getAlgarismoColuna(k+1) + "6:" + SinedExcel.getAlgarismoColuna(k+1) + (listaData.size()+5) + ")");
        cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
        
		
		return wb.getWorkBookResource("overhead_faturamento_" + SinedUtil.datePatternForReport() + ".xls");
		
	}
	
	/**
	 * Cria o header do relat�rio.
	 *
	 * @param map
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void makeHeaderRelatorio(Map<Centrocusto, List<Vwrelatoriooverhead>> map, HSSFSheet p) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) 0);
		p.addMergedRegion(new Region(0, (short) 0, 2, (short) ((map.size()*2) + 3)));
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cell.setCellValue("Rela��o Overhead/Faturamento");
	}
	
	
	/**
	 * Retorna a string com as colunas para que fa�a a soma.
	 *
	 * @param size
	 * @param linha
	 * @param comeco
	 * 				1 - soma dos valores
	 * 				2 - soma das porcentagens
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String getColunas(int size, int linha, int comeco) {
		String string = "";
		
		for (int i = comeco; i < size*2+(comeco-1); i+=2) {
			string+= SinedExcel.getAlgarismoColuna(i) + linha + ";";
		}
		
		return string.substring(0, string.length()-1);
	}
	
	/**
	 * Cria o header da tabela com a descri��o dos centros de custo.
	 *
	 * @param p
	 * @param entrySet
	 * @author Rodrigo Freitas
	 */
	private void createHeaderDataGrid(HSSFSheet p, List<Entry<Centrocusto, List<Vwrelatoriooverhead>>> entrySet) {
		HSSFRow row;
		HSSFCell cell;
		
		row = p.createRow((short) 4);
		
        cell = row.createCell((short) 0);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
        cell.setCellValue("Descri��o");
        
        p.setColumnWidth((short) 0,(short) (91*35));
        
        Centrocusto key;
        int i = 0;
		for (Entry<Centrocusto, List<Vwrelatoriooverhead>> entry : entrySet) {
			key = entry.getKey();
			
			cell = row.createCell((short) (1+i));
	        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
	        cell.setCellValue(key.getNome());
	        
	        p.setColumnWidth((short) (1+i),(short) (170*35));
			
			i++;
			
			cell = row.createCell((short) (1+i));
	        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
	        cell.setCellValue("%");
	        
	        p.setColumnWidth((short) (1+i),(short) (66*35));
	        
	        i++;
		}
		
		cell = row.createCell((short) (i+1));
        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
        cell.setCellValue("Total OH");
        
        p.setColumnWidth((short) (i+1),(short) (144*35));
        
        cell = row.createCell((short) (i+2));
        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
        cell.setCellValue("Total OH");
        
        p.setColumnWidth((short) (i+2),(short) (66*35));
        
        cell = row.createCell((short) (i+3));
        cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
        cell.setCellValue("Faturamento l�quido");
        
        p.setColumnWidth((short) (i+3),(short) (173*35));
	}
	
	/**
	 * Prrenche o map com informa��es da view.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#findForRelatorio
	 *
	 * @param filtro
	 * @param map
	 * @author Rodrigo Freitas
	 */
	private void preencheMap(RelacaoOverheadFiltro filtro, Map<Centrocusto, List<Vwrelatoriooverhead>> map) {
		
		List<Vwrelatoriooverhead> listaRel = this.findForRelatorio(filtro);
		List<Vwrelatoriooverhead> lista;
		
		for (Vwrelatoriooverhead vw : listaRel) {
			lista = map.get(vw.getCentrocusto());
			if(lista == null){
				lista = new ArrayList<Vwrelatoriooverhead>();
			}
			
			lista.add(vw);
			map.put(vw.getCentrocusto(), lista);
		}
		
	}
	
	/**
	 * Cria o map com todos os centros de custo ativos cadastrados no sistema.
	 *
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#findAtivos
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Map<Centrocusto, List<Vwrelatoriooverhead>> criaMapCentroCusto() {
		
		List<Centrocusto> allCentroCusto = centrocustoService.findAtivos();
		
		Map<Centrocusto, List<Vwrelatoriooverhead>> map = new HashMap<Centrocusto, List<Vwrelatoriooverhead>>();
		
		for (Centrocusto centrocusto : allCentroCusto) {
			map.put(centrocusto, null);
		}
		
		return map;
	}

	/**
	 * A partir das datas do filtro cria uma lista de datas do primeiro dia do m�s.
	 *
	 * @param dtref1
	 * @param dtref2
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Date> criaListaData(Date dtref1, Date dtref2) {
		List<Date> lista = new ArrayList<Date>();
		
		Date data = new Date(dtref1.getTime());
		lista.add(data);

		while(SinedDateUtils.beforeIgnoreHour(data, dtref2)){
			data = SinedDateUtils.incrementDate(data, 1, Calendar.MONTH);
			lista.add(data);
		}
		
		return lista;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VwrelatoriooverheadDAO#findForRelatorio
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Vwrelatoriooverhead> findForRelatorio(RelacaoOverheadFiltro filtro) {
		return vwrelatoriooverheadDAO.findForRelatorio(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwrelatoriooverheadDAO#findFaturamentoLiquido
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Vwrelatoriooverhead> findFaturamentoLiquido(RelacaoOverheadFiltro filtro) {
		return vwrelatoriooverheadDAO.findFaturamentoLiquido(filtro);
	}
	
}
