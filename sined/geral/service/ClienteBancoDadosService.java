package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.ClienteBancoDados;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.dao.ClienteBancoDadosDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ClienteBancoDadosService extends GenericService<ClienteBancoDados>{
	
	private ClienteBancoDadosDAO clienteBancoDadosDAO;

	public void setClienteBancoDadosDAO(ClienteBancoDadosDAO clienteBancoDadosDAO) {
		this.clienteBancoDadosDAO = clienteBancoDadosDAO;
	}
	
	public List<ClienteBancoDados> findClienteBancoDados(Categoria categoria, Projeto projeto){
		return clienteBancoDadosDAO.findClienteBancoDados(categoria, projeto);
	}
	
	public List<ClienteBancoDados> findForAcessos(){
		return clienteBancoDadosDAO.findForAcessos();
	}

	public ClienteBancoDados findByContrato(Contrato contrato) {
		return clienteBancoDadosDAO.findByContrato(contrato);
	}
	
	public List<ClienteBancoDados> findForLicenca(String whereIn){
		return clienteBancoDadosDAO.findForLicenca(whereIn);
	}	
	
	public ClienteBancoDados findForVersaoAtual(Integer cdpessoa, Integer cdcontrato){
		return clienteBancoDadosDAO.findForVersaoAtual(cdpessoa, cdcontrato);
	}
	
}
