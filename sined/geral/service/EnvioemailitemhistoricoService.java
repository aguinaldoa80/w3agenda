package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailitemhistorico;
import br.com.linkcom.sined.geral.dao.EnvioemailitemhistoricoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EmailItemWebHook;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EnvioemailitemhistoricoService extends GenericService<Envioemailitemhistorico>{

	private EnvioemailitemhistoricoDAO envioemailitemhistoricoDAO;
	private static EnvioemailitemhistoricoService instance;
	
	public static EnvioemailitemhistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EnvioemailitemhistoricoService.class);
		}
		return instance;
	}
	public void setEnvioemailitemhistoricoDAO(
			EnvioemailitemhistoricoDAO envioemailitemhistoricoDAO) {
		this.envioemailitemhistoricoDAO = envioemailitemhistoricoDAO;
	}
	
	public void atualizarStatus(EmailItemWebHook emailItemWebHook){
		envioemailitemhistoricoDAO.atualizarStatus(emailItemWebHook);
	}
	
	public List<Envioemailitemhistorico> findByEnvioemailitem(Envioemailitem envioemailitem){
		return envioemailitemhistoricoDAO.findByEnvioemailitem(envioemailitem);
	}
}
