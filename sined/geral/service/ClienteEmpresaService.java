package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.geral.dao.ClienteEmpresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ClienteEmpresaService extends GenericService<ClienteEmpresa> {

	private ClienteEmpresaDAO clienteEmpresaDAO;
	
	public void setClienteEmpresaDAO(ClienteEmpresaDAO clienteEmpresaDAO) {
		this.clienteEmpresaDAO = clienteEmpresaDAO;
	}
	
	/* singleton */
	private static ClienteEmpresaService instance;
	public static ClienteEmpresaService getInstance() {
		if (instance==null){
			instance = Neo.getObject(ClienteEmpresaService.class);
		}
		return instance;
	}
	
	public List<ClienteEmpresa> findByCliente(Cliente cliente){
		return clienteEmpresaDAO.findByCliente(cliente);
	}

	public boolean haveClienteEmpresa(ClienteEmpresa clienteEmpresa) {
		return clienteEmpresaDAO.haveClienteEmpresa(clienteEmpresa);
	}
}
