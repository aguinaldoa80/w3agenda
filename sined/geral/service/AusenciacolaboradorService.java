package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Ausenciacolaboradorhistorico;
import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.enumeration.Ausenciacolaboradorhistoricoacao;
import br.com.linkcom.sined.geral.dao.AusenciacolaboradorDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.AusenciacolaboradorFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AusenciacolaboradorService extends GenericService<Ausenciacolaborador>{

	private AusenciacolaboradorDAO ausenciacolaboradorDAO;
	private EscalahorarioService escalahorarioService;
	private AusenciacolaboradorhistoricoService ausenciacolaboradorhistoricoService;
	
	public void setAusenciacolaboradorDAO(
			AusenciacolaboradorDAO ausenciacolaboradorDAO) {
		this.ausenciacolaboradorDAO = ausenciacolaboradorDAO;
	}	
	public void setEscalahorarioService(EscalahorarioService escalahorarioService) {
		this.escalahorarioService = escalahorarioService;
	}
	public void setAusenciacolaboradorhistoricoService(
			AusenciacolaboradorhistoricoService ausenciacolaboradorhistoricoService) {
		this.ausenciacolaboradorhistoricoService = ausenciacolaboradorhistoricoService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 * @param dtReferenciaAte 
	 * @param dtReferencia 
	 */
	public List<Ausenciacolaborador> findAusenciasDoColaborador(Colaborador colaborador, Date dtReferencia, Date dtReferenciaAte) {
		return ausenciacolaboradorDAO.findAusenciasDoColaborador(colaborador, dtReferencia, dtReferenciaAte);
	}
	
	public Integer getNumeroDiasAusenciaColaborador(Colaborador colaborador, Date dtreferencia1, Date dtreferencia2){
		List<Ausenciacolaborador> listaAusencia = this.findAusenciasDoColaborador(colaborador, dtreferencia1, dtreferencia2);
		
		Long numero = 0L;
		for (Ausenciacolaborador ausenciacolaborador : listaAusencia) {
			numero += SinedDateUtils.calculaDiferencaDias(ausenciacolaborador.getDtinicio(), ausenciacolaborador.getDtfim());
		}
		
		return numero.intValue();
	}

	/**
	 * M�todo que verifica se a data esta compreendida na data de aus�ncia do colaborador
	 * 
	 * @param dataAux
	 * @param escalahorarioAux
	 * @param listaAusencias
	 * @param motivo 
	 * @return
	 * @authro Tom�s Rabelo
	 */
	public Boolean verificaColaboradorAusente(Calendar dataAux, Escalahorario escalahorarioAux, List<Ausenciacolaborador> listaAusencias, StringBuilder motivo) {
		if(listaAusencias != null && !listaAusencias.isEmpty()){
			Date data = new Date(dataAux.getTimeInMillis());
			Date dataInicioEscalaHorario = SinedDateUtils.setaHoraData(data, escalahorarioAux.getHorainicio());
			Date dataFimEscalaHorario = SinedDateUtils.setaHoraData(data, escalahorarioAux.getHorafim());
			Date dataInicioHora = null;
			Date dataFimHora = null;
			
			for (Ausenciacolaborador ausenciacolaborador : listaAusencias) {
				if(data.getTime() >= ausenciacolaborador.getDtinicio().getTime() && data.getTime() <= ausenciacolaborador.getDtfim().getTime()){
					dataInicioHora = null;
					dataFimHora = null;
					if(ausenciacolaborador.getHrinicio() != null){
						if(data.equals(ausenciacolaborador.getDtinicio())){
							dataInicioHora = SinedDateUtils.setaHoraData(data, ausenciacolaborador.getHrinicio());
						}else{
							dataInicioHora = SinedDateUtils.setaHoraData(data, new Hora("00:00"));
						}
					}
					if(ausenciacolaborador.getHrfim() != null){
						if(data.equals(ausenciacolaborador.getDtfim())){
							dataFimHora = SinedDateUtils.setaHoraData(data, ausenciacolaborador.getHrfim());
						}else{
							dataFimHora = SinedDateUtils.setaHoraData(data, new Hora("23:59"));
						}
					}
					
					if(ausenciacolaborador.getHrinicio() != null && ausenciacolaborador.getHrfim() != null && 
					   ((dataInicioHora.getTime() >= dataInicioEscalaHorario.getTime() && dataInicioHora.before(dataFimEscalaHorario)) || //Hora no meio
					    (dataFimHora.after(dataInicioEscalaHorario) && dataFimHora.getTime() <= dataFimEscalaHorario.getTime()) || //Hora fim no meio
					    (dataInicioHora.before(dataInicioEscalaHorario) && dataFimHora.after(dataFimEscalaHorario)) || //Escala compreendida na hora
					    (dataInicioHora.getTime() == dataInicioEscalaHorario.getTime() && dataFimHora.getTime() == dataFimEscalaHorario.getTime()))){ //hora iguais
						return false;
					}else if(ausenciacolaborador.getHrinicio() != null && ausenciacolaborador.getHrfim() == null && 
							 (dataInicioHora.before(dataFimEscalaHorario))){
						motivo.append(ausenciacolaborador.getDescricao());
						return false;
					}else if(ausenciacolaborador.getHrinicio() == null && ausenciacolaborador.getHrfim() != null && 
						(dataFimHora.after(dataInicioEscalaHorario))){
						motivo.append(ausenciacolaborador.getDescricao());
						return false;
					}else if(ausenciacolaborador.getHrinicio() == null && ausenciacolaborador.getHrfim() == null){
						motivo.append(ausenciacolaborador.getDescricao());
						return false;
					}else{	
						continue;
					}
				}
			}
			return true;
		}
		return true;
	}
	
	/**
	 * M�todo que registra a aus�ncia
	 *
	 * @param colaborador
	 * @param listaAgendamentoservicoitemapoio
	 * @param motivo
	 * @param ausenciamotivo
	 * @return
	 * @author Luiz Fernando
	 */
	public String registrarAusenciaFlex(Colaborador colaborador, List<Agendamentoservicoitemapoio> listaAgendamentoservicoitemapoio, String motivo, Ausenciamotivo ausenciamotivo){
		String erro = "";
		if(colaborador == null || colaborador.getCdpessoa() == null){
			erro += "Nenhum colaborador selecionado";			
		}else{
			if(listaAgendamentoservicoitemapoio != null && !listaAgendamentoservicoitemapoio.isEmpty()){
				Date data = null;
				Hora horaFim = null;
				int j = 1;
				
				Ausenciacolaborador ac = new Ausenciacolaborador();
				Ausenciacolaboradorhistorico ach = new Ausenciacolaboradorhistorico();
				List<Agendamentoservicoitemapoio> lista = new ArrayList<Agendamentoservicoitemapoio>();
				for(int i = listaAgendamentoservicoitemapoio.size(); i > 0; i--){
					lista.add(listaAgendamentoservicoitemapoio.get(i-1));
				}
				for(Agendamentoservicoitemapoio asia : lista){
					if(asia.getEscalahorarioAux() != null && asia.getEscalahorarioAux().getCdescalahorario() != null){
						Escalahorario escalahorario = escalahorarioService.load(asia.getEscalahorarioAux());
						if(escalahorario != null){
							if(horaFim != null && data != null){
								if(SinedDateUtils.equalsIgnoreHour(asia.getData(),data) && horaFim.equals(escalahorario.getHorainicio())){									
									horaFim = escalahorario.getHorafim();
									ac.setDtfim(asia.getData());
									ac.setHrfim(escalahorario.getHorafim());
								}else if(SinedDateUtils.equalsIgnoreHour(asia.getData(),data) && horaFim.getTime() > escalahorario.getHorainicio().getTime()){
									if(horaFim.getTime() < escalahorario.getHorafim().getTime()){
										ac.setHrfim(escalahorario.getHorafim());
										horaFim = escalahorario.getHorafim();
									}
									ac.setDtfim(asia.getData());
									
								}else {
									this.saveOrUpdate(ac);
									ach = new Ausenciacolaboradorhistorico();
									ach.setCdausenciacolaborador(ac);
									ach.setAcao(Ausenciacolaboradorhistoricoacao.CRIADO);
									ach.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
									ach.setDtaltera(new Timestamp(System.currentTimeMillis()));
									ausenciacolaboradorhistoricoService.saveOrUpdate(ach);
									
									ac = new Ausenciacolaborador();
									
									
									horaFim = escalahorario.getHorafim();
									data = asia.getData();
									ac.setColaborador(colaborador);
									ac.setDtinicio(asia.getData());
									ac.setDtfim(asia.getData());
									ac.setHrfim(horaFim);
									ac.setHrinicio(escalahorario.getHorainicio());
									ac.setDescricao(motivo);
									ac.setAusenciamotivo(ausenciamotivo);
								}								
							}else{
								horaFim = escalahorario.getHorafim();
								data = asia.getData();
								ac = new Ausenciacolaborador();
								ac.setColaborador(colaborador);
								ac.setDtinicio(asia.getData());
								ac.setDtfim(asia.getData());
								ac.setHrfim(horaFim);
								ac.setHrinicio(escalahorario.getHorainicio());
								ac.setDescricao(motivo);
								ac.setAusenciamotivo(ausenciamotivo);
							}
							// removido  j == listaAgendamentoservicoitemapoio.size()
							// para salvar todos as listas.
							if(ac != null){
								this.saveOrUpdate(ac);
								ach = new Ausenciacolaboradorhistorico();
								ach.setCdausenciacolaborador(ac);
								ach.setAcao(Ausenciacolaboradorhistoricoacao.CRIADO);
								ach.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
								ach.setDtaltera(new Timestamp(System.currentTimeMillis()));
								ausenciacolaboradorhistoricoService.saveOrUpdate(ach);
							}
						}
					}
					j++;
				}
			}
		}
		
		return erro;
	}
	
	/**
	 * M�todo que retorna a lista de clientes que estavam agendados nas datas/hor�rios em que foi registrado aus�ncia
	 *
	 * @param listaAsia
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ausenciacolaborador> findClienteAposRegistrarAusenciaFlex(List<Agendamentoservicoitemapoio> listaAsia){
		
		if(listaAsia != null && !listaAsia.isEmpty()){
			List<Ausenciacolaborador> listaAc = new ArrayList<Ausenciacolaborador>();			
			Ausenciacolaborador ac = new Ausenciacolaborador();	
			
			for(Agendamentoservicoitemapoio asia : listaAsia){				
				if(asia.getEscalahorarioAux() != null && asia.getEscalahorarioAux().getCdescalahorario() != null &&
						asia.getListaAgendamentos() != null && asia.getListaAgendamentos().size() > 0 &&
						asia.getListaAgendamentos().get(0).getCliente() != null){
					
						Escalahorario escalahorario = escalahorarioService.load(asia.getEscalahorarioAux());
						if(escalahorario != null){
								ac = new Ausenciacolaborador();
								ac.setDataString(asia.getDataAux());
								ac.setHrfim(escalahorario.getHorafim());
								ac.setHrinicio(escalahorario.getHorainicio());
								ac.setCliente(asia.getListaAgendamentos().get(0).getCliente());
								ac.setDescriptionHour(escalahorario.getHorainicio() + " - " + escalahorario.getHorafim());
								listaAc.add(ac);
						}
				}
			}
				
			return listaAc;
		}
		
		return null;
	}

	/**
	 * 
	 * M�todo que gera o relat�rio de AusenciaColaborador.
	 *
	 * @name gerarReportAusenciaColaborador
	 * @param filtro
	 * @return
	 * @return IReport
	 * @author Thiago Augusto
	 * @date 13/08/2012
	 *
	 */
	public IReport gerarReportAusenciaColaborador(AusenciacolaboradorFiltro filtro){
		
		List<Ausenciacolaborador> listReport = this.findForReport(filtro);
		Report report = new Report("/rh/ausenciacolaborador");
		if (listReport != null && listReport.size() > 0)
			report.setDataSource(listReport);
		return report;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Ausenciacolaborador>
	 * @author Thiago Augusto
	 * @date 13/08/2012
	 *
	 */
	public List<Ausenciacolaborador> findForReport(AusenciacolaboradorFiltro filtro){
		return ausenciacolaboradorDAO.findForReport(filtro);
	}
}
