package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.DespesaviagemHistorico;
import br.com.linkcom.sined.geral.dao.DespesaviagemHistoricoDAO;

public class DespesaviagemHistoricoService extends GenericService<DespesaviagemHistorico>{
	
	private static DespesaviagemHistoricoService instance;
	private DespesaviagemHistoricoDAO despesaviagemHistoricoDAO;
	
	public static DespesaviagemHistoricoService getInstance() {
		if (instance == null)
			instance = Neo.getObject(DespesaviagemHistoricoService.class);
		
		return instance;
	}
	
	public void setDespesaviagemHistoricoDAO(DespesaviagemHistoricoDAO despesaviagemHistoricoDAO) {
		this.despesaviagemHistoricoDAO = despesaviagemHistoricoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param form
	 * @return
	 */
	public List<DespesaviagemHistorico> findForEntradaDespesaViagem(Despesaviagem form) {
		return despesaviagemHistoricoDAO.findForEntradaDespesaViagem(form);
	}

}
