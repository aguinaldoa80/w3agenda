package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacaoadicao;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemdi;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.ProcessoReferenciado;
import br.com.linkcom.sined.geral.bean.ProcessoReferenciadoDocumento;
import br.com.linkcom.sined.geral.bean.Spedpiscofins;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadortipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Metodoapropriacaocredito;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.SpedpiscofinsDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ParticipanteSpedBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.SPEDPiscofinsApoio;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0000;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0110;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0111;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0140;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0150;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0190;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0200;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0206;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0400;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0450;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0500;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro0990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro1001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro1010;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro1011;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro1990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro9001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro9900;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro9990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.Registro9999;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA010;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA110;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA170;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroA990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC010;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC110;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC120;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC170;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC175;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC400;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC405;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC481;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC485;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC500;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC501;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC505;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroC990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroD001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroD010;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroD100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroD500;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroD990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroF001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroF010;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroF100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroF600;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroF990;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM001;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM100;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM200;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM400;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM410;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM600;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM800;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM810;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM990;
import br.com.linkcom.spedfiscal.spedpiscofins.spedpiscofins.SPEDPiscofins;
import br.com.linkcom.utils.LkNfeUtil;
import br.com.linkcom.utils.SPEDUtil;

public class SpedpiscofinsService extends GenericService<Spedpiscofins>{
	
	private EmpresaService empresaService;	
	private FornecedorService fornecedorService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	private SpedpiscofinsDAO spedpiscofinsDAO;
	private PessoaService pessoaService;
	private UnidademedidaService unidademedidaService;
	private MaterialService materialService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ArquivonfnotaService arquivonfnotaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EntradafiscalService entradafiscalService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumvendaService emporiumvendaService;
	private ConfiguracaonfeService configuracaonfeService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private AvisoService avisoService;
	private ContacontabilService contacontabilService;
	private AvisousuarioService avisoUsuarioService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private ProcessoReferenciadoService processoReferenciadoService;
	private ProcessoReferenciadoDocumentoService processoReferenciadoDocumentoService;
	
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setEmporiumvendaService(EmporiumvendaService emporiumvendaService) {this.emporiumvendaService = emporiumvendaService;}
	public void setEmporiumreducaozService(EmporiumreducaozService emporiumreducaozService) {this.emporiumreducaozService = emporiumreducaozService;}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {this.emporiumpdvService = emporiumpdvService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setSpedpiscofinsDAO(SpedpiscofinsDAO spedpiscofinsDAO) {this.spedpiscofinsDAO = spedpiscofinsDAO;}	
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}	
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
    public void setContacontabilService(ContacontabilService contacontabilService) {this.contacontabilService = contacontabilService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {this.materialCustoEmpresaService = materialCustoEmpresaService;}
	public void setProcessoReferenciadoService(ProcessoReferenciadoService processoReferenciadoService) {this.processoReferenciadoService = processoReferenciadoService;}
	public void setProcessoReferenciadoDocumentoService(ProcessoReferenciadoDocumentoService processoReferenciadoDocumentoService) {this.processoReferenciadoDocumentoService = processoReferenciadoDocumentoService;}
	
	public Resource gerarArquivo(SpedpiscofinsFiltro filtro, WebRequestContext request) {
				
		SPEDPiscofins spedPiscofins = new SPEDPiscofins();
		filtro.setApoio(new SPEDPiscofinsApoio(getContadorInicialProdutoServico(materialService.getUltimoIdentificadorCdmaterial())));
		
		List<Naturezabasecalculocredito> listaNaturezabccredito = new ArrayList<Naturezabasecalculocredito>();
		listaNaturezabccredito.add(Naturezabasecalculocredito.ALUGUEIS_PREDIOS);
		listaNaturezabccredito.add(Naturezabasecalculocredito.ALUGUEIS_MAQUINAS_EQUIPAMENTOS);
		listaNaturezabccredito.add(Naturezabasecalculocredito.ARMAZENAGEM_MERCADORIA_FRETE_OP_VENDA);
		listaNaturezabccredito.add(Naturezabasecalculocredito.CONTRAPRESTACAO);
		filtro.setListaNaturezabccredito(listaNaturezabccredito);
		
		filtro.setCnpjEmpresa(empresaService.cnpjEmpresa(filtro.getEmpresa()));
		
		spedPiscofins.setRegistro0000(this.createRegistro0000(filtro));
		spedPiscofins.setRegistro9999(this.createRegistro9999(filtro));
		
		this.resumoProcessamentoSped(spedPiscofins, filtro);
		
		List<String> listaErro = filtro.getListaErro();					
		if(listaErro != null && listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			throw new SinedException("A gera��o do arquivo cont�m erros. Favor corrig�-los.");
		}
		
		String nome = "spedPiscofins_" + SinedUtil.datePatternForReport() + ".txt";
		
		if(((SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1 >= 1 && (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) >= 2021 )))){
			spedPiscofins.getRegistro0000().setCod_ver("007");
		}else if(((SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1 >= 1 && (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) >= 2020 )))){
			spedPiscofins.getRegistro0000().setCod_ver("006");
		}else if(((SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1 >= 1 && (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) >= 2019 )))){
			spedPiscofins.getRegistro0000().setCod_ver("005");
		}else if(((SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1 >= 7 && (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) >= 2018 )))){
			spedPiscofins.getRegistro0000().setCod_ver("004");
		}else if(((SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1 >= 7 && (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) >= 2012 ))) || (SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR) > 2012)){
			spedPiscofins.getRegistro0000().setCod_ver("003");
		}
		byte[] bytes = spedPiscofins.toString().getBytes();
		
		this.saveArquivoSpedpiscofins(filtro, nome, bytes);
		
		return new Resource("text/plain", nome, bytes);
	}
	
	private Long getContadorInicialProdutoServico(Long ultimoCdmaterial) {
		return ultimoCdmaterial != null ? ultimoCdmaterial : 0;
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private Registro0000 createRegistro0000(SpedpiscofinsFiltro filtro) {
		
		Registro0000 registro0000 = new Registro0000();
		
		Empresa empresa = empresaService.loadForSpedReg0000(filtro.getEmpresa());
		filtro.getEmpresa().setCnpj(empresa.getCnpj());
		filtro.getEmpresa().setCpf(empresa.getCpf());
		
		registro0000.setReg(Registro0000.REG);
		registro0000.setCod_ver(Registro0000.COD_VER);
		registro0000.setTipo_escrit(0);
//		registro0000.setInd_sit_esp();
//		registro0000.setNum_rec_anterior();
		registro0000.setDt_ini(filtro.getDtinicio());
		registro0000.setDt_fin(filtro.getDtfim());
		registro0000.setNome(Normalizer.normalize(empresa.getRazaosocialOuNome(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", ""));
		registro0000.setCnpj(empresa.getCnpj() != null ? empresa.getCnpj().getValue() : null);
		registro0000.setUf(empresa.getUfsped() != null ? empresa.getUfsped().getSigla() : null);
		registro0000.setCod_mun(empresa.getMunicipiosped() != null ? empresa.getMunicipiosped().getCdibge() : null);
//		registro0000.setSuframa(null);
//		registro0000.setInd_nat_pj();
		registro0000.setInd_ativ(empresa.getIndiceatividadespedpiscofins() != null ? empresa.getIndiceatividadespedpiscofins().getValue() : null);
		
		registro0000.setRegistro0001(this.createRegistro0001(filtro));
		registro0000.setRegistro0990(this.createRegistro0990(filtro));
		registro0000.setRegistroA001(this.createRegistroA001(filtro));
		registro0000.setRegistroA990(this.createRegistroA990(filtro));
		registro0000.setRegistroC001(this.createRegistroC001(filtro));
		registro0000.setRegistroC990(this.createRegistroC990(filtro));
		registro0000.setRegistroD001(this.createRegistroD001(filtro));
		registro0000.setRegistroD990(this.createRegistroD990(filtro));
//		registro0000.setRegistroF001(this.createRegistroF001(filtro));
//		registro0000.setRegistroF990(this.createRegistroF990(filtro));
		registro0000.setRegistroM001(this.createRegistroM001(filtro));
		registro0000.setRegistroM990(this.createRegistroM990(filtro));
		registro0000.setRegistro1001(this.createRegistro1001(filtro));
		registro0000.setRegistro1990(this.createRegistro1990(filtro));
		registro0000.setRegistro9001(this.createRegistro9001(filtro));
		registro0000.setRegistro9990(this.createRegistro9990(filtro));
		
		filtro.getApoio().addRegistros(Registro0000.REG);
		
		return registro0000;
	}
	
	private RegistroF990 createRegistroF990(SpedpiscofinsFiltro filtro) {
		RegistroF990 registroF990 = new RegistroF990();
		
		registroF990.setReg(RegistroF990.REG);
		registroF990.setQtd_lin_f(2);
		
		filtro.getApoio().addRegistros(RegistroF990.REG);
		
		return registroF990;
	}
	
	private void createRegistroF001(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		
		RegistroF001 registroF001 = new RegistroF001();
		
		registroF001.setReg(RegistroF001.REG);				
		registroF001.setListaRegistroF010(this.createRegistroF010(filtro, spedPiscofins, apoio));
		
		if(registroF001.getListaRegistroF010() != null){
			registroF001.setInd_mov(RegistroF001.IND_MOV);
		}
				
		filtro.getApoio().addRegistros(RegistroF001.REG);
		
		spedPiscofins.getRegistro0000().setRegistroF001(registroF001);
	}
	
	private List<RegistroF010> createRegistroF010(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		List<RegistroF010> lista = new ArrayList<RegistroF010>();
		boolean retorno = false;
		for(Empresa empresa : filtro.getListaEmpresaFiliais()){
			
			RegistroF010 registroF010 = new RegistroF010();
			
			registroF010.setReg(RegistroF010.REG);
			if(empresa.getCnpj() == null || "".equals(empresa.getCnpj().getValue()))
				filtro.addError("O campo CPF da identifica��o do estabelecimento � obrigat�rio.");
			registroF010.setCnpj(empresa.getCnpj().getValue());
			registroF010.setListaRegistroF100(this.createRegistroF100(filtro, empresa));
			registroF010.setListaRegistroF600(this.createRegistroF600(filtro, spedPiscofins, empresa, apoio));
			
			if((registroF010.getListaRegistroF100() != null && !registroF010.getListaRegistroF100().isEmpty()) || 
				(registroF010.getListaRegistroF600() != null && !registroF010.getListaRegistroF600().isEmpty())){
				filtro.getApoio().addRegistros(RegistroF010.REG);
				lista.add(registroF010);
				retorno = true;
			}
		}
		
		if(!retorno) return null;
		
		return lista;		
	}
	
	private List<RegistroF600> createRegistroF600(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, Empresa empresa, SPEDPiscofinsApoio apoio) {
		List<RegistroF600> listaF600 = new ArrayList<RegistroF600>();
				
		if(spedPiscofins.getRegistro0000() != null && spedPiscofins.getRegistro0000().getRegistroA001() != null &&
				spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010() != null && 
				!spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010().isEmpty()){
			List<RegistroA010> listaA010 = spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010();
			RegistroF600 registroF600;
			Double vl_ret = 0.0;
			Boolean duplicidade = Boolean.FALSE;
			for(RegistroA010 registroA010 : listaA010){
				if(registroA010.getCnpj().equals(empresa.getCnpj().getValue()) && registroA010.getListaRegistroA100() != null && !registroA010.getListaRegistroA100().isEmpty()){
					for(RegistroA100 registroA100 : registroA010.getListaRegistroA100()){
						if(!RegistroA100.EMISSAO_PROPRIA.equals(registroA100.getInd_emit())){
							continue;
						}
						if( (registroA100.getVl_pis_ret() != null && registroA100.getVl_pis_ret() > 0) ||
							(registroA100.getVl_cofins_ret() != null && registroA100.getVl_cofins_ret() > 0)){
							registroF600 = new RegistroF600();
							registroF600.setReg(RegistroF600.REG);
							registroF600.setInd_nat_ret("03");
							registroF600.setDt_ret(registroA100.getDt_doc());							
							registroF600.setVl_bc_ret(registroA100.getVl_bc_cofins());
							
							if(registroA100.getVl_cofins_ret() != null)
								vl_ret += registroA100.getVl_cofins_ret();
							if(registroA100.getVl_pis_ret() != null)
								vl_ret += registroA100.getVl_pis_ret();
							registroF600.setVl_ret(vl_ret);							
							vl_ret = 0.0;
							
							registroF600.setInd_nat_rec(1);
							registroF600.setCnpj(apoio.getCpfOuCnpjMapParticipante(registroA100.getCod_part()));
							registroF600.setVl_ret_pis(registroA100.getVl_pis_ret() != null ? registroA100.getVl_pis_ret() : 0d);
							registroF600.setVl_ret_cofins(registroA100.getVl_cofins_ret() != null ? registroA100.getVl_cofins_ret() : 0d);
							registroF600.setInd_dec(0);
							
							// caso seja atribuido um cpf nesse campo, o registro F600 criado n�o � incluido no arquivo
							if(registroF600.getCnpj().length() <= 11) {
								continue;
							}
							
							if(listaF600 != null && !listaF600.isEmpty()){
								for(RegistroF600 f600 : listaF600){
									if(f600.getCnpj() != null && f600.getCnpj().equals(registroF600.getCnpj()) && SinedDateUtils.equalsIgnoreHour(f600.getDt_ret(), registroF600.getDt_ret())){
										f600.setVl_bc_ret(f600.getVl_bc_ret()+registroF600.getVl_bc_ret());
										f600.setVl_ret(f600.getVl_ret()+registroF600.getVl_ret());
										f600.setVl_ret_pis(f600.getVl_ret_pis()+registroF600.getVl_ret_pis());
										f600.setVl_ret_cofins(f600.getVl_ret_cofins()+registroF600.getVl_ret_cofins());
										duplicidade = Boolean.TRUE;
										break;
									}
								}
							}
							if(!duplicidade && registroF600.getCnpj() != null && !"".equals(registroF600.getCnpj())){
								filtro.getApoio().addRegistros(RegistroF600.REG);
								listaF600.add(registroF600);								
							}
							duplicidade = Boolean.FALSE;
						}
					}
				}				
			}
			
			if(listaF600 != null && !listaF600.isEmpty()){
				return listaF600;
			}else
				return null;
		}
		
		return null;
		
	}
	
	private void qtdeLinhasRegistroF990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistroF001()!= null){
			qtdeLinhas++; // Registro F001
						
			if(spedPiscofins.getRegistro0000().getRegistroF001().getListaRegistroF010() != null &&
					spedPiscofins.getRegistro0000().getRegistroF001().getListaRegistroF010().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistroF001().getListaRegistroF010().size(); // Registro F010
				
				for(RegistroF010 registroF010 : spedPiscofins.getRegistro0000().getRegistroF001().getListaRegistroF010()){
					if(registroF010.getListaRegistroF100() != null)
						qtdeLinhas+= registroF010.getListaRegistroF100().size();
					if(registroF010.getListaRegistroF600() != null)
						qtdeLinhas+= registroF010.getListaRegistroF600().size();
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistroF990() != null){
			qtdeLinhas++; // Registro F990
		
			spedPiscofins.getRegistro0000().getRegistroF990().setQtd_lin_f(qtdeLinhas);
		}
	}
	
//	private String buscaCnpjClienteByCodpart(SPEDPiscofins spedPiscofins, String codpart){
//		if(codpart == null || "".equals(codpart) || spedPiscofins == null)
//			return "";
//		
//		if(spedPiscofins.getRegistro0000() != null && spedPiscofins.getRegistro0000().getRegistro0001() != null &&
//				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null && 
//				!spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().isEmpty()){
//			List<Registro0140> lista0140 = spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140();
//			for(Registro0140 registro0140 : lista0140){
//				if(registro0140.getListaRegistro0150() != null && !registro0140.getListaRegistro0150().isEmpty()){
//					for(Registro0150 registro0150 : registro0140.getListaRegistro0150()){
//						if(registro0150.getCod_part() != null && registro0150.getCod_part().equals(codpart)){
//							return registro0150.getCnpj();
//						}
//					}
//				}
//			}
//		}
//		
//		return "";
//	}
	private void qtdeLinhasRegistro1990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		
		if(spedPiscofins.getRegistro0000().getRegistro1001()!= null){
			qtdeLinhas++; // Registro 1001
						
			if(spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010() != null && 
					spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010().size(); // Registro 1010
			
				for(Registro1010 registro1010 : spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010()){
					if(registro1010.getListaRegistro1011() != null && registro1010.getListaRegistro1011().size() > 0){
						qtdeLinhas+= registro1010.getListaRegistro1011().size(); // Registro 1011
						
					}	
					
					/*if(registroD010.getListaRegistroD500() != null && registroD010.getListaRegistroD500().size() > 0){
						qtdeLinhas+= registroD010.getListaRegistroD500().size(); // Registro D500
						
					}*/
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistro1990() != null){
			qtdeLinhas++; // Registro 1990
		
			spedPiscofins.getRegistro0000().getRegistro1990().setQtd_lin_1(qtdeLinhas);
		}
	}
	
	private Registro1990 createRegistro1990(SpedpiscofinsFiltro filtro) {
		Registro1990 registro1990 = new Registro1990();
		
		registro1990.setReg(Registro1990.REG);
	//	registro1990.setQtd_lin_1(2);
		
		filtro.getApoio().addRegistros(Registro1990.REG);
		
		return registro1990;
	}
	
	private Registro1001 createRegistro1001(SpedpiscofinsFiltro filtro) {
		List<Registro1010>registro1010 = new ArrayList<Registro1010>();
		Registro1001 registro1001 = new Registro1001();
		
		registro1001.setReg(Registro1001.REG);
	//	registro1001.setInd_mov(1);
		registro1010 = this.createRegistro1010(filtro);
		registro1001.setListaRegistro1010(registro1010);
		filtro.getApoio().addRegistros(Registro1001.REG);
		
		return registro1001;
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private Registro9990 createRegistro9990(SpedpiscofinsFiltro filtro) {
		Registro9990 registro9990 = new Registro9990();
		
		registro9990.setReg(Registro9990.REG);
		
		filtro.getApoio().addRegistros(Registro9990.REG);
		
		return registro9990;
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private Registro9001 createRegistro9001(SpedpiscofinsFiltro filtro) {
		Registro9001 registro9001 = new Registro9001();
		
		registro9001.setReg(Registro9001.REG);
		registro9001.setInd_mov(0);
		
		filtro.getApoio().addRegistros(Registro9001.REG);
		
		return registro9001;
	}
	
	private void qtdeLinhasRegistroM990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistroM001()!= null){
			qtdeLinhas++; // Registro M001
						
			if(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM100() != null &&
					spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM100().size() > 0){
				qtdeLinhas++; // Registro M100
				
				for(RegistroM100 registroM100 : spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM100()){
					if(registroM100.getListaRegistroM105() != null)
						qtdeLinhas+= registroM100.getListaRegistroM105().size();
					if(registroM100.getListaRegistroM110() != null)
						qtdeLinhas+= registroM100.getListaRegistroM110().size();
				}
			}
			
			if(spedPiscofins.getRegistro0000().getRegistroM001().getRegistroM200() != null){
				qtdeLinhas++; // Registro M200
			}
			if(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM400() != null &&
					spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM400().size() > 0){
				qtdeLinhas+=  spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM400().size(); // Registro M400
				
				for(RegistroM400 registroM400 : spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM400()){
					if(registroM400.getListaRegistroM410() != null)
						qtdeLinhas+= registroM400.getListaRegistroM410().size();
				}
			}
			if(spedPiscofins.getRegistro0000().getRegistroM001().getRegistroM600() != null){
				qtdeLinhas++; // Registro M600
			}
			if(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM800() != null &&
					spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM800().size() > 0){
				qtdeLinhas+=  spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM800().size(); // Registro M800
				
				for(RegistroM800 registroM800 : spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM800()){
					if(registroM800.getListaRegistroM810() != null)
						qtdeLinhas+= registroM800.getListaRegistroM810().size();
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistroM990() != null){
			qtdeLinhas++; // Registro M990
		
			spedPiscofins.getRegistro0000().getRegistroM990().setQtd_lin_m(qtdeLinhas);
		}
	}
	
	private RegistroM990 createRegistroM990(SpedpiscofinsFiltro filtro) {
		RegistroM990 registroM990 = new RegistroM990();
		
		registroM990.setReg(RegistroM990.REG);
		filtro.getApoio().addRegistros(RegistroM990.REG);
		
		return registroM990;
	}
	
	private RegistroM001 createRegistroM001(SpedpiscofinsFiltro filtro) {
		RegistroM001 registroM001 = new RegistroM001();
		
		registroM001.setReg(RegistroM001.REG);
		
		registroM001.setRegistroM200(createRegistroM200(filtro));
		registroM001.setListaRegistroM400(createRegistroM400(filtro));
		registroM001.setRegistroM600(this.createRegistroM600(filtro));
		registroM001.setListaRegistroM800(createRegistroM800(filtro));
		
		filtro.getApoio().addRegistros(RegistroM001.REG);
				
		return registroM001;
	}
	
	private List<RegistroM800> createRegistroM800(SpedpiscofinsFiltro filtro) {
		List<RegistroM800> listaRegistroM800 = new ArrayList<RegistroM800>();
		RegistroM800 registroM800;
		
		if(filtro.getListaAuxRegistroM810() != null && !filtro.getListaAuxRegistroM810().isEmpty()){
			for(RegistroM810 registroM810 : filtro.getListaAuxRegistroM810()){
				registroM800 = new RegistroM800();
				registroM800.setReg(RegistroM800.REG);
				registroM800.setCst_pis(registroM810.getCst_cofins_aux());
				registroM800.setVl_tot_rec(registroM810.getVl_rec());
				registroM800.setCod_cta(registroM810.getCod_cta());
	//			registroM800.setDesc_compl();
				if(registroM800.getCst_pis() != null && !"".equals(registroM800.getCst_pis()))
					addRegistroM800(listaRegistroM800, registroM800, registroM810, filtro);
			}
		}
		
		return listaRegistroM800; 
	}
	
	private List<RegistroM400> createRegistroM400(SpedpiscofinsFiltro filtro) {
		List<RegistroM400> listaRegistroM400 = new ArrayList<RegistroM400>();
		RegistroM400 registroM400;
		
		if(filtro.getListaAuxRegistroM410() != null && !filtro.getListaAuxRegistroM410().isEmpty()){
			for(RegistroM410 registroM410 : filtro.getListaAuxRegistroM410()){
				registroM400 = new RegistroM400();
				registroM400.setReg(RegistroM400.REG);
				registroM400.setCst_pis(registroM410.getCst_pis_aux());
				registroM400.setVl_tot_rec(registroM410.getVl_rec());
				registroM400.setCod_cta(registroM410.getCod_cta());
	//			registroM400.setDesc_compl();
				
				if(registroM400.getCst_pis() != null && !"".equals(registroM400.getCst_pis()))
					addRegistroM400(listaRegistroM400, registroM400, registroM410, filtro);
			}
		}
		
		return listaRegistroM400; 
	}
	
	private RegistroM600 createRegistroM600(SpedpiscofinsFiltro filtro) {
		RegistroM600 registroM600 = new RegistroM600();
		
		registroM600.setReg(RegistroM600.REG);
//		registroM600.setVl_tot_cont_nc_per();
//		registroM600.setVl_tot_cred_desc();
//		registroM600.setVl_tot_cred_desc_ant();
//		registroM600.setVl_tot_cont_nc_dev();
//		registroM600.setVl_ret_nc();
//		registroM600.setVl_out_ded_nc();
//		registroM600.setVl_cont_nc_rec();
//		registroM600.setVl_tot_cont_cum_per();
//		registroM600.setVl_ret_cum();
//		registroM600.setVl_out_ded_cum();
//		registroM600.setVl_cont_cum_rec();
//		registroM600.setVl_tot_cont_rec();
		
		registroM600.setVl_tot_cont_nc_per(0d);
		registroM600.setVl_tot_cred_desc(0d);
		registroM600.setVl_tot_cred_desc_ant(0d);
		registroM600.setVl_tot_cont_nc_dev(0d);
		registroM600.setVl_ret_nc(0d);
		registroM600.setVl_out_ded_nc(0d);
		registroM600.setVl_cont_nc_rec(0d);
		registroM600.setVl_tot_cont_cum_per(0d);
		registroM600.setVl_ret_cum(0d);
		registroM600.setVl_out_ded_cum(0d);
		registroM600.setVl_cont_cum_rec(0d);
		registroM600.setVl_tot_cont_rec(0d);
		
		filtro.getApoio().addRegistros(RegistroM600.REG);
		
//		registroM600.setListaRegistroM610(this.createRegistroM610(filtro));
		
		return registroM600; 
	}
	
//	private List<RegistroM610> createRegistroM610(SpedpiscofinsFiltro filtro) {
//		List<RegistroM610> lista = new ArrayList<RegistroM610>();
//		
//		RegistroM610 registroM610 = new RegistroM610();
//		
//		registroM610.setReg(RegistroM610.REG);
////		registroM610.setCod_cont();
////		registroM610.setVl_rec_brt();
////		registroM610.setVl_bc_cont();
////		registroM610.setAliq_cofins();
////		registroM610.setQuant_bc_cofins();
////		registroM610.setAliq_cofins_quant();
////		registroM610.setVl_cont_apur();
////		registroM610.setVl_ajus_acres();
////		registroM610.setVl_ajus_reduc();
////		registroM610.setVl_cont_difer();
////		registroM610.setVl_cont_difer_ant();
////		registroM610.setVl_cont_per();
//		
//		return lista;
//	}
	
	private RegistroM200 createRegistroM200(SpedpiscofinsFiltro filtro){
		RegistroM200 registroM200 = new RegistroM200();
		
		registroM200.setReg(RegistroM200.REG);
//		registroM200.setVl_cont_nc_rec();
//		registroM200.setVl_tot_cont_cum_per();
//		registroM200.setVl_ret_cum();
//		registroM200.setVl_out_ded_cum();
//		registroM200.setVl_cont_cum_rec();
//		registroM200.setVl_tot_cont_rec();
		
		registroM200.setVl_tot_cont_nc_per(0d);
		registroM200.setVl_tot_cred_desc(0d);
		registroM200.setVl_tot_cred_desc_ant(0d);
		registroM200.setVl_tot_cont_nc_dev(0d);
		registroM200.setVl_ret_nc(0d);
		registroM200.setVl_out_ded_nc(0d);
		registroM200.setVl_cont_nc_rec(0d);
		registroM200.setVl_tot_cont_cum_per(0d);
		registroM200.setVl_ret_cum(0d);
		registroM200.setVl_out_ded_cum(0d);
		registroM200.setVl_cont_cum_rec(0d);
		registroM200.setVl_tot_cont_rec(0d);
		
		filtro.getApoio().addRegistros(RegistroM200.REG);
//		registroM200.setListaRegistroM210(this.createRegistroM210(filtro));
		
		return registroM200;
	}
	
//	private List<RegistroM210> createRegistroM210(SpedpiscofinsFiltro filtro) {
//		List<RegistroM210> lista = new ArrayList<RegistroM210>();
//		
//		RegistroM210 registroM210 =  new RegistroM210();
//		
//		registroM210.setReg(RegistroM210.REG);
////		registroM210.setCod_cont();
////		registroM210.setVl_rec_brt();
////		registroM210.setAliq_pis();
////		registroM210.setQuant_bc_pis();
////		registroM210.setAliq_pis_quant();
////		registroM210.setVl_cont_apur();
////		registroM210.setVl_ajus_acres();
////		registroM210.setVl_ajus_reduc();
////		registroM210.setVl_cont_difer();
////		registroM210.setVl_cont_difer_ant();
////		registroM210.setVl_cont_per();
//		
//		return lista;
//	}
	
//	private RegistroM100 createRegistroM100(SpedpiscofinsFiltro filtro){
//		RegistroM100 registroM100 = new RegistroM100();
//		
//		registroM100.setReg(RegistroM001.REG);
////		registroM100.setCod_cred();
////		registroM100.setInd_cred_ori();
////		registroM100.setVl_bc_pis();
////		registroM100.setAliq_pis();
////		registroM100.setQuant_bc_pis();
////		registroM100.setAliq_pis_quant();
////		registroM100.setVl_cred();
////		registroM100.setVl_ajus_acres();
////		registroM100.setVl_ajus_reduc();
////		registroM100.setVl_cred_dif();
////		registroM100.setVl_cred_disp();
////		registroM100.setInd_desc_cred();
////		registroM100.setVl_cred_desc();
////		registroM100.setSld_cred();
//		
//		registroM100.setListaRegistroM105(this.createRegistroM105(filtro));
//		registroM100.setListaRegistroM110(this.createRegistroM110(filtro));
//		
//		return registroM100;
//	}
	
//	private List<RegistroM110> createRegistroM110(SpedpiscofinsFiltro filtro) {
//		List<RegistroM110> lista = new ArrayList<RegistroM110>();
//		
//		RegistroM110 registroM110 =  new RegistroM110();
//		
//		registroM110.setReg(RegistroM110.REG);
////		registroM110.setInd_aj();
////		registroM110.setVl_aj();
////		registroM110.setCod_aj();
////		registroM110.setNum_doc();
////		registroM110.setDescr_aj();
////		registroM110.setDt_ref();
//		
//		
//		return lista;
//	}
	
//	private List<RegistroM105> createRegistroM105(SpedpiscofinsFiltro filtro) {
//		List<RegistroM105> lista = new ArrayList<RegistroM105>();
//		
//		RegistroM105 registroM105 = new RegistroM105();
//		
//		registroM105.setReg(RegistroM105.REG);
////		registroM105.setNat_bc_cred();
////		registroM105.setCst_pis();
////		registroM105.setVl_bc_pis_tot();
////		registroM105.setVl_bc_pis_cum();
////		registroM105.setVl_bc_pis_nc();
////		registroM105.setVl_bc_pis();
////		registroM105.setQuant_bc_pis_tot();
////		registroM105.setQuant_bc_pis();
////		registroM105.setDesc_cred();
//		
//		
//		return lista;
//	}
	
	private void qtdeLinhasRegistroD990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistroD001()!= null){
			qtdeLinhas++; // Registro D001
						
			if(spedPiscofins.getRegistro0000().getRegistroD001().getListaRegistroD010() != null && 
					spedPiscofins.getRegistro0000().getRegistroD001().getListaRegistroD010().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistroD001().getListaRegistroD010().size(); // Registro D010
			
				for(RegistroD010 registroD010 : spedPiscofins.getRegistro0000().getRegistroD001().getListaRegistroD010()){
					if(registroD010.getListaRegistroD100() != null && registroD010.getListaRegistroD100().size() > 0){
						qtdeLinhas+= registroD010.getListaRegistroD100().size(); // Registro D100
						
					}	
					
					if(registroD010.getListaRegistroD500() != null && registroD010.getListaRegistroD500().size() > 0){
						qtdeLinhas+= registroD010.getListaRegistroD500().size(); // Registro D500
						
					}
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistroD990() != null){
			qtdeLinhas++; // Registro D990
		
			spedPiscofins.getRegistro0000().getRegistroD990().setQtd_lin_d(qtdeLinhas);
		}
	}
	
	private RegistroD990 createRegistroD990(SpedpiscofinsFiltro filtro) {
		RegistroD990 registroD990 = new RegistroD990();
		
		registroD990.setReg(RegistroD990.REG);
		filtro.getApoio().addRegistros(RegistroD990.REG);
		
		return registroD990;
	}
	private RegistroD001 createRegistroD001(SpedpiscofinsFiltro filtro) {
		RegistroD001 registroD001 = new RegistroD001();
		
		registroD001.setReg(RegistroD001.REG);
		registroD001.setListaRegistroD010(this.createRegistroD010(filtro));
		
		filtro.getApoio().addRegistros(RegistroD001.REG);
		return registroD001;
	}
	
	private List<RegistroD010> createRegistroD010(SpedpiscofinsFiltro filtro) {
		List<RegistroD010> lista = new ArrayList<RegistroD010>();
		
		for(Empresa empresa : filtro.getListaEmpresaFiliais()){
			RegistroD010 registroD010 = new RegistroD010();
			
			registroD010.setReg(RegistroD010.REG);
			if(empresa.getCnpj() == null || "".equals(empresa.getCnpj().getValue()))
				filtro.addError("O campo CPF da identifica��o do estabelecimento � obrigat�rio.");
			registroD010.setCnpj(empresa.getCnpj().getValue());
			
			registroD010.setListaRegistroD100(this.createRegistroD100(filtro, empresa));
			if(filtro.getGerarD500() != null && filtro.getGerarD500()){
				registroD010.setListaRegistroD500(this.createRegistroD500(filtro, empresa));
			}
			
			filtro.getApoio().addRegistros(RegistroD010.REG);
			
			lista.add(registroD010);
		}
		
		return lista;
	}
	
	private List<RegistroD500> createRegistroD500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroD500> lista = new ArrayList<RegistroD500>();
		RegistroD500 registroD500;
		Entregamaterial entregameterial;
		List<Entregadocumento> listaDocumentotributo = this.getListaDocumentoD500(filtro, empresa);
		for (Entregadocumento ed : listaDocumentotributo) {
			registroD500 = new RegistroD500();
			
			entregameterial = ed.getListaEntregamaterial().iterator().next();
			
			registroD500.setReg(RegistroD500.REG);
			registroD500.setInd_oper(0);
			registroD500.setInd_emit(1);
			registroD500.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
			registroD500.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroD500.setCod_sit(ed.getSituacaosped());
//			registroD500.setSer();
//			registroD500.setSub();
			registroD500.setNum_doc(ed.getNumero());
			registroD500.setDt_doc(ed.getDtemissao());
			registroD500.setDt_a_p(ed.getDtentrada());
			registroD500.setVl_doc(ed.getValortotaldocumento().getValue().doubleValue());
//			registroD500.setVl_desc();
			registroD500.setVl_serv(ed.getValortotaldocumento().getValue().doubleValue());
//			registroD500.setVl_serv_nt();
//			registroD500.setVl_terc();
//			registroD500.setVl_da();
			registroD500.setVl_bc_icms(entregameterial.getValorbcicms() != null ? entregameterial.getValorbcicms().getValue().doubleValue() : null);
			registroD500.setVl_icms(entregameterial.getValoricms() != null ? entregameterial.getValoricms().getValue().doubleValue() : null);
//			registroD500.setCod_inf();
			registroD500.setVl_pis(entregameterial.getValorpis() != null ? entregameterial.getValorpis().getValue().doubleValue() : null);
			registroD500.setVl_cofins(entregameterial.getValorcofins() != null ? entregameterial.getValorcofins().getValue().doubleValue() : null);
			
			lista.add(registroD500);
			
			filtro.getApoio().addDocumento(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroD500.REG);
		}
		
		return lista;
	}
	private List<RegistroD100> createRegistroD100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroD100> listaDesagrupada = new ArrayList<RegistroD100>();
		
		RegistroD100 registroD100;
		
		List<Entregadocumento> listaEntregadocumento = this.getListaEntregaD100(filtro, empresa);
		for (Entregadocumento ed : listaEntregadocumento) {
			registroD100 = new RegistroD100();
						
			registroD100.setReg(RegistroD100.REG);
			registroD100.setInd_oper(0);
			registroD100.setInd_emit(1);
			registroD100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
			registroD100.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroD100.setCod_sit(ed.getSituacaosped());
			registroD100.setSer(ed.getSerie() != null ? ed.getSerie().toString() : null);
//			registroD100.setSub();
			registroD100.setNum_doc(ed.getNumero());
			registroD100.setChv_cte(ed.getChaveacesso());
			registroD100.setDt_doc(ed.getDtemissao());
			registroD100.setDt_a_p(ed.getDtentrada());
//			registroD100.setTp_ct_e();
//			registroD100.setChv_cte_ref();
			registroD100.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
			registroD100.setVl_desc(ed.getValordesconto() != null ? ed.getValordesconto().getValue().doubleValue() : null);
			registroD100.setInd_frt(ed.getResponsavelfrete() != null ? ed.getResponsavelfrete().getCdnfe() : null);
			registroD100.setVl_serv(ed.getValor() != null ? ed.getValor().getValue().doubleValue() : null);
			registroD100.setVl_bc_icms(0d);//registroD100.setVl_bc_icms(entrega.getValorbcicms() != null ? entrega.getValorbcicms().getValue().doubleValue() : null);
			registroD100.setVl_icms(0d);//registroD100.setVl_icms(entrega.getValoricms() != null ? entrega.getValoricms().getValue().doubleValue() : null);
//			registroD100.setVl_nt();
			
//			Pegar o primeiro material e adicionar a CG
			Entregamaterial entregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class, ed.getListaEntregamaterial()).get(0);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), ed.getNaturezaoperacao(), entregamaterial!=null && entregamaterial.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), entregamaterial.getMaterial(), empresa) : null);
			registroD100.setCod_cta(contacontabil != null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			
			if(ed.getInfoadicionalcontrib() != null && !ed.getInfoadicionalcontrib().trim().equals("") && ed.getInfoadicionalfisco() != null && !ed.getInfoadicionalfisco().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalcontrib() + " " + ed.getInfoadicionalfisco(), ed.getEmpresa().getCnpj().getValue()));
			} else if(ed.getInfoadicionalcontrib() != null && !ed.getInfoadicionalcontrib().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalcontrib(), ed.getEmpresa().getCnpj().getValue()));
			} else if(ed.getInfoadicionalfisco() != null && !ed.getInfoadicionalfisco().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalfisco(), ed.getEmpresa().getCnpj().getValue()));
			}			
			
			listaDesagrupada.add(registroD100);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroD100.REG);
			filtro.getApoio().addIdentificador(registroD100.getCod_cta());
		}
		
		List<RegistroD100> listaAgrupada = this.joinListaRegistroD100(listaDesagrupada, filtro);
		
		return listaAgrupada;
	}
	
	private void qtdeLinhasRegistroC990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistroC001()!= null){
			qtdeLinhas++; // Registro C001
						
			if(spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010() != null && 
					spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010().size(); // Registro C010
				
				for(RegistroC010 registroC010 : spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010()){
					if(registroC010.getListaRegistroC100() != null && registroC010.getListaRegistroC100().size() > 0){
						qtdeLinhas+= registroC010.getListaRegistroC100().size(); // Registro C100
						
						for(RegistroC100 registroC100 : registroC010.getListaRegistroC100()){
							if(registroC100.getListaRegistroC110() != null)
								qtdeLinhas+= registroC100.getListaRegistroC110().size(); // Registro C110
							if(registroC100.getListaRegistroC120() != null)
								qtdeLinhas+= registroC100.getListaRegistroC120().size(); // Registro C120
							if(registroC100.getListaRegistroC170() != null)
								qtdeLinhas+= registroC100.getListaRegistroC170().size(); // Registro C170
							if(registroC100.getListaRegistroC175() != null)
								qtdeLinhas+= registroC100.getListaRegistroC175().size(); // Registro C175
						}
					}
					
					if(registroC010.getListaRegistroC400() != null && registroC010.getListaRegistroC400().size() > 0){
						qtdeLinhas += registroC010.getListaRegistroC400().size(); // Registro C400
						
						for(RegistroC400 registroC400 :  registroC010.getListaRegistroC400()){
							if(registroC400.getListaRegistroC405() != null){
								qtdeLinhas += registroC400.getListaRegistroC405().size(); // Registro C405
								
								for (RegistroC405 registroC405 : registroC400.getListaRegistroC405()) {
									if(registroC405.getListaRegistroC481() != null)
										qtdeLinhas += registroC405.getListaRegistroC481().size(); // Registro C481
									if(registroC405.getListaRegistroC485() != null)
										qtdeLinhas += registroC405.getListaRegistroC485().size(); // Registro C485
								}
								
							}
						}
					}
					
					if(registroC010.getListaRegistroC500() != null && registroC010.getListaRegistroC500().size() > 0){
						qtdeLinhas += registroC010.getListaRegistroC500().size(); // Registro C500
					
						for(RegistroC500 registroC500 :  registroC010.getListaRegistroC500()){
							if(registroC500.getListaRegistroC501() != null)
								qtdeLinhas+= registroC500.getListaRegistroC501().size(); // Registro C501
							if(registroC500.getListaRegistroC505() != null)
								qtdeLinhas+= registroC500.getListaRegistroC505().size(); // Registro C505
						}
					}
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistroC990() != null){
			qtdeLinhas++; // Registro C990
		
			spedPiscofins.getRegistro0000().getRegistroC990().setQtd_lin_c(qtdeLinhas);
		}
	}
	
	private RegistroC990 createRegistroC990(SpedpiscofinsFiltro filtro) {
		RegistroC990 registroC990 = new RegistroC990();
		
		registroC990.setReg(RegistroC990.REG);
		filtro.getApoio().addRegistros(RegistroC990.REG);
		
		return registroC990;
	}
	
	private RegistroC001 createRegistroC001(SpedpiscofinsFiltro filtro) {		
		RegistroC001 registroC001 = new RegistroC001();
		
		registroC001.setReg(RegistroC001.REG);		
		registroC001.setListaRegistroC010(this.createRegistroC010(filtro));
		
		filtro.getApoio().addRegistros(RegistroC001.REG);
		return registroC001;
	}
	
	private List<RegistroC010> createRegistroC010(SpedpiscofinsFiltro filtro) {
		List<RegistroC010> lista = new ArrayList<RegistroC010>();
		
		for(Empresa empresa : filtro.getListaEmpresaFiliais()){
			RegistroC010 registroC010 = new RegistroC010();
			
			registroC010.setReg(RegistroC010.REG);
			if(empresa.getCnpj() == null || "".equals(empresa.getCnpj().getValue()))
				filtro.addError("O campo CPF da identifica��o do estabelecimento � obrigat�rio.");
			registroC010.setCnpj(empresa.getCnpj().getValue());
	//		registroC010.setInd_escri();
			
			registroC010.setListaRegistroC100(this.createRegistroC100(filtro, empresa));
			registroC010.setListaRegistroC400(this.createRegistroC400(filtro, empresa));
			if(filtro.getGerarC500() != null && filtro.getGerarC500()){
				registroC010.setListaRegistroC500(this.createRegistroC500(filtro, empresa));
			}
			
			filtro.getApoio().addRegistros(RegistroC010.REG);
			
			lista.add(registroC010);
		}
		return lista;
	}
	
	private List<RegistroC400> createRegistroC400(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroC400> lista = new ArrayList<RegistroC400>();
		
		boolean saidas = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
		if(saidas){
			RegistroC400 registroC400;
			Empresa empresaBean = empresaService.load(empresa, "empresa.cdpessoa, empresa.integracaopdv");
			if(empresaBean != null){
				empresa.setIntegracaopdv(empresaBean.getIntegracaopdv());
			}
			List<Emporiumpdv> listaEmporiumpdv = emporiumpdvService.findByEmpresa(empresa);
			for (Emporiumpdv emporiumpdv : listaEmporiumpdv) {
				if(Boolean.TRUE.equals(emporiumpdv.getEquipamentoNaoFiscal())){//Para equipamento n�o fiscal, � teoria vai ser gerado o registro C175, que � referente a NFC-e.
					continue;
				}
				List<Emporiumreducaoz> listaEmporiumreducaoz = emporiumreducaozService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
				
				if(listaEmporiumreducaoz != null && listaEmporiumreducaoz.size() > 0){
					registroC400 = new RegistroC400();
					
					registroC400.setReg(RegistroC400.REG);
					registroC400.setCod_mod(emporiumpdv.getModelodocfiscal());
					registroC400.setEcf_mod(emporiumpdv.getModeloequipamento());
					registroC400.setEcf_fab(emporiumpdv.getNumeroserie());
					registroC400.setEcf_cx(emporiumpdv.getCodigoemporium());
					
					registroC400.setListaRegistroC405(this.createRegistroC405(filtro, emporiumpdv, listaEmporiumreducaoz, empresa));
					
					filtro.getApoio().addRegistros(RegistroC400.REG);
					
					lista.add(registroC400);
				}
			}
		}
		
		return lista;
	}
	
	private List<RegistroC405> createRegistroC405(SpedpiscofinsFiltro filtro, Emporiumpdv emporiumpdv, List<Emporiumreducaoz> listaEmporiumreducaoz, Empresa empresaFiltro) {
		List<RegistroC405> lista = new ArrayList<RegistroC405>();
		RegistroC405 registroC405;
		
		Empresa empresa = emporiumpdv.getEmpresa();
		String cnpj = null;
		if(empresa != null && empresa.getCnpj() != null){
			cnpj = empresa.getCnpj().getValue();
		}
		
		List<Emporiumvenda> listaEmporiumvenda = emporiumvendaService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
		
		List<Emporiumvendaitem> listaEmporiumvendaitem = new ArrayList<Emporiumvendaitem>();
		for (Emporiumvenda emporiumvenda : listaEmporiumvenda) {
			if(emporiumvenda.getListaEmporiumvendaitem() != null && emporiumvenda.getListaEmporiumvendaitem().size() > 0){
				Double valortotal = emporiumvenda.getValor();
				Double desconto = emporiumvenda.getValor_desconto();
				Double descontoRestante = desconto != null ? new Double(desconto) : 0d;
				
				// FEITO ISSO POIS O VALOR VEM COM O DESCONTO APLICADO
				valortotal += desconto != null ? desconto : 0d;
				
				for (int i = 0; i < emporiumvenda.getListaEmporiumvendaitem().size(); i++) {
					Emporiumvendaitem it = emporiumvenda.getListaEmporiumvendaitem().get(i);
					if(desconto != null && desconto > 0){
						Double valor_it = it.getValortotal();
						Double percent = (valor_it * 100d)/valortotal;
						Double desconto_it = (percent * desconto)/100d;
						
						descontoRestante -= desconto_it;
						if((i+1) == emporiumvenda.getListaEmporiumvendaitem().size()){
							desconto_it += descontoRestante;
						}
						
						it.setDesconto_it(desconto_it);
					}
					listaEmporiumvendaitem.add(it);
				}
			}
		}
		
		for(Emporiumreducaoz emporiumreducaoz: listaEmporiumreducaoz){
			registroC405 = new RegistroC405();
			
			registroC405.setReg(RegistroC405.REG);
			registroC405.setDt_doc(emporiumreducaoz.getData());
			registroC405.setCro(emporiumreducaoz.getCro());
			registroC405.setCrz(emporiumreducaoz.getCrz());
			registroC405.setNum_coo_fin(emporiumreducaoz.getNumerocoofinal());
			registroC405.setGt_fin(emporiumreducaoz.getGtfinal());
			registroC405.setVl_brt(emporiumreducaoz.getValorvendabruta());
			
			List<Emporiumvendaitem> listaEmporiumvendaitemData = this.getEmporiumvendaitemByDataMovimento(emporiumreducaoz.getData(), listaEmporiumvendaitem, empresaFiltro);
			
			Map<Material, Double> mapMaterial = new HashMap<Material, Double>();
			for(Emporiumvendaitem it: listaEmporiumvendaitemData){
				if(it.getMaterial() == null){
					continue;
				}
				
				if(mapMaterial.containsKey(it.getMaterial())){
					Double valor = mapMaterial.get(it.getMaterial()) + it.getValorComDesconto();
					mapMaterial.put(it.getMaterial(), valor);
				} else {
					mapMaterial.put(it.getMaterial(), it.getValorComDesconto());
				}
			}
			
			registroC405.setListaRegistroC481(this.createRegistroC481(filtro, cnpj, mapMaterial));
			registroC405.setListaRegistroC485(this.createRegistroC485(filtro, cnpj, mapMaterial));
			
			filtro.getApoio().addRegistros(RegistroC405.REG);
			
			lista.add(registroC405);
		}
		
		return lista;
	}
	
	private List<Emporiumvendaitem> getEmporiumvendaitemByDataMovimento(Date datamovimento, List<Emporiumvendaitem> listaEmporiumvendaitem, Empresa empresa) {
		List<Emporiumvendaitem> listaFinal = new ArrayList<Emporiumvendaitem>();
		
		for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitem) {
			if(emporiumvendaitem.getEmporiumvenda() != null &&
					emporiumvendaitem.getEmporiumvenda().getLoja().equals(empresa.getIntegracaopdv()) &&
					emporiumvendaitem.getEmporiumvenda().getData_hora() != null &&
					SinedDateUtils.equalsIgnoreHour(SinedDateUtils.timestampToDate(emporiumvendaitem.getEmporiumvenda().getData_hora()), datamovimento)){
				listaFinal.add(emporiumvendaitem);
			}
		}
		
		return listaFinal;
	}
	
	private List<RegistroC481> createRegistroC481(SpedpiscofinsFiltro filtro, String cnpj, Map<Material, Double> mapMaterial) {
		List<RegistroC481> lista = new ArrayList<RegistroC481>();
		RegistroC481 registroC481;
		
		Set<Entry<Material, Double>> entrySetMaterial = mapMaterial.entrySet();
		for (Entry<Material, Double> entry : entrySetMaterial) {
			Material material = entry.getKey();
			Double valor = SinedUtil.round(entry.getValue(), 2);
			
			if(material != null && 
					material.getTributacaoecf() != null && 
					material.getTributacaoecf().getCstpis() != null &&
					valor > 0){
				
				Tributacaoecf tributacaoecf = material.getTributacaoecf();
				
				Double aliqPis = tributacaoecf.getAliqpis() != null ? tributacaoecf.getAliqpis() : 0d;
				Double vlPis = (aliqPis/100d) * valor;
				
				Tipocobrancapis cstPis = tributacaoecf.getCstpis();
				
				registroC481 = new RegistroC481();
				
				registroC481.setReg(RegistroC481.REG);
				registroC481.setCst_pis(cstPis.getCdnfe());
				registroC481.setVl_item(valor);
				registroC481.setVl_bc_pis(valor);
				registroC481.setAliq_pis(aliqPis);
//				registroC481.setQuant_bc_pis(quantBcPis);
//				registroC481.setAliq_pis_quant(aliqPisQuant);
				registroC481.setVl_pis(vlPis);
				registroC481.setCod_item(filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), cnpj, filtro.isCnpjEmpresaFiltro())));
				ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), null, materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), material, filtro.getEmpresa()));
				registroC481.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil() != null ? contacontabil.getVcontacontabil().getIdentificador() : "");
				
				filtro.getApoio().addRegistros(RegistroC481.REG);
				
				lista.add(registroC481);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC485> createRegistroC485(SpedpiscofinsFiltro filtro, String cnpj, Map<Material, Double> mapMaterial) {
		List<RegistroC485> lista = new ArrayList<RegistroC485>();
		RegistroC485 registroC485;
		
		Set<Entry<Material, Double>> entrySetMaterial = mapMaterial.entrySet();
		for (Entry<Material, Double> entry : entrySetMaterial) {
			Material material = entry.getKey();
			Double valor = SinedUtil.round(entry.getValue(), 2);
			
			if(material != null && 
					material.getTributacaoecf() != null && 
					material.getTributacaoecf().getCstcofins() != null &&
					valor > 0){
				
				Tributacaoecf tributacaoecf = material.getTributacaoecf();
				
				Double aliqcofins = tributacaoecf.getAliqcofins() != null ? tributacaoecf.getAliqcofins() : 0d;
				Double vlcofins = (aliqcofins/100d) * valor;
				
				Tipocobrancacofins cstcofins = tributacaoecf.getCstcofins();
				
				registroC485 = new RegistroC485();
				
				registroC485.setReg(RegistroC485.REG);
				registroC485.setCst_cofins(cstcofins.getCdnfe());
				registroC485.setVl_item(valor);
				registroC485.setVl_bc_cofins(valor);
				registroC485.setAliq_cofins(aliqcofins);
//				registroC485.setQuant_bc_cofins(quantBccofins);
//				registroC485.setAliq_cofins_quant(aliqcofinsQuant);
				registroC485.setVl_cofins(vlcofins);
				registroC485.setCod_item(filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), cnpj, filtro.isCnpjEmpresaFiltro())));
				ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), null, materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), material, filtro.getEmpresa()));
				registroC485.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil() != null ? contacontabil.getVcontacontabil().getIdentificador() : "");
				
				filtro.getApoio().addRegistros(RegistroC485.REG);
				
				lista.add(registroC485);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC500> createRegistroC500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroC500> lista = new ArrayList<RegistroC500>();
		RegistroC500 registroC500;
		Entregamaterial entregamaterial;
		
		List<Entregadocumento> listaDocumento = this.getListaDocumentoC500(filtro, empresa);
		for (Entregadocumento ed : listaDocumento) {
			
			registroC500 = new RegistroC500();
			
			entregamaterial = ed.getListaEntregamaterial().iterator().next();
			
			registroC500.setReg(RegistroC500.REG);
			registroC500.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
			registroC500.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroC500.setCod_sit(ed.getSituacaosped());
//			registroC500.setSer();
//			registroC500.setSub();
			registroC500.setNum_doc(ed.getNumero());
			registroC500.setDt_doc(ed.getDtemissao());
			registroC500.setDt_ent(ed.getDtentrada());
			registroC500.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);			
			registroC500.setVl_icms(entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().getValue().doubleValue() : null);
//			registroC500.setCod_inf();
			registroC500.setVl_pis(entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().getValue().doubleValue() : null);
			registroC500.setVl_cofins(entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().getValue().doubleValue() : null);
			if(("66".equals(registroC500.getCod_mod()) || "55".equals(registroC500.getCod_mod())) && ed.getChaveacesso() != null){
				registroC500.setChv_doce(ed.getChaveacesso());			
			}
			
			registroC500.setListaRegistroC501(this.createRegistroC501(filtro, ed, entregamaterial, new Money(entregamaterial.getValortotalmaterial()), ed.getNumero()));
			registroC500.setListaRegistroC505(this.createRegistroC505(filtro, ed, entregamaterial, new Money(entregamaterial.getValortotalmaterial()), ed.getNumero()));
			
			lista.add(registroC500);
			
			filtro.getApoio().addDocumento(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroC500.REG);
		}
		
		return lista;
	}
	private List<RegistroC505> createRegistroC505(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento, Entregamaterial em, Money valoritem, String numerodocumento ) {
		
		List<RegistroC505> lista = new ArrayList<RegistroC505>();
		
		RegistroC505 registroC505 = new RegistroC505();
		
		registroC505.setReg(RegistroC505.REG);
		registroC505.setCst_cofins(em.getCstcofins() != null ? em.getCstcofins().getCdnfe() : null);
		registroC505.setVl_item(valoritem != null ? valoritem.getValue().doubleValue() : null);
		registroC505.setNat_bc_cred(em.getNaturezabcc() != null ? em.getNaturezabcc().getCdsped() : null);
		if(em.getValorbccofins() != null){
			registroC505.setVl_bc_cofins(em.getValorbccofins().getValue().doubleValue());
		}
		if(em.getCofins() != null){
			registroC505.setAliq_cofins(em.getCofins());
		}
		if(em.getValorcofins() != null){
			registroC505.setVl_cofins(em.getValorcofins().getValue().doubleValue());
		}
		ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento.getNaturezaoperacao(), em.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), em.getMaterial(), entregadocumento.getEmpresa()) : null);
		registroC505.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
		
		filtro.getApoio().addRegistros(RegistroC505.REG);
		filtro.getApoio().addIdentificador(registroC505.getCod_cta());
		lista.add(registroC505);
		
		return lista;
		
	}

	private List<RegistroC501> createRegistroC501(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento, Entregamaterial em, Money valoritem, String numerodocumento) {
		
		List<RegistroC501> lista = new ArrayList<RegistroC501>();
		
		RegistroC501 registroC501 = new RegistroC501();
		
		registroC501.setReg(RegistroC501.REG);
		registroC501.setCst_pis(em.getCstpis() != null ? em.getCstpis().getCdnfe() : null);
		registroC501.setVl_item(valoritem != null ? valoritem.getValue().doubleValue() : null);
		registroC501.setNat_bc_cred(em.getNaturezabcc() != null ? em.getNaturezabcc().getCdsped() : null);
		if(em.getValorbcpis() != null){
			registroC501.setVl_bc_pis( em.getValorbcpis().getValue().doubleValue());
		}
		if(em.getPis() != null){
			registroC501.setAliq_pis(em.getPis());
		}
		if(em.getValorpis() != null){
			registroC501.setVl_pis(em.getValorpis().getValue().doubleValue());
		}
		ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento.getNaturezaoperacao(), em.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), em.getMaterial(), entregadocumento.getEmpresa()) : null);
		registroC501.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
		
		filtro.getApoio().addRegistros(RegistroC501.REG);
		filtro.getApoio().addIdentificador(registroC501.getCod_cta());
		
		lista.add(registroC501);
		
		return lista;
		
	}
	
	private List<RegistroC100> createRegistroC100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroC100> lista = new ArrayList<RegistroC100>();
		RegistroC100 registroC100;
		
		List<Notafiscalproduto> listaNotafiscalproduto = this.getListaNotaC100(filtro, empresa);
		List<Entregadocumento> listaEntregadocumento = this.getListaEntregaC100(filtro, empresa);
		
		int ano = SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR);
		
		Arquivonfnota arquivonfnota;
		for (Notafiscalproduto notafiscalproduto : listaNotafiscalproduto) {
			boolean isNfce = ModeloDocumentoFiscalEnum.NFCE.equals(notafiscalproduto.getModeloDocumentoFiscalEnum());
			registroC100 = new RegistroC100();
			
			registroC100.setReg(RegistroC100.REG);
			registroC100.setInd_oper(notafiscalproduto.getTipooperacaonota() != null ? notafiscalproduto.getTipooperacaonota().getValue() : null);
			registroC100.setInd_emit(RegistroC100.EMISSAO_PROPRIA);
			
			Integer cdpessoa = notafiscalproduto.getCliente().getCdpessoa();
			
			registroC100.setCod_sit(notafiscalproduto.getSituacaosped());
			registroC100.setNum_doc(notafiscalproduto.getNumero());
			registroC100.setDt_doc(notafiscalproduto.getDtEmissao());
			registroC100.setDt_e_s(SinedDateUtils.timestampToDate(notafiscalproduto.getDatahorasaidaentrada()));
			registroC100.setVl_doc(notafiscalproduto.getValor() != null ? notafiscalproduto.getValor().getValue().doubleValue() : null);
			
			arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
			if(arquivonfnota != null){
				registroC100.setChv_nfe(arquivonfnota.getChaveacesso());
				if(arquivonfnota.getArquivonf() != null){
					if(arquivonfnota.getArquivonf().getConfiguracaonfe() != null){
						registroC100.setCod_mod(arquivonfnota.getArquivonf().getConfiguracaonfe().getModelonfe());
					}else if(Boolean.TRUE.equals(notafiscalproduto.getOrigemCupom()) && isNfce){
						registroC100.setCod_mod(ModeloDocumentoFiscalEnum.NFCE.getValue());
					}
				}
				if(arquivonfnota.getNota() != null){
					registroC100.setSer(arquivonfnota.getNota().getSerienfe() != null ? arquivonfnota.getNota().getSerienfe().toString() : null);
				}
			}

			String codPart = "";
			if(notafiscalproduto.getEnderecoCliente() != null && 
					notafiscalproduto.getEnderecoCliente().getInscricaoestadual() != null &&
					!"".equals(notafiscalproduto.getEnderecoCliente().getInscricaoestadual())){
				if(notafiscalproduto.getCliente().getCpfOuCnpjValue() != null || !isNfce){
					codPart = notafiscalproduto.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa, notafiscalproduto.getEmpresa().getCnpj().getValue(), notafiscalproduto.getEnderecoCliente().getCdendereco(), notafiscalproduto.getCliente().getCpfOuCnpjValue()) : null;
					codPart = codPart + "-" + notafiscalproduto.getEnderecoCliente().getCdendereco();
				}
			} else {
				if(notafiscalproduto.getCliente().getCpfOuCnpjValue() != null || !isNfce){
					codPart = notafiscalproduto.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa, notafiscalproduto.getEmpresa().getCnpj().getValue(), null, notafiscalproduto.getCliente().getCpfOuCnpjValue()) : null;
				}
			}
			registroC100.setCod_part(codPart);
			
			
			if(notafiscalproduto.getFormapagamentonfe() != null){
				if(SinedDateUtils.afterOrEqualsIgnoreHour(filtro.getDtinicio(), SinedDateUtils.stringToSqlDate("01/07/2012"))){
					registroC100.setInd_pgto(notafiscalproduto.getFormapagamentonfe().getValue());
				} else {
					if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.A_VISTA)){
						registroC100.setInd_pgto(0);
					} else if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.A_PRAZO)){
						registroC100.setInd_pgto(1);
					} else if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.OUTROS)){
						registroC100.setInd_pgto(9);
					}
				}
			}
			
			if(notafiscalproduto.getResponsavelfrete() != null){
				if(ano >= 2012){
					registroC100.setInd_frt(notafiscalproduto.getResponsavelfrete().getCdnfe());
				} else {
					int cdnfe = notafiscalproduto.getResponsavelfrete().getCdnfe();
					switch (cdnfe) {
						case 0:
							registroC100.setInd_frt(1);
							break;
						case 1:
							registroC100.setInd_frt(2);
							break;
						case 2:
							registroC100.setInd_frt(0);
							break;
						case 9:
							registroC100.setInd_frt(9);
							break;
					}
				}
			}
			
			registroC100.setVl_desc(notafiscalproduto.getValordesconto() != null ? notafiscalproduto.getValordesconto().getValue().doubleValue() : null);
//			registroC100.setVl_abat_nt();
			registroC100.setVl_merc(notafiscalproduto.getValorprodutos() != null ? notafiscalproduto.getValorprodutos().getValue().doubleValue() : null);
			registroC100.setVl_frt(notafiscalproduto.getValorfrete() != null ? notafiscalproduto.getValorfrete().getValue().doubleValue() : null);
			registroC100.setVl_seg(notafiscalproduto.getValorseguro() != null ? notafiscalproduto.getValorseguro().getValue().doubleValue() : null);
			registroC100.setVl_out_da(notafiscalproduto.getOutrasdespesas() != null ? notafiscalproduto.getOutrasdespesas().getValue().doubleValue() : null);
			registroC100.setVl_bc_icms(notafiscalproduto.getValorbcicms() != null && notafiscalproduto.getValorbcicms().getValue().doubleValue() > 0 ? notafiscalproduto.getValorbcicms().getValue().doubleValue() : null);
			registroC100.setVl_icms(notafiscalproduto.getValoricms() != null && notafiscalproduto.getValoricms().getValue().doubleValue() > 0 ? notafiscalproduto.getValoricms().getValue().doubleValue() : null);
			registroC100.setVl_bc_icms_st(notafiscalproduto.getValorbcicmsst() != null && notafiscalproduto.getValorbcicmsst().getValue().doubleValue() > 0 ? notafiscalproduto.getValorbcicmsst().getValue().doubleValue() : null);
			registroC100.setVl_icms_st(notafiscalproduto.getValoricmsst() != null && notafiscalproduto.getValoricmsst().getValue().doubleValue() > 0 ? notafiscalproduto.getValoricmsst().getValue().doubleValue() : null);
			registroC100.setVl_ipi(notafiscalproduto.getValoripi() != null && notafiscalproduto.getValoripi().getValue().doubleValue() > 0 ? notafiscalproduto.getValoripi().getValue().doubleValue() : null);
			registroC100.setVl_pis(notafiscalproduto.getValorpis() != null && notafiscalproduto.getValorpis().getValue().doubleValue() > 0 ? notafiscalproduto.getValorpis().getValue().doubleValue() : null);
			registroC100.setVl_cofins(notafiscalproduto.getValorcofins() != null && notafiscalproduto.getValorcofins().getValue().doubleValue() > 0 ? notafiscalproduto.getValorcofins().getValue().doubleValue() : null);
			registroC100.setVl_pis_st(notafiscalproduto.getValorpisretido() != null && notafiscalproduto.getValorpisretido().getValue().doubleValue() > 0 ? notafiscalproduto.getValorpisretido().getValue().doubleValue():null);
			registroC100.setVl_cofins_st(notafiscalproduto.getValorcofinsretido() != null && notafiscalproduto.getValorcofinsretido().getValue().doubleValue() > 0 ? notafiscalproduto.getValorcofinsretido().getValue().doubleValue():null);
			if(!isNfce){
				registroC100.setListaRegistroC110(this.createRegistroC110(filtro, notafiscalproduto));
			}
			registroC100.setListaRegistroC120(this.createRegistroC120(filtro, notafiscalproduto));
			if(Boolean.TRUE.equals(filtro.getGerarC170Saida()) && !isNfce){
				registroC100.setListaRegistroC170(this.createRegistroC170(filtro, notafiscalproduto));
			}
			if(isNfce){
				registroC100.setListaRegistroC175(this.createRegistroC175(filtro, notafiscalproduto));
			}
			
			lista.add(registroC100);
			
			filtro.getApoio().addNota(notafiscalproduto.getCdNota());
			filtro.getApoio().addRegistros(RegistroC100.REG);
		}
		
		List<RegistroC100> listaDesagrupada = new ArrayList<RegistroC100>();
		for (Entregadocumento ed : listaEntregadocumento) {
			registroC100 = new RegistroC100();
			
			registroC100.setReg(RegistroC100.REG);
			registroC100.setInd_oper(Tipooperacaonota.ENTRADA.getValue());
			registroC100.setInd_emit(RegistroC100.TERCEIROS);
			registroC100.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroC100.setCod_sit(ed.getSituacaosped());
			registroC100.setNum_doc(SPEDUtil.soNumero(ed.getNumero()));
			if(ed.getChaveacesso() != null){
				registroC100.setChv_nfe(ed.getChaveacesso());
			}			
			registroC100.setDt_e_s(ed.getDtentrada());
			registroC100.setVl_doc(ed.getValor() != null ? ed.getValor().getValue().doubleValue() : null);
			
			if(ed.getIndpag() != null){
				if(SinedDateUtils.afterOrEqualsIgnoreHour(filtro.getDtinicio(), SinedDateUtils.stringToSqlDate("01/07/2012"))){
					registroC100.setInd_pgto(ed.getIndpag().getValue());
				} else {
					if(ed.getIndpag().equals(Formapagamentonfe.A_VISTA)){
						registroC100.setInd_pgto(0);
					} else if(ed.getIndpag().equals(Formapagamentonfe.A_PRAZO)){
						registroC100.setInd_pgto(1);
					} else if(ed.getIndpag().equals(Formapagamentonfe.OUTROS)){
						registroC100.setInd_pgto(9);
					}
				}
			}
			
			if(ed.getResponsavelfrete() != null){
				if(ano >= 2012){
					registroC100.setInd_frt(ed.getResponsavelfrete().getCdnfe());
				} else {
					int cdnfe = ed.getResponsavelfrete().getCdnfe();
					switch (cdnfe) {
						case 0:
							registroC100.setInd_frt(1);
							break;
						case 1:
							registroC100.setInd_frt(2);
							break;
						case 2:
							registroC100.setInd_frt(0);
							break;
						case 9:
							registroC100.setInd_frt(9);
							break;
					}
				}
			}
			
			registroC100.setSer(ed.getSerie() != null ? ed.getSerie().toString() : null);
			registroC100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
			registroC100.setDt_doc(ed.getDtemissao());
			registroC100.setVl_desc(ed.getValordesconto() != null ? ed.getValordesconto().getValue().doubleValue() : null);
			registroC100.setVl_merc(ed.getValormercadoriaSPEDPiscofins() != null ? ed.getValormercadoriaSPEDPiscofins().getValue().doubleValue() : null);
			registroC100.setVl_frt(ed.getValorfrete() != null ? ed.getValorfrete().getValue().doubleValue() : null);
			registroC100.setVl_seg(ed.getValorseguro() != null ? ed.getValorseguro().getValue().doubleValue() : null);
			registroC100.setVl_out_da(ed.getValoroutrasdespesas() != null ? ed.getValoroutrasdespesas().getValue().doubleValue() : null);
			registroC100.setVl_bc_icms(0d);//registroC100.setVl_bc_icms(entrega.getValorbcicms() != null && entrega.getValorbcicms().getValue().doubleValue() > 0 ? entrega.getValorbcicms().getValue().doubleValue() : null);
			registroC100.setVl_icms(0d);//registroC100.setVl_icms(entrega.getValoricms() != null && entrega.getValoricms().getValue().doubleValue() > 0 ? entrega.getValoricms().getValue().doubleValue() : null);
			registroC100.setVl_bc_icms_st(ed.getValortotalbcicmsst() != null && ed.getValortotalbcicmsst().getValue().doubleValue() > 0 ? ed.getValortotalbcicmsst().getValue().doubleValue() : 0d);
			registroC100.setVl_icms_st(0d);//registroC100.setVl_icms_st(entrega.getValoricmsst() != null && entrega.getValoricmsst().getValue().doubleValue() > 0 ? entrega.getValoricmsst().getValue().doubleValue() : null);
			registroC100.setVl_ipi(0d);//registroC100.setVl_ipi(entrega.getValoripi() != null && entrega.getValoripi().getValue().doubleValue() > 0 ? entrega.getValoripi().getValue().doubleValue() : null);
			registroC100.setVl_pis(ed.getValortotalpis() != null && ed.getValortotalpis().getValue().doubleValue() > 0 ? ed.getValortotalpis().getValue().doubleValue() : null);
			registroC100.setVl_cofins(ed.getValortotalcofins() != null && ed.getValortotalcofins().getValue().doubleValue() > 0 ? ed.getValortotalcofins().getValue().doubleValue() : null);
			registroC100.setVl_pis_st(ed.getValortotalpisretido() != null && ed.getValortotalpisretido().getValue().doubleValue() > 0 ? ed.getValortotalpisretido().getValue().doubleValue() : null);
			registroC100.setVl_cofins_st(ed.getValortotalcofinsretido() != null && ed.getValortotalcofinsretido().getValue().doubleValue() > 0 ? ed.getValortotalcofinsretido().getValue().doubleValue() : null);
						
			registroC100.setListaRegistroC110(this.createRegistroC110(filtro, ed));			 
			registroC100.setListaRegistroC170(this.createRegistroC170(filtro, ed));
			
			listaDesagrupada.add(registroC100);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroC100.REG);
		}
		
		List<RegistroC100> listaAgrupada = this.joinListaRegistroC100(listaDesagrupada, filtro);
		lista.addAll(listaAgrupada);
		
		return lista;
	}
	
	private List<RegistroC100> joinListaRegistroC100(List<RegistroC100> listaDesagrupada, SpedpiscofinsFiltro filtro) {
		List<RegistroC100> listaAgrupada = new ArrayList<RegistroC100>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroC100 registroC100 : listaDesagrupada) {
				if(listaAgrupada.contains(registroC100)){
					int idx = listaAgrupada.indexOf(registroC100);
					RegistroC100 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroC100(aux, registroC100, filtro);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroC100.REG);
				} else {
					listaAgrupada.add(registroC100);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	private RegistroC100 joinRegistroC100(RegistroC100 ori, RegistroC100 copia, SpedpiscofinsFiltro filtro) {
		
		ori.setVl_doc((ori.getVl_doc() != null ? ori.getVl_doc() : 0d) + (copia.getVl_doc() != null ? copia.getVl_doc() : 0d));
		ori.setVl_desc((ori.getVl_desc() != null ? ori.getVl_desc() : 0d) + (copia.getVl_desc() != null ? copia.getVl_desc() : 0d));
		ori.setVl_abat_nt((ori.getVl_abat_nt() != null ? ori.getVl_abat_nt() : 0d) + (copia.getVl_abat_nt() != null ? copia.getVl_abat_nt() : 0d));
		ori.setVl_merc((ori.getVl_merc() != null ? ori.getVl_merc() : 0d) + (copia.getVl_merc() != null ? copia.getVl_merc() : 0d));
		ori.setVl_frt((ori.getVl_frt() != null ? ori.getVl_frt() : 0d) + (copia.getVl_frt() != null ? copia.getVl_frt() : 0d));
		ori.setVl_seg((ori.getVl_seg() != null ? ori.getVl_seg() : 0d) + (copia.getVl_seg() != null ? copia.getVl_seg() : 0d));
		ori.setVl_out_da((ori.getVl_out_da() != null ? ori.getVl_out_da() : 0d) + (copia.getVl_out_da() != null ? copia.getVl_out_da() : 0d));
		ori.setVl_bc_icms((ori.getVl_bc_icms() != null ? ori.getVl_bc_icms() : 0d) + (copia.getVl_bc_icms() != null ? copia.getVl_bc_icms() : 0d));
		ori.setVl_icms((ori.getVl_icms() != null ? ori.getVl_icms() : 0d) + (copia.getVl_icms() != null ? copia.getVl_icms() : 0d));
		ori.setVl_bc_icms_st((ori.getVl_bc_icms_st() != null ? ori.getVl_bc_icms_st() : 0d) + (copia.getVl_bc_icms_st() != null ? copia.getVl_bc_icms_st() : 0d));
		ori.setVl_icms_st((ori.getVl_icms_st() != null ? ori.getVl_icms_st() : 0d) + (copia.getVl_icms_st() != null ? copia.getVl_icms_st() : 0d));
		ori.setVl_ipi((ori.getVl_ipi() != null ? ori.getVl_ipi() : 0d) + (copia.getVl_ipi() != null ? copia.getVl_ipi() : 0d));
		ori.setVl_pis((ori.getVl_pis() != null ? ori.getVl_pis() : 0d) + (copia.getVl_pis() != null ? copia.getVl_pis() : 0d));
		ori.setVl_cofins((ori.getVl_cofins() != null ? ori.getVl_cofins() : 0d) + (copia.getVl_cofins() != null ? copia.getVl_cofins() : 0d));
		ori.setVl_pis_st((ori.getVl_pis_st() != null ? ori.getVl_pis_st() : 0d) + (copia.getVl_pis_st() != null ? copia.getVl_pis_st() : 0d));
		ori.setVl_cofins_st((ori.getVl_cofins_st() != null ? ori.getVl_cofins_st() : 0d) + (copia.getVl_cofins_st() != null ? copia.getVl_cofins_st() : 0d));
		
		// REGISTRO C110
		List<RegistroC110> listaRegistroC110 = new ArrayList<RegistroC110>();
		if(ori.getListaRegistroC110() != null && ori.getListaRegistroC110().size() > 0){
			listaRegistroC110 = ori.getListaRegistroC110();
			if(copia.getListaRegistroC110() != null && copia.getListaRegistroC110().size() > 0){
				listaRegistroC110.addAll(copia.getListaRegistroC110());
			}
		} else if(copia.getListaRegistroC110() != null && copia.getListaRegistroC110().size() > 0){
			listaRegistroC110 = copia.getListaRegistroC110();
		}
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		for (RegistroC110 registroC110 : listaRegistroC110) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		ori.setListaRegistroC110(listaAgrupada);
		
		
		// REGISTRO 170
		List<RegistroC170> listaRegistroC170 = null;
		if(ori.getListaRegistroC170() != null){
			listaRegistroC170 = ori.getListaRegistroC170();
			if(copia.getListaRegistroC170() != null){
				listaRegistroC170.addAll(copia.getListaRegistroC170());
			}
		} else if(copia.getListaRegistroC170() != null){
			listaRegistroC170 = copia.getListaRegistroC170();
		}
		if(listaRegistroC170 != null){
			int num_item = 1;
			for (RegistroC170 registroC170 : listaRegistroC170) {
				registroC170.setNum_item(num_item);
				num_item++;
			}
		}
		ori.setListaRegistroC170(listaRegistroC170);
		
		return ori;
	}
	
	/**
	 * Faz o registro C175.
	 *
	 * @param listaAnalitico
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC175> createRegistroC175(SpedpiscofinsFiltro filtro, Notafiscalproduto nota) {
		
		List<RegistroC175> lista = new ArrayList<RegistroC175>();
		RegistroC175 registroC175;
		
		for(Notafiscalprodutoitem item: nota.getListaItens()){
			//if(item.getCst() == null || analitico.getCfop() == null) continue;
			//if(analitico.getAliq_icms() == null) analitico.setAliq_icms(0d);
			Tributacaoecf tributacaoEcf = item.getMaterial() != null? item.getMaterial().getTributacaoecf(): null;
			
			registroC175 = new RegistroC175();
			
			MaterialCustoEmpresa materialCustoEmpresa = materialCustoEmpresaService.loadForSpedPisCofins(item.getMaterial(), nota.getEmpresa());
			
			if(Util.objects.isPersistent(materialCustoEmpresa) && Util.objects.isPersistent(materialCustoEmpresa.getContaContabil())){
				if(materialCustoEmpresa.getContaContabil().getVcontacontabil() != null)
					registroC175.setCod_cta(materialCustoEmpresa.getContaContabil().getVcontacontabil().getIdentificador());
			}
			registroC175.setReg(RegistroC175.REG);
			registroC175.setCfop(item.getCfop().getCodigo());
			registroC175.setVl_opr(item.getValorunitario() * item.getQtde());
			if(item.getValordesconto() != null)
				registroC175.setVl_desc(item.getValordesconto().doubleValue());
			String cstPis = item.getCstPisSPED();
			String cstCofins = item.getCstCofinsSPED();
				
			if(item.getValorbcpis() != null)
				registroC175.setVl_bc_pis(item.getValorbcpis().doubleValue());
			registroC175.setQuant_bc_pis(item.getQtdevendidapis());
			registroC175.setAliq_pis(item.getPis());
			if(item.getValorbccofins() != null)
				registroC175.setVl_bc_cofins(item.getValorbccofins().doubleValue());
			registroC175.setAliq_cofins(item.getCofins());
			registroC175.setCst_pis(SPEDUtil.stringCheia(cstPis, "0", 2, false));
			registroC175.setCst_cofins(SPEDUtil.stringCheia(cstCofins, "0", 2, false));
			if(tributacaoEcf != null){
				if(StringUtils.isBlank(cstPis)){
					if(tributacaoEcf.getCstpis() != null){
						cstPis = tributacaoEcf.getCstpis().getCdnfe();
					}
					registroC175.setAliq_pis(tributacaoEcf.getAliqpis());
				}
				if(StringUtils.isBlank(cstCofins)){
					if(tributacaoEcf.getCstcofins() != null){
						cstCofins = tributacaoEcf.getCstcofins().getCdnfe();
					}
					registroC175.setAliq_cofins(tributacaoEcf.getAliqcofins());
				}
			}
			if(addRegistroC175(lista, registroC175)){
				lista.add(registroC175);
				filtro.getApoio().addRegistros(RegistroC175.REG);
				filtro.getApoio().addIdentificador(registroC175.getCod_cta());
			}
		}
		
		return lista;
	}
	
	private boolean addRegistroC175(List<RegistroC175> lista, RegistroC175 registroC175){
		boolean adicionar = true;
		
		if(lista != null && !lista.isEmpty()){
			for(RegistroC175 bean : lista){
				boolean cstCofins = false;
				boolean cstPis = false;
				boolean cfop = false;
				boolean aliquotaCofins = false;
				boolean aliquotaPis = false;
				
				if(bean.getCst_cofins() == null && registroC175.getCst_cofins() == null){
					cstCofins = true;
				}else if(bean.getCst_cofins() != null && registroC175.getCst_cofins() != null && 
						bean.getCst_cofins().equals(registroC175.getCst_cofins())){
					cstCofins = true;
				}
				
				if(bean.getCst_pis() == null && registroC175.getCst_pis() == null){
					cstPis = true;
				}else if(bean.getCst_pis() != null && registroC175.getCst_pis() != null && 
						bean.getCst_pis().equals(registroC175.getCst_pis())){
					cstPis = true;
				}
				
				if(bean.getCfop() == null && registroC175.getCfop() == null){
					cfop = true;
				}else if(bean.getCfop() != null && registroC175.getCfop() != null && 
						bean.getCfop().equals(registroC175.getCfop())){
					cfop = true;
				}
				
				if(bean.getAliq_cofins() == null && registroC175.getAliq_cofins() == null){
					aliquotaCofins = true;
				}else if(bean.getAliq_cofins() != null && registroC175.getAliq_cofins() != null && 
						bean.getAliq_cofins().equals(registroC175.getAliq_cofins())){
					aliquotaCofins = true;
				}
				if(bean.getAliq_pis() == null && registroC175.getAliq_pis() == null){
					aliquotaPis = true;
				}else if(bean.getAliq_pis() != null && registroC175.getAliq_pis() != null && 
						bean.getAliq_pis().equals(registroC175.getAliq_pis())){
					aliquotaPis = true;
				}
				
				if(cstCofins && cstPis && cfop && aliquotaCofins && aliquotaPis){
					if(registroC175.getVl_opr() != null)
						bean.setVl_opr(bean.getVl_opr()+registroC175.getVl_opr());
					
					adicionar = false;
					break;
				}
			}
		}
		return adicionar;
	}

	private List<RegistroC170> createRegistroC170(SpedpiscofinsFiltro filtro, Notafiscalproduto notafiscalproduto) {
		List<RegistroC170> lista = new ArrayList<RegistroC170>();
		RegistroC170 registroC170;
		
		int countItem = 1;

		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = notafiscalproduto.getListaItens();
		if(listaNotafiscalprodutoitem != null && listaNotafiscalprodutoitem.size() > 0){
			
			BigDecimal valor_tot = BigDecimal.ZERO;
			for (Notafiscalprodutoitem item : listaNotafiscalprodutoitem) {
				if(Finalidadenfe.COMPLEMENTAR.equals(notafiscalproduto.getFinalidadenfe()) && item.getQtde() != null && item.getQtde() == 0d){
					continue;
				}
				
				registroC170 = new RegistroC170();
				
				valor_tot = valor_tot.add(item.getValorbruto().getValue());
				
				registroC170.setReg(RegistroC170.REG);
				registroC170.setNum_item(countItem);
				registroC170.setCod_item(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getIdentificadorForNotaSped(), getCNPJEmpresa(filtro.getEmpresa(), notafiscalproduto.getEmpresa(), filtro.isCnpjEmpresaFiltro())) : null);
				registroC170.setDescr_compl(item.getInfoadicional() != null && !item.getInfoadicional().equals("") ? item.getInfoadicional() : null);
				registroC170.setQtd(item.getQtde());
				registroC170.setUnid(item.getMaterial() != null && item.getMaterial().getUnidademedida() != null ? filtro.getApoio().addUnidade(item.getMaterial().getUnidademedida().getCdunidademedida(),notafiscalproduto.getEmpresa().getCnpj().getValue()) : null);
				registroC170.setVl_item(item.getValorbruto().getValue().doubleValue());
				registroC170.setVl_desc(item.getValordesconto() != null && item.getValordesconto().getValue().doubleValue() > 0 ? item.getValordesconto().getValue().doubleValue() : null);
				registroC170.setInd_mov(0);
				registroC170.setCst_icms(item.getCstIcmsSPED() != null ? SPEDUtil.stringCheia(item.getCstIcmsSPED(), "0", 3, false) : null);
				registroC170.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
				registroC170.setCod_nat(notafiscalproduto.getNaturezaoperacao() != null ? filtro.getApoio().addNatureza(notafiscalproduto.getNaturezaoperacao().getNome(), notafiscalproduto.getEmpresa().getCnpj().getValue()) : null);
				registroC170.setVl_bc_icms(item.getValorbcicms() != null && item.getValorbcicms().getValue().doubleValue() > 0 ? item.getValorbcicms().getValue().doubleValue() : 0);
				registroC170.setAliq_icms(item.getIcms() != null && item.getIcms() > 0 ? item.getIcms() : 0);
				registroC170.setVl_icms(item.getValoricms() != null && item.getValoricms().getValue().doubleValue() > 0 ? item.getValoricms().getValue().doubleValue() : 0);
				registroC170.setVl_bc_icms_st(item.getValorbcicmsst() != null && item.getValorbcicmsst().getValue().doubleValue() > 0 ? item.getValorbcicmsst().getValue().doubleValue() : 0);
				registroC170.setAliq_st(item.getIcmsst() != null && item.getIcmsst() > 0 ? item.getIcmsst() : 0);
				registroC170.setVl_icms_st(item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0 ? item.getValoricmsst().getValue().doubleValue() : 0);
//				registroC170.setInd_apur();
				
				registroC170.setCst_ipi(item.getTipocobrancaipi() != null ? item.getTipocobrancaipi().getCdnfe() : null);
//				registroC170.setCod_enq();
				registroC170.setVl_bc_ipi(item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0);
				registroC170.setAliq_ipi(item.getIpi() != null ? item.getIpi() : 0);
				registroC170.setVl_ipi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0);
				
				if(item.getTipocobrancapis() != null){
					boolean operacaooutros = !item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_01) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_02) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_03) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_04) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_06) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_07) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_08) &&
												!item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_09);
					
					boolean calculoPorQtde = false;
					if(operacaooutros){
						calculoPorQtde = item.getTipocalculopis() != null && item.getTipocalculopis().equals(Tipocalculo.EM_VALOR);
					}
					if(item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_03)){
						calculoPorQtde = true;
					}
					
					registroC170.setCst_pis(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
					if(!calculoPorQtde){
						registroC170.setVl_bc_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0);
						registroC170.setAliq_pis(item.getPis() != null ? item.getPis() : 0);
					} else {
						registroC170.setQuant_bc_pis(item.getQtdevendidapis() != null && item.getQtdevendidapis() > 0 ? item.getQtdevendidapis() : null);
						registroC170.setAliq_pis_quant(item.getAliquotareaispis() != null && item.getAliquotareaispis().getValue().doubleValue() > 0 ? item.getAliquotareaispis().getValue().doubleValue() : null);
					}
					registroC170.setVl_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0);
				}
				
				if(item.getTipocobrancacofins() != null){
					boolean operacaooutros = !item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_01) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_02) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_03) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_04) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_06) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_07) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_08) &&
												!item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_09);
					
					boolean calculoPorQtde = false;
					if(operacaooutros){
						calculoPorQtde = item.getTipocalculocofins() != null && item.getTipocalculocofins().equals(Tipocalculo.EM_VALOR);
					}
					if(item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_03)){
						calculoPorQtde = true;
					}
					
					registroC170.setCst_cofins(item.getTipocobrancacofins() != null ? item.getTipocobrancacofins().getCdnfe() : null);
					if(!calculoPorQtde){
						registroC170.setVl_bc_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0);
						registroC170.setAliq_cofins(item.getCofins() != null ? item.getCofins() : 0);
					} else {
						registroC170.setQuant_bc_cofins(item.getQtdevendidacofins() != null && item.getQtdevendidacofins() > 0 ? item.getQtdevendidacofins() : null);
						registroC170.setAliq_cofins_quant(item.getAliquotareaiscofins() != null && item.getAliquotareaiscofins().getValue().doubleValue() > 0 ? item.getAliquotareaiscofins().getValue().doubleValue() : null);
					}
					registroC170.setVl_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0);
				}
				ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), notafiscalproduto.getNaturezaoperacao(), item.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), item.getMaterial(), notafiscalproduto.getEmpresa()) : null);
				registroC170.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
				
				countItem++;
				
				lista.add(registroC170);
				
				filtro.getApoio().addRegistros(RegistroC170.REG);
				filtro.getApoio().addIdentificador(registroC170.getCod_cta());
				
				this.createRegistroM410(filtro, null, item, notafiscalproduto, null, null);
				this.createRegistroM810(filtro, null, item, notafiscalproduto, null, null);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC170> createRegistroC170(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroC170> lista = new ArrayList<RegistroC170>();
		RegistroC170 registroC170;
		
		int countItem = 1;
		
		Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
		if(listaEntregamaterial != null && listaEntregamaterial.size() > 0){
			
			BigDecimal valor_it, valor_tot = BigDecimal.ZERO;
			for (Entregamaterial item : listaEntregamaterial) {
				if(item.getQtde() <= 0d) continue;
				
				registroC170 = new RegistroC170();
				
				valor_it = new BigDecimal(item.getValortotalmaterial());
				
				valor_tot = valor_tot.add(valor_it);
				
				registroC170.setReg(RegistroC170.REG);
				registroC170.setNum_item(countItem);
				registroC170.setCod_item(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), entregadocumento.getEmpresa(), filtro.isCnpjEmpresaFiltro())) : null);
//				registroC170.setDescr_compl();
				registroC170.setQtd(item.getQtde());
				registroC170.setUnid(item.getUnidademedidacomercial() != null ? filtro.getApoio().addUnidade(item.getUnidademedidacomercial().getCdunidademedida(), entregadocumento.getEmpresa().getCnpj().getValue()) : null);
				registroC170.setVl_item(valor_it.doubleValue());
				registroC170.setVl_desc(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d);
				registroC170.setInd_mov(0);
				registroC170.setCst_icms(item.getCstIcmsSPED() != null ? SPEDUtil.stringCheia(item.getCstIcmsSPED(), "0", 3, false) : null);
				registroC170.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
				registroC170.setCod_nat(entregadocumento.getNatop() != null ? filtro.getApoio().addNatureza(entregadocumento.getNatop(), entregadocumento.getEmpresa().getCnpj().getValue()) : null);
				registroC170.setVl_bc_icms(0d);//registroC170.setVl_bc_icms(item.getValorbcicms() != null && item.getValorbcicms().getValue().doubleValue() > 0 ? item.getValorbcicms().getValue().doubleValue() : 0);
				registroC170.setAliq_icms(0d);//registroC170.setAliq_icms(item.getIcms() != null && item.getIcms() > 0 ? item.getIcms() : 0);
				registroC170.setVl_icms(0d);//registroC170.setVl_icms(item.getValoricms() != null && item.getValoricms().getValue().doubleValue() > 0 ? item.getValoricms().getValue().doubleValue() : 0);
				registroC170.setVl_bc_icms_st(0d);//registroC170.setVl_bc_icms_st(item.getValorbcicmsst() != null && item.getValorbcicmsst().getValue().doubleValue() > 0 ? item.getValorbcicmsst().getValue().doubleValue() : 0);
				registroC170.setAliq_st(0d);//registroC170.setAliq_st(item.getIcmsst() != null && item.getIcmsst() > 0 ? item.getIcmsst() : 0);
				registroC170.setVl_icms_st(0d);//registroC170.setVl_icms_st(item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0 ? item.getValoricmsst().getValue().doubleValue() : 0);
//				registroC170.setInd_apur();
				registroC170.setCst_ipi(item.getCstipi() != null ? item.getCstipi().getCdnfe() : null);
//				registroC170.setCod_enq();
				registroC170.setVl_bc_ipi(0d);//registroC170.setVl_bc_ipi(item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0);
				registroC170.setAliq_ipi(0d);//registroC170.setAliq_ipi(item.getIpi() != null ? item.getIpi() : 0);
				registroC170.setVl_ipi(0d);//registroC170.setVl_ipi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0);
				registroC170.setCst_pis(item.getCstpis() != null ? item.getCstpis().getCdnfe() : null);
				registroC170.setVl_bc_pis(item.getQtdebcpis() != null && item.getQtdebcpis() > 0 ? null : item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0);
				registroC170.setAliq_pis(item.getPis() != null ? item.getPis() : 0);
								
				registroC170.setQuant_bc_pis(item.getQtdebcpis() != null && item.getQtdebcpis() > 0 ? item.getQtdebcpis() : null);
				registroC170.setAliq_pis_quant(item.getValorunidbcpis() != null && item.getValorunidbcpis() > 0 ? item.getValorunidbcpis() : null);
				
				registroC170.setVl_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0);
				registroC170.setCst_cofins(item.getCstcofins() != null ? item.getCstcofins().getCdnfe() : null);
				registroC170.setVl_bc_cofins(item.getQtdebccofins() != null && item.getQtdebccofins() > 0 ? null : item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0);
				registroC170.setAliq_cofins(item.getCofins() != null ? item.getCofins() : 0);
				
				registroC170.setQuant_bc_cofins(item.getQtdebccofins() != null && item.getQtdebccofins() > 0 ? item.getQtdebccofins() : null);
				registroC170.setAliq_cofins_quant(item.getValorunidbccofins() != null && item.getValorunidbccofins() > 0 ? item.getValorunidbccofins() : null);
				
				registroC170.setVl_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0);
				ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento.getNaturezaoperacao(), item.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), item.getMaterial(), entregadocumento.getEmpresa()) : null);
				registroC170.setCod_cta(contacontabil !=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
				
				countItem++;
				
				lista.add(registroC170);
				
				filtro.getApoio().addRegistros(RegistroC170.REG);
				filtro.getApoio().addIdentificador(registroC170.getCod_cta());
				
				this.createRegistroM410(filtro, null, null, null, item, entregadocumento);
				this.createRegistroM810(filtro, null, null, null, item, entregadocumento);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC110> createRegistroC110(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroC110> listaDesagrupada = new ArrayList<RegistroC110>();
		
		String info = entregadocumento.getInfoadicionalfisco();
		if(info != null && !info.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(info, entregadocumento.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		String infoContrib = entregadocumento.getInfoadicionalcontrib();
		if(infoContrib != null && !infoContrib.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(infoContrib, entregadocumento.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		
		for (RegistroC110 registroC110 : listaDesagrupada) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		
		return listaDesagrupada;
	}
	
	private List<RegistroC120> createRegistroC120(SpedpiscofinsFiltro filtro, Notafiscalproduto notafiscalproduto) {
		List<RegistroC120> listaAgrupadaC120 = new ArrayList<RegistroC120>(); 
		
		Tipooperacaonota tipooperacaonota = notafiscalproduto.getTipooperacaonota();
		Localdestinonfe localdestinonfe = notafiscalproduto.getLocaldestinonfe();
		if(tipooperacaonota != null && 
				localdestinonfe != null && 
				tipooperacaonota.equals(Tipooperacaonota.ENTRADA) &&
				localdestinonfe.equals(Localdestinonfe.EXTERIOR)){
			
			List<RegistroC120> listaDesagrupadaC120 = new ArrayList<RegistroC120>(); 
			List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = notafiscalproduto.getListaItens();
			for (Notafiscalprodutoitem notafiscalprodutoitem : listaNotafiscalprodutoitem) {
				Notafiscalprodutoitemdi notafiscalprodutoitemdi = null;
				Set<Notafiscalprodutoitemdi> listaNotafiscalprodutoitemdi = notafiscalprodutoitem.getListaNotafiscalprodutoitemdi();
				if(listaNotafiscalprodutoitemdi != null && listaNotafiscalprodutoitemdi.size() > 0){
					notafiscalprodutoitemdi = listaNotafiscalprodutoitemdi.iterator().next();
				}
				
				if(notafiscalprodutoitemdi != null){
					Declaracaoimportacao declaracaoimportacao = notafiscalprodutoitemdi.getDeclaracaoimportacao();
					if(declaracaoimportacao != null){
						RegistroC120 registroC120 = new RegistroC120();
						registroC120.setReg(RegistroC120.REG);
						registroC120.setCod_doc_imp(0);
						registroC120.setNum_doc_imp(br.com.linkcom.neo.util.StringUtils.soNumero(declaracaoimportacao.getNumerodidsida()));
						
						Tipocobrancapis tipocobrancapis = notafiscalprodutoitem.getTipocobrancapis();
						if(tipocobrancapis != null &&
								(
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_50) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_51) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_52) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_53) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_54) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_55) ||
									tipocobrancapis.equals(Tipocobrancapis.OPERACAO_56)
								)){
							registroC120.setVl_pis_imp(notafiscalprodutoitem.getValorpis().getValue().doubleValue());
						} 
						Tipocobrancacofins tipocobrancacofins = notafiscalprodutoitem.getTipocobrancacofins();
						if(tipocobrancacofins != null &&
								(
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_50) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_51) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_52) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_53) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_54) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_55) ||
										tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_56)
										)){
							registroC120.setVl_cofins_imp(notafiscalprodutoitem.getValorcofins().getValue().doubleValue());
						} 
						
						List<Declaracaoimportacaoadicao> listaDeclaracaoimportacaoadicao = declaracaoimportacao.getListaDeclaracaoimportacaoadicao();
						if(listaDeclaracaoimportacaoadicao != null && listaDeclaracaoimportacaoadicao.size() > 0){
							for (Declaracaoimportacaoadicao declaracaoimportacaoadicao : listaDeclaracaoimportacaoadicao) {
								if(declaracaoimportacaoadicao.getNumerodrawback() != null && !declaracaoimportacaoadicao.getNumerodrawback().trim().equals("")){
									registroC120.setNum_acdr_aw(declaracaoimportacaoadicao.getNumerodrawback());
									break;
								}
							}
						}
						
						listaDesagrupadaC120.add(registroC120);
					}
				}
			}
			
			
			for (RegistroC120 registroC120 : listaDesagrupadaC120) {
				boolean achou = false;
				for (RegistroC120 registroC120_2 : listaAgrupadaC120) {
					boolean numDocIgual = registroC120_2.getNum_doc_imp().equals(registroC120.getNum_doc_imp());
					boolean numDrawIgual = (registroC120_2.getNum_acdr_aw() == null && registroC120.getNum_acdr_aw() == null) || 
											(registroC120_2.getNum_acdr_aw() != null && registroC120_2.getNum_acdr_aw().equals(registroC120.getNum_acdr_aw()));
					if(numDocIgual && numDrawIgual){
						achou = true;
						registroC120_2.setVl_pis_imp(registroC120_2.getVl_pis_imp() + registroC120.getVl_pis_imp());
						registroC120_2.setVl_cofins_imp(registroC120_2.getVl_cofins_imp() + registroC120.getVl_cofins_imp());
					}
				}
				
				if(!achou){
					filtro.getApoio().addRegistros(RegistroC120.REG);
					listaAgrupadaC120.add(registroC120);
				}
			}
		}
		
		return listaAgrupadaC120;
	}
	
	private List<RegistroC110> createRegistroC110(SpedpiscofinsFiltro filtro, Notafiscalproduto notafiscalproduto) {
		List<RegistroC110> listaDesagrupada = new ArrayList<RegistroC110>();
		
		String info = notafiscalproduto.getInfoadicionalfisco();
		if(info != null && !info.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(info, notafiscalproduto.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		String infoContrib = notafiscalproduto.getInfoadicionalcontrib();
		if(infoContrib != null && !infoContrib.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(infoContrib, notafiscalproduto.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		
		for (RegistroC110 registroC110 : listaDesagrupada) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		
		return listaDesagrupada;
	}
	
	private List<RegistroF100> createRegistroF100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<RegistroF100> lista = new ArrayList<RegistroF100>();
		RegistroF100 registroF100;
		
		List<Entregadocumento> listaEntregadocumento = this.getListaEntregaF100(filtro, empresa);
		
		for (Entregadocumento ed : listaEntregadocumento) {
			for(Entregamaterial item : ed.getListaEntregamaterial()){
				registroF100 = new RegistroF100();
				
				registroF100.setReg(RegistroF100.REG);
				String indicadortipooperacao = null;
				
				if(item.getCstpis() != null || item.getCstcofins() != null){
					if((item.getCstpis().getValue() >= 10 && item.getCstpis().getValue() <= 23) || 
							(item.getCstcofins().getValue() >= 10 && item.getCstcofins().getValue() <= 23)){
						indicadortipooperacao = "0";
					}else if((item.getCstpis().getValue() == 0 || item.getCstpis().getValue() == 1 ||
							item.getCstpis().getValue() == 2 || item.getCstpis().getValue() == 4) ||
							(item.getCstcofins().getValue() == 0 || item.getCstcofins().getValue() == 1 ||
									item.getCstcofins().getValue() == 2 || item.getCstcofins().getValue() == 4)){
						indicadortipooperacao = "1";
					}else if((item.getCstpis().getValue() == 3 || item.getCstpis().getValue() == 5 ||
							item.getCstpis().getValue() == 7 || item.getCstpis().getValue() == 8 ||
							item.getCstpis().getValue() == 9 || item.getCstpis().getValue() == 32) ||
							(item.getCstcofins().getValue() == 0 || item.getCstcofins().getValue() == 1 ||
									item.getCstcofins().getValue() == 2 || item.getCstcofins().getValue() == 4 ||
									item.getCstcofins().getValue() == 9 || item.getCstcofins().getValue() == 32)){
						indicadortipooperacao = "2";
					} else continue;
				}
				registroF100.setInd_oper(indicadortipooperacao);
				registroF100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
				registroF100.setCod_item(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), ed.getEmpresa(), filtro.isCnpjEmpresaFiltro())) : null);
				registroF100.setDt_oper(ed.getDtentrada());
				registroF100.setVl_oper(item.getValortotalmaterial());
				registroF100.setCst_pis(item.getCstpis() != null ? item.getCstpis().getCdnfe() : null);		
				registroF100.setVl_bc_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : null);
				registroF100.setAliq_pis(item.getPis() != null ? item.getPis() : null);
				registroF100.setVl_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : null);
				registroF100.setCst_cofins(item.getCstcofins() != null ? item.getCstcofins().getCdnfe() : null);		
				registroF100.setVl_bc_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : null);
				registroF100.setAliq_cofins(item.getCofins() != null ? item.getCofins() : null);
				registroF100.setVl_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : null);
				registroF100.setNat_bc_cred(item.getNaturezabcc() != null ? item.getNaturezabcc().getCdsped()  : null);
				registroF100.setInd_orig_cred("0");
				ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), ed.getNaturezaoperacao(), item.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), item.getMaterial(), empresa) : null);
				registroF100.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
//				registroF100.setCod_ccus();
//				registroF100.setDesc_doc_oper();
				
				filtro.getApoio().addRegistros(RegistroF100.REG);
				filtro.getApoio().addIdentificador(registroF100.getCod_cta());
				
				lista.add(registroF100);
			}
		}		
		return lista;
	}
	
	private void qtdeLinhasRegistroA990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistroA001()!= null){
			qtdeLinhas++; // Registro A001
						
			if(spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010() != null && 
					spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010().size(); // Registro A010
			
				for(RegistroA010 registroA010 : spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010()){					
					if(registroA010.getListaRegistroA100() != null && registroA010.getListaRegistroA100().size() > 0){
						qtdeLinhas+= registroA010.getListaRegistroA100().size(); // Registro A100
						
						for(RegistroA100 registroA100 : registroA010.getListaRegistroA100()){
							if(registroA100.getListaRegistroA110() != null)
								qtdeLinhas+= registroA100.getListaRegistroA110().size(); // Registro A110
							if(registroA100.getListaRegistroA170() != null)
								qtdeLinhas+= registroA100.getListaRegistroA170().size(); // Registro A170
						}
					}
				}
			}
		}
		
		if(spedPiscofins.getRegistro0000().getRegistroA990() != null){
			qtdeLinhas++; // Registro A990
		
			spedPiscofins.getRegistro0000().getRegistroA990().setQtd_lin_a(qtdeLinhas);
		}
	}
	
	private RegistroA990 createRegistroA990(SpedpiscofinsFiltro filtro) {
		RegistroA990 registroA990 = new RegistroA990();
		
		registroA990.setReg(RegistroA990.REG);
		filtro.getApoio().addRegistros(RegistroA990.REG);
		
		return registroA990;
	}
	
	private RegistroA001 createRegistroA001(SpedpiscofinsFiltro filtro) {
		
		RegistroA001 registroA001 = new RegistroA001();
		
		registroA001.setReg(RegistroA001.REG);		
		registroA001.setListaRegistroA010(this.createRegistroA010(filtro));
		
		filtro.getApoio().addRegistros(RegistroA001.REG);
		
		return registroA001;		
	}	
	
	private List<RegistroA010> createRegistroA010(SpedpiscofinsFiltro filtro) {
		
		List<RegistroA010> lista = new ArrayList<RegistroA010>();
		
		for(Empresa empresa : filtro.getListaEmpresaFiliais()){
			RegistroA010 registroA010 = new RegistroA010();
					
			registroA010.setReg(RegistroA010.REG);		
			if(empresa.getCnpj() == null || "".equals(empresa.getCnpj().getValue()))
				filtro.addError("O campo CPF da identifica��o do estabelecimento � obrigat�rio.");
			registroA010.setCnpj(empresa.getCnpj().getValue());
			registroA010.setListaRegistroA100(this.createRegistroA100(filtro, empresa));
			
			filtro.getApoio().addRegistros(RegistroA010.REG);
			lista.add(registroA010);	
		}
		
		return lista;
	}
	
	private List<RegistroA100> createRegistroA100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		
		List<RegistroA100> lista = new ArrayList<RegistroA100>();
		
		List<Entregadocumento> listaNFsEntrada = this.getListaEntregaA100(filtro, empresa);
		List<NotaFiscalServico> listaNfs = this.getListaNotaA100(filtro, empresa);
		
		if(listaNfs != null && !listaNfs.isEmpty()){
			String numeroNf, codigoVerificacao, serie;
			Arquivonfnota arquivonfnota;
			
			for(NotaFiscalServico nfs : listaNfs){
				RegistroA100 registroA100 =  new RegistroA100();
				
				numeroNf = nfs.getNumero();
				codigoVerificacao = null;
				serie = null;
				
				arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
				if(arquivonfnota != null){
					numeroNf = arquivonfnota.getNumeronfse();
					codigoVerificacao = arquivonfnota.getCodigoverificacao();
					if(arquivonfnota.getNota() != null &&
							arquivonfnota.getNota().getSerienfe() != null){
						serie = arquivonfnota.getNota().getSerienfe().toString(); 
					}
				}
				
				registroA100.setReg(RegistroA100.REG);
				registroA100.setInd_oper(RegistroA100.SERVICO_PRESTADO);
				registroA100.setInd_emit(RegistroA100.EMISSAO_PROPRIA);
				
				Integer cdpessoa = nfs.getCliente().getCdpessoa();
				
				String codPart = "";
				if(nfs.getEnderecoCliente() != null && 
						nfs.getEnderecoCliente().getInscricaoestadual() != null &&
						!"".equals(nfs.getEnderecoCliente().getInscricaoestadual())){
					codPart = nfs.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa, nfs.getEmpresa().getCnpj().getValue(), nfs.getEnderecoCliente().getCdendereco(), nfs.getCliente().getCpfOuCnpjValue()) : null;
					codPart = codPart + "-" + nfs.getEnderecoCliente().getCdendereco();
				} else {
					codPart = nfs.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa, nfs.getEmpresa().getCnpj().getValue(), null, nfs.getCliente().getCpfOuCnpjValue()) : null;
				}
				registroA100.setCod_part(codPart);
				
				registroA100.setCod_sit(nfs.getSituacaosped());
				registroA100.setSer(serie);
//				registroA100.setSub();
				registroA100.setNum_doc(numeroNf);
				registroA100.setChv_nfse(codigoVerificacao);
				registroA100.setDt_doc(nfs.getDtEmissao());
				registroA100.setDt_exe_serv(nfs.getDtEmissao());
				registroA100.setVl_doc(nfs.getValorTotalServicos() != null ? nfs.getValorTotalServicos().getValue().doubleValue() : null);
				if(nfs.getIndicadortipopagamento() != null)
					registroA100.setInd_pgto(nfs.getIndicadortipopagamento().getValue());
				else {
					if(SinedDateUtils.equalsIgnoreHour(nfs.getDtEmissao(), nfs.getDtVencimento()))
						registroA100.setInd_pgto(Indicadortipopagamento.A_VISTA.getValue());
					else
						registroA100.setInd_pgto(Indicadortipopagamento.A_PRAZO.getValue());
				}
				registroA100.setVl_desc(nfs.getDeducao() != null ? nfs.getDeducao().getValue().doubleValue():null);
				registroA100.setVl_bc_pis(nfs.getBasecalculopis() != null ? nfs.getBasecalculopis().getValue().doubleValue():null);
				
				registroA100.setVl_pis(nfs.getValorPis() != null ? SinedUtil.round(nfs.getValorPis().getValue(),2): BigDecimal.ZERO);
				registroA100.setVl_cofins(nfs.getValorCofins() != null ? SinedUtil.round(nfs.getValorCofins().getValue(),2):BigDecimal.ZERO);
				
				registroA100.setVl_bc_cofins(nfs.getBasecalculocofins() != null ? nfs.getBasecalculocofins().getValue().doubleValue():null);
				
				if(nfs.getIncidepis() != null && nfs.getIncidepis() && nfs.getPisretido() != null)
					registroA100.setVl_pis_ret(nfs.getValorPisRetido() != null ?  SinedUtil.round(nfs.getValorPisRetido().getValue(),2).doubleValue():0d);
				if(nfs.getIncidecofins() != null && nfs.getIncidecofins() && nfs.getCofinsretido() != null)
					registroA100.setVl_cofins_ret(nfs.getValorCofinsRetido() != null ?  SinedUtil.round(nfs.getValorCofinsRetido().getValue(),2).doubleValue():0d);
				
				registroA100.setVl_iss(nfs.getValorIss() != null ?  SinedUtil.round(nfs.getValorIss().getValue(),2).doubleValue():null);
				
				if(nfs.getDadosAdicionais() != null && "".equals(nfs.getDadosAdicionais().trim()))
					registroA100.setListaRegistroA110(this.createRegistroA110(filtro, nfs));
				registroA100.setListaRegistroA170(this.createRegistroA170(filtro, nfs));
				lista.add(registroA100);
				
				this.createRegistroM410(filtro, nfs, null, null, null, null);
				this.createRegistroM810(filtro, nfs, null, null, null, null);
				
				filtro.getApoio().addRegistros(RegistroA100.REG);
			}
		}
		
		if(listaNFsEntrada != null && !listaNFsEntrada.isEmpty()){
			
			for(Entregadocumento ed : listaNFsEntrada){
				RegistroA100 registroA100 =  new RegistroA100();
				
				registroA100.setReg(RegistroA100.REG);
				registroA100.setInd_oper(RegistroA100.SERVICO_CONTRATADO);
				registroA100.setInd_emit(RegistroA100.TERCEIROS);
				registroA100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa(), ed.getEmpresa().getCnpj().getValue(), null, ed.getFornecedor().getCpfOuCnpjValue()) : null);
				registroA100.setCod_sit(ed.getSituacaosped());
				registroA100.setSer(ed.getSerie() != null ? ed.getSerie().toString() : null);
//				registroA100.setSub();
				registroA100.setNum_doc(SPEDUtil.soNumero(ed.getNumero()));
				registroA100.setChv_nfse(ed.getChaveacesso());
				registroA100.setDt_doc(ed.getDtemissao());
//				registroA100.setDt_exe_serv();
				registroA100.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
				if(ed.getIndpag() != null){
					if(ed.getIndpag().equals(Formapagamentonfe.A_VISTA)){
						registroA100.setInd_pgto(0);
					} else if(ed.getIndpag().equals(Formapagamentonfe.A_PRAZO)){
						registroA100.setInd_pgto(1);
					} else if(ed.getIndpag().equals(Formapagamentonfe.OUTROS)){
						registroA100.setInd_pgto(9);
					}
				}
				registroA100.setVl_desc(ed.getValordesconto() != null ? ed.getValordesconto().getValue().doubleValue() : null);
				registroA100.setVl_bc_pis(ed.getValortotalbcpis() != null ? ed.getValortotalbcpis().getValue().doubleValue() : null);
				registroA100.setVl_pis(ed.getValortotalpis() != null && ed.getValortotalpis().getValue().doubleValue() > 0 ? ed.getValortotalpis().getValue() : BigDecimal.ZERO);				
				registroA100.setVl_bc_cofins(ed.getValortotalbccofins() != null ? ed.getValortotalbccofins().getValue().doubleValue() : null);
				registroA100.setVl_cofins(ed.getValortotalcofins() != null && ed.getValortotalcofins().getValue().doubleValue() > 0 ? ed.getValortotalcofins().getValue() : BigDecimal.ZERO);
				
				registroA100.setVl_pis_ret(ed.getValortotalpisretido() != null ? ed.getValortotalpisretido().getValue().doubleValue():0d);
				registroA100.setVl_cofins_ret(ed.getValortotalcofinsretido() != null ? ed.getValortotalcofinsretido().getValue().doubleValue():0d);
				
				registroA100.setVl_iss(ed.getValortotaliss() != null ? ed.getValortotaliss().getValue().doubleValue():null);
				
				registroA100.setListaRegistroA110(this.createRegistroA110(filtro, ed));
				registroA100.setListaRegistroA170(this.createRegistroA170(filtro, ed));
				lista.add(registroA100);
				
				filtro.getApoio().addRegistros(RegistroA100.REG);
			}
		}
		
		return lista;
	}
	
	private List<NotaFiscalServico> getListaNotaA100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<NotaFiscalServico> listaNfs = new ArrayList<NotaFiscalServico>();
		
		List<Configuracaonfe> listaConfiguracaonfe = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, empresa);
		boolean haveNfse = listaConfiguracaonfe != null && listaConfiguracaonfe.size() > 0;
		
		List<NotaFiscalServico> listaNfsNormal = notaFiscalServicoService.findForSpedPiscofinsRegA0100(filtro, empresa, haveNfse);
		for (NotaFiscalServico nf1 : listaNfsNormal) {
			nf1.setSituacaosped("00");
			listaNfs.add(nf1);
		}
		
		return listaNfs;
	}
	
	private List<RegistroA170> createRegistroA170(SpedpiscofinsFiltro filtro, NotaFiscalServico nfs) {
		List<RegistroA170> lista = new ArrayList<RegistroA170>();
		List<NotaFiscalServicoItem> listaItens = notaFiscalServicoService.getListaItensAjustadaSped(nfs);
		int i = 1;
		
		Money totalBcPisNota = nfs.getBasecalculopis() != null ? nfs.getBasecalculopis() : new Money(0),
				totalBcCofinsNota = nfs.getBasecalculocofins() != null ? nfs.getBasecalculocofins() : new Money(0);
				
		Double totalBcPisNotaDouble = Double.parseDouble(totalBcPisNota.toString().replace(".", "").replace(",", ".")), 
				totalBcCofinsNotaDouble = Double.parseDouble(totalBcCofinsNota.toString().replace(".", "").replace(",", ".")), 
				totalBcPisItensDouble = 0d, 
				totalBcCofinsItensDouble = 0d;
		
		for (NotaFiscalServicoItem iten2 : listaItens) {
			totalBcPisItensDouble += Double.parseDouble(iten2.getBasecalculoPis().toString().replace(".", "").replace(",", "."));
			totalBcCofinsItensDouble += Double.parseDouble(iten2.getBasecalculoCofins().toString().replace(".", "").replace(",", "."));
		}
		
		Boolean erroTotalBcPis = LkNfeUtil.arredodamentoValorDuasCasas(totalBcPisItensDouble) > LkNfeUtil.arredodamentoValorDuasCasas(totalBcPisNotaDouble), 
				erroTotalBcCofins = LkNfeUtil.arredodamentoValorDuasCasas(totalBcCofinsItensDouble) > LkNfeUtil.arredodamentoValorDuasCasas(totalBcCofinsNotaDouble);
		
		Integer indexPis = 0, indexCofins = 0, qtdeItems = listaItens.size();
		
		while (erroTotalBcPis || erroTotalBcCofins) {
			if (erroTotalBcPis) {
				if (indexPis == qtdeItems) {
					indexPis = 0;
				}
				
				listaItens.get(indexPis).setBasecalculoPis(listaItens.get(indexPis).getBasecalculoPis().subtract(new Money(0.01)));
				totalBcPisItensDouble -= 0.01;
				
				indexPis++;
			}
			
			erroTotalBcPis = LkNfeUtil.arredodamentoValorDuasCasas(totalBcPisItensDouble) > LkNfeUtil.arredodamentoValorDuasCasas(totalBcPisNotaDouble);
			
			if (erroTotalBcCofins) {
				if (indexCofins == qtdeItems) {
					indexCofins = 0;
				}
				
				listaItens.get(indexCofins).setBasecalculoCofins(listaItens.get(indexCofins).getBasecalculoCofins().subtract(new Money(0.01)));
				totalBcCofinsItensDouble -= 0.01;
				
				indexCofins++;
			}
			
			erroTotalBcCofins = LkNfeUtil.arredodamentoValorDuasCasas(totalBcCofinsItensDouble) > LkNfeUtil.arredodamentoValorDuasCasas(totalBcCofinsNotaDouble);
		}
		
		for(NotaFiscalServicoItem iten2 : listaItens){
			RegistroA170 registroA170 = new RegistroA170();
			
			registroA170.setReg(RegistroA170.REG);
			registroA170.setNum_item(i);
			
			Material material = iten2.getMaterial();
			if(material == null && iten2.getVendamaterial() != null){
				material = iten2.getVendamaterial().getMaterial();
			}
			if(iten2.getMaterial() != null){
				registroA170.setCod_item(filtro.getApoio().addProdutoServico(iten2.getMaterial().getCdmaterial(), iten2.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), nfs.getEmpresa(), filtro.isCnpjEmpresaFiltro())));
			}else {
				registroA170.setCod_item(filtro.getApoio().addDescricaoIten(iten2.getDescricao(), getCNPJEmpresa(filtro.getEmpresa(), nfs.getEmpresa(), filtro.isCnpjEmpresaFiltro())));
			}
	//		registroA170.setDescr_compl(iten.getDescricao());			
			registroA170.setVl_item(iten2.getTotalParcial() != null ? iten2.getTotalParcial().getValue().doubleValue():null);
			registroA170.setVl_desc(iten2.getDeducao() != null ? iten2.getDeducao().getValue().doubleValue() : null);
	//		registroA170.setNat_bc_cred();
	//		registroA170.setInd_orig_cred();
			registroA170.setCst_pis(nfs.getCstpis() != null ? nfs.getCstpis().getCdnfe() : Tipocobrancapis.OPERACAO_49.getCdnfe());
			registroA170.setAliq_pis(nfs.getPis() != null ? nfs.getPis() : null);
			registroA170.setVl_pis(iten2.getValorPis() != null ?  SinedUtil.round(iten2.getValorPis().getValue(),2):null);
			registroA170.setAliq_cofins(nfs.getCofins() != null ? nfs.getCofins() : null);
			registroA170.setVl_cofins(iten2.getValorCofins() != null ?  SinedUtil.round(iten2.getValorCofins().getValue(),2):null);
			registroA170.setCst_cofins(nfs.getCstcofins() != null ? nfs.getCstcofins().getCdnfe() : Tipocobrancacofins.OPERACAO_49.getCdnfe());
			
			registroA170.setVl_bc_pis(iten2.getBasecalculoPis().getValue().doubleValue());
			registroA170.setVl_bc_cofins(iten2.getBasecalculoCofins() != null ? iten2.getBasecalculoCofins().getValue().doubleValue() : null);
			
			//Pegar o primeiro material e adicionar a CG
	//		Vendamaterial vendamaterial = listaItens.get(0).getVendamaterial();
	//		registroA170.setCod_cta(vendamaterial!=null && vendamaterial.getMaterial()!=null && vendamaterial.getMaterial().getContagerencialcontabil()!=null && vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial()!=null ? vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial().getIdentificador() : null);
	//		Material materialNFS = nfs.getMaterialSPED();
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), nfs.getNaturezaoperacao(), nfs.getMaterialSPED() != null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), nfs.getMaterialSPED(), nfs.getEmpresa()) : null);
			registroA170.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
	//		registroA170.setCod_ccus();
			
			lista.add(registroA170);
			i++;
			
			filtro.getApoio().addRegistros(RegistroA170.REG);
			filtro.getApoio().addIdentificador(registroA170.getCod_cta());
		}
		
		return lista;
	}
	
	private String getCNPJEmpresa(Empresa empresaFiltro, Empresa empresa, boolean considerarEmpresaFiltro) {
		if(considerarEmpresaFiltro && empresaFiltro != null && empresaFiltro.getCnpj() != null){
			return empresaFiltro.getCnpj().getValue();
		}
		if(empresa != null && empresa.getCnpj() != null){
			return empresa.getCnpj().getValue();
		}
		
		return null;
	}
	
	private String getCNPJEmpresa(Empresa empresaFiltro, String cnpj, boolean considerarEmpresaFiltro) {
		if(considerarEmpresaFiltro && empresaFiltro != null && empresaFiltro.getCnpj() != null){
			return empresaFiltro.getCnpj().getValue();
		}
		return cnpj;
	}
	
	private ContaContabil getContagerencialSped(SPEDPiscofinsApoio apoio, Naturezaoperacao naturezaoperacao, ContaContabil contagerencialContabilSPED) {
		if(naturezaoperacao != null){
			if(!apoio.getMapNaturezaContacontabil().containsKey(naturezaoperacao.getCdnaturezaoperacao())){
				Naturezaoperacao naturezaoperacao2 = naturezaoperacaoService.loadForSped(naturezaoperacao);
				ContaContabil contacontabil = null;
				if(naturezaoperacao2 != null && naturezaoperacao2.getOperacaocontabilpagamento() != null){
					contacontabil = getContaContabilSped(naturezaoperacao2.getOperacaocontabilpagamento().getListaCredito());
					if(contacontabil == null){
						contacontabil = getContaContabilSped(naturezaoperacao2.getOperacaocontabilpagamento().getListaDebito());
					}
				}
				apoio.getMapNaturezaContacontabil().put(naturezaoperacao.getCdnaturezaoperacao(), contacontabil);
			}
			
			ContaContabil contacontabil = apoio.getMapNaturezaContacontabil().get(naturezaoperacao.getCdnaturezaoperacao());
			if(contacontabil != null){
				return contacontabil;
			}
		}
		
		return contagerencialContabilSPED;
	}
	
	private ContaContabil getContaContabilSped(List<Operacaocontabildebitocredito> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Operacaocontabildebitocredito item : lista){
				if(Boolean.TRUE.equals(item.getSped())) return item.getContaContabil();
			}
		}
		return null;
	}
	
	private List<RegistroA170> createRegistroA170(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroA170> lista = new ArrayList<RegistroA170>();
		Set<Entregamaterial> listaItens = entregadocumento.getListaEntregamaterial();
		
		int i = 1;
		for(Entregamaterial item : listaItens){
			RegistroA170 registroA170 = new RegistroA170();
			
			BigDecimal valor_it = new BigDecimal(item.getValortotalmaterial_com_icmsst());
			
			if(item.getValorfrete() != null)
				valor_it = valor_it.add(item.getValorfrete().getValue());
			
			if(item.getValordesconto() != null)
				valor_it = valor_it.subtract(item.getValordesconto().getValue());
			
			registroA170.setReg(RegistroA170.REG);
			registroA170.setNum_item(i);
			registroA170.setCod_item(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), entregadocumento.getEmpresa(), filtro.isCnpjEmpresaFiltro())) : null);
//			registroA170.setDescr_compl(iten.getDescricao());			
			registroA170.setVl_item(valor_it.doubleValue());
			registroA170.setVl_desc(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : null);
			registroA170.setNat_bc_cred(item.getNaturezabcc() != null ? item.getNaturezabcc().getCdsped() : null);
			registroA170.setInd_orig_cred("0");
			registroA170.setCst_pis(item.getCstpis() != null ? item.getCstpis().getCdnfe() : null);
			registroA170.setVl_bc_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0);
			registroA170.setAliq_pis(item.getPis() != null ? item.getPis() : 0);
			registroA170.setVl_pis(item.getValorpis() != null ? SinedUtil.round(item.getValorpis().getValue(),2) : BigDecimal.ZERO);
			registroA170.setCst_cofins(item.getCstcofins() != null ? item.getCstcofins().getCdnfe() : null);
			registroA170.setVl_bc_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0);
			registroA170.setAliq_cofins(item.getCofins() != null ? item.getCofins() : 0);
			registroA170.setVl_cofins(item.getValorcofins() != null ? SinedUtil.round(item.getValorcofins().getValue(),2) : BigDecimal.ZERO);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento.getNaturezaoperacao(), item.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), item.getMaterial(), entregadocumento.getEmpresa()) : null);
			registroA170.setCod_cta(contacontabil !=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
//			registroA170.setCod_ccus();
			
			lista.add(registroA170);
			i++;
			
			this.createRegistroM410(filtro, null, null, null, item, entregadocumento);
			this.createRegistroM810(filtro, null, null, null, item, entregadocumento);
			
			filtro.getApoio().addRegistros(RegistroA170.REG);
			filtro.getApoio().addIdentificador(registroA170.getCod_cta());			
		}
		
		return lista;
	}
	
	private List<RegistroA110> createRegistroA110(SpedpiscofinsFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroA110> listaDesagrupada = new ArrayList<RegistroA110>();
		
		String info = entregadocumento.getInfoadicionalfisco();
		if(info != null && !info.trim().equals("")){
			RegistroA110 registroA110 = new RegistroA110();
			
			registroA110.setReg(RegistroA110.REG);
			registroA110.setCod_inf(filtro.getApoio().addInfoComplementar(info, entregadocumento.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroA110);
			
			filtro.getApoio().addRegistros(RegistroA110.REG);
		}
		
		String infoContrib = entregadocumento.getInfoadicionalcontrib();
		if(infoContrib != null && !infoContrib.trim().equals("")){
			RegistroA110 registroA110 = new RegistroA110();
			
			registroA110.setReg(RegistroA110.REG);
			registroA110.setCod_inf(filtro.getApoio().addInfoComplementar(infoContrib, entregadocumento.getEmpresa().getCnpj().getValue()));
			
			listaDesagrupada.add(registroA110);
			
			filtro.getApoio().addRegistros(RegistroA110.REG);
		}
		
		List<RegistroA110> listaAgrupada = new ArrayList<RegistroA110>();
		
		for (RegistroA110 registroA110 : listaDesagrupada) {
			if(!listaAgrupada.contains(registroA110)){
				listaAgrupada.add(registroA110);
			} else filtro.getApoio().removeRegistro(RegistroA110.REG);
		}		
		
		return listaDesagrupada;
	}
	
	private List<RegistroA110> createRegistroA110(SpedpiscofinsFiltro filtro, NotaFiscalServico nfs) {
		
		List<RegistroA110> lista = new ArrayList<RegistroA110>();
		
		RegistroA110 registroA110 = new RegistroA110();
		registroA110.setReg(RegistroA110.REG);
		registroA110.setCod_inf(filtro.getApoio().addInfoComplementar(nfs.getDadosAdicionais(), nfs.getEmpresa().getCnpj().getValue()));
		registroA110.setTxt(nfs.getDadosAdicionais());
		
		return lista;
	}
	
	private Registro0990 createRegistro0990(SpedpiscofinsFiltro filtro) {
		Registro0990 registro0990 = new Registro0990();
		
		registro0990.setReg(Registro0990.REG);
		filtro.getApoio().addRegistros(Registro0990.REG);
		
		return registro0990;
	}
	
	private void qtdeLinhasRegistro0990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		qtdeLinhas++; // Registro 0000
		
		if(spedPiscofins.getRegistro0000().getRegistro0001() != null){
			qtdeLinhas++; // Registro 0001
						
			if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0100() != null &&
					spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0100().size() > 0)
				qtdeLinhas += spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0100().size(); // Registro 0100
			
			if(spedPiscofins.getRegistro0000().getRegistro0001().getRegistro0110() != null){
				qtdeLinhas++; // Registro 0110
				if(spedPiscofins.getRegistro0000().getRegistro0001().getRegistro0110().getRegistro0111() != null)
					qtdeLinhas++; // Registro 0111
			}
			
			if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null && 
					spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size(); // Registro 0140
				for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
					if(registro0140.getListaRegistro0150() != null && registro0140.getListaRegistro0150().size() > 0)
						qtdeLinhas += registro0140.getListaRegistro0150().size(); // Registro 0150
					
					if(registro0140.getListaRegistro0190() != null && registro0140.getListaRegistro0190().size() > 0)
						qtdeLinhas += registro0140.getListaRegistro0190().size(); // Registro 0190
					
					if(registro0140.getListaRegistro0200() != null && registro0140.getListaRegistro0200().size() > 0){
						for (Registro0200 registro0200 : registro0140.getListaRegistro0200()) {
							qtdeLinhas++;
							if(registro0200.getRegistro0206() != null){
								qtdeLinhas++;
							}
						} // Registro 0200				
					}
						
					if(registro0140.getListaRegistro0400() != null && registro0140.getListaRegistro0400().size() > 0)
						qtdeLinhas += registro0140.getListaRegistro0400().size(); // Registro 0400
					
					if(registro0140.getListaRegistro0450() != null && registro0140.getListaRegistro0450().size() > 0)
						qtdeLinhas += registro0140.getListaRegistro0450().size(); // Registro 0450
				}
			}
			
			if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0500() != null &&
					spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0500().size() > 0)
				qtdeLinhas+= spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0500().size();
			
		}
		
		if(spedPiscofins.getRegistro0000().getRegistro0990() != null){
			qtdeLinhas++; // Registro 0990
		
			spedPiscofins.getRegistro0000().getRegistro0990().setQtd_lin_0(qtdeLinhas);
		}
	}

	private Registro0001 createRegistro0001(SpedpiscofinsFiltro filtro) {
		Registro0001 registro0001 = new Registro0001();
		
		registro0001.setReg(Registro0001.REG);
		
		registro0001.setListaRegistro0100(this.createRegistro0100(filtro));
		registro0001.setRegistro0110(this.createRegistro0110(filtro));
		registro0001.setListaRegistro0140(this.createRegistro0140(filtro));
//		registro0001.setListaRegistro0500(this.createRegistro0500(filtro));
		
		filtro.getApoio().addRegistros(Registro0001.REG);
		
		return registro0001;
	}

	private List<Registro0140> createRegistro0140(SpedpiscofinsFiltro filtro) {//n�vel 2 - v�rios por arquivo
		
		List<Registro0140> lista = new ArrayList<Registro0140>();
		
		Registro0140 registro0140;
		
		List<Empresa> listaEmpresa = empresaService.findForSpedPiscofinsReg0140(filtro);
		
		for(Empresa empresa : listaEmpresa){
			registro0140 = new Registro0140();
			registro0140.setReg(Registro0140.REG);		
	//		registro0140.setCod_est();
			registro0140.setNome(empresa.getRazaosocialOuNome());
			registro0140.setCnpj(empresa.getCnpj() != null ? empresa.getCnpj().getValue():null);
			registro0140.setUf(empresa.getUfsped() != null ? empresa.getUfsped().getSigla() : null);
			registro0140.setIe(empresa.getInscricaoestadual() != null && !empresa.getInscricaoestadual().equalsIgnoreCase("ISENTO") ? empresa.getInscricaoestadual() : "");
			registro0140.setCod_mun(empresa.getMunicipiosped() != null ? empresa.getMunicipiosped().getCdibge() : null);
			registro0140.setIm(empresa.getInscricaomunicipal());
	//		registro0140.setSuframa();
			
			filtro.getApoio().addRegistros(Registro0140.REG);
			
			lista.add(registro0140);
		}
		return lista;
	}
	
	private List<Registro0500> createRegistro0500(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio){
		List<Registro0500> lista = new ArrayList<Registro0500>();
		Set<String> listaIdentificador = apoio.getListaIdentificador();
		
		if (!listaIdentificador.isEmpty()){
			String whereInEmpresas = filtro.getWhereInEmpresaFiliais();
			List<ContaContabil> listaContaContabil = contacontabilService.loadForSpedPiscofinsReg0500(listaIdentificador, whereInEmpresas);
			List<String> contasJaAdicionadas = new ArrayList<String>();
			for(ContaContabil cc : listaContaContabil){
				if(cc.getVcontacontabil() == null || (StringUtils.isNotBlank(cc.getVcontacontabil().getIdentificador()) && contasJaAdicionadas.contains(cc.getVcontacontabil().getIdentificador()))){
					continue;
				}
				if(StringUtils.isNotBlank(cc.getVcontacontabil().getIdentificador())){
					contasJaAdicionadas.add(cc.getVcontacontabil().getIdentificador());
				}
				Registro0500 registro0500 = new Registro0500();		
				
				registro0500.setReg(Registro0500.REG);
				registro0500.setDt_alt(cc.getDtaltera()!=null ? new Date(cc.getDtaltera().getTime()) : new Date(System.currentTimeMillis()));
				registro0500.setCod_nat_cc(cc.getNatureza().getCdsped());
				if(contacontabilService.isPai(cc))
					registro0500.setInd_cta("A");
				else
					registro0500.setInd_cta("S");
				registro0500.setNivel(cc.getVcontacontabil().getNivel());
				registro0500.setCod_cta(cc.getVcontacontabil().getIdentificador());
				registro0500.setNome_cta(cc.getVcontacontabil().getNome());
//			registro0500.setCod_cta_ref();
//			registro0500.setCnpj_est();
				
				filtro.getApoio().addRegistros(Registro0500.REG);
				lista.add(registro0500);
			}
			
			spedPiscofins.getRegistro0000().getRegistro0001().setListaRegistro0500(lista);
		}		
		
		return lista;
	}	
	
	private void createRegistro0400(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null && 
				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
			for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
				Map<String, String> mapNatureza = filtro.getApoio().getMapNatureza(registro0140.getCnpj());
				if(mapNatureza != null){
					List<Registro0400> lista = new ArrayList<Registro0400>();
					Registro0400 registro0400;
					
					Set<Entry<String, String>> entrySet = mapNatureza.entrySet();
					for (Entry<String, String> entry : entrySet) {
						registro0400 = new Registro0400();
						
						registro0400.setReg(Registro0400.REG);
						registro0400.setCod_nat(entry.getValue());
						registro0400.setDescr_nat(entry.getKey());
						
						lista.add(registro0400);
						
						filtro.getApoio().addRegistros(Registro0400.REG);
					}
					
					registro0140.setListaRegistro0400(lista);
				}
			}
		}
	}
	
	private void createRegistro0200(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null && 
				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
			for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
				Map<String, Map<String, String>> mapProdutoServico = filtro.getApoio().getMapProdutoServico(getCNPJEmpresa(filtro.getEmpresa(), registro0140.getCnpj(), filtro.isCnpjEmpresaFiltro()));
				Map<String, String> mapDescricaoIten = apoio.getDescricaoIten(getCNPJEmpresa(filtro.getEmpresa(), registro0140.getCnpj(), filtro.isCnpjEmpresaFiltro()));
				if(mapProdutoServico == null)
					mapProdutoServico = new HashMap<String, Map<String, String>>();
				if(mapDescricaoIten == null)
					mapDescricaoIten = new HashMap<String, String>();
				
				StringBuilder sbDi = this.getIdsDescricaoItemMap(mapDescricaoIten);
				StringBuilder sb = this.getIdsMaps(mapProdutoServico);
				if(sb.length() == 0 && sbDi.length() == 0) continue;
				
				List<Registro0200> lista = new ArrayList<Registro0200>();
				Registro0200 registro0200;
				
				if(sbDi.length() > 0){
					Set<Entry<String, String>> entrySet = mapDescricaoIten.entrySet();
					for (Entry<String, String> entry : entrySet) {
						registro0200 = new Registro0200();
						
						registro0200.setReg(Registro0200.REG);
						registro0200.setCod_item(entry.getValue());
						
						registro0200.setDescr_item(entry.getKey());
			//			registro0200.setCod_barra(entry.getValue().getCodigobarras());
			//			registro0200.setCod_ant_item();
			//			registro0200.setUnid_inv(mat.getUnidademedida() != null ? apoio.addUnidade(mat.getUnidademedida().getCdunidademedida()) : null);
						registro0200.setTipo_item(Tipoitemsped.SERVICOS.getValue());
			//			registro0200.setCod_ncm(mat.getNcmcompleto());
			//			registro0200.setEx_ipi(mat.getExtipi());
			//			registro0200.setCod_gen(mat.getNcmcapitulo() != null ? mat.getNcmcapitulo().getCodeFormat() : null);
			//			registro0200.setCod_lst(mat.getCodlistaservico());
			//			registro0200.setAliq_icms();
						
						lista.add(registro0200);
						
						filtro.getApoio().addRegistros(Registro0200.REG);
					}
				}
				
				if(sb.length() > 0){
					List<Material> listaMaterial = materialService.findForSpedReg0200(sb.toString(), null);
					for (Material mat : listaMaterial) {
						Map<String, String> mapProd = mapProdutoServico.get(mat.getCdmaterial().toString());
						if(mapProd != null){
							for(String codigoFormatado : mapProd.values()){
								registro0200 = new Registro0200();
								
								registro0200.setReg(Registro0200.REG);
								registro0200.setCod_item(codigoFormatado);
								
								if(mat.getNome().length() > 255){
									registro0200.setDescr_item(mat.getNome().substring(0, 255));
								} else {
									registro0200.setDescr_item(mat.getNome());
								}
								registro0200.setCod_barra(mat.getCodigobarras());
								//			registro0200.setCod_ant_item();
								registro0200.setUnid_inv(mat.getUnidademedida() != null ? apoio.addUnidade(mat.getUnidademedida().getCdunidademedida(),registro0140.getCnpj()) : null);
								registro0200.setTipo_item(mat.getTipoitemsped() != null ? mat.getTipoitemsped().getValue() : null);
								registro0200.setCod_ncm(mat.getNcmcompleto());
								registro0200.setEx_ipi(mat.getExtipi());
								registro0200.setCod_gen(mat.getNcmcapitulo() != null ? mat.getNcmcapitulo().getCodeFormat() : null);
								registro0200.setCod_lst(mat.getCodlistaservico());
								//			registro0200.setAliq_icms();
								
								if(mat.getCodigoanp() != null){
									Registro0206 registro0206 = new Registro0206();
									registro0206.setReg(Registro0206.REG);
									registro0206.setCod_comb(mat.getCodigoanp().getCodigo());
									
									registro0200.setRegistro0206(registro0206);
									
									filtro.getApoio().addRegistros(Registro0206.REG);
								}
								
								lista.add(registro0200);
								
								filtro.getApoio().addRegistros(Registro0200.REG);
							}
						}
					}
				}
				registro0140.setListaRegistro0200(lista);
			}
		}
	}
	
	private void createRegistro0150(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null && 
				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
			for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
				Map<MultiKey, String> mapParticipante = apoio.getMapParticipante(registro0140.getCnpj());
				if(mapParticipante != null){
					StringBuilder sb = this.getIdsMap(mapParticipante);
					if(sb.length() == 0) return;
					
					List<Registro0150> lista = new ArrayList<Registro0150>();
					List<ParticipanteSpedBean> listaBean = pessoaService.findForSpedReg0150(sb.toString());
					
					for (ParticipanteSpedBean bean : listaBean) {
						if(StringUtils.isBlank(bean.getCpf()) && StringUtils.isBlank(bean.getCnpj())){
							continue;
						}
						String codPart = mapParticipante.get(new MultiKey(bean.getCdpessoa(), bean.getCdendereco()));
						if(codPart != null){
							if(bean.getCdendereco() != null){
								lista.add(createRegistro0150ByParticipanteBean(bean, codPart + "-" + bean.getCdendereco()));
							} else {
								lista.add(createRegistro0150ByParticipanteBean(bean, codPart));
							}
							filtro.getApoio().addRegistros(Registro0150.REG);
						}
					}
					
					registro0140.setListaRegistro0150(lista);
				}
			}
		}
	}
	
	private Registro0150 createRegistro0150ByParticipanteBean(ParticipanteSpedBean bean, String codPart) {
		Registro0150 registro0150 = new Registro0150();
		
		registro0150.setReg(Registro0150.REG);
		registro0150.setCod_part(codPart);
		registro0150.setNome(bean.getNome());
		registro0150.setCod_pais(StringUtils.isNotEmpty(bean.getCod_pais()) ? bean.getCod_pais() : "1058");
		registro0150.setCnpj(bean.getCnpj());
		registro0150.setCpf(bean.getCpf());
		registro0150.setIe(SPEDUtil.soNumero(bean.getIe()));
		registro0150.setCod_mun(bean.getCod_municipio());
//		registro0150.setSuframa();
		registro0150.setEnd(bean.getEnd());
		registro0150.setNum(bean.getNum());
		registro0150.setCompl(bean.getCompl());
		registro0150.setBairro(bean.getBairro());
		return registro0150;
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private Registro0110 createRegistro0110(SpedpiscofinsFiltro filtro) {//nivel 2 - um por arquivo
		
		Registro0110 registro0110 = new Registro0110();
		
		registro0110.setReg(Registro0110.REG);		
		
		if(filtro.getIndicadorincidenciatributaria() != null){
			registro0110.setCod_inc_trib(filtro.getIndicadorincidenciatributaria().getValue());
		
//			if(filtro.getMetodoapropriacaocredito() != null && filtro.getMetodoapropriacaocredito().getValue() == 2){
				if(registro0110.getCod_inc_trib() == 1 || registro0110.getCod_inc_trib() == 3){
					if(filtro.getMetodoapropriacaocredito() != null){
						registro0110.setInd_apro_cred(filtro.getMetodoapropriacaocredito().getValue());
						if(registro0110.getInd_apro_cred() == Metodoapropriacaocredito.RATEIO_PROPORCIONAL.getValue())
							registro0110.setRegistro0111(this.createRegistro0111(filtro));
					}			
				}
//			}
		}else
			filtro.addError("C�digo indicador da incid�ncia tribut�ria no per�odo � obrigat�rio.");
		
//		registro0110.setCod_tipo_cont();
//		registro0110.setInd_reg_cum();
		
		filtro.getApoio().addRegistros(Registro0110.REG);
		
		return registro0110;
	}
	
	
	private Registro0111 createRegistro0111(SpedpiscofinsFiltro filtro) {//nivel 3 - 1:1
		
		Registro0111 registro0111 = new Registro0111();
		
		registro0111.setReg(Registro0111.REG);
		
		registro0111.setRec_bru_ncum_trib_mi(0d);
		registro0111.setRec_bru_ncum_nt_mi(0d);
		registro0111.setRec_bru_ncum_exp(0d);
		registro0111.setRec_bru_cum(0d);
		registro0111.setRec_bru_total(0d);
		
		filtro.getApoio().addRegistros(Registro0111.REG);
		
		return registro0111;
	}
	/**
	 *
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Registro0100> createRegistro0100(SpedpiscofinsFiltro filtro) {//nivel 2 - v�rios por arquivo
		
		List<Registro0100> lista = new ArrayList<Registro0100>();
		
		Registro0100 registro0100 = new Registro0100();
		
		Empresa empresa = empresaService.loadForSpedPiscofinsReg0100(filtro.getEmpresa());
		Fornecedor contabilista = empresa.getColaboradorcontabilista() != null  ? fornecedorService.loadForSpedPiscofinsReg0100Contabilista(empresa.getColaboradorcontabilista()) : null;
		Fornecedor escritorio = empresa.getEscritoriocontabilista() != null ? fornecedorService.loadForSpedPiscofinsReg0100Escritorio(empresa.getEscritoriocontabilista()) : null;
		
		registro0100.setReg(Registro0100.REG);
		registro0100.setCrc(empresa.getCrccontabilista());
		
		if(contabilista != null){
			registro0100.setNome(contabilista.getNome());
			registro0100.setCpf(contabilista.getCpf() != null ? contabilista.getCpf().getValue() : null);
			
			if(contabilista.getCpf() == null){
				filtro.addError("O campo CPF do contabilista � obrigat�rio.");
			}
		}
		
		if(escritorio != null){
			registro0100.setCnpj(escritorio.getCnpj() != null ? escritorio.getCnpj().getValue() : null);
			registro0100.setEmail(escritorio.getEmail());
			
			Endereco endereco = escritorio.getEndereco();
			Telefone telefone = SinedUtil.getTelefoneTipo(escritorio.getListaTelefone(), new Telefonetipo(Telefonetipo.PRINCIPAL), new Telefonetipo(Telefonetipo.COMERCIAL), new Telefonetipo(Telefonetipo.RESIDENCIAL));
			Telefone fax = SinedUtil.getTelefoneTipo(escritorio.getListaTelefone(), new Telefonetipo(Telefonetipo.FAX));
			
			if(endereco != null){
				registro0100.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
				registro0100.setEnd(endereco.getLogradouro());
				registro0100.setNum(endereco.getNumero());
				registro0100.setCompl(endereco.getComplemento());
				registro0100.setBairro(endereco.getBairro());
				registro0100.setCod_mun(endereco.getMunicipio() != null ? endereco.getMunicipio().getCdibge() : null);
			}
			registro0100.setFone(telefone != null ? SPEDUtil.soNumero(telefone.getTelefone()) : null);
			registro0100.setFax(fax != null ? SPEDUtil.soNumero(fax.getTelefone()) : null);
		}
		
		filtro.getApoio().addRegistros(Registro0100.REG);
		
		lista.add(registro0100);
		
		return lista;		
	}

	private Registro9999 createRegistro9999(SpedpiscofinsFiltro filtro) {		
		Registro9999 registro9999 = new Registro9999();
		
		registro9999.setReg(Registro9999.REG);
		
		filtro.getApoio().addRegistros(Registro9999.REG);
		
		return registro9999;
	}
	
	private void saveArquivoSpedpiscofins(SpedpiscofinsFiltro filtro, String nome, byte[] bytes) {
		Arquivo arquivo = new Arquivo(bytes, nome, "text/plain");
		
		this.deleteByEmpresaDtinicioDtfim(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		
		Spedpiscofins spedpiscofins = new Spedpiscofins();
		spedpiscofins.setDtinicio(filtro.getDtinicio());
		spedpiscofins.setDtfim(filtro.getDtfim());
		spedpiscofins.setIndicadorincidenciatributaria(filtro.getIndicadorincidenciatributaria());
		spedpiscofins.setMetodoapropriacaocredito(filtro.getMetodoapropriacaocredito());
		spedpiscofins.setArquivo(arquivo);
		spedpiscofins.setEmpresa(filtro.getEmpresa());
		
		arquivoDAO.saveFile(spedpiscofins, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		this.saveOrUpdate(spedpiscofins);		
		
	}
	
	private void deleteByEmpresaDtinicioDtfim(Empresa empresa, Date dtinicio, Date dtfim) {
		spedpiscofinsDAO.deleteByEmpresaDtinicioDtfim(empresa, dtinicio, dtfim);
	}
	
	private StringBuilder getIdsMap(Map<?, String> map) {
		StringBuilder sb = new StringBuilder();
		for (Entry<?, String> entry : map.entrySet()) {
			Object key = entry.getKey();
			if(key != null){
				if(key instanceof MultiKey){
					sb.append(((MultiKey)key).getKey(0)).append(",");
				} else {
					sb.append(key).append(",");
				}
			}
		}
		
		if(sb.length() > 0){
			sb.delete(sb.length() - 1, sb.length());
		}
		
		return sb;
	}
	
	private StringBuilder getIdsMaps(Map<String, Map<String, String>> map) {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Map<String, String>> entry : map.entrySet()) {
			Object key = entry.getKey();
			if(key != null){
				if(key instanceof MultiKey){
					sb.append(((MultiKey)key).getKey(0)).append(",");
				} else {
					sb.append(key).append(",");
				}
			}
		}
		
		if(sb.length() > 0){
			sb.delete(sb.length() - 1, sb.length());
		}
		
		return sb;
	}
	
	private StringBuilder getIdsDescricaoItemMap(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();
		Set<Entry<String, String>> entrySet = map.entrySet();
		for (Entry<String, String> entry : entrySet) {
			if(entry.getKey() != null){
				sb.append(entry.getKey()).append(",");
			}
		}
		
		if(sb.length() > 0){
			sb.delete(sb.length() - 1, sb.length());
		}
		
		return sb;
	}
	
	private List<Entregadocumento> getListaEntregaC100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaEntregadocumento = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaEntregaNormal = entradafiscalService.findForSpedPiscofinsRegC100(filtro, false, empresa);
			for (Entregadocumento e1 : listaEntregaNormal) {
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntregadocumento.add(e1);
			}
		}
		
		return listaEntregadocumento;
	}
	
	private List<Entregadocumento> getListaEntregaF100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaEntregadocumento = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaEntregaNormal = entradafiscalService.findForSpedPiscofinsRegF100(filtro, false, empresa);
			for (Entregadocumento e1 : listaEntregaNormal) {
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntregadocumento.add(e1);
			}
		}
		
		return listaEntregadocumento;
	}
	
	private List<Entregadocumento> getListaEntregaA100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaEntrega = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaEntregadocumentoNormal = entradafiscalService.findForSpedPiscofinsRegC100(filtro, true, empresa);
			for (Entregadocumento e1 : listaEntregadocumentoNormal) {
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntrega.add(e1);
			}
		}
		
		return listaEntrega;
	}
	
	private List<Notafiscalproduto> getListaNotaC100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Notafiscalproduto> listaNotafiscalproduto = new ArrayList<Notafiscalproduto>();
		
		List<Notafiscalproduto> listaNotafiscalprodutoNormal = notafiscalprodutoService.findForSpedPiscofinsRegC100(filtro, empresa);
		for (Notafiscalproduto nf1 : listaNotafiscalprodutoNormal) {
			nf1.setSituacaosped("00");
			listaNotafiscalproduto.add(nf1);
		}
		
		return listaNotafiscalproduto;
	}
	
	private List<Entregadocumento> getListaDocumentoC500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaEntradafiscal = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaDocumentoNormal = entradafiscalService.findForSpedPiscofinsRegC500(filtro, empresa);
			for (Entregadocumento dt1 : listaDocumentoNormal) {
				if(dt1.getSituacaodocumento() != null){
					dt1.setSituacaosped(dt1.getSituacaodocumento().getValue());
				}else {
					dt1.setSituacaosped("00");
				}
				listaEntradafiscal.add(dt1);
			}
		}
		
		return listaEntradafiscal;
	}
	
	private List<Entregadocumento> getListaEntregaD100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaEntrega = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaEntregaNormal = entradafiscalService.findForSpedPiscofinsRegD100(filtro, empresa);
			for (Entregadocumento e1 : listaEntregaNormal) {
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntrega.add(e1);
			}
		}
		
		return listaEntrega;
	}
	
	private List<RegistroD100> joinListaRegistroD100(List<RegistroD100> listaDesagrupada, SpedpiscofinsFiltro filtro) {
		List<RegistroD100> listaAgrupada = new ArrayList<RegistroD100>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroD100 registroD100 : listaDesagrupada) {
				if(listaAgrupada.contains(registroD100)){
					int idx = listaAgrupada.indexOf(registroD100);
					RegistroD100 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroD100(aux, registroD100, filtro);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroD100.REG);
				} else {
					listaAgrupada.add(registroD100);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	private RegistroD100 joinRegistroD100(RegistroD100 ori, RegistroD100 copia, SpedpiscofinsFiltro filtro) {
		
		ori.setVl_doc((ori.getVl_doc() != null ? ori.getVl_doc() : 0d) + (copia.getVl_doc() != null ? copia.getVl_doc() : 0d));
		ori.setVl_desc((ori.getVl_desc() != null ? ori.getVl_desc() : 0d) + (copia.getVl_desc() != null ? copia.getVl_desc() : 0d));
		ori.setVl_bc_icms((ori.getVl_bc_icms() != null ? ori.getVl_bc_icms() : 0d) + (copia.getVl_bc_icms() != null ? copia.getVl_bc_icms() : 0d));
		ori.setVl_icms((ori.getVl_icms() != null ? ori.getVl_icms() : 0d) + (copia.getVl_icms() != null ? copia.getVl_icms() : 0d));
		ori.setVl_serv((ori.getVl_serv() != null ? ori.getVl_serv() : 0d) + (copia.getVl_serv() != null ? copia.getVl_serv() : 0d));
		ori.setVl_nt((ori.getVl_nt() != null ? ori.getVl_nt() : 0d) + (copia.getVl_nt() != null ? copia.getVl_nt() : 0d));
		
		return ori;
	}
	
	private List<Entregadocumento> getListaDocumentoD500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		List<Entregadocumento> listaDocumento = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaDocumentoNormal = entradafiscalService.findForSpedPiscofinsRegD500(filtro, empresa);
			for (Entregadocumento dt1 : listaDocumentoNormal) {
				if(dt1.getSituacaodocumento() != null){
					dt1.setSituacaosped(dt1.getSituacaodocumento().getValue());
				}else {
					dt1.setSituacaosped("00");
				}
				listaDocumento.add(dt1);
			}
		}
		
		return listaDocumento;
	}
	
	/**
	 *
	 *
	 * @param spedPiscofins
	 * @author Luiz Fernando
	 */
	private void existeRegistro0001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistro0001() != null){
			int ind_mov = 1;
			
			if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0100() != null) ind_mov = 0;
			else if(spedPiscofins.getRegistro0000().getRegistro0001().getRegistro0110() != null) ind_mov = 0;
			else if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140())) ind_mov = 0;
			else if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0500())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistro0001().setInd_mov(ind_mov);
		}
	}
	
	private void existeRegistroF001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroF001() != null){
			int ind_mov = 1;
			
			if(spedPiscofins.getRegistro0000().getRegistroF001().getListaRegistroF010() != null) ind_mov = 0;			
			
			spedPiscofins.getRegistro0000().getRegistroF001().setInd_mov(ind_mov);
		}		
	}
	
	/**
	 *
	 *
	 * @param spedPiscofins
	 * @author Luiz Fernando
	 */
	private void existeRegistroA001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroA001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroA001().getListaRegistroA010())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistroA001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 *
	 *
	 * @param spedPiscofins
	 * @author Luiz Fernando
	 */
	private void existeRegistroC001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroC001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistroC001().setInd_mov(ind_mov);
		}
	}
	private void existeRegistro1001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistro1001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistro1001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 *
	 *
	 * @param spedPiscofins
	 * @author Luiz Fernando
	 */
	private void existeRegistroD001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroD001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroD001().getListaRegistroD010())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistroD001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 *
	 *
	 * @param spedPiscofins
	 * @author Luiz Fernando
	 */
	private void existeRegistroM001(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroM001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM100())) ind_mov = 0;
			else if(spedPiscofins.getRegistro0000().getRegistroM001().getRegistroM200() != null) ind_mov = 0;
			else if(spedPiscofins.getRegistro0000().getRegistroM001().getRegistroM600() != null) ind_mov = 0;
			else if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM400())) ind_mov = 0;
			else if(this.listaComRegistro(spedPiscofins.getRegistro0000().getRegistroM001().getListaRegistroM800())) ind_mov = 0;
			
			spedPiscofins.getRegistro0000().getRegistroM001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 *
	 *
	 * @param lista
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean listaComRegistro(List<? extends Object> lista){
		return lista != null && lista.size() > 0;
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @param spedPiscofins
	 * @param mapInfoComplementar
	 * @author Luiz Fernando
	 */
	private void createRegistro0450(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null &&
				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
			for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
				Map<String, String> mapInfoComplementar = apoio.getMapInfoComplementar(registro0140.getCnpj());
				if(mapInfoComplementar != null){
					List<Registro0450> lista = new ArrayList<Registro0450>();
					Registro0450 registro0450;
					
					Set<Entry<String, String>> entrySet = mapInfoComplementar.entrySet();
					for (Entry<String, String> entry : entrySet) {
						registro0450 = new Registro0450();
						
						registro0450.setReg(Registro0450.REG);
						registro0450.setCod_inf(entry.getValue());
						registro0450.setTxt(entry.getKey() != null && entry.getKey().length() > 255 ? entry.getKey().substring(0, 255) : entry.getKey());
						
						lista.add(registro0450);
						
						filtro.getApoio().addRegistros(Registro0450.REG);
					}
					
					registro0140.setListaRegistro0450(lista);
				}
			}
		}		
	}
	
	/**
	 *
	 *
	 * @param filtro
	 * @param spedPiscofins
	 * @param mapUnidade
	 * @author Luiz Fernando
	 */
	private void createRegistro0190(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins, SPEDPiscofinsApoio apoio) {
		if(spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140() != null &&
				spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140().size() > 0){
			for(Registro0140 registro0140 : spedPiscofins.getRegistro0000().getRegistro0001().getListaRegistro0140()){
				Map<Integer, String> mapUnidade = apoio.getMapUnidade(registro0140.getCnpj());
				if(mapUnidade != null){
					StringBuilder sb = this.getIdsMap(mapUnidade);
					if(sb.length() == 0) return;
					
					List<Registro0190> lista = new ArrayList<Registro0190>();
					Registro0190 registro0190;
					
					List<Unidademedida> listaUnidade = unidademedidaService.findForSpedPiscofinsReg0190(sb.toString());
					for (Unidademedida um : listaUnidade) {
						registro0190 = new Registro0190();
						
						registro0190.setReg(Registro0190.REG);
						registro0190.setUnid(mapUnidade.get(um.getCdunidademedida()));
						registro0190.setDescr(um.getNome());
						
						lista.add(registro0190);
						
						filtro.getApoio().addRegistros(Registro0190.REG);
					}
					
					
					registro0140.setListaRegistro0190(lista);
				}
			}			
		}
	}
	
	private int createRegistro9900(SpedpiscofinsFiltro filtro, SPEDPiscofins spedPiscofins) {
		int countLinhaTotal = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistro9001() != null){
			List<Registro9900> lista = new ArrayList<Registro9900>();
			Registro9900 registro9900;
			
			Map<String, Integer> map = filtro.getApoio().getMapTotalizadorRegistros();
			Set<Entry<String, Integer>> entrySet = map.entrySet();
			
			
			int countLinha = 0;
			
			for (Entry<String, Integer> entry : entrySet) {
				registro9900 = new Registro9900();
				
				registro9900.setReg(Registro9900.REG);
				registro9900.setReg_blc(entry.getKey());
				registro9900.setQtd_reg_blc(entry.getValue());
				
				countLinhaTotal += entry.getValue();
				
				lista.add(registro9900);
				
				countLinha++;
			}
			
			registro9900 = new Registro9900();
			
			countLinha++;
			
			registro9900.setReg(Registro9900.REG);
			registro9900.setReg_blc(Registro9900.REG);
			registro9900.setQtd_reg_blc(countLinha);
			
			countLinhaTotal += countLinha;
			
			lista.add(registro9900);
			
			spedPiscofins.getRegistro0000().getRegistro9001().setListaRegistro9900(lista);
		}
		
		return countLinhaTotal;
	}
	
	private void qtdeLinhasRegistro9990(SPEDPiscofins spedPiscofins) {
		int qtdeLinhas = 0;
		
		if(spedPiscofins.getRegistro0000().getRegistro9001() != null){
			qtdeLinhas++; // Registro 9001
			
			if(spedPiscofins.getRegistro0000().getRegistro9001().getListaRegistro9900() != null)
				qtdeLinhas += spedPiscofins.getRegistro0000().getRegistro9001().getListaRegistro9900().size(); // Registro 9900
		}
		
		if(spedPiscofins.getRegistro0000().getRegistro9990() != null){
			qtdeLinhas++; // Registro 9990
			qtdeLinhas++; // Registro 9999
			
			spedPiscofins.getRegistro0000().getRegistro9990().setQtd_lin_9(qtdeLinhas);
		}
	}
	
	private void resumoProcessamentoSped(SPEDPiscofins spedPiscofins, SpedpiscofinsFiltro filtro) {
		SPEDPiscofinsApoio apoio = filtro.getApoio();
		
		if(apoio != null && spedPiscofins != null){
			if(spedPiscofins.getRegistro0000() != null) {
				if(spedPiscofins.getRegistro0000().getRegistro0001() != null){
					this.createRegistroF001(filtro, spedPiscofins, apoio);
					this.createRegistro0200(filtro, spedPiscofins, apoio);					
					this.createRegistro0150(filtro, spedPiscofins, apoio);
					this.createRegistro0190(filtro, spedPiscofins, apoio);
					this.createRegistro0400(filtro, spedPiscofins, apoio);
					this.createRegistro0450(filtro, spedPiscofins, apoio);
					this.createRegistro0500(filtro, spedPiscofins, apoio);
					spedPiscofins.getRegistro0000().setRegistroF990(this.createRegistroF990(filtro));
				}
				
				this.existeRegistro0001(spedPiscofins);
				this.existeRegistroA001(spedPiscofins);
				this.existeRegistroC001(spedPiscofins);
				this.existeRegistro1001(spedPiscofins);
				this.existeRegistroD001(spedPiscofins);
				this.existeRegistroF001(spedPiscofins);
				this.existeRegistroM001(spedPiscofins);
				
				this.qtdeLinhasRegistro0990(spedPiscofins);
				this.qtdeLinhasRegistro1990(spedPiscofins);
				this.qtdeLinhasRegistroA990(spedPiscofins);
				this.qtdeLinhasRegistroC990(spedPiscofins);
				this.qtdeLinhasRegistroD990(spedPiscofins);
				this.qtdeLinhasRegistroF990(spedPiscofins);
				this.qtdeLinhasRegistroM990(spedPiscofins);
				
				int qtdeTotalLinhas = this.createRegistro9900(filtro, spedPiscofins);
				this.qtdeLinhasRegistro9990(spedPiscofins);
				
				this.verificaC170Pata1011(spedPiscofins);
				
				if(spedPiscofins.getRegistro9999() != null) spedPiscofins.getRegistro9999().setQtd_lin(qtdeTotalLinhas);
			}
		}
	}
	
	/**
	 * M�todo que cria o Registro M410
	 *
	 * @param filtro
	 * @param nfs
	 * @param nfpi
	 * @param em
	 * @author Luiz Fernando
	 */
	private void createRegistroM410(SpedpiscofinsFiltro filtro, NotaFiscalServico nfs, Notafiscalprodutoitem nfpi, Notafiscalproduto notafiscalproduto, Entregamaterial em, Entregadocumento entregadocumento) {
		if(filtro.getListaAuxRegistroM410() == null)
			filtro.setListaAuxRegistroM410(new ArrayList<RegistroM410>());
		
		RegistroM410 registroM410 = new RegistroM410();
		registroM410.setReg(RegistroM410.REG);
		Material material = null;
		Double aliquota = null;
		if(nfs != null && nfs.getCstpis() != null){
			registroM410.setNat_rec(nfs.getCodigonaturezareceitaTrans(Faixaimpostocontrole.PIS));
			registroM410.setCst_pis_aux(nfs.getCstpis().getCdnfe());
			registroM410.setVl_rec(nfs.getBasecalculopis() != null ? nfs.getBasecalculopis().getValue().doubleValue() : 0d);
//			Vendamaterial vendamaterial = nfs.getListaItens().get(0).getVendamaterial();
//			registroM410.setCod_cta(vendamaterial!=null && vendamaterial.getMaterial()!=null && vendamaterial.getMaterial().getContagerencialcontabil()!=null && vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial()!=null ? vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial().getIdentificador() : null);
//			Material materialNFS = nfs.getMaterialSPED();
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), nfs.getNaturezaoperacao(), nfs.getMaterialSPED() != null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), material, nfs.getEmpresa()) : null);
			registroM410.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			aliquota = nfs.getPis();
		}else if(nfpi != null && nfpi.getTipocobrancapis() != null){
			registroM410.setCst_pis_aux(nfpi.getTipocobrancapis().getCdnfe());
			registroM410.setVl_rec(nfpi.getValorbruto() != null ? nfpi.getValorbruto().getValue().doubleValue() : 0d);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), notafiscalproduto != null ? notafiscalproduto.getNaturezaoperacao() : null, nfpi.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), nfpi.getMaterial(), notafiscalproduto.getEmpresa()) : null);
			registroM410.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			material = nfpi.getMaterial();
			aliquota = nfpi.getPis();
		}else if(em != null && em.getCstpis() != null){
			registroM410.setCst_pis_aux(em.getCstpis().getCdnfe());
			registroM410.setVl_rec(em.getValorbcpis() != null ? em.getValorbcpis().getValue().doubleValue() : 0d);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento != null ? entregadocumento.getNaturezaoperacao() : null, em.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), em.getMaterial(), entregadocumento.getEmpresa()): null);
			registroM410.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			material = em.getMaterial();
			aliquota = em.getPis();
		}
		
		if(material != null){
			registroM410.setNat_rec(material.getCodigonaturezareceitaTrans(Faixaimpostocontrole.PIS));
		}
		
		if(registroM410.getNat_rec() != null && registroM410.getCst_pis_aux() != null && !"".equals(registroM410.getCst_pis_aux()) && 
				((Tipocobrancapis.OPERACAO_04.getCdnfe().equals(registroM410.getCst_pis_aux()) ||
				 Tipocobrancapis.OPERACAO_06.getCdnfe().equals(registroM410.getCst_pis_aux()) ||
				 Tipocobrancapis.OPERACAO_07.getCdnfe().equals(registroM410.getCst_pis_aux()) ||
				 Tipocobrancapis.OPERACAO_08.getCdnfe().equals(registroM410.getCst_pis_aux()) ||
				 Tipocobrancapis.OPERACAO_09.getCdnfe().equals(registroM410.getCst_pis_aux())) ||
				 (Tipocobrancapis.OPERACAO_05.getCdnfe().equals(registroM410.getCst_pis_aux()) &&
						 new Double(0.0).equals(aliquota)))){
			this.addRegistroM410(registroM410, filtro);
		}
		
	}
	
	/**
	 * M�todo que cria o Registro M810
	 *
	 * @param filtro
	 * @param nfs
	 * @param nfpi
	 * @param em
	 * @author Luiz Fernando
	 */
	private void createRegistroM810(SpedpiscofinsFiltro filtro, NotaFiscalServico nfs, Notafiscalprodutoitem nfpi, Notafiscalproduto notafiscalproduto, Entregamaterial em, Entregadocumento entregadocumento) {
		if(filtro.getListaAuxRegistroM810() == null)
			filtro.setListaAuxRegistroM810(new ArrayList<RegistroM810>());
		
		RegistroM810 registroM810 = new RegistroM810();
		registroM810.setReg(RegistroM810.REG);
		Material material = null;
		Double aliquota = null;
		if(nfs != null && nfs.getCstcofins() != null){
			registroM810.setNat_rec(nfs.getCodigonaturezareceitaTrans(Faixaimpostocontrole.COFINS));
			registroM810.setCst_cofins_aux(nfs.getCstcofins().getCdnfe());
			registroM810.setVl_rec(nfs.getBasecalculocofins() != null ? nfs.getBasecalculocofins().getValue().doubleValue() : 0d);
//			Vendamaterial vendamaterial = nfs.getListaItens().get(0).getVendamaterial();
//			registroM810.setCod_cta(vendamaterial!=null && vendamaterial.getMaterial()!=null && vendamaterial.getMaterial().getContagerencialcontabil()!=null && vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial()!=null ? vendamaterial.getMaterial().getContagerencialcontabil().getVcontagerencial().getIdentificador() : null);
//			Material materialNFS = nfs.getMaterialSPED();
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), nfs.getNaturezaoperacao(), nfs.getMaterialSPED() != null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), nfs.getMaterialSPED(), nfs.getEmpresa()) : null);
			registroM810.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			aliquota = nfs.getPis();
		}else if(nfpi != null && nfpi.getTipocobrancacofins() != null){
			registroM810.setCst_cofins_aux(nfpi.getTipocobrancacofins().getCdnfe());
			registroM810.setVl_rec(nfpi.getValorbruto() != null ? nfpi.getValorbruto().getValue().doubleValue() : 0d);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), notafiscalproduto != null ? notafiscalproduto.getNaturezaoperacao() : null, nfpi.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), nfpi.getMaterial(), notafiscalproduto.getEmpresa()): null);
			registroM810.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			material = nfpi.getMaterial();
			aliquota = nfpi.getPis();
		}else if(em != null && em.getCstcofins() != null){
			registroM810.setCst_cofins_aux(em.getCstcofins().getCdnfe());
			registroM810.setVl_rec(em.getValorbccofins() != null ? em.getValorbccofins().getValue().doubleValue() : 0d);
			ContaContabil contacontabil = getContagerencialSped(filtro.getApoio(), entregadocumento != null ? entregadocumento.getNaturezaoperacao() : null, em.getMaterial()!=null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), em.getMaterial(), entregadocumento.getEmpresa()) : null);
			registroM810.setCod_cta(contacontabil!=null && contacontabil.getVcontacontabil()!=null ? contacontabil.getVcontacontabil().getIdentificador() : null);
			material = em.getMaterial();
			aliquota = em.getPis();
		}
		
		if(material != null){
			registroM810.setNat_rec(material.getCodigonaturezareceitaTrans(Faixaimpostocontrole.COFINS));
		}
		
		if(registroM810.getCst_cofins_aux() != null && !"".equals(registroM810.getCst_cofins_aux()) &&
				((Tipocobrancapis.OPERACAO_04.getCdnfe().equals(registroM810.getCst_cofins_aux()) ||
				 Tipocobrancapis.OPERACAO_06.getCdnfe().equals(registroM810.getCst_cofins_aux()) ||
				 Tipocobrancapis.OPERACAO_07.getCdnfe().equals(registroM810.getCst_cofins_aux()) ||
				 Tipocobrancapis.OPERACAO_08.getCdnfe().equals(registroM810.getCst_cofins_aux()) ||
				 Tipocobrancapis.OPERACAO_09.getCdnfe().equals(registroM810.getCst_cofins_aux())) || 
				 (Tipocobrancapis.OPERACAO_05.getCdnfe().equals(registroM810.getCst_cofins_aux()) &&
						 new Double(0.0).equals(aliquota)))){
			this.addRegistroM810(registroM810, filtro);
		}
	}
	
	private void addRegistroM400(List<RegistroM400> listaRegistroM400, RegistroM400 registroM400, RegistroM410 registroM410, SpedpiscofinsFiltro filtro){
		if(listaRegistroM400 == null)
			listaRegistroM400 = new ArrayList<RegistroM400>();
		
		boolean adicionar = true;
		
		for(RegistroM400 m400 : listaRegistroM400){
			if(m400.getCst_pis().equals(registroM400.getCst_pis())){
				adicionar = false;
				m400.setVl_tot_rec(m400.getVl_tot_rec()+registroM400.getVl_tot_rec());
				if(m400.getListaRegistroM410() == null) 
					m400.setListaRegistroM410(new ArrayList<RegistroM410>());
				m400.getListaRegistroM410().add(registroM410);
				filtro.getApoio().addRegistros(registroM410.getReg());
				break;
			}
		}
		
		if(adicionar){
			if(registroM400.getListaRegistroM410() == null) 
				registroM400.setListaRegistroM410(new ArrayList<RegistroM410>());
			registroM400.getListaRegistroM410().add(registroM410);
			listaRegistroM400.add(registroM400);
			filtro.getApoio().addRegistros(registroM400.getReg());
			filtro.getApoio().addRegistros(registroM410.getReg());
			filtro.getApoio().addIdentificador(registroM400.getCod_cta());
		}
	}
	
	private void addRegistroM800(List<RegistroM800> listaRegistroM800, RegistroM800 registroM800, RegistroM810 registroM810, SpedpiscofinsFiltro filtro){
		if(listaRegistroM800 == null)
			listaRegistroM800 = new ArrayList<RegistroM800>();
		
		boolean adicionar = true;
		
		for(RegistroM800 M800 : listaRegistroM800){
			if(M800.getCst_pis().equals(registroM800.getCst_pis())){
				adicionar = false;
				M800.setVl_tot_rec(M800.getVl_tot_rec()+registroM800.getVl_tot_rec());
				if(M800.getListaRegistroM810() == null) 
					M800.setListaRegistroM810(new ArrayList<RegistroM810>());
				M800.getListaRegistroM810().add(registroM810);
				filtro.getApoio().addRegistros(registroM810.getReg());
				break;
			}
		}
		
		if(adicionar){
			if(registroM800.getListaRegistroM810() == null) 
				registroM800.setListaRegistroM810(new ArrayList<RegistroM810>());
			registroM800.getListaRegistroM810().add(registroM810);
			listaRegistroM800.add(registroM800);
			filtro.getApoio().addRegistros(registroM800.getReg());
			filtro.getApoio().addRegistros(registroM810.getReg());
			filtro.getApoio().addIdentificador(registroM800.getCod_cta());
		}
	}
	
	/**
	 * M�todo que adiciona o registro m410 na lista. Deve ser informado um registro m410 para cada cst.
	 *
	 * @param registroM410
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void addRegistroM410(RegistroM410 registroM410, SpedpiscofinsFiltro filtro) {
		if(registroM410 != null){
			if(filtro.getListaAuxRegistroM410() == null) filtro.setListaAuxRegistroM410(new ArrayList<RegistroM410>());
			boolean adicionar = true;
			for(RegistroM410 m410 : filtro.getListaAuxRegistroM410()){
				if(m410.getCst_pis_aux().equals(registroM410.getCst_pis_aux())){
					if((m410.getNat_rec() == null && registroM410.getNat_rec() == null) || (m410.getNat_rec().equals(registroM410.getNat_rec()))){
						adicionar = false;
						m410.setVl_rec(m410.getVl_rec()+registroM410.getVl_rec());
					}
				}
			}
			
			if(adicionar){
				filtro.getListaAuxRegistroM410().add(registroM410);
				filtro.getApoio().addIdentificador(registroM410.getCod_cta());
			}
		}
		
	}
	
	/**
	 * M�todo que adiciona o registro m810 na lista. Deve ser informado um registro m810 para cada cst.
	 *
	 * @param registroM810
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void addRegistroM810(RegistroM810 registroM810, SpedpiscofinsFiltro filtro) {
		if(registroM810 != null){
			if(filtro.getListaAuxRegistroM810() == null) filtro.setListaAuxRegistroM810(new ArrayList<RegistroM810>());
			boolean adicionar = true;
			for(RegistroM810 m810 : filtro.getListaAuxRegistroM810()){
				if(m810.getCst_cofins_aux() != null && m810.getCst_cofins_aux().equals(registroM810.getCst_cofins_aux())){
					if((m810.getNat_rec() == null && registroM810.getNat_rec() == null) || (m810.getNat_rec() != null && m810.getNat_rec().equals(registroM810.getNat_rec()))){
						adicionar = false;
						m810.setVl_rec(m810.getVl_rec()+registroM810.getVl_rec());
					}
				}
			}
			
			if(adicionar){
				filtro.getListaAuxRegistroM810().add(registroM810);
				filtro.getApoio().addIdentificador(registroM810.getCod_cta());
			}
		}
		
	}
	
	public void criarAvisoSped(Motivoaviso m, Date data) {
		List<Empresa> empresaList = empresaService.findAtivos();
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		List<Spedpiscofins> spedPisCofinsList = null;
		
				for (Empresa e : empresaList) {
					if (e.getGeracaospedpiscofins() != null && e.getGeracaospedpiscofins() && e.getDiageracaospedpiscofins() != null) {
						spedPisCofinsList = spedpiscofinsDAO.getSpedMesAnterior(e);
						if (spedPisCofinsList != null && spedPisCofinsList.size() == 0 && SinedDateUtils.getDia(SinedDateUtils.currentDate()) == e.getDiageracaospedpiscofins()) {
							Aviso aviso = new Aviso("SPED Contribui��es n�o emitido", "Nome da empresa: " + e.getNome(), m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), 
									e.getCdpessoa(), e, SinedDateUtils.currentDate(), m, Boolean.FALSE);
					Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, e.getCdpessoa());
					
					if (lastAvisoDate != null) {
						avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, e.getCdpessoa(), lastAvisoDate);
						
						List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
						
						if (SinedUtil.isListNotEmpty(listaUsuario)) {
							avisoService.salvarAvisos(aviso, listaUsuario, false);
						}
					} else {
						avisoList.add(aviso);
					}
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}	
	
	private List<Registro1010> createRegistro1010(SpedpiscofinsFiltro filtro){
		List<Registro1010> listaRegistro = new ArrayList<Registro1010>();
		List<ProcessoReferenciado> listaProcessoReferenciado = processoReferenciadoService.findForSpedPisconfinsReg1010(filtro);
		
		for(ProcessoReferenciado pr : listaProcessoReferenciado){
			Registro1010 registro1010 = new Registro1010();
			registro1010.setReg(Registro1010.REG);
			registro1010.setNum_proc(Integer.toString(pr.getNumeroProcessoJudicial()));
			registro1010.setId_vara(Integer.toString(pr.getIdVara()));
			registro1010.setId_sec_jud(Integer.toString(pr.getIdSecaoJudiciaria()));
			registro1010.setDesc_dec_jud(pr.getDescricao());
			registro1010.setDt_sent_jud(pr.getDataSentenca());
			if(pr.getAcaoJudicialNatureza()!=null && pr.getAcaoJudicialNatureza().getCodigo() !=null){
			registro1010.setInd_nat_acao(pr.getAcaoJudicialNatureza().getCodigo());
			}
			registro1010.setListaRegistro1011(this.createRegistro1011(filtro));
			listaRegistro.add(registro1010);
			filtro.getApoio().addRegistros(Registro1010.REG);	
		}
		return listaRegistro;
		
	}
	
	private List<Registro1011> createRegistro1011(SpedpiscofinsFiltro filtro){
		List<Registro1011> listaRegistro = new ArrayList<Registro1011>();
		List<ProcessoReferenciado> listaProcessoReferenciado = processoReferenciadoService.findForSpedPisconfinsReg1010(filtro);
		for(ProcessoReferenciado pr : listaProcessoReferenciado){
			for(ProcessoReferenciadoDocumento prd : pr.getListaProcessoReferenciadoDocumento()){
				Registro1011 registro1011 = new Registro1011();
				registro1011.setReg(Registro1011.REG);
			//	registro1011.setReg_ref("C170");
				if(prd.getNotaFiscalProduto() !=null && prd.getNotaFiscalProduto().getListaItens() != null){
					for(Notafiscalprodutoitem nfpi :prd.getNotaFiscalProduto().getListaItens() ){
						registro1011.setChave_doc(nfpi.getChaveacessoexportacao());
					}
				}
				registro1011.setDt_oper(prd.getDataOperacao());
				registro1011.setVl_oper(prd.getValorOperacao().getValue().doubleValue());
				if(prd.getCstPis()!=null){
					registro1011.setCst_pis(prd.getCstPis().getCdnfe());
				}
				registro1011.setAliq_cofins(prd.getAliqCofins());
				if(prd.getCstCofins()!=null){
					registro1011.setCst_cofins(prd.getCstCofins().getCdnfe());
				}
				registro1011.setAliq_pis(prd.getAliqCofins());
				if(prd.getCstPisSuspensa()!=null){
					registro1011.setCst_pis_susp(prd.getCstPisSuspensa().getCdnfe());
				}
				if(prd.getMaterial()!=null){
					if(prd.getNotaFiscalProduto() != null){
						registro1011.setCod_item(filtro.getApoio().addProdutoServico(prd.getMaterial().getCdmaterial(), prd.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), pr.getEmpresa(), filtro.isCnpjEmpresaFiltro())));
					}else {
						registro1011.setCod_item(filtro.getApoio().addProdutoServico(prd.getMaterial().getCdmaterial(), prd.getMaterial().getIdentificacaoOuCdmaterial(), getCNPJEmpresa(filtro.getEmpresa(), pr.getEmpresa(), filtro.isCnpjEmpresaFiltro())));
					}
				}
				Integer cdpessoa =0;
				if(prd.getParticipante()!=null){
				cdpessoa = prd.getParticipante().getCdpessoa();
				}
				String codPart = "";
				if(prd.getNotaFiscalProduto() != null){
					if(prd.getNotaFiscalProduto().getEnderecoCliente() != null && 
							prd.getNotaFiscalProduto().getEnderecoCliente().getInscricaoestadual() != null &&
							!"".equals(prd.getNotaFiscalProduto().getEnderecoCliente().getInscricaoestadual())){
						codPart = prd.getParticipante() != null ? filtro.getApoio().addParticipante(cdpessoa, pr.getEmpresa().getCnpj().getValue(), prd.getNotaFiscalProduto().getEnderecoCliente().getCdendereco(), prd.getParticipante().getCpfOuCnpjValue()) : null;
						codPart = codPart + "-" + prd.getNotaFiscalProduto().getEnderecoCliente().getCdendereco();
					} else {
						codPart = prd.getParticipante() != null ? filtro.getApoio().addParticipante(cdpessoa, pr.getEmpresa().getCnpj().getValue(), null, prd.getParticipante().getCpfOuCnpjValue()) : null;
					} 
					registro1011.setCod_part(codPart);
				}else{
					registro1011.setCod_part(prd.getEntradaFiscal().getFornecedor() != null ? filtro.getApoio().addParticipante(prd.getEntradaFiscal().getFornecedor().getCdpessoa(), pr.getEmpresa().getCnpj().getValue(), null, prd.getEntradaFiscal().getFornecedor().getCpfOuCnpjValue()) : null);
				}
				registro1011.setVl_bc_pis_susp(prd.getBcPisSuspenso());
				registro1011.setAliq_pis_susp(prd.getAliqPisSuspenso());
				registro1011.setVl_pis_susp(prd.getVlrPisSuspenso());
				registro1011.setVl_bc_cofins_susp(prd.getBcCofinsSuspenso());
				registro1011.setAliq_cofins_susp(prd.getAliqCofinsSuspenso());
				registro1011.setVl_cofins_susp(prd.getVlrCofinsSuspenso());
				if(prd.getCstCofinsSuspenso()!=null){
					registro1011.setCst_cofins_susp(prd.getCstCofinsSuspenso().getCdnfe());
				}
				registro1011.setDesc_doc_oper(prd.getDescricaoDocumento());
				if(prd.getContaContabil()!=null && prd.getContaContabil().getVcontacontabil() !=null){
					registro1011.setCod_cta(prd.getContaContabil().getVcontacontabil().getIdentificador());
				}
				registro1011.setVl_bc_cofins(prd.getVlBcCofins().doubleValue());
				registro1011.setVl_bc_pis(prd.getVlBcPis().doubleValue());
				registro1011.setVl_pis(prd.getVlPis().doubleValue());
				registro1011.setVl_cofins(prd.getVlCofins().doubleValue());
				listaRegistro.add(registro1011);
				filtro.getApoio().addRegistros(Registro1011.REG);	
			}
		}
		return listaRegistro;
	}
	
	
	private void verificaC170Pata1011(SPEDPiscofins spedPiscofins) {
		if(spedPiscofins.getRegistro0000().getRegistroC001()!=null){
			if(spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010() !=null){
				for(RegistroC010 registroC010 : spedPiscofins.getRegistro0000().getRegistroC001().getListaRegistroC010()){
					if(registroC010.getListaRegistroC100() != null ){
						for (RegistroC100 registroC100 : registroC010.getListaRegistroC100()){
							if(registroC100.getListaRegistroC170() !=null){
								for(RegistroC170 registroC170 : registroC100.getListaRegistroC170()){
									if(registroC170.getAliq_cofins()!=null || registroC170.getAliq_pis() !=null|| registroC170.getCst_cofins()!=null|| registroC170.getCst_pis()!=null|| registroC170.getVl_bc_cofins()!=null|| registroC170.getVl_bc_pis() !=null|| registroC170.getVl_cofins()!=null || registroC170.getVl_pis()!=null){
										if(spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010() != null){
											for(Registro1010 registro1010 : spedPiscofins.getRegistro0000().getRegistro1001().getListaRegistro1010()){
												if(registro1010.getListaRegistro1011() !=null){
													for(Registro1011 registro1011 : registro1010.getListaRegistro1011() ){
														registro1011.setReg_ref("C170");
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
