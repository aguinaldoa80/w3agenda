package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Registroatividadehora;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.dao.RegistroatividadehoraDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean.ConfirmacaoRegistroatividadehoraItemBean;
import br.com.linkcom.sined.util.SinedDateUtils;

public class RegistroatividadehoraService extends GenericService<Registroatividadehora>{

	private RegistroatividadehoraDAO registroatividadehoraDAO;
	
	public void setregistroatividadehoraDAO(RegistroatividadehoraDAO registroatividadehoraDAO) {
		this.registroatividadehoraDAO = registroatividadehoraDAO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RegistroatividadehoraDAO#findByUsuarioRequisicao(Colaborador colaborador, Requisicao requisicao)
	*
	* @param colaborador
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public Registroatividadehora findByUsuario(Colaborador colaborador) {
		return registroatividadehoraDAO.findByUsuarioRequisicao(colaborador, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param colaborador
	* @param requisicao
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public Registroatividadehora findByUsuarioRequisicao(Colaborador colaborador, Requisicao requisicao) {
		return registroatividadehoraDAO.findByUsuarioRequisicao(colaborador, requisicao);
	}
	
	/**
	* M�todo que cria a lista de registro hora
	*
	* @param atividadetipo
	* @param inicio
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public List<ConfirmacaoRegistroatividadehoraItemBean> makeListaRegistroatividadehora(Atividadetipo atividadetipo, Timestamp inicio) {
		Date dataInicio = new Date(inicio.getTime());
		Hora horaInicio = new Hora(inicio.getTime());
		
		Date dataFim = new Date(System.currentTimeMillis());
		Hora horaFim = new Hora(System.currentTimeMillis());
		
		List<ConfirmacaoRegistroatividadehoraItemBean> listaItem = new ArrayList<ConfirmacaoRegistroatividadehoraItemBean>();
		ConfirmacaoRegistroatividadehoraItemBean bean;
		
		int dias = SinedDateUtils.diferencaDias(dataFim, dataInicio);
		Date data = dataInicio;
		
		for (int i = dias; i >= 0; i--) {
			bean = new ConfirmacaoRegistroatividadehoraItemBean();
			bean.setData(data);
			bean.setInicio(new Hora("00:00"));
			bean.setFim(new Hora("23:59"));
			bean.setAtividadetipo(atividadetipo);
			
			if(i == dias){
				bean.setInicio(horaInicio);
			}
			
			if(i == 0){
				bean.setFim(horaFim);
			}
			
			listaItem.add(bean);
			
			data = SinedDateUtils.incrementDate(data, 1, Calendar.DAY_OF_MONTH);
		}
		
		return listaItem;
	}
}
