package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Dependenciacargo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DependenciacargoService extends GenericService<Dependenciacargo>{
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos dispon�veis atrav�s do nome do cargo
	 *
	 * @see br.com.linkcom.sined.geral.service.DependenciacargoService#ordenaListaForFlex(List)
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargo> ordenaListaDepCargoDispForFlex(List<Dependenciacargo> listaDependenciaCargo){
		return ordenaListaForFlex(listaDependenciaCargo);
	}	
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos selecionados atrav�s do nome do cargo
	 *
	 * @see br.com.linkcom.sined.geral.service.DependenciacargoService#ordenaListaForFlex(List)
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargo> ordenaListaDepCargoSelForFlex(List<Dependenciacargo> listaDependenciaCargo){
		return ordenaListaForFlex(listaDependenciaCargo);
	}	
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos atrav�s do nome do cargo
	 *
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargo> ordenaListaForFlex(List<Dependenciacargo> listaDependenciaCargo){
		if (listaDependenciaCargo != null) {
			Collections.sort(listaDependenciaCargo, new Comparator<Dependenciacargo>() {
			    public int compare(Dependenciacargo c1, Dependenciacargo c2) {
			        return c1.getCargo().getNome().compareTo(c2.getCargo().getNome());
			    }
			});
		}
		return listaDependenciaCargo;
	}	
	
}
