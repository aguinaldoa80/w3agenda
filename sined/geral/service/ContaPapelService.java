package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contapapel;
import br.com.linkcom.sined.geral.dao.ContaPapelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContaPapelService extends GenericService<Contapapel>{

	private ContaPapelDAO contaPapelDAO;
	
	public void setContaPapelDAO(ContaPapelDAO contaPapelDAO) {
		this.contaPapelDAO = contaPapelDAO;
	}
	
	public void deleteListContaPapel(String whereIn, Conta conta){
		contaPapelDAO.deleteListContaPapel(whereIn, conta);
	}

	public List<Contapapel> findByConta(Conta conta) {
		return contaPapelDAO.findByConta(conta);
	}
}
