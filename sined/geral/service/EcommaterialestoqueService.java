package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterialestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EcommaterialestoqueService extends GenericService<Ecommaterialestoque>{
	
	private EcommaterialestoqueDAO ecommaterialestoqueDAO;
	
	public void setEcommaterialestoqueDAO(
			EcommaterialestoqueDAO ecommaterialestoqueDAO) {
		this.ecommaterialestoqueDAO = ecommaterialestoqueDAO;
	}
	
	/* singleton */
	private static EcommaterialestoqueService instance;
	public static EcommaterialestoqueService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EcommaterialestoqueService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO#primeiroFilaSincronizacao()
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param currentTimestamp 
	 * @since 18/07/2013
	 */
	public Ecommaterialestoque primeiroFilaSincronizacao(Timestamp currentTimestamp) {
		return ecommaterialestoqueDAO.primeiroFilaSincronizacao(currentTimestamp);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO#loadEcommaterialestoque(Ecommaterialestoque ecommaterialestoque)
	*
	* @param ecommaterialestoque
	* @return
	* @since 24/04/2015
	* @author Luiz Fernando
	*/
	public Ecommaterialestoque loadEcommaterialestoque(Ecommaterialestoque ecommaterialestoque) {
		return ecommaterialestoqueDAO.loadEcommaterialestoque(ecommaterialestoque);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO#updateSincronizado(Ecommaterialestoque ecommaterialestoque)
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateSincronizado(Ecommaterialestoque ecommaterialestoque) {
		ecommaterialestoqueDAO.updateSincronizado(ecommaterialestoque);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO#updateFinalFila(Ecommaterialestoque ecommaterialestoque)
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateFinalFila(Ecommaterialestoque ecommaterialestoque) {
		ecommaterialestoqueDAO.updateFinalFila(ecommaterialestoque);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialestoqueDAO#updateIdentificador(Material material, String identificador)
	 *
	 * @param material
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 30/07/2013
	 */
	public void updateIdentificador(Material material, String identificador, Configuracaoecom configuracaoecom) {
		ecommaterialestoqueDAO.updateIdentificador(material, identificador, configuracaoecom);
	}
}
