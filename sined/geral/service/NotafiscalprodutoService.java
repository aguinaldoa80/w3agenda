package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.BooleanUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.ImportarXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXMLDuplicata;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXMLItem;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.*;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoDIFALInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.FreteVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.NotaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.OutrosVO;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Emporiumvendaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ImpostoEcfEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetamaterialNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.InformacaoAdicionalProdutoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialInfoadicional;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ParcelasCobrancaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.PedidovendamaterialReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.SituacaoNotaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotafiscalprodutoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxCalculaTotalValorNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.Aux_imposto;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeLegendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.util.ImportacaoXMLEstadoUtil;
import br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusContaProcess;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.InformacaoContribuinteNfeBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.NotaItemBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.FaturarColetaItemBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ColetaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GrupoTributacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.NaturezaOperacaoBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.ImpostoUtil;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.util.rest.TLSSocketConnectionFactory;
import br.com.linkcom.utils.LkMdfeUtil;
import br.com.linkcom.utils.NFCeUtil;

import com.google.gson.Gson;

public class NotafiscalprodutoService extends GenericService<Notafiscalproduto> {
	
	private static NotafiscalprodutoService instance;
	public static NotafiscalprodutoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(NotafiscalprodutoService.class);
		}
		return instance;
	}
	
	private NotafiscalprodutoDAO notafiscalprodutoDAO;
	private EnderecoService enderecoService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private VendahistoricoService vendahistoricoService;
	private EmpresaService empresaService;
	private NotaVendaService notavendaService;
	private DocumentoorigemService documentoorigemService;
	private DocumentoService documentoService;
	private FornecedorService fornecedorService;
	private ClienteService clienteService;
	private VendaService vendaService;
	private VendamaterialService vendamaterialService;
	private ParametrogeralService parametrogeralService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private ArquivonfnotaService arquivonfnotaService;
	private EntregaService entregaService;
	private EntradafiscalService entradafiscalService;
	private ConfiguracaonfeService configuracaonfeService;
	private VendapagamentoService vendapagamentoService;
	private DocumentohistoricoService documentohistoricoService;
	private RomaneioService romaneioService;
	private ColaboradorService colaboradorService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ExpedicaoitemService expedicaoitemService;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private CfopescopoService cfopescopoService;
	private VendamaterialmestreService vendamaterialmestreService;
	private GrupotributacaoService grupotributacaoService;
	private CfopService cfopService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private MateriallegendaService materiallegendaService;
	private NotaService notaService;
	private ExpedicaoService expedicaoService;
	private ExpedicaohistoricoService expedicaohistoricoService;
	private NotaHistoricoService notaHistoricoService;
	private EntregamaterialService entregamaterialService;
	private CategoriaService categoriaService;
	private NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService;
	private RequisicaoService requisicaoService;
	private TabelaimpostoService tabelaimpostoService;
	private LocalarmazenagemService localarmazenagemService;
	private ColetaService coletaService;
	private ReportTemplateService reportTemplateService;
	private PrazopagamentoService prazopagamentoService;
	private ContaService contaService;
	private ColaboradorcargoService colaboradorcargoService;
	private NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	private EntregadocumentoColetaService entregadocumentoColetaService;
	private PedidovendatipoService pedidovendatipoService;
	private TelefoneService telefoneService;
	private NotaDocumentoService notaDocumentoService;
	private ContacarteiraService contacarteiraService;
	private PedidovendaService pedidovendaService;
	private ColetaMaterialService coletaMaterialService;
	private ProjetoService projetoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ContratomaterialService contratomaterialService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private MunicipioService municipioService;
	private ArquivonfService arquivonfService;
	private ArquivoService arquivoService;
	private ItemlistaservicoService itemlistaservicoService;
	private UfService ufService;
	private NcmcapituloService ncmcapituloService;
	private PessoaService pessoaService;
	private EmporiumvendahistoricoService emporiumvendahistoricoService;
	private FormulacustoService formulaCustoService;
	private ArquivotefService arquivotefService;
	
	public void setArquivotefService(ArquivotefService arquivotefService) {this.arquivotefService = arquivotefService;}
	public void setTabelaimpostoService(TabelaimpostoService tabelaimpostoService) {this.tabelaimpostoService = tabelaimpostoService;}
	public void setNotafiscalprodutoautorizacaoService(NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService) {this.notafiscalprodutoautorizacaoService = notafiscalprodutoautorizacaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setMateriallegendaService(MateriallegendaService materiallegendaService) {this.materiallegendaService = materiallegendaService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setExpedicaoitemService(ExpedicaoitemService expedicaoitemService) {this.expedicaoitemService = expedicaoitemService;}
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotafiscalprodutoduplicataService(NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setNotafiscalprodutoDAO(NotafiscalprodutoDAO notafiscalprodutoDAO) {this.notafiscalprodutoDAO = notafiscalprodutoDAO;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setNotavendaService(NotaVendaService notavendaService) {this.notavendaService = notavendaService;}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {this.naturezaoperacaocfopService = naturezaoperacaocfopService;}
	public void setCfopescopoService(CfopescopoService cfopescopoService) {this.cfopescopoService = cfopescopoService;}
	public void setVendamaterialmestreService(VendamaterialmestreService vendamaterialmestreService) {this.vendamaterialmestreService = vendamaterialmestreService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {this.expedicaoService = expedicaoService;}
	public void setExpedicaohistoricoService(ExpedicaohistoricoService expedicaohistoricoService) {this.expedicaohistoricoService = expedicaohistoricoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;}
	public void setEntregadocumentoColetaService(EntregadocumentoColetaService entregadocumentoColetaService) {this.entregadocumentoColetaService = entregadocumentoColetaService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {this.contratomaterialService = contratomaterialService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setItemlistaservicoService(ItemlistaservicoService itemlistaservicoService) {this.itemlistaservicoService = itemlistaservicoService;}
	public void setNcmcapituloService(NcmcapituloService ncmcapituloService) {this.ncmcapituloService = ncmcapituloService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setEmporiumvendahistoricoService(EmporiumvendahistoricoService emporiumvendahistoricoService) {this.emporiumvendahistoricoService = emporiumvendahistoricoService;}
	public void setFormulaCustoService(FormulacustoService formulaCustoService) {this.formulaCustoService = formulaCustoService;}

	@Override
	public void saveOrUpdate(final Notafiscalproduto bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				List<Notafiscalprodutoitem> lista = bean.getListaItens();
				bean.setListaItens(null);
				
				saveOrUpdateNoUseTransaction(bean);
				
				String string = "";
				for (Notafiscalprodutoitem item : lista) {
					if (item.getCdnotafiscalprodutoitem() != null) {
						string += item.getCdnotafiscalprodutoitem();
						string += ",";
					}
				}
				
				if (!string.equals("")) {
					string = string.substring(0, string.length()-1);
					notafiscalprodutoitemService.deleteNaoExistentes(bean,string);
				}
				
				for (Notafiscalprodutoitem item : lista) {
					item.setNotafiscalproduto(bean);
					notafiscalprodutoitemService.saveOrUpdateNoUseTransaction(item);
				}
				bean.setListaItens(lista);
				
				return null;
			}
		});
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. Carrega as informa��es necess�rias para
	 * calcular o total de cada nota fiscal de Produto.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#carregarInfoParaCalcularTotal(List)
	 * @param listaNfProduto
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarInfoParaCalcularTotal(List<Nota> listaNfProduto) {
		return notafiscalprodutoDAO.carregarInfoParaCalcularTotal(listaNfProduto);
	}
	
	public Nota carregarInfoCalcularTotal(Nota nota) {
		List<Nota> lista = new ArrayList<Nota>();
		lista.add(nota);
		return notafiscalprodutoDAO.carregarInfoParaCalcularTotal(lista).get(0);
	}
	
	public List<Notafiscalproduto> carregarInfoParaCalcularTotalProduto(String whereIn, String orderBy, Boolean asc) {
		return notafiscalprodutoDAO.carregarInfoParaCalcularTotalProduto(whereIn,orderBy,asc);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#loadForNfe
	 *
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Notafiscalproduto loadForNfe(Integer cdNota) {
		return notafiscalprodutoDAO.loadForNfe(cdNota);
	}
	
	public Report danfe(Notafiscalproduto nf){
		DanfeBean danfe = converterNotafiscalprodutoEmDanfeBean(nf);
		
		String modelo = parametrogeralService.buscaValorPorNome(Parametrogeral.MODELO_DA_DANFE);
		if(nf.getModeloDocumentoFiscalEnum() != null && nf.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
			modelo = parametrogeralService.buscaValorPorNome(Parametrogeral.MODELO_DA_DANFE_NFCE);
			if(org.apache.commons.lang.StringUtils.isBlank(modelo)) {
				modelo = "nfce";
			}
		}
		return danfe.makeReport(modelo);
	}
	
	/**
	 * 
	 * Seta as informa��es de Notafiscalproduto em um DanfeBean
	 * 
	 * @param nf
	 * @param arquivonfnota
	 * @return DanfeBean
	 * @author Rodrigo Freitas (Extra�do em m�todo separado por Rafael Patr�cio)
	 */
	public DanfeBean converterNotafiscalprodutoEmDanfeBean(Notafiscalproduto nf) {
		
		DanfeItemBean item;
		List<Notafiscalprodutoduplicata> listaDuplicatas;
		DanfeBean danfe = new DanfeBean();
		
		Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(nf);
		if (arquivonfnota == null && !nf.getNotaStatus().equals(NotaStatus.EMITIDA) && !nf.getNotaStatus().equals(NotaStatus.EM_ESPERA)){
			throw new SinedException("N�o foi poss�vel imprimir a DANFE. Lote de Nota Fiscal Eletr�nica n�o encontrado. " + 
									new StringUtils().getLabelAndText("Nota: ", nf.getNumero()));
		}
		
		setInformacaoComplementarDanfe(danfe, nf);
		danfe.setInfocomplementarfisco(nf.getInfoadicionalfisco() != null ? nf.getInfoadicionalfisco() : "");
		
		if(arquivonfnota != null && 
				arquivonfnota.getArquivonf() != null &&
				arquivonfnota.getArquivonf().getTipocontigencia() != null){
			
			StringBuilder infoComplemetarFisco = new StringBuilder(danfe.getInfocomplementarfisco());
			
			infoComplemetarFisco.append(arquivonfnota.getArquivonf().getTipocontigencia().getNome());
			if(arquivonfnota.getArquivonf().getDtentradacontigencia() != null){
				infoComplemetarFisco.append(" - Data de entrada em contig�ncia: ").append(SinedDateUtils.toString(arquivonfnota.getArquivonf().getDtentradacontigencia(), "dd/MM/yyyy HH:mm:ss"));
			}
			if(arquivonfnota.getArquivonf().getJustificativacontigencia() != null){
				infoComplemetarFisco.append(" - Justificativa: ").append(arquivonfnota.getArquivonf().getJustificativacontigencia());
			}
			
			danfe.setInfocomplementarfisco(infoComplemetarFisco.toString());
		}
		
		Naturezaoperacao naturezaoperacao = nf.getNaturezaoperacao();
		Empresa empresa = nf.getEmpresa();
		empresaService.setInformacoesPropriedadeRural(empresa);
		danfe.setEmitenteRazao(empresa != null && empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() != null ? empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa().toUpperCase() : "");
		
		danfe.setNumeroNota(nf.getNumero() != null && !nf.getNumero().equals("") ? Integer.parseInt(nf.getNumero()) : null);
		danfe.setNumeroPaginas(1);
		danfe.setEmitenteInscricao(empresa != null && empresa.getInscricaoestadual() != null ? empresa.getInscricaoestadual() : "");
		danfe.setEmitenteCnpj(empresa != null && empresa.getCpfCnpjProprietarioPrincipalOuEmpresa() != null ? empresa.getCpfCnpjProprietarioPrincipalOuEmpresa().toString() : "");
		danfe.setNatOper(nf.getNaturezaoperacao() != null ? nf.getNaturezaoperacao().getDescricao().toUpperCase() : "");
		danfe.setNotaStatus(nf.getNotaStatus().getCdNotaStatus().toString());

		
		
		Configuracaonfe configuracaoNfe = arquivonfnota != null && arquivonfnota.getArquivonf() != null ? configuracaonfeService.loadForEntrada(arquivonfnota.getArquivonf().getConfiguracaonfe()) : null;
		
		if (arquivonfnota != null && org.apache.commons.lang.StringUtils.isNotEmpty(arquivonfnota.getQrCode())) {
			try {
				danfe.setQrCode(LkMdfeUtil.gerarByteQrCode(arquivonfnota.getQrCode(), 500, 500));
				danfe.setUrl(NFCeUtil.getUrlConsulta(configuracaoNfe.getUf().getSigla(), configuracaoNfe.getPrefixowebservice().name().endsWith("PROD")));
			} catch (Exception e) {
			}
		}
		
		if(arquivonfnota != null){
			danfe.setProtocolo(arquivonfnota.getProtocolonfe() + (arquivonfnota.getDtprocessamento() != null ? (" - " + SinedDateUtils.toString(arquivonfnota.getDtprocessamento(), "dd/MM/yyyy HH:mm:ss")) : ""));
			danfe.setChaveAcesso(arquivonfnota.getChaveacesso());
			danfe.setSerie(arquivonfnota.getNota() != null && arquivonfnota.getNota().getSerienfe() != null ? arquivonfnota.getNota().getSerienfe() : null);
		}
		Endereco enderecoEmpresa = null;
		if(configuracaoNfe != null){
			enderecoEmpresa = configuracaoNfe.getEndereco();
		}else if(empresa != null){
			enderecoEmpresa = enderecoService.getEnderecoPrincipalPessoa(empresa);
		}
		
		if(enderecoEmpresa != null){
			danfe.setEmitenteEndereco(enderecoEmpresa.getLogradouroCompleto().toUpperCase());
			danfe.setEmitenteBairro(enderecoEmpresa.getBairro().toUpperCase());
			danfe.setEmitenteMunicipio(enderecoEmpresa.getMunicipio().getNome().toUpperCase());
			danfe.setEmitenteUf(enderecoEmpresa.getMunicipio().getUf().getSigla().toUpperCase());
			danfe.setEmitenteCep(enderecoEmpresa.getCep().toString());
		}
		if(configuracaoNfe != null){
			danfe.setEmitenteTelefone(configuracaoNfe.getTelefone());
			danfe.setEmitenteInscricaoST(configuracaoNfe.getInscricaoestadualst() != null && Boolean.TRUE.equals(nf.getRetencaoICMSSTUFdestino()) ? configuracaoNfe.getInscricaoestadualst() : "");
		}else{
			Telefone telefone = telefoneService.loadTelefoneUnicoByPessoa(empresa);
			if(telefone!=null){
				danfe.setEmitenteTelefone(telefone.getTelefone());
			}
			danfe.setEmitenteInscricaoST(empresa.getInscricaoestadualst() != null? empresa.getInscricaoestadualst() : "");
		}
		
		danfe.setEntradaSaida(nf.getTipooperacaonota().getValue());
		
		danfe.setLogo(SinedUtil.getLogo(empresa));
		
		if(nf.getEnderecoavulso() != null && nf.getEnderecoavulso() && nf.getEndereconota() != null && 
				nf.getEndereconota().getCdendereco() != null){
			nf.setEnderecoCliente(enderecoService.loadEnderecoNota(nf.getEndereconota()));
		}else if(nf.getEnderecoCliente() != null){
			nf.setEnderecoCliente(enderecoService.loadEndereco(nf.getEnderecoCliente()));
		}
		
		if(nf.getEnderecoCliente() != null){
			boolean exterior = false;
			if(nf.getEnderecoCliente().getPais() != null && 
					nf.getEnderecoCliente().getPais().getNome() != null &&
					!nf.getEnderecoCliente().getPais().getNome().trim().toUpperCase().equals("BRASIL")){
				exterior = true;
			}
			
			if(exterior){
				danfe.setMunicipio("Exterior");
				danfe.setUf("EX");
			} else {
				if(nf.getEnderecoCliente().getMunicipio() != null){
					danfe.setUf(nf.getEnderecoCliente().getMunicipio().getUf().getSigla().toUpperCase());
					danfe.setMunicipio(nf.getEnderecoCliente().getMunicipio().getNome().toUpperCase());
				}
			}
			
			danfe.setBairro(nf.getEnderecoCliente().getBairro() != null ? nf.getEnderecoCliente().getBairro().toUpperCase() : null);
			danfe.setTelefone(nf.getTelefoneCliente() != null ? nf.getTelefoneCliente() : "");
			danfe.setDataEmissao(SinedDateUtils.toString(nf.getDtEmissao()));
			danfe.setEndereco(nf.getEnderecoCliente().getLogradouroCompleto().toUpperCase());
			danfe.setEnderecoCompleto(nf.getEnderecoCliente().getLogradouroCompleto().toUpperCase());
			if(danfe.getBairro()!=null){
				danfe.setEnderecoCompleto(danfe.getEnderecoCompleto()+" - "+danfe.getBairro());
			}
			if(danfe.getMunicipio()!=null){
				danfe.setEnderecoCompleto(danfe.getEnderecoCompleto()+" - "+danfe.getMunicipio());
			}
			if(danfe.getUf()!=null){
				danfe.setEnderecoCompleto(danfe.getEnderecoCompleto()+" - "+danfe.getUf());
			}
			danfe.setCep(nf.getEnderecoCliente().getCep() != null ? nf.getEnderecoCliente().getCep().toString() : "");
		}
		
		String inscricaoEstadualEndereco = nf.getEnderecoCliente() != null && nf.getEnderecoCliente().getInscricaoestadual() != null ? nf.getEnderecoCliente().getInscricaoestadual().toUpperCase() : "";
		String inscricaoEstadualCliente = nf.getCliente().getInscricaoestadual() != null ? nf.getCliente().getInscricaoestadual().toUpperCase() : "";
		danfe.setInscricaoEstadual(inscricaoEstadualEndereco != null && !inscricaoEstadualEndereco.trim().equals("") ? inscricaoEstadualEndereco : inscricaoEstadualCliente);
		danfe.setDataSaida(nf.getDatahorasaidaentrada() != null ? SinedDateUtils.toString(nf.getDatahorasaidaentrada(), "dd/MM/yyyy") : "");
		danfe.setHoraSaida(nf.getDatahorasaidaentrada() != null ? SinedDateUtils.toString(nf.getDatahorasaidaentrada(), "HH:mm") : "");

		boolean ambienteProducao = configuracaoNfe == null || !configuracaoNfe.getPrefixowebservice().name().endsWith("_HOM");
		String nomeHomologacao = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
		if(ambienteProducao){
			danfe.setRazaoSocial(nf.getRazaosocialalternativaOrRazaosocial().toUpperCase());
		} else {
			danfe.setRazaoSocial(nomeHomologacao);
		}
		
		String cpfcnpj = nf.getCliente().getCpfOuCnpj();
		danfe.setCnpj(cpfcnpj != null ? cpfcnpj : "");
		
		danfe.setBaseCalculoIcms(nf.getValorbcicms() != null ? nf.getValorbcicms().getValue().doubleValue() : 0d);
		danfe.setValorIcms(nf.getValoricms() != null ? nf.getValoricms().getValue().doubleValue() : 0d);
		danfe.setValorTotalProdutos(nf.getValorprodutos() != null ? nf.getValorprodutos().getValue().doubleValue() : 0d);
		danfe.setBaseCalculoIcmsSt(nf.getValorbcicmsst() != null ? nf.getValorbcicmsst().getValue().doubleValue() : 0d);
		danfe.setDespesasAcessorias(nf.getOutrasdespesas() != null ? nf.getOutrasdespesas().getValue().doubleValue() : 0d);
		danfe.setValorTotalNota(nf.getValor() != null ? nf.getValor().getValue().doubleValue() : 0d);
		danfe.setValorIpi(nf.getValoripi() != null ? nf.getValoripi().getValue().doubleValue() : 0d);
		danfe.setValorSeguro(nf.getValorseguro() != null ? nf.getValorseguro().getValue().doubleValue() : 0d);
		danfe.setValorFrete(nf.getValorfrete() != null ? nf.getValorfrete().getValue().doubleValue() : 0d);
		danfe.setValorDesoneracao(nf.getValordesoneracao() != null ? nf.getValordesoneracao().getValue().doubleValue() : 0d);
		danfe.setDesconto(nf.getValordesconto() != null ? nf.getValordesconto().getValue().doubleValue() : 0d);
		danfe.setValorIcmsSt(nf.getValoricmsst() != null ? nf.getValoricmsst().getValue().doubleValue() : 0d);
		danfe.setValorTotalTributos(nf.getValortotaltributos() != null ? nf.getValortotaltributos().getValue().doubleValue() : 0d);
		danfe.setValorTotalImpostoFederal(nf.getValorTotalImpostoFederal() != null ? nf.getValorTotalImpostoFederal().getValue().doubleValue() : 0d);
		danfe.setValorTotalImpostoEstadual(nf.getValorTotalImpostoEstadual() != null ? nf.getValorTotalImpostoEstadual().getValue().doubleValue() : 0d);
		danfe.setValorTotalImpostoMunicipal(nf.getValorTotalImpostoMunicipal() != null ? nf.getValorTotalImpostoMunicipal().getValue().doubleValue() : 0d);
		
		danfe.setInscricaoMunicipal(configuracaoNfe != null && configuracaoNfe.getInscricaomunicipal() != null ? configuracaoNfe.getInscricaomunicipal() : "");
		danfe.setInscricaoEstadualEmpresa(configuracaoNfe != null && configuracaoNfe.getInscricaoestadual() != null ? configuracaoNfe.getInscricaoestadual() : "");
		danfe.setValorTotalServicos(nf.getValorservicos() != null ? nf.getValorservicos().getValue().doubleValue() : 0d);
		danfe.setBaseCalculoIss(nf.getValorbciss() != null ? nf.getValorbciss().getValue().doubleValue() : 0d);
		danfe.setValorIss(nf.getValoriss() != null ? nf.getValoriss().getValue().doubleValue() : 0d);

		danfe.setTransportadorCodigoAntt(nf.getRntc() != null ? nf.getRntc() : "");
		danfe.setTransportadorPlaca(nf.getPlacaveiculo() != null ? nf.getPlacaveiculo().toUpperCase() : "");
		danfe.setTransportadorTipoFrete(nf.getResponsavelfrete() != null ? nf.getResponsavelfrete().getCdnfe() : null);
		
		danfe.setTransportadorQtde(nf.getQtdevolume());
		danfe.setTransportadorEspecie(nf.getEspvolume());
		danfe.setTransportadorMarca(nf.getMarcavolume());
		danfe.setTransportadorPesoBruto(nf.getPesobruto());
		danfe.setTransportadorPesoLiquido(nf.getPesoliquido());
		danfe.setTransportadorNumeracao(nf.getNumvolume());
		
		danfe.setFormapagamento(nf.getFormapagamentonfe() != null ? nf.getFormapagamentonfe().getDescricao() : null);
		danfe.setSite(empresa.getSite());
		danfe.setEmail(empresa.getEmail());
		
		if(nf.getNumerofatura() != null && !nf.getNumerofatura().equals("")) danfe.setNumeroFatura(nf.getNumerofatura());
		if(nf.getValororiginalfatura() != null && nf.getValordescontofatura() != null && nf.getValordescontofatura().getValue().doubleValue() > 0) {
			danfe.setValorOriginalFatura(nf.getValororiginalfatura().getValue().doubleValue());
		}
		if(nf.getValordescontofatura() != null && nf.getValordescontofatura().getValue().doubleValue() > 0){
			danfe.setValorDescontoFatura(nf.getValordescontofatura().getValue().doubleValue());
		}
		if(nf.getValorliquidofatura() != null) {
			danfe.setValorLiquidoFatura(nf.getValorliquidofatura().getValue().doubleValue());
		}
		
		listaDuplicatas = notafiscalprodutoduplicataService.findByNotafiscalproduto(nf);
		
		if(listaDuplicatas != null && !listaDuplicatas.isEmpty()){
			if (listaDuplicatas.size()<=10){
				Notafiscalprodutoduplicata duplicata1 = listaDuplicatas.size() > 0 ?  listaDuplicatas.get(0) : null;
				Notafiscalprodutoduplicata duplicata2 = listaDuplicatas.size() > 1 ?  listaDuplicatas.get(1) : null;
				Notafiscalprodutoduplicata duplicata3 = listaDuplicatas.size() > 2 ?  listaDuplicatas.get(2) : null;
				Notafiscalprodutoduplicata duplicata4 = listaDuplicatas.size() > 3 ?  listaDuplicatas.get(3) : null;
				Notafiscalprodutoduplicata duplicata5 = listaDuplicatas.size() > 4 ?  listaDuplicatas.get(4) : null;
				Notafiscalprodutoduplicata duplicata6 = listaDuplicatas.size() > 5 ?  listaDuplicatas.get(5) : null;
				Notafiscalprodutoduplicata duplicata7 = listaDuplicatas.size() > 6 ?  listaDuplicatas.get(6) : null;
				Notafiscalprodutoduplicata duplicata8 = listaDuplicatas.size() > 7 ?  listaDuplicatas.get(7) : null;
				Notafiscalprodutoduplicata duplicata9 = listaDuplicatas.size() > 8 ?  listaDuplicatas.get(8) : null;
				Notafiscalprodutoduplicata duplicata10 = listaDuplicatas.size() > 9 ?  listaDuplicatas.get(9) : null;
				
				if(duplicata1 != null && duplicata1.getNumero() != null && !duplicata1.getNumero().equals("")) danfe.setNumeroDuplicata1(duplicata1.getNumero());
				if(duplicata1 != null && duplicata1.getDtvencimento() != null) danfe.setVencimentoDuplicata1(duplicata1.getDtvencimento());
				if(duplicata1 != null && duplicata1.getValor() != null) danfe.setValorDuplicata1(duplicata1.getValor().getValue().doubleValue());
				
				if(duplicata2 != null && duplicata2.getNumero() != null && !duplicata2.getNumero().equals("")) danfe.setNumeroDuplicata2(duplicata2.getNumero());
				if(duplicata2 != null && duplicata2.getDtvencimento() != null) danfe.setVencimentoDuplicata2(duplicata2.getDtvencimento());
				if(duplicata2 != null && duplicata2.getValor() != null) danfe.setValorDuplicata2(duplicata2.getValor().getValue().doubleValue());
				
				if(duplicata3 != null && duplicata3.getNumero() != null && !duplicata3.getNumero().equals("")) danfe.setNumeroDuplicata3(duplicata3.getNumero());
				if(duplicata3 != null && duplicata3.getDtvencimento() != null) danfe.setVencimentoDuplicata3(duplicata3.getDtvencimento());
				if(duplicata3 != null && duplicata3.getValor() != null) danfe.setValorDuplicata3(duplicata3.getValor().getValue().doubleValue());
				
				if(duplicata4 != null && duplicata4.getNumero() != null && !duplicata4.getNumero().equals("")) danfe.setNumeroDuplicata4(duplicata4.getNumero());
				if(duplicata4 != null && duplicata4.getDtvencimento() != null) danfe.setVencimentoDuplicata4(duplicata4.getDtvencimento());
				if(duplicata4 != null && duplicata4.getValor() != null) danfe.setValorDuplicata4(duplicata4.getValor().getValue().doubleValue());
				
				if(duplicata5 != null && duplicata5.getNumero() != null && !duplicata5.getNumero().equals("")) danfe.setNumeroDuplicata5(duplicata5.getNumero());
				if(duplicata5 != null && duplicata5.getDtvencimento() != null) danfe.setVencimentoDuplicata5(duplicata5.getDtvencimento());
				if(duplicata5 != null && duplicata5.getValor() != null) danfe.setValorDuplicata5(duplicata5.getValor().getValue().doubleValue());
				
				if(duplicata6 != null && duplicata6.getNumero() != null && !duplicata6.getNumero().equals("")) danfe.setNumeroDuplicata6(duplicata6.getNumero());
				if(duplicata6 != null && duplicata6.getDtvencimento() != null) danfe.setVencimentoDuplicata6(duplicata6.getDtvencimento());
				if(duplicata6 != null && duplicata6.getValor() != null) danfe.setValorDuplicata6(duplicata6.getValor().getValue().doubleValue());
				
				if(duplicata7 != null && duplicata7.getNumero() != null && !duplicata7.getNumero().equals("")) danfe.setNumeroDuplicata7(duplicata7.getNumero());
				if(duplicata7 != null && duplicata7.getDtvencimento() != null) danfe.setVencimentoDuplicata7(duplicata7.getDtvencimento());
				if(duplicata7 != null && duplicata7.getValor() != null) danfe.setValorDuplicata7(duplicata7.getValor().getValue().doubleValue());
				
				if(duplicata8 != null && duplicata8.getNumero() != null && !duplicata8.getNumero().equals("")) danfe.setNumeroDuplicata8(duplicata8.getNumero());
				if(duplicata8 != null && duplicata8.getDtvencimento() != null) danfe.setVencimentoDuplicata8(duplicata8.getDtvencimento());
				if(duplicata8 != null && duplicata8.getValor() != null) danfe.setValorDuplicata8(duplicata8.getValor().getValue().doubleValue());
				
				if(duplicata9 != null && duplicata9.getNumero() != null && !duplicata9.getNumero().equals("")) danfe.setNumeroDuplicata9(duplicata9.getNumero());
				if(duplicata9 != null && duplicata9.getDtvencimento() != null) danfe.setVencimentoDuplicata9(duplicata9.getDtvencimento());
				if(duplicata9 != null && duplicata9.getValor() != null) danfe.setValorDuplicata9(duplicata9.getValor().getValue().doubleValue());
				
				if(duplicata10 != null && duplicata10.getNumero() != null && !duplicata10.getNumero().equals("")) danfe.setNumeroDuplicata10(duplicata10.getNumero());
				if(duplicata10 != null && duplicata10.getDtvencimento() != null) danfe.setVencimentoDuplicata10(duplicata10.getDtvencimento());
				if(duplicata10 != null && duplicata10.getValor() != null) danfe.setValorDuplicata10(duplicata10.getValor().getValue().doubleValue());
			}else {
				String textoDuplicatas = "N� Duplicata-Vencimento-Valor: ";
				int total = 1;
				
				for (Notafiscalprodutoduplicata nfpd: listaDuplicatas){
					if (total++==21){
						break;
					}
					if (nfpd.getNumero()!=null && !nfpd.getNumero().trim().equals("")){
						textoDuplicatas += nfpd.getNumero();
					}
					
					textoDuplicatas += " - ";
					
					if (nfpd.getDtvencimento()!=null){
						textoDuplicatas += NeoFormater.getInstance().format(nfpd.getDtvencimento());
					}
					
					textoDuplicatas += " - ";
					
					if (nfpd.getValor()!=null){
						textoDuplicatas += nfpd.getValor().toString();
					}else {
						textoDuplicatas += "0,00";
					}
					
					textoDuplicatas += " / ";
				}
				
				textoDuplicatas = textoDuplicatas.substring(0, textoDuplicatas.length()-3);
				danfe.setTextoDuplicatas(textoDuplicatas);
			}
		}
		
		Fornecedor transportador = nf.getTransportador();
		if(transportador != null){
			danfe.setTransportadorRazao(transportador.getRazaosocial() != null && !transportador.getRazaosocial().equals("") ? transportador.getRazaosocial().toUpperCase() : transportador.getNome().toUpperCase());
			danfe.setTransportadorCnpj(transportador.getCpfOuCnpj());
			danfe.setTransportadorInscricao(transportador.getInscricaoestadual() != null ? transportador.getInscricaoestadual().toUpperCase() : "");
			
			if(nf.getEnderecotransportador() != null){
				nf.setEnderecotransportador(enderecoService.loadEndereco(nf.getEnderecotransportador()));
				
				danfe.setTransportadorEndereco(nf.getEnderecotransportador().getLogradouroCompleto().toUpperCase());
				danfe.setTransportadorUf(nf.getEnderecotransportador().getMunicipio() != null ? nf.getEnderecotransportador().getMunicipio().getUf().getSigla().toUpperCase() : "");
				danfe.setTransportadorMunicipio(nf.getEnderecotransportador().getMunicipio() != null ? nf.getEnderecotransportador().getMunicipio().getNome().toUpperCase() : "");
			}
		}
		
		String paramCabecalhoTipoTributacaoIcmsDanfe = "CST / CSON";
		String paramPrefixoTipoTributacaoIcmsDanfe = "FALSE";
		
		try{
			paramCabecalhoTipoTributacaoIcmsDanfe = parametrogeralService.getValorPorNome(Parametrogeral.CABECALHO_TIPO_TRIBUTACAO_DANFE);
			paramPrefixoTipoTributacaoIcmsDanfe = parametrogeralService.getValorPorNome(Parametrogeral.PREFIXO_TIPO_TRIBUTACAO_DANFE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		danfe.setCabecalhoTipoTributacaoIcms(paramCabecalhoTipoTributacaoIcmsDanfe);
		
		boolean exibirSTJaPaga = false;
		Money totalBCSTJaPaga = new Money();
		Money totalSTJaPaga = new Money();
		
		boolean tributadoIssqn = false;
		if(nf.getListaItens() != null){
			for (Notafiscalprodutoitem nfitem : nf.getListaItens()) {
				item = new DanfeItemBean();
				
				if(nfitem.getTributadoicms() == null || !nfitem.getTributadoicms()){
					tributadoIssqn = true;
				}
				
				if(paramPrefixoTipoTributacaoIcmsDanfe.equals("TRUE")){
					item.setSituacaoTributaria((nfitem.getOrigemproduto() != null ? nfitem.getOrigemproduto().getValue() : "") + "" + (nfitem.getTipocobrancaicms() != null ? nfitem.getTipocobrancaicms().getCdnfe() : ""));
				} else {
					item.setSituacaoTributaria(nfitem.getTipocobrancaicms() != null ? nfitem.getTipocobrancaicms().getCdnfe() : "");
				}
				
				String identificador = nfitem.getIdentificadorespecifico();
				if (notafiscalprodutoColetaService.existsColetaInNotaFiscalProduto(nf.getCdNota()) && 
						nf.getEmpresa() != null && BooleanUtils.isTrue(nf.getEmpresa().getConsiderarIdPneu()) &&
						nfitem.getPneu() != null && nfitem.getPneu().getCdpneu() != null) {
					item.setCodigo(nfitem.getPneu().getCdpneu().toString());
				} else if(identificador != null && !identificador.equals("")){
					item.setCodigo(identificador);
				} else {
					item.setCodigo(nfitem.getMaterial().getIdentificacaoOuCdmaterial());
				}
				
				if(nfitem.getVendamaterialmestre() != null && 
						org.apache.commons.lang.StringUtils.isNotEmpty(nfitem.getVendamaterialmestre().getNomealternativo())){
					item.setDescricao(nfitem.getVendamaterialmestre().getNomealternativo());
				}else if(nfitem.getMaterial().getNomenf() != null && !nfitem.getMaterial().getNomenf().equals("")){
					item.setDescricao(nfitem.getMaterial().getNomenf().toUpperCase());
				} else {
					item.setDescricao(nfitem.getMaterial().getNome().toUpperCase());
				}
				
				if(org.apache.commons.lang.StringUtils.isNotEmpty(nfitem.getRecopi())){
					if(org.apache.commons.lang.StringUtils.isNotEmpty(item.getDescricao())){
						item.setDescricao(item.getDescricao() + " " + nfitem.getRecopi());
					}else {
						item.setDescricao(nfitem.getRecopi());
					}
				}
				
				if(nfitem.getNcmcompleto() != null && !nfitem.getNcmcompleto().equals("")){
					item.setNcm(nfitem.getNcmcompleto());
				} else {
					item.setNcm(nfitem.getNcmcapitulo() != null ? nfitem.getNcmcapitulo().getCodeFormat() : "");
				}
				
				item.setEmbalagem(nfitem.getUnidademedida() != null && nfitem.getUnidademedida().getSimbolo() != null ? nfitem.getUnidademedida().getSimbolo().toUpperCase() : "");
				item.setQuantidade(nfitem.getQtde());
				
				//alterado
				item.setCfop(nfitem.getCfop() != null ? StringUtils.soNumero(nfitem.getCfop().getCodigo()) : "");
				item.setValorUnitario(nfitem.getValorunitario());
				item.setValorTotal(nfitem.getValorbruto().getValue().doubleValue());
				item.setInfoadicional(nfitem.getInfoadicional());
				
				Tipocobrancaicms tipocobrancaicms = nfitem.getTipocobrancaicms();
				
				if(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(tipocobrancaicms)){
					totalBCSTJaPaga = totalBCSTJaPaga.add(nfitem.getValorbcicmsst() != null ? nfitem.getValorbcicmsst() : new Money());
					totalSTJaPaga = totalSTJaPaga.add(nfitem.getValoricmsst() != null ? nfitem.getValoricmsst() : new Money());
					
//					if(nfitem.getMaterial() != null && 
//							nfitem.getMaterial().getMaterialgrupo() != null && 
//							nfitem.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null){
//						Materialgrupo materialgrupo = materialgrupoService.loadForEntrada(nfitem.getMaterial().getMaterialgrupo());
//						Materialgrupotributacao materialgrupotributacao = materialgrupo.getMaterialgrupotributacaoByEmpresa(nf.getEmpresa(), nfitem.getCfop());
//						
//						if(materialgrupotributacao != null 
//								&& materialgrupotributacao.getExibirmensagemstpaga() != null 
//								&& materialgrupotributacao.getExibirmensagemstpaga()){
//							if(materialgrupotributacao.getExibirmensagemstpagatotal() == null || !materialgrupotributacao.getExibirmensagemstpagatotal()){
//								StringBuilder sb = new StringBuilder();
//								if(item.getInfoadicional() != null) sb.append(item.getInfoadicional());
//								if(sb.length() > 0) sb.append("\n");
//								
//								Money valorbcicmsst = nfitem.getValorbcicmsst() != null ? nfitem.getValorbcicmsst() : new Money();
//								Money valoricmsst = nfitem.getValoricmsst() != null ? nfitem.getValoricmsst() : new Money();
//								
//								sb.append("BASE ST J� PAGA: ").append(valorbcicmsst).append(" ");
//								sb.append("ST J� PAGA: ").append(valoricmsst);
//								
//								item.setInfoadicional(sb.toString());
//							} else {
//								exibirSTJaPaga = true;
//							}
//						}
//					}
					if(nfitem.getMaterial() != null && nfitem.getGrupotributacao() == null){
						List<Categoria> listaCategoriaCliente = null;
						if(nf.getCliente() != null){
							listaCategoriaCliente = categoriaService.findByPessoa(nf.getCliente());
						}
						
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
														empresa, 
														naturezaoperacao, 
														nfitem.getMaterial().getMaterialgrupo(),
														nf.getCliente(),
														true, 
														nfitem.getMaterial(),
														nfitem.getNcmcompleto(),
														nf.getEnderecoCliente(), 
														null,
														listaCategoriaCliente,
														null,
														nf.getOperacaonfe(),
														nf.getModeloDocumentoFiscalEnum(),
														nf.getLocaldestinonfe(),
														nf.getDtEmissao()));
						nfitem.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					}else {
						nfitem.setGrupotributacao(grupotributacaoService.loadForEntrada(nfitem.getGrupotributacao()));
					}
					Grupotributacao grupotributacao = nfitem.getGrupotributacao();
					if(grupotributacao != null 
							&& grupotributacao.getExibirmensagemstpaga() != null 
							&& grupotributacao.getExibirmensagemstpaga()){
						if(grupotributacao.getExibirmensagemstpagatotal() == null || !grupotributacao.getExibirmensagemstpagatotal()){
							StringBuilder sb = new StringBuilder();
							if(item.getInfoadicional() != null) sb.append(item.getInfoadicional());
							if(sb.length() > 0) sb.append("\n");
							
							Money valorbcicmsst = nfitem.getValorbcicmsst() != null ? nfitem.getValorbcicmsst() : new Money();
							Money valoricmsst = nfitem.getValoricmsst() != null ? nfitem.getValoricmsst() : new Money();
							
							sb.append("BASE ST J� PAGA: ").append(valorbcicmsst).append(" ");
							sb.append("ST J� PAGA: ").append(valoricmsst);
							
							item.setInfoadicional(sb.toString());
						} else {
							exibirSTJaPaga = true;
						}
					}
				} else {
					item.setAliquotaIcms(nfitem.getIcms() != null ? nfitem.getIcms() : 0d);
					item.setBaseIcms(nfitem.getValorbcicms() != null ? nfitem.getValorbcicms().getValue().doubleValue() : 0d);
					item.setBaseIcmsSt(nfitem.getValorbcicmsst() != null ? nfitem.getValorbcicmsst().getValue().doubleValue() : 0d);
					item.setValorIcms(nfitem.getValoricms() != null ? nfitem.getValoricms().getValue().doubleValue() : 0d);
					item.setValorIcmsSt(nfitem.getValoricmsst() != null ? nfitem.getValoricmsst().getValue().doubleValue() : 0d);
				}
				
				item.setAliquotaIpi(nfitem.getIpi() != null ? nfitem.getIpi() : 0d);
				item.setValorIpi(nfitem.getValoripi() != null ? nfitem.getValoripi().getValue().doubleValue() : 0d);
				item.setPlaqueta(nfitem.getPatrimonioitem() != null ? nfitem.getPatrimonioitem().getPlaqueta() : "");
				
				List<Materiallegenda> listaMateriallegenda = materiallegendaService.findByMaterial(nfitem.getMaterial().getCdmaterial().toString());
				if(listaMateriallegenda != null && listaMateriallegenda.size() > 0){
					DanfeLegendaBean leg = new DanfeLegendaBean();
					leg.setCodigo(nfitem.getMaterial().getCdmaterial().toString());
					
					for (Materiallegenda materiallegenda : listaMateriallegenda) {
						try{
							Arquivo simbolo = ArquivoService.getInstance().loadWithContents( materiallegenda.getSimbolo()); //aqui ele da erro logomarca t� nula
							if(simbolo != null){
								Image imageSimbolo = ArquivoService.getInstance().loadAsImage(simbolo);
								if(imageSimbolo != null){
									leg.getImage().add(imageSimbolo);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					danfe.getListaLegenda().add(leg);
				}
				
				if ((parametrogeralService.getBoolean("EXIBIR_TRIBUTOS_IBPT_DANFE") && ModeloDocumentoFiscalEnum.NFE.equals(nf.getModeloDocumentoFiscalEnum())) || 
						(parametrogeralService.getBoolean("EXIBIR_TRIBUTOS_IBPT_DANFCE") && ModeloDocumentoFiscalEnum.NFCE.equals(nf.getModeloDocumentoFiscalEnum()))) {
					if (nfitem.getValorImpostoFederal() != null && nfitem.getValorImpostoFederal().getValue().doubleValue() > 0d ||
							nfitem.getValorImpostoEstadual() != null && nfitem.getValorImpostoEstadual().getValue().doubleValue() > 0d ||
							nfitem.getValorImpostoMunicipal() != null && nfitem.getValorImpostoMunicipal().getValue().doubleValue() > 0d) {
						String tributos = "Trib. aprox. R$: " + 
								SinedUtil.descriptionDecimal(nfitem.getValorImpostoFederal() != null ? nfitem.getValorImpostoFederal().getValue().doubleValue() : 0d, true) + " Federal, " +
								SinedUtil.descriptionDecimal(nfitem.getValorImpostoEstadual() != null ? nfitem.getValorImpostoEstadual().getValue().doubleValue() : 0d, true) + " Estadual e " +
								SinedUtil.descriptionDecimal(nfitem.getValorImpostoMunicipal() != null ? nfitem.getValorImpostoMunicipal().getValue().doubleValue() : 0d, true) + " Municipal (Fonte: IBPT)";
						
						item.setInfoadicional((item.getInfoadicional() != null ? item.getInfoadicional() : "") + "\n" + tributos);
					} else if(nfitem.getValortotaltributos() != null && nfitem.getValortotaltributos().getValue().doubleValue() > 0d){
						StringBuilder tributos = new StringBuilder();
						tributos.append("VAL. APROX. DOS TRIBUTOS: ");
						
						if (nfitem.getPercentualimpostoecf() != null && nfitem.getPercentualimpostoecf() > 0) {
							tributos.append(SinedUtil.descriptionDecimal(nfitem.getPercentualimpostoecf())).append("% / ");
						}
						
						tributos.append("R$")
							.append(SinedUtil.descriptionDecimal(nfitem.getValortotaltributos() != null ? nfitem.getValortotaltributos().getValue().doubleValue() : 0d, true));
						
						Tabelaimposto tabelaimposto = tabelaimpostoService.getTabelaVigenteByEmpresa(empresa);
						if(tabelaimposto != null && tabelaimposto.getChave() != null){
							tributos.append(" - FONTE: IBPT / VERS�O: " + tabelaimposto.getVersao());
						}
						
						item.setInfoadicional((item.getInfoadicional() != null ? item.getInfoadicional() : "") + "\n" + tributos.toString());
					}
				}
				
				danfe.getListaItens().add(item);
			}
		}
		
		
		List<Arquivotef> listaArquivotef = arquivotefService.findByNota(nf.getCdNota() + "");
		if(listaArquivotef == null || listaArquivotef.size() == 0){
			String whereInVenda = vendaService.getWhereInVendaByNota(nf);
			if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVenda)){
				listaArquivotef = arquivotefService.findByVenda(whereInVenda);
			}
		}
		
		String dadostef = arquivotefService.getDadosTEFForObservacao(listaArquivotef);
		
		String comprovantetef = arquivotefService.getComprovanteTEF(listaArquivotef);
		danfe.setComprovantetef(comprovantetef);
		
		danfe.setTributadoIssqn(tributadoIssqn);
		if(tributadoIssqn){
			danfe.setValorTotalProdutos(0d);
		}
		
		StringBuilder infoComplementar = new StringBuilder();
		String infoNotaReferenciada = getInfoNotaReferenciada(nf);
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(dadostef)){
			infoComplementar.append(dadostef);
		}
		if(nf.getNotaStatus() != null && nf.getNotaStatus().equals(NotaStatus.NFE_DENEGADA)){
			infoComplementar.append("NF-E DENEGADA").append("\n");
		}
		if(!org.apache.commons.lang.StringUtils.isEmpty(nf.getNumeronotaempenho())){
			infoComplementar.append("NOTA DE EMPENHO: ").append(nf.getNumeronotaempenho()).append("\n");
		}
		if(org.apache.commons.lang.StringUtils.isNotBlank(infoNotaReferenciada)){
			infoComplementar.append(infoNotaReferenciada).append("\n");
		}
		if(!org.apache.commons.lang.StringUtils.isEmpty(nf.getNumeropedido())){
			infoComplementar.append("PEDIDO: ").append(nf.getNumeropedido()).append("\n");
		}
		if(!org.apache.commons.lang.StringUtils.isEmpty(nf.getNumerocontrato())){
			infoComplementar.append("CONTRATO: ").append(nf.getNumerocontrato()).append("\n");
		}
		
		if (parametrogeralService.getBoolean("EXIBIR_TRIBUTOS_IBPT_DANFE")) {
			if (danfe.getValorTotalImpostoFederal() != null && danfe.getValorTotalImpostoFederal() > 0d ||
					danfe.getValorTotalImpostoEstadual() != null && danfe.getValorTotalImpostoEstadual() > 0d ||
					danfe.getValorTotalImpostoMunicipal() != null && danfe.getValorTotalImpostoMunicipal() > 0d) {
				infoComplementar.append("Trib. aprox. R$: " + 
						SinedUtil.descriptionDecimal(danfe.getValorTotalImpostoFederal() != null ? danfe.getValorTotalImpostoFederal() : 0d, true) + " Federal, " +
						SinedUtil.descriptionDecimal(danfe.getValorTotalImpostoEstadual() != null ? danfe.getValorTotalImpostoEstadual() : 0d, true) + " Estadual e " +
						SinedUtil.descriptionDecimal(danfe.getValorTotalImpostoMunicipal() != null ? danfe.getValorTotalImpostoMunicipal() : 0d, true) + " Municipal (Fonte: IBPT)");
				
				infoComplementar.append("\n");
			} else if(danfe.getValorTotalTributos() != null && danfe.getValorTotalTributos() > 0d){				
				infoComplementar.append("VAL. APROX. DOS TRIBUTOS: ");
				
				Double totalPercentagemTributos = SinedUtil.round((danfe.getValorTotalTributos() / danfe.getValorTotalNota()) * 100, 2);
				
				if (totalPercentagemTributos > 0) {
					infoComplementar.append(SinedUtil.descriptionDecimal(totalPercentagemTributos)).append("% / ");
				}
				
				infoComplementar.append("R$").append(SinedUtil.descriptionDecimal(danfe.getValorTotalTributos(), true));
				
				Tabelaimposto tabelaimposto = tabelaimpostoService.getTabelaVigenteByEmpresa(empresa);
				if(tabelaimposto != null && tabelaimposto.getChave() != null){
					infoComplementar.append(" - FONTE: IBPT / VERS�O: " + tabelaimposto.getVersao());
				}
				
				infoComplementar.append("\n");
			}
		}
		
		infoComplementar.append(danfe.getInfocomplementar());
		
		if(exibirSTJaPaga){
			if(infoComplementar.length() > 0) infoComplementar.append(" ");
			infoComplementar.append("(BASE ST J� PAGA: R$ ").append(totalBCSTJaPaga).append(" ").append("ST J� PAGA: R$ ").append(totalSTJaPaga).append(")");
		}
		
		if(nf.getEnderecodiferenteentrega() != null && nf.getEnderecodiferenteentrega()){
			if(infoComplementar.length() > 0) infoComplementar.append("\n");
			infoComplementar.append("Endere�o de entrega: ").append(nf.getEnderecoEntregaCompleto());
		}
		if(nf.getEnderecodiferenteretirada() != null && nf.getEnderecodiferenteretirada()){
			if(infoComplementar.length() > 0) infoComplementar.append("\n");
			infoComplementar.append("Endere�o de retirada: ").append(nf.getEnderecoRetiradaCompleto());
		}
		
		danfe.setInfocomplementar(infoComplementar.toString());

		if(danfe.getListaLegenda() != null && danfe.getListaLegenda().size() > 0){
			danfe.setListaLegenda(danfe.getListaLegenda().subList(0, danfe.getListaLegenda().size() > 18 ? 18 : danfe.getListaLegenda().size()));
		}
		return danfe;
	}
	
	private void setInformacaoComplementarDanfe(DanfeBean danfe, Notafiscalproduto nf) {
		StringBuilder info = new StringBuilder();
		
		info.append(nf.getInfoadicionalcontrib() != null ? nf.getInfoadicionalcontrib() : "");
		
		Double difalUfDestino = 0d;
		Double fcpUfDestino = 0d;
		Double difalUfOrigem = 0d;
		
		if(SinedUtil.isListNotEmpty(nf.getListaItens())){
			for(Notafiscalprodutoitem item : nf.getListaItens()){
				if(item.getValoricmsdestinatario() != null){
					difalUfDestino += item.getValoricmsdestinatario().getValue().doubleValue();
				}
				if(item.getValorfcpdestinatario() != null){
					fcpUfDestino += item.getValorfcpdestinatario().getValue().doubleValue();
				}
				if(item.getValoricmsremetente() != null){
					difalUfOrigem += item.getValoricmsremetente().getValue().doubleValue();
				}
			}
		}
		
		if(difalUfDestino > 0 || fcpUfDestino > 0 || fcpUfDestino > 0){
			String infoDifal = "Valores totais do ICMS Interestadual: DIFAL da UF destino R$ " + new Money(difalUfDestino) + 
							   " + FCP R$ " + new Money(fcpUfDestino) + "; DIFAL da UF Origem R$ " + new Money(difalUfOrigem) + ".";
			info.append(infoDifal);
		}
		
		danfe.setInfocomplementar(info.toString());
	}
	
	private String getInfoNotaReferenciada(Notafiscalproduto nf) {
		StringBuilder sb = new StringBuilder();
		if(nf != null && SinedUtil.isListNotEmpty(nf.getListaReferenciada())){
			for(Notafiscalprodutoreferenciada ref : nf.getListaReferenciada()){
				if(org.apache.commons.lang.StringUtils.isNotBlank(ref.getChaveacesso())){
					StringBuilder sbRef = new StringBuilder();
					try {
						sbRef.append("NFe Ref.:");
						sbRef.append(" s�rie:" + ref.getChaveacesso().substring(23, 25));
						sbRef.append(" n�mero:" + Long.parseLong(ref.getChaveacesso().substring(26, 34)));
						sbRef.append(" emit:" + new Cnpj(ref.getChaveacesso().substring(6, 20)).toString());
						sbRef.append(" em " + ref.getChaveacesso().substring(4, 6) + "/20" + ref.getChaveacesso().substring(2, 4));
						sbRef.append(" [" + ref.getChaveacesso() + "]");
						
						sb.append(sbRef).append(" ");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return sb.length() > 0 ? sb.substring(0, sb.length()-1) : null;
	}
	
	public Money getValorAproximadoImposto(Empresa empresa, Naturezaoperacao naturezaoperacao, Notafiscalprodutoitem item, ImpostoEcfEnum impostoEcfEnum) {
		boolean notSimplesRemessa = naturezaoperacao == null || naturezaoperacao.getSimplesremessa() == null || !naturezaoperacao.getSimplesremessa();
		
		Double percentualimposto = getPercentualImposto(empresa, item, impostoEcfEnum);
		
		if(notSimplesRemessa && item != null && item.getMaterial() != null && percentualimposto != null){
			Double valorItem = 0d;
			if(item.getQtde() != null && item.getValorunitario() != null){
				valorItem += item.getQtde() * item.getValorunitario();
			}
			if(item.getValorfrete() != null){
				valorItem += item.getValorfrete().getValue().doubleValue();
			}
			if(item.getValorseguro() != null){
				valorItem += item.getValorseguro().getValue().doubleValue();
			}
			if(item.getOutrasdespesas() != null){
				valorItem += item.getOutrasdespesas().getValue().doubleValue();
			}
			if(item.getValordesconto() != null){
				valorItem -= item.getValordesconto().getValue().doubleValue();
			}
			if(item.getMaterial() != null && percentualimposto != null){
				valorItem = valorItem * percentualimposto / 100;
			}
			if(valorItem != null && valorItem > 0){
				return new Money(valorItem);
			}
		}
		
		return null;
	}
	
	public Double getPercentualImposto(Empresa empresa, Notafiscalprodutoitem item, ImpostoEcfEnum impostoEcfEnum) {
		Double percentualimposto = item.getMaterial() != null ? item.getMaterial().getPercentualimpostoecf() : null;
		Origemproduto origemproduto = item.getOrigemproduto();
		String ncmcompleto = org.apache.commons.lang.StringUtils.trimToNull(item.getNcmcompleto());
		String extipi = org.apache.commons.lang.StringUtils.trimToNull(item.getExtipi());
		String codigonbs = item.getMaterial() != null ? org.apache.commons.lang.StringUtils.trimToNull(item.getMaterial().getCodigonbs()) : null;
		String codlistaservico = item.getMaterial() != null ? org.apache.commons.lang.StringUtils.trimToNull(item.getMaterial().getCodlistaservico()) : null;
		Boolean tributadoicms = item.getTributadoicms();
		
		return getPercentualImposto(empresa, percentualimposto, origemproduto, ncmcompleto, extipi, codigonbs, codlistaservico, tributadoicms, impostoEcfEnum);
	}
	
	public Double getPercentualImposto(Empresa empresa, Double percentualimposto, Origemproduto origemproduto, String ncmcompleto, String extipi, 
			String codigonbs, String codlistaservico, Boolean tributadoicms, ImpostoEcfEnum impostoEcfEnum) {
		Double percentualimpostoIBPT = null;
		boolean produto = tributadoicms != null && tributadoicms;
		if(produto){
			percentualimpostoIBPT = tabelaimpostoService.getPercentualImpostoProduto(empresa, origemproduto, ncmcompleto, extipi, impostoEcfEnum);
		} else {
			percentualimpostoIBPT = tabelaimpostoService.getPercentualImpostoServico(empresa, codigonbs, codlistaservico, impostoEcfEnum);
		}
		if(percentualimpostoIBPT != null){
			percentualimposto = percentualimpostoIBPT;
		}
		return percentualimposto;
	}
	
	public Resource gerarDanfe(String itensSelecionados) throws Exception {
		MergeReport mergeReport = new MergeReport("danfe_" + SinedUtil.datePatternForReport() + ".pdf");
		List<Notafiscalproduto> listaNota = this.findForDanfe(itensSelecionados);
		
		for (Notafiscalproduto notafiscalproduto : listaNota) {
			notafiscalprodutoitemService.ordenaListaItens(notafiscalproduto);
			notafiscalprodutoitemService.ordenarListaPorPedidoVenda(notafiscalproduto);
			mergeReport.addReport(this.danfe(notafiscalproduto));
		}
		return mergeReport.generateResource();
	}
	
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForDanfe(String itensSelecionados)
	 *
	 * @param itensSelecionados
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2013
	 */
	public List<Notafiscalproduto> findForDanfe(String itensSelecionados) {
		return notafiscalprodutoDAO.findForDanfe(itensSelecionados);
	}
	
	public String getNextNumero() {
		return notafiscalprodutoDAO.getNextNumero();
	}
	
	public Notafiscalproduto loadForReceita(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null)
			throw new SinedException("Nota n�o pode ser nula.");
		List<Notafiscalproduto> lista = loadForReceita(notafiscalproduto.getCdNota().toString());
		return lista != null && lista.size() > 0 ? lista.get(0) : null;
	}
	
	public List<Notafiscalproduto> loadForReceita(String whereIn) {
		List<Notafiscalproduto> lista = notafiscalprodutoDAO.loadForReceita(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			List<Notafiscalprodutoitem> listaItens = notafiscalprodutoitemService.loadForReceita(whereIn);
			if(SinedUtil.isListNotEmpty(listaItens)){
				for(Notafiscalprodutoitem item : listaItens){
					if(item.getNotafiscalproduto() != null && item.getNotafiscalproduto().getCdNota() != null){
						for(Notafiscalproduto nfp : lista){
							if(nfp.getCdNota() != null && nfp.getCdNota().equals(item.getNotafiscalproduto().getCdNota())){
								if(!Hibernate.isInitialized(nfp.getListaItens()) || nfp.getListaItens() == null)
									nfp.setListaItens(new ArrayList<Notafiscalprodutoitem>());
								nfp.getListaItens().add(item);
							}
						}
					}
				}
			}
		}
		
		return lista;
	}
	
	public Notafiscalproduto findByArquivonfnota(Arquivonfnota arquivonfnota) {
		return notafiscalprodutoDAO.findByArquivonfnota(arquivonfnota);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForSpedRegC100
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForSpedRegC100(SpedarquivoFiltro filtro) {
		return notafiscalprodutoDAO.findForSpedRegC100(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForSagefiscal(SagefiscalFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Notafiscalproduto> findForSagefiscal(SagefiscalFiltro filtro) {
		return notafiscalprodutoDAO.findForSagefiscal(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2017
	 */
	public Notafiscalproduto loadForSagefiscal(Integer cdNota) {
		return notafiscalprodutoDAO.loadForSagefiscal(cdNota);
	}
	
	public List<Notafiscalproduto> findForSelect(String projetos) {
		return notafiscalprodutoDAO.findForSelect(projetos);
	}
	public List<Notafiscalproduto> findForReport(NotaFiltro filtro) {
		return notafiscalprodutoDAO.findForReport(filtro);
	}
		/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> findByWhereIn(String whereIn) {
		return notafiscalprodutoDAO.findByWhereIn(whereIn);
	}
	
	public Notafiscalproduto geraNotaRomaneios(Notafiscalproduto nf, String romaneios) {
		List<Romaneio> listaRomaneio = romaneioService.findForGerarNota(romaneios);
		
		nf.setCadastrarcobranca(Boolean.FALSE);
		nf.setRomaneios(romaneios);
		nf.setPresencacompradornfe(Presencacompradornfe.OUTROS);
		
		List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
		
		Empresa empresa = null;
		Cliente cliente = null;
		boolean variosclientes = false;
		boolean variosenderecos = false;
		Endereco enderecoCliente = null;
		
		Double pesoliquidoTotal = 0d;
		Double pesobrutoTotal = 0d;
		Double qtdeTotal = 0d;
		boolean valorprodutoByContrato = parametrogeralService.getBoolean(Parametrogeral.CONTRATO_VALOR_PRODUTO);
		
		for (Romaneio romaneio : listaRomaneio) {
			empresa = romaneio.getEmpresa();
			
			if(romaneio.getEndereco() != null){
				if(enderecoCliente == null){
					enderecoCliente = romaneio.getEndereco();
				} else  if(!enderecoCliente.equals(romaneio.getEndereco())){
					variosenderecos = true;
				}
			}
			
			if(romaneio.getCliente() != null){
				if(cliente == null){
					cliente = romaneio.getCliente();
				} else if(!cliente.equals(romaneio.getCliente())){
					variosclientes = true;
				}
			} else {
				if(romaneio.getListaRomaneioorigem() != null && !romaneio.getListaRomaneioorigem().isEmpty()){
					for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
						if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCliente() != null){
							if(cliente == null){
								cliente = romaneioorigem.getContrato().getCliente();
							} else if(!cliente.equals(romaneioorigem.getContrato().getCliente())){
								variosclientes = true;
								break;
							}
						}
					}
				}
			}
			boolean romaneioOriginadoDeContrato = false;
			boolean isContratoFechamento = false;
			if(SinedUtil.isListNotEmpty(romaneio.getListaRomaneioorigem())){
				for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
					if(romaneioorigem.getContrato() != null || romaneioorigem.getContratofechamento() != null){
						romaneioOriginadoDeContrato = true;
						isContratoFechamento = romaneioorigem.getContratofechamento() != null;
						break;
					}
				}
			}
			
			List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
			if(listaRomaneioitem != null){
				for (Romaneioitem romaneioitem : listaRomaneioitem) {
					romaneioitem.setRomaneio(romaneio);
					Notafiscalprodutoitem notafiscalprodutoitem = new Notafiscalprodutoitem();
					Material material = romaneioitem.getMaterial();
					Double quantidade = romaneioitem.getQtde();
					
					qtdeTotal += quantidade;
					
					notafiscalprodutoitem.setMaterial(material);
					notafiscalprodutoitem.setLoteestoque(romaneioitem.getLoteestoque());
					if(valorprodutoByContrato && romaneioOriginadoDeContrato){
						Contratomaterial contratomaterial = contratomaterialService.loadByRomaneioitem(romaneioitem, isContratoFechamento);
						notafiscalprodutoitem.setValorunitario(contratomaterial.getValorunitario());
					}else{
						if(material.getValorindenizacao() != null){
							notafiscalprodutoitem.setValorunitario(material.getValorindenizacao());
						} else {
							notafiscalprodutoitem.setValorunitario(material.getValorcusto());
						}
					}
					notafiscalprodutoitem.setQtde(quantidade);
					if(notafiscalprodutoitem.getValorunitario() != null && notafiscalprodutoitem.getQtde() != null){
						notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getValorunitario() * notafiscalprodutoitem.getQtde()));
					}
					notafiscalprodutoitem.setMaterialclasse(romaneioitem.getMaterialclasse());
					notafiscalprodutoitem.setIncluirvalorprodutos(true);
					notafiscalprodutoitem.setUnidademedida(material.getUnidademedida());
					notafiscalprodutoitem.setNcmcapitulo(material.getNcmcapitulo());
					notafiscalprodutoitem.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
					notafiscalprodutoitem.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
					notafiscalprodutoitem.setLoteestoque(romaneioitem.getLoteestoque());
					if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
						notafiscalprodutoitem.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
					
					if(material.getPeso() != null && material.getPeso() > 0){
						Double pesoliquido = quantidade * material.getPeso();
						if(notafiscalprodutoitem.getUnidademedida() != null && notafiscalprodutoitem.getUnidademedida().getSimbolo() != null && notafiscalprodutoitem.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							pesoliquido = quantidade;
						}
						pesoliquido = SinedUtil.roundByParametro(pesoliquido);
						
						notafiscalprodutoitem.setPesoliquido(pesoliquido);
						pesoliquidoTotal += pesoliquido;
					}
					if(material.getPesobruto() != null && material.getPesobruto() > 0){
						Double pesobruto = quantidade * material.getPesobruto();
						if(notafiscalprodutoitem.getUnidademedida() != null && notafiscalprodutoitem.getUnidademedida().getSimbolo() != null && notafiscalprodutoitem.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							pesobruto = quantidade;
						}
						pesobruto = SinedUtil.roundByParametro(pesobruto);
						
						notafiscalprodutoitem.setPesobruto(pesobruto);
						pesobrutoTotal += pesobruto;
					}
					if(romaneioitem.getPatrimonioitem()!=null && romaneioitem.getPatrimonioitem().getPlaqueta()!=null){
						notafiscalprodutoitem.setInfoadicional("Patrim�nio: "+romaneioitem.getPatrimonioitem().getPlaqueta()+"\n");
						if (material.getNomenfdescricaonota() != null){
							String nomenfdescricaonota = material.getNomenfdescricaonota();
							nomenfdescricaonota = nomenfdescricaonota +" - "+ romaneioitem.getPatrimonioitem().getPlaqueta();
							material.setNomenfdescricaonota(nomenfdescricaonota);
							notafiscalprodutoitem.setMaterial(material);
						}
					}
					
					listaItens.add(notafiscalprodutoitem);
				}
			}
		}
		
		if(qtdeTotal != null && qtdeTotal > 0){
			nf.setQtdevolume(qtdeTotal.intValue());
		}
		if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
			nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
		}
		if(pesobrutoTotal != null && pesobrutoTotal > 0){
			nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
		}
		
		if (empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			nf.setEmpresa(empresa);
		
			Endereco enderecoEmpresa = empresa.getEndereco();
			if(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null){
				nf.setMunicipiogerador(enderecoEmpresa.getMunicipio());
				nf.setUfveiculo(enderecoEmpresa.getMunicipio().getUf());
			}
			
		}
		
		if(!variosclientes && cliente != null && cliente.getCdpessoa() != null){
			cliente = clienteService.carregarDadosCliente(cliente);
			Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
			
			if(variosenderecos || enderecoCliente == null || 
					(enderecoCliente.getEnderecotipo() != null && !enderecoCliente.getEnderecotipo().equals(Enderecotipo.FATURAMENTO))){
				Set<Endereco> listaEndereco = cliente.getListaEndereco();
				if(listaEndereco != null && listaEndereco.size() > 0){
					for (Endereco endereco : listaEndereco) {
						if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
							enderecoCliente = endereco;
							break;
						}
					}
				}
			}
			
			nf.setCliente(cliente);
			nf.setEnderecoCliente(enderecoCliente != null ? enderecoCliente : cliente.getEndereco());
			nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
			nf.setTelefoneClienteTransiente(telefoneCliente);
			
			if(nf.getCliente() != null && nf.getCliente().getCpfOuCnpj() != null){
				nf.getCliente().setCpfcnpj(nf.getCliente().getCpfOuCnpj());
			}
		}
		
		List<Notafiscalprodutoitem> lista = new ArrayList<Notafiscalprodutoitem>();
		for (Notafiscalprodutoitem item : listaItens) {
			if(item.getMaterial() != null){
				List<Categoria> listaCategoriaCliente = null;
				if(cliente != null){
					listaCategoriaCliente = categoriaService.findByPessoa(cliente);
				}
				
				Material material = materialService.loadForTributacao(item.getMaterial());
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
												empresa, 
												nf.getNaturezaoperacao(), 
												material.getMaterialgrupo(),
												cliente,
												true, 
												material, 
												item.getNcmcompleto(),
												nf.getEnderecoCliente(), 
												null,
												listaCategoriaCliente,
												null,
												nf.getOperacaonfe(),
												nf.getModeloDocumentoFiscalEnum(),
												nf.getLocaldestinonfe(),
												nf.getDtEmissao()));
				item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
			}
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
			
			lista.add(item);
		}
		
		nf.setListaItens(lista);
		
		return nf;
	}
	
	/**
	 * Pega a covers�o da unidade de medida.
	 *
	 * @param material
	 * @param unidademedida
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/02/2014
	 */
	public Double getFracaoConversaoUnidademedida(Material material, Unidademedida unidademedida) {
		Double fracao = null;
		Boolean achou = Boolean.FALSE;
		List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);
		
		if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
			for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
				if(materialunidademedida.getUnidademedida() != null && 
						materialunidademedida.getUnidademedida().getCdunidademedida() != null && 
						materialunidademedida.getFracao() != null &&
						materialunidademedida.getUnidademedida().equals(unidademedida)){
					fracao = materialunidademedida.getFracaoQtdereferencia();
				}
			}
		}
		
		if(!achou){
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao unidademedidaconversao : listaUnidademedidaconversao) {
					if(unidademedidaconversao.getUnidademedida() != null && 
							unidademedidaconversao.getUnidademedida().getCdunidademedida() != null && 
							unidademedidaconversao.getFracao() != null &&
							unidademedidaconversao.getUnidademedida().equals(unidademedida)){
						fracao = unidademedidaconversao.getFracaoQtdereferencia();
					}
				}
			}
		}
		
		return fracao;
	}
	
	public Notafiscalproduto geraNotaVenda(Notafiscalproduto nf, Venda venda, String expedicoes, Boolean somenteproduto, String whereInCdentregamaterial, String whereInQtdeEntrada) {
		venda = vendaService.loadForEntrada(venda);
		if (venda.getColaborador() != null && venda.getColaborador().getCdpessoa() != null){
			venda.setColaborador(colaboradorService.load(venda.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
		}
		if (venda.getCliente() != null && venda.getCliente().getCdpessoa() != null){
			venda.setCliente(clienteService.load(venda.getCliente(), "cliente.cdpessoa, cliente.nome"));
		}
		if (venda.getEmpresa()!=null && venda.getEmpresa().getCdpessoa()!=null){
			venda.setEmpresa(empresaService.loadForVenda(venda.getEmpresa()));
		}
		if (venda.getLocalarmazenagem()!=null && venda.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
			venda.setLocalarmazenagem(localarmazenagemService.load(venda.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		}
		
		boolean gerarNotaIndustrializacaoRetorno = false;
		boolean referenciarNotaEntrada = false;
		Cfop cfopindustrializacao = null;
		Cfop cfopretorno = null;
		List<Entregamaterial> listaEntregamaterial = null;
		Venda vendaWithPedidovenda = vendaService.loadWithPedidovenda(venda);
		boolean consideraripiconta = false;
		boolean considerarIcmsStConta = false;
		boolean considerardesoneracaoconta = false;
		boolean considerarFcpStConta = false;
		
		
		if (vendaWithPedidovenda != null
				&& vendaWithPedidovenda.getPedidovendatipo() != null) {
			if (vendaWithPedidovenda.getPedidovendatipo().getNaturezaoperacao() != null && 
					NotaTipo.NOTA_FISCAL_PRODUTO.equals(vendaWithPedidovenda.getPedidovendatipo().getNaturezaoperacao().getNotaTipo())) {
				nf.setNaturezaoperacao(vendaWithPedidovenda.getPedidovendatipo().getNaturezaoperacao());
				if(vendaWithPedidovenda.getPedidovendatipo().getNaturezaoperacao().getOperacaonfe() != null){
					nf.setOperacaonfe(vendaWithPedidovenda.getPedidovendatipo().getNaturezaoperacao().getOperacaonfe());
				}
			}
			
			consideraripiconta = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getConsideraripiconta());
			considerarIcmsStConta = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getConsideraricmsstconta());
			considerardesoneracaoconta = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getConsiderardesoneracaoconta());
			considerarFcpStConta = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getConsiderarfcpstconta());
			gerarNotaIndustrializacaoRetorno = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getGerarNotaIndustrializacaoRetorno());
			referenciarNotaEntrada = Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getReferenciarnotaentradananotasaida());
			
			nf.setGerarNotaIndustrializacaoRetorno(gerarNotaIndustrializacaoRetorno);
			nf.setReferenciarNotaEntrada(referenciarNotaEntrada);
			
			if(gerarNotaIndustrializacaoRetorno){
				cfopindustrializacao = vendaWithPedidovenda.getPedidovendatipo().getCfopindustrializacao();
				cfopretorno = vendaWithPedidovenda.getPedidovendatipo().getCfopretorno();
			}
			if(gerarNotaIndustrializacaoRetorno || referenciarNotaEntrada){
				if (whereInCdentregamaterial != null && !whereInCdentregamaterial.equals("")) {
					listaEntregamaterial = entregamaterialService.findForIndustrializacaoRetorno(whereInCdentregamaterial, null);
					
					String[] idEntregamaterial = whereInCdentregamaterial.split(",");
					String[] qtdeEntradamaterial = whereInQtdeEntrada.split("\\|");
					
					for (int i = 0; i < idEntregamaterial.length; i++) {
						Integer cdentregamaterial = Integer.parseInt(idEntregamaterial[i]);
						for (Entregamaterial entregamaterial : listaEntregamaterial) {
							if(entregamaterial.getCdentregamaterial() != null && 
									entregamaterial.getCdentregamaterial().equals(cdentregamaterial)){
								entregamaterial.setQtdeNotaIndustrializacao(Double.parseDouble(qtdeEntradamaterial[i].replaceAll("\\.", "").replaceAll(",", ".")));
								break;
							}
						}
						
					}
				}
			}
		}
		
		venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
		venda.setListavendamaterial(vendamaterialService.findByVenda(venda));
		venda.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(venda));
		vendamaterialService.verificaVendaKitDiscriminarnota(venda);
		vendamaterialService.verificaVendaKitFlexivelExibir(venda);
		
		boolean recapagem = SinedUtil.isRecapagem();
		if(!recapagem){
			vendamaterialService.verificaVendaProducaoDiscriminarnota(venda);
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO)){
			vendaService.buscarTabelaPrecoVenda(venda);
		}
		
		if(venda.getEmpresa() != null){
			nf.setEmpresa(venda.getEmpresa());
			//nf.setInfoadicionalcontrib(venda.getEmpresa().getTextoinfcontribuinte());
			
			Empresa empresa = empresaService.loadWithEndereco(venda.getEmpresa());
			Endereco enderecoEmpresa = empresa.getEndereco();
			if(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null){
				nf.setMunicipiogerador(enderecoEmpresa.getMunicipio());
				nf.setUfveiculo(enderecoEmpresa.getMunicipio().getUf());
			}
		}
		nf.setNumero("");
		
		Cliente cliente = clienteService.carregarDadosCliente(venda.getCliente());
		Endereco enderecoCliente = null;
		Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
		
		Set<Endereco> listaEndereco = cliente.getListaEndereco();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for (Endereco endereco : listaEndereco) {
				if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
					enderecoCliente = endereco;
					break;
				}
			}
		}
		
		notafiscalprodutoautorizacaoService.preencheAbaAutorizacaoByParametro(nf);
		
		nf.setPresencacompradornfe(venda.getPresencacompradornfe());
		nf.setCliente(cliente);
		nf.setEnderecoCliente(enderecoCliente != null ? enderecoCliente : venda.getEndereco());
		nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
		nf.setTelefoneClienteTransiente(telefoneCliente);
		nf.setTipooperacaonota(Tipooperacaonota.SAIDA);
		nf.setProjeto(venda.getProjeto());
		nf.setContaboleto(venda.getContaboleto());
		if(nf.getContaboleto() != null){
			nf.setContacarteira(contacarteiraService.loadPadraoBoleto(nf.getContaboleto()));
		}
		
		if(nf.getCliente() != null && nf.getCliente().getCpfOuCnpj() != null){
			nf.getCliente().setCpfcnpj(nf.getCliente().getCpfOuCnpj());
		}
		
		if(venda.getFrete() != null){
			nf.setResponsavelfrete(venda.getFrete());
			if(nf.getResponsavelfrete().equals(ResponsavelFrete.TERCEIROS)){
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			nf.setValorfrete(venda.getValorfrete());
		}else{
			nf.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
		}
		Fornecedor transportador = null;
		if(venda.getTerceiro() != null && venda.getTerceiro().getCdpessoa() != null){
			transportador = venda.getTerceiro();
		}else if(venda.getEmpresa() != null && venda.getEmpresa().getTransportador() != null &&
				venda.getEmpresa().getTransportador().getCdpessoa() != null){
			transportador = venda.getEmpresa().getTransportador();
		}
		if(transportador != null){
			transportador = fornecedorService.carregaFornecedor(transportador);
			Endereco enderecotransportador = null;
			if(transportador != null && 
					transportador.getListaEndereco() != null &&
					transportador.getListaEndereco().size() > 0){
				enderecotransportador = transportador.getEndereco();
			}
			
			nf.setTransportador(transportador);
			nf.setEnderecotransportador(enderecotransportador);
		}
		
		//preenche o local de destino com base no endere�o do cliente e no municipio gerador
		this.preencheLocalDestinoNota(nf);
		nf.setOperacaonfe(ImpostoUtil.getOperacaonfe(nf.getCliente(), nf.getNaturezaoperacao()));
		
		List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
		List<Vendamaterial> listavendamaterial = venda.getListavendamaterial();
		
		Money outrasDespesasIt = new Money();
		boolean considerarJurosNota = false;
		if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getConsiderarjurosnota() != null && venda.getPrazopagamento().getConsiderarjurosnota()){
			Money totalParcela = venda.getTotalparcela();
			Money totalProdutos = venda.getTotalvenda();
			vendaService.setTotalImpostos(venda);
			if(consideraripiconta){
				totalProdutos = totalProdutos.add(venda.getTotalipi());
			}
			if(considerardesoneracaoconta){
				totalProdutos = totalProdutos.subtract(venda.getTotalDesoneracaoIcms());
			}
			if(considerarIcmsStConta){
				totalProdutos = totalProdutos.add(venda.getTotalIcmsSt());
			}
			if(considerarFcpStConta){
				totalProdutos = totalProdutos.add(venda.getTotalFcpSt());
			}
			
			Money diferencaJuros = totalParcela.subtract(totalProdutos);
			if(diferencaJuros.getValue().compareTo(BigDecimal.ZERO) > 0){
				considerarJurosNota = true;
				outrasDespesasIt = diferencaJuros.divide(new Money(listavendamaterial.size(), false));
			}
		}
		
		List<Expedicaoitem> listaExpedicaoitem = null;
		if(expedicoes != null && !expedicoes.equals("")){
			listaExpedicaoitem = expedicaoitemService.findForFaturamento(expedicoes);
		}
		
		Double pesoliquidoTotal = 0d;
		Double pesobrutoTotal = 0d;
		Double qtdeTotal = 0d;
		Money descontoTotal = new Money();
		boolean temServico = Boolean.FALSE;
		
//		Cfop cfop = null;
//		Cfop cfopcomodato = this.getCfopByPedidovendaComodato(venda, cliente);
		ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(nf.getNaturezaoperacao());
		for (Vendamaterial vendamaterial : listavendamaterial) {
			boolean servico = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getServico() != null && vendamaterial.getMaterial().getServico();
			boolean tributacaomunicipal = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaomunicipal() != null && vendamaterial.getMaterial().getTributacaomunicipal();
			boolean tributacaoestadual = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaoestadual() != null && vendamaterial.getMaterial().getTributacaoestadual();
			if(somenteproduto != null && somenteproduto && vendamaterial.getMaterial() != null && ((servico && !tributacaoestadual) || tributacaomunicipal)){
				temServico = Boolean.TRUE;
				continue;
			}
			
			Double quantidade = vendamaterial.getQuantidade();
			Double multiplicador = vendamaterial.getMultiplicador();
			if(multiplicador == null) multiplicador = 1.0;
			
			if(listaExpedicaoitem != null && listaExpedicaoitem.size() > 0){
				boolean achou = false;
				Integer cdvendamaterial = vendamaterial.getCdvendamaterial();
				quantidade = 0.0;
				for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
					if(expedicaoitem.getVendamaterial() != null && 
							expedicaoitem.getVendamaterial().getCdvendamaterial() != null &&
							expedicaoitem.getVendamaterial().getCdvendamaterial().equals(cdvendamaterial)){
						quantidade += expedicaoitem.getQtdeexpedicao();
						achou = true;
					}
				}
				
				if(!achou){
					continue;
				}
			}
			
			Material material = vendamaterial.getMaterial();
			Notafiscalprodutoitem item = new Notafiscalprodutoitem();
			
			String nomeComAlturaLarguraComprimento = vendaService.getAlturaLarguraComprimentoConcatenados(vendamaterial, material);
			if(!"".equals(nomeComAlturaLarguraComprimento)){
				material.setNome(material.getNome() + " " + nomeComAlturaLarguraComprimento);
			}
			item.setNotafiscalproduto(nf);
			item.setMaterial(material);
			item.setPneu(vendamaterial.getPneu());
			item.setQtde(quantidade);
			item.setLoteestoque(vendamaterial.getLoteestoque());
			item.setPatrimonioitem(vendamaterial.getPatrimonioitem());
			item.setIdentificadorespecifico(vendamaterial.getIdentificadorespecifico());
			item.setVendamaterialmestre(vendamaterial.getVendamaterialmestre());
			if(vendamaterial.getCdvendamaterial() != null) item.setVendamaterial(vendamaterial);
			if(parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO)){
				if(vendamaterial.getIdentificadorfabricante() != null){
					item.setItempedidocompra(vendamaterial.getIdentificadorfabricante());
				}else if(vendamaterial.getMaterialtabelaprecoitem() != null && vendamaterial.getMaterialtabelaprecoitem().getIdentificadorfabricante() != null){
					item.setItempedidocompra(vendamaterial.getMaterialtabelaprecoitem().getIdentificadorfabricante());
				}
			}
			
			if(material.getCodigoanp() != null){
				item.setCodigoanp(material.getCodigoanp());
				item.setDadoscombustivel(Boolean.TRUE);
			}
			
			StringBuilder infoAdicional = new StringBuilder();
			if(vendamaterial.getPneu() != null && vendamaterial.getPneu().getMaterialbanda() != null && vendamaterial.getPneu().getMaterialbanda().getNome() != null){
				
				infoAdicional.append(vendamaterial.getPneu().getMaterialbanda().getNome());
			}
			if(vendamaterial.getObservacao() != null && 
					!vendamaterial.getObservacao().trim().equals("")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append(vendamaterial.getObservacao());
			}
			if(vendamaterial.getLoteestoque() != null && vendamaterial.getLoteestoque().getNumero() != null){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("Lote: " + vendamaterial.getLoteestoque().getNumero());
			}
			if (parametrogeralService.getValorPorNome(Parametrogeral.PREENCHERINFOADICIONALCOMNUMVENDA).equalsIgnoreCase("TRUE")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("Venda N�: " + venda.getCdvenda());
			}
			if(vendamaterial.getPneu() != null && 
					vendamaterial.getPneu().getPneumarca() != null && 
					vendamaterial.getPneu().getPneumarca().getNome() != null &&
					!vendamaterial.getPneu().getPneumarca().getNome().trim().equals("")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("Marca: " + vendamaterial.getPneu().getPneumarca().getNome());
			}
			if(vendamaterial.getPneu() != null && 
					vendamaterial.getPneu().getSerie() != null && 
					!vendamaterial.getPneu().getSerie().trim().equals("")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("S�rie/Fogo: " + vendamaterial.getPneu().getSerie());
			}
			if(vendamaterial.getPneu() != null && 
					vendamaterial.getPneu().getDot() != null && 
					!vendamaterial.getPneu().getDot().trim().equals("")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("DOT: " + vendamaterial.getPneu().getDot());
			}
			if(vendamaterial.getPneu() != null && 
					vendamaterial.getPneu().getPneumedida() != null && 
					vendamaterial.getPneu().getPneumedida().getNome() != null &&
					!vendamaterial.getPneu().getPneumedida().getNome().trim().equals("")){
				if(infoAdicional.length() > 0) infoAdicional.append(" ");
				infoAdicional.append("Medida: " + vendamaterial.getPneu().getPneumedida().getNome());
			}
			
			if(vendamaterial.getNomeKitParaNF() != null){
				infoAdicional.append("Este item pertence ao kit " + vendamaterial.getNomeKitParaNF());
			}
			item.setInfoadicional(infoAdicional.toString().trim());

			if(material.getEpi() != null && material.getEpi()){
				item.setMaterialclasse(Materialclasse.EPI);
			}else if(material.getPatrimonio() != null && material.getPatrimonio() && (material.getProduto() == null || !material.getProduto())){
				item.setMaterialclasse(Materialclasse.PATRIMONIO);
			}else if(material.getProduto() != null && material.getProduto() && (material.getPatrimonio() == null || !material.getPatrimonio())){
				item.setMaterialclasse(Materialclasse.PRODUTO);
			}else if(material.getServico() != null && material.getServico()){
				item.setMaterialclasse(Materialclasse.SERVICO);
			} 
			
			Double fatorconversao = vendaService.verificaUnidademedidaMaterialDiferenteForNota(item, vendamaterial);
			if(item.getUnidademedida() == null){
				item.setUnidademedida(vendamaterial.getUnidademedida());
			}
			
			quantidade = item.getQtde();
			qtdeTotal += quantidade;
			
			
			Double peso = vendamaterial.getPesoVendaOuMaterial();
			Double pesobruto = material.getPesobruto();
			
			if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){
				Material aux_material = materialService.findListaMaterialunidademedida(item.getMaterial());
				Unidademedida aux_unidademedida = item.getUnidademedida();
				
				if(aux_unidademedida != null && (peso != null || pesobruto != null)){
					Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
					if(!unidadeprincipal){
						Double fracao = vendamaterial.getFatorconversaoQtdereferencia();
						if(fracao != null && fracao > 0){
							if(pesobruto != null){
								pesobruto = pesobruto/fracao;
							}
						}
					}
				}
			}else if(peso != null && vendamaterial.getFatorconversao() != null){
				peso = peso * vendamaterial.getFatorconversaoQtdereferencia();
			}
			
			if(peso != null && peso > 0){
				peso = quantidade * peso;
				if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
					peso = quantidade;
				}
				peso = SinedUtil.roundByParametro(peso);
				
				item.setPesoliquido(peso);
				pesoliquidoTotal += peso;
			}
			if(pesobruto != null && pesobruto > 0){
				pesobruto = quantidade * pesobruto;
				if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
					pesobruto = quantidade;
				}
				pesobruto = SinedUtil.roundByParametro(pesobruto);
				
				item.setPesobruto(pesobruto);
				pesobrutoTotal += pesobruto;
			}
			
			item.setIncluirvalorprodutos(true);
			item.setValorfrete(venda.getValorfrete() != null && venda.getValorfrete().getValue().doubleValue() > 0.0 ? new Money(SinedUtil.truncateFloor(venda.getValorfrete().getValue().doubleValue()/listavendamaterial.size())) : new Money(0.0));
			item.setLocalarmazenagem(venda.getLocalarmazenagem());
			
			if(considerarJurosNota){
				item.setOutrasdespesas(outrasDespesasIt);
			}
			if(vendamaterial.getOutrasdespesas() != null){
				item.setOutrasdespesas(vendamaterial.getOutrasdespesas().add(item.getOutrasdespesas()));
			}
			item.setValorseguro(vendamaterial.getValorSeguro());
			
			Money valorDesconto = new Money();
			Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
			
			if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
				valorDesconto = getValorDescontoVenda(listavendamaterial, vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
				valorDesconto = valorDesconto.add(valorDescontoItem);
				descontoTotal = descontoTotal.add(valorDesconto);
				
				item.setValordesconto(new Money(SinedUtil.round(valorDesconto.getValue(), 2)));
				
				if(item.getValorunitario() == null || item.getValorunitario() == 0){
					item.setValorunitario(vendamaterial.getPreco());
				}
			} else {
				valorDesconto = getValorDescontoVenda(listavendamaterial, vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
				valorDescontoItem = valorDescontoItem.divide(new Money(quantidade*multiplicador));
				valorDesconto = valorDesconto.add(valorDescontoItem);
				
				if(fatorconversao != null && fatorconversao > 0d){
					valorDesconto = valorDesconto.multiply(new Money(fatorconversao));
				}
				
				if(item.getValorunitario() == null || item.getValorunitario() == 0.0){
					item.setValorunitario(vendamaterial.getPreco() - valorDesconto.getValue().doubleValue());
				}else {
					item.setValorunitario(item.getValorunitario() - valorDesconto.getValue().doubleValue());
				}
			}
			
			if(multiplicador != null){
				if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
					item.setQtde(item.getQtde() * multiplicador); 
				} else {
					item.setValorunitario(item.getValorunitario() * multiplicador);
				}
			}
			item.setValorunitario(SinedUtil.round(item.getValorunitario(), 10));
			
			if(item.getMaterial() != null){
				List<Categoria> listaCategoriaCliente = null;
				if(cliente != null){
					listaCategoriaCliente = categoriaService.findByPessoa(cliente);
				}
				
				Material beanMaterial = materialService.loadForTributacao(material);
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
												nf.getEmpresa(), 
												nf.getNaturezaoperacao(), 
												beanMaterial.getMaterialgrupo(),
												cliente,
												true, 
												beanMaterial,
												item.getNcmcompleto(),
												(enderecoCliente != null ? enderecoCliente : venda.getEndereco()), 
												null,
												listaCategoriaCliente,
												null,
												nf.getOperacaonfe(),
												nf.getModeloDocumentoFiscalEnum(),
												nf.getLocaldestinonfe(),
												nf.getDtEmissao()));
				item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao, vendamaterial.getGrupotributacao()));
				item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
			}
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
			
			if (nf.getNaturezaoperacao()!=null && nf.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
				Endereco enderecoEmpresa = null;
				if (nf.getEmpresa()!=null && nf.getEmpresa().getCdpessoa()!=null){
					enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(nf.getEmpresa());
				}
				Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoEmpresa, nf.getEnderecoCliente());
				List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(nf.getNaturezaoperacao(), cfopescopo);
				if (!listaNaturezaoperacaocfop.isEmpty()){
					item.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
				}
			}
			
			item.setNcmcapitulo(material.getNcmcapitulo());
			item.setNve(material.getNve());
			item.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
			item.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
			if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
				item.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
			
			this.setValoresImpostoVenda(item, vendamaterial, false, venda.getEmpresa());
			this.setBaseCalculoItem(material, item);
			item.setOperacaonfe(nf.getOperacaonfe());
			grupotributacaoService.calcularValoresFormulaIpi(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
			grupotributacaoService.calcularValoresFormulaIcms(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
			this.calculaMva(item, material);
			item.setPercentualimpostoecf(this.getPercentualImposto(nf.getEmpresa(), item, null));
			item.setValortotaltributos(this.getValorAproximadoImposto(nf.getEmpresa(), nf.getNaturezaoperacao(), item, null));
			
			item.setPercentualImpostoFederal(this.getPercentualImposto(nf.getEmpresa(), item, ImpostoEcfEnum.FEDERAL));
			item.setPercentualImpostoEstadual(this.getPercentualImposto(nf.getEmpresa(), item, ImpostoEcfEnum.ESTADUAL));
			item.setPercentualImpostoMunicipal(this.getPercentualImposto(nf.getEmpresa(), item, ImpostoEcfEnum.MUNICIPAL));
			
			item.setValorImpostoFederal(this.getValorAproximadoImposto(nf.getEmpresa(), nf.getNaturezaoperacao(), item, ImpostoEcfEnum.FEDERAL));
			item.setValorImpostoEstadual(this.getValorAproximadoImposto(nf.getEmpresa(), nf.getNaturezaoperacao(), item, ImpostoEcfEnum.ESTADUAL));
			item.setValorImpostoMunicipal(this.getValorAproximadoImposto(nf.getEmpresa(), nf.getNaturezaoperacao(), item, ImpostoEcfEnum.MUNICIPAL));
			
			grupotributacaoService.calcularValoresFormulaIcmsST(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
			
			//AT 54951
			if (gerarNotaIndustrializacaoRetorno){
				item.setCfop(cfopindustrializacao);
				listaItens.add(item);
				verificaNotaIndustrializacao(nf, item, cfopindustrializacao, cfopretorno, listaEntregamaterial, listaItens, referenciarNotaEntrada);
			} else {
				includeInfoAdicionalNotaEntrada(referenciarNotaEntrada, listaEntregamaterial, item);
				listaItens.add(item);
			}
			
			item.setInformacaoAdicionalProdutoBean(criaBeanInfoProduto(vendamaterial.getMaterial(), vendamaterial.getPneu(), null, vendamaterial, venda, vendamaterial.getPedidovendamaterial(), venda.getPedidovenda()));
			String templateInfoAdicionalItem = montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
			if(templateInfoAdicionalItem != null){
				item.setInfoadicional(templateInfoAdicionalItem);
				item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
			}else {
				item.setInfoAdicionalItemAnterior("");
			}
			
			if(nf.getModeloDocumentoFiscalEnum() != null && nf.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
				item.setRecopi(null);
				
				item.setNumeropedidocompra(null);
				item.setItempedidocompra(null);
				
				item.setCodigoseloipi(null);
				item.setQtdeseloipi(null);
				item.setCnpjprodutoripi(null);
				item.setTipocobrancaipi(null);
				item.setValorbcipi(null);
				item.setIpi(null);
				item.setValoripi(null);
				item.setTipocalculoipi(null);
				item.setAliquotareaisipi(null);
				item.setQtdevendidaipi(null);
				item.setCodigoenquadramentoipi(null);
				item.setValorFiscalIpi(null);
				
				item.setDadosimportacao(false);
				item.setValorbcii(null);
				item.setDespesasaduaneiras(null);
				item.setValoriof(null);
				item.setValorii(null);
				
				item.setDadosexportacao(false);
				item.setDrawbackexportacao(null);
				item.setNumregistroexportacao(null);
				item.setChaveacessoexportacao(null);
				item.setQuantidadeexportacao(null);
				item.setListaNotafiscalprodutoitemdi(null);
			}
		}
		nf.setListaItens(listaItens);
		
		Money descontoReal = new Money();
		Money valorTotalProdutos = new Money(0.0);
		for (Notafiscalprodutoitem pi : nf.getListaItens()) {
			valorTotalProdutos = valorTotalProdutos.add(pi.getValorbruto());
			if(pi.getValordesconto() != null){
				descontoReal = descontoReal.add(pi.getValordesconto());
			}
		}
		
		double descontoRealDbl = descontoReal.getValue().doubleValue();
		double descontoTotalDbl = descontoTotal.getValue().doubleValue();
		if(descontoRealDbl != descontoTotalDbl){
			double diferenca = descontoTotalDbl - descontoRealDbl;
			if(diferenca != 0d && nf.getListaItens() != null && nf.getListaItens().size() > 0){
				Notafiscalprodutoitem notafiscalprodutoitem = nf.getListaItens().get(0);
				notafiscalprodutoitem.setValordesconto(notafiscalprodutoitem.getValordesconto().add(new Money(diferenca)));
			}
		}
		
		nf.setValorprodutos(valorTotalProdutos);
		if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP))){
			nf.setCadastrartransportador(Boolean.TRUE);
			if(venda.getQuantidadevolumes() != null){
				if(nf.getQtdevolume() != null){
					nf.setQtdevolume(nf.getQtdevolume() + venda.getQuantidadevolumes());
				}else {
					nf.setQtdevolume(venda.getQuantidadevolumes());
				}
			}
		}else{
			if(qtdeTotal != null && qtdeTotal > 0){
				nf.setQtdevolume(qtdeTotal.intValue());
			}			
		}
		if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
			nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
		}
		if(pesobrutoTotal != null && pesobrutoTotal > 0){
			nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
		}
		if (venda.getValorfrete() != null){
			valorTotalProdutos = valorTotalProdutos.add(venda.getValorfrete());
		}
		if(descontoTotal != null){
			valorTotalProdutos = valorTotalProdutos.subtract(descontoTotal);
		}
		
		if(venda.getValorfrete() != null && nf.getListaItens() != null && nf.getListaItens().size() > 0){
			Money valorfreteItens = new Money();
			for (Notafiscalprodutoitem item : nf.getListaItens()) {
				valorfreteItens = valorfreteItens.add(item.getValorfrete());
			}
			nf.getListaItens().get(0).setValorfrete(nf.getListaItens().get(0).getValorfrete().add(venda.getValorfrete().subtract(valorfreteItens)));
		}
		
		nf.setValor(valorTotalProdutos);
		nf.setNotaStatus(NotaStatus.EMITIDA);	
		nf.setCdvendaassociada(venda.getCdvenda());
		nf.setExpedicoes(expedicoes);
		
		if(vendaWithPedidovenda == null || vendaWithPedidovenda.getPedidovendatipo() == null || vendaWithPedidovenda.getPedidovendatipo().getBonificacao() == null ||
				!vendaWithPedidovenda.getPedidovendatipo().getBonificacao()){
			List<Documentoorigem> listaDocumentoOrigem = documentoorigemService.buscarListaParaDuplicatas(venda);
			List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
			nf.setCadastrarcobranca(Boolean.TRUE);
			if(!temServico && listaDocumentoOrigem != null && listaDocumentoOrigem.size() > 0) {
				for (Documentoorigem docOrigem : listaDocumentoOrigem) {
					Notafiscalprodutoduplicata nfDuplicata = new Notafiscalprodutoduplicata();
					nfDuplicata.setDtvencimento(docOrigem.getDocumento().getDtvencimento());
					nfDuplicata.setValor(docOrigem.getDocumento().getValor());
					nfDuplicata.setDocumentotipo(docOrigem.getDocumento().getDocumentotipo());
					listaDuplicata.add(nfDuplicata);
				}
				
				if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
					for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
						for (Notafiscalprodutoduplicata notafiscalprodutoduplicata : listaDuplicata) {
							if(notafiscalprodutoduplicata.getDocumentotipo().equals(vendapagamento.getDocumentotipo()) &&
									SinedDateUtils.equalsIgnoreHour(notafiscalprodutoduplicata.getDtvencimento(), vendapagamento.getDataparcela()) &&
									notafiscalprodutoduplicata.getValor().compareTo(vendapagamento.getValororiginal()) == 0){
								notafiscalprodutoduplicata.setAdquirente(vendapagamento.getAdquirente());
								notafiscalprodutoduplicata.setBandeira(vendapagamento.getBandeira());
								notafiscalprodutoduplicata.setAutorizacao(vendapagamento.getAutorizacao());
							}
						}
					}
				}
			}else if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
				Money valorTotalPagamentosVenda = new Money();
				Money valorTotalPagamentosNota = new Money();
				
				for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
					if(vendapagamento.getValororiginal() != null){
						valorTotalPagamentosVenda = valorTotalPagamentosVenda.add(vendapagamento.getValororiginal());
					}
				}
				vendaService.setTotalImpostos(venda);
				Money valorTotalProdutosComIpi = valorTotalProdutos;
				
				if(consideraripiconta){
					valorTotalProdutosComIpi = valorTotalProdutosComIpi.add(venda.getTotalipi());
				}
				if(considerarIcmsStConta){
					valorTotalProdutosComIpi = valorTotalProdutosComIpi.add(venda.getTotalIcmsSt());
				}
				if(considerardesoneracaoconta){
					valorTotalProdutosComIpi = valorTotalProdutosComIpi.subtract(venda.getTotalDesoneracaoIcms());
				}
				if(considerarFcpStConta){
					valorTotalProdutosComIpi = valorTotalProdutosComIpi.add(venda.getTotalFcpSt());
				}
				
				for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
					Notafiscalprodutoduplicata nfDuplicata = new Notafiscalprodutoduplicata();
					nfDuplicata.setDtvencimento(vendapagamento.getDataparcela());
					Money valor = vendapagamento.getValororiginal();
					if (valor != null) {
						if (valorTotalProdutosComIpi != null && valorTotalPagamentosVenda != null && valorTotalProdutosComIpi.compareTo(valorTotalPagamentosVenda) == 1) {
							valor = valor.divide(valorTotalPagamentosVenda).multiply(valorTotalProdutosComIpi);
							Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(valor.getValue().doubleValue()), 2).doubleValue();
							valor = new Money(valorTruncado);
						}
						
						valorTotalPagamentosNota = valorTotalPagamentosNota.add(valor);
					}
					
					nfDuplicata.setValor(valor);
					nfDuplicata.setDocumentotipo(vendapagamento.getDocumentotipo());
					nfDuplicata.setAdquirente(vendapagamento.getAdquirente());
					nfDuplicata.setBandeira(vendapagamento.getBandeira());
					nfDuplicata.setAutorizacao(vendapagamento.getAutorizacao());
					listaDuplicata.add(nfDuplicata);
				}
				
				if (valorTotalPagamentosNota.compareTo(valorTotalProdutosComIpi) == 1 && valorTotalPagamentosNota.getValue().doubleValue() < valorTotalProdutosComIpi.getValue().doubleValue()) {
					Money diferenca = valorTotalProdutosComIpi.subtract(valorTotalPagamentosNota);
					listaDuplicata.get(listaDuplicata.size() - 1).setValor(listaDuplicata.get(listaDuplicata.size() - 1).getValor().add(diferenca));
				} else if (valorTotalPagamentosNota.compareTo(valorTotalProdutosComIpi) == 1 && valorTotalPagamentosNota.getValue().doubleValue() > valorTotalProdutosComIpi.getValue().doubleValue()) {
					Money diferenca = valorTotalPagamentosNota.subtract(valorTotalProdutosComIpi);
					listaDuplicata.get(listaDuplicata.size() - 1).setValor(listaDuplicata.get(listaDuplicata.size() - 1).getValor().subtract(diferenca));
				}
				
				if(vendaWithPedidovenda != null && vendaWithPedidovenda.getPedidovendatipo() != null && !Boolean.TRUE.equals(vendaWithPedidovenda.getPedidovendatipo().getNaoatualizarvencimento()) && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(vendaWithPedidovenda.getPedidovendatipo().getGeracaocontareceberEnum())){
					Venda v = vendaService.loadForEntrada(venda);
					if (v.getCliente() != null && v.getCliente().getCdpessoa() != null) v.setCliente(clienteService.load(v.getCliente(), "cliente.cdpessoa, cliente.nome"));
					v.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(v));
					v.setListavendamaterial(vendamaterialService.findByVenda(v));
					v.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(v));
					v.setDtvendaEmissaonota(nf.getDtEmissao());
					vendaService.ajustaValorParcelaVenda(v, valorTotalPagamentosNota, listaDuplicata, false, false, true, v.getPrazopagamento(), true);
				}
			}
			nf.setListaDuplicata(listaDuplicata);
		}else {
			nf.setCadastrarcobranca(Boolean.FALSE);
		}
		nf.setValorfrete(venda.getValorfrete());
		
		if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getCdprazopagamento() != null){
			nf.setPrazopagamentofatura(venda.getPrazopagamento());
			if (venda.getPrazopagamento().getAvista() != null && venda.getPrazopagamento().getAvista()){
				nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
			}else {
				nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
			}
		}
		
		nf.setDocumentotipo(venda.getDocumentotipo());
		
//		if(comodato != null && comodato && infadicionarpedidocomodato != null){
//			nf.setInfoadicionalcontrib(
//					(nf.getInfoadicionalcontrib() != null ? (nf.getInfoadicionalcontrib() + ". "): "" ) 
//					+ infadicionarpedidocomodato);
//		}
		
		this.adicionarInfNota(nf, venda, venda.getEmpresa());
		this.setTotalNota(nf);
		
		return nf;
	}
	
	private void setValoresImpostoVenda(Notafiscalprodutoitem item,	Vendamaterial vendamaterial, boolean agrupamento, Empresa empresa) {
		if(empresa != null && Boolean.TRUE.equals(empresa.getExibiripivenda())){
			item.setNaocalcularipi(true);
			if(vendamaterial.getTipocobrancaipi() != null){
				if(agrupamento){
					if(vendamaterial.getValoripi() != null){
						if(item.getQtdevendidaipi() != null){
							item.setQtdevendidaipi(item.getQtde());
						}
						item.setValoripi(item.getValoripi() != null ? item.getValoripi().add(vendamaterial.getValoripi()) : vendamaterial.getValoripi());
					}
				}else {
					item.setTipocobrancaipi(vendamaterial.getTipocobrancaipi());
					if(vendamaterial.getTipocalculoipi() != null){
						item.setTipocalculoipi(vendamaterial.getTipocalculoipi());
					}
					item.setValoripi(vendamaterial.getValoripi());
					item.setIpi(vendamaterial.getIpi());
					item.setAliquotareaisipi(vendamaterial.getAliquotareaisipi());
					if(vendamaterial.getTipocalculoipi() != null && vendamaterial.getTipocalculoipi().equals(Tipocalculo.EM_VALOR)){
						item.setQtdevendidaipi(item.getQtde());
					}
					
					if(vendamaterial.getTipotributacaoicms() != null){
						item.setTipotributacaoicms(vendamaterial.getTipotributacaoicms());
						item.setTipocobrancaicms(vendamaterial.getTipocobrancaicms());
						item.setIcms(vendamaterial.getIcms());
						item.setValoricms(vendamaterial.getValoricms());
						
						item.setAbaterdesoneracaoicms(vendamaterial.getAbaterdesoneracaoicms());
						item.setPercentualdesoneracaoicms(vendamaterial.getPercentualdesoneracaoicms());
						item.setValordesoneracaoicms(vendamaterial.getValordesoneracaoicms());
					
						item.setIcmsst(vendamaterial.getIcmsst());
						item.setValoricmsst(vendamaterial.getValoricmsst());
					
						item.setReducaobcicmsst(vendamaterial.getReducaobcicmsst());
						item.setMargemvaloradicionalicmsst(vendamaterial.getMargemvaloradicionalicmsst());
						
						item.setFcp(vendamaterial.getFcp());
						item.setValorfcp(vendamaterial.getValorfcp());
						
						item.setFcpst(vendamaterial.getFcpst());
						item.setValorfcpst(vendamaterial.getValorfcpst());
						
						item.setDadosicmspartilha(vendamaterial.getDadosicmspartilha());
						item.setIcmsdestinatario(vendamaterial.getIcmsdestinatario());
						item.setFcpdestinatario(vendamaterial.getFcpdestinatario());
						item.setIcmsinterestadual(vendamaterial.getIcmsinterestadual());
						item.setIcmsinterestadualpartilha(vendamaterial.getIcmsinterestadualpartilha());
						item.setDifal(vendamaterial.getDifal());
						item.setValordifal(vendamaterial.getValordifal());
					}
					
					
					item.setAliquotareaisipi(vendamaterial.getAliquotareaisipi());
					if(vendamaterial.getTipocalculoipi() != null && vendamaterial.getTipocalculoipi().equals(Tipocalculo.EM_VALOR)){
						item.setQtdevendidaipi(item.getQtde());
					}
				}
			}
		}
	}
	
	private void includeInfoAdicionalNotaEntrada(boolean referenciarNotaEntrada, List<Entregamaterial> listaEntregamaterial, Notafiscalprodutoitem item) {
		if(referenciarNotaEntrada && listaEntregamaterial != null && listaEntregamaterial.size() > 0){
			StringBuilder infoadicional = new StringBuilder();
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				boolean materialIgual = verificaMaterialIgual(item, entregamaterial);
		
				if (materialIgual){
					infoadicional.append("Referente a NF.: ").append(entregamaterial.getEntregadocumento().getNumero());
					if(entregamaterial.getQtdeNotaIndustrializacao() != null && entregamaterial.getQtdeNotaIndustrializacao() > 0d){
						infoadicional.append(" Qtde.: ").append(SinedUtil.descriptionDecimal(entregamaterial.getQtdeNotaIndustrializacao()));
					}
					infoadicional.append(" - ");
					
					Set<Notafiscalprodutoitemnotaentrada> listaNotafiscalprodutoitemnotaentrada = item.getListaNotafiscalprodutoitemnotaentrada();
					if(listaNotafiscalprodutoitemnotaentrada == null){
						listaNotafiscalprodutoitemnotaentrada = new ListSet<Notafiscalprodutoitemnotaentrada>(Notafiscalprodutoitemnotaentrada.class);
					}
					
					Notafiscalprodutoitemnotaentrada notafiscalprodutoitemnotaentrada = new Notafiscalprodutoitemnotaentrada();
					notafiscalprodutoitemnotaentrada.setEntregamaterial(entregamaterial);
					notafiscalprodutoitemnotaentrada.setQtdereferencia(entregamaterial.getQtdeNotaIndustrializacao());
					listaNotafiscalprodutoitemnotaentrada.add(notafiscalprodutoitemnotaentrada);
					
					item.setListaNotafiscalprodutoitemnotaentrada(listaNotafiscalprodutoitemnotaentrada);
				}
			}
			
			
			if (infoadicional.length() > 0){
				String info = infoadicional.substring(0, infoadicional.length() - 3);
				if (item.getInfoadicional() != null && !item.getInfoadicional().equals("")){
					if(!item.getInfoadicional().contains(info)){
						item.setInfoadicional(item.getInfoadicional() + ". " + info);
					}
				} else {
					item.setInfoadicional(info);
				}
			}
		}
	}
	
	public void calculaMva(CalculoImpostoInterface item, Material material) {
		if(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) || 					//10
				Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.equals(item.getTipocobrancaicms()) ||		//10
				Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||			//201
				Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||			//202
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||	//203
				Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms())){					//900
			Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : null;
			Double valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null;
			Double icms = item.getIcms();
			Double icmsst = item.getIcmsst();	
			Double mva = item.getMargemvaloradicionalicmsst() != null ? item.getMargemvaloradicionalicmsst() : null;
			Double valorbruto = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
			Double qtde = item.getQtde() != null ? item.getQtde() : 0d;
//			Double valoripi = item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
//			Double valorfrete = item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
//			Double valordesconto = item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
//			Double valorseguro = item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
//			Double outrasdespesas = item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : 0d;
			Double aliquotacreditoicms = item.getAliquotacreditoicms();
			if(aliquotacreditoicms != null && Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms())){
				item.setValorcreditoicms(new Money(valorbruto *(aliquotacreditoicms/100)));
			}
			
			Double valoricmsst = 0d;
			if(item.getValorbcicms() != null && item.getIcms() != null && item.getIcmsst() != null){

				icms = item.getIcms();
				icmsst = item.getIcmsst();
				if (mva != null && mva > 0){
					if(icms != icmsst && Tipotributacaoicms.NORMAL.equals(item.getTipotributacaoicms())){
						mva = ((1 + (mva/100)) * (1 - (icms/100))/(1 - (icmsst/100))) - 1;
					} else {
						mva = mva/100;
					}
					
					Double valorunitario = item.getValorunitario();
					qtde = item.getQtde();
					Double reducaobcicmsst = item.getReducaobcicmsst() != null ? item.getReducaobcicmsst() : null;
					if(Tipotributacaoicms.SIMPLES_NACIONAL.equals(item.getTipotributacaoicms()) && reducaobcicmsst != null){
//						Double basecalculo = (valorunitario * qtde) + valorfrete - valordesconto + valorseguro + outrasdespesas;
						Double basecalculo = (valorunitario * qtde);
						Double basecalculoicmsst = basecalculo * (1+mva);
						valorbcicmsst  = basecalculoicmsst * ( 1 - (reducaobcicmsst)/100);
						
					}else  {
//						Double basecalculo = (valorunitario * qtde) + valorfrete - valordesconto + valorseguro + outrasdespesas + valoripi;
						Double basecalculo = (valorunitario * qtde);
						Double mvaapurado = basecalculo * (SinedUtil.round(mva*100, 2))/100;
						valorbcicmsst = basecalculo + mvaapurado;
						
					}
					
					Double debitoicmsst = SinedUtil.round((valorbcicmsst * icmsst)/100, 2);
					Double debitoicms = SinedUtil.round((valorbruto * icms)/100, 2);
					valoricmsst = debitoicmsst - debitoicms;
				} else { 
					valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null;
					if(valorbcicmsst == null || valorbcicmsst == 0){
						valorbcicmsst = valorbcicms;
					}
					valoricmsst = (valorbcicmsst * icmsst)/100;
				}
				item.setValoricmsst(valoricmsst != null ? new Money(valoricmsst) : null);
				item.setValorbcicmsst(valorbcicmsst != null ? new Money(valorbcicmsst) : null);
				
			}else if(valorbcicmsst != null && icmsst != null){
				valoricmsst = (valorbcicmsst * icmsst)/100;
				
				item.setValoricmsst(new Money(valoricmsst));
			} else {
				item.setValoricmsst(new Money());
				item.setValorbcicmsst(new Money());
			}
			setValorFCPSt(item, material, MoneyUtils.moneyToDouble(item.getValorbcicms()));
		}
	}
	
	public void setBaseCalculoItem(Material material, CalculoImpostoInterface item) {
		if(item.getValorunitario() != null && item.getQtde() != null){
			double desconto = SinedUtil.round(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d, 2);
			double frete = SinedUtil.round(item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d, 2);
			double seguro = SinedUtil.round(item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d, 2);
			double outrasdespesas = SinedUtil.round(item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : 0d, 2);
			double basecalculo = SinedUtil.round(SinedUtil.round(item.getValorunitario() * item.getQtde(), 2) - desconto + frete + seguro + outrasdespesas, 2);
			
			if(item.getIcms() != null){
				if(DoubleUtils.isMaiorQueZero(item.getReducaobcicms())){
					item.setValorbcicms(new Money(basecalculo * ( 1 - item.getReducaobcicms()/100)));
				}else {
					if(material.getPercentualBcicms() != null){
						item.setValorbcicms(new Money(basecalculo).multiply(new Money(material.getPercentualBcicms()/100)));
					}else {
						item.setValorbcicms(new Money(basecalculo));
					}
				}
				item.setValoricms(new Money((item.getValorbcicms().getValue().doubleValue() * item.getIcms())/100d));
				if(item.getPercentualdesoneracaoicms() != null){
					item.setValordesoneracaoicms(new Money(((item.getValorbcicms().getValue().doubleValue() * item.getIcms())/100d)*(item.getPercentualdesoneracaoicms()/100d)));
				}
			}
			
			if(item.getIss() != null){
				if(material.getPercentualBciss() != null){
					item.setValorbciss(new Money(basecalculo).multiply(new Money(material.getPercentualBciss()/100)));
				}else {
					item.setValorbciss(new Money(basecalculo));
				}
				item.setValoriss(new Money((item.getValorbciss().getValue().doubleValue() * item.getIss())/100d));
			}
			
			if(item.getIpi() != null){
				if(material.getPercentualBcipi() != null){
					item.setValorbcipi(new Money(basecalculo).multiply(new Money(material.getPercentualBcipi()/100)));
				}else {
					item.setValorbcipi(new Money(basecalculo));
				}
				if(item.getNaocalcularipi() == null || !item.getNaocalcularipi()){
					item.setValoripi(new Money((item.getValorbcipi().getValue().doubleValue() * item.getIpi())/100d));
				}
			} else if(item.getAliquotareaisipi() != null && (item.getNaocalcularipi() == null || !item.getNaocalcularipi())){
				item.setQtdevendidaipi(item.getQtde());
				item.setValoripi(new Money(item.getAliquotareaisipi().getValue().doubleValue() * item.getQtde()));
			}
			
			if(item.getPis() != null){
				if(material.getPercentualBcpis() != null){
					item.setValorbcpis(new Money(basecalculo).multiply(new Money(material.getPercentualBcpis()/100)));
				}else {
					item.setValorbcpis(new Money(basecalculo));
				}
				item.setValorpis(new Money((item.getValorbcpis().getValue().doubleValue() * item.getPis())/100d));
				if(item.getTipocalculopis() != null && Tipocalculo.EM_VALOR.equals(item.getTipocalculopis())){
					item.setQtdevendidapis(item.getQtde());
				}
			} else if(item.getAliquotareaispis() != null){
				item.setQtdevendidapis(item.getQtde());
				item.setValorpis(new Money(item.getAliquotareaispis().getValue().doubleValue() * item.getQtde()));
			}
			
			if(item.getCofins() != null){
				if(material.getPercentualBccofins() != null){
					item.setValorbccofins(new Money(basecalculo).multiply(new Money(material.getPercentualBccofins()/100)));
				}else {
					item.setValorbccofins(new Money(basecalculo));
				}
				item.setValorcofins(new Money((item.getValorbccofins().getValue().doubleValue() * item.getCofins())/100d));
				if(item.getTipocalculocofins() != null && Tipocalculo.EM_VALOR.equals(item.getTipocalculocofins())){
					item.setQtdevendidacofins(item.getQtde());
				}
			} else if(item.getAliquotareaiscofins() != null){
				item.setQtdevendidacofins(item.getQtde());
				item.setValorcofins(new Money(item.getAliquotareaiscofins().getValue().doubleValue() * item.getQtde()));
			}
			
			boolean naoTemIcms = (item.getTributadoicms() != null && !item.getTributadoicms()) ||
					(!Boolean.TRUE.equals(item.getCalcularIcms())) |
					(item.getTipocobrancaicms() != null &&
					(
					Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
					));
			
			boolean naoTemValorIcms = (item.getTributadoicms() != null && !item.getTributadoicms()) ||
					(!Boolean.TRUE.equals(item.getCalcularIcms())) |
					(item.getTipocobrancaicms() != null &&
					(
							Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
							));
			
			boolean naoTemIcmsSt =  (!Boolean.TRUE.equals(item.getTributadoicms())) ||
					(!Boolean.TRUE.equals(item.getCalcularIcms())) ||
					(item.getTipocobrancaicms() != null && 
					(
					Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
					));
			
			
			boolean cobradoAnteriormente = item.getTipocobrancaicms() != null && (
					Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
					);
			
			boolean naoTemFCP = (!Boolean.TRUE.equals(item.getCalcularFcp())) ||
					!(item.getTipocobrancaicms() != null && (
					Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.OUTROS.equals(item.getTipocobrancaicms()) ||
					Tipocobrancaicms.OUTROS_PARTILHA.equals(item.getTipocobrancaicms())
					)); 
			
			if(naoTemFCP){ 
				item.setFcp(null);
				item.setValorfcp(null);
			}
			
			if(naoTemIcms){ 
				item.setValorbcicms(new Money());
				item.setIcms(0d);
			}
			
			if(naoTemValorIcms){
				item.setValoricms(new Money());
			}
			
			if(naoTemIcmsSt){
				item.setValorbcicmsst(new Money());
				item.setIcmsst(0d);
				item.setValoricmsst(new Money());
			}
			
			if(cobradoAnteriormente && material != null && material.getCdmaterial() != null && item.getQtde() != null){
				boolean calcularbcicmsretidoanteriormente = false;
				if(item.getGrupotributacaotrans() != null){
					Grupotributacao grupotributacao = grupotributacaoService.load(item.getGrupotributacaotrans(), "grupotributacao.cdgrupotributacao, grupotributacao.calcularbcicmsretidoanteriormente");
					calcularbcicmsretidoanteriormente = grupotributacao != null && grupotributacao.getCalcularbcicmsretidoanteriormente() != null && grupotributacao.getCalcularbcicmsretidoanteriormente(); 
				}
				
				if(calcularbcicmsretidoanteriormente){
					Aux_imposto aux_imposto = this.buscaValoresCobradosAnteriormenteIcmsst(material, item.getQtde(), item.getGrupotributacaotrans(), null);
					if(aux_imposto != null){
						if(aux_imposto.getValorbcicmsst() != null){
							item.setValorbcicmsst(aux_imposto.getValorbcicmsst());
						}
						if(aux_imposto.getValoricmsst() != null){
							item.setValoricmsst(aux_imposto.getValoricmsst());
						}
					}
				}
			}
			
			if(!Boolean.TRUE.equals(item.getCalcularIpi())) {
				item.setIpi(0d);
				item.setAliquotareaisipi(new Money());
				item.setValoripi(new Money());
				item.setValorbcipi(new Money());
			}
			
			boolean naoTemPis = item.getTipocobrancapis() != null && (
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_04) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_06) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_07) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_08) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_09)
			);
			
			if(naoTemPis){ 
				item.setValorbcpis(new Money());
				item.setPis(0d);
				item.setValorpis(new Money());
			}
			
			boolean naoTemCofins = item.getTipocobrancacofins() != null && (
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_04) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_06) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_07) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_08) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_09)
			);
			
			if(naoTemCofins){ 
				item.setValorbccofins(new Money());
				item.setCofins(0d);
				item.setValorcofins(new Money());
			}
			
			boolean naoTemIss = item.getTributadoicms() != null && item.getTributadoicms();
			
			if(naoTemIss){
				item.setValorbciss(new Money());
				item.setIss(0d);
				item.setValoriss(new Money());
			}
			
			boolean temIcms = item.getTipocobrancaicms() != null && (
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_PARTILHA) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL)
			);
			
			if(temIcms){
				if(item.getValorbcicms() == null) item.setValorbcicms(new Money());
				if(item.getIcms() == null) item.setIcms(0d);
				if(item.getValoricms() == null) item.setValoricms(new Money());
			}
			
			boolean temIcmsSt = item.getTipocobrancaicms() != null && (
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_PARTILHA) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST) ||
					item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL)
			);
			
			if(temIcmsSt){
				if(item.getValorbcicmsst() == null) item.setValorbcicmsst(new Money());
				if(item.getIcmsst() == null) item.setIcmsst(0d);
				if(item.getValoricmsst() == null) item.setValoricmsst(new Money());
			}
			
			boolean temPis = item.getTipocobrancapis() != null && (
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_01) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_02) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_49) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_50) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_51) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_52) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_53) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_54) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_55) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_56) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_60) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_61) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_62) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_63) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_64) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_65) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_66) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_67) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_70) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_71) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_72) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_73) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_74) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_75) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_98) ||
					item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_99)
			);
			
			if(temPis){
				if(item.getValorbcpis() == null) item.setValorbcpis(new Money());
				if(item.getPis() == null) item.setPis(0d);
				if(item.getValorpis() == null) item.setValorpis(new Money());
			}
			
			boolean temCofins = item.getTipocobrancacofins() != null && (
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_01) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_02) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_49) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_50) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_51) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_52) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_53) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_54) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_55) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_56) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_60) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_61) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_62) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_63) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_64) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_65) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_66) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_67) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_70) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_71) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_72) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_73) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_74) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_75) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_98) ||
					item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_99)
			);
			
			if(temCofins){
				if(item.getValorbccofins() == null) item.setValorbccofins(new Money());
				if(item.getCofins() == null) item.setCofins(0d);
				if(item.getValorcofins() == null) item.setValorcofins(new Money());
			}
			
			boolean temIpi = item.getTipocobrancaipi() != null && (
					item.getTipocobrancaipi().equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO) ||
					item.getTipocobrancaipi().equals(Tipocobrancaipi.ENTRADA_OUTRAS) ||
					item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_TRIBUTABA) ||
					item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_OUTRAS)
			);
			
			if(temIpi){
				if(item.getValorbcipi() == null) item.setValorbcipi(new Money());
				if(item.getIpi() == null) item.setIpi(0d);
				if(item.getValoripi() == null) item.setValoripi(new Money());
			}
			
			double valoriss = item.getIncluirissvalor() != null && item.getIncluirissvalor() && item.getValoriss() != null ? item.getValoriss().getValue().doubleValue() : 0d;
			double valoricms = item.getIncluiricmsvalor() != null && item.getIncluiricmsvalor() && item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : 0d;
			double valoripi = item.getIncluiripivalor() != null && item.getIncluiripivalor() && item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
			double valorcofins = item.getIncluircofinsvalor() != null && item.getIncluircofinsvalor() && item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0d;
			double valorpis = item.getIncluirpisvalor() != null && item.getIncluirpisvalor() && item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0d;
			
			double valorbruto = (item.getValorunitario() * item.getQtde()) + valoriss + valoricms + valoripi + valorcofins + valorpis;
			item.setValorbruto(new Money(SinedUtil.round(BigDecimal.valueOf(valorbruto),2)));
			
			if(item.getAliquotacreditoicms() != null && item.getAliquotacreditoicms() > 0 &&
					item.getValorbruto() != null && item.getValorbruto().getValue().doubleValue() > 0 &&
					item.getTipocobrancaicms() != null && 
					item.getTipocobrancaicms().equals(Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO)){
				Double valorcredito = item.getValorbruto().getValue().doubleValue() * item.getAliquotacreditoicms() / 100;
				item.setValorcreditoicms(new Money(valorcredito));
	   		}
			
			if(item.getFcp() != null){
				if(material.getPercentualBcfcp() != null){
					item.setValorbcfcp(new Money(basecalculo).multiply(new Money(material.getPercentualBcfcp()/100)));
				}else {
					item.setValorbcfcp(new Money(basecalculo));
				}
				item.setValorfcp(new Money((item.getValorbcfcp().getValue().doubleValue() * item.getFcp())/100d));
			}
			
			setValorFCPSt(item, material, basecalculo);
		}
	}
	
	private void setValorFCPSt(CalculoImpostoInterface item, Material material, Double basecalculo) {
		boolean temFcpSt = Boolean.TRUE.equals(item.getCalcularFcp()) && item.getTipocobrancaicms() != null && (
				Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.OUTROS.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.OUTROS_PARTILHA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms()));
		
		if(item.getFcpst() != null && temFcpSt){
			Double basecalculoFCP = MoneyUtils.isMaiorQueZero(item.getValorbcicmsst())? item.getValorbcicmsst().doubleValue() : basecalculo;
			if(material.getPercentualBcfcpst() != null){
				item.setValorbcfcpst(new Money(basecalculoFCP).multiply(new Money(material.getPercentualBcfcpst()/100)));
			}else {
				item.setValorbcfcpst(new Money(basecalculoFCP));
			}
			item.setValorfcpst(new Money((item.getValorbcfcpst().getValue().doubleValue() * item.getFcpst())/100d));
		}
	}
	
	/**
	 * M�todo que adiciona informa��es referente a nota do cliente/tipo de pedido e material grupo
	 *
	 * @param nf
	 * @param venda
	 * @author Luiz Fernando
	 */
	public void adicionarInfNota(Notafiscalproduto nf, Venda venda, Empresa empresa) {
		if(nf != null){
			boolean podePreencherInformacoesContribuinte = true;
			if(nf.getCdNota()==null && nf.getNaturezaoperacao()!=null){
				Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(nf.getNaturezaoperacao(), "naturezaoperacao.templateinfcontribuinte");
				if(naturezaoperacao != null && naturezaoperacao.getTemplateinfcontribuinte()!=null){
					podePreencherInformacoesContribuinte = false;
				}
			}
			
			StringBuilder infoadicionalcontrib = new StringBuilder(nf.getInfoadicionalcontrib() != null ? nf.getInfoadicionalcontrib() : "");
			StringBuilder infoadicionalfisco = new StringBuilder(nf.getInfoadicionalfisco() != null ? nf.getInfoadicionalfisco() : "");
			
			//1� - Cliente
			if(nf.getCliente() != null){
				Cliente cliente = clienteService.carregarInfNotaCliente(nf.getCliente());
				if(cliente.getInfoadicionalfisco() != null && !"".equals(cliente.getInfoadicionalfisco())){
					if(infoadicionalfisco.length() == 0) 
						infoadicionalfisco.append(cliente.getInfoadicionalfisco());
					else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalfisco.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(cliente.getInfoadicionalfisco())))
						infoadicionalfisco.append(". ").append(cliente.getInfoadicionalfisco());
				}
				if((podePreencherInformacoesContribuinte) &&
					cliente.getInfoadicionalcontrib() != null && !"".equals(cliente.getInfoadicionalcontrib())){
					if(infoadicionalcontrib.length() == 0) 
						infoadicionalcontrib.append(cliente.getInfoadicionalcontrib());
					else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(cliente.getInfoadicionalcontrib())))
						infoadicionalcontrib.append(". ").append(cliente.getInfoadicionalcontrib());
				}
			}
			
			//2� - Grupo de material
			if(nf.getListaItens() != null && !nf.getListaItens().isEmpty()){
				List<String> listaInfFisco = new ArrayList<String>();
				List<String> listaInfContribuiente = new ArrayList<String>();
				Money totalBcIcmsst = new Money(0.);
				Money totalIcmsst = new Money(0.);
				Boolean exibirSTJaPaga = false;
				for(Notafiscalprodutoitem item : nf.getListaItens()){
					if(item.getGrupotributacaotrans() != null){
						String infoadicionalfiscoGrupo = item.getGrupotributacaotrans().getInfoadicionalfisco();
						String infoadicionalcontribGrupo = item.getGrupotributacaotrans().getInfoadicionalcontrib();
						if(infoadicionalfiscoGrupo != null && !"".equals(infoadicionalfiscoGrupo)){
							infoadicionalfiscoGrupo = infoadicionalfiscoGrupo.trim();
							if(!listaInfFisco.contains(br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalfiscoGrupo)) && !listaInfFisco.contains(infoadicionalfiscoGrupo)){
								listaInfFisco.add(infoadicionalfiscoGrupo);
							}
						}
						if(infoadicionalcontribGrupo != null && !"".equals(infoadicionalcontribGrupo)){
							infoadicionalcontribGrupo = infoadicionalcontribGrupo.trim();
							if(!listaInfContribuiente.contains(br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontribGrupo)) && !listaInfContribuiente.contains(infoadicionalcontribGrupo)){
								listaInfContribuiente.add(infoadicionalcontribGrupo);
							}
						}
					}
					
					if(item.getValorbcicmsst() != null){
						totalBcIcmsst = totalBcIcmsst.add(item.getValorbcicmsst());
					}
					if(item.getValoricmsst() != null){
						totalIcmsst = totalIcmsst.add(item.getValoricmsst());
					}
					
					if(item.getGrupotributacaotrans() != null && item.getGrupotributacaotrans().getExibirmensagemstpagatotal() != null && item.getGrupotributacaotrans().getExibirmensagemstpagatotal()){
							exibirSTJaPaga = true;
					}
				}
				
				String totalizador = "";
				if(exibirSTJaPaga){
					totalizador = "BASE ST J� PAGA: " + totalBcIcmsst.toString() + " ST J� PAGA: " + totalIcmsst.toString();
				}
				
				if(!listaInfFisco.isEmpty()){
					StringBuilder infoAdicinalFiscoStr = new StringBuilder();
					for(String infofisco : listaInfFisco){
						if(infoadicionalfisco.length() == 0 || !br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalfisco.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(infofisco)))
							infoAdicinalFiscoStr.append(infofisco);
					}
					if(infoadicionalfisco.length() == 0) 
						infoadicionalfisco.append(infoAdicinalFiscoStr.toString());
					else if(infoAdicinalFiscoStr.length() > 0)
						infoadicionalfisco.append(". ").append(infoAdicinalFiscoStr.toString());
				}
				if(podePreencherInformacoesContribuinte &&
					!listaInfContribuiente.isEmpty()){
					StringBuilder infoAdicinalContribStr = new StringBuilder();
					for(String infocontrib : listaInfContribuiente){
						if(infoadicionalcontrib.length() == 0 || !br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(infocontrib)))
							infoAdicinalContribStr.append(infocontrib);
					}
					if(infoadicionalcontrib.length() == 0) infoadicionalcontrib.append(infoAdicinalContribStr.toString());
					else if(!"".equals(infoAdicinalContribStr.toString()))
						infoadicionalcontrib.append(". ").append(infoAdicinalContribStr.toString());
				}
				
				if(totalizador != null && totalizador.length() > 0){
					if(infoadicionalcontrib.length() == 0) infoadicionalcontrib.append(totalizador);
					else infoadicionalcontrib.append(". ").append(totalizador);
				}
			}
			
			//3� - Empresa
			if(podePreencherInformacoesContribuinte && empresa!=null){
				Empresa empresa2 = empresaService.load(empresa, "empresa.cdpessoa, empresa.textoinfcontribuinte");
				if(empresa2.getTextoinfcontribuinte() != null && !"".equals(empresa2.getTextoinfcontribuinte())){
					if(infoadicionalcontrib.length() == 0) 
						infoadicionalcontrib.append(empresa2.getTextoinfcontribuinte());
					else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(empresa2.getTextoinfcontribuinte())))
						infoadicionalcontrib.append(". ").append(empresa2.getTextoinfcontribuinte());
				}
			}
			
			//4� - Natureza da opera��o e demais
			if(nf.getNaturezaoperacao() != null){
				Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(nf.getNaturezaoperacao());
				if(podePreencherInformacoesContribuinte &&
					naturezaoperacao.getInfoadicionalcontrib() != null && !"".equals(naturezaoperacao.getInfoadicionalcontrib())){
					if(infoadicionalcontrib.length() == 0) 
						infoadicionalcontrib.append(naturezaoperacao.getInfoadicionalcontrib());
					else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(naturezaoperacao.getInfoadicionalcontrib())))
						infoadicionalcontrib.append(". ").append(naturezaoperacao.getInfoadicionalcontrib());
				}
				if(naturezaoperacao.getInfoadicionalfisco() != null && !"".equals(naturezaoperacao.getInfoadicionalfisco())){
					if(infoadicionalfisco.length() == 0) 
						infoadicionalfisco.append(naturezaoperacao.getInfoadicionalfisco());
					else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalfisco.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(naturezaoperacao.getInfoadicionalfisco())))
						infoadicionalfisco.append(". ").append(naturezaoperacao.getInfoadicionalfisco());
				}
				if(naturezaoperacao.getOperacaonfe() != null){
					nf.setOperacaonfe(naturezaoperacao.getOperacaonfe());
				}
				if(naturezaoperacao.getFinalidadenfe() != null){
					nf.setFinalidadenfe(naturezaoperacao.getFinalidadenfe());
				}
			}
			
			if(venda != null && venda.getCdvenda() != null){
				Venda venda2 = vendaService.carregarInfNotaVenda(venda);
				if(venda2 != null ){
					if(venda2.getPedidovendatipo() != null && venda2.getPedidovendatipo() != null){
						if(venda2.getPedidovendatipo().getInfoadicionalfisco() != null && !"".equals(venda2.getPedidovendatipo().getInfoadicionalfisco())){
							if(infoadicionalfisco.length() == 0) 
								infoadicionalfisco.append(venda2.getPedidovendatipo().getInfoadicionalfisco());
							else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalfisco.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(venda2.getPedidovendatipo().getInfoadicionalfisco())))
								infoadicionalfisco.append(". ").append(venda2.getPedidovendatipo().getInfoadicionalfisco());
						}
						if(podePreencherInformacoesContribuinte &&
							venda2.getPedidovendatipo().getInfoadicionalcontrib() != null && !"".equals(venda2.getPedidovendatipo().getInfoadicionalcontrib())){
							if(infoadicionalcontrib.length() == 0) 
								infoadicionalcontrib.append(venda2.getPedidovendatipo().getInfoadicionalcontrib());
							else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(venda2.getPedidovendatipo().getInfoadicionalcontrib())))
								infoadicionalcontrib.append(". ").append(venda2.getPedidovendatipo().getInfoadicionalcontrib());
						}
					}
					if(podePreencherInformacoesContribuinte &&
						venda2.getObservacao() != null && !"".equals(venda2.getObservacao())){
						if(infoadicionalcontrib.length() == 0) 
							infoadicionalcontrib.append(venda2.getObservacao());
						else if(!br.com.linkcom.utils.StringUtils.retiraQuebras(infoadicionalcontrib.toString()).contains(br.com.linkcom.utils.StringUtils.retiraQuebras(venda2.getObservacao())))
							infoadicionalcontrib.append(". ").append(venda2.getObservacao());
					}
					if(podePreencherInformacoesContribuinte &&
						venda2.getListaEmporiumvenda() != null && venda2.getListaEmporiumvenda().size() > 0){
						if(infoadicionalcontrib.length() == 0) 
							infoadicionalcontrib.append(" Cupom fiscal: ");
						else infoadicionalcontrib.append("Cupom fiscal: ");
						
						for (Emporiumvenda emporiumvenda : venda2.getListaEmporiumvenda()) {
							infoadicionalcontrib.append(emporiumvenda.getNumero_ticket()).append(" ");
						}
					}
				}
			}
			
			if(infoadicionalfisco != null && infoadicionalfisco.length() > 0){
				nf.setInfoadicionalfisco(infoadicionalfisco.toString());
			}
			
			if(podePreencherInformacoesContribuinte && infoadicionalcontrib != null && infoadicionalcontrib.length() > 0){
				nf.setInfoadicionalcontrib(infoadicionalcontrib.toString());
			}
			
			if(podePreencherInformacoesContribuinte && nf.getInfoadicionalcontrib() != null)
				nf.setInfoadicionalcontrib(nf.getInfoadicionalcontrib().trim());
			
			if(nf.getInfoadicionalfisco() != null)
				nf.setInfoadicionalfisco(nf.getInfoadicionalfisco().trim());
		}
	}
	/**
	 * M�todo que retorno o CFOP 5908 ou 6908, caso a venda tenha origem de pedido de venda de comodato
	 *
	 * @param venda
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	/*private Cfop getCfopByPedidovendaComodato(Venda venda, Cliente cliente){
		Cfop cfop = null;
		Venda venda_aux = null;
		if(venda != null && venda.getCdvenda() != null){
			venda_aux = vendaService.loadWithPedidovenda(venda);
		}
		if(venda_aux != null && venda_aux.getPedidovenda() != null && venda_aux.getPedidovenda().getPedidovendatipo() != null && 
				venda_aux.getPedidovenda().getPedidovendatipo().getComodato() != null && 
				venda_aux.getPedidovenda().getPedidovendatipo().getComodato()){
			Endereco enderecoOrigem = null;
			Uf ufOrigem = null;
			if (venda.getEmpresa() != null && venda.getEmpresa().getEndereco() != null && venda.getEmpresa().getEndereco().getCdendereco() != null){
				enderecoOrigem = enderecoService.loadEndereco(venda.getEmpresa().getEndereco());
				if (enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getUf() != null)
					ufOrigem = enderecoOrigem.getMunicipio().getUf();
			}
			
			Endereco enderecoDestino = null;
			Uf ufDestino = null;
			if (cliente != null && cliente.getEndereco() != null && cliente.getEndereco().getCdendereco() != null){
				enderecoDestino = enderecoService.loadEndereco(cliente.getEndereco());
				if (enderecoDestino.getMunicipio() != null && enderecoDestino.getMunicipio().getUf() != null)
					ufDestino = enderecoDestino.getMunicipio().getUf();
			}
			
			if (ufOrigem != null && ufDestino != null){
				if (ufOrigem.getCduf().equals(ufDestino.getCduf())){
					cfop = cfopService.findByCodigo("5908");
				}else {
					cfop = cfopService.findByCodigo("6908");
				}
			}
		}
		return cfop;
	}*/
	
	/**
	 * M�todo que gera as notas de produto com ou sem agrupamento para as vendas
	 * 
	 * @param listaVendas
	 * @param agrupamento
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> getListaNotafiscalproduto(WebRequestContext request, List<Venda> listaVendas, Boolean agrupamento, Boolean somenteproduto, Boolean notaseparada) {
		List<Notafiscalproduto> notas = new ArrayList<Notafiscalproduto>();
		boolean recapagem = SinedUtil.isRecapagem();
		
		HashMap<Material, Localarmazenagem> mapMaterialLocal = getMapMaterialLocal(listaVendas);
		String whereInCdentregamaterial = request.getParameter("whereInCdentregamaterial"); 
		String whereInQtdeEntrada = request.getParameter("whereInQtdeEntrada"); 
		Boolean agruparProdutosNFP = parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_PRODUTOS_NFP);
		Boolean setarColaborador = Boolean.TRUE;
		
		for (Venda venda : listaVendas) {
			vendamaterialService.verificaVendaKitDiscriminarnota(venda);
			vendamaterialService.verificaVendaKitFlexivelExibir(venda);
			
			List<Vendapagamento> listaVendapagamento = vendapagamentoService.findVendaPagamentoByVenda(venda);
			if(SinedUtil.isListNotEmpty(listaVendapagamento) && venda.getQtdeParcelas() == null){
				venda.setQtdeParcelas(listaVendapagamento.size());
			}
			
			if(!recapagem){
				vendamaterialService.verificaVendaProducaoDiscriminarnota(venda);
			}
			if(notavendaService.existNotaEmitida(venda, NotaTipo.NOTA_FISCAL_PRODUTO)){
				continue;
			}
			
			if(parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO)){
				vendaService.buscarTabelaPrecoVenda(venda);
			}
			
			boolean gerarNotaIndustrializacaoRetorno = false;
			boolean referenciarNotaEntrada = false;
			Cfop cfopindustrializacao = null;
			Cfop cfopretorno = null;
			List<Entregamaterial> listaEntregamaterial = null;

			Venda venda_aux = vendaService.loadWithPedidovenda(venda);
			Naturezaoperacao naturezaoperacao = null;
			boolean consideraripiconta = false;
			boolean considerarIcmsStConta = false;
			boolean considerardesoneracaoconta = false;
			boolean considerarFcpStConta = false;
			
			if (venda_aux != null
					&& venda_aux.getPedidovendatipo() != null) {
				if (venda_aux.getPedidovendatipo().getNaturezaoperacao() != null &&
						NotaTipo.NOTA_FISCAL_PRODUTO.equals(venda_aux.getPedidovendatipo().getNaturezaoperacao().getNotaTipo())) {
					naturezaoperacao = venda_aux.getPedidovendatipo().getNaturezaoperacao();
				}
				
				consideraripiconta = venda_aux.getPedidovendatipo().getConsideraripiconta() != null && venda_aux.getPedidovendatipo().getConsideraripiconta();
				considerarIcmsStConta = Boolean.TRUE.equals(venda.getPedidovendatipo().getConsideraricmsstconta());
				considerardesoneracaoconta = Boolean.TRUE.equals(venda.getPedidovendatipo().getConsiderardesoneracaoconta());
				considerarFcpStConta = Boolean.TRUE.equals(venda.getPedidovendatipo().getConsiderarfcpstconta());
				gerarNotaIndustrializacaoRetorno = Boolean.TRUE.equals(venda_aux.getPedidovendatipo().getGerarNotaIndustrializacaoRetorno());
				referenciarNotaEntrada = Boolean.TRUE.equals(venda_aux.getPedidovendatipo().getReferenciarnotaentradananotasaida());
				
				if(gerarNotaIndustrializacaoRetorno){
					cfopindustrializacao = venda_aux.getPedidovendatipo().getCfopindustrializacao();
					cfopretorno = venda_aux.getPedidovendatipo().getCfopretorno();
				}
				if(gerarNotaIndustrializacaoRetorno || referenciarNotaEntrada){
					if (whereInCdentregamaterial != null && !whereInCdentregamaterial.equals("")) {
						listaEntregamaterial = entregamaterialService.findForIndustrializacaoRetorno(whereInCdentregamaterial, null);
						
						String[] idEntregamaterial = whereInCdentregamaterial.split(",");
						String[] qtdeEntradamaterial = whereInQtdeEntrada.split("\\|");
						
						for (int i = 0; i < idEntregamaterial.length; i++) {
							Integer cdentregamaterial = Integer.parseInt(idEntregamaterial[i]);
							for (Entregamaterial entregamaterial : listaEntregamaterial) {
								if(entregamaterial.getCdentregamaterial() != null && 
										entregamaterial.getCdentregamaterial().equals(cdentregamaterial)){
									entregamaterial.setQtdeNotaIndustrializacao(Double.parseDouble(qtdeEntradamaterial[i].replaceAll("\\.", "").replaceAll(",", ".")));
									break;
								}
							}
							
						}
					}
				}
			}
			
			boolean vendaPossuiSomenteServicos = true;
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				boolean servico = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getServico() != null && vendamaterial.getMaterial().getServico();
				boolean tributacaomunicipal = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaomunicipal() != null && vendamaterial.getMaterial().getTributacaomunicipal();
				
				if(somenteproduto != null && somenteproduto && vendamaterial.getMaterial() != null && (servico || tributacaomunicipal)){
					continue;
				}
				vendaPossuiSomenteServicos = false;
			}
			if(vendaPossuiSomenteServicos){
				continue;
			}
					
			
			if (naturezaoperacao == null) {
				naturezaoperacao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO);
			}
			
			ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(naturezaoperacao);
			
			List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(venda);
			for (Documentoorigem documentoorigem : listadocumentoorigem) {
				Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
				if (documento.getDocumentoacao().equals(Documentoacao.PREVISTA)){
					documento.setDocumentoacao(Documentoacao.DEFINITIVA);
					documentoService.updateDocumentoacao(documento);
				}
			}
			
			Notafiscalproduto nf = null;
			
			if(agrupamento){
				for (Notafiscalproduto notafiscalproduto : notas) {
					//Condi��o para agrupamento
					boolean condicaoNaturezaoperacao = false;
					
					if (naturezaoperacao==null && notafiscalproduto.getNaturezaoperacao()==null){
						condicaoNaturezaoperacao = true;
					}else if (naturezaoperacao!=null 
								&& notafiscalproduto.getNaturezaoperacao()!=null
								&& naturezaoperacao.getCdnaturezaoperacao().equals(notafiscalproduto.getNaturezaoperacao().getCdnaturezaoperacao())){
						condicaoNaturezaoperacao = true;
					}
					boolean cliente = notafiscalproduto.getCliente().equals(venda.getCliente()) || clienteService.isMatrizCnpjIgual(notafiscalproduto.getCliente(), venda.getCliente());
					
					ResponsavelFrete responsavelfretevenda = venda.getFrete() == null ? ResponsavelFrete.SEM_FRETE : venda.getFrete();
					boolean frete = notafiscalproduto.getResponsavelfrete().equals(responsavelfretevenda);
					boolean terceiros = (venda.getTerceiro() == null && notafiscalproduto.getTransportador() == null) || 
										(venda.getTerceiro() == null && notafiscalproduto.getTransportador() != null && venda.getEmpresa().getTransportador() != null && 
												venda.getEmpresa().getTransportador().equals(notafiscalproduto.getTransportador())) 
										|| (venda.getTerceiro() != null && venda.getTerceiro().equals(notafiscalproduto.getTransportador()));
					
					if(cliente && terceiros && frete && condicaoNaturezaoperacao){
						nf = notafiscalproduto;
						nf.getListaNotaHistorico().get(0).setObservacao(nf.getListaNotaHistorico().get(0).getObservacao().replace(".", ", ")+"<a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>.");
						break;
					}else {
						request.addMessage("N�o foi poss�vel agrupar as vendas em uma nota:", MessageType.WARN);
						if(!cliente){
							request.addMessage("Os clientes s�o diferentes.", MessageType.WARN);
						}
						if(!frete){
							request.addMessage("Os fretes s�o diferentes.", MessageType.WARN);
						}
						if(!terceiros){
							request.addMessage("Os terceiros s�o diferentes.", MessageType.WARN);
						}
						if(!condicaoNaturezaoperacao){
							request.addMessage("A natureza das opera��es s�o diferentes.", MessageType.WARN);
						}
					}
				}
			}
			
			if(nf == null){
				nf = new Notafiscalproduto();
				nf.setGerarNotaIndustrializacaoRetorno(gerarNotaIndustrializacaoRetorno);
				
				NotaHistorico notaHistorico = new NotaHistorico(nf, "Visualizar venda: <a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>.", NotaStatus.EMITIDA, ((Usuario)Neo.getUser()).getCdpessoa(), new Timestamp(System.currentTimeMillis()));
				List<NotaHistorico> lista = new ArrayList<NotaHistorico>();
				lista.add(notaHistorico);
				nf.setListaNotaHistorico(lista);
				
				nf.setListaItens(new ArrayList<Notafiscalprodutoitem>());
				notas.add(nf);
			}
			nf.getListaVenda().add(venda);
			
			Empresa empresa = venda.getEmpresa();
			if(empresa == null) empresa = empresaService.loadPrincipal();
			
			empresa = empresaService.loadWithEndereco(empresa);
			Endereco enderecoEmpresa = empresa.getEndereco();
			
			if(enderecoEmpresa != null){
				nf.setMunicipiogerador(enderecoEmpresa.getMunicipio());
			}
			
			if(empresa != null){
				Empresa empresa_aux = empresaService.load(empresa, "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf, empresa.serienfe");
				
				if(empresa_aux.getNaopreencherdtsaida() == null || !empresa_aux.getNaopreencherdtsaida()){
					nf.setDatahorasaidaentrada(SinedDateUtils.currentTimestamp());
				}
				
				nf.setMarcavolume(empresa_aux.getMarcanf());
				nf.setEspvolume(empresa_aux.getEspecienf());
				nf.setSerienfe(empresa_aux.getSerienfe());
			}
			nf.setEmpresa(empresa);
			nf.setNaturezaoperacao(naturezaoperacao);
			if(naturezaoperacao != null && Util.strings.isNotEmpty(naturezaoperacao.getRazaosocial())){
				nf.setRazaosocialalternativa(naturezaoperacao.getRazaosocial());
			}
			nf.setOperacaonfe(ImpostoUtil.getOperacaonfe(nf.getCliente(), naturezaoperacao));
			
			if(naturezaoperacao != null && naturezaoperacao.getFinalidadenfe() != null){
				nf.setFinalidadenfe(naturezaoperacao.getFinalidadenfe());
			} else {
				nf.setFinalidadenfe(Finalidadenfe.NORMAL);
			}
			nf.setPresencacompradornfe(venda.getPresencacompradornfe());
			nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
			
			notafiscalprodutoautorizacaoService.preencheAbaAutorizacaoByParametro(nf);
			
			if(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null)
				nf.setUfveiculo(enderecoEmpresa.getMunicipio().getUf());
			
			Cliente cliente = clienteService.carregarDadosCliente(venda.getCliente());
			Endereco enderecoCliente = venda.getEnderecoFaturamento();
			Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
			if(Util.objects.isPersistent(enderecoCliente)){
				enderecoCliente = enderecoService.carregaEnderecoComUfPais(enderecoCliente);
			}
			
			if(Util.objects.isNotPersistent(enderecoCliente)){
				Set<Endereco> listaEndereco = cliente.getListaEndereco();
				if(listaEndereco != null && listaEndereco.size() > 0){
					for (Endereco end : listaEndereco) {
						if(end.getEnderecotipo() != null && end.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
							enderecoCliente = end;
							break;
						}
					}
				}
			}
			
			if(enderecoCliente == null && venda.getEndereco() != null){
				enderecoCliente = enderecoService.carregaEnderecoComUfPais(venda.getEndereco());
			}
			nf.setCliente(cliente);
			nf.setEnderecoCliente(enderecoCliente);
			nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
			nf.setTipooperacaonota(Tipooperacaonota.SAIDA);
			nf.setProjeto(venda.getProjeto());
			
			if (setarColaborador) {
				if (nf.getColaborador() == null) {
					nf.setColaborador(venda.getColaborador());
				} else {
					if (!nf.getColaborador().equals(venda.getColaborador())) {
						nf.setColaborador(null);
						setarColaborador = Boolean.FALSE;
					}
				}
			}
			
			if(venda.getFrete() != null){
				nf.setResponsavelfrete(venda.getFrete());
				if(nf.getResponsavelfrete().equals(ResponsavelFrete.TERCEIROS)){
					nf.setCadastrartransportador(Boolean.TRUE);
				}
				if(nf.getValorfrete() == null)
					nf.setValorfrete(new Money(0.0));
				if(venda.getValorfrete() == null)
					venda.setValorfrete(new Money(0.0));
				nf.setValorfrete(nf.getValorfrete().add(venda.getValorfrete()));
			}else{
				nf.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
			}			
			
			if(venda.getTerceiro() != null){
				nf.setTransportador(venda.getTerceiro());
			}else if(venda.getEmpresa() != null && venda.getEmpresa().getTransportador() != null){
				nf.setTransportador(venda.getEmpresa().getTransportador());
			}
			
			if(nf.getTransportador() != null){
				Fornecedor fornecedor = fornecedorService.carregaFornecedor(nf.getTransportador());
				Endereco enderecoFornecedor = fornecedor.getListaEndereco() != null && fornecedor.getListaEndereco().size() == 1 ? fornecedor.getListaEndereco().iterator().next() : null;
				if(nf.getEnderecotransportador() == null && enderecoFornecedor != null && !Enderecotipo.INATIVO.equals(enderecoFornecedor.getEnderecotipo())){
					nf.setEnderecotransportador(enderecoFornecedor);
				}
				if (org.apache.commons.lang.StringUtils.isBlank(nf.getTelefonetransportador())){
					Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(fornecedor));
					if(listaTelefone != null && listaTelefone.size() == 1){
						nf.setTelefonetransportador(listaTelefone.iterator().next().getTelefone());
					}
				}
			}
			
			if(venda.getTaxavenda() != null){
				if(nf.getTaxaVenda() == null){
					nf.setTaxaVenda(venda.getTaxavenda());
				}else {
					nf.setTaxaVenda(nf.getTaxaVenda().add(venda.getTaxavenda()));
				}
			}
			
			Money outrasDespesasIt = new Money();
			boolean considerarJurosNota = false;
			if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getConsiderarjurosnota() != null && venda.getPrazopagamento().getConsiderarjurosnota()){
				venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
				
				Money totalParcela = venda.getTotalparcela();
				Money totalProdutos = venda.getTotalvenda();
				vendaService.setTotalImpostos(venda);
				if(consideraripiconta){
					totalProdutos = totalProdutos.add(venda.getTotalipi());
				}
				if(considerarIcmsStConta){
					totalProdutos = totalProdutos.add(venda.getTotalIcmsSt());
				}
				if(considerardesoneracaoconta){
					totalProdutos = totalProdutos.subtract(venda.getTotalDesoneracaoIcms());
				}
				if(considerarFcpStConta){
					totalProdutos = totalProdutos.add(venda.getTotalFcpSt());
				}
				
				Money diferencaJuros = totalParcela.subtract(totalProdutos);
				if(diferencaJuros.getValue().compareTo(BigDecimal.ZERO) > 0){
					considerarJurosNota = true;
					outrasDespesasIt = diferencaJuros.divide(new Money(venda.getListavendamaterial().size(), false));
				}
			}
			
			Double pesoliquidoTotal = 0d;
			Double pesobrutoTotal = 0d;
			Double qtdeTotal = 0d;
			Money descontoTotal = new Money();
			
			List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
			Cfop cfop = null;
			Boolean temServico = Boolean.FALSE;
			
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				boolean servico = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getServico() != null && vendamaterial.getMaterial().getServico();
				boolean tributacaomunicipal = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaomunicipal() != null && vendamaterial.getMaterial().getTributacaomunicipal();
				
				if(somenteproduto != null && somenteproduto && vendamaterial.getMaterial() != null && (servico || tributacaomunicipal)){
					venda.setExisteServico(true);
					temServico = Boolean.TRUE;
					continue;
				}
				
				Material material = vendamaterial.getMaterial();
				Notafiscalprodutoitem notafiscalprodutoitem = null;
				if(agruparProdutosNFP){
					
					Double precoUnico = null;
					if (!Boolean.TRUE.equals(cliente.getDiscriminardescontonota())){
						Double multiplicador = vendamaterial.getMultiplicador();
						if(multiplicador == null) multiplicador = 1.0;
						Money valorDescontoItemUnico = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
						Money valorDescontoUnico = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
						valorDescontoItemUnico = valorDescontoItemUnico.divide(new Money(vendamaterial.getQuantidade()*multiplicador));
						valorDescontoUnico = valorDescontoUnico.add(valorDescontoItemUnico);
						
						precoUnico = vendamaterial.getPreco() - valorDescontoUnico.getValue().doubleValue();
					}
					
					for (Notafiscalprodutoitem notafiscalprodutoitemAux : nf.getListaItens()) {
						if((notafiscalprodutoitemAux.getIsItemRetorno() == null || !notafiscalprodutoitemAux.getIsItemRetorno()) && 
								notafiscalprodutoitemAux.getMaterial().equals(vendamaterial.getMaterial()) && 
								notafiscalprodutoitemAux.getUnidademedida() != null &&
								notafiscalprodutoitemAux.getUnidademedida().equals(vendamaterial.getUnidademedida()) && 
								((precoUnico == null && notafiscalprodutoitemAux.getValorunitario().doubleValue() == vendamaterial.getPreco().doubleValue()) ||
								(precoUnico != null && notafiscalprodutoitemAux.getValorunitario().doubleValue() == precoUnico)) &&
									(
										(org.apache.commons.lang.StringUtils.isBlank(notafiscalprodutoitemAux.getIdentificadorespecifico()) && 
										 org.apache.commons.lang.StringUtils.isBlank(vendamaterial.getIdentificadorespecifico())) ||
										(notafiscalprodutoitemAux.getIdentificadorespecifico() != null && 
										 vendamaterial.getIdentificadorespecifico() != null &&
										 vendamaterial.getIdentificadorespecifico().equals(notafiscalprodutoitemAux.getIdentificadorespecifico()))
									)
								){
							notafiscalprodutoitem = notafiscalprodutoitemAux;
							break;
						}
					}
				}
				Double quantidade = vendamaterial.getQuantidade().doubleValue();
				if(notafiscalprodutoitem == null){
					Notafiscalprodutoitem item = new Notafiscalprodutoitem();
					
					if(vendamaterial.getCdvendamaterial() != null)
						item.setVendamaterial(vendamaterial);
					if(vendamaterial.getVendamaterialmestre() != null && 
							vendamaterial.getVendamaterialmestre().getCdvendamaterialmestre() != null){
						item.setVendamaterialmestre(vendamaterial.getVendamaterialmestre());
					}
					
					Double multiplicador = vendamaterial.getMultiplicador();
					if(multiplicador == null) multiplicador = 1.0;
					
					item.setNotafiscalproduto(nf);
					item.setMaterial(material);
					item.setIdentificadorespecifico(vendamaterial.getIdentificadorespecifico());
					item.setLocalarmazenagem(mapMaterialLocal.get(material));
					if(parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO)){
						if(vendamaterial.getIdentificadorfabricante() != null){
							item.setItempedidocompra(vendamaterial.getIdentificadorfabricante());
						}else if(vendamaterial.getMaterialtabelaprecoitem() != null && vendamaterial.getMaterialtabelaprecoitem().getIdentificadorfabricante() != null){
							item.setItempedidocompra(vendamaterial.getMaterialtabelaprecoitem().getIdentificadorfabricante());
						}
					}
					
					if(vendamaterial.getValorcustomaterial() != null && item.getValorcustomaterial() == null){
						item.setValorcustomaterial(vendamaterial.getValorcustomaterial());
					}
					
					if(material.getCodigoanp() != null){
						item.setCodigoanp(material.getCodigoanp());
						item.setDadoscombustivel(Boolean.TRUE);
					}
					
					if(material.getEpi() != null && material.getEpi()){
						item.setMaterialclasse(Materialclasse.EPI);
					}else if(material.getPatrimonio() != null && material.getPatrimonio() && (material.getProduto() == null || !material.getProduto())){
						item.setMaterialclasse(Materialclasse.PATRIMONIO);
					}else if(material.getProduto() != null && material.getProduto() && (material.getPatrimonio() == null || !material.getPatrimonio())){
						item.setMaterialclasse(Materialclasse.PRODUTO);
					}else if(material.getServico() != null && material.getServico()){
						item.setMaterialclasse(Materialclasse.SERVICO);
					}
					item.setQtde(quantidade);
					item.setValorunitario(vendamaterial.getPreco());
					item.setIncluirvalorprodutos(true);
					item.setPatrimonioitem(vendamaterial.getPatrimonioitem());
					item.setValorfrete(venda.getValorfrete() != null && venda.getValorfrete().getValue().doubleValue() > 0.0 ? new Money(SinedUtil.truncateFloor(venda.getValorfrete().getValue().doubleValue()/venda.getListavendamaterial().size())) : new Money(0.0));
					item.setUnidademedida(vendamaterial.getUnidademedida());
					item.setValorseguro(vendamaterial.getValorSeguro());
					item.setOutrasdespesas(vendamaterial.getOutrasdespesas());
					item.setValorComissao(vendamaterial.getValorComissao());
					
					qtdeTotal += quantidade;
					
					Double peso = vendamaterial.getPesoVendaOuMaterial();
					Double pesobruto = material.getPesobruto();
			
					if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){
						Material aux_material = materialService.findListaMaterialunidademedida(item.getMaterial());
						Unidademedida aux_unidademedida = item.getUnidademedida();
						
						if(aux_unidademedida != null && pesobruto != null){
							Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
							if(!unidadeprincipal){
								Double fracao = vendamaterial.getFatorconversaoQtdereferencia();
								if(fracao != null && fracao > 0){
									pesobruto = pesobruto/fracao;
								}
							}
						}
					}else if(peso != null && vendamaterial.getFatorconversao() != null){
						peso = peso * vendamaterial.getFatorconversaoQtdereferencia();
					}
					
					if(peso != null && peso > 0){
						peso = quantidade * peso;
						if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							peso = quantidade;
						}
						peso = SinedUtil.roundByParametro(peso);
						
						item.setPesoliquido(peso);
						pesoliquidoTotal += peso;
					}
					if(pesobruto != null && pesobruto > 0){
						pesobruto = quantidade * pesobruto;
						if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							pesobruto = quantidade;
						}
						pesobruto = SinedUtil.roundByParametro(pesobruto);
						
						item.setPesobruto(pesobruto);
						pesobrutoTotal += pesobruto;
					}
					
					if(considerarJurosNota){
						item.setOutrasdespesas(outrasDespesasIt);
					}
					
					item.setLoteestoque(vendamaterial.getLoteestoque());
					
					StringBuilder infoAdicional = new StringBuilder();

					if(vendamaterial.getLoteestoque() != null && vendamaterial.getLoteestoque().getNumero() != null){
						infoAdicional.append(" Lote: " + vendamaterial.getLoteestoque().getNumero());
					}
					
					if (parametrogeralService.getValorPorNome(Parametrogeral.PREENCHERINFOADICIONALCOMNUMVENDA).equalsIgnoreCase("TRUE")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("Venda N�: " + venda.getCdvenda());
					}
					
					if(vendamaterial.getObservacao() != null && 
							!vendamaterial.getObservacao().trim().equals("")){
						infoAdicional.append(vendamaterial.getObservacao()+" ");
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getPneumarca() != null && 
							vendamaterial.getPneu().getPneumarca().getNome() != null && 
							!vendamaterial.getPneu().getPneumarca().getNome().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("Marca: " + vendamaterial.getPneu().getPneumarca().getNome());
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getSerie() != null && 
							!vendamaterial.getPneu().getSerie().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("S�rie/Fogo: " + vendamaterial.getPneu().getSerie());
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getDot() != null && 
							!vendamaterial.getPneu().getDot().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("DOT: " + vendamaterial.getPneu().getDot());
					}
					if(vendamaterial.getPneu() != null && 
							vendamaterial.getPneu().getPneumedida() != null && 
							vendamaterial.getPneu().getPneumedida().getNome() != null &&
							!vendamaterial.getPneu().getPneumedida().getNome().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("Medida: " + vendamaterial.getPneu().getPneumedida().getNome());
					}
					
					item.setInfoadicional(infoAdicional.toString().trim());
					
					item.setNcmcapitulo(material.getNcmcapitulo());
					item.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
					item.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
					item.setNve(material.getNve() != null ? material.getNve() : "");
                    this.preencheLocalDestinoNota(nf);
					if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
						List<Categoria> listaCategoriaCliente = null;
						if(cliente != null){
							listaCategoriaCliente = categoriaService.findByPessoa(cliente);
						}
						
						Material beanMaterial = materialService.loadForTributacao(item.getMaterial());
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
														empresa, 
														nf.getNaturezaoperacao(), 
														beanMaterial.getMaterialgrupo(),
														cliente,
														true, 
														beanMaterial, 
														item.getNcmcompleto(),
														enderecoCliente, 
														null,
														listaCategoriaCliente,
														null,
														nf.getOperacaonfe(),
														nf.getModeloDocumentoFiscalEnum(),
														nf.getLocaldestinonfe(),
														nf.getDtEmissao()));
						item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao, vendamaterial.getGrupotributacao()));
					}
					item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
					
					if (nf.getNaturezaoperacao()!=null && nf.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
						Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoEmpresa, nf.getEnderecoCliente());
						List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(nf.getNaturezaoperacao(), cfopescopo);
						if (!listaNaturezaoperacaocfop.isEmpty()){
							item.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
						}
					}
					
					Money valorDesconto = new Money();
					Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
					
					if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
						valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
						valorDesconto = valorDesconto.add(valorDescontoItem);
						descontoTotal = descontoTotal.add(valorDesconto);
						
						item.setValordesconto(new Money(SinedUtil.round(valorDesconto.getValue(), 2)));
						if(item.getValorunitario() == null || item.getValorunitario() == 0){
							item.setValorunitario(vendamaterial.getPreco());
						}
					} else {
						valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
						valorDescontoItem = valorDescontoItem.divide(new Money(vendamaterial.getQuantidade()*multiplicador));
						valorDesconto = valorDesconto.add(valorDescontoItem);
						
						if(item.getValorunitario() == null || item.getValorunitario().doubleValue() == 0.0){
							item.setValorunitario(vendamaterial.getPreco() - valorDesconto.getValue().doubleValue());
						}else {
							item.setValorunitario(item.getValorunitario() - valorDesconto.getValue().doubleValue());
						}
					}
					
					if(multiplicador != null){
						if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
							item.setQtde(item.getQtde() * multiplicador); 
						} else {
							item.setValorunitario(item.getValorunitario() * multiplicador);
						}
					}
					item.setValorunitario(SinedUtil.round(item.getValorunitario(), 10));
					
					if(vendamaterial.getQuantidade() != null && vendamaterial.getPreco() != null)
						item.setValorbruto(new Money(quantidade*vendamaterial.getPreco().doubleValue()));

					
					if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
						item.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
					
					setValoresImpostoVenda(item, vendamaterial, false, venda.getEmpresa());
					setBaseCalculoItem(material, item);
					item.setOperacaonfe(nf.getOperacaonfe());
					grupotributacaoService.calcularValoresFormulaIpi(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
					grupotributacaoService.calcularValoresFormulaIcms(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
					calculaMva(item, material);

					if(cfop == null && item.getCfop() != null && item.getCfop().getCdcfop() != null){
						cfop = item.getCfop();
					}
					
					if(!gerarNotaIndustrializacaoRetorno && referenciarNotaEntrada){
						includeInfoAdicionalNotaEntrada(referenciarNotaEntrada, listaEntregamaterial, item);
					}
					
					grupotributacaoService.calcularValoresFormulaIcmsST(item, item.getMaterial().getGrupotributacaoNota() != null ? item.getMaterial().getGrupotributacaoNota() : item.getGrupotributacao());
					listaItens.add(item);
					verificaNotaIndustrializacao(nf, item, cfopindustrializacao, cfopretorno, listaEntregamaterial, listaItens, referenciarNotaEntrada);
					
					item.setInformacaoAdicionalProdutoBean(criaBeanInfoProduto(vendamaterial.getMaterial(), vendamaterial.getPneu(), null, vendamaterial, venda, vendamaterial.getPedidovendamaterial(), venda.getPedidovenda()));
					String templateInfoAdicionalItem = montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
					if(templateInfoAdicionalItem != null){
						item.setInfoadicional(templateInfoAdicionalItem);
						item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
					}else {
						item.setInfoAdicionalItemAnterior("");
					}
				} else {
					Double multiplicador = vendamaterial.getMultiplicador(); 
					if(multiplicador == null) multiplicador = 1.0;
					
					if(multiplicador != null){
						if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
							notafiscalprodutoitem.setQtde(notafiscalprodutoitem.getQtde()+(quantidade*multiplicador));
						}else {
							notafiscalprodutoitem.setQtde(notafiscalprodutoitem.getQtde()+quantidade);
						}
					}
					
					notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
					notafiscalprodutoitem.setValorfrete(notafiscalprodutoitem.getValorfrete().add(venda.getValorfrete() != null && venda.getValorfrete().getValue().doubleValue() > 0.0 ? new Money(venda.getValorfrete().getValue().doubleValue()/venda.getListavendamaterial().size()) : new Money(0.0)));
					
					if(vendamaterial.getPesoVendaOuMaterial() != null && vendamaterial.getPesoVendaOuMaterial() > 0){
						Double pesoliquido = quantidade * vendamaterial.getPesoVendaOuMaterial();
						
						notafiscalprodutoitem.setPesoliquido((notafiscalprodutoitem.getPesoliquido() != null ? notafiscalprodutoitem.getPesoliquido() : 0d) + pesoliquido);
						pesoliquidoTotal += pesoliquido;
					}
					if(notafiscalprodutoitem.getMaterial().getPesobruto() != null && notafiscalprodutoitem.getMaterial().getPesobruto() > 0){
						Double pesobruto = quantidade * notafiscalprodutoitem.getMaterial().getPesobruto();
						
						notafiscalprodutoitem.setPesobruto(notafiscalprodutoitem.getPesobruto() + pesobruto);
						pesobrutoTotal += pesobruto;
					}
					
					Money valorDesconto = new Money();
					Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
					
					if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
						valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
						valorDesconto = valorDesconto.add(valorDescontoItem);
						descontoTotal = descontoTotal.add(valorDesconto);
						
						if(notafiscalprodutoitem.getValordesconto() != null){
							notafiscalprodutoitem.setValordesconto(notafiscalprodutoitem.getValordesconto().add(new Money(SinedUtil.round(valorDesconto.getValue(), 2))));
						}else {
							notafiscalprodutoitem.setValordesconto(new Money(SinedUtil.round(valorDesconto.getValue(), 2)));
						}
					} else {
						valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);

//						Double multiplicador = vendamaterial.getMultiplicador();
//						if(multiplicador == null) multiplicador = 1.0;
						
						if(notafiscalprodutoitem.getQtde() != null && notafiscalprodutoitem.getQtde() != 0){
							valorDescontoItem = valorDescontoItem.divide(new Money(notafiscalprodutoitem.getQtde()*multiplicador));
						}else {
							valorDescontoItem = valorDescontoItem.divide(new Money(vendamaterial.getQuantidade()*multiplicador));
						}
						valorDesconto = valorDesconto.add(valorDescontoItem);
						
						if(notafiscalprodutoitem.getValorunitario() == null || notafiscalprodutoitem.getValorunitario().doubleValue() == 0.0){
							notafiscalprodutoitem.setValorunitario(vendamaterial.getPreco() - valorDesconto.getValue().doubleValue());
							notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
						}else {
							notafiscalprodutoitem.setValorunitario(notafiscalprodutoitem.getValorunitario() - valorDesconto.getValue().doubleValue());
							notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
						}
					}
					qtdeTotal += quantidade;				
					
					StringBuilder infoAdicional = new StringBuilder();
					infoAdicional.append(notafiscalprodutoitem.getInfoadicional());
					if(vendamaterial.getObservacao() != null && !vendamaterial.getObservacao().isEmpty()){
						infoAdicional.append(" "+vendamaterial.getObservacao()+" ");
					}
					if (parametrogeralService.getValorPorNome(Parametrogeral.PREENCHERINFOADICIONALCOMNUMVENDA).equalsIgnoreCase("TRUE")){
						if (infoAdicional.toString().contains("Venda N�: ")){
							notafiscalprodutoitem.setInfoadicional(infoAdicional.toString().replace("Venda N�: ", "Venda N�: " + venda.getCdvenda()+", "));
						}
						else{
							if(infoAdicional.length() > 0) infoAdicional.append(" ");
							infoAdicional.append("Venda N�: " + venda.getCdvenda());
							notafiscalprodutoitem.setInfoadicional(infoAdicional.toString().trim());
						}
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getPneumarca() != null && 
							vendamaterial.getPneu().getPneumarca().getNome() != null && 
							!vendamaterial.getPneu().getPneumarca().getNome().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("Marca: " + vendamaterial.getPneu().getPneumarca().getNome());
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getSerie() != null && 
							!vendamaterial.getPneu().getSerie().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("S�rie/Fogo: " + vendamaterial.getPneu().getSerie());
					}
					if(vendamaterial.getPneu() != null &&
							vendamaterial.getPneu().getDot() != null && 
							!vendamaterial.getPneu().getDot().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("DOT: " + vendamaterial.getPneu().getDot());
					}
					if(vendamaterial.getPneu() != null && 
							vendamaterial.getPneu().getPneumedida() != null && 
							vendamaterial.getPneu().getPneumedida().getNome() != null &&
							!vendamaterial.getPneu().getPneumedida().getNome().trim().equals("")){
						if(infoAdicional.length() > 0) infoAdicional.append(" ");
						infoAdicional.append("Medida: " + vendamaterial.getPneu().getPneumedida().getNome());
					}
					
					notafiscalprodutoitem.setInfoadicional(infoAdicional.toString().length() > 500 ? infoAdicional.toString().substring(0, 499) : infoAdicional.toString());
					if(notafiscalprodutoitem.getValorunitario() != null && notafiscalprodutoitem.getQtde() != null){
						setValoresImpostoVenda(notafiscalprodutoitem, vendamaterial, true, venda.getEmpresa());
						setBaseCalculoItem(material, notafiscalprodutoitem);
						grupotributacaoService.calcularValoresFormulaIpi(notafiscalprodutoitem, notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() != null ? notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() : notafiscalprodutoitem.getGrupotributacao());
						grupotributacaoService.calcularValoresFormulaIcms(notafiscalprodutoitem, notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() != null ? notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() : notafiscalprodutoitem.getGrupotributacao());
						calculaMva(notafiscalprodutoitem, material);
					}
					if(!gerarNotaIndustrializacaoRetorno && referenciarNotaEntrada){
						includeInfoAdicionalNotaEntrada(referenciarNotaEntrada, listaEntregamaterial, notafiscalprodutoitem);
					}
					
					grupotributacaoService.calcularValoresFormulaIcmsST(notafiscalprodutoitem, notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() != null ? notafiscalprodutoitem.getMaterial().getGrupotributacaoNota() : notafiscalprodutoitem.getGrupotributacao());
					
					verificaNotaIndustrializacao(nf, notafiscalprodutoitem, cfopindustrializacao, cfopretorno, listaEntregamaterial, nf.getListaItens(), referenciarNotaEntrada);
					
					notafiscalprodutoitem.setInformacaoAdicionalProdutoBean(criaBeanInfoProduto(vendamaterial.getMaterial(), vendamaterial.getPneu(), null, vendamaterial, venda, vendamaterial.getPedidovendamaterial(), venda.getPedidovenda()));
					String templateInfoAdicionalItem = montaInformacoesAdicionaisItemTemplate(notafiscalprodutoitem.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
					if(templateInfoAdicionalItem != null){
						notafiscalprodutoitem.setInfoadicional(templateInfoAdicionalItem);
						notafiscalprodutoitem.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
					}else {
						notafiscalprodutoitem.setInfoAdicionalItemAnterior("");
					}
				}
			}
			nf.getListaItens().addAll(listaItens);
			
			if(!agrupamento){
				Money descontoReal = new Money();
				for (Notafiscalprodutoitem pi : nf.getListaItens()) {
					if(pi.getValordesconto() != null){
						descontoReal = descontoReal.add(pi.getValordesconto());
					}
				}
				
				double descontoRealDbl = descontoReal.getValue().doubleValue();
				double descontoTotalDbl = descontoTotal.getValue().doubleValue();
				if(descontoRealDbl != descontoTotalDbl){
					double diferenca = descontoTotalDbl - descontoRealDbl;
					if(diferenca != 0d && nf.getListaItens() != null && nf.getListaItens().size() > 0){
						Notafiscalprodutoitem notafiscalprodutoitem = nf.getListaItens().get(0);
						notafiscalprodutoitem.setValordesconto(notafiscalprodutoitem.getValordesconto().add(new Money(diferenca)));
					}
				}
			}

			if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
				nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			if(pesobrutoTotal != null && pesobrutoTotal > 0){
				nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			
			if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP))){
				nf.setCadastrartransportador(Boolean.TRUE);
				if(venda.getQuantidadevolumes() != null){
					if(nf.getQtdevolume() != null){
						nf.setQtdevolume(nf.getQtdevolume() + venda.getQuantidadevolumes());
					}else {
						nf.setQtdevolume(venda.getQuantidadevolumes());
					}
				}				
			}else{
				if(qtdeTotal != null && qtdeTotal > 0){
					if(nf.getQtdevolume() == null){
						nf.setQtdevolume(qtdeTotal.intValue());					
					}
					nf.setCadastrartransportador(Boolean.TRUE);
				}				
			}
			
			if(nf.getValorfrete() != null && nf.getListaItens() != null && nf.getListaItens().size() > 0){
				Money valorfreteItens = new Money();
				for (Notafiscalprodutoitem item : nf.getListaItens()) {
					valorfreteItens = valorfreteItens.add(item.getValorfrete());
				}
				nf.getListaItens().get(0).setValorfrete(nf.getListaItens().get(0).getValorfrete().add(nf.getValorfrete().subtract(valorfreteItens)));
			}
			
			Money totalIi = new Money();
			Money totalIpi = new Money();
			Money totalIcmsst = new Money();
			Money totalSeguro = new Money();
			Money totalFrete = new Money();
			Money totalDesconto = new Money();
			Money totalOutrasdespesas = new Money();
			Money totalProdutos = new Money(0.0);
			for (Notafiscalprodutoitem pi : nf.getListaItens()) {
				totalProdutos = totalProdutos.add(pi.getValorbruto());
				if(pi.getOutrasdespesas() != null) totalOutrasdespesas = totalOutrasdespesas.add(pi.getOutrasdespesas());
				if(pi.getValordesconto() != null) totalDesconto = totalDesconto.add(pi.getValordesconto());
				if(pi.getValorfrete() != null) totalFrete = totalFrete.add(pi.getValorfrete());
				if(pi.getValorseguro() != null) totalSeguro = totalSeguro.add(pi.getValorseguro());
				if(pi.getValoricmsst() != null) totalIcmsst = totalIcmsst.add(pi.getValoricmsst());
				if(pi.getValoripi() != null) totalIpi = totalIpi.add(pi.getValoripi());
				if(pi.getValorii() != null) totalIi = totalIi.add(pi.getValorii());
			}
			nf.setOutrasdespesas(totalOutrasdespesas);
			nf.setValordesconto(totalDesconto);
			nf.setValorfrete(totalFrete);
			nf.setValorseguro(totalSeguro);
			nf.setValorprodutos(totalProdutos);
			nf.setNotaStatus(NotaStatus.EMITIDA);
			
			Money totalNota = totalProdutos
						.subtract(totalDesconto)
						.add(totalFrete)
						.add(totalSeguro)
						.add(totalOutrasdespesas)
						.add(totalIcmsst)
						.add(totalIpi)
						.add(totalIi);	
			
			nf.setValor(totalNota);

			if(!agrupamento){
				if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getCdprazopagamento() != null){
					nf.setPrazopagamentofatura(venda.getPrazopagamento());
					if (venda.getPrazopagamento().getAvista() != null && venda.getPrazopagamento().getAvista()){
						nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
					}else {
						nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
					}
				}
				
				
				
				List<Documentoorigem> listaDocOrigem = documentoorigemService.buscarListaParaDuplicatas(venda);
				if(!temServico && listaDocOrigem != null && !listaDocOrigem.isEmpty()){
					int i = 1;
					Notafiscalprodutoduplicata nfDuplicata;
					List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
					for (Documentoorigem docOrigem : listaDocOrigem) {
						nfDuplicata = new Notafiscalprodutoduplicata();
						if(nf.getNumero() != null){
							nfDuplicata.setNumero(nf.getNumero() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + i : ""));
						}
						nfDuplicata.setDtvencimento(docOrigem.getDocumento().getDtvencimento());
						nfDuplicata.setValor(docOrigem.getDocumento().getValor());
						nfDuplicata.setDocumentotipo(docOrigem.getDocumento().getDocumentotipo());
						listaDuplicata.add(nfDuplicata);
						i++;
					}
					
					if(SinedUtil.isListNotEmpty(listaVendapagamento)){
						for (Vendapagamento vendapagamento : listaVendapagamento) {
							for (Notafiscalprodutoduplicata notafiscalprodutoduplicata : listaDuplicata) {
								if(notafiscalprodutoduplicata.getDocumentotipo().equals(vendapagamento.getDocumentotipo()) &&
										SinedDateUtils.equalsIgnoreHour(notafiscalprodutoduplicata.getDtvencimento(), vendapagamento.getDataparcela()) &&
										notafiscalprodutoduplicata.getValor().compareTo(vendapagamento.getValororiginal()) == 0){
									notafiscalprodutoduplicata.setAdquirente(vendapagamento.getAdquirente());
									notafiscalprodutoduplicata.setBandeira(vendapagamento.getBandeira());
									notafiscalprodutoduplicata.setAutorizacao(vendapagamento.getAutorizacao());
								}
							}
						}
					}
					
					nf.setListaDuplicata(listaDuplicata);
					nf.setCadastrarcobranca(Boolean.TRUE);
				} else if(venda_aux == null || venda_aux.getPedidovendatipo() == null || venda_aux.getPedidovendatipo().getBonificacao() == null ||
						!venda_aux.getPedidovendatipo().getBonificacao()) {
					if(SinedUtil.isListNotEmpty(listaVendapagamento)){
						Notafiscalprodutoduplicata nfDuplicata;
						List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
						
						Money valorTotalPagamentosVenda = new Money();
						Money valorTotalPagamentosNota = new Money();
						
						for (Vendapagamento vendapagamento : listaVendapagamento) {
							if(vendapagamento.getValororiginal() != null){
								valorTotalPagamentosVenda = valorTotalPagamentosVenda.add(vendapagamento.getValororiginal());
							}
						}
						
						for (Vendapagamento vendapagamento : listaVendapagamento) {
							nfDuplicata = new Notafiscalprodutoduplicata();
							nfDuplicata.setDtvencimento(vendapagamento.getDataparcela());
							
							Money valor = vendapagamento.getValororiginal();
							if (valor != null) {
								if (totalNota != null && valorTotalPagamentosVenda != null && totalNota.compareTo(valorTotalPagamentosVenda) == 1) {
									valor = valor.divide(valorTotalPagamentosVenda).multiply(totalNota);
									Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(valor.getValue().doubleValue()), 2).doubleValue();
									valor = new Money(valorTruncado);
								}
								
								valorTotalPagamentosNota = valorTotalPagamentosNota.add(valor);
							}
							
							nfDuplicata.setValor(valor);
							nfDuplicata.setDocumentotipo(vendapagamento.getDocumentotipo());
							nfDuplicata.setAdquirente(vendapagamento.getAdquirente());
							nfDuplicata.setAutorizacao(vendapagamento.getAutorizacao());
							nfDuplicata.setBandeira(vendapagamento.getBandeira());
							listaDuplicata.add(nfDuplicata);
						}
						
						if (valorTotalPagamentosNota.compareTo(totalNota) == 1 && valorTotalPagamentosNota.getValue().doubleValue() < totalNota.getValue().doubleValue()) {
							Money diferenca = totalNota.subtract(valorTotalPagamentosNota);
							listaDuplicata.get(listaDuplicata.size() - 1).setValor(listaDuplicata.get(listaDuplicata.size() - 1).getValor().add(diferenca));
						} else if (valorTotalPagamentosNota.compareTo(totalNota) == 1 && valorTotalPagamentosNota.getValue().doubleValue() > totalNota.getValue().doubleValue()) {
							Money diferenca = valorTotalPagamentosNota.subtract(totalNota);
							listaDuplicata.get(listaDuplicata.size() - 1).setValor(listaDuplicata.get(listaDuplicata.size() - 1).getValor().subtract(diferenca));
						}
						
						nf.setListaDuplicata(listaDuplicata);
						nf.setCadastrarcobranca(Boolean.TRUE);
					}
				}
			}else {
				nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
			}
			this.adicionarInfNota(nf, venda, venda.getEmpresa());
		}
		
		String informacoesContribuinteTemplate = "";
		for (Notafiscalproduto notafiscalproduto : notas){ 
			if(notafiscalproduto.getListaItens() != null && !notafiscalproduto.getListaItens().isEmpty()){
				List<Venda> listaVendaNota = notafiscalproduto.getListaVenda();
				this.loadInfoForTemplateInfoContribuinte(notafiscalproduto);
				informacoesContribuinteTemplate = montaInformacoesContribuinteTemplate(listaVendaNota, null, notafiscalproduto);
				//Se tem template de informa��es do contribuinte, considera ele para montar as informa��es.
				if(!org.apache.commons.lang.StringUtils.isEmpty(informacoesContribuinteTemplate)){
					notafiscalproduto.setInfoadicionalcontrib(informacoesContribuinteTemplate);
				}
			}
			notafiscalproduto.setDocumentotipo(vendaService.getDocumentotipoVenda(notafiscalproduto.getListaVenda()));
			notafiscalproduto.setOperacaonfe(ImpostoUtil.getOperacaonfe(notafiscalproduto.getCliente(), notafiscalproduto.getNaturezaoperacao()));
			notafiscalproduto.setContaboleto(vendaService.getContaboletoVenda(notafiscalproduto.getListaVenda()));
			if(notafiscalproduto.getContaboleto() != null){
				notafiscalproduto.setContacarteira(contacarteiraService.loadPadraoBoleto(notafiscalproduto.getContaboleto()));
			}
		}
		
		return notas;
	}
	
	/**
	 * M�todo que gera as notas de produto com ou sem agrupamento para as vendas
	 * 
	 * @param listaVendas
	 * @param agrupamento
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> gerarFaturamentoVenda(WebRequestContext request, List<Venda> listaVendas, Boolean agrupamento, Boolean somenteproduto, Boolean notaseparada, Boolean isNfe) {
		List<Notafiscalproduto> notas = this.getListaNotafiscalproduto(request, listaVendas, agrupamento, somenteproduto, notaseparada);
//		boolean gerarNfSeparadaTipoPedidoVenda = notaseparada && parametrogeralService.getBoolean(Parametrogeral.GERAR_NF_SEPARADA_TIPOPEDIDOVENDA);
		for (Notafiscalproduto notafiscalproduto : notas){ 
			if(isNfe) notafiscalproduto.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
			else notafiscalproduto.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFCE);
			
			if(notafiscalproduto.getListaItens() != null && !notafiscalproduto.getListaItens().isEmpty()){
				for(Notafiscalprodutoitem item : notafiscalproduto.getListaItens()){
					item.setValortotaltributos(this.getValorAproximadoImposto(notafiscalproduto.getEmpresa(), notafiscalproduto.getNaturezaoperacao(), item, null));
					
					item.setValorImpostoFederal(this.getValorAproximadoImposto(notafiscalproduto.getEmpresa(), notafiscalproduto.getNaturezaoperacao(), item, ImpostoEcfEnum.FEDERAL));
					item.setValorImpostoEstadual(this.getValorAproximadoImposto(notafiscalproduto.getEmpresa(), notafiscalproduto.getNaturezaoperacao(), item, ImpostoEcfEnum.ESTADUAL));
					item.setValorImpostoMunicipal(this.getValorAproximadoImposto(notafiscalproduto.getEmpresa(), notafiscalproduto.getNaturezaoperacao(), item, ImpostoEcfEnum.MUNICIPAL));
				}
				
				List<Venda> listaVendaNota = notafiscalproduto.getListaVenda();
				if(notafiscalproduto.getInfoadicionalcontrib() != null && notafiscalproduto.getInfoadicionalcontrib().length() > 5000){
					notafiscalproduto.setInfoadicionalcontrib(notafiscalproduto.getInfoadicionalcontrib().substring(0, 5000));
				}
				if(notafiscalproduto.getInfoadicionalfisco() != null && notafiscalproduto.getInfoadicionalfisco().length() > 2000){
					notafiscalproduto.setInfoadicionalfisco(notafiscalproduto.getInfoadicionalfisco().substring(0, 2000));
				}
				if(org.apache.commons.lang.StringUtils.isBlank(notafiscalproduto.getInfoadicionalcontrib())){
					this.verificarInfAproveitamentocredito(notafiscalproduto);
				}
				
				this.setTotalNota(notafiscalproduto);
				verificaDuplicatasVariasVenda(notafiscalproduto);
				this.setTotalNota(notafiscalproduto);
				
				saveOrUpdate(notafiscalproduto);
				updateNumeroNFProduto(notafiscalproduto);
				if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaDuplicata())){
					int i = 1;
					for (Notafiscalprodutoduplicata nfDuplicata : notafiscalproduto.getListaDuplicata()) {
						if(notafiscalproduto.getNumero() != null && org.apache.commons.lang.StringUtils.isBlank(nfDuplicata.getNumero())){
							nfDuplicata.setNumero(notafiscalproduto.getNumero() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + i : ""));
							if(nfDuplicata.getCdnotafiscalprodutoduplicata() != null){
								notafiscalprodutoduplicataService.updateNumeroDuplicata(nfDuplicata, nfDuplicata.getNumero());
							}
						}
						i++;
					}
				}
				
				if(listaVendaNota != null && listaVendaNota.size() > 0){
					boolean recalcularValoresParcelas = listaVendaNota.size() == 1;
					BaixaestoqueEnum baixaestoqueEnum = BaixaestoqueEnum.APOS_VENDAREALIZADA;
					GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
					Boolean gerarexpedicaovenda = Boolean.FALSE;
					
					String whereInVenda = CollectionsUtil.listAndConcatenate(listaVendaNota, "cdvenda", ",");
					List<Vendamaterial> listaVendamaterial = vendamaterialService.findByVenda(whereInVenda);
					
					Money totalVenda = new Money();
					for (Venda venda : listaVendaNota) {
						venda.setListavendamaterial(vendamaterialService.getVendamaterialByVenda(listaVendamaterial, venda));
						totalVenda = totalVenda.add(venda.getTotalvenda());
					}
					
					vendaService.adicionarParcelaNumeroDocumento(listaVendaNota, notafiscalproduto, notafiscalproduto.getNumero(), notaseparada, true, false, false);
					
					List<Documento> listaContareceber = new ArrayList<Documento>();
					boolean verificarParcelaNumeroDocumento = false;
					for (Venda venda : listaVendaNota) {
						
						notavendaService.saveOrUpdate(new NotaVenda(notafiscalproduto, venda));
						
						Vendahistorico vendahistorico = new Vendahistorico();
						vendahistorico.setVenda(venda);
						String numero = notafiscalproduto.getNumero() == null || notafiscalproduto.getNumero().equals("") ? "sem n�mero" : notafiscalproduto.getNumero();
						vendahistorico.setAcao("Gera��o da nota " + numero);
						if(isNfe){
							vendahistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNota("+notafiscalproduto.getCdNota()+");\">"+numero+"</a>.");
						} else {
							vendahistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNFCe("+notafiscalproduto.getCdNota()+");\">"+numero+"</a>.");
						}
						vendahistoricoService.saveOrUpdate(vendahistorico);
						
						baixaestoqueEnum = BaixaestoqueEnum.APOS_VENDAREALIZADA;
						geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
						gerarexpedicaovenda = pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false);
						if(venda.getPedidovendatipo() != null){
							baixaestoqueEnum = venda.getPedidovendatipo().getBaixaestoqueEnum();
							geracaocontareceberEnum = venda.getPedidovendatipo().getGeracaocontareceberEnum();
						}
						if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum) && !notaseparada){
							Money valorNota = notafiscalproduto.getValor();
							if(listaVendaNota.size() > 1){
								Money totalNota = notafiscalproduto.getValor();
								Money valorVenda = venda.getTotalvenda();
								Double percentVenda = (valorVenda.getValue().doubleValue() * 100.0)/ totalVenda.getValue().doubleValue();
								valorNota = new Money((percentVenda / 100.0) * totalNota.getValue().doubleValue());
							}
							
							Date dtsaida = notafiscalproduto.getDatahorasaidaentrada() != null ? new Date(notafiscalproduto.getDatahorasaidaentrada().getTime()) : null;
							if(dtsaida == null) {
								Notafiscalproduto notaComDtSaida = this.load(notafiscalproduto, "cdNota, datahorasaidaentrada");
								if(notaComDtSaida != null && notaComDtSaida.getDatahorasaidaentrada() != null) {
									dtsaida = new Date(notaComDtSaida.getDatahorasaidaentrada().getTime());
								}
							}
							
							vendaService.gerarContareceberAposEmissaonota(request, venda, notafiscalproduto.getDtEmissao(), dtsaida, notafiscalproduto.getNumero(), valorNota, notafiscalproduto.getListaDuplicata(), (Boolean.TRUE.equals(somenteproduto) || !Boolean.TRUE.equals(notaseparada) ? NotaTipo.NOTA_FISCAL_PRODUTO : null), somenteproduto != null && somenteproduto, null, recalcularValoresParcelas, listaContareceber, null, notafiscalproduto.getPrazopagamentofatura(), false);
							if(recalcularValoresParcelas){
								verificarParcelaNumeroDocumento = true;
							}
						}
						
						if((gerarexpedicaovenda == null || !gerarexpedicaovenda) && !movimentacaoestoqueService.existeMovimentacaoestoque(venda)){
							if(BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum)){
								if(venda.getPedidovendatipo() != null && SituacaoNotaEnum.EMITIDA.equals(venda.getPedidovendatipo().getSituacaoNota())){
									venda.setDtestoqueEmissaonota(notafiscalproduto.getDtEmissao());
									for (Vendamaterial vendamaterial : venda.getListavendamaterial()){
										if(vendamaterial.getMaterial() != null && baixaestoqueEnum != null && BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum)){
											vendaService.geraMovimentacaoVenda(venda, vendamaterial, notafiscalproduto);
										}
									}
								}
							}
						}
						
						List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(venda);
						if(SinedUtil.isListNotEmpty(listadocumentoorigem)){
							for (Documentoorigem documentoorigem : listadocumentoorigem) {
								Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
								if(documento != null){
									vendaService.gerarMensagensDocumento(venda, documento, notafiscalproduto, false, notaseparada);
									
									NotaDocumento notaDocumento = new NotaDocumento();
									notaDocumento.setDocumento(documento);
									notaDocumento.setNota(notafiscalproduto);
									notaDocumentoService.saveOrUpdate(notaDocumento);
								}
							}
						}
						
						if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getGerarnotaretornovenda()) && venda.getPedidovenda() != null){
							try {
								pedidovendaService.gerarNotaFiscalRetorno(request, pedidovendaService.findForNotaRetorno(venda.getPedidovenda()), venda, true);
							} catch (IOException e) {
								request.addError("N�o foi poss�vel gerar nota de retorno. " + venda.getCdvenda());
								e.printStackTrace();
							}
						}
					}
					
					if(!agrupamento && verificarParcelaNumeroDocumento){
						vendaService.adicionarParcelaNumeroDocumento(listaVendaNota, notafiscalproduto, notafiscalproduto.getNumero(), notaseparada, true, false, false);
					}
					
					if(SinedUtil.isListNotEmpty(listaContareceber) && parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA)){
						for(Documento documento : listaContareceber){
							documentoService.baixaAutomaticaByVinculoProvisionado(documento);
						}
					}
				}
			}
		}	
		return notas;
	}
	
	private void verificaDuplicatasVariasVenda(Notafiscalproduto nota) {
		if(nota != null && SinedUtil.isListNotEmpty(nota.getListaVenda()) && SinedUtil.isListEmpty(nota.getListaDuplicata())){
			List<Documentotipo> listaDocumentotipo = new ArrayList<Documentotipo>();
			List<Prazopagamento> listaPrazopagamento = new ArrayList<Prazopagamento>();
			List<Vendapagamento> listaVendapgamento = new ArrayList<Vendapagamento>();
			HashMap<Integer, Date> mapParcelaData = new HashMap<Integer, Date>();
			HashMap<Integer, Money> mapParcelaValor = new HashMap<Integer, Money>();
			HashMap<Integer, Documentotipo> mapParcelaDocumentotipo = new HashMap<Integer, Documentotipo>();
			boolean verificarVencimento = false;
			boolean vencimentoIgual = true;
			Integer qtdeParcelas = null;
			Date dataparcela;
			
			Money totalPagamentoVenda = new Money();
			for(Venda venda : nota.getListaVenda()){
				if(venda.getDocumentotipo() != null && !listaDocumentotipo.contains(venda.getDocumentotipo())){
					listaDocumentotipo.add(venda.getDocumentotipo());
				}
				
				listaVendapgamento = SinedUtil.isListNotEmpty(venda.getListavendapagamento()) ? venda.getListavendapagamento() : vendapagamentoService.findVendaPagamentoByVenda(venda); 
				if(SinedUtil.isListNotEmpty(listaVendapgamento)){
					Integer i = 0;
					for(Vendapagamento vendapagamento : listaVendapgamento){
						totalPagamentoVenda = totalPagamentoVenda.add(MoneyUtils.zeroWhenNull(vendapagamento.getValororiginal()));
						
						if(vendapagamento.getDocumentotipo() != null && !listaDocumentotipo.contains(vendapagamento.getDocumentotipo())){
							listaDocumentotipo.add(vendapagamento.getDocumentotipo());
						}
						
						mapParcelaDocumentotipo.put(i, vendapagamento.getDocumentotipo());
						mapParcelaValor.put(i, vendapagamento.getValororiginal());
						
						if(vencimentoIgual){
							if(!verificarVencimento){
								mapParcelaData.put(i, vendapagamento.getDataparcela());
							}else if(mapParcelaData.size() != listaVendapgamento.size()){
								vencimentoIgual = false;
							}else {
								dataparcela = mapParcelaData.get(i);
								if(dataparcela == null || vendapagamento.getDataparcela() == null || !SinedDateUtils.equalsIgnoreHour(vendapagamento.getDataparcela(), dataparcela)){
									vencimentoIgual = false;
								}
							}
						}
						i++;
					}
					verificarVencimento = true;
				}
				if(venda.getPrazopagamento() != null && !listaPrazopagamento.contains(venda.getPrazopagamento())){
					listaPrazopagamento.add(venda.getPrazopagamento());
				}
				if(qtdeParcelas == null && venda.getQtdeParcelas() != null && venda.getQtdeParcelas() > 0){
					qtdeParcelas = venda.getQtdeParcelas();
				}
			}
			
			if(listaDocumentotipo.size() == 1){
				nota.setDocumentotipo(listaDocumentotipo.get(0));
			}
			if(listaPrazopagamento.size() == 1){
				nota.setPrazopagamentofatura(listaPrazopagamento.get(0));
				if (nota.getPrazopagamentofatura().getAvista() != null && nota.getPrazopagamentofatura().getAvista()){
					nota.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
				}else {
					nota.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
				}
				List<Notafiscalprodutoduplicata> listaDuplicata = gerarParcelasCobranca(nota, true, qtdeParcelas != null ? qtdeParcelas : 1);
				Money valorTotalDuplicata = getValorTotalDuplicata(listaDuplicata);
				boolean documentoTipoUnico = listaDocumentotipo.size() == 1;
				boolean vendaUnica = nota.getListaVenda().size() == 1;
				if(SinedUtil.isListNotEmpty(listaDuplicata) && (documentoTipoUnico || vendaUnica)){
					if(vencimentoIgual && (documentoTipoUnico || vendaUnica) && listaDuplicata.size() == mapParcelaData.size()){
						for(Integer posicao : mapParcelaData.keySet()){
							try {
								listaDuplicata.get(posicao).setDtvencimento(new java.sql.Date(mapParcelaData.get(posicao).getTime()));
							} catch (Exception e) {}
							
							try {
								if(listaDocumentotipo.size() > 1 && nota.getListaVenda().size() == 1){
									listaDuplicata.get(posicao).setDocumentotipo(mapParcelaDocumentotipo.get(posicao));
								}
							} catch (Exception e) {}
							
							if(vendaUnica && MoneyUtils.isMaiorQueZero(valorTotalDuplicata) && MoneyUtils.isMaiorQueZero(totalPagamentoVenda)){
								try {
									listaDuplicata.get(posicao).setValor(mapParcelaValor.get(posicao).divide(totalPagamentoVenda).multiply(valorTotalDuplicata));
								} catch (Exception e) {}
							}
						}
					}
					nota.setCadastrarcobranca(Boolean.TRUE);
					nota.setListaDuplicata(listaDuplicata);
				}
				
			}
		}
		
	}
	
	private Money getValorTotalDuplicata(List<Notafiscalprodutoduplicata> listaDuplicata) {
		Money valorTotal = new Money();
		if(SinedUtil.isListNotEmpty(listaDuplicata)){
			for(Notafiscalprodutoduplicata notafiscalservicoduplicata : listaDuplicata){
				valorTotal = valorTotal.add(MoneyUtils.zeroWhenNull(notafiscalservicoduplicata.getValor()));
			}
		}
		return valorTotal;
	}
	public void verificaNotaIndustrializacao(Notafiscalproduto nf, Notafiscalprodutoitem item, Cfop cfopindustrializacao, Cfop cfopretorno, List<Entregamaterial> listaEntregamaterial, List<Notafiscalprodutoitem> listaItens, boolean referenciarNotaEntrada){
		//AT 54951
		if (nf != null && nf.isGerarNotaIndustrializacaoRetorno()){
			item.setCfop(cfopindustrializacao);
			
			Boolean isCriarItemRetorno = item.getNotafiscalprodutoitemIndustrializacao() == null;
			Notafiscalprodutoitem itemRetorno = item.getClone();
			
			if(!isCriarItemRetorno){
				if(org.apache.commons.lang.StringUtils.isNotEmpty(item.getNotafiscalprodutoitemIndustrializacao().getInfoadicional())){
					if(org.apache.commons.lang.StringUtils.isNotEmpty(itemRetorno.getInfoadicional())){
						if(!itemRetorno.getInfoadicional().contains(item.getNotafiscalprodutoitemIndustrializacao().getInfoadicional())){
							itemRetorno.setInfoadicional(itemRetorno.getInfoadicional() + " - " + item.getNotafiscalprodutoitemIndustrializacao().getInfoadicional());
						}
					} else {
						itemRetorno.setInfoadicional(item.getNotafiscalprodutoitemIndustrializacao().getInfoadicional());
					}
				}
				
				String identificadorItemRetorno = item.getNotafiscalprodutoitemIndustrializacao().getIdentificadorItemRetorno();
				if(SinedUtil.isListNotEmpty(listaItens) && identificadorItemRetorno != null){
					int index = 0;
					boolean existe = false;
					for(Notafiscalprodutoitem itemListaRetorno : listaItens){
						if(itemListaRetorno.getIdentificadorItemRetorno() != null && 
								itemListaRetorno.getIdentificadorItemRetorno().equals(identificadorItemRetorno)){
							existe = true;
							break;
						}
						index++;
					}
					if(existe){
						listaItens.set(index, itemRetorno); 
					}
				}
				itemRetorno.setIsItemRetorno(true);
				item.setNotafiscalprodutoitemIndustrializacao(itemRetorno);
			}
			itemRetorno.setCfop(cfopretorno);
			
			boolean adicionar = false;
			if (SinedUtil.isListNotEmpty(listaEntregamaterial)){
				Double valorunitario = 0d;
				int cont = 0;
				for (Entregamaterial entregamaterial: listaEntregamaterial){
					boolean materialIgual = verificaMaterialIgual(itemRetorno,entregamaterial);
					
					if (materialIgual){
						valorunitario += entregamaterial.getValorunitario();
						cont++;
					}
				}
				if(cont > 0){
					itemRetorno.setValorunitario(valorunitario / cont);
					if (itemRetorno.getQtde() != null){
						itemRetorno.setValorbruto(new Money(itemRetorno.getQtde() * itemRetorno.getValorunitario()));
						adicionar = true;
					}
				}
			}		
			
			includeInfoAdicionalNotaEntrada(referenciarNotaEntrada, listaEntregamaterial, itemRetorno);
			
			if(adicionar){
				itemRetorno.setIsItemRetorno(true);
				itemRetorno.setIdentificadorItemRetorno(new Long(System.currentTimeMillis()).toString());
				if(isCriarItemRetorno){
					item.setNotafiscalprodutoitemIndustrializacao(itemRetorno);
					listaItens.add(itemRetorno);
				}
			}
		}
	}
	
	private boolean verificaMaterialIgual(Notafiscalprodutoitem itemRetorno, Entregamaterial entregamaterial) {
		return entregamaterial.getMaterial() != null
				&& entregamaterial.getMaterial().getCdmaterial() != null
				&& itemRetorno.getMaterial() != null
				&& itemRetorno.getMaterial().getCdmaterial() != null
				&& entregamaterial.getMaterial().getCdmaterial().equals(itemRetorno.getMaterial().getCdmaterial());
	}
	
	/**
	* M�todo que retorna o mapa de material e local, se n�o tiver local diferente nas vendas
	*
	* @param listaVendas
	* @return
	* @since 05/09/2014
	* @author Luiz Fernando
	*/
	private HashMap<Material, Localarmazenagem> getMapMaterialLocal(List<Venda> listaVendas) {
		HashMap<Material, Localarmazenagem> mapMaterialLocal = new HashMap<Material, Localarmazenagem>();
		List<Material> listaMaterialNaoInformarLocal = new ArrayList<Material>();
		for (Venda venda : listaVendas) {
			if(venda.getLocalarmazenagem() != null){
				if(SinedUtil.isListNotEmpty(venda.getListavendamaterial())){
					for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
						if(vendamaterial.getMaterial() != null && !listaMaterialNaoInformarLocal.contains(vendamaterial.getMaterial())){
							Localarmazenagem localNota = mapMaterialLocal.get(vendamaterial.getMaterial());
							if(localNota != null){
								if(!localNota.equals(venda.getLocalarmazenagem()) && 
										!listaMaterialNaoInformarLocal.contains(vendamaterial.getMaterial())){
									listaMaterialNaoInformarLocal.add(vendamaterial.getMaterial());
									mapMaterialLocal.remove(vendamaterial.getMaterial());
								}
							}else {
								mapMaterialLocal.put(vendamaterial.getMaterial(), venda.getLocalarmazenagem());
							}
						}
					}
				}
			}
		}
		return mapMaterialLocal;
	}
	
	public String getInfParcelaVenda(String boletocomnumconta, Venda venda, List<Vendapagamento> listaVendapagamento, Documento documento) {
		String numVenda = "";
		String numParcela = "";
		
		if(boletocomnumconta != null && venda != null && documento != null){
			numVenda = ("FALSE".equalsIgnoreCase(boletocomnumconta) && venda.getCdvenda() != null ? venda.getCdvenda().toString() : 
				(documento.getCddocumento() != null ? documento.getCddocumento().toString() : "") );
			if(SinedUtil.isListNotEmpty(listaVendapagamento)) {
				Integer numeroParcela = 1;
				for(Vendapagamento vendapagamento : listaVendapagamento){
					if(vendapagamento.getDocumento() != null && documento.equals(vendapagamento.getDocumento())){
						numParcela = " - " + numeroParcela.toString() + "/" + listaVendapagamento.size();
						break;
					}
					numeroParcela++;
				}
			}
			if(org.apache.commons.lang.StringUtils.isNotEmpty(numParcela) &&
					org.apache.commons.lang.StringUtils.isNotEmpty(documento.getNumero()) &&
					(documento.getNumero().contains(numParcela) ||
					(" - " + documento.getNumero()).equals(numParcela) )){
				numParcela = "";
			}
			if(org.apache.commons.lang.StringUtils.isNotEmpty(numVenda) &&
					org.apache.commons.lang.StringUtils.isNotEmpty(documento.getNumero()) &&
					documento.getNumero().contains(numVenda)){
				numVenda = "";
			}
		}
		String infParcela = numVenda;
		if(org.apache.commons.lang.StringUtils.isNotEmpty(numParcela) && 
				(org.apache.commons.lang.StringUtils.isEmpty(documento.getNumero()) ||
				!documento.getNumero().contains(infParcela)) ){
			if(org.apache.commons.lang.StringUtils.isNotEmpty(infParcela)){
				infParcela += " - ";
			}
			infParcela += numParcela;
		}
		if(org.apache.commons.lang.StringUtils.isNotEmpty(infParcela)){
			infParcela = " " + infParcela;
		}
		return infParcela;
	}
	
	public void updateNumeroNFProduto(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto.getCdNota() != null) {
			if(notafiscalproduto.getModeloDocumentoFiscalEnum() != null && notafiscalproduto.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
				Integer proximoNumero = getProximoNumeroNFConsumidor(notafiscalproduto.getEmpresa());
				empresaService.updateProximoNumNFConsumidor(notafiscalproduto.getEmpresa(), proximoNumero + 1);
				notafiscalproduto.setNumero(proximoNumero.toString());
				notaService.updateNumeroNota(notafiscalproduto, proximoNumero);
			} else {
				Integer proximoNumero = getProximoNumeroNFProduto(notafiscalproduto.getEmpresa());
				empresaService.updateProximoNumNFProduto(notafiscalproduto.getEmpresa(), proximoNumero+1);
				notafiscalproduto.setNumero(proximoNumero.toString());
				notaService.updateNumeroNota(notafiscalproduto, proximoNumero);
			}
		}
	}
	
	public Integer getProximoNumeroNFProduto(Empresa empresa){
		Integer proximoNumero = empresaService.carregaProxNumNFProduto(empresa);
		if(proximoNumero == null){
			proximoNumero = 1;
		}
		return proximoNumero;
	}
	
	public Integer getProximoNumeroNFConsumidor(Empresa empresa){
		Integer proximoNumero = empresaService.carregaProxNumNFConsumidor(empresa);
		if(proximoNumero == null){
			proximoNumero = 1;
		}
		return proximoNumero;
	}
	
	/**
	 * M�todo que retorna a informa��o adicional do item para o cfop 5102 ou 6102, com icms que tenha permiss�o de cr�dito 
	 *
	 * @param it
	 * @return
	 * @author Luiz Fernando
	 */
	public String getMsgitemAproveitamentocredito(List<Notafiscalprodutoitem> itens) {
		String infadicionaiscfoppermissaocredito = "";
		if(itens != null && !itens.isEmpty()){
			Money valoricms = new Money();
			Double aliquota = null;
			for(Notafiscalprodutoitem it : itens){
				if(it != null && it.getValorcreditoicms() != null && it.getAliquotacreditoicms() != null && it.getCfop() != null && 
						("5102".equals(it.getCfop().getCodigo()) || "6102".equals(it.getCfop().getCodigo())) &&
						it.getTipocobrancaicms() != null && 
						(Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(it.getTipocobrancaicms()) || 
						 Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(it.getTipocobrancaicms())) &&
						 it.getTipotributacaoicms() != null && Tipotributacaoicms.SIMPLES_NACIONAL.equals(it.getTipotributacaoicms())){
					valoricms = valoricms.add(it.getValorcreditoicms());
					if(aliquota == null) aliquota = it.getAliquotacreditoicms();
				}
			}
			if(valoricms != null && valoricms.getValue().doubleValue() > 0){
				infadicionaiscfoppermissaocredito = "PERMITE O APROVEITAMENTO DO CR�DITO DE ICMS NO VALOR DE " +
				"R$ " + valoricms + " CORRESPONDENTE � AL�QUOTA DE " + aliquota + "%, NOS TERMOS DO ART. 23 DA LC 123.";
			}
		}
		return infadicionaiscfoppermissaocredito;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> carregarInfoParaCalcularTotalNew(NotaFiltro filtro) {
		return notafiscalprodutoDAO.carregarInfoParaCalcularTotalNew(filtro);
	}
	
	/**
	 * Gera o arquivo CSV da listagem de Nota Fiscal Produto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotafiscalprodutoService#findForCsv(NotafiscalprodutoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since Jul 4, 2011
	 * @author Rodrigo Freitas
	 * @param isNfe 
	 */
	public Resource geraArquivoCSVListagem(NotafiscalprodutoFiltro filtro, boolean isNfe) {
		StringBuilder csv = new StringBuilder();
		
		List<Notafiscalproduto> lista = this.findForCsv(filtro);
		
		// CABE�ALHO
		csv.append("\"DATA DE EMISS�O\"").append(";");
		csv.append("\"N�MERO\"").append(";");
		csv.append("\"CLIENTE\"").append(";");
		csv.append("\"CPF/CNPJ\"").append(";");
		csv.append("\"VALOR\"").append(";");
		csv.append("\"TIPO DE OPERA��O\"").append(";");
		csv.append("\"DATA DE SA�DA\"").append(";");
		csv.append("\"NATUREZA DA OPERA��O\"").append(";");
		csv.append("\"FORMA DE PAGAMENTO\"").append(";");
		csv.append("\"VALOR PRODUTOS\"").append(";");
		csv.append("\"RESPONS�VEL PELO FRETE\"").append(";");
		csv.append("\"VALOR FRETE\"").append(";");
		csv.append("\"VALOR SEGURO\"").append(";");
		csv.append("\"VALOR OUTRAS DESPESAS\"").append(";");
		csv.append("\"VALOR DESCONTO\"").append(";");
		csv.append("\"CFOP\"").append(";");
		csv.append("\"TIPO COBRAN�A ICMS\"").append(";");
		csv.append("\"VALOR BC ICMS\"").append(";");
		csv.append("\"VALOR ICMS\"").append(";");
		csv.append("\"VALOR BC ICMS/ST\"").append(";");
		csv.append("\"VALOR ICMS/ST\"").append(";");
		csv.append("\"TIPO COBRAN�A IPI\"").append(";");
		csv.append("\"VALOR IPI\"").append(";");
		csv.append("\"TIPO COBRAN�A PIS\"").append(";");
		csv.append("\"VALOR PIS\"").append(";");
		csv.append("\"TIPO COBRAN�A COFINS\"").append(";");
		csv.append("\"VALOR COFINS\"").append(";");
		csv.append("\"TIPO DE DOCUMENTO\"").append(";");
		csv.append("\n");
		
		for (Notafiscalproduto nf: lista) {
			csv.append("\"" + (nf.getDtEmissao() != null ? SinedDateUtils.toString(nf.getDtEmissao()) : "") + "\";");
			csv.append("\"" + (nf.getNumero() != null ? nf.getNumeroFormatado() : "") + "\";");
			csv.append("\"" + (nf.getCliente() != null ? nf.getCliente().getNome() : "") + "\";");
			
			if(nf.getCliente() != null && nf.getCliente().getCpfcnpj() != null) 
				csv.append("\"" + nf.getCliente().getCpfcnpj() + "\";");
			else
				csv.append("\"\";");
			
			csv.append("\"" + (nf.getValor() != null ? nf.getValor() : "") + "\";");
			csv.append("\"" + (nf.getTipooperacaonota() != null ? nf.getTipooperacaonota().getDescricao() : "") + "\";");
			
			csv.append("\"" + (nf.getDatahorasaidaentrada() != null ? SinedDateUtils.toString(nf.getDatahorasaidaentrada()) : "") + "\";");
			csv.append("\"" + (nf.getNaturezaoperacao() != null ? nf.getNaturezaoperacao().getNome() : "") + "\";");
			
			csv.append("\"" + (nf.getFormapagamentonfe() != null ? nf.getFormapagamentonfe().getDescricao() : "") + "\";");
			csv.append("\"" + (nf.getValorprodutos() != null ? nf.getValorprodutos() : "") + "\";");
			csv.append("\"" + (nf.getResponsavelfrete() != null ? nf.getResponsavelfrete().getNome() : "") + "\";");
			
			csv.append("\"" + (nf.getValorfrete() != null ? nf.getValorfrete() : "") + "\";");
			csv.append("\"" + (nf.getValorseguro() != null ? nf.getValorseguro() : "") + "\";");
			csv.append("\"" + (nf.getOutrasdespesas() != null ? nf.getOutrasdespesas() : "") + "\";");

			
			int indexNotaDocumento = 0;
			int i = 0;
			if (nf.getListaItens() != null){
				for( Notafiscalprodutoitem item : nf.getListaItens()){
					
					if(i > 0){
						csv.append("\n");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
						csv.append("\"\";");
					}
					
					if(item.getValordesconto() != null)
						csv.append("\"" + item.getValordesconto() + "\";");
					else
						csv.append("\" \";");
						
					if(item.getCfop() != null && item.getCfop().getCodigo() != null)
						csv.append("\"" + item.getCfop().getCodigo() + "\";");
					else
						csv.append("\" \";");
					
					if(item.getTipocobrancaicms() != null)
						csv.append("\"" + item.getTipocobrancaicms() + "\";");
					else
						csv.append("\" \";");
						
					if(item.getValorbcicms() != null)
						csv.append("\"" + item.getValorbcicms() + "\";");
					else
						csv.append("\" \";");	
						
					if(item.getValoricms() != null)
						csv.append("\"" + item.getValoricms() + "\";");
					else
						csv.append("\" \";");	
					
					if(item.getValorbcicmsst() != null)
						csv.append("\"" + item.getValorbcicmsst() + "\";");
					else
						csv.append("\" \";");
						
					if(item.getValoricmsst() != null)
						csv.append("\"" + item.getValoricmsst() + "\";");
					else
						csv.append("\" \";");
						
					if(item.getTipocobrancaipi() != null)
						csv.append("\"" + item.getTipocobrancaipi() + "\";");
					else
						csv.append("\"\";");
					
					if(item.getValoripi() != null)
						csv.append("\"" + item.getValoripi() + "\";");
					else
						csv.append("\"\";");
					
					if(item.getTipocobrancapis() != null)
						csv.append("\"" + item.getTipocobrancapis() + "\";");
					else
						csv.append("\"\";");
						
					if(item.getValorpis() != null)
						csv.append("\"" + item.getValorpis() + "\";");
					else
						csv.append("\"\";");
					
					if(item.getTipocobrancacofins() != null)
						csv.append("\"" + item.getTipocobrancacofins() + "\";");
					else
						csv.append("\"\";");
						
					if(item.getValorcofins() != null)
						csv.append("\"" + item.getValorcofins() + "\";");
					else
						csv.append("\"\";");
						
					if(SinedUtil.isListNotEmpty(nf.getListaNotaDocumento()) && indexNotaDocumento < nf.getListaNotaDocumento().size()){
						NotaDocumento notaDocumento = nf.getListaNotaDocumento().get(indexNotaDocumento);
						if(notaDocumento != null && notaDocumento.getDocumento().getDocumentotipo() != null){
							csv.append("\"" + notaDocumento.getDocumento().getDocumentotipo().getNome() + "\";");
						}
						indexNotaDocumento++;
					}
						
					 i++;
	
				}
			}
			
			if(SinedUtil.isListNotEmpty(nf.getListaNotaDocumento())){
				while(indexNotaDocumento < nf.getListaNotaDocumento().size()){
					csv.append("\n");
					csv.append("\"\";").append("\"\";").append("\"\";").append("\"\";").append("\"\";");
					csv.append("\"\";").append("\"\";").append("\"\";").append("\"\";").append("\"\";");
					csv.append("\"\";").append("\"\";").append("\"\";").append("\"\";").append("\"\";");
					csv.append("\"\";").append("\"\";").append("\"\";").append("\"\";").append("\"\";");
					csv.append("\"\";").append("\"\";").append("\"\";").append("\"\";").append("\"\";");
					csv.append("\"\";").append("\"\";");
					
					NotaDocumento notaDocumento = nf.getListaNotaDocumento().get(indexNotaDocumento);
					if(notaDocumento.getDocumento().getDocumentotipo() != null){
						String nomedocumentotipo = notaDocumento.getDocumento().getDocumentotipo().getNome();
						csv.append("\"" + nomedocumentotipo + "\";");
					}
					indexNotaDocumento++;
					
					i++;
				}
			}

			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "notafiscal" + (isNfe ? "produto" : "consumidor") + "_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * Busca a lista de Nota Fiscal de Produto para a gera��o do CSV.
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForCsv(NotafiscalprodutoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since Jul 4, 2011
	 * @author Rodrigo Freitas
	 */
	private List<Notafiscalproduto> findForCsv(NotafiscalprodutoFiltro filtro) {
		return notafiscalprodutoDAO.findForCsv(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca os dados na Nota Fiscal de Produto para Integra��o com Dom�nio
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForDominiointegracaoSaida(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForDominiointegracaoSaida(Empresa empresa, Date dtinicio, Date dtfim){
		return notafiscalprodutoDAO.findForDominiointegracaoSaida(empresa, dtinicio, dtfim);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		return notafiscalprodutoDAO.findForSpedPiscofinsRegC100(filtro, empresa);
	}
	
	/**
	 * 
	 * M�todo que retorna o valor de cada desconto para a lista de produtos
	 * Modifica��o: C�lculo do desconto proporcional ao valor do material e total dos materiais
	 *
	 *@author Thiago Augusto
	 *@author Marden Silva
	 *@date 20/03/2012
	 * @param listavendamaterial
	 * @param vendamaterial
	 * @param descontoTotal
	 * 
	 * @return valorDesconto
	 */
	public Money getValorDescontoVenda(List<Vendamaterial> listavendamaterial, Vendamaterial vendamaterial, Money descontoTotal, Money valorUsadoValeCompra, boolean calcularDescontoUnitario){
			Money valorDesconto = new Money(0);
			Money descontoTotalWithValeCompra = new Money();
			if(valorUsadoValeCompra != null && valorUsadoValeCompra.getValue().doubleValue() > 0){
				descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(valorUsadoValeCompra);
			}
			if(descontoTotal != null && descontoTotal.getValue().doubleValue() > 0){
				descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(descontoTotal);
			}
			
			if (descontoTotalWithValeCompra != null && descontoTotalWithValeCompra.getValue().doubleValue() > 0){
				
				Money totalMaterial = new Money(0);
				if(listavendamaterial != null && !listavendamaterial.isEmpty()){
					for (Vendamaterial vm : listavendamaterial) {
						Double qtde = vm.getQuantidade() == null ? 1 : vm.getQuantidade();
						Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
						totalMaterial = totalMaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
						if(vm.getDesconto() != null){
							totalMaterial = totalMaterial.subtract(vm.getDesconto());
						}
					}
				}
				
				Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() != 0 ? vendamaterial.getMultiplicador() : 1d;
				Double qtde = vendamaterial.getQuantidade() == null ? 1 : vendamaterial.getQuantidade();
				Money valorMaterial = new Money(vendamaterial.getPreco().doubleValue() *  qtde * multiplicador);
				
				if(vendamaterial.getDesconto() != null){
					valorMaterial = valorMaterial.subtract(vendamaterial.getDesconto());
				}
				
				//Calcula o valor total de desconto proporcional para o produto
				if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
					totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
					valorDesconto = descontoTotalWithValeCompra.multiply(valorMaterial.divide(totalMaterial));
				}		
				
				//Calcula o valor do desconto unit�rio
				if (calcularDescontoUnitario){
					valorDesconto = valorDesconto.divide(new Money(qtde*multiplicador));
				}
			}		
			
			return valorDesconto;
	}
	
	public Money getValorFreteVenda(List<Vendamaterial> listavendamaterial, Vendamaterial vendamaterial, Money valorfreteTotal){
		Money valorFrete = new Money(0);
		
		if (valorfreteTotal != null && valorfreteTotal.getValue().doubleValue() > 0){
			
			Money totalMaterial = new Money(0);
			if(listavendamaterial != null && !listavendamaterial.isEmpty()){
				for (Vendamaterial vm : listavendamaterial) {
					Double qtde = vm.getQuantidade() == null ? 1 : vm.getQuantidade();
					Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
					totalMaterial = totalMaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
					if(vm.getDesconto() != null){
						totalMaterial = totalMaterial.subtract(vm.getDesconto());
					}
				}
			}
			
			Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() != 0 ? vendamaterial.getMultiplicador() : 1d;
			Double qtde = vendamaterial.getQuantidade() == null ? 1 : vendamaterial.getQuantidade();
			Money valorMaterial = new Money(vendamaterial.getPreco().doubleValue() *  qtde * multiplicador);
			
			if(vendamaterial.getDesconto() != null){
				valorMaterial = valorMaterial.subtract(vendamaterial.getDesconto());
			}
			
			//Calcula o valor total de desconto proporcional para o produto
			if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
					totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
				valorFrete = valorfreteTotal.multiply(valorMaterial.divide(totalMaterial));
			}		
		}		
		
		return valorFrete;
	}
	
	public Money getValorDescontoVenda(List<Pedidovendamaterial> listavendamaterial, Pedidovendamaterial vendamaterial, Money descontoTotal, Money valorUsadoValeCompra, boolean calcularDescontoUnitario){
		Money valorDesconto = new Money(0);
		Money descontoTotalWithValeCompra = new Money();
		if(valorUsadoValeCompra != null && valorUsadoValeCompra.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(valorUsadoValeCompra);
		}
		if(descontoTotal != null && descontoTotal.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(descontoTotal);
		}
		
		if (descontoTotalWithValeCompra != null && descontoTotalWithValeCompra.getValue().doubleValue() > 0){
			
			Money totalMaterial = new Money(0);
			if(listavendamaterial != null && !listavendamaterial.isEmpty()){
				for (Pedidovendamaterial vm : listavendamaterial) {
					Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
					Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
					totalMaterial = totalMaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
					if(vm.getDesconto() != null){
						totalMaterial = totalMaterial.subtract(vm.getDesconto());
					}
				}
			}
			
			Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() != 0 ? vendamaterial.getMultiplicador() : 1d;
			Money valorMaterial = new Money(vendamaterial.getPreco().doubleValue() * 
									(vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade())
									* multiplicador);
			
			if(vendamaterial.getDesconto() != null){
				valorMaterial = valorMaterial.subtract(vendamaterial.getDesconto());
			}
			
			//Calcula o valor total de desconto proporcional para o produto
			if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
				totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
				valorDesconto = descontoTotalWithValeCompra.multiply(valorMaterial.divide(totalMaterial));
			}		
			
			//Calcula o valor do desconto unit�rio
			if (calcularDescontoUnitario){
				if (vendamaterial.getQuantidade() != null && vendamaterial.getQuantidade().doubleValue() > 0){
					valorDesconto = valorDesconto.divide(new Money(vendamaterial.getQuantidade()*multiplicador));
				}
			}
		}		
		
		return valorDesconto;
	}
	
	/**
	* M�todo que retorna o valor de cada desconto para a lista de produtos
	* Modifica��o: C�lculo do desconto proporcional ao valor do material e total dos materiais
	*
	* @param listavendamaterial
	* @param vendamaterial
	* @param descontoTotal
	* @param valorUsadoValeCompra
	* @param calcularDescontoUnitario
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public Money getValorDescontoVenda(List<Vendaorcamentomaterial> listavendamaterial, Vendaorcamentomaterial vendamaterial, Money descontoTotal, Money valorUsadoValeCompra, boolean calcularDescontoUnitario){
		Money valorDesconto = new Money(0);
		Money descontoTotalWithValeCompra = new Money();
		if(valorUsadoValeCompra != null && valorUsadoValeCompra.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(valorUsadoValeCompra);
		}
		if(descontoTotal != null && descontoTotal.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(descontoTotal);
		}
		
		if (descontoTotalWithValeCompra != null && descontoTotalWithValeCompra.getValue().doubleValue() > 0){
			
			Money totalMaterial = new Money(0);
			if(listavendamaterial != null && !listavendamaterial.isEmpty()){
				for (Vendaorcamentomaterial vm : listavendamaterial) {
					Double qtde = vm.getQuantidade() == null ? 1 : vm.getQuantidade();
					Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
					totalMaterial = totalMaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
					if(vm.getDesconto() != null){
						totalMaterial = totalMaterial.subtract(vm.getDesconto());
					}
				}
			}
			
			Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() != 0 ? vendamaterial.getMultiplicador() : 1d;
			Double qtde = vendamaterial.getQuantidade() == null ? 1 : vendamaterial.getQuantidade();
			Money valorMaterial = new Money(vendamaterial.getPreco().doubleValue() *  qtde * multiplicador);
			
			if(vendamaterial.getDesconto() != null){
				valorMaterial = valorMaterial.subtract(vendamaterial.getDesconto());
			}
			
			//Calcula o valor total de desconto proporcional para o produto
			if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
				totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
				valorDesconto = descontoTotalWithValeCompra.multiply(valorMaterial.divide(totalMaterial));
			}		
			
			//Calcula o valor do desconto unit�rio
			if (calcularDescontoUnitario){
				valorDesconto = valorDesconto.divide(new Money(qtde*multiplicador));
			}
		}		
		
		return valorDesconto;
}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForSIntegraRegistro50(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForSIntegraRegistro50(SintegraFiltro filtro) {
		return notafiscalprodutoDAO.findForSIntegraRegistro50(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#isExisteNota(Empresa empresa, String numeroNota)
	 *
	 * @param empresa
	 * @param numeroNota
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isExisteNota(Empresa empresa, String numeroNota, Integer serienfe, ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum, Notafiscalproduto nfp) {
		return notafiscalprodutoDAO.isExisteNota(empresa, numeroNota, serienfe, modeloDocumentoFiscalEnum, nfp);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForCancelamentoNfe(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForCancelamentoNfe(String whereIn) {
		return notafiscalprodutoDAO.findForCancelamentoNfe(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte)
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		return notafiscalprodutoDAO.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
	}
	
	/**
	 * M�todo que seta o campo AlturaLarguraComprimentoConcatenados do material com origem da venda
	 *
	 * @param nfp
	 * @author Luiz Fernando
	 */
	public void setNomnfComprimentoAlturalargura(Notafiscalproduto nfp) {
		if(nfp != null && nfp.getCdNota() != null && nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
			Notafiscalproduto notafiscalproduto = this.carregaNotaVenda(nfp);
			if(notafiscalproduto != null && notafiscalproduto.getListaNotavenda() != null && 
					!notafiscalproduto.getListaNotavenda().isEmpty()){
				for(Notafiscalprodutoitem nfpi : nfp.getListaItens()){
					if(nfpi.getMaterial() != null && nfpi.getMaterial().getCdmaterial() != null){
						for(NotaVenda notavenda : notafiscalproduto.getListaNotavenda()){
							if(notavenda.getVenda() != null && notavenda.getVenda().getListavendamaterial() != null && 
									!notavenda.getVenda().getListavendamaterial().isEmpty()){
								for(Vendamaterial vm : notavenda.getVenda().getListavendamaterial()){
									if(vm.getMaterial() != null && vm.getMaterial().getCdmaterial() != null && 
											vm.getMaterial().getCdmaterial().equals(nfpi.getMaterial().getCdmaterial())){
										nfpi.getMaterial().setAlturaLarguraComprimentoConcatenados(vendaService.getAlturaLarguraComprimentoConcatenados(vm, nfpi.getMaterial()));
									}
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * M�todo que carrega a nota fiscal de produto com a lista de venda (notavenda)
	 *
	 * @param notafiscalproduto
	 * @return
	 * @author Luiz Fernando
	 */
	public Notafiscalproduto carregaNotaVenda(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoDAO.carregaNotaVenda(notafiscalproduto);
	}
	
	/**
	 * M�todo que busca os valores de cr�ditos de icmsst
	 *
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findEntregaMenorDataEntradaByMaterial(Material material, String whereIn)
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findEntregadocumentoComSaldo(Integer cdentregadocumento)
	 *
	 * @param material
	 * @param qtdenfp
	 * @return
	 * @author Luiz Fernando
	 * @param cdentregadocumento 
	 */
	public Aux_imposto buscaValoresCobradosAnteriormenteIcmsst(Material material, Double qtdenfp, Grupotributacao grupotributacao, Integer cdentregadocumento){
		Money valorbcicmsst = new Money(0.0);
		Money valoricmsst = new Money(0.0);	
		String whereIn;
		Entregadocumento entregadocumento;
			
		if(grupotributacao != null && grupotributacao.getStfixa() != null && grupotributacao.getStfixa()){
			if(material.getBasecalculost() != null && grupotributacao.getMargemvaloradicionalicmsst() != null){
				Double resultado = qtdenfp*material.getBasecalculost();
				resultado = resultado * (1. + grupotributacao.getMargemvaloradicionalicmsst());
				valorbcicmsst = new Money(SinedUtil.round(resultado, 2));
			}
			
			if(material.getValorst() != null){
				valoricmsst = new Money(SinedUtil.round(qtdenfp * material.getValorst(), 2));
			}
		} else{
			whereIn = entregaService.findEntregaMenorDataEntradaByMaterial(material, null, cdentregadocumento);
			if(whereIn != null && !"".equals(whereIn)){
				String[] IdEntregadocumento = whereIn.split(",");
				
				if(IdEntregadocumento != null && IdEntregadocumento.length > 0){
					Double bcicmsst = 0.0;
					Double totalbcicmsst = 0.0;
					Double icmsst = 0.0;
					Double qtdesuficiente = 0.0;
					Double qtdeutilizada = 0.0;
					Double qtdematerial = 1.0;
					Double qtdeutilizadamaterial = 0.0;		
					for(int i = 0; i < IdEntregadocumento.length; i++){
						entregadocumento = entradafiscalService.findEntregadocumentoComSaldo(Integer.parseInt(IdEntregadocumento[i]));
						if(entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
							for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){								
								if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().equals(material)){
									if(entregamaterial.getQtde() != null) qtdematerial = entregamaterial.getQtde();
									if(entregamaterial.getQtdeutilizada() != null){
										qtdeutilizadamaterial = entregamaterial.getQtdeutilizada();
									}else {
										qtdeutilizadamaterial = 0.0;
									}
									qtdesuficiente += (qtdematerial-qtdeutilizadamaterial);
									if(qtdenfp > (qtdematerial-qtdeutilizadamaterial)){
										qtdeutilizada =  (qtdematerial-qtdeutilizadamaterial);
									}else if(qtdenfp <= (qtdematerial-qtdeutilizadamaterial)){
										qtdeutilizada =  qtdenfp;
									}
									if(entregamaterial.getValorbcicmsst() != null && entregamaterial.getValorbcicmsst().getValue().doubleValue() > 0){
										bcicmsst = entregamaterial.getValorbcicmsst().getValue().doubleValue()/qtdematerial;
										totalbcicmsst += bcicmsst*qtdeutilizada;
										
										if(entregamaterial.getIcmsst() != null){
											icmsst = entregamaterial.getIcmsst()*totalbcicmsst;
											if(icmsst != 0){
												icmsst = icmsst/100;
											}
										}
									}
								}
							}
							
							if(icmsst != null){
								valoricmsst = new Money(icmsst); 
							}
							if(bcicmsst != null){
								valorbcicmsst = new Money(totalbcicmsst); 
							}
						}
						if(qtdesuficiente >= qtdenfp){
							break;
						}else {
							qtdenfp = qtdenfp-qtdeutilizada;
						}
					}
				}
			}
		}
		
		if(valorbcicmsst != null && valorbcicmsst.getValue().doubleValue() < 0){
			valorbcicmsst = new Money();
			valoricmsst = new Money();
		}
		
		return new Aux_imposto(valorbcicmsst, valoricmsst);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro) {
		return notafiscalprodutoDAO.findforApuracaopiscofins(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForApuracaoipi(ApuracaoipiFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForApuracaoipi(ApuracaoipiFiltro filtro) {
		return notafiscalprodutoDAO.findForApuracaoipi(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForApuracaoicms(ApuracaoicmsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForApuracaoicms(ApuracaoicmsFiltro filtro, Tipooperacaonota tipooperacaonota) {
		return notafiscalprodutoDAO.findForApuracaoicms(filtro, tipooperacaonota);
	}
	
	/**
	 * M�todo que retorna a menor data da lista de Duplicata da nota fiscal de produto, caso n�o exista a lista de duplicata, 
	 * retorna a data de emiss�o 
	 *
	 * @param notafiscalproduto
	 * @return
	 * @author Luiz Fernando
	 * @param geraReceita 
	 */
	public Date getDtvencimentoGerarReceita(GeraReceita geraReceita, Notafiscalproduto notafiscalproduto) {
		Date dtvencimento = null;
		if(notafiscalproduto != null){
			if(notafiscalproduto.getListaDuplicata() != null && !notafiscalproduto.getListaDuplicata().isEmpty()){
				for(Notafiscalprodutoduplicata duplicata : notafiscalproduto.getListaDuplicata()){
					if(duplicata.getDtvencimento() != null){
						if(dtvencimento != null){
							if(SinedDateUtils.afterIgnoreHour(dtvencimento, duplicata.getDtvencimento())){
								dtvencimento = duplicata.getDtvencimento();
							}
						}else {
							dtvencimento = duplicata.getDtvencimento();
						}
					}
				}
			}
			if(dtvencimento == null){
				if(geraReceita.getDtemissao() != null){
					dtvencimento = geraReceita.getDtemissao();
				}else {
					dtvencimento = notafiscalproduto.getDtEmissao();
				}
			}
		}
		return dtvencimento;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForMovimentacaoestoque(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 */
	public Notafiscalproduto findForMovimentacaoestoque(Nota nota) {
		return notafiscalprodutoDAO.findForMovimentacaoestoque(nota);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForMovimentacaoestoque(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForMovimentacaoestoque(String whereIn) {
		return notafiscalprodutoDAO.findForMovimentacaoestoque(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#findForCalculoTotalGeral(NotafiscalprodutoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForCalculoTotalGeral(NotafiscalprodutoFiltro filtro){
		return notafiscalprodutoDAO.findForCalculoTotalGeral(filtro);
	}
	
	/**
	 * M�todo que cria copia da nota fiscal de produto
	 *
	 * @param form
	 * @return
	 * @author Luiz Fernando
	 */
	public Notafiscalproduto criaCopia(Notafiscalproduto origem) {
		Notafiscalproduto copia = new Notafiscalproduto();
		
		origem = this.loadForEntrada(origem);
		
		copia.setEmpresa(origem.getEmpresa());
		copia.setFinalidadenfe(origem.getFinalidadenfe());
		copia.setTipooperacaonota(origem.getTipooperacaonota());
		copia.setNaturezaoperacao(origem.getNaturezaoperacao());
		copia.setMunicipiogerador(origem.getMunicipiogerador());
		copia.setFormapagamentonfe(origem.getFormapagamentonfe());
		copia.setResponsavelfrete(origem.getResponsavelfrete());
		copia.setOperacaonfe(origem.getOperacaonfe());
		copia.setLocaldestinonfe(origem.getLocaldestinonfe());
		copia.setPresencacompradornfe(origem.getPresencacompradornfe());
		copia.setSerienfe(null);
		
		if(origem.getProjeto() != null && (origem.getProjeto().getCdprojeto() != null)){
			Projeto projetoCopiado = projetoService.load(origem.getProjeto());
			if(Situacaoprojeto.CANCELADO.equals(projetoCopiado.getSituacao()) || Situacaoprojeto.CONCLUIDO.equals(projetoCopiado.getSituacao())){
				copia.setProjeto(null);
			}else{
				copia.setProjeto(origem.getProjeto());
			}
		}
		
		copia.setCliente(origem.getCliente());
		copia.setEnderecoCliente(origem.getEnderecoCliente());
		copia.setEnderecoavulso(origem.getEnderecoavulso());
		copia.setEndereconota(origem.getEndereconota());
		if(copia.getEndereconota() != null){
			copia.getEndereconota().setCdendereco(null);
			copia.getEndereconota().setNota(null);
		}
		
		copia.setTelefoneCliente(origem.getTelefoneCliente());
		if(origem.getCliente() != null && origem.getCliente().getCnpj() != null)
			copia.getCliente().setCpfcnpj(origem.getCliente().getCnpj().toString());

		if(origem.getListaItens() != null && !origem.getListaItens().isEmpty()){
			for(Notafiscalprodutoitem item : origem.getListaItens()){
				item.setCdnotafiscalprodutoitem(null);
				item.setNotafiscalproduto(null);
				item.setCdentregadocumento(null);
				item.setCdusuarioaltera(null);
			}
			copia.setListaItens(origem.getListaItens());
		}
		
		copia.setEnderecodiferenteretirada(origem.getEnderecodiferenteretirada());
		copia.setEnderecodiferenteentrega(origem.getEnderecodiferenteentrega());
		copia.setEnderecotransportador(origem.getEnderecotransportador());
		copia.setCadastrarcobranca(origem.getCadastrarcobranca());
		copia.setCadastrarreferencia(origem.getCadastrarreferencia());
		copia.setCadastrartransportador(origem.getCadastrartransportador());
		
		copia.setPrazopagamentofatura(origem.getPrazopagamentofatura());
		
		if(origem.getListaReferenciada() != null && !origem.getListaReferenciada().isEmpty()){
			for(Notafiscalprodutoreferenciada referencia : origem.getListaReferenciada()){
				referencia.setCdnotafiscalprodutoreferenciada(null);
				referencia.setNotafiscalproduto(null);
			}
			copia.setListaReferenciada(origem.getListaReferenciada());
		}
		
		copia.setTipopessoaretirada(origem.getTipopessoaretirada());
		copia.setLogradouroretirada(origem.getLogradouroretirada());
		copia.setNumeroretirada(origem.getNumeroretirada());
		copia.setComplementoretirada(origem.getComplementoretirada());
		copia.setBairroretirada(origem.getBairroretirada());
		copia.setMunicipioretirada(origem.getMunicipioretirada());
		
		copia.setTipopessoaentrega(origem.getTipopessoaentrega());
		copia.setLogradouroentrega(origem.getLogradouroentrega());
		copia.setNumeroentrega(origem.getNumeroentrega());
		copia.setComplementoentrega(origem.getComplementoentrega());
		copia.setBairroentrega(origem.getBairroentrega());
		copia.setMunicipioentrega(origem.getMunicipioentrega());
		
		copia.setTransportador(origem.getTransportador());
		copia.setEnderecotransportador(origem.getEnderecotransportador());
		copia.setTelefonetransportador(origem.getTelefonetransportador());
		copia.setTransporteveiculo(origem.getTransporteveiculo());
		copia.setPlacaveiculo(origem.getPlacaveiculo());
		copia.setRntc(origem.getRntc());
		copia.setQtdevolume(origem.getQtdevolume());
		copia.setEspvolume(origem.getEspvolume());
		copia.setMarcavolume(origem.getMarcavolume());
		copia.setNumvolume(origem.getNumvolume());
		copia.setPesoliquido(origem.getPesoliquido());
		copia.setPesobruto(origem.getPesobruto());

		copia.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		copia.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		copia.setValornaoincluido(origem.getValornaoincluido());
		copia.setValorprodutos(origem.getValorprodutos());
		copia.setValorfrete(origem.getValorfrete());
		copia.setValordesconto(origem.getValordesconto());
		copia.setValorseguro(origem.getValorseguro());
		copia.setOutrasdespesas(origem.getOutrasdespesas());
		copia.setValor(origem.getValor());
		
		copia.setValorbcicms(origem.getValorbcicms());
		copia.setValoricms(origem.getValoricms());
		copia.setValorbcicmsst(origem.getValorbcicmsst());
		copia.setValoricmsst(origem.getValoricmsst());
		copia.setValoripi(origem.getValoripi());
		copia.setValorii(origem.getValorii());
		copia.setValorpis(origem.getValorpis());
		copia.setValorpisretido(origem.getValorpisretido());
		copia.setValorcofins(origem.getValorcofins());
		copia.setValorcofinsretido(origem.getValorcofinsretido());
		copia.setValorbccsll(origem.getValorbccsll());
		copia.setValorcsll(origem.getValorcsll());
		copia.setValorbcirrf(origem.getValorbcirrf());
		copia.setValorirrf(origem.getValorirrf());
		copia.setValorbcprevsocial(origem.getValorbcprevsocial());
		copia.setValorprevsocial(origem.getValorprevsocial());
		copia.setValorservicos(origem.getValorservicos());
		copia.setValorbciss(origem.getValorbciss());
		copia.setValoriss(origem.getValoriss());
		copia.setValorpisservicos(origem.getValorpisservicos());
		copia.setValorcofinsservicos(origem.getValorcofinsservicos());
		copia.setValorfcp(origem.getValorfcp());
		copia.setValorfcpretidost(origem.getValorfcpretidost());
		copia.setValorfcpretidostanteriormente(origem.getValorfcpretidostanteriormente());
		copia.setValorbcfcpdestinatario(origem.getValorbcfcpdestinatario());
		copia.setValoripidevolvido(origem.getValoripidevolvido());
		
		copia.setInfoadicionalcontrib(origem.getInfoadicionalcontrib());
		copia.setInfoadicionalfisco(origem.getInfoadicionalfisco());
		
		if(SinedUtil.isListNotEmpty(origem.getListaItens()) && parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO)){
			Localarmazenagem localarmazenagemPadrao = localarmazenagemService.loadPrincipalByEmpresa(origem.getEmpresa());
			for(Notafiscalprodutoitem item : origem.getListaItens()){
				if(item.getLocalarmazenagem() == null && origem.getEmpresa() != null){
					item.setLocalarmazenagem(localarmazenagemPadrao);
				}
			}
		}
		
		return copia;
	}
	
	public void verificaUnidademedidaQtde(Notafiscalprodutoitem nfpi) {
		if(nfpi != null && nfpi.getMaterial() != null && nfpi.getUnidademedida() != null && 
				nfpi.getUnidademedida().getCdunidademedida() != null){
			Material material = materialService.unidadeMedidaMaterial(nfpi.getMaterial());
			if(material != null && material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() != null && 
					!nfpi.getUnidademedida().getCdunidademedida().equals(material.getUnidademedida().getCdunidademedida())){
				nfpi.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
												nfpi.getQtde(), 
												nfpi.getUnidademedida(), 
												material,null,1.0));
				if(nfpi.getValorunitario() != null){
					Double fracao = unidademedidaService.getFracaoconversaoUnidademedida(material.getUnidademedida(), nfpi.getQtde(), nfpi.getUnidademedida(), material, null);
					if(fracao != null){
						nfpi.setValorunitario(nfpi.getValorunitario()*fracao);
					}
				}
			}
		}
		
		Material aux_material = materialService.load(nfpi.getMaterial(), "material.cdmaterial, material.unidademedida");
		if(aux_material != null && aux_material.getUnidademedida() != null){
			nfpi.setQtde(SinedUtil.roundByUnidademedida(nfpi.getQtde(), aux_material.getUnidademedida()));
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#isExisteNotaEmitida(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 24/10/2013
	 */
	public Boolean isExisteNotaEmitida(Venda venda) {
		return notafiscalprodutoDAO.isExisteNotaEmitida(venda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#updateDataentradasaidaDataatual(String whereIn)
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 21/11/2013
	 */
	public void updateDataentradasaidaDataatual(String whereIn) {
		notafiscalprodutoDAO.updateDataentradasaidaDataatual(whereIn);
	}

	/**
	 * M�todo que cria a lista de materiais com as informa��es adicionais
	 *
	 * @param nota
	 * @author Luiz Fernando
	 * @since 18/12/2013
	 */
	public void addListaMateralInfoadicional(Notafiscalproduto nota) {
		if(nota != null && nota.getListaItens() != null && !nota.getListaItens().isEmpty()){
			Naturezaoperacao naturezaoperacao = nota.getNaturezaoperacao();
			Empresa empresa = nota.getEmpresa();
			Cliente cliente = nota.getCliente();
			if(cliente != null && cliente.getCdpessoa() != null){
				cliente = clienteService.loadForTributacao(cliente);
			}
			Endereco enderecocliente = nota.getEnderecoCliente();
			for(Notafiscalprodutoitem item : nota.getListaItens()){
				if (item.getMaterial() != null){	
					Material material = materialService.loadForTributacao(item.getMaterial());
					if(material != null){
						List<Categoria> listaCategoriaCliente = null;
						if(cliente != null){
							listaCategoriaCliente = categoriaService.findByPessoa(cliente);
						}
						
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
														empresa,
														naturezaoperacao, 
														material.getMaterialgrupo(),
														cliente,
														true, 
														material, 
														item.getNcmcompleto(),
														enderecocliente, 
														null,
														listaCategoriaCliente,
														null,
														nota.getOperacaonfe(),
														nota.getModeloDocumentoFiscalEnum(),
														nota.getLocaldestinonfe(),
														nota.getDtEmissao()));
						
						Grupotributacao grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
						
						if(grupotributacao != null){
							String contribuinte = escapeSingleQuotes(Util.strings.emptyIfNull(grupotributacao.getInfoadicionalcontrib()));
							String fisco = escapeSingleQuotes(Util.strings.emptyIfNull(grupotributacao.getInfoadicionalfisco()));
							
							if((contribuinte != null && !"".equals(contribuinte)) || (fisco != null && !"".equals(fisco))){ 
								if(nota.getListaMaterialInfoadicional() == null)
									nota.setListaMaterialInfoadicional(new ArrayList<MaterialInfoadicional>());
								
								MaterialInfoadicional materialInfoadicional = new MaterialInfoadicional();
								materialInfoadicional.setMaterial(item.getMaterial());
								materialInfoadicional.setInfoadicionalcontrib(contribuinte);
								materialInfoadicional.setInfoadicionalfisco(fisco);
								nota.getListaMaterialInfoadicional().add(materialInfoadicional);
							}
						}
					}
				}
			}
		}
	}
	
	private String escapeSingleQuotes(String string) {
		return string.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'");
	}
	
	public AjaxCalculaTotalValorNotaBean calculaTotalNota(Finalidadenfe finalidadenfe, List<Notafiscalprodutoitem> listaNotafiscalprodutoitem) {
		Money totalNota = new Money();
		Money totalNaoincluido = new Money();
		Money totalProdutos = new Money();
		Money totalFrete = new Money();
		Money totalDesconto = new Money();
		Money totalSeguro = new Money();
		Money totalOutrasdespesas = new Money();
		Money totalBcicms = new Money();
		Money totalIcms = new Money();
		Money totalBcicmsst = new Money();
		Money totalIcmsst = new Money();
		Money totalIi = new Money();
		Money totalIpi = new Money();
		Money totalPis = new Money();
		Money totalCofins = new Money();
		Money totalServicos = new Money();
		Money totalBciss = new Money();
		Money totalIss = new Money();
		Money totalBccsll = new Money();
		Money totalCsll = new Money();
		Money totalBcir = new Money();
		Money totalIr = new Money();
		Money totalBcinss = new Money();
		Money totalInss = new Money();
		Money totalPisservicos = new Money();
		Money totalCofinsservicos = new Money();
		Money totalTributos = new Money();
		Money totalImpostoFederal = new Money();
		Money totalImpostoEstadual = new Money();
		Money totalImpostoMunicipal = new Money();
		Money totalIcmsdesonerado = new Money();
		Money totalIcmsdesoneradoAbater = new Money();
		
		Money totalFcp = new Money();
		Money totalFcpst = new Money();
		Money totalFcpstretidoanteriormente = new Money();
		Money totalIpidevolvido = new Money();
		
		Money totaldeducaoservico = new Money();
		Money totaloutrasretencoesservico = new Money();
		Money totaldescontocondicionadoservico = new Money();
		Money totaldescontoincondicionadoservico = new Money();
		Money totalissretido = new Money();
		
		Double pesoliquido = 0d;
		Double pesobruto = 0d;
		Double qtdeTotal = 0d;
		
		Localdestinonfe localdestinonfe = Localdestinonfe.INTERNA;
		
		boolean haveServico = false;
		List<Cfop> listaCfop = new ArrayList<Cfop>();
		Money totalNaoGerarReceita = new Money();
		
		if (listaNotafiscalprodutoitem != null) {
			for (Notafiscalprodutoitem notafiscalprodutoitem : listaNotafiscalprodutoitem) {
				Cfop cfop = null;
				if(notafiscalprodutoitem.getCfop() != null){
					if(listaCfop.indexOf(notafiscalprodutoitem.getCfop()) != -1){
						cfop = listaCfop.get(listaCfop.indexOf(notafiscalprodutoitem.getCfop()));
					}else {
						cfop = cfopService.load(notafiscalprodutoitem.getCfop(), "cfop.cdcfop, cfop.naoconsiderarreceita, cfop.cfopescopo");
					}
				}
				
				if(cfop != null && cfop.getCfopescopo() != null){
					if(cfop.getCfopescopo().equals(Cfopescopo.ESTADUAL)){
						localdestinonfe = Localdestinonfe.INTERNA;
					} else if(cfop.getCfopescopo().equals(Cfopescopo.FORA_DO_ESTADO)){
						localdestinonfe = Localdestinonfe.INTERESTADUAL;
					} else if(cfop.getCfopescopo().equals(Cfopescopo.INTERNACIONAL)){
						localdestinonfe = Localdestinonfe.EXTERIOR;
					}
				}
				
				if(notafiscalprodutoitem.getValortotaltributos() != null){
					totalTributos = totalTributos.add(notafiscalprodutoitem.getValortotaltributos().round());
				}
				
				if(notafiscalprodutoitem.getValorImpostoFederal() != null){
					totalImpostoFederal = totalImpostoFederal.add(notafiscalprodutoitem.getValorImpostoFederal().round());
				}
				
				if(notafiscalprodutoitem.getValorImpostoEstadual() != null){
					totalImpostoEstadual = totalImpostoEstadual.add(notafiscalprodutoitem.getValorImpostoEstadual().round());
				}
				
				if(notafiscalprodutoitem.getValorImpostoMunicipal() != null){
					totalImpostoMunicipal = totalImpostoMunicipal.add(notafiscalprodutoitem.getValorImpostoMunicipal().round());
				}
				
				if(notafiscalprodutoitem.getTributadoicms() != null && !notafiscalprodutoitem.getTributadoicms()){
					haveServico = true;
				}
				
				if(notafiscalprodutoitem.getValordesoneracaoicms() != null)
					totalIcmsdesonerado = totalIcmsdesonerado.add(notafiscalprodutoitem.getValordesoneracaoicms().round());
				if(notafiscalprodutoitem.getAbaterdesoneracaoicms() != null && notafiscalprodutoitem.getAbaterdesoneracaoicms() && notafiscalprodutoitem.getValordesoneracaoicms() != null)
					totalIcmsdesoneradoAbater = totalIcmsdesoneradoAbater.add(notafiscalprodutoitem.getValordesoneracaoicms().round());
				if (notafiscalprodutoitem.getValorbciss() != null)
					totalBciss = totalBciss.add(notafiscalprodutoitem.getValorbciss().round());					
				if (notafiscalprodutoitem.getValoriss() != null)
					totalIss = totalIss.add(notafiscalprodutoitem.getValoriss().round());
				if (notafiscalprodutoitem.getValorbccsll() != null)
					totalBccsll = totalBccsll.add(notafiscalprodutoitem.getValorbccsll().round());	
				if (notafiscalprodutoitem.getValorcsll() != null)
					totalCsll = totalCsll.add(notafiscalprodutoitem.getValorcsll().round());
				if (notafiscalprodutoitem.getValorbcir() != null)
					totalBcir = totalBcir.add(notafiscalprodutoitem.getValorbcir().round());	
				if (notafiscalprodutoitem.getValorir() != null)
					totalIr = totalIr.add(notafiscalprodutoitem.getValorir().round());
				if (notafiscalprodutoitem.getValorbcinss() != null)
					totalBcinss = totalBcinss.add(notafiscalprodutoitem.getValorbcinss().round());	
				if (notafiscalprodutoitem.getValorinss() != null)
					totalInss = totalInss.add(notafiscalprodutoitem.getValorinss().round());
				
				if (notafiscalprodutoitem.getValoricms() != null && (notafiscalprodutoitem.getTipocobrancaicms() == null || 
						(!Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.ISENTA.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.NAO_TRIBUTADA.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(notafiscalprodutoitem.getTipocobrancaicms()) &&
						 !Tipocobrancaicms.SUSPENSAO.equals(notafiscalprodutoitem.getTipocobrancaicms())))){
					if (notafiscalprodutoitem.getValorbcicms()!=null){
						totalBcicms = totalBcicms.add(notafiscalprodutoitem.getValorbcicms().round());  
					}
					if(!Tipocobrancaicms.DIFERIMENTO.equals(notafiscalprodutoitem.getTipocobrancaicms())){
						if (notafiscalprodutoitem.getValoricms()!=null){
							totalIcms = totalIcms.add(notafiscalprodutoitem.getValoricms().round());
						}
					}
				}
				
				if(notafiscalprodutoitem.getTipocobrancaicms() != null && 
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO) &&
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE) &&
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO)){
					if (notafiscalprodutoitem.getValorbcicmsst() != null)
						totalBcicmsst = totalBcicmsst.add(notafiscalprodutoitem.getValorbcicmsst().round());
					if (notafiscalprodutoitem.getValoricmsst() != null)
						totalIcmsst = totalIcmsst.add(notafiscalprodutoitem.getValoricmsst().round());
				}
				
				if(notafiscalprodutoitem.getValorfcp() != null)
					totalFcp = totalFcp.add(notafiscalprodutoitem.getValorfcp().round());
				
				if(notafiscalprodutoitem.getTipocobrancaicms() != null && 
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO) &&
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE) &&
						!notafiscalprodutoitem.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO)){
					if(notafiscalprodutoitem.getValorfcpst() != null)
						totalFcpst = totalFcpst.add(notafiscalprodutoitem.getValorfcpst().round());	
				} else {
					if(notafiscalprodutoitem.getValorfcpst() != null)
						totalFcpstretidoanteriormente = totalFcpstretidoanteriormente.add(notafiscalprodutoitem.getValorfcpst().round());	
				}
				
				if (notafiscalprodutoitem.getValoripi() != null)
					totalIpi = totalIpi.add(notafiscalprodutoitem.getValoripi().round());
				
				if(notafiscalprodutoitem.getValoripidevolvido() != null)
					totalIpidevolvido = totalIpidevolvido.add(notafiscalprodutoitem.getValoripidevolvido().round());
				
				if(notafiscalprodutoitem.getValorii() != null)
					totalIi = totalIi.add(notafiscalprodutoitem.getValorii().round());
						
				if (notafiscalprodutoitem.getTributadoicms() == null || !notafiscalprodutoitem.getTributadoicms().booleanValue()) {
					if (notafiscalprodutoitem.getValorbruto() != null)
						totalServicos = totalServicos.add(notafiscalprodutoitem.getValorbruto().round());					
					if (notafiscalprodutoitem.getValorpis() != null)
						totalPisservicos = totalPisservicos.add(notafiscalprodutoitem.getValorpis().round());
					if (notafiscalprodutoitem.getValorcofins() != null)
						totalCofinsservicos = totalCofinsservicos.add(notafiscalprodutoitem.getValorcofins().round()); 	
					if (notafiscalprodutoitem.getValordeducaoservico() != null)
						totaldeducaoservico = totaldeducaoservico.add(notafiscalprodutoitem.getValordeducaoservico().round());
					if (notafiscalprodutoitem.getValoroutrasretencoesservico() != null)
						totaloutrasretencoesservico = totaloutrasretencoesservico.add(notafiscalprodutoitem.getValoroutrasretencoesservico().round());
					if (notafiscalprodutoitem.getValordescontocondicionadoservico() != null)
						totaldescontocondicionadoservico = totaldescontocondicionadoservico.add(notafiscalprodutoitem.getValordescontocondicionadoservico().round());
					if (notafiscalprodutoitem.getValordescontoincondicionadoservico() != null)
						totaldescontoincondicionadoservico = totaldescontoincondicionadoservico.add(notafiscalprodutoitem.getValordescontoincondicionadoservico().round());
					if (notafiscalprodutoitem.getValorissretido() != null)
						totalissretido = totalissretido.add(notafiscalprodutoitem.getValorissretido().round());
				} else {
					if (notafiscalprodutoitem.getValorpis() != null)
						totalPis = totalPis.add(notafiscalprodutoitem.getValorpis().round());
					if (notafiscalprodutoitem.getValorcofins() != null)
						totalCofins = totalCofins.add(notafiscalprodutoitem.getValorcofins().round());
				}
					
				if (notafiscalprodutoitem.getIncluirvalorprodutos() == null || !notafiscalprodutoitem.getIncluirvalorprodutos().booleanValue()) {
					if (notafiscalprodutoitem.getValorbruto() != null)
						totalNaoincluido = totalNaoincluido.add(notafiscalprodutoitem.getValorbruto().round());
				} else {
					if (notafiscalprodutoitem.getValorbruto() != null){
						totalProdutos = totalProdutos.add(notafiscalprodutoitem.getValorbruto().round());
						
						if(cfop != null && cfop.getNaoconsiderarreceita() != null && cfop.getNaoconsiderarreceita()){
							totalNaoGerarReceita = totalNaoGerarReceita.add(notafiscalprodutoitem.getValorbruto().round());
						}
					}
				}				

				if (notafiscalprodutoitem.getValorfrete() != null)
					totalFrete = totalFrete.add(notafiscalprodutoitem.getValorfrete().round());
				if (notafiscalprodutoitem.getValordesconto() != null)
					totalDesconto = totalDesconto.add(notafiscalprodutoitem.getValordesconto().round());
				if (notafiscalprodutoitem.getValorseguro() != null)
					totalSeguro = totalSeguro.add(notafiscalprodutoitem.getValorseguro().round());
				if (notafiscalprodutoitem.getOutrasdespesas() != null)
					totalOutrasdespesas = totalOutrasdespesas.add(notafiscalprodutoitem.getOutrasdespesas().round());			

				if(notafiscalprodutoitem.getPesoliquido() != null)
					pesoliquido += notafiscalprodutoitem.getPesoliquido();
				
				if(notafiscalprodutoitem.getPesobruto() != null)
					pesobruto += notafiscalprodutoitem.getPesobruto();
				
				if(notafiscalprodutoitem.getQtde() != null)
					qtdeTotal += notafiscalprodutoitem.getQtde();
			}
			
			totalNota = totalNota
							.add(totalProdutos)
							.subtract(totalDesconto)
							.add(totalFrete)
							.add(totalSeguro)
							.add(totalOutrasdespesas)
							.add(totalIcmsst)
							.add(totalFcpst)
							.add(totalIpi)
							.add(totalIpidevolvido)
							.add(totalIi)
							.subtract(totalIcmsdesoneradoAbater);	
		}		
		
		
		AjaxCalculaTotalValorNotaBean bean = new AjaxCalculaTotalValorNotaBean();
		
		bean.setLocaldestinonfe(localdestinonfe);
		bean.setTotalNota(totalNota);
		bean.setTotalNaoincluido(totalNaoincluido);
		bean.setTotalProdutos(totalProdutos);
		bean.setTotalFrete(totalFrete);
		bean.setTotalDesconto(totalDesconto);
		bean.setTotalSeguro(totalSeguro);
		bean.setTotalOutrasdespesas(totalOutrasdespesas);
		bean.setTotalBcicms(totalBcicms);
		bean.setTotalIcms(totalIcms);
		bean.setTotalBcicmsst(totalBcicmsst);
		bean.setTotalIcmsst(totalIcmsst);
		bean.setTotalIi(totalIi);
		bean.setTotalIpi(totalIpi);
		bean.setTotalPis(totalPis);
		bean.setTotalCofins(totalCofins);
		bean.setTotalServicos(totalServicos);
		bean.setTotalBciss(totalBciss);
		bean.setTotalIss(totalIss);
		bean.setTotalBccsll(totalBccsll);
		bean.setTotalCsll(totalCsll);
		bean.setTotalBcir(totalBcir);
		bean.setTotalIr(totalIr);
		bean.setTotalBcinss(totalBcinss);
		bean.setTotalInss(totalInss);
		bean.setTotalPisservicos(totalPisservicos);
		bean.setTotalCofinsservicos(totalCofinsservicos);
		bean.setTotalTributos(totalTributos);
		bean.setTotalImpostoFederal(totalImpostoFederal);
		bean.setTotalImpostoEstadual(totalImpostoEstadual);
		bean.setTotalImpostoMunicipal(totalImpostoMunicipal);
		bean.setPesoliquido(pesoliquido);
		bean.setPesobruto(pesobruto);
		bean.setQtdeTotal(qtdeTotal);
		bean.setHaveServico(haveServico);
		bean.setTotalIcmsdesonerado(totalIcmsdesonerado);
		bean.setTotaldeducaoservico(totaldeducaoservico);
		bean.setTotaloutrasretencoesservico(totaloutrasretencoesservico);
		bean.setTotaldescontocondicionadoservico(totaldescontocondicionadoservico);
		bean.setTotaldescontoincondicionadoservico(totaldescontoincondicionadoservico);
		bean.setTotalissretido(totalissretido);
		bean.setTotalNotaReceita(totalNota.subtract(totalNaoGerarReceita));
		bean.setTotalFcp(totalFcp);
		bean.setTotalFcpst(totalFcpst);
		bean.setTotalFcpstretidoanteriormente(totalFcpstretidoanteriormente);
		bean.setTotalIpidevolvido(totalIpidevolvido);
		
		return bean;
	}
	
	public void setTotalNota(Notafiscalproduto nf) {
		AjaxCalculaTotalValorNotaBean beanTot = this.calculaTotalNota(nf.getFinalidadenfe(), nf.getListaItens());
		
		nf.setLocaldestinonfe(beanTot.getLocaldestinonfe());
		nf.setValornaoincluido(beanTot.getTotalNaoincluido());
		nf.setValorprodutos(beanTot.getTotalProdutos());
		nf.setValorfrete(beanTot.getTotalFrete());
		nf.setValordesconto(beanTot.getTotalDesconto());
		nf.setValorseguro(beanTot.getTotalSeguro());
		nf.setOutrasdespesas(beanTot.getTotalOutrasdespesas());
		nf.setValor(beanTot.getTotalNota());
		nf.setValortotaltributos(beanTot.getTotalTributos());
		nf.setValorTotalImpostoFederal(beanTot.getTotalImpostoFederal());
		nf.setValorTotalImpostoEstadual(beanTot.getTotalImpostoEstadual());
		nf.setValorTotalImpostoMunicipal(beanTot.getTotalImpostoMunicipal());
		nf.setValorbcicms(beanTot.getTotalBcicms());
		nf.setValoricms(beanTot.getTotalIcms());
		nf.setValorbcicmsst(beanTot.getTotalBcicmsst());
		nf.setValoricmsst(beanTot.getTotalIcmsst());
		nf.setValorii(beanTot.getTotalIi());
		nf.setValoripi(beanTot.getTotalIpi());
		nf.setValorpis(beanTot.getTotalPis());
		nf.setValorcofins(beanTot.getTotalCofins());
		nf.setValorservicos(beanTot.getTotalServicos());
		nf.setValorbciss(beanTot.getTotalBciss());
		nf.setValoriss(beanTot.getTotalIss());
		nf.setValorpisservicos(beanTot.getTotalPisservicos());
		nf.setValorcofinsservicos(beanTot.getTotalCofinsservicos());
		nf.setValordesoneracao(beanTot.getTotalIcmsdesonerado());
		nf.setValordeducaoservico(beanTot.getTotaldeducaoservico());
		nf.setValoroutrasretencoesservico(beanTot.getTotaloutrasretencoesservico());
		nf.setValordescontocondicionadoservico(beanTot.getTotaldescontocondicionadoservico());
		nf.setValordescontoincondicionadoservico(beanTot.getTotaldescontoincondicionadoservico());
		nf.setValorissretido(beanTot.getTotalissretido());
		
		if(nf.getCadastrartransportador() != null && nf.getCadastrartransportador()){
			nf.setPesobruto(beanTot.getPesobruto());
			nf.setPesoliquido(beanTot.getPesoliquido());
			if(Boolean.FALSE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP))){
				nf.setQtdevolume(beanTot.getQtdeTotal() != null ? beanTot.getQtdeTotal().intValue() : null);				
			}
		}
		
		if(nf.getCadastrarcobranca() != null && nf.getCadastrarcobranca()){
			
			//AT 54951
			if (nf.isGerarNotaIndustrializacaoRetorno()){
				Money valororiginalfaturaAux = new Money(0);
				if (nf.getListaDuplicata()!=null){
					for (Notafiscalprodutoduplicata nfpd: nf.getListaDuplicata()){
						if (nfpd.getValor()!=null){
							valororiginalfaturaAux = valororiginalfaturaAux.add(nfpd.getValor());
						}
					}
					nf.setValororiginalfatura(valororiginalfaturaAux);
				}else {
					nf.setValororiginalfatura(beanTot.getTotalNotaReceita());
				}
			}else {
				nf.setValororiginalfatura(beanTot.getTotalNotaReceita());
			}
			
			Money valororiginal = nf.getValororiginalfatura();
			if(valororiginal == null){
				valororiginal = new Money();
			}
			
			Money valordesconto = nf.getValordescontofatura();
			if(valordesconto == null){
				valordesconto = new Money();
			}
			
			nf.setValorliquidofatura(valororiginal.subtract(valordesconto));
		}else {
			nf.setValororiginalfaturaTransient(beanTot.getTotalNotaReceita());
			
			Money valororiginal = nf.getValororiginalfaturaTransient();
			if(valororiginal == null){
				valororiginal = new Money();
			}
			
			Money valordesconto = nf.getValordescontofatura();
			if(valordesconto == null){
				valordesconto = new Money();
			}
			
			nf.setValorliquidofaturaTransient(valororiginal.subtract(valordesconto));
		}
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao) {
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Material material = materialService.loadWithMatrialgrupo(grupotributacao.getMaterialTrans());
		Naturezaoperacao naturezaoperacao = grupotributacao.getNaturezaoperacaoTrans();
		Cliente cliente = grupotributacao.getClienteTrans() != null && grupotributacao.getClienteTrans().getCdpessoa() != null ? 
										clienteService.loadForTributacao(grupotributacao.getClienteTrans()) : null;
		Empresa empresa = null; 
		Endereco enderecoOrigem = null;
		if (grupotributacao.getEmpresaTributacaoOrigem() != null && grupotributacao.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(grupotributacao.getEmpresaTributacaoOrigem());
			enderecoOrigem = empresa.getEndereco();
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
			enderecoOrigem = empresa.getEndereco();
		}		
		
		Endereco enderecoDestino = null;
		if(grupotributacao.getEnderecoavulso() != null && grupotributacao.getEnderecoavulso() && grupotributacao.getEndereconotaDestino() != null && 
				grupotributacao.getEndereconotaDestino().getMunicipio() != null){
			enderecoDestino =  grupotributacao.getEndereconotaDestino();
		}else {
			enderecoDestino = grupotributacao.getEnderecoTributacaoDestino();
			if(enderecoDestino != null && enderecoDestino.getCdendereco() != null){
				enderecoDestino = enderecoService.carregaEnderecoComUfPais(enderecoDestino);
			}
		}
		
		Grupotributacao gt = grupotributacaoService.loadForEntrada(grupotributacao);
		material = materialService.loadTributacao(material, empresa, material.getMaterialgrupo(), cliente,
													enderecoOrigem, enderecoDestino, gt);
		
		Cfop cfop = null;
		
		if(gt.getCfopgrupo() != null){
			material.getMaterialgrupo().setCfop(gt.getCfopgrupo());
			cfop = cfopService.findForNFProduto(material, enderecoOrigem, enderecoDestino);
		}
		
		if(cfop == null && naturezaoperacao != null && naturezaoperacao.getCdnaturezaoperacao() != null && enderecoOrigem != null && enderecoDestino != null){
			Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoOrigem, enderecoDestino);
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, cfopescopo);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				cfop = listaNaturezaoperacaocfop.get(0).getCfop();
			}
		}
		
		boolean possuiCfop = request.getParameter("cfop")!=null && !request.getParameter("cfop").equals("<null>");
		boolean sugestaoCfop = naturezaoperacao != null && possuiCfop;
		
		
		view.println("var sugestaoCfop = " + sugestaoCfop + ";");
		view.println("var cfop = '" + (cfop != null && cfop.getCdcfop() != null ? "br.com.linkcom.sined.geral.bean.Cfop[cdcfop="+cfop.getCdcfop()+",descricaoCodigo="+cfop.getDescricaoCodigo().replace(",", "")+"]" : "") + "';");
		view.println("var descricaoCfop = '" + (cfop != null && cfop.getCdcfop() != null ? cfop.getDescricaoCodigo() : "") + "';");
		
		view.println("var incluiricmsvalor = " + (gt.getIncluiricmsvalor() != null && gt.getIncluiricmsvalor() ? "true" : "false") + ";");
		view.println("var tributadoicms = '" + (gt.getTributadoicms() != null ? gt.getTributadoicms() : "") + "';");
		view.println("var origemprodutoicms = '" + (gt.getOrigemprodutoicms() != null && gt.getOrigemprodutoicms().getValue() != null ? gt.getOrigemprodutoicms().name() : "") + "';");
		view.println("var origemprodutoicms_desc = '" + (gt.getOrigemprodutoicms() != null && gt.getOrigemprodutoicms().getValue() != null ? gt.getOrigemprodutoicms() : "") + "';");
		view.println("var tipotributacaoicms = '" + (gt.getTipotributacaoicms() != null && gt.getTipotributacaoicms().getValue() != null ? gt.getTipotributacaoicms().name() : "") + "';");
		view.println("var tipotributacaoicms_desc = '" + (gt.getTipotributacaoicms() != null && gt.getTipotributacaoicms().getValue() != null ? gt.getTipotributacaoicms() : "") + "';");
		view.println("var tipocobrancaicms = '" + (gt.getTipocobrancaicms() != null && gt.getTipocobrancaicms().getValue() != null ? gt.getTipocobrancaicms().name() : "") + "';");
		view.println("var tipocobrancaicms_desc = '" + (gt.getTipocobrancaicms() != null && gt.getTipocobrancaicms().getValue() != null ? gt.getTipocobrancaicms() : "") + "';");
		
		view.println("var itemlistaservico_value = '" + (gt.getItemlistaservico() != null && gt.getItemlistaservico().getCditemlistaservico() != null ? 
				"br.com.linkcom.sined.geral.bean.Itemlistaservico[cditemlistaservico="+gt.getItemlistaservico().getCditemlistaservico() +
				",codigo=" + gt.getItemlistaservico().getCodigo() + 
				",descricao=" + (gt.getItemlistaservico().getDescricao() != null ? new StringUtils().addScapesToDescription(gt.getItemlistaservico().getDescricao()) : "")
				+"]" : "") + "';");
		view.println("var descricaoitemlistaservico = '" + (gt.getItemlistaservico() != null && gt.getItemlistaservico().getDescricaoCombo() != null ? Util.strings.escape(gt.getItemlistaservico().getDescricaoCombo()) : "") + "';");
		view.println("var cdmunicipioiss = '" + (gt.getMunicipioincidenteiss() != null? gt.getMunicipioincidenteiss().getCdmunicipio():
																		enderecoOrigem != null && enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getCdmunicipio() != null ? enderecoOrigem.getMunicipio().getCdmunicipio() : "") + "';");
		view.println("var nomemunicipioiss = '" + (gt.getMunicipioincidenteiss() != null? gt.getMunicipioincidenteiss().getNomecompleto():
													enderecoOrigem != null && enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getNome() != null ? Util.strings.escape(enderecoOrigem.getMunicipio().getNomecompleto()) : "") + "';");
		
		view.println("var icms = '" + (material.getAliquotaicms() != null ? SinedUtil.descriptionDecimal(material.getAliquotaicms().getValue().doubleValue()) : "") + "';");		
		view.println("var icmsst = '" + (material.getAliquotaicmsst() != null ? SinedUtil.descriptionDecimal(material.getAliquotaicmsst().getValue().doubleValue()) : "") + "';");
		view.println("var modalidadebcicms = '" + (gt.getModalidadebcicms() != null && gt.getModalidadebcicms().getValue() != null ? gt.getModalidadebcicms().name() : "") + "';");
		view.println("var modalidadebcicms_desc = '" + (gt.getModalidadebcicms() != null && gt.getModalidadebcicms().getValue() != null ? gt.getModalidadebcicms() : "") + "';");		
		view.println("var modalidadebcicmsst = '" + (gt.getModalidadebcicmsst() != null && gt.getModalidadebcicmsst().getValue() != null ? gt.getModalidadebcicmsst().name() : "") + "';");
		view.println("var cest = '" + (org.apache.commons.lang.StringUtils.isNotBlank(material.getCest()) ? material.getCest() : (gt.getCest() != null ? gt.getCest() : "")) + "';");
		view.println("var modalidadebcicmsst_desc = '" + (gt.getModalidadebcicmsst() != null && gt.getModalidadebcicmsst().getValue() != null ? gt.getModalidadebcicmsst() : "") + "';");
		view.println("var reducaobcicmsst = '" + (gt.getReducaobcicmsst() != null ? SinedUtil.descriptionDecimal(gt.getReducaobcicmsst()) : "") + "';");
		view.println("var margemvaloradicionalicmsst = '" + (material.getAliquotamva() != null ? SinedUtil.descriptionDecimal(material.getAliquotamva()) : "") + "';");
		view.println("var reducaobcicms = '" + (gt.getReducaobcicms() != null ? SinedUtil.descriptionDecimal(gt.getReducaobcicms()) : "") + "';");
		view.println("var bcoperacaopropriaicms = '" + (gt.getBcoperacaopropriaicms() != null ? gt.getBcoperacaopropriaicms() : "") + "';");
		view.println("var cduf = '" + (gt.getUficmsst() != null && gt.getUficmsst().getCduf() != null ? "br.com.linkcom.sined.geral.bean.Uf[cduf=" + gt.getUficmsst().getCduf() + "]" : "") + "';");
		view.println("var uficmsst = '" + (gt.getUficmsst() != null && gt.getUficmsst().getSigla() != null ? gt.getUficmsst().getSigla() : "") + "';");
		view.println("var motivodesoneracaoicms = '" + (gt.getMotivodesoneracaoicms() != null && gt.getMotivodesoneracaoicms().getValue() != null ? gt.getMotivodesoneracaoicms().name() : "") + "';");
		view.println("var percentualdesoneracaoicms = '" + (gt.getPercentualdesoneracaoicms() != null ? SinedUtil.descriptionDecimal(gt.getPercentualdesoneracaoicms()) : "") + "';");
		view.println("var abaterdesoneracaoicms = '" + (gt.getAbaterdesoneracaoicms() != null ? gt.getAbaterdesoneracaoicms() : "false") + "';");
		view.println("var aliquotacreditoicms = '" + (gt.getAliquotacreditoicms() != null ? gt.getAliquotacreditoicms() : "") + "';");
		
		view.println("var incluiripivalor = " + (gt.getIncluiripivalor() != null && gt.getIncluiripivalor() ? "true" : "false") + ";");
//		view.println("var codigoseloipi = '" + (materialgt.getCodigoseloipi() != null ? materialgt.getCodigoseloipi() : "") + "';");
//		view.println("var qtdeseloipi = '" + (materialgt.getQtdeseloipi() != null ? materialgt.getQtdeseloipi() : "") + "';");
//		view.println("var cnpjprodutoripi = '" + (materialgt.getCnpjprodutoripi() != null ? materialgt.getCnpjprodutoripi() : "") + "';");
		view.println("var tipocobrancaipi = '" + (gt.getTipocobrancaipi() != null ? gt.getTipocobrancaipi().name() : "") + "';");
		view.println("var tipocobrancaipi_desc = '" + (gt.getTipocobrancaipi() != null ? gt.getTipocobrancaipi() : "") + "';");
		view.println("var ipi = '" + (material.getAliquotaipi() != null ? SinedUtil.descriptionDecimal(material.getAliquotaipi().getValue().doubleValue()) : "") + "';");
		view.println("var tipocalculoipi = '" + (gt.getTipocalculoipi() != null ? gt.getTipocalculoipi().name() : "") + "';");
		view.println("var tipocalculoipi_desc = '" + (gt.getTipocalculoipi() != null ? gt.getTipocalculoipi() : "") + "';");
		view.println("var aliquotareaisipi = '" + (gt.getAliquotareaisipi() != null ? gt.getAliquotareaisipi() : "") + "';");
		view.println("var codigoenquadramentoipi = '" + (gt.getCodigoenquadramentoipi() != null ? gt.getCodigoenquadramentoipi() : "") + "';");
		
		view.println("var incluirpisvalor = " + (gt.getIncluirpisvalor() != null && gt.getIncluirpisvalor() ? "true" : "false") + ";");
		view.println("var tipocobrancapis = '" + (gt.getTipocobrancapis() != null ? gt.getTipocobrancapis().name() : "") + "';");
		view.println("var tipocobrancapis_desc = '" + (gt.getTipocobrancapis() != null ? gt.getTipocobrancapis() : "") + "';");
		view.println("var pis = '" + (material.getAliquotapis() != null ? SinedUtil.descriptionDecimal(material.getAliquotapis().getValue().doubleValue()) : (gt.getTipocobrancapis() != null ? "0,00" :"")) + "';");
		view.println("var tipocalculopis = '" + (gt.getTipocalculopis() != null ? gt.getTipocalculopis().name() : "") + "';");
		view.println("var tipocalculopis_desc = '" + (gt.getTipocalculopis() != null ? gt.getTipocalculopis() : "") + "';");
		view.println("var aliquotareaispis = '" + (gt.getAliquotareaispis() != null ? gt.getAliquotareaispis() : "") + "';");
		
		view.println("var incluircofinsvalor = " + (gt.getIncluircofinsvalor() != null && gt.getIncluircofinsvalor() ? "true" : "false") + ";");
		view.println("var tipocobrancacofins = '" + (gt.getTipocobrancacofins() != null ? gt.getTipocobrancacofins().name() : "") + "';");
		view.println("var tipocobrancacofins_desc = '" + (gt.getTipocobrancacofins() != null ? gt.getTipocobrancacofins() : "") + "';");
		view.println("var cofins = '" + (material.getAliquotacofins() != null ? SinedUtil.descriptionDecimal(material.getAliquotacofins().getValue().doubleValue()) : (gt.getTipocobrancacofins() != null ? "0,00" :"")) + "';");	
		view.println("var tipocalculocofins = '" + (gt.getTipocalculocofins() != null ? gt.getTipocalculocofins().name() : "") + "';");
		view.println("var tipocalculocofins_desc = '" + (gt.getTipocalculocofins() != null ? gt.getTipocalculocofins() : "") + "';");
		view.println("var aliquotareaiscofins = '" + (gt.getAliquotareaiscofins() != null ? gt.getAliquotareaiscofins() : "") + "';");	

		view.println("var incluirissvalor = " + (gt.getIncluirissvalor() != null && gt.getIncluirissvalor() ? "true" : "false") + ";");
		view.println("var iss = '" + (material.getAliquotaiss() != null ? SinedUtil.descriptionDecimal(material.getAliquotaiss().getValue().doubleValue()) : "") + "';");	
		view.println("var tipotributacaoiss = '" + (gt.getTipotributacaoiss() != null ? gt.getTipotributacaoiss().name() : "") + "';");
		view.println("var incentivofiscal = " + (gt.getIncentivofiscal() != null && gt.getIncentivofiscal() ? "true" : "false") + ";");
		
		view.println("var csll = '" + (material.getAliquotacsll() != null ? SinedUtil.descriptionDecimal(material.getAliquotacsll().getValue().doubleValue()) : "") + "';");
		view.println("var ir = '" + (material.getAliquotair() != null ? SinedUtil.descriptionDecimal(material.getAliquotair().getValue().doubleValue()) : "") + "';");
		view.println("var inss = '" + (material.getAliquotainss() != null ? SinedUtil.descriptionDecimal(material.getAliquotainss().getValue().doubleValue()) : "") + "';");
		
		view.println("var fcp = '" + (material.getAliquotaFCP() != null ? SinedUtil.descriptionDecimal(material.getAliquotaFCP().getValue().doubleValue()) : "") + "';");
		view.println("var fcpst = '" + (material.getAliquotaFCPST() != null ? SinedUtil.descriptionDecimal(material.getAliquotaFCPST().getValue().doubleValue()) : "") + "';");
		
		view.println("var valorFiscalIcms = '" + (gt.getValorFiscalIcms() != null ? gt.getValorFiscalIcms().name() : "<null>") + "';");
		view.println("var valorFiscalIpi = '" + (gt.getValorFiscalIpi() != null ? gt.getValorFiscalIpi().name() : "<null>") + "';");
		
		view.println("var percentualBcicms = '" + (material.getPercentualBcicms() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcicms()) : "") + "';");
		view.println("var percentualBcipi = '" + (material.getPercentualBcipi() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcipi()) : "") + "';");
		view.println("var percentualBcpis = '" + (material.getPercentualBcpis() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcpis()) : "") + "';");
		view.println("var percentualBccofins = '" + (material.getPercentualBccofins() != null ? SinedUtil.descriptionDecimal(material.getPercentualBccofins()) : "") + "';");
		
		view.println("var percentualBccsll = '" + (material.getPercentualBccsll() != null ? SinedUtil.descriptionDecimal(material.getPercentualBccsll()) : "") + "';");
		view.println("var percentualBcir = '" + (material.getPercentualBcir() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcir()) : "") + "';");
		view.println("var percentualBcinss = '" + (material.getPercentualBcinss() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcinss()) : "") + "';");
		view.println("var percentualBcfcp = '" + (material.getPercentualBcfcp() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcfcp()) : "") + "';");
		view.println("var percentualBcfcpst = '" + (material.getPercentualBcfcpst() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcfcpst()) : "") + "';");
		
		view.println("var funcaoBaseCalculoICMS = " + grupotributacaoService.criaFuncaoBaseCalculoICMSJavascript(gt, gt.getFormulabcicms(), "NOTAFISCALPRODUTO"));
		view.println("var funcaoBaseCalculoICMSST = " + grupotributacaoService.criaFuncaoBaseCalculoICMSSTJavascript(gt, gt.getFormulabcst(), "NOTAFISCALPRODUTO"));
		view.println("var funcaoValorICMSST = " + grupotributacaoService.criaFuncaoValorICMSSTJavascript(gt, gt.getFormulavalorst(), "NOTAFISCALPRODUTO"));
		view.println("var funcaoBaseCalculoIPI = " + grupotributacaoService.criaFuncaoBaseCalculoIPIJavascript(gt, gt.getFormulabcipi(), "NOTAFISCALPRODUTO"));
		view.println("var index = '" + (request.getParameter("index") != null ? request.getParameter("index") : "") + "';");
		
	}
	
	public List<Movimentacaoestoque> getListaCorrigida(List<Movimentacaoestoque> listaMovimentacaoestoque) {
		List<Movimentacaoestoque> lista = new ArrayList<Movimentacaoestoque>();
		HashMap<Material, Movimentacaoestoque> mapMaterial = new HashMap<Material, Movimentacaoestoque>();
		
		if(listaMovimentacaoestoque != null && !listaMovimentacaoestoque.isEmpty()){
			for(Movimentacaoestoque item : listaMovimentacaoestoque){
				if(item.getMaterial() != null){
//					if(materialService.isControleMaterialgrademestre(item.getMaterial())){
//						Material materialWithMestre = materialService.loadWithGrade(item.getMaterial());
//						if(materialWithMestre != null && materialWithMestre.getMaterialmestregrade() != null){
//							if(mapMaterial.get(materialWithMestre.getMaterialmestregrade()) == null){
//								Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque(item);
//								movimentacaoestoque.setMaterial(materialWithMestre.getMaterialmestregrade());
//								movimentacaoestoque.setNotafiscalproduto(item.getNotafiscalproduto());
//								movimentacaoestoque.setValor(item.getValor());
//								mapMaterial.put(materialWithMestre.getMaterialmestregrade(),movimentacaoestoque);
//							}else {
//								Movimentacaoestoque itemMestreGradeForBaixa = mapMaterial.get(materialWithMestre.getMaterialmestregrade());
//								if(item.getQtde()!= null){
//									if(itemMestreGradeForBaixa.getQtde() != null){
//										itemMestreGradeForBaixa.setQtde(itemMestreGradeForBaixa.getQtde()+item.getQtde());
//									}else {
//										itemMestreGradeForBaixa.setQtde(item.getQtde());
//									}
//								}
//							}
//						}else {
//							lista.add(item);
//						}
//					}else {
						lista.add(item);
//					}
				}
			}
			
			if(mapMaterial.size() > 0){
				for(Material material : mapMaterial.keySet()){ 
					lista.add(mapMaterial.get(material));
				}
			}
		}
		return lista;
	}
	
	public void preencheValoresPadroesEmpresaISS(Contrato contrato, Notafiscalproduto nf) {
		Empresa empresa = empresaService.loadForEntrada(contrato.getEmpresa());
		
		Codigocnae codigocnae = null;
		Itemlistaservico itemlistaservico = null;
		Naturezaoperacao naturezaoperacao = null;
		Endereco endereco = empresa.getEndereco();
		
		if(contrato.getItemlistaservicoBean() != null){
			itemlistaservico = contrato.getItemlistaservicoBean();
		} else if(empresa.getItemlistaservico() != null){
			itemlistaservico = empresa.getItemlistaservico();
		}
		
		if(contrato.getCodigocnaeBean() != null){
			codigocnae = contrato.getCodigocnaeBean();
		} else if(empresa.getListaEmpresacodigocnae() != null){
			for (Empresacodigocnae ec : empresa.getListaEmpresacodigocnae()) {
				if(codigocnae == null) codigocnae = ec.getCodigocnae();
				if(ec.getPrincipal() != null && ec.getPrincipal()){
					codigocnae = ec.getCodigocnae();
					break;
				}
			}
		}
		
		if(empresa.getNaturezaoperacao() != null) {
			naturezaoperacao = empresa.getNaturezaoperacao();
		} 
		
		if(!org.apache.commons.lang.StringUtils.isBlank(empresa.getTextoinfcontribuinte())){
			if(!org.apache.commons.lang.StringUtils.isBlank(nf.getInfoadicionalcontrib())){
				nf.setInfoadicionalcontrib(nf.getInfoadicionalcontrib() + ". " + empresa.getTextoinfcontribuinte());
			}else {
				nf.setInfoadicionalcontrib(empresa.getTextoinfcontribuinte());
			}
		}
		

		nf.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		nf.setPresencacompradornfe(empresa.getPresencacompradornfe());
		nf.setEmpresa(empresa);
		nf.setCodigocnae(codigocnae);
		for (Notafiscalprodutoitem item : nf.getListaItens()) {
			if(item.getMaterial() != null){
				List<Categoria> listaCategoriaCliente = null;
				if(nf.getCliente() != null){
					listaCategoriaCliente = categoriaService.findByPessoa(nf.getCliente());
				}
				
				Material beanMaterial = materialService.loadForTributacao(item.getMaterial());
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
												nf.getEmpresa(), 
												nf.getNaturezaoperacao(), 
												beanMaterial.getMaterialgrupo(),
												nf.getCliente(),
												true, 
												beanMaterial,
												item.getNcmcompleto(),
												(nf.getEnderecoCliente() != null ? nf.getEnderecoCliente() : null), 
												null,
												listaCategoriaCliente,
												null,
												nf.getOperacaonfe(),
												nf.getModeloDocumentoFiscalEnum(),
												nf.getLocaldestinonfe(),
												nf.getDtEmissao()));
				item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
				item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
				
				if(item.getGrupotributacao() != null){
					if(item.getGrupotributacao().getInfoadicionalcontrib() != null && !"".equals(item.getGrupotributacao().getInfoadicionalcontrib())){
						if(nf.getInfoadicionalcontrib() == null){
							nf.setInfoadicionalcontrib(item.getGrupotributacao().getInfoadicionalcontrib());
						}else if(!nf.getInfoadicionalcontrib().contains(item.getGrupotributacao().getInfoadicionalcontrib())){
							nf.setInfoadicionalcontrib(nf.getInfoadicionalcontrib() + " " + item.getGrupotributacao().getInfoadicionalcontrib());
						}
					}
					if(item.getGrupotributacao().getInfoadicionalfisco() != null && !"".equals(item.getGrupotributacao().getInfoadicionalfisco())){
						if(nf.getInfoadicionalfisco() == null){
							nf.setInfoadicionalfisco(item.getGrupotributacao().getInfoadicionalfisco());
						}else if(!nf.getInfoadicionalfisco().contains(item.getGrupotributacao().getInfoadicionalfisco())){
							nf.setInfoadicionalfisco(nf.getInfoadicionalfisco() + " " + item.getGrupotributacao().getInfoadicionalfisco());
						}
					}
				}
				item.getMaterial().setPercentualimpostoecf(beanMaterial.getPercentualimpostoecf());
				item.setPercentualimpostoecf(this.getPercentualImposto(empresa, item, null));
				
				item.setPercentualImpostoFederal(this.getPercentualImposto(empresa, item, ImpostoEcfEnum.FEDERAL));
				item.setPercentualImpostoEstadual(this.getPercentualImposto(empresa, item, ImpostoEcfEnum.ESTADUAL));
				item.setPercentualImpostoMunicipal(this.getPercentualImposto(empresa, item, ImpostoEcfEnum.MUNICIPAL));
			}
			
			if(item.getGrupotributacao() != null && 
					item.getGrupotributacao().getListaGrupotributacaonaturezaoperacao() != null && 
					item.getGrupotributacao().getListaGrupotributacaonaturezaoperacao().size() == 1){
				naturezaoperacao = item.getGrupotributacao().getListaGrupotributacaonaturezaoperacao().iterator().next().getNaturezaoperacao();
			}
			
			item.setItemlistaservico(itemlistaservico);
			
			if(endereco != null && endereco.getMunicipio() != null) 
				item.setMunicipioiss(endereco.getMunicipio());
			
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
			this.setBaseCalculoItem(item.getMaterial(), item);
			item.setValortotaltributos(this.getValorAproximadoImposto(empresa, naturezaoperacao, item, null));
			
			item.setValorImpostoFederal(this.getValorAproximadoImposto(empresa, naturezaoperacao, item, ImpostoEcfEnum.FEDERAL));
			item.setValorImpostoEstadual(this.getValorAproximadoImposto(empresa, naturezaoperacao, item, ImpostoEcfEnum.ESTADUAL));
			item.setValorImpostoMunicipal(this.getValorAproximadoImposto(empresa, naturezaoperacao, item, ImpostoEcfEnum.MUNICIPAL));
		}
		nf.setNaturezaoperacao(naturezaoperacao);
		
	}
	
	public Notafiscalproduto geraNotaExpedicao(Notafiscalproduto nf, Expedicao expedicao) {
		Naturezaoperacao naturezaoperacao = null;
		if (expedicao.getVenda()!=null 
				&& expedicao.getVenda().getPedidovendatipo()!=null){
			if(expedicao.getVenda().getPedidovendatipo().getNaturezaoperacaoexpedicao()!=null){
				naturezaoperacao = expedicao.getVenda().getPedidovendatipo().getNaturezaoperacaoexpedicao();
			}else if(expedicao.getVenda().getPedidovendatipo().getNaturezaoperacao()!=null){
				naturezaoperacao = expedicao.getVenda().getPedidovendatipo().getNaturezaoperacao();
			}
		}
		if(naturezaoperacao != null){
			nf.setNaturezaoperacao(naturezaoperacao);
		}
		
		if(expedicao.getEmpresa() != null){
			Empresa empresa = empresaService.loadWithEndereco(expedicao.getEmpresa());
			Endereco enderecoEmpresa = empresa.getEndereco();
			nf.setEmpresa(expedicao.getEmpresa());
		
			if(enderecoEmpresa != null){
				nf.setMunicipiogerador(enderecoEmpresa.getMunicipio());
				if(enderecoEmpresa.getMunicipio() != null)
					nf.setUfveiculo(enderecoEmpresa.getMunicipio().getUf());
			}
		}
		nf.setNumero("");
		
		Cliente cliente = clienteService.carregarDadosCliente(expedicao.getCliente());
		Endereco enderecoCliente = null;
		Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
		
		Set<Endereco> listaEndereco = cliente.getListaEndereco();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for (Endereco endereco : listaEndereco) {
				if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
					enderecoCliente = endereco;
					break;
				}
			}
		}
		
		nf.setCliente(cliente);
		nf.setEnderecoCliente(enderecoCliente != null ? enderecoCliente : expedicao.getVenda().getEndereco());
		nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
		nf.setTelefoneClienteTransiente(telefoneCliente);
		nf.setTipooperacaonota(Tipooperacaonota.SAIDA);
		nf.setProjeto(expedicao.getVenda().getProjeto());
		nf.setOperacaonfe(ImpostoUtil.getOperacaonfe(nf.getCliente(), nf.getNaturezaoperacao()));
		
		if(nf.getCliente() != null && nf.getCliente().getCpfOuCnpj() != null){
			nf.getCliente().setCpfcnpj(nf.getCliente().getCpfOuCnpj());
		}
		
		if(expedicao.getVenda().getFrete() != null){
			nf.setResponsavelfrete(expedicao.getVenda().getFrete());
			if(nf.getResponsavelfrete().equals(ResponsavelFrete.TERCEIROS)){
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			nf.setValorfrete(expedicao.getVenda().getValorfrete());
		}else{
			nf.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
		}
		Fornecedor transportador = null;
		if(expedicao.getVenda().getTerceiro() != null && expedicao.getVenda().getTerceiro().getCdpessoa() != null){
			transportador = expedicao.getVenda().getTerceiro();
		}else if(expedicao.getEmpresa() != null && expedicao.getEmpresa().getTransportador() != null &&
				expedicao.getEmpresa().getTransportador().getCdpessoa() != null){
			transportador = expedicao.getEmpresa().getTransportador();
		}
		if(transportador != null){
			transportador = fornecedorService.carregaFornecedor(transportador);
			Endereco enderecotransportador = null;
			if(transportador != null && 
					transportador.getListaEndereco() != null &&
					transportador.getListaEndereco().size() > 0){
				enderecotransportador = transportador.getEndereco();
			}
			
			nf.setTransportador(transportador);
			nf.setEnderecotransportador(enderecotransportador);
			nf.setCadastrartransportador(Boolean.TRUE);
		}
		
		//preenche o local de destino com base no endere�o do cliente e no municipio gerador
		this.preencheLocalDestinoNota(nf);
		
		List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
		
		Double pesoliquidoTotal = 0d;
		Double pesobrutoTotal = 0d;
		Double qtdeTotal = 0d;
		List<Materialclasse> listaMaterialclasse;
			
		for (Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()) {
			Double quantidade = expedicaoitem.getQtdeexpedicao();
			Material material = expedicaoitem.getMaterial();
			Notafiscalprodutoitem item = new Notafiscalprodutoitem();
			
			String nomeComAlturaLarguraComprimento = vendaService.getAlturaLarguraComprimentoConcatenados(expedicaoitem.getVendamaterial(), material);
			if(!"".equals(nomeComAlturaLarguraComprimento)){
				material.setNome(material.getNome() + " " + nomeComAlturaLarguraComprimento);
			}
			item.setNotafiscalproduto(nf);
			item.setMaterial(material);
			item.setNcmcompleto(material.getNcmcompleto());
			listaMaterialclasse = materialService.findClasses(material);
			if(SinedUtil.isListNotEmpty(listaMaterialclasse)){
				item.setMaterialclasse(listaMaterialclasse.get(0));
			}
			item.setQtde(quantidade);
			
			Loteestoque loteestoque = expedicaoitem.getLoteEstoque() != null ? expedicaoitem.getLoteEstoque() : expedicaoitem.getVendamaterial() != null ? expedicaoitem.getVendamaterial().getLoteestoque() : null;
			
			item.setLoteestoque(loteestoque);
			item.setPatrimonioitem(expedicaoitem.getVendamaterial().getPatrimonioitem());
			
			if(material.getCodigoanp() != null){
				item.setCodigoanp(material.getCodigoanp());
				item.setDadoscombustivel(Boolean.TRUE);
			}
			
			if(loteestoque != null && loteestoque.getNumero() != null){
				item.setInfoadicional("Lote: " + loteestoque.getNumero());
			}

			if(material.getEpi() != null && material.getEpi()){
				item.setMaterialclasse(Materialclasse.EPI);
			}else if(material.getPatrimonio() != null && material.getPatrimonio() && (material.getProduto() == null || !material.getProduto())){
				item.setMaterialclasse(Materialclasse.PATRIMONIO);
			}else if(material.getProduto() != null && material.getProduto() && (material.getPatrimonio() == null || !material.getPatrimonio())){
				item.setMaterialclasse(Materialclasse.PRODUTO);
			}
			
			Double fatorconversao = vendaService.verificaUnidademedidaMaterialDiferenteForNota(item, expedicaoitem.getVendamaterial());
			if(item.getUnidademedida() == null){
				item.setUnidademedida(expedicaoitem.getUnidademedida());
			}
			
			quantidade = item.getQtde();
			qtdeTotal += quantidade;
			
			
			Double peso = material.getPeso();
			Double pesobruto = material.getPesobruto();
			if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){
				Material aux_material = materialService.findListaMaterialunidademedida(item.getMaterial());
				Unidademedida aux_unidademedida = item.getUnidademedida();
				
				if(aux_unidademedida != null && peso != null && pesobruto != null){
					Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
					if(!unidadeprincipal){
						Double fracao = this.getFracaoConversaoUnidademedida(aux_material, aux_unidademedida);
						if(fracao != null && fracao > 0){
							peso = peso/fracao;
							pesobruto = pesobruto/fracao;
						}
					}
				}
			}
			
			if(peso != null && peso > 0){
				peso = quantidade * peso;
				if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
					peso = quantidade;
				}
				peso = SinedUtil.roundByParametro(peso);
				
				item.setPesoliquido(peso);
				pesoliquidoTotal += peso;
			}
			if(pesobruto != null && pesobruto > 0){
				pesobruto = quantidade * pesobruto;
				if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
					pesobruto = quantidade;
				}
				pesobruto = SinedUtil.roundByParametro(pesobruto);
				
				item.setPesobruto(pesobruto);
				pesobrutoTotal += pesobruto;
			}
			
			item.setIncluirvalorprodutos(true);
			if(expedicao.getExisteNotaVendaEmitida() == null || !expedicao.getExisteNotaVendaEmitida()){
				item.setValorfrete(expedicao.getVenda().getValorfrete() != null && expedicao.getVenda().getValorfrete().getValue().doubleValue() > 0.0 ? new Money(expedicao.getVenda().getValorfrete().getValue().doubleValue()/expedicao.getListaExpedicaoitem().size()) : new Money(0.0));
			}
			item.setLocalarmazenagem(expedicaoitem.getLocalarmazenagem());
			
			Money valorDesconto = new Money();
			Money valorDescontoItem = expedicaoitem.getVendamaterial().getDesconto() != null ? expedicaoitem.getVendamaterial().getDesconto() :  new Money();
			
			if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
				valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), false);

				if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getQuantidade() != null && expedicaoitem.getVendamaterial().getQuantidade() > 0){
					if(valorDesconto.getValue().doubleValue() > 0){
						valorDesconto = valorDesconto.divide(new Money(expedicaoitem.getVendamaterial().getQuantidade())).multiply(new Money(quantidade));
					}
					valorDescontoItem = valorDescontoItem.divide(new Money(expedicaoitem.getVendamaterial().getQuantidade())).multiply(new Money(quantidade));
				}else {
					valorDesconto = valorDesconto.divide(new Money(quantidade));
					valorDescontoItem = valorDescontoItem.divide(new Money(quantidade));
				}
				valorDesconto = valorDesconto.add(valorDescontoItem);
				item.setValordesconto(valorDesconto);
				if(item.getValorunitario() == null || item.getValorunitario() == 0){
					item.setValorunitario(expedicaoitem.getVendamaterial().getPreco());
				}
			} else {
				valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), true);

				if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getQuantidade() != null){
					valorDescontoItem = valorDescontoItem.divide(new Money(expedicaoitem.getVendamaterial().getQuantidade()));
				}else {
					valorDescontoItem = valorDescontoItem.divide(new Money(quantidade));
				}
				
				valorDesconto = valorDesconto.add(valorDescontoItem);
				
				if(fatorconversao != null && fatorconversao > 0d){
					valorDesconto = valorDesconto.multiply(new Money(fatorconversao));
				}
				
				if(item.getValorunitario() == null || item.getValorunitario() == 0.0){
					item.setValorunitario(expedicaoitem.getVendamaterial().getPreco() - valorDesconto.getValue().doubleValue());
				}else {
					item.setValorunitario(item.getValorunitario() - valorDesconto.getValue().doubleValue());
				}
			}
			
			item.setValorunitario(SinedUtil.round(item.getValorunitario(), 10));
			
			if(item.getMaterial() != null){
				List<Categoria> listaCategoriaCliente = null;
				if(cliente != null){
					listaCategoriaCliente = categoriaService.findByPessoa(cliente);
				}
				
				Material beanMaterial = materialService.loadForTributacao(material);
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
												nf.getEmpresa(), 
												nf.getNaturezaoperacao(), 
												beanMaterial.getMaterialgrupo(),
												cliente,
												true, 
												beanMaterial,
												item.getNcmcompleto(),
												(enderecoCliente != null ? enderecoCliente : expedicao.getVenda().getEndereco()), 
												null,
												listaCategoriaCliente,
												null,
												nf.getOperacaonfe(),
												nf.getModeloDocumentoFiscalEnum(),
												nf.getLocaldestinonfe(),
												nf.getDtEmissao()));
				try {
					System.out.println("*** Inicio GrupoTributacao: ");
					for(Grupotributacao gt2 : listaGrupotributacao) System.out.println("*** GrupoTributacao: " + gt2.getCdgrupotributacao());
					System.out.println("*** Fim GrupoTributacao: ");
				} catch (Exception e) {
					// TODO: handle exception
				}
				item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
				item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
			}
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
			
			if (nf.getNaturezaoperacao()!=null && nf.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
				Endereco enderecoEmpresa = null;
				if (nf.getEmpresa()!=null && nf.getEmpresa().getCdpessoa()!=null){
					enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(nf.getEmpresa());
				}
				Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoEmpresa, nf.getEnderecoCliente());
				List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(nf.getNaturezaoperacao(), cfopescopo);
				if (!listaNaturezaoperacaocfop.isEmpty()){
					item.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
				}
			}
			
			item.setNcmcapitulo(material.getNcmcapitulo());
			item.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
			item.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
			item.setNve(material.getNve() != null ? material.getNve() : "");
			if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
				item.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
			
			this.setBaseCalculoItem(material, item);
			this.calculaMva(item, material);
			
			listaItens.add(item);
		}
		nf.setListaItens(listaItens);
		
		Money valorTotalProdutos = new Money(0.0);
		for (Notafiscalprodutoitem pi : nf.getListaItens()) {
			valorTotalProdutos = valorTotalProdutos.add(pi.getValorbruto());
		}
		
		nf.setValorprodutos(valorTotalProdutos);
		
		if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP))){
			if(expedicao.getQuantidadevolumes() != null){
				if(nf.getQtdevolume() != null){
					nf.setQtdevolume(nf.getQtdevolume() + expedicao.getQuantidadevolumes());
				}else {
					nf.setQtdevolume(expedicao.getQuantidadevolumes());
				}
			}
		}else {
			if(qtdeTotal != null && qtdeTotal > 0){
				nf.setQtdevolume(qtdeTotal.intValue());
			}			
		}
		if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
			nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
		}
		if(pesobrutoTotal != null && pesobrutoTotal > 0){
			nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
		}
		
		if (expedicao.getVenda().getValorfrete() != null){
			valorTotalProdutos = valorTotalProdutos.add(expedicao.getVenda().getValorfrete());
		}
		
		if(cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota() && expedicao.getVenda().getDesconto() != null){
			nf.setValordesconto(expedicao.getVenda().getDesconto());
			valorTotalProdutos = valorTotalProdutos.subtract(expedicao.getVenda().getDesconto());
		}
		
		nf.setValor(valorTotalProdutos);
		nf.setNotaStatus(NotaStatus.EMITIDA);	
		nf.setCdvendaassociada(expedicao.getVenda().getCdvenda());
		nf.setWhereInCdexpedicaoassociada(expedicao.getCdexpedicao().toString());
		
		List<Documentoorigem> listaDocumentoOrigem = documentoorigemService.buscarListaParaDuplicatas(expedicao.getVenda());
		List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
		nf.setCadastrarcobranca(Boolean.TRUE);
		if(listaDocumentoOrigem != null && listaDocumentoOrigem.size() > 0) {
			for (Documentoorigem docOrigem : listaDocumentoOrigem) {
				Notafiscalprodutoduplicata nfDuplicata = new Notafiscalprodutoduplicata();
				nfDuplicata.setDtvencimento(docOrigem.getDocumento().getDtvencimento());
				nfDuplicata.setValor(docOrigem.getDocumento().getValor());
				nfDuplicata.setDocumentotipo(docOrigem.getDocumento().getDocumentotipo());
				listaDuplicata.add(nfDuplicata);
			}
		}
		nf.setListaDuplicata(listaDuplicata);
		nf.setValorfrete(expedicao.getVenda().getValorfrete());
		
		if(expedicao.getVenda().getPrazopagamento() != null && expedicao.getVenda().getPrazopagamento().getCdprazopagamento() != null){
			nf.setPrazopagamentofatura(expedicao.getVenda().getPrazopagamento());
			if (expedicao.getVenda().getPrazopagamento().getAvista() != null && expedicao.getVenda().getPrazopagamento().getAvista()){
				nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
			}else {
				nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
			}
		}
		
		if(expedicao.getVenda() != null && expedicao.getVenda().getPedidovendatipo() != null && Boolean.TRUE.equals(expedicao.getVenda().getPedidovendatipo().getBonificacao())){
			nf.setCadastrarcobranca(Boolean.FALSE);
		}
		
		this.adicionarInfNota(nf, expedicao.getVenda(), expedicao.getEmpresa());
		this.setTotalNota(nf);
		
		return nf;
	}
	
	public void gerarFaturamentoExpedicao(WebRequestContext request, List<Expedicao> listaExpedicao, Boolean agrupamento) {
		List<Notafiscalproduto> notas = new ArrayList<Notafiscalproduto>();
		for (Expedicao expedicao : listaExpedicao) {
			if(notavendaService.existeNotaEmitida(expedicao, NotaTipo.NOTA_FISCAL_PRODUTO)){
				continue;
			}
			Venda venda_aux = vendaService.loadWithPedidovenda(expedicao.getVenda());
			Naturezaoperacao naturezaoperacao = null;
			if (venda_aux != null && venda_aux.getPedidovendatipo() != null) {
				if (venda_aux.getPedidovendatipo().getNaturezaoperacao() != null) {
					naturezaoperacao = venda_aux.getPedidovendatipo().getNaturezaoperacao();
				}
			}

			if (naturezaoperacao == null) {
				naturezaoperacao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO);
			}
			
			Notafiscalproduto nf = null;
			if(agrupamento){
				for (Notafiscalproduto notafiscalproduto : notas) {
					//Condi��o para agrupamento
					boolean condicaoNaturezaoperacao = false;
					
					if (naturezaoperacao == null && notafiscalproduto.getNaturezaoperacao() == null) {
						condicaoNaturezaoperacao = true;
					} else if (naturezaoperacao != null
							&& notafiscalproduto.getNaturezaoperacao() != null
							&& naturezaoperacao.getCdnaturezaoperacao().equals(notafiscalproduto.getNaturezaoperacao().getCdnaturezaoperacao())) {
						condicaoNaturezaoperacao = true;
					}
					boolean cliente = notafiscalproduto.getCliente().equals(expedicao.getCliente()) || clienteService.isMatrizCnpjIgual(notafiscalproduto.getCliente(), expedicao.getCliente());
					ResponsavelFrete responsavelfretevenda = expedicao.getVenda().getFrete() == null ? ResponsavelFrete.SEM_FRETE : expedicao.getVenda().getFrete();
					boolean frete = notafiscalproduto.getResponsavelfrete().equals(responsavelfretevenda);
					boolean terceiros = (expedicao.getVenda().getTerceiro() == null && notafiscalproduto.getTransportador() == null) || (expedicao.getVenda().getTerceiro() != null && expedicao.getVenda().getTerceiro().equals(notafiscalproduto.getTransportador()));
					
					if(cliente && frete && terceiros && condicaoNaturezaoperacao){
						nf = notafiscalproduto;
//						nf.getListaNotaHistorico().get(0).setObservacao(nf.getListaNotaHistorico().get(0).getObservacao().replace(".", ", ")+"<a href=\"javascript:visualizaExpedicao("+expedicao.getCdexpedicao()+");\">"+expedicao.getCdexpedicao()+"</a>.");
						break;
					}
				}
			}
			if(nf == null){
				nf = new Notafiscalproduto();
				nf.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
//				NotaHistorico notaHistorico = new NotaHistorico(nf, "Visualizar expedi��o: <a href=\"javascript:visualizaExpedicao("+expedicao.getCdexpedicao()+");\">"+expedicao.getCdexpedicao()+"</a>.", NotaStatus.EMITIDA, ((Usuario)Neo.getUser()).getCdpessoa(), new Timestamp(System.currentTimeMillis()));
//				List<NotaHistorico> lista = new ArrayList<NotaHistorico>();
//				lista.add(notaHistorico);
//				nf.setListaNotaHistorico(lista);
				
				nf.setListaItens(new ArrayList<Notafiscalprodutoitem>());
				notas.add(nf);
			}
			if(!nf.getListaExpedicao().contains(expedicao))
				nf.getListaExpedicao().add(expedicao);
			
			if(nf.getListaExpedicao() != null && !nf.getListaExpedicao().isEmpty()){
				for(Expedicao e : nf.getListaExpedicao()){
					if(e.equals(expedicao)){
						if(e.getListaVenda() == null)
							e.setListaVenda(new ArrayList<Venda>());
						if(!e.getListaVenda().contains(expedicao.getVenda())){
							e.getListaVenda().add(expedicao.getVenda());
						}
					}
				}
			}
			
			Empresa empresa = expedicao.getEmpresa();
			if(empresa == null) empresa = empresaService.loadPrincipal();
			
			empresa = empresaService.loadWithEndereco(empresa);
			Endereco enderecoEmpresa = empresa.getEndereco();
			
			if(enderecoEmpresa != null){
				nf.setMunicipiogerador(enderecoEmpresa.getMunicipio());
			}
			
			if(empresa != null){
				Empresa empresa_aux = empresaService.load(empresa, "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf");
				
				if(empresa_aux.getNaopreencherdtsaida() == null || !empresa_aux.getNaopreencherdtsaida()){
					nf.setDatahorasaidaentrada(SinedDateUtils.currentTimestamp());
				}
				
				nf.setMarcavolume(empresa_aux.getMarcanf());
				nf.setEspvolume(empresa_aux.getEspecienf());
			}
			nf.setEmpresa(empresa);
			nf.setNaturezaoperacao(naturezaoperacao);
			if(naturezaoperacao != null && naturezaoperacao.getOperacaonfe() != null){
				nf.setOperacaonfe(naturezaoperacao.getOperacaonfe());
			}
			if(naturezaoperacao != null && naturezaoperacao.getFinalidadenfe() != null){
				nf.setFinalidadenfe(naturezaoperacao.getFinalidadenfe());
			} else {
				nf.setFinalidadenfe(Finalidadenfe.NORMAL);
			}
			nf.setPresencacompradornfe(empresa != null ? empresa.getPresencacompradornfe() : null);
			nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
			
			if(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null)
				nf.setUfveiculo(enderecoEmpresa.getMunicipio().getUf());
			
			Cliente cliente = clienteService.carregarDadosCliente(expedicao.getCliente());
			Endereco enderecoCliente = null;
			Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
			
			Set<Endereco> listaEndereco = cliente.getListaEndereco();
			if(listaEndereco != null && listaEndereco.size() > 0){
				for (Endereco end : listaEndereco) {
					if(end.getEnderecotipo() != null && end.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						enderecoCliente = end;
						break;
					}
				}
			}
			
			if(enderecoCliente == null && expedicao.getVenda().getEndereco() != null){
				enderecoCliente = enderecoService.carregaEnderecoComUfPais(expedicao.getVenda().getEndereco());
			}
			nf.setCliente(cliente);
			nf.setEnderecoCliente(enderecoCliente);
			nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
			nf.setTipooperacaonota(Tipooperacaonota.SAIDA);
			
			if(expedicao.getVenda().getFrete() != null){
				nf.setResponsavelfrete(expedicao.getVenda().getFrete());
				if(nf.getResponsavelfrete().equals(ResponsavelFrete.TERCEIROS)){
					nf.setCadastrartransportador(Boolean.TRUE);
				}
				if(nf.getValorfrete() == null)
					nf.setValorfrete(new Money(0.0));
				if(expedicao.getVenda().getValorfrete() == null)
					expedicao.getVenda().setValorfrete(new Money(0.0));
				nf.setValorfrete(nf.getValorfrete().add(expedicao.getVenda().getValorfrete()));
			}else{
				nf.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
			}
			
			if(expedicao.getVenda().getTerceiro() != null){
				nf.setTransportador(expedicao.getVenda().getTerceiro());
			}else if(expedicao.getEmpresa() != null && expedicao.getEmpresa().getTransportador() != null){
				nf.setTransportador(expedicao.getEmpresa().getTransportador());
			}
			
			Double pesoliquidoTotal = 0d;
			Double pesobrutoTotal = 0d;
			Double qtdeTotal = 0d;
			
			List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
			Cfop cfop = null;
			List<Materialclasse> listaMaterialclasse;
			for (Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()) {
				if(expedicaoitem.getMaterial() != null && expedicaoitem.getMaterial().getServico() != null && 
						expedicaoitem.getMaterial().getServico()){
					continue;
				}
				
				Material material = expedicaoitem.getMaterial();
				Notafiscalprodutoitem notafiscalprodutoitem = null;
				for (Notafiscalprodutoitem notafiscalprodutoitemAux : nf.getListaItens()) {
					if(notafiscalprodutoitemAux.getMaterial().equals(expedicaoitem.getMaterial()) && 
							notafiscalprodutoitemAux.getUnidademedida() != null &&
							notafiscalprodutoitemAux.getUnidademedida().equals(expedicaoitem.getUnidademedida()) && 
							notafiscalprodutoitemAux.getValorunitario().doubleValue() == expedicaoitem.getVendamaterial().getPreco().doubleValue()){
						notafiscalprodutoitem = notafiscalprodutoitemAux;
						break;
					}
				}
				
				Double quantidade = expedicaoitem.getQtdeexpedicao().doubleValue();
				if(notafiscalprodutoitem == null){
					Notafiscalprodutoitem item = new Notafiscalprodutoitem();
					
					item.setNotafiscalproduto(nf);
					item.setMaterial(material);
					listaMaterialclasse = materialService.findClasses(material);
					if(SinedUtil.isListNotEmpty(listaMaterialclasse)){
						item.setMaterialclasse(listaMaterialclasse.get(0));
					}
					
					if(material.getCodigoanp() != null){
						item.setCodigoanp(material.getCodigoanp());
						item.setDadoscombustivel(Boolean.TRUE);
					}
					
					if(material.getEpi() != null && material.getEpi()){
						item.setMaterialclasse(Materialclasse.EPI);
					}else if(material.getPatrimonio() != null && material.getPatrimonio() && (material.getProduto() == null || !material.getProduto())){
						item.setMaterialclasse(Materialclasse.PATRIMONIO);
					}else if(material.getProduto() != null && material.getProduto() && (material.getPatrimonio() == null || !material.getPatrimonio())){
						item.setMaterialclasse(Materialclasse.PRODUTO);
					}
					item.setQtde(quantidade);
					item.setValorunitario(expedicaoitem.getVendamaterial().getPreco());
					item.setIncluirvalorprodutos(true);
					item.setPatrimonioitem(expedicaoitem.getVendamaterial().getPatrimonioitem());
					item.setValorfrete(expedicao.getVenda().getValorfrete() != null && expedicao.getVenda().getValorfrete().getValue().doubleValue() > 0.0 ? new Money(expedicao.getVenda().getValorfrete().getValue().doubleValue()/expedicao.getListaExpedicaoitem().size()) : new Money(0.0));
					item.setUnidademedida(expedicaoitem.getUnidademedida());
					
					qtdeTotal += quantidade;
					
					Double peso = material.getPeso();
					Double pesobruto = material.getPesobruto();
					if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){
						Material aux_material = materialService.findListaMaterialunidademedida(item.getMaterial());
						Unidademedida aux_unidademedida = item.getUnidademedida();
						
						if(aux_unidademedida != null && peso != null && pesobruto != null){
							Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
							if(!unidadeprincipal){
								Double fracao = this.getFracaoConversaoUnidademedida(aux_material, aux_unidademedida);
								if(fracao != null && fracao > 0){
									peso = peso/fracao;
									pesobruto = pesobruto/fracao;
								}
							}
						}
					}
					
					if(peso != null && peso > 0){
						peso = quantidade * peso;
						if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							peso = quantidade;
						}
						peso = SinedUtil.roundByParametro(peso);
						
						item.setPesoliquido(peso);
						pesoliquidoTotal += peso;
					}
					if(pesobruto != null && pesobruto > 0){
						pesobruto = quantidade * pesobruto;
						if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
							pesobruto = quantidade;
						}
						pesobruto = SinedUtil.roundByParametro(pesobruto);
						
						item.setPesobruto(pesobruto);
						pesobrutoTotal += pesobruto;
					}
					
					item.setLoteestoque(expedicaoitem.getVendamaterial().getLoteestoque());
					if(expedicaoitem.getVendamaterial().getLoteestoque() != null && expedicaoitem.getVendamaterial().getLoteestoque().getNumero() != null){
						item.setInfoadicional("Lote: " + expedicaoitem.getVendamaterial().getLoteestoque().getNumero());
					}
					
					if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
						List<Categoria> listaCategoriaCliente = null;
						if(cliente != null){
							listaCategoriaCliente = categoriaService.findByPessoa(cliente);
						}
						
						Material beanMaterial = materialService.loadForTributacao(item.getMaterial());
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
														empresa, 
														nf.getNaturezaoperacao(), 
														beanMaterial.getMaterialgrupo(),
														cliente,
														true, 
														beanMaterial, 
														item.getNcmcompleto(),
														enderecoCliente, 
														null,
														listaCategoriaCliente,
														null,
														nf.getOperacaonfe(),
														nf.getModeloDocumentoFiscalEnum(),
														nf.getLocaldestinonfe(),
														nf.getDtEmissao()));
						item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					}
					item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
					
					if (nf.getNaturezaoperacao()!=null && nf.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
						Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoEmpresa, nf.getEnderecoCliente());
						List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(nf.getNaturezaoperacao(), cfopescopo);
						if (!listaNaturezaoperacaocfop.isEmpty()){
							item.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
						}
					}
					
					Money valorDesconto = new Money();
					Money valorDescontoItem = expedicaoitem.getVendamaterial().getDesconto() != null ? expedicaoitem.getVendamaterial().getDesconto() :  new Money();
					
					if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
						valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), false);
						valorDesconto = valorDesconto.add(valorDescontoItem);
						item.setValordesconto(valorDesconto);
						if(item.getValorunitario() == null || item.getValorunitario() == 0){
							item.setValorunitario(expedicaoitem.getVendamaterial().getPreco());
						}
					} else {
						valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), true);

						Double multiplicador = expedicaoitem.getVendamaterial().getMultiplicador();
						if(multiplicador == null) multiplicador = 1.0;
						
						if(item.getQtde() != null && item.getQtde() != 0){
							valorDescontoItem = valorDescontoItem.divide(new Money(item.getQtde()*multiplicador));
						}else {
							valorDescontoItem = valorDescontoItem.divide(new Money(expedicaoitem.getVendamaterial().getQuantidade()*multiplicador));
						}
						valorDesconto = valorDesconto.add(valorDescontoItem);
						
						if(item.getValorunitario() == null || item.getValorunitario().doubleValue() == 0.0){
							item.setValorunitario(expedicaoitem.getVendamaterial().getPreco() - valorDesconto.getValue().doubleValue());
						}else {
							item.setValorunitario(item.getValorunitario() - valorDesconto.getValue().doubleValue());
						}
					}
					
					item.setValorunitario(SinedUtil.round(item.getValorunitario(), 10));
					
					if(quantidade != null && expedicaoitem.getVendamaterial().getPreco() != null)
						item.setValorbruto(new Money(quantidade*expedicaoitem.getVendamaterial().getPreco().doubleValue()));
					
					item.setNcmcapitulo(material.getNcmcapitulo());
					item.setNve(material.getNve());
					item.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
					item.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
					
					if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
						item.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
					
					setBaseCalculoItem(material, item);
					
					if(cfop == null && item.getCfop() != null && item.getCfop().getCdcfop() != null){
						cfop = item.getCfop();
					}
					
					listaItens.add(item);
				}else{
					notafiscalprodutoitem.setQtde(notafiscalprodutoitem.getQtde()+quantidade);
					notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
					notafiscalprodutoitem.setValorfrete(notafiscalprodutoitem.getValorfrete().add(expedicao.getVenda().getValorfrete() != null && expedicao.getVenda().getValorfrete().getValue().doubleValue() > 0.0 ? new Money(expedicao.getVenda().getValorfrete().getValue().doubleValue()/expedicao.getListaExpedicaoitem().size()) : new Money(0.0)));
					
					if(notafiscalprodutoitem.getMaterial().getPeso() != null && notafiscalprodutoitem.getMaterial().getPeso() > 0){
						Double pesoliquido = quantidade * notafiscalprodutoitem.getMaterial().getPeso();
						
						notafiscalprodutoitem.setPesoliquido(notafiscalprodutoitem.getPesoliquido() + pesoliquido);
						pesoliquidoTotal += pesoliquido;
					}
					if(notafiscalprodutoitem.getMaterial().getPesobruto() != null && notafiscalprodutoitem.getMaterial().getPesobruto() > 0){
						Double pesobruto = quantidade * notafiscalprodutoitem.getMaterial().getPesobruto();
						
						notafiscalprodutoitem.setPesobruto(notafiscalprodutoitem.getPesobruto() + pesobruto);
						pesobrutoTotal += pesobruto;
					}
					
					Money valorDesconto = new Money();
					Money valorDescontoItem = expedicaoitem.getVendamaterial().getDesconto() != null ? expedicaoitem.getVendamaterial().getDesconto() :  new Money();
					
					if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
						valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), false);
						valorDesconto = valorDesconto.add(valorDescontoItem);
						if(notafiscalprodutoitem.getValordesconto() != null){
							notafiscalprodutoitem.setValordesconto(notafiscalprodutoitem.getValordesconto().add(valorDesconto));
						}else {
							notafiscalprodutoitem.setValordesconto(valorDesconto);
						}
					} else {
						valorDesconto = getValorDescontoVenda(expedicao.getVenda().getListavendamaterial(), expedicaoitem.getVendamaterial(), expedicao.getVenda().getDesconto(), expedicao.getVenda().getValorusadovalecompra(), true);

						Double multiplicador = expedicaoitem.getVendamaterial().getMultiplicador();
						if(multiplicador == null) multiplicador = 1.0;
						
						if(notafiscalprodutoitem.getQtde() != null && notafiscalprodutoitem.getQtde() != 0){
							valorDescontoItem = valorDescontoItem.divide(new Money(notafiscalprodutoitem.getQtde()*multiplicador));
						}else {
							valorDescontoItem = valorDescontoItem.divide(new Money(expedicaoitem.getVendamaterial().getQuantidade()*multiplicador));
						}
						valorDesconto = valorDesconto.add(valorDescontoItem);
						
						if(notafiscalprodutoitem.getValorunitario() == null || notafiscalprodutoitem.getValorunitario().doubleValue() == 0.0){
							notafiscalprodutoitem.setValorunitario(expedicaoitem.getVendamaterial().getPreco() - valorDesconto.getValue().doubleValue());
							notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
						}else {
							notafiscalprodutoitem.setValorunitario(notafiscalprodutoitem.getValorunitario() - valorDesconto.getValue().doubleValue());
							notafiscalprodutoitem.setValorbruto(new Money(notafiscalprodutoitem.getQtde().doubleValue()*notafiscalprodutoitem.getValorunitario()));
						}
					}
					qtdeTotal += quantidade;
					
					if(notafiscalprodutoitem.getValorunitario() != null && notafiscalprodutoitem.getQtde() != null){
						double basecalculo = notafiscalprodutoitem.getValorunitario() * notafiscalprodutoitem.getQtde();
						
						if(notafiscalprodutoitem.getIcms() != null){
							notafiscalprodutoitem.setValorbcicms(new Money(basecalculo));
							notafiscalprodutoitem.setValoricms(new Money((basecalculo * notafiscalprodutoitem.getIcms())/100d));
						}
						
						if(notafiscalprodutoitem.getIss() != null){
							notafiscalprodutoitem.setValorbciss(new Money(basecalculo));
							notafiscalprodutoitem.setValoriss(new Money((basecalculo * notafiscalprodutoitem.getIss())/100d));
						}
						
						if(notafiscalprodutoitem.getIpi() != null){
							notafiscalprodutoitem.setValorbcipi(new Money(basecalculo));
							notafiscalprodutoitem.setValoripi(new Money((basecalculo * notafiscalprodutoitem.getIpi())/100d));
						} else if(notafiscalprodutoitem.getAliquotareaisipi() != null){
							notafiscalprodutoitem.setQtdevendidaipi(notafiscalprodutoitem.getQtde());
							notafiscalprodutoitem.setValoripi(new Money(notafiscalprodutoitem.getAliquotareaisipi().getValue().doubleValue() * notafiscalprodutoitem.getQtde()));
						}
						
						if(notafiscalprodutoitem.getPis() != null){
							notafiscalprodutoitem.setValorbcpis(new Money(basecalculo));
							notafiscalprodutoitem.setValorpis(new Money((basecalculo * notafiscalprodutoitem.getPis())/100d));
						} else if(notafiscalprodutoitem.getAliquotareaispis() != null){
							notafiscalprodutoitem.setQtdevendidapis(notafiscalprodutoitem.getQtde());
							notafiscalprodutoitem.setValorpis(new Money(notafiscalprodutoitem.getAliquotareaispis().getValue().doubleValue() * notafiscalprodutoitem.getQtde()));
						}
						
						if(notafiscalprodutoitem.getCofins() != null){
							notafiscalprodutoitem.setValorbccofins(new Money(basecalculo));
							notafiscalprodutoitem.setValorcofins(new Money((basecalculo * notafiscalprodutoitem.getCofins())/100d));
						} else if(notafiscalprodutoitem.getAliquotareaiscofins() != null){
							notafiscalprodutoitem.setQtdevendidacofins(notafiscalprodutoitem.getQtde());
							notafiscalprodutoitem.setValorcofins(new Money(notafiscalprodutoitem.getAliquotareaiscofins().getValue().doubleValue() * notafiscalprodutoitem.getQtde()));
						}
						
						double valoriss = notafiscalprodutoitem.getIncluirissvalor() != null && notafiscalprodutoitem.getIncluirissvalor() && notafiscalprodutoitem.getValoriss() != null ? notafiscalprodutoitem.getValoriss().getValue().doubleValue() : 0d;
						double valoricms = notafiscalprodutoitem.getIncluiricmsvalor() != null && notafiscalprodutoitem.getIncluiricmsvalor() && notafiscalprodutoitem.getValoricms() != null ? notafiscalprodutoitem.getValoricms().getValue().doubleValue() : 0d;
						double valoripi = notafiscalprodutoitem.getIncluiripivalor() != null && notafiscalprodutoitem.getIncluiripivalor() && notafiscalprodutoitem.getValoripi() != null ? notafiscalprodutoitem.getValoripi().getValue().doubleValue() : 0d;
						double valorcofins = notafiscalprodutoitem.getIncluircofinsvalor() != null && notafiscalprodutoitem.getIncluircofinsvalor() && notafiscalprodutoitem.getValorcofins() != null ? notafiscalprodutoitem.getValorcofins().getValue().doubleValue() : 0d;
						double valorpis = notafiscalprodutoitem.getIncluirpisvalor() != null && notafiscalprodutoitem.getIncluirpisvalor() && notafiscalprodutoitem.getValorpis() != null ? notafiscalprodutoitem.getValorpis().getValue().doubleValue() : 0d;
						
						double valorbruto = basecalculo + valoriss + valoricms + valoripi + valorcofins + valorpis;
						notafiscalprodutoitem.setValorbruto(new Money(valorbruto));
					}
				}
			}
			nf.getListaItens().addAll(listaItens);

			if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
				nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			if(pesobrutoTotal != null && pesobrutoTotal > 0){
				nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
				nf.setCadastrartransportador(Boolean.TRUE);
			}
			
			if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP))){
				nf.setCadastrartransportador(Boolean.TRUE);
				if(expedicao.getQuantidadevolumes() != null){
					if(nf.getQtdevolume() != null){
						nf.setQtdevolume(nf.getQtdevolume() + expedicao.getQuantidadevolumes());
					}else {
						nf.setQtdevolume(expedicao.getQuantidadevolumes());
					}
				}	
			}else {
				if(qtdeTotal != null && qtdeTotal > 0){
					nf.setQtdevolume(qtdeTotal.intValue());
					nf.setCadastrartransportador(Boolean.TRUE);
				}
			}
			
			Money totalIi = new Money();
			Money totalIpi = new Money();
			Money totalIcmsst = new Money();
			Money totalSeguro = new Money();
			Money totalFrete = new Money();
			Money totalDesconto = new Money();
			Money totalOutrasdespesas = new Money();
			Money totalProdutos = new Money(0.0);
			for (Notafiscalprodutoitem pi : nf.getListaItens()) {
				totalProdutos = totalProdutos.add(pi.getValorbruto());
				if(pi.getOutrasdespesas() != null) totalOutrasdespesas = totalOutrasdespesas.add(pi.getOutrasdespesas());
				if(pi.getValordesconto() != null) totalDesconto = totalDesconto.add(pi.getValordesconto());
				if(pi.getValorfrete() != null) totalFrete = totalFrete.add(pi.getValorfrete());
				if(pi.getValorseguro() != null) totalSeguro = totalSeguro.add(pi.getValorseguro());
				if(pi.getValoricmsst() != null) totalIcmsst = totalIcmsst.add(pi.getValoricmsst());
				if(pi.getValoripi() != null) totalIpi = totalIpi.add(pi.getValoripi());
				if(pi.getValorii() != null) totalIi = totalIi.add(pi.getValorii());
			}
			nf.setOutrasdespesas(totalOutrasdespesas);
			nf.setValordesconto(totalDesconto);
			nf.setValorfrete(totalFrete);
			nf.setValorseguro(totalSeguro);
			nf.setValorprodutos(totalProdutos);
			nf.setNotaStatus(NotaStatus.EMITIDA);
			
			Money totalNota = totalProdutos
						.subtract(totalDesconto)
						.add(totalFrete)
						.add(totalSeguro)
						.add(totalOutrasdespesas)
						.add(totalIcmsst)
						.add(totalIpi)
						.add(totalIi);	
			
			nf.setValor(totalNota);
			

			if(!agrupamento){
				if(expedicao.getVenda().getPrazopagamento() != null && expedicao.getVenda().getPrazopagamento().getCdprazopagamento() != null){
					nf.setPrazopagamentofatura(expedicao.getVenda().getPrazopagamento());
					boolean parcelaUnicaMenorIgual7Dias = false;
					List<Prazopagamentoitem> listaPrazopagamentoitem = prazopagamentoitemService.findByPrazo(expedicao.getVenda().getPrazopagamento());
					if (listaPrazopagamentoitem.size()==1
							&& ((listaPrazopagamentoitem.get(0).getDias()==null || listaPrazopagamentoitem.get(0).getDias()<=7)
									&& (listaPrazopagamentoitem.get(0).getMeses()==null || listaPrazopagamentoitem.get(0).getMeses()<1))){
						parcelaUnicaMenorIgual7Dias = true;
					}
					if (listaPrazopagamentoitem.isEmpty() || parcelaUnicaMenorIgual7Dias==true){
						nf.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
					}else {
						nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
					}
				}
			}else {
				nf.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
			}
			
			this.adicionarInfNota(nf, expedicao.getVenda(), expedicao.getEmpresa());
		}
		
		for (Notafiscalproduto notafiscalproduto : notas){
			notafiscalproduto.setOperacaonfe(ImpostoUtil.getOperacaonfe(notafiscalproduto.getCliente(), notafiscalproduto.getNaturezaoperacao()));
			if(notafiscalproduto.getListaItens() != null && !notafiscalproduto.getListaItens().isEmpty()){
				if(notafiscalproduto.getInfoadicionalcontrib() != null && notafiscalproduto.getInfoadicionalcontrib().length() > 5000){
					notafiscalproduto.setInfoadicionalcontrib(notafiscalproduto.getInfoadicionalcontrib().substring(0, 5000));
				}
				if(notafiscalproduto.getInfoadicionalfisco() != null && notafiscalproduto.getInfoadicionalfisco().length() > 2000){
					notafiscalproduto.setInfoadicionalfisco(notafiscalproduto.getInfoadicionalfisco().substring(0, 2000));
				}
				this.setTotalNota(notafiscalproduto);
				saveOrUpdate(notafiscalproduto);
				updateNumeroNFProduto(notafiscalproduto);
				
				List<Expedicao> listaExpedicaoNota = notafiscalproduto.getListaExpedicao();
				if(listaExpedicaoNota != null && listaExpedicaoNota.size() > 0){
					StringBuilder obsNota = new StringBuilder();
					for (Expedicao expedicao : listaExpedicaoNota) {
						StringBuilder obs = new StringBuilder();
						if(expedicao.getListaVenda() != null && !expedicao.getListaVenda().isEmpty()){
							boolean recalcularValoresParcelas = expedicao.getListaVenda().size() == 1;
							for(Venda venda : expedicao.getListaVenda()){
								List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(expedicao.getVenda());
								for (Documentoorigem documentoorigem : listadocumentoorigem) {
									Documento documento = documentoorigem.getDocumento();
									documentoService.updateNumerosNFDocumentoFaturamento(documento, notafiscalproduto.getNumero(), NotaTipo.NOTA_FISCAL_PRODUTO);
									
									documento = documentoService.carregaDocumento(documento);
									documento.setAcaohistorico(Documentoacao.ALTERADA);
									Documentohistorico documentohistorico = new Documentohistorico(documento);
									String obsVenda = "Vinculado a nota <a href=\"javascript:visualizarNotaFiscalProduto(" + notafiscalproduto.getCdNota() + ")\">" + notafiscalproduto.getCdNota() + "</a>. ";
									documentohistorico.setObservacao(obsVenda);
									documentohistoricoService.saveOrUpdate(documentohistorico);
								}
							
								NotaVenda notavenda = new NotaVenda(notafiscalproduto, expedicao.getVenda());
								notavenda.setExpedicao(expedicao);
								notavendaService.saveOrUpdate(notavenda);
						
								Vendahistorico vendahistorico = new Vendahistorico();
								vendahistorico.setVenda(expedicao.getVenda());
								String numero = notafiscalproduto.getNumero() == null || notafiscalproduto.getNumero().equals("") ? "sem n�mero" : notafiscalproduto.getNumero();
								vendahistorico.setAcao("Gera��o da nota " + numero);
								vendahistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNota("+notafiscalproduto.getCdNota()+");\">"+numero+"</a>.");
								vendahistoricoService.saveOrUpdate(vendahistorico);
								obs.append("<a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>, ");
								
								if (!Vendasituacao.FATURADA.equals(venda.getVendasituacao())){
									vendaService.updateSituacaoVenda(venda.getCdvenda().toString(), Vendasituacao.FATURADA);			
								}
								
								GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
								if(venda.getPedidovendatipo() != null){
									geracaocontareceberEnum = venda.getPedidovendatipo().getGeracaocontareceberEnum();
								}
								if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum)){
									Date dtsaida = notafiscalproduto.getDatahorasaidaentrada() != null ? new Date(notafiscalproduto.getDatahorasaidaentrada().getTime()) : null;
									if(dtsaida == null) {
										Notafiscalproduto notaComDtSaida = this.load(notafiscalproduto, "cdNota, datahorasaidaentrada");
										if(notaComDtSaida != null && notaComDtSaida.getDatahorasaidaentrada() != null) {
											dtsaida = new Date(notaComDtSaida.getDatahorasaidaentrada().getTime());
										}
									}
									
									vendaService.gerarContareceberAposEmissaonota(request, venda, notafiscalproduto.getDtEmissao(), dtsaida, notafiscalproduto.getNumero(), notafiscalproduto.getValor(), notafiscalproduto.getListaDuplicata(), null, null, null, recalcularValoresParcelas, null , notafiscalproduto.getPrazopagamentofatura(), false);
								}
							}
						}
						  
						Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
						expedicaohistorico.setExpedicao(expedicao);
						expedicaohistorico.setExpedicaoacao(Expedicaoacao.FATURADA);
						expedicaohistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNota("+notafiscalproduto.getCdNota()+");\">"+(notafiscalproduto.getNumero() != null ? notafiscalproduto.getNumero() : notafiscalproduto.getCdNota() )+"</a>.");
						expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
				
						obsNota.append((!obs.toString().equals("") ? "Visualizar venda: " + obs.toString() : "")); 
						obsNota.append("Expedi��es: " + expedicaoService.makeObservacaoExpedicao(expedicao.getCdexpedicao().toString()));
						obsNota.append("<br>");
						
					}
					
					if(obsNota.length() > 0){
						NotaHistorico notaHistorico = new NotaHistorico(notafiscalproduto, obsNota.substring(0, obsNota.length()-1), NotaStatus.EMITIDA);
						notaHistoricoService.saveOrUpdate(notaHistorico);
					}
				}
			}
		}
		
	}
	
	public void verificarInfAproveitamentocredito(Notafiscalproduto notafiscalproduto){
		if(notafiscalproduto != null){
			String infadicionais = getMsgitemAproveitamentocredito(notafiscalproduto.getListaItens());
			if(infadicionais != null && !"".equals(infadicionais)){
				if(notafiscalproduto.getInfoadicionalcontrib() == null || "".equals(notafiscalproduto.getInfoadicionalcontrib())){
					notafiscalproduto.setInfoadicionalcontrib(infadicionais);
				} else {
					if (!notafiscalproduto.getInfoadicionalcontrib().contains(infadicionais)) {
						notafiscalproduto.setInfoadicionalcontrib(notafiscalproduto.getInfoadicionalcontrib() + ". " + infadicionais);
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#carregarDocumentoParaAjusteFiscal(Notafiscalproduto notafiscalproduto)
	*
	* @param notafiscalproduto
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public Notafiscalproduto carregarDocumentoParaAjusteFiscal(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoDAO.carregarDocumentoParaAjusteFiscal(notafiscalproduto);
	}
	
//	public Notafiscalproduto geraNotaExpedicao(Notafiscalproduto nf, String whereInexpedicao) {
//		List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
//		List<Expedicao> listaExpedicao = expedicaoService.findForNota(whereInexpedicao);
//		
//		List<Venda> listaVenda = expedicaoService.getVendaForExpedicaoNota(listaExpedicao);
//		
//		Double qtdeTotal = 0d;
//		Double pesoliquidoTotal = 0d;
//		Double pesobrutoTotal = 0d;
//		HashMap<Venda, Money> mapFrete = new HashMap<Venda, Money>();
//		HashMap<Venda, Money> mapDesconto = new HashMap<Venda, Money>();
//		HashMap<Venda, Money> mapOutrasdespesas = new HashMap<Venda, Money>();
//		HashMap<Venda, Boolean> mapConsiderarJurosOutrasdespesas = new HashMap<Venda, Boolean>();
//		
//		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
//			for(Expedicao expedicao : listaExpedicao){
//				nf.setEmpresa(expedicao.getEmpresa());
//				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
//					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
//						Notafiscalprodutoitem item = new Notafiscalprodutoitem();
//						Material material = expedicaoitem.getMaterial();
//						item.setMaterial(expedicaoitem.getMaterial());
//						item.setLocalarmazenagem(expedicaoitem.getLocalarmazenagem());
//						item.setQtde(expedicaoitem.getQtdeexpedicao());
//						nf.setCliente(expedicaoitem.getCliente());
//						
//						if(expedicaoitem.getVendamaterial() != null){
//							Vendamaterial vendamaterial = expedicaoitem.getVendamaterial();
//							Venda venda = expedicaoService.getVendaByVendaExpedicao(listaVenda, vendamaterial.getVenda());
//							
//							if(mapOutrasdespesas.get(venda) == null){
//								Money outrasDespesasIt = new Money();
//								boolean considerarJurosNota = false;
//								if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getConsiderarjurosnota() != null && venda.getPrazopagamento().getConsiderarjurosnota()){
//									Money totalParcela = venda.getTotalparcela();
//									Money totalProdutos = venda.getTotalvenda();
//									
//									Money diferencaJuros = totalParcela.subtract(totalProdutos);
//									if(diferencaJuros.getValue().compareTo(BigDecimal.ZERO) > 0){
//										considerarJurosNota = true;
//										outrasDespesasIt = diferencaJuros.divide(new Money(venda.getListavendamaterial().size(), false));
//									}
//								}
//								mapConsiderarJurosOutrasdespesas.put(venda, considerarJurosNota);
//								mapOutrasdespesas.put(venda, outrasDespesasIt);
//							}
//							
//							Double multiplicador = expedicaoitem.getVendamaterial().getMultiplicador();;
//							if(multiplicador == null) multiplicador = 1.0;
//							
//							if(mapFrete.get(venda) == null){
//								mapFrete.put(venda, venda.getValorfrete());
//							}
//							
//							String nomeComAlturaLarguraComprimento = vendaService.getAlturaLarguraComprimentoConcatenados(
//									vendamaterial, material);
//							if(!"".equals(nomeComAlturaLarguraComprimento)){
//								material.setNome(material.getNome() + " " + nomeComAlturaLarguraComprimento);
//							}
//							item.setNotafiscalproduto(nf);
//							item.setLoteestoque(vendamaterial.getLoteestoque());
//							item.setPatrimonioitem(vendamaterial.getPatrimonioitem());
//							
//							if(material.getCodigoanp() != null){
//								item.setCodigoanp(material.getCodigoanp());
//								item.setDadoscombustivel(Boolean.TRUE);
//							}
//							
//							if(vendamaterial.getLoteestoque() != null && vendamaterial.getLoteestoque().getNumero() != null){
//								item.setInfoadicional("Lote: " + vendamaterial.getLoteestoque().getNumero());
//							}
//
//							if(material.getEpi() != null && material.getEpi()){
//								item.setMaterialclasse(Materialclasse.EPI);
//							}else if(material.getPatrimonio() != null && material.getPatrimonio() && (material.getProduto() == null || !material.getProduto())){
//								item.setMaterialclasse(Materialclasse.PATRIMONIO);
//							}else if(material.getProduto() != null && material.getProduto() && (material.getPatrimonio() == null || !material.getPatrimonio())){
//								item.setMaterialclasse(Materialclasse.PRODUTO);
//							}
//							
//							Double fatorconversao = expedicaoService.verificaUnidademedidaMaterialDiferenteForNota(item, expedicaoitem, vendamaterial);
//							if(item.getUnidademedida() == null){
//								item.setUnidademedida(vendamaterial.getUnidademedida());
//							}
//							
//							Double quantidade = item.getQtde();
//							qtdeTotal += quantidade;
//							
//							
//							Double peso = material.getPeso();
//							Double pesobruto = material.getPesobruto();
//							if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){
//								Material aux_material = materialService.findListaMaterialunidademedida(item.getMaterial());
//								Unidademedida aux_unidademedida = item.getUnidademedida();
//								
//								if(aux_unidademedida != null && peso != null && pesobruto != null){
//									Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
//									if(!unidadeprincipal){
//										Double fracao = this.getFracaoConversaoUnidademedida(aux_material, aux_unidademedida);
//										if(fracao != null && fracao > 0){
//											peso = peso/fracao;
//											pesobruto = pesobruto/fracao;
//										}
//									}
//								}
//							}
//							
//							if(peso != null && peso > 0){
//								peso = quantidade * peso;
//								if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
//									peso = quantidade;
//								}
//								peso = SinedUtil.roundByParametro(peso);
//								
//								item.setPesoliquido(peso);
//								pesoliquidoTotal += peso;
//							}
//							if(pesobruto != null && pesobruto > 0){
//								pesobruto = quantidade * pesobruto;
//								if(item.getUnidademedida() != null && item.getUnidademedida().getSimbolo() != null && item.getUnidademedida().getSimbolo().trim().toUpperCase().equals("KG")){
//									pesobruto = quantidade;
//								}
//								pesobruto = SinedUtil.roundByParametro(pesobruto);
//								
//								item.setPesobruto(pesobruto);
//								pesobrutoTotal += pesobruto;
//							}
//							
//							item.setIncluirvalorprodutos(true);
//							item.setValorfrete(venda.getValorfrete() != null && venda.getValorfrete().getValue().doubleValue() > 0.0 ? new Money(venda.getValorfrete().getValue().doubleValue()/venda.getListavendamaterial().size()) : new Money(0.0));
//							
//							if(mapConsiderarJurosOutrasdespesas.get(venda) != null && mapConsiderarJurosOutrasdespesas.get(venda) && 
//									mapOutrasdespesas.get(venda) != null){
//								item.setOutrasdespesas(mapOutrasdespesas.get(venda));
//							}
//							
//							Money valorDesconto = new Money();
//							Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
//							
//							if (expedicaoitem.getCliente() != null && expedicaoitem.getCliente().getDiscriminardescontonota() != null && 
//									expedicaoitem.getCliente().getDiscriminardescontonota()){
//								valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), false);
//								valorDesconto = valorDesconto.add(valorDescontoItem);
//								item.setValordesconto(valorDesconto);
//								if(item.getValorunitario() == null || item.getValorunitario() == 0){
//									item.setValorunitario(vendamaterial.getPreco());
//								}
//								if(mapDesconto.get(venda) == null){
//									mapDesconto.put(venda, venda.getDesconto());
//								}
//							} else {
//								valorDesconto = getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), true);
//
//								if(multiplicador != null){
//									valorDescontoItem = valorDescontoItem.divide(new Money(quantidade*multiplicador));
//								}else {
//									valorDescontoItem = valorDescontoItem.divide(new Money(quantidade));
//								}
//								
//								valorDesconto = valorDesconto.add(valorDescontoItem);
//								
//								if(fatorconversao != null && fatorconversao > 0d){
//									valorDesconto = valorDesconto.multiply(new Money(fatorconversao));
//								}
//								
//								if(item.getValorunitario() == null || item.getValorunitario() == 0.0){
//									item.setValorunitario(vendamaterial.getPreco() - valorDesconto.getValue().doubleValue());
//								}else {
//									item.setValorunitario(item.getValorunitario() - valorDesconto.getValue().doubleValue());
//								}
//							}
//							
//							if(multiplicador != null){
//								if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
//									item.setQtde(item.getQtde() * multiplicador); 
//								} else {
//									item.setValorunitario(item.getValorunitario() * multiplicador);
//								}
//							}
//							item.setValorunitario(SinedUtil.round(item.getValorunitario(), 10));
//							
//							if(item.getMaterial() != null){
//								Cliente cliente = venda.getCliente();
//								Endereco endereco = null;
//								if(cliente != null && cliente.getListaEndereco() != null && !cliente.getListaEndereco().isEmpty()){
//									Set<Endereco> listaEndereco = cliente.getListaEndereco();
//									if(listaEndereco != null && listaEndereco.size() > 0){
//										for (Endereco e : listaEndereco) {
//											if(e.getEnderecotipo() != null && e.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
//												endereco = e;
//												break;
//											}
//										}
//									}
//								}
//								nf.setEnderecoCliente(endereco != null ? endereco : venda.getEndereco());
//								Material beanMaterial = materialService.loadForTributacao(material);
//								List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
//										new GrupotributacaoVO(	Operacao.SAIDA, 
//																nf.getEmpresa(), 
//																nf.getNaturezaoperacao(), 
//																beanMaterial.getMaterialgrupo(),
//																(expedicaoitem.getCliente() != null && expedicaoitem.getCliente().getCrt() != null ? expedicaoitem.getCliente().getCrt().getDescricao() : ""), 
//																(expedicaoitem.getCliente() != null ? expedicaoitem.getCliente().getIncidiriss() : null),
//																true, 
//																beanMaterial,
//																item.getNcmcompleto(),
//																endereco, 
//																null));
//								item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
//								item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
//							}
//							item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, nf.getEmpresa(), nf.getCliente(), nf.getEmpresa() != null ? nf.getEmpresa().getEndereco() : null, nf.getEnderecoCliente(), null);
//							
//							if (nf.getNaturezaoperacao()!=null && nf.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
//								Endereco enderecoEmpresa = null;
//								if (nf.getEmpresa()!=null && nf.getEmpresa().getCdpessoa()!=null){
//									enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(nf.getEmpresa());
//								}
//								Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoEmpresa, nf.getEnderecoCliente());
//								List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(nf.getNaturezaoperacao(), cfopescopo);
//								if (!listaNaturezaoperacaocfop.isEmpty()){
//									item.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
//								}
//							}
//							
//							item.setNcmcapitulo(material.getNcmcapitulo());
//							item.setExtipi(material.getExtipi() != null ? material.getExtipi() : "");
//							item.setNcmcompleto(material.getNcmcompleto() != null ? material.getNcmcompleto() : "");
//							if(material.getCodigobarras() != null && !"".equals(material.getCodigobarras()) && SinedUtil.validaCEAN(material.getCodigobarras()))
//								item.setGtin(material.getCodigobarras() != null ? material.getCodigobarras() : "");
//							
//							if(item.getValorunitario() != null && item.getQtde() != null){
//								double basecalculo = item.getValorunitario() * item.getQtde();
//								
//								if(item.getIcms() != null){
//									item.setValorbcicms(new Money(basecalculo));
//									item.setValoricms(new Money((basecalculo * item.getIcms())/100d));
//								}
//								
//								if(item.getIss() != null){
//									item.setValorbciss(new Money(basecalculo));
//									item.setValoriss(new Money((basecalculo * item.getIss())/100d));
//								}
//								
//								if(item.getIpi() != null){
//									item.setValorbcipi(new Money(basecalculo));
//									item.setValoripi(new Money((basecalculo * item.getIpi())/100d));
//								} else if(item.getAliquotareaisipi() != null){
//									item.setQtdevendidaipi(item.getQtde());
//									item.setValoripi(new Money(item.getAliquotareaisipi().getValue().doubleValue() * item.getQtde()));
//								}
//								
//								if(item.getPis() != null){
//									item.setValorbcpis(new Money(basecalculo));
//									item.setValorpis(new Money((basecalculo * item.getPis())/100d));
//								} else if(item.getAliquotareaispis() != null){
//									item.setQtdevendidapis(item.getQtde());
//									item.setValorpis(new Money(item.getAliquotareaispis().getValue().doubleValue() * item.getQtde()));
//								}
//								
//								if(item.getCofins() != null){
//									item.setValorbccofins(new Money(basecalculo));
//									item.setValorcofins(new Money((basecalculo * item.getCofins())/100d));
//								} else if(item.getAliquotareaiscofins() != null){
//									item.setQtdevendidacofins(item.getQtde());
//									item.setValorcofins(new Money(item.getAliquotareaiscofins().getValue().doubleValue() * item.getQtde()));
//								}
//								
//								boolean naoTemIcms = (item.getTributadoicms() != null && !item.getTributadoicms()) ||
//										(item.getTipocobrancaicms() != null &&
//										(
//										Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
//										));
//								
//								boolean naoTemIcmsSt =  (item.getTributadoicms() != null && !item.getTributadoicms()) ||
//										(item.getTipocobrancaicms() != null && 
//										(
//										Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
//										));
//								
//								boolean cobradoAnteriormente = item.getTipocobrancaicms() != null && (
//										Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//										Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
//										);
//									
//								if(naoTemIcms){ 
//									item.setValorbcicms(new Money());
//									item.setIcms(0d);
//									item.setValoricms(new Money());
//								}
//								
//								if(naoTemIcmsSt){
//									item.setValorbcicmsst(new Money());
//									item.setIcmsst(0d);
//									item.setValoricmsst(new Money());
//								}
//								
//								if(cobradoAnteriormente && material != null && material.getCdmaterial() != null && item.getQtde() != null){
//									Aux_imposto aux_imposto = this.buscaValoresCobradosAnteriormenteIcmsst(material, item.getQtde(), item.getGrupotributacaotrans(), null);
//									if(aux_imposto != null){
//										if(aux_imposto.getValorbcicmsst() != null){
//											item.setValorbcicmsst(aux_imposto.getValorbcicmsst());
//										}
//										if(aux_imposto.getValoricmsst() != null){
//											item.setValoricmsst(aux_imposto.getValoricmsst());
//										}
//									}
//								}
//								
//								boolean naoTemPis = item.getTipocobrancapis() != null && (
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_04) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_06) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_07) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_08) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_09)
//								);
//								
//								if(naoTemPis){ 
//									item.setValorbcpis(new Money());
//									item.setPis(0d);
//									item.setValorpis(new Money());
//								}
//								
//								boolean naoTemCofins = item.getTipocobrancacofins() != null && (
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_04) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_06) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_07) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_08) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_09)
//								);
//								
//								if(naoTemCofins){ 
//									item.setValorbccofins(new Money());
//									item.setCofins(0d);
//									item.setValorcofins(new Money());
//								}
//								
//								boolean naoTemIss = item.getTributadoicms() != null && item.getTributadoicms();
//								
//								if(naoTemIss){
//									item.setValorbciss(new Money());
//									item.setIss(0d);
//									item.setValoriss(new Money());
//								}
//								
//								boolean temIcms = item.getTipocobrancaicms() != null && (
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_PARTILHA) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL)
//								);
//								
//								if(temIcms){
//									if(item.getValorbcicms() == null) item.setValorbcicms(new Money());
//									if(item.getIcms() == null) item.setIcms(0d);
//									if(item.getValoricms() == null) item.setValoricms(new Money());
//								}
//								
//								boolean temIcmsSt = item.getTipocobrancaicms() != null && (
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_PARTILHA) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST) ||
//										item.getTipocobrancaicms().equals(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL)
//								);
//								
//								if(temIcmsSt){
//									if(item.getValorbcicmsst() == null) item.setValorbcicmsst(new Money());
//									if(item.getIcmsst() == null) item.setIcmsst(0d);
//									if(item.getValoricmsst() == null) item.setValoricmsst(new Money());
//								}
//								
//								boolean temPis = item.getTipocobrancapis() != null && (
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_01) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_02) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_49) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_50) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_51) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_52) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_53) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_54) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_55) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_56) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_60) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_61) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_62) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_63) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_64) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_65) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_66) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_67) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_70) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_71) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_72) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_73) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_74) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_75) ||
//										item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_98) ||
//				 						item.getTipocobrancapis().equals(Tipocobrancapis.OPERACAO_99)
//								);
//								
//								if(temPis){
//									if(item.getValorbcpis() == null) item.setValorbcpis(new Money());
//									if(item.getPis() == null) item.setPis(0d);
//									if(item.getValorpis() == null) item.setValorpis(new Money());
//								}
//								
//								boolean temCofins = item.getTipocobrancacofins() != null && (
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_01) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_02) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_49) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_50) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_51) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_52) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_53) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_54) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_55) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_56) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_60) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_61) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_62) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_63) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_64) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_65) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_66) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_67) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_70) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_71) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_72) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_73) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_74) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_75) ||
//										item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_98) ||
//				 						item.getTipocobrancacofins().equals(Tipocobrancacofins.OPERACAO_99)
//								);
//								
//								if(temCofins){
//									if(item.getValorbccofins() == null) item.setValorbccofins(new Money());
//									if(item.getCofins() == null) item.setCofins(0d);
//									if(item.getValorcofins() == null) item.setValorcofins(new Money());
//								}
//								
//								boolean temIpi = item.getTipocobrancaipi() != null && (
//										item.getTipocobrancaipi().equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO) ||
//										item.getTipocobrancaipi().equals(Tipocobrancaipi.ENTRADA_OUTRAS) ||
//										item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_TRIBUTABA) ||
//										item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_OUTRAS)
//								);
//								
//								if(temIpi){
//									if(item.getValorbcipi() == null) item.setValorbcipi(new Money());
//									if(item.getIpi() == null) item.setIpi(0d);
//									if(item.getValoripi() == null) item.setValoripi(new Money());
//								}
//								
//								double valoriss = item.getIncluirissvalor() != null && item.getIncluirissvalor() && item.getValoriss() != null ? item.getValoriss().getValue().doubleValue() : 0d;
//								double valoricms = item.getIncluiricmsvalor() != null && item.getIncluiricmsvalor() && item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : 0d;
//								double valoripi = item.getIncluiripivalor() != null && item.getIncluiripivalor() && item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
//								double valorcofins = item.getIncluircofinsvalor() != null && item.getIncluircofinsvalor() && item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0d;
//								double valorpis = item.getIncluirpisvalor() != null && item.getIncluirpisvalor() && item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0d;
//								
//								double valorbruto = basecalculo + valoriss + valoricms + valoripi + valorcofins + valorpis;
//								item.setValorbruto(new Money(valorbruto));
//							}
//							listaItens.add(item);
//						}
//					}
//				}
//			}
//		}
//		nf.setListaItens(listaItens);
//		
//		Money valorTotalProdutos = new Money(0.0);
//		for (Notafiscalprodutoitem pi : nf.getListaItens()) {
//			valorTotalProdutos = valorTotalProdutos.add(pi.getValorbruto());
//		}
//		
//		nf.setValorprodutos(valorTotalProdutos);
//		
//		if(qtdeTotal != null && qtdeTotal > 0){
//			nf.setQtdevolume(qtdeTotal.intValue());
//		}
//		if(pesoliquidoTotal != null && pesoliquidoTotal > 0){
//			nf.setPesoliquido(SinedUtil.roundByParametro(pesoliquidoTotal));
//		}
//		if(pesobrutoTotal != null && pesobrutoTotal > 0){
//			nf.setPesobruto(SinedUtil.roundByParametro(pesobrutoTotal));
//		}
//		
//		Money totalValorFrete = new Money();
//		for(Money frete : mapFrete.values()){
//			totalValorFrete = totalValorFrete.add(frete);
//		}
//		if (totalValorFrete != null){
//			valorTotalProdutos = valorTotalProdutos.add(totalValorFrete);
//		}
//		
//		Money totalValorDesconto = new Money();
//		for(Money desconto : mapDesconto.values()){
//			totalValorDesconto = totalValorDesconto.add(desconto);
//		}
//		if(totalValorDesconto != null && totalValorDesconto.getValue().doubleValue() > 0){
//			nf.setValordesconto(totalValorDesconto);
//			valorTotalProdutos = valorTotalProdutos.subtract(totalValorDesconto);
//		}
//		
//		nf.setValor(valorTotalProdutos);
//		nf.setNotaStatus(NotaStatus.EMITIDA);	
//		nf.setExpedicoes(whereInexpedicao);
//		nf.setValorfrete(totalValorFrete);
//		
//		this.setTotalNota(nf);
//		
//		return nf;
//	}
		
	/**
	 * Carrega as informa��es necess�rias para Emiss�o de Nota Fiscal de Produto, baseada no Faturamento de Requisi��o(OS)
	 * 
	 * @author Rafael Salvio
	 */
	public Notafiscalproduto preencheNotaForFaturarProdutosRequisiscao(Notafiscalproduto bean, String whereIn) {
		bean.setWhereInRequisicao(whereIn);
		bean.setPresencacompradornfe(Presencacompradornfe.OUTROS);
		List<Requisicao> listRequisicao = requisicaoService.loadForEmitirNotaFiscalProduto(whereIn, "produto");
		List<Notafiscalprodutoitem> listProdutos = new ArrayList<Notafiscalprodutoitem>();
		if(listRequisicao != null){
			for (Requisicao requisicao : listRequisicao) {
				if (requisicao.getEmpresa() != null){
					Empresa empresa = empresaService.loadWithEndereco(requisicao.getEmpresa());
					bean.setEmpresa(empresa);
					
					if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null && bean.getMunicipiogerador() == null){
						Endereco endereco = enderecoService.carregaEnderecoEmpresa(bean.getEmpresa());
						if(endereco != null){
							bean.setMunicipiogerador(endereco.getMunicipio());
						}
					}
				}
				if (requisicao.getCliente() != null){
					Cliente cliente = clienteService.carregarDadosCliente(requisicao.getCliente());
					Endereco enderecoCliente = null;
					
					Set<Endereco> listaEndereco = cliente.getListaEndereco();
					if(listaEndereco != null && listaEndereco.size() > 0){
						for (Endereco endereco : listaEndereco) {
							if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
								enderecoCliente = endereco;
								break;
							}
						}
					}
					bean.setCliente(cliente);
					bean.setEnderecoCliente(enderecoCliente);
				}
				if(requisicao.getEndereco() != null){
					bean.setEnderecoCliente(requisicao.getEndereco());
				}
				if(requisicao.getListaMateriaisrequisicao() != null){
					for (Materialrequisicao requisicaoMaterial : requisicao.getListaMateriaisrequisicao()) {
						if(requisicaoMaterial.getMaterial() != null){
							Notafiscalprodutoitem item = new Notafiscalprodutoitem();
							item.setMaterial(requisicaoMaterial.getMaterial());
							item.setQtde(requisicaoMaterial.getQuantidade());
							item.setUnidademedida(requisicaoMaterial.getMaterial().getUnidademedida());
							item.setInfoadicional(requisicaoMaterial.getObservacao());
							item.setMaterialclasse(Materialclasse.PRODUTO);
							item.setNcmcapitulo(requisicaoMaterial.getMaterial().getNcmcapitulo());
							item.setNcmcompleto(requisicaoMaterial.getMaterial().getNcmcompleto());
							item.setIncluirvalorprodutos(Boolean.TRUE);
							if(requisicaoMaterial.getValorunitario() != null){
								item.setValorunitario(requisicaoMaterial.getValorunitario());
							}else if(requisicaoMaterial.getMaterial().getValorvenda() != null){
								item.setValorunitario(requisicaoMaterial.getMaterial().getValorvenda());
							}
							item.setValorbruto(new Money((requisicaoMaterial.getQuantidade() != null ? requisicaoMaterial.getQuantidade() : 0d)
									* (item.getValorunitario() != null ? item.getValorunitario() : 0d)));
							item.setGrupotributacao(requisicaoMaterial.getMaterial().getGrupotributacao());
							
							if(item.getMaterial() != null){
								List<Categoria> listaCategoriaCliente = null;
								if(bean.getCliente() != null){
									listaCategoriaCliente = categoriaService.findByPessoa(bean.getCliente());
								}
								
								Material beanMaterial = materialService.loadForTributacao(item.getMaterial());
								List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
										grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
																bean.getEmpresa(), 
																bean.getNaturezaoperacao(), 
																beanMaterial.getMaterialgrupo(),
																bean.getCliente(),
																true, 
																beanMaterial,
																item.getNcmcompleto(),
																bean.getEnderecoCliente(), 
																null,
																listaCategoriaCliente,
																null,
																bean.getOperacaonfe(),
																bean.getModeloDocumentoFiscalEnum(),
																bean.getLocaldestinonfe(),
																bean.getDtEmissao()));
								item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
								item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
							}
							item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, bean.getEmpresa(), bean.getCliente(), bean.getEmpresa() != null ? bean.getEmpresa().getEndereco() : null, bean.getEnderecoCliente(), null);
							
							if(item.getGrupotributacao() != null){
								if(org.apache.commons.lang.StringUtils.isNotBlank(item.getGrupotributacao().getInfoadicionalcontrib())){
									if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfoadicionalcontrib()) || !bean.getInfoadicionalcontrib().contains(item.getGrupotributacao().getInfoadicionalcontrib())){
										if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfoadicionalcontrib())){
											bean.setInfoadicionalcontrib(item.getGrupotributacao().getInfoadicionalcontrib()); 
										}else if(!bean.getInfoadicionalcontrib().contains(bean.getGrupotributacao().getInfoadicionalcontrib())){
											bean.setInfoadicionalcontrib(bean.getInfoadicionalcontrib() + ". " + item.getGrupotributacao().getInfoadicionalcontrib());
										}
									}
								}
								if(org.apache.commons.lang.StringUtils.isNotBlank(item.getGrupotributacao().getInfoadicionalfisco())){
									if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfoadicionalfisco()) || !bean.getInfoadicionalfisco().contains(item.getGrupotributacao().getInfoadicionalfisco())){
										if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfoadicionalfisco())){
											bean.setInfoadicionalfisco(item.getGrupotributacao().getInfoadicionalfisco()); 
										}else if(!bean.getInfoadicionalfisco().contains(bean.getGrupotributacao().getInfoadicionalfisco())){
											bean.setInfoadicionalfisco(bean.getInfoadicionalfisco() + ". " + item.getGrupotributacao().getInfoadicionalfisco());
										}
									}
								}
							}
							listProdutos.add(item);
						}
					}
				}
			}
		}
		bean.setListaItens(listProdutos);
		return bean;
	}
		
	/**
	* M�todo para gerar receita em lote
	*
	* @param request
	* @param listaNotafiscalproduto
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public ModelAndView gerarReceitaEmLote(WebRequestContext request, List<Notafiscalproduto> listaNotafiscalproduto) {
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		StringBuilder sucesso = new StringBuilder();
		StringBuilder erro = new StringBuilder();
		StringBuilder erroRateio = new StringBuilder();
		if(SinedUtil.isListNotEmpty(listaNotafiscalproduto)){
			for(Notafiscalproduto notafiscalproduto : listaNotafiscalproduto){
				setPrazogapamentoUnico(notafiscalproduto);
				if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaDuplicata()) && notafiscalproduto.getPrazopagamentofatura() != null){
					GeraReceita geraReceita = notaService.criarGeraReceita(notafiscalproduto);
					geraReceita.setValorUmaParcela(getValorTotalReceita(notafiscalproduto));
					geraReceita.setPrazoPagamento(notafiscalproduto.getPrazopagamentofatura());
					if(notafiscalproduto.getValordescontofatura() != null){
						geraReceita.setDescontoContratualMny(notafiscalproduto.getValordescontofatura());
					}
					geraReceita.setQtdeParcelas(notafiscalproduto.getListaDuplicata().size());
					Documento documento = documentoService.gerarDocumento(geraReceita, true);
					
					if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
						boolean existeErroRateio = false;
						for(Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
							if(rateioitem.getContagerencial() == null){
								erroRateio.append(notafiscalproduto.getNumero() + " - Material ou empresa sem centro de custo para o rateio.").append(",");
								existeErroRateio = true;
							}else if(rateioitem.getCentrocusto() == null){
								erroRateio.append(notafiscalproduto.getNumero() + " - Material ou empresa sem centro de custo para o rateio.").append(",");
								existeErroRateio = true;
							}
							if(existeErroRateio) break;
						}
						if(existeErroRateio) continue;
					}
					documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					documento.ajustaBeanToSave();
					notaService.liquidar(documento.getCdNota(), documento.getNumeroNota(), documento);
					
					sucesso.append(notafiscalproduto.getNumero()).append(",");
				}else {
					erro.append(notafiscalproduto.getNumero()).append(",");
				}
			}
		}
		
		if(sucesso.length() > 0){
			request.addMessage("Receita em lote gerada com sucesso. Notas: " + sucesso.substring(0, sucesso.length()-1));
		}
		if(erro.length() > 0){
			request.addError("N�o � poss�vel gerar receita em lote de nota sem prazo de pagamento. Notas: " + erro.substring(0, erro.length()-1));
		}
		if(erroRateio.length() > 0){
			request.addError("N�o foi poss�vel gerar receita em lote. Nota(s):  " + erroRateio.substring(0, erroRateio.length()-1));
		}
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto" + (entrada ? "?ACAO=consultar&cdNota=" + listaNotafiscalproduto.get(0).getCdNota() : ""));
	}

	public Money getValorTotalReceita(Notafiscalproduto notafiscalproduto) {
		Money valorTotal = new Money();
		if(notafiscalproduto.getValor() != null && notafiscalproduto.getValor().getValue().doubleValue() > 0){
			valorTotal = notafiscalproduto.getValor();
			if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaItens())){
				for(Notafiscalprodutoitem item : notafiscalproduto.getListaItens()){
					if(item.getValorbruto() != null && item.getValorbruto().getValue().doubleValue() > 0 &&
							item.getCfop() != null && item.getCfop().getNaoconsiderarreceita() != null && 
							item.getCfop().getNaoconsiderarreceita()){
						valorTotal = valorTotal.subtract(item.getValorbruto());
					}
				}
			}
		}
		return valorTotal;
	}
	
	/**
	* M�todo que cria o rateio de acordo com os materiais da nota
	*
	* @param notafiscalproduto
	* @param empresa
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public List<Rateioitem> criaRateioByNota(Notafiscalproduto notafiscalproduto, Empresa empresa) {
		List<Rateioitem> listarateioitem = new ArrayList<Rateioitem>();
		if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaItens())){
			for (Notafiscalprodutoitem item : notafiscalproduto.getListaItens()) {
				Material material = item.getMaterial();
				
//				if(item.getCfop() != null && item.getCfop().getNaoconsiderarreceita() != null && 
//						item.getCfop().getNaoconsiderarreceita())
//					continue;
				
				Contagerencial contagerencial = empresa.getContagerencial();
				Centrocusto centrocusto = empresa.getCentrocusto();
				
				if(material.getContagerencialvenda() != null && material.getContagerencialvenda().getCdcontagerencial() != null){
					contagerencial = material.getContagerencialvenda();
				}else if(empresa != null && empresa.getContagerencial() != null && empresa.getContagerencial().getCdcontagerencial() != null){
					contagerencial = empresa.getContagerencial();
				}
				if(material != null && material.getCentrocustovenda() != null && material.getCentrocustovenda().getCdcentrocusto() != null){
					centrocusto = material.getCentrocustovenda();
				}else if(empresa != null && empresa.getCentrocusto() != null && empresa.getCentrocusto().getCdcentrocusto() != null){
					centrocusto = empresa.getCentrocusto();
				}
				
				Rateioitem rateioitem = new Rateioitem();
				rateioitem.setContagerencial(contagerencial);
				rateioitem.setCentrocusto(centrocusto);
				rateioitem.setProjeto(notafiscalproduto.getProjeto());
				
				if(rateioitem.getContagerencial() != null && rateioitem.getCentrocusto() != null){
					if(item.getValorbruto() != null && notafiscalproduto.getValor() != null && 
							notafiscalproduto.getValor().getValue().doubleValue() > 0){
						Money totalProdutos = new Money(item.getValorbruto());
						if(item.getOutrasdespesas() != null) totalProdutos = totalProdutos.add(item.getOutrasdespesas());
						if(item.getValordesconto() != null) totalProdutos = totalProdutos.add(item.getValordesconto());
						if(item.getValorfrete() != null) totalProdutos = totalProdutos.add(item.getValorfrete());
						if(item.getValorseguro() != null) totalProdutos = totalProdutos.add(item.getValorseguro());
						if(item.getValoricmsst() != null) totalProdutos = totalProdutos.add(item.getValoricmsst());
						if(item.getValoripi() != null) totalProdutos = totalProdutos.add(item.getValoripi());
						if(item.getValorii() != null) totalProdutos = totalProdutos.add(item.getValorii());
					
						rateioitem.setValor(totalProdutos);
						rateioitem.setPercentual(totalProdutos.getValue().doubleValue() / notafiscalproduto.getValor().getValue().doubleValue() * 100);
						listarateioitem.add(rateioitem);
					}
				}
			}
		}
	
		return listarateioitem;
	}
	
	/**
	* M�todo que seta o nome do material com o nome alternativo do kit
	*
	* @param nfp
	* @since 02/04/2015
	* @author Luiz Fernando
	*/
	public void setNomeAlternativo(Notafiscalproduto nfp) {
		if(nfp != null && nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
			for(Notafiscalprodutoitem nfpi : nfp.getListaItens()){
				if(nfpi.getMaterial() != null && nfpi.getVendamaterialmestre() != null && 
							org.apache.commons.lang.StringUtils.isNotEmpty(nfpi.getVendamaterialmestre().getNomealternativo())){
					nfpi.getMaterial().setNomealternativo(nfpi.getVendamaterialmestre().getNomealternativo());
					nfpi.getMaterial().setNomenfdescricaonota(nfpi.getVendamaterialmestre().getNomealternativo());
				}
			}
		}
	}
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * param whereIn
	 * @param filtro
	 * @return
	 * @since 26/01/2015
	 * @author Jo�o Vitor
	 */
	public List<Notafiscalproduto> findForSpedRegK200(Material material, SpedarquivoFiltro filtro) {
		return notafiscalprodutoDAO.findForSpedRegK200(material, filtro);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 * @since 26/08/2015
	 * @author Andrey Leonardo
	 */
	public List<Notafiscalproduto> findForMemorandoExportacao(String whereIn){
		return notafiscalprodutoDAO.findForMemorandoExportacao(whereIn);
	}
	
	/**
	 * @param listaNotaFiscalProduto
	 * @return
	 * @since 26/08/2015
	 * @author Andrey Leonardo
	 */
	public Boolean isNotaExportacao(List<Notafiscalproduto> listaNotaFiscalProduto){
		if(listaNotaFiscalProduto == null || listaNotaFiscalProduto.size() == 0){
			return false;
		}
		for(Notafiscalproduto notaProduto : listaNotaFiscalProduto){
			if(notaProduto == null || notaProduto.getNaturezaoperacao() == null){
				return false;
			}
			for(Naturezaoperacaocfop naturezaOpCfop : notaProduto.getNaturezaoperacao().getListaNaturezaoperacaocfop()){
				Cfop cfop = naturezaOpCfop.getCfop();
				if(!cfop.getCfoptipo().equals(Cfoptipo.SAIDA) && !cfop.getCfopescopo().equals(Cfopescopo.INTERNACIONAL)){
					return false;
				}
			}
		}
		return true;
	}
	
	public Boolean isStatusValido(String notafiscalproduto){
		NotaStatus[] situacoes = null;	
		
		situacoes = new NotaStatus[] {NotaStatus.NFE_SOLICITADA,
				NotaStatus.CANCELADA, NotaStatus.NFE_ENVIADA, NotaStatus.NFE_CANCELANDO };
		
		return notaService.haveNFDiferenteStatus(notafiscalproduto, situacoes);
	}
	
	public Boolean isExisteVinculo(Notafiscalproduto notaFiscalProduto){
		if(notaFiscalProduto == null){
			throw new SinedException("Par�metro inv�lido");
		}
		Boolean isExisteVinculo = true;

		if (notaFiscalProduto.getListaItens().isEmpty()) {
			return false;
		}
		for (Notafiscalprodutoitem notaitem : notaFiscalProduto.getListaItens()) {
			if (!notaitem.getListaNotaprodutoitementregamaterial().isEmpty()) {
				isExisteVinculo = true;
				break;
			} else
				isExisteVinculo = false;
		}		
		return isExisteVinculo;
	}
	
	public List<Notafiscalproduto> loadForMemorandoExportacao(String whereIn){
		return notafiscalprodutoDAO.loadForMemorandoExportacao(whereIn);
	}
	
	/**
	* M�todo que cria a nota a partir da coleta
	*
	* @param whereInColeta
	* @param devolucaoBool
	* @return
	* @since 09/06/2016
	* @author Luiz Fernando
	*/
	public Notafiscalproduto criaNotaByColeta(String whereInColeta, Boolean devolucaoBool, List<FaturarColetaItemBean> listaFaturaColetaItem){
		Notafiscalproduto bean = new Notafiscalproduto();
		bean.setListaItens(new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class));
		
		bean.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		bean.setNotaStatus(NotaStatus.EMITIDA);
		bean.setDtEmissao(new Date(System.currentTimeMillis()));
		bean.setOperacaonfe(Operacaonfe.NORMAL);
		bean.setPresencacompradornfe(Presencacompradornfe.PRESENCIAL);
		bean.setFinalidadenfe(Finalidadenfe.NORMAL);
		bean.setTipooperacaonota(Tipooperacaonota.ENTRADA);
		bean.setFormapagamentonfe(Formapagamentonfe.A_VISTA);
		bean.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
		bean.setCadastrarcobranca(false);
		bean.setCadastrartransportador(false);
		bean.setWhereInColeta(whereInColeta);
		
		List<Categoria> listaCategoriaCliente = null;
		List<Coleta> listaColetaOrdenada = new ArrayList<Coleta>();
		Set<Presencacompradornfe> listaPresencacompradornfe = new HashSet<Presencacompradornfe>();
		
		List<Coleta> listaColeta = coletaService.loadForGerarNotafiscalproduto(whereInColeta);
		if(listaColeta != null && !listaColeta.isEmpty()){
			Set<Empresa> listaEmpresas = new ListSet<Empresa>(Empresa.class);
			Set<Cliente> listaClientes = new ListSet<Cliente>(Cliente.class);
			Set<Projeto> listaProjetos = new ListSet<Projeto>(Projeto.class);
			Set<Endereco> listaEnderecos = new ListSet<Endereco>(Endereco.class);
			Set<Naturezaoperacao> listaNaturezaoperacao = new ListSet<Naturezaoperacao>(Naturezaoperacao.class);
			
			if(listaFaturaColetaItem != null && listaFaturaColetaItem.size() > 0){
				for (FaturarColetaItemBean it : listaFaturaColetaItem) {
					for (Iterator<Coleta> iterator = listaColeta.iterator(); iterator.hasNext();) {
						Coleta coleta = (Coleta) iterator.next();
						if(coleta.getPedidovenda() != null && 
								coleta.getPedidovenda().getIdentificador() != null &&
								coleta.getPedidovenda().getIdentificador().equals(it.getIdentificador())){
							listaColetaOrdenada.add(coleta);
							iterator.remove();
						}						
					}
				}
				
				listaColetaOrdenada.addAll(listaColeta);
			} else {
				listaColetaOrdenada.addAll(listaColeta);
			}
			
			for(Coleta coleta : listaColetaOrdenada){
				if(coleta.getEmpresa() != null){
					if(!listaEmpresas.contains(coleta.getEmpresa())){
						listaEmpresas.add(coleta.getEmpresa());
					}
				}else if(coleta.getPedidovenda() != null && 
						coleta.getPedidovenda().getEmpresa() != null && 
						!listaEmpresas.contains(coleta.getPedidovenda().getEmpresa())){
					listaEmpresas.add(coleta.getPedidovenda().getEmpresa());
				}
				if(coleta.getCliente() != null && !listaClientes.contains(coleta.getCliente())){
					listaClientes.add(coleta.getCliente());
				}
				if(coleta.getPedidovenda() != null){
					if(coleta.getPedidovenda().getProjeto() != null && !listaProjetos.contains(coleta.getPedidovenda().getProjeto())){
						listaProjetos.add(coleta.getPedidovenda().getProjeto());
					}
					if(coleta.getPedidovenda().getEndereco() != null && !listaEnderecos.contains(coleta.getPedidovenda().getEndereco())){
						listaEnderecos.add(coleta.getPedidovenda().getEndereco());
					}
					if(coleta.getPedidovenda().getPedidovendatipo() != null && 
							coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada() != null &&
							!listaNaturezaoperacao.contains(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada())){
						listaNaturezaoperacao.add(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada());
					}
				}
				if (coleta.getPedidovenda()!=null &&
						coleta.getPedidovenda().getPedidovendatipo()!=null &&
						coleta.getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta()!=null){
					listaPresencacompradornfe.add(coleta.getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta());
				}
			}
			
			if(listaNaturezaoperacao.size() == 1){
				bean.setNaturezaoperacao(listaNaturezaoperacao.iterator().next());
			}
			if(listaEmpresas.size() == 1){
				bean.setEmpresa(listaEmpresas.iterator().next());
			}
			if(listaClientes.size() == 1){
				bean.setCliente(listaClientes.iterator().next());
				listaCategoriaCliente = categoriaService.findByPessoa(bean.getCliente());
			}
			if(listaProjetos.size() == 1){
				bean.setProjeto(listaProjetos.iterator().next());
			}
			if(listaEnderecos.size() == 1){
				bean.setEnderecoCliente(listaEnderecos.iterator().next());
				bean.setMunicipiogerador(bean.getEnderecoCliente().getMunicipio());
			}
			
			ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(bean.getNaturezaoperacao());
			
			for(Coleta coleta : listaColetaOrdenada){
				bean.setCliente(coleta.getCliente());
				
				if(coleta.getListaColetaMaterial() != null && !coleta.getListaColetaMaterial().isEmpty()){
					Map<ColetamaterialNotaBean, Double> mapaSaldoMaterial = getMapaSaldoMaterial(coleta.getCdcoleta(), coleta.getListaColetaMaterial(), bean.getTipooperacaonota());
					Notafiscalprodutoitem item;
					ColetamaterialNotaBean coletamaterialNotaBean;
					
					for(ColetaMaterial cm : coleta.getListaColetaMaterial()){	
						if((devolucaoBool && cm.getQuantidadedevolvida() != null && cm.getQuantidadedevolvida() > 0) || !devolucaoBool){
							if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())){
								ColetaMaterialPedidovendamaterial itemServico = cm.getListaColetaMaterialPedidovendamaterial().get(0);
								Integer cdpedidovendamaterial = itemServico.getPedidovendamaterial().getCdpedidovendamaterial();
								
//								coletamaterialNotaBean = new ColetamaterialNotaBean(cm.getMaterial().getCdmaterial(), cm.getPedidovendamaterial() != null ? cm.getPedidovendamaterial().getCdpedidovendamaterial() : null);
								coletamaterialNotaBean = new ColetamaterialNotaBean(cm.getMaterial().getCdmaterial(), cdpedidovendamaterial);
								
								item = new Notafiscalprodutoitem();
								item.setCdcoletamaterial(cm.getCdcoletamaterial());
//									item.setMotivodevolucao(cm.getMotivodevolucao());
								item.setMotivodevolucao(Hibernate.isInitialized(cm.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(cm.getListaColetamaterialmotivodevolucao()) ? cm.getListaColetamaterialmotivodevolucao().get(0).getMotivodevolucao() : null);
								item.setListaMotivodevolucao(cm.getListaMotivodevolucao());
								item.setMaterial(cm.getMaterial());
								item.setUnidademedida(cm.getMaterial().getUnidademedida());
								item.setNcmcapitulo(cm.getMaterial().getNcmcapitulo());
								item.setNcmcompleto(cm.getMaterial().getNcmcompleto());
								item.setNve(cm.getMaterial().getNve());
								item.setPedidovendamaterial(itemServico.getPedidovendamaterial());
								
								if(cm.getValorunitario() != null && cm.getValorunitario() > 0d){
									item.setValorunitario(cm.getValorunitario());
								} else {
									item.setValorunitario(cm.getMaterial().getValorcusto() != null ? cm.getMaterial().getValorcusto() : 0d);
								}
								if(devolucaoBool){
									item.setQtde(cm.getQuantidadedevolvida());
								} else {
									Double qtde;
									
									Double saldo = mapaSaldoMaterial.get(coletamaterialNotaBean);
									if (cm.getQuantidade()>=saldo){
										qtde = saldo;
										mapaSaldoMaterial.put(coletamaterialNotaBean, 0D);
									}else {
										qtde = cm.getQuantidade();
										mapaSaldoMaterial.put(coletamaterialNotaBean, saldo - cm.getQuantidade());											
									}
									
									if (qtde<0){
										qtde = 0D;
									}
									item.setQtde(qtde);
								}
								item.setValorbruto(new Money(item.getQtde() * item.getValorunitario()));
								item.setLocalarmazenagem(cm.getLocalarmazenagem());
								item.setInfoadicional(cm.getObservacao());
								item.setMaterialclasse(Materialclasse.PRODUTO);
								item.setIncluirvalorprodutos(Boolean.TRUE);
								
								item.setInformacaoAdicionalProdutoBean(criaBeanInfoProduto(cm.getMaterial(), cm.getPneu(), cm, null, null, itemServico.getPedidovendamaterial(), coleta.getPedidovenda()));
								String templateInfoAdicionalItem = montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
								if(templateInfoAdicionalItem != null){
									item.setInfoadicional(templateInfoAdicionalItem);
									item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
								}else {
									item.setInfoAdicionalItemAnterior("");
								}
								
								if (item.getQtde()!=null && item.getQtde()>0D){
									bean.getListaItens().add(item);
								}
							}	
						}
					}						
				}
				
				bean.setMotivodevolucaocoleta(coleta.getMotivodevolucao());
			}
			
			if (bean.getListaItens().isEmpty()){
				return null;
			}
			
			setTotalNota(bean);
		}
		
		for(Notafiscalprodutoitem item : bean.getListaItens()){
			if(item.getMaterial() != null){
				Material beanMaterial = materialService.loadForTributacao(item.getMaterial());
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
												bean.getEmpresa(), 
												bean.getNaturezaoperacao(), 
												beanMaterial.getMaterialgrupo(),
												bean.getCliente(),
												true, 
												beanMaterial,
												item.getNcmcompleto(),
												bean.getEnderecoCliente(), 
												null,
												listaCategoriaCliente,
												null,
												bean.getOperacaonfe(),
												bean.getModeloDocumentoFiscalEnum(),
												bean.getLocaldestinonfe(),
												bean.getDtEmissao()));
				item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
				item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
			}
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, bean.getEmpresa(), bean.getCliente(), bean.getEmpresa() != null ? bean.getEmpresa().getEndereco() : null, bean.getEnderecoCliente(), null);
		}
		
		if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf");
			
			if(empresa.getNaopreencherdtsaida() == null || !empresa.getNaopreencherdtsaida()){
				bean.setDatahorasaidaentrada(SinedDateUtils.currentTimestamp());
			}
			
			bean.setMarcavolume(empresa.getMarcanf());
			bean.setEspvolume(empresa.getEspecienf());
		}
		
		this.loadInfoForTemplateInfoContribuinte(bean);
		
		String informacoesContribuinte = montaInformacoesContribuinteTemplate(null, listaColetaOrdenada, bean);
		if(informacoesContribuinte != null){
			bean.setInfoadicionalcontrib(informacoesContribuinte);
		}else {
			adicionarInfNota(bean, null, bean.getEmpresa());
		}
		
		if (listaPresencacompradornfe.size()==1){
			bean.setPresencacompradornfe(new ListSet<Presencacompradornfe>(Presencacompradornfe.class, listaPresencacompradornfe).get(0));
		}
		
		return bean;
	}
	
	public void loadInfoForTemplateInfoContribuinte(Notafiscalproduto bean) {
		List<Notafiscalprodutoitem> listaItens = bean.getListaItens();
		if(listaItens != null){
			for (Notafiscalprodutoitem notafiscalprodutoitem : listaItens) {
				if(notafiscalprodutoitem.getMaterial() != null && notafiscalprodutoitem.getMaterial().getCdmaterial() != null){
					Material material_aux = materialService.loadWithMatrialgrupo(notafiscalprodutoitem.getMaterial());
					notafiscalprodutoitem.getMaterial().setMaterialgrupo(material_aux.getMaterialgrupo());
				}
			}
		}
	}
	
	/**
	 * Verifica se a nota necessita de c�lculo do ICMS Interestadual
	 *
	 * @param notafiscalproduto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/06/2016
	 */
	public boolean verificaDIFAL(Notafiscalproduto notafiscalproduto) {
		return this.verificaDIFAL(notafiscalproduto.getOperacaonfe(), notafiscalproduto.getLocaldestinonfe(), notafiscalproduto.getCliente(), notafiscalproduto.getEnderecoCliente());
	}
	
	public boolean verificaDIFAL(CalculoDIFALInterface calculoDIFALInterface) {
		return this.verificaDIFAL(calculoDIFALInterface.getOperacaonfe(), calculoDIFALInterface.getLocaldestinonfe(), calculoDIFALInterface.getCliente(), calculoDIFALInterface.getEnderecoCliente());
	}
	
	public boolean verificaDIFAL(Operacaonfe operacaonfe, Localdestinonfe localdestinonfe, Cliente cliente, Endereco enderecoCliente) {
		if(cliente != null && operacaonfe != null && localdestinonfe != null){
			String ieDest = enderecoCliente != null && enderecoCliente.getInscricaoestadual() != null && !enderecoCliente.getInscricaoestadual().trim().equals("") ? enderecoCliente.getInscricaoestadual() : cliente.getInscricaoestadual();
			Contribuinteicmstipo contribuinteicmstipo = cliente.getContribuinteicmstipo();
			
			if(contribuinteicmstipo == null && (ieDest == null || ieDest.trim().equals(""))){
				contribuinteicmstipo = Contribuinteicmstipo.NAO_CONTRIBUINTE;
			}
			
			boolean verificacaoContribuinte = contribuinteicmstipo != null && contribuinteicmstipo.equals(Contribuinteicmstipo.NAO_CONTRIBUINTE);
			boolean verificacaoOperacao = operacaonfe != null && operacaonfe.equals(Operacaonfe.CONSUMIDOR_FINAL);
			boolean verificacaoLocaldestino = localdestinonfe != null && localdestinonfe.equals(Localdestinonfe.INTERESTADUAL);
			if(verificacaoContribuinte && verificacaoOperacao && verificacaoLocaldestino){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/06/2016
	 */
	public List<Notafiscalproduto> findForDIFALVerificacao(String whereIn) {
		return notafiscalprodutoDAO.findForDIFALVerificacaoConferencia(whereIn, false);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/06/2016
	 */
	public List<Notafiscalproduto> findForDIFALConferencia(String whereIn) {
		return notafiscalprodutoDAO.findForDIFALVerificacaoConferencia(whereIn, true);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param notafiscalproduto
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	public void updateICMSInterestadual(Notafiscalproduto notafiscalproduto) {
		notafiscalprodutoDAO.updateICMSInterestadual(notafiscalproduto);
	}
	
	/**
	 * Calcula o valor total da nota fiscal; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Marden Silva
	 */	
	@SuppressWarnings("unchecked")
	public void ajaxCalculaValorTotal(WebRequestContext request){
		Object attr = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
		if (attr != null) {
			listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) attr;
		}
		String cdpessoa = request.getParameter("cdEmpresa");
		Empresa empresa = null;
		if (cdpessoa != null && !cdpessoa.isEmpty()){
			empresa = new Empresa();
			empresa.setCdpessoa(Integer.parseInt(cdpessoa));
			empresa = empresaService.load(empresa, "empresa.calcularQuantidadeTransporte");
		}
		
		String finalidade = request.getParameter("finalidadenfe");
		Finalidadenfe finalidadenfe = null;
		if(finalidade != null && !finalidade.isEmpty()){
			try{
				finalidadenfe = Finalidadenfe.valueOf(finalidade);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		AjaxCalculaTotalValorNotaBean bean = calculaTotalNota(finalidadenfe, listaNotafiscalprodutoitem);
		
		View.getCurrent().println("var haveServico = '" + bean.isHaveServico() + "';");
		//View.getCurrent().println("var localdestinonfe = '" + (bean.getLocaldestinonfe() != null ? bean.getLocaldestinonfe().name() : "<null>") + "';");
		
		View.getCurrent().println("var totalTributos = '" + bean.getTotalTributos() + "';");
		View.getCurrent().println("var totalImpostoFederal = '" + bean.getTotalImpostoFederal() + "';");
		View.getCurrent().println("var totalImpostoEstadual = '" + bean.getTotalImpostoEstadual() + "';");
		View.getCurrent().println("var totalImpostoMunicipal = '" + bean.getTotalImpostoMunicipal() + "';");
		View.getCurrent().println("var totalBcicms = '" + bean.getTotalBcicms() + "';");
		View.getCurrent().println("var totalIcms = '" + bean.getTotalIcms() + "';");
		View.getCurrent().println("var totalBcicmsst = '" + bean.getTotalBcicmsst() + "';");
		View.getCurrent().println("var totalIcmsst = '" + bean.getTotalIcmsst() + "';");
		View.getCurrent().println("var totalIi = '" + bean.getTotalIi() + "';");
		View.getCurrent().println("var totalIpi = '" + bean.getTotalIpi() + "';");
		View.getCurrent().println("var totalPis = '" + bean.getTotalPis() + "';");
		View.getCurrent().println("var totalCofins = '" + bean.getTotalCofins() + "';");
		View.getCurrent().println("var totalServicos = '" + bean.getTotalServicos() + "';");
		View.getCurrent().println("var totalBciss = '" + bean.getTotalBciss() + "';");
		View.getCurrent().println("var totalIss = '" + bean.getTotalIss() + "';");
		View.getCurrent().println("var totalPisservicos = '" + bean.getTotalPisservicos() + "';");
		View.getCurrent().println("var totalCofinsservicos = '" + bean.getTotalCofinsservicos() + "';");
		View.getCurrent().println("var totalIcmsdesonerado = '" + bean.getTotalIcmsdesonerado() + "';");
		View.getCurrent().println("var totaldeducaoservico = '" + bean.getTotaldeducaoservico() + "';");
		View.getCurrent().println("var totaloutrasretencoesservico = '" + bean.getTotaloutrasretencoesservico() + "';");
		View.getCurrent().println("var totaldescontocondicionadoservico = '" + bean.getTotaldescontocondicionadoservico() + "';");
		View.getCurrent().println("var totaldescontoincondicionadoservico = '" + bean.getTotaldescontoincondicionadoservico() + "';");
		View.getCurrent().println("var totalissretido = '" + bean.getTotalissretido() + "';");
		View.getCurrent().println("var totalBccsll = '" + bean.getTotalBccsll() + "';");
		View.getCurrent().println("var totalCsll = '" + bean.getTotalCsll() + "';");
		View.getCurrent().println("var totalBcir = '" + bean.getTotalBcir() + "';");
		View.getCurrent().println("var totalIr = '" + bean.getTotalIr() + "';");
		View.getCurrent().println("var totalBcinss = '" + bean.getTotalBcinss() + "';");
		View.getCurrent().println("var totalInss = '" + bean.getTotalInss() + "';");
		
		View.getCurrent().println("var totalFcp = '" + bean.getTotalFcp() + "';");
		View.getCurrent().println("var totalFcpst = '" + bean.getTotalFcpst() + "';");
		View.getCurrent().println("var totalFcpstretidoanteriormente = '" + bean.getTotalFcpstretidoanteriormente() + "';");
		View.getCurrent().println("var totalIpidevolvido = '" + bean.getTotalIpidevolvido() + "';");

		View.getCurrent().println("var totalNota = '" + bean.getTotalNota() + "';");
		View.getCurrent().println("var totalNaoincluido = '" + bean.getTotalNaoincluido() + "';");
		View.getCurrent().println("var totalProdutos = '" + bean.getTotalProdutos() + "';");
		View.getCurrent().println("var totalFrete = '" + bean.getTotalFrete() + "';");		
		View.getCurrent().println("var totalDesconto = '" + bean.getTotalDesconto() + "';");
		View.getCurrent().println("var totalSeguro = '" + bean.getTotalSeguro() + "';");
		View.getCurrent().println("var totalOutrasdespesas = '" + bean.getTotalOutrasdespesas() + "';");
		
		if (empresa == null || empresa.getCalcularQuantidadeTransporte() == null || empresa.getCalcularQuantidadeTransporte()){
			View.getCurrent().println("var qtdeTotal = '" + (bean.getQtdeTotal() > 0 ? bean.getQtdeTotal().intValue() : "") + "';");	
		}
		View.getCurrent().println("var pesobruto = '" + (bean.getPesobruto() > 0 ? SinedUtil.descriptionDecimal(bean.getPesobruto()) : "") + "';");	
		View.getCurrent().println("var pesoliquido = '" + (bean.getPesoliquido() > 0 ? SinedUtil.descriptionDecimal(bean.getPesoliquido()) : "") + "';");
		View.getCurrent().println("var totalNotaReceita = '" + bean.getTotalNotaReceita() + "';");
	}
	
	/**
	* M�todo que verifica se o tipo de conbran�a do imposto n�o est� preenchido, para que possa ser zerado o valor do imposto
	*
	* @param notafiscalprodutoitem
	* @since 11/11/2016
	* @author Luiz Fernando
	*/
	public void verificaBaseCalculoTipoCobran�a(Notafiscalprodutoitem notafiscalprodutoitem) {
		if(notafiscalprodutoitem != null){
			if(notafiscalprodutoitem.getTipocobrancaicms() == null){
				notafiscalprodutoitem.setValorbcicms(new Money());
				notafiscalprodutoitem.setIcms(null);
				notafiscalprodutoitem.setValoricms(new Money());
			}
			if(notafiscalprodutoitem.getTipocobrancaipi() == null){
				notafiscalprodutoitem.setValorbcipi(new Money());
				notafiscalprodutoitem.setIpi(null);
				notafiscalprodutoitem.setValoripi(new Money());
			}
			if(notafiscalprodutoitem.getTipocobrancapis() == null){
				notafiscalprodutoitem.setValorbcpis(new Money());
				notafiscalprodutoitem.setPis(null);
				notafiscalprodutoitem.setValorpis(new Money());
			}
			if(notafiscalprodutoitem.getTipocobrancacofins() == null){
				notafiscalprodutoitem.setValorbccofins(new Money());
				notafiscalprodutoitem.setCofins(null);
				notafiscalprodutoitem.setValorcofins(new Money());
			}
		}
	}
	
	public List<Notafiscalproduto> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String whereNotIn, String whereInNaturezaOperacao){
		return notafiscalprodutoDAO.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, whereNotIn, whereInNaturezaOperacao);
	}
	
	public String montaInformacoesContribuinteTemplate(Venda venda, Coleta coleta, Notafiscalproduto bean){
		List<Venda> listaVenda = new ArrayList<Venda>();
		if(venda != null) listaVenda.add(venda);
		
		List<Coleta> listaColeta = new ArrayList<Coleta>();
		if(coleta != null) listaColeta.add(coleta);
		
		return montaInformacoesContribuinteTemplate(listaVenda, listaColeta, bean);
	}
	
	public String montaInformacoesContribuinteTemplate(String whereInVenda, String whereInColeta, Notafiscalproduto bean){
		List<Venda> listaVenda = new ArrayList<Venda>();
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVenda)){
			for(String id : whereInVenda.split(",")){
				listaVenda.add(new Venda(Integer.parseInt(id)));
			}
		}
		
		List<Coleta> listaColeta = new ArrayList<Coleta>();
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInColeta)){
			for(String id : whereInColeta.split(",")){
				listaColeta.add(new Coleta(Integer.parseInt(id)));
			}
		}
		
		return montaInformacoesContribuinteTemplate(listaVenda, listaColeta, bean);
	}
	
	public String montaInformacoesContribuinteTemplate(List<Venda> listaVenda, List<Coleta> listaColeta, Notafiscalproduto bean){
		try {
			if(bean.getNaturezaoperacao() == null){
				return null;
			}
			bean.setNaturezaoperacao(naturezaoperacaoService.load(bean.getNaturezaoperacao()));
	
			if(bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getTemplateinfcontribuinte() != null){
				Cliente cliente = bean.getCliente();
				
				List<Venda> listaVendas = new ArrayList<Venda>();
				if(SinedUtil.isListNotEmpty(listaVenda)){
					listaVendas = vendaService.findForCobranca(CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ","));
				}
				List<Coleta> listaColetas = new ArrayList<Coleta>();
				if(SinedUtil.isListNotEmpty(listaColeta)){
					listaColetas = coletaService.loadForGerarNotafiscalproduto(CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ","));
				}
				
	    		ReportTemplateBean reportTemplate = reportTemplateService.load(bean.getNaturezaoperacao().getTemplateinfcontribuinte());
	    		ITemplateEngine engine = new GroovyTemplateEngine().build(reportTemplate.getLeiaute());
	    		InformacaoContribuinteNfeBean beanInfo = new InformacaoContribuinteNfeBean();
	    		
	    		LinkedList<NotaItemBean> itensNota = new LinkedList<NotaItemBean>();
	    		for(Notafiscalprodutoitem item: bean.getListaItens()){
	    			NotaItemBean itemNota = new NotaItemBean();
	    			itemNota.setAliquotaCreditoIcms(item.getAliquotacreditoicms()==null?0:item.getAliquotacreditoicms());
	    			itemNota.setCfop(item.getCfop()!=null? item.getCfop().getCodigo(): "");
	    			itemNota.setQuantidade(item.getQtde());
	    			itemNota.setValorBruto(item.getValorbruto()!=null? item.getValorbruto().getValue().doubleValue(): 0.0);
	    			itemNota.setTipoTributacaoIcms(item.getTipotributacaoicms() != null? item.getTipotributacaoicms().getDescricao(): "");
	    			itemNota.setTipoCobrancaIcms(item.getTipocobrancaicms()!=null?item.getTipocobrancaicms().getCdnfe():"");
	    			itemNota.setCdmaterial(item.getMaterial() != null ? item.getMaterial().getCdmaterial() : null);
	    			itemNota.setCdmaterialgrupo(item.getMaterial() != null && item.getMaterial().getMaterialgrupo() != null ? item.getMaterial().getMaterialgrupo().getCdmaterialgrupo() : null);
	    			itemNota.setNcmcompleto(item.getNcmcompleto() != null ? item.getNcmcompleto() : "");
	    			
	    			itensNota.add(itemNota);
	    		}
	    		
				Map<String,Object> datasource = new HashMap<String, Object>();
				
				Venda venda = SinedUtil.isListNotEmpty(listaVendas) ? listaVendas.iterator().next() : null;
				Coleta coleta = SinedUtil.isListNotEmpty(listaColetas) ? listaColetas.iterator().next() : null;
	
				cliente = cliente != null ? clienteService.loadForInfoContribuinte(cliente): null;
				ClienteBean clientereportBean = new ClienteBean(cliente);
				
				GrupoTributacaoBean grupotributacaoreportBean = new GrupoTributacaoBean();
				grupotributacaoreportBean.setInformacoesadicionaiscontribuinte(montaInfoContribuinteGruposDeTributacaoDosItens(bean));
				
				Empresa empresa = bean.getEmpresa();
				if(empresa == null){
					if(venda != null && venda.getEmpresa() != null){
						empresa = venda.getEmpresa();
					}else if(coleta != null && coleta.getEmpresa() != null){
						empresa = coleta.getEmpresa();
					}
				}
				
				empresaService.setInformacoesPropriedadeRural(empresa);
				
				EmpresaBean empresareportBean = empresa != null ? new EmpresaBean(empresaService.loadForInfoContribuinte(empresa)) : new EmpresaBean(null);
				
				NaturezaOperacaoBean naturezaoperacaoreportBean = new NaturezaOperacaoBean(bean.getNaturezaoperacao());
				
				LinkedList<VendaInfoContribuinteBean> listaBeanVendas = new LinkedList<VendaInfoContribuinteBean>();
				if(SinedUtil.isListNotEmpty(listaVendas)){
					Colaboradorcargo colaboradorcargo;
					for(Venda v: listaVendas){
						colaboradorcargo = null;
						if(v.getColaborador() != null){
							colaboradorcargo = colaboradorcargoService.findCargoAtual(v.getColaborador());
						}
						listaBeanVendas.add(new VendaInfoContribuinteBean(v, colaboradorcargo));				
					}
				}
				
				LinkedList<ColetaInfoContribuinteBean> listaBeanColetas = new LinkedList<ColetaInfoContribuinteBean>();
				if(SinedUtil.isListNotEmpty(listaColetas)){
					for(Coleta c: listaColetas){
						listaBeanColetas.add(new ColetaInfoContribuinteBean(c, 
								notafiscalprodutoColetaService.getWhereInNotaColeta(bean, c.getCdcoleta().toString()),
								entregadocumentoColetaService.getWhereInNotaColeta(c.getCdcoleta().toString())));				
					}
				}
				
				beanInfo.setCliente(clientereportBean);
				beanInfo.setEmpresa(empresareportBean);
				beanInfo.setGrupoTributacao(grupotributacaoreportBean);
				beanInfo.setNaturezaoperacao(naturezaoperacaoreportBean);
				beanInfo.setVendas(listaBeanVendas);
				beanInfo.setColetas(listaBeanColetas);
				beanInfo.setItensnota(itensNota);
				beanInfo.setValorTotalDesoneracao(bean.getValordesoneracao());
				
				datasource.put("bean", beanInfo);
				String retorno = engine.make(datasource);
				if(retorno != null && retorno.length() > 5000){
					retorno = retorno.substring(0, 4999);
				}
				return retorno;
	    	}
	    	return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String montaInfoContribuinteGruposDeTributacaoDosItens(Notafiscalproduto nota){
		StringBuilder retorno = new StringBuilder();
		if(SinedUtil.isListNotEmpty(nota.getListaItens())){
			Integer qtdeInfoDiferentes = 1;
			
			for(Notafiscalprodutoitem item: nota.getListaItens()){
				if(item.getGrupotributacao() != null && org.apache.commons.lang.StringUtils.isNotBlank(item.getGrupotributacao().getInfoadicionalcontrib()) &&
							!retorno.toString().contains(item.getGrupotributacao().getInfoadicionalcontrib())){
					retorno.append(item.getGrupotributacao().getInfoadicionalcontrib());
					if(qtdeInfoDiferentes > 1){
						retorno.append("\n");
					}
					qtdeInfoDiferentes++;
				}
			}
		}
		return retorno.toString();
	}
	
	/**
	* M�todo que carrega a conta da empresa
	*
	* @param request
	* @param nota
	* @throws Exception
	* @since 16/01/2017
	* @author Luiz Fernando
	*/
	public void carregaContaByEmpresa(WebRequestContext request, Nota nota) throws Exception{
		Empresa empresa = nota.getEmpresa() != null ? nota.getEmpresa() : empresaService.loadPrincipal();
		empresa = empresaService.loadWithContaECarteira(empresa);
	
		Integer cdconta = empresa != null && empresa.getContabancariacontareceber() != null ? empresa.getContabancariacontareceber().getCdconta() : 0;
		Integer cdcontacarteira = empresa != null && empresa.getContacarteiracontareceber() != null ? empresa.getContacarteiracontareceber().getCdcontacarteira() : 0;
		
		if(empresa != null && empresa.getContabancariacontareceber() == null && nota.getConta() != null && nota.getConta().getCdconta() != null){
			cdconta = nota.getConta().getCdconta();
		}
		
		List<Conta> listaConta = contaService.findContaBoletoByEmpresa(empresa);
		List<Contacarteira> listaContacarteira = new ListSet<Contacarteira>(Contacarteira.class);
		for(Conta conta : listaConta){
			if(conta.getCdconta().equals(cdconta) && conta.getListaContacarteiraQueGeraBoleto() != null){
				listaContacarteira = conta.getListaContacarteiraQueGeraBoleto();
			}
		}
		
		View.getCurrent().convertObjectToJs(listaConta, "lista");
		View.getCurrent().convertObjectToJs(listaContacarteira, "listaContacarteira");
		View.getCurrent().println("var cdconta = " + cdconta + ";");
		View.getCurrent().println("var cdcontacarteira = " + cdcontacarteira + ";");
	}
	
	/**
	* M�todo que gera as duplicatas da nota
	*
	* @param request
	* @param bean
	* @return
	* @since 16/01/2017
	* @author Luiz Fernando
	*/
	public ModelAndView parcelasCobranca(WebRequestContext request, ParcelasCobrancaBean bean){	
		return new JsonModelAndView().addObject("bean", gerarParcelasCobranca(bean));
	}
	
	public List<Notafiscalprodutoduplicata> gerarParcelasCobranca(Notafiscalproduto nota, boolean calculoVenda, Integer qtdeParcelas){	
		ParcelasCobrancaBean bean = new ParcelasCobrancaBean();
		bean.setPrazopagamento(nota.getPrazopagamentofatura());
		bean.setData(nota.getDtEmissao());
		bean.setValor(nota.getValorliquidofaturaTransient() != null && nota.getValorliquidofaturaTransient().getValue().doubleValue() > 0 ? nota.getValorliquidofaturaTransient() : nota.getValorliquidofatura());
		bean.setEmpresa(nota.getEmpresa());
		
		Venda venda = new Venda();
		if(calculoVenda){
			venda.setPrazopagamento(bean.getPrazopagamento());
			venda.setValor(bean.getValor() != null ? bean.getValor().getValue().doubleValue() : 0d);
			venda.setQtdeParcelas(qtdeParcelas);
			venda.setDtvenda(bean.getData());
			venda.setEmpresa(bean.getEmpresa());
		}
		
		List<Notafiscalprodutoduplicata> listaDuplicata = null;
		if(bean.getPrazopagamento() != null && bean.getData() != null && bean.getValor() != null && bean.getValor().getValue().doubleValue() > 0 && bean.getEmpresa() != null){
			AjaxRealizarVendaPagamentoBean ajaxRealizarVendaPagamentoBean = null;
			if(calculoVenda){
				ajaxRealizarVendaPagamentoBean = vendaService.pagamento(venda, null);
			}else {
				ajaxRealizarVendaPagamentoBean = gerarParcelasCobranca(bean);
			}
			if(ajaxRealizarVendaPagamentoBean != null && SinedUtil.isListNotEmpty(ajaxRealizarVendaPagamentoBean.getLista())){
				listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
				Notafiscalprodutoduplicata duplicata;
				for(AjaxRealizarVendaPagamentoItemBean itemBean : ajaxRealizarVendaPagamentoBean.getLista()){
					duplicata = new Notafiscalprodutoduplicata();
					duplicata.setDocumentotipo(nota.getDocumentotipo());
					duplicata.setDtvencimento(itemBean.getDtparceladuplicata());
					duplicata.setValor(itemBean.getValorduplicata());
					
					listaDuplicata.add(duplicata);
				}
			}
		}
		
		return listaDuplicata;
	}
	
	public AjaxRealizarVendaPagamentoBean gerarParcelasCobranca(ParcelasCobrancaBean bean){
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(bean.getPrazopagamento());
		Double juros = prazopagamentoService.loadAll(bean.getPrazopagamento()).getJuros();
		AjaxRealizarVendaPagamentoBean b = new AjaxRealizarVendaPagamentoBean();
		Date dtreferencia = bean.getData();
		AjaxRealizarVendaPagamentoItemBean item;
		int qtdeParcelas = 0;
		
		if (bean.getQtdParcela() != null && !bean.getQtdParcela().equals(listaPagItem.size())) {
			List<Prazopagamentoitem> listaPrazoPagItem = new ArrayList<Prazopagamentoitem>();
			
			for (int i = 0; i < bean.getQtdParcela(); i++) {
				listaPrazoPagItem.add(new Prazopagamentoitem(i, listaPagItem.get(0).getDias() * i, null));
			}
			
			listaPagItem = listaPrazoPagItem;
			qtdeParcelas = bean.getQtdParcela();
		} else {
			qtdeParcelas = listaPagItem.size();
		}
		
		b.setParcela(qtdeParcelas);
		
		Money valor;
		Date dtparcela;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Prazopagamento pg = prazopagamentoService.load(bean.getPrazopagamento(), "prazopagamento.cdprazopagamento, prazopagamento.dataparceladiautil, prazopagamento.dataparcelaultimodiautilmes");
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();
		
		for (Prazopagamentoitem it : listaPagItem) {
			item = new AjaxRealizarVendaPagamentoItemBean();
			
			if(it.getDias() != null && it.getDias() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getDias().intValue(), Calendar.DAY_OF_MONTH);
			} else if(it.getMeses() != null && it.getMeses() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getMeses().intValue(), Calendar.MONTH);
			} else {
				dtparcela = dtreferencia;
			}
			
			if(pg.getDataparceladiautil() != null && pg.getDataparceladiautil()){
				dtparcela = SinedDateUtils.getProximaDataUtil(new java.sql.Date(dtparcela.getTime()), bean.getEmpresa(), pg.getDataparcelaultimodiautilmes());
			}
			item.setDtparceladuplicata(dtparcela);
			item.setDtparcela(formatador.format(dtparcela));
			
			if(juros == null || juros.equals(0d)){			  
				Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(bean.getValor().getValue().doubleValue() / b.getParcela()), 2).doubleValue();
				valor = new Money(valorTruncado); 
			}else{
				valor = FluxoStatusContaProcess.calculaJuros(bean.getValor(), b.getParcela(), juros);
			}
			item.setValorduplicata(valor);
			item.setValor(valor.toString());
			
			lista.add(item);
		}
		
		vendaService.verificaDiferencaTotal(bean.getValor().getValue().doubleValue(), lista, qtdeParcelas, juros);
		
		b.setLista(lista);
		
		return b;
	}
	
	/**
	* M�todo que busca informa��es do prazo de pagamento
	*
	* @param request
	* @param bean
	* @throws Exception
	* @since 16/01/2017
	* @author Luiz Fernando
	*/
	public void buscarInfPrazopagamento(WebRequestContext request, Prazopagamento bean) throws Exception{
		boolean parcelaComJuros = false;
		if(bean != null){
			Prazopagamento prazopagamento = prazopagamentoService.load(bean, "prazopagamento.parcelasiguaisjuros, prazopagamento.considerarjurosnota");
			parcelaComJuros = prazopagamento != null && 
					prazopagamento.getParcelasiguaisjuros() != null && prazopagamento.getParcelasiguaisjuros();
		}
		View.getCurrent().println("var parcelaComJuros = " + parcelaComJuros + ";");
	}
	
	public void calcularTributacaoNota(Notafiscalproduto bean) {
		if(bean != null && SinedUtil.isListNotEmpty(bean.getListaItens())){
			Cliente cliente = bean.getCliente();
			Material material;
			Endereco enderecoempresa = null;
			if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
				enderecoempresa = enderecoService.carregaEnderecoEmpresa(bean.getEmpresa());
			}
			for(Notafiscalprodutoitem item : bean.getListaItens()){
				Material beanMaterial = null;
				if(item.getMaterial() != null){
					material = item.getMaterial();
					List<Categoria> listaCategoriaCliente = null;
					if(cliente != null){
						listaCategoriaCliente = categoriaService.findByPessoa(cliente);
					}
					
					beanMaterial = materialService.loadForTributacao(material);
					List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
							grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
													bean.getEmpresa(), 
													bean.getNaturezaoperacao(), 
													beanMaterial.getMaterialgrupo(),
													cliente,
													true, 
													beanMaterial,
													item.getNcmcompleto(),
													bean.getEnderecoCliente(), 
													null,
													listaCategoriaCliente,
													null,
													bean.getOperacaonfe(),
													bean.getModeloDocumentoFiscalEnum(),
													bean.getLocaldestinonfe(),
													bean.getDtEmissao()));
					item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					item.getMaterial().setGrupotributacaoNota(item.getGrupotributacao());
				}
				item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, bean.getEmpresa(), bean.getCliente(), enderecoempresa, bean.getEnderecoCliente(), null);
				
				setBaseCalculoItem(item.getMaterial(), item);
				verificaBaseCalculoTipoCobran�a(item);
				item.setOperacaonfe(bean.getOperacaonfe());
				grupotributacaoService.calcularValoresFormulaIpi(item, item.getGrupotributacao() != null ? item.getGrupotributacao() : item.getMaterial().getGrupotributacaoNota());
				grupotributacaoService.calcularValoresFormulaIcms(item, item.getGrupotributacao() != null ? item.getGrupotributacao() : item.getMaterial().getGrupotributacaoNota());
				calculaMva(item, item.getMaterial());
				grupotributacaoService.calcularValoresFormulaIcmsST(item, item.getGrupotributacao() != null ? item.getGrupotributacao() : item.getMaterial().getGrupotributacaoNota());
				if(beanMaterial != null) item.getMaterial().setPercentualimpostoecf(beanMaterial.getPercentualimpostoecf());
				item.setPercentualimpostoecf(getPercentualImposto(bean.getEmpresa(), item, null));
				item.setValortotaltributos(getValorAproximadoImposto(bean.getEmpresa(), bean.getNaturezaoperacao(), item, null));
				
				item.setPercentualImpostoFederal(getPercentualImposto(bean.getEmpresa(), item, ImpostoEcfEnum.FEDERAL));
				item.setPercentualImpostoEstadual(getPercentualImposto(bean.getEmpresa(), item, ImpostoEcfEnum.ESTADUAL));
				item.setPercentualImpostoMunicipal(getPercentualImposto(bean.getEmpresa(), item, ImpostoEcfEnum.MUNICIPAL));
				
				item.setValorImpostoFederal(this.getValorAproximadoImposto(bean.getEmpresa(), bean.getNaturezaoperacao(), item, ImpostoEcfEnum.FEDERAL));
				item.setValorImpostoEstadual(this.getValorAproximadoImposto(bean.getEmpresa(), bean.getNaturezaoperacao(), item, ImpostoEcfEnum.ESTADUAL));
				item.setValorImpostoMunicipal(this.getValorAproximadoImposto(bean.getEmpresa(), bean.getNaturezaoperacao(), item, ImpostoEcfEnum.MUNICIPAL));
			}
			
			adicionarInfNota(bean, null, bean.getEmpresa());
		}
	}
	
	public  List<Notafiscalproduto> findNotaWithNotaForAutocomplete(String param) {
		String cdempresa = null;
		List<Notafiscalproduto> listaNotas = notafiscalprodutoDAO.findNotaWithNotaForAutocomplete(param, cdempresa);
//		if (SinedUtil.isListNotEmpty(listaNotas)){
//			for (Iterator<Notafiscalproduto> it = listaNotas.iterator(); it.hasNext(); ) {
//				 Notafiscalproduto nota = it.next();
//				 List<Expedicaoitem> listaItem = expedicaoitemService.findByWhereInNota(nota.getCdNota().toString(), false);
//				 if(SinedUtil.isListNotEmpty(listaItem)){
//					 listaNotas.remove(nota);
//				 }
//			}
//		}
		return listaNotas;
	}
	
	public  List<Notafiscalproduto> findNotaWithVendaForAutocomplete(String param) {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {
		}
		
		return notafiscalprodutoDAO.findNotaWithVendaForAutocomplete(param, cdempresa);
	}
	
	public void setPrazogapamentoUnico(Notafiscalproduto nfp) {
		if(nfp != null && nfp.getPrazopagamentofatura() == null && Hibernate.isInitialized(nfp.getListaDuplicata()) && 
				nfp.getListaDuplicata() != null && nfp.getListaDuplicata().size() == 1){
			nfp.setPrazopagamentofatura(Prazopagamento.UNICA);
		}
	}
	
	public Map<ColetamaterialNotaBean, Double> getMapaSaldoMaterial(Integer cdcoleta, List<ColetaMaterial> listaColetaMaterial, Tipooperacaonota tipooperacaonota){
		Map<ColetamaterialNotaBean, Double> mapaSaldoMaterial = new HashMap<ColetamaterialNotaBean, Double>();
		
		if (listaColetaMaterial!=null){
			Map<ColetamaterialNotaBean, Double> mapaQtdeMaterial = new HashMap<ColetamaterialNotaBean, Double>();
			ColetamaterialNotaBean coletamaterialNotaBean;
			
			for(ColetaMaterial cm : listaColetaMaterial){
				if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
					ColetaMaterialPedidovendamaterial itemServico = cm.getListaColetaMaterialPedidovendamaterial().get(0);
					Integer cdpedidovendamaterial = itemServico.getPedidovendamaterial() != null ? 
							itemServico.getPedidovendamaterial().getCdpedidovendamaterial() : null;
						
//					coletamaterialNotaBean = new ColetamaterialNotaBean(cm.getMaterial().getCdmaterial(), cm.getPedidovendamaterial() != null ? cm.getPedidovendamaterial().getCdpedidovendamaterial() : null);
					coletamaterialNotaBean = new ColetamaterialNotaBean(cm.getMaterial().getCdmaterial(), cdpedidovendamaterial);
					
					Double qtde = mapaQtdeMaterial.get(coletamaterialNotaBean);
					if (qtde == null){
						qtde = 0D;
					}
					qtde += cm.getQuantidade();
					mapaQtdeMaterial.put(coletamaterialNotaBean, qtde);
				}
			}
			
			for (ColetamaterialNotaBean bean: mapaQtdeMaterial.keySet()){
				mapaSaldoMaterial.put(bean, mapaQtdeMaterial.get(bean) - coletaService.getQuantidadeItemNota(cdcoleta, bean.getCdmaterial(), bean.getCdpedidovendamaterial(), tipooperacaonota));
			}
		}
		
		return mapaSaldoMaterial;
	}
	
	public Map<ColetamaterialNotaBean, Double> getMapaSaldoMaterial(List<EntregadocumentoColeta> lista, Tipooperacaonota tipooperacaonota){
		Map<ColetamaterialNotaBean, Double> mapaSaldoMaterialColeta = new HashMap<ColetamaterialNotaBean, Double>();
		
		if(SinedUtil.isListNotEmpty(lista)){
			for(EntregadocumentoColeta edc : lista){
				if(edc.getColeta() != null){
					Map<ColetamaterialNotaBean, Double> mapaSaldoMaterial = getMapaSaldoMaterial(edc.getColeta().getCdcoleta(), edc.getColeta().getListaColetaMaterial(), tipooperacaonota);
					mapaSaldoMaterialColeta.putAll(mapaSaldoMaterial);
				}
			}
		}
		
		return mapaSaldoMaterialColeta;
	}
	
	public List<Rateioitem> getListaRateioitemFaturamento(Notafiscalproduto notafiscalproduto) {
		List<Notafiscalprodutoitem> lista = notafiscalprodutoitemService.findForRateioFaturamento(notafiscalproduto);
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		if(SinedUtil.isListNotEmpty(lista) && notavendaService.existNotaVenda(new Nota(notafiscalproduto.getCdNota()))){
			Vendamaterial vendamaterial;
			Empresa empresa;
			Contagerencial contagerencial;
			Centrocusto centrocusto;
			
			Money valorTotal = new Money();
			for (Notafiscalprodutoitem it : lista) {
				if(it.getValorbruto() != null){
					valorTotal = valorTotal.add(it.getValorbruto());
				}
			}
			
			for (Notafiscalprodutoitem it : lista) {
				vendamaterial = it.getVendamaterial();
				if(it.getValorbruto() == null) continue;
				
				empresa = it.getNotafiscalproduto() != null ? it.getNotafiscalproduto().getEmpresa() : null;
				
				Rateioitem rateioitem = new Rateioitem();
				
				contagerencial = empresa != null ? empresa.getContagerencial() : null;
				if (it.getMaterial() != null && it.getMaterial().getContagerencialvenda() != null && it.getMaterial().getContagerencialvenda().getCdcontagerencial() != null) {
					contagerencial = it.getMaterial().getContagerencialvenda();
				}
				rateioitem.setContagerencial(contagerencial);
				
				if (vendamaterial != null && vendamaterial.getVenda() != null && vendamaterial.getVenda().getCentrocusto() != null) {
					centrocusto = vendamaterial.getVenda().getCentrocusto();
				} else {
					centrocusto = empresa != null ? empresa.getCentrocusto() : null;
					if(it.getMaterial() != null && it.getMaterial().getCentrocustovenda() != null && it.getMaterial().getCentrocustovenda().getCdcentrocusto() != null) {
						centrocusto = it.getMaterial().getCentrocustovenda();
					}
				}

				rateioitem.setCentrocusto(centrocusto);
				rateioitem.setProjeto(vendamaterial != null && vendamaterial.getVenda() != null ? vendamaterial.getVenda().getProjeto() : null);
				rateioitem.setValor(it.getValorbruto());
				rateioitem.setPercentual(0d);
				if (valorTotal.getValue().doubleValue() > 0) {
					rateioitem.setPercentual(it.getValorbruto().getValue().doubleValue() / valorTotal.getValue().doubleValue() * 100);
				}
				
				listaRateioitem.add(rateioitem);
			}
		}
		
		return listaRateioitem;
	}
	
	public String validaDivergenciaValoresParcela(WebRequestContext request){
		request.setAttribute("tipoNota", "produto");
		return notaService.validaDivergenciaValoresParcela(request);
	}
	
	public Notafiscalproduto gerarNotaFiscalRetorno(WebRequestContext request, Notafiscalproduto bean, String whereInColeta, String whereInPedidovendaRetorno, String whereInVendaRetorno, boolean devolucaoBool, Set<Presencacompradornfe> listaPresencacompradornfe, boolean geracaoAutomarica) throws IOException{
		if(bean == null) bean = new Notafiscalproduto();
		if(listaPresencacompradornfe == null) listaPresencacompradornfe = new HashSet<Presencacompradornfe>();
		if(bean.getModeloDocumentoFiscalEnum() == null) bean.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		
		bean.setWhereInPedidovendaRetorno(whereInPedidovendaRetorno);
		bean.setWhereInVendaRetorno(whereInVendaRetorno);
		bean.setNotaStatus(NotaStatus.EMITIDA);
		
		String tipooperacaoNota = request.getParameter("tipooperacaoNota");
		if("ENTRADA".equals(tipooperacaoNota)){
			bean.setTipooperacaonota(Tipooperacaonota.ENTRADA);
		}else {
			bean.setTipooperacaonota(Tipooperacaonota.SAIDA);
		}
		
		if (coletaService.isColetaPossuiNotaTodosItensSemSaldo(whereInColeta, bean.getTipooperacaonota()) && !geracaoAutomarica){
			String complementoOtr = SinedUtil.complementoOtr();
			if(org.apache.commons.lang.StringUtils.isNotBlank(whereInPedidovendaRetorno)){
				request.getServletResponse().sendRedirect(request.getServletRequest().getContextPath() +  "/faturamento/crud/Pedidovenda"+complementoOtr+"?ACAO=msgNotaItensColeta");
			}else if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVendaRetorno)){
				request.getServletResponse().sendRedirect(request.getServletRequest().getContextPath() +  "/faturamento/crud/Venda"+complementoOtr+"?ACAO=msgNotaItensColeta");
			}else {
				request.getServletResponse().sendRedirect(request.getServletRequest().getContextPath() +  "/faturamento/crud/Coleta?ACAO=msgNotaItensColeta");
			}
		}				
		
		boolean devolucao = "true".equalsIgnoreCase(request.getParameter("devolucao"));
		boolean registrarNF = "true".equalsIgnoreCase(request.getParameter("registrarNF")) || geracaoAutomarica;
		
		Set<String> listaOrigemOperacao = new HashSet<String>();
		List<Coleta> listaColeta = coletaService.loadForGerarNotafiscalproduto(whereInColeta);
		if(listaColeta != null && !listaColeta.isEmpty()){
			if(bean.getListaItens() == null){
				bean.setListaItens(new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class));
			}
			Set<Empresa> listaEmpresas = new ListSet<Empresa>(Empresa.class);
			Set<Cliente> listaClientes = new ListSet<Cliente>(Cliente.class);
			Set<Fornecedor> listaTerceiro = new ListSet<Fornecedor>(Fornecedor.class);
			ListSet<Naturezaoperacao> listaNaturezaoperacao = new ListSet<Naturezaoperacao>(Naturezaoperacao.class);
			
			for(Coleta coleta : listaColeta){
				if(coleta.getEmpresa() != null){
					if(!listaEmpresas.contains(coleta.getEmpresa())){
						listaEmpresas.add(coleta.getEmpresa());
					}
				}else if(coleta.getPedidovenda() != null && 
						coleta.getPedidovenda().getEmpresa() != null && 
						!listaEmpresas.contains(coleta.getPedidovenda().getEmpresa())){
					listaEmpresas.add(coleta.getPedidovenda().getEmpresa());
				}
				if(coleta.getCliente() != null && !listaClientes.contains(coleta.getCliente())){
					listaClientes.add(coleta.getCliente());
				}
				if(coleta.getPedidovenda() != null && 
						coleta.getPedidovenda().getTerceiro() != null && 
						!listaTerceiro.contains(coleta.getPedidovenda().getTerceiro())){
					listaTerceiro.add(coleta.getPedidovenda().getTerceiro());
				}
				if (coleta.getPedidovenda()!=null
						&& coleta.getPedidovenda().getPedidovendatipo()!=null
						&& coleta.getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta()!=null){
					listaOrigemOperacao.add(coleta.getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta().getDescricao());
					listaPresencacompradornfe.add(coleta.getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta());
				}
				
				if (coleta.getPedidovenda()!=null
						&& coleta.getPedidovenda().getPedidovendatipo()!=null)
					if(Tipooperacaonota.ENTRADA.equals(bean.getTipooperacaonota())){
						if(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada()!=null && 
								!listaNaturezaoperacao.contains(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada())){
							listaNaturezaoperacao.add(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaonotafiscalentrada());
						}
					}else {
						if(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal()!=null && 
								!listaNaturezaoperacao.contains(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal())){
							listaNaturezaoperacao.add(coleta.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal());
						}
				}
			}
			
			if(listaEmpresas.size() == 1){
				bean.setEmpresa(listaEmpresas.iterator().next());
			}
			if(listaClientes.size() == 1){
				bean.setCliente(listaClientes.iterator().next());
				Cliente cliente = clienteService.carregarDadosCliente(bean.getCliente());
				if(cliente != null && SinedUtil.isListNotEmpty(cliente.getListaEndereco())){
					Endereco enderecoCliente = cliente.getEndereco();
					if(enderecoCliente != null){
						bean.setEnderecoCliente(enderecoCliente);
					}
				}	
				
			}
			if(bean.getCadastrartransportador() != null && bean.getCadastrartransportador() && listaTerceiro.size() == 1){
				bean.setTransportador(listaTerceiro.iterator().next());
			}
			
			if ((devolucao || registrarNF || org.apache.commons.lang.StringUtils.isNotBlank(bean.getWhereInPedidovendaRetorno())) && listaNaturezaoperacao.size()==1){
				bean.setNaturezaoperacao(listaNaturezaoperacao.get(0));
			}
			
			Map<Pedidovendamaterial, Vendamaterial> mapaPedidovendamaterialVendamaterial = getMapaPedidovendamaterialVendamaterial(whereInVendaRetorno);
			for(Coleta coleta : listaColeta){
				ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(bean.getNaturezaoperacao());
				if(coleta.getListaColetaMaterial() != null && !coleta.getListaColetaMaterial().isEmpty()){
					Map<ColetamaterialNotaBean, Double> mapaSaldoMaterial = getMapaSaldoMaterial(coleta.getCdcoleta(), coleta.getListaColetaMaterial(), bean.getTipooperacaonota());
					Notafiscalprodutoitem item;
					ColetamaterialNotaBean coletamaterialNotaBean;
					
					for(ColetaMaterial cm : coleta.getListaColetaMaterial()){	
						if((devolucaoBool && cm.getQuantidadedevolvidaSemNota() > 0) || !devolucaoBool){							
							if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
								ColetaMaterialPedidovendamaterial itemServico = cm.getListaColetaMaterialPedidovendamaterial().get(0);
								Integer cdpedidovendamaterial = itemServico.getPedidovendamaterial() != null ? itemServico.getPedidovendamaterial().getCdpedidovendamaterial() : null;									
								coletamaterialNotaBean = new ColetamaterialNotaBean(cm.getMaterial().getCdmaterial(), cdpedidovendamaterial);
									
								item = new Notafiscalprodutoitem();
								item.setCdcoletamaterial(cm.getCdcoletamaterial());
//									item.setMotivodevolucao(cm.getMotivodevolucao());
								item.setMotivodevolucao(Hibernate.isInitialized(cm.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(cm.getListaColetamaterialmotivodevolucao()) ? cm.getListaColetamaterialmotivodevolucao().get(0).getMotivodevolucao() : null);
								item.setListaMotivodevolucao(cm.getListaMotivodevolucao());
								item.setMaterial(cm.getMaterial());
								item.setPneu(cm.getPneu());
								item.setUnidademedida(cm.getMaterial().getUnidademedida());
								item.setNcmcapitulo(cm.getMaterial().getNcmcapitulo());
								item.setNcmcompleto(cm.getMaterial().getNcmcompleto());
								item.setNve(cm.getMaterial().getNve());
								item.setPedidovendamaterial(itemServico.getPedidovendamaterial());
								
								if(cm.getValorunitario() != null && cm.getValorunitario() > 0d){
									item.setValorunitario(cm.getValorunitario());
								} else {
									item.setValorunitario(cm.getMaterial().getValorcusto() != null ? cm.getMaterial().getValorcusto() : 0d);
								}
								
								if(devolucaoBool){
									item.setQtde(cm.getQuantidadedevolvidaSemNota());
								} else {
									Double qtde;										
									Double saldo = mapaSaldoMaterial.get(coletamaterialNotaBean);
									if (cm.getQuantidade() >= saldo){
										qtde = saldo;
										mapaSaldoMaterial.put(coletamaterialNotaBean, 0D);
									} else {
										qtde = cm.getQuantidade();
										mapaSaldoMaterial.put(coletamaterialNotaBean, saldo - cm.getQuantidade());											
									}
									
									if (qtde<0){
										qtde = 0D;
									}
									item.setQtde(qtde);
								}
								
								item.setValorbruto(new Money(item.getQtde() * item.getValorunitario()));
								item.setLocalarmazenagem(cm.getLocalarmazenagem());
								item.setInfoadicional(cm.getObservacao());
								item.setMaterialclasse(Materialclasse.PRODUTO);
								item.setIncluirvalorprodutos(Boolean.TRUE);
								
								item.setInformacaoAdicionalProdutoBean(criaBeanInfoProduto(cm.getMaterial(), cm.getPneu(), cm, itemServico.getPedidovendamaterial() != null ? mapaPedidovendamaterialVendamaterial.get(itemServico.getPedidovendamaterial()) : null, null, itemServico.getPedidovendamaterial(), coleta.getPedidovenda()));
								
								String templateInfoAdicionalItem = montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
								if(templateInfoAdicionalItem != null){
									item.setInfoadicional(templateInfoAdicionalItem);
									item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
								}else {
									item.setInfoAdicionalItemAnterior("");
								}
								
								if (item.getQtde()!=null && item.getQtde()>0D){
									bean.getListaItens().add(item);
								}
							}
						}
					}
					if(!devolucaoBool && Tipooperacaonota.ENTRADA.equals(bean.getTipooperacaonota()) && SinedUtil.isListEmpty(bean.getListaItens()) && !geracaoAutomarica){
						request.getServletResponse().sendRedirect(request.getServletRequest().getContextPath() +  "/faturamento/crud/Coleta?ACAO=msgNotaItensColeta");
					}
				}
				
				bean.setMotivodevolucaocoleta(coleta.getMotivodevolucao());
			}
			
			setTotalNota(bean);
			
			calcularTributacaoNota(bean);
			setTotalNota(bean);
			
			if (devolucao && !listaOrigemOperacao.isEmpty()){
				bean.setOrigemOperacaoStr(CollectionsUtil.concatenate(listaOrigemOperacao, ", "));
			}
			
			if ((devolucao || registrarNF) && listaNaturezaoperacao.size()==1){
				bean.setNaturezaoperacao(listaNaturezaoperacao.get(0));
			}
			
			if(bean.getOperacaonfe() == null){
				bean.setOperacaonfe(ImpostoUtil.getOperacaonfe(bean.getCliente(), bean.getNaturezaoperacao()));
			}
			
			bean.setPossuiTemplateInfoContribuinte(Boolean.FALSE);		
			if(bean.getNaturezaoperacao() != null){
				Naturezaoperacao natureza = naturezaoperacaoService.load(bean.getNaturezaoperacao());
				bean.setPossuiTemplateInfoContribuinte(natureza.getTemplateinfcontribuinte() != null);
			}
	        
			if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
				if(!Boolean.TRUE.equals(bean.getPossuiTemplateInfoContribuinte())){
						Empresa empresa = empresaService.buscarInfContribuinte(bean.getEmpresa());
					if(empresa != null && empresa.getTextoinfcontribuinte() != null){
						if(bean.getInfoadicionalcontrib() == null)
							bean.setInfoadicionalcontrib(empresa.getTextoinfcontribuinte());
						else {
							if(!bean.getInfoadicionalcontrib().contains(empresa.getTextoinfcontribuinte()))
								bean.setInfoadicionalcontrib(empresa.getTextoinfcontribuinte() + ". " + bean.getInfoadicionalcontrib());
						}
					}
				}
				
				Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf");
				
				if(empresa.getNaopreencherdtsaida() == null || !empresa.getNaopreencherdtsaida()){
					bean.setDatahorasaidaentrada(SinedDateUtils.currentTimestamp());
				}
				
				bean.setMarcavolume(empresa.getMarcanf());
				bean.setEspvolume(empresa.getEspecienf());
				
				Endereco endereco = enderecoService.carregaEnderecoEmpresa(bean.getEmpresa());
				if(endereco != null){
					bean.setMunicipiogerador(endereco.getMunicipio());
				}
			}
		}
		
		return bean;
	}
	
	private Map<Pedidovendamaterial, Vendamaterial> getMapaPedidovendamaterialVendamaterial(String whereInVendaRetorno) {
		HashMap<Pedidovendamaterial, Vendamaterial> map = new HashMap<Pedidovendamaterial, Vendamaterial>();
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVendaRetorno)){
			List<Vendamaterial> listaVendamaterial = vendamaterialService.findByVenda(whereInVendaRetorno);
			if(SinedUtil.isListNotEmpty(listaVendamaterial)){
				for(Vendamaterial vendamaterial : listaVendamaterial){
					if(vendamaterial.getPedidovendamaterial() != null){
						map.put(vendamaterial.getPedidovendamaterial(), vendamaterial);
					}
				}
			}
		}
		
		return map;
	}
	
	public void setObservacaoHistoricoNota(Notafiscalproduto bean, String[] ids, String link, String descricao, boolean concatenar) {
		StringBuilder sb = new StringBuilder();
		sb.append(descricao);
		
		for(int i=0; i<ids.length; i++){
			sb.append("<a href='" + link + ids[i] +"'>"+ids[i]+", </a>");
		}
		
		if(sb.length() > 0){
			sb.setCharAt(sb.lastIndexOf(","), '.');

			if(bean.getListaNotaHistorico() == null) bean.setListaNotaHistorico(new ArrayList<NotaHistorico>());
			if(bean.getListaNotaHistorico().size() == 0) bean.getListaNotaHistorico().add(new NotaHistorico());
			
			NotaHistorico nh = bean.getListaNotaHistorico().get(0);
			if(concatenar){
				nh.setObservacao((org.apache.commons.lang.StringUtils.isNotBlank(nh.getObservacao()) ? nh.getObservacao() + "<br>": "") + sb.toString());
			}else {
				nh.setObservacao(sb.toString());
			}
			bean.getListaNotaHistorico().set(0, nh);
		}
		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param empresa
	 * @param numeronota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public Notafiscalproduto findNotaByNumeroEmpresa(Empresa empresa, String numeronota, boolean includeCancelada) {
		return notafiscalprodutoDAO.findNotaByNumeroEmpresa(empresa, numeronota, includeCancelada);
	}
	
	public String montaInformacoesAdicionaisItemTemplate(InformacaoAdicionalProdutoBean beanInfo, ReportTemplateBean reportTemplateBean) {
		if(reportTemplateBean != null && beanInfo != null){
			try {
	    		ITemplateEngine engine = new GroovyTemplateEngine().build(reportTemplateBean.getLeiaute());
	    		Map<String,Object> datasource = new HashMap<String, Object>();
				datasource.put("bean", beanInfo);
				String retorno = engine.make(datasource);
				
				if(retorno != null && retorno.length() > 500){
					retorno = retorno.substring(0, 499);
				}
				return retorno;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
	public InformacaoAdicionalProdutoBean criaBeanInfoProduto(Pedidovenda pedidovenda, Pedidovendamaterial pedidovendamaterial) {
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null && pedidovendamaterial != null && pedidovendamaterial.getCdpedidovendamaterial() != null){
			try {
				List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByPedidovendaForProducao(pedidovenda.getCdpedidovenda().toString(), pedidovendamaterial.getCdpedidovendamaterial().toString());
				ColetaMaterial coletamaterial = listaColetaMaterial != null && listaColetaMaterial.size() == 1 ? listaColetaMaterial.get(0) : null;
				return criaBeanInfoProduto(pedidovendamaterial.getMaterial(), pedidovendamaterial.getPneu(), coletamaterial, null, null, pedidovendamaterial, pedidovenda);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	public InformacaoAdicionalProdutoBean criaBeanInfoProduto(Material material, Pneu pneu, ColetaMaterial coletaMaterial, Vendamaterial vendamaterial, Venda venda, Pedidovendamaterial pedidovendamaterial, Pedidovenda pedidovenda) {
		InformacaoAdicionalProdutoBean beanInfo = new InformacaoAdicionalProdutoBean();
 		beanInfo.setMaterial(new MaterialReportBean(material));
 		beanInfo.setPneu(new PneuReportBean(pneu));
 		beanInfo.setVendamaterial(new VendamaterialReportBean(vendamaterial, venda));
 		beanInfo.setPedidovendamaterial(new PedidovendamaterialReportBean(pedidovendamaterial, pedidovenda));
 		beanInfo.setRecusado(coletaMaterial != null && coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0);
 		beanInfo.setObservacao_coleta(coletaMaterial != null && coletaMaterial.getObservacao() != null ? coletaMaterial.getObservacao() : "");
 		return beanInfo;
	}
	
	public List<Notafiscalproduto> findNotasSemSerie(String whereIn){
		return notafiscalprodutoDAO.findNotasSemSerie(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param chaveacesso
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2017
	 */
	public List<Notafiscalproduto> findByChaveacesso(String chaveacesso) {
		return notafiscalprodutoDAO.findByChaveacesso(chaveacesso);
	}
	
	public void criarAvisoNotaSemContaReceberNaoLiquidada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Notafiscalproduto> notaList = notafiscalprodutoDAO.findByNotaSemContaReceber(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Nota n : notaList) {
			Aviso aviso = new Aviso("Nota fiscal sem v�nculo a uma conta a receber", "N�mero da nota fiscal: " + (!org.apache.commons.lang.StringUtils.isEmpty(n.getNumero()) ? n.getNumero() : "Sem n�mero"), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), n.getCdNota(), n.getEmpresa() != null ? n.getEmpresa() : empresa, SinedDateUtils.currentDate(), m, 
					Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, n.getCdNota());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, n.getCdNota(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	public List<Notafiscalproduto> findForAutocompleteCompleteNota(String busca){
		return notafiscalprodutoDAO.findForAutocompleteCompleteNota(busca, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}

	public Notafiscalproduto findNotaByNumeroSerie(String numero, Integer serie) {
		return notafiscalprodutoDAO.findNotaByNumeroSerie(numero, serie);
	}
	
	public void updateHoraEmissao(Nota nota, Hora hora) {
		notafiscalprodutoDAO.updateHoraEmissao(nota, hora);
	}
	
	public void atualizaExpedicao(String obsParaHistorico,SituacaoVinculoExpedicao situacaoExpedicao, Notafiscalproduto nf,NotaStatus status, boolean criarHistoricoNota) {
		notafiscalprodutoDAO.updateSituacaoExpedicao(nf,situacaoExpedicao);
		if(criarHistoricoNota){
			try{
			NotaHistorico notaHistorico = new NotaHistorico(nf, obsParaHistorico,status, ((Usuario)Neo.getUser()).getCdpessoa(), new Timestamp(System.currentTimeMillis()));
			notaHistoricoService.saveOrUpdate(notaHistorico);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviaEmailNotaErroCorrecao(Notaerrocorrecao notaerrocorrecao) {
		
		String emailRemetente = null;
		if(SinedUtil.getUsuarioLogado() != null){
			emailRemetente = SinedUtil.getUsuarioLogado().getEmail();
		}
		
		String emailDestinatario = " suporte@linkcom.com.br";
		String assunto = "C�digo de Rejei��o: "+notaerrocorrecao.getCodigo()+" NFe / NFCe n�o cadastrado.";
		String mensagem = "Favor cadastrar as informa��es sobre o  C�digo de Rejei��o: "+notaerrocorrecao.getCodigo()+
				" na base origem para que possa ser replicado para o sistema W3ERP. O coordenador de desenvolvimento " +
				"deve ser notificado para a atualiza��o das demais bases do sistema W3ERP.";
		
		EmailManager emailmanager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		
		try{
			emailmanager
					.setFrom(emailRemetente)
					.setSubject(assunto)
					.addHtmlText(mensagem)
					.setTo(emailDestinatario);
			
			emailmanager.sendMessage();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void atualizaExpedicao(String obsParaHistorico,SituacaoVinculoExpedicao situacaoExpedicao, Notafiscalproduto nf,NotaStatus status) {
		atualizaExpedicao(obsParaHistorico, situacaoExpedicao, nf, status, true);
	}
	
    public void updateInfoAdicionalContrib(Notafiscalproduto notafiscalproduto) {
        notafiscalprodutoDAO.updateInfoAdicionalContrib(notafiscalproduto);
    }
    
    public Notafiscalproduto carregaInfoAdicionalContrib(Notafiscalproduto notafiscalproduto) {
        return notafiscalprodutoDAO.carregaInfoAdicionalContrib(notafiscalproduto);
    }
    
	public ImportarXMLEstadoBean processarXmlNfceEmporium(String stringXml) throws Exception {
		NotaXML notaxml = ImportarXML.preencheBean(stringXml);
		Empresa empresa = null;
		if(org.apache.commons.lang.StringUtils.isBlank(notaxml.getCNPJ_emit())){
			String cdEmpresa = parametrogeralService.buscaValorPorNome(Parametrogeral.EMPORIUM_VENDA_EMPRESA);
			if(org.apache.commons.lang.StringUtils.isNotBlank(cdEmpresa)){
				empresa = empresaService.load(new Empresa(Integer.parseInt(cdEmpresa)), "empresa.cdpessoa, empresa.cnpj");
				if(Util.objects.isPersistent(empresa) && empresa.getCnpj() != null){
					notaxml.setCNPJ_emit(empresa.getCnpj().getValue());
				}
			}
		}
		if(empresa == null){
			Cnpj cnpj_emit = new Cnpj(notaxml.getCNPJ_emit());
			empresa = empresaService.findByCnpj(cnpj_emit);
		}
		Cliente cliente = null;
		if(org.apache.commons.lang.StringUtils.isBlank(notaxml.getCPF_dest()) && org.apache.commons.lang.StringUtils.isBlank(notaxml.getCNPJ_dest())){
			String cdCliente = parametrogeralService.buscaValorPorNome(Parametrogeral.EMPORIUM_VENDA_CLIENTE);
			if(org.apache.commons.lang.StringUtils.isNotBlank(cdCliente)){
				cliente = clienteService.loadForEmissaoNf(new Cliente(Integer.parseInt(cdCliente)));
				if(Util.objects.isPersistent(cliente)){
					if(cliente.getCnpj() != null)
						notaxml.setCNPJ_dest(cliente.getCnpj().getValue());
					if(cliente.getCpf() != null)
						notaxml.setCPF_dest(cliente.getCpf().getValue());
					notaxml.setxNome(cliente.getNome());
					notaxml.setIE_dest(cliente.getInscricaoestadual());
					notaxml.setEmail_dest(cliente.getEmail());
					if(SinedUtil.isListNotEmpty(cliente.getListaEndereco())){
						Endereco endereco = cliente.getListaEndereco().iterator().next();
						notaxml.setxLgr_dest(endereco.getLogradouro());
						notaxml.setNro_dest(endereco.getNumero());
						notaxml.setxCpl_dest(endereco.getComplemento());
						notaxml.setxBairro_dest(endereco.getBairro());
						notaxml.setcMun_dest(endereco.getMunicipio().getNome());
						notaxml.setCEP_dest(endereco.getCep().getValue());
						notaxml.setcPais_dest(endereco.getPais().getNome());
//						notaxml.setFone_dest(endereco.getTe);
					}
				}
			}
		}
		if(cliente == null){
			cliente = ImportacaoXMLEstadoUtil.buscarCliente(notaxml);
		}
		
		List<ImportarXMLEstadoItemBean> listaItens = new ArrayList<ImportarXMLEstadoItemBean>();
		if(notaxml.getListadet() != null){
			ImportarXMLEstadoItemBean item;
			for (NotaXMLItem it: notaxml.getListadet()) {
				item = new ImportarXMLEstadoItemBean();
				item.setItem(it);
				
				String codigo = it.getcProd();
				try{
					Material material = materialService.findByCdmaterial(Integer.parseInt(codigo));
					item.setMaterial(material);
					item.setNomeMaterial(material.getAutocompleteDescription());
				} catch (Exception e) {
				}
				
				listaItens.add(item);
			}
		}
		
		ImportarXMLEstadoBean bean = new ImportarXMLEstadoBean();
		bean.setNotaXML(notaxml);
		bean.setListaItens(listaItens);
		bean.setEmpresa(empresa);
//		bean.setCliente(buscarCliente(notaxml));
		bean.setCliente(cliente);
		if(notaxml.getTpNF() != null) bean.setTipooperacaonota(Tipooperacaonota.values()[notaxml.getTpNF()]);
		if(notaxml.getvNF() != null) bean.setValornota(new Money(notaxml.getvNF()));
		
		// ADICIONA UMA LINHA NO RATEIO
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		listaRateioitem.add(new Rateioitem());
		rateio.setListaRateioitem(listaRateioitem);
		bean.setRateio(rateio);
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(notaxml.getNatOp())){
			bean.setNaturezaoperacao(naturezaoperacaoService.createIfNotExistsByNome(notaxml.getNatOp()));
		}
		
		return bean;
	}
	
	/**
	 * M�todo que faz a importa��o do arquivo.
	 *
	 * @param request
	 * @param notaxml
	 * @param arquivo
	 * @param listaItensAssociados
	 * @throws Exception
	 * @since 14/06/2012
	 * @author Rodrigo Freitas
	 * @param rateio 
	 * @param criarConteceber 
	 */
	public Nota importarArquivo(WebRequestContext request, NotaXML notaxml, Arquivo arquivo, ImportarXMLEstadoBean bean, Configuracaonfe configuracaonfe, Venda venda, Emporiumvenda emporiumVenda) throws Exception{
		Notafiscalproduto nota = new Notafiscalproduto();
		
		List<ImportarXMLEstadoItemBean> listaItensAssociados = bean.getListaItens();
//		Rateio rateio = bean.getRateio();
		Naturezaoperacao naturezaPadrao = bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getCdnaturezaoperacao() != null? naturezaoperacaoService.load(bean.getNaturezaoperacao()): null;
		
		nota.setNotaTipo(NotaTipo.NOTA_FISCAL_CONSUMIDOR);
		
		if(naturezaPadrao != null){
			nota.setNaturezaoperacao(naturezaPadrao);
			nota.setOperacaonfe(naturezaPadrao.getOperacaonfe());
		}
		nota.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
		if(notaxml.getIndPag() != null){
			nota.setFormapagamentonfe(Formapagamentonfe.values()[notaxml.getIndPag()]);
		}
		nota.setNumero(notaxml.getnNF());
		nota.setDtEmissao(notaxml.getdEmi() != null ? notaxml.getdEmi() : notaxml.getDhEmi());
		
		if(notaxml.getTpNF() != null) nota.setTipooperacaonota(Tipooperacaonota.values()[notaxml.getTpNF()]);
		if(notaxml.getcMunFG() != null) nota.setMunicipiogerador(municipioService.findByCdibge(notaxml.getcMunFG()));
		if(notaxml.getFinNFe() != null) nota.setFinalidadenfe(Finalidadenfe.values()[notaxml.getFinNFe() - 1]);
		if(notaxml.getIdDest() != null) nota.setLocaldestinonfe(Localdestinonfe.values()[notaxml.getIdDest() - 1]);
		if(notaxml.getIndFinal() != null) nota.setOperacaonfe(Operacaonfe.values()[notaxml.getIndFinal()]);
		if(notaxml.getIndPres() != null) nota.setPresencacompradornfe(Presencacompradornfe.getByCdnfe(notaxml.getIndPres()));

//		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		/*		if(documentotipo == null){
			request.addError("Tipo de documento 'Nota Fiscal' n�o encontrado.");
			return null;
		}*/
		
		Cnpj cnpj_emit = new Cnpj(notaxml.getCNPJ_emit());
		Empresa empresa = empresaService.findByCnpj(cnpj_emit);
		if(empresa == null){
			//request.addError("Empresa n�o encontrada. - " + notaxml.getCNPJ_emit());
			//return null;
		}
		
		/*if(notaxml.getSerie() == null){
			request.addError("S�rie da nf-e n�o pode ficar em branco.");
			return null;
		}*/
		
		if(notaxml.getdSaiEnt() != null && !empresaService.getNaopreencherdtsaidaNota(empresa)){
			String hora = "00:00";
			if(notaxml.gethSaiEnt() != null){
				hora = notaxml.gethSaiEnt().substring(0, 5);
			}
			String data = SinedDateUtils.toString(notaxml.getdSaiEnt()) + " " + hora;
			nota.setDatahorasaidaentrada(SinedDateUtils.stringToTimestamp(data, "dd/MM/yyyy HH:mm"));
		}
		
		if(notaxml.getnNF() != null && this.isExisteNota(empresa, notaxml.getnNF(), notaxml.getSerie(), ModeloDocumentoFiscalEnum.NFE, null)){
			//request.addError("Nota j� cadastrada. - " + notaxml.getnNF());
			//return null;
		}
		nota.setEmpresa(empresa);
		nota.setSerienfe(notaxml.getSerie());
		nota.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFCE);
		
		//Cliente cliente = buscarCliente(notaxml);
		Cliente cliente = bean.getCliente();
	    nota.setCliente(cliente);
	    
	    Endereco endereco = null;
	    
	    if(notaxml.getCEP_dest() != null)
			//pesquisar endere�o
			endereco = enderecoService.loadEndereco(notaxml.getCEP_dest(), 
															notaxml.getNro_dest(),
															cliente.getCdpessoa());
		if(endereco == null){
			Municipio municipio = municipioService.findByCdibge(String.valueOf(notaxml.getcMun_dest()));
			endereco = new Endereco();
			endereco.setLogradouro(notaxml.getxLgr_dest());
			endereco.setNumero(notaxml.getNro_dest());
			endereco.setBairro(notaxml.getxBairro_dest());
			endereco.setComplemento(notaxml.getxCpl_dest() != null ? (notaxml.getxCpl_dest().length() > 50 ? notaxml.getxCpl_dest().substring(0, 50) : null) : null);
			if(notaxml.getCEP_dest() != null) endereco.setCep(new Cep(notaxml.getCEP_dest()));
			endereco.setMunicipio(municipio);
			endereco.setPessoa(cliente);
			endereco.setEnderecotipo(Enderecotipo.UNICO);
			
			enderecoService.saveOrUpdate(endereco);
		}
		nota.setEnderecoCliente(endereco);
		
		if(notaxml.getCNPJ_retirada() != null) {
			nota.setTipopessoaretirada(Tipopessoa.PESSOA_JURIDICA);
			nota.setCnpjretirada(new Cnpj(notaxml.getCNPJ_retirada()));
		}
		if(notaxml.getCPF_retirada() != null) {
			nota.setTipopessoaretirada(Tipopessoa.PESSOA_FISICA);
			nota.setCpfretirada(new Cpf(notaxml.getCPF_retirada()));
		}
		nota.setLogradouroretirada(notaxml.getxLgr_retirada());
		nota.setNumeroretirada(notaxml.getNro_retirada());
		nota.setComplementoretirada(notaxml.getxCpl_retirada());
		nota.setBairroretirada(notaxml.getxBairro_retirada());
		if(notaxml.getcMun_retirada() != null) nota.setMunicipioretirada(municipioService.findByCdibge(notaxml.getcMun_retirada()));
		
		if(notaxml.getCNPJ_entrega() != null) {
			nota.setTipopessoaentrega(Tipopessoa.PESSOA_JURIDICA);
			nota.setCnpjentrega(new Cnpj(notaxml.getCNPJ_entrega()));
		}
		if(notaxml.getCPF_entrega() != null) {
			nota.setTipopessoaentrega(Tipopessoa.PESSOA_FISICA);
			nota.setCpfentrega(new Cpf(notaxml.getCPF_entrega()));
		}
		nota.setLogradouroentrega(notaxml.getxLgr_entrega());
		nota.setNumeroentrega(notaxml.getNro_entrega());
		nota.setComplementoentrega(notaxml.getxCpl_entrega());
		nota.setBairroentrega(notaxml.getxBairro_entrega());
		if(notaxml.getcMun_entrega() != null) nota.setMunicipioentrega(municipioService.findByCdibge(notaxml.getcMun_entrega()));
		
		if(notaxml.getvBC_ICMSTot() != null) nota.setValorbcicms(new Money(notaxml.getvBC_ICMSTot()));
		if(notaxml.getvICMS() != null) nota.setValoricms(new Money(notaxml.getvICMS()));
		if(notaxml.getvBCST() != null) nota.setValorbcicmsst(new Money(notaxml.getvBCST()));
		if(notaxml.getvST() != null) nota.setValoricmsst(new Money(notaxml.getvST()));
		if(notaxml.getvProd() != null) nota.setValorprodutos(new Money(notaxml.getvProd()));
		if(notaxml.getvFrete() != null) nota.setValorfrete(new Money(notaxml.getvFrete()));
		if(notaxml.getvSeg() != null) nota.setValorseguro(new Money(notaxml.getvSeg()));
		if(notaxml.getvDesc() != null) nota.setValordesconto(new Money(notaxml.getvDesc()));
		if(notaxml.getvIPI() != null) nota.setValoripi(new Money(notaxml.getvIPI()));
		if(notaxml.getvPIS_ICMSTot() != null) nota.setValorpis(new Money(notaxml.getvPIS_ICMSTot()));
		if(notaxml.getvCOFINS_ICMSTot() != null) nota.setValorcofins(new Money(notaxml.getvCOFINS_ICMSTot()));
		if(notaxml.getvOutro() != null) nota.setOutrasdespesas(new Money(notaxml.getvOutro()));
		if(notaxml.getvNF() != null) nota.setValor(new Money(notaxml.getvNF()));
		
		if(notaxml.getvServ() != null) nota.setValorservicos(new Money(notaxml.getvServ()));
		if(notaxml.getvBC_ISSQNtot() != null) nota.setValorbciss(new Money(notaxml.getvBC_ISSQNtot()));
		if(notaxml.getvISS() != null) nota.setValoriss(new Money(notaxml.getvISS()));
		if(notaxml.getvPIS_ISSQNtot() != null) nota.setValorpisservicos(new Money(notaxml.getvPIS_ISSQNtot()));
		if(notaxml.getvCOFINS_ISSQNtot() != null) nota.setValorcofinsservicos(new Money(notaxml.getvCOFINS_ISSQNtot()));
		
		if(notaxml.getvRetPIS() != null) nota.setValorpisretido(new Money(notaxml.getvRetPIS()));
		if(notaxml.getvRetCOFINS() != null) nota.setValorcofinsretido(new Money(notaxml.getvRetCOFINS()));
		if(notaxml.getvRetCSLL() != null) nota.setValorcsll(new Money(notaxml.getvRetCSLL()));
		if(notaxml.getvBCIRRF() != null) nota.setValorbcirrf(new Money(notaxml.getvBCIRRF()));
		if(notaxml.getvIRRF() != null) nota.setValorirrf(new Money(notaxml.getvIRRF()));
		if(notaxml.getvBCRetPrev() != null) nota.setValorbcprevsocial(new Money(notaxml.getvBCRetPrev()));
		if(notaxml.getvRetPrev() != null) nota.setValorprevsocial(new Money(notaxml.getvRetPrev()));
		
		
		if(notaxml.getModFrete() != null){
			switch (notaxml.getModFrete()) {
			case 0:
				nota.setResponsavelfrete(ResponsavelFrete.EMITENTE);
				break;
			case 1:
				nota.setResponsavelfrete(ResponsavelFrete.DESTINATARIO);
				break;
			case 2:
				nota.setResponsavelfrete(ResponsavelFrete.TERCEIROS);
				break;
			case 3:
				nota.setResponsavelfrete(ResponsavelFrete.PROPRIO_REMETENTE);
				break;
			case 4:
				nota.setResponsavelfrete(ResponsavelFrete.PROPRIO_DESTINATARIO);
				break;
			case 9:
				nota.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
				break;

			default:
				nota.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
				break;
			}
		}
		
		Fornecedor fornecedor = null;
		Pessoa pessoa = null;
		//pesquisar pessoa 
	    if(notaxml.getCPF_transporta() != null && !notaxml.getCPF_transporta().equals("") ){
	    	Cpf cpf = new Cpf(notaxml.getCPF_transporta());
	    	fornecedor = fornecedorService.findByCpf(cpf);
	    	if(fornecedor == null){
	    		pessoa = pessoaService.findPessoaByCpf(cpf);
	    		
	    		fornecedor = new Fornecedor();
    			fornecedor.setCpf(cpf);
    			fornecedor.setNome(notaxml.getxNome_transporta());
    			fornecedor.setAtivo(true);
    			fornecedor.setAcesso(true);
    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_FISICA);
    			
	    		if(pessoa == null){
	    			fornecedorService.saveOrUpdate(fornecedor);
	    		} else if(pessoa != null){
	    			fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    			fornecedorService.insertSomenteFornecedor(fornecedor);
	    		}
	    	}
	    } else if(notaxml.getCNPJ_transporta() != null && !notaxml.getCNPJ_transporta().equals("")){
	    	Cnpj cnpj = new Cnpj(notaxml.getCNPJ_transporta());
	    	fornecedor = fornecedorService.findByCnpj(cnpj);
	    	if(fornecedor == null){
	    		pessoa = pessoaService.findPessoaByCnpj(cnpj);
	    		
	    		fornecedor = new Fornecedor();
    			fornecedor.setCnpj(cnpj);
    			fornecedor.setNome(notaxml.getxNome_transporta());
    			fornecedor.setAtivo(true);
    			fornecedor.setAcesso(true);
    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			fornecedorService.saveOrUpdate(fornecedor);
	    		} else if(pessoa != null){
	    			fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    			fornecedorService.insertSomenteFornecedor(fornecedor);
	    		}
	    	}
	    } else if(notaxml.getxNome_transporta() != null && !notaxml.getxNome_transporta().equals("")){ 
	    	try {
	    		fornecedor = fornecedorService.findByNome(notaxml.getxNome_transporta());
	    		if(fornecedor == null){
	    			pessoa = pessoaService.findPessoaByNome(notaxml.getxNome_transporta());
	    			
	    			fornecedor = new Fornecedor();
	    			fornecedor.setNome(notaxml.getxNome_transporta());
	    			fornecedor.setAtivo(true);
	    			fornecedor.setAcesso(true);
	    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    			
	    			if(pessoa == null){
	    				fornecedorService.saveOrUpdate(fornecedor);
	    			} else if(pessoa != null){
	    				fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    				fornecedorService.insertSomenteFornecedor(fornecedor);
	    			}
	    		}
			} catch (Exception e) {}
	    }
	    
	    nota.setTransportador(fornecedor);
	    
	    if(notaxml.getPlaca() != null || notaxml.getUF_veicTransp() != null && notaxml.getRNTC() != null){
	    	nota.setTransporteveiculo(Boolean.TRUE);
	    	nota.setPlacaveiculo(notaxml.getPlaca());
	    	if(notaxml.getUF_veicTransp() != null) nota.setUfveiculo(ufService.findBySigla(notaxml.getUF_veicTransp()));
	    	nota.setRntc(notaxml.getRNTC());
	    } else {
	    	nota.setTransporteveiculo(Boolean.FALSE);
	    }
	    
	    nota.setQtdevolume(notaxml.getqVol());
	    nota.setEspvolume(notaxml.getEsp());
	    nota.setMarcavolume(notaxml.getMarca());
	    nota.setNumvolume(notaxml.getnVol());
	    nota.setPesoliquido(notaxml.getPesoL());
	    nota.setPesobruto(notaxml.getPesoB());
	    nota.setEmporiumVenda(emporiumVenda);
	    
		nota.setInfoadicionalcontrib(notaxml.getInfCpl());
		nota.setInfoadicionalfisco(notaxml.getInfAdFisco());
		
		nota.setNumerofatura(notaxml.getnFat());
		if(notaxml.getvOrig_fat() != null) nota.setValororiginalfatura(new Money(notaxml.getvOrig_fat()));
		if(notaxml.getvDesc_fat() != null) nota.setValordescontofatura(new Money(notaxml.getvDesc_fat()));
		if(notaxml.getvLiq_fat() != null) nota.setValorliquidofatura(new Money(notaxml.getvLiq_fat()));
		
		List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
		if(notaxml.getListadup() != null){
			Notafiscalprodutoduplicata notafiscalprodutoduplicata;
			for (NotaXMLDuplicata notaXMLDuplicata : notaxml.getListadup()) {
				notafiscalprodutoduplicata = new Notafiscalprodutoduplicata();
				notafiscalprodutoduplicata.setNotafiscalproduto(nota);
				notafiscalprodutoduplicata.setNumero(notaXMLDuplicata.getnDup());
				notafiscalprodutoduplicata.setDtvencimento(notaXMLDuplicata.getdVenc());
				if(notaXMLDuplicata.getvDup() != null) notafiscalprodutoduplicata.setValor(new Money(notaXMLDuplicata.getvDup()));
				
				listaDuplicata.add(notafiscalprodutoduplicata);
			}
			nota.setListaDuplicata(listaDuplicata);
		}
		/*Boolean criarContaReceber = Boolean.TRUE.equals(bean.getCriarContareceber()) && SinedUtil.isListNotEmpty(nota.getListaDuplicata());
		if(criarContaReceber){
			nota.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		} else {
			nota.setNotaStatus(NotaStatus.NFE_EMITIDA);
		}*/
		nota.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		
		if(notaxml.getListadet() != null){
			List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
			Notafiscalprodutoitem notafiscalprodutoitem;
			for (NotaXMLItem notaxmlitem : notaxml.getListadet()) {
				notafiscalprodutoitem = new Notafiscalprodutoitem();
				notafiscalprodutoitem.setNotafiscalproduto(nota);
				
				Material material = null;
				for (ImportarXMLEstadoItemBean itemAssociado : listaItensAssociados) {
					if(itemAssociado.getItem() != null && 
							itemAssociado.getItem().getSequencial() != null &&
							notaxmlitem.getSequencial() != null &&
							itemAssociado.getItem().getSequencial().equals(notaxmlitem.getSequencial())){
						material = itemAssociado.getMaterial();
						break;
					}
				}
				
				if(material == null){
					//request.addError("Material n�o encontrado.");
					return null;
				}				
				
				notafiscalprodutoitem.setMaterial(material);
				
				List<Materialclasse> listaMaterialclasse = materialService.findClasses(material);
				
				for (Materialclasse classe : listaMaterialclasse) {
					if(Materialclasse.PRODUTO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
						break;
					}else if(Materialclasse.EPI.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
						break;
					}else if(Materialclasse.PATRIMONIO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
					}else if(Materialclasse.SERVICO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
					}
				}				
				
				if(notaxmlitem.getcEAN() != null && !"".equals(notaxmlitem.getcEAN()) && SinedUtil.validaCEAN(notaxmlitem.getcEAN()))
					notafiscalprodutoitem.setGtin(notaxmlitem.getcEAN());
				
				if(notaxmlitem.getNCM() != null && notaxmlitem.getNCM().length() >= 2){
					String ncmcapituloStr = notaxmlitem.getNCM().substring(0, 2);
					Ncmcapitulo ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(Integer.parseInt(ncmcapituloStr)));
					
					notafiscalprodutoitem.setNcmcapitulo(ncmcapitulo);
					notafiscalprodutoitem.setNcmcompleto(notaxmlitem.getNCM());
				}
				notafiscalprodutoitem.setExtipi(notaxmlitem.getEXTIPI());
				if(notaxmlitem.getCFOP() != null) notafiscalprodutoitem.setCfop(cfopService.findByCodigo(notaxmlitem.getCFOP()));
				
				if(notaxmlitem.getuCom() != null && !notaxmlitem.getuCom().equals("")){
					Unidademedida unidademedida = unidademedidaService.findBySimbolo(notaxmlitem.getuCom());
					notafiscalprodutoitem.setUnidademedida(unidademedida);
				}
				
				notafiscalprodutoitem.setQtde(notaxmlitem.getqCom());
				notafiscalprodutoitem.setValorunitario(notaxmlitem.getvUnCom());
				if(notaxmlitem.getvProd() != null) notafiscalprodutoitem.setValorbruto(new Money(notaxmlitem.getvProd()));
				if(notaxmlitem.getvFrete() != null) notafiscalprodutoitem.setValorfrete(new Money(notaxmlitem.getvFrete()));
				if(notaxmlitem.getvDesc() != null) notafiscalprodutoitem.setValordesconto(new Money(notaxmlitem.getvDesc()));
				if(notaxmlitem.getvSeg() != null) notafiscalprodutoitem.setValorseguro(new Money(notaxmlitem.getvSeg()));
				if(notaxmlitem.getvOutro() != null) notafiscalprodutoitem.setOutrasdespesas(new Money(notaxmlitem.getvOutro()));
				
				if(notaxmlitem.getIndTot() != null){
					switch (notaxmlitem.getIndTot()) {
					case 0:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.FALSE);
						break;
					case 1:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.TRUE);
						break;
					default:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.TRUE);
						break;
					}
				}
				
				notafiscalprodutoitem.setTributadoicms(notaxmlitem.getcSitTrib_ISSQN() == null);
				
				if(notaxmlitem.getOrig() != null) notafiscalprodutoitem.setOrigemproduto(Origemproduto.getEnum(notaxmlitem.getOrig()));
				if(notaxmlitem.getCST_ICMS() != null) notafiscalprodutoitem.setTipocobrancaicms(Tipocobrancaicms.getTipocobrancaicms(notaxmlitem.getCST_ICMS()));
				if(notaxmlitem.getCSOSN_ICMS() != null) notafiscalprodutoitem.setTipocobrancaicms(Tipocobrancaicms.getTipocobrancaicms(notaxmlitem.getCSOSN_ICMS()));
				if(notafiscalprodutoitem.getTipocobrancaicms() != null){
					if(notafiscalprodutoitem.getTipocobrancaicms().isSimplesnacional()){
						notafiscalprodutoitem.setTipotributacaoicms(Tipotributacaoicms.SIMPLES_NACIONAL);
					} else {
						notafiscalprodutoitem.setTipotributacaoicms(Tipotributacaoicms.NORMAL);
					}
				}
				if(notaxmlitem.getModBC() != null) notafiscalprodutoitem.setModalidadebcicms(Modalidadebcicms.values()[notaxmlitem.getModBC()]);
				if(notaxmlitem.getpRedBC() != null) notafiscalprodutoitem.setReducaobcicms(notaxmlitem.getpRedBC());
				if(notaxmlitem.getvBC() != null) notafiscalprodutoitem.setValorbcicms(new Money(notaxmlitem.getvBC()));
				notafiscalprodutoitem.setIcms(notaxmlitem.getpICMS());
				if(notaxmlitem.getvICMS() != null) notafiscalprodutoitem.setValoricms(new Money(notaxmlitem.getvICMS()));
				if(notaxmlitem.getMotDesICMS() != null) notafiscalprodutoitem.setMotivodesoneracaoicms(Motivodesoneracaoicms.getMotivodesoneracaoicms(notaxmlitem.getMotDesICMS()));
				if(notaxmlitem.getModBCST() != null) notafiscalprodutoitem.setModalidadebcicmsst(Modalidadebcicmsst.values()[notaxmlitem.getModBCST()]);
				if(notaxmlitem.getpMVAST() != null) notafiscalprodutoitem.setMargemvaloradicionalicmsst(notaxmlitem.getpMVAST());
				if(notaxmlitem.getpRedBCST() != null) notafiscalprodutoitem.setReducaobcicmsst(notaxmlitem.getpRedBCST());
				if(notaxmlitem.getvBCST() != null) notafiscalprodutoitem.setValorbcicmsst(new Money(notaxmlitem.getvBCST()));
				notafiscalprodutoitem.setIcmsst(notaxmlitem.getpICMSST());
				if(notaxmlitem.getvICMSST() != null) notafiscalprodutoitem.setValoricmsst(new Money(notaxmlitem.getvICMSST()));
				if(notaxmlitem.getpBCOp() != null) notafiscalprodutoitem.setBcoperacaopropriaicms(new Money(notaxmlitem.getpBCOp()));
				if(notaxmlitem.getUFST() != null) notafiscalprodutoitem.setUficmsst(ufService.findBySigla(notaxmlitem.getUFST()));
				if(notaxmlitem.getvBCSTRet() != null) notafiscalprodutoitem.setValorbcicms(new Money(notaxmlitem.getvBCSTRet()));
				if(notaxmlitem.getvICMSSTRet() != null) notafiscalprodutoitem.setValoricms(new Money(notaxmlitem.getvICMSSTRet()));
				if(notaxmlitem.getvBCSTDest() != null) notafiscalprodutoitem.setValorbcicmsst(new Money(notaxmlitem.getvBCSTDest()));
				if(notaxmlitem.getvICMSSTDest() != null) notafiscalprodutoitem.setValoricmsst(new Money(notaxmlitem.getvICMSSTDest()));
				notafiscalprodutoitem.setAliquotacreditoicms(notaxmlitem.getpCredSN());
				if(notaxmlitem.getvCredICMSSN() != null) notafiscalprodutoitem.setValorcreditoicms(new Money(notaxmlitem.getvCredICMSSN()));
		
				if(notaxmlitem.getCNPJProd() != null) notafiscalprodutoitem.setCnpjprodutoripi(new Cnpj(notaxmlitem.getCNPJProd()));
				notafiscalprodutoitem.setCodigoseloipi(notaxmlitem.getcSelo());
				notafiscalprodutoitem.setQtdeseloipi(notaxmlitem.getqSelo());
				if(notaxmlitem.getCST_IPI() != null) notafiscalprodutoitem.setTipocobrancaipi(Tipocobrancaipi.getTipocobrancaipi(notaxmlitem.getCST_IPI()));
				if(notaxmlitem.getvBC_IPI() != null){
					notafiscalprodutoitem.setTipocalculoipi(Tipocalculo.PERCENTUAL);
				} else if(notaxmlitem.getqUnid_IPI() != null){
					notafiscalprodutoitem.setTipocalculoipi(Tipocalculo.EM_VALOR);
				}
				if(notaxmlitem.getvBC_IPI() != null) notafiscalprodutoitem.setValorbcipi(new Money(notaxmlitem.getvBC_IPI()));
				notafiscalprodutoitem.setIpi(notaxmlitem.getpIPI());
				notafiscalprodutoitem.setQtdevendidaipi(notaxmlitem.getqUnid_IPI());
				if(notaxmlitem.getvUnid_IPI() != null) notafiscalprodutoitem.setAliquotareaisipi(new Money(notaxmlitem.getvUnid_IPI()));
				if(notaxmlitem.getvIPI() != null) notafiscalprodutoitem.setValoripi(new Money(notaxmlitem.getvIPI()));
				
				if(notaxmlitem.getCST_PIS() != null) notafiscalprodutoitem.setTipocobrancapis(Tipocobrancapis.getTipocobrancapis(notaxmlitem.getCST_PIS()));
				if(notaxmlitem.getvBC_PIS() != null) notafiscalprodutoitem.setValorbcpis(new Money(notaxmlitem.getvBC_PIS()));
				notafiscalprodutoitem.setPis(notaxmlitem.getpPIS());
				notafiscalprodutoitem.setQtdevendidapis(notaxmlitem.getqBCProd_PIS());
				if(notaxmlitem.getvAliqProd_PIS() != null) notafiscalprodutoitem.setAliquotareaispis(new Money(notaxmlitem.getvAliqProd_PIS()));
				if(notaxmlitem.getvPIS() != null) notafiscalprodutoitem.setValorpis(new Money(notaxmlitem.getvPIS()));
				
				if(notaxmlitem.getCST_COFINS() != null) notafiscalprodutoitem.setTipocobrancacofins(Tipocobrancacofins.getTipocobrancacofins(notaxmlitem.getCST_COFINS()));
				if(notaxmlitem.getvBC_COFINS() != null) notafiscalprodutoitem.setValorbccofins(new Money(notaxmlitem.getvBC_COFINS()));
				notafiscalprodutoitem.setCofins(notaxmlitem.getpCOFINS());
				notafiscalprodutoitem.setQtdevendidacofins(notaxmlitem.getqBCProd_COFINS());
				if(notaxmlitem.getvAliqProd_COFINS() != null) notafiscalprodutoitem.setAliquotareaiscofins(new Money(notaxmlitem.getvAliqProd_COFINS()));
				if(notaxmlitem.getvCOFINS() != null) notafiscalprodutoitem.setValorcofins(new Money(notaxmlitem.getvCOFINS()));
				
				if(notaxmlitem.getvBC_ISSQN() != null) notafiscalprodutoitem.setValorbciss(new Money(notaxmlitem.getvBC_ISSQN()));
				notafiscalprodutoitem.setIss(notaxmlitem.getvAliq_ISSQN());
				if(notaxmlitem.getvISSQN() != null) notafiscalprodutoitem.setValoriss(new Money(notaxmlitem.getvISSQN()));
				if(notaxmlitem.getcMunFG_ISSQN() != null) notafiscalprodutoitem.setMunicipioiss(municipioService.findByCdibge(notaxmlitem.getcMunFG_ISSQN()));
				if(notaxmlitem.getcListServ_ISSQN() != null) notafiscalprodutoitem.setItemlistaservico(itemlistaservicoService.loadByCodigo(notaxmlitem.getcListServ_ISSQN()));
				if(notaxmlitem.getcSitTrib_ISSQN() != null) notafiscalprodutoitem.setTipotributacaoiss(Tipotributacaoiss.getTipotributacaoiss(notaxmlitem.getcSitTrib_ISSQN()));
				
				notafiscalprodutoitem.setInfoadicional(notaxmlitem.getInfAdProd());
				
				listaItens.add(notafiscalprodutoitem);
			}
			nota.setListaItens(listaItens);
		}
		nota.setOrigemCupom(true);
		this.saveOrUpdate(nota);
		arquivoService.saveOrUpdate(arquivo);
		
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setNota(nota);
		notaHistorico.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		notaHistorico.setObservacao("Nota importada via integra��o com ECF. <a href='" + SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + arquivo.getCdarquivo() + "'>Arquivo XML</a>.");
		
		notaHistoricoService.saveOrUpdate(notaHistorico);
		
		notaHistorico = new NotaHistorico();
		notaHistorico.setNota(nota);
		notaHistorico.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		//arquivoService.saveOrUpdate(arquivo);
		notaHistorico.setObservacao("Visualizar venda: <a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>");
		
		notaHistoricoService.saveOrUpdate(notaHistorico);
		
		Emporiumvendahistorico historicoEmporium = new Emporiumvendahistorico();
		historicoEmporium.setAcao(Emporiumvendaacao.NFCE_EMITIDA);
		historicoEmporium.setEmporiumvenda(emporiumVenda);
		historicoEmporium.setObservacao("Visualizar NFC-e: <a href=\"javascript:visualizaNota("+nota.getCdNota()+");\">"+nota.getCdNota()+"</a>");
		emporiumvendahistoricoService.saveOrUpdate(historicoEmporium);
		
		Arquivonfnota arquivonfnota = new Arquivonfnota();
		
		
		arquivonfnota.setArquivoxml(arquivo);
	    arquivoService.saveFile(arquivonfnota, "arquivoxml");
		
		Arquivonf arquivonf = new Arquivonf();
		arquivonf.setDtenvio(SinedDateUtils.currentDate());
		arquivonf.setEmpresa(empresa);
		arquivonf.setConfiguracaonfe(configuracaonfe);
		arquivonf.setArquivonfsituacao(Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		arquivonf.setDtrecebimento(notaxml.getDhRecbto());
		
		arquivonf.setAssinando(Boolean.TRUE);
		arquivonf.setEnviando(Boolean.TRUE);
		arquivonf.setConsultando(Boolean.TRUE);
		
		arquivonf.setArquivoxml(arquivo);
		arquivonf.setArquivoxmlassinado(arquivo);
		
		arquivonfService.saveOrUpdate(arquivonf);
		
		arquivonfnota.setArquivonf(arquivonf);
		arquivonfnota.setNota(nota);
		arquivonfnota.setChaveacesso(notaxml.getChNFe());
		arquivonfnota.setDtprocessamento(notaxml.getDhRecbto());
		arquivonfnota.setProtocolonfe(notaxml.getnProt());
		
		arquivonfnotaService.saveOrUpdate(arquivonfnota);
		
		/*if(criarContaReceber){
			for (Notafiscalprodutoduplicata nfpDup : listaDuplicata) {
				Documento documento = new Documento();
				documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				documento.setDocumentotipo(documentotipo);
				documento.setDescricao(notaxml.getNatOp());
				documento.setNumero(notaxml.getnNF());
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.setPessoa(nota.getCliente());
				documento.setCliente(nota.getCliente());
				documento.setDtemissao(nota.getDtEmissao());
				documento.setDtvencimento(nfpDup.getDtvencimento() != null ? nfpDup.getDtvencimento() : nota.getDtEmissao());
				documento.setValor(nfpDup.getValor());
				documento.setEmpresa(nota.getEmpresa());
				
				Rateio rat = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>(rateio.getListaRateioitem());
				rat.setListaRateioitem(listaRateioitem);
				rateioService.atualizaValorRateio(rat, documento.getValor());
				
				documento.setRateio(rat);
				
				StringBuilder historicoDocumento = new StringBuilder();
				historicoDocumento.append("Origem da nota <a href=\"javascript:visualizarNotaFiscalProduto(")
									 .append(nota.getCdNota()).append(")\">")
									 .append(nota.getNumero() != null ? nota.getNumero() : "sem n�mero")
									 .append("</a>");
				
				documento.setObservacaoHistorico(historicoDocumento.toString());
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
					documento.setMensagem1("Referente a NF: " + (nota.getNumero() != null ? nota.getNumero() : "sem n�mero"));
				}
				
				Documentohistorico documentohistorico = new Documentohistorico(documento);
				
				List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
				listaHistorico.add(documentohistorico);
				
				documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
				
				contareceberService.saveOrUpdate(documento);
				
				NotaDocumento notaDocumento = new NotaDocumento(nota, documento);
				notaDocumentoService.saveOrUpdate(notaDocumento);
				
				notaHistorico = new NotaHistorico(null, 
						nota, 
						nota.getNotaStatus(), 
						"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
						SinedUtil.getUsuarioLogado().getCdpessoa(), 
						new Timestamp(System.currentTimeMillis()));

				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		}*/
		notavendaService.saveOrUpdate(new NotaVenda(nota, venda));
		Vendahistorico vendahistorico = new Vendahistorico();
		vendahistorico.setVenda(venda);
		String numero = nota.getNumero() == null || nota.getNumero().equals("") ? "sem n�mero" : nota.getNumero();
		vendahistorico.setAcao("Gera��o da nota " + numero);
		vendahistorico.setObservacao("Visualizar NFC-e: <a href=\"javascript:visualizaNFCe("+nota.getCdNota()+");\">"+numero+"</a>.");
		vendahistoricoService.saveOrUpdate(vendahistorico);
		return nota;
	}
	
	public void criasNotasByCupom(List<Emporiumvenda> listaEmporiumVenda){
		for(final Emporiumvenda emporiumvenda: listaEmporiumVenda){
			try {
				getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
					public Object doInTransaction(TransactionStatus status) {
						try {
							if(org.apache.commons.lang.StringUtils.isNotBlank(emporiumvenda.getXml_env())){
								String string = new String(Util.strings.tiraAcento(emporiumvenda.getXml_env()).getBytes(), "UTF-8");
								Arquivo arquivoXmlNota = new Arquivo(string.getBytes(), "nfceenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
								emporiumvenda.setArquivoXmlNota(arquivoXmlNota);
								ImportarXMLEstadoBean beanNfe = NotafiscalprodutoService.getInstance().processarXmlNfceEmporium(emporiumvenda.getXml_env());
								beanNfe.getNotaXML().setChNFe(emporiumvenda.getChv_acs());
								beanNfe.getNotaXML().setnProt(emporiumvenda.getProtocolonfe());
								beanNfe.getNotaXML().setDhRecbto(emporiumvenda.getDtprocessamento());
								Configuracaonfe configuracaoNfe = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR, beanNfe.getEmpresa());
								
								NotafiscalprodutoService.getInstance().importarArquivo(null, beanNfe.getNotaXML(), emporiumvenda.getArquivoXmlNota(), beanNfe, configuracaoNfe, emporiumvenda.getVenda(), emporiumvenda);
								EmporiumvendaService.getInstance().updateNotaGerada(emporiumvenda);
							}
						}catch (Exception e) {
							e.printStackTrace();
							if(Util.objects.isPersistent(emporiumvenda)){
								System.out.println("Erro ao tentar criar nota do emporiumvenda "+emporiumvenda.getCdemporiumvenda()+".");
							}
							return null;
						}		
						return null;
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

    public Cfop carregaCfopForTributacaoItem(Grupotributacao grupotributacao){
		Material material = materialService.loadWithMatrialgrupo(grupotributacao.getMaterialTrans());
		Naturezaoperacao naturezaoperacao = grupotributacao.getNaturezaoperacaoTrans();
		Cliente cliente = grupotributacao.getClienteTrans() != null && grupotributacao.getClienteTrans().getCdpessoa() != null ? 
										clienteService.loadForTributacao(grupotributacao.getClienteTrans()) : null;
		Empresa empresa = null; 
		Endereco enderecoOrigem = null;
		if (grupotributacao.getEmpresaTributacaoOrigem() != null && grupotributacao.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(grupotributacao.getEmpresaTributacaoOrigem());
			enderecoOrigem = empresa.getEndereco();
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
			enderecoOrigem = empresa.getEndereco();
		}		
		
		Endereco enderecoDestino = null;
		if(grupotributacao.getEnderecoavulso() != null && grupotributacao.getEnderecoavulso() && grupotributacao.getEndereconotaDestino() != null && 
				grupotributacao.getEndereconotaDestino().getMunicipio() != null){
			enderecoDestino =  grupotributacao.getEndereconotaDestino();
		}else {
			enderecoDestino = grupotributacao.getEnderecoTributacaoDestino();
			if(enderecoDestino != null && enderecoDestino.getCdendereco() != null){
				enderecoDestino = enderecoService.carregaEnderecoComUfPais(enderecoDestino);
			}
		}
		
		Grupotributacao gt = grupotributacaoService.loadForEntrada(grupotributacao);
		material = materialService.loadTributacao(material, empresa, material.getMaterialgrupo(), cliente,
													enderecoOrigem, enderecoDestino, gt);
		
		Cfop cfop = null;
		
		if(gt.getCfopgrupo() != null){
			material.getMaterialgrupo().setCfop(gt.getCfopgrupo());
			cfop = cfopService.findForNFProduto(material, enderecoOrigem, enderecoDestino);
		}
		
		if(cfop == null && naturezaoperacao != null && naturezaoperacao.getCdnaturezaoperacao() != null && enderecoOrigem != null && enderecoDestino != null){
			Cfopescopo cfopescopo = cfopescopoService.loadByEndereco(enderecoOrigem, enderecoDestino);
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, cfopescopo);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				cfop = listaNaturezaoperacaocfop.get(0).getCfop();
			}
		}
		return cfop;
    }
    
	public void calculaImpostoDIFAL(CalculoDIFALInterface itemCalculoDIFAL){
		Empresa empresa = itemCalculoDIFAL.getEmpresa();
		Cliente cliente = itemCalculoDIFAL.getCliente();
		Endereco enderecoEmpresa = empresa.getEndereco();
		Endereco enderecoCliente = itemCalculoDIFAL.getEnderecoCliente();
		
		Grupotributacao grupotributacao = itemCalculoDIFAL.getGrupotributacao();
		if(grupotributacao == null){
			List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
					grupotributacaoService.createGrupotributacaoVOForTributacao(
											Operacao.SAIDA, 
											empresa, 
											itemCalculoDIFAL.getNaturezaoperacao(), 
											itemCalculoDIFAL.getMaterial().getMaterialgrupo(),
											cliente,
											true, 
											itemCalculoDIFAL.getMaterial(),
											itemCalculoDIFAL.getNcmcompleto(),
											enderecoCliente, 
											null,
											null,
											null,
											itemCalculoDIFAL.getOperacaonfe(),
											itemCalculoDIFAL.getModeloDocumentoFiscalEnum(),
											itemCalculoDIFAL.getLocaldestinonfe(),
											itemCalculoDIFAL.getDtemissao()));
			
			grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
		}
		
		Double aliquotaFCP = 0.0;
		Double aliquotaICMSIntra = 0.0;
		Double aliquotaICMSInter = 0.0;
		
		if(grupotributacao != null){
			Uf ufOrigem = null;
			if (enderecoEmpresa != null && enderecoEmpresa.getCdendereco() != null){
				Endereco enderecoOrigem = enderecoService.loadEndereco(enderecoEmpresa);
				if (enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getUf() != null)
					ufOrigem = enderecoOrigem.getMunicipio().getUf();
			}
			
			Uf ufDestino = null;
			if (enderecoCliente != null && enderecoCliente.getCdendereco() != null){
				Endereco enderecoDestino = enderecoService.loadEnderecoNota(enderecoCliente);
				if (enderecoDestino != null && enderecoDestino.getMunicipio() != null && enderecoDestino.getMunicipio().getUf() != null)
					ufDestino = enderecoDestino.getMunicipio().getUf();
			}
			
			aliquotaFCP = grupotributacaoService.getAliquotaByGrupotributacao(Faixaimpostocontrole.FCP, grupotributacao, empresa, cliente, null, ufDestino);
			aliquotaICMSIntra = grupotributacaoService.getAliquotaByGrupotributacao(Faixaimpostocontrole.ICMS, grupotributacao, empresa, cliente, ufDestino, ufDestino);
			aliquotaICMSInter = grupotributacaoService.getAliquotaByGrupotributacao(Faixaimpostocontrole.ICMS, grupotributacao, empresa, cliente, ufOrigem, ufDestino);
		}
		
		Date dtEmissao = itemCalculoDIFAL.getDtemissao();
		int anoEmissao = SinedDateUtils.getDateProperty(dtEmissao, Calendar.YEAR);
		
		Double aliquotaICMSPart = 100d;
		if(anoEmissao == 2016)  aliquotaICMSPart = 40d;
		if(anoEmissao == 2017)  aliquotaICMSPart = 60d;
		if(anoEmissao == 2018)  aliquotaICMSPart = 80d;
		
		Money cem = new Money(100d);
		
		Money valorbcdestinatario = null;
		if(grupotributacao != null && org.apache.commons.lang.StringUtils.isNotBlank(grupotributacao.getFormulabcicmsdestinatario())){
			grupotributacaoService.calcularValorBCIcmsDestinatario(itemCalculoDIFAL, grupotributacao);
			valorbcdestinatario = itemCalculoDIFAL.getValorbcdestinatario();
		} else {
			Money valorbruto = itemCalculoDIFAL.getValorbruto() != null ? itemCalculoDIFAL.getValorbruto() : new Money();
			Money valorfrete = itemCalculoDIFAL.getValorfrete() != null ? itemCalculoDIFAL.getValorfrete() : new Money();
			Money outrasdespesas = itemCalculoDIFAL.getOutrasdespesas() != null ? itemCalculoDIFAL.getOutrasdespesas() : new Money();
			Money valordesconto = itemCalculoDIFAL.getValordesconto() != null ? itemCalculoDIFAL.getValordesconto() : new Money();
			Money valoripi = itemCalculoDIFAL.getValoripi() != null ? itemCalculoDIFAL.getValoripi() : new Money();
			
			valorbcdestinatario = valorbruto.add(valorfrete).add(outrasdespesas).subtract(valordesconto).add(valoripi);
		}
		
		Money valorfcpdestinatario = valorbcdestinatario.multiply(new Money(aliquotaFCP).divide(cem));
		
		Money subtracaoaliquota = new Money(aliquotaICMSIntra).subtract(new Money(aliquotaICMSInter));
		Money valordifal = valorbcdestinatario.multiply(subtracaoaliquota.divide(cem));
		
		Money valoricmsremetente = valordifal.multiply(new Money(100d - aliquotaICMSPart).divide(cem));
		Money valoricmsdestinatario = valordifal.multiply(new Money(aliquotaICMSPart).divide(cem));
		
		itemCalculoDIFAL.setValordifal(valordifal);
		itemCalculoDIFAL.setDifal(subtracaoaliquota.doubleValue());
		itemCalculoDIFAL.setValorbcdestinatario(valorbcdestinatario);
		itemCalculoDIFAL.setValorbcfcpdestinatario(valorbcdestinatario);
		itemCalculoDIFAL.setFcpdestinatario(aliquotaFCP);
		itemCalculoDIFAL.setIcmsdestinatario(aliquotaICMSIntra);
		itemCalculoDIFAL.setIcmsinterestadual(aliquotaICMSInter);
		itemCalculoDIFAL.setIcmsinterestadualpartilha(aliquotaICMSPart);
		itemCalculoDIFAL.setValorfcpdestinatario(valorfcpdestinatario);
		itemCalculoDIFAL.setValoricmsdestinatario(valoricmsdestinatario);
		itemCalculoDIFAL.setValoricmsremetente(valoricmsremetente);
	}
	
	public List<Notafiscalproduto> carregarDadosMargemContribuicao(RelatorioMargemContribuicaoFiltro filtro) {
		return notafiscalprodutoDAO.carregarDadosMargemContribuicao(filtro);
	}
	
	public List<Notafiscalproduto> carregarDadosMargemContribuicaoNota(RelatorioMargemContribuicaoFiltro filtro) {
		return notafiscalprodutoDAO.carregarDadosMargemContribuicaoNota(filtro);
	}
	
    public void geraContaReceberDeNotaSeparadaAgrupandoConta(WebRequestContext request, List<Notafiscalproduto> notas){
    	if(!parametrogeralService.getBoolean(Parametrogeral.GERAR_NF_SEPARADA_TIPOPEDIDOVENDA)){
			return;
    	}
    }
    
	public void preencheLocalDestinoNota(Notafiscalproduto nf) {
        Localdestinonfe localDestinoNfe = ImpostoUtil.getLocalDestino(nf.getMunicipiogerador(), nf.getEnderecoCliente());
        if(localDestinoNfe != null){
            nf.setLocaldestinonfe(localDestinoNfe);
		}
	}

    public Notafiscalproduto loadForCalcularEntradaMaterial (Notafiscalproduto notaFiscalProduto){
    	return notafiscalprodutoDAO.loadForCalcularEntradaMaterial(notaFiscalProduto);
    }
    
	public void calcularCustoEntradaMaterial(Notafiscalproduto bean){
		if(formulaCustoService.existsFormulaAtiva() && 
				"FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.ENTREGA_NAOATUALIZACUSTO))){
			Notafiscalproduto notaFiscalProduto = loadForCalcularEntradaMaterial(bean);
			List<Material> listaMaterial = new ArrayList<Material>();
			List<Notafiscalprodutoitem> listaItem = new ArrayList<Notafiscalprodutoitem>();
			NotaVO notaVO = new NotaVO(notaFiscalProduto);
			for(Notafiscalprodutoitem nfpi : notaFiscalProduto.getListaItens()){
				if(nfpi.getMaterial() != null && nfpi.getMaterial().getCdmaterial() != null &&
						nfpi.getCfop() != null && nfpi.getCfop().getAtualizarvalor() != null && 
								nfpi.getCfop().getAtualizarvalor()){
					
					MaterialVO materialVO = new MaterialVO(nfpi, notaFiscalProduto);
					FreteVO freteVO = new FreteVO();
					OutrosVO outrosVO = new OutrosVO();
					
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine engine = manager.getEngineByName("JavaScript");
					
					engine.put("material", materialVO);
					engine.put("nota", notaVO);
					engine.put("frete", freteVO);
					engine.put("outros", outrosVO);
					
					Double valorEntrada = 0d;
					boolean erro = false;
					String msgm = "";
					Formulacusto formulacusto = formulaCustoService.getFormulacustoForcalcularCustoEntradaMaterial();
					for (Formulacustoitem formulacustoitem : formulacusto.getListaFormulacustoitem()) {
						try{
							Object obj = engine.eval(formulacustoitem.getFormula());
			
							Double resultado = 0d;
							if(obj != null){
								String resultadoStr = obj.toString();
								resultado = new Double(resultadoStr);
							}
							
							engine.put(formulacustoitem.getIdentificador(), resultado);
							if(!Double.isInfinite(resultado)){
								valorEntrada = SinedUtil.roundByParametro(resultado);
							}else {
								erro = true;
							}
						} catch (ScriptException e) {
							e.printStackTrace();
							erro = true;
						}
						if(erro){
							msgm = "Erro ao calcular o valor de custo. F�rmula: " + 
							formulacusto.getNome() + ". (" +
							formulacustoitem.getIdentificador() + " - " +
							formulacustoitem.getFormula() + " )";
							break;
						}
					}
					if(erro){
						NeoWeb.getRequestContext().addError(msgm);
					}else {
						nfpi.setValorEntrada(valorEntrada);
						listaItem.add(nfpi);
						if(!listaMaterial.contains(nfpi.getMaterial())){
							listaMaterial.add(nfpi.getMaterial());
						}
					}
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaItem) && SinedUtil.isListNotEmpty(listaMaterial)){
				List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByNotaFiscalProdutoItem(CollectionsUtil.listAndConcatenate(listaItem, "cdnotafiscalprodutoitem", ","));
				if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
					for(Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque){
						if(movimentacaoestoque.getMovimentacaoestoqueorigem() != null && 
								Util.objects.isPersistent(movimentacaoestoque.getMovimentacaoestoqueorigem().getNotaFiscalProdutoItem())){
							for(Notafiscalprodutoitem notaFiscalProdutoItem :listaItem){
								if(movimentacaoestoque.getMovimentacaoestoqueorigem().getNotaFiscalProdutoItem().getCdnotafiscalprodutoitem().equals(notaFiscalProdutoItem.getCdnotafiscalprodutoitem())){
									movimentacaoestoqueService.preencheValorCusto(notaFiscalProdutoItem.getValorEntrada(), movimentacaoestoque);
								
									listaMaterial.remove(movimentacaoestoque.getMaterial());
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	
	public List<Notafiscalproduto> findForSIntegraRegistro61(SintegraFiltro filtro) {
		return notafiscalprodutoDAO.findForSIntegraRegistro61(filtro);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmitirComprovanteTEFBean> getListaComprovanteTEFBean(String whereIn, boolean nfe) {
		List<EmitirComprovanteTEFBean> lista = new ArrayList<EmitirComprovanteTEFBean>();
		List<Notafiscalproduto> listaNota = this.findForComprovanteTEF(whereIn);
		for (Notafiscalproduto nf : listaNota) {
			EmitirComprovanteTEFBean bean = new EmitirComprovanteTEFBean();
			
			List<Notafiscalprodutoduplicata> listaDuplicata = nf.getListaDuplicata();
			if(listaDuplicata != null && listaDuplicata.size() > 0){
				for (Notafiscalprodutoduplicata notafiscalprodutoduplicata : listaDuplicata) {
					EmitirComprovanteTEFPagamentoBean pagamentoBean = new EmitirComprovanteTEFPagamentoBean();
					pagamentoBean.setFormapagamento(notafiscalprodutoduplicata.getDocumentotipo() != null ? notafiscalprodutoduplicata.getDocumentotipo().getNome() : "");
					pagamentoBean.setValor(notafiscalprodutoduplicata.getValor() != null ? notafiscalprodutoduplicata.getValor().doubleValue() : 0d);
					bean.addPagamento(pagamentoBean);
				}
			}
			
			int qtdeItens = 0;
			List<Notafiscalprodutoitem> listaItens = nf.getListaItens();
			if(listaItens != null && listaItens.size() > 0){
				for (Notafiscalprodutoitem it : listaItens) {
					EmitirComprovanteTEFItemBean itBean = new EmitirComprovanteTEFItemBean();
					
					Material material = it.getMaterial();
					if(material != null){
						itBean.setProduto_codigo(material.getCdmaterial());
						itBean.setProduto_identificador(material.getIdentificacao());
						itBean.setProduto_nome(material.getNome());
					}
					
					Unidademedida unidademedida = it.getUnidademedida();
					if(unidademedida != null){
						itBean.setUnidademedida_nome(unidademedida.getNome());
						itBean.setUnidademedida_simbolo(unidademedida.getSimbolo());
					}
					
					itBean.setQuantidade(it.getQtde() != null ? it.getQtde() : 0d);
					itBean.setValor_desconto(it.getValordesconto() != null ? it.getValordesconto().doubleValue() : 0d);
					itBean.setValor_total(it.getValorbruto() != null ? it.getValorbruto().doubleValue() : 0d);
					itBean.setValor_unitario(it.getValorunitario() != null ? it.getValorunitario().doubleValue() : 0d);
					bean.addItem(itBean);
					
					qtdeItens++;
				}
			}
			
			List<Arquivotef> listaArquivotef = arquivotefService.findByNota(nf.getCdNota() + "");
			if(listaArquivotef == null || listaArquivotef.size() == 0){
				String whereInVenda = vendaService.getWhereInVendaByNota(nf);
				if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVenda)){
					listaArquivotef = arquivotefService.findByVenda(whereInVenda);
				}
			}
			arquivotefService.preencheAdquirenteComprovante(bean, listaArquivotef);
			
			if(nf.getEmpresa() != null){
				Endereco enderecoWithPrioridade = nf.getEmpresa().getEnderecoWithPrioridade();
				
				bean.setEmpresa_nome(nf.getEmpresa().getNome() != null ? nf.getEmpresa().getNome() : "");
				bean.setEmpresa_cnpj(nf.getEmpresa().getCnpj() != null ? nf.getEmpresa().getCnpj().toString() : "");
				bean.setEmpresa_endereco(enderecoWithPrioridade != null ? enderecoWithPrioridade.getLogradouroCompletoComBairro() : "");
				bean.setEmpresa_telefone(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getEmpresa().getTelefonesSemQuebraLinha()));
			}
			
			if(nf.getCliente() != null){
				bean.setCliente_cpfcnpj(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getCliente().getCpfCnpj()));
				bean.setCliente_ie(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getCliente().getInscricaoestadual()));
				bean.setCliente_nome(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getCliente().getNome()));
				bean.setCliente_telefone(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getCliente().getTelefonesSemQuebraLinha()));
			}
			
			if(nf.getEnderecoCliente() != null){
				bean.setCliente_endereco(org.apache.commons.lang.StringUtils.trimToEmpty(nf.getEnderecoCliente().getLogradouroCompletoComBairro()));
			}
			
			bean.setCodigo(nf.getCdNota());
			bean.setCondicao_pagamento(nf.getPrazopagamentofatura() != null ? nf.getPrazopagamentofatura().getNome() : "");
			bean.setData(nf.getDtEmissao() != null ? SinedDateUtils.toString(nf.getDtEmissao()) : "");
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(nf);
			if (arquivonfnota == null && !nf.getNotaStatus().equals(NotaStatus.EMITIDA) && !nf.getNotaStatus().equals(NotaStatus.EM_ESPERA)){
				throw new SinedException("N�o foi poss�vel imprimir o comprovante. Lote de Nota Fiscal Eletr�nica n�o encontrado.");
			}
			
			if(arquivonfnota != null){
				bean.setNota_chaveacesso(arquivonfnota.getChaveacesso());
				bean.setNota_dataautorizacao(SinedDateUtils.toString(arquivonfnota.getDtprocessamento(), "dd/MM/yyyy HH:mm"));
				bean.setNota_protocoloautorizacao(arquivonfnota.getProtocolonfe());
				String qrCode = arquivonfnota.getQrCode();
				
				try {
					String qrCodeEncoded = URLEncoder.encode(qrCode, "UTF-8");
					String url = "https://cutt.ly/api/api.php?key=35b333db7bbd47cbdb71630303e2774e75e4d&short=" + qrCodeEncoded;
					SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
					
					HttpGet httpGet = new HttpGet(url);
				    HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpGet);
				    HttpEntity entityResponse = response.getEntity();
				    
				    String jsonRetorno = EntityUtils.toString(entityResponse, "UTF-8");
					Map<String, Object> mapRetorno = new Gson().fromJson(jsonRetorno, Map.class);
					if(mapRetorno.containsKey("url")){
						Map<String, Object> mapUrl = (Map<String, Object>) mapRetorno.get("url");
						if(mapUrl.containsKey("shortLink")){
							Object shortLinkObj = mapUrl.get("shortLink");
							if(shortLinkObj != null){
								qrCode = shortLinkObj.toString();
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				bean.setNota_qrcode(qrCode);
			}
			
			bean.setNota_numero(nf.getNumero());
			bean.setNota_serie(nf.getSerienfe());
			bean.setNota_dataemissao(nf.getDtEmissao() != null ? SinedDateUtils.toString(nf.getDtEmissao()) : "");
			bean.setNota_horaemissao(nf.getHremissao() != null ? nf.getHremissao().toString() : "");
			bean.setNota_valor_total_imposto_federal(nf.getValorTotalImpostoFederal() != null ? nf.getValorTotalImpostoFederal().getValue().doubleValue() : 0d);
			bean.setNota_valor_total_imposto_estadual(nf.getValorTotalImpostoEstadual() != null ? nf.getValorTotalImpostoEstadual().getValue().doubleValue() : 0d);
			bean.setNota_valor_total_imposto_municipal(nf.getValorTotalImpostoMunicipal() != null ? nf.getValorTotalImpostoMunicipal().getValue().doubleValue() : 0d);
			
			Money totalparcela = nf.getValorliquidofatura();
			Money totalproduto = nf.getValorprodutos();
			Double totalpesobruto = nf.getPesobruto();
			Double totalpesoliquido = nf.getPesoliquido();
			Money total = nf.getValor();
			
			bean.setObservacao(nf.getInfoadicionalcontrib());
			bean.setPesobruto_total(totalpesobruto != null ? totalpesobruto : 0d);
			bean.setPesoliquido_total(totalpesoliquido != null ? totalpesoliquido : 0d);
			bean.setQuantidade_itens(qtdeItens);
			bean.setValor_desconto(nf.getValordesconto() != null ? nf.getValordesconto().doubleValue() : 0d);
			bean.setValor_pagamento(totalparcela != null ? totalparcela.doubleValue() : 0d);
			bean.setValor_produtos(totalproduto != null ? totalproduto.doubleValue() : 0d);
			bean.setValor_total(total != null ? total.doubleValue() : 0d);
			
			lista.add(bean);
		}
		
		return lista;
	}
	
	private List<Notafiscalproduto> findForComprovanteTEF(String whereIn) {
		return notafiscalprodutoDAO.findForComprovanteTEF(whereIn);
	}
	
	public List<Notafiscalproduto> findNotaCriadasViaIntegracaoECF(String whereIn) {
		return notafiscalprodutoDAO.findNotaCriadasViaIntegracaoECF(whereIn);
	}
	
	public boolean haveNotaCriadasViaIntegracaoECF(String whereIn){
		return SinedUtil.isListNotEmpty(this.findNotaCriadasViaIntegracaoECF(whereIn));
	}
	
	public List<Notafiscalproduto> findForGnre(String whereIn) {
		return notafiscalprodutoDAO.findForGnre(whereIn);
	}

	public List<Notafiscalproduto> findForAutocompleteByProcessoReferenciado(String numero){
		String tipoOperacaoStr = SinedUtil.getTipoOperacaoParametroRequisicaoAutocomplete();
		Tipooperacaonota tipoOperacao = null;
		if("ENTRADA".equalsIgnoreCase(tipoOperacaoStr)){
			tipoOperacao = Tipooperacaonota.ENTRADA;
		}else if("SAIDA".equalsIgnoreCase(tipoOperacaoStr)){
			tipoOperacao = Tipooperacaonota.SAIDA;
		}
		return notafiscalprodutoDAO.findForAutocompleteByProcessoReferenciado(numero, tipoOperacao,SinedUtil.getEmpresaParametroRequisicaoAutocomplete());
	}
	
	public Notafiscalproduto findEmitidaNaReceitaByVenda(Venda venda){
		return notafiscalprodutoDAO.findEmitidaNaReceitaByVenda(venda);
	}
	
	public boolean isPossuiNotaEmitidaNaReceita(Venda venda){
		return Util.objects.isPersistent(findEmitidaNaReceitaByVenda(venda));
	}
}