package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.GerenciamentomaterialFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.rest.w3producao.estoque.EstoqueMaterialW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.estoque.EstoqueW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.estoque.EstoqueW3producaoRESTWSBean;

public class GerenciamentomaterialService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Vgerenciarmaterial> {

	private GerenciamentomaterialDAO gerenciamentomaterialDAO;
	private MaterialService materialService;
	private MaterialunidademedidaService materialunidademedidaService;
	private LocalarmazenagemService localarmazenagemService;
	private ParametrogeralService parametrogeralService;
	
	public void setGerenciamentomaterialDAO(GerenciamentomaterialDAO gerenciamentomaterialDAO) {
		this.gerenciamentomaterialDAO = gerenciamentomaterialDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/* singleton */
	private static GerenciamentomaterialService instance;
	public static GerenciamentomaterialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(GerenciamentomaterialService.class);
		}
		return instance;
	}
	
	public IReport gerarRelatorio(GerenciamentomaterialFiltro filtro) {
		Report report = new Report("/suprimento/gerenciamentomaterial");
		Report subreport = new Report("/suprimento/sub_totaisunidademateriais");
		String whereIn = new String();
		Map<Integer, Double> mapaMateriais = new HashMap<Integer, Double>();
		List<Vgerenciarmaterial> materiais = findForReport(filtro);
		if(materiais != null && materiais.size() > 0){
			List<Integer> listaCdmaterial = new ArrayList<Integer>();
			for(Vgerenciarmaterial vgerenciarmaterial : materiais){
				if(vgerenciarmaterial.getCdmaterial() != null){
					addCdmaterial(listaCdmaterial, vgerenciarmaterial.getCdmaterial());	
					mapaMateriais.put(vgerenciarmaterial.getCdmaterial(), vgerenciarmaterial.getQtdedisponivel());
				}
			}			
			if(listaCdmaterial != null && !listaCdmaterial.isEmpty()){				
				whereIn = CollectionsUtil.concatenate(listaCdmaterial, ",");							
				List<Material> listaMateriaisComUnidademedidaconversao = materialService.findForRelatorioComUnidademedidaconversao(whereIn, null);
				if(listaMateriaisComUnidademedidaconversao != null && !listaMateriaisComUnidademedidaconversao.isEmpty()){
					StringBuilder conversoes;
					for(Material material : listaMateriaisComUnidademedidaconversao){
						if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
							for(Vgerenciarmaterial vgerenciarmaterial : materiais){								
								if(vgerenciarmaterial.getCdmaterial() != null && vgerenciarmaterial.getCdmaterial().equals(material.getCdmaterial())){
									if(vgerenciarmaterial.getNomeunidademedida() == null || "".equals(vgerenciarmaterial.getNomeunidademedida())){
										if(material.getUnidademedida() != null)
											vgerenciarmaterial.setNomeunidademedida(material.getUnidademedida().getNome());
									}
									conversoes = new StringBuilder();
									for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
										if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao() && 
												materialunidademedida.getFracao() != null && materialunidademedida.getUnidademedida() != null){
											if("".equals(conversoes.toString())) conversoes.append("\n");
											conversoes
												.append(materialunidademedida.getUnidademedida().getNome())
												.append(" " + new DecimalFormat("#,##0.00").format(materialunidademedida.getFracaoQtdereferencia() * vgerenciarmaterial.getQtdedisponivel()))
												.append("\n");
										}
									}
									if(!"".equals(conversoes.toString())){
										vgerenciarmaterial.setConversoes(conversoes.toString());
									}
									
								}							
							}
						}
					}
				}
			}
			
			if(materiais != null && !materiais.isEmpty()){
				Collections.sort(materiais, new Comparator<Vgerenciarmaterial>(){
					public int compare(Vgerenciarmaterial a1, Vgerenciarmaterial a2){
						try {
							return a1.getNome().compareTo(a2.getNome());
						} catch (Exception e) {
							return 0;
						}
					}
				});
			}
			
			Boolean ocultarCusto = parametrogeralService.getBoolean(Parametrogeral.OCULTAR_CUSTO_ESTOQUE);
			if (!ocultarCusto){
				ocultarCusto = !SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE);
			}
			
			report.addSubReport("SUB_TOTAISUNIDADEMATERIAIS", subreport);
			report.addParameter("LISTATOTAISUNITARIOS", materialunidademedidaService.getTotaisUnidadeMedida(whereIn, mapaMateriais));
			report.addParameter("OCULTAR_CUSTO_ESTOQUE", ocultarCusto);
			report.addParameter("CONTROLE_GADO", parametrogeralService.getBoolean(Parametrogeral.CONTROLE_GADO));
			report.addParameter("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
			report.setDataSource(materiais);
		}

		return report;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.GerenciamentomaterialService#findForReport(GerenciamentomaterialFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	private List<Vgerenciarmaterial> findForReport(GerenciamentomaterialFiltro filtro) {
		filtro.setWhereInEmpresaRestricaoUsuario(null);
		filtro.setWhereInLocalRestricaoUsuario(null);
		if(filtro.getLocalarmazenagem() == null){
			filtro.setWhereInLocalRestricaoUsuario(localarmazenagemService.getWhereInLocalRestricaoUsuarioLogado());
		}
		if(filtro.getEmpresa() == null){
			filtro.setWhereInEmpresaRestricaoUsuario(new SinedUtil().getListaEmpresa());
		}
		
		return gerenciamentomaterialDAO.findForReport(filtro);
	}

	public IReport gerarRelatorioPorPeriodo(GerenciamentomaterialFiltro filtro) {
		Report report = new Report("/suprimento/gerenciamentomaterialperiodo");
		Report subreport = new Report("/suprimento/sub_totaisunidademateriais");
		Map<Integer, Double> mapaMateriais = new HashMap<Integer, Double>();
		String whereIn = new String();
		List<Vgerenciarmaterial> materiais = gerenciamentomaterialDAO.findForReportPeriodo(filtro, true);
		if(materiais != null && materiais.size() > 0){
			List<Integer> listaCdmaterial = new ArrayList<Integer>();
			for(Vgerenciarmaterial vgerenciarmaterial : materiais){
				if(vgerenciarmaterial.getCdmaterial() != null){
					addCdmaterial(listaCdmaterial, vgerenciarmaterial.getCdmaterial());	
					mapaMateriais.put(vgerenciarmaterial.getCdmaterial(), vgerenciarmaterial.getQtdedisponivel());
				}
			}
			
			if(listaCdmaterial != null && !listaCdmaterial.isEmpty()){				
				whereIn = CollectionsUtil.concatenate(listaCdmaterial, ",");				
				List<Material> listaMateriaisComUnidademedidaconversao = materialService.findForRelatorioComUnidademedidaconversao(whereIn, null);
				if(listaMateriaisComUnidademedidaconversao != null && !listaMateriaisComUnidademedidaconversao.isEmpty()){
					StringBuilder conversoes;
					for(Material material : listaMateriaisComUnidademedidaconversao){
						if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
							for(Vgerenciarmaterial vgerenciarmaterial : materiais){								
								if(vgerenciarmaterial.getCdmaterial() != null && vgerenciarmaterial.getCdmaterial().equals(material.getCdmaterial())){
									if(vgerenciarmaterial.getNomeunidademedida() == null || "".equals(vgerenciarmaterial.getNomeunidademedida())){
										if(material.getUnidademedida() != null)
											vgerenciarmaterial.setNomeunidademedida(material.getUnidademedida().getNome());
									}
									conversoes = new StringBuilder();
									for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
										if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao() && 
												materialunidademedida.getFracao() != null && materialunidademedida.getUnidademedida() != null){
											if("".equals(conversoes.toString())) conversoes.append("\n");
											conversoes
												.append(materialunidademedida.getUnidademedida().getNome())
												.append(" " + new DecimalFormat("#,##0.00").format(materialunidademedida.getFracaoQtdereferencia() * vgerenciarmaterial.getQtdedisponivel()))
												.append("\n");
										}
									}
									if(!"".equals(conversoes.toString())){
										vgerenciarmaterial.setConversoes(conversoes.toString());
									}
									
								}							
							}
						}
					}
				}
			}
			
			report.addSubReport("SUB_TOTAISUNIDADEMATERIAIS", subreport);
			report.addParameter("LISTATOTAISUNITARIOS", materialunidademedidaService.getTotaisUnidadeMedida(whereIn, mapaMateriais));
			report.addParameter("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
			
			report.setDataSource(materiais);
		}

		return report;
	}
	
	public List<Vgerenciarmaterial> findForReportPeriodo(GerenciamentomaterialFiltro filtro, boolean pdf) {
		return gerenciamentomaterialDAO.findForReportPeriodo(filtro, false);
	}
	
	private void addCdmaterial(List<Integer> lista, Integer cdmaterial){
		boolean repetido = false;
		if(!lista.isEmpty() && cdmaterial != null){			
			for(Integer codigo : lista){
				if(codigo.equals(cdmaterial)){
					repetido = true;
					break;
				}
			}			
		}
		if(!repetido){
			lista.add(cdmaterial);	
		}
	}

	/**
	 * M�todo verifica se alguma plaqueta foi preenchida para enviar msg de registro salvo para usu�rio
	 * 
	 * @param lista
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existePlaquetaPreenchida(List<Patrimonioitem> lista) {
		for (Patrimonioitem patrimonioitem : lista)
			if(!patrimonioitem.getPlaqueta().equals(""))
				return true;
		
		return false;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * Calcula o total geral
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO#findForTotalGeral(GerenciamentomaterialFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public TotalizadorGerenciamentoMaterial findForTotalGeral(GerenciamentomaterialFiltro filtro){
		return gerenciamentomaterialDAO.findForTotalGeral(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO#getQtdeTotalReservada(GerenciamentomaterialFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQtdeTotalReservada(GerenciamentomaterialFiltro filtro){
		return gerenciamentomaterialDAO.getQtdeTotalReservada(filtro);
	}
	
	public List<Vgerenciarmaterial> findForCsv(GerenciamentomaterialFiltro filtro){
		filtro.setWhereInEmpresaRestricaoUsuario(null);
		filtro.setWhereInLocalRestricaoUsuario(null);
		if(filtro.getLocalarmazenagem() == null){
			filtro.setWhereInLocalRestricaoUsuario(localarmazenagemService.getWhereInLocalRestricaoUsuarioLogado());
		}
		if(filtro.getEmpresa() == null){
			filtro.setWhereInEmpresaRestricaoUsuario(new SinedUtil().getListaEmpresa());
		}
		return gerenciamentomaterialDAO.findForCsv(filtro);
	}

	public List<Vgerenciarmaterial> findForSolicitacaocompra(String whereIngerenciamentomaterial) {
		return gerenciamentomaterialDAO.findForSolicitacaocompra(whereIngerenciamentomaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO#findForDefinirCustoInicial(String whereIn, Date dtcustoinicial)
	*
	* @param whereIn
	* @param dtcustoinicial
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public List<Material> findForDefinirCustoInicial(String whereIn, Date dtcustoinicial, Movimentacaoestoque movimentacaoestoque, Empresa empresa) {
		return gerenciamentomaterialDAO.findForDefinirCustoInicial(whereIn, null, dtcustoinicial, movimentacaoestoque, empresa);
	}
	
	public List<Material> findForDefinirCustoInicial(String whereIn, Date dtcustoinicial, Movimentacaoestoque movimentacaoestoque) {
		return findForDefinirCustoInicial(whereIn, dtcustoinicial, movimentacaoestoque, null);
	}
		
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO#findForDefinirCustoInicial(String whereIn, Date dtcustoinicial, Movimentacaoestoque movimentacaoestoque)
	*
	* @param bean
	* @param dtcustoinicial
	* @param movimentacaoestoque
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	 * @param dtFim 
	*/
	public Double getQtdeDisponivelCustoEntrada(Material bean, Date dtinicial, Date dtfim, Movimentacaoestoque movimentacaoestoque) {
		Double qtde = 0d;
		if(bean != null && bean.getCdmaterial() != null){
			List<Material> lista = gerenciamentomaterialDAO.findForDefinirCustoInicial(bean.getCdmaterial().toString(), dtinicial, dtfim, movimentacaoestoque, null);
			if(SinedUtil.isListNotEmpty(lista)){
				qtde = lista.get(0).getQtdecustoinicial();
			}
		}
		return qtde;
	}
	
	public Double getQtdeDisponivelCustoEntradaPorEmpresa(Material bean, Date dtinicial, Date dtfim, Movimentacaoestoque movimentacaoestoque, Empresa empresa) {
		if(empresa == null) {
			return getQtdeDisponivelCustoEntrada(bean, dtinicial, dtfim, movimentacaoestoque);
		} else {
			Double qtde = 0d;
			if(bean != null && bean.getCdmaterial() != null){
				List<Material> lista = gerenciamentomaterialDAO.findForDefinirCustoInicial(bean.getCdmaterial().toString(), dtinicial, dtfim, movimentacaoestoque, empresa);
				if(SinedUtil.isListNotEmpty(lista)){
					qtde = lista.get(0).getQtdecustoinicial();
				}
			}
			return qtde;
		}
	}
	
	/**
	 * 
	 *Busca os dados do material para alimentar o pop-up de sele��o de local de origem de equipamento no cadastro de colaborador
	 * 
	 * @param material
	 * @return
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 */
	public List<Vgerenciarmaterial> findForCadastroColaborador(Material material){
		return gerenciamentomaterialDAO.findForCadastroColaborador(material);
	}
	
	/**
	 * 
	 * Busca a lista de materiais e locais para a entrega de EPI
	 * 
	 * @param filtro
	 * @author Rafael Patr�cio
	 * @return List<Vgerenciarmaterial>
	 * @since 27/08/2014
	 */
	public List<Vgerenciarmaterial> loadListaGerenciarmaterialForEntregaEPI(ColaboradorFiltro filtro) {
		List<Vgerenciarmaterial> listaGerenciamentomaterial = this.findForCadastroColaborador(filtro.getMaterial());
		List<Vgerenciarmaterial> listaGerenciamentomaterialCorrigida = new ArrayList<Vgerenciarmaterial>();
		if(SinedUtil.isListNotEmpty(listaGerenciamentomaterial)){
			for(Vgerenciarmaterial vgerenciarmaterial : listaGerenciamentomaterial){
				if(vgerenciarmaterial.getQtdedisponivel() != null && vgerenciarmaterial.getQtdedisponivel() > 0){
					listaGerenciamentomaterialCorrigida.add(vgerenciarmaterial);
				}
			}
		}
		return listaGerenciamentomaterialCorrigida;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.GerenciamentomaterialDAO#existeQtdeEmEstoque(Localarmazenagem localarmazenagem)
	*
	* @param localarmazenagem
	* @return
	* @since 18/11/2016
	* @author Luiz Fernando
	*/
	public boolean existeQtdeEmEstoque(Localarmazenagem localarmazenagem) {
		return gerenciamentomaterialDAO.existeQtdeEmEstoque(localarmazenagem);
	}
	
	public EstoqueW3producaoRESTModel getEstoqueForW3Producao(EstoqueW3producaoRESTWSBean estoqueW3producaoRESTWSBean) {
		EstoqueW3producaoRESTModel estoqueW3producaoRESTModel = new EstoqueW3producaoRESTModel();
		List<EstoqueMaterialW3producaoRESTModel> listaEstoqueMaterialW3producaoRESTModel = new ArrayList<EstoqueMaterialW3producaoRESTModel>();
		List<Vgerenciarmaterial> listaVgerenciarmaterial = gerenciamentomaterialDAO.findForW3Producao(estoqueW3producaoRESTWSBean);
		
		for (Vgerenciarmaterial vgerenciarmaterial: listaVgerenciarmaterial){
			EstoqueMaterialW3producaoRESTModel estoqueMaterialW3producaoRESTModel = new EstoqueMaterialW3producaoRESTModel();
			estoqueMaterialW3producaoRESTModel.setCdmaterial(vgerenciarmaterial.getCdmaterial());
			estoqueMaterialW3producaoRESTModel.setQtdedisponivel(vgerenciarmaterial.getQtdedisponivel());
			listaEstoqueMaterialW3producaoRESTModel.add(estoqueMaterialW3producaoRESTModel);
		}
		
		estoqueW3producaoRESTModel.setEstoqueMaterialRESTModelArray(listaEstoqueMaterialW3producaoRESTModel.toArray(new EstoqueMaterialW3producaoRESTModel[listaEstoqueMaterialW3producaoRESTModel.size()]));
		
		return estoqueW3producaoRESTModel;
	}
	public String criarDimentacao(Double largura, Double altura,Double comprimento) {
		String [] ordem = parametrogeralService.getValorPorNome(Parametrogeral.ORDEM_DIMENSAOVENDA).split(",");
		String dimenssao = "";
		for (String str : ordem) {
			if(!dimenssao.equals("")){
				dimenssao = dimenssao.concat("x");
			}
			if(str.equals("altura") && altura != null){
				dimenssao = dimenssao.concat(altura.toString().replace(".", ","));
			}else if(str.equals("comprimento") && comprimento != null){
				dimenssao = dimenssao.concat(comprimento.toString().replace(".", ","));
			}else if(str.equals("largura") && largura != null){
				dimenssao = dimenssao.concat(largura.toString().replace(".", ","));
			}
		}
		return dimenssao;
	}
}
