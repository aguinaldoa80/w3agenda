package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwcolaboradoretiqueta;
import br.com.linkcom.sined.geral.dao.VwcolaboradoretiquetaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwcolaboradoretiquetaService extends GenericService<Vwcolaboradoretiqueta>{
	 private VwcolaboradoretiquetaDAO vwcolaboradoretiquetaDAO;
	 
	 public void setVwcolaboradoretiquetaDAO(VwcolaboradoretiquetaDAO vwcolaboradoretiquetaDAO) {this.vwcolaboradoretiquetaDAO = vwcolaboradoretiquetaDAO;}
	 
	 public List<Vwcolaboradoretiqueta> findForColaboradorEtiqueta(String whereIn){
		 return vwcolaboradoretiquetaDAO.findForColaboradorEtiqueta(whereIn);
	 }
}
