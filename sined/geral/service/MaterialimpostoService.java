package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialimposto;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.geral.bean.Tabelaimpostoitem;
import br.com.linkcom.sined.geral.dao.MaterialimpostoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialimpostoService extends GenericService<Materialimposto>{
	
	private MaterialimpostoDAO materialimpostoDAO;
	private TabelaimpostoService tabelaimpostoService;
	private EmpresaService empresaService;
	private MaterialService materialService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setTabelaimpostoService(
			TabelaimpostoService tabelaimpostoService) {
		this.tabelaimpostoService = tabelaimpostoService;
	}	
	public void setMaterialimpostoDAO(MaterialimpostoDAO materialimpostoDAO) {
		this.materialimpostoDAO = materialimpostoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	/* singleton */
	private static MaterialimpostoService instance;
	public static MaterialimpostoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialimpostoService.class);
		}
		return instance;
	}
	
	/**
	 * Calcula e salva os impostos do material baseado nas tabelas importadas
	 *
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public void calculaByMaterialInTabelaimposto(Material material) {
		this.deleteByMaterial(material);
		
		String codigonbs = material.getCodigonbs();
		String codigoitem = material.getCodlistaservico();
		String ncmcompleto = material.getNcmcompleto();
		String extipi = material.getExtipi();
		
		if(!StringUtils.isBlank(ncmcompleto)) ncmcompleto = br.com.linkcom.neo.util.StringUtils.stringCheia(ncmcompleto, "0", 9, false);
		if(!StringUtils.isBlank(codigoitem)) codigoitem = br.com.linkcom.neo.util.StringUtils.stringCheia(codigoitem, "0", 9, false);
		if(!StringUtils.isBlank(codigonbs)) codigonbs = br.com.linkcom.neo.util.StringUtils.stringCheia(codigonbs, "0", 9, false);
		
		List<Materialimposto> listaMaterialimposto = new ArrayList<Materialimposto>();
		
		List<Empresa> listaEmpresa = empresaService.findAtivosSemFiltro();
		for (Empresa empresa : listaEmpresa) {
			boolean servico = material.getServico() != null && material.getServico();
			if(servico){
				Tabelaimposto tabelaimposto = tabelaimpostoService.getTabelaVigenteByEmpresa(empresa, new String[]{codigonbs, codigoitem}, null, true);
				if(tabelaimposto != null){
					Tabelaimpostoitem tabelaimpostoitemNBS = tabelaimpostoService.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemServicoNBS(), codigonbs);
					if(tabelaimpostoitemNBS != null){
						Materialimposto materialimposto = new Materialimposto();
						materialimposto.setEmpresa(empresa);
						materialimposto.setImportadofederal(tabelaimpostoitemNBS.getImportadofederal());
						materialimposto.setNacionalfederal(tabelaimpostoitemNBS.getNacionalfederal());
						materialimposto.setEstadual(tabelaimpostoitemNBS.getEstadual());
						materialimposto.setMunicipal(tabelaimpostoitemNBS.getMunicipal());
						listaMaterialimposto.add(materialimposto);
						
						continue;
					}
					
					Tabelaimpostoitem tabelaimpostoitemItem = tabelaimpostoService.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemServicoItem(), codigoitem);
					if(tabelaimpostoitemItem != null){
						Materialimposto materialimposto = new Materialimposto();
						materialimposto.setEmpresa(empresa);
						materialimposto.setImportadofederal(tabelaimpostoitemItem.getImportadofederal());
						materialimposto.setNacionalfederal(tabelaimpostoitemItem.getNacionalfederal());
						materialimposto.setEstadual(tabelaimpostoitemItem.getEstadual());
						materialimposto.setMunicipal(tabelaimpostoitemItem.getMunicipal());
						listaMaterialimposto.add(materialimposto);
						
						continue;
					}
				}
			} else {
				Tabelaimposto tabelaimposto = tabelaimpostoService.getTabelaVigenteByEmpresa(empresa, new String[]{ncmcompleto}, extipi, true);
				if(tabelaimposto != null){
					Tabelaimpostoitem tabelaimpostoitemProduto = tabelaimpostoService.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemProduto(), ncmcompleto);
					if(tabelaimpostoitemProduto != null){
						Materialimposto materialimposto = new Materialimposto();
						materialimposto.setEmpresa(empresa);
						materialimposto.setImportadofederal(tabelaimpostoitemProduto.getImportadofederal());
						materialimposto.setNacionalfederal(tabelaimpostoitemProduto.getNacionalfederal());
						materialimposto.setEstadual(tabelaimpostoitemProduto.getEstadual());
						materialimposto.setMunicipal(tabelaimpostoitemProduto.getMunicipal());
						listaMaterialimposto.add(materialimposto);
						
						continue;
					}
				}
			}
		}
		
		if(listaMaterialimposto != null && listaMaterialimposto.size() > 0){
			for (Materialimposto materialimposto : listaMaterialimposto) {
				materialimposto.setMaterial(material);
				this.saveOrUpdate(materialimposto);
			}
		}
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param material
	 * @param whereNotIn
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public void deleteByMaterialWhereNotIn(Material material, String whereNotIn) {
		materialimpostoDAO.deleteByMaterialWhereNotIn(material, whereNotIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public void deleteByMaterial(Material material) {
		materialimpostoDAO.deleteByMaterial(material);
	}
	
	public void atualizarImpostoIBPTMaterial(){
		List<Material> listaMaterial = materialService.findForAtualizacaoTributacao();
		for (Material material : listaMaterial) {
			calculaByMaterialInTabelaimposto(material);
		}
	}
}