package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

/**
 * Classe respons�vel por manipular as movimenta��es com v�nculo em cart�o de cr�dito, agrupando-as para gerar o relat�rio
 * de fluxo de caixa.
 * 
 * @author Fl�vio Tavares
 */
@Bean
public class MovimentacaoCartaocreditoService {

	static final int MES = Calendar.MONTH;
	static final int DIA = Calendar.DAY_OF_MONTH;
	
	/**
	 * M�todo para retornar a lista de movimenta��es pronta para o relat�rio de fluxo de caixa.
	 * 
	 * @see #agrupaMovPorContaReferencia(List, br.com.linkcom.sined.geral.service.MovimentacaoCartaocreditoService.Periodo)
	 * @param filtro
	 * @param listaMovCartao
	 * @return
	 */
	public List<Movimentacao> processaListaMovCartoesForFluxocaixa(FluxocaixaFiltroReport filtro, List<Movimentacao> listaMovCartao){
		Periodo periodo = new Periodo(filtro.getPeriodoDe(), filtro.getPeriodoAte());
		return this.agrupaMovPorContaReferencia(listaMovCartao, periodo);
	}
		
	/**
	 * M�todo para agrupar as movimenta��es e retornando somente uma com a soma dos cr�ditos e 
	 * dos d�bitos das movimenta��es na lista de acordo com o tipo de opera��o de cada uma.
	 * 
	 * @param referencia
	 * @param lista
	 * @return
	 */
	private Movimentacao agrupaMovimentacoesSomandoValores(Date referencia,	List<Movimentacao> lista) {
		Movimentacao mov = null;
		if(SinedUtil.isListNotEmpty(lista)){
			mov = lista.get(0);
			mov.setDtbanco(referencia);
			mov.setMovCartao(true);
			if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao())){
				mov.setCredito(mov.getValor());
			}
			if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao())){
				mov.setDebito(mov.getValor());
			}
			for (int i = 1; i< lista.size(); i++) {
				Movimentacao get = lista.get(i);
				get.setMovCartao(true);
				if(Tipooperacao.TIPO_CREDITO.equals(get.getTipooperacao())){
					mov.setCredito(mov.getCredito().add(get.getValor()));
				}
				if(Tipooperacao.TIPO_DEBITO.equals(get.getTipooperacao())){
					mov.setDebito(mov.getDebito().add(get.getValor()));
				}
			}
		}
		return mov;
	}
	
	/**
	 * M�todo respons�vel por agrupar as movimenta��es por Conta e por data de refer�ncia.
	 * 
	 * @see #agrupaMovimentacoesPorConta(List)
	 * @see #retornaMovimentacoesPorConta(br.com.linkcom.sined.geral.service.MovimentacaoCartaocreditoService.Periodo, Conta, List)
	 * 
	 * @param listaMovCartao
	 * @param periodoFiltro
	 * @return
	 */
	private List<Movimentacao> agrupaMovPorContaReferencia(List<Movimentacao> listaMovCartao, Periodo periodoFiltro){
		
		List<Movimentacao> movPorContaReferencia = new ArrayList<Movimentacao>();
		
		Map<Conta, List<Movimentacao>> grupoMovimentacao = this.agrupaMovimentacoesPorConta(listaMovCartao);
		for (Entry<Conta, List<Movimentacao>> entry : grupoMovimentacao.entrySet()) {
			Conta conta = entry.getKey();
			List<Movimentacao> lista = entry.getValue();
			Collections.sort(lista);
			List<Movimentacao> movPorReferencia = this.retornaMovimentacoesPorConta(periodoFiltro, conta, lista);
			movPorContaReferencia.addAll(movPorReferencia);
		}
		return movPorContaReferencia;
	}
	
	/**
	 * M�todo para obter as movimenta��es por conta que est�o no per�odo informado.
	 * Este m�todo cria todos os per�odos da conta e agrupa as movimenta��es cuja data
	 * est� entre alguma data dos per�odos gerados.
	 * 
	 * @see #agrupaMovimentacoesSomandoValores(Date, List)
	 * @see Periodo#criaListaPeriodoFechamento(Conta)
	 * @see Periodo#findMovEntrePeriodo(List)
	 * 
	 * @param periodoFiltro
	 * @param conta
	 * @param listaMovConta
	 * @return 
	 */
	private List<Movimentacao> retornaMovimentacoesPorConta(Periodo periodoFiltro, Conta conta, List<Movimentacao> listaMovConta) {
		List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();
		
		// Cria uma lista com os per�odos de fechamento da conta.
		List<Periodo> listaPeriodoFechamento = periodoFiltro.criaListaPeriodoFechamento(conta);
		
		 // Varre a lista de per�odos buscando as movimenta��es cujas datas est�o dentro do per�odo em quest�o.
		for (Periodo p : listaPeriodoFechamento) {
			Date referencia = p.getReferencia();
			
			// Busca as movimenta�es que est�o entre o per�odo
			List<Movimentacao> movMatchesPeriod = p.findMovEntrePeriodo(listaMovConta);
			
			if(SinedUtil.isListNotEmpty(movMatchesPeriod)){ // se foi encontrado alguma movimenta��o 
				// Agrupa as movimenta��es encontradas e adiciona ao map.
				Movimentacao movimentacaoAgrupada = this.agrupaMovimentacoesSomandoValores(referencia, movMatchesPeriod);
				listaMovimentacao.add(movimentacaoAgrupada);
			}
		}
		
		return listaMovimentacao;
	}
	
	/**
	 * M�todo para agrupar uma lista de movimenta��es por conta. Retorna um Map cuja chave � uma conta e o
	 * value � a lista de movimenta��es correspondentes �quela conta.
	 * 
	 * @param listaMovimentacao
	 * @return
	 */
	private Map<Conta, List<Movimentacao>> agrupaMovimentacoesPorConta(List<Movimentacao> listaMovimentacao){
		Map<Conta, List<Movimentacao>> contas = new HashMap<Conta, List<Movimentacao>>();
		for (Movimentacao m : listaMovimentacao) {
			Conta conta = m.getConta();
			if(contas.containsKey(conta)){
				contas.get(conta).add(m);
			}else{
				List<Movimentacao> listaAux = new ArrayList<Movimentacao>();
				listaAux.add(m);
				contas.put(conta, listaAux);
			}
		}
		return contas;
	}
	
	
	/**
	 * Classe auxiliar respons�vel por controlar os per�odos das movimenta��es ao gerar o relat�rio de fluxo de caixa.
	 * 
	 * @author Fl�vio Tavares
	 */
	class Periodo{
		/**
		 * Data limite inicial
		 */
		private Date de;
		/**
		 * Data limite final
		 */
		private Date ate;
		/**
		 * Data em que ser� lan�ada as movimenta��es no relat�rio. 
		 */
		private Date referencia;
		
		public Periodo(Date de, Date ate) {
			if(SinedDateUtils.afterIgnoreHour(de,ate)){
				throw new IllegalArgumentException("A data de deve ser anterior � data ate.");
			}
			this.de = de;
			this.ate = ate;
		}
		
		public Date getDe() {
			return de;
		}
		public Date getAte() {
			return ate;
		}
		public Date getReferencia() {
			return referencia;
		}
		public Periodo setReferencia(Date referencia) {
			this.referencia = referencia;
			return this;
		}
		
		/**
		 * M�todo para criar uma lista de per�odos de fechamento de uma determinada conta
		 * levando em considera��o que o objeto corrente do per�odo refere-se ao per�odo m�ximo 
		 * de quando as movimenta��es ocorreram, ou seja, deve ser o per�odo do filtro.
		 * Esta lista de per�odos � para separar as movimenta��es cuja data est� entre a 
		 * data de e a data at� de algum per�odo da lista de per�odos. Cada lista � gerada para
		 * cada conta e s� deve ser utilizada para comparar as movimenta��es cuja conta seja igual
		 * � que foi usada para gerar a lista de per�odos.
		 * 
		 * <pre>
		 * Exemplos:
		 * Considere o filtro como sendo de 01/01/2008 � 31/03/2008.
		 * 
		 * Conta 1:
		 * Considere uma conta com dia de fechamento 10 e dia de vencimento 15, 
		 * ser� retornado uma lista com os seguintes per�odos:
		 *	De: 11/12/2007	At�: 10/01/2008	Refer�ncia: 15/01/2008
		 * 	De: 11/01/2007	At�: 10/02/2008	Refer�ncia: 15/02/2008
		 * 	De: 11/02/2007	At�: 10/03/2008	Refer�ncia: 15/03/2008
		 * 
		 * Conta 2:
		 * Agora considere uma conta com dia de fechamento 15 e dia de vencimento 10, 
		 * ser� retornado uma lista com os seguintes per�odos:
		 *	De: 16/11/2007	At�: 15/12/2008	Refer�ncia: 10/01/2008
		 * 	De: 16/12/2007	At�: 15/01/2008	Refer�ncia: 10/02/2008
		 * 	De: 16/01/2007	At�: 15/02/2008	Refer�ncia: 10/03/2008
		 * </pre>
		 * 
		 * @see #criaListaVencimentoMaior(Conta)
		 * @see #criaListaFechamentoMaior(Conta)
		 * 
		 * @param conta
		 * @return
		 */
		public List<Periodo> criaListaPeriodoFechamento(Conta conta){
			int diaV = conta.getDiavencimento(), diaF = conta.getDiafechamento();
			if(diaV > diaF){
				return criaListaVencimentoMaior(conta);
			}else{
				return criaListaFechamentoMaior(conta);
			}
		}
		
		/**
		 * M�todo para criar uma lista de per�odos de contas cuja data de vencimento seja
		 * maior que a data de fechamento.
		 * 
		 * @param conta
		 * @return
		 */
		private List<Periodo> criaListaVencimentoMaior(Conta conta){
			int diaV = conta.getDiavencimento(), diaF = conta.getDiafechamento();
			List<Periodo> list = new ArrayList<Periodo>(1);
			Date proxVencimento = this.proximaDataReferencia(diaV, de);
			
			while(SinedDateUtils.beforeIgnoreHour(proxVencimento, ate)){// enquanto proxVencimento <= ate
				Date dtFechamentoFim = SinedDateUtils.setDateProperty(proxVencimento, diaF, DIA);
				int mesF = SinedDateUtils.getDateProperty(dtFechamentoFim, MES);
				Date dtFechamentoInicio = SinedDateUtils.setDateProperty(dtFechamentoFim, mesF-1, MES);
				dtFechamentoInicio = SinedDateUtils.setDateProperty(dtFechamentoInicio, diaF+1, DIA);
				
				list.add(new Periodo(dtFechamentoInicio, dtFechamentoFim).setReferencia(proxVencimento));
				
				proxVencimento = this.proximaDataReferencia(diaV, proxVencimento);
			}
			return list;
		}

		/**
		 * M�todo para criar uma lista de per�odos de contas cuja data de fechamento seja
		 * maior que a data de vencimento.
		 * 
		 * @see #proximaDataReferencia(int, Date)
		 * @param conta
		 * @return
		 */
		private List<Periodo> criaListaFechamentoMaior(Conta conta) {
			int diaV = conta.getDiavencimento(), diaF = conta.getDiafechamento();
			List<Periodo> list = new ArrayList<Periodo>(1);
			Date proxVencimento = this.proximaDataReferencia(diaV, de);
			
			while(SinedDateUtils.beforeIgnoreHour(proxVencimento, ate)){// enquanto proxVencimento <= ate
				Date dtFechamentoFim = SinedDateUtils.setDateProperty(proxVencimento, diaF, DIA);
				dtFechamentoFim = SinedDateUtils.incrementDate(dtFechamentoFim, -1, MES);
				int mesF = SinedDateUtils.getDateProperty(dtFechamentoFim, MES);
				Date dtFechamentoInicio = SinedDateUtils.setDateProperty(dtFechamentoFim, mesF-1, MES);
				dtFechamentoInicio = SinedDateUtils.setDateProperty(dtFechamentoInicio, diaF+1, DIA);
				
				list.add(new Periodo(dtFechamentoInicio, dtFechamentoFim).setReferencia(proxVencimento));
				
				proxVencimento = this.proximaDataReferencia(diaV, proxVencimento);
			}
			return list;
		}
		
		/**
		 * M�todo para criar uma data com determinado dia e com refer�ncia em uma outra data.
		 * � utilizado para criar as datas do per�odo de fechamento.
		 * 
		 * <pre>
		 * Utiliza��o:
		 * 	proximaDataReferencia(10, 20/01/2008) retorna 10/02/2008
		 * 	proximaDataReferencia(10, 05/01/2008) retorna 10/01/2008
		 * </pre>
		 * 
		 * @param dia
		 * @param data
		 * @return
		 */
		private Date proximaDataReferencia(int dia, Date data){
			Date aux = SinedDateUtils.setDateProperty(data, dia, DIA);
			
			if(!aux.after(data)){ // Se aux <= data
				aux = SinedDateUtils.incrementDate(aux, 1, MES);
			}
			
			return aux;
		}
		
		/**
		 * M�todo para encontrar em uma lista, as movimenta��es que se encaixam no per�odo da pr�pria classe, ou seja,
		 * entre a data <tt>de</tt> e a data <tt>ate</tt>.
		 * 
		 * @param listaMovimentacao
		 * @return
		 */
		public List<Movimentacao> findMovEntrePeriodo(List<Movimentacao> listaMovimentacao){
			List<Movimentacao> matches = new ArrayList<Movimentacao>();
			for (Movimentacao m : listaMovimentacao) {
				Date dtmov = m.getDtmovimentacao();
				
				if(!SinedDateUtils.beforeIgnoreHour(dtmov, this.de) 
						&& !SinedDateUtils.afterIgnoreHour(dtmov, this.ate)){
					
					matches.add(m);
				}
			}
			return matches;
		}
		
		
		@Override
		public String toString() {
			return "De: " + SinedDateUtils.toString(de) +
					" At�: " + SinedDateUtils.toString(ate) +
					(referencia != null ? " Refer�ncia: " + SinedDateUtils.toString(referencia) : "");
		}
	}
}
