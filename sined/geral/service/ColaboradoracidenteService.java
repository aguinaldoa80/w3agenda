package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradoracidente;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradoracidenteService extends GenericService<Colaboradoracidente> {	
	
	/* singleton */
	private static ColaboradoracidenteService instance;
	public static ColaboradoracidenteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradoracidenteService.class);
		}
		return instance;
	}
	
}
