package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicohistorico;
import br.com.linkcom.sined.geral.bean.Contratojuridicoparcela;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.enumeration.Contratojuridicoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohonorario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ContratojuridicoDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratojuridicoRTF;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratojuridicoService extends GenericService<Contratojuridico> {

	private PrazopagamentoitemService prazopagamentoitemService;
	private ContratojuridicoDAO contratojuridicoDAO;
	private DocumentotipoService documentotipoService;
	private RateioitemService rateioitemService;
	private RateioService rateioService;
	private DocumentoService documentoService;
	private DocumentoorigemService documentoorigemService;
	private ContratojuridicohistoricoService contratojuridicohistoricoService;
	private DocumentohistoricoService documentohistoricoService;
	private ContratojuridicoparcelaService contratojuridicoparcelaService;
	private ContaService contaService;
	
	public void setContratojuridicoparcelaService(
			ContratojuridicoparcelaService contratojuridicoparcelaService) {
		this.contratojuridicoparcelaService = contratojuridicoparcelaService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setContratojuridicohistoricoService(
			ContratojuridicohistoricoService contratojuridicohistoricoService) {
		this.contratojuridicohistoricoService = contratojuridicohistoricoService;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}	
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setContratojuridicoDAO(ContratojuridicoDAO contratojuridicoDAO) {
		this.contratojuridicoDAO = contratojuridicoDAO;
	}
	public void setPrazopagamentoitemService(
			PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
	public List<String> geraParcelamento(Contratojuridico contratojuridico) {
		List<String> datas = new ArrayList<String>();
		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(contratojuridico.getPrazopagamento());
		
		Date data = contratojuridico.getDtinicio() == null ? new Date(System.currentTimeMillis()) : contratojuridico.getDtinicio();
		Date dataAux;
		for (Prazopagamentoitem prazopagamentoitem : listaPrazoItem) {
			dataAux = null;
			
			if(prazopagamentoitem.getDias() != null)
				dataAux = SinedDateUtils.addDiasData(data, prazopagamentoitem.getDias());
			if(prazopagamentoitem.getMeses() != null)
				dataAux = SinedDateUtils.addMesData(data, prazopagamentoitem.getMeses());
			if(dataAux != null)
				datas.add(new SimpleDateFormat("dd/MM/yyyy").format(dataAux));
		}
		
		return datas;
	}
	
	@Override
	public void saveOrUpdate(Contratojuridico bean) {
		super.saveOrUpdate(bean);
		this.updateAux(bean.getCdcontratojuridico());
	}
	
	public void updateAux(Integer cdcontratojuridico) {
		contratojuridicoDAO.updateAux(cdcontratojuridico);
	}
	
	public void updateTudo() {
		contratojuridicoDAO.updateTudo();
	}
	
	/* singleton */
	private static ContratojuridicoService instance;
	public static ContratojuridicoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContratojuridicoService.class);
		}
		return instance;
	}
	
	public List<Contratojuridico> findForReceita(String whereIn) {
		return contratojuridicoDAO.findForReceita(whereIn);
	}
	
	public void gerarReceita(Contratojuridico contratojuridico) {
		Documento documento = this.criaContareceberReceita(contratojuridico);
		this.saveContareceberReceitaHonorarios(documento, Contratojuridicoacao.COSOLIDADO);
		this.atualizaContrato(contratojuridico);
	}
	
	private void atualizaContrato(Contratojuridico contratojuridico) {
		contratojuridicoparcelaService.updateParcelaCobrada(contratojuridico);
		this.updateAux(contratojuridico.getCdcontratojuridico());
	}
	
	private void saveContareceberReceitaHonorarios(Documento documento, Contratojuridicoacao acao) {
		Contratojuridico contratojuridico = documento.getContratojuridico();
		
		documentoService.saveOrUpdate(documento);
		if(contratojuridico != null && contratojuridico.getCdcontratojuridico() != null && documento.getCddocumento() != null){
			Documentoorigem documentoorigem = new Documentoorigem();
			documentoorigem.setDocumento(documento);
			documentoorigem.setContrato(documento.getContrato());
			documentoorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			documentoorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));	
			
			documentoorigemService.saveOrUpdate(documentoorigem);
		}
		
		Contratojuridicohistorico historico = new Contratojuridicohistorico();
		historico.setAcao(acao);
		historico.setContratojuridico(documento.getContratojuridico());
		historico.setObservacao("Gerada a conta <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>");
		contratojuridicohistoricoService.saveOrUpdate(historico);
		
		Documentohistorico documentohistorico = new Documentohistorico();
		
		documentohistorico.setObservacao("Faturamento do contrato <a href=\"javascript:visualizarContratojuridico("+contratojuridico.getCdcontratojuridico()+")\">"+contratojuridico.getCdcontratojuridico()+"</a>");
		documentohistorico.setDocumento(documento);
		documentohistorico.setValor(documento.getValor());
		documentohistorico.setDocumento(documento);
		documentohistorico.setDocumentoacao(documento.getDocumentoacao());
		documentohistorico.setDocumentotipo(documento.getDocumentotipo());
		documentohistorico.setDtemissao(documento.getDtemissao());
		documentohistorico.setDtvencimento(documento.getDtvencimento());
		documentohistorico.setTipopagamento(documento.getTipopagamento());
		documentohistorico.setOutrospagamento(documento.getOutrospagamento());
		documentohistorico.setNumero(documento.getNumero());
		documentohistorico.setDescricao(documento.getDescricao());
		documentohistorico.setPessoa(documento.getPessoa());
		documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		documentohistoricoService.saveOrUpdate(documentohistorico);
	}
	
	private Documento criaContareceberReceita(Contratojuridico contratojuridico) {
		Documento documento = new Documento();
		
		documento.setContratojuridico(contratojuridico);
		
		Documentotipo documentotipo = documentotipoService.findForFaturamentoContareceber();
		documento.setDocumentotipo(documentotipo);
		
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDtemissao(SinedDateUtils.currentDate());
		documento.setDtvencimento(SinedDateUtils.getProximaDataUtil(contratojuridico.getAux_contratojuridico().getDtproximovencimento(), contratojuridico.getEmpresa()));
		documento.setEmpresa(contratojuridico.getEmpresa());
		documento.setConta(contratojuridico.getConta());
		documento.setPessoa(contratojuridico.getCliente());
		documento.setCliente(contratojuridico.getCliente());
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setDescricao(contratojuridico.getDescricao());
		
		List<Contratojuridicoparcela> listaParcela = contratojuridico.getListaContratojuridicoparcela();
		if(listaParcela != null){
			Contratojuridicoparcela parcela = this.getParcelaAtual(listaParcela);
			documento.setValor(parcela.getValor());
		}
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		documento.setAcaohistorico(Documentoacao.PREVISTA);
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		if(contratojuridico.getRateio() != null && contratojuridico.getRateio().getListaRateioitem() != null){
			listaRateioitem.addAll(contratojuridico.getRateio().getListaRateioitem());
			for (Rateioitem ri : listaRateioitem) {
				ri.setCdrateioitem(null);
			}
		}
			
		if(contratojuridico.getTaxa() != null && contratojuridico.getTaxa().getListaTaxaitem() != null){
			listaTaxaitem.addAll(contratojuridico.getTaxa().getListaTaxaitem());
			for (Taxaitem ti : listaTaxaitem) {
				ti.setCdtaxaitem(null);
			}
		}
		
		rateioitemService.agruparItensRateio(listaRateioitem);
		rateio.setListaRateioitem(listaRateioitem);
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		
		String msg;
		String msg1 = "";
		String msg2 = "";
		
		for (Taxaitem taxaitem : listaTaxaitem) {
			taxaitem.setDtlimite(documento.getDtvencimento());
			
			taxaitem.setGerarmensagem(true);
			
			msg = 	taxaitem.getTipotaxa().getNome() + 
					" de " + 
					(taxaitem.isPercentual() ? "" : "R$") + 
					taxaitem.getValor().toString() + 
					(taxaitem.isPercentual() ? "%" : "") + 
					ateApos(taxaitem.getTipotaxa().getCdtipotaxa())+
					SinedDateUtils.toString(documento.getDtvencimento()) +
					". ";
			
			if((msg1+msg).length() <= 80){
				msg1 += msg;
			} else if((msg2+msg).length() <= 80){
				msg2 += msg;
			}
		}
		
		documento.setMensagem2(msg1);
		documento.setMensagem3(msg2);
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		
		documento.setRateio(rateio);
		documento.setTaxa(taxa);
		
		
		if (contratojuridico.getConta() != null){
			Conta conta = contaService.carregaContaComMensagem(contratojuridico.getConta(), null);
			
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			if(conta.getContacarteira().getMsgboleto1() != null)
				documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			
			if(conta.getContacarteira().getMsgboleto2() != null)
				documento.setMensagem5(conta.getContacarteira().getMsgboleto2());
		}
				
		return documento;
	}
	
	private String ateApos(Integer cdtipotaxa){
		if(cdtipotaxa == 3 || cdtipotaxa == 4){
			return " at� ";
		}
		return " ap�s ";
	}
	
	private Contratojuridicoparcela getParcelaAtual(List<Contratojuridicoparcela> listaContratoparcela) {
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			Contratojuridicoparcela contratoparcela = null;
			for (Contratojuridicoparcela cp : listaContratoparcela) {
				if(cp.getCobrada() == null || !cp.getCobrada()){
					if(contratoparcela == null || SinedDateUtils.beforeIgnoreHour(cp.getDtvencimento(), contratoparcela.getDtvencimento())){
						contratoparcela = cp;
					}
				}
			}
			return contratoparcela;
		}
		return null;
	}
	
	public void executarSentenca(Contratojuridico contratojuridico) {
		this.atualizaDataSentenca(contratojuridico);
		this.updateAux(contratojuridico.getCdcontratojuridico());
		
		if(contratojuridico.getValorcausa() != null && contratojuridico.getValorcausa().getValue().doubleValue() > 0 && 
				contratojuridico.getTipohonorario() != null && 
				contratojuridico.getHonorario() != null && contratojuridico.getHonorario().getValue().doubleValue() > 0){
			Documento documento = this.criaContareceberHonorarios(contratojuridico);
			this.saveContareceberReceitaHonorarios(documento, Contratojuridicoacao.SENTENCA);
		} else {
			Contratojuridicohistorico historico = new Contratojuridicohistorico();
			historico.setAcao(Contratojuridicoacao.SENTENCA);
			historico.setContratojuridico(contratojuridico);
			historico.setObservacao("Senten�a sem honor�rios");
			contratojuridicohistoricoService.saveOrUpdate(historico);
		}
	}
	
	private Documento criaContareceberHonorarios(Contratojuridico contratojuridico) {
		Documento documento = new Documento();
		
		documento.setContratojuridico(contratojuridico);
		
		Documentotipo documentotipo = documentotipoService.findForFaturamentoContareceber();
		documento.setDocumentotipo(documentotipo);
		
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDtemissao(SinedDateUtils.currentDate());
		documento.setDtvencimento(SinedDateUtils.getProximaDataUtil(contratojuridico.getAux_contratojuridico().getDtproximovencimento(), contratojuridico.getEmpresa()));
		documento.setEmpresa(contratojuridico.getEmpresa());
		documento.setConta(contratojuridico.getConta());
		documento.setPessoa(contratojuridico.getCliente());
		documento.setCliente(contratojuridico.getCliente());
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setDescricao(contratojuridico.getDescricao() + " - Honor�rios");
		
		Money valor = contratojuridico.getHonorario();
		if(contratojuridico.getTipohonorario().equals(Tipohonorario.PERCENTUAL)){
			valor = contratojuridico.getValorcausa().multiply(contratojuridico.getHonorario().divide(new Money(100d)));
		}
		documento.setValor(valor);
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		documento.setAcaohistorico(Documentoacao.PREVISTA);
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		if(contratojuridico.getRateio() != null && contratojuridico.getRateio().getListaRateioitem() != null){
			listaRateioitem.addAll(contratojuridico.getRateio().getListaRateioitem());
			for (Rateioitem ri : listaRateioitem) {
				ri.setCdrateioitem(null);
			}
		}
			
		if(contratojuridico.getTaxa() != null && contratojuridico.getTaxa().getListaTaxaitem() != null){
			listaTaxaitem.addAll(contratojuridico.getTaxa().getListaTaxaitem());
			for (Taxaitem ti : listaTaxaitem) {
				ti.setCdtaxaitem(null);
			}
		}
		
		rateioitemService.agruparItensRateio(listaRateioitem);
		rateio.setListaRateioitem(listaRateioitem);
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		
		String msg;
		String msg1 = "";
		String msg2 = "";
		
		for (Taxaitem taxaitem : listaTaxaitem) {
			taxaitem.setDtlimite(documento.getDtvencimento());
			
			taxaitem.setGerarmensagem(true);
			
			msg = 	taxaitem.getTipotaxa().getNome() + 
					" de " + 
					(taxaitem.isPercentual() ? "" : "R$") + 
					taxaitem.getValor().toString() + 
					(taxaitem.isPercentual() ? "%" : "") + 
					ateApos(taxaitem.getTipotaxa().getCdtipotaxa())+
					SinedDateUtils.toString(documento.getDtvencimento()) +
					". ";
			
			if((msg1+msg).length() <= 80){
				msg1 += msg;
			} else if((msg2+msg).length() <= 80){
				msg2 += msg;
			}
		}
		
		documento.setMensagem2(msg1);
		documento.setMensagem3(msg2);
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		
		documento.setRateio(rateio);
		documento.setTaxa(taxa);
		
		if (contratojuridico.getConta() != null){
			Conta conta = contaService.carregaContaComMensagem(contratojuridico.getConta(), null);
					
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			if(conta.getContacarteira().getMsgboleto1() != null)
				documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			
			if(conta.getContacarteira().getMsgboleto2() != null)
				documento.setMensagem5(conta.getContacarteira().getMsgboleto2());
		}
		
		return documento;
	}
	
	public void atualizaDataSentenca(Contratojuridico contratojuridico) {
		contratojuridicoDAO.atualizaDataSentenca(contratojuridico);
	}
	
	/**
	 * M�todo que retorna o contrato para a emiss�o do relatorio, baseado no modelo RTF
	 * 
	 * @param contratojuridico
 	 * @author Thiago Clemente
	**/
	public Contratojuridico loadForEmissaoContratoRTF(Contratojuridico contratojuridico) {
		return contratojuridicoDAO.loadForEmissaoContratoRTF(contratojuridico);
	}
	
	/**
	 *	M�todo que busca o contratojuridico do banco e logo depois transforma o Contrato em um ContratojuridicoRTF.
	 *
	 * @param contratojuridico
	 * @return
	 * @since 19/10/2011
	 * @author Rodrigo Freitas
	 */
	public ContratojuridicoRTF makeContratojuridicoRTF(Contratojuridico contratojuridico) {
		contratojuridico = this.loadForEmissaoContratoRTF(contratojuridico);
		return contratojuridico.getContratojuridicoRTF();
	}
	
	public boolean haveContratoNaoCancelado(Processojuridico pj) {
		return contratojuridicoDAO.haveContratoNaoCancelado(pj);
	}
	
	public void cancelar(String whereIn) {
		contratojuridicoDAO.cancelar(whereIn);
	}

}