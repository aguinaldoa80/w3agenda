package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.dao.ProducaodiariaDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaodiariaService extends GenericService<Producaodiaria>{
	
	private ProducaodiariaDAO producaodiariaDAO;
	private ProducaodiariaService producaodiariaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaService notaService;
	
	public void setProducaodiariaDAO(ProducaodiariaDAO producaodiariaDAO) {
		this.producaodiariaDAO = producaodiariaDAO;
	}
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {
		this.producaodiariaService = producaodiariaService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	
	/**
	 * Retorna uma lista de producaodiaria de um determinado planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#findByPlanejamento(Planejamento)
	 *
	 * @param planejamento
	 * @return lista de Producaodiaria
	 * @throws SinedException - quando o par�metro planejamento for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */		
	public List<Producaodiaria> findByPlanejamento(Planejamento planejamento) {
		return producaodiariaDAO.findByPlanejamento(planejamento);
	}
	
	/**
	 * Retorna uma lista de producaodiaria.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#findByWhereIn(String)
	 * 
	 * @param whereIn
	 * @return lista de producaodiaria
	 * @throws SinedException - quando o par�metro whereIn for nulo ou vazio.
	 * 
	 * @author Fl�vio Tavares
	 * 
	 */
	public List<Producaodiaria> findByWhereIn(String whereIn){
		return producaodiariaDAO.findByWhereIn(whereIn);
	}
	
	/**
	 * Associa � uma lista de produ��es di�rias uma nota.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#associaNotaProducoesdiarias(Nota, List)
	 * @param nota
	 * @param listaProducaodiaria
	 * @author Fl�vio Tavares
	 */
	public void associaNotaProducoesdiarias(Nota nota, List<Producaodiaria> listaProducaodiaria){
		producaodiariaDAO.associaNotaProducoesdiarias(nota, listaProducaodiaria);
	}
	
	/**
	 * Salva uma nota fiscal e associa �s produ��es di�rias cujos c�digos devem ser informados
	 * na propriedade <code>codProducoesDiaria</code> em Nota. O m�todo usa transaction para garantir
	 * a integridade dos dados em caso de erros.
	 * 
	 * @see #associaNotaProducoesdiarias(Nota, List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#saveOrUpdateNoUseTransaction(NotaFiscalServico)
	 * @param nota
	 * @throws SinedException - se a propriedade codProducoesDiaria do bean Nota estiver vazia.
	 * @author Fl�vio Tavares
	 */
	public void salvarNotaAoFaturarProducaodiaria(final Nota nota){
		if(StringUtils.isBlank(nota.getCodProducoesDiaria())){
			throw new SinedException("A nota deve estar com a propriedade codProducoesDiaria preenchida.");
		}
		
		final List<Producaodiaria> listaProducaodiaria = new ArrayList<Producaodiaria>();
		for (String cdproducaodiaria : nota.getCodProducoesDiaria().split(",")) {
			listaProducaodiaria.add(new Producaodiaria(Integer.valueOf(cdproducaodiaria)));
		}
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				if (nota instanceof NotaFiscalServico) {
					NotaFiscalServico nfs = (NotaFiscalServico) nota;
					notaFiscalServicoService.saveOrUpdateNoUseTransaction(nfs);
				}
				
				producaodiariaService.associaNotaProducoesdiarias(nota, listaProducaodiaria);
				return null;
			}
		});
	}
	
	/**
	 * Estorna uma lista de medi��es. Para cada medi��o a ser estornada, a nota correspondente � cancelada.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaService#cancelar(String, String)
	 * @see #estornarProducaoDiaria(Producaodiaria)
	 * @param listaProducaodiaria
	 * @return o n�mero de produ��es di�rias que foram estornadas
	 * @author Fl�vio Tavares
	 */
	public int doEstornarProducoesDiarias(final List<Producaodiaria> listaProducaodiaria){
		
		final String observacao = "Medi��o estornada.";
		
		return (Integer)getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				int count = 0;
				
				// Lista para conter as notas que foram canceladas.
				List<Nota> listaCanceladas = new ArrayList<Nota>();
				
				for (Producaodiaria pd : listaProducaodiaria) {
					Nota nota = pd.getNota();
					
					if(!listaCanceladas.contains(nota)){ // Se a nota j� foi cancelada n�o h� necessidade de cancelar novamente.
						
						int sucesso = notaService.cancelar(null, nota.getCdNota() + "", observacao, null);
						
						if(sucesso == 1){ // A nota foi cancelada com sucesso
							listaCanceladas.add(nota);
						} 
						else // Se a nota n�o foi cancelada a produ��o n�o deve ser estornada. 
							continue;
					}
					
					producaodiariaService.estornarProducaoDiaria(pd);
					count++;
				}
				
				return count;
			}
		});
	}
	
	/**
	 * Refer�ncia ao DAO.<br>
	 * Estorna uma produ��o di�ria.
	 * Atualiza as propriedades <code>dtfatura</code> de cada uma pra <code>null</code>.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#estornarProducaoDiaria(Producaodiaria)
	 * @param producaodiaria
	 * @author Fl�vio Tavares
	 */
	public void estornarProducaoDiaria(Producaodiaria producaodiaria){
		producaodiariaDAO.estornarProducaoDiaria(producaodiaria);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#loadWithProjeto
	 * 
	 * @param itensSelecionados
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String loadWithProjeto(String itensSelecionados) {
		return producaodiariaDAO.loadWithProjeto(itensSelecionados);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMenorDataProducaoDiariaProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return producaodiariaDAO.getMenorDataProducaoDiariaProjetoPlanejamento(projeto, planejamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMaiorDataProducaoDiariaProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return producaodiariaDAO.getMaiorDataProducaoDiariaProjetoPlanejamento(projeto, planejamento);
	}
	
	/**
	 * M�todo que retorna a m�dia do percentual da produ��o di�ria de um dado projeto
	 * 
	 * @param projeto
	 * @return
	 * @author Giovane Freitas
	 */
	public double getMediaPercentualProducaoDiaria(Projeto projeto) {
		return producaodiariaDAO.getMediaPercentualProducaoDiaria(projeto);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaodiariaDAO#getQtdeRealizadaTarefa(Tarefa tarefa)
	 *
	 * @param tarefa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public Double getQtdeRealizadaTarefa(Tarefa tarefa) {
		return producaodiariaDAO.getQtdeRealizadaTarefa(tarefa);
	}
		
}
