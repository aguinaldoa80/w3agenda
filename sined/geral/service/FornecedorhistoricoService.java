package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorhistorico;
import br.com.linkcom.sined.geral.dao.FornecedorhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FornecedorhistoricoService extends GenericService<Fornecedorhistorico>{

	private FornecedorhistoricoDAO fornecedorhistoricoDAO;
	
	public void setFornecedorhistoricoDAO(
			FornecedorhistoricoDAO fornecedorhistoricoDAO) {
		this.fornecedorhistoricoDAO = fornecedorhistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/07/2014
	 */
	public List<Fornecedorhistorico> findByFornecedor(Fornecedor fornecedor) {
		return fornecedorhistoricoDAO.findByFornecedor(fornecedor);
	}
	
}
