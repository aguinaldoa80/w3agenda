package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.CodigoAjusteIpi;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.geral.dao.CodigoAjusteIpiDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CodigoAjusteIpiService extends GenericService<CodigoAjusteIpi> {
	
	private CodigoAjusteIpiDAO codigoAjusteIpiDAO;
	
	public void setCodigoAjusteIpiDAO(CodigoAjusteIpiDAO codigoAjusteIpiDAO) {
		this.codigoAjusteIpiDAO = codigoAjusteIpiDAO;
	}

	public List<CodigoAjusteIpi> findByTipoAjuste(CreditoDebitoEnum tipoAjuste) {
		return codigoAjusteIpiDAO.findByTipoAjuste(tipoAjuste);
	}
}
