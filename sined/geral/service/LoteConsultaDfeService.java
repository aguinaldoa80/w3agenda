package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.lknfe.xml.dde.ConsChNfe;
import br.com.linkcom.lknfe.xml.dde.ConsNsu;
import br.com.linkcom.lknfe.xml.dde.DistDFeInt;
import br.com.linkcom.lknfe.xml.dde.DistNsu;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.LoteConsultaDfeDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeItem;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.LkNfeUtil;

public class LoteConsultaDfeService extends GenericService<LoteConsultaDfe> {

	private ArquivoService arquivoService;
	private ConfiguracaonfeService configuracaonfeService;
	private LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService;
	private ConfiguracaonfeService configuracaoNfeService;
	private EmpresaService empresaService;
	private TipoEventoNfeService tipoEventoNfeService;
	private LoteConsultaDfeItemService loteConsultaDfeItemService;
	private MotivoavisoService motivoAvisoService;
	private AvisoService avisoService;
	private ManifestoDfeService manifestoDfeService;
	private ManifestoDfeEventoService manifestoDfeEventoService;
	private ManifestoDfeHistoricoService manifestoDfeHistoricoService;
	private LoteConsultaDfeDAO loteConsultaDfeDAO;
	
	private static LoteConsultaDfeService instance;
	
	public static LoteConsultaDfeService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(LoteConsultaDfeService.class);
		}
		
		return instance;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	public void setLoteConsultaDfeHistoricoService(LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService) {
		this.loteConsultaDfeHistoricoService = loteConsultaDfeHistoricoService;
	}
	
	public void setConfiguracaoNfeService(ConfiguracaonfeService configuracaoNfeService) {
		this.configuracaoNfeService = configuracaoNfeService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setTipoEventoNfeService(TipoEventoNfeService tipoEventoNfeService) {
		this.tipoEventoNfeService = tipoEventoNfeService;
	}
	
	public void setLoteConsultaDfeItemService(LoteConsultaDfeItemService loteConsultaDfeItemService) {
		this.loteConsultaDfeItemService = loteConsultaDfeItemService;
	}
	
	public void setMotivoAvisoService(MotivoavisoService motivoAvisoService) {
		this.motivoAvisoService = motivoAvisoService;
	}
	
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {
		this.manifestoDfeService = manifestoDfeService;
	}
	
	public void setManifestoDfeHistoricoService(ManifestoDfeHistoricoService manifestoDfeHistoricoService) {
		this.manifestoDfeHistoricoService = manifestoDfeHistoricoService;
	}
	
	public void setManifestoDfeEventoService(ManifestoDfeEventoService manifestoDfeEventoService) {
		this.manifestoDfeEventoService = manifestoDfeEventoService;
	}
	
	public void setLoteConsultaDfeDAO(LoteConsultaDfeDAO loteConsultaDfeDAO) {
		this.loteConsultaDfeDAO = loteConsultaDfeDAO;
	}
	
	public void atualizarArquivoXml(LoteConsultaDfe loteConsultaDfe) {
		loteConsultaDfeDAO.atualizarArquivoXml(loteConsultaDfe);
	}

	public void atualizarSituacaoEnum(LoteConsultaDfe loteConsultaDfe) {
		loteConsultaDfeDAO.atualizarSituacaoEnum(loteConsultaDfe);
	}
	
	public void atualizarConfiguracaoNfe(LoteConsultaDfe loteConsultaDfe) {
		loteConsultaDfeDAO.atualizarConfiguracaoNfe(loteConsultaDfe);
	}
	
	public void atualizarArquivoRetornoXml(LoteConsultaDfe loteConsultaDfe) {
		loteConsultaDfeDAO.atualizarArquivoRetornoXml(loteConsultaDfe);
	}
	
	public void atualizarNsu(LoteConsultaDfe loteConsultaDfe) {
		loteConsultaDfeDAO.atualizarNsu(loteConsultaDfe);
	}

	public List<LoteConsultaDfe> procurarLoteConsultaDfeParaConsulta(Empresa empresa) {
		return loteConsultaDfeDAO.procurarLoteConsultaDfeParaConsulta(empresa);
	}
	
	public LoteConsultaDfe procurarPorId(Integer cdLoteConsultaDfe) {
		return loteConsultaDfeDAO.procurarPorId(cdLoteConsultaDfe);
	}
	
	public Integer marcarLoteConsultaDfe(String whereInLoteConsultaDfe, String flag) {
		return loteConsultaDfeDAO.marcarLoteConsultaDfe(whereInLoteConsultaDfe, flag);
	}

	public Boolean temLoteConsultaDfeDiferenteDe(String selectedItens, SituacaoEmissorEnum situacaoEnum) {
		return loteConsultaDfeDAO.temLoteConsultaDfeDiferenteDe(selectedItens, situacaoEnum).size() > 0;
	}

	public LoteConsultaDfe procurarSituacao(LoteConsultaDfe bean) {
		return loteConsultaDfeDAO.procurarSituacao(bean);
	}
	
	public Boolean procurarEmpresasDoLoteConsultaDfe(String selectedItens) {
		return loteConsultaDfeDAO.procurarEmpresasDoLoteConsultaDfe(selectedItens);
	}

	public List<LoteConsultaDfeHistorico> procurarHistoricoPorLoteConsultaDfe(LoteConsultaDfe form) {
		return loteConsultaDfeHistoricoService.procurarPorLoteConsultaDfe(form);
	}

	public List<LoteConsultaDfeItem> procurarItemPorLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		return loteConsultaDfeItemService.procurarItemPorLoteConsultaDfe(loteConsultaDfe);
	}
	
	public void salvarLoteConsultaDfeHistorico(LoteConsultaDfeHistorico loteConsultaDfeHistorico) {
		loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
	}
	
	public String criarXmlLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe, Empresa empresa, Configuracaonfe configuracaoNfe) {
		if (empresa == null || empresa.getCnpj() == null || empresa.getCpf() == null) {
			empresa = empresaService.carregaEmpresa(empresa);
		}
		
		if (configuracaoNfe == null || configuracaoNfe.getUf() == null || configuracaoNfe.getUf().getCdibge() == null || configuracaoNfe.getUltNsu() == null) {
			configuracaoNfe = configuracaoNfeService.carregaConfiguracao(configuracaoNfe);
		}
		
		TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum = loteConsultaDfe.getTipoLoteConsultaDfeEnum();
		
        DistDFeInt distDFeInt = new DistDFeInt();
        distDFeInt.setcUfAutor(configuracaoNfe != null && configuracaoNfe.getUf() != null ? configuracaoNfe.getUf().getCdibge().toString() : "");
        distDFeInt.setCnpj(empresa != null && empresa.getCnpj() != null ? empresa.getCnpj().getValue() : null);
        distDFeInt.setCpf(empresa != null && empresa.getCpf() != null ? empresa.getCpf().getValue() : null);
        distDFeInt.setTpAmb(configuracaoNfe.getPrefixowebservice() != null && configuracaoNfe.getPrefixowebservice().name().contains("_PROD") ? 1 : 2);
        distDFeInt.setVersao("1.01");
        
        if (tipoLoteConsultaDfeEnum != null) {
	        if (TipoLoteConsultaDfeEnum.NSU_SEQUENCIA.equals(tipoLoteConsultaDfeEnum)) {
	        	DistNsu distNsu = new DistNsu();
		        distNsu.setUltNSU(configuracaoNfe != null && configuracaoNfe.getUltNsu() != null ? 
		        		br.com.linkcom.neo.util.StringUtils.stringCheia(configuracaoNfe.getUltNsu().toString(), "0", 15, false) : "1");
		        
		        loteConsultaDfe.setNsu(configuracaoNfe.getUltNsu());
		        this.atualizarNsu(loteConsultaDfe);
		        
		        distDFeInt.setDistNSU(distNsu);
	        } else if (TipoLoteConsultaDfeEnum.NSU.equals(tipoLoteConsultaDfeEnum)) {
	        	ConsNsu consNsu = new ConsNsu();
	        	consNsu.setNsu(loteConsultaDfe.getNsu() != null && StringUtils.isNotEmpty(loteConsultaDfe.getNsu().toString()) ? 
		        		br.com.linkcom.neo.util.StringUtils.stringCheia(loteConsultaDfe.getNsu().toString(), "0", 15, false) : "");
	        	
	        	distDFeInt.setConsNsu(consNsu);
	        } else if (TipoLoteConsultaDfeEnum.CHAVE_ACESSO.equals(tipoLoteConsultaDfeEnum)) {
	        	ConsChNfe consChNfe = new ConsChNfe();
	        	consChNfe.setChNfe(StringUtils.isNotEmpty(loteConsultaDfe.getChaveAcesso()) ? loteConsultaDfe.getChaveAcesso() : "");
	        	
	        	distDFeInt.setConsChNfe(consChNfe);
	        }
        }
        
        return distDFeInt.toString();
	}
	
	public LoteConsultaDfe criarNovoLoteConsultaDfeNsuSeq(Configuracaonfe configuracaonfe, Boolean ultimo) {
		LoteConsultaDfe loteConsultaDfe = new LoteConsultaDfe();
		
		loteConsultaDfe.setTipoLoteConsultaDfeEnum(TipoLoteConsultaDfeEnum.NSU_SEQUENCIA);
		loteConsultaDfe.setNsu(configuracaonfe.getUltNsu() != null ? configuracaonfe.getUltNsu() : 1);
		loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
		loteConsultaDfe.setConfiguracaoNfe(configuracaonfe);
		loteConsultaDfe.setEmpresa(configuracaonfe.getEmpresa());
		
		if (ultimo) {
			Timestamp timestamp = SinedDateUtils.incrementTimestamp(SinedDateUtils.currentTimestamp(), 2, Calendar.HOUR);
			loteConsultaDfe.setDhEnvio(timestamp);
		} else {
			
			loteConsultaDfe.setDhEnvio(SinedDateUtils.currentTimestamp());
		}
		
		this.saveOrUpdate(loteConsultaDfe);
		
		String xml = this.criarXmlLoteConsultaDfe(loteConsultaDfe, configuracaonfe.getEmpresa(), configuracaonfe);
		Arquivo arquivoXml = new Arquivo(xml.getBytes(), "loteConsultaDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		loteConsultaDfe.setArquivoXml(arquivoXml);
		
		arquivoService.saveFile(loteConsultaDfe, "arquivoXml");
		arquivoService.saveOrUpdate(arquivoXml);
		
		this.atualizarArquivoXml(loteConsultaDfe);
		
		return loteConsultaDfe;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean processaRetornoLoteConsultaDfe(Arquivo arquivoRetornoXml, LoteConsultaDfe loteConsultaDfeEmissor) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
 		LoteConsultaDfe loteConsultaDfe = this.loadForEntrada(loteConsultaDfeEmissor);
		loteConsultaDfe.setArquivoRetornoXml(arquivoRetornoXml);
		loteConsultaDfe.setListaLoteConsultaDfeHistorico(null);
		loteConsultaDfe.setListaLoteConsultaDfeItem(null);
		
		arquivoService.saveFile(loteConsultaDfe, "arquivoRetornoXml");
		arquivoService.saveOrUpdate(arquivoRetornoXml);
		
		String ultNsuStr = "", maxNsuStr = "";
		Integer ultNsuInt = 0, maxNsuInt = 0;
		
		Element nfeDistDFeInteresseResultElement = SinedUtil.getRootElementXML(arquivoRetornoXml.getContent());
		
		if (nfeDistDFeInteresseResultElement != null) {
			Element retDistDFeIntElement = SinedUtil.getChildElement("retDistDFeInt", nfeDistDFeInteresseResultElement.getContent());
			
			if (retDistDFeIntElement != null) {
				Element cStatLoteElement = SinedUtil.getChildElement("cStat", retDistDFeIntElement.getContent());
				Element dhRespLoteElement = SinedUtil.getChildElement("dhResp", retDistDFeIntElement.getContent());
				Element ultNsuLoteElement = SinedUtil.getChildElement("ultNSU", retDistDFeIntElement.getContent());
				Element maxNsuLoteElement = SinedUtil.getChildElement("maxNSU", retDistDFeIntElement.getContent());
				Element xMotivoLoteElement = SinedUtil.getChildElement("xMotivo", retDistDFeIntElement.getContent());
				
				Timestamp dhResp = dhRespLoteElement != null && StringUtils.isNotEmpty(dhRespLoteElement.getValue()) ? 
						new Timestamp(LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRespLoteElement.getValue()).getTime()) : SinedDateUtils.currentTimestamp();
				
				loteConsultaDfe.setCoStat(cStatLoteElement != null && StringUtils.isNotEmpty(cStatLoteElement.getValue()) ? cStatLoteElement.getValue() : null);
				loteConsultaDfe.setMotivo(xMotivoLoteElement != null && StringUtils.isNotEmpty(xMotivoLoteElement.getValue()) ? xMotivoLoteElement.getValue() : null);
				loteConsultaDfe.setDhResp(dhResp);
				
				ultNsuStr = ultNsuLoteElement != null ? ultNsuLoteElement.getValue() : "0";
				maxNsuStr = maxNsuLoteElement != null ? maxNsuLoteElement.getValue() : "0";
				
				if (cStatLoteElement != null && StringUtils.isNotEmpty(cStatLoteElement.getValue()) && "138".equals(cStatLoteElement.getValue())) {
					ultNsuStr = ultNsuLoteElement != null && StringUtils.isNotEmpty(ultNsuLoteElement.getValue()) ? ultNsuLoteElement.getValue() : "";
					
					Element loteDistDFeIntElement = SinedUtil.getChildElement("loteDistDFeInt", retDistDFeIntElement.getContent());
					
					if (loteDistDFeIntElement != null) {
						List<Element> listaDocZipsElement = SinedUtil.getListChildElement("docZip", loteDistDFeIntElement.getContent());
						List<LoteConsultaDfeItem> listaLoteConsultaDfeItem = new ArrayList<LoteConsultaDfeItem>();
						
						for (Element docZipElement : listaDocZipsElement) {
							if (docZipElement != null && StringUtils.isNotEmpty(docZipElement.getValue())) {
                                String xml = Util.strings.tiraAcento(docZipElement.getValue());
                                byte[] decoded = Base64.decodeBase64(xml.getBytes());
								String arquivoXmlStr = LkNfeUtil.gZipToXml(decoded);
								
								if (StringUtils.isNotEmpty(arquivoXmlStr)) {
									if (StringUtils.isNotEmpty(docZipElement.getAttributeValue("schema")) && docZipElement.getAttributeValue("schema").contains("resNFe")) {
										Arquivo arquivoItemXml = new Arquivo(arquivoXmlStr.getBytes(), "resNFe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
										
										if (arquivoItemXml != null) {
											Element resNfeElement = SinedUtil.getRootElementXML(arquivoItemXml.getContent());
											
											LoteConsultaDfeItem loteConsultaDfeItem = this.preencherLoteConsultaDfeItem(loteConsultaDfe, arquivoItemXml, resNfeElement, null);
											
											if (loteConsultaDfeItem != null) {
												listaLoteConsultaDfeItem.add(loteConsultaDfeItem);
											}
										}
									} else if (StringUtils.isNotEmpty(docZipElement.getAttributeValue("schema")) && docZipElement.getAttributeValue("schema").contains("procNFe_v4.00.xsd")) {
										if(docZipElement.getAttributeValue("schema").contains("procNFe_v4.00.xsd")){
											Arquivo arquivoItemXml = new Arquivo(arquivoXmlStr.getBytes(), "procNFe_v4_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
											
											if (arquivoItemXml != null) {
												Element nfeProcElement = SinedUtil.getRootElementXML(arquivoItemXml.getContent());
												Element protNfeElement = SinedUtil.getChildElement("protNFe", nfeProcElement.getContent());
												Element infProtElement = SinedUtil.getChildElement("infProt", protNfeElement.getContent());
												
												LoteConsultaDfeItem loteConsultaDfeItem = this.preencherLoteConsultaDfeItem(loteConsultaDfe, arquivoItemXml, infProtElement, nfeProcElement);
												
												if (loteConsultaDfeItem != null) {
													listaLoteConsultaDfeItem.add(loteConsultaDfeItem);
												}
											}
										}
									} else if (StringUtils.isNotEmpty(docZipElement.getAttributeValue("schema")) && (docZipElement.getAttributeValue("schema").contains("nfeProc"))) {
										Arquivo arquivoItemXml = new Arquivo(arquivoXmlStr.getBytes(), "nfeProc_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
										
										if (arquivoItemXml != null) {
											Element nfeProcElement = SinedUtil.getRootElementXML(arquivoItemXml.getContent());
											Element protNfeElement = SinedUtil.getChildElement("protNFe", nfeProcElement.getContent());
											Element infProtElement = SinedUtil.getChildElement("infProt", protNfeElement.getContent());
											
											if(infProtElement != null){
												Element chNfeElement = SinedUtil.getChildElement("chNFe", infProtElement.getContent());
												if(chNfeElement != null && StringUtils.isNotBlank(chNfeElement.getValue())){
													arquivoItemXml.setName( "nfeProc_" +chNfeElement.getValue() + "_" + SinedUtil.datePatternForReport() + ".xml");
												}
											}
											
											LoteConsultaDfeItem loteConsultaDfeItem = this.preencherLoteConsultaDfeItem(loteConsultaDfe, arquivoItemXml, infProtElement, nfeProcElement);
											
											if (loteConsultaDfeItem != null) {
												listaLoteConsultaDfeItem.add(loteConsultaDfeItem);
											}
										}
										
									} else if (StringUtils.isNotEmpty(arquivoXmlStr)) {
										if (StringUtils.isNotEmpty(docZipElement.getAttributeValue("schema")) && docZipElement.getAttributeValue("schema").contains("resEvento")) {
											Arquivo arquivoItemXml = new Arquivo(arquivoXmlStr.getBytes(), "resEvento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
											
											if (arquivoItemXml != null) {
												Element resEventoElement = SinedUtil.getRootElementXML(arquivoItemXml.getContent());
												
												LoteConsultaDfeItem loteConsultaDfeItem = this.preencherLoteConsultaDfeItem(loteConsultaDfe, arquivoItemXml, resEventoElement, null);
												
												if (loteConsultaDfeItem != null) {
													listaLoteConsultaDfeItem.add(loteConsultaDfeItem);
												}
											}
										} else if (StringUtils.isNotEmpty(docZipElement.getAttributeValue("schema")) && docZipElement.getAttributeValue("schema").contains("procEventoNFe")) {
											Arquivo arquivoItemXml = new Arquivo(arquivoXmlStr.getBytes(), "procEventoNFe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
											
											if (arquivoItemXml != null) {
												Element procEventoNfeElement = SinedUtil.getRootElementXML(arquivoItemXml.getContent());
												Element retEventoElement = SinedUtil.getChildElement("retEvento", procEventoNfeElement.getContent());
												Element infEventoElement = SinedUtil.getChildElement("infEvento", retEventoElement.getContent());
												
												LoteConsultaDfeItem loteConsultaDfeItem = this.preencherLoteConsultaDfeItem(loteConsultaDfe, arquivoItemXml, infEventoElement, null);
												
												if (loteConsultaDfeItem != null) {
													listaLoteConsultaDfeItem.add(loteConsultaDfeItem);
												}
											}
										}
									}
								}
							}
						}
						
						loteConsultaDfe.setListaLoteConsultaDfeItem(listaLoteConsultaDfeItem);
					}
					
					loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_SUCESSO);
				} else {
					loteConsultaDfe.setCoStat(cStatLoteElement != null && StringUtils.isNotEmpty(cStatLoteElement.getValue()) ? cStatLoteElement.getValue() : null);
					loteConsultaDfe.setMotivo(xMotivoLoteElement != null && StringUtils.isNotEmpty(xMotivoLoteElement.getValue()) ? xMotivoLoteElement.getValue() : null);
					loteConsultaDfe.setDhResp(dhResp);
					loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_ERRO);
					
					Motivoaviso motivoAviso = motivoAvisoService.findByMotivo(MotivoavisoEnum.LOTE_NOTA_GERADO);
					
					if (motivoAviso != null) {
						List<Aviso> avisoList = new ArrayList<Aviso>();
						
						Aviso aviso = new Aviso("Lote de notas gerada com situa��o 'Processado com erro'.", "Lote de Consulta DF-e: " + loteConsultaDfe.getCdLoteConsultaDfe(), 
								motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getAvisoorigem(), loteConsultaDfe.getCdLoteConsultaDfe(), 
								loteConsultaDfe.getEmpresa() != null ? loteConsultaDfe.getEmpresa() : empresaService.loadPrincipal(), SinedDateUtils.currentDate(), motivoAviso, Boolean.FALSE);
						avisoList.add(aviso);
						
						avisoService.salvarAvisos(avisoList, true);
					}
				}
				
				this.saveOrUpdate(loteConsultaDfe);
				
				if (loteConsultaDfe != null && loteConsultaDfe.getListaLoteConsultaDfeItem() != null && loteConsultaDfe.getListaLoteConsultaDfeItem().size() > 0) {
					for (LoteConsultaDfeItem loteConsultaDfeItem : loteConsultaDfe.getListaLoteConsultaDfeItem()) {
						if(StringUtils.isBlank(loteConsultaDfeItem.getChaveAcesso())){
							continue;
						}
						ManifestoDfe manifestoDfeFound = manifestoDfeService.procurarPorChaveAcesso(loteConsultaDfeItem.getChaveAcesso());
						
						if (StringUtils.isEmpty(loteConsultaDfeItem.getEvento())) {
							if (manifestoDfeFound != null) {
								if (TipoLoteConsultaDfeEnum.CHAVE_ACESSO.equals(loteConsultaDfe.getTipoLoteConsultaDfeEnum())) {
									manifestoDfeFound.setArquivoNfeCompletoXml(loteConsultaDfeItem.getArquivoXml());
									
									manifestoDfeService.atualizarArquivoNfeCompletoXml(manifestoDfeFound);
									
									ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
									Usuario usuario = SinedUtil.getUsuarioLogado();
						            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
									manifestoDfeHistorico.setManifestoDfe(manifestoDfeFound);
									manifestoDfeHistorico.setObservacao("NF-e completa atualizada a partir do Lote Consulta DF-e " +
											"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
									
									manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
								} else {
									if(loteConsultaDfeItem.getStatusEnum() != null){
										manifestoDfeFound.setStatusEnum(loteConsultaDfeItem.getStatusEnum());
										manifestoDfeService.atualizarStatusEnum(manifestoDfeFound);
									}
									if(loteConsultaDfeItem.getArquivoXml() != null && loteConsultaDfeItem.getArquivoXml().getNome() != null && 
											loteConsultaDfeItem.getArquivoXml().getNome().contains("procNFe_v4_")){
										manifestoDfeFound.setArquivoNfeCompletoXml(loteConsultaDfeItem.getArquivoXml());
										manifestoDfeService.atualizarArquivoNfeCompletoXml(manifestoDfeFound);
									}else {
										manifestoDfeFound.setArquivoXml(loteConsultaDfeItem.getArquivoXml());
										manifestoDfeService.atualizarArquivoXml(manifestoDfeFound);
									}
									
									
									ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
									Usuario usuario = SinedUtil.getUsuarioLogado();
						            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
									manifestoDfeHistorico.setManifestoDfe(manifestoDfeFound);
									manifestoDfeHistorico.setObservacao("Manifesto DF-e atualizado a partir do Lote Consulta DF-e " + 
											"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
									
									manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
								}
							} else {
								ManifestoDfe manifestoDfe = new ManifestoDfe();
								manifestoDfe.setLoteConsultaDfeItem(loteConsultaDfeItem);
								
								manifestoDfe.setChaveAcesso(loteConsultaDfeItem.getChaveAcesso());
								manifestoDfe.setCnpj(loteConsultaDfeItem.getCnpj());
								manifestoDfe.setCpf(loteConsultaDfeItem.getCpf());
								manifestoDfe.setRazaoSocial(loteConsultaDfeItem.getRazaoSocial());
								manifestoDfe.setIe(loteConsultaDfeItem.getIe());
								manifestoDfe.setTipoOperacaoNota(loteConsultaDfeItem.getTipoOperacaoNota());
								manifestoDfe.setValorNf(loteConsultaDfeItem.getValorNf());
								manifestoDfe.setDhEmissao(loteConsultaDfeItem.getDhEmissao());
								manifestoDfe.setDhRecbto(loteConsultaDfeItem.getDhRecbto());
								manifestoDfe.setNuProt(loteConsultaDfeItem.getNuProt());
								manifestoDfe.setStatusEnum(loteConsultaDfeItem.getStatusEnum());
								manifestoDfe.setCoStat(loteConsultaDfeItem.getCoStat());
								manifestoDfe.setMotivo(loteConsultaDfeItem.getMotivo());
								manifestoDfe.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
								
								if(loteConsultaDfeItem.getArquivoXml() != null && loteConsultaDfeItem.getArquivoXml().getNome() != null && 
										loteConsultaDfeItem.getArquivoXml().getNome().contains("procNFe_v4_")){
									manifestoDfe.setArquivoNfeCompletoXml(loteConsultaDfeItem.getArquivoXml());
								}else {
									manifestoDfe.setArquivoXml(loteConsultaDfeItem.getArquivoXml());
								}
								
								manifestoDfeService.saveOrUpdate(manifestoDfe);
								
								ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
								Usuario usuario = SinedUtil.getUsuarioLogado();
					            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
								manifestoDfeHistorico.setManifestoDfe(manifestoDfe);
								manifestoDfeHistorico.setObservacao("Manifesto DF-e criado a partir do Lote Consulta DF-e " + 
										"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
								
								manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
								
								Configuracaonfe configuracaonfe = loteConsultaDfe.getConfiguracaoNfe();
								
								if (BooleanUtils.isTrue(configuracaonfe.getCienciaAutomatica()) && configuracaonfe.getConfiguracaoNfeManifestoAutomatico() != null) {
									ManifestoDfeEvento manifestoDfeEvento = new ManifestoDfeEvento();
									
									manifestoDfeEvento.setManifestoDfe(manifestoDfe);
									manifestoDfeEvento.setCnpj(manifestoDfe.getCnpj());
									manifestoDfeEvento.setCpf(manifestoDfe.getCpf());
									manifestoDfeEvento.setChaveAcesso(manifestoDfe.getChaveAcesso());
									
									TipoEventoNfe tipoEventoNfe = tipoEventoNfeService.load(TipoEventoNfe.CIENCIA_OPERACAO);
									manifestoDfeEvento.setTpEvento(tipoEventoNfe);
									
									String xml = manifestoDfeEventoService.criarEventoManifestacaoDfe(manifestoDfeEvento, loteConsultaDfe.getEmpresa(), configuracaonfe.getConfiguracaoNfeManifestoAutomatico());
									Arquivo arquivoXml = new Arquivo(xml.getBytes(), "manifestoDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
									
									manifestoDfeEvento.setArquivoXml(arquivoXml);
									manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
									manifestoDfeEvento.setConfiguracaoNfe(configuracaonfe.getConfiguracaoNfeManifestoAutomatico());
									
									arquivoService.saveFile(manifestoDfeEvento, "arquivoXml");
									arquivoService.saveOrUpdate(arquivoXml);
									
									manifestoDfeEventoService.saveOrUpdate(manifestoDfeEvento);
									
									ManifestoDfeHistorico manifestoDfeHistoricoEvento = new ManifestoDfeHistorico();
						            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
									manifestoDfeHistoricoEvento.setManifestoDfe(manifestoDfeEvento.getManifestoDfe());
									manifestoDfeHistoricoEvento.setObservacao("Gerado o lote de " + manifestoDfeEvento.getTpEvento().getDescricao() + " da Manifesto DF-e.");
									manifestoDfeHistoricoEvento.setSituacaoEnum(manifestoDfeEvento.getSituacaoEnum());
									
									manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistoricoEvento);
								}
							}
						} else {
							if (manifestoDfeFound != null) {
								ManifestoDfeEvento manifestoDfeEvento = new ManifestoDfeEvento();
								manifestoDfeEvento.setManifestoDfe(manifestoDfeFound);
								
								manifestoDfeEvento.setChaveAcesso(loteConsultaDfeItem.getChaveAcesso());
								manifestoDfeEvento.setCnpj(loteConsultaDfeItem.getCnpj());
								manifestoDfeEvento.setCpf(loteConsultaDfeItem.getCpf());
								manifestoDfeEvento.setNuProt(loteConsultaDfeItem.getNuProt());
								manifestoDfeEvento.setTpEvento(loteConsultaDfeItem.getTpEvento());
								manifestoDfeEvento.setEvento(loteConsultaDfeItem.getEvento());
								manifestoDfeEvento.setDhEvento(loteConsultaDfeItem.getDhEvento());
								manifestoDfeEvento.setDhRegEvento(loteConsultaDfeItem.getDhRegEvento());
								manifestoDfeEvento.setArquivoRetornoXml(loteConsultaDfeItem.getArquivoXml());
								manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_SUCESSO);
								
								manifestoDfeEventoService.saveOrUpdate(manifestoDfeEvento);
								
								if (TipoEventoNfe.CIENCIA_OPERACAO.equals(loteConsultaDfeItem.getTpEvento())) {
									manifestoDfeFound.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.CIENCIA);
								} else if (TipoEventoNfe.CONFIRMACAO_OPERACAO.equals(loteConsultaDfeItem.getTpEvento())) {
									manifestoDfeFound.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.CONFIRMACAO);
								} else if (TipoEventoNfe.DESCONHECIMENTO_OPERACAO.equals(loteConsultaDfeItem.getTpEvento())) {
									manifestoDfeFound.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.DESCONHECIMENTO);
								} else if (TipoEventoNfe.OPERACAO_NAO_REALIZADA.equals(loteConsultaDfeItem.getTpEvento())) {
									manifestoDfeFound.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.NAO_REALIZADA);
								} else {
									manifestoDfeFound.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
								}
								
								manifestoDfeService.atualizarManifestoDfeSituacaoEnum(manifestoDfeFound);
								
								ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
								Usuario usuario = SinedUtil.getUsuarioLogado();
					            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
								manifestoDfeHistorico.setManifestoDfe(manifestoDfeFound);
								manifestoDfeHistorico.setObservacao("Evento de " + manifestoDfeEvento.getEvento() + " criado a partir do Lote Consulta DF-e " + 
										"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
								
								manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
							}
						}
					}
				}
			} else {
				throw new SinedException("Erro no XML de retorno de Lote de Consulta DF-e");
			}
		} else {
			throw new SinedException("Erro no XML de retorno de Lote de Consulta DF-e");
		}
		
		if (SituacaoEmissorEnum.PROCESSADO_SUCESSO.equals(loteConsultaDfe.getSituacaoEnum()) && TipoLoteConsultaDfeEnum.NSU_SEQUENCIA.equals(loteConsultaDfe.getTipoLoteConsultaDfeEnum())) {
			ultNsuInt = Integer.parseInt(ultNsuStr);
			if (StringUtils.isNotEmpty(ultNsuStr)) {
				
				Configuracaonfe configuracaoNfe = loteConsultaDfe.getConfiguracaoNfe();
				configuracaoNfe.setUltNsu(Integer.parseInt(ultNsuStr));
				configuracaonfeService.updateUltNsu(configuracaoNfe);
				
				if (StringUtils.isNotEmpty(maxNsuStr)) {
					maxNsuInt = Integer.parseInt(maxNsuStr);
					
					if (ultNsuInt < maxNsuInt && TipoLoteConsultaDfeEnum.NSU_SEQUENCIA.equals(loteConsultaDfe.getTipoLoteConsultaDfeEnum())) {
						LoteConsultaDfe loteConsultaDfe2 = this.criarNovoLoteConsultaDfeNsuSeq(configuracaoNfe, Boolean.FALSE);
						
						LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
						Usuario usuario = SinedUtil.getUsuarioLogado();
						loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
						loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe2);
						loteConsultaDfeHistorico.setObservacao("Lote de Consulta de DF-e criado no retorno do processamento do Lote Consulta DF-e " +
								"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
						loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
						
						loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
					} else if (ultNsuInt.equals(maxNsuInt)) {
						LoteConsultaDfe loteConsultaDfe2 = this.criarNovoLoteConsultaDfeNsuSeq(configuracaoNfe, Boolean.TRUE);
						
						LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
						Usuario usuario = SinedUtil.getUsuarioLogado();
						loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
						loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe2);
						loteConsultaDfeHistorico.setObservacao("Lote de Consulta de DF-e criado no retorno do processamento do Lote Consulta DF-e " +
								"<a href=\"javascript:visualizarLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>.");
						loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
						
						loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
					}
				}
			}
		} else if (SituacaoEmissorEnum.PROCESSADO_ERRO.equals(loteConsultaDfe.getSituacaoEnum()) && TipoLoteConsultaDfeEnum.NSU_SEQUENCIA.equals(loteConsultaDfe.getTipoLoteConsultaDfeEnum())) {
			if ("137".equals(loteConsultaDfe.getCoStat())) {
				Timestamp timestamp = SinedDateUtils.incrementTimestamp(SinedDateUtils.currentTimestamp(), 2, Calendar.HOUR);
				loteConsultaDfe.setDhEnvio(timestamp);
				loteConsultaDfe.setEnviado(Boolean.FALSE);
				loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				//loteConsultaDfe.setArquivoRetornoXml(null);
				loteConsultaDfe.setMotivo(null);
				loteConsultaDfe.setCoStat(null);
				loteConsultaDfe.setDhResp(null);
				
				this.saveOrUpdate(loteConsultaDfe);
				
				LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
				Usuario usuario = SinedUtil.getUsuarioLogado();
				loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
				loteConsultaDfeHistorico.setObservacao("Lote de Consulta de DF-e atualizado pois n�o foi localizado documento novo para consulta.");
				loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_ERRO);
				
				loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
			}
		}
		
		return Boolean.TRUE;
	}
	
	@SuppressWarnings("unchecked")
	private LoteConsultaDfeItem preencherLoteConsultaDfeItem(LoteConsultaDfe loteConsultaDfe, Arquivo arquivoXml, Element element, Element nfeProcElement) throws ParseException {
		LoteConsultaDfeItem loteConsultaDfeItem = null;
		
		if (element != null) {
			loteConsultaDfeItem = new LoteConsultaDfeItem();
			
			Element chNfeElement = SinedUtil.getChildElement("chNFe", element.getContent());
			Element cnpjElement = SinedUtil.getChildElement("CNPJ", element.getContent());
			Element cpfElement = SinedUtil.getChildElement("CPF", element.getContent());
			Element xNomeElement = SinedUtil.getChildElement("xNome", element.getContent());
			Element ieElement = SinedUtil.getChildElement("IE", element.getContent());
			Element dhEmiElement = SinedUtil.getChildElement("dhEmi", element.getContent());
			Element tpNfElement = SinedUtil.getChildElement("tpNF", element.getContent());
			Element vNfElement = SinedUtil.getChildElement("vNF", element.getContent());
			Element dhRecbtoElement = SinedUtil.getChildElement("dhRecbto", element.getContent());
			Element nProtElement = SinedUtil.getChildElement("nProt", element.getContent());
			Element cSitNfeElement = SinedUtil.getChildElement("cSitNFe", element.getContent());
			Element cStatElement = SinedUtil.getChildElement("cStat", element.getContent());
			Element xMotivoElement = SinedUtil.getChildElement("xMotivo", element.getContent());
			Element dhEventoElement = SinedUtil.getChildElement("dhEvento", element.getContent());
			Element tpEventoElement = SinedUtil.getChildElement("tpEvento", element.getContent());
			Element xEventoElement = SinedUtil.getChildElement("xEvento", element.getContent());
			Element dhRegEventoElement = SinedUtil.getChildElement("dhRegEvento", element.getContent());
			
			if (nfeProcElement != null) {
				Element nfeElement = SinedUtil.getChildElement("NFe", nfeProcElement.getContent());
				
				if (nfeElement != null) {
					Element infNFeElement = SinedUtil.getChildElement("infNFe", nfeElement.getContent());
					
					if (infNFeElement != null) {
						Element ideElement = SinedUtil.getChildElement("ide", infNFeElement.getContent());
						
						if (ideElement != null) {
							tpNfElement = SinedUtil.getChildElement("tpNF", ideElement.getContent());
						}
						
						Element emitElement = SinedUtil.getChildElement("emit", infNFeElement.getContent());
						
						if (emitElement != null) {
							cnpjElement = SinedUtil.getChildElement("CNPJ", emitElement.getContent());
							cpfElement = SinedUtil.getChildElement("CPF", emitElement.getContent());
							xNomeElement = SinedUtil.getChildElement("xNome", emitElement.getContent());
							ieElement = SinedUtil.getChildElement("IE", emitElement.getContent());
						}
						
						Element totalElement = SinedUtil.getChildElement("total", infNFeElement.getContent());
						
						if (totalElement != null) {
							Element iCMSTotElement = SinedUtil.getChildElement("ICMSTot", totalElement.getContent());
							
							if (iCMSTotElement != null) {
								vNfElement = SinedUtil.getChildElement("vNF", iCMSTotElement.getContent());
							}
						}
					}
				}
			}
			
			loteConsultaDfeItem.setChaveAcesso(chNfeElement != null ? chNfeElement.getValue() : null);
			loteConsultaDfeItem.setNuProt(nProtElement != null ? nProtElement.getValue() : null);
			loteConsultaDfeItem.setCoStat(cStatElement != null ? cStatElement.getValue() : null);
			loteConsultaDfeItem.setMotivo(xMotivoElement != null ? xMotivoElement.getValue() : null);
			loteConsultaDfeItem.setDhRecbto(dhRecbtoElement != null ? SinedDateUtils.stringToTimestamp(dhRecbtoElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
			loteConsultaDfeItem.setDhEmissao(dhEmiElement != null ? SinedDateUtils.stringToTimestamp(dhEmiElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
			
			if (tpEventoElement != null && StringUtils.isNotEmpty(tpEventoElement.getValue())) {
				TipoEventoNfe tpEvento = tipoEventoNfeService.procurarPorCodigo(tpEventoElement.getValue());
				if(tpEvento != null){
					loteConsultaDfeItem.setTpEvento(tpEvento);
					loteConsultaDfeItem.setEvento(tpEvento.getDescricao());
				}
			}
			loteConsultaDfeItem.setEvento(xEventoElement != null ? xEventoElement.getValue() : null);
			loteConsultaDfeItem.setDhEvento(dhEventoElement != null ? SinedDateUtils.stringToTimestamp(dhEventoElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
			loteConsultaDfeItem.setDhRegEvento(dhRegEventoElement != null ? SinedDateUtils.stringToTimestamp(dhRegEventoElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
			
			loteConsultaDfeItem.setCnpj(cnpjElement != null ? new Cnpj(cnpjElement.getValue()) : null);
			loteConsultaDfeItem.setCpf(cpfElement != null ? new Cpf(cpfElement.getValue()) : null);
			loteConsultaDfeItem.setRazaoSocial(xNomeElement != null ? xNomeElement.getValue() : null);
			loteConsultaDfeItem.setIe(ieElement != null ? ieElement.getValue() : null);
			loteConsultaDfeItem.setTipoOperacaoNota(tpNfElement != null && tpNfElement.getValue().equals("1") ? Tipooperacaonota.SAIDA : Tipooperacaonota.ENTRADA);
			loteConsultaDfeItem.setValorNf(vNfElement != null ? new Money(vNfElement.getValue()) : null);
			
			if (cSitNfeElement != null) {
				if (StringUtils.isNotEmpty(cSitNfeElement.getValue()) && cSitNfeElement.getValue().equals("1")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA);
				} else if (StringUtils.isNotEmpty(cSitNfeElement.getValue()) && cSitNfeElement.getValue().equals("2")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_DENEGADA);
				} else if (StringUtils.isNotEmpty(cSitNfeElement.getValue()) && cSitNfeElement.getValue().equals("3")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_CANCELADA);
				}
			}
			
			if (cStatElement != null) {
				if (StringUtils.isNotEmpty(cStatElement.getValue()) && cStatElement.getValue().equals("100")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA);
				} else if (StringUtils.isNotEmpty(cStatElement.getValue()) && cStatElement.getValue().equals("110")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_DENEGADA);
				} else if (StringUtils.isNotEmpty(cStatElement.getValue()) && cStatElement.getValue().equals("101")) {
					loteConsultaDfeItem.setStatusEnum(LoteConsultaDfeItemStatusEnum.DFE_CANCELADA);
				}
			}
			
			loteConsultaDfeItem.setLoteConsultaDfe(loteConsultaDfe);
			
			loteConsultaDfeItem.setArquivoXml(arquivoXml);
			arquivoService.saveFile(loteConsultaDfeItem, "arquivoXml");
			arquivoService.saveOrUpdate(arquivoXml);
		}
		
		return loteConsultaDfeItem;
	}
}
