package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.lknfe.xml.nfe.DetEvento;
import br.com.linkcom.lknfe.xml.nfe.EnvEvento;
import br.com.linkcom.lknfe.xml.nfe.Evento;
import br.com.linkcom.lknfe.xml.nfe.InfEvento;
import br.com.linkcom.lknfe.xml.nfe.Listaevento;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.ManifestoDfeEventoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ManifestoDfeEventoService extends GenericService<ManifestoDfeEvento> {

	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaoNfeService;
	private ManifestoDfeService manifestoDfeService;
	private ManifestoDfeEventoDAO manifestoDfeEventoDAO;
	private ArquivoService arquivoService;
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setConfiguracaoNfeService(ConfiguracaonfeService configuracaoNfeService) {
		this.configuracaoNfeService = configuracaoNfeService;
	}
	
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {
		this.manifestoDfeService = manifestoDfeService;
	}
	
	public void setManifestoDfeEventoDAO(ManifestoDfeEventoDAO manifestoDfeEventoDAO) {
		this.manifestoDfeEventoDAO = manifestoDfeEventoDAO;
	}

	public List<ManifestoDfeEvento> procurarPorManifestoDfe(ManifestoDfe manifestoDfe) {
		return manifestoDfeEventoDAO.procurarPorManifestoDfe(manifestoDfe);
	}

	public Boolean existeManifestoDfeEventoSituacaoDiferenteDe(String selectedItens, SituacaoEmissorEnum situacaoEnum) {
		return manifestoDfeEventoDAO.procurarManifestoDfeEventoSituacaoDiferenteDe(selectedItens, situacaoEnum).size() > 0;
	}

	public ManifestoDfeEvento procurarPorId(Integer cdManifestoDfeEvento) {
		return manifestoDfeEventoDAO.procurarPorId(cdManifestoDfeEvento);
	}
	
	public List<ManifestoDfeEvento> procurarPorIds(String cdManifestoDfeEvento) {
		return manifestoDfeEventoDAO.procurarPorIds(cdManifestoDfeEvento);
	}
	
	public ManifestoDfeEvento procurarEmpresaPorId(Integer cdManifestoDfeEvento) {
		return manifestoDfeEventoDAO.procurarEmpresaPorId(cdManifestoDfeEvento);
	}

	public ManifestoDfe procurarEmpresaPorIdManifestoDfe(Integer cdManifestoDfe) {
		return manifestoDfeService.procurarEmpresaPorId(cdManifestoDfe);
	}

	public String validarManifestoDfe(String selectedItens, String operacao) {
		return manifestoDfeService.validarManifestoDfe(selectedItens, operacao);
	}

	public ManifestoDfe procurarManifestoDfePorId(Integer cdManifestoDfe) {
		return manifestoDfeService.procurarPorId(cdManifestoDfe);
	}
	
	public List<ManifestoDfeEvento> procurarManifestoDfeEventoParaConsulta(Empresa empresa) {
		return manifestoDfeEventoDAO.procurarManifestoDfeEventoParaConsulta(empresa);
	}

	public String criarEventoManifestacaoDfe(ManifestoDfeEvento manifestoDfeEvento, Empresa empresa, Configuracaonfe configuracaoNfe) {
		if (empresa == null || empresa.getCnpj() == null) {
			empresa = empresaService.carregaEmpresa(empresa);
		}
		
		if (configuracaoNfe == null || configuracaoNfe.getUf() == null || configuracaoNfe.getUf().getCdibge() == null || configuracaoNfe.getUltNsu() == null || 
				configuracaoNfe.getPrefixowebservice() == null) {
			configuracaoNfe = configuracaoNfeService.carregaConfiguracao(configuracaoNfe);
		}
		
		Boolean ambienteProducao = !configuracaoNfe.getPrefixowebservice().name().endsWith("_HOM");
		
		DetEvento detEvento = new DetEvento(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
		detEvento.setDescEvento(manifestoDfeEvento.getTpEvento().getDescricao());
		detEvento.setVersao("1.00");
		
		if (TipoEventoNfe.OPERACAO_NAO_REALIZADA.equals(manifestoDfeEvento.getTpEvento())) {
			detEvento.setxJust("Essa operacao nao foi realizada");
		}
		
		InfEvento infEvento = new InfEvento();
		infEvento.setChNFe(manifestoDfeEvento.getChaveAcesso());
		infEvento.setcNPJ(empresa.getCnpj() != null && StringUtils.isNotEmpty(empresa.getCnpj().getValue()) ? empresa.getCnpj().getValue() : null);
		
		if (Prefixowebservice.AN_EVENTO_HOM.equals(configuracaoNfe.getPrefixowebservice()) || Prefixowebservice.AN_EVENTO_PROD.equals(configuracaoNfe.getPrefixowebservice())) {
			infEvento.setcOrgao("91");
		} else {
			if (configuracaoNfe.getUf().getCdibge() != null) {
				infEvento.setcOrgao(configuracaoNfe.getUf().getCdibge().toString());
			}
		}
		
		infEvento.setcPF(manifestoDfeEvento.getCpf() != null && StringUtils.isNotEmpty(manifestoDfeEvento.getCpf().getValue()) ? manifestoDfeEvento.getCpf().getValue() : null);
		infEvento.setDhEvento(SinedDateUtils.currentDate());
		infEvento.setId("ID" + manifestoDfeEvento.getTpEvento().getCodigo() + manifestoDfeEvento.getChaveAcesso() + "01");
		infEvento.setnSeqEvento(1);
		infEvento.setTpAmb(ambienteProducao ? 1 : 2);
		infEvento.setTpEvento(manifestoDfeEvento.getTpEvento().getCodigo());
		infEvento.setVerEvento("1.00");
		infEvento.setDetEvento(detEvento);
		
		Evento evento = new Evento();
		evento.setInfEvento(infEvento);
		evento.setVersao("1.00");
		
		List<Evento> listaEvento = new ArrayList<Evento>();
		listaEvento.add(evento);
		
		Listaevento listaevento = new Listaevento();
		listaevento.setListaevento(listaEvento);
		
		EnvEvento envEvento = new EnvEvento();
		envEvento.setIdLote("1");
		envEvento.setVersao("1.00");
		envEvento.setListaevento(listaevento);
		
		return envEvento.toString();
	}
	
	public Integer marcarManifestoDfeEventoService(String whereInManifestoDfeEvento, String flag) {
		return manifestoDfeEventoDAO.marcarManifestoDfeEventoService(whereInManifestoDfeEvento, flag);
	}

	@SuppressWarnings("unchecked")
	public Boolean processaRetornoManifestoDfeEvento(Arquivo arquivoRetornoXml, ManifestoDfeEvento manifestoDfeEventoEmissor) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		ManifestoDfeEvento manifestoDfeEvento = this.loadForEntrada(manifestoDfeEventoEmissor);
		
		manifestoDfeEvento.setArquivoRetornoXml(arquivoRetornoXml);
		
		arquivoService.saveFile(manifestoDfeEvento, "arquivoRetornoXml");
		arquivoService.saveOrUpdate(arquivoRetornoXml);
		
		Element retEnvEventoElement = SinedUtil.getRootElementXML(arquivoRetornoXml.getContent());
		
		if (retEnvEventoElement != null) {
			Element cStatLoteElement = SinedUtil.getChildElement("cStat", retEnvEventoElement.getContent());
			Element xMotivoLoteElement = SinedUtil.getChildElement("xMotivo", retEnvEventoElement.getContent());
			
			if (cStatLoteElement != null && StringUtils.isNotEmpty(cStatLoteElement.getValue()) && "128".equals(cStatLoteElement.getValue())) {
				Element retEventoElement = SinedUtil.getChildElement("retEvento", retEnvEventoElement.getContent());
				Element infEventoElement = SinedUtil.getChildElement("infEvento", retEventoElement.getContent());
				
				if (infEventoElement != null) {
					Element cStatElement = SinedUtil.getChildElement("cStat", infEventoElement.getContent());
					Element xMotivoElement = SinedUtil.getChildElement("xMotivo", infEventoElement.getContent());
					Element xEventoElement = SinedUtil.getChildElement("xEvento", infEventoElement.getContent());
					Element dhRegEventoElement = SinedUtil.getChildElement("dhRegEvento", infEventoElement.getContent());
					Element nProtElement = SinedUtil.getChildElement("nProt", infEventoElement.getContent());
					
					if (cStatElement != null && StringUtils.isNotEmpty(cStatElement.getValue()) && 
							("135".equals(cStatElement.getValue()) || "136".equals(cStatElement.getValue()) || "573".equals(cStatElement.getValue()))) {
						manifestoDfeEvento.setCoStat(cStatElement != null ? cStatElement.getValue() : null);
						manifestoDfeEvento.setMotivo(xMotivoElement != null ? xMotivoElement.getValue() : null);
						manifestoDfeEvento.setEvento(xEventoElement != null ? xEventoElement.getValue() : null);
						manifestoDfeEvento.setDhRegEvento(dhRegEventoElement != null ? SinedDateUtils.stringToTimestamp(dhRegEventoElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
						manifestoDfeEvento.setNuProt(nProtElement != null ? nProtElement.getValue() : null);
						
						manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_SUCESSO);
					} else {
						manifestoDfeEvento.setCoStat(cStatElement != null && StringUtils.isNotEmpty(cStatElement.getValue()) ? cStatElement.getValue() : null);
						manifestoDfeEvento.setMotivo(xMotivoElement != null && StringUtils.isNotEmpty(xMotivoElement.getValue()) ? xMotivoElement.getValue() : null);
						manifestoDfeEvento.setEvento(xEventoElement != null && StringUtils.isNotEmpty(xEventoElement.getValue()) ? xEventoElement.getValue() : null);
						manifestoDfeEvento.setDhRegEvento(dhRegEventoElement != null ? SinedDateUtils.stringToTimestamp(dhRegEventoElement.getValue(), "yyyy-MM-dd'T'HH:mm:ss") : null);
						manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_ERRO);
					}
				} else {
					throw new SinedException("Erro no XML de retorno de Manifestação de DF-e");
				}
			} else {
				manifestoDfeEvento.setCoStat(cStatLoteElement != null && StringUtils.isNotEmpty(cStatLoteElement.getValue()) ? cStatLoteElement.getValue() : null);
				manifestoDfeEvento.setMotivo(xMotivoLoteElement != null && StringUtils.isNotEmpty(xMotivoLoteElement.getValue()) ? xMotivoLoteElement.getValue() : null);
				manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.PROCESSADO_ERRO);
			}
		}
		
		this.saveOrUpdate(manifestoDfeEvento);
		
		return Boolean.TRUE;
	}
}