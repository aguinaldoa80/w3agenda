package br.com.linkcom.sined.geral.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSON;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.dao.PneuDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericPneuSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PneuWSBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.VinculoPneuBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.W3ProducaoUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.pneu.PneuRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneu.PneuW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoBandaProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoBandaProducaoW3producaoRESTWSBean;

public class PneuService extends GenericService<Pneu> {
	
	private ProducaoagendaService producaoagendaService;
	private ProducaoordemService producaoordemService;
	private VendaService vendaService;
	private PedidovendaService pedidovendaService;
	private PneuDAO pneuDAO;
	private ProducaochaofabricaService producaochaofabricaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ParametrogeralService parametrogeralService;
	private MaterialproducaoService materialproducaoService;
	private MaterialService materialService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private MaterialsimilarService materialsimilarService;
	private PneuSegmentoService pneuSegmentoService;
	
	public void setProducaoordemService(ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setPneuDAO(PneuDAO pneuDAO) {
		this.pneuDAO = pneuDAO;
	}
	public void setProducaochaofabricaService(ProducaochaofabricaService producaochaofabricaService) {
		this.producaochaofabricaService = producaochaofabricaService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setMaterialsimilarService(
			MaterialsimilarService materialsimilarService) {
		this.materialsimilarService = materialsimilarService;
	}
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {
		this.pneuSegmentoService = pneuSegmentoService;
	}

	/* singleton */
	private static PneuService instance;
	public static PneuService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneuService.class);
		}
		return instance;
	}

	public List<VinculoPneuBean> montaVinculo(Pneu pneu) {
		List<VinculoPneuBean> listaVinculo = new ArrayList<VinculoPneuBean>();
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findByPneu(pneu);
		for (Pedidovenda pedidovenda : listaPedidovenda) {
			listaVinculo.add(new VinculoPneuBean(VinculoPneuBean.PEDIDOVENDA, "Pedido de venda", pedidovenda.getCdpedidovenda()));
		}
		
		List<Venda> listaVenda = vendaService.findByPneu(pneu);
		for (Venda venda : listaVenda) {
			listaVinculo.add(new VinculoPneuBean(VinculoPneuBean.VENDA, "Venda", venda.getCdvenda()));
		}
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByPneu(pneu);
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			listaVinculo.add(new VinculoPneuBean(VinculoPneuBean.PRODUCAOAGENDA, "Agenda de produ��o", producaoagenda.getCdproducaoagenda()));
		}
		
		List<Producaoordem> listaProducaoordem = producaoordemService.findByPneu(pneu);
		for (Producaoordem producaoordem : listaProducaoordem) {
			listaVinculo.add(new VinculoPneuBean(VinculoPneuBean.PRODUCAOORDEM, "Ordem de produ��o", producaoordem.getCdproducaoordem()));
		}
		
		return listaVinculo;
	}
	
	public List<PneuW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de pneus para ser sincronizada com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PneuW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PneuW3producaoRESTModel> lista = new ArrayList<PneuW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneu p : pneuDAO.findForW3Producao(whereIn))
				lista.add(new PneuW3producaoRESTModel(p));
		}
		
		return lista;
	}
	
	class ContadorArquivoW3Producao {
		
		int contador = 0;
		
		private void increment(){
			contador++;
		}
		
		@Override
		public String toString() {
			return contador + "";
		}
		
	}
	
	
	/**
	 * M�todo que gera os arquivos JSON a serem enviados utilizados pelo W3Producao.
	 * @param nomeBanco
	 */
	public void gerarArquivosW3Producao(String nomeBanco){
		removerArquivosW3Producao(nomeBanco);
		
		ContadorArquivoW3Producao contador = new ContadorArquivoW3Producao();
		
		exportarUsuario(contador, nomeBanco);
		exportarEmpresa(contador, nomeBanco);
		exportarCliente(contador, nomeBanco);
		exportarUnidadeMedida(contador, nomeBanco);
		exportarProducaoEtapaNome(contador, nomeBanco);
		exportarProducaoEtapaCampo(contador, nomeBanco);
		exportarProducaoEtapa(contador, nomeBanco);
		exportarMaterialGrupo(contador, nomeBanco);
		exportarMaterial(contador, nomeBanco);
		exportarMaterialSimilar(contador, nomeBanco);
		exportarMaterialUnidademedida(contador, nomeBanco);
		exportarMaterialProducao(contador, nomeBanco);
		exportarPatrimonioItem(contador, nomeBanco);
		exportarProducaoEtapaItem(contador, nomeBanco);
		exportarMotivoDevolucao(contador, nomeBanco);
		exportarPneuMarca(contador, nomeBanco);
		exportarPneuModelo(contador, nomeBanco);
		exportarPneuMedida(contador, nomeBanco);
		exportarPneu(contador, nomeBanco);
		exportarPedidovenda(contador, nomeBanco);
		exportarMaterialPneuModelo(contador, nomeBanco);
		
	}
	
	
	/**
	 * M�todo que remove os arquivos JSON exitentes no diret�rio de JSONs do w3Pneu.
	 * @param nomeBanco
	 */
	private void removerArquivosW3Producao(String nomeBanco) {
		File[] files = new File(W3ProducaoUtil.getDirW3ProducaoJSON(nomeBanco)).listFiles(); 
		if(files != null && files.length > 0){
			for(File file : files) { 
				file.delete(); 
			}
		}
	}
	
	private void prepareAndSaveToFile(ContadorArquivoW3Producao contador,List<?> list, String filename,String nomeBanco){
		int pagina = 1;
		final Integer REGISTRO_POR_ARQUIVO = 1000;
		for (int i = 0; i < list.size(); i += REGISTRO_POR_ARQUIVO){
			int fim = (i + 1000 > list.size()) ? list.size() : i + 1000;
			saveToFile(View.convertToJson(list.subList(i, fim)), (contador + "_" + filename + "_" + pagina),nomeBanco);
			pagina++;
			contador.increment();
		}
	}
	
	/**
	* M�todo que salva os arquivos jsons na pasta de arquivos do w3producao
	*
	* @param json
	* @param filename
	*/
	private void saveToFile(JSON json, String filename, String nomeBanco) {
		try {
			final String SEPARATOR = System.getProperty("file.separator");
			String path = W3ProducaoUtil.getDirW3ProducaoJSON(nomeBanco);
			
			File fileNew = new File(path + SEPARATOR + filename + "_temp.json");
			String jsonStr = json.toString();
			FileOutputStream fileOS; 
			
			if(!fileNew.exists()){
				fileNew.getParentFile().mkdirs();
				fileNew.createNewFile();
			}
			fileOS = FileUtils.openOutputStream(fileNew);
			FileChannel outChannel = fileOS.getChannel();
			ByteBuffer buf = ByteBuffer.allocate(jsonStr.getBytes("iso-8859-1").length);
			
			byte[] bytes = jsonStr.getBytes("iso-8859-1");
			buf.put(bytes);
			buf.flip();
	
			outChannel.write(buf);
			
			fileOS.close();
			
			File fileOld = new File(path + SEPARATOR + filename + ".json");
			fileOld.delete();
			
			fileNew.renameTo(fileOld);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void exportarUsuario(ContadorArquivoW3Producao contador, String nomeBanco) {
		UsuarioService usuarioService = Neo.getObject(UsuarioService.class);
		prepareAndSaveToFile(contador, usuarioService.findForW3Producao(), "usuario", nomeBanco);
	}
	
	private void exportarEmpresa(ContadorArquivoW3Producao contador, String nomeBanco) {
		EmpresaService empresaService = Neo.getObject(EmpresaService.class);
		prepareAndSaveToFile(contador, empresaService.findForW3Producao(), "empresa", nomeBanco);
	}
	
	private void exportarCliente(ContadorArquivoW3Producao contador, String nomeBanco) {
		ClienteService clienteService = Neo.getObject(ClienteService.class);
		prepareAndSaveToFile(contador, clienteService.findForW3Producao(), "cliente", nomeBanco);
	}
	
	private void exportarUnidadeMedida(ContadorArquivoW3Producao contador, String nomeBanco) {
		UnidademedidaService unidademedidaService = Neo.getObject(UnidademedidaService.class);
		prepareAndSaveToFile(contador, unidademedidaService.findForW3Producao(), "unidademedida", nomeBanco);
	}
	
	private void exportarProducaoEtapaCampo(ContadorArquivoW3Producao contador, String nomeBanco) {
		ProducaoetapanomecampoService producaoetapanomecampoService = Neo.getObject(ProducaoetapanomecampoService.class);
		prepareAndSaveToFile(contador, producaoetapanomecampoService.findForW3Producao(), "producaoetapanomecampo", nomeBanco);
	}
	
	private void exportarProducaoEtapaNome(ContadorArquivoW3Producao contador, String nomeBanco) {
		ProducaoetapanomeService producaoetapanomeService = Neo.getObject(ProducaoetapanomeService.class);
		prepareAndSaveToFile(contador, producaoetapanomeService.findForW3Producao(), "producaoetapanome", nomeBanco);
	}
	
	private void exportarProducaoEtapa(ContadorArquivoW3Producao contador, String nomeBanco) {
		ProducaoetapaService producaoetapaService = Neo.getObject(ProducaoetapaService.class);
		prepareAndSaveToFile(contador, producaoetapaService.findForW3Producao(), "producaoetapa", nomeBanco);
	}
	
	private void exportarMaterial(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialService materialService = Neo.getObject(MaterialService.class);
		prepareAndSaveToFile(contador, materialService.findForW3Producao(), "material", nomeBanco);
	}
	
	private void exportarMaterialSimilar(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialsimilarService materialsimilarService = Neo.getObject(MaterialsimilarService.class);
		prepareAndSaveToFile(contador, materialsimilarService.findForW3Producao(), "materialsimilar", nomeBanco);
	}
	
	private void exportarMaterialUnidademedida(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialunidademedidaService materialunidademedidaService = Neo.getObject(MaterialunidademedidaService.class);
		prepareAndSaveToFile(contador, materialunidademedidaService.findForW3Producao(), "materialunidademedida", nomeBanco);
	}
	
	private void exportarMaterialProducao(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialproducaoService materialproducaoService = Neo.getObject(MaterialproducaoService.class);
		prepareAndSaveToFile(contador, materialproducaoService.findForW3Producao(), "materialproducao", nomeBanco);
	}
	
	private void exportarPatrimonioItem(ContadorArquivoW3Producao contador, String nomeBanco) {
		PatrimonioitemService patrimonioitemService = Neo.getObject(PatrimonioitemService.class);
		prepareAndSaveToFile(contador, patrimonioitemService.findForW3Producao(), "patrimonioitem", nomeBanco);
	}
	
	private void exportarProducaoEtapaItem(ContadorArquivoW3Producao contador, String nomeBanco) {
		ProducaoetapaitemService producaoetapaitemService = Neo.getObject(ProducaoetapaitemService.class);
		prepareAndSaveToFile(contador, producaoetapaitemService.findForW3Producao(), "producaoetapaitem", nomeBanco);
	}
	
	private void exportarMotivoDevolucao(ContadorArquivoW3Producao contador, String nomeBanco) {
		MotivodevolucaoService motivodevolucaoService = Neo.getObject(MotivodevolucaoService.class);
		prepareAndSaveToFile(contador, motivodevolucaoService.findForW3Producao(), "motivodevolucao", nomeBanco);
	}
	
	private void exportarPneuMarca(ContadorArquivoW3Producao contador, String nomeBanco) {
		PneumarcaService pneumarcaService = Neo.getObject(PneumarcaService.class);
		prepareAndSaveToFile(contador, pneumarcaService.findForW3Producao(), "pneumarca", nomeBanco);
	}
	
	private void exportarPneuModelo(ContadorArquivoW3Producao contador, String nomeBanco) {
		PneumodeloService pneumodeloService = Neo.getObject(PneumodeloService.class);
		prepareAndSaveToFile(contador, pneumodeloService.findForW3Producao(), "pneumodelo", nomeBanco);
	}
	
	private void exportarMaterialPneuModelo(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialpneumodeloService materialpneumodeloService = Neo.getObject(MaterialpneumodeloService.class);
		prepareAndSaveToFile(contador, materialpneumodeloService.findForW3Producao(), "materialpneumodelo", nomeBanco);
	}
	
	private void exportarPneuMedida(ContadorArquivoW3Producao contador, String nomeBanco) {
		PneumedidaService pneumedidaService = Neo.getObject(PneumedidaService.class);
		prepareAndSaveToFile(contador, pneumedidaService.findForW3Producao(), "pneumedida", nomeBanco);
	}
	
	private void exportarPneu(ContadorArquivoW3Producao contador, String nomeBanco) {
		prepareAndSaveToFile(contador, findForW3Producao(), "pneu", nomeBanco);
	}
	
	private void exportarPedidovenda(ContadorArquivoW3Producao contador, String nomeBanco) {
		prepareAndSaveToFile(contador, pedidovendaService.findForW3Producao(), "pedidovenda", nomeBanco);
	}
	
	private void exportarMaterialGrupo(ContadorArquivoW3Producao contador, String nomeBanco) {
		MaterialgrupoService materialgrupoService = Neo.getObject(MaterialgrupoService.class);
		prepareAndSaveToFile(contador, materialgrupoService.findForW3Producao(), "materialgrupo", nomeBanco);
	}
	
	public void enviarEmailErro(String assunto,String mensagem) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject(assunto)
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.PneuService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneuRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneuDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneuRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PneuRESTModel> lista = new ArrayList<PneuRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneu bean : pneuDAO.findForAndroid(whereIn))
				lista.add(new PneuRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdpneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneu insertWithId(Integer cdpneu) {
		return pneuDAO.insertWithId(cdpneu);
	}
	
	/**
	* M�todo que verifica se a banda do pneu foi alterada
	*
	* @param pneu
	* @param materialbanda
	* @return
	* @since 21/06/2016
	* @author Luiz Fernando
	*/
	public boolean bandaAlterada(Pneu pneu, Material materialbanda) {
		if(pneu != null && pneu.getCdpneu() != null){
			Pneu pneu_aux = loadForEntrada(pneu);
			if(pneu_aux != null){
				if((materialbanda == null && pneu_aux.getMaterialbanda() != null) ||
				   (materialbanda != null && pneu_aux.getMaterialbanda() == null) ||
				   (materialbanda != null && pneu_aux.getMaterialbanda() != null && !materialbanda.equals(pneu_aux.getMaterialbanda()))){
					return true;
				}
			}
		}
		return false;
	}

	/**
	* M�todo que verifica se um pneu � igual ao outro. (condi��o de igualdade: cdpneu ou serie/fogo)
	*
	* @param pneu1
	* @param pneu2
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public boolean pneuIgual(Pneu pneu1, Pneu pneu2){
		if((pneu1 == null && pneu2 != null) || (pneu1 != null && pneu2 == null)){
			return false;
		}
		
		boolean cdpneuDiferente = false;
		if((pneu1.getCdpneu() == null && pneu2.getCdpneu() != null) || 
				(pneu1.getCdpneu() != null && pneu2.getCdpneu() == null) ||
				(!pneu1.getCdpneu().equals(pneu2.getCdpneu()))){ 
			cdpneuDiferente = true;
		}
		
		boolean serieDiferente = false;
		if(!(pneu1.getSerie() == null && pneu2.getSerie() == null)){
			if((pneu1.getSerie() == null && pneu2.getSerie() != null) || 
					(pneu1.getSerie() != null && pneu2.getSerie() == null) ||
					(pneu1.getSerie() != null && pneu2.getSerie() == null) ||
					(!pneu1.getSerie().equals(pneu2.getSerie()))){ 
				serieDiferente = true;
			}
		}
		
		return !cdpneuDiferente || !serieDiferente;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pneu
	 * @param materialbanda
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public void updateBanda(Pneu pneu, Material materialbanda) {
		pneuDAO.updateBanda(pneu, materialbanda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdpneu
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public void atualizaW3producao(Integer cdpneu) {
		pneuDAO.atualizaW3producao(cdpneu);
	}
	
	public List<Pneu> findPneuComProducaoagenda(String whereInPneu, Producaoetapa producaoEtapa, Producaoagendasituacao... situacoes) {
		return pneuDAO.findPneuComProducaoagenda(whereInPneu, producaoEtapa, situacoes);
	}
	
	public void updatePneuqualificacao(Pneu pneu, Integer cdpneuqualificacao) {
		if (pneu!=null && cdpneuqualificacao!=null){
			pneuDAO.updatePneuqualificacao(pneu, cdpneuqualificacao);
		}
	}
	
	public Pneu getPneuProducaoagenda(Producaoagenda producaoagenda) {
		Pneu pneu = null;
		if(producaoagenda != null 
				&& producaoagenda.getListaProducaoagendamaterial() != null 
				&& producaoagenda.getListaProducaoagendamaterial().size() == 1){
			pneu = producaoagenda.getListaProducaoagendamaterial().iterator().next().getPneu();
		}
		return pneu;
	}
	
	public RetornoBandaProducaoW3producaoRESTModel retornoBandaProducaoForW3producao(RetornoBandaProducaoW3producaoRESTWSBean command) {
		RetornoBandaProducaoW3producaoRESTModel model = new RetornoBandaProducaoW3producaoRESTModel();
		
		try {
			if (command.getCdproducaochaofabrica()==null){
				model.setSincronizado(Boolean.TRUE);
				return model;
			}
			
			Producaochaofabrica producaochaofabrica = producaochaofabricaService.loadForRetorno(new Producaochaofabrica(command.getCdproducaochaofabrica()));
			Producaoagendamaterial producaoagendamaterial = producaochaofabrica.getProducaoagendamaterial();
			Pedidovendamaterial pedidovendamaterial = producaoagendamaterial.getPedidovendamaterial();
			
			Pneu pneu = new Pneu(command.getCdpneu());
			Material materialbanda = new Material(command.getCdmaterialbanda());
			updateBanda(pneu, materialbanda);
			
			if (pedidovendamaterial!=null) {
				pedidovendamaterialService.updateBandaAlterada(pedidovendamaterial.getCdpedidovendamaterial().toString());
			}
			
			Unidademedida unidademedidabanda = command.getCdunidademedida() != null && command.getCdunidademedida() > 0 ? new Unidademedida(command.getCdunidademedida()) : null;
			Double qtdeprevistabanda = command.getQtdeprevista();
			
			Material materialacompanhabanda = command.getCdmaterialacompanhabanda() != null && command.getCdmaterialacompanhabanda() > 0 ? new Material(command.getCdmaterialacompanhabanda()) : null;
			Double qtdeprevistaacompanhabanda = command.getQtdeprevistaacompanhabanda();
			Unidademedida unidademedidaacompanhabanda = command.getCdunidademedidaacompanhabanda() != null && command.getCdunidademedidaacompanhabanda() > 0 ? new Unidademedida(command.getCdunidademedidaacompanhabanda()) : null;
			
			producaoagendaService.doBaixaEstoqueBanda(producaoagendamaterial, materialbanda, qtdeprevistabanda, unidademedidabanda, materialacompanhabanda, qtdeprevistaacompanhabanda, unidademedidaacompanhabanda);
			
			producaoagendamaterialService.updateBandaenviada(producaoagendamaterial, Boolean.TRUE); 
			
			model.setSincronizado(Boolean.TRUE);
		} catch(Exception e){
			model.setSincronizado(Boolean.FALSE);
		}
		
		return model;
	}
	
	public boolean validaObrigatoriedadePneu(Material material, Pneu pneu, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(material != null && material.getCdmaterial() != null && pneu != null && 
				(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_CAMPOS_PNEU) || parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA))){
			if(isObrigaBanda(material.getCdmaterial()) && pneu.existeDados() && pneu.getMaterialbanda() == null){
				if(request != null || errors != null){
					String nome = material.getNome() == null ? materialService.loadSined(material).getNome() : material.getNome();
					if(errors != null){
						errors.reject("001", "O campo Banda do pneu � obrigat�rio. " + (StringUtils.isNotBlank(nome) ? "(Produto: " + nome + ")": ""));
					}else if(request != null){
						request.addError("O campo Banda do pneu � obrigat�rio. " + (StringUtils.isNotBlank(nome) ? "(Produto: " + nome + ")": ""));
					}
				}
				valido = false;
			}
		}
		return valido;
	}
	
	public boolean isObrigaBanda(Integer cdmaterial){
		Boolean isObrigaBanda = false;
		if(cdmaterial != null && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA)){
			for(Materialproducao mp: materialproducaoService.findListaProducao(cdmaterial)){
				if(Boolean.TRUE.equals(mp.getExibirvenda())){
					isObrigaBanda = true;
					break;
				}
			}
		}else{
			isObrigaBanda = parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_CAMPOS_PNEU);
		}
		return isObrigaBanda;
	}
	
	public Pneu informacoesPneu(WebRequestContext request, Pneu bean) {
		boolean escape = !"false".equals(request.getParameter("escape"));
		return this.informacoesPneu(request, bean, escape);
	}
	
	public Pneu informacoesPneu(WebRequestContext request, Pneu bean, boolean escape) {
		Pneu pneu = null;
		if(Util.objects.isPersistent(bean)){
			pneu = this.loadPneu(bean);
			if(Util.objects.isPersistent(pneu)){
				
				if(pneu.getPneuSegmento() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getPneuSegmento().getNome())){
					pneu.getPneuSegmento().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getPneuSegmento().getNome(), escape));
				}
				if(pneu.getPneumarca() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getPneumarca().getNome())){
					pneu.getPneumarca().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getPneumarca().getNome(), escape));
				}
				if(pneu.getPneumedida() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getPneumedida().getNome())){
					pneu.getPneumedida().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getPneumedida().getNome(), escape));
				}
				if(pneu.getPneumodelo() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getPneumodelo().getNome())){
					pneu.getPneumodelo().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getPneumodelo().getNome(), escape));
				}
				if(pneu.getPneuqualificacao() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getPneuqualificacao().getNome())){
					pneu.getPneuqualificacao().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getPneuqualificacao().getNome(), escape));
				}
				if(pneu.getOtrPneuTipo() != null && org.apache.commons.lang.StringUtils.isNotBlank(pneu.getOtrPneuTipo().getNome())){
					pneu.getOtrPneuTipo().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getOtrPneuTipo().getNome(), escape));
				}
				if(pneu.getMaterialbanda() != null){
					if(org.apache.commons.lang.StringUtils.isNotBlank(pneu.getMaterialbanda().getNome()))
						pneu.getMaterialbanda().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(pneu.getMaterialbanda().getNome(), escape));
					if(pneu.getMaterialbanda().getProfundidadesulco() != null){
						pneu.setProfundidadesulco(pneu.getMaterialbanda().getProfundidadesulco());
					}
				}
			}
		}
		return pneu;
	}
	
	public ModelAndView ajaxVerificaMaterialsimilarBanda(Material materialBanda){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		boolean existMaterialSimilar = false;
		if(Util.objects.isPersistent(materialBanda)){
			existMaterialSimilar = materialsimilarService.existMaterialsimilar(materialBanda);
		}
		return jsonModelAndView.addObject("existMaterialSimilar", existMaterialSimilar);
	}
	
	public ModelAndView ajaxDadosMaterialbanda(Pneu pneu){
		Double profundidadeSulco = null;
		if(pneu != null && pneu.getMaterialbanda() != null){
			Material banda = materialService.load(pneu.getMaterialbanda(), "material.profundidadesulco");
			if(banda != null){
				profundidadeSulco = banda.getProfundidadesulco();
			}
		}
		return new JsonModelAndView().addObject("profundidadeSulco", profundidadeSulco);
	}
	
	public List<Pneu> findAutocompletePneu(String value) {
		return pneuDAO.findAutocompletePneu(value);
	}
	
	public Pneu loadForWS(Pneu pneu) {
		return pneuDAO.loadForWS(pneu);
	}
	
	public boolean existeDados(Pneu bean){
		if(!Util.objects.isPersistent(bean.getPneuSegmento())){
			return false;
		}
		PneuSegmento pneuSegmento = pneuSegmentoService.load(bean.getPneuSegmento());
		return (Boolean.TRUE.equals(pneuSegmento.getMarca()) && bean.getPneumarca() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getMedida()) && bean.getPneumedida() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getModelo()) && bean.getPneumodelo() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getSerieFogo()) && bean.getSerie() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getDot()) && bean.getDot() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getBandaCamelbak()) && bean.getMaterialbanda() != null) ||
				(Boolean.TRUE.equals(pneuSegmento.getNumeroReformas()) && bean.getNumeroreforma() != null);
	}
	
	public ModelAndView ajaxFindForPesquisaPneu(GenericPneuSearchBean bean) {
		List<Pneu> listPneu = this.findForPesquisaPneu(bean);
		List<PneuWSBean> listaBean = new ArrayList<PneuWSBean>();
		for(Pneu pneu: listPneu){
			listaBean.add(new PneuWSBean(pneu));
		}
		return new JsonModelAndView()
						.addObject("count", this.countForPesquisaPneu(bean))
						.addObject("listPneu", listaBean);
	}
	
	public Long countForPesquisaPneu(GenericPneuSearchBean bean) {
		return pneuDAO.countForPesquisaPneu(bean);
	}
	
	public List<Pneu> findForPesquisaPneu(GenericPneuSearchBean bean) {
		return pneuDAO.findForPesquisaPneu(bean);
	}
	
	public Pneu loadPneu(Pneu pneu){
		return pneuDAO.loadPneu(pneu);
	}
}