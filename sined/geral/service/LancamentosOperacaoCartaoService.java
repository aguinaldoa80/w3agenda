package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.LancamentosOperacaoCartao;
import br.com.linkcom.sined.geral.dao.LancamentosOperacaoCartaoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LancamentosOperacaoCartaoService  extends GenericService<LancamentosOperacaoCartao>{
	
	private LancamentosOperacaoCartaoDAO lancamentosOperacaoCartaoDAO;
	
	public void setLancamentosOperacaoCartaoDAO(LancamentosOperacaoCartaoDAO lancamentosOperacaoCartaoDAO) {this.lancamentosOperacaoCartaoDAO = lancamentosOperacaoCartaoDAO;}

	public List<LancamentosOperacaoCartao> findForSpedFiscal1600(SpedarquivoFiltro filtro) {
		return lancamentosOperacaoCartaoDAO.findForSpedFiscal1600(filtro);
	}

	public boolean existeOperacaoPeriodoCredenciadora(LancamentosOperacaoCartao bean) {
		return lancamentosOperacaoCartaoDAO.existeOperacaoPeriodoCredenciadora(bean);
	}

	public boolean existeRegistroPeríodo(SpedarquivoFiltro filtro) {
		return lancamentosOperacaoCartaoDAO.existeRegistroPeríodo(filtro);
	}

}
