package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendaOtrService extends GenericService<Pedidovenda>{

	private PedidovendaService pedidovendaService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	
	@Override
	public ListagemResult<Pedidovenda> findForListagem(FiltroListagem filtro) {
		return pedidovendaService.findForListagem(filtro);
	}
	
	@Override
	public Pedidovenda loadForEntrada(Pedidovenda bean) {
		return pedidovendaService.loadForEntrada(bean);
	}
}
