package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocst;
import br.com.linkcom.sined.geral.dao.NaturezaoperacaocstDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NaturezaoperacaocstService extends GenericService<Naturezaoperacaocst> {
	
	private NaturezaoperacaocstDAO naturezaoperacaocstDAO;
	
	public void setNaturezaoperacaocstDAO(NaturezaoperacaocstDAO naturezaoperacaocstDAO) {
		this.naturezaoperacaocstDAO = naturezaoperacaocstDAO;
	}
	
	/**
	 * 
	 * @param naturezaoperacao
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacaocst> findByNaturezaoperacao(Naturezaoperacao naturezaoperacao){
		return naturezaoperacaocstDAO.findByNaturezaoperacao(naturezaoperacao);
	}	
}
