package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.EnderecoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTModel;
import br.com.linkcom.sined.wscep.service.CepSearch;
import br.com.linkcom.sined.wscep.service.CepSearchServiceLocator;

public class EnderecoService extends GenericService<Endereco> {	
	
	private EnderecoDAO enderecoDAO;
	public void setEnderecoDAO(EnderecoDAO enderecoDAO) {
		this.enderecoDAO = enderecoDAO;
	}
	
	/* singleton */
	private static EnderecoService instance;
	public static EnderecoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EnderecoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo para validar uma lista de endere�os quanto aos tipos duplicados.
	 * 
	 * @param errors
	 * @param listaEndereco
	 * @author Fl�vio Tavares
	 */
	public void validateListaEndereco(BindException errors, Collection<Endereco> listaEndereco){
		if(listaEndereco == null) return;
		
		int unico = 0, fatur = 0, cobr = 0, corr = 0, inativo = 0;
		
		for (Endereco end : listaEndereco) {
			if(end.getEnderecotipo().equals(Enderecotipo.UNICO)) unico++;
			else if(end.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)) fatur++;
			else if(end.getEnderecotipo().equals(Enderecotipo.COBRANCA)) cobr++;
			else if(end.getEnderecotipo().equals(Enderecotipo.CORRESPONDENCIA)) corr++;
			else if(end.getEnderecotipo().equals(Enderecotipo.INATIVO)) inativo++;
		}
		
		if(unico > 0 && (listaEndereco.size()-inativo) != 1){
			errors.reject("001","Se houver algum endere�o do tipo �nico, n�o deve haver outros endere�os cadastrados.");
		}
		
		if(fatur > 1 || cobr > 1 || corr > 1){
			errors.reject("001","N�o deve haver endere�os dos tipos Faturamento ou Cobran�a duplicados.");
		}
		
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Endereco carregaEnderecoEmpresa(Empresa empresa) {
		return EnderecoDAO.getInstance().carregaEnderecoEmpresa(empresa, false);
	}
	
	public Endereco carregaEnderecoAtivoEmpresa(Empresa empresa) {
		return EnderecoDAO.getInstance().carregaEnderecoEmpresa(empresa, true);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @return
	 * @param fornecedor
	 * @author Tom�s Rabelo
	 */
	public Endereco getEnderecoPrincipalFornecedor(Fornecedor fornecedor) {
		return enderecoDAO.getEnderecoPrincipalFornecedor(fornecedor);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO;
	 * Carrega todos os endere�os de uma pessoa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EnderecoDAO#carregarListaEndereco(Pessoa)
	 * @param pessoa
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Endereco> carregarListaEndereco(Pessoa pessoa) {
		return enderecoDAO.carregarListaEndereco(pessoa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EnderecoDAO#findForEtiqueta
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Endereco> findForEtiqueta(String whereIn) {
		return enderecoDAO.findForEtiqueta(whereIn);
	}
	
	public List<Endereco> findByCliente(Cliente cliente) {
		return enderecoDAO.findByCliente(cliente);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EnderecoDAO#findByFornecedor(Fornecedor fornecedor)
	*
	* @param fornecedor
	* @return
	* @since 23/09/2014
	* @author Luiz Fernando
	*/
	public List<Endereco> findByFornecedor(Fornecedor fornecedor) {
		return enderecoDAO.findByFornecedor(fornecedor);
	}
	
	public List<Endereco> findAtivosByCliente(Cliente cliente) {
		return enderecoDAO.findAtivosByCliente(cliente);
	}

	public Endereco loadEndereco(Endereco enderecoCliente) {
		return enderecoDAO.loadEndereco(enderecoCliente);
	}
	
	public Endereco loadEnderecoNota(Endereco enderecoCliente) {
		return enderecoDAO.loadEnderecoNota(enderecoCliente);
	}

	/**
	 * Faz refer�ncia ao DAO
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 04/08/2010
	 */
	public List<Endereco> findForEtiquetaColaborador(String whereIn) {
		return enderecoDAO.findForEtiquetaColaborador(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO
	 * @param endereco
	 * @param colaborador
	 */
	public void setPessoaEndereco(Endereco endereco, Colaborador colaborador) {
		enderecoDAO.setPessoaEndereco(endereco, colaborador); 
	}

	/**
	 * M�todo que carrega o endere�o do cliente seguindo a regra:
	 * Instala��o, Correspond�ncia, �nico, qualquer um
	 * 
	 * @param cliente
	 * @author Tom�s Rabelo
	 */
	public void carregaEnderecoCliente(Cliente cliente) {
		List<Endereco> listaEnderecos = carregarListaEndereco(cliente);
		Endereco enderecoAux = null;
		if(listaEnderecos != null && !listaEnderecos.isEmpty()){
			enderecoAux = listaEnderecos.get(0);
			for (Endereco endereco : listaEnderecos) {
				if(endereco.getEnderecotipo().equals(Enderecotipo.INSTALACAO) || endereco.getEnderecotipo().equals(Enderecotipo.CORRESPONDENCIA) || endereco.getEnderecotipo().equals(Enderecotipo.UNICO)){
					enderecoAux = endereco;
					break;
				}
			}
		}
		cliente.setEnderecoAux(enderecoAux);
	}
	
	public List<Endereco> findByPessoaWhereNotIn(Integer cdpessoa,String whereNotIn){
		return enderecoDAO.findByPessoaWhereNotIn(cdpessoa,whereNotIn);
	}
	
	public Endereco loadEndereco(String cep, String numero, Integer cdpessoa) {
		return enderecoDAO.loadEndereco(cep, numero, cdpessoa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#loadEnderecoLogradouro(String logradouro, String numero, Integer cdpessoa)
	 *
	 * @param logradouro
	 * @param numero
	 * @param cdpessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 16/10/2013
	 */
	public Endereco loadEnderecoLogradouro(String logradouro, String numero, Integer cdpessoa) {
		return enderecoDAO.loadEnderecoLogradouro(logradouro, numero, cdpessoa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.EnderecoDAO#carregaEnderecoComUfPais(Endereco endereco)
	 *
	 * @param endereco
	 * @return
	 * @author Luiz Fernando
	 */
	public Endereco carregaEnderecoComUfPais(Endereco endereco) {
		return enderecoDAO.carregaEnderecoComUfPais(endereco);
	}

	public void updatePessoa(Endereco endereco, Pessoa pessoa) {
		enderecoDAO.updatePessoa(endereco, pessoa);
	}
	
	public void updatePessoa(Endereco endereco, Nota pessoa) {
		enderecoDAO.updateNota(endereco, pessoa);
	}
	
	/**
	 * M�todo que carrega o endereco do cliente para o documento
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 * @since 16/10/2013
	 */
	public Endereco carregaEnderecoClienteForDocumento(Cliente cliente) {
		Endereco endereco = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Endereco> listaEnderecos = carregarListaEndereco(cliente);
			Endereco enderecoCobranca = null;
			Endereco enderecoFaturamento = null;
			Endereco enderecoEntrega = null;
			Endereco enderecoOutros = null;
			if(listaEnderecos != null && !listaEnderecos.isEmpty()){
				for (Endereco e : listaEnderecos) {
					if(e.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
						enderecoCobranca = e;
						break;
					}else if(e.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						enderecoFaturamento = e;
					}else if(e.getEnderecotipo().equals(Enderecotipo.ENTREGA)){
						enderecoEntrega = e;
					}else {
						if(enderecoOutros == null){
							enderecoOutros = e;
						}else if(Enderecotipo.INATIVO.equals(enderecoOutros.getEnderecotipo()) && !e.getEnderecotipo().equals(Enderecotipo.INATIVO)){
							enderecoOutros = e;
						}
					}
				}
			}
			if(enderecoCobranca != null) endereco = enderecoCobranca;
			else if(enderecoFaturamento != null) endereco = enderecoFaturamento;
			else if(enderecoEntrega != null) endereco = enderecoEntrega;
			else endereco = enderecoOutros;
		}
		return endereco;
	}
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * 
	 */
	public List<Endereco> findByPessoaWithoutInativo(Pessoa pessoa) {
		return this.findByPessoaWithoutInativo(pessoa, null);
	}
	
	public List<Endereco> findByPessoaWithoutInativo(Pessoa pessoa, Endereco endereco) {
		return enderecoDAO.findByPessoaWithoutInativo(pessoa, endereco);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param fornecedor
	 * @return
	 */
	public List<Endereco> findAtivosByFornecedor(Fornecedor fornecedor) {
		return enderecoDAO.findAtivosByForncedor(fornecedor);
	}
	
	public List<EnderecoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<EnderecoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<EnderecoRESTModel> lista = new ArrayList<EnderecoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Endereco m : enderecoDAO.findForAndroid(whereIn))
				lista.add(new EnderecoRESTModel(m));
		}
		
		return lista;
	}
	
	/**
	* M�todo que carrega o endere�o do fornecedor para o documento
	*
	* @param fornecedor
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public Endereco carregaEnderecoFornecedorForDocumento(Fornecedor fornecedor) {
		Endereco endereco = null;
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			List<Endereco> listaEnderecos = carregarListaEndereco(fornecedor);
			Endereco enderecoCobranca = null;
			Endereco enderecoFaturamento = null;
			Endereco enderecoEntrega = null;
			Endereco enderecoOutros = null;
			if(listaEnderecos != null && !listaEnderecos.isEmpty()){
				for (Endereco e : listaEnderecos) {
					if(e.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
						enderecoCobranca = e;
						break;
					}else if(e.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						enderecoFaturamento = e;
					}else if(e.getEnderecotipo().equals(Enderecotipo.ENTREGA)){
						enderecoEntrega = e;
					}else {
						enderecoOutros = e;
					}
				}
			}
			if(enderecoCobranca != null) endereco = enderecoCobranca;
			else if(enderecoFaturamento != null) endereco = enderecoFaturamento;
			else if(enderecoEntrega != null) endereco = enderecoEntrega;
			else endereco = enderecoOutros;
		}
		return endereco;
	}
	
	public Endereco getEnderecoPrincipalPessoa(Pessoa pessoa) {
		return enderecoDAO.getEnderecoPrincipalPessoa(pessoa);
	}

	public List<Endereco> findEnderecosDoClienteDoTipo(Cliente cliente, Enderecotipo ...enderecotipos) {
		return enderecoDAO.findEnderecosDoClienteDoTipo(cliente, Arrays.asList(enderecotipos));
	}
	
	public List<Endereco> findForVenda(Cliente cliente) {
		return this.findForVenda(cliente, false);
	}
	
	public List<Endereco> findForVenda(Cliente cliente, boolean allTipos) {
		List<Endereco> lista = this.findByPessoaWithoutInativo(cliente);
		if(allTipos){
			return lista;
		}
		List<Endereco> listaRetorno = new ArrayList<Endereco>();
		for (Endereco endereco : lista) {
			if (endereco.getEnderecotipo().equals(Enderecotipo.ENTREGA) || endereco.getEnderecotipo().equals(Enderecotipo.UNICO) || endereco.getEnderecotipo().equals(Enderecotipo.OUTRO)
					|| endereco.getEnderecotipo().equals(Enderecotipo.INSTALACAO)) {
				listaRetorno.add(endereco);
			}
		}
		return listaRetorno;
	}
	
	public Endereco getEnderecoFaturamento(Pessoa pessoa){
		return enderecoDAO.getEnderecoFaturamento(pessoa);
	}

	public ModelAndView ajaxEnderecoFaturamento(WebRequestContext request, Pessoa pessoa){
		Endereco endereco = this.getEnderecoFaturamento(pessoa);
		return new JsonModelAndView()
					.addObject("cdEnderecoFaturamento", endereco != null? endereco.getCdendereco(): null);
	}
	
	public Endereco findAddressWebServiceByCep(String cep) throws Exception{
		Endereco beanEndereco = null;
		CepSearchServiceLocator loc = new CepSearchServiceLocator();
		CepSearch cepSearch = loc.getCepSearch();;
		
		String endereco = cepSearch.findAddressByCep(cep.replaceAll("\\.", "").replaceAll("-", ""));;
		
		if (endereco!=null){
			String logradouro    = "";
			String complemento   = "";
			String bairro        = "";
			String nomeMunicipio = "";
			String siglaUf       = "";
			
			String[] campos = endereco.split("#",5);
			logradouro    = (campos.length > 0 ? campos[0] : "");
			complemento   = (campos.length > 1 ? campos[1] : "");
			bairro        = (campos.length > 2 ? campos[2] : "");
			nomeMunicipio = (campos.length > 3 ? campos[3] : "");
			siglaUf       = (campos.length > 4 ? campos[4] : "");

			
			beanEndereco = new Endereco();
			beanEndereco.setLogradouro(logradouro);
			beanEndereco.setComplemento(complemento);
			beanEndereco.setBairro(bairro);
			if (!nomeMunicipio.equals("") && !siglaUf.equals("")) {
				Municipio municipio = MunicipioService.getInstance().getByNomeAndUf( nomeMunicipio, siglaUf);
				if (municipio != null) {
					beanEndereco.setMunicipio(municipio);
					beanEndereco.setCdmunicipioaux(municipio.getCdmunicipio());
					beanEndereco.setCdufaux(municipio.getUf().getCduf());
				}
			}
		}else{
			throw new Exception("CEP informado n�o encontrado.");
		}
			
		return beanEndereco;
		
	}
}