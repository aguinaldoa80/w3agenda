package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.InventarioMaterialHistorico;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioMaterialEnum;
import br.com.linkcom.sined.geral.dao.InventarioMaterialHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InventarioMaterialHistoricoService extends GenericService<InventarioMaterialHistorico>{
	InventarioMaterialHistoricoDAO inventarioMaterialHistoricoDao;

	public void setInventarioMaterialHistoricoDao(
			InventarioMaterialHistoricoDAO inventarioMaterialHistoricoDao) {
		this.inventarioMaterialHistoricoDao = inventarioMaterialHistoricoDao;
	}
	
	public List<InventarioMaterialHistorico> findByInventarioMaterialHistorico (Inventariomaterial inventarioMaterial){
		return inventarioMaterialHistoricoDao.findByInventarioMaterialHistorico(inventarioMaterial);
	}
	
	public void preencherHistorico(Inventariomaterial inventarioMaterial, Boolean blocoK, Boolean blocoH) {
		InventarioMaterialHistorico novoHistorico = new InventarioMaterialHistorico();
		
		novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		if(blocoK != null){
			if(blocoK){
				novoHistorico.setAcao(InventarioMaterialEnum.GERARBLOCOK);
				novoHistorico.setInventarioMaterial(inventarioMaterial);
				novoHistorico.setObservacao("Bloco K marcado para o registro");
			}else{
				novoHistorico.setAcao(InventarioMaterialEnum.NAOGERARBLOCOK);
				novoHistorico.setInventarioMaterial(inventarioMaterial);
				novoHistorico.setObservacao("Bloco K desmarcado para o registro");
			}
		}
		if(blocoH!=null){
			if(blocoH){
				novoHistorico.setAcao(InventarioMaterialEnum.GERARBLOCOH);
				novoHistorico.setInventarioMaterial(inventarioMaterial);
				novoHistorico.setObservacao("Bloco H marcado para o registro");
			}else{
				novoHistorico.setAcao(InventarioMaterialEnum.NAOGERARBLOCOH);
				novoHistorico.setInventarioMaterial(inventarioMaterial);
				novoHistorico.setObservacao("Bloco H desmarcado para o registro");
			}
		}if(blocoH==null && blocoK==null){
			novoHistorico.setAcao(InventarioMaterialEnum.CRIADO);
			novoHistorico.setInventarioMaterial(inventarioMaterial);
			novoHistorico.setObservacao("Cria��o do Invetario de Material");
		}
		saveOrUpdate(novoHistorico);
	}
}
