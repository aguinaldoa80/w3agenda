package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipodocumentomilitar;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipodocumentomilitarService extends GenericService<Tipodocumentomilitar> {	
	
	
	
	/* singleton */
	private static TipodocumentomilitarService instance;
	public static TipodocumentomilitarService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipodocumentomilitarService.class);
		}
		return instance;
	}
	
}
