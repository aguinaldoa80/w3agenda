package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Notificacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.AvisousuarioDAO;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.thread.EmailManagerThread;

public class AvisousuarioService extends GenericService<Avisousuario> {
	
	private final String emailRemetente = "w3erp@w3erp.com.br";
	
	private AvisousuarioDAO avisousuarioDAO;
	private EmpresaService empresaService;
	private UsuarioService usuarioService;
	private EnvioemailService envioemailService;
	
	public void setAvisousuarioDAO(AvisousuarioDAO avisousuarioDAO) {
		this.avisousuarioDAO = avisousuarioDAO;
	}

	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	private static AvisousuarioService instance;
	public static AvisousuarioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AvisousuarioService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que salva avisos do usu�rio como lidos
	 * 
	 * @see #buscaItensQueNaoForamLidos(List)
	 * @see #salvaListaAvisoUsuario(List)
	 * @param avisos
	 * @author Tom�s Rabelo
	 */
	public void marcaItensComoLidos(List<Aviso> avisos) {
		List<Avisousuario> list = buscaItensQueNaoForamLidos(avisos);
		
		if(list != null && list.size() > 0)
			salvaListaAvisoUsuario(list);
	}

	
	/**
	 * M�todo que salva os avisos.
	 * 
	 * @see #saveOrUpdateNoUseTransaction(Avisousuario)
	 * @param list
	 * @author Tom�s Rabelo
	 */
	private void salvaListaAvisoUsuario(final List<Avisousuario> list) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Avisousuario avisousuario : list) 
					saveOrUpdateNoUseTransaction(avisousuario);
				
				return status;
			}});
	}

	/**
	 * M�todo que pega somente os avisos que ainda n�o foram salvos.
	 * 
	 * @param avisos
	 * @author Tom�s Rabelo
	 */
	private List<Avisousuario> buscaItensQueNaoForamLidos(List<Aviso> avisos) {
		List<Avisousuario> list = new ListSet<Avisousuario>(Avisousuario.class);
		Usuario usuario = (Usuario)Neo.getUser();
		for (Aviso aviso : avisos) 
			if(aviso.getCdavisousuario() == 0)
				list.add(new Avisousuario(aviso, usuario));
		
		return list;
	}


	/**
	 * Deleta avisos lidos
	 * 
	 * @see #buscaItensQueForamLidos(List)
	 * @see #delete(Avisousuario)
	 * @param avisos
	 * @author Tom�s Rabelo
	 */
	public void marcaItensComoNaoLidos(List<Aviso> avisos) {
		List<Avisousuario> list = buscaItensQueForamLidos(avisos);
		
		if(list != null && list.size() > 0)
			for (Avisousuario avisousuario : list)
				delete(avisousuario);
	}


	/**
	 * M�todo que busca os avisos que est�o marcados como lidos
	 * 
	 * @param avisos
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Avisousuario> buscaItensQueForamLidos(List<Aviso> avisos) {
		List<Avisousuario> list = new ListSet<Avisousuario>(Avisousuario.class);
		for (Aviso aviso : avisos) 
			if(aviso.getCdavisousuario() != 0)
				list.add(new Avisousuario(aviso.getCdavisousuario()));
		
		return list;
	}
	
	public List<Avisousuario> findAvisousuario(Usuario usuario, String whereInEmpresa, boolean notificado, Integer limit, Date dtInicio, Boolean notifica) {
		return avisousuarioDAO.findAvisousuario(usuario, whereInEmpresa, notificado, limit, dtInicio, notifica);
	}

	public void updateNotificado(Avisousuario avisousuario) {
		avisousuarioDAO.updateNotificado(avisousuario);
	}
	
	public void updateLido(Avisousuario avisousuario) {
		avisousuarioDAO.updateLido(avisousuario);
	}
	
	public void updateLidoTodos(Usuario usuario, String whereInAviso) {
		avisousuarioDAO.updateLidoTodos(usuario, whereInAviso);
	}

	public Integer getQtdeNaoLidos(Usuario usuarioLogado, String whereInEmpresa) {
		return avisousuarioDAO.getQtdeNaoLidos(usuarioLogado, whereInEmpresa);
	}
	
	public void deleteAllFromAviso(Aviso aviso){
		avisousuarioDAO.deleteAllFromAviso(aviso);
	}

	public boolean existeUsuarioNotificado(Aviso aviso) {
		return avisousuarioDAO.existeUsuarioNotificado(aviso);
	}
	
	public void atualizarAvisoComoLido(String selectedItem, String cdusuario) {
		avisousuarioDAO.atualizarAvisoComoLido(selectedItem, cdusuario);
	}
	
	public List<Notificacao> findAllNaoLidasNotificacao() {
		return avisousuarioDAO.findAllNaoLidasNotificacao();
	}
	
	public List<Notificacao> findAllNaoLidasPopup() {
		return avisousuarioDAO.findAllNaoLidasPopup();
	}
	
	public Avisousuario findAvisoNaoLidasEmail() {
		return avisousuarioDAO.findAvisoNaoLidasEmail();
	}
	
	public void envioEmailAvisoDiario() {
		EmailManager email = null;
		Envioemail envioEmail = null;
		Empresa empresa = empresaService.loadPrincipal();
		TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioEmailAvisoDiario.tpl");
		String template = null;
		StringBuilder avisosTempalte;
		
		for (Usuario u : usuarioService.findAll()) {
			if (!u.getLogin().equals("admin") && (u.getBloqueado() == null || !u.getBloqueado())) {
				List<Notificacao> notificacaoList = avisousuarioDAO.findAllEnvioEmailDiario(u);
				
				if (notificacaoList != null && notificacaoList.size() > 0) {
					avisosTempalte = new StringBuilder();
					avisosTempalte.append("<table><tr class='header'><td>Assunto</td><td class='header center'>Quantidade</td><td class='header center'>Visualizar</td></tr>");
					
					for (int i = 0 ; i < notificacaoList.size() ; i++) {
						avisosTempalte.append("<tr " + (i % 2 == 0 ? "style='background-color: #dadada;'" : "background-color: #f5f7f8;") + "><td>" + notificacaoList.get(i).getDescricao() + "</td><td class='center'>" + notificacaoList.get(i).getQuantidade() + "</td><td class='center'>" +
								"<a style='text-decoration: none;' target='_blank' href='" + SinedUtil.getUrlWithoutContext() + "/w3erp/sistema/crud/Aviso?ACAO=listagem&lido=false&amp;notificacao=true&cdmotivoaviso=" + 
								notificacaoList.get(i).getCdmotivoaviso() + "&cdusuario=" + u.getCdpessoa() + "'><img src=\""+SinedUtil.getUrlWithoutContext()+"/w3erp/imagens/consultar_icon.gif\" border=\"0\"></a></td></tr>");
					}
					
					avisosTempalte.append("</table>");
					
					try {
						template = templateAux
								.assign("avisosTemplate", avisosTempalte.toString())
								.assign("empresa", empresa.getNome())
								.getTemplate();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
					
					envioEmail = envioemailService.registrarEnvio(emailRemetente, email.getSubject(), template, Envioemailtipo.ENVIO_AVISO_DIARIO, u, email.getTo(), u.getNome(), email);
					
					try {			
						email
							.setFrom(emailRemetente)
							.setSubject(Envioemailtipo.ENVIO_AVISO.getNome())
							.setTo(u.getEmail())
							.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioEmail, u.getEmail()));
						
						if (SinedUtil.isAmbienteDesenvolvimento()) {
							Thread.sleep(1000);
						}
						if(!Boolean.TRUE.equals(u.getBloqueado())){
							String subdominio = SinedUtil.getSubdominioClienteByThread();
							if (subdominio != null) {
								new EmailManagerThread(email, subdominio).start();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void updatePreparadoEnvioEmail(Avisousuario avisousuario, Boolean preparadoEnvioEmail) {
		avisousuarioDAO.updatePreparadoEnvioEmail(avisousuario, preparadoEnvioEmail);
	}
	
	public void updateAvisoComoEnviadoEmail(Avisousuario avisousuario) {
		avisousuarioDAO.updateAvisoComoEnviadoEmail(avisousuario);
	}

	public List<Avisousuario> findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(Motivoaviso motivoaviso, Integer cdagendamento, Date lastAvisoDate) {
		return avisousuarioDAO.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(motivoaviso, cdagendamento, lastAvisoDate);
	}
}