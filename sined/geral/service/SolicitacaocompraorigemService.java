package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.geral.dao.SolicitacaocompraorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaocompraorigemService extends GenericService<Solicitacaocompraorigem> {
	
	private SolicitacaocompraorigemDAO solicitacaocompraorigemDAO;
	
	public void setSolicitacaocompraorigemDAO(SolicitacaocompraorigemDAO solicitacaocompraorigemDAO) {
		this.solicitacaocompraorigemDAO = solicitacaocompraorigemDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraorigemDAO#removeVinculoOrigemRecustogeral(Solicitacaocompraorigem solicitacaocompraorigem)
	*
	* @param solicitacaocompraorigem
	* @since 11/06/2015
	* @author Luiz Fernando
	*/
	public void removeVinculoOrigemRecustogeral(Solicitacaocompraorigem solicitacaocompraorigem) {
		solicitacaocompraorigemDAO.removeVinculoOrigemRecustogeral(solicitacaocompraorigem);
	}
}
