package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ColetaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ColetaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirColetaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirColetaMaterialBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoitemBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ServicoReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColetaService extends GenericService<Coleta> {
	
	private ColetaDAO coletaDAO;
	private PedidovendaService pedidovendaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ProducaoordemService producaoordemService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ColetaMaterialService coletaMaterialService;
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ColetahistoricoService coletahistoricoService;
	private ProducaoordemhistoricoService producaoordemhistoricoService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private DocumentoService documentoService;
	private DocumentohistoricoService documentohistoricoService;
	private DocumentoorigemService documentoorigemService;
	private ContapagarService contapagarService;
	private ValecompraService valecompraService;
	private ValecompraorigemService valecompraorigemService;
	private ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService;
	private EmpresaService empresaService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private ParametrogeralService parametrogeralService;
	private PneuService pneuService;
	private EntradafiscalService entradafiscalService;
	private ClientevendedorService clientevendedorService;
	private NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	
	public void setProducaoordemService(
			ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setPedidovendahistoricoService(
			PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setPedidovendamaterialService(
			PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setColetaDAO(ColetaDAO coletaDAO) {
		this.coletaDAO = coletaDAO;
	}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setColetahistoricoService(ColetahistoricoService coletahistoricoService) {
		this.coletahistoricoService = coletahistoricoService;
	}
	public void setProducaoordemhistoricoService(ProducaoordemhistoricoService producaoordemhistoricoService) {
		this.producaoordemhistoricoService = producaoordemhistoricoService;
	}
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setValecompraorigemService(ValecompraorigemService valecompraorigemService) {
		this.valecompraorigemService = valecompraorigemService;
	}
	public void setColetamaterialmotivodevolucaoService(ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService) {
		this.coletamaterialmotivodevolucaoService = coletamaterialmotivodevolucaoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {
		this.clientevendedorService = clientevendedorService;
	}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {
		this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;
	}
	
	/**
	 * M�todo que gera a lista de beans para ser utilizado no template
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public LinkedList<EmitirColetaBean> gerarListaDadosForEmitirFichaColeta(String whereIn, String whereInPedidoVendaMaterial) throws Exception{
		LinkedList<EmitirColetaBean> lista = new LinkedList<EmitirColetaBean>();
		List<Coleta> listaColeta = coletaDAO.findForEmitirFichaColeta(whereIn, whereInPedidoVendaMaterial);
		if(listaColeta != null && !listaColeta.isEmpty()){
			HashMap<Cliente, String> mapClienteVendedorPrincipal = new HashMap<Cliente, String>();
			for(Coleta coleta : listaColeta){
				if(coleta.getCdcoleta() != null){
					coleta.setListaNotafiscalprodutoColeta(notafiscalprodutoColetaService.findNotaDevolucao(coleta.getCdcoleta().toString(), coleta.getWhereInPedidoVendaMaterial(), Boolean.FALSE));
				}
				
				EmitirColetaBean bean = new EmitirColetaBean();
				bean.setCodigoColeta(coleta.getCdcoleta());
				bean.setMotivodevolucao(coleta.getMotivodevolucao());
				bean.setObservacao(coleta.getObservacao());
				bean.setTipo(coleta.getTipo().name());
				if(Coleta.TipoColeta.CLIENTE.equals(coleta.getTipo())){
					bean.setPessoa(coleta.getCliente().getNome());
				}else if(Coleta.TipoColeta.FORNECEDOR.equals(coleta.getTipo())){
					bean.setPessoa(coleta.getFornecedor().getNome());
				}
				bean.setColetor(coleta.getColaborador() != null ? coleta.getColaborador().getNome() : "");
				if(coleta.getPedidovenda() != null){
					coleta.getPedidovenda().setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(coleta.getPedidovenda().getCliente(), mapClienteVendedorPrincipal));
					bean.setResponsavelPedido(coleta.getPedidovenda().getColaborador() != null ? coleta.getPedidovenda().getColaborador().getNome() : "");
					bean.setPedidovenda_id(coleta.getPedidovenda().getCdpedidovenda());
					if(coleta.getPedidovenda().getEndereco() != null){
						bean.setClienteEndereco(coleta.getPedidovenda().getEndereco().getLogradouroCompleto());
					}
					bean.setDataPedido(coleta.getPedidovenda().getDtpedidovenda() != null ? SinedDateUtils.toString(coleta.getPedidovenda().getDtpedidovenda()) : "");
					bean.setIdentificadorPedido(coleta.getPedidovenda().getIdentificador());
					bean.setVendedorPrincipalPedido(coleta.getPedidovenda().getVendedorprincipal());
					bean.setIdentificadorExternoPedido(coleta.getPedidovenda().getIdentificacaoexterna());
				}
				bean.setNotaRemessaEntrega(SinedUtil.isListNotEmpty(coleta.getListaEntregadocumentoColeta()) ? 1 : 0);
				bean.setNotaRemessaPropria(0);
				bean.setNotaRetorno(0);
				
				if(coleta.getListaNotafiscalprodutoColeta() != null){
					StringBuilder sb = new StringBuilder();
					for(NotafiscalprodutoColeta nfpc : coleta.getListaNotafiscalprodutoColeta()){
						if(nfpc.getNotafiscalproduto() != null){
							if(Tipooperacaonota.SAIDA.equals(nfpc.getNotafiscalproduto().getTipooperacaonota())){
								bean.setNotaRetorno(1);
							}
							if(Tipooperacaonota.ENTRADA.equals(nfpc.getNotafiscalproduto().getTipooperacaonota())){
								bean.setNotaRemessaPropria(1);
							}
							if(!(sb.length() == 0)) sb.append(", ");
							sb.append(nfpc.getNotafiscalproduto().getCdNota());
						}
					}
					bean.setNotafiscal_ids(sb.toString());
				}
				if(coleta.getListaEntregadocumentoColeta() != null){
					StringBuilder sb = new StringBuilder();
					for(EntregadocumentoColeta edc : coleta.getListaEntregadocumentoColeta()){
						if(!(sb.length() == 0)) sb.append(", ");
						sb.append(edc.getEntregadocumento().getCdentregadocumento());
					}
					bean.setEntradafiscal_ids(sb.toString());
				}
				if(coleta.getListaColetaMaterial() != null && !coleta.getListaColetaMaterial().isEmpty()){
					for(ColetaMaterial cm : coleta.getListaColetaMaterial()){
						EmitirColetaMaterialBean ecmb = new EmitirColetaMaterialBean();
						ecmb.setLocalentrada_nome(cm.getLocalarmazenagem().getNome());
						ecmb.setMaterial_nome(cm.getMaterial().getNome());
						ecmb.setMaterial_codigo(cm.getMaterial().getCdmaterial() != null ? cm.getMaterial().getCdmaterial().toString() : "");
						ecmb.setObservacao(cm.getObservacao());
						ecmb.setQuantidade(cm.getQuantidade());
						ecmb.setQuantidadedevolvida(cm.getQuantidadedevolvida());
						ecmb.setValorunitario(cm.getValorunitario());
						
						if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
							LinkedList<ServicoReportBean> listaServicoReportBean = new LinkedList<ServicoReportBean>();
							LinkedList<Integer> listaCdpedidovendamaterial = new LinkedList<Integer>();
							
							for(ColetaMaterialPedidovendamaterial itemServico : cm.getListaColetaMaterialPedidovendamaterial()) {
								if(itemServico.getPedidovendamaterial() != null) {
									if(ecmb.getServicoReportBean() == null){
										ecmb.setServicoReportBean(new ServicoReportBean(itemServico.getPedidovendamaterial()));
									}
									
									listaServicoReportBean.add(new ServicoReportBean(itemServico.getPedidovendamaterial()));
									listaCdpedidovendamaterial.add(itemServico.getPedidovendamaterial().getCdpedidovendamaterial());
									
									if(itemServico.getPedidovendamaterial().getPneu() != null && ecmb.getPneuReportBean() == null) {
										ecmb.setPneuReportBean(new PneuReportBean(itemServico.getPedidovendamaterial().getPneu()));
									} else {
										continue;
									}
								}
							}
							
							if(SinedUtil.isListNotEmpty(listaServicoReportBean)) {
								ecmb.setListaServicoReportBean(listaServicoReportBean);
							}
							ecmb.setListaCdpedidovendamaterial(listaCdpedidovendamaterial);
						}
						
//						ecmb.setServicoReportBean(new ServicoReportBean(cm.getPedidovendamaterial()));
//						if(cm.getMotivodevolucao() != null){
//							ecmb.setMotivodevolucao_descricao(cm.getMotivodevolucao().getDescricao());
//						}
						if(Hibernate.isInitialized(cm.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(cm.getListaColetamaterialmotivodevolucao())){
							ecmb.setMotivodevolucao_descricao(CollectionsUtil.listAndConcatenate(cm.getListaColetamaterialmotivodevolucao(), "motivodevolucao.descricao", ", "));
						}							
//						if(cm.getPedidovendamaterial() != null){
//							ecmb.setCdpedidovendamaterial(cm.getPedidovendamaterial().getCdpedidovendamaterial());
//						}
//						if(cm.getPedidovendamaterial() != null && cm.getPedidovendamaterial().getPneu() != null){
//							ecmb.setPneuReportBean(new PneuReportBean(cm.getPedidovendamaterial().getPneu()));
//						}
						bean.getListaColetaMaterial().add(ecmb);
					}
				}
				lista.add(bean);
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/07/2014
	 */
	public List<Coleta> loadForGerarNotafiscalproduto(String whereIn) {
		return loadForGerarNotafiscalproduto(whereIn, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ColetaDAO#loadForGerarNotafiscalproduto(String whereIn, String whereInPedidovendamaterial)
	*
	* @param whereIn
	* @param whereInPedidovendamaterial
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public List<Coleta> loadForGerarNotafiscalproduto(String whereIn, String whereInPedidovendamaterial) {
		return coletaDAO.loadForGerarNotafiscalproduto(whereIn, whereInPedidovendamaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/07/2014
	 */
	public List<Coleta> findByWhereInPedidovenda(String whereIn) {
		return findByWhereInPedidovenda(whereIn, null);
	}
	
	public List<Coleta> findByWhereInVenda(String whereIn) {
		return findByWhereInVenda(whereIn, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ColetaDAO#findByWhereInPedidovenda(String whereIn, String whereInPedidovendamaterial)
	*
	* @param whereIn
	* @param whereInPedidovendamaterial
	* @return
	* @since 21/01/2016
	* @author Luiz Fernando
	*/
	public List<Coleta> findByWhereInPedidovenda(String whereIn, String whereInPedidovendamaterial) {
		return coletaDAO.findByWhereInPedidovenda(whereIn, whereInPedidovendamaterial);
	}
	
	public List<Coleta> findByWhereInVenda(String whereIn, String whereInVendamaterial) {
		return coletaDAO.findByWhereInVenda(whereIn, whereInVendamaterial);
	}
	
	/**
	 * Prepara os dados para a tela de lan�amento de estorno da devolu��o
	 *
	 * @param listaColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2015
	 */
	public List<ColetaMaterial> prepareForEstornarDevolucaoColeta(List<Coleta> listaColeta) {
		List<ColetaMaterial> listaColetaMaterialTela = new ArrayList<ColetaMaterial>();
		
		for (Coleta coleta : listaColeta) {
			List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
			for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
				if(coletaMaterial.getQuantidadedevolvida() == null || coletaMaterial.getQuantidadedevolvida() <= 0){
					continue;
				}
				if(coletaMaterial.getQuantidadeestorno() == null){
					coletaMaterial.setQuantidadeestorno(coletaMaterial.getQuantidadedevolvida());
				}
				
				coletaMaterial.setColeta(coleta);
				listaColetaMaterialTela.add(coletaMaterial);
			}
		}
		
		// ORDENA PELO NOME DO MATERIAL
		Collections.sort(listaColetaMaterialTela, new Comparator<ColetaMaterial>(){
			public int compare(ColetaMaterial o1, ColetaMaterial o2) {
				if(o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
					return o1.getColeta().getCdcoleta().compareTo(o2.getColeta().getCdcoleta());
				} else return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		return listaColetaMaterialTela;
	}
	
	/**
	 * Prepara os dados para a tela de lan�amento de registro de devolu��o.
	 *
	 * @param listaColeta
	 * @return
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public List<ColetaMaterial> prepareForDevolverColeta(List<Coleta> listaColeta) throws Exception {
		List<ColetaMaterial> listaColetaMaterialTela = new ArrayList<ColetaMaterial>();
		
		if(SinedUtil.isListNotEmpty(listaColeta)){
			for (Coleta coleta : listaColeta) {
				List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
				for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
					if(coletaMaterial.getQuantidade() == null){
						coletaMaterial.setQuantidade(0.);
					}
					if(coletaMaterial.getQuantidadedevolvida() == null){
						coletaMaterial.setQuantidadedevolvida(0.);
					}
					
					Double quantidadeColetada = coletaMaterial.getQuantidade();
					Double quantidadeVenda = 0d;
					Double quantidadeDevolvida = 0d;
					Double quantidadeComprada = 0d;
					
//					Pedidovendamaterial pedidovendamaterial = coletaMaterial.getPedidovendamaterial();
					if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
						for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
							if(itemServico.getPedidovendamaterial() != null){
								Set<Vendamaterial> listaVendamaterial = itemServico.getPedidovendamaterial().getListaVendamaterial();
								if(listaVendamaterial != null && listaVendamaterial.size() > 0){
									for (Vendamaterial vendamaterial : listaVendamaterial) {
										if(vendamaterial != null && 
												vendamaterial.getVenda() != null && 
												vendamaterial.getVenda().getVendasituacao() != null &&
												!vendamaterial.getVenda().getVendasituacao().equals(Vendasituacao.CANCELADA)){
											quantidadeVenda += vendamaterial.getQuantidade();
										}
									}
								}
							}
						}
					}
					
//					if(pedidovendamaterial != null){
//						Set<Vendamaterial> listaVendamaterial = pedidovendamaterial.getListaVendamaterial();
//						if(listaVendamaterial != null && listaVendamaterial.size() > 0){
//							for (Vendamaterial vendamaterial : listaVendamaterial) {
//								if(vendamaterial != null && 
//										vendamaterial.getVenda() != null && 
//										vendamaterial.getVenda().getVendasituacao() != null &&
//										!vendamaterial.getVenda().getVendasituacao().equals(Vendasituacao.CANCELADA)){
//									quantidadeVenda += vendamaterial.getQuantidade();
//								}
//							}
//						}
//					}
					
					if(coletaMaterial.getQuantidadedevolvida() != null){
						quantidadeDevolvida = coletaMaterial.getQuantidadedevolvida();
					}
					if(coletaMaterial.getQuantidadecomprada() != null){
						quantidadeComprada = coletaMaterial.getQuantidadecomprada();
					}
					
					Double qtdeDisponivel = quantidadeColetada - quantidadeDevolvida - quantidadeComprada - quantidadeVenda;
					
					coletaMaterial.setQuantidade(qtdeDisponivel > 0. ? qtdeDisponivel : 0.);
					coletaMaterial.setQuantidadedevolvida(0.);
					coletaMaterial.setColeta(coleta);
					
					listaColetaMaterialTela.add(coletaMaterial);
				}
			}
			
			// ORDENA PELO NOME DO MATERIAL
			Collections.sort(listaColetaMaterialTela, new Comparator<ColetaMaterial>(){
				public int compare(ColetaMaterial o1, ColetaMaterial o2) {
					if(o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
						return o1.getColeta().getCdcoleta().compareTo(o2.getColeta().getCdcoleta());
					} else return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				}
			});
		}
		
		return listaColetaMaterialTela;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public Coleta loadForPagamento(Coleta coleta) {
		return coletaDAO.loadForPagamento(coleta);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Coleta> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return coletaDAO.loadWithLista(whereIn, orderBy, asc);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param data
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public void updateDtConfirmacaoDevolucao(String whereIn, Date data) {
		coletaDAO.updateDtConfirmacaoDevolucao(whereIn, data);
	}

	public void updateSituacaoPedidovendaProducaoordemByColeta(String whereInColeta) {
		if(StringUtils.isNotBlank(whereInColeta)){
			// ATUALIZA AS SITUA��ES DOS PEDIDOS DE VENDA RELACIONADOS COM A COLETA
			List<Pedidovenda> listaPedidovenda = pedidovendaService.findByColeta(whereInColeta);
			
			for (Pedidovenda pedidovenda : listaPedidovenda) {
				Pedidovendasituacao pedidovendasituacao = pedidovenda.getPedidovendasituacao();
				
				boolean confirmado_total = true;
				for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
					Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
					if(qtdeRestante == null || qtdeRestante > 0){
						confirmado_total = false;
						break;
					}
				}
				
				if(confirmado_total){
					Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
					pedidovendahistorico.setPedidovenda(pedidovenda);
					pedidovendahistorico.setObservacao("Coleta comprada/devolvida");
					if(pedidovendasituacao.equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
						pedidovendahistorico.setAcao("Confirmado");
						pedidovendaService.updateConfirmacao(pedidovenda.getCdpedidovenda().toString());
					}
					pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param motivodevolucao
	 * @author Rodrigo Freitas
	 * @since 27/04/2015
	 */
	public void updateMotivoDevolucao(String whereIn, String motivodevolucao) {
		coletaDAO.updateMotivoDevolucao(whereIn, motivodevolucao);
	}
	
	/**
	* M�todo que retorna a empresa de acordo com as coletas. Caso exista mais de uma empresa, ir� retornar nulo.
	*
	* @param lista
	* @return
	* @since 28/05/2015
	* @author Luiz Fernando
	*/
	public Empresa getEmpresa(List<Coleta> lista){
		Empresa empresa = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Coleta bean : lista){
				if(bean.getEmpresa() != null){
					if(empresa == null){
						empresa = bean.getEmpresa();
					}else if(!bean.getEmpresa().equals(empresa)){
						empresa = null;
						break;
					}
				}
			}
		}
		return empresa;
	}
	
	public Naturezaoperacao getNaturezaoperacaosaidafiscal(List<Coleta> lista){
		Naturezaoperacao naturezaoperacao = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Coleta bean : lista){
				if (bean.getPedidovenda()!=null
						&& bean.getPedidovenda().getPedidovendatipo()!=null
						&& bean.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal()!=null){
					if(naturezaoperacao == null){
						naturezaoperacao = bean.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal();
					}else if(!bean.getPedidovenda().getPedidovendatipo().getNaturezaoperacaosaidafiscal().equals(naturezaoperacao)){
						naturezaoperacao = null;
						break;
					}
				}
			}
		}
		return naturezaoperacao;
	}
	
	/**
	 * M�todo que retorna o cliente de acordo com as coletas. Caso exista mais de um cliente, ir� retornar nulo.
	 *
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/10/2015
	 */
	public Cliente getCliente(List<Coleta> lista){
		Cliente cliente = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Coleta bean : lista){
				if(bean.getCliente() != null){
					if(cliente == null){
						cliente = bean.getCliente();
					}else if(!bean.getCliente().equals(cliente)){
						cliente = null;
						break;
					}
				}
			}
		}
		return cliente;
	}
	
	/**
	 * Monta o link para o hist�rico.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/10/2015
	 */
	public String makeLinkHistoricoColeta(Coleta coleta) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<a href=\"javascript:visualizarColeta(")
			.append(coleta.getCdcoleta())
			.append(");\">")
			.append(coleta.getCdcoleta())
			.append("</a>");
		
		return sb.toString();
	}
	
	/**
	 * Monta o link para o hist�rico.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/10/2015
	 */
	public String makeLinkHistoricoColeta(String whereInColeta) {
		if(whereInColeta == null || whereInColeta.trim().equals("")) return "";
		
		StringBuilder sb = new StringBuilder();
		
		String[] idsColeta = whereInColeta.split(",");
		for (String idStr : idsColeta) {
			sb.append(this.makeLinkHistoricoColeta(new Coleta(Integer.parseInt(idStr)))).append(", ");
		}
		
		return sb.length() > 0 ? sb.substring(0, sb.length() - 2) : sb.toString();
	}
	
	/**
	* M�todo que salva e estorna a devolu��o de ordem de produ��o
	*
	* @param request
	* @param bean
	* @return
	* @since 16/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView salvarEstornarDevolucao(WebRequestContext request, Coleta bean){
		boolean estornarProducaoordem = false;
		List<ColetaMaterial> listaColetaMaterial = bean.getListaColetaMaterial();
		List<Producaoordem> listaProducaoordem = StringUtils.isNotBlank(bean.getWhereInProducaoordem()) ? producaoordemService.findForDevolverColeta(bean.getWhereInProducaoordem()) : null;
		HashMap<Coleta, StringBuilder> mapColetaObservacaoHistorico = new HashMap<Coleta, StringBuilder>();
		HashMap<Producaoordem, StringBuilder> mapProducaoordemObservacaoHistorico = new HashMap<Producaoordem, StringBuilder>();
		Date dataAtual = new Date(System.currentTimeMillis());
		
		if(listaColetaMaterial != null && !listaColetaMaterial.isEmpty()){
			for(ColetaMaterial cm : listaColetaMaterial){
				if(cm.getQuantidadeestorno() != null && cm.getQuantidadeestorno() > 0){
					estornarProducaoordem = true;
					coletaMaterialService.updateQtdeDevolvidaAposEstornoDevolucao(cm, cm.getQuantidadeestorno());
					
					Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
					movimentacaoestoque.setEmpresa(cm.getColeta().getEmpresa());
					movimentacaoestoque.setLocalarmazenagem(cm.getLocalarmazenagem());
					movimentacaoestoque.setMaterial(cm.getMaterial());
					movimentacaoestoque.setQtde(cm.getQuantidadeestorno());
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
					movimentacaoestoque.setDtmovimentacao(dataAtual);
					
					List<Materialclasse> listaClasse = materialService.findClasses(cm.getMaterial());
					if(SinedUtil.isListNotEmpty(listaClasse)){
						movimentacaoestoque.setMaterialclasse(listaClasse.get(0));
					}
					
					Producaoordem producaoordem = coletaMaterialService.getProducaoordemByColetaMaterial(cm, listaProducaoordem);
					Producaoordemmaterial producaoordemmaterial = coletaMaterialService.getProducaoordemmaterialByColetaMaterial(cm, listaProducaoordem);
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
					
					if(producaoordem != null){
						String obsLink = SinedUtil.makeLinkHistorico(movimentacaoestoque.getCdmovimentacaoestoque().toString(), movimentacaoestoque.getCdmovimentacaoestoque().toString(), "visualizarMovimentacaoestoque");
						if(mapProducaoordemObservacaoHistorico.get(producaoordem) == null){
							mapProducaoordemObservacaoHistorico.put(producaoordem, new StringBuilder(obsLink));
						}else {
							mapProducaoordemObservacaoHistorico.get(producaoordem).append(",").append(obsLink);
						}
					}
					if(producaoordemmaterial != null && producaoordemmaterial.getQtdeperdadescarte() != null && producaoordemmaterial.getQtdeperdadescarte() > 0){
						producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, -cm.getQuantidadeestorno());
					}
					
					if(mapColetaObservacaoHistorico.get(cm.getColeta()) == null){
						mapColetaObservacaoHistorico.put(cm.getColeta(), new StringBuilder(coletaMaterialService.getDescricaoEstornoDevolucao(cm, listaProducaoordem)));
					}else {
						mapColetaObservacaoHistorico.get(cm.getColeta()).append("\n").append(coletaMaterialService.getDescricaoEstornoDevolucao(cm, listaProducaoordem));
					}
				}
			}
		}
		
		if(estornarProducaoordem && StringUtils.isNotBlank(bean.getWhereInProducaoordem())){
			producaoordemService.updateSituacao(bean.getWhereInProducaoordem(), Producaoordemsituacao.EM_ESPERA);
			
			Usuario usuario = SinedUtil.getUsuarioLogado();
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			for(Producaoordem producaoordem : mapProducaoordemObservacaoHistorico.keySet()){
				Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
				producaoordemhistorico.setProducaoordem(producaoordem);
				producaoordemhistorico.setCdusuarioaltera(usuario.getCdpessoa());
				producaoordemhistorico.setDtaltera(dtaltera);
				producaoordemhistorico.setObservacao("Devolu��o de Coleta Cancelada. Entrada no Estoque: " + mapProducaoordemObservacaoHistorico.get(producaoordem).toString());
				if(StringUtils.isNotBlank(bean.getMotivoestorno())){
					producaoordemhistorico.setObservacao(producaoordemhistorico.getObservacao() + ". Motivo: " + bean.getMotivoestorno());
				}
				producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
			}
			
			for(Coleta coleta : mapColetaObservacaoHistorico.keySet()){
				Coletahistorico coletahistorico = new Coletahistorico();
				coletahistorico.setColeta(coleta);
				coletahistorico.setAcao(Coletaacao.ESTORNO_DEVOLUCAO);
				coletahistorico.setObservacao("Estorno de devolu��o do(s) material(is):\n" + mapColetaObservacaoHistorico.get(coleta).toString());
				coletahistorico.setCdusuarioaltera(usuario.getCdpessoa());
				coletahistorico.setDtaltera(dtaltera);
				coletahistoricoService.saveOrUpdate(coletahistorico);
			}
			
			List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByProducaoordem(bean.getWhereInProducaoordem());
			if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
				for(Producaoagenda producaoagenda : listaProducaoagenda){
					if(producaoagenda.getProducaoagendasituacao() != null && 
							!Producaoagendasituacao.EM_ANDAMENTO.equals(producaoagenda.getProducaoagendasituacao())){
						producaoagendaService.updateSituacao(producaoagenda, Producaoagendasituacao.EM_ANDAMENTO);
						
						Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
						producaoagendahistorico.setProducaoagenda(producaoagenda);
						producaoagendahistorico.setCdusuarioaltera(usuario.getCdpessoa());
						producaoagendahistorico.setDtaltera(dtaltera);
						producaoagendahistorico.setObservacao("Estorno de devolu��o de coleta na Ordem de Produ��o: " + SinedUtil.makeLinkHistorico(bean.getWhereInProducaoordem(), "visualizarProducaoordem"));
						producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
						
						if(Producaoagendasituacao.CANCELADA.equals(producaoagenda.getProducaoagendasituacao()) && 
								producaoagenda.getPedidovenda() != null && 
								producaoagenda.getPedidovenda().getPedidovendasituacao() != null && 
								Pedidovendasituacao.CANCELADO.equals(producaoagenda.getPedidovenda().getPedidovendasituacao())){
							
							pedidovendaService.updatePrevista(producaoagenda.getPedidovenda().getCdpedidovenda().toString());
							
							Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
							pedidovendahistorico.setPedidovenda(producaoagenda.getPedidovenda());
							pedidovendahistorico.setObservacao("Estorno de Devolu��o de Coleta");
							pedidovendahistorico.setAcao("Estono");
							pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
						}
					}
				}
			}
		}
		
		if(estornarProducaoordem){
			request.addMessage("Estorno de devolu��o realizado com sucesso.");
		}else {
			request.addMessage("Nenhum item estornado");
		}
		
		return new ModelAndView("redirect:/producao/crud/Producaoordem");
	}
	
	/**
	* M�todo que retorna as coletas para registrar devolu��o
	*
	* @param whereInColeta
	* @param listaCdPedidovendamaterial
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Coleta> findForRegistrarDevolucao(String whereInColeta, List<Integer> listaCdPedidovendamaterial) {
		List<Coleta> listaColeta = loadForGerarNotafiscalproduto(whereInColeta);
		if(listaColeta == null || listaColeta.isEmpty()){
			throw new SinedException("N�o existe(m) coleta(s) v�lida(s) gerar devolu��o(�es).");
		}
		
		StringBuilder whereInPedidovendamaterial = new StringBuilder();
		for (Coleta coleta : listaColeta) {
			if(coleta.getListaColetaMaterial() != null && coleta.getListaColetaMaterial().size() > 0){
				for (ColetaMaterial coletaMaterial : coleta.getListaColetaMaterial()) {
					if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
						for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
							if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
								if(whereInPedidovendamaterial.length() > 0) whereInPedidovendamaterial.append(",");
								whereInPedidovendamaterial.append(itemServico.getPedidovendamaterial().getCdpedidovendamaterial());
							}
						}
					}
					
//					if(coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//						if(whereInPedidovendamaterial.length() > 0) whereInPedidovendamaterial.append(",");
//						whereInPedidovendamaterial.append(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial());
//					}
				}
			}
		}
		
		if(whereInPedidovendamaterial != null && whereInPedidovendamaterial.length() > 0){
			List<Integer> cdpedidovendamaterial_naopermitido = new ArrayList<Integer>();
			List<Producaoordemmaterial> listaProducaoordemmaterial = producaoordemmaterialService.findByPedidovendamaterial(whereInPedidovendamaterial.toString());
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				if(producaoordemmaterial.getProducaoordem() != null && 
						producaoordemmaterial.getProducaoordem().getProducaoordemsituacao() != null &&
						!producaoordemmaterial.getProducaoordem().getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA) &&
						!producaoordemmaterial.getProducaoordem().getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA) &&
						producaoordemmaterial.getQtdeproduzido() != null && producaoordemmaterial.getQtdeproduzido() > 0){
					Integer cdpedidovendamaterial = producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial();
					cdpedidovendamaterial_naopermitido.add(cdpedidovendamaterial);
				}
			}
			
			boolean existeItemParaDevolver = false;
			for (Coleta coleta : listaColeta) {
				List<ColetaMaterial> listaColetaMaterialNova = new ArrayList<ColetaMaterial>();
				List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
				if(listaColetaMaterial != null && listaColetaMaterial.size() > 0){
					for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
						coletaMaterial.setEmpresa(coleta.getEmpresa());
						if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
								if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
									if(!cdpedidovendamaterial_naopermitido.contains(itemServico.getPedidovendamaterial().getCdpedidovendamaterial())){
										if(listaCdPedidovendamaterial != null && listaCdPedidovendamaterial.size() > 0){
											if(listaCdPedidovendamaterial.contains(itemServico.getPedidovendamaterial().getCdpedidovendamaterial())){
												listaColetaMaterialNova.add(coletaMaterial);
												break;
											}
										} else {
											listaColetaMaterialNova.add(coletaMaterial);
											break;
										}
									}
								} else {
									listaColetaMaterialNova.add(coletaMaterial);
									break;
								}
							}
						}
						
//						if(coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//							if(!cdpedidovendamaterial_naopermitido.contains(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial())){
//								if(listaCdPedidovendamaterial != null && listaCdPedidovendamaterial.size() > 0){
//									if(listaCdPedidovendamaterial.contains(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial())){
//										listaColetaMaterialNova.add(coletaMaterial);
//									}
//								} else {
//									listaColetaMaterialNova.add(coletaMaterial);
//								}
//							}
//						} else listaColetaMaterialNova.add(coletaMaterial);
					}
				}
				if(SinedUtil.isListNotEmpty(listaColetaMaterialNova)){
					existeItemParaDevolver = true;
				}
				
				coleta.setListaColetaMaterial(listaColetaMaterialNova);
			}
			if(!existeItemParaDevolver){
				throw new SinedException("N�o h� material dispon�vel para devolu��o, todas as ordens de produ��o j� est�o conclu�das.");
			}
		}
		
		return listaColeta;
	}
	
	/**
	* M�todo que salva as devolu��es das coletas
	*
	* @param request
	* @param whereInColeta
	* @param listaColetaMaterial
	* @param motivodevolucao
	* @since 22/12/2015
	* @author Luiz Fernando
	 * @param isNaoRegistrarMovimentacao 
	*/
	public void salvarRegistrarDevolucao(WebRequestContext request, String whereInColeta, List<ProducaoordemdevolucaoitemBean> listaColetaMaterial, String motivodevolucao, boolean interromperProducao, Boolean isNaoRegistrarMovimentacao) {
		if(listaColetaMaterial != null && !listaColetaMaterial.isEmpty()){
			for(ProducaoordemdevolucaoitemBean item : listaColetaMaterial){
				if(item.getColetaMaterial() != null){
					ColetaMaterial cm = item.getColetaMaterial();
					if((item.getQuantidadedevolvida() != null && item.getQuantidadedevolvida() > 0) || interromperProducao){
						if(item.getQuantidadedevolvida() != null && item.getQuantidadedevolvida() > 0){
							if(item.getMaterial().getServico() == null || !item.getMaterial().getServico()){
								Materialclasse materialclasse = Materialclasse.PRODUTO;
								if(item.getMaterial().getProduto() == null || !item.getMaterial().getProduto()){
									if(item.getMaterial().getEpi() != null && item.getMaterial().getEpi()){
										materialclasse = Materialclasse.EPI;
									} else materialclasse = null;
								}
								
								if(materialclasse != null){
									Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
									movimentacaoestoque.setMaterial(item.getMaterial());
									movimentacaoestoque.setMaterialclasse(materialclasse);
									movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
									movimentacaoestoque.setQtde(item.getQuantidadedevolvida());
									movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
									movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
									movimentacaoestoque.setEmpresa(item.getEmpresa());
									movimentacaoestoque.setValor(item.getValorunitario());
									
									Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
									if(cm.getColeta() != null && cm.getColeta().getPedidovenda() != null && cm.getColeta().getPedidovenda().getCdpedidovenda() != null){
										movimentacaoestoqueorigem.setPedidovenda(cm.getColeta().getPedidovenda());
									}else {
										movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
									}
									movimentacaoestoqueorigem.setColeta(cm.getColeta());
									movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
									if(!Boolean.TRUE.equals(isNaoRegistrarMovimentacao)){
										movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
									}
								}
							}
							
							coletaMaterialService.adicionaQtdeDevolvida(cm, item.getQuantidadedevolvida());
//							coletaMaterialService.adicionaMotivodevolucao(cm, item.getMotivodevolucao());
//							coletamaterialmotivodevolucaoService.save(cm, item.getMotivodevolucao(), true);
							
							coletamaterialmotivodevolucaoService.delete(cm);
							for (Motivodevolucao motivodevolucaoTela: item.getListaMotivodevolucao()){
								coletamaterialmotivodevolucaoService.save(cm, motivodevolucaoTela, false);								
							}
							
							StringBuilder obs = new StringBuilder();
							obs.append("Devolu��o do material: " + item.getMaterial().getNome());
							if(item.getObservacao() != null && !item.getObservacao().trim().equals("")){
								obs.append(" (").append(item.getObservacao()).append(")");
							}
							if(item.getPedidovendamaterial() != null && item.getPedidovendamaterial().getCdpedidovendamaterial() != null){
								obs.append(" (").append(item.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
							}
							
							Coletahistorico coletahistorico = new Coletahistorico();
							coletahistorico.setAcao(Coletaacao.DEVOLVIDO);
							coletahistorico.setColeta(cm.getColeta());
							coletahistorico.setObservacao(obs.toString());
							coletahistoricoService.saveOrUpdate(coletahistorico);
						}
						
						if(item.getPedidovendamaterial() != null && item.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							StringBuilder whereInProducaoordemmaterial = new StringBuilder();
							List<Producaoordemmaterial> listaProducaoordemmaterial = producaoordemmaterialService.findByPedidovendamaterial(item.getPedidovendamaterial().getCdpedidovendamaterial().toString());
							for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
								if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
									whereInProducaoordemmaterial.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
								}
								if(item.getQuantidadedevolvida() != null && item.getQuantidadedevolvida() > 0){
									producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, item.getQuantidadedevolvida());
								}
								if(interromperProducao){
									Producaoordemmaterial pom = producaoordemmaterialService.load(producaoordemmaterial, "producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeperdadescarte, producaoordemmaterial.qtdeproduzido");
									if(pom != null && pom.getQtde() != null){
										Double qtdePerdaDescarteRestante = pom.getQtde();
										if(pom.getQtdeperdadescarte() != null){
											qtdePerdaDescarteRestante -= pom.getQtdeperdadescarte();
										}
										if(pom.getQtdeproduzido() != null){
											qtdePerdaDescarteRestante -= pom.getQtdeproduzido();
										}
										if(qtdePerdaDescarteRestante > 0){
											producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, qtdePerdaDescarteRestante);
										}
									}
								}
							}
							if(!whereInProducaoordemmaterial.toString().equals("")){
								producaoordemService.verificaCancelamentoProducaoordemQtdeDevolvida(request, whereInProducaoordemmaterial.substring(0, whereInProducaoordemmaterial.length()-1), interromperProducao);
							}
						}
					}
				}
			}
		}
		
		if(StringUtils.isNotBlank(whereInColeta)){
			updateSituacaoPedidovendaProducaoordemByColeta(whereInColeta);
		}
		if(StringUtils.isNotBlank(motivodevolucao)){
			updateMotivoDevolucao(whereInColeta, motivodevolucao);
		}
	}
	
	/**
	* M�todo que cria o bean de coleta a partir do bean de devolu��o
	*
	* @param bean
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public Coleta criarDevolverColetaBean(ProducaoordemdevolucaoBean bean) {
		Coleta coleta = new Coleta();
		coleta.setEmpresa(bean.getEmpresacoleta());
		coleta.setCliente(bean.getClientecoleta());
		
		List<ColetaMaterial> listaColetaMaterial = new ArrayList<ColetaMaterial>();
		for(ProducaoordemdevolucaoitemBean item : bean.getListaItens()){
			if(item.getColetaMaterial() != null &&
					item.getQuantidadedevolvida() != null && 
					item.getQuantidadedevolvida() > 0){
				
				ColetaMaterial cm = item.getColetaMaterial();
				cm.setMaterial(item.getMaterial());
				cm.setLocalarmazenagem(item.getLocalarmazenagem());
				cm.setMotivodevolucao(item.getMotivodevolucao());
				cm.setValorunitario(item.getValorunitario());
				cm.setQuantidadedevolvida(item.getQuantidadedevolvida());
				cm.setObservacao(item.getObservacao());
				cm.setPedidovendamaterial(item.getPedidovendamaterial());
				
				listaColetaMaterial.add(cm);
			}
		}
		
		coleta.setListaColetaMaterial(listaColetaMaterial);
		return coleta;
	}
	
	/**
	* M�todo que cria uma conta a pagar referente a compra de coleta
	*
	* @param request
	* @param coleta
	* @return
	* @since 17/11/2016
	* @author Luiz Fernando
	*/
	public Documento inserirContapagar(WebRequestContext request, Coleta coleta, String whereInColeta) {
		StringBuilder erros = new StringBuilder();
		Documento doc = new Documento();
		doc.setEmpresa(coleta.getEmpresa());
		doc.setDocumentoacao(coleta.getContapaga() != null && coleta.getContapaga() ? Documentoacao.AUTORIZADA : Documentoacao.DEFINITIVA);
		doc.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		doc.setTipopagamento(Tipopagamento.CLIENTE);
		doc.setReferencia(Referencia.MES_ANO_CORRENTE);
		doc.setPessoa(coleta.getCliente());
		doc.setDocumentotipo(coleta.getDocumentotipo());
		doc.setDtemissao(new Date(System.currentTimeMillis()));
		doc.setDtvencimento(coleta.getDtvencimento());
		doc.setValor(coleta.getValor());
		doc.setValoratual(coleta.getValor());
		doc.setDescricao("Compra do cliente. Coleta " + (StringUtils.isNotBlank(whereInColeta) ? whereInColeta : coleta.getCdcoleta()));
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
		Rateioitem rateioitem = new Rateioitem();
		rateioitem.setRateio(rateio);
		rateioitem.setContagerencial(coleta.getContagerencial());
		rateioitem.setCentrocusto(coleta.getCentrocusto());
		rateioitem.setProjeto(coleta.getProjeto());
		rateioitem.setPercentual(100.);
		rateioitem.setValor(coleta.getValor());
		rateio.getListaRateioitem().add(rateioitem);
		doc.setRateio(rateio);
		
		documentoService.saveOrUpdate(doc);
		
		Documentohistorico dh = documentohistoricoService.geraHistoricoDocumento(doc);
		StringBuilder obs = new StringBuilder();
		if(StringUtils.isNotBlank(coleta.getWhereIn())){
			for(String id : coleta.getWhereIn().split(",")){
				obs.append("<a href=\"javascript:visualizarColeta(" + id + ");\">"+id+"</a>, ");
			}
		}
		dh.setObservacao("Criado a partir da compra de coleta. " + (obs.length() > 0 ? obs.substring(0, obs.length() -2) : ""));
		documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
		
		if(StringUtils.isNotBlank(coleta.getWhereIn())){
			for(String id : coleta.getWhereIn().split(",")){
				Documentoorigem documentoorigem = new Documentoorigem();
				documentoorigem.setDocumento(doc);
				documentoorigem.setColeta(new Coleta(Integer.parseInt(id)));
				documentoorigemService.saveOrUpdate(documentoorigem);
			}
		}
		
		if(coleta.getContapaga() != null && coleta.getContapaga()){
			BaixarContaBean bcb = new BaixarContaBean();
			bcb.setListaDocumento(new ListSet<Documento>(Documento.class));
			bcb.getListaDocumento().add(doc);
			bcb.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
			bcb.setVinculo(coleta.getVinculo());
			bcb.setFormapagamento(coleta.getFormapagamento());
			bcb.setRateio(doc.getRateio());
			bcb.setDtpagamento(coleta.getDtpagamento());
			
			try{	
				contapagarService.transactionSaveBaixarConta(request, bcb);
			}catch (Exception e) {
				erros.append(doc.getCddocumento()+",");
			}
		}
		
		if(erros.length() > 0){
			request.addError("A conta n�o foi baixada automaticamente: " + erros.deleteCharAt(erros.length()-1).toString());
		}
		
		return doc;
	}
	
	/**
	* M�todo que cria vale compra referente a compra de coleta
	*
	* @param coleta
	* @return
	* @since 17/11/2016
	* @author Luiz Fernando
	*/
	public List<Valecompra> inserirValecompra(Coleta coleta) {
		List<Valecompra> lista = new ArrayList<Valecompra>();
		if(coleta != null && SinedUtil.isListNotEmpty(coleta.getListaColetaMaterial())){
			HashMap<Coleta, Money> mapColetaValor = new HashMap<Coleta, Money>();
			for (ColetaMaterial coletaMaterial : coleta.getListaColetaMaterial()) {
				if(coletaMaterial.getQuantidaderestante() != null && coletaMaterial.getQuantidaderestante() > 0 && coletaMaterial.getColeta() != null &&
						coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().getValorcusto() != null && 
						coletaMaterial.getMaterial().getValorcusto() > 0){
					Money valor = mapColetaValor.get(coletaMaterial.getColeta());
					Money valorTotalItem = new Money(coletaMaterial.getMaterial().getValorcusto() * coletaMaterial.getQuantidaderestante());
					if(valor == null){
						valor = valorTotalItem;
					}else {
						valor = valor.add(valorTotalItem);
					}
					mapColetaValor.put(coletaMaterial.getColeta(), valor);
				}
			}
			
			if(mapColetaValor.size() > 0){
				for(Coleta coletaCompra : mapColetaValor.keySet()){
					Valecompra valecompra = new Valecompra();
					valecompra.setData(SinedDateUtils.currentDate());
					valecompra.setCliente(coletaCompra.getCliente());
					valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
					valecompra.setValor(mapColetaValor.get(coletaCompra));
					valecompra.setIdentificacao("Referente a compra de coleta " + coletaCompra.getCdcoleta());
					valecompraService.saveOrUpdate(valecompra);
					
					Valecompraorigem valecompraorigem = new Valecompraorigem();
					valecompraorigem.setValecompra(valecompra);
					valecompraorigem.setColeta(coletaCompra);
					valecompraorigemService.saveOrUpdate(valecompraorigem);
					
					lista.add(valecompra);
				}
			}
		}
		
		return lista;
	}
	
	public String getDescricaoOrigem(NotafiscalprodutoColeta bean) {
		String descricao = "";
		if(bean != null && bean.getNotafiscalproduto() != null){
			descricao = Tipooperacaonota.ENTRADA.equals(bean.getNotafiscalproduto().getTipooperacaonota()) ? "ENTRADA FISCAL" : "NOTA FISCAL";
			if(bean.getDevolucao() != null && bean.getDevolucao()){	
				descricao += " DEVOLU��O";
			}
		}
		return descricao;
	}
	
	public boolean isColetaPossuiNotaTodosItensSemSaldo(String whereIn, Tipooperacaonota tipooperacaonota){
		return coletaDAO.isColetaPossuiNotaTodosItensSemSaldo(whereIn, tipooperacaonota);
	}
	
	public Double getQuantidadeItemNota(Integer cdcoleta, Integer cdmaterial, Integer cdpedidovendamaterial, Tipooperacaonota tipooperacaonota){
		return coletaDAO.getQuantidadeItemNota(cdcoleta, cdmaterial, cdpedidovendamaterial, tipooperacaonota);
	}
	
	public void criarAvisoColetaDevolucaoNaoConfirmada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Coleta> coletaList = coletaDAO.findAllWithColetaMaterial(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Coleta c : coletaList) {
			if (c.getDevolucaoConfirmada()) {
				Aviso aviso = new Aviso("Coleta com devolu��o n�o confirmada", "C�digo da coleta: " + c.getCdcoleta(), 
						m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), c.getCdcoleta(), c.getEmpresa() != null ? c.getEmpresa() : empresa, 
						SinedDateUtils.currentDate(), m, Boolean.FALSE);
				Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, c.getCdcoleta());
				
				if (lastAvisoDate != null) {
					avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, c.getCdcoleta(), lastAvisoDate);
					
					List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
					
					if (SinedUtil.isListNotEmpty(listaUsuario)) {
						avisoService.salvarAvisos(aviso, listaUsuario, false);
					}
				} else {
					avisoList.add(aviso);
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public boolean validaObrigatoriedadePneu(Coleta bean, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(SinedUtil.isRecapagem() && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA) &&
				bean != null && SinedUtil.isListNotEmpty(bean.getListaColetaMaterial())){
			for(ColetaMaterial item : bean.getListaColetaMaterial()){
				if(item.getPneu() != null){
					if(!pneuService.validaObrigatoriedadePneu(item.getMaterial(), item.getPneu(), request, errors)){
						valido = false;
					}
				}
			}
		}
		return valido;
	}
	public List<Coleta> findByWhereIn(String whereIn) throws Exception {
		return coletaDAO.findByWhereIn(whereIn);
	}
	public void updateSituacaoColeta(Coleta coleta,SituacaoVinculoExpedicao situaColeta,Boolean isSalvarHistorio,String obs,Coletaacao acao) {
		coletaDAO.updateSituacaoColeta(coleta,situaColeta);
		if(Boolean.TRUE.equals(isSalvarHistorio)){
			Coletahistorico historico = new Coletahistorico();
			historico.setAcao(acao);
			historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			historico.setColeta(coleta);
			historico.setDtaltera(SinedDateUtils.currentTimestamp());
			historico.setObservacao(obs);
			coletahistoricoService.saveOrUpdate(historico);
		}
	}
	public Coleta findById(Integer id) {
		return coletaDAO.findById(id);
	}
	
	public void processarListagem(ListagemResult<Coleta> listagemResult, ColetaFiltro filtro, boolean isListagem) {
		List<Coleta> lista = listagemResult.list();
		
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdcoleta", ",");
			
			lista.removeAll(lista);
			lista.addAll(loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
			
			if(isListagem) {
				for(Coleta coleta : lista){
					if(SinedUtil.isListNotEmpty(coleta.getListaEntregadocumentoColeta())){
						for(EntregadocumentoColeta entregadocumentoColeta : coleta.getListaEntregadocumentoColeta()){
							if(entregadocumentoColeta.getEntregadocumento() != null){
								entregadocumentoColeta.getEntregadocumento().setExisteNotaDevolucao(entradafiscalService.existeNotaDevolucao(entregadocumentoColeta.getEntregadocumento()));
							}
						}
					}
					if(coleta.getCdcoleta() != null && coleta.getExisteNotaSaidaAssociada() && isColetaPossuiNotaTodosItensSemSaldo(coleta.getCdcoleta().toString(), Tipooperacaonota.SAIDA)){
						coleta.setNotaSaidaCompleto(Boolean.TRUE);
					}
//				
//				coleta.getMateriaisListagem().replace("<BR/>", "\n");
//				coleta.getQtdeListagem().replace("<BR/>", "\n");
//				coleta.getQtdeDevolvidaListagem().replace("<BR/>", "\n");
				}
			}
		}
	}
	
	public Resource gerarListagemCSV(ColetaFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Coleta> listagemResult = findForListagem(filtro);
		processarListagem(listagemResult, filtro, false);
		List<Coleta> lista = listagemResult.list();
		
		String cabecalho = "C�digo;Empresa;Coletor;Tipo;Cliente/Fornecedor;Material(is);Pedido de venda;Identificador externo;Qtde.;Qtde. Devolvida;\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Coleta bean : lista) {
			String materiaisListagem  = bean.getMateriaisListagem() != null ? bean.getMateriaisListagem().replace("<BR/>", ", ") : "";
			String qtdeListagem = bean.getQtdeListagem() != null ? bean.getQtdeListagem().replace("<BR/>", ", ") : "";
			
			if(!materiaisListagem.equals("")) 
				materiaisListagem = materiaisListagem.substring(0, materiaisListagem.length() - 2);
			if(!qtdeListagem.equals("")) 
				qtdeListagem = qtdeListagem.substring(0, qtdeListagem.length() - 2);
			
			csv.append(bean.getCdcoleta() + ";")
				.append((bean.getEmpresa() != null ? bean.getEmpresa().getNome() : "") + ";")
				.append((bean.getColaborador() != null ? bean.getColaborador().getNome() : "") + ";")
				.append((bean.getTipo() != null ? bean.getTipo() : "") + ";")
				.append((bean.getClienteFornecedorListagem() != null ? bean.getClienteFornecedorListagem() : "") + ";")
				.append(materiaisListagem + ";")
				.append((bean.getIdentificadorpedidovenda() != null ? bean.getIdentificadorpedidovenda() : "") + ";")
				.append((bean.getPedidovenda() != null ? bean.getPedidovenda().getIdentificacaoexterna() : "") + ";")
				.append(qtdeListagem + ";");
			
			if(bean.getQtdeDevolvidaListagem() != null && bean.getQtdeDevolvidaListagem().equals("")) {
				String qtdeDevolvida = bean.getQtdeDevolvidaListagem();
				
				if(StringUtils.isNotBlank(qtdeDevolvida) && qtdeDevolvida.indexOf("<a") != -1) {
					csv.append(qtdeDevolvida.substring(0, qtdeDevolvida.indexOf("<a")) + ";");
				} else {
					csv.append(";");
				}
			} else {
				csv.append("0;");
			}
			
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "coleta.csv", csv.toString().getBytes());
		return resource;
	}

	@Override
	public void saveOrUpdate(final Coleta bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				saveOrUpdateNoUseTransaction(bean);	
				
				if(SinedUtil.isListNotEmpty(bean.getListaColetaMaterial())){
					for(ColetaMaterial coletaMaterial : bean.getListaColetaMaterial()){	
						coletaMaterial.setColeta(bean);
						coletaMaterialService.saveOrUpdateNoUseTransaction(coletaMaterial);
					}
				}
				return null;
			}
		});
	}
}