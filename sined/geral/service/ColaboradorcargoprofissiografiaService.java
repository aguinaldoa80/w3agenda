package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargoprofissiografia;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorcargoprofissiografiaService extends GenericService<Colaboradorcargoprofissiografia> {	
	
	/* singleton */
	private static ColaboradorcargoprofissiografiaService instance;
	public static ColaboradorcargoprofissiografiaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoprofissiografiaService.class);
		}
		return instance;
	}
	
}
