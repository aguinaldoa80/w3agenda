package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.Documentodigitaldestinatario;
import br.com.linkcom.sined.geral.dao.DocumentodigitaldestinatarioDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedVerificationCode;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentodigitaldestinatarioService extends GenericService<Documentodigitaldestinatario>{
	
	private DocumentodigitaldestinatarioDAO documentodigitaldestinatarioDAO;
	private DocumentodigitalusuarioService documentodigitalusuarioService;
	
	public void setDocumentodigitalusuarioService(
			DocumentodigitalusuarioService documentodigitalusuarioService) {
		this.documentodigitalusuarioService = documentodigitalusuarioService;
	}
	public void setDocumentodigitaldestinatarioDAO(
			DocumentodigitaldestinatarioDAO documentodigitaldestinatarioDAO) {
		this.documentodigitaldestinatarioDAO = documentodigitaldestinatarioDAO;
	}

	public String generateAndSaveToken(Documentodigitaldestinatario documentodigitaldestinatario) {
		String token = new SinedVerificationCode().toString();
		while(this.haveToken(token)){
			token = new SinedVerificationCode().toString();
		}
		
		this.updateToken(documentodigitaldestinatario, token);
		return token;
	}

	private void updateToken(Documentodigitaldestinatario documentodigitaldestinatario, String token) {
		documentodigitaldestinatarioDAO.updateToken(documentodigitaldestinatario, token);
	}

	private boolean haveToken(String token) {
		return documentodigitaldestinatarioDAO.haveToken(token);
	}

	public void updateDtenvio(Documentodigitaldestinatario documentodigitaldestinatario, Timestamp dtenvio) {
		documentodigitaldestinatarioDAO.updateDtenvio(documentodigitaldestinatario, dtenvio);
	}

	public Documentodigitaldestinatario loadByToken(String token) {
		return documentodigitaldestinatarioDAO.loadByToken(token);
	}

	public void doAceite(Documentodigitaldestinatario documentodigitaldestinatario) {
		HttpServletRequest request = NeoWeb.getRequestContext().getServletRequest();
		String ip = request.getHeader("X-FORWARDED-FOR");
		if (ip == null) {
			ip = request.getRemoteAddr();
		}
		
		String useragent = request.getHeader("User-Agent");
		
		this.updateDtaceite(documentodigitaldestinatario, SinedDateUtils.currentTimestamp());
		this.updateIpaceite(documentodigitaldestinatario, ip);
		this.updateUseragentaceite(documentodigitaldestinatario, useragent);
		documentodigitalusuarioService.saveOrUpdate(documentodigitaldestinatario.getDocumentodigitalusuario());
	}

	private void updateUseragentaceite(Documentodigitaldestinatario documentodigitaldestinatario, String useragent) {
		documentodigitaldestinatarioDAO.updateUseragentaceite(documentodigitaldestinatario, useragent);
	}

	private void updateIpaceite(Documentodigitaldestinatario documentodigitaldestinatario, String ip) {
		documentodigitaldestinatarioDAO.updateIpaceite(documentodigitaldestinatario, ip);
	}

	private void updateDtaceite(Documentodigitaldestinatario documentodigitaldestinatario, Timestamp dtaceite) {
		documentodigitaldestinatarioDAO.updateDtaceite(documentodigitaldestinatario, dtaceite);
	}
	
	public List<Documentodigitaldestinatario> findByDocumentodigital(Documentodigital documentodigital) {
		return documentodigitaldestinatarioDAO.findByDocumentodigital(documentodigital);
	}
	
	public Documentodigitaldestinatario loadForReenvio(Documentodigitaldestinatario documentodigitaldestinatario) {
		return documentodigitaldestinatarioDAO.loadForReenvio(documentodigitaldestinatario);
	}
	
	public Documentodigitaldestinatario loadForEnvioEmailObservadores(Documentodigitaldestinatario documentodigitaldestinatario) {
		return documentodigitaldestinatarioDAO.loadForEnvioEmailObservadores(documentodigitaldestinatario);
	}

}
