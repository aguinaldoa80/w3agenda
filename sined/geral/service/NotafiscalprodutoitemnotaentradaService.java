package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemnotaentrada;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoitemnotaentradaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoitemnotaentradaService extends GenericService<Notafiscalprodutoitemnotaentrada> {
	
	private NotafiscalprodutoitemnotaentradaDAO notafiscalprodutoitemnotaentradaDAO;
	
	public void setNotafiscalprodutoitemnotaentradaDAO(
			NotafiscalprodutoitemnotaentradaDAO notafiscalprodutoitemnotaentradaDAO) {
		this.notafiscalprodutoitemnotaentradaDAO = notafiscalprodutoitemnotaentradaDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInEntregamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/04/2015
	 */
	public List<Notafiscalprodutoitemnotaentrada> findByEntregamaterial(String whereInEntregamaterial) {
		return notafiscalprodutoitemnotaentradaDAO.findByEntregamaterial(whereInEntregamaterial);
	}
	
}
