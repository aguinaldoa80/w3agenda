package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.geral.dao.GrupoquestionarioDAO;
import br.com.linkcom.sined.util.SinedUtil;

public class GrupoquestionarioService extends GenericService<Grupoquestionario>{
	
	QuestionarioquestaoService questionarioquestaoService;
	QuestionariorespostaService questionariorespostaService;
	GrupoquestionarioDAO grupoquestionarioDAO;
	
	public void setQuestionarioquestaoService(QuestionarioquestaoService questionarioquestaoService) {
		this.questionarioquestaoService = questionarioquestaoService;
	}	
	
	public void setQuestionariorespostaService(QuestionariorespostaService questionariorespostaService) {
		this.questionariorespostaService = questionariorespostaService;
	}
	
	public void setGrupoquestionarioDAO(GrupoquestionarioDAO grupoquestionarioDAO) {
		this.grupoquestionarioDAO = grupoquestionarioDAO;
	}
	
	/**
	  * M�todo que busca grupoquestionario atrav�s da descricao
	 *
	 * @see br.com.linkcom.sined.geral.dao.GrupoquestionarioDAO#findGrupoquestionarioByDescricao(Grupoquestionario)
	 * @param grupoquestionario
	 * @author Jo�o Vitor
	 * @since 10/03/2015
	 */
		public Grupoquestionario findGrupoquestionarioByDescricao(Grupoquestionario grupoquestionario){
			return grupoquestionarioDAO.findGrupoquestionarioByDescricao(grupoquestionario);
	}
		
		@Override
		public void saveOrUpdate(final Grupoquestionario bean) {
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					saveOrUpdateNoUseTransaction(bean);
					String whereIn = "";
					if (SinedUtil.isListNotEmpty(bean.getListaquestionarioquestao())) {
						whereIn = SinedUtil.listAndConcatenate(bean.getListaquestionarioquestao(), "cdquestionarioquestao", ",");
						questionarioquestaoService.findForDelete(whereIn, bean);
						for (Questionarioquestao questionarioquestao : bean.getListaquestionarioquestao()) {
							questionarioquestao.setGrupoquestionario(bean);
							questionarioquestaoService.saveOrUpdateNoUseTransaction(questionarioquestao);
						}
					} else {
						questionarioquestaoService.findForDelete(whereIn, bean);
					}	
					return status;
				}			
			});
			
		}
		
		public List<Grupoquestionario> carregaGrupoquestionarioForCalculoMediaAprovacao(String whereIn){
			return grupoquestionarioDAO.carregaGrupoquestionarioForCalculoMediaAprovacao(whereIn);
		}
}
