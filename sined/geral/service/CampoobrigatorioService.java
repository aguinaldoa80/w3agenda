package br.com.linkcom.sined.geral.service;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.NonUniqueResultException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Campoobrigatorio;
import br.com.linkcom.sined.geral.bean.Campoobrigatorioitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.CampoobrigatorioDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.ValidacaoCampoObrigatorio;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CampoobrigatorioService extends GenericService<Campoobrigatorio> {

	private CampoobrigatorioDAO campoobrigatorioDAO;
	
	public void setCampoobrigatorioDAO(CampoobrigatorioDAO campoobrigatorioDAO) {
		this.campoobrigatorioDAO = campoobrigatorioDAO;
	}
	
	/* singleton */
	private static CampoobrigatorioService instance;
	public static CampoobrigatorioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CampoobrigatorioService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param path
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2017
	 */
	@SuppressWarnings("unchecked")
	public List<ValidacaoCampoObrigatorio> getListaCampoobrigatorioitem(String path){
		if (path != null && !path.trim().equals("")){
			Campoobrigatorio campoobrigatorio = this.loadByPath(path);
			return (List<ValidacaoCampoObrigatorio>) (campoobrigatorio != null ? campoobrigatorio.getListaCampoobrigatorioitem() : new ArrayList<ValidacaoCampoObrigatorio>());
		} else {
			return new ArrayList<ValidacaoCampoObrigatorio>();
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param path
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2017
	 */
	public Campoobrigatorio loadByPath(String path) {
		return campoobrigatorioDAO.loadByPath(path);
	}

	public List<ValidacaoCampoObrigatorio> validaListaCampoObrigatorio(List<ValidacaoCampoObrigatorio> listaValidacaoCampoObrigatorioAdicional, String path, Object form, BindException errors){
		List<ValidacaoCampoObrigatorio> listaValidacaoCampoObrigatorioBanco = null;
		
		try {
			listaValidacaoCampoObrigatorioBanco = this.getListaCampoobrigatorioitem(path);
		} catch (NonUniqueResultException e) {
			errors.reject("N�o � poss�vel ter mais de um cadastro de campo para a mesma url.");
		}
		
		if(listaValidacaoCampoObrigatorioAdicional != null && listaValidacaoCampoObrigatorioAdicional.size() > 0){
			listaValidacaoCampoObrigatorioBanco.addAll(listaValidacaoCampoObrigatorioAdicional);
		}
		
		List<ValidacaoCampoObrigatorio> listaValidacaoCampoObrigatorio = new ArrayList<ValidacaoCampoObrigatorio>();
		Class<? extends Object> clazz = form.getClass();
		
		for (ValidacaoCampoObrigatorio validacaoCampoObrigatorio : listaValidacaoCampoObrigatorioBanco) {
			if(validacaoCampoObrigatorio instanceof Campoobrigatorioitem){
				((Campoobrigatorioitem) validacaoCampoObrigatorio).setCampoobrigatorio(null);
			}
			
			String displayNameLista = null;
			String displayNameCampo = null;
			try {
				if(validacaoCampoObrigatorio.getPrefixo() != null && !validacaoCampoObrigatorio.getPrefixo().equals("")){
					Method metodoGetLista = clazz.getMethod("get" + Util.strings.captalize(validacaoCampoObrigatorio.getPrefixo()));
					
					displayNameLista = Util.strings.captalize(validacaoCampoObrigatorio.getPrefixo());
					DisplayName displayNameAnnotation = metodoGetLista.getAnnotation(DisplayName.class);
					if (displayNameAnnotation!=null && displayNameAnnotation.value()!=null){
						displayNameLista = displayNameAnnotation.value();
					}
					
					displayNameCampo = Util.strings.captalize(validacaoCampoObrigatorio.getCampo());
					if(metodoGetLista.getReturnType().equals(List.class) || metodoGetLista.getReturnType().equals(Set.class)){
						ParameterizedType integerListType = (ParameterizedType) metodoGetLista.getGenericReturnType();
						Class<?> clazzIt = (Class<?>) integerListType.getActualTypeArguments()[0];
						Method metodoGetCampo = clazzIt.getMethod("get" + Util.strings.captalize(validacaoCampoObrigatorio.getCampo()));
						displayNameAnnotation = metodoGetCampo.getAnnotation(DisplayName.class);
						if (displayNameAnnotation!=null && displayNameAnnotation.value()!=null){
							displayNameCampo = displayNameAnnotation.value();
						}
					}
				} else {
					Method metodoGetCampo = clazz.getMethod("get" + Util.strings.captalize(validacaoCampoObrigatorio.getCampo()));
					
					displayNameCampo = Util.strings.captalize(validacaoCampoObrigatorio.getCampo());
					DisplayName displayNameAnnotation = metodoGetCampo.getAnnotation(DisplayName.class);
					if (displayNameAnnotation!=null && displayNameAnnotation.value()!=null){
						displayNameCampo = displayNameAnnotation.value();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			validacaoCampoObrigatorio.setDisplayNameLista(displayNameLista);
			validacaoCampoObrigatorio.setDisplayNameCampo(displayNameCampo);
			
			String condicao = validacaoCampoObrigatorio.getCondicao();
			boolean remover = false;
			
			if(StringUtils.isNotBlank(condicao)){
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				if(form != null && form instanceof Pessoa){
					String tipopessoa = ((Pessoa) form)!=null && ((Pessoa) form).getTipopessoa()!=null &&((Pessoa) form).getTipopessoa().name()!=null ?((Pessoa) form).getTipopessoa().name():"";
					Boolean exterior = ((Pessoa) form)!=null ? ((Pessoa) form).getPessoaExterior() : Boolean.FALSE;
					engine.put("tipopessoa", tipopessoa);	
					engine.put("exterior", exterior);
					try {
						Object obj = engine.eval(condicao);	
						if(obj != null && obj.toString().equalsIgnoreCase("false")){
							remover = true;
						}					
					} catch (ScriptException e) {
						if(errors != null)
							errors.reject("001","A express�o informada na configura��o do campo obrigat�rio: '" + validacaoCampoObrigatorio.getCondicao() + "' n�o e v�lida.");
					} catch (SinedException e) {
						if(errors != null)
							errors.reject("001", e.getMessage());
					}
				}	
			}
			
			if(!remover) listaValidacaoCampoObrigatorio.add(validacaoCampoObrigatorio);
		}	
		
		return listaValidacaoCampoObrigatorio;
	}
	
}
