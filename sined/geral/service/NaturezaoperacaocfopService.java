package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.geral.dao.NaturezaoperacaocfopDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NaturezaoperacaocfopService extends GenericService<Naturezaoperacaocfop> {
	
	private NaturezaoperacaocfopDAO naturezaoperacaocfopDAO;
	
	public void setNaturezaoperacaocfopDAO(NaturezaoperacaocfopDAO naturezaoperacaocfopDAO) {
		this.naturezaoperacaocfopDAO = naturezaoperacaocfopDAO;
	}
	

	/**
	 * 
	 * @param naturezaoperacao
	 * @param cfopescopo
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacaocfop> find(Naturezaoperacao naturezaoperacao, Cfopescopo cfopescopo){
		return naturezaoperacaocfopDAO.find(naturezaoperacao, cfopescopo);
	}	
}
