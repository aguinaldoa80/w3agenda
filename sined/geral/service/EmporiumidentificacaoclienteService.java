package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Emporiumidentificacaocliente;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumidentificacaoclienteService extends GenericService<Emporiumidentificacaocliente>{
	
	/* singleton */
	private static EmporiumidentificacaoclienteService instance;
	public static EmporiumidentificacaoclienteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EmporiumidentificacaoclienteService.class);
		}
		return instance;
	}
	

}
