package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.FechamentoContabil;
import br.com.linkcom.sined.geral.bean.FechamentoContabilHistorico;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FechamentoContabilHistoricoService extends GenericService<FechamentoContabilHistorico> {
	public FechamentoContabilHistorico preencherHistorico(FechamentoContabil atual, FechamentoContabil anterior) {
		FechamentoContabilHistorico historico = new FechamentoContabilHistorico(atual, SinedUtil.getUsuarioLogado());
		
		if(atual.getCdFechamentoContabil() == null) {
			historico.setAcao("CRIADO");
		} else {
			historico.setAcao("ALTERADO");
			
			StringBuilder observacaoSb = new StringBuilder();
			if(anterior != null) {
				if((anterior.getHistorico() == null && atual.getHistorico() != null)
						|| (anterior.getHistorico() != null && atual.getHistorico() != null && !anterior.getHistorico().equals(atual.getHistorico()))) {
					observacaoSb.append("O campo Historico foi alterado. ");
				}
				if(anterior.getDtfechamento() != null && atual.getDtfechamento() != null && !anterior.getDtfechamento().equals(atual.getDtfechamento())) {
					observacaoSb.append("O campo Data foi alterado. Data anterior: "+ SinedDateUtils.toString(anterior.getDtfechamento()) +".");
				}
				historico.setObservacao(observacaoSb.toString());
			}
			
		}
		
		return historico;
	}
}
