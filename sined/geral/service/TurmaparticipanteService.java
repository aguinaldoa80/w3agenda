package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Turmaparticipante;
import br.com.linkcom.sined.geral.dao.TurmaparticipanteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TurmaparticipanteService extends GenericService<Turmaparticipante> {

	private TurmaparticipanteDAO turmaparticipanteDAO;
	public void setTurmaparticipanteDAO(TurmaparticipanteDAO turmaparticipanteDAO) {
		this.turmaparticipanteDAO = turmaparticipanteDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaparticipanteDAO#findByTurma(Turma)
	 * @param turma
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Turmaparticipante> findByTurma(Turma turma){
		return turmaparticipanteDAO.findByTurma(turma);
	}
	
	/**
	 * M�todo para salvar uma lista de <code>Turmaparticipante</code>.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaparticipanteDAO#saveOrUpdateNoUseTransaction(Turmaparticipante)
	 * @param lista
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateListaTurmaparticipante(final Collection<Turmaparticipante> lista){
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Turmaparticipante tp : lista) {
					turmaparticipanteDAO.saveOrUpdateNoUseTransaction(tp);
				}
				return null;
			}
		});
	}
	
	/* singleton */
	private static TurmaparticipanteService instance;
	public static TurmaparticipanteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TurmaparticipanteService.class);
		}
		return instance;
	}
	
	
}
