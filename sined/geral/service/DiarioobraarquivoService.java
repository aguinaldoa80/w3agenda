package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Diarioobra;
import br.com.linkcom.sined.geral.bean.Diarioobraarquivo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DiarioobraarquivoService extends GenericService<Diarioobraarquivo>{

protected ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	/* singleton */
	private static DiarioobraarquivoService instance;
	public static DiarioobraarquivoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DiarioobraarquivoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que salva os arquivos inseridos no cadastro do di�rio de obra.
	 * @param diarioobra
	 * @author Danilo Guimar�es
	 */
	public void saveArquivos(Diarioobra diarioobra) {
		if (diarioobra == null) {
			throw new SinedException("O par�metro diarioobra n�o pode ser null.");
		}
		List<Diarioobraarquivo> listaDiarioobraarquivo = diarioobra.getListaDiarioobraarquivo();
		if (listaDiarioobraarquivo != null && listaDiarioobraarquivo.size() > 0) {
			for (Diarioobraarquivo diarioobraarquivo : listaDiarioobraarquivo) {
				diarioobraarquivo.setDtarquivo(diarioobra.getDtdia());
				arquivoDAO.saveFile(diarioobraarquivo, "arquivo");
			}
		}
	}
	
}
