package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Notaromaneio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratomateriallocacaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.geral.bean.enumeration.Romaneiotipo;
import br.com.linkcom.sined.geral.dao.RomaneioDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioClienteBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioEmpresaBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioEnderecoBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioItemBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioMaterialBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioMaterialNumeroSerieBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioPatrimonioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.ResumoMaterialRomaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.ResumoRomaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterialItem;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

public class RomaneioService extends GenericService<Romaneio> {

	private RomaneioDAO romaneioDAO;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialService materialService;
	private LocalarmazenagemService localarmazenagemService;
	private ProjetoService projetoService;
	private ContratoService contratoService;
	private PatrimonioitemService patrimonioitemService;
	private ClienteService clienteService;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private MovpatrimonioService movpatrimonioService;
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private TarefaService tarefaService;
	private NotaromaneioService notaromaneioService;
	private ContratohistoricoService contratohistoricoService;
	private ContratomateriallocacaoService contratomateriallocacaoService;
	private ContratomaterialService contratomaterialService;
	private EntregaService entregaService;
	private ParametrogeralService parametrogeralService;
	private RequisicaomaterialService requisicaomaterialService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private RateioService rateioService;
	private InventarioService inventarioService;
	
	public void setMovimentacaoEstoqueHistoricoService(MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setLocalarmazenagemempresaService(
			LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setMovpatrimonioorigemService(
			MovpatrimonioorigemService movpatrimonioorigemService) {
		this.movpatrimonioorigemService = movpatrimonioorigemService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setRomaneioDAO(RomaneioDAO romaneioDAO) {
		this.romaneioDAO = romaneioDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setNotaromaneioService(NotaromaneioService notaromaneioService) {
		this.notaromaneioService = notaromaneioService;
	}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	public void setContratomateriallocacaoService(ContratomateriallocacaoService contratomateriallocacaoService) {
		this.contratomateriallocacaoService = contratomateriallocacaoService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	/**
	 * M�todo que gera os relat�rios dos romaneios e atualiza a situa��o dos romaneios selecionados para 'Em tr�nsito'.
	 * 
	 * @see #getRomaneios(String)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#carregaEnderecoEmpresa(Empresa)
	 * @see #doUpdateSituacaoRomaneioAndRomaneioSolicitacao(List, String, Romaneiosituacao)
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public Resource gerarRelatorio(WebRequestContext request) throws Exception {
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		MergeReport mergeReport = new MergeReport("romaneio_"+SinedUtil.datePatternForReport()+".pdf");
		
		List<Romaneio> listaRomaneio = this.findForImprimirRomaneio(whereIn);
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		Endereco enderecoEmpresaPrincipal = enderecoService.carregaEnderecoEmpresa(empresaPrincipal);
		Image logoPrincipal = SinedUtil.getLogo(empresaPrincipal);
		
		Date dataAtual = new Date(System.currentTimeMillis());
		
		for (Romaneio romaneio: listaRomaneio) {
			
			Report report = new Report("/suprimento/romaneio");
			report.addParameter("TITULO", "ROMANEIO " + romaneio.getCdromaneio() + "/" + new SimpleDateFormat("yy").format(dataAtual));
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", dataAtual);
			
			if (romaneio.getEmpresa()==null){
				report.addParameter("LOGO", logoPrincipal);
				report.addParameter("empresa", empresaPrincipal);
				report.addParameter("endereco", enderecoEmpresaPrincipal);
			}else {
				report.addParameter("LOGO", SinedUtil.getLogo(romaneio.getEmpresa()));
				report.addParameter("empresa", romaneio.getEmpresa());
				report.addParameter("endereco", enderecoService.carregaEnderecoEmpresa(romaneio.getEmpresa()));
			}
			
			Contrato contrato = null;
			Contrato contratofechamento = null;
			Localarmazenagem localarmazenagemorigem = romaneio.getLocalarmazenagemorigem();
			Localarmazenagem localarmazenagemdestino = romaneio.getLocalarmazenagemdestino();
			Endereco enderecoOrigem = null;
			Endereco enderecoDestino = null;
			
			if (romaneio.getListaRomaneioorigem()!=null && !romaneio.getListaRomaneioorigem().isEmpty()){
				ListSet<Romaneioorigem> listaRomaneioorigem = new ListSet<Romaneioorigem>(Romaneioorigem.class, romaneio.getListaRomaneioorigem());
				contrato = listaRomaneioorigem.get(0).getContrato();
				contratofechamento = listaRomaneioorigem.get(0).getContratofechamento();
			}
						
			if (Boolean.TRUE.equals(localarmazenagemorigem.getClientelocacao()) && (contrato != null || contratofechamento != null)){
				if(contrato != null){
					enderecoOrigem = contrato.getEnderecoentrega();
					if(enderecoOrigem == null){
						enderecoOrigem = contrato.getEndereco();
					}
				}else if(contratofechamento != null){
					enderecoOrigem = contratofechamento.getEnderecoentrega();
					if(enderecoOrigem == null){
						enderecoOrigem = contratofechamento.getEndereco();
					}
				}
			}else {
				enderecoOrigem = localarmazenagemorigem.getEndereco();
			}
			
			if (Boolean.TRUE.equals(localarmazenagemdestino.getClientelocacao()) && (contrato != null || contratofechamento != null)){
				if(contrato != null){
					enderecoDestino = contrato.getEnderecoentrega();
					if(enderecoDestino == null){
						enderecoDestino = contrato.getEndereco();
					}
				}else if(contratofechamento != null){
					enderecoDestino = contratofechamento.getEnderecoentrega();
					if(enderecoDestino == null){
						enderecoDestino = contratofechamento.getEndereco();
					}
				}
			}else {
				if(Boolean.TRUE.equals(localarmazenagemdestino.getClientelocacao()) && romaneio.getEndereco() != null && romaneio.getCliente() != null){ 
					enderecoDestino = romaneio.getEndereco();
				}else {
					enderecoDestino = localarmazenagemdestino.getEndereco();
				}
			}
			
			Contrato contratoRelatorio = null;
			if(contrato != null && contrato.getCliente() != null){
				contratoRelatorio = contrato;
			}else if (contratofechamento != null && contratofechamento.getCliente() != null){						
				contratoRelatorio = contratofechamento;
			} else{
				contratoRelatorio = new Contrato();
				contratoRelatorio.setCliente(romaneio.getCliente());
				contratoRelatorio.setDescricao("");
			}			
						
			List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
			
			if (listaRomaneioitem!=null){
				for (Romaneioitem romaneioitem: listaRomaneioitem){
					Patrimonioitem patrimonioitem = romaneioitem.getPatrimonioitem();
					String serie = patrimonioitem != null ? romaneioitem.getMaterial().getSeriePatrimonio(patrimonioitem.getMaterialnumeroserie()) : null;
					
					String plaqueta = "-";
					if(patrimonioitem != null && patrimonioitem.getPlaqueta() != null){
						plaqueta = patrimonioitem.getPlaqueta(); 
					}
					
					if (serie==null){
						serie = "-";
					}
					
					String plaquetaSerie = plaqueta + " / " + serie;
					if (plaquetaSerie.equals("- / -")){
						plaquetaSerie = "";
					}					
					
					if(romaneioitem.getPatrimonioitem() != null){
						romaneioitem.setMaterial(romaneioitem.getMaterial().copiaMaterial());
						romaneioitem.getMaterial().setPlaquetaSerie(plaquetaSerie);
					}else {
						romaneioitem.getMaterial().setPlaquetaSerie(plaquetaSerie);
					}
				}
			}
			
			report.addParameter("ROMANEIO", romaneio);
			report.addParameter("CONTRATO", contratoRelatorio);
			report.addParameter("ENDERECO_ORIGEM", enderecoOrigem);
			report.addParameter("ENDERECO_DESTINO", enderecoDestino);
			report.addParameter("DANIFICADO", contratofechamento!=null);
			
			report.addParameter("localarmazenagemorigem", localarmazenagemorigem);
			report.addParameter("localarmazenagemdestino", localarmazenagemdestino);
			report.setDataSource(listaRomaneioitem);
			
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que gera o pdf da listagem de romaneio
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public IReport createRelatorioRomaneio(WebRequestContext request, RomaneioFiltro filtro) throws Exception {
		Report report = new Report("/suprimento/romaneiolistagem");
		Report subreport = new Report("/suprimento/romaneiolistagem_subreport");
		Report subreportresumo = new Report("/suprimento/resumomaterialromaneio_subreport");
		
		if(filtro.getCliente() != null){
			Cliente cliente = clienteService.carregarDadosCliente(filtro.getCliente());
			if(cliente != null){
				Endereco endereco = cliente.getEndereco();
				
				report.addParameter("CLIENTENOME", cliente.getNome());
				String cpfOuCnpj = "";
				if(cliente.getTipopessoa() != null){
					if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) && cliente.getCnpj() != null){
						cpfOuCnpj = cliente.getCnpj().toString();
					} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA) && cliente.getCpf() != null){
						cpfOuCnpj = cliente.getCpf().toString();
					}
				} 
				report.addParameter("CLIENTECPFCNPJ", cpfOuCnpj);
				if(endereco != null){
					String logradouroCompletoComBairroSemMunicipio = endereco.getLogradouroCompletoComBairroSemMunicipio();
					String municipioUf = endereco.getMunicipioUf();
					
					report.addParameter("CLIENTEENDERECO", logradouroCompletoComBairroSemMunicipio != null ? logradouroCompletoComBairroSemMunicipio : "");
					report.addParameter("CLIENTEMUNICIPIOUF", municipioUf != null ?  municipioUf : "");
				}
				report.addParameter("CLIENTEIE", cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "");
			}
			report.addParameter("CLIENTEFILTRO", Boolean.TRUE);
		} else {
			report.addParameter("CLIENTEFILTRO", Boolean.FALSE);
		}

		List<Romaneio> listaRomaneio = this.findForReport(filtro);
		if(listaRomaneio != null && !listaRomaneio.isEmpty()){
			StringBuilder whereInContrato = new StringBuilder();
			
			for(Romaneio romaneio : listaRomaneio){
				Collections.sort(romaneio.getListaRomaneioitem(),new Comparator<Romaneioitem>(){
					public int compare(Romaneioitem o1, Romaneioitem o2) {
						try{
							Integer o1Id = Integer.parseInt(o1.getMaterial().getIdentificacao());
							Integer o2Id = Integer.parseInt(o2.getMaterial().getIdentificacao());
							
							return o1Id.compareTo(o2Id);
						} catch (Exception e) {
							try{
								return o1.getMaterial().getIdentificacao().compareTo(o2.getMaterial().getIdentificacao());
							} catch (Exception e2) {
								return 0;
							}
						}
					}
				});
				
				if(romaneio.getListaRomaneioorigem() != null && !romaneio.getListaRomaneioorigem().isEmpty()){
					for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
						if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCdcontrato() != null){
							romaneio.setExistContratotrans(true);
							if(!whereInContrato.toString().contains(romaneioorigem.getContrato().getCdcontrato().toString())){
								if(!"".equals(whereInContrato.toString())) whereInContrato.append(",");
								whereInContrato.append(romaneioorigem.getContrato().getCdcontrato());
							}
						}else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento().getCdcontrato() != null){
							romaneio.setExistContratotrans(true);
							if(!whereInContrato.toString().contains(romaneioorigem.getContratofechamento().getCdcontrato().toString())){
								if(!"".equals(whereInContrato.toString())) whereInContrato.append(",");
								whereInContrato.append(romaneioorigem.getContratofechamento().getCdcontrato());
							}
						}
					}
				}
			}
			
			ResumoRomaneio resumoRomaneio = this.makeResumoRomaneio(whereInContrato.toString(), listaRomaneio, false, null);
			
			subreportresumo.addParameter("QTDETOTALENVIADO", resumoRomaneio.getQtdetotalenviada());
			subreportresumo.addParameter("QTDETOTALDEVOLVIDO", resumoRomaneio.getQtdetotaldevolvida());
			subreportresumo.addParameter("QTDESALDO", resumoRomaneio.getQtdetotalenviada() - resumoRomaneio.getQtdetotaldevolvida());
			
			report.addParameter("LISTARESUMOMATERIAL", resumoRomaneio.getListaResumoMaterialRomaneio());
		}
		
		String valorAFaturar = request.getParameter("valorAFaturar");
		String totalLocado = request.getParameter("totalLocado");
		String totalDevolvido = request.getParameter("totalDevolvido");
		String totalAcrescimo = request.getParameter("totalAcrescimo");
		String periodoDe = request.getParameter("periodoDe");
		String periodoAte = request.getParameter("periodoAte");
		
		report.addParameter("VALORFATURAR", valorAFaturar != null && !valorAFaturar.trim().equals("") ? valorAFaturar : null);
		report.addParameter("TOTALLOCADO", totalLocado != null && !totalLocado.trim().equals("") ? totalLocado : null);
		report.addParameter("TOTALDEVOLVIDO", totalDevolvido != null && !totalDevolvido.trim().equals("") ? totalDevolvido : null);
		report.addParameter("TOTALACRESCIMO", totalAcrescimo != null && !totalAcrescimo.trim().equals("") ? totalAcrescimo : null);
		
		String periodo = null;
		if(!StringUtils.isEmpty(periodoDe) && !StringUtils.isEmpty(periodoAte)){
			periodo = periodoDe + " at� " + periodoAte;
		} else if(!StringUtils.isEmpty(periodoDe)){
			periodo = "A partir de " + periodoDe;
		} else if(!StringUtils.isEmpty(periodoAte)){
			periodo = "At� " + periodoAte;
		}
		report.addParameter("PERIODO", periodo);
		
		report.addSubReport("SUB_RESUMOMATERIAL", subreportresumo);
		report.addSubReport("ROMANEIOLISTAGEM_SUBREPORT", subreport);	
		report.setDataSource(listaRomaneio);
		
		return report;
	}
	
	/**
	 * Cria o resumo do romaneio para o relat�rio e na gera��o de romaneio no contrato
	 *
	 * @param whereInContrato
	 * @param listaRomaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/10/2013
	 */
	public ResumoRomaneio makeResumoRomaneio(String whereInContrato, List<Romaneio> listaRomaneio, Boolean fromFaturarLocacao, List<Contratomaterial> listaContratomaterialFaturamento) {
		ResumoRomaneio resumoRomaneio = new ResumoRomaneio();
		
		if(whereInContrato != null && !"".equals(whereInContrato)){
			HashMap<Material, Double> mapMaterialRemessa = new HashMap<Material, Double>();
			HashMap<Material, Double> mapMaterialDevolvida = new HashMap<Material, Double>();
			
			Double qtdetotalenviada = 0.0;
			Double qtdetotaldevolvida = 0.0;
			List<ResumoMaterialRomaneio> listaResumoMaterialRomaneio = new ArrayList<ResumoMaterialRomaneio>();
			
			Date dataatual = new Date(System.currentTimeMillis());
			Integer diferencadias = 0;
			
			List<Contrato> listaContrato = contratoService.findForRomaneioreport(whereInContrato); 
			if(listaContrato != null && !listaContrato.isEmpty()){
				
				if(fromFaturarLocacao == null || !fromFaturarLocacao){
					if(listaRomaneio != null){
						for(Romaneio romaneio : listaRomaneio){
							boolean devolucao = false;
							for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
								if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato() != null ){
									devolucao = false;
								} else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento() != null){
									devolucao = true;
								}
							}
							
							if(romaneio.getDtromaneio() != null){
								diferencadias = SinedDateUtils.diferencaDias(dataatual, romaneio.getDtromaneio());
								for(Romaneioitem romaneioitem : romaneio.getListaRomaneioitem()){
									this.addInfoMaterialContrato(romaneioitem, diferencadias, romaneio, listaContrato);
									
									romaneioitem.getMaterial().setValordiatrans(romaneioitem.getValordiatrans());
									if(devolucao){
										qtdetotaldevolvida += romaneioitem.getQtde();
										if(mapMaterialDevolvida.get(romaneioitem.getMaterial()) != null){
											mapMaterialDevolvida.put(romaneioitem.getMaterial(), mapMaterialDevolvida.get(romaneioitem.getMaterial()) + romaneioitem.getQtde());
										} else {
											mapMaterialDevolvida.put(romaneioitem.getMaterial(), romaneioitem.getQtde());
										}
									} else {
										qtdetotalenviada += romaneioitem.getQtde();
										if(mapMaterialRemessa.get(romaneioitem.getMaterial()) != null){
											mapMaterialRemessa.put(romaneioitem.getMaterial(), mapMaterialRemessa.get(romaneioitem.getMaterial()) + romaneioitem.getQtde());
										} else {
											mapMaterialRemessa.put(romaneioitem.getMaterial(), romaneioitem.getQtde());
										}
									}
								}
							}
						}
					}
				}else {
					for(Contrato contrato : listaContrato){
						if(contrato != null && contrato.getListaContratomaterial() != null && 
								!contrato.getListaContratomaterial().isEmpty()){
							boolean faturamentoporitemdevolvido = contrato.getContratotipo() != null && Boolean.TRUE.equals(contrato.getContratotipo().getFaturamentoporitensdevolvidos());
							
							for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
								contratomaterial.getServico().setPatrimonioitem(contratomaterial.getPatrimonioitem());
								
								if(getConsiderarQtdeTotalItem(contratomaterial, listaContratomaterialFaturamento)){
									if(contratomaterial.getQtde() != null){
										if(contratomaterial.getValorfechadoOrValorunitario() != null)
											contratomaterial.getServico().setValordiatrans(contratomaterial.getValorfechadoOrValorunitario());
										qtdetotalenviada += contratomaterial.getQtde();
										if(mapMaterialRemessa.get(contratomaterial.getServico()) != null){
											mapMaterialRemessa.put(contratomaterial.getServico(), mapMaterialRemessa.get(contratomaterial.getServico()) + contratomaterial.getQtde());
										}else {
											mapMaterialRemessa.put(contratomaterial.getServico(), contratomaterial.getQtde());
										}
									}
								}else if(contratomaterial.getListaContratomateriallocacao() != null && 
										!contratomaterial.getListaContratomateriallocacao().isEmpty()){
									for(Contratomateriallocacao contratomateriallocacao : contratomaterial.getListaContratomateriallocacao()){
										if(contratomateriallocacao.getQtde() != null && !Boolean.TRUE.equals(contratomateriallocacao.getSubstituicao())){
											if(Contratomateriallocacaotipo.SAIDA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
												if(contratomaterial.getValorfechadoOrValorunitario() != null)
													contratomaterial.getServico().setValordiatrans(contratomaterial.getValorfechadoOrValorunitario());
												qtdetotalenviada += contratomateriallocacao.getQtde();
												if(mapMaterialRemessa.get(contratomaterial.getServico()) != null){
													mapMaterialRemessa.put(contratomaterial.getServico(), mapMaterialRemessa.get(contratomaterial.getServico()) + contratomateriallocacao.getQtde());
												}else {
													mapMaterialRemessa.put(contratomaterial.getServico(), contratomateriallocacao.getQtde());
												}
											}else if(Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
												if(	!fromFaturarLocacao || 
													!faturamentoporitemdevolvido || 
													!Boolean.TRUE.equals(contrato.getIgnoravalormaterial()) || 
													contratomateriallocacao.getDtmovimentacao() == null ||
													contrato.getDtiniciolocacao() == null ||
													(SinedDateUtils.beforeIgnoreHour(contratomateriallocacao.getDtmovimentacao(), contrato.getDtiniciolocacao()))
													){
													if(contratomaterial.getValorfechadoOrValorunitario() != null)
														contratomaterial.getServico().setValordiatrans(contratomaterial.getValorfechadoOrValorunitario());
													qtdetotaldevolvida += contratomateriallocacao.getQtde();
													if(mapMaterialDevolvida.get(contratomaterial.getServico()) != null){
														mapMaterialDevolvida.put(contratomaterial.getServico(), mapMaterialDevolvida.get(contratomaterial.getServico()) + contratomateriallocacao.getQtde());
													}else {
														mapMaterialDevolvida.put(contratomaterial.getServico(), contratomateriallocacao.getQtde());
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			if(mapMaterialRemessa != null && mapMaterialRemessa.size() > 0){
				for (Entry<Material,Double> entry : mapMaterialRemessa.entrySet()){
					Material material = entry.getKey();
					Double qtderemessa = entry.getValue();
					Double qtdedevolvida = mapMaterialDevolvida.get(material);
					
					ResumoMaterialRomaneio resumoMaterialRomaneio = new ResumoMaterialRomaneio();
					resumoMaterialRomaneio.setMaterial(material);
					resumoMaterialRomaneio.setQtderemessa(qtderemessa != null ? qtderemessa : 0.0);
					resumoMaterialRomaneio.setQtdedevolvida(qtdedevolvida != null ? qtdedevolvida : 0.0);
					resumoMaterialRomaneio.setValor(material.getValordiatrans() != null ? new Money(material.getValordiatrans()) : new Money());
					listaResumoMaterialRomaneio.add(resumoMaterialRomaneio);
				}
			} else {
				for (Entry<Material,Double> entry : mapMaterialDevolvida.entrySet()){
					Material material = entry.getKey();
					Double qtderemessa = 0d;
					Double qtdedevolvida = entry.getValue();
					
					ResumoMaterialRomaneio resumoMaterialRomaneio = new ResumoMaterialRomaneio();
					resumoMaterialRomaneio.setMaterial(material);
					resumoMaterialRomaneio.setQtderemessa(qtderemessa != null ? qtderemessa : 0.0);
					resumoMaterialRomaneio.setQtdedevolvida(qtdedevolvida != null ? qtdedevolvida : 0.0);
					resumoMaterialRomaneio.setValor(material.getValordiatrans() != null ? new Money(material.getValordiatrans()) : new Money());
					listaResumoMaterialRomaneio.add(resumoMaterialRomaneio);
				}
			}
			
			Collections.sort(listaResumoMaterialRomaneio, new Comparator<ResumoMaterialRomaneio>(){
				public int compare(ResumoMaterialRomaneio o1, ResumoMaterialRomaneio o2) {
					return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				}
			});
			
			resumoRomaneio.setListaResumoMaterialRomaneio(listaResumoMaterialRomaneio);
			resumoRomaneio.setQtdetotaldevolvida(qtdetotaldevolvida);
			resumoRomaneio.setQtdetotalenviada(qtdetotalenviada);
		}
		
		return resumoRomaneio;
	}
	
	private boolean getConsiderarQtdeTotalItem(Contratomaterial contratomaterial, List<Contratomaterial> listaContratomaterialFaturamento) {
		if(contratomaterial != null && contratomaterial.getCdcontratomaterial() != null && Hibernate.isInitialized(listaContratomaterialFaturamento) &&
				SinedUtil.isListNotEmpty(listaContratomaterialFaturamento)){
			for(Contratomaterial contratomaterialFaturamento : listaContratomaterialFaturamento){
				if(contratomaterial.equals(contratomaterialFaturamento)){
					contratomaterial.setValorfechadoCalculado(contratomaterialFaturamento.getValorfechado());
					return Boolean.TRUE.equals(contratomaterialFaturamento.getConsiderarQtdeTotalItem());
				}
			}
		}
		
		return false;
	}
	/**
	 * Adiciona informa��es no bean de romaneio para o relat�rio
	 *
	 * @param romaneioitem
	 * @param diferencadias
	 * @param romaneio
	 * @param listaContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/10/2013
	 */
	private Contratomaterial addInfoMaterialContrato(Romaneioitem romaneioitem, Integer diferencadias, Romaneio romaneio, List<Contrato> listaContrato) {
		Contrato contrato = null;
		for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
			if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato() != null ){
				for(Contrato c : listaContrato){
					if(c.getCdcontrato() != null && c.getCdcontrato().equals(romaneioorigem.getContrato().getCdcontrato())){
						contrato = c;
						break;
					}
				}
				
			}else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento() != null){
				for(Contrato c : listaContrato){
					if(c.getCdcontrato() != null && c.getCdcontrato().equals(romaneioorigem.getContratofechamento().getCdcontrato())){
						contrato = c;
						break;
					}
				}
				
			}
			if(contrato != null){
				Double qtdeFrequencia = 1d;
				if(contrato.getFrequencia() != null && contrato.getFrequencia().getCdfrequencia() != null){
					if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.SEMANAL)){
						qtdeFrequencia = 7d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.QUINZENAL)){
						qtdeFrequencia = 15d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL)){
						qtdeFrequencia = 30d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.TRIMESTRAL)){
						qtdeFrequencia = 3d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.SEMESTRE)){
						qtdeFrequencia = 6d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.SEMANAL)){
						qtdeFrequencia = 7d;
					}else if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.SEMANAL)){
						qtdeFrequencia = 7d;
					}
				}
					
				for(Contratomaterial cm : contrato.getListaContratomaterial()){
					if(cm.getServico() != null && cm.getServico().equals(romaneioitem.getMaterial())){
						romaneioitem.setValordiatrans(cm.getValorfechadoOrValorunitario() != null ? cm.getValorfechadoOrValorunitario() : 0.0);
						romaneioitem.setDiastrans(diferencadias);
						if(contrato.getIgnoravalormaterial() != null && contrato.getIgnoravalormaterial() && romaneioitem.getValordiatrans() != null && cm.getQtde() != null){
							romaneioitem.setValortotaltrans(SinedUtil.round(romaneioitem.getValordiatrans() * cm.getQtde(), 2));
						}else if(romaneioitem.getValordiatrans() != null && romaneioitem.getDiastrans() != null){
							if(romaneioitem.getQtde() != null && romaneioitem.getQtde() * romaneioitem.getValordiatrans() > 0){
								romaneioitem.setValortotaltrans(SinedUtil.round(romaneioitem.getQtde() * romaneioitem.getValordiatrans() / qtdeFrequencia * romaneioitem.getDiastrans(), 2));
							}else {
								romaneioitem.setValortotaltrans(SinedUtil.round(romaneioitem.getValordiatrans() * romaneioitem.getDiastrans(), 2));
							}
						}else {
							romaneioitem.setValortotaltrans(0.0);
						}
					}
				}
			}
			contrato = null;
		}
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#findForReport(RomaneioFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Romaneio> findForReport(RomaneioFiltro filtro) {
		return romaneioDAO.findForReport(filtro);
	}
	/**
	 * M�todo com referencia no DAO
	 * 
	 * @param whereInRomaneios
	 * @param romaneioSituacao
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoRomaneio(String whereInRomaneios, Romaneiosituacao romaneioSituacao) {
		romaneioDAO.doUpdateSituacaoRomaneio(whereInRomaneios, romaneioSituacao);
	}
	
	/**
	 * M�todo com referencia no DAO
	 * 
	 * @param localarmazenagemorigem
	 * @param whereInDestino
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Romaneio> getRomaneiosPossiveisParaEntregas(Localarmazenagem localarmazenagemorigem, String whereInDestino) {
		return romaneioDAO.getRomaneiosPossiveisParaEntregas(localarmazenagemorigem, whereInDestino);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Romaneio> getListaRomaneioDaEntrega(Entrega entrega) {
		return romaneioDAO.getListaRomaneioDaEntrega(entrega);
	}
	
	/**
	 * Monta a lista das origens do romaneio para a tela de entrada;
	 *
	 * @param romaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Romaneio romaneio) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		if(romaneio.getListaRomaneioorigem() != null && romaneio.getListaRomaneioorigem().size() > 0){
			for (Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()) {
				// ORIGEM A TELA DE GERENCIAMENTO ESTOQUE
				if(romaneioorigem.isEstoque()){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.GERENCIAMENTOESTOQUE, "GERENCIAMENTO DE ESTOQUE");
					listaBean.add(origemsuprimentosBean);
				}
				
				// ORIGEM ENTREGA
				if(romaneioorigem.getEntrega() != null && romaneioorigem.getEntrega().getCdentrega() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTREGA, "RECEBIMENTO", romaneioorigem.getEntrega().getCdentrega().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
				
				// ORIGEM CONTRATO
				if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCdcontrato() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.CONTRATO, "CONTRATO", romaneioorigem.getContrato().getCdcontrato().toString(), romaneioorigem.getContrato().getIdentificador());
					listaBean.add(origemsuprimentosBean);
				}
				
				// ORIGEM CONTRATO (FECHAMENTO)
				if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento().getCdcontrato() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.CONTRATO, "CONTRATO (FECHAMENTO)", romaneioorigem.getContratofechamento().getCdcontrato().toString(), romaneioorigem.getContratofechamento().getIdentificador());
					listaBean.add(origemsuprimentosBean);
				}
				
				// ORIGEM TAREFA
				if(romaneioorigem.getTarefa() != null && romaneioorigem.getTarefa().getCdtarefa() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.TAREFA, "TAREFA", romaneioorigem.getTarefa().getCdtarefa().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
				
				// ORIGEM REQUISI��O DE MATERIAL
				if(romaneioorigem.getRequisicaomaterial() != null && romaneioorigem.getRequisicaomaterial().getCdrequisicaomaterial() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.REQUISICAO, "REQUISI��O DE MATERIAL", romaneioorigem.getRequisicaomaterial().getCdrequisicaomaterial().toString(), romaneioorigem.getRequisicaomaterial().getIdentificadorOuCdrequisicaomaterial());
					listaBean.add(origemsuprimentosBean);
				}
			}
		}
		
		// CASO N�O ACHAR NENHUMA ORIGEM, COLOCA A ORIGEM CADASTRO
		if(listaBean.size() == 0){
			OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.CADASTRO, "CADASTRO");
			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * Monta a lista dos destinos do romaneio para a tela de entrada;
	 *
	 * @param romaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<OrigemsuprimentosBean> montaDestino(Romaneio romaneio) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		// DESTINO MOVIMENTA��ES DE PATRIM�NIO
		List<Movpatrimonio> listaMovpatrimonio = movpatrimonioService.findByRomaneio(romaneio);
		if(listaMovpatrimonio != null && listaMovpatrimonio.size() > 0){
			for (Movpatrimonio movpatrimonio : listaMovpatrimonio) {
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.MOVPATRIMONIO, "MOVIMENTA��O DE PATRIM�NIO", movpatrimonio.getCdmovpatrimonio().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		
		// DESTINO AS ENTRADAS E SA�DAS
		List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRomaneio(romaneio); 
		if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
			for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
				String descricao = "SA�DA";
				if(movimentacaoestoque.getMovimentacaoestoquetipo() != null && movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
					descricao = "ENTRADA";
				}
				
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTRADASAIDA, descricao, movimentacaoestoque.getCdmovimentacaoestoque().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		
		List <Notaromaneio> listaNotaRomaneio = notaromaneioService.findByRomaneio(romaneio);
		if (listaNotaRomaneio != null && !listaNotaRomaneio.isEmpty()){
			for(Notaromaneio notaromaneio : listaNotaRomaneio){
				if(notaromaneio.getNota() != null && notaromaneio.getNota().getCdNota() != null){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.NOTA, "NOTA FISCAL DE SIMPLES REMESSA", notaromaneio.getNota().getCdNota().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
			}
		}
		
		
		return listaBean;
	}
	
	/**
	 * Gera os registro de entrada e sa�da do romaneio passado por par�metro.
	 *
	 * @param romaneio
	 * @param localarmazenagemorigem
	 * @param localarmazenagemdestino
	 * @param material
	 * @param materialclasse
	 * @param qtde
	 * @param valor
	 * @param empresadestino
	 * @param projetodestino
	 * @param observacao
	 * @author Rodrigo Freitas
	 * @param requisicaomaterial 
	 * @since 13/12/2012
	 */
	public void gerarEntradaSaidaRomaneio(Romaneio romaneio, Romaneioitem romaneioitem, Double valor, 
			Empresa empresadestino, Projeto projetodestino, String observacao, Empresa empresaorigem, 
			Projeto projetoorigem, Boolean romaneiomanual, Cliente clientedestino, Loteestoque loteestoque, Centrocusto centrocusto, Requisicaomaterial requisicaomaterial) {
		Localarmazenagem localarmazenagemorigem = romaneio.getLocalarmazenagemorigem();
		if(localarmazenagemorigem != null && localarmazenagemorigem.getCdlocalarmazenagem() != null){
			Empresa empresa = this.getEmpresaUnicaLocalarmazenagem(localarmazenagemorigem);
			if(empresa != null) empresaorigem = empresa;
		}
		
		Localarmazenagem localarmazenagemdestino = romaneio.getLocalarmazenagemdestino();
		if(localarmazenagemdestino != null && localarmazenagemdestino.getCdlocalarmazenagem() != null){
			Empresa empresa = this.getEmpresaUnicaLocalarmazenagem(localarmazenagemdestino);
			if(empresa != null) empresadestino = empresa;
		}
		
		if(romaneioitem.getMaterialclasse() != null && romaneioitem.getMaterialclasse().equals(Materialclasse.PATRIMONIO) && romaneioitem.getPatrimonioitem() != null){
			Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
			movpatrimonioorigem.setRomaneio(romaneio);
			movpatrimonioorigem.setRequisicaomaterial(requisicaomaterial);
			
			movpatrimonioorigemService.saveOrUpdate(movpatrimonioorigem);
			
			Movpatrimonio movpatrimonio = new Movpatrimonio();
			movpatrimonio.setPatrimonioitem(romaneioitem.getPatrimonioitem());
			movpatrimonio.setMovpatrimonioorigem(movpatrimonioorigem);
			movpatrimonio.setLocalarmazenagem(localarmazenagemdestino);
			movpatrimonio.setEmpresa(empresadestino);
			if (romaneio.getProjeto() != null && (romaneio.getConsiderarProjetoorigem() == null || !romaneio.getConsiderarProjetoorigem())){
				movpatrimonio.setProjeto(romaneio.getProjeto());
			}
			else{
				movpatrimonio.setProjeto(projetodestino);
			}
			movpatrimonio.setValormovimentacao(valor);
			movpatrimonio.setCliente(clientedestino);
			movpatrimonio.setDtmovimentacao(new Timestamp(System.currentTimeMillis()));
			
			movpatrimonioService.saveOrUpdate(movpatrimonio);
		} else {
			// SAIDA
			Movimentacaoestoqueorigem movimentacaoestoqueorigemSaida = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigemSaida.setRomaneio(romaneio);
			movimentacaoestoqueorigemSaida.setRomaneiomanual(romaneiomanual);
			Material mat = romaneioitem.getMaterial();
			Movimentacaoestoque movimentacaoestoqueSaida = new Movimentacaoestoque();
			movimentacaoestoqueSaida.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoqueSaida.setLocalarmazenagem(localarmazenagemorigem);
			movimentacaoestoqueSaida.setLoteestoque(romaneioitem.getLoteestoque());
			movimentacaoestoqueSaida.setMaterial(mat);
			movimentacaoestoqueSaida.setMaterialclasse(romaneioitem.getMaterialclasse());
			movimentacaoestoqueSaida.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoqueSaida.setObservacao(observacao);
			movimentacaoestoqueSaida.setQtde(romaneioitem.getQtde());
			movimentacaoestoqueSaida.setMovimentacaoestoqueorigem(movimentacaoestoqueorigemSaida);
			movimentacaoestoqueSaida.setValor(valor);
			movimentacaoestoqueSaida.setEmpresa(empresaorigem);

			if (romaneio.getProjeto() != null && (romaneio.getConsiderarProjetoorigem() == null || !romaneio.getConsiderarProjetoorigem())){
				movimentacaoestoqueSaida.setProjeto(romaneio.getProjeto());
			}
			else{
				movimentacaoestoqueSaida.setProjeto(projetoorigem);
			}
			if(loteestoque != null) {
				movimentacaoestoqueSaida.setLoteestoque(loteestoque);
			}
			Movimentacaoanimalmotivo movimentacaoanimalmotivo = null;
			Material material = materialService.load(romaneioitem.getMaterial(), "material.materialgrupo");
			if(material!=null && material.getMaterialgrupo()!=null && Boolean.TRUE.equals(material.getMaterialgrupo().getAnimal())){
				movimentacaoanimalmotivo = Movimentacaoanimalmotivo.TRANSFERENCIA;
				movimentacaoestoqueSaida.setMovimentacaoanimalmotivo(movimentacaoanimalmotivo);
			}
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				if(valor != null && romaneioitem.getQtde() != null){
					movimentacaoestoqueSaida.setContaGerencial(mat.getMaterialRateioEstoque() != null ? mat.getMaterialRateioEstoque().getContaGerencialRomaneioSaida() : null);
					movimentacaoestoqueSaida.setProjetoRateio(movimentacaoestoqueSaida.getProjeto());
					if(centrocusto != null && centrocusto.getCdcentrocusto() != null){
						movimentacaoestoqueSaida.setCentrocusto(centrocusto);
					} else {
						movimentacaoestoqueSaida.setCentrocusto(romaneio.getCentroCusto());
					}
					movimentacaoestoqueSaida.setRateio(rateioService.criarRateio(movimentacaoestoqueSaida));
				}
			}
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoqueSaida);
			movimentacaoestoqueService.salvarHisotirico(movimentacaoestoqueSaida,"Criado a partir do romaneio " + SinedUtil.makeLinkHistorico(romaneio.getCdromaneio().toString(), "visualizarRomaneio"),MovimentacaoEstoqueAcao.CRIAR);
			// ENTRADA
			Movimentacaoestoqueorigem movimentacaoestoqueorigemEntrada = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigemEntrada.setRomaneio(romaneio);
			movimentacaoestoqueorigemEntrada.setRomaneiomanual(romaneiomanual);
			
			Movimentacaoestoque movimentacaoestoqueEntrada = new Movimentacaoestoque();
			movimentacaoestoqueEntrada.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoqueEntrada.setLocalarmazenagem(localarmazenagemdestino);
			movimentacaoestoqueEntrada.setLoteestoque(romaneioitem.getLoteestoque());
			movimentacaoestoqueEntrada.setMaterial(mat);
			movimentacaoestoqueEntrada.setMaterialclasse(romaneioitem.getMaterialclasse());
			movimentacaoestoqueEntrada.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
			movimentacaoestoqueEntrada.setObservacao(observacao);
			movimentacaoestoqueEntrada.setQtde(romaneioitem.getQtde());
			movimentacaoestoqueEntrada.setMovimentacaoestoqueorigem(movimentacaoestoqueorigemEntrada);
			movimentacaoestoqueEntrada.setValor(null);
			movimentacaoestoqueEntrada.setValorcusto(valor);
			movimentacaoestoqueEntrada.setEmpresa(empresadestino);
			
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				if(valor != null && romaneioitem.getQtde() != null){
					movimentacaoestoqueEntrada.setContaGerencial(mat.getMaterialRateioEstoque() != null ? mat.getMaterialRateioEstoque().getContaGerencialRomaneioEntrada() : null);
					movimentacaoestoqueEntrada.setProjetoRateio(movimentacaoestoqueEntrada.getProjeto());
					if(centrocusto != null && centrocusto.getCdcentrocusto() != null){
						movimentacaoestoqueEntrada.setCentrocusto(centrocusto);
					} else {
						movimentacaoestoqueEntrada.setCentrocusto(romaneio.getCentroCusto());
					}
					movimentacaoestoqueEntrada.setRateio(rateioService.criarRateio(movimentacaoestoqueEntrada));
				}
			}
			if(movimentacaoanimalmotivo!=null){
				movimentacaoestoqueEntrada.setMovimentacaoanimalmotivo(movimentacaoanimalmotivo);
			}
			if (romaneio.getProjeto() != null && (romaneio.getConsiderarProjetoorigem() == null || !romaneio.getConsiderarProjetoorigem())){
				movimentacaoestoqueEntrada.setProjeto(romaneio.getProjeto());
			}
			else{
				movimentacaoestoqueEntrada.setProjeto(projetodestino);
			}
			if(loteestoque != null) {
				movimentacaoestoqueEntrada.setLoteestoque(loteestoque);
			}
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoqueEntrada);
			movimentacaoestoqueService.salvarHisotirico(movimentacaoestoqueEntrada,"Criado a partir do romaneio " + SinedUtil.makeLinkHistorico(romaneio.getCdromaneio().toString(), "visualizarRomaneio"), MovimentacaoEstoqueAcao.CRIAR);
			
		}
	}
	
	/**
	 * Pega a empresa se tiver um s� na lista.
	 * 
	 * @param localarmazenagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa getEmpresaUnicaLocalarmazenagem(Localarmazenagem localarmazenagem) {
		List<Localarmazenagemempresa> listaLocalEmpresa = localarmazenagemempresaService.findByLocalarmazenagem(localarmazenagem);
		if(listaLocalEmpresa != null && 
				listaLocalEmpresa.size() == 1 && 
				listaLocalEmpresa.get(0) != null && 
				listaLocalEmpresa.get(0).getEmpresa() != null){
			return listaLocalEmpresa.get(0).getEmpresa();
		}
		return null;
	}
	
	
	public Empresa getEmpresaOrigem(Empresa empresa, Localarmazenagem localarmazenagem) {
		if(Util.objects.isPersistent(localarmazenagem)){
			List<Localarmazenagemempresa> listaLocalEmpresa = localarmazenagemempresaService.findByLocalarmazenagem(localarmazenagem);
			if(listaLocalEmpresa != null && 
					listaLocalEmpresa.size() == 1 && 
					listaLocalEmpresa.get(0) != null && 
					listaLocalEmpresa.get(0).getEmpresa() != null){
				return listaLocalEmpresa.get(0).getEmpresa();
			}
		}
		return empresa;
	}
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#findForBaixar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Romaneio> findForBaixar(String whereIn) {
		return romaneioDAO.findForBaixar(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/03/2014
	 */
	public List<Romaneio> findForGerarNota(String whereIn) {
		return romaneioDAO.findForGerarNota(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#findForEmissaoNotaValidacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Romaneio> findForEmissaoNotaValidacao(String whereIn) {
		return romaneioDAO.findForEmissaoNotaValidacao(whereIn);
	}
	
	/**
	 * M�todo que preenche a lista para abrir a tela de Gerar romaneio.
	 *
	 * @param request
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2012
	 */
	public ModelAndView abrirTelaGerarRomaneio(WebRequestContext request, List<GerarRomaneioBean> lista, String whereInContrato, 
			boolean fromGerenciamento, boolean indenizacao, String whereInRequisicaomaterial) {
		
		boolean fromContrato = whereInContrato != null && !whereInContrato.equals("");
		boolean fromRequisicaomaterial = whereInRequisicaomaterial != null && !whereInRequisicaomaterial.equals("");
		
		List<RomaneioGerenciamentoMaterialItem> listaMateriais = new ArrayList<RomaneioGerenciamentoMaterialItem>();
		
		boolean valorprodutoByContrato = parametrogeralService.getBoolean(Parametrogeral.CONTRATO_VALOR_PRODUTO);
		boolean exibirAlertaAtualizacaoPeriodolocacao = false;
		Localarmazenagem localarmazenagemorigem = null;
		for (GerarRomaneioBean gerarRomaneioBean : lista) {			
			Material material = gerarRomaneioBean.getMaterial();
			Materialclasse materialclasse = gerarRomaneioBean.getMaterialclasse();
			
			if(!exibirAlertaAtualizacaoPeriodolocacao && gerarRomaneioBean.getExibirAlertaAtualizacaoPeriodolocacao() != null &&
					gerarRomaneioBean.getExibirAlertaAtualizacaoPeriodolocacao()){
				exibirAlertaAtualizacaoPeriodolocacao = true;
			}
			material = materialService.load(material, "material.cdmaterial, material.identificacao, material.nome, material.valorcusto, material.valorindenizacao, material.obrigarlote");
			
			RomaneioGerenciamentoMaterialItem item = new RomaneioGerenciamentoMaterialItem();
			
			if(fromGerenciamento){
				Loteestoque loteestoque = gerarRomaneioBean.getLoteestoque();
				item.setLoteestoque(loteestoque);
				Localarmazenagem localarmazenagem = gerarRomaneioBean.getLocalarmazenagem();
				
				if(localarmazenagem != null)
					localarmazenagem = localarmazenagemService.load(localarmazenagem, "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome");
				
				Double qtdeorigem = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServicoFromConferencia(material, materialclasse, localarmazenagem, loteestoque, gerarRomaneioBean.getEmpresa());
				
				item.setQtdeorigem(qtdeorigem);
				item.setQtdedestino(qtdeorigem != null && qtdeorigem > 0 ? qtdeorigem : 0d);
				item.setLocalarmazenagemorigem(localarmazenagem);
				
				if(!fromContrato && localarmazenagem == null){
					request.addError("N�o � possivel gerar romaneio de entrada/sa�da sem local.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
					return null;
				}
				
				if(!fromContrato && qtdeorigem != null && qtdeorigem <= 0 && 
						localarmazenagem != null && !localarmazenagemService.getPermitirestoquenegativo(localarmazenagem)){
					request.addError("N�o � possivel gerar romaneio com quantidade negativa.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
					return null;
				}
					
			}
			
			if(fromContrato){
				item.setQtdedestino(gerarRomaneioBean.getQtde());
				item.setContratomaterial(gerarRomaneioBean.getContratomaterial());
				item.setQtderestante(gerarRomaneioBean.getQtdeRestante());
				item.setPatrimonioitem(gerarRomaneioBean.getPatrimonioitem());
				item.setQtdedestino(item.getQtderestante());
				item.setQtdedestinoOriginal(gerarRomaneioBean.getQtdedestinoOriginal());
				if(gerarRomaneioBean.getLocalarmazenagem() != null){
					localarmazenagemorigem = gerarRomaneioBean.getLocalarmazenagem();
				}
			}
			
			if(fromRequisicaomaterial){
				item.setQtdedestino(gerarRomaneioBean.getQtde());
				item.setQtderestante(gerarRomaneioBean.getQtdeRestante());
				item.setLocalarmazenagemdestino(gerarRomaneioBean.getLocalarmazenagem());
				item.setProjetodestino(gerarRomaneioBean.getProjeto());
				item.setRequisicaomaterial(gerarRomaneioBean.getRequisicaomaterial());
			}
			
			item.setMaterial(material);
			item.setMaterialclasse(materialclasse);
			if(valorprodutoByContrato && fromContrato){
				if(gerarRomaneioBean.getContratomaterial() != null)
					item.setValor(SinedUtil.roundByParametro(gerarRomaneioBean.getContratomaterial().getValorunitario()));
			}else{
				if(material.getValorindenizacao() == null){
					item.setValor(SinedUtil.roundByParametro(material.getValorcusto()));
				} else{
					item.setValor(SinedUtil.roundByParametro(material.getValorindenizacao()));
				}
			}
			
			listaMateriais.add(item);
		}
		
		RomaneioGerenciamentoMaterial bean = new RomaneioGerenciamentoMaterial();
		bean.setListaMateriais(listaMateriais);
		bean.setWhereInContrato(whereInContrato);
		bean.setWhereInRequisicaomaterial(whereInRequisicaomaterial);
		bean.setLocalarmazenagemorigem(localarmazenagemorigem);
		bean.setExibirAlertaAtualizacaoPeriodolocacao(exibirAlertaAtualizacaoPeriodolocacao);
		
		if(fromContrato){
			Empresa empresa = contratoService.getEmpresaByContratos(whereInContrato);
			bean.setEmpresadestino(empresa);
			bean.setEmpresaorigem(empresa);
			
			if(empresa != null){
				Localarmazenagem localarmazenagemdestino = localarmazenagemService.loadByEmpresaAndClienteLocacao(empresa);
				bean.setLocalarmazenagemdestino(localarmazenagemdestino);
				
				if (indenizacao){
					bean.setIsindenizacao(Boolean.TRUE);
					bean.setLocalarmazenagemorigem(localarmazenagemService.loadForRomaneioFromContrato(empresa, true, null, null));
					bean.setLocalarmazenagemdestino(localarmazenagemService.loadForRomaneioFromContrato(empresa, null, null, true));
				}else {
					bean.setLocalarmazenagemorigem(localarmazenagemService.loadForRomaneioFromContrato(empresa, null, true, null));
				}
			}
			
			Cliente cliente = contratoService.getClienteByContratos(whereInContrato);
			bean.setClientedestino(cliente);
		}
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(null));
		request.setAttribute("existeClassePatrimionio", bean.existeClassePatrimionio());
//		request.setAttribute("PERMITIR_ESTOQUE_NEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO));
		
		return new ModelAndView("direct:/crud/popup/romaneio").addObject("bean", bean);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.RomaneioDAO#findByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public List<Romaneio> findByContrato(Contrato contrato) {
		return romaneioDAO.findByContrato(contrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.RomaneioDAO#findByWhereInContrato(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Jo�o Vitor
	 * @since 25/03/2015
	 */
	public List<Romaneio> findForCancelamentoByWhereInContrato(String whereIn) {
		return romaneioDAO.findForCancelamentoByWhereInContrato(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.RomaneioDAO#findByContratoFechamento(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public List<Romaneio> findByContratoFechamento(Contrato contrato) {
		return romaneioDAO.findByContratoFechamento(contrato);
	}
	
	/**
	 * Gera a string com link de romaneio para o hist�rico.
	 *
	 * @param romaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public String geraLinkRomaneio(Romaneio romaneio) {
		return "<a href='javascript:visualizarRomaneio(" + romaneio.getCdromaneio() + ");'>" + romaneio.getCdromaneio() + "</a>";
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.RomaneioService#findForCancelar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Romaneio> findForCancelar(String whereIn) {
		List<Romaneio> lista = romaneioDAO.findForCancelar(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Romaneio romaneio : lista){
				if(SinedUtil.isListNotEmpty(romaneio.getListaRomaneioitem())){
					for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
						if(romaneioorigem.getContrato() != null){
							romaneioorigem.setContrato(contratoService.findForCancelarRomaneio(romaneioorigem.getContrato()));
						}else if(romaneioorigem.getContratofechamento() != null){
							romaneioorigem.setContratofechamento(contratoService.findForCancelarRomaneio(romaneioorigem.getContratofechamento()));
						}
					}
				}
			}
		}
		return lista;
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Romaneio> findForImprimirRomaneio(String whereIn){
		return romaneioDAO.findForImprimirRomaneio(whereIn);
	}
	
	/**
	 * M�todo que cria o romaneio ap�s gerar movimenta��o de patrimonio do contrato de fornecimento
	 *
	 * @param request
	 * @param bean
	 * @author Luiz Fernando
	 */
	public void criaRomaneioByMovimentacao(WebRequestContext request, Romaneio bean) {
		Empresa empresa = null;
		Localarmazenagem localarmazenagemorigem = null;
		String whereInPatrimonioitem = null;
		List<Patrimonioitem> listaPatrimonioitem = null;
		List<Romaneioitem> listaRomaneioitem = new ListSet<Romaneioitem>(Romaneioitem.class);
		Romaneioitem romaneioitem;
		if(request.getParameter("cdempresa") != null && !"".equals(request.getParameter("cdempresa"))){
			empresa = new Empresa(Integer.parseInt(request.getParameter("cdempresa")));
		}
		if(request.getParameter("cdlocal") != null && !"".equals(request.getParameter("cdlocal"))){
			localarmazenagemorigem = localarmazenagemService.load(new Localarmazenagem(Integer.parseInt(request.getParameter("cdlocal"))), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome");
		}
		if(request.getParameter("whereInPatrimonioitem") != null && !"".equals(request.getParameter("whereInPatrimonioitem"))){
			whereInPatrimonioitem = request.getParameter("whereInPatrimonioitem");
			listaPatrimonioitem = patrimonioitemService.findForRomaneio(whereInPatrimonioitem);
		}
		bean.setEmpresa(empresa);
		bean.setLocalarmazenagemorigem(localarmazenagemorigem);
		if(listaPatrimonioitem != null && !listaPatrimonioitem.isEmpty()){
			for(Patrimonioitem patrimonioitem : listaPatrimonioitem){
				romaneioitem = new Romaneioitem();
				romaneioitem.setMaterial(patrimonioitem.getBempatrimonio());
				romaneioitem.setMaterialclasse(Materialclasse.PATRIMONIO);
				listaRomaneioitem.add(romaneioitem);
			}
		}
		
		bean.setListaRomaneioitem(listaRomaneioitem);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#haveRomaneioOrigemContrato(String whereInContrato)
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public Boolean haveRomaneioOrigemContrato(String whereInContrato) {
		return romaneioDAO.haveRomaneioOrigemContrato(whereInContrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#haveRomaneioEntradaOrigemContrato(String whereInContrato)
	 *
	 * @param whereInContrato
	 * @return
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public Boolean haveRomaneioEntradaOrigemContrato(String whereInContrato) {
		return romaneioDAO.haveRomaneioEntradaOrigemContrato(whereInContrato);
	}
	
	/**
	 * Cancela os romaneios e movimenta��es de estoque relacionadas.
	 *
	 * @param contrato
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public void cancelaRomaneioByContrato(Contrato contrato) {
		List<Romaneio> listaRomaneio = new ArrayList<Romaneio>();
		listaRomaneio.addAll(this.findByContrato(contrato));
		listaRomaneio.addAll(this.findByContratoFechamento(contrato));
		
		this.cancelaRomaneios(listaRomaneio);
	}
	
	/**
	 * M�todo que faz o cancelamento de uma lista de romaneio
	 *
	 * @param listaRomaneio
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public void cancelaRomaneios(List<Romaneio> listaRomaneio) {
		if(listaRomaneio != null && listaRomaneio.size() > 0){
			for (Romaneio romaneio : listaRomaneio) {
				List<Movpatrimonio> listaMovpatrimonio = movpatrimonioService.findByRomaneio(romaneio);
				if(listaMovpatrimonio != null && listaMovpatrimonio.size() > 0){
					movpatrimonioService.updateCancelada(CollectionsUtil.listAndConcatenate(listaMovpatrimonio, "cdmovpatrimonio", ","));
				}
				
				List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRomaneio(romaneio);
				if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
					movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
				}
			}
			
			String whereInRomaneio = CollectionsUtil.listAndConcatenate(listaRomaneio, "cdromaneio", ",");
			if(whereInRomaneio != null && !"".equals(whereInRomaneio)){
				this.doUpdateSituacaoRomaneio(whereInRomaneio, Romaneiosituacao.CANCELADA);
			}
		}
	}
	
	public void criaRomaneioByTarefa(String tarefas, Romaneio romaneio) {
		List<Tarefa> listaTarefa = tarefaService.findForRomaneio(tarefas);
		
		Projeto projeto = null;
		Cliente cliente = null;
		List<Romaneioitem> listaRomaneioitem = new ArrayList<Romaneioitem>();
		
		boolean primeiraPassagem = true;
		for (Tarefa tarefa : listaTarefa) {
			if(primeiraPassagem) {
				if(tarefa.getPlanejamento() != null && tarefa.getPlanejamento().getProjeto() != null){
					projeto = tarefa.getPlanejamento().getProjeto();
					cliente = tarefa.getPlanejamento().getProjeto().getCliente();
				}
				primeiraPassagem = false;
			} else {
				if(tarefa.getPlanejamento() != null && tarefa.getPlanejamento().getProjeto() != null){
					if(cliente != null && tarefa.getPlanejamento().getProjeto().getCliente() != null && 
							!cliente.equals(tarefa.getPlanejamento().getProjeto().getCliente())){
						cliente = null;
					}
					if(projeto != null && !projeto.equals(tarefa.getPlanejamento().getProjeto())){
						projeto = null;
					}
				}
			}
			
			Set<Tarefarecursogeral> listaTarefarecursogeral = tarefa.getListaTarefarecursogeral();
			if(listaTarefarecursogeral != null && listaTarefarecursogeral.size() > 0){
				for (Tarefarecursogeral tarefarecursogeral : listaTarefarecursogeral) {
					if(tarefarecursogeral.getMaterial() != null){
						if(tarefarecursogeral.getMaterial().getPatrimonio() != null && tarefarecursogeral.getMaterial().getPatrimonio()){
							Double qtde = tarefarecursogeral.getQtde();
							for (int i = 0; i < qtde; i++) {
								Romaneioitem romaneioitem = new Romaneioitem();
								romaneioitem.setMaterial(tarefarecursogeral.getMaterial());
								romaneioitem.setQtde(1d);
								listaRomaneioitem.add(romaneioitem);
							}
						} else {
							Romaneioitem romaneioitem = new Romaneioitem();
							romaneioitem.setMaterial(tarefarecursogeral.getMaterial());
							romaneioitem.setQtde(tarefarecursogeral.getQtde());
							listaRomaneioitem.add(romaneioitem);
						}
					}
				}
			}
		}
		
		romaneio.setListaRomaneioitem(listaRomaneioitem);
		romaneio.setProjeto(projeto);
		romaneio.setCliente(cliente);
		romaneio.setWhereInTarefa(tarefas);
		
		if(cliente != null){
			cliente = clienteService.carregarDadosCliente(cliente);
			Set<Endereco> listaEndereco = cliente.getListaEndereco();
			if(listaEndereco != null && listaEndereco.size() > 0){
				for (Endereco endereco : listaEndereco) {
					if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						romaneio.setEndereco(endereco);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/04/2014
	 */
	public List<Romaneio> findByRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		return romaneioDAO.findByRequisicaomaterial(requisicaomaterial);
	}
	
	/**
	* M�todo que verifica se o romaneio � o �ltimo gerado
	*
	* @param romaneio
	* @param whereInContrato
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public boolean isUltimoRomaneioGeradoByContrato(Romaneio romaneio, String whereInContrato) {
		boolean ultimoRomaneio = false;
		if(romaneio != null && romaneio.getCdromaneio() != null && romaneio.getDtromaneio() != null && whereInContrato != null && !"".equals(whereInContrato)){
			ultimoRomaneio = isUltimoRomaneioContrato(romaneio, whereInContrato);
			if(!ultimoRomaneio){
				List<Contrato> listaContrato = contratoService.findForVerificarUltimoRomaneio(whereInContrato);
				if(SinedUtil.isListNotEmpty(listaContrato)){
					for(Contrato c : listaContrato){
						if(SinedUtil.isListNotEmpty(c.getListaContratohistorico())){
							for(Contratohistorico ch : c.getListaContratohistorico()){
								if(ch.getObservacao() != null && ch.getObservacao().contains("visualizarRomaneio(")){
									String observacao = ch.getObservacao().substring(ch.getObservacao().indexOf("visualizarRomaneio(")+19);
									if(observacao.indexOf(")") != -1){
										Romaneio bean = loadWithSituacao(new Romaneio(Integer.parseInt(observacao.substring(0, observacao.indexOf(")")))));
										if(bean == null || !Romaneiosituacao.CANCELADA.equals(bean.getRomaneiosituacao())){
											if(bean == null || romaneio.getCdromaneio().equals(bean.getCdromaneio()) ||
													SinedDateUtils.afterIgnoreHour(romaneio.getDtromaneio(), bean.getDtromaneio())){
												ultimoRomaneio = true;
											}
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return ultimoRomaneio;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
    * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#isUltimoRomaneioContrato(Romaneio romaneio, String whereInContrato)
	*
	* @param romaneio
	* @param whereInContrato
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public boolean isUltimoRomaneioContrato(Romaneio romaneio, String whereInContrato){
		return romaneioDAO.isUltimoRomaneioContrato(romaneio, whereInContrato);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RomaneioDAO#isUltimoRomaneioDevolucaoNotCanceladoByContrato(Contrato contrato)
	*
	* @param contrato
	* @return
	* @since 18/08/2014
	* @author Luiz Fernando
	*/
	public boolean isUltimoRomaneioDevolucaoNotCanceladoByContrato(Contrato contrato){
		return romaneioDAO.isUltimoRomaneioDevolucaoNotCanceladoByContrato(contrato);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
    * @see br.com.linkcom.sined.geral.dao.RomaneioDAO#loadWithSituacao(Romaneio romaneio)
	*
	* @param romaneio
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public Romaneio loadWithSituacao(Romaneio romaneio){
		return romaneioDAO.loadWithSituacao(romaneio);
	}
	
	public void cancelar(WebRequestContext request, Boolean fromContratoCrud){
		String whereIn = request.getParameter("selectedItens");
		String whereInCdContrato = request.getParameter("whereInContrato");
		if(StringUtils.isBlank(whereIn) && StringUtils.isBlank(whereInCdContrato)){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Romaneio> listaRomaneio = null;
		List<Romaneio> listaRomaneioSubstituido = null;
		
		if (fromContratoCrud == null) {
			fromContratoCrud = Boolean.FALSE;
		}
		
		if (fromContratoCrud) {
			listaRomaneio = findForCancelamentoByWhereInContrato(whereInCdContrato);
		} else {
			listaRomaneio = findForCancelar(whereIn);
			
			if(SinedUtil.isListNotEmpty(listaRomaneio)){
				for(Romaneio romaneio : listaRomaneio){
					if(romaneio.getRomaneiosubstituido() != null && romaneio.getRomaneiosubstituido().getCdromaneio() != null && 
							romaneio.getRomaneiosituacao() != null && !Romaneiosituacao.CANCELADA.equals(romaneio.getRomaneiosituacao())){
						if(listaRomaneioSubstituido == null){
							listaRomaneioSubstituido = new ArrayList<Romaneio>();
						}
						boolean addSubstituido = true;
						for(Romaneio romaneioSubstituido : listaRomaneioSubstituido){
							if(romaneioSubstituido.getCdromaneio().equals(romaneio.getRomaneiosubstituido().getCdromaneio())){
								addSubstituido = false;
								break;
							}
						}
						if(addSubstituido){
							listaRomaneioSubstituido.add(romaneio.getRomaneiosubstituido());
						}
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaRomaneioSubstituido)){
					listaRomaneioSubstituido = findForCancelar(SinedUtil.listAndConcatenate(listaRomaneioSubstituido, "cdromaneio", ","));
					
					if(SinedUtil.isListNotEmpty(listaRomaneioSubstituido)){
						for(Romaneio romaneioSubstituido : listaRomaneioSubstituido){
							romaneioSubstituido.setCancelamentoRomaneiosubstituido(true);
							if(SinedUtil.isListNotEmpty(romaneioSubstituido.getListaRomaneioitem())){
								List<Romaneioitem> listaRomaneioitem = romaneioSubstituido.getListaRomaneioitem();
								for (Romaneioitem romaneioitem : listaRomaneioitem) {
									Romaneioitem romaneioitemsubstituido = this.getRomaneioitemSubstituido(romaneioitem, listaRomaneio);
									if(romaneioitemsubstituido != null &&
											romaneioitemsubstituido.getMaterial() != null &&
											SinedUtil.isListNotEmpty(romaneioSubstituido.getListaRomaneioorigem())){
										boolean executarUpdatePatrimonioContrato = false;
										for(Romaneioorigem romaneioorigem : romaneioSubstituido.getListaRomaneioorigem()){
											if(romaneioorigem.getContratofechamento() != null && 
													SinedUtil.isListNotEmpty(romaneioorigem.getContratofechamento().getListaContratomaterial())){
												Contratomaterial contratomaterial = contratoService.getContratomaterialByMaterial(romaneioorigem.getContratofechamento().getListaContratomaterial(), romaneioitemsubstituido.getMaterial(), romaneioitemsubstituido.getPatrimonioitem(), romaneioitemsubstituido.getQtde());
												if(contratomaterial != null){
													if(romaneioitem.getContratomaterialSubstituto() != null && 
															!contratomaterial.equals(romaneioitem.getContratomaterialSubstituto())){
														executarUpdatePatrimonioContrato = false;
														romaneioitem.setContratomaterialSubstituto(null);
														break;
													}else {
														romaneioitem.setContratomaterialSubstituto(contratomaterial);
														executarUpdatePatrimonioContrato = true;
													}
												}
											}
										}
										romaneioitem.setExecutarUpdatePatrimonioContrato(executarUpdatePatrimonioContrato);
									}
								}
							}
						}
						
						listaRomaneio.addAll(listaRomaneioSubstituido);
					}
				}
			}
		}
		
		StringBuilder whereInRomaneio = new StringBuilder();
		StringBuilder whereInRequisicaomaterial = new StringBuilder();
//		String paramEstoquenegativo = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO);
		Integer qtdesucesso = 0;
		Integer qtdeerro = 0;
		Integer qtdeerropatrimonio = 0;
		for (Romaneio romaneio : listaRomaneio) {
			boolean erro = false;
			if(romaneio.getListaRomaneioorigem() != null && !romaneio.getListaRomaneioorigem().isEmpty()){
				StringBuilder whereInContrato = new StringBuilder();
				for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
					if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCdcontrato() != null){
						if(!whereInContrato.toString().contains(romaneioorigem.getContrato().getCdcontrato().toString())){
							if(!whereInContrato.toString().equals(""))
								whereInContrato.append(",");
							whereInContrato.append(romaneioorigem.getContrato().getCdcontrato());
						}
					}else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento().getCdcontrato() != null){
						if(!whereInContrato.toString().contains(romaneioorigem.getContratofechamento().getCdcontrato().toString())){
							if(!whereInContrato.toString().equals(""))
								whereInContrato.append(",");
							whereInContrato.append(romaneioorigem.getContratofechamento().getCdcontrato());
						}
					}
					
				}
				if((romaneio.getCancelamentoRomaneiosubstituido() == null || !romaneio.getCancelamentoRomaneiosubstituido()) && 
						whereInContrato != null && 
						!whereInContrato.toString().equals("") && 
						!isUltimoRomaneioGeradoByContrato(romaneio, whereInContrato.toString())){
					request.addError("N�o � permitido cancelar o romaneio " + romaneio.getCdromaneio() + ". Este, n�o � o �ltimo romaneio gerado.");
					erro = true;
				}
			}
			
			if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), romaneio.getLocalarmazenagemorigem(), romaneio.getProjeto())) {
				request.addError("N�o � poss�vel realizar o cancelamento,  pois o registro est� vinculado � um registro de invent�rio. Para cancelar, � necess�rio cancelar o invent�rio.");
				erro = true;
			}
			
			if (inventarioService.hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), romaneio.getLocalarmazenagemdestino(), romaneio.getProjeto())) {
				request.addError("N�o � poss�vel realizar o cancelamento,  pois o registro est� vinculado � um registro de invent�rio. Para cancelar, � necess�rio cancelar o invent�rio.");
				erro = true;
			}
			
			if(!erro){
				StringBuilder whereInMovimentacaoestoque = new StringBuilder();
				List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRomaneio(romaneio); 
				if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
					for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
						if(movimentacaoestoque.getDtcancelamento() == null){
							if(!"".equals(whereInMovimentacaoestoque.toString())) whereInMovimentacaoestoque.append(","); 
							whereInMovimentacaoestoque.append(movimentacaoestoque.getCdmovimentacaoestoque());
							
						}
					}
				}
				
				if(!"".equals(whereInMovimentacaoestoque.toString())){
//					if("FALSE".equalsIgnoreCase(paramEstoquenegativo)){
						listaMovimentacaoestoque = movimentacaoestoqueService.findMovimentacoesForQtdedisponivel(whereInMovimentacaoestoque.toString());
						Double qtdeDisponivel;
						for (Movimentacaoestoque movimentacaoestoque2 : listaMovimentacaoestoque) {
							if(!localarmazenagemService.getPermitirestoquenegativo(movimentacaoestoque2.getLocalarmazenagem())){
								if(movimentacaoestoque2.getMovimentacaoestoquetipo() != null && 
										movimentacaoestoque2.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
									if(movimentacaoestoque2.getMaterialclasse() != null && !movimentacaoestoque2.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
										qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( movimentacaoestoque2.getMaterial(), movimentacaoestoque2.getMaterialclasse(), movimentacaoestoque2.getLocalarmazenagem(), movimentacaoestoque2.getEmpresa(), movimentacaoestoque2.getProjeto());
										if(qtdeDisponivel != null && (qtdeDisponivel - movimentacaoestoque2.getQtde()) < 0){
											erro = true;
											qtdeerro++;
											break;
										}
									}
								}
							}
						}
//					}
					if(!erro){
						movimentacaoestoqueService.doUpdateMovimentacoes(whereInMovimentacaoestoque.toString());
						for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
							movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CANCELAR, "Cancelado devido ao cancelamento do romaneio "+ SinedUtil.makeLinkHistorico(romaneio.getCdromaneio().toString(), "visualizarRomaneio"), true);
						}
					}
				}
			}
			
			if(!erro){
				List<Movpatrimonio> listaMovpatrimonio = movpatrimonioService.findByRomaneio(romaneio);
				if(listaMovpatrimonio != null && listaMovpatrimonio.size() > 0){
					for (Movpatrimonio movpatrimonio : listaMovpatrimonio) {
						if(!movpatrimonioService.isLastMov(movpatrimonio)){
							qtdeerropatrimonio++;
							erro = true;
							break;
						}
					}
					
					if(!erro){
						String whereInMovpatrimonio = CollectionsUtil.listAndConcatenate(listaMovpatrimonio, "cdmovpatrimonio", ",");
						movpatrimonioService.updateCancelada(whereInMovpatrimonio);
					}
				}
			}
			
			if(!erro){
				for (Romaneioitem item : romaneio.getListaRomaneioitem()) {
					if(item.getExecutarUpdatePatrimonioContrato() != null && 
							item.getExecutarUpdatePatrimonioContrato() && 
							item.getContratomaterialSubstituto() != null){
						contratomaterialService.updateMaterial(item.getContratomaterialSubstituto(), item.getMaterial());
						if(item.getPatrimonioitem() != null){
							contratomaterialService.updatePatrimonioitem(item.getContratomaterialSubstituto(), item.getPatrimonioitem());
						}
					}
				}
				
				if(romaneio.getListaRomaneioorigem() != null && !romaneio.getListaRomaneioorigem().isEmpty()){
					StringBuilder whereInContrato = new StringBuilder();
					for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
						if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCdcontrato() != null){
							if(!whereInContrato.toString().contains(romaneioorigem.getContrato().getCdcontrato().toString())){
								if(!whereInContrato.toString().equals(""))
									whereInContrato.append(",");
								whereInContrato.append(romaneioorigem.getContrato().getCdcontrato());
							}
						}else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento().getCdcontrato() != null){
							if(!whereInContrato.toString().contains(romaneioorigem.getContratofechamento().getCdcontrato().toString())){
								if(!whereInContrato.toString().equals(""))
									whereInContrato.append(",");
								whereInContrato.append(romaneioorigem.getContratofechamento().getCdcontrato());
							}
						}else if(romaneioorigem.getRequisicaomaterial() != null && romaneioorigem.getRequisicaomaterial() != null){
							whereInRequisicaomaterial.append(romaneioorigem.getRequisicaomaterial().getCdrequisicaomaterial().toString());
						}
					}
					if(whereInContrato != null && !whereInContrato.toString().equals("")){
						String[] ids = whereInContrato.toString().split(",");
						Usuario usuario = (Usuario)NeoWeb.getUser();
						Timestamp dthistorico = new Timestamp(System.currentTimeMillis());
						String observacao;
						if (fromContratoCrud) {
							observacao = "Confirma��o de contrato cancelada: " + 
							"<a href=\"javascript:visualizarRomaneio("+romaneio.getCdromaneio()+")\">"+romaneio.getCdromaneio()+"</a> cancelado";
						} else {
							observacao = "Romaneio " + (Romaneiotipo.ENTRADA.equals(romaneio.getRomaneiotipo()) ? " de Devolu��o": "") + 
							"<a href=\"javascript:visualizarRomaneio("+romaneio.getCdromaneio()+")\">"+romaneio.getCdromaneio()+"</a> cancelado";
						}
								
						for(String idContrato : ids){
							Contratohistorico contratohistorico = new Contratohistorico();
							contratohistorico.setContrato(new Contrato(Integer.parseInt(idContrato)));
							contratohistorico.setDthistorico(dthistorico);
							contratohistorico.setUsuario(usuario);
							contratohistorico.setAcao(Contratoacao.ROMANEIO_CANCELADO);
							contratohistorico.setObservacao(observacao);
							contratohistoricoService.saveOrUpdate(contratohistorico);
						}
						
						List<Contrato> listacontratos = new ArrayList<Contrato>();
						listacontratos = contratoService.findContratosFinalizados(whereInContrato.toString());
						if(listacontratos != null){
							for(Contrato contrato : listacontratos){
								Contratohistorico contratohistorico = new Contratohistorico();
								contratohistorico.setContrato(contrato);
								contratohistorico.setDthistorico(dthistorico);
								contratohistorico.setAcao(Contratoacao.ESTORNADO);
								contratohistorico.setUsuario(usuario);
								contratohistorico.setObservacao("Reaberto devido ao cancelamento do romaneio "+romaneio.getCdromaneio());
						
								contratohistoricoService.saveOrUpdate(contratohistorico);
								contratoService.updateDtfimContrato(contrato);
							}
						}
					}
				}
			}
			
			if(!erro){
				if(!"".equals(whereInRomaneio.toString())) whereInRomaneio.append(","); 
				whereInRomaneio.append(romaneio.getCdromaneio());
			}
		}
		
		if(whereInRomaneio.length() > 0){
			doUpdateSituacaoRomaneio(whereInRomaneio.toString(), Romaneiosituacao.CANCELADA);
			qtdesucesso = whereInRomaneio.toString().split(",").length;
			
			for (Romaneio romaneio : listaRomaneio) {
				for (Romaneioitem romaneioitem : romaneio.getListaRomaneioitem()) {
					if(romaneio.getRomaneiotipo() != null && (Romaneiotipo.ENTRADA.equals(romaneio.getRomaneiotipo()) || Boolean.TRUE.equals(romaneio.getIndenizacao()))){
						updateQtdeLocacaoCancelamentoRomaneio(romaneio, romaneioitem, Contratomateriallocacaotipo.ENTRADA);
					}else if(romaneio.getRomaneiotipo() != null && Romaneiotipo.SAIDA.equals(romaneio.getRomaneiotipo())){
						updateQtdeLocacaoCancelamentoRomaneio(romaneio, romaneioitem, Contratomateriallocacaotipo.SAIDA);
					}
				}
			}
			
			if(whereInRomaneio != null && whereInRomaneio.length() > 0){
				entregaService.verificaRomaneioPendenteAposCancelamento(whereInRomaneio.toString());
			}
			
			if(StringUtils.isNotBlank(whereInRequisicaomaterial.toString())){
				List<Requisicaomaterial> requisicoes2 = requisicaomaterialService.findRequisicoesParaBaixa(whereInRequisicaomaterial.toString());
				List<GerarRomaneioBean> lista = requisicaomaterialService.preparaRomaneioList(request, requisicoes2, whereInRequisicaomaterial.toString());
				if(SinedUtil.isListNotEmpty(lista)){
					for(GerarRomaneioBean item : lista){
						if(item.getQtdeRestante() != null && item.getRequisicaomaterial() != null && item.getRequisicaomaterial().getQtde() != null){
							Situacaorequisicao situacao = Situacaorequisicao.ROMANEIO_GERADO;
							if(item.getQtdeRestante() >= item.getRequisicaomaterial().getQtde()){
								situacao = Situacaorequisicao.AUTORIZADA;
							} 
							
							// ATUALIZA A SITUA��O DA REQUISI��O
							requisicaomaterialService.doUpdateRequisicoes(item.getRequisicaomaterial().getCdrequisicaomaterial().toString(), situacao, true);
						}
					}
				}
			}
		}
		
			
		if(qtdesucesso > 0){
			request.addMessage(qtdesucesso + " romaneio(s) cancelado(s) com sucesso.");
		} else {
			request.addError("Nenhum romaneio foi cancelado.");
		}
		if(qtdeerropatrimonio > 0){
			request.addError(qtdeerropatrimonio + " romaneio(s) n�o cancelado(s). Somente a �ltima movimenta��o do patrim�nio pode ser cancelado.");
		}
		if(qtdeerro > 0){
			request.addError( qtdeerro + " romaneio(s) n�o cancelado(s). O estoque ficar� negativo.");
		}
	}
	
	
	private void updateQtdeLocacaoCancelamentoRomaneio(Romaneio romaneio, Romaneioitem romaneioitem, Contratomateriallocacaotipo contratomateriallocacaotipo) {
		for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
			Contrato contrato = Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacaotipo) ? romaneioorigem.getContratofechamento() : romaneioorigem.getContrato();
			if(contrato != null){
				for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
					if(contratomaterial.getServico() != null && contratomaterial.getServico().equals(romaneioitem.getMaterial())){
						boolean updateExecutado = updateQtdeLocacaoCancelamentoRomaneio(romaneio, romaneioitem, contratomaterial, contratomateriallocacaotipo, true);
						if(!updateExecutado){
							updateQtdeLocacaoCancelamentoRomaneio(romaneio, romaneioitem, contratomaterial, contratomateriallocacaotipo, false);
						}
					}
				}
			}
		}
		
	}
	
	private boolean updateQtdeLocacaoCancelamentoRomaneio(Romaneio romaneio, Romaneioitem romaneioitem, Contratomaterial contratomaterial, Contratomateriallocacaotipo contratomateriallocacaotipo, boolean considerarCdromaneio) {
		for(Contratomateriallocacao contratomateriallocacao : contratomaterial.getListaContratomateriallocacao()){
			if(contratomateriallocacaotipo.equals(contratomateriallocacao.getContratomateriallocacaotipo()) && 
					contratomateriallocacao.getQtde() != null && contratomateriallocacao.getQtde().equals(romaneioitem.getQtde()) &&
					((!considerarCdromaneio && contratomateriallocacao.getCdromaneio() == null) || 
							(considerarCdromaneio && contratomateriallocacao.getCdromaneio() != null && contratomateriallocacao.getCdromaneio().equals(romaneio.getCdromaneio())))){
				contratomateriallocacaoService.updateQtdeLocacao(contratomateriallocacao, 0.0);
				contratomateriallocacao.setQtde(0.0);
				return true;
			}
		}
		return false;
	}
	
	public Romaneioitem getRomaneioitemSubstituido(Romaneioitem romaneioitem,	List<Romaneio> listaRomaneio) {
		if(SinedUtil.isListNotEmpty(listaRomaneio) && romaneioitem != null && romaneioitem.getCdromaneioitem() != null){
			for(Romaneio romaneio : listaRomaneio){
				if(SinedUtil.isListNotEmpty(romaneio.getListaRomaneioitem())){
					for(Romaneioitem ri : romaneio.getListaRomaneioitem()){
						if(ri.getRomaneioitemsubstituido() != null && ri.getRomaneioitemsubstituido().getCdromaneioitem() != null &&
								ri.getRomaneioitemsubstituido().getCdromaneioitem().equals(romaneioitem.getCdromaneioitem())){
							return ri;
						}
					}
				}
			}
		}
		return null;
	}
	
	/**
	* M�todo que cria copia do romaneio
	*
	* @param origem
	* @return
	* @since 17/03/2016
	* @author Luiz Fernando
	*/
	public Romaneio criarCopia(Romaneio origem) {
		origem = loadForEntrada(origem);
		
		Romaneio copia = new Romaneio();
		copia = origem;
		copia.setCdromaneio(null);
		copia.setRomaneiosituacao(Romaneiosituacao.EM_ABERTO);
		copia.setListaMovimentacaoestoqueorigem(null);
		copia.setListaRomaneioorigem(null);
		
		if(SinedUtil.isListNotEmpty(origem.getListaRomaneioitem())){
			for(Romaneioitem item : origem.getListaRomaneioitem()){
				item.setCdromaneioitem(null);
			}
		}
		
		return copia;
	}
	
	public List<Romaneio> findRomaneioForBuscaGeral(String busca) {
		return romaneioDAO.findRomaneioForBuscaGeral(busca);
	}
	
	public List<RomaneioBean> getRomaneioBeanForTemplate(String selectedItens) {
		List<RomaneioBean> romaneioBeanList = new ArrayList<RomaneioBean>();
		RomaneioBean romaneioBean = null;
		RomaneioEmpresaBean romaneioEmpresaBean = null;
		RomaneioClienteBean romaneioClienteBean = null;
		RomaneioEnderecoBean romaneioEnderecoBean = null;
		RomaneioItemBean romaneioItemBean = null;
		RomaneioMaterialBean romaneioMaterialBean = null;
		RomaneioPatrimonioBean romaneioPatrimonioBean = null;
		
		Romaneio romaneio = null;
		
		if(!StringUtils.isEmpty(selectedItens)){
			String[] ids = selectedItens.split(",");
			
			for (String id : ids) {
				romaneioBean = new RomaneioBean();
				
				romaneio = romaneioDAO.findRomaneioById(Integer.parseInt(id));
				
				romaneioBean.setCdRomaneio(romaneio.getCdromaneio());
				romaneioBean.setRomaneioTipo(romaneio.getRomaneiotipo());
				
				if (romaneio.getLocalarmazenagemorigem() != null) {
					romaneioBean.setLocalOrigem(romaneio.getLocalarmazenagemorigem().getNome());
				}
				
				if (romaneio.getLocalarmazenagemdestino() != null) {
					romaneioBean.setLocalDestino(romaneio.getLocalarmazenagemdestino().getNome());
				}
				
				romaneioBean.setDescricao(romaneio.getDescricao());
				romaneioBean.setPlaca(romaneio.getPlaca());
				romaneioBean.setMotorista(romaneio.getMotorista());
				romaneioBean.setSolicitante(romaneio.getSolicitante());
				romaneioBean.setConferente(romaneio.getConferente());
				romaneioBean.setQtdeAjudantes(romaneio.getQtdeajudantes());
				romaneioBean.setDtRomaneio(romaneio.getDtromaneio());
				
				if (romaneio.getEmpresa() != null) {
					romaneioEmpresaBean = new RomaneioEmpresaBean();
					
					romaneioEmpresaBean.setNome(romaneio.getEmpresa().getNome());
					romaneioEmpresaBean.setRazaoSocial(romaneio.getEmpresa().getRazaosocial());
					
					if (romaneio.getEmpresa().getCnpj() != null) {
						romaneioEmpresaBean.setCnpj(romaneio.getEmpresa().getCnpj().toString());
					}
					
					romaneioEmpresaBean.setInscricaoEstadual(romaneio.getEmpresa().getInscricaoestadual());
					romaneioEmpresaBean.setInscricaoMunicipal(romaneio.getEmpresa().getInscricaomunicipal());
					
					romaneioBean.setEmpresaBean(romaneioEmpresaBean);
					
					if (romaneio.getEmpresa().getListaEndereco() != null) {
						for (Endereco endereco : romaneio.getEmpresa().getListaEndereco()) {
							romaneioEnderecoBean = new RomaneioEnderecoBean();
							
							romaneioEnderecoBean.setCep(endereco.getCep().toString());
							if (endereco.getMunicipio() != null) {
								romaneioEnderecoBean.setCidade(endereco.getMunicipio().getNome());
								
								if (endereco.getMunicipio().getUf() != null) {
									romaneioEnderecoBean.setEstado(endereco.getMunicipio().getUf().getSigla());
								}
							}
							
							romaneioEnderecoBean.setEndereco(endereco.getLogradouro());
							
							romaneioBean.getEmpresaBean().getRomaneioEnderecoBeanList().add(romaneioEnderecoBean);
						}
					}
					
					if (romaneio.getEmpresa().getListaTelefone() != null) {
						for (Telefone telefone : romaneio.getEmpresa().getListaTelefone()) {
							romaneioBean.getEmpresaBean().getTelefoneList().add(telefone.getTelefone());
						}
					}
				}
				
				if (romaneio.getCliente() != null) {
					romaneioClienteBean = new RomaneioClienteBean();
					
					romaneioClienteBean.setNome(romaneio.getCliente().getNome());
					romaneioClienteBean.setRazaoSocial(romaneio.getCliente().getRazaosocial());
					
					if (romaneio.getCliente().getCnpj() != null) {
						romaneioClienteBean.setCpfCnpj(romaneio.getCliente().getCnpj().toString());
					} else if (romaneio.getCliente().getCpf() != null) {
						romaneioClienteBean.setCpfCnpj(romaneio.getCliente().getCpf().toString());
					}
					
					romaneioClienteBean.setInscricaoEstadual(romaneio.getCliente().getInscricaoestadual());
					romaneioClienteBean.setInscricaoMunicipal(romaneio.getCliente().getInscricaomunicipal());
					
					romaneioBean.setClienteBean(romaneioClienteBean);
					
					if (romaneio.getCliente().getListaEndereco() != null) {
						for (Endereco endereco : romaneio.getCliente().getListaEndereco()) {
							romaneioEnderecoBean = new RomaneioEnderecoBean();
							
							romaneioEnderecoBean.setCep(endereco.getCep().toString());
							if (endereco.getMunicipio() != null) {
								romaneioEnderecoBean.setCidade(endereco.getMunicipio().getNome());
								
								if (endereco.getMunicipio().getUf() != null) {
									romaneioEnderecoBean.setEstado(endereco.getMunicipio().getUf().getSigla());
								}
							}
							
							romaneioEnderecoBean.setEndereco(endereco.getLogradouro());
							
							romaneioBean.getClienteBean().getRomaneioEnderecoBeanList().add(romaneioEnderecoBean);
						}
					}
					
					if (romaneio.getCliente().getListaTelefone() != null) {
						for (Telefone telefone : romaneio.getCliente().getListaTelefone()) {
							romaneioBean.getClienteBean().getTelefoneList().add(telefone.getTelefone());
						}
					}
				}
				
				if (romaneio.getListaRomaneioitem() != null) {
					for (Romaneioitem romaneioItem : romaneio.getListaRomaneioitem()) {
						romaneioItemBean = new RomaneioItemBean();
						
						if (romaneioItem.getLoteestoque() != null) {
							romaneioItemBean.setNumeroLote(romaneioItem.getLoteestoque().getNumero());
						}
						
						if (romaneioItem.getMaterial() != null) {
							romaneioMaterialBean = new RomaneioMaterialBean();
							
							romaneioMaterialBean.setCdMaterial(romaneioItem.getMaterial().getCdmaterial());
							romaneioMaterialBean.setCodigoBarras(romaneioItem.getMaterial().getCodigobarras());
							romaneioMaterialBean.setQuantidade(romaneioItem.getQtde());
							romaneioMaterialBean.setIdentificador(romaneioItem.getMaterial().getIdentificacao());
							romaneioMaterialBean.setNome(romaneioItem.getMaterial().getNome());
							
							if (romaneioItem.getMaterial().getListaMaterialnumeroserie() != null) {
								for (Materialnumeroserie manterialNumeroSerie : romaneioItem.getMaterial().getListaMaterialnumeroserie()) {
									RomaneioMaterialNumeroSerieBean romaneioMaterialNumeroSerieBean = new RomaneioMaterialNumeroSerieBean();
									romaneioMaterialNumeroSerieBean.setNumeroSerie(manterialNumeroSerie.getNumero());
									romaneioMaterialBean.getMaterialNumeroSerieList().add(romaneioMaterialNumeroSerieBean);
								}
							}
							
							romaneioItemBean.setRomaneioMaterialBean(romaneioMaterialBean);
						}
						
						if (romaneioItem.getPatrimonioitem() != null) {
							romaneioPatrimonioBean = new RomaneioPatrimonioBean();
							
							romaneioPatrimonioBean.setCdPatrimonioItem(romaneioItem.getPatrimonioitem().getCdpatrimonioitem());
							romaneioPatrimonioBean.setPlaqueta(romaneioItem.getPatrimonioitem().getPlaqueta());
							romaneioPatrimonioBean.setQuantidade(romaneioItem.getQtde());
							
							if (romaneioItem.getPatrimonioitem().getMaterialnumeroserie() != null) {
								romaneioPatrimonioBean.setNumeroSerie(romaneioItem.getPatrimonioitem().getMaterialnumeroserie().getNumero());
							}
							
							romaneioItemBean.setRomaneioPatrimonioBean(romaneioPatrimonioBean);
						}
						
						romaneioBean.getRomaneioItemBeanList().add(romaneioItemBean);
					}
				}
				
				romaneioBeanList.add(romaneioBean);
			}
		}
		
		return romaneioBeanList;
	}

	public Resource generateCsvReport(RomaneioFiltro filtro, String nomeArquivo) {
		List<Romaneio> lista = romaneioDAO.findForCsvReport(filtro);
		
		String [] fields = {"dtromaneio", "cdromaneio", "descricao", "empresa.nomefantasia", "localarmazenagemorigem.nome", "localarmazenagemdestino.nome", "romaneiosituacao.descricao"};
		String cabecalho = "\"Data do Romaneio\";\"C�digo\";\"Descri��o\";\"Empresa\";\"Origem\";\"Destino\";\"Situa��o\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
			
		for(Romaneio bean : lista) {
			beanToCSV(fields, csv, bean);
		}
			
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
}