package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoferiado;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;
import br.com.linkcom.sined.geral.bean.view.Vwrelatorioinspecao;
import br.com.linkcom.sined.geral.dao.VeiculousoDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.AnaliseveiculousoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.FichaalocacaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.Veiculousoapoio;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoapoioFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.Veiculousoitemapoio;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AnaliseveiculousoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AnaliseveiculousoitemReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AnaliseveiculousotipoitemReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FichaUsoVeiculo;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FichaUsoVeiculoBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FichaUsoVeiculoResult;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FormularioInspecaoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FormularioInspecaoReportBeanItem;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FichausoveiculoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculousoService extends GenericService<Veiculouso> {

	/* singleton */
	private static VeiculousoService instance;
	public static VeiculousoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VeiculousoService.class);
		}
		return instance;
	}
	
	private VeiculousoDAO veiculousoDAO;
	private VeiculoferiadoService veiculoferiadoService;
	private EscalaService escalaService;
	private VeiculousotipoService veiculousotipoService;
	private ContagerencialService contagerencialService;
	private VeiculoordemservicoService veiculoOrdemServicoService;
	private EscalahorarioService escalahorarioService;
	private VwrelatorioinspecaoService vwrelatorioinspecaoService;
	
	public void setVeiculousoDAO(VeiculousoDAO veiculousoDAO) {
		this.veiculousoDAO = veiculousoDAO;
	}
	public void setVeiculoferiadoService(
			VeiculoferiadoService veiculoferiadoService) {
		this.veiculoferiadoService = veiculoferiadoService;
	}
	public void setEscalaService(EscalaService escalaService) {
		this.escalaService = escalaService;
	}
	public void setVeiculousotipoService(
			VeiculousotipoService veiculousotipoService) {
		this.veiculousotipoService = veiculousotipoService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setVeiculoOrdemServicoService(
			VeiculoordemservicoService veiculoOrdemServicoService) {
		this.veiculoOrdemServicoService = veiculoOrdemServicoService;
	}
	public void setEscalahorarioService(
			EscalahorarioService escalahorarioService) {
		this.escalahorarioService = escalahorarioService;
	}
	public void setVwrelatorioinspecaoService(VwrelatorioinspecaoService vwrelatorioinspecaoService) {
		this.vwrelatorioinspecaoService = vwrelatorioinspecaoService;
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(VeiculousoFiltro filtro) {
		Report report = new Report("/veiculo/veiculouso");
		Report subreport = new Report("/veiculo/veiculouso_subreport");
		report.addSubReport("LISTAVEICULOUSOPARADA_SUBREPORT", subreport);
		
		filtro.setPageSize(Integer.MAX_VALUE);		
		List<Veiculouso> listaVeiculosuso = this.findForReport(filtro);
		if(listaVeiculosuso != null && listaVeiculosuso.size() > 0){
			report.setDataSource(listaVeiculosuso);
		}
		return report;
	}
	
	/**
	 * M�todo que gera relat�rio csv padr�o da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Resource gerarRelatorioCSV(VeiculousoFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);		
		List<Veiculouso> listaVeiculosuso = this.findForReportCSV(filtro);
		
		StringBuilder csv = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		csv.append("\"Ve�culo\";\"Tipo de Uso\";\"Escala hor�rio\";\"Condutor\";\"Local de Destino\";\"Cliente\";\"Solicitante\";\"In�cio do uso\";\"Fim do uso\";")
		   .append("\"Km inicial\";\"Km final\";\"Empresa\";\"Projeto\";\"Centro de Custo\";\"Conta Gerencial\";\n");
		
		if(listaVeiculosuso != null && !listaVeiculosuso.isEmpty()){
			for(Veiculouso veiculouso : listaVeiculosuso){
				csv
				.append("\"" + (veiculouso.getVeiculo() != null ? veiculouso.getVeiculo().getDescriptionProperty() : "")).append("\";")
				.append("\"" + (veiculouso.getVeiculousotipo() != null ? veiculouso.getVeiculousotipo().getDescricao() : "")).append("\";")
				.append("\"" + (veiculouso.getEscalahorario() != null ? veiculouso.getEscalahorario().getDescriptionProperty() : "")).append("\";")
				.append("\"" + (veiculouso.getCondutor() != null ? veiculouso.getCondutor().getNome() : "")).append("\";")
				.append("\"" + (veiculouso.getLocalarmazenagem() != null ? veiculouso.getLocalarmazenagem().getNome() : "")).append("\";")
				.append("\"" + (veiculouso.getCliente() != null ? veiculouso.getCliente().getNome() : "")).append("\";")
				.append("\"" + (veiculouso.getContato() != null ? veiculouso.getContato().getNome() : "")).append("\";")
				.append("\"" + (veiculouso.getDtiniciouso() != null ? format.format(veiculouso.getDtiniciouso()) : "")).append("\";")
				.append("\"" + (veiculouso.getDtiniciouso() != null ? format.format(veiculouso.getDtiniciouso()) : "")).append("\";")
				.append("\"" + (veiculouso.getKminicio() != null ? veiculouso.getKminicio() : "")).append("\";")
				.append("\"" + (veiculouso.getKmfim() != null ? veiculouso.getKmfim() : "")).append("\";")
				.append("\"" + (veiculouso.getEmpresa() != null ? veiculouso.getEmpresa().getRazaosocialOuNome() : "")).append("\";")
				.append("\"" + (veiculouso.getProjeto() != null ? veiculouso.getProjeto() : "")).append("\";")
				.append("\"" + (veiculouso.getCentrocusto() != null ? veiculouso.getCentrocusto().getNome() : "")).append("\";")
				.append("\"" + (veiculouso.getContagerencial() != null ? veiculouso.getContagerencial().getNome() : "")).append("\";");
			
				csv.append("\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "usoveiculo" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#findForReportCSV(VeiculousoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForReportCSV(VeiculousoFiltro filtro) {
		return veiculousoDAO.findForReportCSV(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#findForReport(VeiculousoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForReport(VeiculousoFiltro filtro) {
		return veiculousoDAO.findForReport(filtro);
	}
		/**
	 * Fazer o cancelamento do uso do ve�culo.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void doCancelar(String whereIn) {
		
		if(this.haveVeiculousoCancelado(whereIn)){
			throw new SinedException("Uso(s) de ve�culo j� cancelado(s).");
		}
		if(this.haveVeiculoUsoFinalizado(whereIn)) {
			throw new SinedException("N�o � poss�vel cancelar uma utiliza��o finalizada.");
		}
		
		this.updateSituacao(whereIn, SituacaoVeiculouso.CANCELADO);
	}


	/**
	 * Modifica a situa��o do uso do ve�culo
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#updateSituacaoCancelado
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	private void updateSituacao(String whereIn, SituacaoVeiculouso situacao) {
		if(SituacaoVeiculouso.CANCELADO.equals(situacao)){
			veiculousoDAO.updateSituacaoCancelado(whereIn);
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#haveVeiculousoCancelado
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean haveVeiculousoCancelado(String whereIn) {
		return veiculousoDAO.haveVeiculousoCancelado(whereIn);
	}
	
	/**
	 * M�todo que monta os beans necess�rios para montar o datagrid de aloca��o de ve�culo flex
	 * 
	 * @see #findAlocacaoVeiculoData(Date, Date, VeiculousoapoioFiltro)
	 * @see br.com.linkcom.sined.geral.service.EscalaService#findEscalaDoVeiculo(br.com.linkcom.sined.geral.bean.Veiculo)
	 * @see br.com.linkcom.sined.geral.service.VeiculoferiadoService
	 * @see #montaListaAlocacaoVeiculo(Escala, Date, Date, List, List)
	 * @param veiculousoapoioFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculousoapoio> montaAlocacoVeiculoFlex(VeiculousoapoioFiltro veiculousoapoioFiltro){
		Date dtReferencia = SinedDateUtils.stringToSqlDate(veiculousoapoioFiltro.getDataReferencia());
		Date dtReferenciaLimite = SinedDateUtils.addDiasData(dtReferencia, Veiculousoapoio.QTDE_DIAS_TABELA);
		
		List<Veiculouso> listaAlocacaoVeiculo = findAlocacaoVeiculoData(dtReferencia, dtReferenciaLimite, veiculousoapoioFiltro.getVeiculo());
		Escala escala = escalaService.findEscalaDoVeiculo(veiculousoapoioFiltro.getVeiculo());
		List<Veiculoferiado> listFeriados = new ArrayList<Veiculoferiado>();
		if(escala.getNaotrabalhaferiado())
			listFeriados = veiculoferiadoService.listaIntervaloFeriado(dtReferencia, dtReferenciaLimite);
		
		List<Veiculoordemservico> listAgendamentos = new ArrayList<Veiculoordemservico>();
		listAgendamentos = veiculoOrdemServicoService.listIntervaloAgendamento(dtReferencia, dtReferenciaLimite, veiculousoapoioFiltro.getVeiculo());
		
		
		return montaListaAlocacaoVeiculo(escala, dtReferencia, dtReferenciaLimite, listaAlocacaoVeiculo, listFeriados, listAgendamentos);
	}

	/**
	 * M�todo que monta o bean veiculo uso apoio. 
	 * Veiculousoapoio faz a representa��o de uma linha na tela flex
	 * Veiculousoitemapoio faz a representa��o dos dias na tela flex
	 * 
	 * @see br.com.linkcom.sined.geral.service.VeiculoferiadoService#isFeriado(Date, List)
	 * @param escala
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param listaAlocacaoVeiculo
	 * @param listFeriados
	 * @return
	 * @author Tom�s Rabelo, Fernando Boldrini
	 */
	private List<Veiculousoapoio> montaListaAlocacaoVeiculo(Escala escala, Date dtReferencia, Date dtReferenciaLimite, 
			List<Veiculouso> listaAlocacaoVeiculo, List<Veiculoferiado> listFeriados, List<Veiculoordemservico> listAgendamentos) {
		int dias = SinedDateUtils.diferencaDias(dtReferenciaLimite, dtReferencia);
		List<Veiculousoapoio> list = new ArrayList<Veiculousoapoio>();
		
		Escalahorario escalahorarioAux = null;
		Veiculousoapoio veiculousoapoio = null;
		
		//Colocado esse m�todo devido ao fato de ter mudado a forma de criar escala de hor�rio
		List<Escalahorario> listHorariosDiferentes = escalahorarioService.getHorariosDiferentes(escala);
		
		for (Escalahorario escalahorario : listHorariosDiferentes) {
			boolean novo = true;
			veiculousoapoio = new Veiculousoapoio(escalahorario);
			if(list.contains(veiculousoapoio)){
				veiculousoapoio = findVeiculoUsoApoio(list, escala, escalahorario);
				novo = false;
			}
			
			List<Veiculousoitemapoio> listItemApoio = new ArrayList<Veiculousoitemapoio>();
			veiculousoapoio.setListaveiculousoitemapoio(new ArrayList<Veiculousoitemapoio>());
			for (int i = 0; i < dias; i++) {
				Date data = SinedDateUtils.addDiasData(dtReferencia, i);
				Calendar dataAux = Calendar.getInstance();
				dataAux.setTimeInMillis(data.getTime());
				Veiculouso veiculouso = new Veiculouso();
				
				boolean feriado = false, domingo = false, manutencao = false;
				if(escala.getNaotrabalhaferiado() && listFeriados != null && !listFeriados.isEmpty())
					feriado = veiculoferiadoService.isFeriado(data, listFeriados);
				if(escala.getNaotrabalhadomingo())
					domingo = SinedDateUtils.isDaySunday(data);
				
				for(Veiculoordemservico ordem : listAgendamentos) {
					if(SinedDateUtils.dateToBeginOfDay(ordem.getDtprevista()).getTime() == SinedDateUtils.dateToBeginOfDay(data).getTime()) {
						if(ordem.getOrdemservicotipo().equals(Ordemservicotipo.AGENDAMENTO) || 
								ordem.getOrdemservicotipo().equals(Ordemservicotipo.INSPECAO))
							veiculouso.setIsInspecao(true);
						else manutencao = true;
						break;
					}					
				}
				
				//Colocado esse m�todo devido ao fato de ter mudado a forma de criar escala de hor�rio
				escalahorarioAux = escalahorarioService.achaEscalaDeHorarioCorreta(data, escala, escalahorario);
				
				if(!feriado && !domingo && !veiculouso.getIsInspecao())
					veiculouso = findVeiculoNoDiaHora(data, escalahorarioAux, listaAlocacaoVeiculo);
				
				veiculouso.setIsFeriado(feriado);
				veiculouso.setIsDomingo(domingo);
				veiculouso.setIsManutencao(manutencao);

				verificaSeHorarioDisponivelNoDia(escala, escalahorario, dataAux, veiculouso);
				
				Veiculousoitemapoio veiculousoitemapoio = new Veiculousoitemapoio(data, veiculouso);
				listItemApoio.add(veiculousoitemapoio);
			}
			veiculousoapoio.getListaveiculousoitemapoio().addAll(listItemApoio);
			if(novo)
				list.add(veiculousoapoio);
		}
		
		return list;
	}
	
	/**
	 * Verifica se o hor�rio esta disponivel caso a escala n�o seja igual para todos os dias
	 * 
	 * @param escala
	 * @param escalahorario
	 * @param dataAux
	 * @param veiculouso
	 * @author Tom�s Rabelo
	 */
	private void verificaSeHorarioDisponivelNoDia(Escala escala, Escalahorario escalahorario, Calendar dataAux, Veiculouso veiculouso) {
		if(escala.getHorariotodosdias() != null && !escala.getHorariotodosdias()){
			boolean horarioDisponivel = false;
			for (Escalahorario escalahorarioAux : escala.getListaescalahorario()) {
				if(dataAux.get(Calendar.DAY_OF_WEEK) == escalahorarioAux.getDiasemana().ordinal()+1){
					if(escalahorarioAux.getHorainicio().getTime() == escalahorario.getHorainicio().getTime() && 
					   escalahorarioAux.getHorafim().getTime() == escalahorario.getHorafim().getTime()){
						horarioDisponivel = true;
						break;
					}
				}
			}
			veiculouso.setIsHorarioDisponivel(horarioDisponivel);
		}
	}
	
	/**
	 * Verifica se h� o veiculo uso em alguma celula
	 * 
	 * @param list
	 * @param escala
	 * @param escalahorario
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Veiculousoapoio findVeiculoUsoApoio(List<Veiculousoapoio> list,	Escala escala, Escalahorario escalahorario) {
		if(list != null && !list.isEmpty()){
			for (Veiculousoapoio veiculousoapoioAux : list) {
				Escalahorario escalahorarioAux = veiculousoapoioAux.getEscalahorario();
				if(((escala.getHorariotodosdias() != null && escala.getHorariotodosdias()) || 
				   (escala.getHorariotodosdias() != null && !escala.getHorariotodosdias())) &&
				   escalahorario.getHorainicio().getTime() == escalahorarioAux.getHorainicio().getTime() && escalahorario.getHorafim().getTime() == escalahorarioAux.getHorafim().getTime()){
					return veiculousoapoioAux;
				}
			}
		}
		return new Veiculousoapoio(escalahorario);
	}
	
	/**
	 * Retorna o ve�culo correto de acordo com o dia e o hor�rio
	 * 
	 * @param currentDay
	 * @param escalahorario
	 * @param listaAlocacaoVeiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Veiculouso findVeiculoNoDiaHora(Date currentDay, Escalahorario escalahorario,	List<Veiculouso> listaAlocacaoVeiculo) {
		for (Veiculouso veiculouso : listaAlocacaoVeiculo)
			if((currentDay.getTime() == veiculouso.getDtiniciouso().getTime() || currentDay.getTime() == veiculouso.getDtfimuso().getTime()) || 
			  (currentDay.after(veiculouso.getDtiniciouso()) && currentDay.before(veiculouso.getDtfimuso()))){
				if(veiculouso.getDtiniciouso().getTime() != veiculouso.getDtfimuso().getTime())
					veiculouso.setPossuiUsoVariosDias(true);
				if(veiculouso.getEscalahorario() != null && veiculouso.getEscalahorario().getCdescalahorario() != null){
					if(veiculouso.getEscalahorario().equals(escalahorario))
						return veiculouso;
				}else{
					return veiculouso;
				}
			}
		return new Veiculouso(escalahorario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Veiculouso> findAlocacaoVeiculoData(Date dtReferencia,	Date dtReferenciaLimite, Veiculo veiculo) {
		return veiculousoDAO.findAlocacaoVeiculoData(dtReferencia, dtReferenciaLimite, veiculo);
	}
	
		/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#haveVeiculousoFinalizado(String)
	 *
	 * @param whereIn
	 * @return
	 * @author Fernando Boldrini
	 */
	private boolean haveVeiculoUsoFinalizado(String whereIn) {
		return veiculousoDAO.haveVeiculousoFinalizado(whereIn);
	}
	
	/**
	 * Gera o relat�rio de ficha de uso de ve�culo
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#findForFichaUso
	 *  
	 * @param filtro
	 * @return
	 * @author Thiago Gon�alves, Fernando Boldrini
	 */
	public IReport gerarRelatorioFichaUso(FichausoveiculoFiltro filtro) throws SinedException {
		
		//List<Veiculouso> veiculosuso = veiculousoDAO.findForFichaUso(filtro);
		List<FichaUsoVeiculoResult> results = veiculousoDAO.findForFichaUsoVeiculo(filtro);
		
		List<FichaUsoVeiculo> list = new ArrayList<FichaUsoVeiculo>();
		FichaUsoVeiculo fichaUsoVeiculo = null;
		FichaUsoVeiculoBean fichaBean = null;
		Integer aux_veic = -1;
		for(FichaUsoVeiculoResult result : results) {
			if(!aux_veic.equals(result.getCdveiculo())) {
				aux_veic = result.getCdveiculo();
				
				fichaUsoVeiculo = new FichaUsoVeiculo();
				list.add(fichaUsoVeiculo);
				
				fichaUsoVeiculo.setEmpresa(result.getEmpresa());
				fichaUsoVeiculo.setResponsavel(result.getResponsavel());
				fichaUsoVeiculo.setVeiculo(result.getVeiculo());
			}
			
			fichaBean = new FichaUsoVeiculoBean();
			
			fichaBean.setCliente(result.getCliente());
			
			
			fichaBean.setHoraFim(result.getHoraFinal() == null ? null : new Hora(result.getHoraFinal()));
			fichaBean.setHoraInicio(result.getHoraInicial() == null ? null : new Hora(result.getHoraInicial()));
			
			fichaUsoVeiculo.getListHorarioCliente().add(fichaBean);
		}
		
		for(FichaUsoVeiculo f : list) {
			f.setTotalAulas(f.getListHorarioCliente().size());
		}
		
		results = null;
		
		Report report = null;
		if(list != null && list.size() > 0) {
			report = new Report("veiculo/fichausoveiculo");
			report.addParameter("DTFILTRO", filtro.getData());
			report.setDataSource(list);
			report.addSubReport("SUBFICHAUSOVEICULO", new Report("veiculo/fichaUsoVeiculoSub"));
		}else
			throw new SinedException("Nenhuma ficha de uso foi encontrada.");
		
		return report;
	}
	
	/**
	 * M�todo que reestrutura os registros para realizar as aloca��es de veiculo.
	 * Toda vez que o usu�rio manda salvar, ele busca os registros antigos e faz uma compara��o de qual foi modificado ou criado novo.
	 * Todos que foram modificados e/ou retirados ser�o excluidos. Os modificados s�o gerados novos registros.
	 * 
	 * @see #findAlocacaoVeiculoData(Date, Date, VeiculousoapoioFiltro)
	 * @see br.com.linkcom.sined.geral.service.VeiculousotipoService#veiculoUsoTipoNormal()
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findContaGerencialVeiculo(br.com.linkcom.sined.geral.bean.Veiculo)
	 * @see #saveDeleteVeiculoUso(List, List)
	 * @param lista
	 * @param veiculousoapoioFiltro
	 * @param empresa
	 * @author Tom�s Rabelo
	 */
	public void prepareToSaveAlocacaoVeiculos(List<Veiculousoapoio> lista, VeiculousoapoioFiltro veiculousoapoioFiltro, Empresa empresa){
		Date dtReferencia = SinedDateUtils.stringToSqlDate(veiculousoapoioFiltro.getDataReferencia());
		Date dtReferenciaLimite = SinedDateUtils.addDiasData(dtReferencia, Veiculousoapoio.QTDE_DIAS_TABELA);
		List<Veiculouso> listaAlocacaoVeiculoAntigos = findAlocacaoVeiculoData(dtReferencia, dtReferenciaLimite, veiculousoapoioFiltro.getVeiculo());
		List<Veiculouso> listaVeiculosAtulizados = new ArrayList<Veiculouso>();
		List<Veiculouso> listaVeiculosExcluidos = new ArrayList<Veiculouso>();

		Veiculousotipo veiculousotipoNormal = veiculousotipoService.veiculoUsoTipoNormal();
		Contagerencial contagerencial = contagerencialService.findContaGerencialVeiculo(veiculousoapoioFiltro.getVeiculo());
		
		List<Veiculouso> listaVeiculosNovos = new ArrayList<Veiculouso>();
		for (Veiculousoapoio veiculousoapoio : lista) {
			for (Veiculousoitemapoio veiculousoitemapoio : veiculousoapoio.getListaveiculousoitemapoio()) {
				Veiculouso veiculouso = veiculousoitemapoio.getVeiculouso();
				//Se tiver ID setado so atualiza
				if(veiculouso.getCdveiculouso() == null || veiculouso.getCdveiculouso() == 0){
					veiculouso.setCdveiculouso(null);
					veiculouso.setVeiculo(veiculousoapoioFiltro.getVeiculo());
					veiculouso.setDtiniciouso(veiculousoitemapoio.getData());
					veiculouso.setDtfimuso(veiculousoitemapoio.getData());
//					veiculouso.setEscalahorario(veiculousoapoio.getEscalahorario());
					veiculouso.setEmpresa(empresa);
					veiculouso.setCentrocusto(empresa.getCentrocustoveiculouso());
					veiculouso.setCondutor(veiculousoapoioFiltro.getColaborador());
					veiculouso.setContagerencial(contagerencial);
					if(veiculouso.getCliente() != null && veiculouso.getCliente().getCdpessoa() != null){
						veiculouso.setVeiculousotipo(veiculousotipoNormal);
						listaVeiculosNovos.add(veiculouso);
					}else if(veiculouso.getVeiculousotipo() != null && veiculouso.getVeiculousotipo().getCdveiculousotipo() != null){
						listaVeiculosNovos.add(veiculouso);
					}
				}else{
					listaVeiculosAtulizados.add(new Veiculouso(veiculouso.getCdveiculouso()));
				}
			}
		}
			
		for (Veiculouso antigo : listaAlocacaoVeiculoAntigos) 
			if(!listaVeiculosAtulizados.contains(antigo))
				listaVeiculosExcluidos.add(antigo);
		
		saveDeleteVeiculoUso(listaVeiculosNovos, listaVeiculosExcluidos);
	}
	
	/**
	 * M�todo que salva e/ou deleta os registros de veiculouso. Chamado da tela flex.
	 * 
	 * @param listaVeiculosNovos
	 * @param listaVeiculosExcluidos
	 * @author Tom�s Rabelo
	 */
	private void saveDeleteVeiculoUso(final List<Veiculouso> listaVeiculosNovos, final List<Veiculouso> listaVeiculosExcluidos) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Veiculouso veiculouso : listaVeiculosNovos) 
					saveOrUpdateNoUseTransaction(veiculouso);
				
				for (Veiculouso veiculouso : listaVeiculosExcluidos) 
					delete(veiculouso);
				
				return status;
			}
		});
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculouso
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeVeiculoAlocadoMesmoHorarioData(Veiculouso veiculouso) {
		return veiculousoDAO.existeVeiculoAlocadoMesmoHorarioData(veiculouso);
	}
	
	/**
	 * M�todo que gera relat�rio de ficha de aloca��o
	 * 
	 * @see #findVeiculoUsoForFichaAlocacao(FichaalocacaoFiltro)
	 * @see #remontaListaFichaAlocacao(List, java.util.Date)
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarFichaAlocacaoRelatorio(FichaalocacaoFiltro filtro) {
		Report report = new Report("/veiculo/fichaAlocacao");
		
		List<Veiculouso> veiculosuso = findVeiculoUsoForFichaAlocacao(filtro);
		List<Veiculoferiado> listFeriados = veiculoferiadoService.listaIntervaloFeriado(filtro.getDtinicio(), filtro.getDtfim());
		List<Veiculouso> veiculousoNova = remontaListaFichaAlocacao(veiculosuso, listFeriados);
		if(veiculousoNova != null && veiculousoNova.size() > 0)
			report.setDataSource(veiculousoNova);
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Veiculouso> findVeiculoUsoForFichaAlocacao(FichaalocacaoFiltro filtro) {
		return veiculousoDAO.findVeiculoUsoForFichaAlocacao(filtro);
	}
	
	/**
	 * M�todo que remonta a lista de veiculouso, pois podem haver registros que possuem a data inicio diferente da data fim isso gera novos 
	 * registros. � colocado tb o n� e total de aulas do cliente
	 * 
	 * @see #adicionaTotalAulas(List, int)
	 * @see #validaDomingoFeriadoVeiculoUso(Date, Escala, List)
	 * @see #adicionaVeiculoUsoLista(List, Date, Veiculouso, int)
	 * @param lista
	 * @param listFeriados 
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Veiculouso> remontaListaFichaAlocacao(List<Veiculouso> lista, List<Veiculoferiado> listFeriados) {
		List<Veiculouso> listaNova = new ArrayList<Veiculouso>();
		if(lista != null && lista.size() > 0){
			Cliente clienteAnterior = lista.get(0).getCliente();
			Integer totalAulas = 1;
			for (int i = 0; i < lista.size(); i++) {
				Veiculouso veiculouso = lista.get(i);
				if(veiculouso.getCliente() == null || (veiculouso.getCliente() != clienteAnterior && !veiculouso.getCliente().equals(clienteAnterior))){
					if(clienteAnterior != null)
						adicionaTotalAulas(listaNova, totalAulas-1);
					totalAulas = 1;
					clienteAnterior = veiculouso.getCliente();
				}
				int dias = SinedDateUtils.diferencaDias(veiculouso.getDtfimuso(), veiculouso.getDtiniciouso());
				for (int j = 0; j <= dias; j++) {
					Date data = SinedDateUtils.addDiasData(veiculouso.getDtiniciouso(), j);
					if(validaDomingoFeriadoVeiculoUso(data, veiculouso.getVeiculo().getEscala(), listFeriados))
						totalAulas = adicionaVeiculoUsoLista(listaNova, data, veiculouso, totalAulas);
				}
			}
		}
		
		return listaNova;
	}
	
	/**
	 * Valida se uma determinada data esta em um domingo ou feriado de acordo com a escala ou n�o. Caso seja domingo ou feriado, 
	 * return false, se os dois registros forem falso o registro � valido
	 * 
	 * @param data
	 * @param escala
	 * @param listFeriados
	 * @return
	 * @author Tom�s Rabelo, Fernando Boldrini
	 */
	private boolean validaDomingoFeriadoVeiculoUso(Date data, Escala escala, List<Veiculoferiado> listFeriados) {
		boolean feriado = false, domingo = false;
		if(escala != null) {
			if(escala.getNaotrabalhaferiado() && listFeriados != null && !listFeriados.isEmpty())
				feriado = this.veiculoferiadoService.isFeriado(data, listFeriados);
			if(escala.getNaotrabalhadomingo())
				domingo = SinedDateUtils.isDaySunday(data);
		}else {
			if(listFeriados != null && !listFeriados.isEmpty())
				feriado = this.veiculoferiadoService.isFeriado(data, listFeriados);
			domingo = SinedDateUtils.isDaySunday(data);
		}
		return (!feriado && !domingo) ? true : false;
	}
	
	/**
	 * M�todo que adiciona um novo veiculo na nova lista
	 * 
	 * @param listaNova
	 * @param data
	 * @param veiculouso
	 * @param totalAulas
	 * @author Tom�s Rabelo
	 */
	private Integer adicionaVeiculoUsoLista(List<Veiculouso> listaNova, Date data, Veiculouso veiculouso, Integer totalAulas) {
		Veiculouso veiculouso2 = new Veiculouso(data, veiculouso.getEmpresa(), veiculouso.getVeiculo(), veiculouso.getCondutor(), 
				veiculouso.getEscalahorario(), veiculouso.getCliente(), 
				veiculouso.getCliente() != null ? (totalAulas+++"/") : null);
		listaNova.add(veiculouso2);
		return totalAulas;
	}
	
	/**
	 * Volta na lista para colocar o total das aulas
	 * 
	 * @param listaNova
	 * @param totalAulas
	 * @author Tom�s Rabelo
	 */
	private void adicionaTotalAulas(List<Veiculouso> listaNova, Integer totalAulas) {
		for (int i = listaNova.size()-1; listaNova.size()-totalAulas <= i; i--) 
			listaNova.get(i).setAula(listaNova.get(i).getAula()+totalAulas);
	}
	
	/**
	 * M�todo que gera relat�rio de analise de veiculo de uso
	 *  
	 * @see #findVeiculoUsoForAnalise(AnaliseveiculousoFiltro)
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarAnaliseVeiculoUsoRelatorio(AnaliseveiculousoFiltro filtro) {
		Report report = new Report("/veiculo/analiseVeiculoUso");
		Report subreport1 = new Report("veiculo/analiseVeiculoUsoSub");
		Report subreport2 = new Report("veiculo/analiseVeiculoUsoSubSub");
		report.addSubReport("ANALISEVEICULOUSOSUB", subreport1);
		subreport1.addSubReport("ANALISEVEICULOUSOSUBSUB", subreport2);
		
		List<AnaliseveiculousoReportBean> lista = findVeiculoUsoForAnalise(filtro);
		if(lista != null && lista.size() > 0)
			report.setDataSource(lista);
		return report;
	}
	
	/**
	 * M�todo que faz a contagem para a analise
	 * 
	 * @see br.com.linkcom.sined.geral.service.VeiculoferiadoService#listaIntervaloFeriado(Date, Date)
	 * @see #validaDomingoFeriadoVeiculoUso(Date, Escala, List)
	 * @see #retornaAnaliseveiculouso(Veiculouso, int, List)
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo, Fernando Boldrini
	 */
	private List<AnaliseveiculousoReportBean> findVeiculoUsoForAnalise(AnaliseveiculousoFiltro filtro) {
		List<Veiculouso> lista = null;
		List<Veiculoferiado> listFeriados = null;
		List<AnaliseveiculousoReportBean> listaReport = new ArrayList<AnaliseveiculousoReportBean>();
		try {
			listFeriados = veiculoferiadoService.listaIntervaloFeriado(new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/"+filtro.getAno()).getTime()), 
																	   new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/"+ (filtro.getAno()+1)).getTime()));
			lista = veiculousoDAO.findVeiculoUsoForAnalise(filtro);
		} catch (ParseException e) {
			throw new SinedException("Ano inv�lido");
		}
		
		Calendar dataAux = Calendar.getInstance();
		if(lista != null && lista.size() >0){
			for (Veiculouso veiculouso : lista) {
				dataAux.setTime(veiculouso.getDtiniciouso());
				int anoInicial = dataAux.get(Calendar.YEAR);
				dataAux.setTime(veiculouso.getDtfimuso());
				int anoFinal = dataAux.get(Calendar.YEAR);
				
				int dias = SinedDateUtils.diferencaDias(veiculouso.getDtfimuso(), veiculouso.getDtiniciouso()) + 1;
				int x = 0;
				if(anoInicial != anoFinal) {
					if(anoInicial == filtro.getAno()) {
						dias = this.obterDiasValidos(anoInicial, anoFinal, filtro.getAno(), veiculouso);
					}
					else if(anoFinal == filtro.getAno()) {
						x = dias - this.obterDiasValidos(anoInicial, anoFinal, filtro.getAno(), veiculouso);
					}
				}
				for (int j = x; j < dias; j++) {
					Date data = SinedDateUtils.addDiasData(veiculouso.getDtiniciouso(), j);
					dataAux.setTimeInMillis(data.getTime());
					if(validaDomingoFeriadoVeiculoUso(data, veiculouso.getVeiculo().getEscala(), listFeriados)){
						AnaliseveiculousoitemReportBean item = retornaAnaliseveiculouso(veiculouso, dataAux.get(Calendar.MONTH), listaReport);
						if(!veiculouso.getAux_veiculouso().getSituacao().equals(SituacaoVeiculouso.CANCELADO))
							item.setQtdeRealizada(item.getQtdeRealizada()+1);
						item.setQtdePrevista(item.getQtdePrevista()+1);
					}
				}	
			}
		}
		
		return listaReport;
	}
	/**
	 * Obtem quantos dias pertence ao ano do filtro. No caso em que o uso do ve�culo come�a em um ano e termino ano seguinte.
	 * @param anoInicial
	 * @param anoFinal
	 * @param anoFiltro
	 * @param veiculouso
	 * @return int
	 * @author Fernando Boldrini
	 */
	private int obterDiasValidos(int anoInicial, int anoFinal, int anoFiltro, Veiculouso veiculouso) {
		Calendar aux = Calendar.getInstance();
		
		if(anoFinal == anoFiltro) {
			aux.set(Calendar.DAY_OF_MONTH, 1);
			aux.set(Calendar.MONTH, 0);
			aux.set(Calendar.YEAR, anoFiltro);
			return SinedDateUtils.diferencaDias(veiculouso.getDtfimuso(), new Date(aux.getTimeInMillis())) + 1;
		}else if(anoInicial == anoFiltro) {
			aux.set(Calendar.DAY_OF_MONTH, 31);
			aux.set(Calendar.MONTH, 11);
			aux.set(Calendar.YEAR, anoFiltro);
			return SinedDateUtils.diferencaDias(new Date(aux.getTimeInMillis()), veiculouso.getDtiniciouso()) + 1;
		}
		return 0;
	}
	/**
	 * M�todo recursivo que retorna o atem exato no mes para contagem. Caso n�o encontre ele gera 1 novo
	 * 
	 * @param veiculouso
	 * @param mes
	 * @param listaReport
	 * @return
	 * @author Tom�s Rabelo
	 */
	private AnaliseveiculousoitemReportBean retornaAnaliseveiculouso(Veiculouso veiculouso, int mes,List<AnaliseveiculousoReportBean> listaReport) {
		for (AnaliseveiculousoReportBean bean : listaReport) 
			if(bean.getVeiculo().equals(veiculouso.getVeiculo()) && bean.getCondutor().equals(veiculouso.getCondutor())){
				for (AnaliseveiculousotipoitemReportBean beanTipo : bean.getListaItens()) 
					if(beanTipo.getVeiculousotipo().equals(veiculouso.getVeiculousotipo()))
						for (AnaliseveiculousoitemReportBean item : beanTipo.getListaItens()) 
							if(item.getMes().ordinal() == mes)
								return item;
				bean.getListaItens().add(new AnaliseveiculousotipoitemReportBean(veiculouso.getVeiculousotipo()));
				return retornaAnaliseveiculouso(veiculouso, mes, listaReport);
			}
		
		listaReport.add(new AnaliseveiculousoReportBean(veiculouso.getVeiculo(), veiculouso.getCondutor(), veiculouso.getVeiculousotipo()));
		return retornaAnaliseveiculouso(veiculouso, mes, listaReport);
	}
	/**
	 * M�todo para verificar no banco a existencia de uso de ve�culo na data escolhida para a ordem de servi�o
	 * @param bean
	 * @return boolean
	 * @author Fernando Boldrini
	 */
	public boolean existeVeiculoAlocadoData(Veiculoordemservico bean) {
		return this.veiculousoDAO.existeVeiculoAlocadoData(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @author Tom�s Rabelo
	 */
	public void updateAux() {
		veiculousoDAO.updateAux();
	}
	
	/**
	 * M�todo que cria a lista de inspe��o para o relat�rio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<FormularioInspecaoReportBean> createListForInspecaoByVeiculouso(FormularioinspecaoFiltro filtro){
		List<Vwrelatorioinspecao> listVeiculoParaInspecao = vwrelatorioinspecaoService.findForVeiculoParaInspecao(filtro, "veiculo.cdveiculo");
		
		FormularioInspecaoReportBean beanFormulario = null;
		List<FormularioInspecaoReportBeanItem> listaItem = null;
		List<FormularioInspecaoReportBean> listaFormulario = new ArrayList<FormularioInspecaoReportBean>();
		FormularioInspecaoReportBeanItem beanItem = null;
		
		Integer aux_cdVeiculo = -1;
		
		for(Vwrelatorioinspecao vriBean : listVeiculoParaInspecao) {
			if(vriBean.getVeiculo() != null && !vriBean.getVeiculo().getCdveiculo().equals(aux_cdVeiculo)){ 
				aux_cdVeiculo = vriBean.getVeiculo().getCdveiculo();
			
				beanFormulario = new FormularioInspecaoReportBean();
				listaItem = new ArrayList<FormularioInspecaoReportBeanItem>();	
			
				beanFormulario.setCooperado(vriBean.getNomepessoa() == null ? "" : vriBean.getNomepessoa());
				beanFormulario.setKm(vriBean.getCalculakmatual());
				beanFormulario.setHorimetro(vriBean.getCalculahorimetroatual());
				beanFormulario.setPlaca(vriBean.getPlaca());
				beanFormulario.setPrefixo(vriBean.getPrefixo());
				beanFormulario.setNomeultimoprojetousoveiculo(Util.strings.emptyIfNull(vriBean.getNomeultimoprojetousoveiculo()));
				beanFormulario.setNomeultimocondutorusoveiculo(Util.strings.emptyIfNull(vriBean.getNomeultimocondutorusoveiculo()));
				beanFormulario.setTipocontroleveiculo(vriBean.getVeiculo().getTipocontroleveiculo());
				
				beanFormulario.setListaItem(listaItem);
				listaFormulario.add(beanFormulario);
			}
			if(vriBean.getNomeiteminspecao() != null) {
				beanItem = new FormularioInspecaoReportBeanItem();
				beanItem.setDescricaoitens(vriBean.getNomeiteminspecao());
				beanItem.setDtmanutencao(vriBean.getDtmanutencao());
				beanItem.setKminspecao(vriBean.getKminspecao());
				beanItem.setHorimetroinspecao(vriBean.getHorimetroinspecao());
				beanItem.setTipoitens(vriBean.getNometipoiteminspecao());
				listaItem.add(beanItem);			
			}
		}
		
		return listaFormulario;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#findForInspecao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForInspecao(String whereIn){
		return veiculousoDAO.findForInspecao(whereIn);
	}
	
	/**
	 * M�todo que cria o forml�rio de inspe��o
	 *
	 * @param whereInVeiuculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport createReportFormularioInspecao(String whereInVeiuculouso) {
		Report report= new Report("veiculo/relInspecaoVeicular");
		report.addSubReport("SUBFOMULARIOINSPECAO", new Report("veiculo/relInspecaoVeicularSub"));	 
		
		report.addParameter("VISUAL", false);
		report.addParameter("PREVENTIVA", false);
		report.addParameter("USOVEICULO", true);
		
		List<Veiculouso> listaVeiculouso = this.findForInspecao(whereInVeiuculouso);
		StringBuilder whereInVeiculo = new StringBuilder();
		for(Veiculouso veiculouso : listaVeiculouso){
			if(veiculouso.getVeiculo() != null && veiculouso.getVeiculo().getCdveiculo() != null){
				if(!whereInVeiculo.toString().equals("")) whereInVeiculo.append(",");
				whereInVeiculo.append(veiculouso.getVeiculo().getCdveiculo());
			}
		}
		
		if(whereInVeiculo.toString().equals("")) throw new SinedException("N�o existe itens para emitir inspe��o.");
		
		FormularioinspecaoFiltro filtro = new FormularioinspecaoFiltro();
		filtro.setWhereInVeiculo(whereInVeiculo.toString());
		List<FormularioInspecaoReportBean> listaFormulario = this.createListForInspecaoByVeiculouso(filtro);
		
		if(listaFormulario == null || listaFormulario.isEmpty()) 
			throw new SinedException("N�o existem itens para emitir inspe��o.");
		
		try {
			Image image = null;
			URL arqImagem = new URL(SinedUtil.getUrlWithContext() + "/imagens/veiculo/relatorio/inspecao_visual.gif");
			image = ImageIO.read(arqImagem.openStream());
			report.addParameter("IMAGEMINSPECAOVISUAL", image);
		} catch (Exception e) {}
		
		report.setDataSource(listaFormulario);
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculousoDAO#loadForInspecao(Veiculouso veiculouso)
	 *
	 * @param veiculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public Veiculouso loadForInspecao(Veiculouso veiculouso){
		return veiculousoDAO.loadForInspecao(veiculouso);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.VeiculoordemservicoService#findByVeiculouso(Veiculouso veiculouso)
	 *
	 * @param veiculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculoordemservico> montaOrigem(Veiculouso veiculouso) {
		return veiculoOrdemServicoService.findByVeiculouso(veiculouso);
	}
	
	/**
	 * 
	 * @param veiculo
	 * @author Thiago Clemente
	 * 
	 */
	public Veiculouso getUltimoUso(Veiculo veiculo){
		return veiculousoDAO.getUltimoUso(veiculo);
	}
}