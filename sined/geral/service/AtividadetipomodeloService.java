package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipomodelo;
import br.com.linkcom.sined.geral.dao.AtividadetipomodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AtividadetipomodeloService extends GenericService<Atividadetipomodelo>{
	
	private AtividadetipomodeloDAO atividadetipomodeloDAO;
	
	public void setAtividadetipomodeloDAO(AtividadetipomodeloDAO atividadetipomodeloDAO) {
		this.atividadetipomodeloDAO = atividadetipomodeloDAO;
	}
	
	/**
	 * 
	 * @param atividadetipo
	 * @author Thiago Clemente
	 */
	public List<Atividadetipomodelo> findByAtividadetipo(Atividadetipo atividadetipo){
		return atividadetipomodeloDAO.findByAtividadetipo(atividadetipo);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Atividadetipomodelo> findForEmitirOS(String whereIn){
		return atividadetipomodeloDAO.findForEmitirOS(whereIn);
	}
}
