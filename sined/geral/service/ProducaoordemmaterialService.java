package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;

import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemmaterialService extends GenericService<Producaoordemmaterial> {

	private ProducaoordemmaterialDAO producaoordemmaterialDAO;
	private ProducaoetapaService producaoetapaService;
	
	public void setProducaoordemmaterialDAO(
			ProducaoordemmaterialDAO producaoordemmaterialDAO) {
		this.producaoordemmaterialDAO = producaoordemmaterialDAO;
	}
	public void setProducaoetapaService(
			ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}

	/**
	* M�todo que retorna um whereIn de producaoagendamaterial
	*
	* @param pom
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public String getWhereInProducaoagendamaterialOrigem(Producaoordemmaterial pom) {
		StringBuilder whereIn = new StringBuilder();
		if(pom != null && Hibernate.isInitialized(pom.getListaProducaoordemmaterialorigem()) &&
				SinedUtil.isListNotEmpty(pom.getListaProducaoordemmaterialorigem())){
			for(Producaoordemmaterialorigem producaoordemmaterialorigem : pom.getListaProducaoordemmaterialorigem()){
				if(producaoordemmaterialorigem.getProducaoagendamaterial() != null && producaoordemmaterialorigem.getProducaoagendamaterial().getCdproducaoagendamaterial() != null){
					whereIn.append(producaoordemmaterialorigem.getProducaoagendamaterial().getCdproducaoagendamaterial()).append(",");
				}
			}
		}
		return org.apache.commons.lang.StringUtils.isNotBlank(whereIn.toString()) ? whereIn.substring(0, whereIn.length()-1) : null;
	}
	
	/**
	* M�todo que verifica se existe o item da agenda de produ��o no item da ordem de produ��o
	*
	* @param producaoagendamaterial
	* @param listaProducaoordemmaterialAux
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public boolean existeProducaoagendamaterial(Producaoagendamaterial producaoagendamaterial, List<Producaoordemmaterial> listaProducaoordemmaterialAux) {
		if(producaoagendamaterial != null && producaoagendamaterial.getCdproducaoagendamaterial() != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterialAux)){
			for(Producaoordemmaterial pom : listaProducaoordemmaterialAux){
				if(org.apache.commons.lang.StringUtils.isNotBlank(pom.getWhereInProducaoagendamaterial()) && 
						pom.getWhereInProducaoagendamaterial().contains(producaoagendamaterial.getCdproducaoagendamaterial().toString())){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#adicionaQtdeProduzido(Producaoordemmaterial producaoordemmaterial, Double qtde)
	 *
	 * @param producaoordemmaterial
	 * @param qtde
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public void adicionaQtdeProduzido(Producaoordemmaterial producaoordemmaterial, Double qtde) {
		producaoordemmaterialDAO.adicionaQtdeProduzido(producaoordemmaterial, qtde);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#zerarQtdeProduzido(String whereIn)
	*
	* @param whereIn
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public void zerarQtdeProduzido(String whereIn) {
		producaoordemmaterialDAO.zerarQtdeProduzido(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#zerarQtdePerdaDescarte(String whereIn)
	*
	* @param whereIn
	* @since 04/05/2018
	* @author Luiz Fernando
	*/
	public void zerarQtdePerdaDescarte(String whereIn) {
		producaoordemmaterialDAO.zerarQtdePerdaDescarte(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#findByProducaoordem(Producaoordem producaoordem)
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public List<Producaoordemmaterial> findByProducaoordem(Producaoordem producaoordem) {
		return producaoordemmaterialDAO.findByProducaoordem(producaoordem);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordemmaterial> findAllForEtiquetaProducaoordem(String whereIn, String fromPage){
		return producaoordemmaterialDAO.findAllForEtiquetaProducaoordem(whereIn, fromPage);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordemmaterial> findAllForEtiquetaExpedicaoproducao(String whereIn){
		return producaoordemmaterialDAO.findAllForEtiquetaExpedicaoproducao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param etiqueta
	 * @return
	 * @author Rafael Salvio
	 */
	public Producaoordemmaterial loadByEtiqueta(Integer etiqueta){
		return producaoordemmaterialDAO.loadByEtiqueta(etiqueta);
	}

	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param producaoordemmaterial
	 * @param qtdePerda
	 * @author Rafael Salvio
	 */
	public void adicionaQtdeperdadescarte(Producaoordemmaterial producaoordemmaterial, Double qtdePerda) {
		producaoordemmaterialDAO.adicionaQtdeperdadescarte(producaoordemmaterial, qtdePerda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInPedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public List<Producaoordemmaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		return producaoordemmaterialDAO.findByPedidovendamaterial(whereInPedidovendamaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#buscarUltimaOrdemproducao(Producaoagenda producaoagenda, Producaoetapaitem producaoetapaitem)
	*
	* @param producaoagenda
	* @param producaoetapaitem
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public Producaoordemmaterial buscarUltimaOrdemproducao(Producaoagenda producaoagenda, Producaoetapaitem producaoetapaitem) {
		return producaoordemmaterialDAO.buscarUltimaOrdemproducao(producaoagenda, producaoetapaitem);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#loadForDevolucao(Producaoordemmaterial producaoordemmaterial)
	*
	* @param producaoordemmaterial
	* @return
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	public Producaoordemmaterial loadForDevolucao(Producaoordemmaterial producaoordemmaterial) {
		return producaoordemmaterialDAO.loadForDevolucao(producaoordemmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProducaoordemmaterialService.findByItensInterropidos(String WhereIn)
	 * 
	 * @param WhereIn
	 * @return
	 * @since 12/02/2016
	 * @author C�sar
	 */
	public List<Producaoordemmaterial> findByItensInterrompidos(String WhereIn){
		return producaoordemmaterialDAO.findByItensInterrompidos(WhereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#findByProducaoordemmaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 09/03/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findByProducaoordemmaterial(String whereIn) {
		return producaoordemmaterialDAO.findByProducaoordemmaterial(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#findForDelete(Producaoordem producaoordem, String whereIn)
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findForDelete(Producaoordem producaoordem, String whereIn) {
		return producaoordemmaterialDAO.findForDelete(producaoordem, whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#findByProducaoordem(String whereInProducaoordem, String whereInProducaoordemmaterial)
	*
	* @param whereInProducaoordem
	* @param whereInProducaoordemmaterial
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findByProducaoordem(String whereInProducaoordem, String whereInProducaoordemmaterial) {
		return producaoordemmaterialDAO.findByProducaoordem(whereInProducaoordem, whereInProducaoordemmaterial);
	}

	/**
	* M�todo que verifica se existe o item da agenda de produ��o na lista de itens da ordem de produ��o
	*
	* @param producaoagendamaterial
	* @param listaProducaoordemmaterialAux
	* @return
	* @since 06/04/2016
	* @author Luiz Fernando
	*/
	public boolean existeItem(Producaoagendamaterial producaoagendamaterial, List<Producaoordemmaterial> listaProducaoordemmaterialAux) {
		if(producaoagendamaterial != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterialAux)){
			for(Producaoordemmaterial pom : listaProducaoordemmaterialAux){
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = pom.getListaProducaoordemmaterialorigem();
				boolean producaoagendamaterialMatch = false;
				for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
					producaoagendamaterialMatch = producaoagendamaterial.equals(producaoordemmaterialorigem.getProducaoagendamaterial());
					if(producaoagendamaterialMatch) break;
				}
				if(producaoagendamaterialMatch){ 
					return producaoagendamaterialMatch;
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialDAO#findOrdemCanceladaPedidovenda(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 18/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findOrdemCanceladaPedidovenda(String whereIn) {
		return producaoordemmaterialDAO.findOrdemCanceladaPedidovenda(whereIn);
	}
	
	public void transactionProducaoTrocarProdutoFinal(Producaoordem producaoordem) {
		producaoordemmaterialDAO.transactionProducaoTrocarProdutoFinal(producaoordem);
	}
	
	public void transactionProducaoOperador(Producaoordemmaterial producaoordemmaterial) {
		producaoordemmaterialDAO.transactionProducaoOperador(producaoordemmaterial);
	}
	
	public boolean isUltimaEtapa(Producaoordemmaterial bean){
		Producaoordemmaterial producaoordemmaterial = producaoordemmaterialDAO.load(bean, "producaoetapaitem");
		if(producaoordemmaterial != null && producaoordemmaterial.getProducaoetapaitem() != null){
			return producaoetapaService.isUltimaEtapa(producaoordemmaterial.getProducaoetapaitem());
		}
		
		return false;
	}
	
	public Producaoordemmaterial loadWithProducaoetapaitem(Producaoordemmaterial bean){
		return producaoordemmaterialDAO.loadWithProducaoetapaitem(bean);
	}
}