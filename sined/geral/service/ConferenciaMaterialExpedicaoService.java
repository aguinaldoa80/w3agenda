package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ConferenciaMaterialExpedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.dao.ConferenciaMaterialExpedicaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConferenciaMaterialExpedicaoService extends GenericService<ConferenciaMaterialExpedicao>{

	private ConferenciaMaterialExpedicaoDAO conferenciaMaterialExpedicaoDAO;
	private ExpedicaohistoricoService expedicaohistoricoService;
	
	public void setExpedicaohistoricoService(
			ExpedicaohistoricoService expedicaohistoricoService) {
		this.expedicaohistoricoService = expedicaohistoricoService;
	}
	public void setConferenciaMaterialExpedicaoDAO(ConferenciaMaterialExpedicaoDAO conferenciaMaterialExpedicaoDAO) {
		this.conferenciaMaterialExpedicaoDAO = conferenciaMaterialExpedicaoDAO;
	}
	
	public List<ConferenciaMaterialExpedicao> findByHistorico(Expedicaohistorico historico){
		return conferenciaMaterialExpedicaoDAO.findByHistorico(historico);
	}

	public ConferenciaMaterialExpedicao getLastEmConferenciaByExpedicaoitem(Expedicaoitem expedicaoitem) {
		Expedicaohistorico expedicaohistorico = expedicaohistoricoService.getLastExpedicaohistoricoEmconferencia(expedicaoitem);
		if(expedicaohistorico != null){
			return conferenciaMaterialExpedicaoDAO.loadConferenciaByHistoricoAndItem(expedicaohistorico, expedicaoitem);
		} else return null;
	}

}
