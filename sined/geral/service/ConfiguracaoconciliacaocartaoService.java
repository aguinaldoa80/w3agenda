package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Configuracaoconciliacaocartao;
import br.com.linkcom.sined.geral.dao.ConfiguracaoconciliacaocartaoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoBean.TipoOperadoraCartaoCredito;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaoconciliacaocartaoService extends GenericService<Configuracaoconciliacaocartao>{

	private ConfiguracaoconciliacaocartaoDAO configuracaoconciliacaocartaoDAO;
	
	public void setConfiguracaoconciliacaocartaoDAO(
			ConfiguracaoconciliacaocartaoDAO configuracaoconciliacaocartaoDAO) {
		this.configuracaoconciliacaocartaoDAO = configuracaoconciliacaocartaoDAO;
	}
	
	public Configuracaoconciliacaocartao loadByTipooperadora(TipoOperadoraCartaoCredito tipooperadora){
		return configuracaoconciliacaocartaoDAO.loadByTipooperadora(tipooperadora);
	}
}
