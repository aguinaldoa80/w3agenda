package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import br.com.linkcom.lkutil.csv.CSVWriter;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoCampo;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.BancoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.ContaBancariaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.DocumentoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.GeralVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.MovimentacaoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.PessoaVO;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoPreencherEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoSegmentoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoSegmentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoSegmentoService extends GenericService<BancoConfiguracaoSegmento>{

	private BancoConfiguracaoSegmentoDAO bancoConfiguracaoSegmentoDAO;
	private BancoService bancoService;
	
	public void setBancoConfiguracaoSegmentoDAO(BancoConfiguracaoSegmentoDAO bancoConfiguracaoSegmentoDAO) {
		this.bancoConfiguracaoSegmentoDAO = bancoConfiguracaoSegmentoDAO;
	}
	public void setBancoService(BancoService bancoService) {
		this.bancoService = bancoService;
	}

	public BancoConfiguracaoSegmento criarCopiaSegmento(BancoConfiguracaoSegmento form){
		form = loadForEntrada(form);
		
		if(form.getNome() != null && !form.getNome().isEmpty()){
			int inicioStr = form.getNome().length() - 1;
			int fimStr = form.getNome().length();
			
			String confereNumero = form.getNome().substring(inicioStr, fimStr);
			//Verificando n�mero copia e pulando para pr�ximo n�mero
			if(form.getNome().contains("_") && NumberUtils.isNumber(confereNumero)){
				Integer proxnumero =  Integer.parseInt(confereNumero) + 1;
				form.setNome(form.getNome().replace(confereNumero, proxnumero.toString()));
			}else{
				form.setNome(form.getNome()+"_1");
			}
		}
		
		form.setCdbancoconfiguracaosegmento(null);
		if (form.getListaBancoConfiguracaoSegmentoCampo() != null){
			for (BancoConfiguracaoSegmentoCampo campo : form.getListaBancoConfiguracaoSegmentoCampo()){
				campo.setCdbancoconfiguracaosegmentocampo(null);
			}
		}
		return form;
	}
		
	public void setListaVariaveisDisponiveisOnRequest(WebRequestContext request){
		//Gera a lista de propriedades e m�todos dispon�veis para os campos do segmento
		List<Field> listaBancoField = Arrays.asList(BancoVO.class.getDeclaredFields());
		List<Field> listaContaField = Arrays.asList(ContaBancariaVO.class.getDeclaredFields());
		List<Field> listaEmpresaField = Arrays.asList(EmpresaVO.class.getDeclaredFields());
		List<Field> listaDocumentoField = Arrays.asList(DocumentoVO.class.getDeclaredFields());
		List<Field> listaPessoaField = Arrays.asList(PessoaVO.class.getDeclaredFields());
		List<Field> listaGeralField = Arrays.asList(GeralVO.class.getDeclaredFields());
		List<Field> listaEmpresaDOCField = Arrays.asList(EmpresaVO.class.getDeclaredFields());
		List<Field> listaEmpresaTITULARField = Arrays.asList(EmpresaVO.class.getDeclaredFields());
		List<Method> listaGeralMethod = Arrays.asList(GeralVO.class.getDeclaredMethods());
		List<Field> listaMovimentacaoField = Arrays.asList(MovimentacaoVO.class.getDeclaredFields());
		
		List<String> listaGeralMethodString = new ArrayList<String>();
		DisplayName displayName;
		for (Method m : listaGeralMethod){
			if (m != null && !m.getName().startsWith("get") && !m.getName().startsWith("set")){
				displayName = m.getAnnotation(DisplayName.class); 
				if (displayName.value() != null && !"".equals(displayName.value())){
					listaGeralMethodString.add(displayName.value());
				} else
					listaGeralMethodString.add(m.getName());
			}
		}
		
		Comparator<Field> fieldComparator = new Comparator<Field>() {
			@Override
			public int compare(Field o1, Field o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		
		Collections.sort(listaBancoField, fieldComparator);
		Collections.sort(listaContaField, fieldComparator);
		Collections.sort(listaEmpresaField, fieldComparator);
		Collections.sort(listaDocumentoField, fieldComparator);
		Collections.sort(listaPessoaField, fieldComparator);
		Collections.sort(listaGeralField, fieldComparator);	
		Collections.sort(listaGeralMethodString);
		Collections.sort(listaMovimentacaoField, fieldComparator);	
		
		request.setAttribute("listaBancoField", listaBancoField);
		request.setAttribute("listaContaField", listaContaField);
		request.setAttribute("listaEmpresaField", listaEmpresaField);
		request.setAttribute("listaEmpresaDOCField", listaEmpresaDOCField);
		request.setAttribute("listaEmpresaTITULARField", listaEmpresaTITULARField);
		request.setAttribute("listaDocumentoField", listaDocumentoField);
		request.setAttribute("listaPessoaField", listaPessoaField);
		request.setAttribute("listaGeralField", listaGeralField);
		request.setAttribute("listaGeralMethodString", listaGeralMethodString);
		request.setAttribute("listaMovimentacaoField", listaMovimentacaoField);
	}
	/**
	 * M�todo respons�vel por processar exporta��o do arquivo
	 * @param request
	 * @param filtro
	 * @throws IOException
	 * @author C�sar
	 * @since 07/06/2016
	 */
	public void processaExportar(WebRequestContext request, BancoConfiguracaoSegmentoFiltro filtro, ByteArrayOutputStream conteudoZip) throws IOException{
		List<BancoConfiguracaoSegmento> listaBancoConfiguracaosegmento = new ArrayList<BancoConfiguracaoSegmento>();		
		ZipOutputStream zip = new ZipOutputStream(conteudoZip);
		zip.setLevel(Deflater.BEST_COMPRESSION);
		
		//Recebendo os ids
		String whereIn = filtro.getSelecteditens();
		//Carregando os campos necess�rios para exportar
		listaBancoConfiguracaosegmento =  this.findforWhereIn(whereIn);
		//Preenchendo arquivo para exportar
		if(listaBancoConfiguracaosegmento != null && !listaBancoConfiguracaosegmento.isEmpty()){				
			for (BancoConfiguracaoSegmento segmento : listaBancoConfiguracaosegmento) {
				
				CSVWriter csv = new CSVWriter();
				csv.setDelimiter("@");
							
				//preenchendo dados do segmento
				csv.add(segmento.getNome());
				csv.add(segmento.getBanco().getNumero());
				csv.add(segmento.getTipoSegmento().name());
				csv.add(segmento.getTamanho());
				csv.newLine();
				
				if(segmento.getListaBancoConfiguracaoSegmentoCampo() != null && SinedUtil.isListNotEmpty(segmento.getListaBancoConfiguracaoSegmentoCampo())){
					csv.useQuotes(false);
					for(BancoConfiguracaoSegmentoCampo campos : segmento.getListaBancoConfiguracaoSegmentoCampo()){
						csv.addString("\"");
						csv.add(campos.getPosInicial());
						csv.add(campos.getPosFinal());
						csv.add(campos.getTamanho());
						csv.add(campos.getCampo() != null ? campos.getCampo().replaceAll("\\r\\n", "**") : "");
						csv.add(campos.getDescricao() != null ? campos.getDescricao().replaceAll("\\r\\n", "**") : "");
						csv.add(campos.isCompletarZero());
						csv.add(campos.isCompletarDireita());
						csv.add(campos.getPreencherCom().name());
						csv.add(campos.getFormatoData());
						csv.add(campos.isObrigatorio());
						csv.addString("\"");
						csv.newLine();
					}
					csv.useQuotes(true);
				}
				zip.putNextEntry(new ZipEntry("planilha_bancario_config_segmento_" + segmento.getNome() + segmento.getTipoSegmento().getNome().replace(" ", "_")  + ".csv"));
				zip.write(csv.toString().getBytes());
				zip.closeEntry();
			}
		}
		zip.close();		
	}
	/**
	 * M�todo com refer�ncias no DAO
	 * @param ids
	 * @return
	 * @author C�sar
	 * @since 07/06/2016
	 */
	public List<BancoConfiguracaoSegmento> findforWhereIn(String whereIn){
		return  bancoConfiguracaoSegmentoDAO.findforWhereIn(whereIn);
	}
	/**
	 * M�todo respons�vel pelo recebimento e salvar os arquivos para importa��o
	 * @param arquivos
	 * @return
	 * @author C�sar
	 * @throws IOException 
	 * @since 07/06/2016
	 */
	public String importacaoDados (List<Arquivo> arquivos) throws IOException{
		String mensagemErro = "";
		if(arquivos != null){
			for (Arquivo arquivo : arquivos) {				
				if(arquivo.getArquivo() != null){			
					BancoConfiguracaoSegmento bancoConfiguracaoSegmento = this.processaArquivo(arquivo.getArquivo());								
					if(bancoConfiguracaoSegmento != null)
						bancoConfiguracaoSegmentoDAO.saveOrUpdate(bancoConfiguracaoSegmento);
					else
						mensagemErro += "A planilha " + arquivo.getArquivo().getNome() + " n�o foi importada por inconsist�ncias. <br>";
				}				
			}
		}
		return mensagemErro;
	}	
	/**
	 * M�todo respons�vel por processar os CSV importados
	 * @param arquivo
	 * @param bancoConfiguracaoSegmento
	 * @author C�sar
	 * @throws Exception 
	 * @since 07/06/2016
	 */
	private BancoConfiguracaoSegmento processaArquivo(Arquivo arquivo) throws IOException {
		BufferedReader br = null;
		InputStream csv = new ByteArrayInputStream(arquivo.getContent());
		BancoConfiguracaoSegmento bancoConfiguracaoSegmento = new BancoConfiguracaoSegmento();	
		
		String linha;
		Integer cont=0;
		String[] dados;
		List<BancoConfiguracaoSegmentoCampo> listacampos = new ArrayList<BancoConfiguracaoSegmentoCampo>();
		
		try {
			br = new BufferedReader(new InputStreamReader(csv));
			while ((linha = br.readLine()) != null) {			
				dados = linha.split("@");			
				//Cria��o do Segmento Banc�rio
				if(cont == 0){		
					bancoConfiguracaoSegmento.setNome(dados[0].replace("\"", ""));
					if(!dados[1].replace("\"", "").isEmpty()){
						bancoConfiguracaoSegmento.setBanco(bancoService.findByNumero(Integer.parseInt(dados[1].replace("\"", ""))));
					}
					if(!dados[2].replace("\"", "").isEmpty()){
						bancoConfiguracaoSegmento.setTipoSegmento(BancoConfiguracaoTipoSegmentoEnum.valueOf(dados[2].replace("\"", "")));
					}
					if(!dados[3].replace("\"", "").isEmpty()){
						bancoConfiguracaoSegmento.setTamanho(Integer.parseInt((dados[3].replace("\"", ""))));
					}
				 //Cria��o dos campos Segmento Banc�rio	
				}else{
					BancoConfiguracaoSegmentoCampo bancoDetalhe = new BancoConfiguracaoSegmentoCampo();										
					if(!dados[0].replace("\"", "").isEmpty()){
						bancoDetalhe.setPosInicial(Integer.parseInt(dados[0].replace("\"", "")));
					}
					if(!dados[1].replace("\"", "").isEmpty()){
						bancoDetalhe.setPosFinal(Integer.parseInt(dados[1].replace("\"", "")));					
					}
					if(!dados[2].replace("\"", "").isEmpty()){
						bancoDetalhe.setTamanho(Integer.parseInt(dados[2].replace("\"", "")));
					}
					bancoDetalhe.setCampo(dados[3].replaceAll("\"", "").replaceAll("\\*\\*","\r\n"));
					bancoDetalhe.setDescricao(dados[4].replace("\"", "").replaceAll("\\*\\*","\r\n"));
					if(!dados[5].replace("\"", "").isEmpty()){
						bancoDetalhe.setCompletarZero(Boolean.parseBoolean(dados[5].replace("\"", "")));
					}
					if(!dados[6].replace("\"", "").isEmpty()){
						bancoDetalhe.setCompletarDireita(Boolean.parseBoolean(dados[6].replace("\"", "")));
					}
					if(!dados[7].replace("\"", "").isEmpty()){
						bancoDetalhe.setPreencherCom(BancoConfiguracaoTipoPreencherEnum.valueOf(dados[7].replace("\"", "")));
					}
					bancoDetalhe.setFormatoData(dados[8].replace("\"", ""));
					if(!dados[9].replace("\"", "").isEmpty()){
						bancoDetalhe.setObrigatorio(Boolean.parseBoolean(dados[9].replace("\"", "")));
					}
					listacampos.add(bancoDetalhe);		
				}
				cont ++;
				linha = "";

				//Verifica��o se cont�m nome duplicado e add caracteres para evitar nome duplicados
				if(StringUtils.isNotBlank(bancoConfiguracaoSegmento.getNome())){
					Integer index = bancoConfiguracaoSegmento.getNome().lastIndexOf("_");
					if(index != -1){
						String str = bancoConfiguracaoSegmento.getNome().substring(index);
						if(StringUtils.isNotBlank(str) && str.length() > 1){
							bancoConfiguracaoSegmento.setNome(bancoConfiguracaoSegmento.getNome().substring(0, index));
						}
					}
				}
				List<BancoConfiguracaoSegmento> listaBancoConfiguracaoSegmentoAux = this.validateForImportacao(bancoConfiguracaoSegmento);
				Integer proxNumero = 0;
				if(SinedUtil.isListNotEmpty(listaBancoConfiguracaoSegmentoAux)){
					for(BancoConfiguracaoSegmento bcs : listaBancoConfiguracaoSegmentoAux){
						if(StringUtils.isNotBlank(bcs.getNome())){
							Integer index = bcs.getNome().lastIndexOf("_");
							if(index != -1){
								String str = bcs.getNome().substring(index);
								if(StringUtils.isNotBlank(str) && str.length() > 1){
									try {
										Integer numero = Integer.parseInt(bcs.getNome().substring(index+1)) + 1;
										if(numero > proxNumero){
											proxNumero = numero;
										}
									} catch (Exception e) {}
								}
							}
						}
					}
				}
				bancoConfiguracaoSegmento.setNome(bancoConfiguracaoSegmento.getNome()+ "_" + proxNumero);
			}
		} catch (Exception e) {		
			e.printStackTrace();
			bancoConfiguracaoSegmento = null;
		} finally {
			if(bancoConfiguracaoSegmento != null){
				bancoConfiguracaoSegmento.setListaBancoConfiguracaoSegmentoCampo(listacampos);
			}
			if (br != null) {
				br.close();		
			}
		}
		return bancoConfiguracaoSegmento;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * @param bancoConfiguracaoSegmento
	 * @return
	 * @since 08/07/2016
	 * @author C�sar
	 */
	public Boolean validaBancoConfiguracaSeg (BancoConfiguracaoSegmento bancoConfiguracaoSegmento){
		return bancoConfiguracaoSegmentoDAO.validaBancoConfiguracaSeg(bancoConfiguracaoSegmento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param bancoConfiguracaoSegmento
	 * @return
	 * @since 18/07/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoSegmento> validateForImportacao (BancoConfiguracaoSegmento bancoConfiguracaoSegmento){
		return bancoConfiguracaoSegmentoDAO.validateForImportacao(bancoConfiguracaoSegmento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.BancoConfiguracaoSegmentoDAO#findByBanco(Banco banco)
	*
	* @param banco
	* @return
	* @since 30/01/2017
	* @author Luiz Fernando
	*/
	public List<BancoConfiguracaoSegmento> findByBanco(Banco banco) {
		return bancoConfiguracaoSegmentoDAO.findByBanco(banco);
	}
}
