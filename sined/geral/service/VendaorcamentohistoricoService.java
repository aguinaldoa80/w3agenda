package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.dao.VendaorcamentohistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaorcamentohistoricoService extends GenericService<Vendaorcamentohistorico>{

	private VendaorcamentohistoricoDAO vendaorcamentohistoricoDAO;
	private MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService;
	
	private static VendaorcamentohistoricoService instance;
	public static VendaorcamentohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendaorcamentohistoricoService.class);
		}
		return instance;
	}
	public void setMotivoReprovacaoFaturamentoService(
			MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService) {
		this.motivoReprovacaoFaturamentoService = motivoReprovacaoFaturamentoService;
	}
	
	public void setVendaorcamentohistoricoDAO(VendaorcamentohistoricoDAO vendaorcamentohistoricoDAO) {
		this.vendaorcamentohistoricoDAO = vendaorcamentohistoricoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VendaorcamentoorcamentohistoricoService#findByVendaorcamento(Vendaorcamento vendaorcamento)
	 * 
	 * @param vendaorcamento
	 * @return
	 * @since 16/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentohistorico> findByVendaorcamento(Vendaorcamento vendaorcamento) {
		return vendaorcamentohistoricoDAO.findByVendaorcamento(vendaorcamento);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * 
	 * @param cpfcnpj, tipopessoa
	 * @since 21/05/2012
	 * @author Rafael Salvio
	 */
	public List<Vendaorcamentohistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa){
		return vendaorcamentohistoricoDAO.findByCpfCnpjCliente(cpfcnpj, tipopessoa);
	}

	/**
	 * Cria hist�rico de emiss�o do comprovante do or�amento, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Vendaorcamento
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(Vendaorcamento vendaorcamento) {
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setAcao(Comprovantestatus.EMITIDO.toString());
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setObservacao("Comprovante emitido");
		vendaorcamentohistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
		this.saveOrUpdate(vendaorcamentohistorico);
	}
	
	/**
	 * Cria hist�rico de emiss�o de uma lista de comprovantes de or�amento, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param List<Vendaorcamento>
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(List<Vendaorcamento> listaVendaorcamento) {
		for (Vendaorcamento vendaorcamento: listaVendaorcamento) {
			makeHistoricoComprovanteEmitido(vendaorcamento);
		}
	}
	
	/**
	 * Cria hist�rico de envio de uma lista de comprovantes de or�amento via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param List<Vendaorcamento>
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(List<Vendaorcamento> listaVendaorcamento) {
		for (Vendaorcamento vendaorcamento: listaVendaorcamento) {
			makeHistoricoComprovanteEnviado(vendaorcamento);
		}
	}
	
	/**
	 * Cria hist�rico de envio do comprovante do or�amento via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Vendaorcamento
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(Vendaorcamento vendaorcamento) {
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setAcao("Envio de or�amento");
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setObservacao("Or�amento enviado");
		vendaorcamentohistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
		this.saveOrUpdate(vendaorcamentohistorico);
	}

	public List<MotivoReprovacaoFaturamento> findMotivosReprovacao() {
		return motivoReprovacaoFaturamentoService.findAllAtivos();
	}
}
