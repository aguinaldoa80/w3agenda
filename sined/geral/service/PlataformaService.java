package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Plataforma;
import br.com.linkcom.sined.geral.dao.PlataformaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PlataformaService extends GenericService<Plataforma>{
	private PlataformaDAO plataformaDAO;
	
	public void setPlataformaDAO(PlataformaDAO plataformaDAO) {
		this.plataformaDAO = plataformaDAO;
	}
	
	/**
	 * Retorna um objeto de acordo com o nome informado
	 * @see br.com.linkcom.sined.geral.dao.AmbienteredeDAO#findByNome
	 * @param nome
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Plataforma findByNome(String nome){
		return plataformaDAO.findByNome(nome);
	}
}
