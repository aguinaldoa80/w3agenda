package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Despesaavulsarh;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhhistorico;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhrateio;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaavulsarhacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoDespesaAvulsaRH;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.DespesaavulsarhDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DespesaavulsarhService  extends GenericService<Despesaavulsarh>{

	private static DespesaavulsarhService instance;
	private DespesaavulsarhDAO despesaavulsarhDAO;
	private DespesaavulsarhhistoricoService despesaavulsarhhistoricoService;
	private DocumentoorigemService documentoorigemService;
	private DocumentoService documentoService;
	
	public void setDespesaavulsarhDAO(DespesaavulsarhDAO despesaavulsarhDAO) {
		this.despesaavulsarhDAO = despesaavulsarhDAO;
	}
	
	public void setDespesaavulsarhhistoricoService(DespesaavulsarhhistoricoService despesaavulsarhhistoricoService) {
		this.despesaavulsarhhistoricoService = despesaavulsarhhistoricoService;
	}
	
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}

	public static DespesaavulsarhService getInstance() {
		if (instance == null)
			instance = Neo.getObject(DespesaavulsarhService.class);
		
		return instance;
	}

	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#AUTORIZADA}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param itensSelecionados
	 */
	public void autorizar(String itensSelecionados) {
		List<Despesaavulsarh> list = despesaavulsarhDAO.findByIds(itensSelecionados, false);
		for (Despesaavulsarh item : list){
			if (!item.getSituacao().equals(SituacaoDespesaAvulsaRH.EM_ABERTO)){
				throw new SinedException("Somente despesas com situa��o \"" + 
						SituacaoDespesaAvulsaRH.EM_ABERTO.getNome() + "\" podem ser autorizadas.");
			}
		}

		for (Despesaavulsarh item : list){
			if (item.getSituacao().equals(SituacaoDespesaAvulsaRH.AUTORIZADA))
				continue;
			
			item.setSituacao(SituacaoDespesaAvulsaRH.AUTORIZADA);
			despesaavulsarhDAO.saveOrUpdate(item);
			
			Despesaavulsarhhistorico historico = new Despesaavulsarhhistorico();
			historico.setAcao(Despesaavulsarhacao.AUTORIZADA);
			historico.setDespesaavulsarh(item);
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			
			despesaavulsarhhistoricoService.saveOrUpdate(historico);
		}
	}
	
	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#EM_ABERTO}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param itensSelecionados
	 */
	public void estornar(String itensSelecionados) {
		List<Despesaavulsarh> list = despesaavulsarhDAO.findByIds(itensSelecionados, false);
		for (Despesaavulsarh item : list){
			if (!item.getSituacao().equals(SituacaoDespesaAvulsaRH.AUTORIZADA)){
				throw new SinedException("Somente despesas com situa��o \"" + 
						SituacaoDespesaAvulsaRH.AUTORIZADA.getNome() + "\" podem ser estornadas.");
			}
		}

		for (Despesaavulsarh item : list){
			if (item.getSituacao().equals(SituacaoDespesaAvulsaRH.EM_ABERTO))
				continue;
			
			item.setSituacao(SituacaoDespesaAvulsaRH.EM_ABERTO);
			despesaavulsarhDAO.saveOrUpdate(item);
			
			Despesaavulsarhhistorico historico = new Despesaavulsarhhistorico();
			historico.setAcao(Despesaavulsarhacao.ESTORNADA);
			historico.setDespesaavulsarh(item);
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			
			despesaavulsarhhistoricoService.saveOrUpdate(historico);
		}
	}

	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#CANCELADA}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param list
	 */
	public void cancelar(List<Despesaavulsarh> list) {
		
		for (Despesaavulsarh item : list){
			if (item.getSituacao().equals(SituacaoDespesaAvulsaRH.CANCELADA))
				continue;

			item.setSituacao(SituacaoDespesaAvulsaRH.CANCELADA);
			item.setDtcancelado(new Date(System.currentTimeMillis()));
			despesaavulsarhDAO.saveOrUpdate(item);
			
			
			Despesaavulsarhhistorico historico = new Despesaavulsarhhistorico();
			historico.setAcao(Despesaavulsarhacao.CANCELADA);
			historico.setDespesaavulsarh(item);
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			
			despesaavulsarhhistoricoService.saveOrUpdate(historico);
		}
	}

	/**
	 * Busca todas as {@link Despesaavulsarh} a partir de uma lista de IDs.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param itensSelecionados Uma lista de IDs separados por v�rgula.
	 * @param carregarDependencias 
	 * @return Uma lista de {@link Despesaavulsarh}.
	 */
	public List<Despesaavulsarh> findByIds(String itensSelecionados, boolean carregarDependencias) {
		return despesaavulsarhDAO.findByIds(itensSelecionados, carregarDependencias);
	}

	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#PROCESSADA} e gera as contas a pagar.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 */
	public void processar(Despesaavulsarh despesaavulsarh, Empresa empresa, Documentotipo documentotipo) {

		if (!despesaavulsarh.getSituacao().equals(SituacaoDespesaAvulsaRH.AUTORIZADA))
			throw new SinedException("Apenas despesas avulsas de RH com situa��o \"" + SituacaoDespesaAvulsaRH.AUTORIZADA.getNome() + "\" podem ser processadas.");

		////////////////////////////////////////////////////////////////////////
		//Gerando a conta a pagar
		Documento documento = new Documento();
		
		if(despesaavulsarh.getDtdeposito() == null){
			documento.setDtvencimento(new Date(System.currentTimeMillis()));
		}else {
			documento.setDtvencimento(despesaavulsarh.getDtdeposito());
		}
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setDocumentotipo(documentotipo);
		documento.setDescricao("Despesa avulsa de RH - " + despesaavulsarh.getDespesaavulsarhmotivo().getDescricao());
		documento.setDtemissao(SinedDateUtils.currentDate());
		documento.setValor(despesaavulsarh.getValor());
		documento.setEmpresa(empresa);
		documento.setTipopagamento(Tipopagamento.FORNECEDOR);
		documento.setPessoa(despesaavulsarh.getFornecedor());
		
		documento.setDocumentoacao(Documentoacao.PREVISTA);
				
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		documentohistorico.setObservacao("Criada a partir do processamento da despesa avulsa de RH.");
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class));
		documento.getListaDocumentohistorico().add(documentohistorico);

		documento.setRateio(new Rateio());
		documento.getRateio().setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
		
		for (Despesaavulsarhrateio despesaRateio : despesaavulsarh.getListaDespesaavulsarhrateio()){
			Rateioitem rateioitem = new Rateioitem();
			rateioitem.setContagerencial(despesaRateio.getContagerencial());
			rateioitem.setCentrocusto(despesaRateio.getCentrocusto());
			rateioitem.setProjeto(despesaRateio.getProjeto());
			rateioitem.setPercentual(despesaRateio.getPercentual());
			rateioitem.setValor(despesaRateio.getValor());
			
			documento.getRateio().getListaRateioitem().add(rateioitem);
		}
		
		documentoService.saveOrUpdate(documento);
		documentoService.callProcedureAtualizaDocumento(documento);
				
		Documentoorigem documentoorigem = new Documentoorigem();
		documentoorigem.setDespesaavulsarh(despesaavulsarh);
		documentoorigem.setDocumento(documento);
		
		documentoorigemService.saveOrUpdate(documentoorigem);		
		////////////////////////////////////////////////////////////////////////
		
		despesaavulsarh.setSituacao(SituacaoDespesaAvulsaRH.PROCESSADA);
		despesaavulsarhDAO.saveOrUpdate(despesaavulsarh);

		Despesaavulsarhhistorico historico = new Despesaavulsarhhistorico();
		historico.setAcao(Despesaavulsarhacao.PROCESSADA);
		historico.setDespesaavulsarh(despesaavulsarh);
		historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		historico.setUsuario(SinedUtil.getUsuarioLogado());
		historico.setObservacao("Foi gerada a conta a pagar <a href=\"javascript:visualizarContapagar(" + 
						documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a>");
		
		despesaavulsarhhistoricoService.saveOrUpdate(historico);
	}

	public Resource gerarRelatorioDespesaavulsaCSV(WebRequestContext request, DespesaavulsarhFiltro filtro) {
		List<Despesaavulsarh> listaDespesaavulsarhs = despesaavulsarhDAO.findForCSV(filtro, true);
		if(listaDespesaavulsarhs == null || listaDespesaavulsarhs.isEmpty()){
//			throw new SinedException("Erro relat�rio!");
			request.addError("Nenhum dado para gerar o relat�rio.");
			return null;
		}
			
		
		
		StringBuilder csv = new StringBuilder();

		//CABE�ALHO
		csv.append("FORNECEDOR");
		csv.append(";");
		csv.append("VALOR");
		csv.append(";");
		csv.append("MOTIVO");
		csv.append(";");
		csv.append("SITUA��O");
		csv.append(";");
		csv.append("DATA INSER��O");
		csv.append(";");
		csv.append("DATA DEP�SITO");
		csv.append(";");
		csv.append("CONTA GERENCIAL");
		csv.append(";");
		csv.append("CENTRO DE CUSTO");
		csv.append(";");
		csv.append("PROJETO");
		csv.append(";");
		csv.append("VALOR");
		csv.append(";");
		csv.append("PERCENTUAL");
		csv.append(";");
		csv.append("\n");
		
		//DADOS
		for(Despesaavulsarh darh : listaDespesaavulsarhs){
			if(darh.getFornecedor() != null){
				csv.append(darh.getFornecedor().getNome());
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			if(darh.getValor() != null){
				csv.append(darh.getValor().toString());
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			if(darh.getDespesaavulsarhmotivo() != null){
				csv.append(darh.getDespesaavulsarhmotivo().getDescricao());
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			if(darh.getSituacao() != null){
				csv.append(darh.getSituacao().getNome());
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			if(darh.getDtinsercao() != null){
				csv.append(SinedDateUtils.toString(darh.getDtinsercao()));
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			if(darh.getDtdeposito()!= null){
				csv.append(SinedDateUtils.toString(darh.getDtdeposito()));
				csv.append(";");
			}else{
				csv.append(";");
			}
			
			List<Despesaavulsarhrateio> listaDespesaavulsarhrateio = darh.getListaDespesaavulsarhrateio();
			if(listaDespesaavulsarhrateio != null && !listaDespesaavulsarhrateio.isEmpty()){
				boolean first = true;
				for(Despesaavulsarhrateio darhrateio : listaDespesaavulsarhrateio){
					if(!first){
						csv.append(";;;;;;");	
					}
					if(darhrateio.getContagerencial()!= null){
						csv.append(darhrateio.getContagerencial().getNome());
						csv.append(";");
					}else{
						csv.append(";");
					}
					if(darhrateio.getCentrocusto()!= null){
						csv.append(darhrateio.getCentrocusto().getNome());
						csv.append(";");
					}else{
						csv.append(";");
					}
					if(darhrateio.getProjeto()!= null){
						csv.append(darhrateio.getProjeto().getNome());
						csv.append(";");
					}else{
						csv.append(";");
					}
					if(darhrateio.getValor()!= null){
						csv.append(darhrateio.getValor().toString());
						csv.append(";");
					}else{
						csv.append(";");
					}
					if(darhrateio.getPercentual()!= null){
						csv.append(darhrateio.getPercentual().toString());
						csv.append(";");
					}else{
						csv.append(";");
						
					}
					if(first){
						first = false;
					}
					csv.append("\n");
				}
			}else{
				csv.append("\n");
			}
		}

		Resource resource = new Resource("text/csv", "despesaavulsarh_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
}
