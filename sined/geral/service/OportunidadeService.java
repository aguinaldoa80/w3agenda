package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.IndicadorOportunidadeSituacaoProjetoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.IndicadorOportunidadeSituacaoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.IndicadorOportunidadeVO;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizadorOportunidade;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.OportunidadeDAO;
import br.com.linkcom.sined.geral.dao.OportunidadehistoricoDAO;
import br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeBean;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeFiltro;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorortunidadeBean;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.EmitirfunilvendasFiltro;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OportunidadeService extends GenericService<Oportunidade>{
	
	protected OportunidadeDAO oportunidadeDAO;
	protected OportunidadehistoricoService oportunidadehistoricoService;
	protected OportunidadehistoricoDAO oportunidadehistoricoDAO;
	protected OportunidadesituacaoDAO oportunidadesituacaoDAO;
	protected OportunidadesituacaoService oportunidadesituacaoService;
	protected ContacrmcontatoService contacrmcontatoService;
	protected EmpresaService empresaService;
	protected MaterialService materialService;
	protected UnidademedidaconversaoService unidademedidaconversaoService;
	protected OportunidadematerialService oportunidadematerialService;
	protected MaterialformulamargemcontribuicaoService materialformulamargemcontribuicaoService;
	protected ParametrogeralService parametrogeralService;
	protected PessoaService pessoaService;
	protected ContacrmService contacrmService;
	protected ClienteService clienteService;
	protected VendaorcamentoService vendaorcamentoService;
	protected ContatoService contatoService;
	protected ColaboradorService colaboradorService;
	protected PessoaContatoService pessoaContatoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setOportunidadeDAO(OportunidadeDAO oportunidadeDAO) {
		this.oportunidadeDAO = oportunidadeDAO;
	}
	public void setOportunidadehistoricoService(
			OportunidadehistoricoService oportunidadehistoricoService) {
		this.oportunidadehistoricoService = oportunidadehistoricoService;
	}
	public void setOportunidadehistoricoDAO(
			OportunidadehistoricoDAO oportunidadehistoricoDAO) {
		this.oportunidadehistoricoDAO = oportunidadehistoricoDAO;
	}
	
	public void setOportunidadesituacaoDAO(
			OportunidadesituacaoDAO oportunidadesituacaoDAO) {
		this.oportunidadesituacaoDAO = oportunidadesituacaoDAO;
	}
	
	public List<Oportunidade> findByContacrm(Contacrm contacrm){
		return oportunidadeDAO.findByContacrm(contacrm);
	}
	
	public List<Oportunidade> findByContacrmForCombo(Contacrm contacrm){
		return oportunidadeDAO.findByContacrmForCombo(contacrm);
	}
	
	public void setOportunidadesituacaoService(
			OportunidadesituacaoService oportunidadesituacaoService) {
		this.oportunidadesituacaoService = oportunidadesituacaoService;
	}
	
	public void setContacrmcontatoService(ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}

	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}

	public void setOportunidadematerialService(OportunidadematerialService oportunidadematerialService) {
		this.oportunidadematerialService = oportunidadematerialService;
	}
	
	public void setMaterialformulamargemcontribuicaoService(MaterialformulamargemcontribuicaoService materialformulamargemcontribuicaoService) {
		this.materialformulamargemcontribuicaoService = materialformulamargemcontribuicaoService;
	}

	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {
		this.vendaorcamentoService = vendaorcamentoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}

	/* singleton */
	private static OportunidadeService instance;
	
	public static OportunidadeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(OportunidadeService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que o flex chama para preencher o combo de oportunidade
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2014
	 */
	public List<Oportunidade> findForComboFlex(){
		return oportunidadeDAO.findForComboFlex();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* Atualiza o ciclo de todas as oportunidades
	* Obs: este m�todo � chamado toda vez que � logado um usu�rio
	* 
	* @see	br.com.linkcom.sined.geral.dao.OportunidadeDAO#updateCicloTodasOportunidade(Oportunidade oportunidade, Integer ciclo)
	*
	* @since Sep 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaCicloTodasOportunidade(){
		List<Oportunidade> lista = this.findForAtualizarcicloByOportunidade();
		List<Oportunidade> listaNaosalva = new ArrayList<Oportunidade>();
		if(lista != null && !lista.isEmpty()){
			Date dataAtual = new Date(System.currentTimeMillis());			
			for(Oportunidade oportunidade : lista){
				if(oportunidade.getDtInicio() != null){	
					if(oportunidade.getDtInicio() != null){
						oportunidade.setCiclo(SinedDateUtils.diferencaDias(dataAtual, oportunidade.getDtInicio()));
						listaNaosalva.add(oportunidade);
					}
				}
			}
			
			if(listaNaosalva != null && !listaNaosalva.isEmpty()){
				for(Oportunidade oportunidade : listaNaosalva){
					if(oportunidade.getCiclo() != null)
						oportunidadeDAO.updateCicloTodasOportunidade(oportunidade, oportunidade.getCiclo());
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca as oportunidades que tenham situa��o diferente de cancelada e final, para atualizar o ciclo
	* 
	* @see	br.com.linkcom.sined.geral.dao.OportunidadeDAO#findForAtualizarcicloByOportunidade()
	*
	* @return
	* @since Sep 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Oportunidade> findForAtualizarcicloByOportunidade(){
		return oportunidadeDAO.findForAtualizarcicloByOportunidade();
	}
	
	public void saveOrUpdateListOportunidade(Contacrm contacrm, List<Oportunidade> listaOportunidade){
		if(listaOportunidade == null){
			return;
		}
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser null.");
		}
		
		List<Oportunidade> listaAtual = this.findByContacrm(contacrm);
		List<Oportunidade> listaDelete = new ArrayList<Oportunidade>();
			
		for (Oportunidade op : listaAtual) {
			if(op.getCdoportunidade() != null && !listaOportunidade.contains(op)){
				listaDelete.add(op);
			}
		}
		
		for (Oportunidade op : listaDelete) {
			this.delete(op);
		}
		
		List<Oportunidadehistorico> lista;
		for (Oportunidade op : listaOportunidade) {
			op.setContacrm(contacrm);
			
			lista = null;
			if(op.getListoportunidadehistorico() != null){
				lista = op.getListoportunidadehistorico();
			}
			
			this.saveOrUpdateNoUseTransaction(op);
			
			if(lista != null){
				for(Oportunidadehistorico oportunidadehistorico : lista){
					oportunidadehistorico.setOportunidade(op);
					oportunidadehistoricoService.saveOrUpdateNoUseTransactionWithoutLog(oportunidadehistorico);
				}
			}
		}
	}
	@Override
	public void saveOrUpdate(Oportunidade bean) {
		if(bean.getListoportunidadehistorico() != null && !bean.getListoportunidadehistorico().isEmpty() && bean.getListoportunidadehistorico().size() > 0){
			List<Oportunidadehistorico> novaLista = new ListSet<Oportunidadehistorico>(Oportunidadehistorico.class);
			novaLista = oportunidadehistoricoService.findByOportunidade(bean);
			if(bean.getObservacao() == null || bean.getObservacao().isEmpty() || bean.getObservacao().trim().equals("")){
				novaLista.get(novaLista.size()-1).setProbabilidade(bean.getProbabilidade());
			}
			bean.setListoportunidadehistorico(novaLista);
		}
		super.saveOrUpdate(bean);
	}
	
	public List<Oportunidade> findForReport(OportunidadeFiltro oportunidadeFiltro){
		return oportunidadeDAO.findForReport(oportunidadeFiltro);
	}
	
	public IReport createRelatorioOportunidades(OportunidadeFiltro oportunidadeFiltro){
		Report report = new Report("/crm/oportunidades");
		List<Oportunidade> lista = this.findForReport(oportunidadeFiltro);
		
		for(Oportunidade op : lista){
			if(op.getProbabilidade() != null){
				op.setShowProbabilidade(op.getProbabilidade().toString() + "%"); 
			}
		}
		
		report.setDataSource(lista);
		return report;
		
	}
	
	public List<Oportunidade> loadWithLista(String whereIn, String orderBy, boolean asc){
		return oportunidadeDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Respons�vel por preparar os dados para o relat�rio
	 * @author Taidson Santos
	 * @since 30/03/2010
	 * @param filtro
	 */
	public IReport createRelatorioIndicadorOportunidade(IndicadoroportunidadeFiltro filtro) throws Exception{
		Report report = new Report("/crm/indicadoroportunidade");
		List<IndicadorortunidadeBean> listaIndicadores = montaListaIndicadorOportunidade(filtro);
		report.setDataSource(listaIndicadores);
		return report;
	}
	
	public List<IndicadorortunidadeBean> montaListaIndicadorOportunidade (IndicadoroportunidadeFiltro filtro) throws Exception {
		List<Oportunidadehistorico> historicos = oportunidadehistoricoDAO.historicos(filtro);
//		if(historicos.size() == 0){
//			throw new SinedException("N�o h� dados suficientes para a gera��o do relat�rio.");
//		}
		List<Oportunidadehistorico> novaLista = new ListSet<Oportunidadehistorico>(Oportunidadehistorico.class);
		Oportunidadehistorico itemAnterior = null;
		List<IndicadorortunidadeBean> listaIndicadores = new ListSet<IndicadorortunidadeBean>(IndicadorortunidadeBean.class);
		if(historicos.size() > 0){
			for (Oportunidadehistorico item : historicos) {
				if(itemAnterior == null){
					itemAnterior = item;
					continue;
				}
				
				int mesAnterior = SinedDateUtils.getDateProperty(itemAnterior.getDtaltera(), Calendar.MONTH);
				int mesAtual = SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.MONTH);
				
				int anoAnterior = SinedDateUtils.getDateProperty(itemAnterior.getDtaltera(), Calendar.YEAR);
				int anoAtual = SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.YEAR);
				
				
				if(!item.getOportunidade().equals(itemAnterior.getOportunidade()) || 
				   !item.getOportunidadesituacao().equals(itemAnterior.getOportunidadesituacao()) ||
				   mesAtual > mesAnterior || anoAtual > anoAnterior ){
					
					novaLista.add(itemAnterior);
					itemAnterior = item;
					
				}else{
					itemAnterior = item;
				}
			}
			novaLista.add(historicos.get(historicos.size()-1));
//			for (Oportunidadehistorico item : novaLista) {
//				IndicadorortunidadeBean indicador = new IndicadorortunidadeBean();
				
//				indicador.setCdProjeto(item.getOportunidade().getProjeto().getCdprojeto());
//				indicador.setCdSituacao(item.getOportunidadesituacao().getCdoportunidadesituacao());
//				if (SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.MONTH) <= 8){
//					indicador.setMesAno("0"+
//							(SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.MONTH)+1)+"/"+
//							SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.YEAR)
//					);
//				}else{
//					indicador.setMesAno(
//							SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.MONTH)+1+"/"+
//							SinedDateUtils.getDateProperty(item.getDtaltera(), Calendar.YEAR)
//					);
//				}
////				indicador.setNomeProjeto(item.getOportunidade().getProjeto().getNome());
//				indicador.setNomeSituacao(item.getOportunidadesituacao().getNome());
//				indicador.setCdoportunidade(item.getOportunidade().getCdoportunidade());
//				if(item.getOportunidadesituacao().getCdoportunidadesituacao() == 1){
//					indicador.setProbProspectando(item.getProbabilidade());
//				}else if(item.getOportunidadesituacao().getCdoportunidadesituacao() == 2){
//					indicador.setProbPropostaEnviada(item.getProbabilidade());
//				}else if(item.getOportunidadesituacao().getCdoportunidadesituacao() == 3){
//					indicador.setProbContratoEnviado(item.getProbabilidade());
//				}
//				listaIndicadores.remove(indicador);
//				listaIndicadores.add(indicador);
//			}
		}
		return listaIndicadores;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#atualizaDataRetorno
	 *
	 * @param oportunidade
	 * @author Rodrigo Freitas
	 */
	public void atualizaDataRetorno(Oportunidade oportunidade) {
		oportunidadeDAO.atualizaDataRetorno(oportunidade);
	}
	
	public void atualizaDataRetornoOportunidade(Oportunidade oportunidade){
		oportunidadeDAO.atualizaDataRetornoOportunidade(oportunidade);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#loadForAgendamento
	 * 
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForAgendamento(Oportunidade oportunidade) {
		return oportunidadeDAO.loadForAgendamento(oportunidade);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#loadForContrato
	 * 
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForContrato(Oportunidade oportunidade) {
		return oportunidadeDAO.loadForContrato(oportunidade);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#loadForCliente
	 * 
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForCliente(Oportunidade oportunidade) {
		return oportunidadeDAO.loadForCliente(oportunidade);
	}
	
	public void updateSituacao(Oportunidade oportunidade, Oportunidadesituacao contratoAssinado) {
		oportunidadeDAO.updateSituacao(oportunidade, contratoAssinado);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#updateValortotal(Oportunidade oportunidade, Money valortotal)
	*
	* @param oportunidade
	* @param valortotal
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public void updateValortotal(Oportunidade oportunidade, Money valortotal) {
		oportunidadeDAO.updateValortotal(oportunidade, valortotal);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @param oportunidade
	 * @return
	 * @author Taidson
	 * @since 03/09/2010
	 */
	public Oportunidade loadForInteracao(Oportunidade oportunidade) {
		return oportunidadeDAO.loadForInteracao(oportunidade);
	}
	
	/**
	 * Faz refer�ncia a DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public boolean haveSituacaoCancelada(String whereIn){
		return oportunidadeDAO.haveSituacaoCancelada(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Oportunidade> findOportunidadeForBuscaGeral(String busca){
		return oportunidadeDAO.findOportunidadeForBuscaGeral(busca);
	}
	
	/**
	* M�todo que gera arquivo CSV para Oportunidade com base no filtro da oportunidade
	*
	* @param filtro
	* @return
	* @since Jul 21, 2011
	* @author Luiz Fernando F Silva
	*/
	public Resource prepararArquivoOportunidadeCSV(OportunidadeFiltro filtro) {
		List<Oportunidade> listaOportunidade = this.findForReport(filtro);
		
		for(Oportunidade oportunidade : listaOportunidade){			
				StringBuilder strContato = new StringBuilder("");				
				
			if(oportunidade.getContacrm().getListcontacrmcontato() != null){
				StringBuilder strEmails = new StringBuilder("");				
				
				for(Contacrmcontato contato : oportunidade.getContacrm().getListcontacrmcontato()){
					strContato.append(contato.getNome() + "\n");

					if(contato.getListcontacrmcontatoemail() != null){					
						
						strEmails.append(strEmails + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(), "email", "\n"));
					}
					
					if(contato.getListcontacrmcontatofone() != null){
						StringBuilder strFone = new StringBuilder("");						
						
						for(Contacrmcontatofone fone : contato.getListcontacrmcontatofone()){
							strFone.append( fone.getTelefone() +   
									(fone.getTelefonetipo() != null ? "(" + fone.getTelefonetipo().getNome() + ")" + "\n ": "") );
						}
						
						String stringFone = strFone.toString();
						if(!stringFone.equals("")){							
							stringFone = stringFone.substring(0, (stringFone.length()-1));
							
						}
						
						oportunidade.getContacrm().setListaFones(stringFone);
					}
					
				}
				
				String emails = strEmails.toString();
				if(!emails.equals("")){
					emails = emails.substring(0, (emails.length()-1));
					
				}
				
				oportunidade.getContacrm().setEmailsreport(strEmails.toString());								
				oportunidade.getContacrm().setListacontatos(strContato.toString());
			}
			
		}
		
		StringBuilder csv = new StringBuilder("Id; Nome; Conta; Telefone; Email; In�cio; Respons�vel; Situa��o; Probabilidade \n");		
		
		for(Oportunidade oportunidade : listaOportunidade){
			csv.append(oportunidade.getCdoportunidade());
			csv.append(";");
			csv.append(oportunidade.getNome() != null ? oportunidade.getNome() : "");
			csv.append(";");
			csv.append(oportunidade.getContacrm().getNome() != null ? oportunidade.getContacrm().getNome() : "");
			csv.append(";");
			csv.append(oportunidade.getContacrm().getListaFones() != null ? ("\"" + oportunidade.getContacrm().getListaFones() + "\"") : "");
			csv.append(";");			
			csv.append(oportunidade.getContacrm().getEmailsreport() != null ? "\"" + oportunidade.getContacrm().getEmailsreport() + "\"": "");
			csv.append(";");
			csv.append(oportunidade.getDtInicio() != null ? oportunidade.getDtInicio() : "");
			csv.append(";");
			csv.append(oportunidade.getResponsavel() != null ? oportunidade.getResponsavel().getNome() : "");
			csv.append(";");
			csv.append(oportunidade.getOportunidadesituacao() != null ? oportunidade.getOportunidadesituacao().getNome() : "");
			csv.append(";");
			csv.append(oportunidade.getProbabilidade() != null ? oportunidade.getProbabilidade() : "");
			csv.append("\n");			
			
		}
		Resource resource = new Resource("text/csv", "oportunidade" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public IndicadorOportunidadeVO findForGraficoIndicadorOportunidadeFlex(Date dtPeriodo3) throws Exception {
		
		IndicadorOportunidadeVO indicadorOportunidadeVO = new IndicadorOportunidadeVO();
		List<IndicadorOportunidadeSituacaoVO> listaOportunidadeSituacao = new ArrayList<IndicadorOportunidadeSituacaoVO>();
		List<IndicadorOportunidadeSituacaoProjetoVO> listaOportunidadeSituacaoProjeto = new ArrayList<IndicadorOportunidadeSituacaoProjetoVO>();
		
//		IndicadorOportunidadeSituacaoVO indicadorOportunidadeSituacaoVO;
//		IndicadorOportunidadeSituacaoProjetoVO indicadorOportunidadeSituacaoProjetoVO;
		
		Map<Integer, IndicadorOportunidadeSituacaoVO> mapaOportunidadeSituacao = new LinkedHashMap<Integer, IndicadorOportunidadeSituacaoVO>();
		Map<MultiKey, IndicadorOportunidadeSituacaoProjetoVO> mapaOportunidadeSituacaoProjeto = new LinkedHashMap<MultiKey, IndicadorOportunidadeSituacaoProjetoVO>();
		
//		MultiKey keyOportunidadeSituacaoProjeto;
		
		IndicadoroportunidadeFiltro filtro = new IndicadoroportunidadeFiltro();
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM/yyyy");
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MMM/yy");
		
		// Obt�m as datas referentes a 2 meses anteriores.
		Date dtPeriodo2 = SinedDateUtils.addMesData(dtPeriodo3, -1);
		Date dtPeriodo1 = SinedDateUtils.addMesData(dtPeriodo3, -2);
		
		String[] arrayPeriodo = {simpleDateFormat1.format(dtPeriodo1), simpleDateFormat1.format(dtPeriodo2), simpleDateFormat1.format(dtPeriodo3)};
		String[] arrayPeriodoExt = {simpleDateFormat2.format(dtPeriodo1), simpleDateFormat2.format(dtPeriodo2), simpleDateFormat2.format(dtPeriodo3)};
		
		filtro.setDtPeriodoDe(arrayPeriodo[0]);
		filtro.setDtPeriodoAte(arrayPeriodo[2]);
		
		// Busca os indicadores de oportunidade de acordo com o filtro selecionado.
		List<IndicadorortunidadeBean> listaIndicadorOportunidadeBean = montaListaIndicadorOportunidade(filtro);
		
		if (listaIndicadorOportunidadeBean != null) {
//			for (IndicadorortunidadeBean indicadorOportunidadeBean : listaIndicadorOportunidadeBean) {

//				// Agrupa a lista com a quantidade de situa��es encontradas em cada m�s.
//				indicadorOportunidadeSituacaoVO = mapaOportunidadeSituacao.get(indicadorOportunidadeBean.getCdSituacao());
//				
//				if (indicadorOportunidadeSituacaoVO == null) {
//					indicadorOportunidadeSituacaoVO = new IndicadorOportunidadeSituacaoVO();
//					indicadorOportunidadeSituacaoVO.setCdSituacao(indicadorOportunidadeBean.getCdSituacao());
//					indicadorOportunidadeSituacaoVO.setNomeSituacao(indicadorOportunidadeBean.getNomeSituacao());
//				}
//				
//				if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[0])) {
//					indicadorOportunidadeSituacaoVO.setValorMes1(indicadorOportunidadeSituacaoVO.getValorMes1() + 1);
//				}
//				else if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[1])) {
//					indicadorOportunidadeSituacaoVO.setValorMes2(indicadorOportunidadeSituacaoVO.getValorMes2() + 1);
//				}
//				else if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[2])) {
//					indicadorOportunidadeSituacaoVO.setValorMes3(indicadorOportunidadeSituacaoVO.getValorMes3() + 1);
//				}
//				
//				mapaOportunidadeSituacao.put(indicadorOportunidadeBean.getCdSituacao(), indicadorOportunidadeSituacaoVO);
//				
//				
//				// Agrupa a lista com a quantidade de situa��es encontradas em cada m�s por projeto.
//				keyOportunidadeSituacaoProjeto = new MultiKey(new Object[]{indicadorOportunidadeBean.getCdSituacao(),indicadorOportunidadeBean.getCdProjeto()});
//				
//				indicadorOportunidadeSituacaoProjetoVO = mapaOportunidadeSituacaoProjeto.get(keyOportunidadeSituacaoProjeto);
//				
//				if (indicadorOportunidadeSituacaoProjetoVO == null) {
//					indicadorOportunidadeSituacaoProjetoVO = new IndicadorOportunidadeSituacaoProjetoVO();
//					indicadorOportunidadeSituacaoProjetoVO.setCdSituacao(indicadorOportunidadeBean.getCdSituacao());
//					indicadorOportunidadeSituacaoProjetoVO.setNomeSituacao(indicadorOportunidadeBean.getNomeSituacao());
//					indicadorOportunidadeSituacaoProjetoVO.setCdProjeto(indicadorOportunidadeBean.getCdProjeto());
//					indicadorOportunidadeSituacaoProjetoVO.setNomeProjeto(indicadorOportunidadeBean.getNomeProjeto());
//				}
//				
//				if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[0])) {
//					indicadorOportunidadeSituacaoProjetoVO.setValorMes1(indicadorOportunidadeSituacaoProjetoVO.getValorMes1() + 1);
//				}
//				else if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[1])) {
//					indicadorOportunidadeSituacaoProjetoVO.setValorMes2(indicadorOportunidadeSituacaoProjetoVO.getValorMes2() + 1);
//				}
//				else if (indicadorOportunidadeBean.getMesAno().equals(arrayPeriodo[2])) {
//					indicadorOportunidadeSituacaoProjetoVO.setValorMes3(indicadorOportunidadeSituacaoProjetoVO.getValorMes3() + 1);
//				}
//				
//				mapaOportunidadeSituacaoProjeto.put(keyOportunidadeSituacaoProjeto, indicadorOportunidadeSituacaoProjetoVO);
//			}
			
			Iterator<Integer> itOportunidadeSituacao = mapaOportunidadeSituacao.keySet().iterator();
			while (itOportunidadeSituacao.hasNext()) {
				listaOportunidadeSituacao.add(mapaOportunidadeSituacao.get(itOportunidadeSituacao.next()));
			}
			
			Iterator<MultiKey> itOportunidadeSituacaoProjeto = mapaOportunidadeSituacaoProjeto.keySet().iterator();
			while (itOportunidadeSituacaoProjeto.hasNext()) {
				listaOportunidadeSituacaoProjeto.add(mapaOportunidadeSituacaoProjeto.get(itOportunidadeSituacaoProjeto.next()));
			}
			
			indicadorOportunidadeVO.setDescMes1(arrayPeriodoExt[0]);
			indicadorOportunidadeVO.setDescMes2(arrayPeriodoExt[1]);
			indicadorOportunidadeVO.setDescMes3(arrayPeriodoExt[2]);
			indicadorOportunidadeVO.setListaIndicadorOportunidadeSituacaoVO(listaOportunidadeSituacao);
			indicadorOportunidadeVO.setListaIndicadorOportunidadeSituacaoProjetoVO(listaOportunidadeSituacaoProjeto);
		}
		
		return indicadorOportunidadeVO;
	}
	
	public List<Oportunidade> findForAgenda(Date data) {
		return oportunidadeDAO.findForAgenda(data);
	}
	
	/**
	* M�todo com refer�ncia no DAO 
	* busca os Cd's da oportunidade da listagem
	*
	* @see	br.com.linkcom.sined.geral.dao.OportunidadeDAO#whereInCdoportunidadeListagem(OportunidadeFiltro filtro)
	* 
	* @param filtro
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	public String whereInCdoportunidadeListagem(OportunidadeFiltro filtro){
		return oportunidadeDAO.whereInCdoportunidadeListagem(filtro);
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* calcula os totais por Situa��o da listagem de acordo com o filtro
	* 
	* @see	br.com.linkcom.sined.geral.dao.OportunidadeDAO#calculaTotaisOportunidadesituacaoListagemOportunidade(OportunidadeFiltro filtro, String whereIn)
	*
	* @param filtro
	* @param whereIn
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<TotalizadorOportunidade> calculaTotaisOportunidadesituacaoListagemOportunidade(OportunidadeFiltro filtro, String whereIn){
		return oportunidadeDAO.calculaTotaisOportunidadesituacaoListagemOportunidade(filtro, whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* calcular os totais por Material/Projeto da listagem de acordo com o filtro
	*
	* @see	br.com.linkcom.sined.geral.dao.OportunidadeDAO#calculaTotaisMaterialProjetoListagemOportunidade(OportunidadeFiltro filtro, String whereIn)
	* 
	* @param filtro
	* @param whereIn
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<TotalizadorOportunidade> calculaTotaisMaterialProjetoListagemOportunidade(OportunidadeFiltro filtro, String whereIn){
		return oportunidadeDAO.calculaTotaisMaterialProjetoListagemOportunidade(filtro, whereIn);
	}
	
	/**
	 * M�todo que cria a c�pia da oportunidade
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Oportunidade criarCopia(Oportunidade origem){
		origem = loadForEntrada(origem);
		
		Oportunidade copia = new Oportunidade();
				
		copia.setContacrm(origem.getContacrm());
		copia.setContacrmcontato(origem.getContacrmcontato());
		
		if(origem.getResponsavel() != null && origem.getResponsavel().getCdpessoa() != null){
			copia.setResponsavel(origem.getResponsavel());
			if(origem.getTiporesponsavel() != null && origem.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR)){
				copia.setColaborador(new Colaborador(origem.getResponsavel().getCdpessoa(), origem.getResponsavel().getNome()));
			}else if(origem.getTiporesponsavel() != null && origem.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA)){
				copia.setAgencia(new Fornecedor(origem.getResponsavel().getCdpessoa(), origem.getResponsavel().getNome()));
			}
		}
		
		copia.setDtInicio(new Date(System.currentTimeMillis()));
//		if(origem.getTipomaterialprojeto() != null)		
//			copia.setTipomaterialprojeto(origem.getTipomaterialprojeto());
		
//		copia.setProjeto(origem.getProjeto());
		copia.setValortotal(origem.getValortotal());
		copia.setOportunidadesituacao(oportunidadesituacaoService.findSituacaoinicial());
		copia.setDtPrevisaotermino(origem.getDtPrevisaotermino());
		copia.setOportunidadefonte(origem.getOportunidadefonte());
		copia.setCampanha(origem.getCampanha());

		copia.setListaOportunidadematerial(origem.getListaOportunidadematerial());
		if(copia.getListaOportunidadematerial() != null && !copia.getListaOportunidadematerial().isEmpty()){
			for(Oportunidadematerial oportunidadematerial : copia.getListaOportunidadematerial()){
				oportunidadematerial.setCdoportunidadematerial(null);
			}
		}
		
		return copia;
		
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 13/01/2012
	 * @param bean
	 * @return
	 */
	public Oportunidade carregarOportunidade(Oportunidade bean){
		return oportunidadeDAO.carregarOportunidade(bean);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 16/01/2012
	 * @param bean
	 * @return
	 */
	public Oportunidade carregarOportunidadeCompleta(Oportunidade bean){
		return oportunidadeDAO.carregarOportunidadeCompleta(bean);
	}
	
	/**
	 *	M�todo que busca a Oportunidade do banco e logo depois transforma a Oportunidade em um OportunidadeRTF.
	 *
	 * @param oportunidade
	 * @return
	 * @since 23/01/2012
	 * @author Giovane Freitas
	 */
	@SuppressWarnings("unchecked")
	public OportunidadeBean makeOportunidadeRTF(Oportunidade oportunidade) {
		oportunidade = this.findForEmissaoOportunidadeRTF(oportunidade);
		if(oportunidade.getContacrmcontato() != null && oportunidade.getContacrmcontato().getCdcontacrmcontato() != null){
			Contacrmcontato contacrmcontato = contacrmcontatoService.buscaEmailsTelefones(oportunidade.getContacrmcontato());
    		if(contacrmcontato != null){
    			oportunidade.setContacrmcontato(contacrmcontato);
    		}
		}
		if(oportunidade.getResponsavel() != null && oportunidade.getResponsavel().getCdpessoa() != null){
			oportunidade.setResponsavel(pessoaService.carregaPessoaForContato(oportunidade.getResponsavel()));
		}
		if(oportunidade.getContacrm()!=null){
			Contacrm conta = contacrmService.loadForMakeOportunidadeRTF(oportunidade.getContacrm());
			oportunidade.setCnpj(conta.getCnpj());
			oportunidade.setCpf(conta.getCpf());
			if(conta.getLead()!=null){
				oportunidade.setBairro(conta.getLead().getBairro());
				oportunidade.setMunicipio(conta.getLead().getMunicipio());
				oportunidade.setLogradouro(conta.getLead().getLogradouro());
				oportunidade.setNumero(conta.getLead().getNumero());
				oportunidade.setCep(conta.getLead().getCep());
				oportunidade.setComplementoEndereco(conta.getLead().getComplemento());
			}
		}else if(oportunidade.getCliente()!=null){
			Cliente cliente = clienteService.loadForMakeOportunidadeRTF(oportunidade.getCliente());
			Endereco endereco = new Endereco();
			
			oportunidade.setCpf(cliente.getCpf());
			oportunidade.setCnpj(cliente.getCnpj());
			
			if(cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){
				List<Endereco> listaEndereco = new ArrayList<Endereco>(cliente.getListaEndereco());
				
				Collections.sort(listaEndereco, new Comparator<Endereco>(){
					public int compare(Endereco o1, Endereco o2) {
						try {
							return o1.getEnderecotipo().getCdenderecotipo().compareTo(o2.getEnderecotipo().getCdenderecotipo());
						} catch (Exception e) {
							return 0;
						}
					}					
				});
				
				if(!Enderecotipo.INATIVO.equals(listaEndereco.get(0).getEnderecotipo()))
					endereco = listaEndereco.get(0);
			}
			
			oportunidade.setBairro(endereco.getBairro());
			oportunidade.setMunicipio(endereco.getMunicipio());
			oportunidade.setLogradouro(endereco.getLogradouro());
			oportunidade.setNumero(endereco.getNumero());
			oportunidade.setCep(endereco.getCep());
			oportunidade.setComplementoEndereco(endereco.getComplemento());
		}
		
		if(oportunidade.getResponsavel() != null){
			Pessoa responsavel = pessoaService.loadWithTelefone(oportunidade.getResponsavel());
			if(responsavel != null){
				oportunidade.getResponsavel().setListaTelefone(responsavel.getListaTelefone());
			}
		}
		
		
		List<Oportunidadematerial> listaOportunidadematerial = oportunidadematerialService.findByOportunidade(oportunidade);
		if(listaOportunidadematerial != null){
			oportunidade.setListaOportunidadematerial(SinedUtil.listToSet(listaOportunidadematerial, Oportunidadematerial.class));
		}
		
		if(oportunidade.getEmpresa() != null){
			Empresa empresa = empresaService.loadForTemplate(oportunidade.getEmpresa());
			if(empresa != null){
				oportunidade.setEmpresa(empresa);
			}
		}
		return oportunidade.getOportunidadeRTF();
	}
	
	/**
	 * M�todo que retorna uma Oportunidade para a emiss�o do relatorio, baseado no modelo RTF
	 * 
 	 * @author Rodrigo Freitas
	**/
	public Oportunidade findForEmissaoOportunidadeRTF(Oportunidade oportunidade) {
		return oportunidadeDAO.findForEmissaoOportunidadeRTF(oportunidade);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeRetornoAtrasado() {
		return oportunidadeDAO.getQtdeRetornoAtrasado();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeRetornoHoje() {
		return oportunidadeDAO.getQtdeRetornoHoje();
	}
	
	/**
	 * Gera o n�mero sequencial do identificador da oportunidade
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 06/09/2013
	 */
	public void gerarNumeroSequencial(Oportunidade bean) {
		if(bean.getEmpresa() != null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.proximonumoportunidade");
			Integer proxNum = 1;
			if(empresa.getProximonumoportunidade() != null){ 
				proxNum = empresa.getProximonumoportunidade();
			}
			bean.setIdentificador(proxNum);
			
			proxNum++;
			empresaService.updateProximoNumOportunidade(empresa, proxNum);
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#loadWithPropostacaixa(Oportunidade oportunidade)
	 *
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/09/2013
	 */
	public Oportunidade loadWithPropostacaixa(Oportunidade oportunidade) {
		return oportunidadeDAO.loadWithPropostacaixa(oportunidade);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#isClienteDiferenteOportunidade(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public Boolean isClienteDiferenteOportunidade(String whereIn) {
		return oportunidadeDAO.isClienteDiferenteOportunidade(whereIn);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OportunidadeDAO#findForVenda(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public List<Oportunidade> findForVenda(String whereIn) {
		return oportunidadeDAO.findForVenda(whereIn);
	}
	
	/**
	 * M�todo que atualiza a situa��o da oportunidade ap�s a cria��o da venda
	 *
	 * @param venda
	 * @param oportunidade
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public void updateOportunidadeAposVenda(Venda venda, Oportunidade oportunidade){
		Oportunidadesituacao oportunidadesituacao = oportunidadesituacaoService.findSituacaofinal(); 
		this.updateSituacao(oportunidade, oportunidadesituacao);
		
		Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
		
		oportunidadehistorico.setOportunidade(oportunidade);
		oportunidadehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		oportunidadehistorico.setOportunidadesituacao(oportunidadesituacao);
		oportunidadehistorico.setObservacao("Gera��o de Venda: <a href=\"javascript:visualizarVenda(" + 
						venda.getCdvenda() + 
						");\">" + 
						venda.getCdvenda() + 
						"</a>");
		
		oportunidadehistoricoService.saveOrUpdate(oportunidadehistorico);		
	}
	
	/**
	* M�todo que atualiza a situa��o da oportunidade e gera o hist�rico referente ao or�amento
	*
	* @param venda
	* @param oportunidade
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public void updateOportunidadeAposOrcamento(Vendaorcamento venda, Oportunidade oportunidade){
		Oportunidadesituacao oportunidadesituacao = oportunidadesituacaoService.findSituacaoantesfinal();
		if(oportunidadesituacao != null){
			this.updateSituacao(oportunidade, oportunidadesituacao);
		}
		
		Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
		oportunidadehistorico.setOportunidade(oportunidade);
		oportunidadehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		oportunidadehistorico.setOportunidadesituacao(oportunidadesituacao);
		oportunidadehistorico.setObservacao("<u>Gera��o de Or�amento: <a href=\"javascript:visualizarOrcamento(" + venda.getCdvendaorcamento() + ");\";>" + venda.getCdvendaorcamento() + "</a></u>");
		oportunidadehistoricoService.saveOrUpdate(oportunidadehistorico);		
	}
	
	/**
	 * M�todo que cria atributo para o combo de unidade de medida no detalhe
	 *
	 * @param request
	 * @param oportunidade
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public void criaMapaAtributoUnidademedida(WebRequestContext request, Oportunidade oportunidade) {
		Set<Material> materiais = new HashSet<Material>();
		Map<Material, List<Unidademedida>> mapaUnidades = new HashMap<Material, List<Unidademedida>>();
		if (oportunidade != null && oportunidade.getListaOportunidadematerial() != null && !oportunidade.getListaOportunidadematerial().isEmpty()) {
			for (Oportunidadematerial oportunidadematerial : oportunidade.getListaOportunidadematerial()) {
				materiais.add(oportunidadematerial.getMaterial());
			}
		}
		for (Material material : materiais) {
			material = materialService.carregaMaterial(material);
			mapaUnidades.put(material, unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material));
		}
		request.setAttribute("mapaUnidades", mapaUnidades);
	}
	
	public ModelAndView getInfMaterialUnidademedida(WebRequestContext request, Oportunidadematerial bean) {
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Material material = new Material();
		
		material = materialService.unidadeMedidaMaterial(bean.getMaterial());
		listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material);
		
		return new JsonModelAndView()
									 .addObject("unidadeMedidaSelecionada", material.getUnidademedida())
									 .addObject("listaUnidademedida", listaUnidademedida);
	}
	
	/**
	 * M�todo que cria o Hist�rio ao emitir proposta/contrato da oportunidade
	 * @param oportunidade
	 * @author Luiz Fernando
	 * @since 23/10/2013
	 */
	public void createHistoricoEmissaoPropostaContrato(Oportunidade oportunidade) {
		Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
		oportunidadehistorico.setOportunidade(oportunidade);
		oportunidadehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		oportunidadehistorico.setObservacao("Emiss�o de Proposta/Contrato");
		
		oportunidadehistoricoDAO.saveOrUpdate(oportunidadehistorico);
		
	}
	
	public void createRevisaoOrcamento(Orcamento orcamento){
		Oportunidade oportunidade = orcamento.getOportunidade();
		
		if(oportunidade != null && oportunidade.getCdoportunidade() != null){
			oportunidade = this.load(oportunidade, "oportunidade.cdoportunidade, oportunidade.oportunidadesituacao");
			if(oportunidade != null){
				Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
				oportunidadehistorico.setOportunidade(oportunidade);
				oportunidadehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				oportunidadehistorico.setObservacao(orcamento.getObservacao() != null && !"".equals(orcamento.getObservacao().trim()) ? orcamento.getObservacao() : "Revis�o de or�amento");
				oportunidadehistorico.setOportunidadesituacao(oportunidade.getOportunidadesituacao());
				
				oportunidadehistoricoDAO.saveOrUpdate(oportunidadehistorico);
			}
		}
	}
	
	public Resource gerarExcelEmitirfunilvendas (EmitirfunilvendasFiltro emitirfunilvendasFiltro) throws IOException{
		List<Oportunidade> oportunidades = oportunidadeDAO.findForEmitirfunilvendas(emitirfunilvendasFiltro);
		
		List<Oportunidade> oportunidadesProbMaiorIgualDez = new ArrayList<Oportunidade>();
		List<Oportunidade> oportunidadesProbMaiorIgualVinteCinco = new ArrayList<Oportunidade>();
		List<Oportunidade> oportunidadesProbMaiorIgualCinquenta = new ArrayList<Oportunidade>();
		List<Oportunidade> oportunidadesProbMaiorIgualSetentaCinco = new ArrayList<Oportunidade>();
		List<Oportunidade> oportunidadesProbMaiorIgualNoventa = new ArrayList<Oportunidade>();
		List<Oportunidade> oportunidadesProbIgualCem = new ArrayList<Oportunidade>();
		
		int numeroLinhas = 0;
		
		List<Integer> linhasValorAcumulado = new ArrayList<Integer>();
		
		for (Oportunidade oportunidade : oportunidades) {
			if (oportunidade.getProbabilidade() != null) {
				if (oportunidade.getProbabilidade() >= 10 && oportunidade.getProbabilidade() < 25) {
					oportunidadesProbMaiorIgualDez.add(oportunidade);
				} else if(oportunidade.getProbabilidade() >= 25 && oportunidade.getProbabilidade() < 50){
					oportunidadesProbMaiorIgualVinteCinco.add(oportunidade);
				} else if(oportunidade.getProbabilidade() >= 50 && oportunidade.getProbabilidade() < 75){
					oportunidadesProbMaiorIgualCinquenta.add(oportunidade);
				} else if(oportunidade.getProbabilidade() >= 75 && oportunidade.getProbabilidade() < 90){
					oportunidadesProbMaiorIgualSetentaCinco.add(oportunidade);
				} else if(oportunidade.getProbabilidade() >= 90 && oportunidade.getProbabilidade() < 100){
					oportunidadesProbMaiorIgualNoventa.add(oportunidade);
				} else if(oportunidade.getProbabilidade() == 100){
					oportunidadesProbIgualCem.add(oportunidade);
				}
			}
		}
		
		SinedExcel wb = new SinedExcel();
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 30);
        
		HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 0, (short) 4));
		headerRow.setHeight((short)1000);
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Funil de Vendas");
		
		cellHeaderTitle = headerRow.createCell((short) 1);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle = headerRow.createCell((short) 2);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle = headerRow.createCell((short) 3);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle = headerRow.createCell((short) 4);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		
		numeroLinhas++;
		
        HSSFRow row = null;
		HSSFCell cell = null;
		
		numeroLinhas++;
		
		Material material = emitirfunilvendasFiltro.getMaterial();
		if(material != null){
			material = materialService.load(material,"material.cdmaterial, material.nome");
			
			row = planilha.createRow((short) numeroLinhas);
			numeroLinhas ++;
			
			cell = row.createCell((short)0);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue("Material/Servi�o");
			
			planilha.addMergedRegion(new Region(numeroLinhas -1, (short) 1, numeroLinhas -1, (short) 4));
			cell = row.createCell((short)1);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue(material.getNome());
			
			cell = row.createCell((short)2);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)3);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)4);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
		}
		
		if(emitirfunilvendasFiltro.getDtinicio() != null || emitirfunilvendasFiltro.getDtfim() != null){
			row = planilha.createRow((short) numeroLinhas);
			numeroLinhas ++;
			
			cell = row.createCell((short)0);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue("Per�odo");
			
			planilha.addMergedRegion(new Region(numeroLinhas -1, (short) 1, numeroLinhas -1, (short) 4));
			cell = row.createCell((short)1);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue(SinedUtil.getDescricaoPeriodo(emitirfunilvendasFiltro.getDtinicio(), emitirfunilvendasFiltro.getDtfim()));
			
			cell = row.createCell((short)2);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)3);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)4);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
		}

		numeroLinhas++;
				
		row = planilha.createRow((short) numeroLinhas);
		numeroLinhas++;
		
		cell= row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("Porc(%)");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("N� oportunidade");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("Consultor");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("Cliente");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("Valor");
		
		
		Integer linhasCabecalho = numeroLinhas + 1;
		boolean usarLinhasCabecalho = true;
		
		if (SinedUtil.isListNotEmpty(oportunidadesProbMaiorIgualDez)) {
			
			for (Oportunidade oportunidade : oportunidadesProbMaiorIgualDez) {
				row = planilha.createRow(planilha.getLastRowNum()+1);
				
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HORICENTER_VERTCENTER);
				cell.setCellValue(">=10%");
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue(oportunidade.getCdoportunidade() != null ? oportunidade.getCdoportunidade().toString() : "");
				
				cell = row.createCell((short) 2);
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue(oportunidade.getResponsavel() != null && oportunidade.getResponsavel().getNome() != null ? oportunidade.getResponsavel().getNome() : "");
				
				cell = row.createCell((short) 3);
				cell.setCellStyle(SinedExcel.STYLE_LEFT);
				if (oportunidade.getCliente() != null && oportunidade.getCliente().getNome() != null) {
					cell.setCellValue(oportunidade.getCliente().getNome());
				} else if (oportunidade.getContacrm() != null && oportunidade.getContacrm().getNome() != null){
					cell.setCellValue(oportunidade.getContacrm().getNome());
				} else {
					cell.setCellValue("");
				}
					
				cell = row.createCell((short) 4);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(oportunidade.getValortotal() != null ? oportunidade.getValortotal().getValue().doubleValue() : 0);
				
				numeroLinhas++;
				
			}
			
			planilha.addMergedRegion(new Region(linhasCabecalho - 1, (short) 0, numeroLinhas - 1, (short) 0));
			
			row = planilha.createRow(planilha.getLastRowNum() + 1);
			linhasValorAcumulado.add(planilha.getLastRowNum() + 1);
			numeroLinhas++;
			
			planilha.addMergedRegion(new Region((planilha.getLastRowNum()), (short) 0, (planilha.getLastRowNum()), (short) 3));
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
			cell.setCellValue("Valor Acumulado");

			StringBuilder valorAcumulado = new StringBuilder("SUM(");
			cell = row.createCell((short) 4);
		
			for (int index = linhasCabecalho; index < numeroLinhas; index++) {
				valorAcumulado.append(SinedExcel.getAlgarismoColuna(4)).append(index).append(";");
			}
			
			
			if(!valorAcumulado.toString().equals("SUM(")){
				cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
				cell.setCellFormula(valorAcumulado.substring(0, valorAcumulado.length()-1)+")");
				usarLinhasCabecalho = false;
			}else{
				cell.setCellValue(0.0);
			}
		}
		
		MutableDouble acumulado25 = new MutableDouble(0);
		MutableDouble acumulado50 = new MutableDouble(0); 
		MutableDouble acumulado75 = new MutableDouble(0);
		MutableDouble acumulado90 = new MutableDouble(0);
		MutableDouble acumulado100 = new MutableDouble(0);
		
		numeroLinhas = criaGrupoFunilVendas(oportunidadesProbMaiorIgualVinteCinco, numeroLinhas, planilha, linhasCabecalho, usarLinhasCabecalho, ">=25%", linhasValorAcumulado, acumulado25);
		numeroLinhas = criaGrupoFunilVendas(oportunidadesProbMaiorIgualCinquenta, numeroLinhas, planilha, linhasCabecalho, usarLinhasCabecalho, ">=50%", linhasValorAcumulado, acumulado50);
		numeroLinhas = criaGrupoFunilVendas(oportunidadesProbMaiorIgualSetentaCinco, numeroLinhas, planilha, linhasCabecalho, usarLinhasCabecalho, ">=75%", linhasValorAcumulado, acumulado75);
		numeroLinhas = criaGrupoFunilVendas(oportunidadesProbMaiorIgualNoventa, numeroLinhas, planilha, linhasCabecalho, usarLinhasCabecalho, ">=90%", linhasValorAcumulado, acumulado90);
		numeroLinhas = criaGrupoFunilVendas(oportunidadesProbIgualCem, numeroLinhas, planilha, linhasCabecalho, usarLinhasCabecalho, "=100%", linhasValorAcumulado, acumulado100);
		
		row = planilha.createRow(planilha.getLastRowNum() + 1);
		numeroLinhas++;
		
		planilha.addMergedRegion(new Region((planilha.getLastRowNum()), (short) 0, (planilha.getLastRowNum()), (short) 3));
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_RIGHT);
		
		cell.setCellValue("Total");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		
		
		

		StringBuilder valorAcumulado = new StringBuilder("SUM(");
		cell = row.createCell((short) 4);
	
		for (Integer linhaValorAcumulado : linhasValorAcumulado) {
			valorAcumulado.append(SinedExcel.getAlgarismoColuna(4)).append(linhaValorAcumulado).append(";");
		}
		
//		if(!valorAcumulado.toString().equals("SUM(")){
//			cell.setCellStyle(SinedExcel.STYLE_TOTAL_RIGHT);
//			cell.setCellFormula(valorAcumulado.substring(0, valorAcumulado.length()-1)+")");
//			usarLinhasCabecalho = false;
//		} else {
//			cell.setCellValue(0.0);
//		}
		
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_RIGHT);
		
		cell.setCellValue(new Double(acumulado25.doubleValue()+acumulado50.doubleValue()+acumulado75.doubleValue()+acumulado90.doubleValue()+acumulado100.doubleValue()));
		usarLinhasCabecalho = false;
		
		
		
		return wb.getWorkBookResource("funilvendas.xls");
	}
	
	public Resource gerarExcelEmitirfunilvendasSintetico(EmitirfunilvendasFiltro emitirfunilvendasFiltro) throws IOException{
		List<Oportunidade> oportunidades = oportunidadeDAO.findForEmitirfunilvendas(emitirfunilvendasFiltro);
		
		int oportunidadesProbMaiorIgualDez = 0;
		int oportunidadesProbMaiorIgualVinteCinco = 0;
		int oportunidadesProbMaiorIgualCinquenta = 0;
		int oportunidadesProbMaiorIgualSetentaCinco = 0;
		int oportunidadesProbMaiorIgualNoventa = 0;
		int oportunidadesProbIgualCem = 0;
		
		int numeroLinhas = 0;
		
		for (Oportunidade oportunidade : oportunidades) {
			if (oportunidade.getProbabilidade() != null) {
				if (oportunidade.getProbabilidade() >= 10 && oportunidade.getProbabilidade() < 25) {
					oportunidadesProbMaiorIgualDez++;
				} else if(oportunidade.getProbabilidade() >= 25 && oportunidade.getProbabilidade() < 50){
					oportunidadesProbMaiorIgualVinteCinco++;
				} else if(oportunidade.getProbabilidade() >= 50 && oportunidade.getProbabilidade() < 75){
					oportunidadesProbMaiorIgualCinquenta++;
				} else if(oportunidade.getProbabilidade() >= 75 && oportunidade.getProbabilidade() < 90){
					oportunidadesProbMaiorIgualSetentaCinco++;
				} else if(oportunidade.getProbabilidade() >= 90 && oportunidade.getProbabilidade() < 100){
					oportunidadesProbMaiorIgualNoventa++;
				} else if(oportunidade.getProbabilidade() == 100){
					oportunidadesProbIgualCem++;
				}
			}
		}
		
		SinedExcel wb = new SinedExcel();
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 20);
        
		HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 0, (short) 4));
		headerRow.setHeight((short)1000);
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Funil de Vendas");
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		numeroLinhas++;
		
		if(emitirfunilvendasFiltro.getDtinicio() != null || emitirfunilvendasFiltro.getDtfim() != null){
			row = planilha.createRow((short) numeroLinhas);
			numeroLinhas ++;
			
			cell = row.createCell((short)0);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue("Per�odo");
			
			planilha.addMergedRegion(new Region(numeroLinhas -1, (short) 1, numeroLinhas -1, (short) 4));
			cell = row.createCell((short)1);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell.setCellValue(SinedUtil.getDescricaoPeriodo(emitirfunilvendasFiltro.getDtinicio(), emitirfunilvendasFiltro.getDtfim()));
			
			cell = row.createCell((short)2);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)3);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
			cell = row.createCell((short)4);
			cell.setCellStyle(SinedExcel.STYLE_FILTRO_BORDER);
		}
		
		numeroLinhas++;
		
		numeroLinhas = criaGrupoFunilVendasSintetico(oportunidadesProbMaiorIgualVinteCinco, numeroLinhas, planilha, "25%");
		numeroLinhas = criaGrupoFunilVendasSintetico(oportunidadesProbMaiorIgualCinquenta, numeroLinhas, planilha, "50%");
		numeroLinhas = criaGrupoFunilVendasSintetico(oportunidadesProbMaiorIgualSetentaCinco, numeroLinhas, planilha, "75%");
		numeroLinhas = criaGrupoFunilVendasSintetico(oportunidadesProbMaiorIgualNoventa, numeroLinhas, planilha, "90%");
		numeroLinhas = criaGrupoFunilVendasSintetico(oportunidadesProbIgualCem, numeroLinhas, planilha, "100%");
		
		return wb.getWorkBookResource("funilvendassintetico.xls");
		
	}
	
	private Integer criaGrupoFunilVendas(List<Oportunidade> oportunidadesProb, Integer numeroLinhas, HSSFSheet planilha, Integer linhasCabecalho, boolean usarLinhasCabecalho, String probabilidade, List<Integer> linhasValorAcumulado, MutableDouble acumulado) {
		HSSFRow row;
		HSSFCell cell;
		Integer numeroLinhasUltimoGrupo;
		if (SinedUtil.isListNotEmpty(oportunidadesProb)) {
			numeroLinhasUltimoGrupo = numeroLinhas;
			
			for (Oportunidade oportunidade : oportunidadesProb) {
				row = planilha.createRow(planilha.getLastRowNum()+1);
				
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HORICENTER_VERTCENTER);
				cell.setCellValue(probabilidade);
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue(oportunidade.getCdoportunidade() != null ? oportunidade.getCdoportunidade().toString() : "");
				
				cell = row.createCell((short) 2);
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue(oportunidade.getResponsavel() != null && oportunidade.getResponsavel().getNome() != null ? oportunidade.getResponsavel().getNome() : "");
				
				cell = row.createCell((short) 3);
				cell.setCellStyle(SinedExcel.STYLE_LEFT);
				if (oportunidade.getCliente() != null && oportunidade.getCliente().getNome() != null) {
					cell.setCellValue(oportunidade.getCliente().getNome());
				} else if (oportunidade.getContacrm() != null && oportunidade.getContacrm().getNome() != null){
					cell.setCellValue(oportunidade.getContacrm().getNome());
				} else {
					cell.setCellValue("");
				}
				
				cell = row.createCell((short) 4);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(oportunidade.getValortotal() != null ? oportunidade.getValortotal().getValue().doubleValue() : 0);
				
				acumulado.setValue(acumulado.doubleValue() + (oportunidade.getValortotal() != null ? oportunidade.getValortotal().getValue().doubleValue() : 0)); 
				
				numeroLinhas++;
				
			}
			
			planilha.addMergedRegion(new Region(numeroLinhasUltimoGrupo, (short) 0, numeroLinhas - 1, (short) 0));
			
			row = planilha.createRow(planilha.getLastRowNum() + 1);
			linhasValorAcumulado.add(planilha.getLastRowNum() + 1);
			numeroLinhas++;
			
			planilha.addMergedRegion(new Region((planilha.getLastRowNum()), (short) 0, (planilha.getLastRowNum()), (short) 3));
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
			cell.setCellValue("Valor Acumulado");

			StringBuilder valorAcumulado = new StringBuilder("SUM(");
			cell = row.createCell((short) 4);
			
			Integer numeroLinhasValor = 0;
			
			if (usarLinhasCabecalho) {
				numeroLinhasValor = linhasCabecalho;
			} else {
				numeroLinhasValor = numeroLinhasUltimoGrupo + 1;
			}
			
			for (int index = numeroLinhasValor; index < numeroLinhas; index++) {
				valorAcumulado.append(SinedExcel.getAlgarismoColuna(4)).append(index).append(";");
			}
			
			
//			if(!valorAcumulado.toString().equals("SUM(")){
//				cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
//				cell.setCellFormula(valorAcumulado.substring(0, valorAcumulado.length()-1)+")");
//				usarLinhasCabecalho = false;
//			}else{
//				cell.setCellValue(0.0);
//			}
			
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
			cell.setCellValue(acumulado.floatValue());
			usarLinhasCabecalho = false;
			
			
		}
		return numeroLinhas;
	}
	
	public Integer criaGrupoFunilVendasSintetico(int oportunidadesProb, Integer numeroLinhas, HSSFSheet planilha, String probabilidade){
		HSSFRow row;
		HSSFCell cell;
		row = planilha.createRow(numeroLinhas);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HORICENTER_VERTCENTER);
		cell.setCellValue(probabilidade);
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_LEFT);
		cell.setCellValue(oportunidadesProb);
		
		numeroLinhas++;
		
		return numeroLinhas;
	}
	
	/**
	* M�todo que calcula a margem de contribui��o de todos os itens da oportunidade
	*
	* @param oportunidade
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public void calcularMargemcontribuicaoByFormula(Oportunidade oportunidade){
		if(oportunidade != null && SinedUtil.isListNotEmpty(oportunidade.getListaOportunidadematerial())){
			boolean paramCalculoMargemcontribOportunidade = parametrogeralService.getBoolean(Parametrogeral.CALCULO_MARGEMCONTRIB_OPORTUNIDADE); 
			for(Oportunidadematerial oportunidadematerial : oportunidade.getListaOportunidadematerial()){
				if(paramCalculoMargemcontribOportunidade){
					calcularMargemcontribuicaoByFormula(oportunidadematerial, oportunidade.getRevendedor());
				}else {
					oportunidadematerial.setDesconto(null);
					oportunidadematerial.setComissao(null);
					oportunidadematerial.setMargcontribpercentual(null);
					oportunidadematerial.setMargcontribvalor(null);
				}
			}
		}
	}
	
	/**
	* M�todo que calcula a margem de contribuicao do item da oportunidade
	*
	* @param oportunidadematerial
	* @param revendedor
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public void calcularMargemcontribuicaoByFormula(Oportunidadematerial oportunidadematerial, Boolean revendedor){
		if(parametrogeralService.getBoolean(Parametrogeral.CALCULO_MARGEMCONTRIB_OPORTUNIDADE)){
			try {
				oportunidadematerial.setMargcontribvalor(0d);
				oportunidadematerial.setMargcontribpercentual(0d);
				Material material = materialService.loadForCalcularMargemcontribuicao(oportunidadematerial.getMaterial());
				material.setRevendedor(revendedor);
				oportunidadematerial.setMargcontribvalor(materialformulamargemcontribuicaoService.calculaMargemcontribuicaoMaterial(material, oportunidadematerial));
				if(oportunidadematerial.getValorunitario() != null && oportunidadematerial.getValorunitario() != 0){
					oportunidadematerial.setMargcontribpercentual((oportunidadematerial.getMargcontribvalor()/oportunidadematerial.getValorunitario())*100);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * @param cliente
	 * @return
	 * @since 12/07/2016
	 * @author C�sar
	 */
	public List<Oportunidade> findForPainelInteracao (Cliente cliente, Empresa empresa){
		return oportunidadeDAO.findForPainelInteracao(cliente, empresa);
	}
	
	/**
	* M�todo que atualiza os materiais da oportunidade de acordo com o or�amento
	*
	* @param oportunidade
	* @param listavendaorcamentomaterial
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public void atualizarMaterialOportunidadeAposOrcamento(Oportunidade oportunidade, Vendaorcamento vendaorcamento, List<Vendaorcamentomaterial> listavendaorcamentomaterial) {
		if(oportunidade != null && SinedUtil.isListNotEmpty(listavendaorcamentomaterial)){
			Oportunidade bean = loadForEntrada(oportunidade);
			
			if(bean != null){
				List<Oportunidadematerial> listaOportunidadematerialSalvar = new ArrayList<Oportunidadematerial>();
				Oportunidadematerial oportunidadematerial;
				Money valorDesconto;
				Money valorDescontoItem;
				Double qtde = 1d;
				for(Vendaorcamentomaterial item : listavendaorcamentomaterial){
					if(item.getMaterial() != null){
						oportunidadematerial = new Oportunidadematerial();
						oportunidadematerial.setOportunidade(bean);
						oportunidadematerial.setMaterial(item.getMaterial());
						oportunidadematerial.setUnidademedida(item.getUnidademedida());
						
						qtde = item.getQuantidade() != null ? item.getQuantidade() : 1d;
						valorDescontoItem = item.getDesconto() != null ? item.getDesconto() :  new Money();
						
						valorDesconto = vendaorcamentoService.getValorDescontoVenda(listavendaorcamentomaterial, item, vendaorcamento.getDesconto(), vendaorcamento.getValorusadovalecompra(), true);
						valorDescontoItem = valorDescontoItem.divide(new Money(qtde));
						valorDesconto = valorDesconto.add(valorDescontoItem);
						
						oportunidadematerial.setValorunitario(item.getPreco() - valorDesconto.getValue().doubleValue());
						
						oportunidadematerial.setQuantidade(item.getQuantidade());
						oportunidadematerial.setObservacao(item.getObservacao() != null && item.getObservacao().length() > 1000 ? item.getObservacao().substring(0, 999) : item.getObservacao());
						listaOportunidadematerialSalvar.add(oportunidadematerial);
					}
				}
				
				if(SinedUtil.isListNotEmpty(bean.getListaOportunidadematerial())){
					for(Oportunidadematerial itemRemover : bean.getListaOportunidadematerial()){
						oportunidadematerialService.delete(itemRemover);
					}
				}
				
				Money valortotal = new Money(); 
				for(Oportunidadematerial itemSalvar : listaOportunidadematerialSalvar){
					itemSalvar.setOportunidade(bean);
					oportunidadematerialService.saveOrUpdate(itemSalvar);
					
					if(itemSalvar.getValorunitario() != null && itemSalvar.getQuantidade() != null){
						valortotal = valortotal.add(new Money(itemSalvar.getValorunitario() * itemSalvar.getQuantidade()));
					}
				}
				updateValortotal(bean, valortotal);
			}
		}
	}
	
	public String getWhereInOportunidadeFiltro(OportunidadeFiltro filtro) {
		return oportunidadeDAO.getWhereInOportunidadeFiltro(filtro);
	}
	
	public List<Oportunidade> findWithResponsavel(String whereIn){
		return oportunidadeDAO.findWithResponsavel(whereIn);
	}
	
	public boolean isUsuarioPodeEditarOportunidades(String whereInOportunidade){
		if(SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado())){
			for(Oportunidade op: this.findWithResponsavel(whereInOportunidade)){
				if(!SinedUtil.getUsuarioLogado().equals(op.getResponsavel())){
					return false;
				}
			}
		}
		return true;
	}
	
	public Oportunidade loadWithResponsavel(Integer cdoportunidade){
		return this.findWithResponsavel(cdoportunidade.toString()).get(0);
	}
	
	public Resource gerarRelatorioCSVListagem(WebRequestContext request, OportunidadeFiltro filtro){
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Oportunidade> listaOportunidade = this.findForListagem(filtro).list();//getLista(request, filtro).list();
		
		String whereIn = SinedUtil.listAndConcatenateIDs(listaOportunidade);
		if(whereIn != null && !whereIn.equals("")) {
			List<Oportunidadematerial> list = oportunidadematerialService.findByOportunidade(whereIn);
			for (Oportunidade oportunidade : listaOportunidade) {
				oportunidade.setListaOportunidadematerial(new HashSet<Oportunidadematerial>(oportunidadematerialService.getListaOportunidadematerialByOportunidade(list, oportunidade)));
			}
		}
		
		StringBuilder csv = new StringBuilder();
		csv.append("\"C�digo\";");
		csv.append("\"Prob. %\";");
		csv.append("\"Situa��o\";");
		csv.append("\"Nome Oportunidade/Nome Conta\";");
		csv.append("\"Produto/Servi�o\";");
		csv.append("\"Valor\";");
		csv.append("\"In�cio\";");
		csv.append("\"Data de Retorno\";");
		csv.append("\"Ciclo\";");
		csv.append("\"Pr�ximo Passo\";");
		csv.append("\"Respons�vel Colaborador/Ag�ncia\";");
		csv.append("\"Tipo de Pessoa\";");
		csv.append("\"Nome Cliente\";");
		csv.append("\"Endere�o(s) Cliente\";");
		csv.append("\"Telefone(s)\";");
		csv.append("\"Contato\";");
		csv.append("\"Email\";");
		String materiais; 
		
		for (Oportunidade oportunidade: listaOportunidade){
			materiais = oportunidade.getMateriais();
			
			if(oportunidade.getProbabilidade() != null){
				oportunidade.setShowProbabilidade(oportunidade.getProbabilidade().toString() + "%"); 
			}
			
			Cliente cliente = oportunidade.getCliente();
			if(cliente != null) cliente = clienteService.carregarDadosCliente(cliente);
			if(cliente != null) cliente.setListaContato(pessoaContatoService.findByPessoa(cliente));
			Contacrm conta = oportunidade.getContacrm();
			if(conta != null) conta = contacrmService.carregaContacrmcontato(conta.getCdcontacrm().toString());
			
			StringBuilder csvAux = new StringBuilder();
			addCell(csvAux, oportunidade.getCdoportunidade());
			addCell(csvAux, oportunidade.getShowProbabilidade());
			addCell(csvAux, oportunidade.getOportunidadesituacao() != null ? oportunidade.getOportunidadesituacao().getNome() : " ");
			addCell(csvAux, Util.strings.emptyIfNull(oportunidade.getNome()) + "/" + (oportunidade.getContacrm()!=null ? oportunidade.getContacrm().getNome() : " "));
			addCell(csvAux, materiais);
			addCell(csvAux, oportunidade.getValortotal());
			addCell(csvAux, oportunidade.getDtInicio());
			addCell(csvAux, oportunidade.getDtretorno());
			addCell(csvAux, oportunidade.getCiclo());
			addCell(csvAux, oportunidade.getProximopasso());
			addCell(csvAux, oportunidade.getResponsavel()!= null ? oportunidade.getResponsavel().getNome() : " ");
			addCell(csvAux, Util.strings.emptyIfNull(oportunidade.getTipopessoacrm() != null && oportunidade.getTipopessoacrm().getTipo() != null ? oportunidade.getTipopessoacrm().getTipo(): " "));
			addCell(csvAux, Util.strings.emptyIfNull(cliente != null && cliente.getNome() != null ? cliente.getNome() : " "));
			addCell(csvAux, cliente != null && cliente.getEnderecos() != null ? cliente.getEnderecos() : " ");
			
			if (cliente != null) {
				addCell(csvAux, Util.strings.emptyIfNull(cliente.getTelefones() != null ? cliente.getTelefones() : " "));
				addCell(csvAux, Util.strings.emptyIfNull(cliente.getContatos() != null ? cliente.getContatos() : " "));
				addCell(csvAux, Util.strings.emptyIfNull(cliente.getEmail() != null ? cliente.getEmail() : " "));
			} else {
				String nome = "";
				String email = "";
				String telefone = "";
				
				if(conta != null && SinedUtil.isListNotEmpty(conta.getListcontacrmcontato())){
					for (Contacrmcontato c : conta.getListcontacrmcontato()) {
						nome += c.getNome() + "|";
						
						for (Contacrmcontatoemail ce : c.getListcontacrmcontatoemail()) {
							email += ce.getEmail() + "|";
						}
						
						for (Contacrmcontatofone cf : c.getListcontacrmcontatofone()) {
							telefone += cf.getTelefone() + "|";
						}
					}
				}
				
				addCell(csvAux, Util.strings.emptyIfNull(telefone.length() > 1 ? telefone.substring(0, telefone.length() - 1) : telefone));
				addCell(csvAux, Util.strings.emptyIfNull(nome.length() > 1 ? nome.substring(0, nome.length() - 1) : nome));
				addCell(csvAux, Util.strings.emptyIfNull(email.length() > 1 ? email.substring(0, email.length() - 1) : email));
			}
			csv.append("\n");
			csv.append(csvAux.toString());
		}		
		
		Resource resource = new Resource();
		resource.setContentType("text/csv");
		resource.setFileName("oportunidade.csv");
		resource.setContents(csv.toString().getBytes());
		
		return resource;
	}
	
	private void addCell(StringBuilder csv, String value){
		csv.append("\"" + (value != null ? value : ";") + "\";");
	}
	
	private void addCell(StringBuilder csv, Object value){
		csv.append("\"" + (value != null ? value : "") + "\";");
	}

	@Override
	public ListagemResult<Oportunidade> findForListagem(FiltroListagem filtro) {
		OportunidadeFiltro _filtro = (OportunidadeFiltro)filtro;
		if (!filtro.isNotFirstTime()) {
			Colaborador colaborador = new Colaborador(((Usuario)NeoWeb.getRequestContext().getUser()).getCdpessoa());
			colaborador = colaboradorService.carregaColaboradorresponsavel(colaborador.getCdpessoa());
			if(colaborador != null && colaborador.getCdpessoa() != null)
				_filtro.setColaborador(colaborador);
		}
		return super.findForListagem(_filtro);
	}
}
