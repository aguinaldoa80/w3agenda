package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRemessaInstrucaoDAO;

public class BancoConfiguracaoRemessaInstrucaoService extends GenericService<BancoConfiguracaoRemessaInstrucao>{

	private BancoConfiguracaoRemessaInstrucaoDAO bancoConfiguracaoRemessaInstrucaoDAO;
	
	public void setBancoConfiguracaoRemessaInstrucaoDAO(BancoConfiguracaoRemessaInstrucaoDAO bancoConfiguracaoRemessaInstrucaoDAO) {this.bancoConfiguracaoRemessaInstrucaoDAO = bancoConfiguracaoRemessaInstrucaoDAO;}

	public List<BancoConfiguracaoRemessaInstrucao> findByContacarteiraForRemessa(Contacarteira contacarteira){
		return bancoConfiguracaoRemessaInstrucaoDAO.findByContacarteiraForRemessa(contacarteira);
	}
	
}
