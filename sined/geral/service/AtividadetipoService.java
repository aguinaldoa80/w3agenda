package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.dao.AtividadetipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.atividadetipo.AtividadetipoRESTModel;

public class AtividadetipoService extends GenericService<Atividadetipo>{

	private AtividadetipoDAO atividadetipoDAO;
	public void setAtividadetipoDAO(AtividadetipoDAO atividadetipoDAO) {this.atividadetipoDAO = atividadetipoDAO;}
	
	/* singleton */
	private static AtividadetipoService instance;
	public static AtividadetipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AtividadetipoService.class);
		}
		return instance;
	}
	
	public Atividadetipo findAtividadePadrao(){
		return atividadetipoDAO.findAtividadePadrao();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.AtividadetipoDAO#findAtividadetipoAtivo()
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 26/11/2013
	 */
	public List<Atividadetipo> findAtividadetipoAtivo() {
		return atividadetipoDAO.findAtividadetipoAtivo();
	}
	
	public Atividadetipo findPrazoConclusao (Atividadetipo atividadetipo) {
		return atividadetipoDAO.findPrazoConclusao(atividadetipo);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.AtividadetipoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<AtividadetipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.AtividadetipoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<AtividadetipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<AtividadetipoRESTModel> lista = new ArrayList<AtividadetipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Atividadetipo bean : atividadetipoDAO.findForAndroid(whereIn))
				lista.add(new AtividadetipoRESTModel(bean));
		}
		
		return lista;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.AtividadetipoDAO#findForServicos()
	*
	* @return
	* @since 03/01/2017
	* @author Mairon Cezar
	*/
	public List<Atividadetipo> findForServicos() {
		return atividadetipoDAO.findForServicos();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.AtividadetipoDAO#findForCrm()
	*
	* @return
	* @since 03/01/2017
	* @author Mairon Cezar
	*/
	public List<Atividadetipo> findForCrm() {
		return atividadetipoDAO.findForCrm();
	}
}
