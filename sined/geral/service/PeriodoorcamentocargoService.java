package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Dependenciacargo;
import br.com.linkcom.sined.geral.bean.Dependenciafaixa;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Periodoorcamento;
import br.com.linkcom.sined.geral.bean.Periodoorcamentocargo;
import br.com.linkcom.sined.geral.bean.Relacaocargo;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PeriodoorcamentocargoService extends GenericService<Periodoorcamentocargo> {

	private PeriodoorcamentocargoDAO periodoorcamentocargoDAO;
	private PlanejamentoService planejamentoService;
	private RelacaocargoService relacaocargoService;
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setPeriodoorcamentocargoDAO(PeriodoorcamentocargoDAO periodoorcamentocargoDAO) {
		this.periodoorcamentocargoDAO = periodoorcamentocargoDAO;
	}
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	public void setRelacaocargoService(RelacaocargoService relacaocargoService) {
		this.relacaocargoService = relacaocargoService;
	}
	
	/***
	 * Verifica se j� existe o histograma calculado para um determinado or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#existeHistograma(Orcamento)
	 * 
	 * @param orcamento
	 * @return verdadeiro ou falso
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Boolean existeHistograma(Orcamento orcamento) {		
		return periodoorcamentocargoDAO.existeHistograma(orcamento);
	}
	
	/***
	 * Retorna os cargos e suas quantidades associados aos per�odos de um determinado or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#findByOrcamento(Orcamento)
	 * 
	 * @param orcamento
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Periodoorcamentocargo> findByOrcamentoFlex(Orcamento orcamento) {
		return periodoorcamentocargoDAO.findByOrcamento(orcamento);
	}	
	
	/***
	 * Retorna os cargos e suas quantidades associados aos per�odos de um determinado or�amento.
	 * Se o par�metro calcular for verdadeiro, o sistema obrigatoriamente calcula todos os valores.
	 * Se o par�metro calcular for falso, o sistema procura por dados cadastrados no banco. Caso n�o tenha, calcula todos os valores
	 * e salva-os no banco de dados.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresHistograma(Orcamento)
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#findByOrcamento(Orcamento)
	 * 
	 * @param orcamento
	 * @param calcular
	 * @return List<Periodoorcamentocargo>

	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Periodoorcamentocargo> findByOrcamentoFlex(Orcamento orcamento, Boolean calcular) {
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo;
		
		if (calcular) {
			listaPeriodoOrcamentoCargo = calculaValoresHistograma(orcamento, null);
		}
		else {
			listaPeriodoOrcamentoCargo = periodoorcamentocargoDAO.findByOrcamento(orcamento);
			if (listaPeriodoOrcamentoCargo == null || listaPeriodoOrcamentoCargo.isEmpty()) {
				listaPeriodoOrcamentoCargo = calculaValoresHistograma(orcamento, null);
				saveListaPeriodoOrcamentoCargoFlex(orcamento, listaPeriodoOrcamentoCargo);
			}
		}
		return listaPeriodoOrcamentoCargo;
	}
	
	/***
	 * Calcula os valores para serem exibidos no histograma de m�o-de-obra 
	 * por�m utilizando os valores de carga hor�ria semanal definidos no par�metro listaPeriodoOrcamentoCargo
	 * Se o par�metro calcular for verdadeiro, o sistema obrigatoriamente calcula todos os valores.
	 * Se o par�metro calcular for falso, o sistema procura por dados cadastrados no banco. Caso n�o tenha, calcula todos os valores.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresHistograma(Orcamento)
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#findByOrcamento(Orcamento)
	 * 
	 * @param orcamento
	 * @param calcular
	 * @return List<Periodoorcamentocargo>

	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Periodoorcamentocargo> atualizaCargaHorariaSemanalFlex(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		Map<Cargo, Double> mapaCargoCHSemanal = new HashMap<Cargo, Double>();
		
		if (listaPeriodoOrcamentoCargo != null) {
			for (Periodoorcamentocargo periodoorcamentocargo : listaPeriodoOrcamentoCargo) {
				mapaCargoCHSemanal.put(periodoorcamentocargo.getCargo(), periodoorcamentocargo.getCargohorasemanal());
			}
		}
		return calculaValoresHistograma(orcamento, mapaCargoCHSemanal);
	}
	
	/***
	 * Atualiza os valores do histograma e salva no banco de dados
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresHistograma(Orcamento, Map)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#saveListaPeriodoOrcamentoCargoFlex(Orcamento, List)
	 * 
	 * @param orcamento 
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public void atualizaValoresHistograma(Orcamento orcamento) {
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo = calculaValoresHistograma(orcamento, null);
		saveListaPeriodoOrcamentoCargoFlex(orcamento, listaPeriodoOrcamentoCargo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#deleteByOrcamento
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteByOrcamento(Orcamento orcamento) {
		periodoorcamentocargoDAO.deleteByOrcamento(orcamento);
	}
	
	/***
	 * Retorna os cargos (com suas quantidades) associados �s tarefas de um determinado or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#carregaListaCargoQuantidade(Orcamento)
	 * 
	 * @param orcamento
	 * @return List<Periodoorcamentocargo>
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	private List<Periodoorcamentocargo> carregaListaCargoQuantidade(Orcamento orcamento) {
		return periodoorcamentocargoDAO.carregaListaCargoQuantidade(orcamento);
	}	
	
	/***
	 * Retorna os cargos (com suas quantidades) associados �s tarefas de um determinado or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#carregaListaCargoQuantidadeSemHistograma(Orcamento)
	 * 
	 * @param orcamento
	 * @return List<Orcamentorecursohumano>
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Freitas
	 */
	public Map<Cargo, Double> carregaListaCargoQuantidadeSemHistograma(Orcamento orcamento){
		return periodoorcamentocargoDAO.carregaListaCargoQuantidadeSemHistograma(orcamento);
	}
	
	/***
	 * Calcula os valores para serem exibidos no histograma de m�o-de-obra
	 * Se o par�metro mapaCargoHoraSemanal for diferente de nulo, ser�o utilizados os seus elementos para os valores da carga hor�ria semanal
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#carregaListaCargoQuantidade(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#distribuirRecursos(int, int)
	 * @see br.com.linkcom.sined.geral.service.RelacaocargoService#findByOrcamento(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresRelacaoCargo(Orcamento, List, List)
	 * 
	 * 
	 * @param orcamento
	 * @param mapaCargoCHSemanal
	 * 
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @author Rodrigo Alvarenga
	 */
	private List<Periodoorcamentocargo> calculaValoresHistograma(Orcamento orcamento, Map<Cargo, Double> mapaCargoCHSemanal) {
		
		//Listas
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo;
		List<Double> listaPeriodo;
		List<Periodoorcamento> listaPeriodoOrcamento;
		List<Relacaocargo> listaRelacaoCargo;
		List<Relacaocargo> listaRelacaoCargoMOD;
		List<Relacaocargo> listaRelacaoCargoMOI;
		
		//Mapas
		Map<Cargo, Periodoorcamentocargo> mapaHistograma = new HashMap<Cargo, Periodoorcamentocargo>();
		
		//Beans
		Periodoorcamento periodoOrcamento;
		Set<Cargo> keySetCargo;
		Double cargaHorariaSemanal;
		Double totalHH;

		/******************************************
		 * Busca os cargos relacionados �s tarefas
		 ******************************************/
		
		//Busca os cargos e suas quantidades necess�rias para as tarefas do or�amento
		listaPeriodoOrcamentoCargo = this.carregaListaCargoQuantidade(orcamento);
		
		if (listaPeriodoOrcamentoCargo != null) {
			//Para cada cargo
			for (Periodoorcamentocargo periodoOrcamentoCargo : listaPeriodoOrcamentoCargo) {				
		
				listaPeriodoOrcamento = new ArrayList<Periodoorcamento>();
				
				//Busca a carga hor�ria semanal do cargo
				if (mapaCargoCHSemanal != null) {
					cargaHorariaSemanal = mapaCargoCHSemanal.get(periodoOrcamentoCargo.getCargo());
					if (cargaHorariaSemanal != null) {
						periodoOrcamentoCargo.setCargohorasemanal(cargaHorariaSemanal);
					}
				}
				
				if (periodoOrcamentoCargo.getCargohorasemanal() == null || periodoOrcamentoCargo.getCargohorasemanal().intValue() == 0) {
					totalHH = 0d;
				}
				else {
					totalHH = periodoOrcamentoCargo.getTotalhoracalculada().doubleValue() / periodoOrcamentoCargo.getCargohorasemanal().doubleValue(); 
				}
				
				//Calcula a distribui��o do recurso nos per�odos do projeto
				listaPeriodo = planejamentoService.distribuirRecursos(orcamento.getPrazosemana(),totalHH);
				
				if (listaPeriodo != null) {					
					for (int i = 0; i < listaPeriodo.size(); i++) {
						periodoOrcamento = new Periodoorcamento();
						periodoOrcamento.setNumero(i+1);
						periodoOrcamento.setPeriodoorcamentocargo(periodoOrcamentoCargo);
						
						//Do Caso de Uso:
						//Na distribui��o de m�o de obra, sempre devem existir valores inteiros. 
						//Caso ocorra valores decimais, deve usar o arredondamento para o valor mais pr�ximo
						periodoOrcamento.setQtde(listaPeriodo.get(i));
						
						listaPeriodoOrcamento.add(periodoOrcamento);
					}
				}
				periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);
				
				mapaHistograma.put(periodoOrcamentoCargo.getCargo(), periodoOrcamentoCargo);
			}
		}
		
		/*********************************************
		 * Busca os cargos n�o relacionados �s tarefas
		 *********************************************/
		
		//Busca a lista de rela��es entre os cargos do or�amento
		listaRelacaoCargo = relacaocargoService.findByOrcamento(orcamento);
		
		listaRelacaoCargoMOD = new ArrayList<Relacaocargo>();
		listaRelacaoCargoMOI = new ArrayList<Relacaocargo>();
		
		//Separa a lista de Rela��o entre Cargos em MDO e MDI
		if (listaRelacaoCargo != null) {
			
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				
				//Busca a carga hor�ria semanal do cargo
				if (mapaCargoCHSemanal != null) {
					cargaHorariaSemanal = mapaCargoCHSemanal.get(relacaocargo.getCargo());
					if (cargaHorariaSemanal != null) {
						relacaocargo.getCargo().setTotalhorasemana(cargaHorariaSemanal);
					}
				}				
				
				if (relacaocargo.getCargo().getTipocargo().equals(Tipocargo.MOD)) {
					listaRelacaoCargoMOD.add(relacaocargo);
				}
				else {
					listaRelacaoCargoMOI.add(relacaocargo);
				}					
			}
		}
		
		//Primeiro calcula os valores dos cargos do tipo MOD
		mapaHistograma = calculaValoresRelacaoCargo(orcamento, mapaHistograma, listaRelacaoCargoMOD);
		
		//Depois calcula os valores dos cargos do tipo MOI
		mapaHistograma = calculaValoresRelacaoCargo(orcamento, mapaHistograma, listaRelacaoCargoMOI);
		

		listaPeriodoOrcamentoCargo.clear();
		
		//Transfere os valores do mapa para a lista
		keySetCargo = mapaHistograma.keySet();		
		for (Cargo cargo : keySetCargo) {
			listaPeriodoOrcamentoCargo.add(mapaHistograma.get(cargo));
		}
		
		//Ordena a lista pelos nomes dos cargos
		Collections.sort(listaPeriodoOrcamentoCargo, 
				new Comparator<Periodoorcamentocargo>() {
					public int compare(Periodoorcamentocargo o1, Periodoorcamentocargo o2) {
						return o1.getCargo().getNome().compareTo(o2.getCargo().getNome());
				}});
		
		
		return listaPeriodoOrcamentoCargo;
	}
	
	/***
	 * Calcula os valores referentes aos cargos presentes na listaRelacaoCargo e acrescenta na listaPeriodoOrcamentoCargo
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresRelacaoCargoSemDependencia(Orcamento, Map, List)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresRelacaoCargoDependenciaMDO(Orcamento, Map, List, Boolean)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaValoresRelacaoCargoDependenciaSelecaoCargo(Orcamento, Map, List)
	 * 
	 * @param orcamento
	 * @param mapaHistograma
	 * @param listaRelacaoCargo
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @throws SinedException - caso haja algum erro no c�lculo dos valores
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Map<Cargo, Periodoorcamentocargo> calculaValoresRelacaoCargo(Orcamento orcamento, Map<Cargo, Periodoorcamentocargo> mapaHistograma, List<Relacaocargo> listaRelacaoCargo) {
		
		//1. Busca os cargos que n�o possuem depend�ncia
		mapaHistograma = calculaValoresRelacaoCargoSemDependencia(orcamento, mapaHistograma, listaRelacaoCargo);
		
		//2. Busca os cargos que possuem depend�ncia com cargos do tipo M�o-de-Obra Direta
		mapaHistograma = calculaValoresRelacaoCargoDependenciaMDO(orcamento, mapaHistograma, listaRelacaoCargo, true);		

		//3. Busca os cargos que possuem depend�ncia com alguns cargos
		mapaHistograma = calculaValoresRelacaoCargoDependenciaSelecaoCargo(orcamento, mapaHistograma, listaRelacaoCargo);
		
		//5. Busca os cargos que possuem depend�ncia com todos os cargos
		mapaHistograma = calculaValoresRelacaoCargoDependenciaMDO(orcamento, mapaHistograma, listaRelacaoCargo, false);
		
		return mapaHistograma;
	}
	
	/***
	 * Calcula os valores referentes aos cargos presentes na listaRelacaoCargo cujo tipo de depend�ncia seja SEM_DEPENDENCIA
	 * e acrescenta no mapaHistograma
	 * 
	 * @param orcamento
	 * @param mapaHistograma
	 * @param listaRelacaoCargo
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @throws SinedException - caso haja algum erro no c�lculo dos valores
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Map<Cargo, Periodoorcamentocargo> calculaValoresRelacaoCargoSemDependencia(Orcamento orcamento, Map<Cargo, Periodoorcamentocargo> mapaHistograma, List<Relacaocargo> listaRelacaoCargo) {
		
		//Listas
		List<Periodoorcamento> listaPeriodoOrcamento;
		
		//Beans
		Periodoorcamentocargo periodoOrcamentoCargo;
		Periodoorcamento periodoOrcamento;
		
		if (listaRelacaoCargo != null) {
			
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if (relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
					
					//Verifica se j� existe o cargo no Histograma
					if (mapaHistograma.get(relacaocargo.getCargo()) != null) {
						throw new SinedException("O cargo " + relacaocargo.getCargo().getNome() + " n�o pode simultaneamente ser recurso de uma tarefa e depender de outros cargos.");
					}
					
					periodoOrcamentoCargo = new Periodoorcamentocargo();
					periodoOrcamentoCargo.setCargo(relacaocargo.getCargo());
					periodoOrcamentoCargo.setCargohorasemanal(relacaocargo.getCargo().getTotalhorasemana());
					periodoOrcamentoCargo.setOrcamento(orcamento);

					listaPeriodoOrcamento = new ArrayList<Periodoorcamento>();
					
					for (int i = 1; i <= orcamento.getPrazosemana(); i++) {
						periodoOrcamento = new Periodoorcamento();
						periodoOrcamento.setNumero(i);
						periodoOrcamento.setPeriodoorcamentocargo(periodoOrcamentoCargo);
						periodoOrcamento.setQtde(relacaocargo.getQuantidade());
						
						listaPeriodoOrcamento.add(periodoOrcamento);
					}
					periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);
					
					mapaHistograma.put(relacaocargo.getCargo(), periodoOrcamentoCargo);
				}
			}
		}
		return mapaHistograma;
	}
	
	/***
	 * Calcula os valores referentes aos cargos presentes na listaRelacaoCargo cujo tipo de depend�ncia seja TODOS_CARGOS ou MOD
	 * e acrescenta no mapaHistograma.
	 * Se o par�metro somenteMOD for verdadeiro, ser� calculada somente a quantidade dos cargos que tiverem o tipo de depend�ncia MOD
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaTotalMDO(Map, List, Boolean)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#montaListaPeriodoOrcamentoValorUnico(Periodoorcamentocargo, Double, Map)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#montaListaPeriodoOrcamentoValorPorFaixa(Periodoorcamentocargo, List, Map)
	 * 
	 * @param orcamento
	 * @param mapaHistograma
	 * @param listaRelacaoCargo
	 * @param somenteMOD
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @throws SinedException - caso haja algum erro no c�lculo dos valores
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Map<Cargo, Periodoorcamentocargo> calculaValoresRelacaoCargoDependenciaMDO(Orcamento orcamento, Map<Cargo, Periodoorcamentocargo> mapaHistograma, List<Relacaocargo> listaRelacaoCargo, Boolean somenteMOD) {
		
		//Listas
		List<Periodoorcamento> listaPeriodoOrcamento;
		
		//Mapas
		Map<Integer, Double> mapaQuantidadeTotal;
		
		//Beans
		Periodoorcamentocargo periodoOrcamentoCargo;
		
		if (listaRelacaoCargo != null) {
			
			mapaQuantidadeTotal = calculaTotalMDO(mapaHistograma, null, somenteMOD);			
			
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if ((somenteMOD && relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.MOD)) ||
				   (!somenteMOD && relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.TODOS_CARGOS))) {
					
					//Verifica se j� existe o cargo no Histograma
					if (mapaHistograma.get(relacaocargo.getCargo()) != null) {
						throw new SinedException("O cargo " + relacaocargo.getCargo().getNome() + " n�o pode simultaneamente ser recurso de uma tarefa e depender de outros cargos.");
					}
					
					periodoOrcamentoCargo = new Periodoorcamentocargo();
					periodoOrcamentoCargo.setCargo(relacaocargo.getCargo());
					periodoOrcamentoCargo.setCargohorasemanal(relacaocargo.getCargo().getTotalhorasemana());					
					periodoOrcamentoCargo.setOrcamento(orcamento);
					
					//Verifica qual o tipo de valor
					if (relacaocargo.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
						listaPeriodoOrcamento = montaListaPeriodoOrcamentoValorUnico(periodoOrcamentoCargo, relacaocargo.getQuantidade(), mapaQuantidadeTotal);						
					}
					else {
						listaPeriodoOrcamento = montaListaPeriodoOrcamentoValorPorFaixa(periodoOrcamentoCargo, relacaocargo.getListaDependenciafaixa(), mapaQuantidadeTotal);
					}
					periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);					
					mapaHistograma.put(relacaocargo.getCargo(), periodoOrcamentoCargo);
				}
			}
		}
		return mapaHistograma;
	}
	
	/***
	 * Calcula os valores referentes aos cargos presentes na listaRelacaoCargo cujo tipo de depend�ncia seja SELECAO_CARGOS
	 * e acrescenta no mapaHistograma.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#ordenaListaSelecaoCargo(List)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#calculaTotalMDO(Map, List, Boolean)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#montaListaPeriodoOrcamentoValorUnico(Periodoorcamentocargo, Double, Map)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#montaListaPeriodoOrcamentoValorPorFaixa(Periodoorcamentocargo, List, Map)
	 * 
	 * @param orcamento
	 * @param mapaHistograma
	 * @param listaRelacaoCargo
	 * @return List<Periodoorcamentocargo>
	 * 
	 * @throws SinedException - caso haja algum erro no c�lculo dos valores
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Map<Cargo, Periodoorcamentocargo> calculaValoresRelacaoCargoDependenciaSelecaoCargo(Orcamento orcamento, Map<Cargo, Periodoorcamentocargo> mapaHistograma, List<Relacaocargo> listaRelacaoCargo) {
		
		//Listas
		List<Periodoorcamento> listaPeriodoOrcamento;
		List<Cargo> listaCargo;
		
		//Mapas
		Map<Integer, Double> mapaQuantidadeTotal;
		
		//Beans
		Periodoorcamentocargo periodoOrcamentoCargo;
		
		if (listaRelacaoCargo != null) {
			
			//Ordena a lista de rela��es entre cargos, de modo que um cargo que dependa de outro cargo da lista seja calculado por �ltimo
			listaRelacaoCargo = ordenaListaSelecaoCargo(listaRelacaoCargo);
			
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if (relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
					
					//Verifica se j� existe o cargo no Histograma
					if (mapaHistograma.get(relacaocargo.getCargo()) != null) {
						throw new SinedException("O cargo " + relacaocargo.getCargo().getNome() + " n�o pode simultaneamente ser recurso de uma tarefa e depender de outros cargos.");
					}
					
					periodoOrcamentoCargo = new Periodoorcamentocargo();
					periodoOrcamentoCargo.setCargo(relacaocargo.getCargo());
					periodoOrcamentoCargo.setCargohorasemanal(relacaocargo.getCargo().getTotalhorasemana());					
					periodoOrcamentoCargo.setOrcamento(orcamento);
					
					listaCargo = new ArrayList<Cargo>();
					if (relacaocargo.getListaDependenciacargo() != null) {
						for (Dependenciacargo dependenciaCargo : relacaocargo.getListaDependenciacargo()) {
							listaCargo.add(dependenciaCargo.getCargo());
						}
					}
					
					//Calcula a quantidade dos cargos selecionados por per�odo
					mapaQuantidadeTotal = calculaTotalMDO(mapaHistograma, listaCargo, false);					
					
					//Verifica qual o tipo de valor
					if (relacaocargo.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
						listaPeriodoOrcamento = montaListaPeriodoOrcamentoValorUnico(periodoOrcamentoCargo, relacaocargo.getQuantidade(), mapaQuantidadeTotal);						
					}
					else {
						listaPeriodoOrcamento = montaListaPeriodoOrcamentoValorPorFaixa(periodoOrcamentoCargo, relacaocargo.getListaDependenciafaixa(), mapaQuantidadeTotal);
					}
					periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);					
					mapaHistograma.put(relacaocargo.getCargo(), periodoOrcamentoCargo);
				}
			}
		}
		return mapaHistograma;
	}
	
	/***
	 * Ordena a lista de rela��o entre os cargos de acordo com a ordem de depend�ncia entre os mesmos
	 * 
	 * @see br.com.linkcom.sined.geral.service.RelacaocargoService#verificaDependenciaCiclica(Relacaocargo, List, List)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#existeDependencia(Cargo, Cargo, List)
	 * 	 
	 * @param listaRelacaoCargo
	 * @return verdadeiro ou falso
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private List<Relacaocargo> ordenaListaSelecaoCargo(List<Relacaocargo> listaRelacaoCargo) {
		List<Relacaocargo> listaRelacaoCargoOrdenada = new ArrayList<Relacaocargo>();
		int index;
		boolean elementoInserido;
//		String msg;
		
//		//A DEPEND�NCIA C�CLICA SER� VERIFICADA AO SALVAR UMA RELA��O ENTRE CARGOS		
//		//Verifica se existe depend�ncia c�clica entre os cargos presentes na listaRelacaoCargo
//		msg = relacaocargoService.existeDependenciaCiclica(listaRelacaoCargo);
//		if (!msg.equals("")) {
//			throw new SinedException(msg);
//		}
		
		for (Relacaocargo relacaoCargo : listaRelacaoCargo) {
			if (relacaoCargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
				
				if (listaRelacaoCargoOrdenada.isEmpty()) {
					listaRelacaoCargoOrdenada.add(relacaoCargo);
				}
				else {
					index = 0;
					elementoInserido = false;
					
					for (Relacaocargo relacaoCargoOrdenada : listaRelacaoCargoOrdenada) {
						//Verifica se existe a depend�ncia entre o cargo A e o cargo B, ou seja, se B � dependente de A.
						//Caso afirmativo A deve vir antes de B.
						if (existeDependencia(relacaoCargo.getCargo(), relacaoCargoOrdenada.getCargo(), listaRelacaoCargo)) {
							listaRelacaoCargoOrdenada.add(index, relacaoCargo);
							elementoInserido = true;
							break;
						}
						index++;
					}
					
					//Se n�o houver depend�ncia alguma entre o cargo a ser inserido e os cargos presentes na lista,
					//o cargo pode ser inserido no final da mesma.
					if (!elementoInserido) {
						listaRelacaoCargoOrdenada.add(relacaoCargo);
					}
				}
			}
		}
		return listaRelacaoCargoOrdenada;
	}
	
	/***
	 * M�todo recursivo que verifica se cargoDependente depende direta ou indiretamente de cargoOrigem, dada uma lista de rela��o entre cargos 
	 * 	 
	 * @param cargoOrigem
	 * @param cargoDependente
	 * @param listaRelacaoCargo
	 * @return verdadeiro ou falso
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Boolean existeDependencia(Cargo cargoOrigem, Cargo cargoDependente, List<Relacaocargo> listaRelacaoCargo) {
		if (listaRelacaoCargo != null) {
			for (Relacaocargo relacaoCargo : listaRelacaoCargo) {
				if (relacaoCargo.getCargo().equals(cargoDependente)) {
					if (relacaoCargo.getListaDependenciacargo() != null) {
						for (Dependenciacargo dependenciaCargo : relacaoCargo.getListaDependenciacargo()) {
							if (dependenciaCargo.getCargo().equals(cargoOrigem)) {
								return true;
							}
							else {
								if (existeDependencia(cargoOrigem, dependenciaCargo.getCargo(), listaRelacaoCargo)) {
									return true;
								}								
							}
						}
					}
				}
			}
		}
		return false;
	}	
	
	/***
	 * Monta a lista de necessidade de m�o-de-obra por per�odo 
	 * baseada na rela��o 1/n (par�metro quantidade) e na quantidade de m�o-de-obra contida no par�metro mapaQuantidadeTotal.
	 * 	 
	 * @param periodoOrcamentoCargo
	 * @param quantidade
	 * @param mapaQuantidadeTotal
	 * @return List<Periodoorcamento>
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private List<Periodoorcamento> montaListaPeriodoOrcamentoValorUnico(Periodoorcamentocargo periodoOrcamentoCargo, Double quantidade, Map<Integer, Double> mapaQuantidadeTotal) {
		List<Periodoorcamento> listaPeriodoOrcamento = new ArrayList<Periodoorcamento>();
		Periodoorcamento periodoOrcamento;
		Double quantidadeTotal;
		
		for (int i = 1; i <= periodoOrcamentoCargo.getOrcamento().getPrazosemana(); i++) {
			periodoOrcamento = new Periodoorcamento();
			periodoOrcamento.setNumero(i);
			periodoOrcamento.setPeriodoorcamentocargo(periodoOrcamentoCargo);
			quantidadeTotal = mapaQuantidadeTotal.get(Integer.valueOf(i));
			if (quantidadeTotal == null) {
				quantidadeTotal = 0.0;
			}
			periodoOrcamento.setQtde(quantidadeTotal / quantidade);
			
			listaPeriodoOrcamento.add(periodoOrcamento);
		}
		periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);		
		
		return listaPeriodoOrcamento;
	}
	
	/***
	 * Monta a lista de necessidade de m�o-de-obra por per�odo 
	 * baseada na rela��o de faixas de valores (par�metro listaDependenciaFaixa) e na quantidade de m�o-de-obra contida no par�metro mapaQuantidadeTotal.
	 * 	 
	 * @param periodoOrcamentoCargo
	 * @param qtdehoras
	 * @param mapaQuantidadeTotal
	 * @return List<Periodoorcamento>
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private List<Periodoorcamento> montaListaPeriodoOrcamentoValorPorFaixa(Periodoorcamentocargo periodoOrcamentoCargo, List<Dependenciafaixa> listaDependenciaFaixa, Map<Integer, Double> mapaQuantidadeTotal) {
		List<Periodoorcamento> listaPeriodoOrcamento = new ArrayList<Periodoorcamento>();
		Periodoorcamento periodoOrcamento;
		Double quantidadeTotalPeriodo;
		Double quantidadeCargoPeriodo;
		Double faixaDe;
		Double faixaAte;
		
		for (int i = 1; i <= periodoOrcamentoCargo.getOrcamento().getPrazosemana(); i++) {
			periodoOrcamento = new Periodoorcamento();
			periodoOrcamento.setNumero(i);
			periodoOrcamento.setPeriodoorcamentocargo(periodoOrcamentoCargo);
			
			quantidadeTotalPeriodo = mapaQuantidadeTotal.get(Integer.valueOf(i));
			if (quantidadeTotalPeriodo == null) {
				quantidadeTotalPeriodo = 0.0;
			}
			
			quantidadeCargoPeriodo = 0d;
			if (listaDependenciaFaixa != null) {
				for (Dependenciafaixa dependenciaFaixa : listaDependenciaFaixa) {
					faixaDe  = dependenciaFaixa.getFaixade();
					faixaAte = dependenciaFaixa.getFaixaate() != null ? dependenciaFaixa.getFaixaate() : Double.MAX_VALUE;

					//O primeiro valor que satisfizer a rela��o t� dentro.
					if (quantidadeTotalPeriodo >= faixaDe && quantidadeTotalPeriodo <= faixaAte) {
						quantidadeCargoPeriodo = dependenciaFaixa.getQuantidade();
						break;
					}
				}
			}			
			periodoOrcamento.setQtde(quantidadeCargoPeriodo);			
			listaPeriodoOrcamento.add(periodoOrcamento);
		}
		periodoOrcamentoCargo.setListaPeriodoorcamento(listaPeriodoOrcamento);		
		
		return listaPeriodoOrcamento;
	}	
	
	/***
	 * Calcula a quantidade total de m�o-de-obra por per�odo do or�amento.
	 * Pode ser filtrada por alguns cargos (par�metro listaCargo) ou somente m�o-de-obra direta (par�metro somenteMOD)
	 * 
	 * @param mapaHistograma
	 * @param listaCargo
	 * @param somenteMOD
	 * @return Map<Integer, Integer> - mapa da quantidade por per�odo
	 * 
	 * @author Rodrigo Alvarenga
	 */		
	private Map<Integer, Double> calculaTotalMDO(Map<Cargo, Periodoorcamentocargo> mapaHistograma, List<Cargo> listaCargo, Boolean somenteMOD) {
		Map<Integer, Double> mapaQuantidadeTotal = new HashMap<Integer, Double>();
		Double total;
		Periodoorcamentocargo periodoOrcamentoCargo;
		Set<Cargo> keySet;
		
		if (mapaHistograma != null) {
			keySet = mapaHistograma.keySet();
			for (Cargo cargo : keySet) {
				
				if ((listaCargo == null || listaCargo.contains(cargo)) && (!somenteMOD || cargo.getTipocargo().equals(Tipocargo.MOD))) {
					periodoOrcamentoCargo = mapaHistograma.get(cargo);
					
					if (periodoOrcamentoCargo != null) {
						if (periodoOrcamentoCargo.getListaPeriodoorcamento() != null) {
							for (Periodoorcamento periodoOrcamento : periodoOrcamentoCargo.getListaPeriodoorcamento()) {
								total = mapaQuantidadeTotal.get(periodoOrcamento.getNumero());
								if (total == null) {
									total = 0.0;
								}
								
								total += periodoOrcamento.getQtde();
								
								mapaQuantidadeTotal.put(periodoOrcamento.getNumero(), total);
							}
						}
					}
				}				
			}
		}		
		return mapaQuantidadeTotal;
	}
	
	/***
	 * Insere/atualiza os registros presentes em listaPeriodoOrcamentoCargoForUpdate.
	 * Apaga os registros presentes em listaPeriodoOrcamentoCargoForDelete.
	 * Atualiza os registros na tabela Orcamentorecursohumano
	 * Atualiza os registros na tabela Recursocomposicao
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#atualizaValoresHistograma(List, List)
	 * 
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargoApp
	 * @return 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveListaPeriodoOrcamentoCargoFlex(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoApp) {
		
		if (orcamento == null || orcamento.getCdorcamento() == null || listaPeriodoOrcamentoCargoApp == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoForUpdate = new ArrayList<Periodoorcamentocargo>();
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoForDelete = new ArrayList<Periodoorcamentocargo>();		
		List<Periodoorcamento> listaPeriodoOrcamentoForUpdate;		
		Periodoorcamento periodoOrcamentoForUpdate;
		
		boolean existePeriodoOrcamentoCargo;
		boolean existePeriodoOrcamento;
		int i, j;
		int indexPeriodoOrcamentoCargo;
		int indexPeriodoOrcamento;
		
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoBanco = findByOrcamentoFlex(orcamento);
		
		if (listaPeriodoOrcamentoCargoBanco != null) {
			for (Periodoorcamentocargo periodoOrcamentoCargoBanco : listaPeriodoOrcamentoCargoBanco) {
				existePeriodoOrcamentoCargo = false;
				indexPeriodoOrcamentoCargo  = -1;
				
				//Verifica se existe o cargo na lista que veio da aplica��o
				i = 0;
				for (Periodoorcamentocargo periodoOrcamentoCargoApp : listaPeriodoOrcamentoCargoApp) {
					if (periodoOrcamentoCargoApp.getCargo().equals(periodoOrcamentoCargoBanco.getCargo())) {
						
						periodoOrcamentoCargoBanco.setOrcamento(orcamento);
						periodoOrcamentoCargoBanco.setCargohorasemanal(periodoOrcamentoCargoApp.getCargohorasemanal());
						periodoOrcamentoCargoBanco.setTotalhoracalculada(periodoOrcamentoCargoApp.getTotalhoracalculada());
						
						listaPeriodoOrcamentoForUpdate = new ArrayList<Periodoorcamento>();
						
						//Verifica se existem os per�odos na lista que veio da aplica��o
						for (Periodoorcamento periodoOrcamentoBanco : periodoOrcamentoCargoBanco.getListaPeriodoorcamento()) {
							
							existePeriodoOrcamento = false;
							indexPeriodoOrcamento  = -1;
							
							j = 0;							
							for (Periodoorcamento periodoOrcamentoApp : periodoOrcamentoCargoApp.getListaPeriodoorcamento()) {
								if (periodoOrcamentoApp.getNumero().equals(periodoOrcamentoBanco.getNumero())) {
									
									periodoOrcamentoForUpdate = new Periodoorcamento();
									periodoOrcamentoForUpdate.setCdperiodoorcamento(periodoOrcamentoBanco.getCdperiodoorcamento());
									periodoOrcamentoForUpdate.setNumero(periodoOrcamentoBanco.getNumero());									
									periodoOrcamentoForUpdate.setQtde(periodoOrcamentoApp.getQtde());
									listaPeriodoOrcamentoForUpdate.add(periodoOrcamentoForUpdate);
									
									indexPeriodoOrcamento  = j;
									existePeriodoOrcamento = true;									
									break;
								}
								j++;
							}
							
							if (existePeriodoOrcamento) {
								//Remove o elemento da lista da aplica��o, j� que ele foi adicionado na lista para atualiza��o								
								periodoOrcamentoCargoApp.getListaPeriodoorcamento().remove(indexPeriodoOrcamento);
							}
						}
						
						//Adiciona os per�odos que ainda existem na aplica��o para o cargo
						for (Periodoorcamento periodoOrcamentoApp : periodoOrcamentoCargoApp.getListaPeriodoorcamento()) {
							periodoOrcamentoApp.setCdperiodoorcamento(null);
							listaPeriodoOrcamentoForUpdate.add(periodoOrcamentoApp);
						}						
						
						periodoOrcamentoCargoBanco.setListaPeriodoorcamento(listaPeriodoOrcamentoForUpdate);
						indexPeriodoOrcamentoCargo  = i; 
						existePeriodoOrcamentoCargo = true;
						break;
					}
					i++;
				}
				
				if (existePeriodoOrcamentoCargo) {
					listaPeriodoOrcamentoCargoForUpdate.add(periodoOrcamentoCargoBanco);
					
					//Remove o elemento da lista da aplica��o, j� que ele foi adicionado na lista para atualiza��o
					listaPeriodoOrcamentoCargoApp.remove(indexPeriodoOrcamentoCargo);
				}
				else {
					//Adiciona o cargo na lista para exclus�o
					listaPeriodoOrcamentoCargoForDelete.add(periodoOrcamentoCargoBanco);
				}
			}
		}
		
		//Adiciona os cargos que ainda existem na lista da aplica��o
		for (Periodoorcamentocargo periodoOrcamentoCargoApp : listaPeriodoOrcamentoCargoApp) {
			periodoOrcamentoCargoApp.setCdperiodoorcamentocargo(null);
			periodoOrcamentoCargoApp.setOrcamento(orcamento);
			
			for (Periodoorcamento periodoOrcamentoApp : periodoOrcamentoCargoApp.getListaPeriodoorcamento()) {
				periodoOrcamentoApp.setCdperiodoorcamento(null);
			}
			
			listaPeriodoOrcamentoCargoForUpdate.add(periodoOrcamentoCargoApp);
		}
		
		//Atualiza os valores no banco de dados
		periodoorcamentocargoDAO.saveHistograma(orcamento, listaPeriodoOrcamentoCargoForUpdate, listaPeriodoOrcamentoCargoForDelete);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PeriodoorcamentocargoDAO#findByOrcamento
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Periodoorcamentocargo> findByOrcamento(Orcamento orcamento){
		return periodoorcamentocargoDAO.findByOrcamento(orcamento);		
	}

	/**
	 * Prepara o arquivo excel para exibi��o do histograma.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createHeaderDataGrid
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createDetalheDataGrid
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createFooterDataGrid
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createDetalheDataGridIndireta
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createFooterDataGridIndireta
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createFooterTotal
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createFooterTotal2
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createHeaderRelat�rio
	 * @param orcamento
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoExcelFormatado(Orcamento orcamento) throws IOException {
		
		SinedExcel e = new SinedExcel();
		
		Integer linhasAbaixo = 1;
		Integer numeroSemanas = 0;
		
		Integer diretaDe = 0;
		Integer diretaAte = 0;
		Integer indiretaDe = 0;
		Integer indiretaAte = 0;
		
		List<Periodoorcamentocargo> listaDireta = new ArrayList<Periodoorcamentocargo>();
		List<Periodoorcamentocargo> listaIndireta = new ArrayList<Periodoorcamentocargo>();
		
		List<Periodoorcamentocargo> lista = this.findByOrcamento(orcamento);
		for (Periodoorcamentocargo periodoorcamentocargo : lista) {
			if(periodoorcamentocargo.getCargo().getTipocargo().getCdtipocargo().equals(Tipocargo.MOD.getCdtipocargo())){
				listaDireta.add(periodoorcamentocargo);
			} else {
				listaIndireta.add(periodoorcamentocargo);
			}
		}

		HSSFSheet p = e.createSheet("Histograma");
		
		p.setDefaultColumnWidth((short) 5);
		p.setDisplayGridlines(false);
		
//		SETANDO OS TAMANHOS DAS COLUNAS
		p.setColumnWidth((short) 0, (short) (257*35));
		p.setColumnWidth((short) 1, (short) (91*35));
		p.setColumnWidth((short) 2, (short) (91*35));
		p.setColumnWidth((short) 3, (short) (91*35));
		p.setColumnWidth((short) 4, (short) (91*35));

		HSSFRow row = null;
		HSSFCell cell = null;
		
		Periodoorcamentocargo poc;
		int i = 0;
		int j = 0;
		for (i = 0; i < listaDireta.size(); i++) {
			if(i == 0){
				this.createHeaderDataGrid(linhasAbaixo, p);
			}
			poc = listaDireta.get(i);
			
			numeroSemanas = poc.getListaPeriodoorcamento().size();
			
			row = p.createRow(linhasAbaixo + i + 6);
			row.setHeightInPoints(22);
			
			j = this.createDetalheDataGrid(linhasAbaixo, p, row, poc, i);
		}
		
		
		if(listaDireta.size() > 0){
			row = p.createRow(linhasAbaixo + i + 6);
			row.setHeightInPoints(20);
			
			diretaDe = linhasAbaixo + 7;
			diretaAte = linhasAbaixo + i + 6;
			
			this.createFooterDataGrid(linhasAbaixo, row, i, j);
		}
		
		Integer numAbaixo = listaDireta.size() + linhasAbaixo + 6 + 1;
		Integer index = 0;
		for (i = numAbaixo; i < listaIndireta.size()+numAbaixo; i++) {
			if(numAbaixo.equals(i)){
				p.addMergedRegion(new Region(i,(short)0, i,(short)(numeroSemanas + 4)));
				
				row = p.createRow(i);
				row.setHeightInPoints(20);
				
				for (int q = 0; q <= numeroSemanas + 4; q++){
					cell = row.createCell((short)q);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					if(q==0) cell.setCellValue("M�o-de-Obra Indireta");
				}
			}
			
			poc = listaIndireta.get(index);
			index++;
			
			row = p.createRow(i+1);
			row.setHeightInPoints(20);
			
			j = this.createDetalheDataGridIndireta(row, poc, i);
		}
		
		if(listaIndireta.size() > 0){
			row = p.createRow(i+1);
			row.setHeightInPoints(20);
			
			indiretaDe = numAbaixo + 2;
			indiretaAte = i+1;
			
			this.createFooterDataGridIndireta(row, i, j, numAbaixo);
		}
		
		if(listaDireta.size() > 0 && listaIndireta.size() > 0){
			row = p.createRow(listaDireta.size() + listaIndireta.size() + linhasAbaixo + 9);
			row.setHeightInPoints(20);
			
			this.createFooterTotal(diretaDe, diretaAte, indiretaDe, indiretaAte, row, j);

		} else if(listaDireta.size() > 0) {
			row = p.createRow(listaDireta.size() + linhasAbaixo + 7);
			row.setHeightInPoints(20);
			
			this.createFooterTotal2(diretaDe, diretaAte, row, j);
		}
		
		p.addMergedRegion(new Region(0,(short)0, 0,(short)(numeroSemanas + 4)));
		p.addMergedRegion(new Region(1,(short)0, 1,(short)(numeroSemanas + 4)));
		
		row = p.createRow(0);
		row.setHeightInPoints(45);
		
		this.createHeaderRelat�rio(numeroSemanas, p, row);
		
		return e.getWorkBookResource("histograma_" + SinedUtil.datePatternForReport() + ".xls");
	}

	/**
	 * Cria o header do relat�rio em Excel.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal
	 * @param numeroSemanas
	 * @param p
	 * @param row
	 * @author Rodrigo Freitas
	 */
	private void createHeaderRelat�rio(Integer numeroSemanas, HSSFSheet p, HSSFRow row) {
		HSSFCell cell;
		for (int k = 0; k <= numeroSemanas + 4; k++) {
			cell = row.createCell((short)k);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
			if(k==0) cell.setCellValue(empresaService.loadPrincipal().getRazaosocialOuNome());
		}
		
		row = p.createRow(1);
		row.setHeightInPoints(45);
		
		for (int k = 0; k <= numeroSemanas + 4; k++) {
			cell = row.createCell((short)k);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
			if(k==0) cell.setCellValue("HISTOGRAMA");
		}
	}

	/**
	 * Cria o footer do relat�rio quando somente tem uma lista de MOD.
	 *
	 * @param diretaDe
	 * @param diretaAte
	 * @param row
	 * @param j
	 * @author Rodrigo Freitas
	 */
	private void createFooterTotal2(Integer diretaDe, Integer diretaAte, HSSFRow row, int j) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
		cell.setCellValue("Total Geral");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
//			cell.setCellFormula("SUM(B" + (diretaDe) + ":" + "B" + (diretaAte) + ")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		cell.setCellFormula("SUM(C" + (diretaDe) + ":" + "C" + (diretaAte) + ")");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		cell.setCellFormula("SUM(E" + (diretaDe) + ":" + "E" + (diretaAte) + ")");
		
		for (int q = 5; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
			cell.setCellFormula("SUM("+ SinedExcel.getAlgarismoColuna(q) + (diretaDe) + ":" + SinedExcel.getAlgarismoColuna(q) + (diretaAte) + ")");
		}
	}

	/**
	 * Cria o footer geral do relat�rio quando tiver tanto lista de MOI quanto de MOD.
	 *
	 * @param diretaDe
	 * @param diretaAte
	 * @param indiretaDe
	 * @param indiretaAte
	 * @param row
	 * @param j
	 * @author Rodrigo Freitas
	 */
	private void createFooterTotal(Integer diretaDe, Integer diretaAte,
			Integer indiretaDe, Integer indiretaAte, HSSFRow row, int j) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
		cell.setCellValue("Total Geral");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
//			cell.setCellFormula("SUM(B" + (diretaDe) + ":" + "B" + (diretaAte) + ";B" + (indiretaDe) + ":" + "B" + (indiretaAte) + ")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		cell.setCellFormula("SUM(C" + (diretaDe) + ":" + "C" + (diretaAte) + ";C" + (indiretaDe) + ":" + "C" + (indiretaAte) + ")");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
		cell.setCellFormula("SUM(E" + (diretaDe) + ":" + "E" + (diretaAte) + ";E" + (indiretaDe) + ":" + "E" + (indiretaAte) + ")");
		
		for (int q = 5; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_CENTER);
			cell.setCellFormula("SUM("+ SinedExcel.getAlgarismoColuna(q) + (diretaDe) + ":" + SinedExcel.getAlgarismoColuna(q) + (diretaAte) + ";" + SinedExcel.getAlgarismoColuna(q) + (indiretaDe) + ":" + SinedExcel.getAlgarismoColuna(q) + (indiretaAte) + ")");
		}
	}

	/**
	 * Cria o somat�rio da lista de cargo indiretos.
	 * 
	 * @see br.com.linkcom.sined.util.controller.SinedExcel#getAlgarismoColuna
	 * @param row
	 * @param i
	 * @param j
	 * @param numAbaixo
	 * @author Rodrigo Freitas
	 */
	private void createFooterDataGridIndireta(HSSFRow row, int i, int j,
			Integer numAbaixo) {
		HSSFCell cell;
		cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
		cell.setCellValue("Total M�o-de-Obra Indireta");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
//			cell.setCellFormula("SUM(B" + (numAbaixo + 2) + ":" + "B" + (i+1) + ")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellFormula("SUM(C" + (numAbaixo + 2) + ":" + "C" + (i+1) + ")");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellFormula("SUM(E" + (numAbaixo + 2) + ":" + "E" + (i+1) + ")");
		
		for (int q = 5; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
			cell.setCellFormula("SUM("+ SinedExcel.getAlgarismoColuna(q) + (numAbaixo + 2) + ":" + SinedExcel.getAlgarismoColuna(q) + (i+1) + ")");
		}
	}

	/**
	 * Cria o detalhe do datagrid da lista de cargos indiretos.
	 *
	 * @param row
	 * @param poc
	 * @param i
	 * @return
	 * @author Rodrigo Freitas
	 */
	private int createDetalheDataGridIndireta(HSSFRow row, Periodoorcamentocargo poc, int i) {
		HSSFCell cell;
		Periodoorcamento po;
		int j;
		cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(poc.getCargo().getNome());
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		if(poc.getTotalhoracalculada() != null) cell.setCellValue(poc.getTotalhoracalculada());
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(poc.getCargohorasemanal());
		
		for (j = 0; j < poc.getListaPeriodoorcamento().size(); j++) {
			po = poc.getListaPeriodoorcamento().get(j);
			
			cell = row.createCell((short)(5 + j));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(po.getQtde());
		}
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellFormula("SUM(F" + (i+2) + ":" + SinedExcel.getAlgarismoColuna(j+5-2) + (i+2) +")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellFormula("D" + (i+2) + "*E" + (i+2));
		return j;
	}

	/**
	 * Cria o footer do datagrid da lista de cargos diretos.
	 *
	 * @param linhasAbaixo
	 * @param row
	 * @param i
	 * @param j
	 * @author Rodrigo Freitas
	 */
	private void createFooterDataGrid(Integer linhasAbaixo, HSSFRow row, int i, int j) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
		cell.setCellValue("Total M�o-de-Obra Direta");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
//			cell.setCellFormula("SUM(B" + (linhasAbaixo + 7) + ":" + "B" + (linhasAbaixo + i + 6) + ")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellFormula("SUM(C" + (linhasAbaixo + 7) + ":" + "C" + (linhasAbaixo + i + 6) + ")");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellFormula("SUM(E" + (linhasAbaixo + 7) + ":" + "E" + (linhasAbaixo + i + 6) + ")");
		
		for (int q = 5; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
			cell.setCellFormula("SUM("+ SinedExcel.getAlgarismoColuna(q) + (linhasAbaixo + 7) + ":" + SinedExcel.getAlgarismoColuna(q) + (linhasAbaixo + i + 6) + ")");
		}
	}

	/**
	 * Cria o detalhe do data grid da lista direta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createLinhaDetalhe
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#createHeaderDataGrid2
	 * @param linhasAbaixo
	 * @param p
	 * @param row
	 * @param poc
	 * @param i
	 * @return
	 * @author Rodrigo Freitas
	 */
	private int createDetalheDataGrid(Integer linhasAbaixo, HSSFSheet p, HSSFRow row, Periodoorcamentocargo poc, int i) {
		HSSFCell cell;
		int j;
		cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(poc.getCargo().getNome());
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		if(poc.getTotalhoracalculada() != null) cell.setCellValue(poc.getTotalhoracalculada());
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		if(poc.getCargohorasemanal() != null) cell.setCellValue(poc.getCargohorasemanal());
		
		for (j = 0; j < poc.getListaPeriodoorcamento().size(); j++) {
			createLinhaDetalhe(linhasAbaixo, p, poc, i, j);
		}
		
		if (i == 0){
			createHeaderDataGrid2(linhasAbaixo, p, j);
		}
		
		row = p.getRow(linhasAbaixo + i + 6);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellFormula("SUM(F" + (linhasAbaixo + i + 6 + 1) + ":" + SinedExcel.getAlgarismoColuna(j+5-1) + (linhasAbaixo + i + 6 + 1) +")");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellFormula("D" + (linhasAbaixo + i + 6 + 1) + "*E" + (linhasAbaixo + i + 6 + 1));
		return j;
	}

	/**
	 * Segunda parte da cria��o do header do datagrid.
	 *
	 * @param linhasAbaixo
	 * @param p
	 * @param j
	 * @author Rodrigo Freitas
	 */
	private void createHeaderDataGrid2(Integer linhasAbaixo, HSSFSheet p, int j) {
		HSSFRow row;
		HSSFCell cell;
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)5, linhasAbaixo + 3,(short)(j+5-1)));
		p.addMergedRegion(new Region(linhasAbaixo + 5,(short)0, linhasAbaixo + 5,(short)(j+5-1)));
		
		row = p.getRow(linhasAbaixo + 3);
		
		for (int q = 5; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			if(q==5) cell.setCellValue("Semanas");
		}
		
		row = p.createRow(linhasAbaixo + 5);
		row.setHeightInPoints(20);
		
		for (int q = 0; q <= j+5-1; q++){
			cell = row.createCell((short)q);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			if(q==0) cell.setCellValue("M�o-de-Obra Direta");
		}
	}

	/**
	 * Cria uma linha no detalhe da lista de cargos diretos.
	 *
	 * @param linhasAbaixo
	 * @param p
	 * @param poc
	 * @param i
	 * @param j
	 * @author Rodrigo Freitas
	 */
	private void createLinhaDetalhe(Integer linhasAbaixo, HSSFSheet p, Periodoorcamentocargo poc, int i, int j) {
		HSSFRow row;
		HSSFCell cell;
		Periodoorcamento po = poc.getListaPeriodoorcamento().get(j);
		
		if(i == 0){
			row = p.getRow(linhasAbaixo + 4);
			
			cell = row.createCell((short)(5 + j));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue(po.getNumero());
		}
		
		row = p.getRow(linhasAbaixo + i + 6);
		
		cell = row.createCell((short)(5 + j));
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(po.getQtde());
	}

	/**
	 * Cria o header do datagrid.
	 *
	 * @param linhasAbaixo
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void createHeaderDataGrid(Integer linhasAbaixo, HSSFSheet p) {
		
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)0, linhasAbaixo + 4,(short)0));
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)1, linhasAbaixo + 4,(short)1));
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)2, linhasAbaixo + 4,(short)2));
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)3, linhasAbaixo + 4,(short)3));
		p.addMergedRegion(new Region(linhasAbaixo + 3,(short)4, linhasAbaixo + 4,(short)4));
		
		HSSFRow row = p.createRow(linhasAbaixo + 3);
		row.setHeightInPoints(27);
		
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Fun��o");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Total Hs Calculada");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Total Hs Efetiva");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Hs por semana");
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Total Efetivo (Semanal)");
		
		row = p.createRow(linhasAbaixo + 4);
		row.setHeightInPoints(27);
		
		cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
	}
}

