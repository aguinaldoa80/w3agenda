package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.DominioDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominioService extends GenericService<Dominio>{

	private DominioDAO dominioDAO;
	
	public void setDominioDAO(DominioDAO dominioDAO) {
		this.dominioDAO = dominioDAO;
	}
	
	/**
	 * Retorna uma lista de dom�nios de acordo com o contrato especificado
	 * @see br.com.linkcom.sined.geral.dao.DominioDAO#findByContrato
	 * @param contrato
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominio> findByContrato(Contrato contrato){
		return dominioDAO.findByContrato(contrato);
	}
	
	@Override
	public void saveOrUpdate(final Dominio bean) {
		boolean novoRegistro = false;
		Dominio dominioAnterior = null;
		if(bean.getCddominio() == null){
			novoRegistro = true;
		}else{
			dominioAnterior = load(bean);
		}
		
		//Muda a data da vers�o e incrementa o contador.
		if(dominioAnterior != null){
			bean.setVersao(SinedUtil.mudaVersaoBriefCase(bean.getVersao()));
		}else{
			bean.setVersao(SinedUtil.criaVersaoBriefCase());
		}
		
		super.saveOrUpdate(bean);
		
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCddominio());
		if(bean.getEmail() && (dominioAnterior == null || !dominioAnterior.getEmail()))
			SinedUtil.executaBriefCase(Servicoservidortipo.CAIXAPOSTAL, AcaoBriefCase.CADASTRAR, bean.getCddominio());
		else if(!bean.getEmail() && dominioAnterior != null && dominioAnterior.getEmail())
			SinedUtil.executaBriefCase(Servicoservidortipo.CAIXAPOSTAL, AcaoBriefCase.REMOVER, bean.getCddominio());
	}
	
	@Override
	public void delete(Dominio bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, AcaoBriefCase.REMOVER, bean.getCddominio());
		SinedUtil.executaBriefCase(Servicoservidortipo.CAIXAPOSTAL, AcaoBriefCase.REMOVER, bean.getCddominio());
		super.delete(bean);
	}

	/**
	 * Retorna lista com os dom�nios que podem ser utilizados em e-mail considerando o propriet�rio
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominio> listEmail(Cliente cliente){
		return dominioDAO.listEmail(cliente);
	}

	public int countContratomaterial(Integer cddominio, Contratomaterial contratomaterial) {
		return dominioDAO.countContratomaterial(cddominio, contratomaterial);
	}
	
}
