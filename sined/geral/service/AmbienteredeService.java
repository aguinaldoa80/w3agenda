package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.dao.AmbienteredeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AmbienteredeService extends GenericService<Ambienterede>{
	private AmbienteredeDAO ambienteredeDAO;
	
	public void setAmbienteredeDAO(AmbienteredeDAO ambienteredeDAO) {
		this.ambienteredeDAO = ambienteredeDAO;
	}
	
	/**
	 * Retorna um objeto de acordo com o nome informado
	 * @see br.com.linkcom.sined.geral.dao.AmbienteredeDAO#findByNome
	 * @param nome
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Ambienterede findByNome(String nome){
		return ambienteredeDAO.findByNome(nome);
	}
}
