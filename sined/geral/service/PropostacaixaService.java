package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.syntax.jedit.tokenmarker.PropsTokenMarker;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.dao.PropostaDAO;
import br.com.linkcom.sined.geral.dao.PropostacaixaDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PropostacaixaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PropostacaixaService extends GenericService<Propostacaixa>{
	private PropostacaixaDAO propostacaixaDAO;
	private PropostaDAO propostaDAO;
	
	public void setPropostacaixaDAO(PropostacaixaDAO propostacaixaDAO) {
		this.propostacaixaDAO = propostacaixaDAO;
	}
	public void setPropostaDAO(PropostaDAO propostaDAO) {
		this.propostaDAO = propostaDAO;
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * <p>Recupera uma lista de caixas de proposta com suas respectivas propostas.
	 * Uma caixa pode ter, no m�ximo, 5 propostas.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.PropostacaixaService#findForRelatorioCapaCaixa(String)
	 * @param caixasSelecionadas
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Propostacaixa> findForRelatorioCapaCaixa(String caixasSelecionadas) {
		return this.propostacaixaDAO.findforRelatorioCapaCaixa(caixasSelecionadas);
	}
	
	/**
	 * <p>Gera o relat�rio de capa de caixa de arquivo.</p>
	 * 
	 * @see #findForRelatorioCapaCaixa(String)
	 * @see #processaLista(List)
	 * @param caixasSelecionadas
	 * @return
	 * @author Hugo Ferreira
	 */
	public IReport gerarRelatorioCapaCaixa(String caixasSelecionadas) {
		Report report = new Report("/crm/capaCaixaArquivo");
		Report subCapaCaixa = new Report("/crm/subCapaCaixaArquivo");
		
		List<Propostacaixa> listaPropostaCaixa = this.findForRelatorioCapaCaixa(caixasSelecionadas);
		this.processaLista(listaPropostaCaixa);
		
		
		report.setDataSource(listaPropostaCaixa);
		report.addSubReport("subCapaCaixaArquivo", subCapaCaixa);

		return report;
	}


	/**
	 * Seta a propriedade transiente "ano", de propostacaixa, para exibir
	 * no relat�rio.
	 * 
	 * @param listaPropostaCaixa
	 * @author Hugo Ferreira
	 */
	private void processaLista(List<Propostacaixa> listaPropostaCaixa) {
		String sufixo = null;
		
		for (Propostacaixa propostacaixa : listaPropostaCaixa) {
			if (propostacaixa.getListaProposta() == null || propostacaixa.getListaProposta().isEmpty()) {
				continue;
			}
			sufixo = propostacaixa.getListaProposta().iterator().next().getSufixo();
			propostacaixa.setAno(sufixo);
		}
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * @see br.com.linkcom.sined.geral.dao.PropostacaixaDAO#findListaPropostasCaixa()
	 * @return
	 * @author Simon
	 */
	public List<Propostacaixa> findListaPropostasCaixa() {
		return propostacaixaDAO.findListaPropostasCaixa();
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * @see br.com.linkcom.sined.geral.dao.PropostacaixaDAO#findListaPropostaCaixa(String)
	 * @return
	 * @author Simon
	 */
	public List<Propostacaixa> findListaPropostaCaixa(String whereIn) {
		return propostacaixaDAO.findListaPropostaCaixa(whereIn);
	}

	@Override
	protected ListagemResult<Propostacaixa> findForExportacao(
			FiltroListagem filtro) {
		ListagemResult<Propostacaixa> lista = propostacaixaDAO.findForExportacao(filtro);
		List<Propostacaixa> list = lista.list();
		
		for (Propostacaixa propostacaixa : list) {
			propostacaixa.setListaProposta(propostaDAO.findByPropostacaixa(propostacaixa));
		}
		return lista;
	}
	
	public Resource gerarCSV(PropostacaixaFiltro filtro) {
		StringBuilder csv = new StringBuilder();
		ListagemResult<Propostacaixa> list = findForExportacao(filtro);
		List<Propostacaixa> lista = list.list();
		
		csv.append("\"Descri��o\";\"N�mero(s) da(s) Proposta(s)\";\"Cliente(s)\";\"Descri��o da Proposta\"\n");
		
		for(Propostacaixa p : lista) {
			csv.append("\"" + (p.getNome() != null ? p.getNome() : "") + "\";");
			csv.append("\"" + (p.getNumeroPropostaString() != null ? p.getNumeroPropostaString().replace("<BR>", "\n") : "") + "\";");
			csv.append("\"" + (p.getClientesPropostaString() != null ? p.getClientesPropostaString().replace("<BR>", "\n") : "") + "\";");
			csv.append("\"" + (p.getDescricaoPropostaString() != null ? p.getDescricaoPropostaString().replace("<BR>", "\n") : "") + "\";");
			csv.append("\n");
		}
		
		return new Resource("text/csv", "Propostacaixa.csv", csv.toString().getBytes());
	}
}
