package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfproduto;
import br.com.linkcom.sined.geral.dao.EmpresaconfiguracaonfprodutoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresaconfiguracaonfprodutoService extends GenericService<Empresaconfiguracaonfproduto>{
	EmpresaconfiguracaonfprodutoDAO empresaconfiguracaonfprodutoDAO;
	
	public void setEmpresaconfiguracaonfprodutoDAO(EmpresaconfiguracaonfprodutoDAO empresaconfiguracaonfprodutoDAO) {
		this.empresaconfiguracaonfprodutoDAO = empresaconfiguracaonfprodutoDAO;
	}
	
	public Boolean verificaConfiguracao(){
		return empresaconfiguracaonfprodutoDAO.verificaConfiguracao();
	}
}
