package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.validator.GenericValidator;
import org.hibernate.Hibernate;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialitem;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.bean.Fatorajustevalor;
import br.com.linkcom.sined.geral.bean.Faturamentocontrato;
import br.com.linkcom.sined.geral.bean.Faturamentocontratohistorico;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratomateriallocacaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NotaFiscalTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobccomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipodesconto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.bean.view.Vcontratoqtdelocacao;
import br.com.linkcom.sined.geral.dao.ContratoDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratoRTF;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FaturamentoConjuntoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.RealizarFechamentoRomaneioContratoItem;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ArquivoRetornoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirIndenizacaoContratoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirIndenizacaoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitiracompanhamentometaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.FinalizarContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GerarFaturamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GerarFaturamentoBean.Tipoidentificador;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PagamentoClienteServicoBean;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterialItem;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.ImpostoUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.ArquivoW3erpUtil;
import br.com.linkcom.utils.DateUtils;
import br.com.linkcom.utils.StringUtils;

public class ContratoService extends GenericService<Contrato> {
	
	private ContratoDAO contratoDAO;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ContratohistoricoService contratohistoricoService;
	private NotaContratoService notaContratoService;
	private EmpresaService empresaService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ContratoparcelaService contratoparcelaService;
	private IndicecorrecaoService indicecorrecaoService;
	private DocumentotipoService documentotipoService;
	private RateioitemService rateioitemService;
	private TaxaService taxaService;
	private DocumentoService documentoService;
	private RateioService rateioService;
	private DocumentohistoricoService documentohistoricoService;
	private DocumentoorigemService documentoorigemService;
	private DocumentocomissaoService documentocomissaoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private FaturamentocontratoService faturamentocontratoService;
	private ParametrogeralService parametrogeralService;
	private ContareceberService contareceberService;
	private ContaService contaService;
	private EnvioemailService envioemailService;
	private FatorajusteService fatorajusteService;
	private ComissionamentoService comissionamentoService;
	private ContratocolaboradorService contratocolaboradorService;
	private ContratoService contratoService;
	private ContratodescontoService contratodescontoService;
	private FaturamentohistoricoService faturamentohistoricoService;
	private NotaDocumentoService notaDocumentoService;
	private ContratomaterialService contratomaterialService;
	private ColaboradorService colaboradorService;
	private PessoaService pessoaService;
	private ParcelamentoService parcelamentoService;
	private ClienteService clienteService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService; 
	private PessoacategoriaService pessoacategoriaService;
	private MaterialService materialService;
	private ApontamentoService apontamentoService;
	private RequisicaohistoricoService requisicaohistoricoService;
	private RequisicaoService requisicaoService;
	private NotaService notaService;
	private VcontratoqtdelocacaoService vcontratoqtdelocacaoService;
	private ContagerencialService contagerencialService;
	private CentrocustoService centrocustoService;
	private FaturamentocontratohistoricoService faturamentocontratohistoricoService;
	private RomaneioService romaneioService;
	private PrazopagamentoService prazopagamentoService;
	private ConfiguracaonfeService configuracaonfeService;
	private TaxaitemService taxaitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MovpatrimonioService movpatrimonioService;
	private EnderecoService enderecoService;
	private ContacarteiraService contacarteiraService;
	private FrequenciaService frequenciaService;
	private ProjetoService projetoService;
	private PessoaContatoService pessoaContatoService;
	private PatrimonioitemService patrimonioitemService;
	
	public void setFrequenciaService(FrequenciaService frequenciaService) {this.frequenciaService = frequenciaService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setFaturamentocontratohistoricoService(FaturamentocontratohistoricoService faturamentocontratohistoricoService) {this.faturamentocontratohistoricoService = faturamentocontratohistoricoService;}
	public void setVcontratoqtdelocacaoService(VcontratoqtdelocacaoService vcontratoqtdelocacaoService) {this.vcontratoqtdelocacaoService = vcontratoqtdelocacaoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setParcelamentoService(ParcelamentoService parcelamentoService) {this.parcelamentoService = parcelamentoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setFaturamentohistoricoService(FaturamentohistoricoService faturamentohistoricoService) {this.faturamentohistoricoService = faturamentohistoricoService;}
	public void setContratodescontoService(ContratodescontoService contratodescontoService) {this.contratodescontoService = contratodescontoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setDocumentoService(DocumentoService documentoService) { this.documentoService = documentoService; }
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaContratoService(NotaContratoService notaContratoService) {this.notaContratoService = notaContratoService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setContratoDAO(ContratoDAO contratoDAO) {this.contratoDAO = contratoDAO;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {	this.documentohistoricoService = documentohistoricoService;	}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setContratoparcelaService(ContratoparcelaService contratoparcelaService) {this.contratoparcelaService = contratoparcelaService;}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {this.indicecorrecaoService = indicecorrecaoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setFaturamentocontratoService(FaturamentocontratoService faturamentocontratoService) {this.faturamentocontratoService = faturamentocontratoService;}	
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setFatorajusteService(FatorajusteService fatorajusteService) {this.fatorajusteService = fatorajusteService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setContratocolaboradorService(ContratocolaboradorService contratocolaboradorService) {this.contratocolaboradorService = contratocolaboradorService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {this.contratomaterialService = contratomaterialService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setMaterialtabelaprecoitemService(MaterialtabelaprecoitemService materialtabelaprecoitemService) {this.materialtabelaprecoitemService = materialtabelaprecoitemService;}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {this.pessoacategoriaService = pessoacategoriaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}
	public void setRequisicaohistoricoService(RequisicaohistoricoService requisicaohistoricoService) {this.requisicaohistoricoService = requisicaohistoricoService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setTaxaitemService(TaxaitemService taxaitemService) {this.taxaitemService = taxaitemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {this.movpatrimonioService = movpatrimonioService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {this.patrimonioitemService = patrimonioitemService;}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#updateAux
	 * 
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		contratoDAO.updateAux();
	}
	
	public void updateAux(Integer cdcontrato) {
		contratoDAO.updateAux(cdcontrato);
	}
	
	/* singleton */
	private static ContratoService instance;
	public static ContratoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContratoService.class);
		}
		return instance;
	}

	
	@Override
	public Contrato loadForEntrada(Contrato bean) {
		Contrato contrato = super.loadForEntrada(bean);
		if(contrato != null){
			contrato.setListaContratocolaborador(contratocolaboradorService.findForEntradaByContrato(contrato.getCdcontrato().toString()));
		}
		return contrato;
	}

	/**
	 * Verifica se o contrato possui comissionamento.
	 * Caso positivo cria-se o(s) comissionamento(s) conforme regras de neg�cio definidas no UC.
	 * 
	 * @param documentos
	 * @author Taidson
	 * @since 05/11/2010
	 */
	public void verificaComissionamento(List<Documento> documentos){
		Integer contVendedor = 1;
		Integer contColaborador = 1;
		for (Documento doc : documentos) {
			List<Contrato> listaContrato = doc.getListaContrato();
			if(listaContrato != null && listaContrato.size() > 0){
				for (Contrato contrato : listaContrato) {
					this.comissinamentoDocumentoContrato(doc, contrato, contVendedor, contColaborador);
				}
			} else {
				Contrato contrato = doc.getContrato();
				this.comissinamentoDocumentoContrato(doc, contrato, contVendedor, contColaborador);
			}
		}
	}	
	
	private void comissinamentoDocumentoContrato(Documento doc, Contrato contrato, Integer contVendedor, Integer contColaborador){
		if(contrato != null && contrato.getCdcontrato() != null){
			
			if(contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null 
				&& contrato.getComissionamento() != null && contrato.getComissionamento().getCdcomissionamento() != null){
				
				if(contrato.getComissionamento().getQuantidade() != null && contrato.getComissionamento().getQuantidade() >= contVendedor){
					Documentocomissao comissao = new Documentocomissao();
					comissao.setVendedortipo(contrato.getVendedortipo());
					comissao.setPessoa(contrato.getVendedor());
					comissao.setComissionamento(contrato.getComissionamento());
					comissao.setDocumento(doc);
					comissao.setContrato(contrato);
					documentocomissaoService.saveOrUpdate(comissao);
					contVendedor++;
				}
			}
			
			if(contrato.getListaContratocolaborador() != null && contrato.getListaContratocolaborador().size() > 0){
				
				for (Contratocolaborador item : contrato.getListaContratocolaborador()) {
					if(item.getComissionamento() != null && item.getComissionamento().getCdcomissionamento() != null){
						if(item.getComissionamento().getQuantidade() != null && item.getComissionamento().getQuantidade() >= contColaborador){
							Documentocomissao comissao = new Documentocomissao();
							comissao.setVendedortipo(contrato.getVendedortipo());
							comissao.setPessoa(item.getColaborador());
							comissao.setComissionamento(item.getComissionamento());
							comissao.setDocumento(doc);
							comissao.setContrato(contrato);
							documentocomissaoService.saveOrUpdate(comissao);
						}		
					}
				}
				contColaborador++;
			}
		}
	}
				
	private String ateApos(Integer cdtipotaxa){
		if(cdtipotaxa == 3 || cdtipotaxa == 4){
			return " at� ";
		}
		return " ap�s ";
	}
	
	/**
	 * M�todo que valida as notas, verificando se o valor � menor ou igual a 0
	 * 
	 * @param listaProduto
	 * @param listaServico
	 * @return
	 * @author Tom�s Rabelo
	 */
	private String validaValorNotas(List<Notafiscalproduto> listaProduto, List<NotaFiscalServico> listaServico) {
		String msg = "";
		if(listaServico != null && listaServico.size() > 0){
			List<Integer> indexes = new ArrayList<Integer>();
			for (int i = 0; i < listaServico.size(); i++) {
				if(listaServico.get(i).getValorNota().getValue().doubleValue() <= 0.0){
					indexes.add(i);
					msg += "A nota fiscal de servi�o do contrato "+listaServico.get(i).getListaContrato().get(0).getCdcontrato()+" n�o foi gerada, pois seu valor � igual � 0.\n";
				}
			}
			
			if(indexes != null && !indexes.isEmpty())
				for (Integer index : indexes) 
					listaServico.remove(index.intValue());
		}
		
		if(listaProduto != null && listaProduto.size() > 0){
			List<Integer> indexes = new ArrayList<Integer>();
			for (int i = 0; i < listaProduto.size(); i++) {
				if(listaProduto.get(i).getValor().getValue().doubleValue() <= 0.0){
					indexes.add(i);
					msg += "A nota fiscal de produto do contrato "+listaProduto.get(i).getListaContrato().get(0).getCdcontrato()+" n�o foi gerada, pois seu valor � igual � 0.\n";
				}
			}
			
			if(indexes != null && !indexes.isEmpty())
				for (Integer index : indexes) 
					listaProduto.remove(index.intValue());
		}
		return msg;
	}
	
	/**
	 * Salva documento origem de contrato
	 * 
	 * @param documento
	 * @author Taidson
	 * @since 27/10/2010
	 */
	public void salvaDocumentoorigemContrato(Documento documento){
		Documentoorigem documentoorigem = new Documentoorigem();
		documentoorigem.setDocumento(documento);
		documentoorigem.setContrato(documento.getContrato());
		if(SinedUtil.getUsuarioLogado() != null){
			documentoorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		}
		documentoorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));	
		
		documentoorigemService.saveOrUpdate(documentoorigem);
	}
	
	public Documentohistorico criaDocumentoHistoricoFaturamentoContrato(Documento documento, Integer id){
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(id);
		return criaDocumentoHistoricoFaturamentoContrato(documento, lista);
	}
	
	/**
	 * Metodo que cria um registro de Documentohistorico para p faturamento de um contrato
	 * @param documento documento do qual ser� gerado o hist�rico
	 * @param cdContrato c�digo do contrato faturado, que ser� informado no campo "Observa��o"
	 * @author Igor
	 * @return
	 */
	public Documentohistorico criaDocumentoHistoricoFaturamentoContrato(Documento documento, List<Integer> ids){
		Documentohistorico documentohistorico = new Documentohistorico();
		
		String observacao = "Faturamento do contrato: ";
		if(SinedUtil.isListNotEmpty(ids)){
			StringBuilder whereInContrato = new StringBuilder();
			for (Integer id : ids) {
				whereInContrato.append(id).append(",");
			}
			List<Contrato> listaContrato = contratoService.findWithIdentificador(whereInContrato.toString().substring(0, whereInContrato.toString().length()-1));
			if(SinedUtil.isListNotEmpty(listaContrato)){
				for(Contrato contrato : listaContrato){
					observacao += "<a href=\"javascript:visualizarContrato("+contrato.getCdcontrato()+")\">"+contrato.getIdentificadorOrCdcontrato()+"</a>, ";
				}
				observacao = observacao.substring(0, observacao.length() - 2);
			}
		}
		
		documentohistorico.setObservacao(observacao);
		documentohistorico.setDocumento(documento);
		documentohistorico.setValor(documento.getValor());
		documentohistorico.setDocumento(documento);
		documentohistorico.setDocumentoacao(documento.getDocumentoacao());
		documentohistorico.setDocumentotipo(documento.getDocumentotipo());
		documentohistorico.setDtemissao(documento.getDtemissao());
		documentohistorico.setDtvencimento(documento.getDtvencimento());
		documentohistorico.setTipopagamento(documento.getTipopagamento());
		documentohistorico.setOutrospagamento(documento.getOutrospagamento());
		documentohistorico.setNumero(documento.getNumero());
		documentohistorico.setDescricao(documento.getDescricao());
		documentohistorico.setPessoa(documento.getPessoa());
		documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		if(SinedUtil.getUsuarioLogado() != null)
			documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		
		
		return documentohistorico;
	}
	
	public Documentohistorico criaDocumentoHistoricoFaturamentoListaContrato(Documento documento, List<Contrato> contratos){
		Documentohistorico documentohistorico = new Documentohistorico();

		StringBuilder sbHistorico = new StringBuilder();
		sbHistorico.append("Faturamento do(s) contrato(s) ");
		for (Contrato contrato : contratos) {
			sbHistorico
				.append("<a href=\"javascript:visualizarContrato(")
				.append(contrato.getCdcontrato())
				.append(")\">")
				.append(contrato.getIdentificadorOrCdcontrato())
				.append("</a>; ");
		}
		
		documentohistorico.setObservacao(sbHistorico.toString());
		documentohistorico.setDocumento(documento);
		documentohistorico.setValor(documento.getValor());
		documentohistorico.setDocumento(documento);
		documentohistorico.setDocumentoacao(documento.getDocumentoacao());
		documentohistorico.setDocumentotipo(documento.getDocumentotipo());
		documentohistorico.setDtemissao(documento.getDtemissao());
		documentohistorico.setDtvencimento(documento.getDtvencimento());
		documentohistorico.setTipopagamento(documento.getTipopagamento());
		documentohistorico.setOutrospagamento(documento.getOutrospagamento());
		documentohistorico.setNumero(documento.getNumero());
		documentohistorico.setDescricao(documento.getDescricao());
		documentohistorico.setPessoa(documento.getPessoa());
		documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		if(SinedUtil.getUsuarioLogado() != null)
			documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		
		
		return documentohistorico;
	}
	
	private String motaStringLink(Contrato contrato) {
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		listaContrato.add(contrato);
		return motaStringLink(listaContrato);
	}
	
	/**
	 * Monta a string dse observa��o do hist�rico da nota.
	 *
	 * @param listaContrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String motaStringLink(List<Contrato> listaContrato) {
		
		StringBuilder resultado = new StringBuilder();
		
		for (Contrato contrato : listaContrato) {
			resultado.append("<a href=\"javascript:visualizarContrato("+contrato.getCdcontrato()+")\">"+contrato.getCdcontrato()+"</a> ,");
		}
		
		return resultado.toString() != null && resultado.toString().length() > 2 ? resultado.toString().substring(0, resultado.toString().length()-2) : "";
	}
	
	private boolean verificaSeNotaContemClienteFreq(Notafiscalproduto nf, List<Notafiscalproduto> lista) {
		boolean retorno = false;
		
		if(lista != null){
			for (Notafiscalproduto nf2 : lista) {
				boolean clienteIgual = nf2.getCliente().getCdpessoa().equals(nf.getCliente().getCdpessoa());
				boolean contaIgual = nf2.getConta().getCdconta().equals(nf.getConta().getCdconta());
				boolean empresaIgual = nf2.getEmpresa().getCdpessoa().equals(nf.getEmpresa().getCdpessoa());
				boolean permiteFaturamentoLote = nf.getFaturamentolote() && nf.getFaturamentolote() != null && nf2.getFaturamentolote() && nf2.getFaturamentolote() != null;
				boolean grupoTributacaoIgual = 
					(nf2.getGrupotributacao() == null && nf.getGrupotributacao() == null) ||
					(nf2.getGrupotributacao() != null && nf.getGrupotributacao() != null && nf2.getGrupotributacao().getCdgrupotributacao().equals(nf.getGrupotributacao().getCdgrupotributacao()));
				
				if(clienteIgual && contaIgual && empresaIgual && permiteFaturamentoLote && grupoTributacaoIgual){
					if(nf2.getListaItens() != null && nf.getListaItens() != null){
						nf2.getListaItens().addAll(nf.getListaItens());
					}
					if(nf2.getListaContrato() != null && nf.getListaContrato() != null){
						nf2.getListaContrato().addAll(nf.getListaContrato());
					}
					
					retorno = true;
					break;
				}
			}
		}
		
		return retorno;
	}
	
	/**
	 * Verifica se cont�m o cliente e a frequencia na lista de notas.
	 *
	 * @param nf
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean verificaSeNotaContemClienteFreq(NotaFiscalServico nf, List<NotaFiscalServico> lista) {
		
		boolean retorno = false;
		
		if(lista != null){
			for (NotaFiscalServico nf2 : lista) {
				
				boolean clienteIgual = nf2.getCliente().getCdpessoa().equals(nf.getCliente().getCdpessoa());
				boolean contaIgual = (nf2.getConta() == null && nf.getConta() == null) || (nf2.getConta() != null && nf2.getConta().getCdconta().equals(nf.getConta().getCdconta()));
				boolean empresaIgual = nf2.getEmpresa().getCdpessoa().equals(nf.getEmpresa().getCdpessoa());
//				boolean frequenciaExisteIgual = (nf2.getFrequencia() != null && nf.getFrequencia() != null && nf2.getFrequencia().getCdfrequencia().equals(nf.getFrequencia().getCdfrequencia()));
				boolean permiteFaturamentoLote = nf.getFaturamentolote() && nf.getFaturamentolote() != null && nf2.getFaturamentolote() && nf2.getFaturamentolote() != null;
				boolean grupoTributacaoIgual = 
					(nf2.getGrupotributacao() == null && nf.getGrupotributacao() == null) ||
					(nf2.getGrupotributacao() != null && nf.getGrupotributacao() != null && nf2.getGrupotributacao().getCdgrupotributacao().equals(nf.getGrupotributacao().getCdgrupotributacao()));
				
				if(clienteIgual && contaIgual && empresaIgual && permiteFaturamentoLote && grupoTributacaoIgual){
					if(nf2.getListaItens() != null && nf.getListaItens() != null){
						nf2.getListaItens().addAll(nf.getListaItens());
					}
					if(nf2.getListaContrato() != null && nf.getListaContrato() != null){
						nf2.getListaContrato().addAll(nf.getListaContrato());
					}
					if(nf.getImpostocumulativoiss() != null){
						nf2.setImpostocumulativoiss(nf.getImpostocumulativoiss());
					}
					if(nf.getImpostocumulativoir() != null){
						nf2.setImpostocumulativoir(nf.getImpostocumulativoir());
					}
					if(nf.getImpostocumulativoinss() != null){
						nf2.setImpostocumulativoinss(nf.getImpostocumulativoinss());
					}
					if(nf.getDescontoincondicionado() != null && nf2.getDescontoincondicionado() != null){
						nf2.setDescontoincondicionado(nf2.getDescontoincondicionado().add(nf.getDescontoincondicionado()));
					}
					if(nf.getBasecalculo() != null && nf2.getBasecalculo() != null){
						if(nf2.getDescontoincondicionado() != null){
							nf2.setBasecalculo((nf2.getBasecalculo().add(nf.getBasecalculo())).subtract(nf2.getDescontoincondicionado()));
						}else{
							nf2.setBasecalculo(nf2.getBasecalculo().add(nf.getBasecalculo()));
						}
					}
					
					if(nf.getBasecalculoir() != null && nf2.getBasecalculoir() != null){
						nf2.setBasecalculoir(nf2.getBasecalculoir().add(nf.getBasecalculoir()));
					}
					if(nf.getBasecalculoiss() != null && nf2.getBasecalculoiss() != null){
						nf2.setBasecalculoiss(nf2.getBasecalculoiss().add(nf.getBasecalculoiss()));
					}
					if(nf.getBasecalculopis() != null && nf2.getBasecalculopis() != null){
						nf2.setBasecalculopis(nf2.getBasecalculopis().add(nf.getBasecalculopis()));
					}
					if(nf.getBasecalculocofins() != null && nf2.getBasecalculocofins() != null){
						nf2.setBasecalculocofins(nf2.getBasecalculocofins().add(nf.getBasecalculocofins()));
					}
					if(nf.getBasecalculocsll() != null && nf2.getBasecalculocsll() != null){
						nf2.setBasecalculocsll(nf2.getBasecalculocsll().add(nf.getBasecalculocsll()));
					}
					if(nf.getBasecalculoinss() != null && nf2.getBasecalculoinss() != null){
						nf2.setBasecalculoinss(nf2.getBasecalculoinss().add(nf.getBasecalculoinss()));
					}
					if(nf.getBasecalculoicms() != null && nf2.getBasecalculoicms() != null){
						nf2.setBasecalculoicms(nf2.getBasecalculoicms().add(nf.getBasecalculoicms()));
					}
					
					if(nf.getDadosAdicionais() != null && nf2.getDadosAdicionais() != null){
						StringBuilder obs = new StringBuilder();
						obs.append(nf2.getDadosAdicionais()+", "+nf.getDadosAdicionais());
						nf2.setDadosAdicionais(obs.toString());
					}
					
					retorno = true;
					break;
				}
			}
		}
		
		return retorno;
	}
	
	private boolean verificaSeDocumentoContemClienteFreq(Documento doc, List<Documento> lista) {
		
		boolean retorno = false;
		
		if(lista != null){
			for (Documento doc2 : lista) {
				
				boolean pessoaIgual = doc2.getPessoa().getCdpessoa().equals(doc.getPessoa().getCdpessoa());
				boolean contaIgual = (doc2.getConta() == null || doc.getConta() == null) || (doc2.getConta() != null && doc2.getConta().getCdconta().equals(doc.getConta().getCdconta()));
				boolean empresaIgual = doc2.getEmpresa().getCdpessoa().equals(doc.getEmpresa().getCdpessoa());
				boolean permiteFaturamentoLote = doc.getFaturamentolote() && doc.getFaturamentolote() != null && doc2.getFaturamentolote() && doc2.getFaturamentolote() != null;
				
				if(pessoaIgual && contaIgual && empresaIgual && permiteFaturamentoLote){
					// TODO FAZER O AGRUPAMENTO AQUI
					if(doc.getListaContrato() != null && doc2.getListaContrato() != null){
						doc2.getListaContrato().addAll(doc.getListaContrato());
					}
					if(doc.getValor() != null && doc2.getValor() != null){
						doc2.setValor(doc2.getValor().add(doc.getValor()));
					}
					if(doc.getRateio() != null && doc.getRateio().getListaRateioitem() != null && 
							doc2.getRateio() != null && doc2.getRateio().getListaRateioitem() != null){
						Rateio rateio = doc2.getRateio();
						List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
						listaRateioitem.addAll(doc.getRateio().getListaRateioitem());						
						
						rateioitemService.agruparItensRateio(listaRateioitem);
						rateio.setListaRateioitem(listaRateioitem);
						rateioService.rateioItensPercentual(rateio, doc2.getValor());
						rateioService.atualizaValorRateio(rateio, doc2.getValor());
						doc2.setRateio(rateio);						
					}
					if(doc.getDescricao() != null && doc2.getDescricao() != null){
						StringBuilder obs = new StringBuilder();
						obs.append(doc2.getDescricao() + ", " + doc.getDescricao());
						doc2.setDescricao(obs.toString().length() > 150 ? obs.substring(0, 150) : obs.toString());
					}
					if((doc2.getNumero() != null && doc.getNumero() == null) || (doc2.getNumero() == null && doc.getNumero() != null) || 
							(doc2.getNumero() != null && doc.getNumero() != null && !doc2.getNumero().equals(doc.getNumero()))){
						doc2.setNumero(null);
					}
					
					retorno = true;
					break;
				}
			}
		}
		
		return retorno;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForCobranca
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findForCobranca(String whereIn) {
		List<Contrato> listaContrato = contratoDAO.findForCobranca(whereIn);
		if(listaContrato != null && !listaContrato.isEmpty()){
			addListaContratocolaborador(contratocolaboradorService.findForEntradaByContrato(whereIn), listaContrato);
			addListaContratodesconto(contratodescontoService.findForCobranca(whereIn), listaContrato);
			addListaNotaContrato(notaContratoService.findForCobranca(whereIn), listaContrato);
		}
		return listaContrato;
	}
	
	/**
	 * M�todo que adiciona a lista de colaborador ao contrato
	 *
	 * @param listaContratocolaborador
	 * @param listaContrato
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	private void addListaContratocolaborador(List<Contratocolaborador> listaContratocolaborador, List<Contrato> listaContrato) {
		if(listaContrato != null && !listaContrato.isEmpty() && listaContratocolaborador != null && !listaContratocolaborador.isEmpty()){
			for(Contratocolaborador contratocolaborador : listaContratocolaborador){
				if(contratocolaborador.getContrato() != null){
					for(Contrato contrato : listaContrato){
						if(contrato.equals(contratocolaborador.getContrato())){
							if(contrato.getListaContratocolaborador() == null){
								contrato.setListaContratocolaborador(new ArrayList<Contratocolaborador>());
							}
							contrato.getListaContratocolaborador().add(contratocolaborador);
							break;
						}
					}
				}
			}
		}
		
	}
	/**
	 * M�todo que adiciona a lista de notacontrato ao contrato
	 *
	 * @param listaNotaContrato
	 * @param listaContrato
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	private void addListaNotaContrato(List<NotaContrato> listaNotaContrato, List<Contrato> listaContrato) {
		if(listaContrato != null && !listaContrato.isEmpty() && listaNotaContrato != null && !listaNotaContrato.isEmpty()){
			for(NotaContrato notaContrato : listaNotaContrato){
				if(notaContrato.getContrato() != null){
					for(Contrato contrato : listaContrato){
						if(contrato.equals(notaContrato.getContrato())){
							if(contrato.getListaNotacontrato() == null){
								contrato.setListaNotacontrato(new ArrayList<NotaContrato>());
							}
							contrato.getListaNotacontrato().add(notaContrato);
							break;
						}
					}
				}
			}
		}
		
	}
	/**
	 * M�todo que adiciona a lista de desconto ao contrato
	 *
	 * @param listaContratodesconto
	 * @param listaContrato
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	private void addListaContratodesconto(List<Contratodesconto> listaContratodesconto, List<Contrato> listaContrato) {
		if(listaContrato != null && !listaContrato.isEmpty() && listaContratodesconto != null && !listaContratodesconto.isEmpty()){
			for(Contratodesconto contratodesconto : listaContratodesconto){
				if(contratodesconto.getContrato() != null){
					for(Contrato contrato : listaContrato){
						if(contrato.equals(contratodesconto.getContrato())){
							if(contrato.getListaContratodesconto() == null){
								contrato.setListaContratodesconto(new ArrayList<Contratodesconto>());
							}
							contrato.getListaContratodesconto().add(contratodesconto);
							break;
						}
					}
				}
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForCobranca
	 * 
	 * @param identificador
	 * @return
	 * @author Rafael Salvio
	 */
	public Contrato findForCobrancaByIdentificador(String identificador) {
		return contratoDAO.findForCobrancaByIdentificador(identificador);
	}
	
	/**
	 * Gera o relat�rio de listagem dos contratos, do bot�o "GERAR PDF" do filtro.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorioListagem(ContratoFiltro filtro) {
		Report report = new Report("/faturamento/contrato");
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Contrato> listagemResult = this.findForListagem(filtro);
		
		if(listagemResult.list() == null || listagemResult.list().isEmpty())
			throw new SinedException("Nenhum registro a ser mostrado.");
		
		String whereIn = CollectionsUtil.listAndConcatenate(listagemResult.list(), "cdcontrato", ",");
		List<Contrato> listaContrato = this.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc());
		
		report.setDataSource(listaContrato);
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#haveContratoSituacao
	 *
	 * @param situacoes
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveContratoSituacao(String whereIn, SituacaoContrato... situacoes) {
		return contratoDAO.haveContratoSituacao(whereIn, situacoes);
	}
	
	/**
	 * Gera um contrato a partir de uma proposta.
	 *
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Contrato gerarContrato(Proposta proposta) {
		
		Contrato contrato = new Contrato();
		
		contrato.setCliente(proposta.getCliente());
		contrato.setDescricao(proposta.getDescricao());
		contrato.setValor(proposta.getAux_proposta().getValor());
		contrato.setDtinicio(new Date(System.currentTimeMillis()));
		contrato.setProposta(proposta);
		
		return contrato;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForReportFluxocaixa
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return contratoDAO.findForReportFluxocaixa(filtro);
	}
	
	/**
	 * Adiciona os registros de contrato na lista de fluxo de caixa.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#gerarIteracoesContrato
	 * @see br.com.linkcom.sined.geral.service.ContratoService#criaListaFluxocaixa
	 *
	 * @param filtro
	 * @param listaFluxo
	 * @param listaContrato
	 * @param periodoDe
	 * @param periodoAte
	 * @author Rodrigo Freitas
	 * @param radEvento 
	 * @param boolean1 
	 */
	public void adicionaContratoFluxocaixa(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo,
			List<Contrato> listaContrato, Date periodoDe, Date periodoAte, Boolean radEvento, Boolean valoresAnteriores) {
		
		List<Contrato> listaIteracoesContrato = new ArrayList<Contrato>();
		for (Contrato ct : listaContrato) {
			listaIteracoesContrato.addAll(this.gerarIteracoesContrato(ct, periodoDe, periodoAte, valoresAnteriores));
		}
		
		listaFluxo.addAll(this.criaListaFluxocaixa(listaIteracoesContrato, radEvento, periodoDe, valoresAnteriores, periodoAte));
	}
	
	/**
	 * Cria a lista de <code>FluxocaixaBeanReport</code> para o relat�rio de fluxo de caixa.
	 * 
	 *
	 * @param listaContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @param radEvento 
	 * @param periodoDe 
	 * @param valoresAnteriores 
	 */
	private List<FluxocaixaBeanReport> criaListaFluxocaixa(List<Contrato> listaContrato, Boolean radEvento, Date periodoDe, Boolean valoresAnteriores, Date periodoAte) {
		List<FluxocaixaBeanReport> listaFluxo = new ArrayList<FluxocaixaBeanReport>();
		Integer mesesFimContrato;
		for (Contrato ct : listaContrato) {
			if(ct.getDtfim() != null){
				mesesFimContrato = SinedDateUtils.mesesEntre(SinedDateUtils.currentDate(), ct.getDtfim());
			}else{
				mesesFimContrato = SinedDateUtils.mesesEntre(periodoDe, periodoAte);
			}
			if(mesesFimContrato == 0){
				ct.setMesesFinalContrato(1);
			}else{
				ct.setMesesFinalContrato(mesesFimContrato);				
			}
			
			FluxocaixaBeanReport fluxocaixaBeanReport = new FluxocaixaBeanReport(ct, radEvento);
			if(valoresAnteriores != null && valoresAnteriores && ct.getDtproximovencimento().before(periodoDe)){
				fluxocaixaBeanReport.setDataAtrasada(ct.getDtproximovencimento());
				fluxocaixaBeanReport.setData(SinedDateUtils.remoteDate());
			}
			listaFluxo.add(fluxocaixaBeanReport);
		}
		return listaFluxo;
	}
	
	/**
	 * Gera a simula��o das intera��es dos contratos para o fluxo de caixa.
	 * 
	 * @see br.com.linkcom.sined.util.SinedDateUtils#calculaDiferencaDias
	 * @see br.com.linkcom.sined.util.SinedDateUtils#incrementDateFrequencia
	 * 
	 * @param contrato
	 * @param dataInicio
	 * @param dataFim
	 * @return
	 * @author Rodrigo Freitas
	 * @param valoresAnteriores 
	 */
	private List<Contrato> gerarIteracoesContrato(Contrato contrato, Date dataInicio, Date dataFim, Boolean valoresAnteriores) {
		List<Contrato> listaAgrupada = new ArrayList<Contrato>();
		
		if(contrato.getFrequencia() != null && contrato.getCdcontrato() != null){
			Frequencia frequencia = contrato.getFrequencia();
			Date dtproximo = new Date(contrato.getDtproximovencimento().getTime());
			
			while(SinedDateUtils.calculaDiferencaDias(dtproximo, dataFim) >= 0){
				
				/*
				 *	A lista de itera��es do contrato s� poder� ser composta por contratos cuja data de consolida��o(dtproximo)
				 *	esteja entre a data in�cio e a data fim do filtro, as quais s�o informadas por par�metro. 
				 */
				if(!dtproximo.before(dataInicio)){
					Contrato ct = new Contrato(contrato);
					if(contrato.getDtfim() != null){
						if(!dtproximo.after(contrato.getDtfim())){
							listaAgrupada.add(ct);
							ct.setDtproximovencimento(dtproximo);
						}
					}else{
						listaAgrupada.add(ct);
						ct.setDtproximovencimento(dtproximo);
					}
					
				} else {
					if(valoresAnteriores){
						Contrato ct = new Contrato(contrato);
						listaAgrupada.add(ct);
						ct.setDtproximovencimento(dtproximo);
					}
				}
				
				/*
				 * A data da pr�xima consolida��o do contrato(dtproximo) � incrementada de acordo com a frequ�ncia do mesmo.
				 * (Vide Documenta��o do m�todo)
				 */
				dtproximo = SinedDateUtils.incrementDateFrequencia(dtproximo, frequencia, 1);
				
				/*
				 *  Se o contrato for de frequ�ncia �nica, s� possui uma itera��o, que � ele mesmo.
				 *  O loop deve ser encerrado pois, com frequ�ncia �NICA, a data n�o � incrementada. Portanto este seria eterno.
				 */
				if(frequencia.getCdfrequencia().equals(Frequencia.UNICA)){
					break;
				}
			}
		}else if(contrato.getListaContratoparcela() != null && !contrato.getListaContratoparcela().isEmpty()){
			for (Contratoparcela contratoparcela : contrato.getListaContratoparcela()) {
				if(contratoparcela.getCobrada() == null || !contratoparcela.getCobrada()){
					if(contratoparcela.getDtvencimento().getTime() >= dataInicio.getTime() && contratoparcela.getDtvencimento().getTime() <=  dataFim.getTime()){
						Contrato ct = new Contrato(contrato);
						ct.setValor(contratoparcela.getValor()); //O valor ser� igual o valor de cada parcela. 
						ct.setDtproximovencimento(contratoparcela.getDtvencimento()); //A data � a data da parcela.
						Money valorContrato = ct.getValorContrato();
						if(contrato.getRateio() != null && valorContrato != null){
							ct.setRateio(contrato.getRateio());
							rateioService.atualizaValorRateio(ct.getRateio(), valorContrato);
						}
						listaAgrupada.add(ct);
					}else if(contratoparcela.getDtvencimento().getTime() < dataInicio.getTime()){
						if(valoresAnteriores){
							Contrato ct = new Contrato(contrato);
							ct.setValor(contratoparcela.getValor()); //O valor ser� igual o valor de cada parcela. 
							ct.setDtproximovencimento(contratoparcela.getDtvencimento()); //A data � a data da parcela.
							Money valorContrato = ct.getValorContrato();
							if(contrato.getRateio() != null && valorContrato != null){
								ct.setRateio(contrato.getRateio());
								rateioService.atualizaValorRateio(ct.getRateio(), valorContrato);
							}
							listaAgrupada.add(ct);
						}
					}
				}
			}
		}
		return listaAgrupada;
	}
	
	public List<Contrato> findByClienteAbertosApontamento(Cliente cliente){
		return contratoDAO.findByClienteAbertosApontamento(cliente, null);
	}
	
	public List<Contrato> findByClienteEmpresaAbertosApontamento(Cliente cliente, Empresa empresa){
		return contratoDAO.findByClienteEmpresaAbertosApontamento(cliente, empresa);
	}
	
	public List<Contrato> findByClienteAbertosApontamento(Cliente cliente, Integer cdapontamento){
		return contratoDAO.findByClienteAbertosApontamento(cliente, cdapontamento);
	}
	
	/**
	 * M�todo que coloca as datas de acordo com o prazo de pagamento escolhido.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoitemService#findByPrazo
	 * 
	 * @param contrato
	 * @return
	 * @Author Tom�s Rabelo, Rodrigo Freitas
	 */
	public List<String> geraParcelamento(Contrato contrato) {
		List<String> datas = new ArrayList<String>();
		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(contrato.getPrazopagamento());
		Prazopagamento prazopagamento = prazopagamentoService.load(contrato.getPrazopagamento());
		
		Date data = contrato.getDtinicio() == null ? new Date(System.currentTimeMillis()) : contrato.getDtinicio();
		Date dataAux;
		for (Prazopagamentoitem prazopagamentoitem : listaPrazoItem) {
			dataAux = null;
			
			if(prazopagamentoitem.getDias() != null)
				dataAux = SinedDateUtils.addDiasData(data, prazopagamentoitem.getDias());
			if(prazopagamentoitem.getMeses() != null)
				dataAux = SinedDateUtils.addMesData(data, prazopagamentoitem.getMeses());
			if(dataAux != null){
				if(prazopagamento.getDataparceladiautil() != null && prazopagamento.getDataparceladiautil()){
					dataAux = SinedDateUtils.getProximaDataUtil(dataAux, contrato.getEmpresa(), prazopagamento.getDataparcelaultimodiautilmes());
				}
				datas.add(new SimpleDateFormat("dd/MM/yyyy").format(dataAux));
			}
		}
		
		return datas;
	}
	
	/**
	 * M�todo que coloca as datas de acordo com o prazo de pagamento escolhido.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoitemService#findByPrazo
	 * 
	 * @param contrato
	 * @return
	 * @Author Tom�s Rabelo, Rodrigo Freitas
	 */
	private List<Date> geraParcelamentoDate(Contrato contrato) {
		List<Date> datas = new ArrayList<Date>();
		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(contrato.getPrazopagamento());
		
		Date data = contrato.getDtproximovencimento();
		Date dataAux;
		for (int i = 0; i < listaPrazoItem.size(); i++) {
			Prazopagamentoitem prazopagamentoitem = listaPrazoItem.get(i);
			dataAux = null;
			
			if(i == 0) {
				datas.add(new Date(data.getTime()));
				
				if(prazopagamentoitem.getDias() != null)
					data = SinedDateUtils.addDiasData(data, prazopagamentoitem.getDias()*-1);
				if(prazopagamentoitem.getMeses() != null)
					data = SinedDateUtils.addMesData(data, prazopagamentoitem.getMeses()*-1);
				
				continue;
			}
			
			if(prazopagamentoitem.getDias() != null)
				dataAux = SinedDateUtils.addDiasData(data, prazopagamentoitem.getDias());
			if(prazopagamentoitem.getMeses() != null)
				dataAux = SinedDateUtils.addMesData(data, prazopagamentoitem.getMeses());
			
			if(dataAux != null)
				datas.add(dataAux);
		}
		
		return datas;
	}
	
	public DadosEstatisticosBean getDadosEstatisticosContrato(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Integer qtdeAtrasadas = getQtdeAtrasada();
		Integer total = getQtdeTotalContratos();
		return new DadosEstatisticosBean(DadosEstatisticosBean.CONTRATO, ultimoCadastro, qtdeAtrasadas, total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalContratos() {
		return contratoDAO.getQtdeTotalContratos();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeAtrasada() {
		return contratoDAO.getQtdeAtrasada();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAFaturar() {
		return contratoDAO.getQtdeAFaturar();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return contratoDAO.getDtUltimoCadastro();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @author Tom�s Rabelo
	 */
	public void atualizaDataFimContratos() {
		contratoDAO.atualizaDataFimContratos();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contrato> findContratosForBuscaGeral(String busca) {
		return contratoDAO.findContratosForBuscaGeral(busca);
	}
	
	/**
	 * M�todo que atualiza o valor do contrato verificando os servi�os que j� foram prestados
	 * @param listaContrato
	 */
	public void atualizaValor(List<Contrato> listaContrato, boolean salvar) {
		for (Contrato contrato : listaContrato) {
			atualizaValor(contrato, salvar);
		}
	}	
	
	public void atualizaValor(Contrato contrato, boolean salvar) {
		
		Money valor = new Money(0l);
		Date dtfim;
		
		java.util.Date dataAtual = new java.util.Date(System.currentTimeMillis());
		
		List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
		
		if(listaContratomaterial != null && !listaContratomaterial.isEmpty() && (contrato.getIgnoravalormaterial() == null || !contrato.getIgnoravalormaterial())){
			
			valor = new Money();
			
			for (Contratomaterial contratomaterial : listaContratomaterial) {
				dtfim = contratomaterial.getDtfim();
				if((dtfim == null || dtfim.compareTo(dataAtual) >= 0) && dataAtual.compareTo(contratomaterial.getDtinicio()) >= 0){
					Double qtde = contratomaterial.getQtde();
					Integer periodocobranca = contratomaterial.getPeriodocobranca();
					if(periodocobranca != null){
						qtde = qtde * periodocobranca;
					}
					Money valorunitario = new Money(contratomaterial.getValorunitario());
					valor = valor.add(valorunitario.multiply(new Money(qtde)));
				}
			}
			contrato.setValor(valor);
			if(salvar){
				contratoDAO.updateValoreContrato(contrato);
			}

			contrato.getRateio().setListaRateioitem(rateioitemService.findByRateio(contrato.getRateio()));
			rateioService.atualizaValorRateio(contrato.getRateio(), contrato.getValor());
			if(salvar){
				rateioService.saveOrUpdate(contrato.getRateio());
			}
		}
	}
	
	public Contrato carregaTodosCampos(String whereIn){
		return contratoDAO.carregaTodosCampos(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cliente
	 * @param contratotipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/12/2012
	 */
	public List<Contrato> findByClienteContratotipo(Cliente cliente, Contratotipo contratotipo) {
		return contratoDAO.findByClienteContratotipo(cliente, contratotipo);
	}
	
	public List<Contrato> findByContrato(Cliente cliente){
		return contratoDAO.findByContrato(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findByContratoWithCliente(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 12/05/2014
	 */
	public List<Contrato> findByContratoWithCliente(String whereIn) {
		return contratoDAO.findByContratoWithCliente(whereIn);
	}
	
	/**
	 * M�todo que cria relat�rio CSV, com os mesmo dados do relat�rio PDF
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource gerarRelarorioCSVListagem(ContratoFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Contrato> listagemResult = this.findForListagem(filtro);
		
		if(listagemResult.list() == null || listagemResult.list().isEmpty())
			throw new SinedException("Nenhum registro a ser mostrado.");
		
		String whereIn = CollectionsUtil.listAndConcatenate(listagemResult.list(), "cdcontrato", ",");
		List<Contrato> listaContrato = this.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc());

		StringBuilder csv = new StringBuilder();
		csv.append("C�digo;Identificador;Cliente;CPF/CNPJ;Descri��o;In�cio;T�rmino;Renova��o;Forma de Faturamento;Pr�ximo Vencimento;Empresa;Conta Banc�ria;Respons�vel;Valor Bruto;Valor;Situa��o;Hist�rico;Anota��es;\n");
		for (Contrato contrato : listaContrato) {
			csv
			.append(contrato.getCdcontrato()).append(";")
			.append(contrato.getIdentificador() != null ? contrato.getIdentificador() : "").append(";")
			.append(contrato.getCliente().getNome()).append(";")
			.append(contrato.getCliente().getCpfOuCnpj() != null ? contrato.getCliente().getCpfOuCnpj() : "").append(";")
			.append(contrato.getDescricao()).append(";")
			.append(contrato.getDtinicio()).append(";")
			.append(contrato.getDtfim() == null ? "" : contrato.getDtfim()).append(";")
			.append(contrato.getDtrenovacao() == null ? "" : contrato.getDtrenovacao()).append(";")
			.append(contrato.getFormafaturamento() != null ? contrato.getFormafaturamento().getNome() : "").append(";")
			.append(contrato.getDtproximovencimento()).append(";")
			.append(contrato.getEmpresa().getRazaosocialOuNome()).append(";")
			.append((contrato.getConta()!=null ? contrato.getConta().getNome() : "")).append(";")
			.append(contrato.getResponsavel().getNome()).append(";")
			.append(contrato.getValor()).append(";")
			.append(contrato.getValorContrato()).append(";")
			.append(contrato.getAux_contrato().getSituacao().getNome()).append(";")
			.append(contrato.getHistorico() != null ? contrato.getHistorico() : "").append(";")
			.append(contrato.getAnotacao() != null ? contrato.getAnotacao() : "").append(";\n");
		}
		
		Resource resource = new Resource("text/csv", "contrato_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param dtconfirma
	 * @author Tom�s Rabelo
	 * @param ano 
	 * @param mes 
	 */
	public void confirmaContratos(String whereIn, Date dtproximovencimento, Mes mes, Integer ano) {
		List<Contrato> listaContrato = this.findForCobranca(whereIn);
		Contratoparcela contratoparcela;
		List<Date> lista;
		List<Contratoparcela> listaParcelas;
		for (Contrato contrato : listaContrato) {
			if(contrato.getIsento() != null && contrato.getIsento()){
				this.atualizaCampoIsentoContrato(contrato, false);
			}
			
			Contratohistorico ch = new Contratohistorico(contrato, new Timestamp(System.currentTimeMillis()), 
					Contratoacao.CONFIRMADO, SinedUtil.getUsuarioLogado());
			
			String observacao = "";
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			if(dtproximovencimento != null && contrato.getDtproximovencimento() == null){
				observacao += "Altera��o da data do pr�ximo vencimento de 'vazio' para " + format.format(dtproximovencimento) + ".<br>";
			}else if(dtproximovencimento == null && contrato.getDtproximovencimento() != null){
				observacao += "Altera��o da data do pr�ximo vencimento de " + format.format(contrato.getDtproximovencimento()) + " para 'vazio'.<br>";
			}else if(dtproximovencimento != null && contrato.getDtproximovencimento() != null && !SinedDateUtils.equalsIgnoreHour(dtproximovencimento, contrato.getDtproximovencimento())){
				observacao += "Altera��o da data do pr�ximo vencimento de " + format.format(contrato.getDtproximovencimento()) + " para " + format.format(dtproximovencimento) + ".<br>";
			}
			
			if (!org.apache.commons.lang.StringUtils.isEmpty(observacao)) {
				ch.setValor(contrato.getValor());
				ch.setDtproximovencimento(contrato.getDtproximovencimento());
				ch.setFormafaturamento(contrato.getFormafaturamento());
				ch.setHistorico(contrato.getHistorico());
				ch.setAnotacao(contrato.getAnotacao());
				ch.setObservacao((ch.getObservacao() != null ? ch.getObservacao() : "") + observacao);
				ch.setVisualizarContratoHistorico(Boolean.TRUE);
				
				StringBuilder rateio = new StringBuilder();
				for (Rateioitem ri : contrato.getRateio().getListaRateioitem()) {
					rateio.append("Conta gerencial: " + contagerencialService.loadForEntrada(ri.getContagerencial()).getDescricao() + "\n");
					rateio.append("Centro de custo: " + centrocustoService.loadForEntrada(ri.getCentrocusto()).getNome() + "\n");
					
					Projeto projeto = projetoService.loadForEntrada(ri.getProjeto());
					if (projeto != null) {
						rateio.append("Projeto: " + projeto.getNome() + "\n");
					}
					
					rateio.append("Percentual: " + ri.getPercentual() + "%\n");
					rateio.append("Valor: R$" + ri.getValor() + "-quebra-");
				}				
				ch.setRateio(rateio.toString());
				
				StringBuilder parcelas = new StringBuilder();
				for (Contratoparcela p : contrato.getListaContratoparcela()) {
					parcelas.append("Data de vencimento: " + p.getDtvencimento() + "\n");
					parcelas.append("Valor da NF de Servi�o: R$" + p.getValornfservico() + "\n");
					parcelas.append("Valor da NF de Produto: R$" + p.getValornfproduto() + "\n");
					parcelas.append("Valor: R$" + p.getValor() + "\n");
					parcelas.append("Aplicar �ndice: " + (p.getAplicarindice() != null && p.getAplicarindice() ? "Sim" : "N�o") + "\n");
					parcelas.append("Cobrada: " + (p.getCobrada() != null && p.getCobrada() ? "Sim" : "N�o") + "-quebra-");
				}				
				ch.setParcelas(parcelas.toString());
			}
			
			ch.setObservacao(observacao);
			
			contratohistoricoService.saveOrUpdate(ch);
			
			if (contrato.getContratotipo()==null || Boolean.TRUE.equals(contrato.getContratotipo().getAtualizarProximoVencimentoConfirmar())){
				contrato.setDtproximovencimento(dtproximovencimento);
			}
			
			this.updateDtProximoVencimentoContrato(contrato);
			this.atualizaDtConfirmaContrato(contrato);
			this.atualizaValorRateioFaturamento(contrato);
			
			if(contrato.getPrazopagamento() != null && 
					contrato.getPrazopagamento().getCdprazopagamento() != null && 
					contrato.getListaContratoparcela() != null && 
					contrato.getListaContratoparcela().size() > 0){
				lista = this.geraParcelamentoDate(contrato);
				listaParcelas = contrato.getListaContratoparcela();
				
				if(lista.size() == listaParcelas.size()){
					Collections.sort(listaParcelas,new Comparator<Contratoparcela>(){
						public int compare(Contratoparcela o1, Contratoparcela o2) {
							return o1.getDtvencimento().compareTo(o2.getDtvencimento());
						}
					});
					
					for (int i = 0; i < lista.size(); i++) {
						contratoparcela = listaParcelas.get(i);
						contratoparcela.setDtvencimento(lista.get(i));
						contratoparcelaService.updateDtVencimento(contratoparcela);
					}
				}
			}
			
			if(contrato.getFrequencia() != null && 
					contrato.getFrequencia().getCdfrequencia() != null && 
					contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL) &&
					mes != null && 
					ano != null){
				contrato.setMes(mes);
				contrato.setAno(ano);
				this.atualizaMesAnoContrato(contrato);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaCampoIsentoContrato(Contrato contrato, boolean isento)
	 *
	 * @param contrato
	 * @param isento
	 * @author Luiz Fernando
	 */
	public void atualizaCampoIsentoContrato(Contrato contrato, boolean isento) {
		contratoDAO.atualizaCampoIsentoContrato(contrato, isento);
		
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param nfs
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contrato findInscricaoEstadualContratoByNota(NotaFiscalServico nfs) {
		return contratoDAO.findInscricaoEstadualContratoByNota(nfs);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean haveContratoGerarNotaAntes(String whereIn) {
		return contratoDAO.haveContratoGerarNotaAntes(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 03/08/2010
	 */
	public boolean haveDesagioDesconto(String whereIn){
		return contratoDAO.haveDesagioDesconto(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#updateDataCancelamento
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 */
	public void updateDataCancelamento(String itensSelecionados) {
		contratoDAO.updateDataCancelamento(itensSelecionados);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#updateDtsupensaoCancelamen
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 */
	public void updateDtsupensaoCancelamento(String itensSelecionados) {
		contratoDAO.updateDtsupensaoCancelamento(itensSelecionados);
	}
	
	public void updateEstornoCancelamento(String itensSelecionados) {
		contratoDAO.updateEstornoCancelamento(itensSelecionados);
	}
	
	public Contrato findLastByProjeto(Projeto projeto) {
		return contratoDAO.findLastByProjeto(projeto);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public Contrato clienteByContrato(Integer cdcontrato){
		return contratoDAO.clienteByContrato(cdcontrato);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public Contrato servicosByContrato(Contrato contrato) {
		return contratoDAO.servicosByContrato(contrato);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @author Taidson
	 * @since 17/12/2010
	 */
	public void suspendeContratos(final String whereIn, MotivoCancelamentoFaturamento motivoCancelamentoFaturamento, String observacao) {
		String[] ids = whereIn.split(",");
		Contratohistorico contratohistorico;
		for (int i = 0; i < ids.length; i++) {
			contratoDAO.setDataSuspensao(Integer.parseInt(ids[i]));
			this.updateAux(Integer.parseInt(ids[i]));
			contratohistorico = new Contratohistorico(new Contrato(Integer.parseInt(ids[i])), new Timestamp(System.currentTimeMillis()), Contratoacao.SUSPENSO, SinedUtil.getUsuarioLogado());
			contratohistorico.setMotivoCancelamentoFaturamento(motivoCancelamentoFaturamento);
			contratohistorico.setObservacao(observacao);
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
	}
	
	/**
	 * M�todo que prepara os contratos suspensos para envio de email
	 * 
	 * @param whereIn
	 * @return
	 * @Author Tom�s Rabelo 
	 */
	public String emailContratoSuspenso(String whereIn) {
		String msg = "";
		List<Contrato> lista = this.findContratosComResponsaveis(whereIn);
		if(lista != null && !lista.isEmpty()){
			try{
				enviaEmailContratoSuspenso(lista);
			} catch (Exception e) {
				e.printStackTrace();
				msg += "E-mail n�o enviado.";
			}
			return msg;
		}
		return msg;
	}
	
	/**
	 * M�todo que envia email para os respons�veis avisando que o contrato foi suspenso
	 * 
	 * @param lista
	 * @author Tom�s Rabelo
	 * @throws Exception 
	 */
	private void enviaEmailContratoSuspenso(List<Contrato> lista) throws Exception {
		Empresa empresa = empresaService.loadPrincipal();
		
		for (Contrato contrato : lista) {

			String assunto = "Contrato "+contrato.getDescricao()+" suspenso.";
			
			if(contrato.getCliente() != null && contrato.getCliente().getNome() != null && !"".equals(contrato.getCliente().getNome())){
				assunto += " - " + contrato.getCliente().getNome();
			}
			String mensagem = "<b>Aviso do sistema:</b> O contrato "+contrato.getDescricao()+", do cliente "+contrato.getCliente().getNome()+", foi suspenso.";
			
			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			
			Envioemail envioemail = envioemailService.registrarEnvio(
					empresa.getEmail(),
					assunto,
					mensagem,
					Envioemailtipo.AVISO_CONTRATO_SUSPENSO,
					contrato.getEmpresa(),
					contrato.getResponsavel().getEmail(),
					contrato.getResponsavel().getNome(),
					email);
			
			email.setFrom(empresa.getEmail());
			email.setSubject(assunto);
			email.setTo(contrato.getResponsavel().getEmail());
			email.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, contrato.getResponsavel().getEmail()));
			email.sendMessage();
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contrato> findContratosComResponsaveis(String whereIn) {
		return contratoDAO.findContratosComResponsaveis(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#loadWithLista
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return contratoDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ContratoDAO#findTodosContrato
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findTodosContrato(ContratoFiltro filtro) {
		return contratoDAO.findTodosContrato(filtro);
	}
	
	/**
	 * Faz a gera��o do faturamento do(s) contrato(s).
	 *
	 * @param listaContrato
	 * @param agrupamento
	 * @author Rodrigo Freitas
	 * 
	 */
	public String gerarFaturamento(List<Contrato> listaContrato, Boolean agrupamento) {
		List<NotaFiscalServico> listaNotaServico = new ArrayList<NotaFiscalServico>();
		List<Notafiscalproduto> listaNotaProduto = new ArrayList<Notafiscalproduto>();
		List<Documento> listaContaReceber = new ArrayList<Documento>();
		List<FaturamentoConjuntoBean> listaFaturamentoConjunto = new ArrayList<FaturamentoConjuntoBean>();
		
		Documentotipo documentotipo = documentotipoService.findForFaturamentoContareceber();
		
		String whereInContrato = CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ",");
		if(faturamentocontratohistoricoService.haveFaturamentoNaoFinalizadoByContrato(whereInContrato)){
			throw new SinedException("Existem contrato(s) com faturamento n�o finalizados, favor entrar em contato com o administrador do sistema para a resolu��o do problema.");
		}
		
//		String paramValidacao = parametrogeralService.getValorPorNome(Parametrogeral.VALIDACAO_FATURAMENTO_DATAVENCIMENTO);
//		if(paramValidacao != null && "TRUE".equals(paramValidacao.trim().toUpperCase())){
//			for (Contrato c : listaContrato) {
//				if(faturamentocontratohistoricoService.haveFaturamentoByContratoDtvencimento(c, c.getDtproximovencimento())){
//					throw new SinedException("Exitem contrato(s) com faturamento com a mesma data de vencimento, favor entrar em contato com o administrador do sistema para a resolu��o do problema.");
//				}
//			}
//		}
		
		List<Faturamentocontratohistorico> listaFaturamentocontratohistorico = new ArrayList<Faturamentocontratohistorico>();
		for (Contrato c : listaContrato) {
			Faturamentocontratohistorico faturamentocontratohistorico = new Faturamentocontratohistorico();
			faturamentocontratohistorico.setContrato(c);
			faturamentocontratohistorico.setDtvencimento(c.getDtproximovencimento());
			faturamentocontratohistorico.setFinalizado(Boolean.FALSE);
			faturamentocontratohistoricoService.saveOrUpdate(faturamentocontratohistorico);
			
			listaFaturamentocontratohistorico.add(faturamentocontratohistorico);
		}
		
		for (Contrato c : listaContrato) {
			if(c.getRateio() != null && c.getRateio().getCdrateio() != null){
				c.setRateio(rateioService.findRateio(c.getRateio()));
			}
			if(c.getTaxa() != null && c.getTaxa().getCdtaxa() != null){
				c.setTaxa(taxaService.findTaxa(c.getTaxa()));
			}
			if(c.getGrupotributacao() == null){	
				Cliente clienteGrupotributacao = clienteService.load(c.getCliente(), "cliente.grupotributacao");
				if (clienteGrupotributacao != null && clienteGrupotributacao.getGrupotributacao() != null){
					c.setGrupotributacao(clienteGrupotributacao.getGrupotributacao());
				}		
			}
			
			Formafaturamento formafaturamento = c.getFormafaturamento();
			
			boolean briefcase = SinedUtil.isBriefcase();
			
			Documentotipo documentotipoBoleto = null;
			if(Formafaturamento.SOMENTE_BOLETO.equals(c.getFormafaturamento())){
				documentotipoBoleto = documentotipoService.findBoleto();
			}
			if(formafaturamento.equals(Formafaturamento.SOMENTE_BOLETO) ||
					(briefcase && formafaturamento.equals(Formafaturamento.NOTA_APOS_RECEBIMENTO))){
				this.geraContareceberFaturamento(c, listaContaReceber, documentotipoBoleto != null ? documentotipoBoleto : documentotipo);
			} else if(formafaturamento.equals(Formafaturamento.NOTA_ANTES_RECEBIMENTO)){
				Faturamentocontrato faturamentocontrato = new Faturamentocontrato();
				faturamentocontratoService.saveOrUpdate(faturamentocontrato);
				c.setFaturamentocontrato(faturamentocontrato);
				Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(c.getEmpresa());
				if(c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (isMunicipioBrasilia || (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial()))){
					this.geraNotasSeparadasFaturamento(c, listaNotaServico, listaNotaProduto);
				} else {
					if(c.getContratotipo()!=null && c.getContratotipo().getLocacao()!=null && c.getContratotipo().getLocacao()){
						this.geraNotasSeparadasFaturamento(c, listaNotaServico, listaNotaProduto);
					}else{
						this.geraNotaServicoFaturamento(c, listaNotaServico, null);	
					}
				}
			} else if(formafaturamento.equals(Formafaturamento.NOTA_APOS_RECEBIMENTO)){
				this.geraContarecerNotasFaturamento(c, listaFaturamentoConjunto, documentotipo);
			}
		}
		
		if(agrupamento){
			listaNotaServico = this.agrupamentoNotaServico(listaNotaServico);
			listaNotaProduto = this.agrupamentoNotaProduto(listaNotaProduto);
			listaContaReceber = this.agrupamentoContaReceber(listaContaReceber);
		}
		
		String msg = validaValorNotas(listaNotaProduto, listaNotaServico);
		if(msg == null || msg.equals("")){
			this.salvaListaNotaServico(listaNotaServico);
			this.salvaListaNotaProduto(listaNotaProduto);
			this.salvaListaContaReceber(listaContaReceber);
			this.salvaListaFaturamentoConjunto(listaFaturamentoConjunto, true);
			
			this.verificaComissionamento(listaNotaServico, listaNotaProduto, listaContaReceber);
			
			if(listaFaturamentoConjunto != null && !listaFaturamentoConjunto.isEmpty()){
				List<Documento> listaDocumentoConjunto = new ArrayList<Documento>();
				Documento documento;
				for(FaturamentoConjuntoBean faturamentoConjuntoBean : listaFaturamentoConjunto){
					if(faturamentoConjuntoBean.getDocumento() != null && faturamentoConjuntoBean.getDocumento().getCddocumento() != null){
						documento = faturamentoConjuntoBean.getDocumento();
						if(faturamentoConjuntoBean.getNotafiscalproduto() != null){
							documento.setNota(faturamentoConjuntoBean.getNotafiscalproduto());
						}else if(faturamentoConjuntoBean.getNotaFiscalServico() != null){
							documento.setNota(faturamentoConjuntoBean.getNotaFiscalServico());
						}
						listaDocumentoConjunto.add(documento);
					}
				}
				this.verificaComissionamento(null, null, listaDocumentoConjunto);
			}
			
			for (Contrato c : listaContrato) {
				if(c.getAcrescimoproximofaturamento() != null && c.getAcrescimoproximofaturamento()){
					this.updateZeraAcrescimo(c);
				}
				this.atualizaParcelaCobradaFaturamento(c);
				this.atualizaDtProximoVencimentoFaturamento(c);
				this.atualizaValorRateioFaturamento(c);
				this.updateAux(c.getCdcontrato());
			}
		}
		
		this.enviaEmailFaturamentoLote(listaContrato);
		
		if(listaFaturamentocontratohistorico != null && listaFaturamentocontratohistorico.size() > 0){
			String whereInFaturamentohitorico = CollectionsUtil.listAndConcatenate(listaFaturamentocontratohistorico, "cdfaturamentocontratohistorico", ",");
			faturamentocontratohistoricoService.updateFinalizado(whereInFaturamentohitorico);
		}
		
		return msg;
	}
	
	private void enviaEmailFaturamentoLote(List<Contrato> listaContrato) {
		if(listaContrato.size() >= 0){
			String emailResumoFaturamentoParam = parametrogeralService.getValorPorNome(Parametrogeral.EMAIL_FATURAMENTO_LOTE);
			if(emailResumoFaturamentoParam != null && !emailResumoFaturamentoParam.trim().equals("")){
				try {
					StringBuilder corpoEmail = new StringBuilder();
					
					corpoEmail.append("Resumo do faturamento de " + listaContrato.size() + " contrato(s): <BR><BR>");
					
					for (Contrato contrato : listaContrato) {
						corpoEmail
							.append("Contrato: ")
							.append(contrato.getCdcontrato())
							.append(" (")
							.append(contrato.getCliente().getNome())
							.append(") - R$ ")
							.append(contrato.getValor())
							.append("<BR>");
					}
					
					String[] emails = emailResumoFaturamentoParam.split(";");
					Empresa empresa = empresaService.loadPrincipal();
					
					new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
						.setSubject("Resumo faturamento - " + SinedDateUtils.dataExtenso(SinedDateUtils.currentDate()) + " " + SinedDateUtils.toString(SinedDateUtils.currentTimestamp(), "HH:mm:ss"))
						.setFrom(empresa.getEmail())
						.setListTo(Arrays.asList(emails))
						.addHtmlText(corpoEmail.toString())
						.sendMessage();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Contratohistorico criaHistoricoOSContratoAndUpdateRequisicaoestado(Contrato contrato, Contratohistorico contratohistorico) {
		if(contrato != null && contrato.getCdcontrato() != null && contrato.getContratotipo() != null && 
				contrato.getContratotipo().getFaturarhoratrabalhada() != null &&
				contrato.getContratotipo().getFaturarhoratrabalhada()){
			List<Apontamento> listaApontamento = apontamentoService.findForFaturarByContrato(contrato);
			if(listaApontamento != null && !listaApontamento.isEmpty()){
				StringBuilder whereInRequisicao = new StringBuilder();
				StringBuilder whereInRequisicaoHistorico = new StringBuilder();
				List<Requisicao> listaRequisicao = new ArrayList<Requisicao>();
				for(Apontamento apontamento : listaApontamento){
					if(apontamento.getRequisicao() != null && apontamento.getRequisicao().getCdrequisicao() != null){
						if(!listaRequisicao.contains(apontamento.getRequisicao())){
							whereInRequisicao.append(!whereInRequisicao.toString().equals("") ? "," : "")
											 .append(apontamento.getRequisicao().getCdrequisicao());
							
							whereInRequisicaoHistorico.append(!whereInRequisicaoHistorico.toString().equals("") ? ", " : "")
							 .append("<a href=\"javascript:visualizarOS("+apontamento.getRequisicao().getCdrequisicao()+")\">"+apontamento.getRequisicao().getCdrequisicao()+"</a>");
							
							listaRequisicao.add(apontamento.getRequisicao());
						}
					}
				}
				
				if(listaRequisicao != null && !listaRequisicao.isEmpty()){
					if(whereInRequisicao != null && !whereInRequisicao.toString().equals("")){
						requisicaoService.updateEstados(whereInRequisicao.toString(), new Requisicaoestado(Requisicaoestado.FATURADO));
					}
					
					for(Requisicao requisicao : listaRequisicao){
						Requisicaohistorico requisicaohistorico = new Requisicaohistorico();
						requisicaohistorico.setRequisicao(requisicao);
						requisicaohistorico.setObservacao("Faturamento do contrato <a href=\"javascript:visualizarContrato("+contrato.getCdcontrato()+")\">"+contrato.getCdcontrato()+"</a>. ");
						requisicaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
						requisicaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						
						requisicaohistoricoService.saveOrUpdate(requisicaohistorico);
					}
					
					if(whereInRequisicaoHistorico != null && !whereInRequisicaoHistorico.toString().equals("")){
						String obs = "Ordem(ns) de Servi�o(s) relacionadas � apura��o: " + whereInRequisicaoHistorico.toString();
						if(contratohistorico != null){
							 contratohistorico.setObservacao((contratohistorico.getObservacao() != null && !contratohistorico.getObservacao().equals("") ?
									 contratohistorico.getObservacao() + "<BR>" : "") + obs);
						}else {
							contratohistorico = new Contratohistorico();
							contratohistorico.setContrato(contrato);
							contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
							contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
							contratohistorico.setObservacao((contratohistorico.getObservacao() != null && !contratohistorico.getObservacao().equals("") ?
									 contratohistorico.getObservacao() : "") + obs);
						}
					}
				}
			}
		}
		
		return contratohistorico;
	}
	
	private void geraContarecerNotasFaturamento(Contrato c, List<FaturamentoConjuntoBean> listaFaturamentoConjunto, Documentotipo documentotipo) {
		Documento documento = this.criaContaReceberContrato(c, documentotipo, false);
		if(documento.getValor() == null || documento.getValor().getValue().doubleValue() <= 0d){
			return;
		}
		
		List<NotaFiscalServico> listaNotaServico = new ArrayList<NotaFiscalServico>(); 
		List<Notafiscalproduto> listaNotaProduto = new ArrayList<Notafiscalproduto>(); 
		Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(c.getEmpresa());
		
		if(c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (isMunicipioBrasilia || (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial()))){
			this.geraNotasSeparadasFaturamento(c, listaNotaServico, listaNotaProduto);
		} else {
			if(c.getContratotipo() != null && 
					c.getContratotipo().getLocacao() != null && 
					c.getContratotipo().getLocacao()){
				this.geraNotasSeparadasFaturamento(c, listaNotaServico, listaNotaProduto);
			}else{
				this.geraNotaServicoFaturamento(c, listaNotaServico, null);	
			}
			
		}
		
		NotaFiscalServico notaFiscalServico = listaNotaServico != null && listaNotaServico.size() > 0 ? listaNotaServico.get(0) : null;
		Notafiscalproduto notafiscalproduto = listaNotaProduto != null && listaNotaProduto.size() > 0 ? listaNotaProduto.get(0) : null;
		
		if(notaFiscalServico != null){
			notaFiscalServico.setNotaStatus(NotaStatus.EM_ESPERA);
		}
		if(notafiscalproduto != null){
			notafiscalproduto.setNotaStatus(NotaStatus.EM_ESPERA);
		}
		
		FaturamentoConjuntoBean faturamentoConjuntoBean = new FaturamentoConjuntoBean();
		faturamentoConjuntoBean.setDocumento(documento);
		faturamentoConjuntoBean.setNotaFiscalServico(notaFiscalServico);
		faturamentoConjuntoBean.setNotafiscalproduto(notafiscalproduto);
		
		listaFaturamentoConjunto.add(faturamentoConjuntoBean);
	}
	
	/**
	 * Verifica o comissionamento das listas passadas por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#verificaComissionamentoDocumento
	 * @see br.com.linkcom.sined.geral.service.ContratoService#verificaComissionamentoNotaServico
	 * @see br.com.linkcom.sined.geral.service.ContratoService#verificaComissionamentoNotaProduto
	 *
	 * @param listaNotaServico
	 * @param listaNotaProduto
	 * @param listaContaReceber
	 * @author Rodrigo Freitas
	 */
	private void verificaComissionamento(List<NotaFiscalServico> listaNotaServico, List<Notafiscalproduto> listaNotaProduto, List<Documento> listaContaReceber) {
		this.verificaComissionamentoDocumento(listaContaReceber, false);
		this.verificaComissionamentoNotaServico(listaNotaServico, false);
		this.verificaComissionamentoNotaProduto(listaNotaProduto, false);
	}
	
	/**
	 * Cria o comissionamento para Nota fiscal de produto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#criaComissaoFaturamento
	 *
	 * @param listaNotaProduto
	 * @author Rodrigo Freitas
	 */
	private void verificaComissionamentoNotaProduto(List<Notafiscalproduto> listaNotaProduto, boolean verificarExistencia) {
		if(listaNotaProduto != null && !listaNotaProduto.isEmpty()){
			for (Notafiscalproduto nota : listaNotaProduto) {
				if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
					for (Contrato contrato : nota.getListaContrato()) {
						this.criaComissaoFaturamento(contrato, null, nota, verificarExistencia);
					}
				}
			}
		}
	}
	
	/**
	 * Cria o comissionamento para Nota fiscal de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#criaComissaoFaturamento
	 *
	 * @param listaNotaServico
	 * @author Rodrigo Freitas
	 */
	private void verificaComissionamentoNotaServico(List<NotaFiscalServico> listaNotaServico, boolean verificarExistencia) {
		if(listaNotaServico != null && !listaNotaServico.isEmpty()){
			for (NotaFiscalServico nota : listaNotaServico) {
				if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
					for (Contrato contrato : nota.getListaContrato()) {
						this.criaComissaoFaturamento(contrato, null, nota, verificarExistencia);
					}
				}
			}
		}
	}
	
	/**
	 * Cria o comissionamento para Documentos.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#verificaComissionamentoDocumento(Documento documento, boolean verificarExistencia)
	 *
	 * @param listaContaReceber
	 * @author Rodrigo Freitas
	 */
	public void verificaComissionamentoDocumento(List<Documento> listaContaReceber, boolean verificarExistencia, boolean forBaixaConta) {
		if(listaContaReceber != null && !listaContaReceber.isEmpty()){
			for (Documento documento : listaContaReceber) {
				verificaComissionamentoDocumento(documento, verificarExistencia, forBaixaConta);
			}
		}
	}
	
	public void verificaComissionamentoDocumento(List<Documento> listaContaReceber, boolean verificarExistencia) {
		verificaComissionamentoDocumento(listaContaReceber, verificarExistencia, false);
	}
	
	/**
	* Cria o comissionamento para o Documento.
	*
	* @see br.com.linkcom.sined.geral.service.ContratoService#criaComissaoFaturamento(Contrato contrato, Documento documento, Nota nota, boolean verificarExistencia)
	*
	* @param documento
	* @param verificarExistencia
	* @since 14/08/2014
	* @author Luiz Fernando
	*/
	public void verificaComissionamentoDocumento(Documento documento, boolean verificarExistencia, boolean forBaixaConta) {
		if(documento != null && documento.getCddocumento() != null){
			List<Contrato> listaContrato = documento.getListaContrato();
			if(listaContrato == null || listaContrato.size() == 0){
				listaContrato = new ArrayList<Contrato>();
				if(documento.getContrato() != null){
					listaContrato.add(documento.getContrato());
				}
			}
			for (Contrato contrato : listaContrato) {
				if(forBaixaConta){
					this.reprocessaValoresComissionamento(contrato, documento, documento.getNota(), verificarExistencia);
				}else{
					this.criaComissaoFaturamento(contrato, documento, documento.getNota(), verificarExistencia);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os dados para verificar a cria��o do comissionamento
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findForComissaoByContrato(Contrato contrato)
	*
	* @param contrato
	* @return
	* @since Nov 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public Contrato findForComissaoByContrato(Contrato contrato){
		return contratoDAO.findForComissaoByContrato(contrato);
	}
	
	/**
	 * M�todo gen�rico para criar comiss�o para o contrato de um documento ou nota.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#findForComissaoByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @param documento
	 * @param nota
	 * @author Rodrigo Freitas
	 * @param verificarExistencia 
	 */
	private void criaComissaoFaturamento(Contrato contrato, Documento documento, Nota nota, boolean verificarExistencia) {
		Documentocomissao documentocomissao;
		Comissionamento vendedorComissionamento, colaboradorComissionamento;
		Pessoa vendedor;
		Colaborador colaborador;
		
		if(documento != null && documento.getCddocumento() != null && (documento.getValoratual() == null || documento.getValoratual().getValue().doubleValue() <= 0)){
			Documento docauxiliar = contareceberService.loadWithValoratual(documento);
			if(docauxiliar != null && docauxiliar.getAux_documento() != null && docauxiliar.getAux_documento().getValoratual() != null){
				documento.setValoratual(docauxiliar.getAux_documento().getValoratual());
				documento.setAux_documento(docauxiliar.getAux_documento());
			}
		}
		
		Contrato aux_contrato = this.findForComissaoByContrato(contrato);
		Boolean documentonegociado = Boolean.FALSE;
		Boolean comissaovalorpago = Boolean.FALSE;
		
		if(aux_contrato.getVendedor() != null){
			vendedor = aux_contrato.getVendedor();
			vendedorComissionamento = aux_contrato.getComissionamento();			
			
			if(vendedorComissionamento != null){
				documentocomissao = new Documentocomissao();
				
				documentocomissao.setDocumento(documento);
				documentocomissao.setNota(nota);
				documentocomissao.setPessoa(vendedor);
				documentocomissao.setComissionamento(vendedorComissionamento);
				documentocomissao.setContrato(contrato);
				documentocomissao.setVendedor(true);
				
				boolean podeGerarComissao = documentocomissaoService.podeGerarComissao(aux_contrato, nota, documento, vendedor, 
						vendedorComissionamento.getQuantidade(), verificarExistencia, documentocomissao);
				if(podeGerarComissao || verificarExistencia){
					
					if(documento != null){
						if(vendedorComissionamento.getTipobccomissionamento() != null && vendedorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
							if(documento.getDocumentoacao() != null && documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
								documentonegociado = Boolean.TRUE;
								comissaovalorpago = Boolean.TRUE;;
							}else {
								documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValorPagoComissionamento(), vendedor, Boolean.TRUE, null, documentocomissao));
							}
						}else {
							if(nota != null && vendedorComissionamento != null && vendedorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
								Integer numParcela = parcelamentoService.getNumeroParcelaByDocumento(documento);
								if(vendedorComissionamento.getConsiderardiferencapagamento() != null && vendedorComissionamento.getConsiderardiferencapagamento()){
									Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, nota.getValorBruto(), documento.getValorPagoComissionamento(), nota.getValorNota(),numParcela);
									documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, vendedor, Boolean.TRUE));
								}else {
									documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorBruto().divide(new Money(numParcela)), vendedor, Boolean.TRUE));
								}
							} else { 
								if(vendedorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO) && vendedorComissionamento.getConsiderardiferencapagamento() != null && vendedorComissionamento.getConsiderardiferencapagamento() && 
										vendedorComissionamento.getConsiderardiferencapagamento() != null && vendedorComissionamento.getConsiderardiferencapagamento()){
									Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, documento.getValor(), documento.getValorPagoComissionamento(), documento.getValor(), 1);
									documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, vendedor, Boolean.TRUE, null, documentocomissao));
								}else {
									documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValor(), vendedor, Boolean.TRUE, null, documentocomissao));
								}
							}
						}
					} else if(nota != null){
						if(vendedorComissionamento.getTipobccomissionamento() == null || vendedorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_LIQUIDO)){
							documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorNota(), vendedor, Boolean.TRUE, null, documentocomissao));
						} else if(vendedorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
							documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorBruto(), vendedor, Boolean.TRUE, null, documentocomissao));
						} else {
							documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorNota(), vendedor, Boolean.TRUE, null, documentocomissao));					
						}
					}
					
					if(!comissaovalorpago || !documentonegociado){
						if(verificarExistencia){
							List<Documentocomissao> lista = documentocomissaoService.findDocumentocomissao(documento, nota, vendedor, vendedorComissionamento, contrato, Boolean.TRUE);
							List<Documentocomissao> listaRemover = new ArrayList<Documentocomissao>();
							if(lista != null && lista.size() > 0){
								for (Documentocomissao doccomissao : lista) {
									if(doccomissao.getColaboradorcomissao() == null){
										if(documentocomissao.getComissionamento() != null){
											doccomissao.setComissionamento(documentocomissao.getComissionamento());
										}
										if(podeGerarComissao){
											documentocomissaoService.updateValorComissao(doccomissao, documentocomissao.getValorcomissao());
										}else {
											listaRemover.add(doccomissao);
										}
									}
								}
								if(listaRemover != null && !listaRemover.isEmpty()){
									for (Documentocomissao doccomissaoRemover : lista) {
										documentocomissaoService.deleteDocumentocomissao(doccomissaoRemover);
									}
								}
							} else if(podeGerarComissao){
								documentocomissaoService.saveOrUpdate(documentocomissao);
							}
						} else  if(podeGerarComissao){
							documentocomissaoService.saveOrUpdate(documentocomissao);
						}
					}else {
						List<Documentocomissao> listaExlusaoNegociado = documentocomissaoService.findDocumentocomissaoByDocumentonegociado(documento, nota, vendedor, vendedorComissionamento, contrato, Boolean.TRUE);
						if(listaExlusaoNegociado != null && !listaExlusaoNegociado.isEmpty()){
							for (Documentocomissao doccomissao : listaExlusaoNegociado) {
								documentocomissaoService.deleteDocumentocomissao(doccomissao);
							}
						}
					}
				}
			}else if(verificarExistencia){
				List<Documentocomissao> lista = documentocomissaoService.findDocumentocomissao(documento, nota, vendedor, null, contrato, Boolean.TRUE);
				if(lista != null && lista.size() > 0){
					List<Documentocomissao> listaRemover = new ArrayList<Documentocomissao>();
					for (Documentocomissao doccomissao : lista) {
						if(doccomissao.getColaboradorcomissao() == null){
							listaRemover.add(doccomissao);
						}
					}
					if(listaRemover != null && !listaRemover.isEmpty()){
						for (Documentocomissao doccomissaoRemover : lista) {
							documentocomissaoService.deleteDocumentocomissao(doccomissaoRemover);
						}
					}
				}
			}
		}
		
		if(aux_contrato.getListaContratocolaborador() != null && aux_contrato.getListaContratocolaborador().size() > 0){
			for (Contratocolaborador cc : aux_contrato.getListaContratocolaborador()) {
				colaborador = cc.getColaborador();
				colaboradorComissionamento = cc.getComissionamento();				
				
				if(colaboradorComissionamento != null ){
					documentocomissao = new Documentocomissao();
					
					documentocomissao.setDocumento(documento);
					documentocomissao.setNota(nota);
					documentocomissao.setPessoa(colaborador);
					documentocomissao.setComissionamento(colaboradorComissionamento);
					documentocomissao.setContrato(contrato);
					documentocomissao.setVendedor(false);
					
					boolean podeGerarComissao = documentocomissaoService.podeGerarComissao(aux_contrato, nota, documento, colaborador, 
							colaboradorComissionamento.getQuantidade(), verificarExistencia, documentocomissao);
					if(podeGerarComissao || verificarExistencia){
						documentonegociado = Boolean.FALSE;
						comissaovalorpago = Boolean.FALSE;
						
						
						if(documento != null){
							if(colaboradorComissionamento.getTipobccomissionamento() != null && colaboradorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
								if(documento.getDocumentoacao() != null && documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
									documentonegociado = Boolean.TRUE;
									comissaovalorpago = Boolean.TRUE;;
								}else {
									documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValorPagoComissionamento(), colaborador, Boolean.FALSE, null, documentocomissao));
								}
							}else {
								if(nota != null && colaboradorComissionamento != null && colaboradorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
									Integer numParcela = parcelamentoService.getNumeroParcelaByDocumento(documento);
									if(colaboradorComissionamento.getConsiderardiferencapagamento() != null && colaboradorComissionamento.getConsiderardiferencapagamento()){
										Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, nota.getValorBruto(), documento.getValorPagoComissionamento(), nota.getValorNota(), numParcela);
										documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, colaborador, Boolean.FALSE, null, documentocomissao));
									}else {
										documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorBruto().divide(new Money(numParcela)), colaborador, Boolean.FALSE, null, documentocomissao));
									}
								} else {
									if(colaboradorComissionamento != null && colaboradorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO) && 
											colaboradorComissionamento.getConsiderardiferencapagamento() != null && colaboradorComissionamento.getConsiderardiferencapagamento()){
										Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, documento.getValor(), documento.getValorPagoComissionamento(), documento.getValor(), 1);
										documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, colaborador, Boolean.FALSE, null, documentocomissao));
									}else {
										documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValor(), colaborador, Boolean.FALSE, null, documentocomissao));
									}
								}
							}
						} else if(nota != null){
							if(colaboradorComissionamento.getTipobccomissionamento() == null || colaboradorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_LIQUIDO)){
								documentocomissao.setValorcomissao(calculaValorComissao(contrato, nota.getValorNota(), colaborador, Boolean.FALSE, null, documentocomissao));
							} else if(colaboradorComissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
								documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorBruto(), colaborador, Boolean.FALSE, null, documentocomissao));
							}else {
								documentocomissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorNota(), colaborador, Boolean.FALSE, null, documentocomissao));						
							}
						}
	
						if(!comissaovalorpago || !documentonegociado){
							if(verificarExistencia){
								List<Documentocomissao> lista = documentocomissaoService.findDocumentocomissao(documento, nota, colaborador, colaboradorComissionamento, contrato, Boolean.FALSE);
								List<Documentocomissao> listaRemover = new ArrayList<Documentocomissao>();
								if(lista != null && lista.size() > 0){
									for (Documentocomissao doccomissao : lista) {
										if(doccomissao.getColaboradorcomissao() == null){
											if(documentocomissao.getComissionamento() != null){
												doccomissao.setComissionamento(documentocomissao.getComissionamento());
											}
											if(podeGerarComissao){
												documentocomissaoService.updateValorComissao(doccomissao, documentocomissao.getValorcomissao());
											}else {
												listaRemover.add(doccomissao);
											}
										}
									}
									if(listaRemover != null && !listaRemover.isEmpty()){
										for (Documentocomissao doccomissaoRemover : lista) {
											documentocomissaoService.deleteDocumentocomissao(doccomissaoRemover);
										}
									}
								} else if(podeGerarComissao){
									documentocomissaoService.saveOrUpdate(documentocomissao);
								}
							} else if(podeGerarComissao){
								documentocomissaoService.saveOrUpdate(documentocomissao);
							}
						}else {
							List<Documentocomissao> listaExlusaoNegociado = documentocomissaoService.findDocumentocomissaoByDocumentonegociado(documento, nota, colaborador, colaboradorComissionamento, contrato, Boolean.FALSE);
							if(listaExlusaoNegociado != null && !listaExlusaoNegociado.isEmpty()){
								for (Documentocomissao doccomissao : listaExlusaoNegociado) {
									documentocomissaoService.deleteDocumentocomissao(doccomissao);
								}
							}
						}
					}
				}else if(verificarExistencia){
					List<Documentocomissao> lista = documentocomissaoService.findDocumentocomissao(documento, nota, colaborador, null, contrato, Boolean.FALSE);
					if(lista != null && lista.size() > 0){
						List<Documentocomissao> listaRemover = new ArrayList<Documentocomissao>();
						for (Documentocomissao doccomissao : lista) {
							if(doccomissao.getColaboradorcomissao() == null){
								listaRemover.add(doccomissao);
							}
						}
						if(listaRemover != null && !listaRemover.isEmpty()){
							for (Documentocomissao doccomissaoRemover : lista) {
								documentocomissaoService.deleteDocumentocomissao(doccomissaoRemover);
							}
						}
					}
				}
			}
		}
	}
	
	private void reprocessaValoresComissionamento(Contrato contrato, Documento documento, Nota nota, boolean verificarExistencia) {
		if(!Documentoacao.NEGOCIADA.equals(documento.getDocumentoacao())){
		
			if(documento != null && documento.getCddocumento() != null && (documento.getValoratual() == null || documento.getValoratual().getValue().doubleValue() <= 0)){
				Documento docauxiliar = contareceberService.loadWithValoratual(documento);
				if(docauxiliar != null && docauxiliar.getAux_documento() != null && docauxiliar.getAux_documento().getValoratual() != null){
					documento.setValoratual(docauxiliar.getAux_documento().getValoratual());
					documento.setAux_documento(docauxiliar.getAux_documento());
				}
			}
			
			Contrato aux_contrato = this.findForComissaoByContrato(contrato);

			List<Documentocomissao> listadocumentoComissao = documentocomissaoService.findDocumentocomissao(documento, nota, null, null, contrato, Boolean.FALSE);
			listadocumentoComissao.addAll(documentocomissaoService.findDocumentocomissao(documento, nota, null, null, contrato, Boolean.TRUE));

			for(Documentocomissao documentoComissao: listadocumentoComissao){
				boolean isVendedor = Boolean.TRUE.equals(documentoComissao.getVendedor());
				if(documento != null){
					if(Tipobccomissionamento.VALOR_PAGO.equals(documentoComissao.getComissionamento().getTipobccomissionamento())){
						if(!Documentoacao.NEGOCIADA.equals(documento.getDocumentoacao())){
							documentoComissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValorPagoComissionamento(), null, isVendedor, null, documentoComissao));
						}
					}else {
						Comissionamento comissionamento = documentoComissao.getComissionamento();
						if(nota != null && comissionamento != null && comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
							Integer numParcela = parcelamentoService.getNumeroParcelaByDocumento(documento);
							if(Boolean.TRUE.equals(comissionamento.getConsiderardiferencapagamento())){
								Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, nota.getValorBruto(), documento.getValorPagoComissionamento(), nota.getValorNota(), numParcela);
								documentoComissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, null, isVendedor, null, documentoComissao));
							}else {
								documentoComissao.setValorcomissao(calculaValorComissao(aux_contrato, nota.getValorBruto().divide(new Money(numParcela)), null, isVendedor, null, documentoComissao));
								
							}
						} else {
							if(comissionamento != null && comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO) && 
									Boolean.TRUE.equals(comissionamento.getConsiderardiferencapagamento())){
								Money valordiferencapagamento = parcelamentoService.getValorDividoNumParcelasDiferencapagamento(documento, documento.getValor(), documento.getValorPagoComissionamento(), documento.getValor(), 1);
								documentoComissao.setValorcomissao(calculaValorComissao(aux_contrato, valordiferencapagamento, null, isVendedor, null, documentoComissao));
							}else {
								documentoComissao.setValorcomissao(calculaValorComissao(aux_contrato, documento.getValor(), null, isVendedor, null, documentoComissao));
							}
						}
					}
				}
				documentocomissaoService.updateValorComissao(documentoComissao, documentoComissao.getValorcomissao());
			}
		}
	}
	
	/**
	 * M�todo que calcula a diferen�a de pagamento
	 *
	 * @param valorbruto
	 * @param valorpago
	 * @param valorliquido
	 * @return
	 * @author Luiz Fernando
	 */
	public Money calculaValorDiferencapagamento(Money valorbruto, Money valorpago, Money valorliquido) {
		Money valor = valorbruto;
		if(valorbruto != null && valorpago != null && valorliquido != null){
			valor = valor.add((valorpago.subtract(valorliquido)));
		}
		return valor;
	}
	/**
	 * M�todo para exlcuir comiss�es dos colaboradores que foram removidos do contrato
	 *
	 * @see br.com.linkcom.sined.geral.service.ContratoService#excluiComissaoContrato(Contrato contrato, boolean verificarExistencia)
	 *
	 * @param listaNotaServico
	 * @param listaContaReceber
	 * @param listaNotaProduto
	 * @param verificarExistencia
	 * @author Luiz Fernando
	 */
	private void excluirComissioesColaboradores(List<NotaFiscalServico> listaNotaServico, List<Documento> listaContaReceber, List<Documento> listaContaReceberComNota, List<Notafiscalproduto> listaNotaProduto, boolean verificarExistencia) {		
		if(listaNotaProduto != null && !listaNotaProduto.isEmpty()){
			for (Notafiscalproduto nota : listaNotaProduto) {
				if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
					for (Contrato contrato : nota.getListaContrato()) {
						this.excluiComissaoContrato(contrato, nota, null, verificarExistencia);
					}
				}
			}
		}
		if(listaNotaServico != null && !listaNotaServico.isEmpty()){
			for (NotaFiscalServico nota : listaNotaServico) {
				if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
					for (Contrato contrato : nota.getListaContrato()) {
						this.excluiComissaoContrato(contrato, nota, null, verificarExistencia);
					}
				}
			}
		}
		if(listaContaReceber != null && !listaContaReceber.isEmpty()){
			for (Documento documento : listaContaReceber) {
				if(documento.getContrato() != null){
					this.excluiComissaoContrato(documento.getContrato(), null, documento, verificarExistencia);
				}
			}
		}
		if(listaContaReceberComNota != null && !listaContaReceberComNota.isEmpty()){
			for (Documento documento : listaContaReceberComNota) {
				if(documento.getContrato() != null){
					this.excluiComissaoContrato(documento.getContrato(), null, documento, verificarExistencia);
				}
			}
		}
	}
	
	/**
	 * M�todo que busca as comiss�es a serem exclu�das e executa a exclus�o
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentocomissaoService#deleteDocumentocomissao(Documentocomissao documentocomissao)
	 * @see br.com.linkcom.sined.geral.service.ContratoService#findForComissaoByContrato(Contrato contrato)
	 * 
	 * @param contrato
	 * @param verificarExistencia
	 * @author Luiz Fernando
	 * @param nota 
	 * @param documento 
	 */
	private void excluiComissaoContrato(Contrato contrato, Nota nota, Documento documento, boolean verificarExistencia){
		Contrato aux_contrato = this.findForComissaoByContrato(contrato);
		
		StringBuilder whereNotInExclusaoVendedor = new StringBuilder();
		StringBuilder whereNotInExclusao = new StringBuilder();
		if(verificarExistencia){
			if(aux_contrato.getVendedor() != null && aux_contrato.getVendedor().getCdpessoa() != null){
				if(!"".equals(whereNotInExclusao.toString())) whereNotInExclusao.append(",");
				whereNotInExclusaoVendedor.append(aux_contrato.getVendedor().getCdpessoa());
			}
			if(aux_contrato.getListaContratocolaborador() != null && !aux_contrato.getListaContratocolaborador().isEmpty()){
				for (Contratocolaborador cc : aux_contrato.getListaContratocolaborador()) {
					if(cc.getColaborador() != null && cc.getColaborador().getCdpessoa() != null && cc.getComissionamento() != null){
						if(!"".equals(whereNotInExclusao.toString())) whereNotInExclusao.append(",");
						whereNotInExclusao.append(cc.getColaborador().getCdpessoa());
					}
				}
			}
		
			if(contrato != null && contrato.getCdcontrato() != null){
				List<Documentocomissao> listaDocumentocomissaoExclusao = documentocomissaoService.findForExcluircomissao(contrato, nota, documento, whereNotInExclusao.toString(), whereNotInExclusaoVendedor.toString());
				if(listaDocumentocomissaoExclusao != null && !listaDocumentocomissaoExclusao.isEmpty()){
					for(Documentocomissao documentocomissao : listaDocumentocomissaoExclusao){
						if(documentocomissao.getColaboradorcomissao() == null && documentocomissao.getCddocumentocomissao() != null){
							documentocomissaoService.deleteDocumentocomissao(documentocomissao);
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo para calcular a comiss�o da Pessoa
	 * 
	 * @param contrato
	 * @param valor
	 * @param pessoa
	 * @param isVendedor
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2012
	 */
	public Money calculaValorComissao(Contrato contrato, Money valor, Pessoa pessoa, Boolean isVendedor){
		return this.calculaValorComissao(contrato, valor, pessoa, isVendedor, null);
	}
	
	/**
	 * M�todo para calcular a comiss�o da Pessoa
	 * 
	 * @param contrato
	 * @param valor
	 * @param pessoa
	 * @param isVendedor
	 * @param listaContratocolaborador
	 * @param documentocomissao  
	 * @return
	 */
	public Money calculaValorComissao(Contrato contrato, Money valor, Pessoa pessoa, Boolean isVendedor, List<Contratocolaborador> listaContratocolaborador, Documentocomissao documentocomissao){
		Comissionamento comissionamento = new Comissionamento();
		Money valordeduzido = new Money();
		
		if(contrato.getValordedutivocomissionamento() != null){
			valor = valor.subtract(contrato.getValordedutivocomissionamento());
		}
		
		if(documentocomissao != null && documentocomissao.getCddocumentocomissao() != null){
			documentocomissao = documentocomissaoService.load(documentocomissao);
			if(documentocomissao.getOrigemvalorhora() != null && documentocomissao.getOrigemvalorhora().getValue().doubleValue() > 0){
				valordeduzido = documentocomissao.getOrigemvalorhora();
			}else{
				comissionamento = documentocomissao.getComissionamento();
				 if(comissionamento != null){
					 comissionamento = comissionamentoService.load(comissionamento);
					 if(comissionamento.getPercentual() != null)
						 valordeduzido = valordeduzido.add(new Money(comissionamento.getPercentual() * valor.getValue().doubleValue() / 100d));
					 else
						valordeduzido = valordeduzido.add(new Money(comissionamento.getValor()));				 
				 }
				if(!isVendedor){
					if(comissionamento.getDeduzirvalor() != null && comissionamento.getDeduzirvalor()){
						valordeduzido = valor.subtract(valordeduzido);
					}else {
						valordeduzido = valor;
					}
					
					if(comissionamento.getPercentual() != null)
						valordeduzido = new Money(comissionamento.getPercentual() * valordeduzido.getValue().doubleValue() / 100);
					else
						valordeduzido = new Money(comissionamento.getValor());
				}
			}
		}else{
			if(contrato.getComissionamento() != null){
				comissionamento = comissionamentoService.load(contrato.getComissionamento());
				if(comissionamento != null){
					if(comissionamento.getPercentual() != null)
						valordeduzido = valordeduzido.add(new Money(comissionamento.getPercentual() * valor.getValue().doubleValue() / 100d));
					else
						valordeduzido = valordeduzido.add(new Money(comissionamento.getValor()));				 
				}
			}
			
			if(!isVendedor){
				if(comissionamento.getDeduzirvalor() != null && comissionamento.getDeduzirvalor()){
					valordeduzido = valor.subtract(valordeduzido);
				}else{
					valordeduzido = valor;
				}
				
				List<Contratocolaborador> lista;
				if(listaContratocolaborador != null){
					lista = listaContratocolaborador;
				} else {
					lista = contratocolaboradorService.findContratocolaboradorByContrato(contrato);
				}
				
				if(lista != null && !lista.isEmpty()){
					for(Contratocolaborador cc : lista){
						if(cc.getColaborador().getCdpessoa().equals(pessoa.getCdpessoa())){
							if(cc.getValorhora() != null && cc.getValorhora().getValue().doubleValue() > 0){
								valordeduzido = cc.getValorhora();
								if(documentocomissao != null){
									documentocomissao.setOrigemvalorhora(cc.getValorhora());
								}
							}else if(cc.getComissionamento().getPercentual() != null)
								valordeduzido = new Money(cc.getComissionamento().getPercentual() * valordeduzido.getValue().doubleValue() / 100);
							else
								valordeduzido = new Money(cc.getComissionamento().getValor());
							break;					
						}else {
							if(cc.getComissionamento().getDeduzirvalor() != null && cc.getComissionamento().getDeduzirvalor()){
								if(cc.getValorhora() != null && cc.getValorhora().getValue().doubleValue() > 0){
									valordeduzido = cc.getValorhora();
									if(documentocomissao != null){
										documentocomissao.setOrigemvalorhora(cc.getValorhora());
									}
								}else if(cc.getComissionamento().getPercentual() != null)
									valordeduzido = valordeduzido.subtract(new Money(cc.getComissionamento().getPercentual() * valordeduzido.getValue().doubleValue() / 100));
								else
									valordeduzido = valordeduzido.subtract(new Money(cc.getComissionamento().getValor()));
							}
						}
					}
				}
			}
		}
		
		return (valordeduzido != null && valordeduzido.getValue().doubleValue() > 0 ? valordeduzido : new Money(0.0));
		
	}
	
	public Money calculaValorComissao(Contrato contrato, Money valor, Pessoa pessoa, Boolean isVendedor, List<Contratocolaborador> listaContratocolaborador){
		return calculaValorComissao(contrato, valor, pessoa, isVendedor, listaContratocolaborador, null);
	}
	
	/**
	 * Atualiza os valores do contrato e o rateio.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaValorRateioFaturamento(Contrato c) {
		boolean isMaterial = c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial());
		boolean isParcela = c.getListaContratoparcela() != null && c.getListaContratoparcela().size() > 0;
		
		if(isMaterial || isParcela){
			if(isMaterial){
				c.setValor(c.getValorServicos());
			} else if(isParcela){
				c.setValor(c.getValorParcelas());
			}
			this.atualizaValorContrato(c);
		}
		
		// CARREGAMENTO NOVAMENTE PARA N�O CONFUNDIR OS ITENS DE RATEIO
		Contrato contrato = this.load(c, "contrato.cdcontrato, contrato.rateio");
		if(contrato != null && contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null && contrato.getRateio().getListaRateioitem() != null){
			Rateio rateio = rateioService.findRateio(contrato.getRateio());
			rateioService.atualizaValorRateio(rateio, c.getValorContrato());
			rateioService.saveOrUpdate(rateio);
		}
	}
	
	/**
	 * Atualiza a data do proximo vencimento do contrato no faturamento.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaDtProximoVencimentoFaturamento(Contrato c) {
		if(c.getFrequencia() != null && c.getFrequencia().getCdfrequencia() != null){
			Calendar novaDtProximo = this.atualizaDataByFrequencia(c.getFrequencia(), SinedDateUtils.javaSqlDateToCalendar(c.getDtproximovencimento()));
			if(novaDtProximo == null) {
				c.setDtfim(new Date(System.currentTimeMillis()));
				this.atualizaDataFimContrato(c);
				return;
			}
			if(c.getFrequencia() != null && 
					c.getFrequencia().getCdfrequencia() != null &&
					c.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL) &&
					c.getMes() != null && 
					c.getAno() != null){
				if(c.getMes().ordinal() == 11){
					c.setMes(Mes.values()[0]);	
					c.setAno(c.getAno() + 1);
				} else {
					c.setMes(Mes.values()[c.getMes().ordinal() + 1]);			
				}
				this.atualizaMesAnoContrato(c);
			}
			
			c.setDtproximovencimento(new Date(novaDtProximo.getTimeInMillis()));
			this.updateDtProximoVencimentoContrato(c);
						
		} else if(c.getListaContratoparcela() != null && c.getListaContratoparcela().size() > 0){
			Contratoparcela contratoparcela = this.getParcelaAtual(c.getListaContratoparcela());
			
			if(contratoparcela != null){
				c.setDtproximovencimento(contratoparcela.getDtvencimento());
				this.updateDtProximoVencimentoContrato(c);
			} else {
				if((c.getContratotipo() == null || 
						c.getContratotipo().getLocacao() == null || 
						!c.getContratotipo().getLocacao()) &&
						c.getFrequenciarenovacao() != null){
					this.renovaParcelasContrato(c);
				} else {
					if (c.getFinalizarcontratofinalizarpagamentos() != null && c.getFinalizarcontratofinalizarpagamentos()) {
						c.setDtfim(c.getDtproximovencimento());
						this.atualizaDataFimContrato(c);
					}
				}
			}
		}
		
	}
	
	/**
	* M�todo que registra no hist�rico o estorno do contrato ap�s cancelar a �ltima nota
	*
	* @param contrato
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	private void criaHistoricoEstornoCancelamentoNota(Contrato contrato) {
		if(contrato != null && contrato.getCdcontrato() != null){
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setContrato(contrato);
			contratohistorico.setAcao(Contratoacao.ESTORNADO);
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			Usuario usuario = SinedUtil.getUsuarioLogado();
			if(usuario != null){
				contratohistorico.setUsuario(usuario);
			}
			contratohistorico.setObservacao("Estorno devido ao cancelamento da nota fiscal de servi�o");
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
	}
	
	private Calendar atualizaDataByFrequencia(Frequencia frequencia, Calendar data) {
		return atualizaDataByFrequencia(frequencia, data, false);
	}
	
	/**
	* M�todo que calcula a frequencia anterior (estorno do contrato)
	*
	* @param frequencia
	* @param data
	* @return
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	private Calendar estornarDataByFrequencia(Frequencia frequencia, Calendar data) {
		return atualizaDataByFrequencia(frequencia, data, true);
	}
	
	private Calendar atualizaDataByFrequencia(Frequencia frequencia, Calendar data, boolean estornar) {
		Calendar novaDtProximo = Calendar.getInstance();
		novaDtProximo.setTimeInMillis(data.getTimeInMillis());
		
		Integer cdfrequencia = frequencia.getCdfrequencia();
		
		switch (cdfrequencia) {
		
			case 1: //UNICA
				return null;
				
			case 2: //DIARIA
				novaDtProximo.add(Calendar.DAY_OF_MONTH, (estornar ? -1 : 1));
				break;
				
			case 3: //SEMANAL
				novaDtProximo.add(Calendar.DAY_OF_MONTH, (estornar ? -7 : 7));
				break;
				
			case 4: //QUINZENAL
				novaDtProximo.add(Calendar.DAY_OF_MONTH, (estornar ? -15 : 15));
				break;
				
			case 5: //MENSAL
				Calendar dtAux = (Calendar)novaDtProximo.clone();
				dtAux.set(Calendar.MONTH, dtAux.get(Calendar.MONTH) + (estornar ? -1 : 1));

				if (dtAux.get(Calendar.MONTH) > (novaDtProximo.get(Calendar.MONTH) + (estornar ? -1 : 1))) {
					dtAux.set(Calendar.DAY_OF_MONTH, (estornar ? -1 : 1));
					dtAux.add(Calendar.DAY_OF_MONTH, (estornar ? +1 : -1));
				}

				novaDtProximo = dtAux;
				break;
				
			case 6: // ANUAL
				novaDtProximo.add(Calendar.YEAR, (estornar ? -1 : 1));
				break;
				
			case 7: //SEMESTRAL
				novaDtProximo.add(Calendar.MONTH, (estornar ? -6 : 6));
				break;
				
			case 9: //TRIMESTE
				novaDtProximo.add(Calendar.MONTH, (estornar ? -3 : 3));
				break;
		}
		
		return novaDtProximo;
	}
	
	/**
	 * M�todo que atualiza a data inicio e fim da loca��o de acordo com a periodicidade de renova��o
	 *
	 * @param c
	 * @param dataromaneio
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public void atualizaDtinicioFimLocacao(Contrato c, Date dataromaneio) {
		if(c.getFrequenciarenovacao() != null && c.getFrequenciarenovacao().getCdfrequencia() != null && dataromaneio != null){
			Calendar novaDtProximo = this.atualizaDataByFrequencia(c.getFrequenciarenovacao(), SinedDateUtils.javaSqlDateToCalendar(dataromaneio));
			
			c.setDtiniciolocacao(dataromaneio);
			c.setDtfimlocacao(new Date(novaDtProximo.getTimeInMillis()));
			this.updatePeriodoLocacao(c, c.getDtiniciolocacao(), c.getDtfimlocacao());
		} 
	}
	
	/**
	 * Renova as parcelas do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 * @since 05/10/2012
	 */
	private void renovaParcelasContrato(Contrato c) {
		List<Contratoparcela> listaContratoparcela = c.getListaContratoparcela();
		for (Contratoparcela cp : listaContratoparcela) {
			Calendar novaDtProximo = this.atualizaDataByFrequencia(c.getFrequenciarenovacao(), SinedDateUtils.javaSqlDateToCalendar(cp.getDtvencimento()));
	
			cp.setDtvencimento(new Date(novaDtProximo.getTimeInMillis()));
			cp.setCobrada(Boolean.FALSE);
			
			contratoparcelaService.updateRenovacao(cp);
		}
		
		Contratoparcela contratoparcela = this.getParcelaAtual(listaContratoparcela);
		if(contratoparcela != null){
			c.setDtproximovencimento(contratoparcela.getDtvencimento());
			this.updateDtProximoVencimentoContrato(c);
		} else {
			c.setDtfim(c.getDtproximovencimento());
			this.atualizaDataFimContrato(c);
		}
		
		Contratohistorico contratohistorico = new Contratohistorico();
		contratohistorico.setAcao(Contratoacao.RENOVADO);
		contratohistorico.setContrato(c);
		contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
		contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
		contratohistoricoService.saveOrUpdate(contratohistorico);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaValorContrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	private void atualizaValorContrato(Contrato c) {
		contratoDAO.atualizaValorContrato(c);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaDtProximoVencimentoContrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void updateDtProximoVencimentoContrato(Contrato c) {
		contratoDAO.atualizaDtProximoVencimentoContrato(c);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaMesAnoContrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	private void atualizaMesAnoContrato(Contrato c) {
		contratoDAO.atualizaMesAnoContrato(c);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaDataFimContrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaDataFimContrato(Contrato c) {
		contratoDAO.atualizaDataFimContrato(c);
	}
	
	public void setNullDataFimContrato(Contrato c) {
		contratoDAO.setNullDataFimContrato(c);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaDtConfirmaContrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	private void atualizaDtConfirmaContrato(Contrato c) {
		contratoDAO.atualizaDtConfirmaContrato(c);
	}
	
	/**
	 * Atualiza a parcela cobrada do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaParcelaCobradaFaturamento(Contrato c) {
		if(c.getListaContratoparcela() != null && c.getListaContratoparcela().size() > 0){
			Contratoparcela contratoparcela = this.getParcelaAtual(c.getListaContratoparcela());
			if(contratoparcela != null && contratoparcela.getCdcontratoparcela() != null){
				contratoparcela.setCobrada(Boolean.TRUE);
				contratoparcelaService.updateParcelaCobrada(contratoparcela);
			}
		}
	}

	public void updateZeraAcrescimo(Contrato c) {
		Money acrescimo = c.getValoracrescimo();
		contratoDAO.updateZeraAcrescimo(c);
		c.setValoracrescimo(new Money());
		Empresa empresa = empresaService.loadComContagerencialCentrocusto(c.getEmpresa());
		c.getRateio().setListaRateioitem(rateioitemService.findByRateio(c.getRateio()));
		if(empresa != null && empresa.getContagerencial() != null && empresa.getContagerencial().getCdcontagerencial() != null && 
				empresa.getCentrocusto() != null && empresa.getCentrocusto().getCdcentrocusto() != null){
			for (Rateioitem rateioItem : c.getRateio().getListaRateioitem()){
				if ((rateioItem.getValor() != null && acrescimo != null && rateioItem.getValor().getValue().equals(acrescimo.getValue())) &&
					(rateioItem.getContagerencial() != null && 
							rateioItem.getContagerencial().getCdcontagerencial() != null &&
							rateioItem.getContagerencial().getCdcontagerencial().equals(empresa.getContagerencial().getCdcontagerencial())) &&
					(rateioItem.getCentrocusto() != null && 
							rateioItem.getCentrocusto().getCdcentrocusto() != null &&
							rateioItem.getCentrocusto().getCdcentrocusto().equals(empresa.getCentrocusto().getCdcentrocusto()))){
					rateioitemService.delete(rateioItem);
					c.getRateio().getListaRateioitem().remove(rateioItem);
					rateioService.rateioItensPercentual(c.getRateio(), c.getValorContrato());
					for (Rateioitem r : c.getRateio().getListaRateioitem()){
						rateioitemService.updateProcentagemRateioItem(r);
					}
					break;
				}
			}
		}
	}
	
	public void salvaListaFaturamentoConjunto(List<FaturamentoConjuntoBean> listaFaturamentoConjunto, boolean situacaoEmespera) {
		for (FaturamentoConjuntoBean faturamentoConjuntoBean : listaFaturamentoConjunto) {
			Documento documento = faturamentoConjuntoBean.getDocumento();
			NotaFiscalServico notaFiscalServico = faturamentoConjuntoBean.getNotaFiscalServico();
			Notafiscalproduto notafiscalproduto = faturamentoConjuntoBean.getNotafiscalproduto();
			Contrato contrato = documento.getContrato();
			
			StringBuilder msgHistoricoConta = new StringBuilder();
			StringBuilder msgHistoricoContrato = new StringBuilder();
			
			if(notaFiscalServico != null){
				Grupotributacao grupotributacao = notaFiscalServico.getGrupotributacao();
				if(contrato != null){
					this.calculaTributacaoNotaFiscalServico(contrato, grupotributacao, notaFiscalServico);
				} else if(grupotributacao != null){
					this.calculaTributacaoNotaFiscalServico(null, grupotributacao, notaFiscalServico);
				}
				
				Money retencao = new Money();
				
				if(notaFiscalServico.getIncideiss() != null && 
						notaFiscalServico.getIncideiss() && 
						notaFiscalServico.getIss() != null && 
						notaFiscalServico.getIss() > 0d &&
						notaFiscalServico.getValorIss() != null){
					retencao = retencao.add(notaFiscalServico.getValorIss());
				}
				
				if(notaFiscalServico.getIncideir() != null && 
						notaFiscalServico.getIncideir() && 
						notaFiscalServico.getIr() != null && 
						notaFiscalServico.getIr() > 0d && 
						notaFiscalServico.getValorIr() != null){
					retencao = retencao.add(notaFiscalServico.getValorIr());
				}
				
				if(notaFiscalServico.getIncidepis() != null && 
						notaFiscalServico.getIncidepis() && 
						notaFiscalServico.getPis() != null && 
						notaFiscalServico.getPis() > 0d &&
						notaFiscalServico.getValorPis() != null){
					retencao = retencao.add(notaFiscalServico.getValorPis());
				}
				
				if(notaFiscalServico.getIncidecofins() != null && 
						notaFiscalServico.getIncidecofins() && 
						notaFiscalServico.getCofins() != null && 
						notaFiscalServico.getCofins() > 0d &&
						notaFiscalServico.getValorCofins() != null){
					retencao = retencao.add(notaFiscalServico.getValorCofins());
				}
				
				if(notaFiscalServico.getIncidecsll() != null && 
						notaFiscalServico.getIncidecsll() && 
						notaFiscalServico.getCsll() != null && 
						notaFiscalServico.getCsll() > 0d &&
						notaFiscalServico.getValorCsll() != null){
					retencao = retencao.add(notaFiscalServico.getValorCsll());
				}
				
				if(notaFiscalServico.getIncideinss() != null && 
						notaFiscalServico.getIncideinss() && 
						notaFiscalServico.getInss() != null && 
						notaFiscalServico.getInss() > 0d &&
						notaFiscalServico.getValorInss() != null){
					retencao = retencao.add(notaFiscalServico.getValorInss());
				}
				
				if(notaFiscalServico.getIncideicms() != null && 
						notaFiscalServico.getIncideicms() && 
						notaFiscalServico.getIcms() != null && 
						notaFiscalServico.getIcms() > 0d &&
						notaFiscalServico.getValorIcms() != null){
					retencao = retencao.add(notaFiscalServico.getValorIcms());
				}
				
				if(contrato != null){
					// FEITO ISSO POIS QUANDO PREENCHE O VALOR DO DOCUMENTO PEGA O VALOR J� COM A RETEN��O DO CONTRATO SEM GRUPO DE TRIBUTA��O
					Money totalImpostosSemGrupotributacao = contrato.getValorTotalImpostos();
					if(totalImpostosSemGrupotributacao != null){
						retencao = retencao.subtract(totalImpostosSemGrupotributacao);
					}
				}
				
				if(retencao != null && retencao.getValue().doubleValue() > 0d){
					Money valor = documento.getValor();
					valor = valor.subtract(retencao);
					
					documento.setValor(valor);
					rateioService.atualizaValorRateio(documento.getRateio(), valor);
				}
			}
			if(notaFiscalServico != null && SinedUtil.isListNotEmpty(notaFiscalServico.getListaDuplicata()) && 
					notaFiscalServico.getListaDuplicata().size() == 1){
				notaFiscalServico.setValororiginalfatura(documento.getValor());
				notaFiscalServico.setValorliquidofatura(documento.getValor());
				notaFiscalServico.getListaDuplicata().get(0).setValor(documento.getValor());
			}
			documentoService.saveOrUpdate(documento);
			boolean haveContrato = contrato != null && contrato.getCdcontrato() != null && documento.getCddocumento() != null;
			
			if(notaFiscalServico != null){
				if(situacaoEmespera){
					notaFiscalServico.setNotaStatus(NotaStatus.EM_ESPERA);
					notaFiscalServico.setNumero(null);
				} else {
					boolean haveVotorantim = configuracaonfeService.havePrefixoativo(notaFiscalServico.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
					if(!haveVotorantim){
						Integer proximoNumero = empresaService.carregaProxNumNF(notaFiscalServico.getEmpresa());
						if(proximoNumero == null){
							proximoNumero = 1;
						}
						notaFiscalServico.setNumero(proximoNumero.toString());
						proximoNumero++;
		
						empresaService.updateProximoNumNF(notaFiscalServico.getEmpresa(), proximoNumero);
					}
				}
				
				List<NotaHistorico> listaHistorico = new ArrayList<NotaHistorico>();
				NotaHistorico historico = new NotaHistorico();
				
				if(situacaoEmespera){
					historico.setNotaStatus(NotaStatus.EM_ESPERA);
				} else {
					historico.setNotaStatus(notaFiscalServico.getNotaStatus());
				}
				if(SinedUtil.getUsuarioLogado() != null)
					historico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				String historicoNotaStr = "";
				if(haveContrato){
					historicoNotaStr += "Origem contrato "+ this.motaStringLink(contrato) + ". ";
				}
				historicoNotaStr += "<BR>Vinculado � conta a receber " + SinedUtil.makeLinkContareceber(documento);
				
				historico.setObservacao(historicoNotaStr);
				
				listaHistorico.add(historico);
				notaFiscalServico.setListaNotaHistorico(listaHistorico);
				
				notaFiscalServicoService.saveOrUpdate(notaFiscalServico);
				
				msgHistoricoConta.append("<BR>Vinculada � Nota Fiscal de Servi�o ").append(SinedUtil.makeLinkNota(notaFiscalServico));
				
				if(haveContrato){
					msgHistoricoContrato.append("<BR>Foi gerada a Nota Fiscal de Servi�o ").append(SinedUtil.makeLinkNota(notaFiscalServico));
					
					NotaContrato notaContrato = new NotaContrato();
					notaContrato.setContrato(contrato);
					notaContrato.setNota(notaFiscalServico);
					
					notaContratoService.saveOrUpdate(notaContrato);
				}
				
				NotaDocumento notaDocumento = new NotaDocumento();
				notaDocumento.setNota(notaFiscalServico);
				notaDocumento.setDocumento(documento);
				notaDocumento.setFromwebservice(Boolean.TRUE);
				
				notaDocumentoService.saveOrUpdate(notaDocumento);
			}
			
			if(notafiscalproduto != null){
				notafiscalproduto.setNotaStatus(NotaStatus.EM_ESPERA);
				notafiscalproduto.setNumero(null);
				
				List<NotaHistorico> listaHistorico = new ArrayList<NotaHistorico>();
				NotaHistorico historico = new NotaHistorico();
				
				historico.setNotaStatus(NotaStatus.EM_ESPERA);
				historico.setCdusuarioaltera(((Usuario)NeoWeb.getUser()).getCdpessoa());
				historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				String historicoNotaStr = "";
				if(haveContrato){
					historicoNotaStr += "Origem contrato "+ this.motaStringLink(contrato) + ". ";
				}
				historicoNotaStr += "<BR>Vinculado � conta a receber " + SinedUtil.makeLinkContareceber(documento);
				
				historico.setObservacao(historicoNotaStr);
				
				listaHistorico.add(historico);
				
				notafiscalproduto.setListaNotaHistorico(listaHistorico);
				
				notafiscalprodutoService.setTotalNota(notafiscalproduto);
				notafiscalprodutoService.saveOrUpdate(notafiscalproduto);
				
				msgHistoricoConta.append("<BR>Vinculada � Nota Fiscal de Produto ").append(SinedUtil.makeLinkNota(notafiscalproduto));

				if(haveContrato){
					msgHistoricoContrato.append("<BR>Foi gerada a Nota Fiscal de Produto ").append(SinedUtil.makeLinkNota(notafiscalproduto));
					NotaContrato notaContrato = new NotaContrato();
					notaContrato.setContrato(contrato);
					notaContrato.setNota(notafiscalproduto);
					
					notaContratoService.saveOrUpdate(notaContrato);
				}
				
				NotaDocumento notaDocumento = new NotaDocumento();
				notaDocumento.setNota(notafiscalproduto);
				notaDocumento.setDocumento(documento);
				notaDocumento.setFromwebservice(Boolean.TRUE);
				
				notaDocumentoService.saveOrUpdate(notaDocumento);
			}
			
			if(haveContrato){
				this.salvaDocumentoorigemContrato(documento);
				contrato.setDocumento(documento);
				
				Documentohistorico documentohistorico = this.criaDocumentoHistoricoFaturamentoContrato(documento, contrato.getCdcontrato());
				
				if(documentohistorico.getObservacao() != null) documentohistorico.setObservacao(documentohistorico.getObservacao() + msgHistoricoConta.toString());
				else documentohistorico.setObservacao(msgHistoricoConta.toString());
				
				documentohistoricoService.saveOrUpdate(documentohistorico);
				
				Contratohistorico historico = new Contratohistorico();
				
				historico.setAcao(Contratoacao.COSOLIDADO);
				historico.setContrato(documento.getContrato());
				historico.setUsuario((Usuario)NeoWeb.getUser());
				historico.setDthistorico(new Timestamp(System.currentTimeMillis()));
				historico.setObservacao("Gerada a conta <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>. " + msgHistoricoContrato.toString());
				
				historico = criaHistoricoOSContratoAndUpdateRequisicaoestado(documento.getContrato(), historico);
				
				contratohistoricoService.saveOrUpdate(historico);
				
				contratoService.updateReajuste(contrato, Boolean.FALSE, null);
			}
			
		}
	}
	
	/**
	 * Salva a lista de contas a receber no faturamento.
	 *
	 * @param listaContaReceber
	 * @author Rodrigo Freitas
	 */
	private void salvaListaContaReceber(List<Documento> listaContaReceber) {
		Contratohistorico historico;
		Documentohistorico documentohistorico;
		for (Documento documento : listaContaReceber) {
			List<Contrato> listaContrato = documento.getListaContrato();

			if(listaContrato == null) listaContrato = new ArrayList<Contrato>();
			
			Contrato contrato = documento.getContrato();
			if(contrato != null && !listaContrato.contains(contrato)){
				listaContrato.add(contrato);
			}
			
			documentoService.saveOrUpdate(documento);
			
			if(listaContrato != null && listaContrato.size() > 0){
				List<Integer> listaId = new ArrayList<Integer>();
				for (Contrato c : listaContrato) {
					documento.setContrato(c);
					this.salvaDocumentoorigemContrato(documento);
					
					historico = new Contratohistorico();
					historico.setAcao(Contratoacao.COSOLIDADO);
					historico.setContrato(documento.getContrato());
					historico.setUsuario((Usuario)NeoWeb.getUser());
					historico.setDthistorico(new Timestamp(System.currentTimeMillis()));
					historico.setObservacao("Gerada a conta <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>.");
					
					historico = criaHistoricoOSContratoAndUpdateRequisicaoestado(c, historico);
					contratohistoricoService.saveOrUpdate(historico);
					
					listaId.add(c.getCdcontrato());
					
					faturamentohistoricoService.saveHistoricoFaturamento(c, documento);
					
					contratoService.updateReajuste(c, Boolean.FALSE, null);
				}
				
				documentohistorico = this.criaDocumentoHistoricoFaturamentoContrato(documento, listaId);
				documentohistoricoService.saveOrUpdate(documentohistorico);
			}
		}
	}
	
	/**
	 * Salva a lista de nota de produto para o faturamento.
	 *
	 * @param listaNotaServico
	 * @author Rodrigo Freitas
	 * @param faturamentocontrato 
	 */
	private void salvaListaNotaProduto(List<Notafiscalproduto> listaNotaProduto) {
		
		List<NotaHistorico> listaHistorico;
		NotaHistorico historico;
		List<Contrato> listaContrato;
		
//		Integer proximoNumero;
		for (Notafiscalproduto nfp : listaNotaProduto) {
//			proximoNumero = empresaService.carregaProxNumNFProduto(nfp.getEmpresa());
//			if(proximoNumero == null){
//				proximoNumero = 1;
//			}
//			nfp.setNumero(proximoNumero.toString());
//			proximoNumero++;
//
//			empresaService.updateProximoNumNFProduto(nfp.getEmpresa(), proximoNumero);
			
			listaHistorico = new ArrayList<NotaHistorico>();
			historico = new NotaHistorico();
			
			historico.setNotaStatus(NotaStatus.EMITIDA);
			historico.setCdusuarioaltera(((Usuario)NeoWeb.getUser()).getCdpessoa());
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao("Origem contrato "+ this.motaStringLink(nfp.getListaContrato())+".");
			
			listaHistorico.add(historico);
			
			nfp.setListaNotaHistorico(listaHistorico);
			
			listaContrato = new ArrayList<Contrato>(nfp.getListaContrato());
			
			notafiscalprodutoService.setTotalNota(nfp);
			notafiscalprodutoService.saveOrUpdate(nfp);
			notafiscalprodutoService.updateNumeroNFProduto(nfp);
			
			this.updateContratoHistoricoFaturamento(listaContrato, nfp, true);
		}
	}
	
	/**
	 * Salva a lista de nota de servi�o para o faturamento.
	 *
	 * @param listaNotaServico
	 * @author Rodrigo Freitas
	 * @param faturamentocontrato 
	 */
	private void salvaListaNotaServico(List<NotaFiscalServico> listaNotaServico) {
		Integer proximoNumero = null;
		List<NotaHistorico> listaHistorico;
		NotaHistorico historico;
		List<Contrato> listaContrato;
		
		// ORDENA NOTAS DESC POR VALOR
		Collections.sort(listaNotaServico,new Comparator<NotaFiscalServico>(){
			public int compare(NotaFiscalServico o1, NotaFiscalServico o2) {
				return o2.getValorBruto().compareTo(o1.getValorBruto());
			}
		});
		
		for (NotaFiscalServico nfs : listaNotaServico) {
			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
			if(!haveVotorantim){
				proximoNumero = empresaService.carregaProxNumNF(nfs.getEmpresa());
				if(proximoNumero == null){
					proximoNumero = 1;
				}
				nfs.setNumero(proximoNumero.toString());
				proximoNumero++;
	
				empresaService.updateProximoNumNF(nfs.getEmpresa(), proximoNumero);
			}
			
			listaHistorico = new ArrayList<NotaHistorico>();
			historico = new NotaHistorico();
			
			historico.setNotaStatus(NotaStatus.EMITIDA);
			if(NeoWeb.getUser() != null){
				historico.setCdusuarioaltera(((Usuario)NeoWeb.getUser()).getCdpessoa());
			}
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao("Origem contrato "+ this.motaStringLink(nfs.getListaContrato())+".");
			
			listaHistorico.add(historico);
			nfs.setListaNotaHistorico(listaHistorico);
			
			//Calcula a tributa��o com base no grupo de tributa��o do contrato, busca do primeiro contrato
			//pois � feita a valida��o para que todos os contratos possuem o mesmo grupo
			if (nfs.getListaContrato() != null && nfs.getListaContrato().size() > 0) {
				Contrato contrato = nfs.getListaContrato().get(0);
				calculaTributacaoNotaFiscalServico(contrato, contrato.getGrupotributacao(), nfs);
				if(Hibernate.isInitialized(nfs.getListaDuplicata()) && nfs.getListaDuplicata() != null && nfs.getListaDuplicata().size() == 1){
					Notafiscalservicoduplicata duplicata = nfs.getListaDuplicata().get(0);
					nfs.setValororiginalfatura(nfs.getValorNota());
					nfs.setValorliquidofatura(nfs.getValororiginalfatura());
					duplicata.setValor(nfs.getValororiginalfatura());
				}
			}
			
			listaContrato = new ArrayList<Contrato>(nfs.getListaContrato());
			notaFiscalServicoService.saveOrUpdate(nfs);
			
			this.updateContratoHistoricoFaturamento(listaContrato, nfs, false);
		}
	}
	
	/**
	 * Atualiza as liga��es da tabela NotaContrato e o hist�rico do contrato.
	 *
	 * @param listaContrato
	 * @param nf
	 * @author Rodrigo Freitas
	 * @param faturamentocontrato 
	 */
	private void updateContratoHistoricoFaturamento(List<Contrato> listaContrato, Nota nf, boolean produto) {
		Contratohistorico contratohistorico;
		NotaContrato notaContrato;
		
		for (Contrato contrato : listaContrato) {
			contratohistorico = new Contratohistorico();
			contratohistorico.setAcao(Contratoacao.COSOLIDADO);
			contratohistorico.setContrato(contrato);
			contratohistorico.setUsuario((Usuario)NeoWeb.getUser());
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			
			String obs = produto ?  
				"Foi gerada a Nota Fiscal de Produto <a href=\"javascript:visualizarNotaProduto("+nf.getCdNota()+")\">"+(org.apache.commons.lang.StringUtils.isNotBlank(nf.getNumero()) ? nf.getNumero() : nf.getCdNota()) +"</a>" :
				"Foi gerada a Nota Fiscal de Servi�o <a href=\"javascript:visualizarNota("+nf.getCdNota()+")\">"+(org.apache.commons.lang.StringUtils.isNotBlank(nf.getNumero()) ? nf.getNumero() : nf.getCdNota()) +"</a>";
			
			contratohistorico.setObservacao(obs);
			
			contratohistorico = criaHistoricoOSContratoAndUpdateRequisicaoestado(contrato, contratohistorico);
			
			contratohistoricoService.saveOrUpdate(contratohistorico);
		
			notaContrato = new NotaContrato();
			notaContrato.setContrato(contrato);
			notaContrato.setFaturamentocontrato(contrato.getFaturamentocontrato());
			notaContrato.setNota(nf);
			
			notaContratoService.saveOrUpdate(notaContrato);
		}
	}
	
	private List<Notafiscalproduto> agrupamentoNotaProduto(List<Notafiscalproduto> listaNotaProduto) {
		List<Notafiscalproduto> lista = new ArrayList<Notafiscalproduto>();
		
		for (Notafiscalproduto nf : listaNotaProduto) {
			if(!this.verificaSeNotaContemClienteFreq(nf,lista)){
				lista.add(nf);
			}
		}
		
		return lista;
	}
	
	/**
	 * Realiza o agrupamento das Notas Fiscais de Servi�o.
	 *  
	 * @see br.com.linkcom.sined.geral.service.ContratoService#verificaSeNotaContemClienteFreq
	 *
	 * @param listaNotaServico
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<NotaFiscalServico> agrupamentoNotaServico(List<NotaFiscalServico> listaNotaServico) {
		List<NotaFiscalServico> lista = new ArrayList<NotaFiscalServico>();
		
		for (NotaFiscalServico nf : listaNotaServico) {
			if(!this.verificaSeNotaContemClienteFreq(nf,lista)){
				lista.add(nf);
			}
		}
		
		return lista;
	}
	
	private List<Documento> agrupamentoContaReceber(List<Documento> listaContaReceber) {
		List<Documento> lista = new ArrayList<Documento>();
		
		for (Documento doc : listaContaReceber) {
			if(!this.verificaSeDocumentoContemClienteFreq(doc, lista)){
				lista.add(doc);
			}
		}
		
		return lista;
	}
	
	/**
	 * Gera notas separadas de PRODUTO E SERVI�O, pois no contrato possui cadastrado os materiais no contrato.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#geraNotaServicoFaturamento
	 * @see br.com.linkcom.sined.geral.service.ContratoService#geraNotaProdutoFaturamento
	 *
	 * @param c
	 * @param listaNotaServico
	 * @param listaNotaProduto 
	 * @author Rodrigo Freitas
	 */
	private void geraNotasSeparadasFaturamento(Contrato c, List<NotaFiscalServico> listaNotaServico, List<Notafiscalproduto> listaNotaProduto) {
		List<Contratomaterial> listaServico = new ArrayList<Contratomaterial>();
		List<Contratomaterial> listaProduto = new ArrayList<Contratomaterial>();

		Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(c.getEmpresa());
		if(c.getContratotipo() != null && c.getContratotipo().getLocacao() != null && c.getContratotipo().getLocacao() && 
				c.getContratotipo().getMaterial()!=null && c.getContratotipo().getMaterial().getCdmaterial()!=null){
			Contratomaterial ct = new Contratomaterial();
			ct.setServico(materialService.carregaMaterial(c.getContratotipo().getMaterial()));
			ct.setValorunitario(c.getValor().getValue().doubleValue());
			ct.setDtinicio(new Date(System.currentTimeMillis()));
			ct.setDtfim(new Date(System.currentTimeMillis()));
			ct.setQtde(1.0);
			ct.setConsiderarDescricaoTipoLocacaoComMaterial(true);
			ct.setValordesconto(new Money());
			String observacao = " - ";
			for(Contratomaterial it : c.getListaContratomaterial()){
				if(org.apache.commons.lang.StringUtils.isNotBlank(it.getObservacao())){
					observacao += it.getObservacao() + " - ";
				}
			}
			ct.setObservacao(observacao);
			
			if(!isMunicipioBrasilia){
				listaServico.add(ct);
			}else {
				listaProduto.add(ct);
			}
		}else{		
			for (Contratomaterial item : c.getListaContratomaterial()) {
				if(SinedDateUtils.afterOrEqualsIgnoreHour(c.getDtproximovencimento(), item.getDtinicio()) && 
						(item.getDtfim() == null || SinedDateUtils.beforeOrEqualIgnoreHour(c.getDtproximovencimento(), item.getDtfim()))){
					if(item.getServico() != null && (item.getServico().getServico() && (
							(item.getServico().getTributacaoestadual() == null && !isMunicipioBrasilia) || 
							(!item.getServico().getTributacaoestadual() && !isMunicipioBrasilia))) || 
							(item.getServico().getTributacaomunicipal() != null && item.getServico().getTributacaomunicipal())){ 
						listaServico.add(item);
					}else{
						listaProduto.add(item);
					}
				}
			}		
		}
		
		if(listaServico != null && listaServico.size() > 0){
			this.geraNotaServicoFaturamento(c, listaNotaServico, listaServico);
		}
		
		if(listaProduto != null && listaProduto.size() > 0){
			this.geraNotaProdutoFaturamento(c, listaNotaProduto, listaProduto);
		}
	}
	
	/**
	 * Gera uma nota fiscal de produto do faturamento do contrato.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#valorDescontoAba
	 *
	 * @param contrato
	 * @param listaNotaProduto
	 * @param listaProduto
	 * @author Rodrigo Freitas
	 */
	private void geraNotaProdutoFaturamento(Contrato contrato, List<Notafiscalproduto> listaNotaProduto, List<Contratomaterial> listaProduto) {
		Notafiscalproduto nota = new Notafiscalproduto();
		
		if(contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null){
			for (Rateioitem ri : contrato.getRateio().getListaRateioitem()) {
				if(ri.getProjeto() != null){
					nota.setProjeto(ri.getProjeto());
				}
			}
		}
		
		nota.setCliente(contrato.getCliente());
//		nota.setNomefantasiaCliente(contrato.getNomefantasia());
		nota.setEnderecoCliente(contrato.getEnderecoPrioritario());
		
		if(nota.getEnderecoCliente() == null){
			throw new SinedException("Nenhum endere�o cadastrado para o cliente " + contrato.getCliente().getNome() + ".");
		}
		
		Money totalDesconto = this.valorDescontoAba(contrato, Tipodesconto.NF_PRODUTO);
		boolean possuiDescontoGeral = totalDesconto != null && (totalDesconto.getValue().compareTo(BigDecimal.ZERO) > 0);
		
		Money valorTotalProdutos = new Money(0.0);
		Money valorTotalDesconto = new Money(0.0);
		Money valorAcrescimo = contrato.getValoracrescimo();
		if(valorAcrescimo == null){
			valorAcrescimo = new Money(0.0);
		}
		
		Notafiscalprodutoitem item;
		List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
		Money totalAcrescimo = new Money();
		Money valorTotalProdutosAbatendoDesconto = new Money(0.0);
		for (Contratomaterial cm : listaProduto) {
			valorTotalProdutosAbatendoDesconto = valorTotalProdutosAbatendoDesconto.add(cm.getValorbrutoForCriacaoNota());
			if(cm.getValordesconto() != null){
				valorTotalProdutosAbatendoDesconto = valorTotalProdutosAbatendoDesconto.subtract(cm.getValordesconto());
			}
		}
		Notafiscalprodutoitem itemComMaiorValor = null;
		Money valorAbatendoDescontoItem = new Money(0.0);
		Money somaDescontoProporcional = new Money(0.0);
		for (Contratomaterial cm : listaProduto) {
			item = new Notafiscalprodutoitem();
			
			item.setMaterial(cm.getServico());
			item.setUnidademedida(cm.getServico().getUnidademedida());
			item.setNcmcompleto(cm.getServico().getNcmcompleto());
			item.setNve(cm.getServico().getNve());
			item.setExtipi(cm.getServico().getExtipi());
			item.setNcmcapitulo(cm.getServico().getNcmcapitulo());
			item.setIncluirvalorprodutos(true);
			item.setMaterialclasse(materialService.getMaterialClasse(cm.getServico(), true));
			if(cm.getServico().getTributacaoestadual() != null && cm.getServico().getTributacaoestadual() && 
					cm.getServico().getServico() != null && cm.getServico().getServico()){
				item.setTributadoicms(false);
			}
			if(cm.getServico().getCodigobarras() != null && !"".equals(cm.getServico().getCodigobarras()) && SinedUtil.validaCEAN(cm.getServico().getCodigobarras()))
				item.setGtin(cm.getServico().getCodigobarras());
			
			if(valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
				Money acres = new Money(SinedUtil.round(valorAcrescimo.getValue().doubleValue() / listaProduto.size() / (item.getQtde() != null ? item.getQtde() : 1.0), 2));
				totalAcrescimo = totalAcrescimo.add(acres);
				item.setOutrasdespesas(acres);
			}
			
			if(cm.getPeriodocobranca() != null){
				cm.setQtde(cm.getQtde() * cm.getPeriodocobranca());
			}
			
			if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
				double tamParcela = new Double(contrato.getListaContratoparcela().size()).doubleValue();
				item.setQtde(cm.getQtde().doubleValue()/tamParcela);
			} else {
				item.setQtde(cm.getQtde().doubleValue());
			}
			
			item.setValorunitario(cm.getValorfechadoOrValorunitario() != null ? cm.getValorfechadoOrValorunitario() : null);
			item.setValorbruto(cm.getValorbrutoForCriacaoNota());
			item.setValordesconto(cm.getValordesconto());
			
			valorAbatendoDescontoItem = item.getValorSemDesconto();
			if(possuiDescontoGeral){
				Money percentualDescontoItem = valorAbatendoDescontoItem.divide(valorTotalProdutosAbatendoDesconto).multiply(new Money(100));
				Money valorDescontoProporcional = totalDesconto.divide(new Money(100)).multiply(percentualDescontoItem);
				somaDescontoProporcional = somaDescontoProporcional.add(valorDescontoProporcional);
				item.setValordesconto(cm.getValordesconto() != null ? valorDescontoProporcional.add(cm.getValordesconto()) : valorDescontoProporcional);
			}
			
			if(contrato.getDescricao() != null && !contrato.getDescricao().equals("") && 
					cm.getConsiderarDescricaoTipoLocacaoComMaterial() != null && 
					cm.getConsiderarDescricaoTipoLocacaoComMaterial()){
				if(item.getInfoadicional() == null || !item.getInfoadicional().contains(contrato.getDescricao())){
					item.setInfoadicional(contrato.getDescricao() + (item.getInfoadicional() != null ? (" " + item.getInfoadicional()) : ""));
				}
			}
			
			valorTotalDesconto = valorTotalDesconto.add(cm.getValordesconto());
			valorTotalProdutos = valorTotalProdutos.add(item.getValorbruto());
			
			listaItens.add(item);
		}
		
		if(possuiDescontoGeral){
			if(totalDesconto.compareTo(somaDescontoProporcional) > 0){
				Money diferencaRateioDesconto = totalDesconto.subtract(somaDescontoProporcional);
				for(Notafiscalprodutoitem itemNota: listaItens){
					if(itemComMaiorValor == null){
						itemComMaiorValor = itemNota;
					}else{
						if(itemNota.getValorSemDesconto().compareTo(itemComMaiorValor.getValorSemDesconto()) > 0){
							itemComMaiorValor = itemNota;
						}
					}
				}
				itemComMaiorValor.setValordesconto(itemComMaiorValor.getValordesconto().add(diferencaRateioDesconto));
			}
		}
		
		if(listaItens.size() > 0 && valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
			Double diferenca = valorAcrescimo.getValue().doubleValue() - totalAcrescimo.getValue().doubleValue();
			if(diferenca > 0){
				Notafiscalprodutoitem ultimoItem = listaItens.get(listaItens.size()-1);
				if(ultimoItem.getOutrasdespesas() == null){
					ultimoItem.setOutrasdespesas(new Money(diferenca));
				}else {
					ultimoItem.setOutrasdespesas(ultimoItem.getOutrasdespesas().add(new Money(diferenca)));
				}
			}
		}
		
		nota.setListaItens(listaItens);

		nota.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		nota.setValordesconto(valorTotalDesconto);
		nota.setValorprodutos(valorTotalProdutos);
		nota.setOutrasdespesas(valorAcrescimo);
		nota.setValor(valorTotalProdutos.subtract(valorTotalDesconto).add(valorAcrescimo));
		nota.setDadosAdicionais(contrato.getAnotacaocorpo());
		nota.setDtEmissao(SinedDateUtils.currentDate());
		nota.setFinalidadenfe(Finalidadenfe.NORMAL);
		nota.setTipooperacaonota(Tipooperacaonota.SAIDA);
		nota.setNotaStatus(NotaStatus.EMITIDA);
		nota.setNotaTipo(NotaTipo.NOTA_FISCAL_PRODUTO);
		nota.setGrupotributacao(contrato.getGrupotributacao());
		nota.setFaturamentolote(contrato.getFaturamentolote());
		nota.setConta(contrato.getConta());
		nota.setInfoadicionalcontrib(contrato.getAnotacao());
		if(org.apache.commons.lang.StringUtils.isNotEmpty(contrato.getAnotacaocorpo())){
			if(org.apache.commons.lang.StringUtils.isNotEmpty(nota.getInfoadicionalcontrib()) && 
					!nota.getInfoadicionalcontrib().contains(contrato.getAnotacaocorpo())){
				nota.setInfoadicionalcontrib(nota.getInfoadicionalcontrib()+ ". " + contrato.getAnotacaocorpo());
			}else if(org.apache.commons.lang.StringUtils.isEmpty(nota.getInfoadicionalcontrib())){
				nota.setInfoadicionalcontrib(contrato.getAnotacaocorpo());
			}
		}
		
		if(contrato.getContratotipo() != null && 
				contrato.getContratotipo().getLocacao() != null && 
				contrato.getContratotipo().getLocacao()){
			StringBuilder dadosAdicionais = new StringBuilder();
			
			if(nota.getInfoadicionalcontrib() != null && !nota.getInfoadicionalcontrib().trim().equals("")){
				dadosAdicionais.append(nota.getInfoadicionalcontrib()).append(", ");
			}
			
			dadosAdicionais
				.append(contrato.getDescricao())
				.append(" - CONTRATO ")
				.append(contrato.getIdentificador() != null && !contrato.getIdentificador().equals("") ? contrato.getIdentificador() : contrato.getCdcontrato())
				.append(" (R$ ")
				.append(nota.getValorNota())
				.append(") - Per�odo: ")
				.append(SinedDateUtils.toString(contrato.getDtiniciolocacao()))
				.append(" at� ")
				.append(SinedDateUtils.toString(contrato.getDtfimlocacao()));
			
			nota.setInfoadicionalcontrib(dadosAdicionais.toString());
		}
		
		List<Contrato> listaOrigem = new ArrayList<Contrato>();
		listaOrigem.add(contrato);
		nota.setListaContrato(listaOrigem);

		if(contrato.getEmpresa() != null && contrato.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.loadForInfNota(contrato.getEmpresa());
			nota.setSerienfe(empresa.getSerienfe());
			nota.setEmpresa(contrato.getEmpresa());
			if(contrato.getEmpresa().getListaEndereco() != null && contrato.getEmpresa().getListaEndereco().size() > 0){
				nota.setMunicipiogerador(contrato.getEmpresa().getEndereco().getMunicipio());
			}
		}
		notafiscalprodutoService.preencheValoresPadroesEmpresaISS(contrato, nota);
		nota.setOperacaonfe(ImpostoUtil.getOperacaonfe(nota.getCliente(), nota.getNaturezaoperacao()));
		
//		if(!Formafaturamento.NOTA_APOS_RECEBIMENTO.equals(contrato.getFormafaturamento())){
//			preencherDadosCobranca(contrato, nota);
//		}
		
		listaNotaProduto.add(nota);
	}
	
//	private void preencherDadosCobranca(Contrato contrato, Notafiscalproduto nota) {
//		nota.setCadastrarcobranca(Boolean.TRUE);
//		nota.setContaboleto(contrato.getConta());
//		nota.setContacarteira(contrato.getContacarteira());
//		nota.setDocumentotipo(Documentotipo.BOLETO);
//		nota.setNumerofatura(contrato.getIdentificadorOrCdcontrato());
//		nota.setValororiginalfatura(nota.getValorNota());
//		nota.setValorliquidofatura(nota.getValororiginalfatura());
//		
//		List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
//		Notafiscalprodutoduplicata duplicata = new Notafiscalprodutoduplicata();
//		duplicata.setDocumentotipo(nota.getDocumentotipo());
//		duplicata.setDtvencimento(this.getDataVencimento(contrato, contrato.getDtproximovencimento()));
//		duplicata.setValor(nota.getValororiginalfatura());
//		listaDuplicata.add(duplicata);
//		nota.setListaDuplicata(listaDuplicata);
//	}
	
	/**
	 * Gera uma nota fiscal de servi�o do faturamento do contrato.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#valorDescontoAba
	 * @see br.com.linkcom.sined.geral.service.ContratoService#getValorFaixaImposto
	 *
	 * @param contrato
	 * @param listaNotaServico
	 * @param listaServico
	 * @author Rodrigo Freitas
	 */
	private void geraNotaServicoFaturamento(Contrato contrato, List<NotaFiscalServico> listaNotaServico, List<Contratomaterial> listaServico) {
		NotaFiscalServico nota = new NotaFiscalServico();
		
		nota.setCliente(contrato.getCliente());
		nota.setNomefantasiaCliente(contrato.getNomefantasia());
		nota.setInscricaomunicipal(contrato.getCliente().getInscricaomunicipal());
		
		notaFiscalServicoService.preencheValoresPadroesEmpresa(contrato.getEmpresa(), nota);
		
		if(contrato.getGrupotributacao() != null){
			if(contrato.getGrupotributacao().getCodigocnae() != null) nota.setCodigocnaeBean(contrato.getGrupotributacao().getCodigocnae());
			if(contrato.getGrupotributacao().getCodigotributacao() != null) nota.setCodigotributacao(contrato.getGrupotributacao().getCodigotributacao());
			if(contrato.getGrupotributacao().getItemlistaservico() != null) nota.setItemlistaservicoBean(contrato.getGrupotributacao().getItemlistaservico());
		}
		
		
		if(contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null){
			for (Rateioitem ri : contrato.getRateio().getListaRateioitem()) {
				if(ri.getProjeto() != null){
					nota.setProjeto(ri.getProjeto());
				}
			}
		}
		nota.setEnderecoCliente(contrato.getEnderecoPrioritario());
		
		if(nota.getEnderecoCliente() == null){
			if(contrato.getEndereco() != null){
				nota.setEnderecoCliente(contrato.getEndereco());
			} else {
				if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
					List<Enderecotipo> listaEnderecotipoPrioridade = new ArrayList<Enderecotipo>();
					listaEnderecotipoPrioridade.add(Enderecotipo.FATURAMENTO);
					listaEnderecotipoPrioridade.add(Enderecotipo.UNICO);
					nota.setEnderecoCliente(contrato.getCliente().getEnderecoWithPrioridade(Enderecotipo.FATURAMENTO, Enderecotipo.UNICO));
				}
			}
		}
		
		if(nota.getEnderecoCliente() == null){
			throw new SinedException("Nenhum endere�o cadastrado para o cliente " + contrato.getCliente().getNome() + ".");
		}
		

		boolean tributacaomunicipiocliente_empresa = contrato.getEmpresa() != null && contrato.getEmpresa().getTributacaomunicipiocliente() != null && contrato.getEmpresa().getTributacaomunicipiocliente();
		boolean tributacaomunicipiocliente_grupotributacao = contrato.getGrupotributacao() != null && contrato.getGrupotributacao().getTributacaomunicipiocliente() != null && contrato.getGrupotributacao().getTributacaomunicipiocliente();
		
		if(tributacaomunicipiocliente_empresa || tributacaomunicipiocliente_grupotributacao){
			if(nota.getEnderecoCliente() != null && 
					nota.getEnderecoCliente().getMunicipio() != null && 
					nota.getMunicipioissqn() != null &&
					!nota.getMunicipioissqn().equals(nota.getEnderecoCliente().getMunicipio())){
				nota.setMunicipioissqn(nota.getEnderecoCliente().getMunicipio());
				nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO);
			}
		}
		
		List<Contratoparcela> listaContratoparcela = contratoparcelaService .findByContrato(contrato);
		int numeroParcela = 0;
		Contratoparcela contratoparcelaAtual = null;
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			contratoparcelaAtual = contratoService.getParcelaAtual(listaContratoparcela);
			numeroParcela = listaContratoparcela.indexOf(contratoparcelaAtual);
			numeroParcela++;
		}
		
		Map<NotaFiscalTipoEnum, Money> mapDescontos = contratodescontoService.getDescontosContrato(contrato.getDtproximovencimento(), contrato.getListaContratodesconto());
		
		Money valorBruto = new Money();
		
		Set<Entry<NotaFiscalTipoEnum, Money>> entrySet = mapDescontos.entrySet();
		for (Entry<NotaFiscalTipoEnum, Money> entry : entrySet) {
			NotaFiscalTipoEnum tiponota = entry.getKey();
			Money valor = entry.getValue();
			
			if (tiponota.equals(NotaFiscalTipoEnum.ABATIMENTO)){
				valorBruto = valorBruto.add(valor);
			} else if (tiponota.equals(NotaFiscalTipoEnum.DEDUCAO)){
				if (nota.getDeducao() != null){
					nota.setDeducao(nota.getDeducao().add(valor));
				} else { 
					nota.setDeducao(valor);
				}
			} else if (tiponota.equals(NotaFiscalTipoEnum.DESCONTO_CONDICIONADO)){
				if (nota.getDescontocondicionado() != null){
					nota.setDescontocondicionado(nota.getDescontocondicionado().add(valor));
				} else {
					nota.setDescontocondicionado(valor);
				}
			} else if (tiponota.equals(NotaFiscalTipoEnum.DESCONTO_INCONDICIONADO)){
				if (nota.getDescontoincondicionado() != null){
					nota.setDescontoincondicionado(nota.getDescontoincondicionado().add(valor));
				} else {
					nota.setDescontoincondicionado(valor);
				}
			} else if(tiponota.equals(NotaFiscalTipoEnum.OUTRAS_RETENCOES)){
				if (nota.getOutrasretencoes() != null){
					nota.setOutrasretencoes(nota.getOutrasretencoes().add(valor));
				} else {
					nota.setOutrasretencoes(valor);
				}
			}
		}
		
		String paramAnotacaocorpoContrato = parametrogeralService.getValorPorNome(Parametrogeral.CONTRATO_ANOTACAOCORPO_CONCATENAR);
		boolean concatenarAnotacaocorpo = paramAnotacaocorpoContrato != null && paramAnotacaocorpoContrato.toUpperCase().trim().equals("TRUE");
		
		NotaFiscalServicoItem item;
		List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
		
//		String identificadorContrato = null;
//		if(contrato.getIdentificador() != null && !contrato.getIdentificador().equals("")){
//			identificadorContrato = " Contrato: " + contrato.getIdentificador();
//		}
		
		if(listaServico != null){
			boolean forcarDescricaoContrato = "TRUE".equals(parametrogeralService.getValorPorNome("FaturamentoContratoForcarDescricao"));
			Money valorTotalItens = new Money();
			
			for (Contratomaterial cm : listaServico) {
				// CASO O VALOR UNIT�RIO DO SERVI�O/PRODUTO SEJA ZERO ELE N�O INCLUI O ITEM NA NOTA
				if(cm.getValorfechadoOrValorunitario() == null || cm.getValorfechadoOrValorunitario().equals(0d)){
					continue;
				}
				
				item = new NotaFiscalServicoItem();
				
				item.setMaterial(cm.getServico());
				
				if(cm.getConsiderarDescricaoTipoLocacaoComMaterial() != null && cm.getConsiderarDescricaoTipoLocacaoComMaterial()){
					item.setDescricao(contrato.getDescricao());
					if (numeroParcela > 0)
						item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
				}else {
					if(!concatenarAnotacaocorpo && contrato.getAnotacaocorpo() != null && !contrato.getAnotacaocorpo().trim().equals("")){
						item.setDescricao(contrato.getAnotacaocorpo());
						if (numeroParcela > 0)
							item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
					} else if(forcarDescricaoContrato){
						item.setDescricao(contrato.getDescricao());
						if (numeroParcela > 0)
							item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
					} else {
						item.setDescricao(cm.getServico().getNome());
						if (numeroParcela > 0)
							item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
					}
					
					if (contrato.getMes() != null && contrato.getAno() != null){
						item.setDescricao(item.getDescricao() + " " + contrato.getMes().getNome() + "/" + contrato.getAno());
					}
				}
				
				if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0 && contratoparcelaAtual != null){
					Double valorParcela = contratoparcelaAtual.getValornfservico().getValue().doubleValue();
					Double valorContratoFinal = contrato.getValorQtde().getValue().doubleValue();
					
					Double percentParcela = (valorParcela * 100d)/valorContratoFinal;
					
					item.setQtde(cm.getQtde().doubleValue() * (percentParcela/100d));
					item.setPrecoUnitario(cm.getValorfechadoOrValorunitario());
					item.setDesconto(new Money(cm.getValordesconto().getValue().doubleValue()* (percentParcela/100d)));
				} else {
					item.setQtde(cm.getQtde().doubleValue());
					item.setPrecoUnitario(cm.getValorfechadoOrValorunitario());
					item.setDesconto(cm.getValordesconto());
				}
				
//				if(identificadorContrato != null){
//					item.setDescricao((item.getDescricao() != null ? item.getDescricao() : "") + identificadorContrato);
//				}

				if(org.apache.commons.lang.StringUtils.isNotEmpty(cm.getObservacao())){
					item.setDescricao(item.getDescricao() + cm.getObservacao());
				}else {
					item.setDescricao(item.getDescricao());
				}
				listaItens.add(item);
				
				valorTotalItens = valorTotalItens.add(item.getTotalParcial());
			}
			
			if(valorBruto != null && valorBruto.getValue().doubleValue() > 0 && valorTotalItens != null && 
					valorTotalItens.getValue().doubleValue() > 0){
				Money maiorTotalItem = new Money();
				Money totalValorBruto = new Money();
				NotaFiscalServicoItem itemMaiorTotal = null;
				for (NotaFiscalServicoItem notaFiscalServicoItem : listaItens) {
					if(notaFiscalServicoItem.getTotalParcial() != null && notaFiscalServicoItem.getTotalParcial().getValue().doubleValue() > 0){
						Money percentualDescontoProporcionalItem = notaFiscalServicoItem.getTotalParcial().multiply(new Money(100d)).divide(valorTotalItens);
						if(percentualDescontoProporcionalItem != null && percentualDescontoProporcionalItem.getValue().doubleValue() > 0){
							Money valorDescontoItem = valorBruto.multiply(percentualDescontoProporcionalItem).divide(new Money(100d));
							if(notaFiscalServicoItem.getDesconto() != null){
								notaFiscalServicoItem.setDesconto(notaFiscalServicoItem.getDesconto().add(valorDescontoItem));
							}else {
								notaFiscalServicoItem.setDesconto(valorDescontoItem);
							}
							totalValorBruto = totalValorBruto.add(valorDescontoItem);
							
							if(notaFiscalServicoItem.getTotalParcial() != null && 
									notaFiscalServicoItem.getTotalParcial().getValue().doubleValue() > maiorTotalItem.getValue().doubleValue()){
								itemMaiorTotal = notaFiscalServicoItem;
							}
						}
					}
				}
				Money diferenca = valorBruto.subtract(totalValorBruto);
				if(itemMaiorTotal != null && diferenca.getValue().doubleValue() > 0){
					if(itemMaiorTotal.getDesconto() != null){
						itemMaiorTotal.setDesconto(itemMaiorTotal.getDesconto().add(diferenca));
					}else {
						itemMaiorTotal.setDesconto(diferenca);
					}
				}
			}
			
			Money valorAcrescimo = contrato.getValoracrescimo();
			if(valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
				if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
					double tamParcela = new Double(contrato.getListaContratoparcela().size()).doubleValue();
					valorAcrescimo = valorAcrescimo.divide(new Money(tamParcela));
				}
				
				int numItens = listaItens.size();
				double acrescimoItem = valorAcrescimo.getValue().doubleValue() / new Double(numItens);
				
				for (NotaFiscalServicoItem notaFiscalServicoItem : listaItens) {
					Money acres = new Money(acrescimoItem).divide(new Money((notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : 1.0)));
					notaFiscalServicoItem.setPrecoUnitario(new Money(notaFiscalServicoItem.getPrecoUnitario()).add(acres).getValue().doubleValue());
				}
			}
		} else {
			item = new NotaFiscalServicoItem();
			if(!concatenarAnotacaocorpo && contrato.getAnotacaocorpo() != null && !contrato.getAnotacaocorpo().trim().equals("")){
				item.setDescricao(contrato.getAnotacaocorpo());
				if (numeroParcela > 0)
					item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
			}else{
				item.setDescricao(contrato.getDescricao());
				if (numeroParcela > 0)
					item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
			}
			item.setQtde(1.0);
			
			boolean faturarPorMedicao = false;
			if(contrato.getFrequencia() != null){
				StringBuilder descricao = new StringBuilder();
				if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL)){
					if(contrato.getMes() != null && contrato.getAno() != null){
						descricao.append(" Referente a: ").append(contrato.getMes().getNome() + " / " + contrato.getAno());
					} else if(contrato.getMes() != null){
						descricao.append(" Referente a: ").append(contrato.getMes().getNome());
					} else if (contrato.getAno() != null){
						descricao.append(" Referente a: ").append(contrato.getAno());
					}
				}
				
				item.setDescricao(item.getDescricao() + descricao.toString());
				Money valorHoraFaturada = getValorContratoForFaturarHoraTrabalhada(contrato, null);
				if(valorHoraFaturada != null && valorHoraFaturada.getValue().doubleValue() > 0){
					item.setPrecoUnitario(valorHoraFaturada.subtract(valorBruto).getValue().doubleValue());
				} else if(contrato.getValorContratoAFaturarPorMedicao() != null && 
						contrato.getValorContratoAFaturarPorMedicao().getValue().doubleValue() > 0){
					item.setPrecoUnitario(contrato.getValorContratoAFaturarPorMedicao().getValue().doubleValue());
					faturarPorMedicao = true;
				}else {
					item.setPrecoUnitario(contrato.getValorQtde().subtract(valorBruto).getValue().doubleValue());
				}
			} else if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0) {
								
				
				Contratoparcela parcela = this.getParcelaAtual(contrato.getListaContratoparcela());
				Money valorHoraFaturada = getValorContratoForFaturarHoraTrabalhada(contrato, parcela.getValor());
				if(valorHoraFaturada != null && valorHoraFaturada.getValue().doubleValue() > 0){
					item.setPrecoUnitario(valorHoraFaturada.getValue().doubleValue());
				} else if(contrato.getValorContratoAFaturarPorMedicao() != null && 
						contrato.getValorContratoAFaturarPorMedicao().getValue().doubleValue() > 0){
					item.setPrecoUnitario(contrato.getValorContratoAFaturarPorMedicao().getValue().doubleValue());
					faturarPorMedicao = true;
				}else {
					item.setPrecoUnitario(parcela.getValor().getValue().doubleValue());
				}
				if(parcela.getDesconto() != null && parcela.getDesconto().getValue().doubleValue() > 0){
					item.setPrecoUnitario(new Money(item.getPrecoUnitario()).subtract(parcela.getDesconto()).getValue().doubleValue());
				}
			} else {
				throw new SinedException("N�o foi poss�vel pegar as parcelas nem a frequ�ncia do contratro.");
			}
			
			Money valorAcrescimo = contrato.getValoracrescimo();
			if(!faturarPorMedicao && valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
				if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
					double tamParcela = new Double(contrato.getListaContratoparcela().size()).doubleValue();
					valorAcrescimo = valorAcrescimo.divide(new Money(tamParcela));
				}
				
				Money acres = valorAcrescimo.divide(new Money((item.getQtde() != null ? item.getQtde() : 1.0)));
				item.setPrecoUnitario(new Money(item.getPrecoUnitario()).add(acres).getValue().doubleValue());
			}
			
			item.setPrecoUnitario(indicecorrecaoService.calculaIndiceCorrecao(contrato.getIndicecorrecao(), new Money(item.getPrecoUnitario()), contrato.getDtproximovencimento(), contrato.getDtinicio()).getValue().doubleValue());
			
//			if(identificadorContrato != null){
//				item.setDescricao((item.getDescricao() != null ? item.getDescricao() : "") + identificadorContrato);
//			}
			
			listaItens.add(item);
		}
		nota.setListaItens(listaItens);
		
		if(concatenarAnotacaocorpo && contrato.getAnotacaocorpo() != null && !contrato.getAnotacaocorpo().trim().equals("")){
			nota.setInfocomplementar(contrato.getAnotacaocorpo());
		}
		
		if(contrato.getGrupotributacao()!=null && org.apache.commons.lang.StringUtils.isNotBlank(contrato.getGrupotributacao().getInfoadicionalfisco())){
			String infoComplementar = nota.getInfocomplementar()!=null?nota.getInfocomplementar():"";
			nota.setInfocomplementar(infoComplementar+" "+contrato.getGrupotributacao().getInfoadicionalfisco());
		}
		
		if(contrato.getGrupotributacao()!= null && org.apache.commons.lang.StringUtils.isNotBlank(contrato.getGrupotributacao().getInfoadicionalcontrib())){
			nota.setDadosAdicionais(contrato.getGrupotributacao().getInfoadicionalcontrib());
		}
	
		if(contrato.getContratotipo() != null && 
				contrato.getContratotipo().getLocacao() != null && 
				contrato.getContratotipo().getLocacao()){
			List<NotaFiscalServicoItem> itens = nota.getListaItens();
			if(itens != null && itens.size() > 0){
				NotaFiscalServicoItem it = itens.get(itens.size() - 1);
				
				String complemento = " Contrato: " + (contrato.getIdentificador() != null && !contrato.getIdentificador().trim().equals("") ? contrato.getIdentificador() : contrato.getCdcontrato());
				complemento += " Per�odo: " + SinedUtil.getDescricaoPeriodo(contrato.getDtiniciolocacao(), contrato.getDtfimlocacao());
				
				it.setDescricao(it.getDescricao() + complemento);
			}
		}
				
//		calculaTributacaoNotaFiscalServico(contrato, nota);
		
		Money baseCalculo = nota.getValorTotalServicos();
		if(nota.getDeducao() != null) baseCalculo = baseCalculo.subtract(nota.getDeducao());
		if(nota.getDescontoincondicionado() != null) baseCalculo = baseCalculo.subtract(nota.getDescontoincondicionado());
		
		nota.setBasecalculo(baseCalculo);
		nota.setBasecalculocofins(baseCalculo);
		nota.setBasecalculocsll(baseCalculo);
		nota.setBasecalculoicms(baseCalculo);
		nota.setBasecalculoinss(baseCalculo);
		nota.setBasecalculoir(baseCalculo);
		nota.setBasecalculoiss(baseCalculo);
		nota.setBasecalculopis(baseCalculo);
//		nota.setDadosAdicionais(contrato.getAnotacaocorpo());
		nota.setDtVencimento(this.getDataVencimento(contrato, contrato.getDtproximovencimento()));
		nota.setNotaStatus(NotaStatus.EMITIDA);
		nota.setDtEmissao(SinedDateUtils.currentDate());
		nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
		nota.setFaturamentolote(contrato.getFaturamentolote());
		nota.setConta(contrato.getConta());
		if(nota.getNaturezaoperacao() == null) nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
		nota.setFrequencia(contrato.getFrequencia());
		nota.setGrupotributacao(contrato.getGrupotributacao());
		
		if(contrato.getItemlistaservicoBean() != null) nota.setItemlistaservicoBean(contrato.getItemlistaservicoBean());
		if(contrato.getCodigocnaeBean() != null) nota.setCodigocnaeBean(contrato.getCodigocnaeBean());
		if(contrato.getCodigotributacao() != null) nota.setCodigotributacao(contrato.getCodigotributacao());
//		nota.setItemlistaservico(contrato.getItemlistaservico());
//		nota.setCodigocnae(contrato.getCodigocnae());
		
		List<Contrato> listaOrigem = new ArrayList<Contrato>();
		listaOrigem.add(contrato);
		nota.setListaContrato(listaOrigem);
		
//		if(contrato.getEmpresa() != null && contrato.getEmpresa().getCdpessoa() != null){
//			nota.setEmpresa(contrato.getEmpresa());
//			if(contrato.getEmpresa().getListaEndereco() != null && contrato.getEmpresa().getListaEndereco().size() > 0){
//				if(contrato.getEmpresa().getEndereco().getMunicipio() != null)
//					nota.setMunicipioissqn(contrato.getEmpresa().getEndereco().getMunicipio());
//			}
//		}

		if(contrato.getInscricaoMunicipal() != null && !contrato.getInscricaoMunicipal().equals(""))
			nota.setInscricaomunicipal(contrato.getInscricaoMunicipal());
		
//		if(!Formafaturamento.NOTA_APOS_RECEBIMENTO.equals(contrato.getFormafaturamento())){
//			preencherDadosCobranca(contrato, nota);
//		}else {
			nota.setCadastrarcobranca(Boolean.FALSE);
//		}
		
		listaNotaServico.add(nota);
	}
	
//	private void preencherDadosCobranca(Contrato contrato, NotaFiscalServico nota) {
//		nota.setContaboleto(contrato.getConta());
//		nota.setContacarteira(contrato.getContacarteira());
//		nota.setDocumentotipo(Documentotipo.BOLETO);
//		nota.setNumerofatura(contrato.getIdentificadorOrCdcontrato());
//		nota.setValororiginalfatura(nota.getValorNota());
//		nota.setValorliquidofatura(nota.getValororiginalfatura());
//		
//		List<Notafiscalservicoduplicata> listaDuplicata = new ArrayList<Notafiscalservicoduplicata>();
//		Notafiscalservicoduplicata duplicata = new Notafiscalservicoduplicata();
//		duplicata.setDocumentotipo(nota.getDocumentotipo());
//		duplicata.setDtvencimento(this.getDataVencimento(contrato, contrato.getDtproximovencimento()));
//		duplicata.setValor(nota.getValororiginalfatura());
//		listaDuplicata.add(duplicata);
//		nota.setListaDuplicata(listaDuplicata);
//	}
	
	/**
	 * C�lcula a tributa��o da nota fiscal de servi�o.
	 *
	 * @param contrato
	 * @param nota
	 * @return
	 * @author Marden Silva
	 */
	public void calculaTributacaoNotaFiscalServico(Contrato contrato, Grupotributacao grupotributacao, NotaFiscalServico nota) {		
		if (grupotributacao != null){
			if(contrato != null){
				nota.setCliente(contrato.getCliente());
				if(nota.getEnderecoCliente() == null) nota.setEnderecoCliente(contrato.getEndereco());
				nota.setEmpresa(contrato.getEmpresa());
			}
			
			notaFiscalServicoService.calculaTributacaoNota(nota, grupotributacao, nota.getBasecalculo());
		} else if(contrato != null) {		
			if(contrato.getIss() != null && contrato.getIss() > 0){
				nota.setIss(contrato.getIss());
				nota.setIncideiss(contrato.getIncideiss());
			} 			
			if(contrato.getIr() != null && contrato.getIr() > 0){
				nota.setIr(contrato.getIr());
				nota.setIncideir(contrato.getIncideir());
			}
			if(contrato.getInss() != null && contrato.getInss() > 0){
				nota.setInss(contrato.getInss());
				nota.setIncideinss(contrato.getIncideinss());
			}
			if(contrato.getPis() != null && contrato.getPis() > 0){
				nota.setPis(contrato.getPis());
				nota.setIncidepis(contrato.getIncidepis());
			}
			if(contrato.getIcms() != null && contrato.getIcms() > 0){
				nota.setIcms(contrato.getIcms());
				nota.setIncideicms(contrato.getIncideicms());
			} 
			if(contrato.getCsll() != null && contrato.getCsll() > 0){
				nota.setCsll(contrato.getCsll());
				nota.setIncidecsll(contrato.getIncidecsll());
			}
			if(contrato.getCofins() != null && contrato.getCofins() > 0){
				nota.setCofins(contrato.getCofins());
				nota.setIncidecofins(contrato.getIncidecofins());
			} 		
		}
	}
	
	/**
	 * Retorna a parcela atual para c�lculo dos valores da nota.
	 *
	 * @param listaContratoparcela
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Contratoparcela getParcelaAtual(List<Contratoparcela> listaContratoparcela) {
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			Contratoparcela contratoparcela = null;
			for (Contratoparcela cp : listaContratoparcela) {
				if(cp.getCobrada() == null || !cp.getCobrada()){
					if(contratoparcela == null || SinedDateUtils.beforeIgnoreHour(cp.getDtvencimento(), contratoparcela.getDtvencimento())){
						contratoparcela = cp;
					}
				}
			}
			return contratoparcela;
		}
		return null;
	}
	
	public Contratoparcela getUltimaParcelaCobrada(List<Contratoparcela> listaContratoparcela) {
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			Contratoparcela contratoparcela = null;
			for (Contratoparcela cp : listaContratoparcela) {
				if(cp.getCobrada() != null && cp.getCobrada()){
					if(contratoparcela == null || SinedDateUtils.afterIgnoreHour(cp.getDtvencimento(), contratoparcela.getDtvencimento())){
						contratoparcela = cp;
					}
				}
			}
			return contratoparcela;
		}
		return null;
	}
	
	/**
	 * Retorna o valor total do desconto dependendo do tipo de desconto passado por par�metro.
	 *
	 * @param contrato
	 * @param tipodesconto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money valorDescontoAba(Contrato contrato, Tipodesconto tipodesconto) {
		Money totalDesconto = new Money();
		
		List<Contratodesconto> listaDesconto = contrato.getListaContratodesconto();
		if(listaDesconto != null && listaDesconto.size() > 0){
			for (Contratodesconto cd : listaDesconto) {
				if(SinedDateUtils.afterOrEqualsIgnoreHour(contrato.getDtproximovencimento(), cd.getDtinicio()) && 
						(cd.getDtfim() == null || SinedDateUtils.beforeOrEqualIgnoreHour(contrato.getDtproximovencimento(), cd.getDtfim())) && 
						(cd.getTipodesconto() == null || tipodesconto == null || cd.getTipodesconto().equals(tipodesconto))){
					totalDesconto = totalDesconto.add(cd.getValor());
				}
			}
		}
		
		return totalDesconto;
	}
	
	/**
	 * Cria a conta a receber para o faturamento do contrato, e adiciona o 
	 * documento na lista passada por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#criaContaReceberContrato
	 *
	 * @param contrato
	 * @param listaContaReceber
	 * @param documentotipo
	 * @author Rodrigo Freitas
	 */
	private void geraContareceberFaturamento(Contrato contrato, List<Documento> listaContaReceber, Documentotipo documentotipo) {
		listaContaReceber.add(this.criaContaReceberContrato(contrato, documentotipo, false));
	}
	
	/**
	 * A partir de um contrato � gerada uma conta a receber.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#agruparItensRateio
	 * @see br.com.linkcom.sined.geral.service.RateioService#atualizaValorRateio
	 * 
	 * @param contrato
	 * @param documentotipo
	 * @return
	 * @author Rodrigo Freitas
	 * @param considerarFormaFaturamento 
	 */
	public Documento criaContaReceberContrato(Contrato contrato, Documentotipo documentotipo, Boolean locacao) {
		Documento documento = new Documento();
		
		documento.getListaContrato().add(contrato);
		documento.setContrato(contrato);
		if(contrato.getDocumentotipo() != null){
			documento.setDocumentotipo(contrato.getDocumentotipo());
		} else{
			documento.setDocumentotipo(documentotipo);
		}
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setFaturamentolote(contrato.getFaturamentolote());
		if(contrato.getViaWebService() != null && contrato.getViaWebService()){
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
		} else {
			documento.setDocumentoacao(Documentoacao.PREVISTA);
		}
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDtemissao(SinedDateUtils.currentDate());
		documento.setDtvencimento(this.getDataVencimento(contrato, contrato.getDtproximovencimento()));
		documento.setEmpresa(contrato.getEmpresa());
		documento.setConta(contrato.getConta());
		if(contrato.getConta() != null){
			documento.setVinculoProvisionado(contrato.getConta());
		} else {
			documento.setVinculoProvisionado(documentotipo.getContadestino());
		}
		
		if(contrato.getConta() != null){
			Conta conta = contaService.carregaConta(contrato.getConta());
			
			//Preenche a carteira do documento com a carteira padr�o
			if(conta.getContacarteira() != null && conta.getContacarteira().getCdcontacarteira() != null){
				documento.setContacarteira(conta.getContacarteira());
			}
			
			if (conta.getBanco() != null && conta.getBanco().getNumero() != null && conta.getBanco().getNumero() == 341){ //Ita�
				if (conta.getNossonumerointervalo() != null && conta.getNossonumerointervalo() && documento.getNossonumero() == null)
					documento.setNossonumero(StringUtils.stringCheia(contareceberService.getNextNossoNumero(conta).toString(), "0", 8, false));				
			}			
			
			if(contrato.getContacarteira() != null && contrato.getContacarteira().getCdcontacarteira() != null){
				documento.setContacarteira(contrato.getContacarteira());
			}
		}
		
		List<Contratoparcela> listaContratoparcela = contratoparcelaService.findByContrato(contrato);
		int numeroParcela = 0;
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			Contratoparcela contratoparcela = this.getParcelaAtual(listaContratoparcela);
			if(contratoparcela == null){
				throw new SinedException("Parcela atual do contrato " + contrato.getCdcontrato() + " nula.");
			}
			
			numeroParcela = listaContratoparcela.indexOf(contratoparcela);
			numeroParcela++;
			Money valorHoraFaturada = getValorContratoForFaturarHoraTrabalhada(contrato, contratoparcela.getValor()); 
			if(valorHoraFaturada != null && valorHoraFaturada.getValue().doubleValue() > 0){
				documento.setValor(valorHoraFaturada);
			}else {
				documento.setValor(contratoparcela.getValor());
			}
			
			Money desconto = this.valorDescontoAba(contrato, Tipodesconto.CONTA_RECEBER);
			if(desconto != null && desconto.getValue().doubleValue() > 0){
				documento.setValor(documento.getValor().subtract(desconto));
			}
			
			Money valorAcrescimo = contrato.getValoracrescimo();
			if(valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
				if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
					double tamParcela = new Double(contrato.getListaContratoparcela().size()).doubleValue();
					valorAcrescimo = valorAcrescimo.divide(new Money(tamParcela));
				}
				documento.setValor(documento.getValor().add(valorAcrescimo));
			}
			
			if(contratoparcela.getDesconto() != null && contratoparcela.getDesconto().getValue().doubleValue() > 0){
				documento.setValor(documento.getValor().subtract(contratoparcela.getDesconto()));
			}
		} else if(locacao != null && locacao){
			Money valorFaturarLocacao = contrato.getFaturalocacaoValorfaturar(); 
			if(valorFaturarLocacao != null && valorFaturarLocacao.getValue().doubleValue() > 0){
				documento.setValor(valorFaturarLocacao);
			}else {
				documento.setValor(contrato.getValorContrato());
			}
		}else {
			Money valorHoraFaturada = getValorContratoForFaturarHoraTrabalhada(contrato, null); 
			if(valorHoraFaturada != null && valorHoraFaturada.getValue().doubleValue() > 0){
				documento.setValor(valorHoraFaturada);
			}else {
				documento.setValor(contrato.getValorContrato());
			}
		}
		
		documento.setValor(indicecorrecaoService.calculaIndiceCorrecao(contrato.getIndicecorrecao(), documento.getValor(), documento.getDtvencimento(), contrato.getDtinicio()));
		
		if(contrato.getIndicecorrecao() != null)
			documento.setIndicecorrecao(contrato.getIndicecorrecao());
		documento.setEndereco(contrato.getEnderecoPrioritario());
		
		if(contrato.getNomefantasia() == null || contrato.getNomefantasia().equals("")){
			documento.setPessoa(contrato.getCliente());
			documento.setCliente(contrato.getCliente());
			documento.setTipopagamento(Tipopagamento.CLIENTE);
		}else{
			documento.setTipopagamento(Tipopagamento.OUTROS);
			documento.setOutrospagamento(contrato.getNomefantasia());
		}
		
		if(contrato.getFrequencia() != null && contrato.getFrequencia().getCdfrequencia() != null && 
		   contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL)){
			
			StringBuilder descricao = new StringBuilder();
			if(contrato.getMes() != null && contrato.getAno() != null){
				descricao.append(" Referente a: ").append(imprimeMesAno(contrato.getMes(), contrato.getAno()));
			} else if(contrato.getMes() != null){
				descricao.append(" Referente a: ").append(contrato.getMes().getNome());
			} else if (contrato.getAno() != null){
				descricao.append(" Referente a: ").append(contrato.getAno());
			}
			
			if (numeroParcela > 0){
				documento.setDescricao(contrato.getDescricao() + descricao.toString() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
			} else {
				documento.setDescricao(contrato.getDescricao() + descricao.toString());
			}
		} else {
			if (numeroParcela > 0){
				documento.setDescricao(contrato.getDescricao() + " - Parcela " + numeroParcela + "/" + listaContratoparcela.size());
			} else {
				documento.setDescricao(contrato.getDescricao());
			}
		}
		
		if (parametrogeralService.getBoolean("FATURAR_CONTRATO_ENVIA_PRODUTOS_ABABOLETO")) {
			if (contrato.getListaContratomaterial() != null && contrato.getListaContratomaterial().size() > 0) {
				StringBuilder observacao = new StringBuilder();
				observacao.append("Produtos / Servi�os: ");
				
				for (Contratomaterial c : contrato.getListaContratomaterial()) {
					observacao.append(c.getServico().getNome() + ": R$" + c.getValortotal() + " | ");
				}
				
				documento.setObservacao(observacao.substring(0, observacao.length() - 2) + (documento.getObservacao() != null ? " " + documento.getObservacao() : ""));
			}
		}
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		documento.setAcaohistorico(Documentoacao.PREVISTA);
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		if(contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null){
			listaRateioitem.addAll(contrato.getRateio().getListaRateioitem());
			for (Rateioitem ri : listaRateioitem) {
				ri.setCdrateioitem(null);
				ri.setRateio(null);
			}
		}
		
		contratoService.preencherTaxasContrato(listaTaxaitem, contrato);
		
		rateioitemService.agruparItensRateio(listaRateioitem);
		rateio.setListaRateioitem(listaRateioitem);
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		
		// FEITO ISSO PARA AJUSTAR O RATEIO
		BigDecimal somaItens = new BigDecimal(0);
		for (Rateioitem ri : listaRateioitem) {
			somaItens = somaItens.add(SinedUtil.roundFloor(ri.getValor().getValue(), 2));			
		}
		BigDecimal diferenca = documento.getValor().getValue().subtract(somaItens);
		if(diferenca.compareTo(BigDecimal.ZERO) != 0){
			listaRateioitem.get(0).setValor(listaRateioitem.get(0).getValor().add(new Money(diferenca)));
		}
		
		String msg;
		String msg1 = "";
		String msg2 = "";
		for (Taxaitem taxaitem : listaTaxaitem) {
			taxaitem.setDtlimite(documento.getDtvencimento());
			
			if(taxaitem.getDias() != null && documento.getDtvencimento() != null){
				if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), -taxaitem.getDias()));
				}else {
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), taxaitem.getDias()));
				}
			}
			
			msg = 	taxaitem.getTipotaxa().getNome() + 
					" de " + 
					(taxaitem.isPercentual() ? "" : "R$") + 
					taxaitem.getValor().toString() + 
					(taxaitem.isPercentual() ? "%" : "") + 
					ateApos(taxaitem.getTipotaxa().getCdtipotaxa())+
					SinedDateUtils.toString(taxaitem.getDtlimite()) +
					". ";
			
			if((msg1+msg).length() <= 80){
				msg1 += msg;
			} else if((msg2+msg).length() <= 80){
				msg2 += msg;
			}
		}
		
		documento.setMensagem2(msg1);
		documento.setMensagem3(msg2);
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		
		documento.setRateio(rateio);
		documento.setTaxa(taxa);
		
		if (contrato.getConta() != null){
			Conta conta = contaService.carregaContaComMensagem(contrato.getConta(), null);
			
			if(conta.getContacarteira().getMsgboleto1() != null)
				documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			
			if(conta.getContacarteira().getMsgboleto2() != null) {
				documento.setMensagem5(conta.getContacarteira().getMsgboleto2());				
			}
		
		}
		
		if (Boolean.TRUE.equals(contrato.getReajuste())){
			documento.setMensagem6(contrato.getMensagemBoletoReajuste());
		}
		
		documento.setNumero(contrato.getCdcontrato().toString());
		
//		if (contrato.getListaNotacontrato()!=null && !contrato.getListaNotacontrato().isEmpty()){
//			String cdfaturamentocontrato = "";
//			for (NotaContrato notaContrato: contrato.getListaNotacontrato()){
//				if (notaContrato.getFaturamentocontrato()!=null && notaContrato.getFaturamentocontrato().getCdfaturamentocontrato()!=null){
//					cdfaturamentocontrato += notaContrato.getFaturamentocontrato().getCdfaturamentocontrato() + ", ";
//				}
//			}
//			if (!cdfaturamentocontrato.equals("")){
//				cdfaturamentocontrato = cdfaturamentocontrato.substring(0, cdfaturamentocontrato.length()-1);
//				documento.setNumero(documento.getNumero() + " (" + cdfaturamentocontrato + ")");
//			}
//		}
		
		return documento;
	}
	
	@SuppressWarnings("unchecked")
	public void preencherTaxasContrato(List<Taxaitem> listaTaxaitem, Contrato contrato) {
		if(contrato != null){
			Taxa taxaAux = null;
			boolean gerarMensagemTaxa = false;
			if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
				taxaAux = taxaService.findTaxa(contrato.getTaxa());
				gerarMensagemTaxa = true;
			}
			
			if(taxaAux == null || SinedUtil.isListEmpty(taxaAux.getListaTaxaitem())){
				if(contrato.getConta() != null && contrato.getConta().getCdconta() != null){
					Conta conta = contaService.loadWithTaxa(contrato.getConta());
					if(conta != null){
						taxaAux = conta.getTaxa();
						if(taxaAux != null){
							taxaAux.setCdconta(conta.getCdconta());
						}
					}
				}
			}
		
			if(taxaAux != null && taxaAux.getListaTaxaitem() != null){
				for(Taxaitem taxaitemAux : taxaAux.getListaTaxaitem()){
					boolean adicionar = true;
					if(taxaAux.getCdconta() != null && taxaitemAux.getTipotaxa() != null && SinedUtil.isListNotEmpty(listaTaxaitem)){
						for(Taxaitem taxaitem : listaTaxaitem){
							if(taxaitem.getCdconta() != null && taxaAux.getCdconta().equals(taxaitem.getCdconta()) && 
									taxaitem.getTipotaxa() != null && taxaitemAux.getTipotaxa().equals(taxaitem.getTipotaxa())){
								adicionar = false;
								break;
							}
						}
					}
					
					if(adicionar && !taxaitemService.existeTaxaitem(SinedUtil.listToSet(listaTaxaitem, Taxaitem.class), taxaitemAux.getTipotaxa())){
						taxaitemAux.setCdconta(taxaAux.getCdconta());
						listaTaxaitem.add(taxaitemAux);
					}
				}
				
//				listaTaxaitem.addAll(taxaAux.getListaTaxaitem());
				for (Taxaitem ti : listaTaxaitem) {
					ti.setCdtaxaitem(null);
					if(gerarMensagemTaxa){
						ti.setGerarmensagem(Boolean.TRUE);
					}
				}
			}
		}
	}
	
	public Documento criaContaReceberFaturaLocacaoAvulsaOrIndenizacao(Contratofaturalocacao contratofaturalocacao) {
		
		Documento documento = new Documento();
		documento.setNumero(contratofaturalocacao.getNumero().toString());
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDtemissao(SinedDateUtils.currentDate());
		documento.setDtvencimento(contratofaturalocacao.getDtvencimento()!=null ? contratofaturalocacao.getDtvencimento() : SinedDateUtils.currentDate());
		documento.setEmpresa(contratofaturalocacao.getEmpresa());
		documento.setValor(contratofaturalocacao.getValortotal());
		documento.setPessoa(contratofaturalocacao.getCliente());
		documento.setCliente(contratofaturalocacao.getCliente());
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setDescricao(contratofaturalocacao.getDescricao()!=null && contratofaturalocacao.getDescricao().length()>150 ? contratofaturalocacao.getDescricao().substring(0, 150) : "Receita de faturamento");
		documento.setMensagem1("Referente a fatura: " + contratofaturalocacao.getNumero().toString());
		documento.setDocumentotipo(documentotipoService.findBoleto());
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		documento.setAcaohistorico(Documentoacao.PREVISTA);		
		
		if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
			Contagerencial contagerencial = contagerencialService.getContagerencialIndenizacaoByParametro();
			Centrocusto centrocusto = centrocustoService.getCentrocustoIndenizacaoByParametro();
			if(contagerencial != null || centrocusto != null){
				Rateio rateio = new Rateio();
				Rateioitem rateioitem = new Rateioitem(contagerencial, centrocusto, null, contratofaturalocacao.getValortotal(), 100d);
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				listaRateioitem.add(rateioitem);
				rateio.setListaRateioitem(listaRateioitem);
				documento.setRateio(rateio);
			}
		}
		
		return documento;
	}
	
	/**
	 * Retorna uma string com m�s e ano para o faturamento.
	 *
	 * @param mes
	 * @param ano
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String imprimeMesAno(Mes mes, Integer ano){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.MONTH, mes.ordinal());
		c.set(Calendar.YEAR, ano);
		
		return new SimpleDateFormat("MMMM/yyyy").format(c.getTime());
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#buscaListaPagamentoWebservice
	 *
	 * @param cliente
	 * @param material
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<PagamentoClienteServicoBean> buscaListaPagamentoWebservice(Cliente cliente, Material material, Date dtinicio, Date dtfim) {
		return contratoDAO.buscaListaPagamentoWebservice(cliente, material, dtinicio, dtfim);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os contrato para atualizar dados
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findContratoAtualizarDados(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findContratoAtualizarDados(String whereIn){
		return contratoDAO.findContratoAtualizarDados(whereIn);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* atualiza os dados de vencimento, valor, reponsavel, quantidade e fator de ajuste do contrato
	* 
	* @see	void br.com.linkcom.sined.geral.dao.ContratoDAO#atualizaDados(Contrato c, Date dtVencimento, Money valor, Colaborador responsavel, Fatorajuste fatorajuste)
	*
	* @param c
	* @param dtVencimento
	* @param valor
	* @param responsavel
	* @param qtde
	* @param fatorajuste
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaDados(Contrato c, Date dtVencimento, Money valor, Colaborador responsavel, Double qtde, Fatorajuste fatorajuste, Indicecorrecao indicecorrecao, Date dtRenovacao) {
		contratoDAO.atualizaDados(c, dtVencimento, valor, responsavel, qtde, fatorajuste, indicecorrecao, dtRenovacao);		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os contrato para calcular o valor do contrato
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findContratoParaCalculoValorcontrato(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Oct 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findContratoParaCalculoValorcontrato(String whereIn){
		return contratoDAO.findContratoParaCalculoValorcontrato(whereIn);
	}
	
	/**
	 *	M�todo que busca o contrato do banco e logo depois transforma o Contrato em um ContratoRTF.
	 *
	 * @param contrato
	 * @return
	 * @since 18/10/2011
	 * @author Rodrigo Freitas
	 */
	public ContratoRTF makeContratoRTF(Contrato contrato) {
		contrato = this.findForEmissaoContratoRTF(contrato);
		if(contrato != null && contrato.getEmpresa() != null){
			contrato.setEmpresa(empresaService.loadForEntrada(contrato.getEmpresa()));
		}
		if(contrato.getResponsavel() != null && contrato.getResponsavel().getCdpessoa() != null){
			contrato.setResponsavel(colaboradorService.loadForEntrada(contrato.getResponsavel()));
		}
		if(contrato.getCliente() != null && contrato.getCliente().getCdpessoa() != null){
			contrato.setCliente(clienteService.findForContratoRtf(contrato.getCliente()));
		}
		List<Contratoparcela> listaContratoparcela = contratoparcelaService.findByContrato(contrato);
		contrato.setListaContratoparcela(listaContratoparcela);
		contrato.setListaContratomaterial(contratomaterialService.findForRTFByContrato(contrato));
		contrato.setListaContratohistorico(contratohistoricoService.findByContrato(contrato));
		contrato.setListaNotacontrato(new ListSet<NotaContrato>(NotaContrato.class));
		contrato.getCliente().setListaContato(pessoaContatoService.findByPessoaContatotipo(contrato.getCliente(), null));
		if(contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null){
			contrato.setVendedor(pessoaService.load(contrato.getVendedor(), "pessoa.cdpessoa, pessoa.nome"));
		}
		if(contrato.getVendedortipo() != null && Vendedortipo.COLABORADOR.equals(contrato.getVendedortipo()) && 
				contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null){
			contrato.setColaborador(colaboradorService.load(new Colaborador(contrato.getVendedor().getCdpessoa()), "colaborador.cdpessoa, colaborador.nome, colaborador.rg"));
		}
		List<Nota> notaremessa = notaService.findForNotaRemessaLocacao(contrato);
		String numeronota = "";
		String dtnota = "";
		for (Nota nota : notaremessa) {
			numeronota += nota.getNumero() + ", ";
			dtnota += SinedDateUtils.toString(nota.getDtEmissao()) + ", ";
			
		}
		numeronota = !numeronota.isEmpty() ? numeronota.substring(0, numeronota.length()-2) : "";
		dtnota = !dtnota.isEmpty() ? dtnota.substring(0, dtnota.length()-2) : "";
		
		contrato.setNotaremessalocacao(numeronota);
		contrato.setDtemissaonotaremessalocacao(dtnota);
		
		Double saldoRomaneio = 0.0;
		List<Romaneio> romaneios = romaneioService.findByContrato(contrato);
		for (Romaneio romaneio : romaneios){
			// Quantidade enviada � Quantidade devolvida (considerar somente romaneios baixados sem ser indeniza��o).
			if (romaneio.getRomaneiosituacao().equals(Romaneiosituacao.BAIXADA) &&
					( romaneio.getLocalarmazenagemdestino().getIndenizacao() == null || 
							!romaneio.getLocalarmazenagemdestino().getIndenizacao()) &&
					(romaneio.getLocalarmazenagemorigem().getIndenizacao() == null ||
							!romaneio.getLocalarmazenagemorigem().getIndenizacao())){
				
				if (romaneio.getLocalarmazenagemdestino().getClientelocacao() != null && 
						romaneio.getLocalarmazenagemdestino().getClientelocacao()){
					for (Romaneioitem romaneioitem : romaneio.getListaRomaneioitem()){
						saldoRomaneio += romaneioitem.getQtde();
					}
				}else if (romaneio.getLocalarmazenagemorigem().getClientelocacao() != null &&
						romaneio.getLocalarmazenagemorigem().getClientelocacao()) {
					for (Romaneioitem romaneioitem : romaneio.getListaRomaneioitem()){
						saldoRomaneio -= romaneioitem.getQtde();
					}
				}
			}
		}
		contrato.setSaldoromaneio(saldoRomaneio );
		
		return contrato.getContratoRTF();
	}
	
	/**
	 * M�todo que retorna a lista de contrato para a emiss�o do relatorio, baseado no modelo RTF
	 * 
 	 * @author Rodrigo Freitas
	**/
	public Contrato findForEmissaoContratoRTF(Contrato contrato) {
		return contratoDAO.findForEmissaoContratoRTF(contrato);
	}
	
	/**
	* M�todo com refer�ncia no DAO 
	* retorna a lista de contrato do cliente passado como par�metro
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findByCliente(Cliente cliente)
	*
	* @param cliente
	* @return
	* @since Nov 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findBycliente(Cliente cliente){
		return contratoDAO.findByCliente(cliente);
	}
	
	/**
	* M�todo com refer�ncia no DAO 
	* busca os dados para calculo do valor do contrato e inscri��o municipal do cliente
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findForFaturarPorMedicao(String whereIn) 
	*
	* @param whereIn
	* @return
	* @since Nov 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findForFaturarPorMedicao(String whereIn) {
		return contratoDAO.findForFaturarPorMedicao(whereIn);
	}
	
	/**
	 * Faz a gera��o do faturamento por medi��o do contrato.
	 *
	 * @param listaContrato
	 * @param agrupamento
	 * @return
	 * @author Luiz Fernando
	 */
	public String gerarFaturamentoPorMedicao(Contrato c, Boolean agrupamento) {
		List<NotaFiscalServico> listaNotaServico = new ArrayList<NotaFiscalServico>();
		List<Notafiscalproduto> listaNotaProduto = new ArrayList<Notafiscalproduto>();
		List<Documento> listaContaReceber = new ArrayList<Documento>();
		
		Documentotipo documentotipo = documentotipoService.findForFaturamentoContareceber();
		
		
		if(c.getRateio() != null && c.getRateio().getCdrateio() != null){
			c.setRateio(rateioService.findRateio(c.getRateio()));
		}
		if(c.getTaxa() != null && c.getTaxa().getCdtaxa() != null){
			c.setTaxa(taxaService.findTaxa(c.getTaxa()));
		}
		
		Formafaturamento formafaturamento = c.getFormafaturamento();
		
		Documentotipo documentotipoBoleto = null;
		if(Formafaturamento.SOMENTE_BOLETO.equals(c.getFormafaturamento())){
			documentotipoBoleto = documentotipoService.findBoleto();
		}
		if(formafaturamento.equals(Formafaturamento.SOMENTE_BOLETO)){
			this.geraContareceberFaturamento(c, listaContaReceber, documentotipoBoleto != null ? documentotipoBoleto : documentotipo);
		} else if(formafaturamento.equals(Formafaturamento.NOTA_ANTES_RECEBIMENTO)){
			Faturamentocontrato faturamentocontrato = new Faturamentocontrato();
			faturamentocontratoService.saveOrUpdate(faturamentocontrato);
			c.setFaturamentocontrato(faturamentocontrato);
			
			Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(c.getEmpresa());
			
			if(c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (isMunicipioBrasilia || (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial()))){
				this.geraNotasSeparadasFaturamento(c, listaNotaServico, listaNotaProduto);
			} else {
				this.geraNotaServicoFaturamento(c, listaNotaServico, null);
			}
		} else if(formafaturamento.equals(Formafaturamento.NOTA_APOS_RECEBIMENTO)){
			throw new SinedException("N�o � poss�vel gerar o faturamento por medi��o com a forma de faturamento 'Nota ap�s Recebimento'.");
		}
		
		String msg = validaValorNotas(listaNotaProduto, listaNotaServico);
		if(msg == null || msg.equals("")){
			this.salvaListaNotaServico(listaNotaServico);
			this.salvaListaNotaProduto(listaNotaProduto);
			this.salvaListaContaReceber(listaContaReceber);
			
			this.verificaComissionamento(listaNotaServico, listaNotaProduto, listaContaReceber);			
			
			if(c.getAcrescimoproximofaturamento() != null && c.getAcrescimoproximofaturamento()){
				this.updateZeraAcrescimo(c);
			}
			this.atualizaParcelaCobradaFaturamento(c);
			this.atualizaDtProximoVencimentoFaturamento(c);
//			this.atualizaValorRateioFaturamento(c);
			this.updateAux(c.getCdcontrato());
			
			if(listaNotaServico != null && listaNotaServico.size() > 0)
				msg = "sucesso cdnotafiscalservico=" + listaNotaServico.get(0).getCdNota();
		}
		return msg;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findForAtualizarValorFatorajuste()
	 * 
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForAtualizarValorFatorajuste(){
		return contratoDAO.findForAtualizarValorFatorajuste();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see	br.com.linkcom.sined.geral.dao.ContratoDAO#updateValorFatorajusteContrato(Integer cdcontrato, Money valor)
	 * 
	 * @param cdcontrato
	 * @param valor
	 * @author Luiz Fernando
	 */
	public void updateValorFatorajusteContrato(Integer cdcontrato, Money valor){
		contratoDAO.updateValorFatorajusteContrato(cdcontrato, valor);
	}
	
	/**
	 * M�todo para atualizar os valores dos contrato que tenham fator de ajuste
	 * obs: Este m�todo � executado quando h� o primeiro acesso ao sistema no dia, quando h� altera��o no fator de ajuste e 
	 * ao gerar faturamento
	 *
	 * @author Luiz Fernando
	 */
	public void atualizaValorContratoFatorAjuste() {		
		List<Contrato> lista = findForAtualizarValorFatorajuste();
		if(lista != null && !lista.isEmpty()){
			String dtMesAnoAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
			Fatorajuste f = new Fatorajuste();
			for(Contrato c : lista){
				if(c.getFatorajuste() != null && c.getFatorajuste().getCdfatorajuste() != null){					
					f = fatorajusteService.carregaFatorajuste(c.getFatorajuste());
					if(f != null && f.getListaFatorajustevalor() != null && !f.getListaFatorajustevalor().isEmpty()){
						for(Fatorajustevalor fatorajustevalor : f.getListaFatorajustevalor()){
							if(fatorajustevalor.getMesano() != null && fatorajustevalor.getMesanoAux() != null &&
									fatorajustevalor.getMesanoAux().equals(dtMesAnoAtual) && fatorajustevalor.getValor() != null){								
								if(fatorajustevalor.getValor() != null){
									updateValorFatorajusteContrato(c.getCdcontrato(), fatorajustevalor.getValor());
									break;
								}									
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * retorna o total faturado do contrato no m�s
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ContratoDAO#findForTotalFaturado(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalFaturado(Contrato contrato){
		return contratoDAO.findForTotalFaturado(contrato);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 03/02/2012
	 * @param contrato
	 * @return
	 */
	public String carregaUrlSistema(Contrato contrato){
		return contratoDAO.carregaUrlSistema(contrato);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 16/03/2012
	 * @param bean
	 * @return
	 */
	public Contrato carregarContratoComDocumentoOrigem(Documento bean){
		return contratoDAO.carregarContratoComDocumentoOrigem(bean);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 16/03/2012
	 * @param bean
	 * @return
	 */
	public Contrato carregarContratoComNotaFiscal(Documento bean){
		return contratoDAO.carregarContratoComNotaFiscal(bean);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name LoadContratoWithColaborador
	 * @param contrato
	 * @return
	 * @return Contrato
	 * @author Thiago Augusto
	 * @date 18/06/2012
	 *
	 */
	public Contrato loadContratoWithColaborador(Contrato contrato){
		return contratoDAO.loadContratoWithColaborador(contrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2015
	 */
	public Contrato loadContratoWithEmpresa(Contrato contrato){
		return contratoDAO.loadContratoWithEmpresa(contrato);
	}
	
	/**
	 * M�todo que realiza o rec�lculo das comiss�es dos contratos selecionados.
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public void recalcularComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		// BUSCA OS DADOS NECESS�RIOS
		Map<Integer, List<Contrato>> mapNota = new HashMap<Integer, List<Contrato>>();
		List<Contrato> listaContrato;
		
		List<NotaFiscalServico> listaNFServico = notaFiscalServicoService.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
		for (NotaFiscalServico nf : listaNFServico) {
			listaContrato = new ArrayList<Contrato>();
			for (NotaContrato nc : nf.getListaNotaContrato()) {
				if(nc.getContrato() != null){
					listaContrato.add(nc.getContrato());
				}
			}
			mapNota.put(nf.getCdNota(), listaContrato);
		}
		if(listaNFServico != null && !listaNFServico.isEmpty()){
			listaNFServico = notaFiscalServicoService.carregarInfoCalcularTotalForComissao(listaNFServico);
			for (NotaFiscalServico nf : listaNFServico) {
				nf.setListaContrato(mapNota.get(nf.getCdNota()));
			}
		}
		
		List<Notafiscalproduto> listaNFProduto = notafiscalprodutoService.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
		for (Notafiscalproduto nf : listaNFProduto) {
			listaContrato = new ArrayList<Contrato>();
			for (NotaContrato nc : nf.getListaNotaContrato()) {
				if(nc.getContrato() != null){
					listaContrato.add(nc.getContrato());
				}
			}
			mapNota.put(nf.getCdNota(), listaContrato);
		}
		if(listaNFProduto != null && !listaNFProduto.isEmpty()){
			listaNFProduto = notafiscalprodutoService.carregarInfoParaCalcularTotalProduto(CollectionsUtil.listAndConcatenate(listaNFProduto, "cdNota", ","), "notafiscalproduto.dtEmissao, notafiscalproduto.cdNota", true);
			for (Notafiscalproduto nf : listaNFProduto) {
				nf.setListaContrato(mapNota.get(nf.getCdNota()));
			}
		}
		
		List<Documento> listaContareceber = contareceberService.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
		for (Documento documento : listaContareceber) {
			if(documento.getListaDocumentoOrigem() != null && documento.getListaDocumentoOrigem().size() > 0){
				Contrato contrato = null;
				for (Documentoorigem ori : documento.getListaDocumentoOrigem()) {
					if(ori.getContrato() != null){
						contrato = ori.getContrato();
						break;
					}
				}
				documento.setContrato(contrato);
			}
		}
		
		List<Documento> listaContareceberComNota = contareceberService.findForRecalculoComissaoComNota(whereIn, dtrecalculoDe, dtrecalculoAte);
		for (Documento documento : listaContareceberComNota) {
			if(documento.getListaNotaDocumento() != null && documento.getListaNotaDocumento().size() > 0){
				Contrato contrato = null;
				Nota nota = null;
				for (NotaDocumento nd : documento.getListaNotaDocumento()) {
					if(nd.getNota().getListaNotaContrato() != null && nd.getNota().getListaNotaContrato().size() > 0){
						for(NotaContrato nc : nd.getNota().getListaNotaContrato()){
							contrato = nc.getContrato();
							nota = nd.getNota();
							
							if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
								nota = notaFiscalServicoService.carregarInfoCalcularTotal(new NotaFiscalServico(nota));
							} else if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
								nota = notafiscalprodutoService.carregarInfoCalcularTotal(nota);
							}
							break;
						}
						if(contrato != null){
							break;
						}
					}
				}
				documento.setContrato(contrato);
				documento.setNota(nota);
			}
		}
				
		// RECRIA OS COMISSIONAMENTOS DAS NOTAS/CONTAS A RECEBER
		this.verificaComissionamentoNotaServico(listaNFServico, true);
		this.verificaComissionamentoNotaProduto(listaNFProduto, true);
		this.verificaComissionamentoDocumento(listaContareceber, true);
		
		// caso a nota tenha conta a receber, gerar comissionamento pelo documento
		this.verificaComissionamentoDocumento(listaContareceberComNota, true);
		
		// VINCULA OS DOCUMENTOS J� EXISTENTES
		this.vinculaDocumentoComissinamentoNotaServico(listaNFServico);
		this.vinculaDocumentoComissinamentoNotaProduto(listaNFProduto);
		
		try {			
			//Obs: M�todo criado para excluir as comiss�es de colaboradores que foram retirados do contrato e que tinham comiss�o gerada
			//excluir apenas as comiss�es que n�o foram repassadas.
			this.excluirComissioesColaboradores(listaNFServico, listaContareceber, listaContareceberComNota, listaNFProduto, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// HIST�RICO DO CONTRATO
		String[] ids = whereIn.split(",");
		String observacao = "Per�odo: ";
		if(dtrecalculoDe != null || dtrecalculoAte != null){
			observacao += SinedUtil.getDescricaoPeriodo(dtrecalculoDe, dtrecalculoAte);
		} else {
			observacao += "N�o informado";
		}
		
		for (int i = 0; i < ids.length; i++) {
			Contrato contrato = new Contrato(Integer.parseInt(ids[i]));
			
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setAcao(Contratoacao.RECALCULO_COMISSAO);
			contratohistorico.setContrato(contrato);
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
			contratohistorico.setObservacao(observacao);
			
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
	}
	
	/**
	 * M�todo que realiza o rec�lculo das comiss�es de contas a receber baixadas.
	 *
	 * @see br.com.linkcom.sined.geral.service.ContareceberService#findForRecalculoComissaoDocumentoBaixado(String whereInCddocumento)
	 *
	 * @param listaDocumento
	 * @author Luiz Fernando
	 */
	public void recalcularComissaoContaBaixada(List<Documento> listaDocumento) {
		if(listaDocumento != null && !listaDocumento.isEmpty()){
			StringBuilder whereInCddocumento = new StringBuilder();
			for (Documento documento : listaDocumento){
				if(documento.getCddocumento() != null){
					if(!"".equals(whereInCddocumento.toString()))
						whereInCddocumento.append(",");
					whereInCddocumento.append(documento.getCddocumento());
				}
			}
			if(!"".equals(whereInCddocumento.toString())){
				// BUSCA OS DADOS NECESS�RIOS
				List<Documento> listaContareceber = contareceberService.findForRecalculoComissaoDocumentoBaixado(whereInCddocumento.toString());
				if(listaContareceber != null && !listaContareceber.isEmpty()){
					for (Documento documento : listaContareceber) {
						if(documento.getListaDocumentoOrigem() != null && documento.getListaDocumentoOrigem().size() > 0){
							Contrato contrato = null;
							for (Documentoorigem ori : documento.getListaDocumentoOrigem()) {
								if(ori.getContrato() != null){
									contrato = ori.getContrato();
									break;
								}
							}
							documento.setContrato(contrato);
						}
					}
							
					// RECRIA OS COMISSIONAMENTOS DAS CONTAS A RECEBER
					this.verificaComissionamentoDocumento(listaContareceber, true, true);
				}
				
				List<Documento> listaContareceberComNota = contareceberService.findForRecalculoComissaoDocumentoBaixadoComNota(whereInCddocumento.toString());
				if(listaContareceberComNota != null && !listaContareceberComNota.isEmpty()){
					for (Documento documento : listaContareceberComNota) {
						if(documento.getListaNotaDocumento() != null && documento.getListaNotaDocumento().size() > 0){
							Nota nota = null;
							Contrato contrato = null;
							for (NotaDocumento nd : documento.getListaNotaDocumento()) {
								if(nd.getNota().getListaNotaContrato() != null && nd.getNota().getListaNotaContrato().size() > 0){
									for(NotaContrato nc : nd.getNota().getListaNotaContrato()){
										contrato = nc.getContrato();
										nota = nd.getNota();
										
										if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
											nota = notaFiscalServicoService.carregarInfoCalcularTotal(new NotaFiscalServico(nota));
										} else if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
											nota = notafiscalprodutoService.carregarInfoCalcularTotal(nota);
										}
										break;
									}
									if(contrato != null){
										break;
									}
								}
							}
							if(contrato != null) documento.setContrato(contrato);
							if(nota != null) documento.setNota(nota);
						}
					}
					
					// RECRIA OS COMISSIONAMENTOS DAS CONTAS A RECEBER COM NOTA
					this.verificaComissionamentoDocumento(listaContareceberComNota, true, true);
				}
			}
		}
	}
	
	/**
	 * Vincula os registros existentes de receitas j� geradas de NF de produto
	 *
	 * @param listaNFServico
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	private void vinculaDocumentoComissinamentoNotaProduto(List<Notafiscalproduto> listaNFProduto) {
		for (Notafiscalproduto nota : listaNFProduto) {
			if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
				List<Documento> listaDocumento = documentoService.findByNota(nota);
				if(listaDocumento != null && listaDocumento.size() > 0){
					for (Documento doc : listaDocumento) {
						for (Contrato contrato : nota.getListaContrato()) {
							List<Documentocomissao> listaDocumentocomissao = documentocomissaoService.findComissaocontrato(contrato, nota);
							if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
								for(Documentocomissao documentocomissao : listaDocumentocomissao){					
									documentocomissaoService.atualizaDocumentocomissao(documentocomissao, doc);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Vincula os registros existentes de receitas j� geradas de NF de servi�os
	 *
	 * @param listaNFServico
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	private void vinculaDocumentoComissinamentoNotaServico(List<NotaFiscalServico> listaNFServico) {
		for (NotaFiscalServico nota : listaNFServico) {
			if(nota.getListaContrato() != null && nota.getListaContrato().size() > 0){
				List<Documento> listaDocumento = documentoService.findByNota(nota);
				if(listaDocumento != null && listaDocumento.size() > 0){
					for (Documento doc : listaDocumento) {
						for (Contrato contrato : nota.getListaContrato()) {
							List<Documentocomissao> listaDocumentocomissao = documentocomissaoService.findComissaocontrato(contrato, nota);
							if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
								for(Documentocomissao documentocomissao : listaDocumentocomissao){					
									documentocomissaoService.atualizaDocumentocomissao(documentocomissao, doc);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		return contratoDAO.verificaListaFechamento(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findSomenteComParcelasEmAberto(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 14/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findSomenteComParcelasEmAberto(String whereIn) {
		return contratoDAO.findSomenteComParcelasEmAberto(whereIn);
	}
	
	
	/**
	 * M�todo que c�lcula o valor total da comiss�o por per�odo de data de assinatura do contrato para o colaborador  
	 *
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getCalculaTotalComissaoColaboradorMesDtassinatura(Colaborador colaborador, Date dtreferencia1, Date dtreferencia2) {
		List<Contrato> listaContrato = this.findForComissaoByColaboradorDtassinatura(colaborador, dtreferencia1, dtreferencia2);
		
		Money totalcomissao = new Money();
		Comissionamento vendedorComissionamento, colaboradorComissionamento;
		Pessoa vendedor;
		Colaborador colaboradoraux;
		
		if(listaContrato != null && !listaContrato.isEmpty()){
			for(Contrato contrato : listaContrato){
				if(contrato.getVendedor() != null && contrato.getComissionamento() != null && contrato.getVendedor().equals(colaborador)){
					vendedor = contrato.getVendedor();
					vendedorComissionamento = contrato.getComissionamento();			
					
					if(vendedorComissionamento != null){
						totalcomissao = totalcomissao.add(calculaValorComissao(contrato, contrato.getValor(), vendedor, Boolean.TRUE));
					}
				}
				
				if(contrato.getListaContratocolaborador() != null && contrato.getListaContratocolaborador().size() > 0){
					for (Contratocolaborador cc : contrato.getListaContratocolaborador()) {
						if(cc.getColaborador().equals(colaborador)){
							colaboradoraux = cc.getColaborador();
							colaboradorComissionamento = cc.getComissionamento();				
							
							if(colaboradorComissionamento != null ){
								totalcomissao = totalcomissao.add(calculaValorComissao(contrato, contrato.getValor(), colaboradoraux, Boolean.FALSE));
							}
						}
					}
				}
			}
		}
		
		return totalcomissao;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForComissaoByColaboradorDtassinatura(Colaborador colaborador, Date dataGerada)
	 *
	 * @param colaborador
	 * @param dtassinatura
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForComissaoByColaboradorDtassinatura(Colaborador colaborador, Date dtreferencia1, Date dtreferencia2) {
		return contratoDAO.findForComissaoByColaboradorDtassinatura(colaborador, dtreferencia1, dtreferencia2);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#updateIsento(String whereIn)
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 04/12/2012
	 */
	public void updateIsento(String whereIn) {
		contratoDAO.updateIsento(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#loadForRomaneio(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public Contrato loadForRomaneio(Contrato contrato) {
		return contratoDAO.loadForRomaneio(contrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#loadForRomaneio(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Filipe Santos
	 * @since 21/03/2014
	 */
	public List<Contrato> loadListForRomaneio(String whereIn) {
		return contratoDAO.loadListForRomaneio(whereIn);
	}
	
	/**
	 * Pega o valor unit�rio do material da lista de material do contrato.
	 *
	 * @param listaContratomaterial
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public Double getValorUnitarioMaterial(List<Contratomaterial> listaContratomaterial, Material material) {
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			if(contratomaterial.getServico() != null && contratomaterial.getServico().equals(material)){
				return contratomaterial.getValorunitario();
			}
		}
		return null;
	}
	
	/**
	 * Pega a qtde do material da lista de material do contrato.
	 *
	 * @param listaContratomaterial
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/12/2012
	 */
	public Double getQtdeMaterial(List<Contratomaterial> listaContratomaterial, Material material) {
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			if(contratomaterial.getListaContratomaterialitem() != null && contratomaterial.getListaContratomaterialitem().size() > 0){
				for (Contratomaterialitem contratomaterialitem : contratomaterial.getListaContratomaterialitem()) {
					if(contratomaterialitem.getMaterial() != null && contratomaterialitem.getMaterial().equals(material)){
						return contratomaterialitem.getQtde();
					}
				}
			} else {
				if(contratomaterial.getServico() != null && contratomaterial.getServico().equals(material)){
					return contratomaterial.getQtde().doubleValue();
				}
			}
		}
		return null;
	}
	
	/**
	 * Retorna a empresa dos contratos.
	 * Caso tenha empresas diferentes retorna null.
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2012
	 */
	public Empresa getEmpresaByContratos(String whereInContrato) {
		List<Contrato> listaContrato = this.findContratoAtualizarDados(whereInContrato);
		Empresa empresa = null;
		
		for (Contrato contrato : listaContrato) {
			if(empresa == null){
				empresa = contrato.getEmpresa();
			} else if(contrato.getEmpresa() != null && !contrato.getEmpresa().equals(empresa)){
				empresa = null;
				break;
			}
		}
		
		return empresa;
	}
	
	public Cliente getClienteByContratos(String whereInContrato) {
		List<Contrato> listaContrato = this.findContratoAtualizarDados(whereInContrato);
		Cliente cliente = null;
		
		for (Contrato contrato : listaContrato) {
			if(cliente == null){
				cliente = contrato.getCliente();
			} else if(contrato.getCliente() != null && !contrato.getCliente().equals(cliente)){
				cliente = null;
				break;
			}
		}
		
		return cliente;
	}
	
	public List<Contrato> findForRelatorioComissionamentoContrato(DocumentocomissaoFiltro filtro) {
		List<Contrato> listaContrato = contratoDAO.findForRelatorioComissionamentoContrato(filtro);
		if(listaContrato != null && !listaContrato.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ",");
			addListaContratocolaborador(contratocolaboradorService.findForEntradaByContrato(whereIn), listaContrato);
		}
		return listaContrato;
	}
	
	/**
	 * 
	 * @param filtro
	 * @return
	 */
	public String createReportMatricial(ContratoFiltro filtro) {
		
		Contrato contrato = this.findForEmissaoContratoRTF(new Contrato(Integer.parseInt(filtro.getWhereIn())));
		if(contrato != null && contrato.getEmpresa() != null){
			contrato.setEmpresa(empresaService.loadForEntrada(contrato.getEmpresa()));
		}
		if(contrato.getResponsavel() != null && contrato.getResponsavel().getCdpessoa() != null){
			contrato.setResponsavel(colaboradorService.loadForEntrada(contrato.getResponsavel()));
		}
		if(contrato.getCliente() != null && contrato.getCliente().getCdpessoa() != null){
			contrato.setCliente(clienteService.findForContratoRtf(contrato.getCliente()));
		}
		List<Contratoparcela> listaContratoparcela = contratoparcelaService.findByContrato(contrato);
		contrato.setListaContratoparcela(listaContratoparcela);
		contrato.setListaContratomaterial(contratomaterialService.findForRTFByContrato(contrato));
		contrato.setListaContratodesconto(contratodescontoService.findByContrato(contrato));
		contrato.getCliente().setListaContato(pessoaContatoService.findByPessoaContatotipo(contrato.getCliente(), null));
		if(contrato.getVendedortipo() != null && Vendedortipo.COLABORADOR.equals(contrato.getVendedortipo()) && 
				contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null){
			contrato.setColaborador(colaboradorService.load(new Colaborador(contrato.getVendedor().getCdpessoa()), "colaborador.cdpessoa, colaborador.nome, colaborador.rg"));
		}
		
		Cliente cliente = contrato.getCliente();
		
		Endereco enderecoContratoCliente = contrato.getEndereco();
		Endereco enderecoEntregaCliente = cliente.getEnderecoWithPrioridade(Enderecotipo.ENTREGA);
		Telefone telefoneCliente = cliente.getTelefone();
		
		Boolean ignoravalormaterial = contrato.getIgnoravalormaterial();
		contrato.setIgnoravalormaterial(Boolean.FALSE);
		Money valorServicos = contrato.getValorServicos();
		contrato.setIgnoravalormaterial(ignoravalormaterial);
		
		StringBuilder relatorioMatricial = new StringBuilder();
		
		String dataAtual = SinedDateUtils.toString(SinedDateUtils.currentDate());
		
		String contratoDataInicio = contrato.getDtinicio() != null ? SinedDateUtils.toString(contrato.getDtinicio(), "dd/MM/yy") : "";
		String contratoDataVencimento = contrato.getDtinicio() != null ? SinedDateUtils.toString(contrato.getDtinicio(), "dd/MM/yy") : "";
		String contratoPeriodoLocacao = contratoDataInicio + "-" + contratoDataVencimento;
		String contratoValorLocacao = contrato.getValor() != null ? contrato.getValor().toString() : "";
		String contratoIndiceCorrecao = contrato.getIndicecorrecao() != null && contrato.getIndicecorrecao().getDescricao() != null ? contrato.getIndicecorrecao().getDescricao() : "";
		String contratoContato = contrato.getContato() != null && contrato.getContato().getNome() != null ? contrato.getContato().getNome() : "";
		String contratoValorExtenso = contrato.getValor() != null ? new Extenso(contrato.getValor()).toString() : "";
		String contratoValorServicos = valorServicos != null ? valorServicos.toString() : "";
		String contratoValorServicosExtenso = valorServicos != null ? new Extenso(valorServicos).toString() : "";
		String contratoResponsavel = contrato.getResponsavel() != null && contrato.getResponsavel().getNome() != null ? contrato.getResponsavel().getNome() : "";
		
		String clienteTelefone = telefoneCliente != null && telefoneCliente.getTelefone() != null ? telefoneCliente.getTelefone() : "";
		
		String clienteEnderecoEntregaLogradouro = enderecoEntregaCliente != null && enderecoEntregaCliente.getLogradouro() != null ? enderecoEntregaCliente.getLogradouro() : "";
		String clienteEnderecoEntregaNumero = enderecoEntregaCliente != null && enderecoEntregaCliente.getNumero() != null ? enderecoEntregaCliente.getNumero() : "";
		String clienteEnderecoEntregaComplemento = enderecoEntregaCliente != null && enderecoEntregaCliente.getComplemento() != null ? enderecoEntregaCliente.getComplemento() : "";
		String clienteEnderecoEntregaBairro = enderecoEntregaCliente != null && enderecoEntregaCliente.getBairro() != null ? enderecoEntregaCliente.getBairro() : "";
		String clienteEnderecoEntregaCidade = enderecoEntregaCliente != null && enderecoEntregaCliente.getMunicipio() != null && enderecoEntregaCliente.getMunicipio().getNome() != null ? enderecoEntregaCliente.getMunicipio().getNome() : "";
		String clienteEnderecoEntregaUf = enderecoEntregaCliente != null && enderecoEntregaCliente.getMunicipio() != null && enderecoEntregaCliente.getMunicipio().getUf() != null && enderecoEntregaCliente.getMunicipio().getUf().getSigla() != null ? enderecoEntregaCliente.getMunicipio().getUf().getSigla() : "";
		
		String clienteEnderecoContratoLogradouro = enderecoContratoCliente != null && enderecoContratoCliente.getLogradouro() != null ? enderecoContratoCliente.getLogradouro() : "";
		String clienteEnderecoContratoNumero = enderecoContratoCliente != null && enderecoContratoCliente.getNumero() != null ? enderecoContratoCliente.getNumero() : "";
		String clienteEnderecoContratoBairro = enderecoContratoCliente != null && enderecoContratoCliente.getBairro() != null ? enderecoContratoCliente.getBairro() : "";
		String clienteEnderecoContratoCidade = enderecoContratoCliente != null && enderecoContratoCliente.getMunicipio() != null && enderecoContratoCliente.getMunicipio().getNome() != null ? enderecoContratoCliente.getMunicipio().getNome() : "";
		String clienteEnderecoContratoUf = enderecoContratoCliente != null && enderecoContratoCliente.getMunicipio() != null && enderecoContratoCliente.getMunicipio().getUf() != null && enderecoContratoCliente.getMunicipio().getUf().getSigla() != null ? enderecoContratoCliente.getMunicipio().getUf().getSigla() : "";
		String clienteEnderecoContratoCep = enderecoContratoCliente != null && enderecoContratoCliente.getCep() != null ? enderecoContratoCliente.getCep().toString() : "";
		
//		LINHA 1 - 9
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 9);
		
//		LINHA 10 - 11
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataInicio, 10, ' ', true); // DATA DE IN�CIO DO CONTRATO
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoPeriodoLocacao, 16, ' ', true); // PER�ODO DE LOCA��O DO CONTRATO
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorLocacao, 17, ' ', false); // VALOR DE LOCA��O DO CONTRATO
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoIndiceCorrecao, 9, ' ', true); // IND�CE DE CORRE��O DO CONTRATO
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 12);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoContato, 9, ' ', true); // CONTATO DO CONTRATO
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 12 - 13
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorExtenso, 65, ' ', true); // VALOR POR EXTENSO DO CONTRATO
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteTelefone, 14, ' ', true); // TELEFONE DO CLIENTE
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 14 - 17
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaLogradouro, 29, ' ', true); // LOGRADOURO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaNumero, 5, ' ', true); // N�MERO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaComplemento, 6, ' ', true); // COMPLEMENTO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteTelefone, 10, ' ', true); // TELEFONE DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaBairro, 11, ' ', true); // BAIRRO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaCidade, 13, ' ', true); // CIDADE DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoEntregaUf, 2, ' ', true); // UF DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4);
		
//		LINHA 18 - 38 - CORPO
		
		List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
		
		for (int i = 0; i < 21; i++) {
			if(listaContratomaterial.size() <= i) {
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
				continue;
			}
			Contratomaterial contratomaterial = listaContratomaterial.get(i);
			Money valortotalMoney = contratomaterial.getValortotal();
			
			String qtde = contratomaterial.getQtde() != null ?  contratomaterial.getQtde().toString() : "";
			String materialNome = contratomaterial.getServico() != null && contratomaterial.getServico().getNome() != null ? contratomaterial.getServico().getNome() : "";
			String valorUnitario = contratomaterial.getValorunitario() != null ? SinedUtil.descriptionDecimal(contratomaterial.getValorunitario(), true) : "";
			String valorTotal = valortotalMoney != null ? valortotalMoney.toString() : "";
			
			if(contratomaterial.getPatrimonioitem() != null && contratomaterial.getPatrimonioitem().getPlaqueta() != null){
				materialNome += " - " + contratomaterial.getPatrimonioitem().getPlaqueta();
			}
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, qtde, 4, ' ', true);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, materialNome, 32, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, valorUnitario, 10, ' ', false);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, valorTotal, 11, ' ', false);
			
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
		}
		
//		LINHA 39 - 43
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorServicosExtenso, 53, ' ', true); // VALOR DOS SERVI�OS POR EXTENSO DO CONTRATO
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 12);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorServicos, 15, ' ', false); // VALOR DOS SERVI�OS DO CONTRATO
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 5);
		
//		LINHA 44
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 9);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoLogradouro, 45, ' ', true); // LOGRADOURO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoNumero, 9, ' ', true); // N�MERO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoBairro, 12, ' ', true); // BAIRRO DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
		
//		LINHA 45 - 55
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoCidade, 29, ' ', true); // CIDADE DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 6);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoUf, 4, ' ', true); // UF DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoContratoCep, 9, ' ', true); // CEP DO ENDERE�O DO CLIENTE
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 2);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteTelefone, 20, ' ', true); // TELEFONE DO CLIENTE
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 11);
		
//		LINHA 56 - 57
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoResponsavel, 35, ' ', true); // RESPONS�VEL DO CONTRATO
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoContato, 35, ' ', true); // CONTATO DO CONTRATO
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 58
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, dataAtual, 10, ' ', true); // RESPONS�VEL DO CONTRATO
		
		return relatorioMatricial.toString();
		
		
	}	
	
	public List<Contrato> findByClienteForProducaoAgenda(Cliente cliente) {
		return contratoDAO.findByClienteForProducaoAgenda(cliente);
	}
	
	/**
	 * Gera faturamento personalizado a partir de um contrato.
	 * 
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public Documento gerarFaturamentoWebService(GerarFaturamentoBean bean) throws Exception{
		Contrato contrato = null;
		if(bean.getTipoidentificador() != null && bean.getTipoidentificador().equals(Tipoidentificador.CDCONTRATO)){
			List<Contrato> lista = contratoService.findForCobranca(bean.getCdcontrato().toString());
			if(lista != null && lista.size() > 0) 
				contrato = lista.get(0);
		} else {
			contrato = contratoService.findForCobrancaByIdentificador(bean.getCdcontrato().toString());
		}
		if(contrato == null)
			throw new SinedException("Nenhum contrato encontrado.");
		
		
		Documento documento = this.geraContareceberFaturamentoWebService(contrato, bean);
		documento = this.salvaContareceberWebService(documento);
		
		if(bean.getCriarnota() != null && bean.getCriarnota()){
			faturamentohistoricoService.realizarFaturamentoServicoDocumento(null, documento.getCddocumento() + "", true, true);
		}
		
		this.gerarComissaoFaturamentoWebService(documento, bean);
		
		return  documento; 
	}
	
	/**
	 * M�todo que gera um documento de comiss�o caso o cpf fornecido via webservice sej� v�lido.
	 * 
	 * @param documento
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void gerarComissaoFaturamentoWebService(Documento documento, GerarFaturamentoBean bean){
		try {
			if(bean.getCpf() != null && !bean.getCpf().trim().equals("")){
				Cpf cpf = new Cpf(bean.getCpf());

				if(Cpf.cpfValido(cpf.getValue())){
					Colaborador colaborador = colaboradorService.findByCpf(cpf);

					if(colaborador != null){
						documentocomissaoService.gerarComissaoFaturamentoWebService(documento, colaborador);
					}
				}
			}
		} catch (Exception e) {
//			System.out.println("Falha ao gerar documento de comiss�o.");
			e.printStackTrace();
		}
	}
	
	/**
	 * M�todo que gera uma conta a receber para faturamento via WebService
	 * 
	 * @param contrato
	 * @param bean
	 * @param documentotipo
	 * @return
	 * @author Rafael Salvio
	 */
	private Documento geraContareceberFaturamentoWebService(Contrato contrato, GerarFaturamentoBean bean) throws Exception{
		try{
			String param = parametrogeralService.getValorPorNome("FATURAMENTO_WEBSERVICE");
			if(param == null || param.trim().equals("")){
				throw new Exception("O parametro 'FATURAMENTO_WEBSERVICE' que referencia o tipo de documento para faturamento por WebService n�o foi definido.");
			}
			
			Documentotipo documentotipo = documentotipoService.load(new Documentotipo(Integer.parseInt(param)));
			if(documentotipo == null)
				throw new Exception("O parametro 'FATURAMENTO_WEBSERVICE' que referencia o tipo de documento para faturamento por WebService n�o foi definido.");
			
			if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
				contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			}
			if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
				contrato.setTaxa(taxaService.findTaxa(contrato.getTaxa()));
			}
			
			Date dtvencimento = bean.getDtvencimento();
			if(dtvencimento == null){
				dtvencimento = SinedDateUtils.currentDate();
			}
			
			Documento documento = new Documento();
			documento.setContrato(contrato);
			documento.setDocumentotipo(documentotipo);
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
			documento.setReferencia(Referencia.MES_ANO_CORRENTE);
			documento.setDtemissao(SinedDateUtils.currentDate());
			documento.setDtvencimento(this.getDataVencimento(contrato, dtvencimento));
			documento.setEmpresa(contrato.getEmpresa());
			documento.setConta(contrato.getConta());
			documento.setNumero(bean.getDocumento());
			
			if(bean.getDescricao() == null){
				bean.setDescricao("");
			}else if(bean.getDescricao().length() > 150){
				bean.setDescricao(bean.getDescricao().substring(0, 149));
			}
			documento.setDescricao(bean.getDescricao());
			
			if(contrato.getConta() != null){
				Conta conta = contaService.carregaConta(contrato.getConta());
				
				//Preenche a carteira do documento com a carteira padr�o
				documento.setContacarteira(conta.getContacarteira());
				
				if (conta.getBanco() != null && conta.getBanco().getNumero() != null && conta.getBanco().getNumero() == 341){ //Ita�
					if (conta.getNossonumerointervalo() != null && conta.getNossonumerointervalo() && documento.getNossonumero() == null)
						documento.setNossonumero(StringUtils.stringCheia(contareceberService.getNextNossoNumero(conta).toString(), "0", 8, false));				
				}			
			}
			if(bean.getValor() == null)
				throw new Exception("Valor para faturamento inv�lido.");
			
			documento.setValor(indicecorrecaoService.calculaIndiceCorrecao(contrato.getIndicecorrecao(), new Money(bean.getValor()), documento.getDtvencimento(), contrato.getDtinicio()));
			
			if(contrato.getIndicecorrecao() != null)
				documento.setIndicecorrecao(contrato.getIndicecorrecao());
			if(contrato.getEndereco() != null){
				documento.setEndereco(contrato.getEndereco());
			} else {
				if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
					documento.setEndereco(contrato.getCliente().getEndereco());
				}
			}
			
			if(contrato.getNomefantasia() == null || contrato.getNomefantasia().equals("")){
				documento.setPessoa(contrato.getCliente());
				documento.setCliente(contrato.getCliente());
				documento.setTipopagamento(Tipopagamento.CLIENTE);
			}else{
				documento.setTipopagamento(Tipopagamento.OUTROS);
				documento.setOutrospagamento(contrato.getNomefantasia());
			}
			
			List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
			documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
			documento.setAcaohistorico(Documentoacao.PREVISTA);
			
			Rateio rateio = new Rateio();
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			
			Taxa taxa = new Taxa();
			List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
			
			if(contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null){
				listaRateioitem.addAll(contrato.getRateio().getListaRateioitem());
				for (Rateioitem ri : listaRateioitem) {
					ri.setCdrateioitem(null);
					ri.setRateio(null);
				}
			}
				
			if(contrato.getTaxa() != null && contrato.getTaxa().getListaTaxaitem() != null){
				listaTaxaitem.addAll(contrato.getTaxa().getListaTaxaitem());
				for (Taxaitem ti : listaTaxaitem) {
					ti.setCdtaxaitem(null);
				}
			}
			
			rateioitemService.agruparItensRateio(listaRateioitem);
			rateio.setListaRateioitem(listaRateioitem);
			rateioService.atualizaValorRateio(rateio, documento.getValor());
			
			// FEITO ISSO PARA AJUSTAR O RATEIO
			BigDecimal somaItens = new BigDecimal(0);
			for (Rateioitem ri : listaRateioitem) {
				somaItens = somaItens.add(SinedUtil.roundFloor(ri.getValor().getValue(), 2));			
			}
			BigDecimal diferenca = documento.getValor().getValue().subtract(somaItens);
			if(diferenca.compareTo(BigDecimal.ZERO) != 0){
				listaRateioitem.get(0).setValor(listaRateioitem.get(0).getValor().add(new Money(diferenca)));
			}
			
			String msg;
			String msg1 = "";
			String msg2 = "";
			
			for (Taxaitem taxaitem : listaTaxaitem) {
				taxaitem.setDtlimite(documento.getDtvencimento());
				
				taxaitem.setGerarmensagem(true);
				
				msg = 	taxaitem.getTipotaxa().getNome() + 
						" de " + 
						(taxaitem.isPercentual() ? "" : "R$") + 
						taxaitem.getValor().toString() + 
						(taxaitem.isPercentual() ? "%" : "") + 
						ateApos(taxaitem.getTipotaxa().getCdtipotaxa())+
						SinedDateUtils.toString(documento.getDtvencimento()) +
						". ";
				
				if((msg1+msg).length() <= 80){
					msg1 += msg;
				} else if((msg2+msg).length() <= 80){
					msg2 += msg;
				}
			}
			
			documento.setMensagem2(msg1);
			documento.setMensagem3(msg2);
			taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
			
			if(bean.getMensagem1() != null && !bean.getMensagem1().trim().equals("")){
				documento.setMensagem1(bean.getMensagem1());
			}
			if(bean.getMensagem2() != null && !bean.getMensagem2().trim().equals("")){
				documento.setMensagem2(bean.getMensagem2());
			}
			if(bean.getMensagem3() != null && !bean.getMensagem3().trim().equals("")){
				documento.setMensagem3(bean.getMensagem3());
			}
			
			documento.setRateio(rateio);
			documento.setTaxa(taxa);
			
			return documento;
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(!e.getMessage().isEmpty() ? e.getMessage() : "Erro ao gerar nova conta a receber.");
		}
	}
	
	public Date getDataVencimento(Contrato contrato, Date dtvencimento) {
		if(contrato.getFrequencia() != null){
			if(contrato.getDataparceladiautil() != null && contrato.getDataparceladiautil()){
				dtvencimento = SinedDateUtils.getProximaDataUtil(dtvencimento, contrato.getEmpresa(), contrato.getDataparcelaultimodiautilmes());
			}
		}else {
			dtvencimento = SinedDateUtils.getProximaDataUtil(dtvencimento, contrato.getEmpresa());
		}
		
		return dtvencimento;
	}
	
	public Date getDtVencimento(List<NotaContrato> listaNotascontrato) {
		Date dtVencimento = null;
		Date dtEmissao = null;
		
		for (NotaContrato notaContrato : listaNotascontrato) {
			if(notaContrato.getNota() instanceof NotaFiscalServico)
				dtVencimento = ((NotaFiscalServico)notaContrato.getNota()).getDtVencimento();
			if(notaContrato.getNota() instanceof Notafiscalproduto)
				dtEmissao = ((Notafiscalproduto)notaContrato.getNota()).getDtEmissao();
		}
		return dtVencimento != null ? dtVencimento : dtEmissao;
	}
	
	/**
	 * M�todo respons�vel por salvar a nova conta gerada pelo faturamento via webService assim como os hist�ricos necess�rios.
	 * 
	 * @param documento
	 * @return
	 * @author Rafael Salvio
	 */
	public Documento salvaContareceberWebService(Documento documento) throws Exception{
		try{
			Contrato contrato = documento.getContrato();
			
			documentoService.saveOrUpdate(documento);
			if(contrato != null && contrato.getCdcontrato() != null && documento.getCddocumento() != null){
				this.salvaDocumentoorigemContrato(documento);
			}
			contrato.setDocumento(documento);
			
			Contratohistorico historico = new Contratohistorico();
			historico.setAcao(Contratoacao.COSOLIDADO);
			historico.setContrato(documento.getContrato());
			historico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao("Gerada a conta <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>.");
			contratohistoricoService.saveOrUpdate(historico);
			
			Documentohistorico documentohistorico = this.criaDocumentoHistoricoFaturamentoContrato(documento, contrato.getCdcontrato());
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			faturamentohistoricoService.saveHistoricoFaturamento(contrato, documento);
		
			return documento;
		}catch (Exception e) {
			throw new Exception("Erro ao salvar a conta a receber.");
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContratoService#loadForFaturarlocacao(String whereIn)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> loadForFaturarlocacao(String whereIn) {
		return contratoDAO.loadForFaturarlocacao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findContratoWithRateioProjeto(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findContratoWithRateioProjeto(String whereIn) {
		return contratoDAO.findContratoWithRateioProjeto(whereIn);
	}	
	
	public Contrato loadContratoWithRateioProjeto(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		List<Contrato> lista = contratoDAO.findContratoWithRateioProjeto(contrato.getCdcontrato().toString());
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForRomaneioreport(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForRomaneioreport(String whereIn) {
		return contratoDAO.findForRomaneioreport(whereIn);
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public void enviaEmailResponsavelContratoRenovacao() {
		
		List<Contrato> listaContrato = contratoDAO.findForEnviarEmailResponsavelContrato();
		StringBuilder html;
		
		for (Contrato contrato: listaContrato){
			if (GenericValidator.isEmail(contrato.getResponsavel().getEmail())){
				html = new StringBuilder();
				html.append("Contrato abaixo para ser renovado:");
				html.append("<br><br>");
				html.append("Contrato: " + contrato.getCdcontrato());
				html.append("<br>");
				html.append("Identificador: " + Util.strings.emptyIfNull(contrato.getIdentificador()));
				html.append("<br>");
				html.append("Empresa: " + Util.strings.emptyIfNull(contrato.getEmpresa().getRazaosocialOuNome()));
				html.append("<br>");
				html.append("Cliente: " + Util.strings.emptyIfNull(contrato.getCliente().getNome()));
				html.append("<br>");
				html.append("Data de Renova��o: " + new SimpleDateFormat("dd/MM/yyyy").format(contrato.getDtrenovacao()));
				try {
					new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
						.setFrom("w3erp@w3erp.com.br")
						.setSubject("W3erp - Renova��o de Contrato")
						.setTo(contrato.getResponsavel().getEmail())
						.addHtmlText(html.toString())
						.sendMessage();				
				} catch (Exception e) {
//					System.out.println("E-mail n�o enviado.");
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContratoService#findForLancardesconto(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForLancardesconto(String whereIn) {
		return contratoDAO.findForLancardesconto(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO.findForValidacaoAgrupamentoCliente(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2013
	 */
	public List<Contrato> findForValidacaoAgrupamentoCliente(String whereIn) {
		return contratoDAO.findForValidacaoAgrupamentoCliente(whereIn);
	}
	
	public void calculaValorFaturaLocacao(Contrato contrato) {
		Double totallocado = 0.0;
		Double totaldevolvido = 0.0;
		Date dataatual = new Date(System.currentTimeMillis());
		Integer diferencadias = 0;
		boolean faturamentoporitemdevolvido = contrato.getContratotipo() != null && Boolean.TRUE.equals(contrato.getContratotipo().getFaturamentoporitensdevolvidos());
		
		if(contrato.getIgnoravalormaterial() != null && contrato.getIgnoravalormaterial()){
			Money valorContrato = contrato.getValorContratoSemDescontoAba();
			Double valorContratoDbl = valorContrato != null ? valorContrato.getValue().doubleValue() : 0d;
			
			totallocado = valorContratoDbl;
			
			if(faturamentoporitemdevolvido && contrato.getDtiniciolocacao() != null){
				recalculaValorunitarioByValortotalfechado(contrato);
				for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
					Double saldoQtde = 0d;
					Contratomateriallocacao ultimaDevolucao = null;
					Contratomateriallocacao ultimoEnvio = null;
					if(contratomaterial.getListaContratomateriallocacao() != null && !contratomaterial.getListaContratomateriallocacao().isEmpty()){
						for(Contratomateriallocacao contratomateriallocacao : contratomaterial.getListaContratomateriallocacao()){
							if(contratomaterial.getValorfechado() != null && contratomateriallocacao.getQtde() != null && contratomateriallocacao.getQtde() > 0 && contratomateriallocacao.getDtmovimentacao() != null &&
									!Boolean.TRUE.equals(contratomateriallocacao.getSubstituicao())){
								if(Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
									if(SinedDateUtils.beforeIgnoreHour(contratomateriallocacao.getDtmovimentacao(), contrato.getDtiniciolocacao())){
										saldoQtde -= contratomateriallocacao.getQtde();
										if(ultimaDevolucao == null){
											ultimaDevolucao = contratomateriallocacao; 
										}else if(SinedDateUtils.afterIgnoreHour(contratomateriallocacao.getDtmovimentacao(), ultimaDevolucao.getDtmovimentacao()) ||
												(SinedDateUtils.equalsIgnoreHour(contratomateriallocacao.getDtmovimentacao(), ultimaDevolucao.getDtmovimentacao())) && 
												contratomateriallocacao.getCdcontratomateriallocacao() > ultimaDevolucao.getCdcontratomateriallocacao()){
											ultimaDevolucao = contratomateriallocacao;
										}
									}
								}else if(Contratomateriallocacaotipo.SAIDA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
									saldoQtde += contratomateriallocacao.getQtde();
									if(ultimoEnvio == null){
										ultimoEnvio = contratomateriallocacao; 
									}else if(SinedDateUtils.afterIgnoreHour(contratomateriallocacao.getDtmovimentacao(), ultimoEnvio.getDtmovimentacao()) ||
											(SinedDateUtils.equalsIgnoreHour(contratomateriallocacao.getDtmovimentacao(), ultimoEnvio.getDtmovimentacao())) && 
											contratomateriallocacao.getCdcontratomateriallocacao() > ultimoEnvio.getCdcontratomateriallocacao()){
										ultimoEnvio = contratomateriallocacao;
									}
								}
							}
						}
					}

					if((ultimaDevolucao != null && SinedDateUtils.beforeIgnoreHour(ultimaDevolucao.getDtmovimentacao(), contrato.getDtiniciolocacao()) && 
							contratomaterial.getValorfechado() != null)){
						if(saldoQtde <= 0){
							totaldevolvido += contratomaterial.getValorfechado() * (contratomaterial.getQtde() != null ? contratomaterial.getQtde() : 1d);  
						}else if(contratomaterial.getQtde() != null){
							Double qtdeDevolvida = contratomaterial.getQtde() - saldoQtde;
							if(qtdeDevolvida > 0){
								totaldevolvido += contratomaterial.getValorfechado() * qtdeDevolvida;
							}
						}
					}
					
					if(ultimoEnvio == null || (ultimaDevolucao == null || 
							SinedDateUtils.afterOrEqualsIgnoreHour(ultimaDevolucao.getDtmovimentacao(), contrato.getDtiniciolocacao()) ||
							SinedDateUtils.afterIgnoreHour(ultimoEnvio.getDtmovimentacao(), ultimaDevolucao.getDtmovimentacao())) ||
							(SinedDateUtils.equalsIgnoreHour(ultimoEnvio.getDtmovimentacao(), ultimaDevolucao.getDtmovimentacao()) && 
								ultimoEnvio.getCdcontratomateriallocacao() > ultimaDevolucao.getCdcontratomateriallocacao())){
						contratomaterial.setConsiderarQtdeTotalItem(Boolean.TRUE);
					}
					
				}
			}
			
//			C�DIGO COMENTADO TEMPORIARIAMENTE
//			
//			Double qtdeMateriasDevolvidos = 0d;
//			Integer qtdeMateriaisPrevistos = 0;
//			for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
//				if(contratomaterial.getQtde() != null){
//					qtdeMateriaisPrevistos += contratomaterial.getQtde();
//				}
//				
//				if(contratomaterial.getListaContratomateriallocacao() != null && !contratomaterial.getListaContratomateriallocacao().isEmpty()){
//					for(Contratomateriallocacao contratomateriallocacao : contratomaterial.getListaContratomateriallocacao()){
//						if(contratomateriallocacao.getQtde() != null && 
//								Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
//							qtdeMateriasDevolvidos += contratomateriallocacao.getQtde();
//						}
//					}
//				}
//			}
//			
//			if(qtdeMateriasDevolvidos > 0 && qtdeMateriaisPrevistos > 0){
//				totaldevolvido = (valorContratoDbl/new Double(qtdeMateriaisPrevistos)) * qtdeMateriasDevolvidos;
//			}
			
		} else {
			boolean existParcela = false;
			if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
				Contratoparcela contratoparcela = this.getParcelaAtual(contrato.getListaContratoparcela());
				
				if(contratoparcela != null){
					existParcela = true;
					totaldevolvido = 0d;
					if(contratoparcela.getValor() != null){
						totallocado = contratoparcela.getValor().getValue().doubleValue();
					}
				} 
			} 
			
			if(!existParcela){
				for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
					if(contratomaterial.getListaContratomateriallocacao() != null && !contratomaterial.getListaContratomateriallocacao().isEmpty()){
						for(Contratomateriallocacao contratomateriallocacao : contratomaterial.getListaContratomateriallocacao()){
							if(contratomaterial.getValorfechado() != null && !Boolean.TRUE.equals(contratomateriallocacao.getSubstituicao())){
								diferencadias = SinedDateUtils.diferencaDias(dataatual,contratomateriallocacao.getDtmovimentacao());
								if(Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
									totaldevolvido += diferencadias * ((contratomateriallocacao.getQtde() * contratomaterial.getValorfechado()) / 30);
								}else if(Contratomateriallocacaotipo.SAIDA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
									totallocado += diferencadias * ((contratomateriallocacao.getQtde() * contratomaterial.getValorfechado()) / 30);
								}
							}
						}
					}
				}
			}
		}
		
		Money desconto = contrato.getValorTotalDesconto();
		
		contrato.setFaturalocacaoTotallocado(totallocado != null ? new Money(totallocado) : new Money());
		contrato.setFaturalocacaoTotaldevolvido(totaldevolvido != null ? new Money(totaldevolvido) : new Money());
		contrato.setFaturalocacaoTotalacrescimo(new Money());
		contrato.setFaturalocacaoTotaldesconto(desconto != null ? desconto : new Money());
		contrato.setFaturalocacaoValorfaturar(contrato.getFaturalocacaoValorfaturar());
	}
	
	private void calculaTotalCheio(Contrato contrato){
		Double valortotalcheio = 0d;
		if(SinedUtil.isListNotEmpty(contrato.getListaContratomaterial())){
			Double qtde;
			Double valorunitario;
			for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
				qtde = contratomaterial.getQtde();
				valorunitario = contratomaterial.getValorunitario();
				if(qtde != null && valorunitario != null){
					valortotalcheio += (valorunitario * qtde);
				}
			}
		}
		contrato.setValortotalcheio(new Money(valortotalcheio));
	}
	
	private void calculaAliquotatotalvalorfechado(Contrato contrato){
		calculaTotalCheio(contrato);
		if(contrato.getValortotalfechado() != null && contrato.getValortotalcheio().getValue().doubleValue() > 0){
			contrato.setAliquotavalortotalfechado(SinedUtil.round(contrato.getValortotalfechado().getValue().doubleValue()/contrato.getValortotalcheio().getValue().doubleValue(), 6));
		}
	}
	
	private void removeDescontoForValorFechado(Contrato contrato){
		if(SinedUtil.isListNotEmpty(contrato.getListaContratomaterial())){
			for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
				contratomaterial.setValordesconto(new Money());
			}
		}
	}
	
	private void recalculaValorunitarioByValortotalfechado(Contrato contrato){
		contrato.setValortotalfechado(contrato.getValorContrato());
		removeDescontoForValorFechado(contrato);
		calculaAliquotatotalvalorfechado(contrato);
		
		Double aliquotavalortotalfechado = contrato.getAliquotavalortotalfechado();
		Money valortotalfechado = contrato.getValortotalfechado();
		if(aliquotavalortotalfechado != null && valortotalfechado != null && SinedUtil.isListNotEmpty(contrato.getListaContratomaterial())){
			Double qtde;
			Double valorfechado;
			Double valorunitario;
			Double desconto;
			for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
				qtde = contratomaterial.getQtde();
				valorunitario = contratomaterial.getValorunitario();
				if(qtde != null && valorunitario != null){
					valorfechado = valorunitario * aliquotavalortotalfechado;
					desconto = 0d;
					if(valorfechado < valorunitario){
						desconto = (valorunitario - valorfechado) * qtde;
					}
					contratomaterial.setValorfechado(valorfechado);
					contratomaterial.setValordesconto(desconto != null ? new Money(desconto) : new Money());
					contratomaterial.setValorfechadoCalculado(valorfechado);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findWithIdentificador(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/10/2013
	 */
	public List<Contrato> findWithIdentificador(String whereIn) {
		return contratoDAO.findWithIdentificador(whereIn);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarConfirmarContratoLocacao(String whereIn){
		return contratoDAO.verificarConfirmarContratoLocacao(whereIn);
	}

	public Date getDtinicioFaturalocacao(Contrato c) {
		Date dtinicio = c.getDtinicio();
		Date dtFim = new Date(System.currentTimeMillis());
		if(c.getDtrenovacao() != null){
			dtFim = c.getDtrenovacao();
		}
		if(c.getFrequencia() != null && c.getFrequencia().getCdfrequencia() != null){
			Calendar novaDtProximo = SinedDateUtils.javaSqlDateToCalendar(dtFim);
			Integer frequencia = c.getFrequencia().getCdfrequencia();
			
			switch (frequencia) {
			
				case 1: //UNICA
					break;
					
				case 2: //DIARIA
					dtinicio = SinedDateUtils.addDiasData(dtFim, -1);
					break;
					
				case 3: //SEMANAL
					dtinicio = SinedDateUtils.addDiasData(dtFim, -7);
					break;
					
				case 4: //QUINZENAL
					dtinicio = SinedDateUtils.addDiasData(dtFim, -15);
					break;
					
				case 5: //MENSAL
					dtinicio = SinedDateUtils.addMesData(dtFim, -1);
					break;
					
				case 6: // ANUAL
					novaDtProximo.add(Calendar.YEAR, -1);
					dtinicio = new Date(novaDtProximo.getTime().getTime());
					break;
					
				case 7: //SEMESTRAL
					novaDtProximo.add(Calendar.MONTH, -6);
					dtinicio = new Date(novaDtProximo.getTime().getTime());
					break;
					
				case 9: //TRIMESTE
					novaDtProximo.add(Calendar.MONTH, -3);
					dtinicio = new Date(novaDtProximo.getTime().getTime());
					break;
			}
			
		}
		
		return dtinicio;
	}
	
	/**
	 * 
	 * @param contrato
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isLocacao(Contrato contrato){
		return contratoDAO.isLocacao(contrato);
	}
	
	public Double getValorvendaByTabela(Double valorvenda, Material material, Cliente cliente, Frequencia frequencia, Empresa empresa) {
		Double valor = valorvenda;
		String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
		Integer numCasasDecimais = null; 
		if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
			String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			if(aux != null && !aux.trim().isEmpty()){
				try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
			}
		}
		if(material != null && material.getCdmaterial() != null){
			Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtualForContrato(material, cliente, frequencia, empresa);
			if(materialtabelapreco != null){
				valor = materialtabelapreco.getValorComDescontoAcrescimo(valor, numCasasDecimais);
			}else {
				List<Categoria> listaCategoria = null;
				if(cliente != null && cliente.getCdpessoa() != null){
					List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
					if(listaPessoacategoria != null && !listaPessoacategoria.isEmpty()){
						listaCategoria = new ArrayList<Categoria>();
						for(Pessoacategoria pessoacategoria : listaPessoacategoria){
							if(pessoacategoria.getCategoria() != null && pessoacategoria.getCdpessoacategoria() != null){
								listaCategoria.add(pessoacategoria.getCategoria());
							}
						}
					}
				}
				Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getMaterialtabelaprecoitemForContrato(material, cliente, listaCategoria, frequencia, empresa);
				if(materialtabelaprecoitem != null){
					Double fatorconversao = null;
					if(materialtabelaprecoitem.getUnidademedida() != null){
						fatorconversao = materialService.getFatorConversao(material, materialtabelaprecoitem.getUnidademedida());
					}
					if(fatorconversao != null){
						valorvenda = materialtabelaprecoitem.getValorcomtaxa(valor, numCasasDecimais) * fatorconversao;
					}else {
						valorvenda = materialtabelaprecoitem.getValorcomtaxa(valor, numCasasDecimais);
					}
				}
				
			}
		}
		return valorvenda;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#isMunicipioBrasiliaAndNotContratomaterial(String whereInContrato)
	 *
	 * @param whereInContrato
	 * @return
	 * @author Luiz Fernando
	 * @since 27/11/2013
	 */
	public Boolean isMunicipioBrasiliaAndNotContratomaterial(String whereInContrato) {
		return contratoDAO.isMunicipioBrasiliaAndNotContratomaterial(whereInContrato);
	}
	
	public Money getValorContratoForFaturarHoraTrabalhada(Contrato contrato, Money valorParcela){
		Money valor = null;
		if(contrato != null && contrato.getCdcontrato() != null && contrato.getQtde() != null && contrato.getValor() != null &&
				contrato.getContratotipo() != null && contrato.getContratotipo().getFaturarhoratrabalhada() != null &&
				contrato.getContratotipo().getFaturarhoratrabalhada()){
			if(contrato.getValorhoraextra() != null && contrato.getValorhoraextra().getValue().doubleValue() > 0){
				Double qtdeHoras = apontamentoService.getTotalHoraByContrato(contrato);
				if(qtdeHoras != null && qtdeHoras > 0){
					if(qtdeHoras > contrato.getQtde()){
						Double valorTotalHora = (qtdeHoras - contrato.getQtde())*contrato.getValorhoraextra().getValue().doubleValue(); 
						Double valorTotalContrato = contrato.getQtde() * contrato.getValor().getValue().doubleValue();
						
						if(valorParcela != null && valorParcela.getValue().doubleValue() > 0){
							valor = valorParcela.add(new Money(valorTotalHora));
						}else {
							valor = new Money(valorTotalHora + valorTotalContrato);
						}
					}
				}
			}
			
		}
		return valor;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#findForAcompanhamentometa(EmitiracompanhamentometaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public List<Contrato> findForAcompanhamentometa(EmitiracompanhamentometaFiltro filtro) {
		return contratoDAO.findForAcompanhamentometa(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO.loadWithContratotipo(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public Contrato loadWithContratotipo(Contrato contrato){
		return contratoDAO.loadWithContratotipo(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @param
	 * @return
	 * @author Lucas Costa
	 */
	public  List<Contrato> findContratoForAutocomplete(String param) {
		return contratoDAO.findContratoForAutocomplete(param);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoDAO#existsIdentificadorEmpresa(Contrato contrato, String identificador, Empresa empresa)
	 *
	 * @param contrato
	 * @param identificador
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public boolean existsIdentificadorEmpresa(Contrato contrato, String identificador, Empresa empresa){
		return contratoDAO.existsIdentificadorEmpresa(contrato, identificador, empresa);
	}
	
	/**
	 * M�todo que faz a atualiza��o autom�tica do per�odo de loca��o.
	 *
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	public void atualizaPeriodoLocacaoAutomatico() {
		int count = 0;
		while(count < 30){
			count++;
			List<Vcontratoqtdelocacao> lista = vcontratoqtdelocacaoService.findForAtualizacao();
			if(lista != null && !lista.isEmpty()){
				this.atualizaPeriodoLocacaoByContrato(CollectionsUtil.listAndConcatenate(lista, "cdcontrato", ","));
			}else {
				break;
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInContrato
	 * @author Rodrigo Freitas
	 * @since 05/06/2014
	 */
	public void atualizaPeriodoLocacaoByContrato(String whereInContrato) {
		List<Contrato> listaContrato = this.findForAtualizacaoPeriodoLocacao(whereInContrato);
		for (Contrato contrato : listaContrato) {
			Calendar dtiniciolocacao = SinedDateUtils.javaSqlDateToCalendar(contrato.getDtiniciolocacao());
			Calendar dtfimlocacao = SinedDateUtils.javaSqlDateToCalendar(contrato.getDtfimlocacao());
			Frequencia frequencia = contrato.getFrequenciarenovacao();
			if(frequencia != null && dtiniciolocacao != null && dtfimlocacao != null){
				Calendar dtiniciolocacao_nova = this.atualizaDataByFrequencia(frequencia, dtiniciolocacao);
				Calendar dtfimlocacao_nova = this.atualizaDataByFrequencia(frequencia, dtfimlocacao);
				
				if(dtiniciolocacao_nova != null && dtfimlocacao_nova != null){
					Contratohistorico contratohistorico = new Contratohistorico();
					contratohistorico.setAcao(Contratoacao.ALTERADO);
					contratohistorico.setContrato(contrato);
					contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
//					contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
					contratohistorico.setObservacao("Renova��o automatica de periodo de loca��o de " + SinedUtil.getDescricaoPeriodo(new Date(dtiniciolocacao.getTimeInMillis()), new Date(dtfimlocacao.getTimeInMillis())) + " para " + SinedUtil.getDescricaoPeriodo(new Date(dtiniciolocacao_nova.getTimeInMillis()), new Date(dtfimlocacao_nova.getTimeInMillis())));
					
					contratohistoricoService.saveOrUpdate(contratohistorico);
					
					this.updatePeriodoLocacao(contrato, new Date(dtiniciolocacao_nova.getTimeInMillis()), new Date(dtfimlocacao_nova.getTimeInMillis()));
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	private List<Contrato> findForAtualizacaoPeriodoLocacao(String whereInContrato) {
		return contratoDAO.findForAtualizacaoPeriodoLocacao(whereInContrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @param dtiniciolocacao
	 * @param dtfimlocacao
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	private void updatePeriodoLocacao(Contrato contrato, Date dtiniciolocacao, Date dtfimlocacao) {
		contratoDAO.updatePeriodoLocacao(contrato, dtiniciolocacao, dtfimlocacao);
	}
	
	public void ajustaListaFechamentoVariosContratos(List<RealizarFechamentoRomaneioContratoItem> listaFinal, List<Contrato> listaContrato) {
		if(listaContrato != null && !listaContrato.isEmpty() && listaFinal != null && !listaFinal.isEmpty()){
			HashMap<Material, Double> mapMaterialQtdeUtilizada = new HashMap<Material, Double>();
			for(Contrato contrato : listaContrato){
				if(contrato.getListaContratomaterial() != null && !contrato.getListaContratomaterial().isEmpty()){
					for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
						if(contratomaterial.getServico() != null){
							for(RealizarFechamentoRomaneioContratoItem item : listaFinal){
								if(item.getMaterial() != null && item.getMaterial().equals(contratomaterial.getServico())){
									if(item.getQtdeutilizada() != null && item.getQtdecontratada() != null && 
											item.getQtdeutilizada() > item.getQtdecontratada()){
										Double qtde = mapMaterialQtdeUtilizada.get(item.getMaterial());
										contratomaterial.setContrato(contrato);
										item.setContratomaterial(contratomaterial);
										if(qtde != null){
											Double qtdeRestante = qtde - item.getQtdecontratada();
											if(qtdeRestante >= 0){
												item.setQtdeutilizada(item.getQtdecontratada());
												item.setQtdedevolvida(item.getQtdecontratada());
												mapMaterialQtdeUtilizada.put(item.getMaterial(), qtdeRestante);
											}else {
												item.setQtdeutilizada(0d);
												item.setQtdedevolvida(0d);
												mapMaterialQtdeUtilizada.put(item.getMaterial(), 0d);
											}
										}else {
											Double qtdeRestante = item.getQtdeutilizada() - item.getQtdecontratada();
											item.setQtdeutilizada(item.getQtdecontratada());
											item.setQtdedevolvida(item.getQtdecontratada());
											mapMaterialQtdeUtilizada.put(item.getMaterial(), qtdeRestante);
										}
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public boolean validaEstornoIdentificadorContrato(String whereIn) {
		return contratoDAO.validaEstornoIdentificadorContrato(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @author Rodrigo Freitas
	 * @since 09/07/2014
	 */
	public void atualizaDataConclusaoContrato(Contrato contrato) {
		contratoDAO.atualizaDataConclusaoContrato(contrato);
	}
	
	
	/**
	* M�todo com refer�ncia no DAO
	*
    * @see  br.com.linkcom.sined.geral.dao.ContratoDAO#findForVerificarUltimoRomaneio(String whereInContrato)
	*
	* @param whereInContrato
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public List<Contrato> findForVerificarUltimoRomaneio(String whereInContrato) {
		return contratoDAO.findForVerificarUltimoRomaneio(whereInContrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereInContrato
	 * @return
	 */
	public boolean existeContratoFinalizado(String whereInContrato){
		return contratoDAO.existeContratoFinalizado(whereInContrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereInContrato
	 * @return
	 */
	public List<Contrato> findContratosFinalizados(String whereInContrato) {
		return contratoDAO.findContratosFinalizados(whereInContrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param contrato
	 */
	public void updateDtfimContrato(Contrato contrato) {
		contratoDAO.updateDtfimContrato(contrato);	
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see  br.com.linkcom.sined.geral.dao.ContratoDAO#findWithParcela(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 14/07/2014
	* @author Luiz Fernando
	*/
	public List<Contrato> findWithParcela(String whereIn){
		return contratoDAO.findWithParcela(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @author Danilo Guimar�es
	 */
	public boolean existeFaturamentolote(String whereIn){
		return contratoDAO.existeFaturamentolote(whereIn);
	}
	
	/**
	* M�todo que verifica se o romaneio � parcial
	*
	* @param listaMateriaisNova
	* @return
	* @since 11/09/2014
	* @author Luiz Fernando
	*/
	public boolean isRomaneioParcial(List<RomaneioGerenciamentoMaterialItem> listaMateriaisNova) {
		if(SinedUtil.isListNotEmpty(listaMateriaisNova)){
			for(RomaneioGerenciamentoMaterialItem item : listaMateriaisNova){
				if(item.getQtdedestinoOriginal() != null && item.getQtdedestino() != null &&
						(item.getQtdedestinoOriginal() - item.getQtdedestino()) > 0){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo que busca os contratos a serem estornados ap�s o cancelamento da nota de servi�o
	*
	* @see br.com.linkcom.sined.geral.service.ContratoService#estornarDtProximoVencimentoFaturamento(Contrato c)
	*
	* @param request
	* @param whereInNota
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public void estornarContratoAposCancelarNota(WebRequestContext request, String whereInNota) {
		List<Contrato> listaContratoestornar = buscarContratoAposUltimaNotaCancelada(whereInNota);
		if(SinedUtil.isListNotEmpty(listaContratoestornar)){
			List<Contrato> listaContrato = findForCobranca(CollectionsUtil.listAndConcatenate(listaContratoestornar, "cdcontrato", ","));
			if(SinedUtil.isListNotEmpty(listaContrato)){
				for(Contrato c : listaContrato){
					try {
						this.estornarDtProximoVencimentoFaturamento(request, c);
					} catch (Exception e) {
//						System.out.println("Erro no estorno de contrato apos cancelamento de nota: " + e.getMessage());
						e.printStackTrace();
						if(request != null)
							request.addError("N�o foi poss�vel estornar o contrato " + c.getCdcontrato());
					}
				}
			}
		}
	}
	
	/**
	* M�todo que retorna a data do pr�ximo vencimento do contrato para o valor anterior (estorno do contrato)
	*
	* @param c
	* @param request 
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public void estornarDtProximoVencimentoFaturamento(WebRequestContext request, Contrato c) {
		if(c.getFrequencia() != null && c.getFrequencia().getCdfrequencia() != null){
			Calendar novaDtProximo = this.estornarDataByFrequencia(c.getFrequencia(), SinedDateUtils.javaSqlDateToCalendar(c.getDtproximovencimento()));
			if(novaDtProximo != null) {
				c.setDtfim(null);
				this.setNullDataFimContrato(c);
				if(c.getFrequencia() != null && 
						c.getFrequencia().getCdfrequencia() != null &&
						c.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL) &&
						c.getMes() != null && 
						c.getAno() != null){
					if(c.getMes().ordinal() == 11){
						c.setMes(Mes.values()[0]);	
						c.setAno(c.getAno() + -1);
					} else {
						c.setMes(Mes.values()[c.getMes().ordinal() + -1]);			
					}
					this.atualizaMesAnoContrato(c);
				}
			
				c.setDtproximovencimento(new Date(novaDtProximo.getTimeInMillis()));
				this.updateDtProximoVencimentoContrato(c);
				criaHistoricoEstornoCancelamentoNota(c);
			}			
		} else if(c.getListaContratoparcela() != null && c.getListaContratoparcela().size() > 0){
			if(c.getFrequenciarenovacao() != null){
				if(request != null){
					request.addMessage("O contrato n�o foi estornado por ter renova��o autom�tica.", MessageType.WARN);
				}
			} else {
				Contratoparcela contratoparcela = this.getUltimaParcelaCobrada(c.getListaContratoparcela());
				
				if(contratoparcela != null){
					contratoparcela.setCobrada(null);
					contratoparcelaService.updateParcelaNaoCobrada(contratoparcela);
					c.setDtproximovencimento(contratoparcela.getDtvencimento());
					this.updateDtProximoVencimentoContrato(c);
					c.setDtfim(null);
					this.setNullDataFimContrato(c);
				} else {
					c.setDtfim(null);
					this.setNullDataFimContrato(c);
				}
			}
		}
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContratoDAO#buscarContratoAposUltimaNotaCancelada(String whereInNota)
	*
	* @param whereInNota
	* @return
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public List<Contrato> buscarContratoAposUltimaNotaCancelada(String whereInNota) {
		return contratoDAO.buscarContratoAposUltimaNotaCancelada(whereInNota);
	}
	public boolean existeContratoAtivo(Cliente cliente, Contagerencial contagerencialMensalidade, Projeto projetoW3erp, Projeto projetoSindis, Projeto projetoSupergov) {
		return contratoDAO.existeContratoAtivo(cliente, contagerencialMensalidade, projetoW3erp, projetoSindis, projetoSupergov);
	}
	
	/**
	 * Verfica se o contrato � de medi��o
	 * @param contrato
	 * @return
	 */
	public boolean isContratoMedicao(Contrato contrato){
		return contratoDAO.isContratoMedicao(contrato);
	}
	
	public Contratomaterial getContratomaterialByMaterial(List<Contratomaterial> listaContratomaterial, Material material, Patrimonioitem patrimonioitem, Double qtderomaneio) {
		Contratomaterial contratomaterial = null;
		
		if(listaContratomaterial != null && !listaContratomaterial.isEmpty() && material != null && material.getCdmaterial() != null){
			for(Contratomaterial cm : listaContratomaterial){
				if(cm.getServico() != null && material.equals(cm.getServico()) && cm.getQtde() != null && cm.getQtde() >= qtderomaneio){
					if((patrimonioitem == null && cm.getPatrimonioitem() == null) || (patrimonioitem != null && cm.getPatrimonioitem() != null && patrimonioitem.equals(cm.getPatrimonioitem()))){
						contratomaterial = cm;
						break;
					}
				}
			}
		}
		return contratomaterial;
	}
	
		
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @author Danilo Guimar�es
	 */
	public boolean existeFaturamentoWebService(String whereIn){
		return contratoDAO.existeFaturamentoWebService(whereIn);
	}
	
	/**
	 * Carrega os dados do contrato para utiliza��o no WS de Pedidovenda
	 * @param contrato
	 * @return
	 */
	public Contrato loadForWS(Contrato contrato) {
		return contratoDAO.loadForWS(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Contrato> findByCentroCustoValidacao (Centrocusto centrocusto){
		return contratoDAO.findByCentroCustoValidacao (centrocusto);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param contagerencial
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Contrato> findByContaGerencialValidacao (Contagerencial contagerencial){
		return contratoDAO.findByContaGerencialValidacao (contagerencial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param contacontabil
	 * @return
	 * @since 29/10/2019	
	 * @author Arthur Gomes
	 */
	public List<Contrato> findByContaGerencialValidacao (ContaContabil contacontabil){
		return contratoDAO.findByContaGerencialValidacao (contacontabil);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContratoDAO#existeContratoComTodasParcelasCobradas(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 24/03/2016
	* @author Luiz Fernando
	*/
	public Boolean existeContratoComTodasParcelasCobradas(String whereIn) {
		return contratoDAO.existeContratoComTodasParcelasCobradas(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param dataAtual
	 * @param dataLimite
	 * @return
	 * @since 27/06/2016
	 * @author C�sar
	 */
	public List<Contrato> findForPainelContratos(Date dataAtual, Date dataLimite){
		return contratoDAO.findForPainelContratos(dataAtual, dataLimite);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public boolean haveContratoProdutoServico(String whereIn) {
		return contratoDAO.haveContratoProdutoServico(whereIn);
	}
	
	public boolean haveContratoProduto(String whereIn) {
		return contratoDAO.haveContratoProduto(whereIn);
	}

	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param contrato
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 09/02/2017
	 */
	public void updateIdentificador(Contrato contrato, String identificador) {
		contratoDAO.updateIdentificador(contrato, identificador);
	}

	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param whereIn
	 * @author Mairon Cezar
	 * @since 13/03/2017
	 */
	public List<Contrato> findContratosCancelados(String whereIn){
		return contratoDAO.findContratosCancelados(whereIn);
	}
	
	public void updateReajuste(Contrato c, Boolean reajuste, Double percentualreajuste) {
		contratoDAO.updateReajuste(c, reajuste, percentualreajuste);
	}
	
	public List<Contrato> findForArquivoRetornoContrato(ArquivoRetornoContratoFiltro filtro){
		return contratoDAO.findForArquivoRetornoContrato(filtro);
	}
	
	public void transactionArquivoRetornoContrato(final ArquivoRetornoContratoFiltro filtro){
		contratoDAO.transactionArquivoRetornoContrato(filtro);
	}
	
	public boolean existeContratotipo(Contrato contrato) {
		return contratoDAO.existeContratotipo(contrato);
	}
	
	public List<Contrato> findForIndenizacao(Cliente cliente){
		return contratoDAO.findForIndenizacao(cliente);
	}
	
	public List<Contrato> findByEmitirindenizacaofiltro(EmitirIndenizacaoContratoFiltro filtro){
		return contratoDAO.findByEmitirindenizacaofiltro(filtro);
	}
	
	public List<EmitirIndenizacaoContratoBean> montaListaForIndenizacaoContrato(EmitirIndenizacaoContratoFiltro filtro){
		List<EmitirIndenizacaoContratoBean> listaReport = new ArrayList<EmitirIndenizacaoContratoBean>();
		List<Contrato> listacontrato = this.findByEmitirindenizacaofiltro(filtro);
		for(Contrato contrato: listacontrato){
			List<Movimentacaoestoque> listamovimentacaomaterial = movimentacaoestoqueService.findForIndenizacaoContrato(contrato.getCdcontrato().toString(), filtro.getMaterial());
			List<Movpatrimonio> listamovimentacaoatrimonio = movpatrimonioService.findForIndenizacaoContrato(contrato.getCdcontrato().toString());
			for(Movimentacaoestoque movimentacao: listamovimentacaomaterial){
				EmitirIndenizacaoContratoBean bean = new EmitirIndenizacaoContratoBean();
				bean.setCliente(contrato.getCliente().getNome());
				bean.setContrato(contrato.getIdentificadorOrCdcontrato());
				bean.setDtindenizacao(movimentacao.getDtmovimentacao());
				bean.setMaterialindenizado(movimentacao.getMaterial().getIdentificacaoOuCdmaterial() + " - " + movimentacao.getMaterialDescricao());
				bean.setQuantidade(movimentacao.getQtde());
				bean.setValorindenizado(movimentacao.getValorcusto());
				bean.setValorcusto(movimentacao.getMaterial().getValorcusto());
				if(movimentacao.getQtde() != null && movimentacao.getValorcusto() != null){
					bean.setTotalindenizado(movimentacao.getQtde() * movimentacao.getValorcusto());
				}
				
				listaReport.add(bean);
			}
			
			for(Movpatrimonio movimentacao: listamovimentacaoatrimonio){
				Material material = movimentacao.getPatrimonioitem().getBempatrimonio();
				EmitirIndenizacaoContratoBean bean = new EmitirIndenizacaoContratoBean();
				bean.setCliente(contrato.getCliente().getNome());
				bean.setContrato(contrato.getIdentificadorOrCdcontrato());
				bean.setDtindenizacao(new Date(movimentacao.getDtmovimentacao().getTime()));
				bean.setMaterialindenizado(material.getIdentificacaoOuCdmaterial() + " - " + material.getNome());
				if(!Util.strings.isEmpty(movimentacao.getPatrimonioitem().getPlaqueta())){
					bean.setMaterialindenizado(bean.getMaterialindenizado() + " - " + movimentacao.getPatrimonioitem().getPlaqueta());
				}
				bean.setQuantidade(1d);
				bean.setValorcusto(material.getValorcusto());
				bean.setValorindenizado(movimentacao.getValormovimentacao());
				bean.setTotalindenizado(movimentacao.getValormovimentacao());
				
				listaReport.add(bean);
			}
		}
		return listaReport;
	}
	
	public IReport makeReportIndenizacaoContrato(EmitirIndenizacaoContratoFiltro filtro){
		List<EmitirIndenizacaoContratoBean> lista = this.montaListaForIndenizacaoContrato(filtro);
		if(lista == null || lista.isEmpty()){
			throw new SinedException("N�o existem registros no sistema para os dados informados.");
		}
		Report report = new Report("/faturamento/emitirindenizacaocontrato");
		Empresa empresaAux = filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? empresaService.loadComArquivo(filtro.getEmpresa()) : empresaService.loadPrincipal();
		Image image = SinedUtil.getLogo(empresaAux);
		report.addParameter("LOGO", image);
		report.addParameter("EMPRESA", empresaService.getEmpresaRazaosocialOuNome(empresaAux));
		report.addParameter("PERIODO", DateUtils.dateToString(filtro.getDtinicio(), "dd/MM/yyyy")+" a "+DateUtils.dateToString(filtro.getDtinicio(), "dd/MM/yyyy"));
		report.addParameter("DATA", SinedDateUtils.currentDate());
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.setDataSource(lista);
		return report;
	}
	
	public Contrato loadContratoByCriarRequisicao(Contrato contrato){
		return contratoDAO.loadContratoByCriarRequisicao(contrato);
	}
	
	public Contrato findForCancelarRomaneio(Contrato contrato) {
		return contratoDAO.findForCancelarRomaneio(contrato);
	}

	public boolean finalizarContratoByWebservice(FinalizarContratoBean bean) {
		if(bean.getContrato() == null){
			throw new SinedException("Par�metro inv�lido. Id do contrato n�o informado.");
		}
		
		Contrato contrato = contratoService.load(new Contrato(bean.getContrato()), "contrato.cdcontrato, contrato.aux_contrato, contrato.dtfim");
		
		if(contrato == null){
			throw new SinedException("Contrato n�o encontrado.");
		}
		
		if(contrato.getDtconclusao() == null){
			atualizaDataConclusaoContrato(contrato);
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Contrato criarContratoByWebservice(CriarContratoBean bean) {
		StringBuilder msgErro = new StringBuilder();
		
		Double multa = parametrogeralService.getDouble(Parametrogeral.INTEGRACAO_CONTRATO_MULTA);
		Double juros = parametrogeralService.getDouble(Parametrogeral.INTEGRACAO_CONTRATO_JUROS);
		Integer cdcentrocusto = parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_CENTROCUSTO);
		Integer cdcontagerencial = bean.getContagerencial() != null ? bean.getContagerencial() : parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_CONTAGERENCIAL);
		Integer cdempresa = parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_EMPRESA);
		Integer cdcontatipo = bean.getContatipo() != null ? bean.getContatipo() : parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_CONTATIPO);
		Integer cdconta = bean.getConta() != null ? bean.getConta() : parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_CONTA);
		Integer cdcontacarteira = bean.getContacarteira() != null ? bean.getContacarteira() : parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_CONTACARTEIRA);
		Integer cdresponsavel = parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_RESPONSAVEL);
		Integer cdformafaturamento = parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_FORMAFATURAMENTO);
		Integer diasproximovencimento = parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_DIASPROXIMOVENCIMENTO);
		Integer cdfrequencia = bean.getPeriodicidade() != null ? bean.getPeriodicidade() : parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_CONTRATO_FREQUENCIA);
		Integer cddocumentotipo = bean.getDocumentotipo();
		
		Centrocusto centrocusto = null;
		Contagerencial contagerencial = null;
		Empresa empresa = null;
		Contatipo contatipo = null;
		Conta conta = null;
		Contacarteira contacarteira = null;
		Colaborador colaborador = null;
		Formafaturamento formafaturamento = null;
		Frequencia frequencia = null;
		Documentotipo documentotipo = null;
		
		if(cdcentrocusto == null){
			msgErro.append("Centro de custo � obrigat�rio.\n");
		} else {
			centrocusto = centrocustoService.load(new Centrocusto(cdcentrocusto));
			if(centrocusto == null){
				msgErro.append("Centro de custo n�o encontrado.\n");
			}
		}
		
		if(cdcontagerencial == null){
			msgErro.append("Conta gerencial � obrigat�rio.\n");
		} else {
			contagerencial = contagerencialService.load(new Contagerencial(cdcontagerencial));
			if(contagerencial == null){
				msgErro.append("Conta gerencial n�o encontrada.\n");
			}
		}
		
		if(cdempresa == null){
			msgErro.append("Empresa � obrigat�rio.\n");
		} else {
			empresa = empresaService.load(new Empresa(cdempresa));
			if(empresa == null){
				msgErro.append("Empresa n�o encontrada.\n");
			}
		}
		
		if(cdcontatipo == null){
			msgErro.append("Tipo de conta � obrigat�rio.\n");
		} else {
			switch (cdcontatipo) {
				case 1:
				case 2:
				case 3:
					contatipo = new Contatipo(cdcontatipo);
					break;
	
				default:
					msgErro.append("Tipo de conta n�o encontrado.\n");
					break;
			}
		}
		
		if(cdconta == null){
			msgErro.append("Conta � obrigat�ria.\n");
		} else {
			conta = contaService.load(new Conta(cdconta));
			if(conta == null){
				msgErro.append("Conta n�o encontrado.\n");
			}
		}
		
		if(cdcontacarteira == null){
			msgErro.append("Carteira da conta � obrigat�ria.\n");
		} else {
			contacarteira = contacarteiraService.load(new Contacarteira(cdcontacarteira));
			if(conta == null){
				msgErro.append("Carteira da conta n�o encontrado.\n");
			}
		}
		
		if(cdresponsavel == null){
			msgErro.append("Respons�vel � obrigat�ria.\n");
		} else {
			colaborador = colaboradorService.load(new Colaborador(cdresponsavel));
			if(colaborador == null){
				msgErro.append("Respons�vel n�o encontrado.\n");
			}
		}
		
		if(cdformafaturamento == null){
			msgErro.append("Forma de faturamento � obrigat�ria.\n");
		} else {
			switch (cdformafaturamento) {
				case 0:
				case 1:
				case 2:
					formafaturamento = Formafaturamento.values()[cdformafaturamento];
					break;
	
				default:
					msgErro.append("Forma de faturamento n�o encontrada.\n");
					break;
			}
		}
		
		if(cdfrequencia == null){
			msgErro.append("Periodicidade � obrigat�ria.\n");
		} else {
			frequencia = frequenciaService.load(new Frequencia(cdfrequencia));
			if(frequencia == null){
				msgErro.append("Periodicidade n�o encontrada.\n");
			}
		}
		
		Cliente cliente = null;
		if(bean.getCliente() == null){
			msgErro.append("Cliente � obrigat�rio.\n");
		} else {
			cliente = clienteService.load(new Cliente(bean.getCliente()));
			if(cliente == null){
				msgErro.append("Cliente n�o encontrado.\n");
			}
		}
		
		if(cddocumentotipo != null){
			documentotipo = documentotipoService.load(new Documentotipo(cddocumentotipo));
			if(documentotipo == null){
				msgErro.append("Tipo de documento n�o encontrado.\n");
			}
		}
		
		if(bean.getValor() == null){
			msgErro.append("Valor � obrigat�rio.\n");
		}
		
		if(org.apache.commons.lang.StringUtils.isBlank(bean.getDescricao())){
			msgErro.append("Descri��o � obrigat�rio.\n");
		}
		
		if(msgErro.length() > 0){
			throw new SinedException(msgErro.toString());
		}
		
		List<Endereco> listaEnderecos = enderecoService.carregarListaEndereco(cliente);
		cliente.setListaEndereco(SinedUtil.listToSet(listaEnderecos, Endereco.class));
		Endereco endereco = cliente.getEndereco();
		
		Contratohistorico contratohistorico = new Contratohistorico();
		contratohistorico.setAcao(Contratoacao.CRIADO);
		contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
		contratohistorico.setObservacao("Criado a partir de webservice.");
		
		List<Contratohistorico> listaContratohistorico = new ArrayList<Contratohistorico>();
		listaContratohistorico.add(contratohistorico);
		
		Set<Taxaitem> listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
		if(multa != null){
			Taxaitem taxaitem_multa = new Taxaitem();
			taxaitem_multa.setTipotaxa(Tipotaxa.MULTA);
			taxaitem_multa.setPercentual(Boolean.TRUE);
			taxaitem_multa.setValor(new Money(multa));
			taxaitem_multa.setAodia(Boolean.FALSE);
			listaTaxaitem.add(taxaitem_multa);
		}
		
		if(juros != null){
			Taxaitem taxaitem_juros = new Taxaitem();
			taxaitem_juros.setTipotaxa(Tipotaxa.JUROS);
			taxaitem_juros.setPercentual(Boolean.TRUE);
			taxaitem_juros.setValor(new Money(juros));
			taxaitem_juros.setAodia(Boolean.FALSE);
			listaTaxaitem.add(taxaitem_juros);
		}
		
		Taxa taxa = new Taxa();
		taxa.setListaTaxaitem(listaTaxaitem);
		
		Rateioitem rateioitem = new Rateioitem();
		rateioitem.setCentrocusto(centrocusto);
		rateioitem.setContagerencial(contagerencial);
		rateioitem.setPercentual(100d);
		rateioitem.setValor(bean.getValor());
		
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		listaRateioitem.add(rateioitem);
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listaRateioitem);
		
		Date currentDate = SinedDateUtils.currentDate();
		Date dtproximovencimento = SinedDateUtils.currentDate();
		if(diasproximovencimento != null){
			dtproximovencimento = SinedDateUtils.addDiasData(dtproximovencimento, diasproximovencimento);
		}
		
		Contrato contrato = new Contrato();
		contrato.setEmpresa(empresa);
		contrato.setContatipo(contatipo);
		contrato.setConta(conta);
		contrato.setContacarteira(contacarteira);
		contrato.setIdentificador(bean.getIdentificador() != null ? "" + bean.getIdentificador() : null);
		contrato.setCliente(cliente);
		contrato.setEndereco(endereco);
		contrato.setEnderecoentrega(endereco);
		contrato.setDescricao(bean.getDescricao());
		contrato.setDtinicio(currentDate);
		contrato.setDtassinatura(currentDate);
		contrato.setDtconfirma(currentDate);
		contrato.setResponsavel(colaborador);
		contrato.setDocumentotipo(documentotipo);
		contrato.setFormafaturamento(formafaturamento);
		contrato.setDtproximovencimento(dtproximovencimento);
		contrato.setDataparceladiautil(Boolean.TRUE);
		contrato.setFrequencia(frequencia);
		contrato.setQtde(1d);
		contrato.setValor(bean.getValor());
		contrato.setIsento(bean.getValor().getValue().doubleValue() <= 0);
		contrato.setListaContratohistorico(listaContratohistorico);
		contrato.setRateio(rateio);
		contrato.setTaxa(taxa);
		this.saveOrUpdate(contrato);
		
		return contrato;
	}
	public Contrato selecionarEnderecoFaturamento(Contrato c) {
		Endereco  endereco = null;
		if(Formafaturamento.SOMENTE_BOLETO.equals(c.getFormafaturamento()) || Formafaturamento.NOTA_APOS_RECEBIMENTO.equals(c.getFormafaturamento()))
			endereco = c.getEnderecocobranca();
		if(endereco == null)
			endereco = c.getEndereco();
		if(endereco == null)
			endereco = c.getCliente().getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO);
		if(endereco == null)
			endereco = c.getEndereco();
		c.setEnderecoPrioritario(endereco);
		return c;
	}
	
	public Contrato selecionarEnderecoAlternativoDeCobranca(Contrato c){
		Endereco endereco = null;
		if(c.getEnderecocobranca() != null)
			endereco = c.getEnderecocobranca();
		else
			endereco = c.getCliente().getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO);
		c.setEnderecoPrioritario(endereco);
		return c;
	}
	public Contrato loadContratoWithEndereco(Contrato contrato) {
		return contratoDAO.loadContratoWithEndereco(contrato);
	}
	
	public void preencheMaterialRetornoContrato(List<ArquivoRetornoContratoBean> listaBean) {
		if(SinedUtil.isListNotEmpty(listaBean)){
			for(ArquivoRetornoContratoBean bean : listaBean){
				if(Boolean.TRUE.equals(bean.getSelecao()) && bean.getContrato() != null && SinedUtil.isListNotEmpty(bean.getListaItens())){
					Contrato contrato = loadForEntrada(bean.getContrato());
					if(contrato != null){
						contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
						boolean recalcularRateio = false;
						
						List<Contratomaterial> listaContratoMaterial = new ArrayList<Contratomaterial>();
						for(ArquivoRetornoContratoItemBean item : bean.getListaItens()){
							if(item.getMaterial() != null){
								boolean add = true; 
								
								if(SinedUtil.isListNotEmpty(contrato.getListaContratomaterial())){
									for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
										if(item.getMaterial().equals(contratomaterial.getServico())){
											if(contratomaterial.getDtfim() != null && item.getDatageracao() != null && SinedDateUtils.afterOrEqualsIgnoreHour(contratomaterial.getDtfim(), item.getDatageracao())){
												contratomaterial.setDtfim(SinedDateUtils.addDiasData(item.getDatageracao(), -1));
											}
											contratomaterial.setQtde(item.getQtde());
											if(contratomaterial.getValorunitario() == null) contratomaterial.setValorunitario(item.getMaterial().getValorvenda());
											recalcularRateio = true;
											item.setContratomaterial(contratomaterial);
											listaContratoMaterial.add(contratomaterial);
											add = false;
											break;
										}
									}
								}
								if(add){
									Contratomaterial contratomaterial = new Contratomaterial();
									contratomaterial.setContrato(bean.getContrato());
									contratomaterial.setServico(item.getMaterial());
									contratomaterial.setQtde(item.getQtde());
									contratomaterial.setDtinicio(item.getDatageracao());
									contratomaterial.setValorunitario(item.getMaterial().getValorvenda());
									item.setContratomaterial(contratomaterial);
									listaContratoMaterial.add(contratomaterial);
									recalcularRateio = true;
								}
							}
						}
						contrato.setListaContratomaterial(listaContratoMaterial);
						
						if(recalcularRateio){
							atualizaValor(contrato, false);
							rateioService.atualizaValorRateio(contrato.getRateio(), contrato.getValorContrato());
							bean.setRateio(contrato.getRateio());
							bean.setValorContrato(contrato.getValorContrato());
						}
					}
				}
			}
		}
	}
	
	public String criaObservacaoHistorico(Contrato bean, Contrato contrato) {
		String observacao = "";
		
		if(bean != null && bean.getCdcontrato() != null && contrato != null){
			//Valor
			if(bean.getValor() != null && contrato.getValor() == null){
				observacao += "Altera��o do valor de 'vazio' para R$ " + new Money(bean.getValor()) + ".<br>";
			}else if(bean.getValor() == null && contrato.getValor() != null){
				observacao += "Altera��o do valor de R$ " + new Money(contrato.getValor()) + " para 'vazio'.<br>";
			}else if(bean.getValor() != null && contrato.getValor() != null && bean.getValor().compareTo(contrato.getValor()) != 0){
				observacao += "Altera��o do valor de R$ " + new Money(contrato.getValor()) + " para R$ " + new Money(bean.getValor()) + ".<br>";
			}
			
			//DtProximoVencimento
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			if(bean.getDtproximovencimento() != null && contrato.getDtproximovencimento() == null){
				observacao += "Altera��o da data do pr�ximo vencimento de 'vazio' para " + format.format(bean.getDtproximovencimento()) + ".<br>";
			}else if(bean.getDtproximovencimento() == null && contrato.getDtproximovencimento() != null){
				observacao += "Altera��o da data do pr�ximo vencimento de " + format.format(contrato.getDtproximovencimento()) + " para 'vazio'.<br>";
			}else if(bean.getDtproximovencimento() != null && contrato.getDtproximovencimento() != null && !SinedDateUtils.equalsIgnoreHour(bean.getDtproximovencimento(), contrato.getDtproximovencimento())){
				observacao += "Altera��o da data do pr�ximo vencimento de " + format.format(contrato.getDtproximovencimento()) + " para " + format.format(bean.getDtproximovencimento()) + ".<br>";
			}
			
			//Forma de Faturamento
			if(bean.getFormafaturamento() != null && contrato.getFormafaturamento() == null){
				observacao += "Altera��o da forma de faturamento de 'vazio' para '" + bean.getFormafaturamento().getNome() + "'.<br>";
			}else if(bean.getFormafaturamento() == null && contrato.getFormafaturamento() != null){
				observacao += "Altera��o da forma de faturamento de '" + contrato.getFormafaturamento().getNome() + "' para 'vazio'.<br>";
			}else if(bean.getFormafaturamento() != null && contrato.getFormafaturamento() != null && !bean.getFormafaturamento().equals(contrato.getFormafaturamento())){
				observacao += "Altera��o da forma de faturamento de '" + contrato.getFormafaturamento().getNome() + "' para '" + bean.getFormafaturamento().getNome() + "'.<br>";
			}
			
			//Historico
			if(bean.getHistorico() != null && contrato.getHistorico() == null){
				observacao += "Altera��o do hist�rico.<br>";
			}else if(bean.getHistorico() == null && contrato.getHistorico() != null){
				observacao += "Altera��o do hist�rico.<br>";
			}else if(bean.getHistorico() != null && contrato.getHistorico() != null && !bean.getHistorico().equals(contrato.getHistorico())){
				observacao += "Altera��o do hist�rico.<br>";
			}
			
			//Anotacao
			if(bean.getAnotacao() != null && contrato.getAnotacao() == null){
				observacao += "Altera��o da anota��o.<br>";
			}else if(bean.getAnotacao() == null && contrato.getAnotacao() != null){
				observacao += "Altera��o da anota��o.<br>";
			}else if(bean.getAnotacao() != null && contrato.getAnotacao() != null && !bean.getAnotacao().equals(contrato.getAnotacao())){
				observacao += "Altera��o da anota��o.<br>";
			}
			
			//Rateio
			String rateio = "";
			
			if (bean.getRateio() != null && bean.getRateio().getListaRateioitem() != null && bean.getRateio().getListaRateioitem().size() > 0 &&
					contrato.getRateio() == null) {
				rateio = "Altera��o na(s) parcela(s) do contrato.<br>";
			} else if (bean.getRateio() == null && contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null && 
					contrato.getRateio().getListaRateioitem().size() > 0) {
				rateio = "Altera��o na(s) parcela(s) do contrato.<br>";
			} else if (bean.getRateio() != null && bean.getRateio().getListaRateioitem() != null && 
					contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null &&
					(bean.getRateio().getListaRateioitem().size() != contrato.getRateio().getListaRateioitem().size())) {
				rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
			} else if (bean.getRateio() != null && bean.getRateio().getListaRateioitem() != null && bean.getRateio().getListaRateioitem().size() > 0 &&
					contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null && contrato.getRateio().getListaRateioitem().size() > 0) {
				for (Rateioitem ric : contrato.getRateio().getListaRateioitem()) {
					if (ric.getCdrateioitem() == null) {
						rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
						break;
					} else {
						Boolean naoAchou = true;
						for (Rateioitem rib : bean.getRateio().getListaRateioitem()) {
							if (ric.getCdrateioitem().equals(rib.getCdrateioitem())) {
								naoAchou = false;
								
								if (rib.getContagerencial() != null && ric.getContagerencial() == null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getContagerencial() == null && ric.getContagerencial() != null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getContagerencial() != null && ric.getContagerencial() != null && 
										!rib.getContagerencial().getCdcontagerencial().equals(ric.getContagerencial().getCdcontagerencial())) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								}
								
								if (rib.getCentrocusto() != null && ric.getCentrocusto() == null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getCentrocusto() == null && ric.getCentrocusto() != null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getCentrocusto() != null && ric.getCentrocusto() != null && 
										!rib.getCentrocusto().getCdcentrocusto().equals(ric.getCentrocusto().getCdcentrocusto())) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								}
								
								if (rib.getProjeto() != null && ric.getProjeto() == null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getProjeto() == null && ric.getProjeto() != null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getProjeto() != null && ric.getProjeto() != null && 
										!rib.getProjeto().getCdprojeto().equals(ric.getProjeto().getCdprojeto())) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								}
								
								if (rib.getValor() != null && ric.getValor() == null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getValor() == null && ric.getValor() != null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getValor() != null && ric.getValor() != null && 
										rib.getValor().compareTo(ric.getValor()) != 0) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								}
								
								if (rib.getPercentual() != null && ric.getPercentual() == null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getPercentual() == null && ric.getPercentual() != null) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								} else if(rib.getPercentual() != null && ric.getPercentual() != null && 
										!SinedUtil.round(rib.getPercentual(), 13).equals(ric.getPercentual())) {
									rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
									break;
								}
							}
						}
						
						if (naoAchou) {
							rateio = "Altera��o no(s) rateio(s) do contrato.<br>";
							break;
						}
						
						if (!org.apache.commons.lang.StringUtils.isEmpty(rateio)) {
							break;
						}
					}
				}
			}
			
			observacao += rateio;

			//Parcela
			String parcela = "";
			
			if (bean.getListaContratoparcela() != null && bean.getListaContratoparcela().size() > 0 && contrato.getListaContratoparcela() == null) {
				parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
			} else if (bean.getListaContratoparcela() == null && contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0) {
				parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
			} else if (bean.getListaContratoparcela() != null && contrato.getListaContratoparcela() != null &&
					(bean.getListaContratoparcela().size() != contrato.getListaContratoparcela().size())) {
				parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
			} else if (bean.getListaContratoparcela() != null && bean.getListaContratoparcela().size() > 0 &&
					contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0) {
				for (Contratoparcela cpc : contrato.getListaContratoparcela()) {
					if (cpc.getCdcontratoparcela() == null) {
						parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
						break;
					} else {
						Boolean naoAchou = true;
						for (Contratoparcela cpb : bean.getListaContratoparcela()) {
							if (cpc.getCdcontratoparcela().equals(cpb.getCdcontratoparcela())) {
								naoAchou = false;
								
								if (cpc.getDtvencimento() != null && cpb.getDtvencimento() == null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getDtvencimento() == null && cpb.getDtvencimento() != null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getDtvencimento() != null && cpb.getDtvencimento() != null && 
										!cpc.getDtvencimento().equals(cpb.getDtvencimento())) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								}
								
								if ((cpc.getDesconto() != null && cpc.getDesconto().getValue().intValue() > 0) && cpb.getDesconto() == null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getDesconto() == null && (cpb.getDesconto() != null && cpb.getDesconto().getValue().intValue() > 0)) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if((cpc.getDesconto() != null && cpc.getDesconto().getValue().intValue() > 0) && 
										(cpb.getDesconto() != null && cpb.getDesconto().getValue().intValue() > 0) && 
										cpc.getDesconto().compareTo(cpb.getDesconto()) != 0) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								}
								
								if (cpc.getValor() != null && cpb.getValor() == null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getValor() == null && cpb.getValor() != null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getValor() != null && cpb.getValor() != null && 
										cpc.getValor().compareTo(cpb.getValor()) != 0) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								}
								
								if (cpc.getAplicarindice() != null && cpb.getAplicarindice() == null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getAplicarindice() == null && cpb.getAplicarindice() != null) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								} else if(cpc.getAplicarindice() != null && cpb.getAplicarindice() != null && 
										!cpc.getAplicarindice().equals(cpb.getAplicarindice())) {
									parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
									break;
								}
							}
						}
						
						if (naoAchou) {
							parcela = "Altera��o na(s) parcela(s) do contrato.<br>";
							break;
						}
						
						if (!org.apache.commons.lang.StringUtils.isEmpty(parcela)) {
							break;
						}
					}
				}
			}
			
			observacao += parcela;
		}
		
		return observacao;
	}
	public boolean isOrigemOrcamento(Vendaorcamento bean) {
		if(contratoDAO.isOrigemOrcamento(bean.getCdvendaorcamento())){
			return true;
		}
		return false;
	}
	
	public ModelAndView abrirPopUpPatrimonioItem(WebRequestContext request, Integer cdmaterial) {
		List<Patrimonioitem> listaPatrimonioitems = new ArrayList<Patrimonioitem>();
		if(cdmaterial != null){
			listaPatrimonioitems = patrimonioitemService.findByMaterialContrato(cdmaterial);			
		}
		
		if(SinedUtil.isListNotEmpty(listaPatrimonioitems)){
			StringBuilder sb = new StringBuilder();
			for (Patrimonioitem patrimonioitem : listaPatrimonioitems) {
				sb.append("<li><span class=\"folha\" onclick=\"selecionarFirstClick(this, ");
				sb.append(patrimonioitem.getCdpatrimonioitem());
				sb.append(", '");
				sb.append(patrimonioitem.getPlaqueta());
				sb.append("')\">");
				sb.append(patrimonioitem.getPlaqueta());
				sb.append("</span></li>");
			}
			
			request.setAttribute("codigo_treeview", sb);
		}	
		request.setAttribute("propriedade", request.getParameter("propriedade"));
		return new ModelAndView("direct:/crud/popup/patrimonioItemTreeView");
	}

	public Map<String, Integer> createMapForValidacaoClientesEmAtraso(List<Contrato> listaContrato){
		Map<String, Integer> mapa = new HashMap<String, Integer>();
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		for(Contrato contrato: listaContrato){
			if(!listaClientes.contains(contrato.getCliente())){
				listaClientes.add(contrato.getCliente());
			}
		}
		Integer countClientesEmAtraso = 0;
		for(Cliente cliente: listaClientes){
			if(contareceberService.clientePossuiContasAtrasadas(cliente)){
				countClientesEmAtraso++;
			}
		}
		
		if(listaClientes.size() == 1 && listaClientes.size() == countClientesEmAtraso){
			mapa.put("diasEmAtraso", clienteService.diasEmAtraso(listaClientes.get(0)));
		}
		
		mapa.put("qtdeClientes", listaClientes.size());
		mapa.put("qtdeClientesEmAtraso", countClientesEmAtraso);
		return mapa;
	}
	
	public JsonModelAndView createModlAndViewForValidacaoClientesEmAtraso(List<Contrato> listaContrato){
		JsonModelAndView retorno = new JsonModelAndView();
		
		Map<String, Integer> mapa = contratoService.createMapForValidacaoClientesEmAtraso(listaContrato);
		retorno.addObject("qtdeClientes", mapa.get("qtdeClientes"));
		retorno.addObject("qtdeClientesEmAtraso", mapa.get("qtdeClientesEmAtraso"));
		retorno.addObject("diasEmAtraso", mapa.get("diasEmAtraso"));
		
		return retorno;
	}
	
	public List<Contrato> removeContratosClientesInadimplemntes(List<Contrato> listaContrato){
		List<Contrato> listaContratoClienteAdimplentes = new ArrayList<Contrato>();
		for(Contrato contrato: listaContrato){
			if(!contareceberService.clientePossuiContasAtrasadas(contrato.getCliente())){
				listaContratoClienteAdimplentes.add(contrato);
			}
		}
		return listaContratoClienteAdimplentes;
	}
}