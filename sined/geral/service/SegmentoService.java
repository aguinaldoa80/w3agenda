package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.dao.SegmentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SegmentoService extends GenericService<Segmento>{

	private SegmentoDAO segmentoDAO;
	
	public void setSegmentoDAO(SegmentoDAO segmentoDAO) {
		this.segmentoDAO = segmentoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2016
	 */
	public Segmento findByNome(String nome) {
		return segmentoDAO.findByNome(nome);
	}

}
