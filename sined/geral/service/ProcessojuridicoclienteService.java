package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;
import br.com.linkcom.sined.geral.dao.ProcessojuridicoclienteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessojuridicoclienteService extends GenericService<Processojuridicocliente>{
	
	private ProcessojuridicoclienteDAO processojuridicoclienteDAO;
	
	public void setProcessojuridicoclienteDAO(ProcessojuridicoclienteDAO processojuridicoclienteDAO) {
		this.processojuridicoclienteDAO = processojuridicoclienteDAO;
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Processojuridicocliente> findForFichaProcessoJuridico(String whereIn) {
		return processojuridicoclienteDAO.findForFichaProcessoJuridico(whereIn);
	}

	public Integer countByParteContraria(Partecontraria partecontraria) {
		return processojuridicoclienteDAO.countByParteContraria(partecontraria);
	}	
}
