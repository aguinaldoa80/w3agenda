package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.dao.RomaneioorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RomaneioorigemService extends GenericService<Romaneioorigem> {

	private RomaneioorigemDAO romaneioorigemDAO;
	
	public void setRomaneioorigemDAO(RomaneioorigemDAO romaneioorigemDAO) {
		this.romaneioorigemDAO = romaneioorigemDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public List<Romaneioorigem> findByRequisicaomaterial(String whereIn) {
		return romaneioorigemDAO.findByRequisicaomaterial(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param entrega
	 * @return
	 * @author Lucas Costa
	 * @since 05/11/2014
	 */
	public List<Romaneioorigem> findForValidaRomaneioEntrega(Entrega entrega) {
		return romaneioorigemDAO.findForValidaRomaneioEntrega(entrega);
	}

}
