package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.MovimentacaoEstoqueHistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialunidademedidaVO;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.dao.InventarioDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventarioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InventarioService extends GenericService<Inventario>{

	private InventarioDAO inventarioDAO;
	private MaterialService materialService;
	private MaterialunidademedidaService materialunidademedidaService;
	private ParametrogeralService parametrogeralService;
	private MovimentacaoestoqueService movimentacaoestoqueService;	
	private RateioService rateioService;
	private LocalarmazenagemService localarmazenagemService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private InventariomaterialService inventariomaterialService;
	
	public void setMovimentacaoEstoqueHistoricoService(MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setInventarioDAO(InventarioDAO inventarioDAO) {this.inventarioDAO = inventarioDAO;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setInventariomaterialService(InventariomaterialService inventariomaterialService) {this.inventariomaterialService = inventariomaterialService;}
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Inventario> findForSpedRegistroH010(SpedarquivoFiltro filtro) {
		return inventarioDAO.findForSpedRegistroH010(filtro);
	}
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Inventario> findForSpedRegistroK200(SpedarquivoFiltro filtro) {
		return inventarioDAO.findForSpedRegistroK200(filtro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.InventarioDAO#findForSIntegraRegistro74(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Inventario findForSIntegraRegistro74(SintegraFiltro filtro) {
		return inventarioDAO.findForSIntegraRegistro74(filtro);
	}
	
	public Inventario findForSagefiscal(SagefiscalFiltro filtro) {
		return inventarioDAO.findForSagefiscal(filtro);
	}

	/**
	 * M�todo que gera a String para o CSV de invent�rio
	 *
	 * @param inventario
	 * @return
	 * @author Luiz Fernando
	 * @since 20/09/2013
	 */
	public String gerarCSVInventario(Inventario inventario) {
		if(inventario != null && inventario.getCdinventario() != null && inventario.getListaInventariomaterial() == null){
			inventario.setListaInventariomaterial(inventariomaterialService.findByInventario(inventario));
		}
		if(inventario == null || inventario.getListaInventariomaterial() == null || inventario.getListaInventariomaterial().isEmpty()) 
			return "";
		
		StringBuilder rel = new StringBuilder();
		String whereInMaterial = CollectionsUtil.listAndConcatenate(inventario.getListaInventariomaterial(), "material.cdmaterial", ",");
		boolean controleGado = parametrogeralService.getBoolean(Parametrogeral.CONTROLE_GADO);
		boolean ocultaCusto = parametrogeralService.getBoolean(Parametrogeral.OCULTAR_CUSTO_ESTOQUE);
		boolean qtdeDisponivelConsiderarReservado = parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO);
		String casasDecimaisArredondamentoStr = parametrogeralService.getValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
		Integer casasDecimaisArredondamento = null;
		
		if (casasDecimaisArredondamentoStr != null && casasDecimaisArredondamentoStr.length() > 0) {
			casasDecimaisArredondamento = Integer.parseInt(casasDecimaisArredondamentoStr);
		}
		
		if(!ocultaCusto){
			ocultaCusto = !SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE);
		}
		
		final List<Material> listaMaterial = materialService.findListagem(whereInMaterial, "", true,null);
		Map<Integer,Double> mapaMateriais = new HashMap<Integer,Double>();
		
		rel.append("Local;");
		rel.append("Refer�ncia;");
		rel.append("C�digo do Material;");
		rel.append("Identificador;");
		rel.append("Material;");
		rel.append("Unidade Medida;");
		if(controleGado){
			rel.append("Peso bruto;");
		}
		
		if(qtdeDisponivelConsiderarReservado){
			rel.append("Qtd. Atual;");
		}else {
			rel.append("Qtd. Dispon�vel;");			
		}
		if(!ocultaCusto){
			rel.append("Valor Unit�rio;");
			rel.append("Total;");
		}
		rel.append("Total Peso L�quido;");
		rel.append("Total Peso Bruto;");
		rel.append("Grupo de Material;");
		rel.append("\n");
		
		Double qtdetotal = 0.0;
		Double valortotal = 0.0;
		Double valortotalcusto = 0.0;
		
		String formataValorStr = "#,###", dfStr = "###";
		if (casasDecimaisArredondamento > 0) {
			formataValorStr += ".";
			dfStr += ".";
		}
		
		for (int i = 0 ; i < casasDecimaisArredondamento ; i++) {
			formataValorStr += "0";
			dfStr += "0";
		}
		
		DecimalFormat df = new DecimalFormat(dfStr);
		DecimalFormat formataValor = new DecimalFormat(formataValorStr);
		
		Material mat = null;
		String valor = "";
		String qtde = "";
		
		if(inventario.getListaInventariomaterial() != null && inventario.getListaInventariomaterial().size() > 0){
			
			
			Collections.sort(inventario.getListaInventariomaterial(), new Comparator<Inventariomaterial>(){
				public int compare(Inventariomaterial a1, Inventariomaterial a2){
					Material m1 = listaMaterial.get(listaMaterial.indexOf(a1.getMaterial()));
					Material m2 = listaMaterial.get(listaMaterial.indexOf(a2.getMaterial()));
					if(m1.getMaterialgrupo() != null && m1.getMaterialgrupo().getNome() != null){
						if(m2.getMaterialgrupo() != null && m2.getMaterialgrupo().getNome() != null){							
							return m1.getMaterialgrupo().getNome().compareTo(m2.getMaterialgrupo().getNome());
						} else{
							return -1;
						}
					}else if(m2.getMaterialgrupo() != null && m2.getMaterialgrupo().getNome() != null){	
						return 1;
					} else{
						return 0;
					}
				}					
			});
			
			for (Inventariomaterial im : inventario.getListaInventariomaterial()) {
				mat = im.getMaterial();
				mat = listaMaterial.get(listaMaterial.indexOf(mat));
				
				if(inventario.getLocalarmazenagem() != null)
					rel.append(inventario.getLocalarmazenagem().getNome());
				rel.append(";");
				rel.append(mat.getReferencia()).append(";");
				rel.append((mat.getCdmaterial() != null ? mat.getCdmaterial().toString() : "")).append(";");
				rel.append((mat.getIdentificacao() != null ? mat.getIdentificacao() : "")).append(";");
				rel.append("\"").append(mat.getNome().replaceAll("\"", "")).append("\"").append(";");
				
				if(mat.getUnidademedida() != null && mat.getUnidademedida().getNome() != null){
					rel.append(mat.getUnidademedida().getNome());
				}
				rel.append(";");
				
				if(controleGado){
					if(mat.getPesobruto() != null){
						rel.append(mat.getPesobruto().toString());
					}
					rel.append(";");
				}
				
				if(im.getQtde() != null){
					if(im.getMaterial().getCdmaterial()!= null && mapaMateriais.get(im.getMaterial().getCdmaterial())!=null){
						Double qtdeAux = mapaMateriais.get(im.getMaterial().getCdmaterial()).doubleValue();
						mapaMateriais.put(im.getMaterial().getCdmaterial(), im.getQtde() + qtdeAux);
					}else if(im.getMaterial().getCdmaterial()!= null){
						mapaMateriais.put(im.getMaterial().getCdmaterial(), im.getQtde());
					}
						
					qtde = df.format(im.getQtde());
					rel.append(qtde.replace(".", ","));
					qtdetotal += im.getQtde();
				}
				rel.append(";");
				
				if(!ocultaCusto){
					if(im.getValorUnitario() != null){
						valor = formataValor.format(im.getValorUnitario());
						rel.append(valor);	
					}
					rel.append(";");
					
					if(im.getValorUnitario() != null){
						Double valorAux = im.getValorUnitario() * im.getQtde();
						valor = formataValor.format(valorAux);
						rel.append(valor);
						valortotal += valorAux;
						valortotalcusto += im.getValorUnitario();
					}
					rel.append(";");
				}
				
				if(mat.getPeso() != null){
					rel.append(im.getQtde() != null ? SinedUtil.roundByParametro(im.getQtde()*mat.getPeso()): "");
				}
				rel.append(";");
				if(mat.getPesobruto() != null){
					rel.append(im.getQtde() != null ? SinedUtil.roundByParametro(im.getQtde()*mat.getPesobruto()): "");
				}
				rel.append(";");
				if(mat.getMaterialgrupo() != null && mat.getMaterialgrupo().getNome() != null){
					rel.append(mat.getMaterialgrupo().getNome());
				}
				rel.append(";");
				
				if(im.getQtde() != null){
					if(mat.getListaMaterialunidademedida() != null && !mat.getListaMaterialunidademedida().isEmpty()){				
						for(Materialunidademedida materialunidademedida : mat.getListaMaterialunidademedida()){
							if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao() && 
									materialunidademedida.getFracao() != null && materialunidademedida.getUnidademedida() != null){						
									
								rel.append("\n;;;;;").append(materialunidademedida.getUnidademedida().getNome());
								qtde = df.format(materialunidademedida.getFracaoQtdereferencia() * im.getQtde());
								rel.append(";").append(qtde.replace(".", ","));
							}
						}
						
					}
				}
				
				rel.append("\n");
			}
			
			rel
			.append("\n\n;;;;;;")
			.append(df.format(qtdetotal))
			.append(";");
			if(!ocultaCusto){
				rel
				.append(formataValor.format(valortotalcusto))
				.append(";")
				.append(formataValor.format(valortotal));
			}
		}
		
		List<MaterialunidademedidaVO> listaMaterialunidademedidaVO = materialunidademedidaService.getTotaisUnidadeMedida(whereInMaterial, mapaMateriais); 
		
		rel.append("\n\n\n\n\n");
		rel.append("Unidade de Medida;");
		if(!ocultaCusto){
			rel.append("Total;");
		}
//		rel.append("Qtde;");
		rel.append("\n");
		
		for (MaterialunidademedidaVO mum : listaMaterialunidademedidaVO) {
			rel.append(mum.getDescricao()).append(";");
			if(!ocultaCusto){
				rel.append(mum.getTotal() != null ? formataValor.format(mum.getTotal()) : "0").append(";");
			}
//			rel.append(mum.getQtde()).append(";");
			rel.append("\n");
		}
		
		return rel.toString();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @param whereIn
	 * @return
	 * @author Lucas Costa
	 */
	public List<Inventario> findByEmpresa(Empresa empresa, Date dtinventario,InventarioTipo inventarioTipo) {
		return inventarioDAO.findByEmpresa(empresa, dtinventario,inventarioTipo);
	}
	
	public List<Inventario> findByEmpresa(Empresa empresa) {
		return findByEmpresa(empresa, null,null);
	}
	public List<Inventario> findForSpedRegistroK280(SpedarquivoFiltro filtro) {
		return inventarioDAO.findForSpedRegistroK280(filtro);
	}
	public Inventario buscarInventarioOrigem(Inventario inventarioAux) {
		 Inventario origem = inventarioDAO.buscarInventarioOrigem(inventarioAux);
		 return origem.getInventarioOrigem();
	}
	
	public Resource generateCsvReport(InventarioFiltro filtro, String nomeArquivo) {
		List<Inventario> lista = inventarioDAO.findForListagemReport(filtro);
		
		String [] fields = {"cdinventario", "empresa.nomefantasia", "mesanoAux"};
		String cabecalho = "\"Id\";\"Empresa\";\"M�s/ano\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
			
		for(Inventario bean : lista) {
			beanToCSV(fields, csv, bean);
		}
			
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasNotAtSituacao(String whereIn, InventarioSituacao... situacoes) {
		return inventarioDAO.hasNotAtSituacao(whereIn, situacoes);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param inventarioSituacao
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public void atualizaSituacao(String whereIn, InventarioSituacao inventarioSituacao) {
		inventarioDAO.atualizaSituacao(whereIn, inventarioSituacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param inventarioOrigem
	 * @return
	 * @since 14/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Inventario> findAjustesByOrigem(Inventario inventarioOrigem) {
		return inventarioDAO.findAjustesByOrigem(inventarioOrigem);
	}
	
	/**
	 * Registra movimenta��o de estoque a partir de um invent�rio de ajuste.
	 *
	 * @param inventario
	 * @param listaInventariomaterial
	 * @since 14/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacaoestoque> registraMovimentacaoestoqueForAjuste(Inventario inventario, List<Inventariomaterial> listaInventariomaterial) {
		List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
		Date dtmovimentacaoMesAtual = SinedDateUtils.lastDateOfMonth(inventario.getDtinventario());
		Date dtmovimentacaoMesPosterior = SinedDateUtils.firstDateOfMonth(SinedDateUtils.addMesData(inventario.getDtinventario(), 1));
		
		boolean hasInventarioPosterior = this.hasInventarioPosterior(inventario);
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			Double diff = inventariomaterial.getQtdeOrigem() - inventariomaterial.getQtde();
			Double quantidade = diff < 0 ? (diff * -1d) : diff;
			
			Movimentacaoestoquetipo movimentacaoestoquetipoMesAtual = diff < 0 ? Movimentacaoestoquetipo.ENTRADA : Movimentacaoestoquetipo.SAIDA;
			Movimentacaoestoque movimentacaoestoqueMesAtual = makeMovimentacaoestoque(
					inventario, 
					dtmovimentacaoMesAtual, 
					inventariomaterial, 
					quantidade, 
					movimentacaoestoquetipoMesAtual);
			
			listaMovimentacaoestoque.add(movimentacaoestoqueMesAtual);
			
			if(hasInventarioPosterior){
				Movimentacaoestoquetipo movimentacaoestoquetipoMesPosterior = diff > 0 ? Movimentacaoestoquetipo.ENTRADA : Movimentacaoestoquetipo.SAIDA;
				Movimentacaoestoque movimentacaoestoqueMesPosterior = makeMovimentacaoestoque(
						inventario, 
						dtmovimentacaoMesPosterior, 
						inventariomaterial, 
						quantidade, 
						movimentacaoestoquetipoMesPosterior);
				
				listaMovimentacaoestoque.add(movimentacaoestoqueMesPosterior);
			}
		}
		
		return listaMovimentacaoestoque;
	}
	
	/**
	 * Cria movimenta��o de estoque de um ajuste de invent�rio.
	 *
	 * @param inventario
	 * @param dtmovimentacao
	 * @param inventariomaterial
	 * @param quantidade
	 * @param movimentacaoestoquetipo
	 * @return
	 * @since 18/10/2019
	 * @author Rodrigo Freitas
	 */
	private Movimentacaoestoque makeMovimentacaoestoque(Inventario inventario, Date dtmovimentacao, Inventariomaterial inventariomaterial, Double quantidade, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		Material material = materialService.loadForMaterialRateioEstoque(inventariomaterial.getMaterial());
		Localarmazenagem localarmazenagem = localarmazenagemService.loadWithCentrocusto(inventario.getLocalarmazenagem());
		Contagerencial contaGerencial = material.getMaterialRateioEstoque() != null ? (movimentacaoestoquetipo.equals(Movimentacaoestoquetipo.ENTRADA) ? 
											material.getMaterialRateioEstoque().getContaGerencialAjusteEntrada() : 
											material.getMaterialRateioEstoque().getContaGerencialAjusteSaida()) : null;
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setInventario(inventario);
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setMaterial(material);
		movimentacaoestoque.setMovimentacaoestoquetipo(movimentacaoestoquetipo);
		movimentacaoestoque.setQtde(quantidade);
		movimentacaoestoque.setDtmovimentacao(dtmovimentacao);
		movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
		movimentacaoestoque.setLoteestoque(inventariomaterial.getLoteestoque());
		movimentacaoestoque.setEmpresa(inventario.getEmpresa());
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setValor(inventariomaterial.getValorUnitario());
		movimentacaoestoque.setValorcusto(inventariomaterial.getValorUnitario());
		movimentacaoestoque.setProjeto(inventario.getProjeto());
		
		movimentacaoestoque.setContaGerencial(contaGerencial);
		movimentacaoestoque.setCentrocusto(localarmazenagem.getCentrocusto());
		movimentacaoestoque.setProjetoRateio(inventario.getProjeto());
		movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque));
		
		movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
		
		MovimentacaoEstoqueHistorico movimentacaoEstoqueHistorico = new MovimentacaoEstoqueHistorico();
		movimentacaoEstoqueHistorico.setMovimentacaoEstoque(movimentacaoestoque);
		movimentacaoEstoqueHistorico.setMovimentacaoEstoqueAcao(MovimentacaoEstoqueAcao.CRIAR);
		movimentacaoEstoqueHistorico.setObservacao("Ajuste do invent�rio " + this.makeLinkHistorico(inventario));
		movimentacaoEstoqueHistoricoService.saveOrUpdate(movimentacaoEstoqueHistorico);
		
		return movimentacaoestoque;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param inventario
	 * @return
	 * @since 18/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasInventarioPosterior(Inventario inventario) {
		return inventarioDAO.hasInventarioPosterior(inventario);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param data
	 * @param empresa
	 * @param localarmazenagem
	 * @param projeto
	 * @return
	 * @since 29/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(Date data, Empresa empresa, Localarmazenagem localarmazenagem, Projeto projeto){
		return inventarioDAO.hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(data, empresa, localarmazenagem, projeto);
	}
	
	public boolean hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(Date data, Empresa empresa, Localarmazenagem localarmazenagem, Projeto projeto){
		return hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(data, empresa, localarmazenagem, projeto, null);
	}
	
	public boolean hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(Date data, Empresa empresa, Localarmazenagem localarmazenagem, Projeto projeto, InventarioTipo inventarioTipo){
		if(parametrogeralService.getBoolean(Parametrogeral.PERMITIR_MOVIMENTAR_ESTOQUE_PERIODO_INVENTARIO_GERADO)) return false;
		
		return inventarioDAO.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(data, empresa, localarmazenagem, projeto, inventarioTipo);
	}
	
	/**
	 * Verifica se existe invent�rio apos os par�metros da movimenta��o de estoque.
	 *
	 * @param movimentacaoestoque
	 * @return
	 * @since 29/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(Movimentacaoestoque movimentacaoestoque){
		return this.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(
				movimentacaoestoque.getDtmovimentacao(), 
				movimentacaoestoque.getEmpresa(),
				movimentacaoestoque.getLocalarmazenagem(),
				movimentacaoestoque.getProjeto()
		);
	}
	
	/**
	 * Cria o link para o hist�rico do invent�rio.
	 *
	 * @param inventario
	 * @return
	 * @since 18/10/2019
	 * @author Rodrigo Freitas
	 */
	public String makeLinkHistorico(Inventario inventario) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<a href=\"javascript:visualizarInventario(")
			.append(inventario.getCdinventario())
			.append(");\">")
			.append(inventario.getCdinventario())
			.append("</a>");
		
		return sb.toString();
	}
	
	public String getErrosInventarioSubsequente(String whereIn) {
		String erro = "";
		
		List<Inventario> listaInventario = this.carregarInventario(whereIn);
		
		for (Inventario inventario : listaInventario) {
			if (inventarioDAO.existeInveitarioSubsequente(inventario)) {
				if (erro.equals("")) {
					erro = "N�o � poss�vel cancelar invent�rio com outro invent�rio subsequente aberto/autorizado.\nInvent�rios com erro: " + inventario.getCdinventario() + ", ";
				} else {
					erro += inventario.getCdinventario() + ", ";
				}
			}
		}
		
		if (!erro.equals("")) {
			erro = erro.substring(0, erro.length() - 2);
		}
		
		return erro;
	}
	
	public List<Inventario> carregarInventario(String whereIn) {
		return inventarioDAO.carregarInventario(whereIn);
	}
}
