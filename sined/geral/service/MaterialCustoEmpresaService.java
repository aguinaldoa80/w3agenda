package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Map;

import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.dao.MaterialCustoEmpresaDAO;

public class MaterialCustoEmpresaService extends br.com.linkcom.sined.util.neo.persistence.GenericService<MaterialCustoEmpresa> {
	
	private MaterialCustoEmpresaDAO materialCustoEmpresaDAO;
	
	public void setMaterialCustoEmpresaDAO(MaterialCustoEmpresaDAO materialCustoEmpresaDAO) { this.materialCustoEmpresaDAO = materialCustoEmpresaDAO; }
	
	public List<MaterialCustoEmpresa> findByMaterial(Material material, Empresa empresa) {
		return materialCustoEmpresaDAO.findByMaterial(material, empresa);
	}
	
	public void updateValorCusto(MaterialCustoEmpresa materialCustoEmpresa, Double valorCustoMedio) {
		materialCustoEmpresaDAO.updateValorCusto(materialCustoEmpresa, valorCustoMedio);
	}
	
	public void updateQuantidadeReferencia(MaterialCustoEmpresa materialCustoEmpresa, Double quantidade) {
		materialCustoEmpresaDAO.updateQuantidadeReferencia(materialCustoEmpresa, quantidade);
	}
	
	public void updateValorUltimaCompra(MaterialCustoEmpresa materialCustoEmpresa, Double valorUltimaCompra) {
		materialCustoEmpresaDAO.updateValorUltimaCompra(materialCustoEmpresa, valorUltimaCompra);
	}
	
	public MaterialCustoEmpresa loadForSpedPisCofins(Material material, Empresa empresa) {
		return materialCustoEmpresaDAO.loadforSpedPisCofins(material, empresa);
	}
	
	public ContaContabil getContaContabilCustoEmpresa(Map<String, ContaContabil> mapaEmpresaMaterialContaContabil, Material material, Empresa empresa) {
		if(material != null && material.getCdmaterial() != null && empresa != null && empresa.getCdpessoa() != null){
			String chave = empresa.getCdpessoa() + "-" + material.getCdmaterial();
			
			if(mapaEmpresaMaterialContaContabil.containsKey(chave)){
				return mapaEmpresaMaterialContaContabil.get(chave);
			}else{
				MaterialCustoEmpresa materialCustoEmpresa = loadForSpedPisCofins(material, empresa);
				mapaEmpresaMaterialContaContabil.put(chave, materialCustoEmpresa != null ? materialCustoEmpresa.getContaContabil() : null);
				if(materialCustoEmpresa != null && materialCustoEmpresa.getContaContabil() != null){
					return materialCustoEmpresa.getContaContabil();
				}
			}
		}
		return null;
	}
	
	public MaterialCustoEmpresa loadForLancamentoContabil(Material material, Empresa empresa) {
		return materialCustoEmpresaDAO.loadForLancamentoContabil(material, empresa);
	}
}
