package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialoperador;
import br.com.linkcom.sined.geral.dao.ProducaoordemmaterialoperadorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemmaterialoperadorService extends GenericService<Producaoordemmaterialoperador> {

	private ProducaoordemmaterialoperadorDAO producaoordemmaterialoperadorDAO;
	
	public void setProducaoordemmaterialoperadorDAO(ProducaoordemmaterialoperadorDAO producaoordemmaterialoperadorDAO) {this.producaoordemmaterialoperadorDAO = producaoordemmaterialoperadorDAO;}
	
	public List<Producaoordemmaterialoperador> findByProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		return producaoordemmaterialoperadorDAO.findByProducaoordemmaterial(producaoordemmaterial);
	}
	
	public void delete(Producaoordemmaterial producaoordemmaterial) {
		producaoordemmaterialoperadorDAO.delete(producaoordemmaterial);
	}
	
	public void delete(Producaoordem producaoordem) {
		producaoordemmaterialoperadorDAO.delete(producaoordem);
	}

	public void preencherQtdeRegistroPerda(Producaoordemmaterial producaoordemmaterial) {
		if(producaoordemmaterial != null && producaoordemmaterial.getCdproducaoordemmaterial() != null){
			List<Producaoordemmaterialoperador> lista = findByProducaoordemmaterial(producaoordemmaterial);
			if(SinedUtil.isListNotEmpty(lista)){
				Double qtdeRegistro = 0d;
				Double qtdeRegistroperda = 0d;
				for(Producaoordemmaterialoperador item : lista){
					if(item.getCdproducaoordemmaterialoperador() == null){//Somente se o registro � novo. Feito isso pra n�o contabilizar registro de produ��o j� realizada, considerando que a rotina de registrar produ��o em lote permite realizar o processo por partes. 
						if(item.getQtdeproduzido() != null){
							qtdeRegistro += item.getQtdeproduzido();
						}
						if(item.getQtdeperdadescarte() != null){
							qtdeRegistroperda += item.getQtdeperdadescarte();
						}
					}
				}
				if(producaoordemmaterial.getQtderegistro() != null){
					qtdeRegistro += producaoordemmaterial.getQtderegistro();
				}
				if(producaoordemmaterial.getQtderegistroperda() != null){
					qtdeRegistroperda += producaoordemmaterial.getQtderegistroperda();
				}
				producaoordemmaterial.setQtderegistro(qtdeRegistro);
				producaoordemmaterial.setQtderegistroperda(qtdeRegistroperda);
			}
		}
	}
}
