package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratopedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.dao.ContratohistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratohistoricoService extends GenericService<Contratohistorico> {

	private static ContratohistoricoService instance;
	private ContratohistoricoDAO contratohistoricoDAO;
	private ContratohistoricoService contratohistoricoService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private ContratopedidovendaService contratopedidovendaService;
	
	public void setContratopedidovendaService(ContratopedidovendaService contratopedidovendaService) {this.contratopedidovendaService = contratopedidovendaService;}
	public void setContratohistoricoDAO(ContratohistoricoDAO contratohistoricoDAO) {this.contratohistoricoDAO = contratohistoricoDAO;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}

	public static ContratohistoricoService getInstance() {
		if(instance==null)
			instance = Neo.getObject(ContratohistoricoService.class);
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratohistoricoDAO#findByContrato
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratohistorico> findByContrato(Contrato bean) {
		return contratohistoricoDAO.findByContrato(bean);
	}
	
	public void criaHistoricoForContrato(Contratofaturalocacao bean, Contrato contrato) {
		if(bean != null && contrato != null){
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
			contratohistorico.setAcao(Contratoacao.FECHAMENTO_ROMANEIO);
			contratohistorico.setContrato(contrato);
			if(bean.getIndenizacao() != null && bean.getIndenizacao()){
				contratohistorico.setObservacao("Fatura de indeniza��o " + contratofaturalocacaoService.createLink(bean) + " gerada.");				
			}
			else{
				contratohistorico.setObservacao("Fatura de loca��o " + contratofaturalocacaoService.createLink(bean) + " gerada.");				
			}
			
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
	}
	
	public void criaHistoricoForPedidovendaWS(Contrato contrato, Pedidovenda pedidovenda) {
		if(pedidovenda!=null && contrato!=null){
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			contratohistorico.setContrato(contrato);
			contratohistorico.setObservacao("Pedido de venda criado: <a href=\"javascript:visualizarPedidovenda("+pedidovenda.getCdpedidovenda()+")\">"+pedidovenda.getCdpedidovenda()+"</a>.");				
			contratohistoricoService.saveOrUpdate(contratohistorico);
			
			Contratopedidovenda contratopedidovenda = new Contratopedidovenda();
			contratopedidovenda.setContrato(contrato);
			contratopedidovenda.setPedidovenda(pedidovenda);
			contratopedidovendaService.saveOrUpdate(contratopedidovenda);
		}
	}
	
	public void  criaHistoricoForAgendainteracao(Contrato contrato, List<Agendainteracao> listaagendainteracao) {
		String linkAgendas = "";
		int count = 0;
		for(Agendainteracao agendainteracao: listaagendainteracao){
			count++;
			linkAgendas += "<a href=\"javascript:visualizarAgendainteracao("+agendainteracao.getCdagendainteracao()+")\">"+agendainteracao.getCdagendainteracao()+"</a>";
			if(count < listaagendainteracao.size()){
				linkAgendas += ", ";	
			}
		}
		if(contrato != null && linkAgendas != ""){
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			contratohistorico.setContrato(contrato);
			contratohistorico.setObservacao("Gerada(s) a(s) intera��o(�es) "+linkAgendas+".");				
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
	}
	public Contratohistorico findForVisualizacao(Contratohistorico contratohistorico) {
		return contratohistoricoDAO.findForVisualizacao(contratohistorico);
	}

}
