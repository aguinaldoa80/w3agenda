package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.dao.ClassificacaoDAO;

public class ClassificacaoService extends GenericService<Classificacao>{

	private ClassificacaoDAO classificacaoDAO;
	private static ClassificacaoService instance;
	
	public void setClassificacaoDAO(ClassificacaoDAO classificacaoDAO) {this.classificacaoDAO = classificacaoDAO;}
	
	public static ClassificacaoService getInstance() {
		if (instance == null)
			instance = Neo.getObject(ClassificacaoService.class);
		
		return instance;
	}
	
	public void preparaBeanParaSalvar(Classificacao bean) {
		//setar o contagerencialpai.
		if (bean.getPai() != null) {
			bean.setClassificacaosuperior(bean.getPai());
		}

		if (bean.getItem() == null) {
			//setar o campo item para gera��o do identificador
			if (bean.getClassificacaosuperior() != null && bean.getClassificacaosuperior().getCdclassificacao() != null) {
				List<Classificacao> listaFilhos = findFilhos(bean.getClassificacaosuperior());
				if (listaFilhos.size() > 0) {
					bean.setItem(listaFilhos.get(0).getItem() + 1);
				} else {
					bean.setItem(1);
				}
			} else {
				List<Classificacao> listaPais = findRaiz();
				if (listaPais.size() > 0) {
					bean.setItem(listaPais.get(0).getItem() + 1);
				} else {
					bean.setItem(1);
				}
			}
		}
	}
	
	public Classificacao carregaClassificacao(Classificacao bean) {
		return classificacaoDAO.carregaClassificacao(bean);
	}
	
	public List<Classificacao> findFilhos(Classificacao pai){
		return classificacaoDAO.findFilhos(pai);
	}
	
	public List<Classificacao> findRaiz(){
		return classificacaoDAO.findRaiz();
	}
	
	public Boolean isAnalitica(Classificacao classificacao){
		return classificacaoDAO.haveRegister(classificacao);
	}
	
	public List<Classificacao> findAutocomplete(String q) {
		return classificacaoDAO.findAutocompleteFolhas(q);
	}
	
	public List<Classificacao> findForTreeView(){
		return classificacaoDAO.findForTreeView();
	}
	
	public Classificacao loadWithIdentificador(Classificacao classificacao) {
		return classificacaoDAO.loadWithIdentificador(classificacao);
	}
	
	private List<Classificacao> somenteFilhos(Classificacao pai, List<Classificacao> lista) {
		List<Classificacao> filhos = new ArrayList<Classificacao>();
		for (Classificacao c : lista) {
			if(c.getClassificacaosuperior() != null && 
					c.getClassificacaosuperior().getCdclassificacao() != null &&
					c.getClassificacaosuperior().getCdclassificacao().equals(pai.getCdclassificacao())){
				c.setFilhos(somenteFilhos(c, lista));
				filhos.add(c);
			}
		}
		return filhos;
	}
	
	public List<Classificacao> findTreeView() {
		List<Classificacao> lista = this.findForTreeView();
		
		List<Classificacao> raiz = new ArrayList<Classificacao>();
		for (Classificacao c : lista) {
			if(c.getClassificacaosuperior() == null || c.getClassificacaosuperior().getCdclassificacao() == null){
				raiz.add(c);
			}
		}
		for (Classificacao pai : raiz) {
			pai.setFilhos(this.somenteFilhos(pai, lista));
		}
		return raiz;
	}
	
}
