package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.AgendamentoHistorico;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.DespesaviagemHistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.GeraMovimentacao;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.MovimentacaoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoOcorrenciaTarifa;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.state.AgendamentoAcao;
import br.com.linkcom.sined.geral.dao.MovimentacaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ResumoDocumentotipoVenda;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.GeraMovimentacaocheque;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.MovimentacaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.process.ArquivoConciliacaoProcess;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoCAIXABean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ConciliacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.filter.GerarArquivoRemessaConfiguravelChequeFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ChequeMovimentacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.CopiaChequeReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.CopiaChequeSubReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ExtratoContaSubReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.CopiaChequeReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ExtratoContaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.CustooperacionalFiltro;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoService extends GenericService<Movimentacao>{

	private AgendamentoService agendamentoService;
	private AgendamentoHistoricoService agendamentoHistoricoService;
	private DocumentoService documentoService;
	private ContaService contaService;
	private RateioitemService rateioitemService;
	private MovimentacaoDAO movimentacaoDAO;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private MovimentacaoService movimentacaoService;
	private ArquivobancarioService arquivobancarioService;
	private MovimentacaoCartaocreditoService movimentacaoCartaocreditoService;
	private DocumentohistoricoService documentohistoricoService;
	private CaixaService caixaService;
	private NotaDocumentoService notaDocumentoService;
	private ChequeService chequeService;
	private RateioService rateioService;
	private ChequehistoricoService chequehistoricoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ParametrogeralService parametrogeralService;
	private TaxaService taxaService;
	private FormapagamentoService formapagamentoService;
	private VendaService vendaService;
	private ValecompraorigemService valecompraorigemService;
	private DespesaviagemHistoricoService despesaviagemHistoricoService;
	private DespesaviagemService despesaviagemService;
	private CentrocustoService centrocustoService;
	private HistoricooperacaoService historicooperacaoService;
	private ContapagarService contapagarService;
	private ValecompraService valecompraService;
	private EmpresaService empresaService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	
	public void setValecompraorigemService(
			ValecompraorigemService valecompraorigemService) {
		this.valecompraorigemService = valecompraorigemService;
	}
	public void setFormapagamentoService(
			FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setAgendamentoHistoricoService(AgendamentoHistoricoService agendamentoHistoricoService) {
		this.agendamentoHistoricoService = agendamentoHistoricoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setMovimentacaoDAO(MovimentacaoDAO movimentacaoDAO) {
		this.movimentacaoDAO = movimentacaoDAO;
	}
	public void setMovimentacaohistoricoService(MovimentacaohistoricoService movimentacaohistoricoService) {
		this.movimentacaohistoricoService = movimentacaohistoricoService;
	}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {
		this.movimentacaoorigemService = movimentacaoorigemService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {
		this.arquivobancarioService = arquivobancarioService;
	}
	public void setMovimentacaoCartaocreditoService(MovimentacaoCartaocreditoService movimentacaoCartaocreditoService) {
		this.movimentacaoCartaocreditoService = movimentacaoCartaocreditoService;
	}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setChequeService(ChequeService chequeService) {
		this.chequeService = chequeService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {
		this.chequehistoricoService = chequehistoricoService;
	}
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setDespesaviagemHistoricoService(DespesaviagemHistoricoService despesaviagemHistoricoService) {
		this.despesaviagemHistoricoService = despesaviagemHistoricoService;
	}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setHistoricooperacaoService(
			HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {
		this.historicoAntecipacaoService = historicoAntecipacaoService;
	}
	
	@Override
	public Movimentacao loadForEntrada(Movimentacao bean) {
		bean = super.loadForEntrada(bean);
		if(bean != null){
			if(bean.getRateio() != null && bean.getRateio().getCdrateio() != null){
				bean.getRateio().setListaRateioitem(rateioitemService.findByRateio(bean.getRateio()));
			}
			if(bean.getEmpresa() != null){
				bean.setEmpresa(empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.razaosocial"));
			}
		}
		return bean;
	}
	
	/*###################################################################################*/
	/*##################### M�todos de refer�ncia ao DAO ################################*/
	/*###################################################################################*/
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Atualiza a a��o de uma movimentacao.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateAcao(Movimentacao)
	 * @param movimentacao
	 * @author Fl�vio Tavares
	 */
	public void updateAcao(Movimentacao movimentacao){
		movimentacaoDAO.updateAcao(movimentacao);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Estorna uma movimenta��o.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#estornarMovimentacao(Movimentacao, Movimentacaoacao)
	 * @param movimentacao
	 * @author Fl�vio Tavares
	 */
	public void estornarMovimentacao(Movimentacao movimentacao){
		movimentacaoDAO.estornarMovimentacao(movimentacao, null);
	}
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForListagemRelatorio
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findForListagemRelatorio(CopiaChequeReportFiltro filtro) {
		return movimentacaoDAO.findForListagemRelatorio(filtro);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findMovimentacaoGerada(Documento)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findMovimentacaoGerada(Documento documento) {
		return movimentacaoDAO.findMovimentacaoGerada(documento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForCopiaCheque
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Movimentacao> findForCopiaCheque(String whereIn) {
		return movimentacaoDAO.findForCopiaCheque(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#nextvalCopiacheque
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer nextvalCopiacheque(){
		return movimentacaoDAO.nextvalCopiacheque();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#isMovimentacaoCancelada
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isMovimentacaoCancelada(Documento documento){
		return movimentacaoDAO.isMovimentacaoCancelada(documento);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega determinados campos de uma <code>movimentacao</code> informados por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#load(Movimentacao, String)
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacao load(Movimentacao bean,String campos){
		return movimentacaoDAO.load(bean, campos);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#listaMovimentcaoNaoCancelada
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long listaMovimentcaoNaoCancelada(String whereIn){
		return movimentacaoDAO.listaMovimentcaoNaoCancelada(whereIn, null);
	}
	
	public Long listaMovimentcaoNaoCancelada(String whereIn, String whereInMovDelete){
		return movimentacaoDAO.listaMovimentcaoNaoCancelada(whereIn, whereInMovDelete);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega lista de <code>Movimentacao</code> com base nos CD's.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#carregaLista(String)
	 * @param cds
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> carregaLista(String cds){
		return movimentacaoDAO.carregaLista(cds);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findMovimentacaoDestino(Movimentacao)
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacao findMovimentacaoDestino(Movimentacao movimentacao){
		return movimentacaoDAO.findMovimentacaoDestino(movimentacao);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * <p>Fornece os dados de origem para relat�rio de Extrato de Conta.
	 * Pega uma lista de movimenta��es que corresponda aos crit�rios do par�metro filtro.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForExtratoContaReport(ExtratoContaReportFiltro)
	 * @see #agrupaMovimentacoesByDocumento(List)
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Movimentacao> findForExtratoContaReport(ExtratoContaReportFiltro filtro) {
		List<Movimentacao> listaMovimentacaoExtratoConta = movimentacaoDAO.findForExtratoContaReport(filtro);
		if(filtro.getCentrocusto() != null && SinedUtil.isListNotEmpty(listaMovimentacaoExtratoConta)){
			Money totalRateio;
			for(Movimentacao movimentacao : listaMovimentacaoExtratoConta){
				if(movimentacao.getRateio() != null && SinedUtil.isListNotEmpty(movimentacao.getRateio().getListaRateioitem())){
					totalRateio = new Money();
					for(Rateioitem rateioitem : movimentacao.getRateio().getListaRateioitem()){
						if(rateioitem.getValor() != null){
							totalRateio = totalRateio.add(rateioitem.getValor());
						}
					}
					movimentacao.setValor(totalRateio);
				}
			}
		}
		
		if(filtro.getExibircontasvinculadas() != null && filtro.getExibircontasvinculadas()){
			StringBuilder nomepessoa;
			StringBuilder descricaodocumento;
			for(Movimentacao m : listaMovimentacaoExtratoConta){
				if(m.getListaMovimentacaoorigem() != null && !m.getListaMovimentacaoorigem().isEmpty()){
					nomepessoa = new StringBuilder();
					descricaodocumento = new StringBuilder();
					for(Movimentacaoorigem mo : m.getListaMovimentacaoorigem()){
						if(mo.getDocumento() != null){
							if(mo.getDocumento().getPessoa() != null && mo.getDocumento().getPessoa().getNome() != null){
								nomepessoa.append(!"".equals(nomepessoa.toString()) ? ", " : "").append(mo.getDocumento().getPessoa().getNome());
							}else if(mo.getDocumento().getOutrospagamento() != null && !"".equals(mo.getDocumento().getOutrospagamento())){
								nomepessoa.append(!"".equals(nomepessoa.toString()) ? ", " : "").append(mo.getDocumento().getOutrospagamento());
							}
							if(mo.getDocumento().getDescricao() != null){
								descricaodocumento.append(!"".equals(descricaodocumento.toString()) ? ", " : "").append(mo.getDocumento().getCddocumento() + " - " + mo.getDocumento().getDescricao());
							}
						}
					}
					m.setNomepessoa(nomepessoa.toString());
					m.setDescricaodocumento(descricaodocumento.toString());
				}
			}
		}
		if(Util.booleans.isTrue(filtro.getAgruparPorDocumento())){
			listaMovimentacaoExtratoConta = this.agrupaMovimentacoesByDocumento(listaMovimentacaoExtratoConta);
		}
		return listaMovimentacaoExtratoConta;
	}
	
	/**
	 * M�todo para agrupar as movimenta��es por documento (campo 'checknum')
	 * 
	 * @see #contains(Collection, Movimentacao)
	 * @param listaMovimentacao
	 * @author Fl�vio Tavares
	 */
	private List<Movimentacao> agrupaMovimentacoesByDocumento(List<Movimentacao> listaMovimentacao) {
		List<Movimentacao> listaAgrupada = new ArrayList<Movimentacao>();
		for (Movimentacao mov : listaMovimentacao) {
			Movimentacao movLista = contains(listaAgrupada, mov);
			if(movLista != null){
				Money valor = movLista.getValor().add(mov.getValor());
				movLista.setValor(valor);
				movLista.appendHistorico(mov.getHistorico());
				List<Rateioitem> listaRateioAdd = new ArrayList<Rateioitem>();
				for(Rateioitem rateioitem: mov.getRateio().getListaRateioitem()){
					boolean possui = false;
					for(Rateioitem rateioitem2: movLista.getRateio().getListaRateioitem()){
						if(rateioitem.getCentrocusto().equals(rateioitem2.getCentrocusto()) &&
							rateioitem.getContagerencial().equals(rateioitem2.getContagerencial()) &&
							((rateioitem.getProjeto() == null && rateioitem2.getProjeto() == null) || rateioitem.getProjeto() != null && rateioitem.getProjeto().equals(rateioitem2.getProjeto()))){
							rateioitem2.setValor(rateioitem2.getValor().add(rateioitem.getValor()));
							possui = true;
							break;
						}
					}
					if(!possui){
						listaRateioAdd.add(rateioitem);
					}
				}
				if(!listaRateioAdd.isEmpty()){
					movLista.getRateio().getListaRateioitem().addAll(listaRateioAdd);
				}
			}else{
				listaAgrupada.add(mov);
			}
		}
		return listaAgrupada;
	}
	
	/**
	 * M�todo para verificar se a movimenta��o informada por par�metro est� contida na lista.
	 * Utiliza o campo checknum para compara��o.
	 * 
	 * @param listaMovimentacao
	 * @param movimentacao
	 * @return A Movimenta��o, caso exista. Do contr�rio retorna null.
	 * @author Fl�vio Tavares
	 */
	private Movimentacao contains(Collection<Movimentacao> listaMovimentacao, Movimentacao movimentacao){
		String checknum1 = movimentacao.getChecknum() != null && !movimentacao.getChecknum().trim().equals("") ? movimentacao.getChecknum().trim() : null; 
		String fitid1 = movimentacao.getFitid() != null && !movimentacao.getFitid().trim().equals("") ? movimentacao.getFitid().trim() : null; 
		Tipooperacao tipooperacao1 = movimentacao.getTipooperacao() != null ? movimentacao.getTipooperacao() : null;
		Date dtmovimentacao1 = movimentacao.getDtmovimentacao();
		
		for (Movimentacao mov : listaMovimentacao) {
			String checknum2 = mov.getChecknum() != null && !mov.getChecknum().trim().equals("") ? mov.getChecknum().trim() : null;
			String fitid2 = mov.getFitid() != null && !mov.getFitid().trim().equals("") ? mov.getFitid().trim() : null;
			Tipooperacao tipooperacao2 = mov.getTipooperacao() != null ? mov.getTipooperacao() : null;
			Date dtmovimentacao2 = mov.getDtmovimentacao();
			
			boolean checknumIgual = (checknum1 != null && checknum2 != null && checknum1.equals(checknum2)) || (checknum1 == null && checknum2 == null);
			boolean tipooperacaoIgual = (tipooperacao1 != null && tipooperacao2 != null && tipooperacao1.equals(tipooperacao2)) || (tipooperacao1 == null && tipooperacao2 == null);
			boolean fitidIgual = (fitid1 != null && fitid2 != null && fitid1.equals(fitid2)) || (fitid1 == null && fitid2 == null);
			boolean dataIgual = (dtmovimentacao1 != null && dtmovimentacao2 != null && SinedDateUtils.equalsIgnoreHour(dtmovimentacao1, dtmovimentacao2)) || (dtmovimentacao1 == null && dtmovimentacao2 == null);
			
			if(checknumIgual && tipooperacaoIgual && fitidIgual && dataIgual){
				return mov;
			}
		}
		return null;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return movimentacaoDAO.findForReportFluxocaixa(filtro, false, false);
	}
	
	public synchronized List<Movimentacao> findAnterioresForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		List<Movimentacao> listaMov = movimentacaoDAO.findForReportFluxocaixa(filtro, true, false);
		List<Movimentacao> listaMovOrigem = movimentacaoDAO.findForReportFluxocaixa(filtro, true, true);
		if(SinedUtil.isListNotEmpty(listaMovOrigem)){
			if(listaMov == null)
				listaMov = new ArrayList<Movimentacao>();
			listaMov.addAll(listaMovOrigem);
		}
		
		for (Movimentacao mov : listaMov) {
			if(mov.getDtbanco() != null){
				mov.setDataAtrasada(mov.getDtbanco());
				mov.setDtbanco(SinedDateUtils.remoteDate());
			}else {
				mov.setDataAtrasada(mov.getDtmovimentacao());
				mov.setDtmovimentacao(SinedDateUtils.remoteDate());
			}
		}
		Collections.sort(listaMov,new Comparator<Movimentacao>(){
			public int compare(Movimentacao o1, Movimentacao o2) {
				return o1.getDataAtrasada().compareTo(o2.getDataAtrasada());
			}
		});
		
		return listaMov;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findMovCartoesForFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	private List<Movimentacao> findMovCartoesForFluxocaixa(FluxocaixaFiltroReport filtro, Boolean considerarApenasAnteriores){ 
		return movimentacaoDAO.findMovCartoesForFluxocaixa(filtro, considerarApenasAnteriores);
	}
	
	/*###################################################################################*/
	/*##################### M�todos de cria��o de relat�rio #############################*/
	/*###################################################################################*/
	
	/**
	 * M�todo para adicionar a lista de Movimenta��o � lista de Fluxo de caixa para gera��o do relat�rio.
	 *
	 * @param listaFluxo
	 * @param listaMovimentacao
	 * @author Fl�vio Tavares
	 * @param isFiltroOrigem 
	 * @param radEvento 
	 */
	public void adicionaMovimentacaoFluxocaixa(List<FluxocaixaBeanReport> listaFluxo, List<Movimentacao> listaMovimentacao, boolean isFiltroOrigem){
		for (Movimentacao mov : listaMovimentacao) {
			listaFluxo.add(new FluxocaixaBeanReport(mov, isFiltroOrigem));
		}
	}
	
	/**
	 * M�todo para buscar uma lista de movimenta��es para o relat�rio de fluxo de caixa.
	 * 
	 * @see #findMovCartoesForFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findForFluxocaixa(FluxocaixaFiltroReport filtro){
		List<Movimentacao> listaMovCartao = this.findMovCartoesForFluxocaixa(filtro, false);
		return movimentacaoCartaocreditoService.processaListaMovCartoesForFluxocaixa(filtro, listaMovCartao);
	}
	
	public List<Movimentacao> findAnterioresForFluxocaixa(FluxocaixaFiltroReport filtro){
		List<Movimentacao> listaMovCartao = this.findMovCartoesForFluxocaixa(filtro, true);
		
		for (Movimentacao mov : listaMovCartao) {
			if(mov.getDtbanco() != null){
				mov.setDataAtrasada(mov.getDtbanco());
				mov.setDtbanco(SinedDateUtils.remoteDate());
			}else {
				mov.setDataAtrasada(mov.getDtmovimentacao());
				mov.setDtmovimentacao(SinedDateUtils.remoteDate());
			}
		}
		Collections.sort(listaMovCartao,new Comparator<Movimentacao>(){
			public int compare(Movimentacao o1, Movimentacao o2) {
				return o1.getDataAtrasada().compareTo(o2.getDataAtrasada());
			}
		});
		
		return listaMovCartao;
	}
	
	
	/**
	 * Cria o relat�rio de c�pia de cheque.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findForCopiaCheque
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#preparaListaBean
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createCopiaChequeReport(WebRequestContext request) {

		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		if(StringUtils.isNotBlank(whereInCheque)){
			List<Movimentacao> lista = findForChequeMovimentacao(whereIn, whereInCheque);
			if(SinedUtil.isListEmpty(lista)){
				throw new SinedException("Nenhuma movimenta��o encontrada.");
			}
			whereIn = CollectionsUtil.listAndConcatenate(lista, "cdmovimentacao", ",");
		}
		
		if (StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}

		String obs = request.getParameter("obs");

		List<Movimentacao> listaMovimentacao = findForCopiaCheque(whereIn);

		List<CopiaChequeReportBean> listaBean = preparaListaBean(listaMovimentacao,obs,true);
		
		Report report = new Report("financeiro/copiacheque");
		Report subreport1 = new Report("financeiro/sub_copiacheque");

		report.addSubReport("SUB_COPIACHEQUE", subreport1);
		report.setDataSource(listaBean);

		return report;
	}


	/**
	 * Prepara a lista de beans do relat�rio de c�pia de cheque.
	 * 
	 * - Query colocada dentro do for provisoriamente, pois n�o est� carregando a lista completa de itens de rateio.
	 * - M�todo pegando informa��es do primeiro documento da lista provisoriamente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#preencheBean
	 * @param listaMovimentacao
	 * @param obs
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<CopiaChequeReportBean> preparaListaBean(List<Movimentacao> listaMovimentacao, String obs, boolean telaCopiaCheque) {

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		DecimalFormat valorformat = new DecimalFormat("R$ ###,###,##0.00");

		List<CopiaChequeReportBean> listaBean = new ArrayList<CopiaChequeReportBean>();
		CopiaChequeReportBean bean = null;


		for (Movimentacao movimentacao : listaMovimentacao) {
			bean = preencheBean(obs, format, valorformat, movimentacao);				
			
			listaBean.add(bean);
		}

		return listaBean;
	}
	
	/**
	 * Preenche o bean para inserir na lista.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#listaItens
	 * - Preenche a lista de itens de rateio do bean.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#preencheCampos
	 * - Preenche os campos com diversos ou o �nico item da lista
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#distinctDataVencimento
	 * - Preenche o campo dtvencimento com diversos ou com uma �nica data distinta
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#distinctFornecedores
	 * - Preenche o campo fornecedores com 50 caracteres de fornecedores distintos ou coloca diversos
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#descricaoContas
	 * - Preenche a lista de descri��o das contas com 6 descri��es ou coloca diversos.
	 * 
	 * @param obs
	 * @param format
	 * @param valorformat
	 * @param movimentacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	private CopiaChequeReportBean preencheBean(String obs, SimpleDateFormat format, DecimalFormat valorformat, Movimentacao movimentacao) {
		CopiaChequeReportBean bean;
		List<Movimentacaoorigem> listaMovimentacaoorigem;

		bean = new CopiaChequeReportBean();

		bean.setBancosacado(movimentacao.getConta().getBanco() != null ? movimentacao.getConta().getBanco().getNome() : "");
		bean.setNumerocheque(movimentacao.getCheque() != null && movimentacao.getCheque().getNumero() != null ? movimentacao.getCheque().getNumero().toString() : "");
		
		if (movimentacao.getDtbanco() == null) {
			bean.setDatacheque(format.format(movimentacao.getDtmovimentacao()));
		} else {
			bean.setDatacheque(format.format(movimentacao.getDtbanco()));
		}
		
		bean.setEmpresa(null);
		bean.setLogo(null);
		
		if(movimentacao.getConta() != null && movimentacao.getConta().getEmpresa() != null && movimentacao.getConta().getEmpresa().getRazaosocialOuNome() != null){
			bean.setEmpresa(movimentacao.getConta().getEmpresa().getRazaosocialOuNome());
			
			if(movimentacao.getConta().getEmpresa().getLogomarca() != null){
				Arquivo rubrica = ArquivoService.getInstance().loadWithContents(movimentacao.getConta().getEmpresa().getLogomarca());
				Image image = ArquivoService.getInstance().loadAsImage(rubrica);
				bean.setLogo(image);
			}
		}

		listaMovimentacaoorigem = descricaoContas(bean, movimentacao);

		distinctFornecedores(bean, listaMovimentacaoorigem);

		distinctDataVencimento(format, bean, listaMovimentacaoorigem);

		preencheCampos(format, bean, listaMovimentacaoorigem);

		bean.setValordocumento(valorformat.format(movimentacao.getValor().getValue().doubleValue()));

		bean.setDigitadopor(((Usuario)Neo.getUser()).getNome());
		if (movimentacao.getConta().getAgencia() != null) {
			bean.setAgenciaconta(movimentacao.getConta().getAgencia() + 
					(movimentacao.getConta().getDvagencia() != null && !movimentacao.getConta().getDvagencia().equals("") ? "-"+movimentacao.getConta().getDvagencia():"")+
					" / " + 
					movimentacao.getConta().getNumero() +
					(movimentacao.getConta().getDvnumero() != null && !movimentacao.getConta().getDvnumero().equals("") ? "-"+movimentacao.getConta().getDvnumero():""));
		}
		bean.setDataatual(format.format(new Date(System.currentTimeMillis())));
		bean.setObservacao(movimentacao.getObservacaocopiacheque() != null ? movimentacao.getObservacaocopiacheque() : "");

		listaItens(valorformat, bean, movimentacao);

		return bean;
	}

	/**
	 * Monta a lista de itens de rateio do bean.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#montagemListaItens
	 * @param valorformat
	 * @param bean
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	private void listaItens(DecimalFormat valorformat, CopiaChequeReportBean bean, Movimentacao movimentacao) {
		CopiaChequeSubReportBean sub;

		List<CopiaChequeSubReportBean> listaSub;
		List<Rateioitem> listaItem;
		listaSub = new ArrayList<CopiaChequeSubReportBean>();


		listaItem = rateioitemService.findByRateio(movimentacao.getRateio());
//		listaItem = movimentacao.getRateio().getListaRateioitem();

		montagemListaItens(bean, listaItem);


		for (Rateioitem rateioitem : listaItem) {
			sub = new CopiaChequeSubReportBean();
			sub.setContagerencial(rateioitem.getContagerencial().getNome());
			sub.setProjeto(rateioitem.getProjeto() != null ? rateioitem.getProjeto().getNome() : "");
			sub.setValor(valorformat.format(rateioitem.getValor().getValue().doubleValue()));
			listaSub.add(sub);
		}

		bean.setListaItens(listaSub);
	}

	/**
	 * <p>Gera um relat�rio de Extrato de Conta.</p>
	 * 
	 * @see #calcularDtSaldoAnterior(ExtratoContaReportFiltro, Conta)
	 * @see #calculaSaldoAtual(Conta, Date)
	 * @see #findForExtratoContaReport(ExtratoContaReportFiltro)
	 * @see #calcularSaldoMovimentacoes(List, Money)
	 * @see #formatarHistorico(String)
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public IReport gerarRelatorioExtratoConta(ExtratoContaReportFiltro filtro) {
		Report report = null;
		if(Boolean.TRUE.equals(filtro.getExibirRateio())){
			report = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoConta_com_rateio"): new Report("/financeiro/extratoConta_com_rateio_retrato");
		}else{
			report = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoConta"): new Report("/financeiro/extratoConta_retrato");
		}
		Conta conta = filtro.getDescricaoVinculo();
		conta = contaService.load(conta);
		
		if(filtro.getExibircontasvinculadas() != null && filtro.getExibircontasvinculadas()){
			report.addParameter("exibircontasvinculadas", filtro.getExibircontasvinculadas());
		}
		
		//Se for cart�o de cr�dito, a descri��o s�o os 4 �ltimos d�gitos do cart�o
		if (filtro.getContaTipo().getCdcontatipo().equals(Contatipo.CARTAO_DE_CREDITO)) {
			String numero = conta.getNumero();
			if ((numero.length()-4) >= 4) {
				numero = numero.substring(numero.length()-4, numero.length());
			}
			conta.setNome(StringUtils.join(new Object[]{conta.getNome(), " - ************", numero}));
		}

		Date dtSaldoAnterior = this.calcularDtSaldoAnterior(filtro.getDtInicio(), filtro.getDtFim(), conta.getDtsaldo(), conta.getContatipo());
		Money valorSaldoAnterior = filtro.getCentrocusto() != null ? centrocustoService.calculaSaldoAtualCentroCusto(filtro.getCentrocusto(), dtSaldoAnterior, filtro.getContaTipo().getCdcontatipo(), 
				 filtro.getDescricaoVinculo().getCdconta()) : this.calculaSaldoAtual(conta, filtro.getDtInicio(), dtSaldoAnterior, filtro.getListaMovimentacaoacao());

		List<Movimentacao> listaMovimentacao = this.findForExtratoContaReport(filtro);
		if (listaMovimentacao == null || listaMovimentacao.isEmpty()) {
			throw new SinedException("N�o h� movimenta��es neste per�odo.");
		}

		Map<Integer, Money> mapSaldoInicial = this.calculaSaldoInicialCentroCusto(listaMovimentacao, filtro);
		this.calcularSaldoMovimentacoes(listaMovimentacao, valorSaldoAnterior);
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			valorSaldoAnterior = mapSaldoInicial.get(filtro.getCentrocusto().getCdcentrocusto());
		}
		
		Report subreport_credito = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoConta_resumoDocumentotipo_credito"): new Report("/financeiro/extratoConta_resumoDocumentotipo_credito_retrato");
		Report subreport_debito = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoConta_resumoDocumentotipo_debito"): new Report("/financeiro/extratoConta_resumoDocumentotipo_debito_retrato");
		Report subreport_totalizadorCentroCusto = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoContaTotalizadorCentroCusto"): new Report("/financeiro/extratoContaTotalizadorCentroCusto_retrato");
		
		report.addParameter("LISTARESUMODOCUMENTOTIPO_CREDITO", this.makeResumoDocumentotipo(filtro, listaMovimentacao, Tipooperacao.TIPO_CREDITO));
		report.addParameter("LISTARESUMODOCUMENTOTIPO_DEBITO", this.makeResumoDocumentotipo(filtro, listaMovimentacao, Tipooperacao.TIPO_DEBITO));
		
		report.addSubReport("SUB_RESUMODOCUMENTOTIPO_CREDITO", subreport_credito);
		report.addSubReport("SUB_RESUMODOCUMENTOTIPO_DEBITO", subreport_debito);
		report.addSubReport("SUB_TOTALIZADORCENTROCUSTO", subreport_totalizadorCentroCusto);
		if(Boolean.TRUE.equals(filtro.getExibirRateio())){
			Report subreport = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoConta_detalheRateio"): new Report("/financeiro/extratoConta_detalheRateio_retrato");
			report.addSubReport("SUB_RATEIO", subreport);
		}
		
		Report subreport2 = Boolean.TRUE.equals(filtro.getModoPaisagem())? new Report("/financeiro/extratoContaTotalizadorMes"): new Report("/financeiro/extratoContaTotalizadorMes_retrato");
		
		ExtratoContaSubReportBean subReportBean;
		Calendar calendar = Calendar.getInstance();
		Map<Integer, ExtratoContaSubReportBean> map = new HashMap<Integer, ExtratoContaSubReportBean>();
		Map<Integer, Centrocusto> maptotalCentroCusto = new HashMap<Integer, Centrocusto>();
		
		/*
		 * Faz a formata��o dos campos do relat�rio.
		 */
		for (Movimentacao movimentacao : listaMovimentacao) {
			//movimentacao.setHistorico(SinedUtil.inserirQuebraLinha(movimentacao.getHistorico(), 30));
			calendar.setTime(movimentacao.getDataMovimentacao());
			
			Integer mes = calendar.get(Calendar.MONTH);
			subReportBean = map.get(mes);
			if(subReportBean == null){
				subReportBean = new ExtratoContaSubReportBean();
				subReportBean.setTotalDebito(new Money(0.0));
				subReportBean.setTotalCredito(new Money(0.0));
				subReportBean.setMes(DateUtils.mesPorExtenso(mes));
			}
			if(movimentacao.getTipooperacao().getNome().equalsIgnoreCase("D")){
				subReportBean.setTotalDebito(subReportBean.getTotalDebito().add(movimentacao.getValor()));
			}if(movimentacao.getTipooperacao().getNome().equalsIgnoreCase("C")){
				subReportBean.setTotalCredito(subReportBean.getTotalCredito().add(movimentacao.getValor()));
			}
			map.put(mes, subReportBean);
			
			if(movimentacao.getRateio() != null && movimentacao.getRateio().getListaRateioitem() != null){
				List<Rateioitem> listarateio = movimentacao.getRateio().getListaRateioitem();
				if(SinedUtil.isListNotEmpty(listarateio)){
					for (Rateioitem rateioitem : listarateio) {
						if(rateioitem != null && rateioitem.getCdrateioitem() != null 
											  && rateioitem.getCentrocusto() != null && rateioitem.getValor() != null
											  && rateioitem.getCentrocusto().getCdcentrocusto() != null){
							if(maptotalCentroCusto.containsKey(rateioitem.getCentrocusto().getCdcentrocusto())){
								if(movimentacao.getTipooperacao().getNome().equalsIgnoreCase("D")){
									Centrocusto c = maptotalCentroCusto.get(rateioitem.getCentrocusto().getCdcentrocusto());
									c.setValorTotalDebito(c.getValorTotalDebito().add(rateioitem.getValor()));
									maptotalCentroCusto.put(c.getCdcentrocusto(), c);
								}else{
									Centrocusto c = maptotalCentroCusto.get(rateioitem.getCentrocusto().getCdcentrocusto());
									c.setValorTotalCredito(c.getValorTotalCredito().add(rateioitem.getValor()));
									maptotalCentroCusto.put(c.getCdcentrocusto(), c);
								}
							}else{
								if(movimentacao.getTipooperacao().getNome().equalsIgnoreCase("D")){
									Centrocusto c = rateioitem.getCentrocusto();
									c.setValorSaldoInicial(mapSaldoInicial.get(c.getCdcentrocusto()));
									c.setValorTotalDebito(rateioitem.getValor());
									maptotalCentroCusto.put(c.getCdcentrocusto(), c);
								}else{
									Centrocusto c = rateioitem.getCentrocusto();
									c.setValorSaldoInicial(mapSaldoInicial.get(c.getCdcentrocusto()));
									c.setValorTotalCredito(rateioitem.getValor());
									maptotalCentroCusto.put(c.getCdcentrocusto(), c);
								}
							}
						}
					}
				}
			}
		}
		List<ExtratoContaSubReportBean> listaSomatorioMes = new ArrayList<ExtratoContaSubReportBean>(map.values());
		List<Centrocusto> listaCentroCusto = new ArrayList<Centrocusto>(maptotalCentroCusto.values());
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			Centrocusto centrocusto = centrocustoService.load(filtro.getCentrocusto());
			report.addParameter("centrocusto", centrocusto != null && centrocusto.getNome() != null ? centrocusto.getNome() : "");
		}
		
		report.addSubReport("SUB_TOTALIZADORMES", subreport2);
		report.addParameter("LISTASOMATORIOMES", listaSomatorioMes);
		report.addParameter("LISTATOTALIZADORCENTROCURTO", listaCentroCusto);
		
		report.setDataSource(listaMovimentacao);
		report.addParameter("conta", conta.getNome());
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		report.addParameter("dataSaldoAnterior", dtSaldoAnterior);
		report.addParameter("valorSaldoAnterior", valorSaldoAnterior);

		return report;
	}
	
	/**
	 * M�todo respons�vel pelo c�culo do saldo inicial dos centros de custo
	 * @param listamovimentacoes
	 * @return
	 * @since 25/11/2016
	 * @author C�sar
	 */
	public Map<Integer, Money> calculaSaldoInicialCentroCusto (List<Movimentacao> listamovimentacoes, ExtratoContaReportFiltro filtro){
		if(!SinedUtil.isListNotEmpty(listamovimentacoes)){
			throw new SinedException("N�o � poss�vel calcular saldo inicial sem movimenta��es.");
		}
		
		Map<Integer, Money> mapvalores = new HashMap<Integer, Money>();
		
		for (Movimentacao movimentacao : listamovimentacoes) {
			if(movimentacao != null && movimentacao.getRateio() != null 
									&& SinedUtil.isListNotEmpty(movimentacao.getRateio().getListaRateioitem())){
				List<Rateioitem> listaRateioitem = movimentacao.getRateio().getListaRateioitem();
				for (Rateioitem rateioitem : listaRateioitem) {
					if(rateioitem != null && rateioitem.getCentrocusto() != null 
										  && rateioitem.getCentrocusto().getCdcentrocusto() != null){
						Centrocusto c = rateioitem.getCentrocusto();
						if(!mapvalores.containsKey(c.getCdcentrocusto())){
							Date dtSaldoAnterior = SinedDateUtils.incrementDate(filtro.getDtInicio(), -1, Calendar.DAY_OF_MONTH);
							Money valor = centrocustoService.calculaSaldoAtualCentroCusto(c, dtSaldoAnterior, filtro.getContaTipo().getCdcontatipo(), 
																							 filtro.getDescricaoVinculo().getCdconta());
							mapvalores.put(c.getCdcentrocusto(), valor);
						}
					}
				}
			}
		}
		return mapvalores;
	}
	
	private List<ResumoDocumentotipoVenda> makeResumoDocumentotipo(ExtratoContaReportFiltro filtro, List<Movimentacao> listaMovimentacao, Tipooperacao tipooperacao) {
		Map<Documentotipo, Money> mapDocumentotipo = new HashMap<Documentotipo, Money>();
		
		Documentotipo documentotipoOutros = new Documentotipo(-1, "Outros");
		List<Documento> listaDocumento = new ArrayList<Documento>();
		for (Movimentacao mov : listaMovimentacao) {
			if(mov.getTipooperacao() == null || !mov.getTipooperacao().equals(tipooperacao)) continue;
			
			List<Movimentacaoorigem> listaMovimentacaoorigem = mov.getListaMovimentacaoorigem();
			if(listaMovimentacaoorigem != null && listaMovimentacaoorigem.size() > 0){
				if(listaMovimentacaoorigem.size() > 1){
					for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
						if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getDocumentotipo() != null){
							if(!listaDocumento.contains(movimentacaoorigem.getDocumento())){
								Documentotipo documentotipo = movimentacaoorigem.getDocumento().getDocumentotipo();
								Money valorDoc = new Money();
								if(filtro.getCentrocusto() != null){
									valorDoc = getValorTotalCentrocusto(movimentacaoorigem.getDocumento(), filtro.getCentrocusto());
								}else {
									valorDoc = movimentacaoorigem.getDocumento().getAux_documento() != null ? 
													movimentacaoorigem.getDocumento().getAux_documento().getValoratual() : 
													movimentacaoorigem.getDocumento().getValor();
								}
								
								if(filtro.getCentrocusto() == null || valorDoc != null){
									if(mapDocumentotipo.containsKey(documentotipo)){
										Money valor = mapDocumentotipo.get(documentotipo);
										valor = valor.add(valorDoc);
										mapDocumentotipo.put(documentotipo, valor);
									} else {
										mapDocumentotipo.put(documentotipo, valorDoc);
									}
									listaDocumento.add(movimentacaoorigem.getDocumento());
								}
							}
						}
					}
				} else {
					Movimentacaoorigem movimentacaoorigem = listaMovimentacaoorigem.get(0);
					if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getDocumentotipo() != null){
						Documentotipo documentotipo = movimentacaoorigem.getDocumento().getDocumentotipo();
						if(mapDocumentotipo.containsKey(documentotipo)){
							Money valor = mapDocumentotipo.get(documentotipo);
							valor = valor.add(mov.getValor());
							mapDocumentotipo.put(documentotipo, valor);
						} else {
							mapDocumentotipo.put(documentotipo, mov.getValor());
						}
					} else {
						if(mapDocumentotipo.containsKey(documentotipoOutros)){
							Money valor = mapDocumentotipo.get(documentotipoOutros);
							valor = valor.add(mov.getValor());
							mapDocumentotipo.put(documentotipoOutros, valor);
						} else {
							mapDocumentotipo.put(documentotipoOutros, mov.getValor());
						}
					}
				}
			} else {
				if(mapDocumentotipo.containsKey(documentotipoOutros)){
					Money valor = mapDocumentotipo.get(documentotipoOutros);
					valor = valor.add(mov.getValor());
					mapDocumentotipo.put(documentotipoOutros, valor);
				} else {
					mapDocumentotipo.put(documentotipoOutros, mov.getValor());
				}
			}
		}
		
		
		List<ResumoDocumentotipoVenda> lista = new ArrayList<ResumoDocumentotipoVenda>();
		Set<Entry<Documentotipo, Money>> entrySet = mapDocumentotipo.entrySet();
		
		if(entrySet != null && entrySet.size() > 0){
			for (Entry<Documentotipo, Money> entry : entrySet) {
				ResumoDocumentotipoVenda resumoDocumentotipoVenda = new ResumoDocumentotipoVenda();
				resumoDocumentotipoVenda.setDocumentotipo(entry.getKey());
				resumoDocumentotipoVenda.setValor(entry.getValue());
				lista.add(resumoDocumentotipoVenda);
			}
			
			Collections.sort(lista,new Comparator<ResumoDocumentotipoVenda>(){
				public int compare(ResumoDocumentotipoVenda o1, ResumoDocumentotipoVenda o2) {
					return o1.getDocumentotipo().getNome().toLowerCase().compareTo(o2.getDocumentotipo().getNome().toLowerCase());
				}
			});
		}
		
		return lista;
	}
	
	private Money getValorTotalCentrocusto(Documento documento,	Centrocusto centrocusto) {
		Money total = null;
		if(centrocusto != null && documento != null && documento.getRateio() != null && 
				SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
			for(Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
				if(centrocusto.equals(rateioitem.getCentrocusto()) && rateioitem.getValor() != null){
					if(total == null) total = new Money();
					total = total.add(rateioitem.getValor());
				}
			}
		}
		return total;
	}
	
	/**
	 * Calcula o saldo atual da conta.
	 * 
	 * @param conta
	 * @param dtSaldoAnterior
	 * @return
	 * @author Hugo Ferreira
	 * @param listaSituacao 
	 */
	private Money calculaSaldoAtual(Conta conta, Date dtInicio, Date dtSaldoAnterior, List<Movimentacaoacao> listaSituacao) {
	
		if (!conta.getContatipo().getCdcontatipo().equals(Contatipo.CARTAO_DE_CREDITO) 
				&& !SinedDateUtils.afterIgnoreHour(dtInicio, dtSaldoAnterior)) {
			return conta.getSaldo();
		}
		
		if(listaSituacao != null && listaSituacao.size() == 1){
			if(listaSituacao.get(0).equals(Movimentacaoacao.NORMAL)){
				return contaService.calculaSaldoAtualNormal(conta, dtSaldoAnterior);
			} else if(listaSituacao.get(0).equals(Movimentacaoacao.CONCILIADA)){
				return contaService.calculaSaldoAtualConciliada(conta, dtSaldoAnterior);
			} else {
				return contaService.calculaSaldoAtual(conta, dtSaldoAnterior);
			}
		} else return contaService.calculaSaldoAtual(conta, dtSaldoAnterior);
	}
	
	/**
	 * Faz o c�lculo da data refer�ncia para obter o saldo anterior.
	 * <P>Regras:</P>
	 * <ul>
	 * 	<li> Cart�o de cr�dito n�o possui a propriedade "dtsaldo"
	 * 	<li> A data final do per�odo no filtro deve ser maior ou igual da data do saldo inicial da conta
	 * 	<li> Se a data in�cio for menor ou igual a data do saldo inicial da conta, ent�o a data do saldo anterior
	 * 		 deve ser a data do saldo inicial da conta
	 * 	<li> Se a data in�cio for maior que a data do saldo inicial da conta, ent�o a data do saldo anterior
	 * 		 deve ser a (data inicial - 1 dia)
	 * </ul>
	 * 
	 * @see br.com.linkcom.sined.util.SinedDateUtils#beforeIgnoreHour(Date, Date)
	 * @see br.com.linkcom.sined.util.SinedDateUtils#incrementDate(Date, int, int)
	 * @param dtInicio
	 * @param dtFim
	 * @param dtSaldo
	 * @param contaTipo
	 * @return
	 * @author Hugo Ferreira
	 */
	private Date calcularDtSaldoAnterior(Date dtInicio, Date dtFim, Date dtSaldo, Contatipo contaTipo) {
		if (dtSaldo == null && !contaTipo.getCdcontatipo().equals(Contatipo.CARTAO_DE_CREDITO)) {
			throw new SinedException("A data do saldo n�o pode ser nula.");
		}
		
		Date dtSaldoAnterior = null;
		
		if (contaTipo.getCdcontatipo().equals(Contatipo.CARTAO_DE_CREDITO)) {
			dtSaldoAnterior = SinedDateUtils.incrementDate(dtInicio, -1, Calendar.DAY_OF_MONTH);
		} else if (SinedDateUtils.beforeIgnoreHour(dtFim, dtSaldo)) {
			throw new SinedException("O per�odo do extrato deve ser superior a data do saldo inicial definido na conta banc�ria.");
		} else if (SinedDateUtils.beforeIgnoreHour(dtInicio, dtSaldo) || SinedDateUtils.equalsIgnoreHour(dtInicio, dtSaldo)) {
			dtSaldoAnterior = dtSaldo;
		} else {
			dtSaldoAnterior = SinedDateUtils.incrementDate(dtInicio, -1, Calendar.DAY_OF_MONTH);
		}
		
		return dtSaldoAnterior;
	}

	/**
	 * <p>Faz o c�lculo do valor do saldo de cada movimenta��o de uma lista.</p>
	 * 
	 * @param listaMovimentacao
	 * @param valorSaldoAnterior utilizado para calcular o saldo da primeira movimenta��o
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Movimentacao> calcularSaldoMovimentacoes(List<Movimentacao> listaMovimentacao, Money valorSaldoAnterior) {
		if (valorSaldoAnterior == null) {
			throw new SinedException("N�o foi poss�vel obter o valor do saldo anterior.");	
		}

		if (listaMovimentacao == null || listaMovimentacao.size() == 0) {
			return listaMovimentacao;
		}

		Movimentacao mv = listaMovimentacao.get(0);
		Money valorMv = new Money(mv.getValor());
		String op = mv.getTipooperacao().getNome();
		final Money negativo = new Money(-1D);
		if (op.equals("D")) {
			valorMv = valorMv.multiply(negativo);
		}
		mv.setSaldo(valorSaldoAnterior.add(valorMv));
		for (int i = 1; i < listaMovimentacao.size(); i++) {
			mv = listaMovimentacao.get(i);
			valorMv = new Money(mv.getValor());
			op = mv.getTipooperacao().getNome();
			if (op.equals("D")) {
				valorMv = valorMv.multiply(negativo);
			}
			mv.setSaldo(listaMovimentacao.get(i-1).getSaldo().add(valorMv));
		}
		return listaMovimentacao;
	}
	
	/*###################################################################################*/
	/*############################## M�todos diversos ###################################*/
	/*###################################################################################*/
	
	/**
	 *	M�todo de refer�ncia ao DAO.
	 *	@see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForSomaListagem(MovimentacaoFiltro)
	 *
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findForSomaListagem(MovimentacaoFiltro filtro){
		return movimentacaoDAO.findForSomaListagem(filtro);
	}
	
	@Override
	public void saveOrUpdate(Movimentacao bean) {
		// CRIA O HIST�RICO AUTOMATICAMENTE AO CRIAR UMA MOVIMENTA��O
		if(bean.getCdmovimentacao() == null){
			List<Movimentacaohistorico> listaMovimentacaohistorico = new ArrayList<Movimentacaohistorico>();
			Movimentacaohistorico mh = movimentacaohistoricoService.geraHistoricoMovimentacao(bean);
			mh.setMovimentacaoacao(Movimentacaoacao.CRIADA);
			listaMovimentacaohistorico.add(mh);
			bean.setListaMovimentacaohistorico(new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class,listaMovimentacaohistorico));
		}else{
			bean.setListaMovimentacaoorigem(movimentacaoorigemService.findByMovimentacao(bean));
		}
		super.saveOrUpdate(bean);
		this.callProcedureAtualizaMovimentacao(bean);
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Movimentacao bean) {
		super.saveOrUpdateNoUseTransaction(bean);
//		this.callProcedureAtualizaMovimentacao(bean);
	}
	
	/**
	 * Prepara e salva a movimenta��o.
	 * - Usado na tela de baixar conta a pagar. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#geraHistoricoMovimentacao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#validateMovimentacao(BindException, Movimentacao)
	 * @see #saveOrUpdateNoUseTransaction(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#saveOrUpdateNoUseTransaction(Movimentacaohistorico)
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public void salvarMovimentacao(final BaixarContaBean baixarContaBean, Movimentacao movimentacao, List<Documento> listaDocumento) {

		movimentacao.setConta(baixarContaBean.getVinculo());
		movimentacao.setValor(new Money(baixarContaBean.getValorTotal()));
		if(baixarContaBean.getValortaxaTotal() != null){
			movimentacao.setValortaxa(new Money(baixarContaBean.getValortaxaTotal()));
		}
		movimentacao.setRateio(baixarContaBean.getRateio());
		movimentacao.setFormapagamento(baixarContaBean.getFormapagamento());
		movimentacao.setDtmovimentacao(baixarContaBean.getDtpagamento());
		movimentacao.setChecknum(baixarContaBean.getChecknum());
		movimentacao.setHistorico(baixarContaBean.getHistorico());
		
		if(baixarContaBean.getDocumentoVinculado() != null && baixarContaBean.getDocumentoVinculado().getEmpresa() != null && 
				baixarContaBean.getDocumentoVinculado().getEmpresa().getCdpessoa() != null){
			movimentacao.setEmpresa(baixarContaBean.getDocumentoVinculado().getEmpresa());
		}else if(baixarContaBean.getEmpresa() != null && baixarContaBean.getEmpresa().getCdpessoa() != null){
			movimentacao.setEmpresa(baixarContaBean.getEmpresa());
		}
		
		if(baixarContaBean.getComprovante() != null)
			movimentacao.setComprovante(baixarContaBean.getComprovante());
		
		if (baixarContaBean.getFormapagamento().equals(Formapagamento.CHEQUE)) {
			movimentacao.setCheque(baixarContaBean.getCheque());
		}
		if (baixarContaBean.getDocumentoclasse().equals(new Documentoclasse(Documentoclasse.CD_PAGAR))) {
			movimentacao.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		} else {
			movimentacao.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		}

		movimentacao.setMovimentacaoacao(Movimentacaoacao.CRIADA);
		
		Movimentacaohistorico mh = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
		Movimentacaohistorico mh2 = null;
		
		boolean conciliada = formapagamentoService.isConciliacaoAuto(baixarContaBean.getFormapagamento()) || caixaService.isCaixaAndConciliacaoAuto(baixarContaBean.getVinculo());
		boolean fromRetorno = baixarContaBean.getFromRetorno() != null && baixarContaBean.getFromRetorno();
		if(fromRetorno){
			try{
				String paramSituacaoRetorno = parametrogeralService.getValorPorNome(Parametrogeral.SITUACAO_PROCESSAMENTO_RETORNO);
				if(paramSituacaoRetorno != null && !paramSituacaoRetorno.trim().equals("")){
					paramSituacaoRetorno = paramSituacaoRetorno.trim().toUpperCase();
					if(paramSituacaoRetorno.equals("CONCILIADA")){
						conciliada = true;
					} else if(paramSituacaoRetorno.equals("NORMAL")){
						conciliada = false;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(conciliada){
			movimentacao.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
			
			if(baixarContaBean.getDtcredito() != null){
				movimentacao.setDtbanco(baixarContaBean.getDtcredito());
			} else {
				movimentacao.setDtbanco(movimentacao.getDtmovimentacao());
			}
			
			if(baixarContaBean.getFromRetorno() != null && baixarContaBean.getFromRetorno()){
				movimentacao.setObservacao("Concilia��o via arquivo de retorno.");
				mh2 = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
			}
		} else {
			movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		}
		
		if(baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER) &&
			(Boolean.TRUE.equals(baixarContaBean.getFromBaixamanual()) || Boolean.TRUE.equals(baixarContaBean.getFromBaixaautomatica()) ||
				Boolean.TRUE.equals(baixarContaBean.getFromWebservice()))){
			java.util.Date dtmovimentacao = movimentacao.getDtmovimentacao();
			Cheque chequeBaixa = baixarContaBean.getFormapagamento().equals(Formapagamento.CHEQUE)? baixarContaBean.getCheque(): null;
			Date dtbanco = documentoService.calculaDtbancoForBaixa(new Date(dtmovimentacao.getTime()), CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ","), chequeBaixa);
			if(dtbanco != null){
				movimentacao.setDtbanco(dtbanco);
			}
		}
		
		if (baixarContaBean.getDocumentoclasse().equals(new Documentoclasse(Documentoclasse.CD_PAGAR))) {
			movimentacaoService.validateMovimentacao(null, movimentacao, baixarContaBean.getValorTotalMovReferente());
		}
		boolean maisDeUmaMovimentacao = (baixarContaBean.getListaDocumento().size() > 1 && Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar()));
		boolean fromBaixaManual = Boolean.TRUE.equals(baixarContaBean.getFromBaixamanual());
		/* Se baixando pela baixa manual(parcial ou n�o), caso seja gerada apenas uma movimenta��o, o sistema monta o hist�rico direto na tela via ajax,
		 * portanto n�o precisa montar novamente.*/
		boolean deveMontarHistorico = (fromBaixaManual && maisDeUmaMovimentacao) || !fromBaixaManual;
		if(deveMontarHistorico){
			preencheHistoricoConfiguravelMovimentacao(baixarContaBean, movimentacao, listaDocumento);
		}
		
		this.saveOrUpdateNoUseTransaction(movimentacao);
		movimentacaohistoricoService.saveOrUpdateNoUseTransaction(mh);
		movimentacao.setMovimentacaohistoricoTransient(mh);
		if(mh2 != null){
			movimentacaohistoricoService.saveOrUpdateNoUseTransaction(mh2);
		}
		documentoService.insertUpdateValortaxa(movimentacao.getValortaxa(), movimentacao.getDtmovimentacao(), listaDocumento, Tipotaxa.TAXABAIXA_ACRESCIMO, false);
	}
	
	private void preencheHistoricoConfiguravelMovimentacao(BaixarContaBean baixarContaBean, Movimentacao movimentacao, List<Documento> listadocumento){
		String historicoAux = "";
		Tipohistoricooperacao historicotipo = Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())? Tipohistoricooperacao.CONTA_PAGAR_BAIXADA_FORNECEDOR:
													Boolean.TRUE.equals(baixarContaBean.getFromRetorno())? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO:
															historicooperacaoService.findHistoricooperacaoTipoReceberByFormapagamento(baixarContaBean.getFormapagamento());
		
		if(historicotipo != null){
			List<Documento> listaContas = contapagarService.findContasPreenchidas(CollectionsUtil.listAndConcatenate(listadocumento, "cddocumento", ","));
			for(Documento doc: listaContas){
				documentoService.ajustaNumeroParcelaDocumento(doc);
			}
			
			if (baixarContaBean.getFormapagamento().equals(Formapagamento.CHEQUE) &&
				(Util.strings.emptyIfNull(movimentacao.getHistorico())
					.contains(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE))){
				Cheque cheque = chequeService.load(movimentacao.getCheque());
				movimentacao.setCheque(cheque);
			}
			historicoAux = movimentacao.getHistorico();
			Historicooperacao historicoOperacao = historicooperacaoService.findByTipo(historicotipo);
			if(historicoOperacao != null){
				historicoAux = historicooperacaoService.findHistoricoByMovimentofinanceiro(movimentacao, listaContas, historicotipo, baixarContaBean.getDocumentoclasse());
				movimentacao.setHistorico(historicoAux);					
			}
			
			if(movimentacao.getHistorico() != null && movimentacao.getHistorico().length() > 150){
				movimentacao.setHistorico(movimentacao.getHistorico().substring(0, 150));
			}
		}
	}
	
	/**
	 * Monta a lista de itens de rateio em tr�s colunas cada uma com 6 itens.
	 * 
	 * @param bean
	 * @param listaItem
	 * @author Rodrigo Freitas
	 */
	private void montagemListaItens(CopiaChequeReportBean bean, List<Rateioitem> listaItem) {
		Integer size;
		//Faz a montagem da string para a exibi��o da lista de rateio no relat�rio.
		size = listaItem.size()/6;
		if (size>=0) {
			bean.setItensrateio1(CollectionsUtil.listAndConcatenate(listaItem.subList(0,listaItem.size() < 6 ? listaItem.size() : 6), "relatorio", "\n"));
		}
		if (size>=1) {
			bean.setItensrateio2(CollectionsUtil.listAndConcatenate(listaItem.subList(6,listaItem.size() < 12 ? listaItem.size() : 12), "relatorio", "\n"));
		}
		if (size>=2) {
			bean.setItensrateio3(CollectionsUtil.listAndConcatenate(listaItem.subList(12,listaItem.size() < 18 ? listaItem.size() : 18), "relatorio", "\n"));
		}
	}


	/**
	 * Preenche os campos com um �nico registro de doucmento ou com a palavra diversos.
	 * 
	 * @param format
	 * @param bean
	 * @param listaMovimentacaoorigem
	 * @author Rodrigo Freitas
	 */
	private void preencheCampos(SimpleDateFormat format,
			CopiaChequeReportBean bean,
			List<Movimentacaoorigem> listaMovimentacaoorigem) {
		if (listaMovimentacaoorigem.size() > 1) {
			bean.setTipodocumento("Diversos");
			bean.setNumerodocumento("Diversos");
			bean.setDataemissao("Diversos");
		} else {
			if(listaMovimentacaoorigem.size() > 0){
				if (listaMovimentacaoorigem.get(0).getDocumento() != null && listaMovimentacaoorigem.get(0).getDocumento().getCddocumento() != null) {
					bean.setTipodocumento(listaMovimentacaoorigem.get(0).getDocumento().getDocumentotipo().getNome());
					bean.setNumerodocumento(listaMovimentacaoorigem.get(0).getDocumento().getNumero());
					bean.setDataemissao(format.format(listaMovimentacaoorigem.get(0).getDocumento().getDtemissao()));
				} else if (listaMovimentacaoorigem.get(0).getAgendamento() != null) {
					bean.setTipodocumento(listaMovimentacaoorigem.get(0).getAgendamento().getDocumentotipo().getNome());
				}
			}
		}
	}

	/**
	 * Cria uma lista de descri��es de contas com 6 descri��es se vier mais coloca a palavra diversos.
	 * 
	 * @param bean
	 * @param movimentacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Movimentacaoorigem> descricaoContas(CopiaChequeReportBean bean, Movimentacao movimentacao) {
		List<Movimentacaoorigem> listaMovimentacaoorigem;
		listaMovimentacaoorigem = movimentacao.getListaMovimentacaoorigem();
		if (listaMovimentacaoorigem.size() > 6) {
			bean.setDescricaodocumento("Diversos");
		} else {
			String descricaodocumento = "";
			for (Movimentacaoorigem mo : listaMovimentacaoorigem) {
				if (mo.getDocumento() != null) {
					descricaodocumento += mo.getDocumento().getDescricao();
					if (SinedUtil.isListNotEmpty(mo.getDocumento()
							.getListaParcela())) {
						Parcela parcela = mo.getDocumento().getListaParcela()
								.iterator().next();
						if (parcela.getOrdem() != null
								&& parcela.getParcelamento() != null
								&& parcela.getParcelamento().getIteracoes() != null)
							descricaodocumento += " - Parc.: "
									+ parcela.getOrdem() + "/"
									+ parcela.getParcelamento().getIteracoes();
					}
					descricaodocumento += "\n";
				}
				if (mo.getAgendamento() != null) {
					descricaodocumento += mo.getAgendamento().getDescricao();
					descricaodocumento += "\n";
				}
			}
			bean.setDescricaodocumento(descricaodocumento);
		}
		return listaMovimentacaoorigem;
	}


	/**
	 * Cria uma lista de fornecedores distintos que n�o ultrapasse 50 caracteres, se passar coloca a palavra diversos.
	 * 
	 * @param bean
	 * @param listaMovimentacaoorigem
	 * @author Rodrigo Freitas
	 */
	private void distinctFornecedores(CopiaChequeReportBean bean, List<Movimentacaoorigem> listaMovimentacaoorigem) {
		List<String> listaString = new ArrayList<String>();
		for (Movimentacaoorigem mo : listaMovimentacaoorigem) {
			if (mo.getDocumento() != null) {
				if (mo.getDocumento().getTipopagamento().equals(Tipopagamento.OUTROS)) 
					addString(listaString, mo.getDocumento().getOutrospagamento());
				else 
					addString(listaString, mo.getDocumento().getPessoa().getNome());
			}
			
			if (mo.getAgendamento() != null) {
				if (mo.getAgendamento().getTipopagamento().equals(Tipopagamento.OUTROS)) 
					addString(listaString, mo.getAgendamento().getOutrospagamento());
				else 
					addString(listaString, mo.getAgendamento().getPessoa().getNome());
			}
		}
		
		String todosForn = CollectionsUtil.concatenate(listaString, ", ");
		if (todosForn.length() > 50) {
			bean.setFornecedor("Diversos");
		} else {
			bean.setFornecedor(todosForn);
		}
	}
	
	/**
	 * Adiciona string a lista.
	 *
	 * @param listaString
	 * @param string
	 * @author Rodrigo Freitas
	 */
	private void addString(List<String> listaString, String string){
		if (listaString != null && !listaString.contains(string)) 
			listaString.add(string);
	}

	/**
	 * Preenche o campo de data de vencimento com uma �nica data distinta na lista se n�o coloca a palavra diversos.
	 * 
	 * @param format
	 * @param bean
	 * @param listaMovimentacaoorigem
	 * @author Rodrigo Freitas
	 */
	private void distinctDataVencimento(SimpleDateFormat format, CopiaChequeReportBean bean, List<Movimentacaoorigem> listaMovimentacaoorigem) {
		String dtvencimento;
		int dia;
		int mes;
		int ano;
		Calendar calendar;
		//Verifica se todas as datas s�o as mesmas.
		dtvencimento = SinedUtil.listAndConcatenate(listaMovimentacaoorigem, "documento.dtvencimento", ",");
		if (dtvencimento.indexOf(",") != -1) {
			bean.setDatavenc("Diversos");
		} else if(dtvencimento.equals("null") || dtvencimento.equals(""))
			bean.setDatavenc("");
		else {
			ano = Integer.parseInt(dtvencimento.substring(0, 4));
			mes = Integer.parseInt(dtvencimento.substring(5, 7));
			dia = Integer.parseInt(dtvencimento.substring(8, 10));
			calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, dia);
			calendar.set(Calendar.MONTH, mes-1);
			calendar.set(Calendar.YEAR, ano);

			bean.setDatavenc(format.format(new Date(calendar.getTimeInMillis())));
		}
	}
	
	

	/**
	 * M�todo para ajustar os valores das Movimenta��es de origem e destino.
	 * 
	 * @param geraMovimentacao
	 * @author Fl�vio Tavares
	 */
	public void ajustaValoresGeraMovimentacao(GeraMovimentacao geraMovimentacao){
		Movimentacao origem = geraMovimentacao.getMovimentacaoorigem();
		Movimentacao destino = geraMovimentacao.getMovimentacaodestino();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		
		Historicooperacao historicoOperacao = historicooperacaoService.findByTipo(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);

		destino.setValor(origem.getValor());
		destino.setDtmovimentacao(origem.getDtmovimentacao());
		origem.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		destino.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		
		if(historicoOperacao == null || Util.strings.isEmpty(historicoOperacao.getHistoricocomplementar())){
			destino.setHistorico(origem.getHistorico());
		}
		
		destino.setDtaltera(hoje);
		destino.setCdusuarioaltera(cdusuarioaltera);

		origem.setRateio(geraMovimentacao.getRateio());
		destino.setRateio(geraMovimentacao.getRateio2());
		origem.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		destino.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		origem.setDtaltera(hoje);
		origem.setCdusuarioaltera(cdusuarioaltera);
	}

	/**
	 * M�todo utilizado para salvar as movimenta��es de origem e destino na tela de Gerar Movimentacao.
	 * 
	 * @see #ajustaValoresGeraMovimentacao(GeraMovimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#geraHistoricoMovimentacao(Movimentacao)
	 * @see #saveOrUpdateNoUseTransaction(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoorigemService#saveOrUpdateNoUseTransaction(Movimentacaoorigem)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoorigemService#createMovimentacaoorigem(Movimentacao)
	 * @see #callProcedureAtualizaMovimentacao(Movimentacao)
	 * @param geraMovimentacao
	 * @author Fl�vio Tavares
	 */
	public void saveGeraMovimentacao(final GeraMovimentacao geraMovimentacao){
		Movimentacao[] movs = (Movimentacao[])getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				movimentacaoService.ajustaValoresGeraMovimentacao(geraMovimentacao);
				
				Movimentacao origem = geraMovimentacao.getMovimentacaoorigem();
				Movimentacao destino = geraMovimentacao.getMovimentacaodestino();
				List<Movimentacao> listaDestino = new ArrayList<Movimentacao>();

				List<GeraMovimentacaocheque> listaGeraMovimentacaoCheque = geraMovimentacao.getListaGeraMovimentacaocheque();
				if(listaGeraMovimentacaoCheque != null && listaGeraMovimentacaoCheque.size() == 1){
					origem.setCheque(listaGeraMovimentacaoCheque.get(0).getCheque());
					destino.setCheque(listaGeraMovimentacaoCheque.get(0).getCheque());
				}
				
				List<Movimentacao> listaMovimentacaoByCheque = getMovimentacaoByCheques(listaGeraMovimentacaoCheque);
				if(destino.getGerarMovimentacaoPorCheque() != null && destino.getGerarMovimentacaoPorCheque()){
					criaMovimentacaoDestinoPorCheque(destino, listaGeraMovimentacaoCheque, listaDestino);
				}
				Historicooperacao historicoOperacao = historicooperacaoService.findByTipo(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);
				
				boolean alterouHistoricoAutomaticoOrigemManualmente = historicoOperacao != null && Boolean.TRUE.equals(geraMovimentacao.getNaoBuscarHistoricoPadrao());
				boolean alterouHistoricoAutomaticoDestinoManualmente = historicoOperacao != null && (Boolean.TRUE.equals(geraMovimentacao.getNaoBuscarHistoricoComplementarPadrao()) ||
																			(alterouHistoricoAutomaticoOrigemManualmente && StringUtils.isEmpty(historicoOperacao.getHistoricocomplementar())));
				boolean adicionarCheque = true;
				List<Cheque> listaChequeAlterarSituacao = new ArrayList<Cheque>();
				if(SinedUtil.isListNotEmpty(listaGeraMovimentacaoCheque)){
					for(GeraMovimentacaocheque geraMovimentacaocheque : listaGeraMovimentacaoCheque){
						if(geraMovimentacaocheque.getCheque() != null && geraMovimentacaocheque.getCheque().getCdcheque() != null){
							adicionarCheque = true;
							if(SinedUtil.isListNotEmpty(listaMovimentacaoByCheque)){
								for(Movimentacao movimentacao : listaMovimentacaoByCheque){
									if(movimentacao.getCheque() != null && geraMovimentacaocheque.getCheque().equals(movimentacao.getCheque())){
										adicionarCheque = false;
										break;
									}
								}
							}
							if(adicionarCheque){
								listaChequeAlterarSituacao.add(geraMovimentacaocheque.getCheque());
							}
						}
					}
				}
				
				String descricaoHistoricoEditadoManual = "Hist�rico lan�ado manualmente.";
				
				Set<Movimentacaohistorico> listaHistorico = new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class);
				Movimentacaohistorico mh;
				
				origem = verificaConciliaAutomaticoCaixa(origem);
			
				origem.setListaMovimentacaohistorico(listaHistorico);
//				if(origem.getCheque() != null && origem.getCheque().getNumero() != null)
//					chequeService.saveOrUpdate(origem.getCheque());
				movimentacaoService.saveOrUpdateNoUseTransaction(origem);
				listaHistorico.clear();
				
				/*
				 * Array para retorno das movimenta��es salvas nesta transaction.
				 * Para chamar a procedure que atualiza as descri��es ao terminar a transaction.
				 */
				Movimentacao[] movs = new Movimentacao[]{origem, destino};
				StringBuilder observacaoOrigem = new StringBuilder();
				if(destino.getGerarMovimentacaoPorCheque() != null && destino.getGerarMovimentacaoPorCheque() && listaDestino != null && !listaDestino.isEmpty()){
					Chequehistorico chequehistorico;
					for(Movimentacao movimentacao : listaDestino){
						mh = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
						mh.setObservacao("Transfer�ncia de <a href=\"javascript:visualizaMovimentacao("+ 
										origem.getCdmovimentacao()+");\">"+origem.getCdmovimentacao() +"</a>");
						mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
						listaHistorico.add(mh);
						
						if(alterouHistoricoAutomaticoDestinoManualmente){
							mh = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
							mh.setObservacao(descricaoHistoricoEditadoManual);
							mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
							listaHistorico.add(mh);
						}
						
						rateioService.limpaReferenciaRateio(movimentacao.getRateio());
						rateioService.atualizaValorRateio(movimentacao.getRateio(), movimentacao.getValor());
						movimentacao.setListaMovimentacaohistorico(listaHistorico);
						
						movimentacao = verificaConciliaAutomaticoCaixa(movimentacao);
						if(Boolean.TRUE.equals(geraMovimentacao.getGerarConciliacaoCredito()) &&
							!Movimentacaoacao.CONCILIADA.equals(movimentacao.getMovimentacaoacao())){
							movimentacao.conciliar();
							movimentacao.setDtbanco(movimentacao.getDtmovimentacao());
						}
						
						movimentacaoService.saveOrUpdateNoUseTransaction(movimentacao);
						observacaoOrigem.append("<a href=\"javascript:visualizaMovimentacao("+ 
								movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>,");
						
						if(movimentacao.getCheque() != null && movimentacao.getCheque().getCdcheque() != null){
							if(listaChequeAlterarSituacao.contains(movimentacao.getCheque()) || Boolean.TRUE.equals(geraMovimentacao.getGerarConciliacaoCredito()) ||
									!chequeService.isNotSituacao(movimentacao.getCheque().getCdcheque().toString(), Chequesituacao.DEVOLVIDO)){
								if(Movimentacaoacao.CONCILIADA.equals(movimentacao.getMovimentacaoacao())){
									movimentacao.getCheque().setChequesituacao(Chequesituacao.COMPENSADO);
								}else {
									movimentacao.getCheque().setChequesituacao(Chequesituacao.BAIXADO);
								}
								chequeService.updateSituacao(movimentacao.getCheque().getCdcheque().toString(), movimentacao.getCheque().getChequesituacao());
								if(Movimentacaoacao.CONCILIADA.equals(movimentacao.getMovimentacaoacao())){
									chequehistorico = chequehistoricoService.criaHistoricoConciliacao(movimentacao.getCheque(), movimentacao, "Concilia��o de transfer�ncia");
									chequehistoricoService.saveOrUpdate(chequehistorico);
								}else {
									chequehistorico = chequehistoricoService.criaHistoricoTransferencia(movimentacao.getCheque(), movimentacao, "Transfer�ncia entre Contas");
									if(chequehistorico != null){
										chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
									}
								}
							}else {
								chequehistorico = chequehistoricoService.criaHistoricoTransferencia(movimentacao.getCheque(), movimentacao, "Transfer�ncia entre Contas");
								if(chequehistorico != null){
									chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
								}
							}
						}
						listaHistorico.clear();
					}
				} else {
					destino = verificaConciliaAutomaticoCaixa(destino);
					if(Boolean.TRUE.equals(geraMovimentacao.getGerarConciliacaoCredito()) &&
						!Movimentacaoacao.CONCILIADA.equals(destino.getMovimentacaoacao())){
						destino.conciliar();
						destino.setDtbanco(destino.getDtmovimentacao());
					}
					mh = movimentacaohistoricoService.geraHistoricoMovimentacao(destino);
					mh.setObservacao("Transfer�ncia de <a href=\"javascript:visualizaMovimentacao("+ 
										origem.getCdmovimentacao()+");\">"+origem.getCdmovimentacao() +"</a>");
					mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
					listaHistorico.add(mh);
					
					if(alterouHistoricoAutomaticoDestinoManualmente){
						mh = movimentacaohistoricoService.geraHistoricoMovimentacao(destino);
						mh.setObservacao(descricaoHistoricoEditadoManual);
						mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
						listaHistorico.add(mh);
					}
					
					destino.setListaMovimentacaohistorico(listaHistorico);
					movimentacaoService.saveOrUpdateNoUseTransaction(destino);
					observacaoOrigem.append("<a href=\"javascript:visualizaMovimentacao("+ 
							destino.getCdmovimentacao()+");\">"+destino.getCdmovimentacao() +"</a>,");

					if(destino.getCheque() != null && destino.getCheque().getCdcheque() != null){
						Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoTransferencia(destino.getCheque(), destino, "Transfer�ncia entre Contas");
						if(chequehistorico != null){
							chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
						}
					}
					
					if(SinedUtil.isListNotEmpty(listaGeraMovimentacaoCheque)){
						for(GeraMovimentacaocheque geraMovimentacaocheque : listaGeraMovimentacaoCheque){
							if(geraMovimentacaocheque.getCheque() != null && geraMovimentacaocheque.getCheque().getCdcheque() != null &&
									listaChequeAlterarSituacao.contains(geraMovimentacaocheque.getCheque())){
								if(destino.getCheque() != null && geraMovimentacaocheque.getCheque().equals(destino.getCheque())){
									if(Movimentacaoacao.CONCILIADA.equals(destino.getMovimentacaoacao())){
										geraMovimentacaocheque.getCheque().setChequesituacao(Chequesituacao.COMPENSADO);
									}else {
										geraMovimentacaocheque.getCheque().setChequesituacao(Chequesituacao.BAIXADO);
									}
								}else {
									geraMovimentacaocheque.getCheque().setChequesituacao(Chequesituacao.COMPENSADO);
								}
								chequeService.updateSituacao(geraMovimentacaocheque.getCheque().getCdcheque().toString(), geraMovimentacaocheque.getCheque().getChequesituacao());
								if(Chequesituacao.COMPENSADO.equals(geraMovimentacaocheque.getCheque().getChequesituacao())){
									Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoConciliacao(geraMovimentacaocheque.getCheque(), destino, "Concilia��o de transfer�ncia");
									chequehistoricoService.saveOrUpdate(chequehistorico);
								}
							}
						}
					}
				}
				
				if(StringUtils.isNotBlank(observacaoOrigem.toString())){
					mh = movimentacaohistoricoService.geraHistoricoMovimentacao(origem);
					mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
					mh.setObservacao("Transfer�ncia para " + observacaoOrigem.substring(0, observacaoOrigem.length()-1));
					movimentacaohistoricoService.saveOrUpdateNoUseTransaction(mh);
				}
				
				if(alterouHistoricoAutomaticoOrigemManualmente){
					mh = movimentacaohistoricoService.geraHistoricoMovimentacao(origem);
					mh.setMovimentacaoacao(Movimentacaoacao.TRANSFERENCIA);
					mh.setObservacao(descricaoHistoricoEditadoManual);
					movimentacaohistoricoService.saveOrUpdateNoUseTransaction(mh);
				}
				
				if(destino.getGerarMovimentacaoPorCheque() != null && destino.getGerarMovimentacaoPorCheque() && listaDestino != null && !listaDestino.isEmpty()){
					movs = new Movimentacao[listaDestino.size()+1];
					movs[0] = origem;
					for(int i = 0; i < listaDestino.size(); i++){
						movs[i+1] = listaDestino.get(i);
					}
					
					for(Movimentacao movimentacaoDestino : listaDestino){
						Movimentacaoorigem moOrigem = movimentacaoorigemService.createMovimentacaoorigem(origem);
						moOrigem.setConta(movimentacaoDestino.getConta());
						moOrigem.setMovimentacaorelacionada(movimentacaoDestino);
						movimentacaoorigemService.saveOrUpdateNoUseTransaction(moOrigem);
						
						Movimentacaoorigem moDestino = movimentacaoorigemService.createMovimentacaoorigem(movimentacaoDestino);
						moDestino.setConta(origem.getConta());
						moDestino.setMovimentacaorelacionada(origem);
						movimentacaoorigemService.saveOrUpdateNoUseTransaction(moDestino);
						
						if(movimentacaoDestino != null && movimentacaoDestino.getCheque() != null && movimentacaoDestino.getEmpresa() != null)
							chequeService.updateEmpresaCheque(movimentacaoDestino);
					}
					
				}else {
					Movimentacaoorigem moOrigem = movimentacaoorigemService.createMovimentacaoorigem(origem);
					moOrigem.setConta(destino.getConta());
					moOrigem.setMovimentacaorelacionada(destino);
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(moOrigem);
					
					Movimentacaoorigem moDestino = movimentacaoorigemService.createMovimentacaoorigem(destino);
					moDestino.setConta(origem.getConta());
					moDestino.setMovimentacaorelacionada(origem);
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(moDestino);
					
					if(destino != null && destino.getCheque() != null && destino.getEmpresa() != null)
						chequeService.updateEmpresaCheque(destino);
				}
				
				if(listaMovimentacaoByCheque != null && !listaMovimentacaoByCheque.isEmpty()){
					for(Movimentacao bean : listaMovimentacaoByCheque){
						if(bean.getCheque() != null && bean.getCheque().getNumero() != null){
							bean.setDtbanco(bean.getDtmovimentacao() != null ? bean.getDtmovimentacao() : new Date(System.currentTimeMillis()));
							if(StringUtils.isBlank(bean.getChecknum())){
								bean.setChecknum(bean.getCheque().getNumero().toString());
							}
							movimentacaoService.salvaMovimentacaoConciliacao(bean, "Concilia��o de transfer�ncia");
						}
					}
				}

				return movs;
			}
		});
		for (Movimentacao movimentacao : movs) {
			this.callProcedureAtualizaMovimentacao(movimentacao);
		}
	}
	
	public Movimentacao verificaConciliaAutomaticoCaixa(Movimentacao bean){
		if(bean.getCdmovimentacao() == null && bean.getConta() != null){
			Conta conta = contaService.loadConta(bean.getConta());
			if(conta.getContatipo() != null && 
					conta.getContatipo().equals(Contatipo.TIPO_CAIXA) &&
					conta.getConciliacaoautomatica() != null &&
					conta.getConciliacaoautomatica()){
				bean.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
				bean.setDtbanco(bean.getDtmovimentacao());
			}
		}
		return bean;
	}
	
	/**
	 * M�todo que retorna a lista de movimentacao com cheques para conciliar
	 *
	 * @param listaGeraMovimentacaoCheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> getMovimentacaoByCheques(List<GeraMovimentacaocheque> listaGeraMovimentacaoCheque) {
		List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();
		Movimentacao movimentacao;
		if(listaGeraMovimentacaoCheque != null && !listaGeraMovimentacaoCheque.isEmpty()){
			for(GeraMovimentacaocheque bean : listaGeraMovimentacaoCheque){
				if(bean.getCheque() != null && bean.getCheque().getCdcheque() != null){
					movimentacao = this.findMovimentacaoByCheque(bean.getCheque().getCdcheque().toString());
					if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
						listaMovimentacao.add(movimentacao);
					}
				}
			}
		}
		return listaMovimentacao;
	}
	
	/**
	 * M�todo que cria movimenta��es de origem de acordo com os cheques
	 *
	 * @param origem
	 * @param listaGeraMovimentacaoCheque
	 * @return
	 * @author Luiz Fernando
	 */
	public void criaMovimentacaoDestinoPorCheque(Movimentacao destino, List<GeraMovimentacaocheque> listaGeraMovimentacaoCheque, List<Movimentacao> listaDestino) {
		Movimentacao mov;
		Rateio rateio;
		if(destino != null && listaGeraMovimentacaoCheque != null && !listaGeraMovimentacaoCheque.isEmpty()){
			for(GeraMovimentacaocheque gmcheque : listaGeraMovimentacaoCheque){
				mov = new Movimentacao(destino);
				mov.setFormapagamento(destino.getFormapagamento());
				mov.setCheque(gmcheque.getCheque());
				mov.setValor(chequeService.getValor(gmcheque.getCheque()));
				mov.setMovimentacaoacao(Movimentacaoacao.NORMAL);
				mov.setObservacao(destino.getObservacao());
				mov.setHistorico(destino.getHistorico());
				mov.setTipooperacao(Tipooperacao.TIPO_CREDITO);
				mov.setBordero(destino.getBordero()); 
				mov.setEmpresa(destino.getEmpresa());
				rateio = rateioService.getNovoRateio(destino.getRateio());
				mov.setRateio(rateio);
				rateioService.atualizaValorRateio(mov.getRateio(), mov.getValor());
				listaDestino.add(mov);
			}
		}
	}

	/**
	 * <p>Gera uma movimenta��o para o processo de consolidar agendamento.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#limparDadosAgendamento(Agendamento)
	 * @param agendamento
	 * @author Hugo Ferreira
	 */
	public Movimentacao gerarMovimentacao(Agendamento agendamento) {
		if(agendamento == null){
			throw new SinedException("O par�metro agendamento n�o pode ser null.");
		}
		Movimentacao movimentacao = new Movimentacao();

		agendamentoService.limparDadosAgendamento(agendamento);

		movimentacao.setConta(agendamento.getConta());
		movimentacao.setValor(agendamento.getValor());
		movimentacao.setDtmovimentacao(new Date(System.currentTimeMillis()));
		movimentacao.setTipooperacao(agendamento.getTipooperacao());
		movimentacao.setRateio(agendamento.getRateio());
		movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		movimentacao.setOperacaocontabil(agendamento.getOperacaocontabil());
		movimentacao.setFormapagamento(agendamento.getFormapagamento());
		if(agendamento.getConta() != null && agendamento.getEmpresa() != null && contaService.isPermissaoConta(agendamento.getConta(), agendamento.getEmpresa())){
			movimentacao.setEmpresa(agendamento.getEmpresa());
		}

		return movimentacao;
	}
	
	public void validateMovimentacao(BindException errors, Movimentacao movimentacao){
		validateMovimentacao(errors, movimentacao, null);
	}

	/**
	 * <p>Valida��o para gera��o de movimenta��o financeira.</p>
	 * <pre>Verifica se o valor da movimenta��o � v�lido para o tipo de 
	 * conta na qual o valor ser� debitado.</pre>
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#load(Conta)
	 * @see br.com.linkcom.sined.geral.service.ContaService#obterValorMaximo(Conta)
	 * @param errors
	 * @param movimentacao
	 * @author Fl�vio Tavares
	 */
	public void validateMovimentacao(BindException errors, Movimentacao movimentacao, Money valorTotalMovReferente){
		if (movimentacao == null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		if (movimentacao.getTipooperacao() == null || movimentacao.getTipooperacao().getCdtipooperacao() == null) {
			throw new SinedException("Tipo de opera��o n�o pode ser nula.");
		}
		
		if(movimentacao.getDtbanco() != null){
			if(movimentacao.getDtbanco().before(movimentacao.getDtmovimentacao())){
				if(errors != null) 
					errors.reject("001","A data de banco n�o deve ser anterior � data da movimenta��o.");
				else throw new SinedException("A data de banco n�o deve ser anterior � data da movimenta��o.");
			}
		}
		
		Conta conta = contaService.load(movimentacao.getConta());

		if (movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
			if (conta == null) {
				throw new SinedException("O par�metro conta n�o pode ser null.");
			}
			
			double valorMov = movimentacao.getValor().getValue().doubleValue();
			double valorMaximo = contaService.obterValorMaximo(conta).getValue().doubleValue();
			
			if(valorTotalMovReferente != null && valorTotalMovReferente.getValue().doubleValue() > 0){
				valorMaximo += valorTotalMovReferente.getValue().doubleValue();
			}
			
			if(movimentacao.getCdmovimentacao() != null){
				Movimentacao movimentacaoBanco = movimentacaoService.load(movimentacao, "movimentacao.conta, movimentacao.valor");
				if(movimentacaoBanco.getConta().equals(conta)){
					valorMaximo = movimentacaoBanco.getValorDouble() + valorMaximo;
				}
			}
			
			String descricao = conta.getNome();
			if (valorMov > valorMaximo) {
				String msg = "N�o � poss�vel lan�ar movimenta��o financeira em "+ descricao + ", o saldo ficaria negativo.";
				if (errors != null) {
					errors.reject("001", msg);
				} else {
					throw new SinedException(msg);
				}
			}
			
		}
		if(Contatipo.CAIXA.equals(conta.getContatipo().getCdcontatipo()) || 
				Contatipo.CONTA_BANCARIA.equals(conta.getContatipo().getCdcontatipo())){
			if(movimentacao.getDtmovimentacao() != null && conta.getDtsaldo()!=null && 
					movimentacao.getDtmovimentacao().before(conta.getDtsaldo())){
				String msg = "A data da movimenta��o n�o deve ser anterior � data do saldo inicial de " +
						conta.getDescricao() + " (" + SinedDateUtils.toString(conta.getDtsaldo()) + ").";
				if (errors != null) {
					errors.reject("001", msg);
				} else {
					throw new SinedException(msg);
				}
			}
		}

		if (movimentacao.getCheque()!=null && movimentacao.getCheque().getCdcheque()!=null){
			Cheque cheque = chequeService.load(movimentacao.getCheque());
			if (cheque.getValor()!=null && movimentacao.getValor()!=null && cheque.getValor().toLong()!=movimentacao.getValor().toLong()){
				String msg = "O valor da movimenta��o est� diferente do valor do cheque.";
				if(errors != null){
					errors.reject("001", msg);
				} else {
					throw new SinedException(msg);
				}
			}
		}		
	}

	/**
	 * M�todo para estornar v�rias movimenta��es.
	 * 
	 * @see #mudarEstado(String, String, Movimentacaoacao)
	 * @param ids
	 * @param observacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public int estornar(String ids, String observacao){
		return this.mudarEstado(ids, observacao, Movimentacaoacao.ESTORNADA);
	}
	
	/**
	 * M�todo para cancelar v�rias movimenta��es.
	 * 
	 * @see #mudarEstado(String, String, Movimentacaoacao)
	 * @param ids
	 * @param observacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public int cancelar(String ids, String observacao){
		return this.mudarEstado(ids, observacao, Movimentacaoacao.CANCELADA);
	}
	
	/**
	 * M�todo para mudar o estado de v�rias movimenta��es.
	 * 
	 * @see #carregaLista(String)
	 * @see #salvarAcaoTransaction(Movimentacaohistorico)
	 * @param ids - ID's das movimenta��es concatenados e separados por v�rgula.
	 * @param observacao - Para hist�rico
	 * @param movimentacaoacao - Estado para o qual se mudar� as movimenta��es.
	 * @return - N�mero de movimenta��es atualizadas com sucesso.
	 * @author Fl�vio Tavares
	 */
	private int mudarEstado(String ids, String observacao, Movimentacaoacao movimentacaoacao){
		boolean estorno = Movimentacaoacao.ESTORNADA.equals(movimentacaoacao);
		boolean cancelamento = Movimentacaoacao.CANCELADA.equals(movimentacaoacao);
		
		int sucesso = 0;
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		
		Movimentacaohistorico movimentacaohistorico;		
		List<Movimentacao> listaMovimentacao = this.carregaLista(ids);
		String whereInCddocumento = "-1";
		
		for (Movimentacao mov : listaMovimentacao) {
			movimentacaohistorico = new Movimentacaohistorico(null, mov, movimentacaoacao, observacao,cdusuarioaltera, hoje);
			boolean mudou = false;
			if(estorno){
				mudou = mov.estornar();
			}
			if(cancelamento){
				mudou = mov.cancelar();
				
			}
			
			if(mudou) sucesso++;
			else continue;
						
			this.salvarAcaoTransaction(movimentacaohistorico);
			
			if(mudou){
				List<Movimentacao> listaMovimentacaoOrigemDevolucao = findOrigemDevolucao(mov, cancelamento);
				if(SinedUtil.isListNotEmpty(listaMovimentacaoOrigemDevolucao)){
					for(Movimentacao movimentacaoDevolucao : listaMovimentacaoOrigemDevolucao){
						if(movimentacaoDevolucao.getMovimentacaoacao() != null && 
								!Movimentacaoacao.CANCELADA.equals(movimentacaoDevolucao.getMovimentacaoacao())){
							movimentacaohistorico = new Movimentacaohistorico(null, movimentacaoDevolucao, movimentacaoacao, observacao,cdusuarioaltera, hoje);
							if(estorno){
								movimentacaoDevolucao.estornar();
								movimentacaohistorico.setObservacao("Estorno realizado ap�s o estorno da movimenta��o " +
										"<a href=\"javascript:visualizaMovimentacao("+ 
										mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
							}
							if(cancelamento){
								movimentacaoDevolucao.cancelar();
								movimentacaohistorico.setObservacao("Cancelamento realizado ap�s o cancelamento da movimenta��o " +
										"<a href=\"javascript:visualizaMovimentacao("+ 
										mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
							}
							
							this.salvarAcaoTransaction(movimentacaohistorico);
						}
					}
				}
				
				if(cancelamento){
					List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacaoorigemService.findByMovimentacao(mov);
					if(SinedUtil.isListNotEmpty(listaMovimentacaoorigem)){
						List<Despesaviagem> listaDespesaviagem = new ArrayList<Despesaviagem>();
						Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
						Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
						
						for(Movimentacaoorigem mo : listaMovimentacaoorigem){
							Despesaviagem despesaviagem = null;
							if(mo.getDespesaviagemacerto() != null){
								despesaviagem = mo.getDespesaviagemacerto();
							}else if(mo.getDespesaviagemadiantamento() != null){
								despesaviagem = mo.getDespesaviagemadiantamento();
							}
							
							if(despesaviagem != null){
								DespesaviagemHistorico dvh = new DespesaviagemHistorico();
								dvh.setDespesaviagem(despesaviagem);
								dvh.setDtaltera(dtaltera);
								dvh.setUsuario(usuarioLogado);
								dvh.setObservacao("Movimenta��o cancelada: " + "<a href=\"javascript:visualizaMovimentacao("+ 
										mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
								despesaviagemHistoricoService.saveOrUpdate(dvh);
								
								if(!listaDespesaviagem.contains(despesaviagem)){
									listaDespesaviagem.add(despesaviagem);
								}
							}
							
							if (mo.getDocumento()!=null){
								if(cancelamento){
									documentoService.insertUpdateValortaxa(mov.getValortaxa(), mov.getDtmovimentacao(), mo.getDocumento(), Tipotaxa.TAXABAIXA_ACRESCIMO, true);
								}
								documentoService.callProcedureAtualizaDocumento(mo.getDocumento());
							}
							
							if (mo.getDocumento()!=null && mo.getDocumento().getCddocumento()!=null){
								whereInCddocumento += ", " + mo.getDocumento().getCddocumento();
							}
						}
						
						despesaviagemService.verificaSituacaoDespesaviagem(listaDespesaviagem);
					}
				}
				
				List<Movimentacao> listaMovimentacaoTaxa = findMovimenacaoTaxa(mov);
				if(SinedUtil.isListNotEmpty(listaMovimentacaoTaxa)){
					for(Movimentacao movTaxa : listaMovimentacaoTaxa){
						if(movTaxa.getMovimentacaoacao() != null && 
								!Movimentacaoacao.CANCELADA.equals(movTaxa.getMovimentacaoacao())){
							movimentacaohistorico = new Movimentacaohistorico(null, movTaxa, movimentacaoacao, observacao,cdusuarioaltera, hoje);
							if(estorno){
								if(Boolean.TRUE.equals(movTaxa.getTaxageradaprocessamentocartao())){
									movimentacaohistorico = new Movimentacaohistorico(null, movTaxa, Movimentacaoacao.CANCELADA, observacao,cdusuarioaltera, hoje);
									movTaxa.cancelar();
									movimentacaohistorico.setObservacao("Cancelamento realizado ap�s o estorno da movimenta��o " +
											"<a href=\"javascript:visualizaMovimentacao("+ 
											mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
								}else{
									movTaxa.estornar();
									movimentacaohistorico.setObservacao("Estorno realizado ap�s o estorno da movimenta��o " +
											"<a href=\"javascript:visualizaMovimentacao("+ 
											mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
								}
							}
							if(cancelamento){
								movTaxa.cancelar();
								movimentacaohistorico.setObservacao("Cancelamento realizado ap�s o cancelamento da movimenta��o " +
										"<a href=\"javascript:visualizaMovimentacao("+ 
										mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
							}
							
							this.salvarAcaoTransaction(movimentacaohistorico);
						}
					}
				}
			}
		}
		
		if (cancelamento){
			valecompraService.estornarContaReceber(false, whereInCddocumento);
		}
		
		return sucesso;
	}
	
	/**
	 * Seta n�meros no filtro para que seja feita a pagina��o da listagem de cheques, 
	 * no relat�rio de c�pia de cheque.
	 * 
	 * @param filtro
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void setaNumeros(CopiaChequeReportFiltro filtro,
			List<Movimentacao> listaMovimentacao) {
		filtro.setNumberOfResults(listaMovimentacao.size());

		int i = filtro.getNumberOfResults() / filtro.getPageSize();
		if(filtro.getNumberOfResults() % filtro.getPageSize() != 0){
			i++;
		}
		filtro.setNumberOfPages(i);
	}

	/**
	 * Seta o orderBy do filtro para que seja feita a pagina��o correta.
	 * 
	 * @param filtro
	 * @param currentPage
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void ordenaBanco(CopiaChequeReportFiltro filtro, Integer currentPage, List<Movimentacao> listaMovimentacao) {
		if ("banco.nome".equals(filtro.getOrderBy())) {
			if (filtro.isAsc()) {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o1.getConta().getDescricao().compareTo(o2.getConta().getDescricao());
					}
				});
			} else {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o2.getConta().getDescricao().compareTo(o1.getConta().getDescricao());
					}
				});
			}
			filtro.setOrderBy("movimentacao.conta.descricao");
			filtro.setCurrentPage(currentPage);
		}
	}

	/**
	 * Faz a ordena��o do campo todosFornecedores de movimenta��o.
	 * - Isso foi feito pois o campo � montado manualmente.
	 * 
	 * @param filtro
	 * @param currentPage
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void ordenaTodosFornecedores(CopiaChequeReportFiltro filtro,
			Integer currentPage, List<Movimentacao> listaMovimentacao) {
		if (filtro.getOrdenaTodosFornecedores()) {
			if (filtro.isAsc()) {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o1.getTodosFornecedores().compareTo(o2.getTodosFornecedores());
					}
				});
			} else {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o2.getTodosFornecedores().compareTo(o1.getTodosFornecedores());
					}
				});
			}
			filtro.setOrderBy("movimentacao.todosFornecedores");
			filtro.setCurrentPage(currentPage);
		}
	}

	/**
	 * Faz a ordena��o do campo descricao de movimenta��o.
	 * - Isso foi feito pois o campo � montado manualmente.
	 * 
	 * @param filtro
	 * @param currentPage
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void ordenaMovDescricao(CopiaChequeReportFiltro filtro,
			Integer currentPage, List<Movimentacao> listaMovimentacao) {
		if (filtro.getOrdenaMovDescricao()) {
			if (filtro.isAsc()) {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o1.getDescricaocontas().toUpperCase().compareTo(o2.getDescricaocontas().toUpperCase());
					}
				});
			} else {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o2.getDescricaocontas().toUpperCase().compareTo(o1.getDescricaocontas().toUpperCase());
					}
				});
			}
			filtro.setOrderBy("movimentacao.descricao");
			filtro.setCurrentPage(currentPage);
		}
	}

	/**
	 * Faz a ordena��o manual pelo campo de data da movimenta��o da listagem de movimenta��es.
	 * 
	 * @param filtro
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void ordenaDtmovimentacao(CopiaChequeReportFiltro filtro,
			List<Movimentacao> listaMovimentacao) {

		if ("movimentacao.dtmovimentacao".equals(filtro.getOrderBy())) {
			if (filtro.isAsc()) {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o1.getDtmovimentacao().compareTo(o2.getDtmovimentacao());
					}
				});
			} else {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o2.getDtmovimentacao().compareTo(o1.getDtmovimentacao());
					}
				});
			}
		}

	}

	/**
	 * Faz a ordena��o manual pelo campo valor da listagem de movimenta��es.
	 * 
	 * @param filtro
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void ordenaValor(CopiaChequeReportFiltro filtro,
			List<Movimentacao> listaMovimentacao) {
		if ("movimentacao.valor".equals(filtro.getOrderBy())) {
			if (filtro.isAsc()) {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o1.getValor().compareTo(o2.getValor());
					}
				});
			} else {
				Collections.sort(listaMovimentacao,new Comparator<Movimentacao>(){
					public int compare(Movimentacao o1, Movimentacao o2) {
						return o2.getValor().compareTo(o1.getValor());
					}
				});
			}
		}

	}


	/**
	 * Seta o orderBy corretamente para que seja feita a ordena��o.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer setaOrderBy(CopiaChequeReportFiltro filtro) {
		int currentPage = 0;
		if ("movimentacao.descricao".equals(filtro.getOrderBy())) {
			currentPage = filtro.getCurrentPage();
			filtro.setOrderBy("");
			filtro.setCurrentPage(currentPage);
			filtro.setOrdenaMovDescricao(true);
			filtro.setOrdenaTodosFornecedores(false);
		} else if ("movimentacao.todosFornecedores".equals(filtro.getOrderBy())) {
			currentPage = filtro.getCurrentPage();
			filtro.setOrderBy("");
			filtro.setCurrentPage(currentPage);
			filtro.setOrdenaTodosFornecedores(true);
			filtro.setOrdenaMovDescricao(false);
		} else {
			filtro.setOrdenaMovDescricao(false);
			filtro.setOrdenaTodosFornecedores(false);
			if ("movimentacao.conta.descricao".equals(filtro.getOrderBy())) {
				currentPage = filtro.getCurrentPage();
				filtro.setOrderBy("banco.nome");
				filtro.setCurrentPage(currentPage);
			}
		}
		return currentPage;
	}

	/**
	 * M�todo utilizado para salvar a a��o de uma movimenta��o ao alterar pelo combo "Mais A��es" na listagem.<br>
	 * 
	 * @see #cancelarOrigensMovimentacao(Movimentacao, Movimentacaohistorico)
	 * @see #estornarOrigensMovimentacao(Movimentacao, Movimentacaohistorico)
	 * @param historico
	 * @author Fl�vio Tavares
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public void salvarAcaoTransaction(final Movimentacaohistorico historico){
		final Movimentacaoacao acao = historico.getMovimentacaoacao();
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Movimentacao mov = historico.getMovimentacao();
				mov.setMovimentacaoacao(acao);
				if(acao.equals(Movimentacaoacao.CANCELADA)){
					movimentacaoService.cancelarOrigensMovimentacao(mov, historico);
				} else if(acao.equals(Movimentacaoacao.ESTORNADA)){
					movimentacaoService.estornarOrigensMovimentacao(mov, historico);
				}
				return null;
			}
		});
	}

	/**
	 * M�todo para estornar a movimenta��o e suas origens (documentos).
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#load(Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#updateDocumentoacao(Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#geraHistoricoDocumento(Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @param mov
	 * @param historico
	 * @author Fl�vio Tavares
	 */
	private void estornarOrigensMovimentacao(Movimentacao mov, Movimentacaohistorico historico){
		Cheque cheque = mov.getCheque();
		if(cheque != null && cheque.getCdcheque() != null){
			chequeService.updateSituacao(cheque.getCdcheque() + "", Chequesituacao.BAIXADO);
			
			Chequehistorico chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setChequesituacao(Chequesituacao.BAIXADO);
			chequehistorico.setObservacao("Estorno da movimentacao: Movimenta��o <a href=\"javascript:visualizaMovimentacao("+ mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>.");
			chequehistoricoService.saveOrUpdate(chequehistorico);
		}
		
		List<Movimentacaoorigem> origens = mov.getListaMovimentacaoorigem();
		if(SinedUtil.isListNotEmpty(origens)){
			for (Movimentacaoorigem origem : origens) {
				Documento documento = origem.getDocumento();
				if(documento != null && !notaDocumentoService.isDocumentoWebservice(documento)){
					boolean aBaixar =
						(Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse()) &&
						(Documentoacao.AUTORIZADA.equals(documento.getDocumentoacao()) || Documentoacao.AUTORIZADA_PARCIAL.equals(documento.getDocumentoacao())))
						||
						(Documentoclasse.OBJ_RECEBER.equals(documento.getDocumentoclasse()) && Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao()))
					;
					if(aBaixar){
						documento = documentoService.load(documento);
						documento.setDocumentoacao(Documentoacao.BAIXADA);
						documentoService.updateDocumentoacao(documento);
						Documentohistorico historicoDocumento = documentohistoricoService.geraHistoricoDocumento(documento);
						historicoDocumento.setObservacao("Baixada devido ao estorno da movimenta��o correspondente.");
						documentohistoricoService.saveOrUpdateNoUseTransaction(historicoDocumento);
					}
				}
			}
		}
		
		movimentacaohistoricoService.saveOrUpdateNoUseTransaction(historico);
		mov.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		movimentacaoService.estornarMovimentacao(mov);
	}
	
	/**
	 * M�todo utilizado quando a a��o for para cancelar uma movimenta��o financeira.
	 * Se a movimenta��o teve origem em Conta a pagar/receber, Agendamento ou foi gerada, as A��es das origens s�o atualizadas de acordo
	 * com este cancelamento, bem como o hist�rico de cada um.
	 * PS.: O m�todo deve ser usado em uma Transaction pois possui muitos updates e inserts no banco.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#saveOrUpdateNoUseTransaction(Movimentacaohistorico)
	 * @see #updateAcao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoorigemService#findByMovimentacao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#retornaDocumentosAcaoAnt(Documento, Documentoacao, String)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#geraHistoricoAgendamento(Agendamento)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#saveOrUpdateNoUseTransaction(AgendamentoHistorico)
	 * @see #findMovimentacaoDestino(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#geraHistoricoMovimentacao(Movimentacao)
	 * @param movimentacao
	 * @param movimentacaohistorico
	 * @author Fl�vio Tavares
	 */
	public void cancelarOrigensMovimentacao(Movimentacao movimentacao, Movimentacaohistorico movimentacaohistorico){
		Movimentacaoacao acao = movimentacao.getMovimentacaoacao();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		
		movimentacaohistorico.setMovimentacaoacao(Movimentacaoacao.CANCEL_MOVIMENTACAO);
		movimentacaohistorico.setCdusuarioaltera(cdusuarioaltera);
		movimentacaohistorico.setDtaltera(hoje);
		movimentacaohistoricoService.saveOrUpdateNoUseTransaction(movimentacaohistorico);
		this.updateAcao(movimentacao);

		List<Movimentacaoorigem> origens = movimentacaoorigemService.findByMovimentacao(movimentacao);
		if(origens == null){
			return;
		}
		
		for (Movimentacaoorigem origem : origens) {
			Documento documento = origem.getDocumento(); 
			Agendamento agendamento = origem.getAgendamento();
			if(documento != null){
				List<Movimentacao> listaMovimentacao = this.findByDocumentoVariasMovimentacoes(documento);
				
				if(listaMovimentacao == null || listaMovimentacao.size() == 0){
					notaDocumentoService.estornaNotaDocumentoWebservice(documento);
					
					if(Documentoclasse.OBJ_RECEBER.equals(documento.getDocumentoclasse()) &&
							documento.getPessoa() != null){
						List<Valecompraorigem> listaValecompraCredito = valecompraorigemService.findByDocumento(documento, Tipooperacao.TIPO_CREDITO);
						if(SinedUtil.isListNotEmpty(listaValecompraCredito)){
							for(Valecompraorigem valecompraorigem : listaValecompraCredito){
								if(valecompraorigem.getValecompra() != null){
									valecompraService.delete(valecompraorigem.getValecompra());
								}
							}
						}
					}
					
					// A MOVIMENTA��O TEVE ORIGEM ORIGEM EM CONTA PAGAR/RECEBER
					documentoService.retornaDocumentosAcaoAnt(documento, Documentoacao.CANCEL_MOVIMENTACAO, null, true);
					
					// COLOCA NA TELA O AVISO
					notaDocumentoService.validaEstornoContaWebserviceTrue(NeoWeb.getRequestContext(), documento);
				} else {
					if(documento.getCddocumento() != null && documento.getDocumentoclasse() != null &&
							Documentoclasse.OBJ_RECEBER.equals(documento.getDocumentoclasse())){ 
						documento.setDocumentoacao(Documentoacao.BAIXADA_PARCIAL);
						documentoService.updateDocumentoacao(documento);
					}
					
					documento.setDocumentoacao(Documentoacao.CANCEL_MOVIMENTACAO);

					Documentohistorico dh = documentohistoricoService.geraHistoricoDocumento(documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
				}

			}else 
				if(agendamento != null){
					// A MOVIMENTA��O TEVE ORIGEM ORIGEM EM AGENDAMENTO

					AgendamentoHistorico h = agendamentoHistoricoService.geraHistoricoAgendamento(agendamento);
					h.setAgendamentoAcao(new AgendamentoAcao(AgendamentoAcao.CANCEL_MOVIMENTACAO));
					h.setDtaltera(hoje);
					h.setCdusuarioaltera(cdusuarioaltera);
					h.setObservacao(movimentacaohistorico.getObservacao());

					agendamentoHistoricoService.saveOrUpdateNoUseTransaction(h);
				}else{
					// A MOVIMENTA��O FOI GERADA DE UMA CONTA PARA OUTRA
					Movimentacao destino = this.findMovimentacaoDestino(movimentacao);
					if(destino != null){
						Movimentacaohistorico historicoDestino = movimentacaohistoricoService.geraHistoricoMovimentacao(destino);
						historicoDestino.setObservacao(movimentacaohistorico.getObservacao());
						historicoDestino.setMovimentacaoacao(Movimentacaoacao.CANCEL_MOVIMENTACAO);
						movimentacaohistoricoService.saveOrUpdateNoUseTransaction(historicoDestino);
						destino.setMovimentacaoacao(acao);
						this.updateAcao(destino);
					}
				}
		}
		
		Movimentacao movimentacaoWithCheque = loadWithCheque(movimentacao);
		if(movimentacaoWithCheque != null && movimentacaoWithCheque.getCdmovimentacao() != null && 
				movimentacaoWithCheque.getCheque() != null && 
				movimentacaoWithCheque.getCheque().getCdcheque() != null){
			executarUpdateSituacaoChequeAposCancelamentoMovimentacao(movimentacaoWithCheque.getCheque(), movimentacaoWithCheque);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findMovimentacaoArquivo
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findMovimentacaoArquivo(String whereIn) {
		return movimentacaoDAO.findMovimentacaoArquivo(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateConciliacao
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void updateConciliacao(Movimentacao movimentacao) {
		movimentacaoDAO.updateConciliacao(movimentacao);
	}
	
	/**
	 * Se acaso a tela para no validate este m�todo preenche as informa��es antigas que j� foram digitadas.
	 * 
	 * @param listaAntiga
	 * @param historico
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void preencheValoresAntigos(List<Movimentacao> listaAntiga, String historico, Movimentacao movimentacao) {
		if (listaAntiga != null) {
			for (Movimentacao mov : listaAntiga) {
				if (mov.getCdmovimentacao().equals(movimentacao.getCdmovimentacao())) {
					if (mov.getHistorico() != null) {
						historico = mov.getHistorico();
					}
					if (mov.getDtbanco() != null) {
						movimentacao.setDtbanco(mov.getDtbanco());
					}
					if (mov.getChecknum() != null) {
						movimentacao.setChecknum(mov.getChecknum());
					}
				}
			}
		}
		
		if (historico == null) {
			historico = movimentacao.getAux_movimentacao().getMovimentacaodescricao();
		}
		movimentacao.setHistorico(historico);
	}
	
	/**
	 * Atualiza a movimenta��o e salva todos os seus hist�ricos correspondentes na concilia��o banc�ria.
	 * 
	 * <b>OBS.: Os savesOrUpdates utilizados em chamadas desta fun��o para dentro N�O
	 *    devem possuir transactions. Quem deve garantir a transa��o ser� o m�todo
	 *    que invocar esta fun��o.</b>
	 *    
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#geraHistoricoMovimentacao
	 * @see #saveOrUpdateConciliacao(Movimentacao, Movimentacaohistorico)
	 * @param bean
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions 
	 */
	public void salvaMovimentacaoConciliacao(Movimentacao movimentacao, String observacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
//		movimentacao.setObservacaocopiacheque(observacao);
		if(movimentacao.getObservacaocopiacheque() == null){
			Movimentacao mov = movimentacaoService.load(movimentacao, "movimentacao.cdmovimentacao, movimentacao.observacaocopiacheque");
			if(mov != null){
				movimentacao.setObservacaocopiacheque(mov.getObservacaocopiacheque());
			}
		}
		movimentacao.setObservacaoConciliacao(observacao);
		movimentacao.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
		Movimentacaohistorico mh = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
		this.saveOrUpdateConciliacao(movimentacao, mh);
		
		List<Movimentacao> listaMovimentacaoOrigemDevolucao = findOrigemDevolucao(movimentacao, Boolean.FALSE);
		if(SinedUtil.isListNotEmpty(listaMovimentacaoOrigemDevolucao)){
			for(Movimentacao movimentacaoDevolucao : listaMovimentacaoOrigemDevolucao){
				if(movimentacaoDevolucao.getMovimentacaoacao() != null && 
						!Movimentacaoacao.CANCELADA.equals(movimentacaoDevolucao.getMovimentacaoacao()) &&
						!Movimentacaoacao.CONCILIADA.equals(movimentacaoDevolucao.getMovimentacaoacao())){
					movimentacaoDevolucao.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
					movimentacaoDevolucao.setDtbanco(movimentacao.getDtbanco());
					
					Movimentacaohistorico mhDevolucao = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacaoDevolucao);
					mhDevolucao.setObservacao("Concilia��o realizada ap�s a concilia��o da movimenta��o " +
							"<a href=\"javascript:visualizaMovimentacao("+ 
							movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
					
					this.saveOrUpdateConciliacao(movimentacaoDevolucao, mhDevolucao);
				}
			}
		}
		
		Movimentacao mov = this.loadMovimentacaoWithCheque(movimentacao);
		if(mov.getCheque() != null && mov.getCheque().getCdcheque() != null){
			Movimentacao ultimMov = this.loadUltimaMovimentacaoWithCheque(mov.getCheque());
			if((ultimMov == null || ultimMov.equals(mov)) && (mov.getCheque().getChequesituacao() == null || !Chequesituacao.DEVOLVIDO.equals(mov.getCheque().getChequesituacao()))){
				mov.getCheque().setChequesituacao(Chequesituacao.COMPENSADO);
				chequeService.updateSituacao(mov.getCheque().getCdcheque().toString(), mov.getCheque().getChequesituacao());
				Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoConciliacao(mov.getCheque(), mov, observacao);
				chequehistoricoService.saveOrUpdate(chequehistorico);
			}
		}
		
		List<Movimentacao> listaMovimentacaoTaxa = findMovimenacaoTaxa(movimentacao);
		if(SinedUtil.isListNotEmpty(listaMovimentacaoTaxa)){
			for(Movimentacao movTaxa : listaMovimentacaoTaxa){
				if(movTaxa.getMovimentacaoacao() != null && Movimentacaoacao.NORMAL.equals(movTaxa.getMovimentacaoacao())){
					movTaxa.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
					movTaxa.setDtbanco(movimentacao.getDtbanco());
					Movimentacaohistorico mhTaxa = movimentacaohistoricoService.geraHistoricoMovimentacao(movTaxa);
					this.saveOrUpdateConciliacao(movTaxa, mhTaxa);
				}
			}
		}
	}
	
	public void salvaMovimentacaoConciliacaoAntecipacaoCredito(Movimentacao movimentacao, String observacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		movimentacao.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
		movimentacao.setObservacaocopiacheque(observacao);
		movimentacao.setObservacao(observacao);
		Movimentacaohistorico mh = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacao);
		this.saveOrUpdateConciliacao(movimentacao, mh);
		
		Movimentacao mov = this.loadMovimentacaoWithCheque(movimentacao);
		if(mov.getCheque() != null && mov.getCheque().getCdcheque() != null){
			Movimentacao ultimMov = this.loadUltimaMovimentacaoWithCheque(mov.getCheque());
			if(ultimMov == null || ultimMov.equals(mov)){
				mov.getCheque().setChequesituacao(Chequesituacao.COMPENSADO);
				chequeService.updateSituacao(mov.getCheque().getCdcheque().toString(), mov.getCheque().getChequesituacao());
				Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoConciliacao(mov.getCheque(), mov, (observacao != null && !"".equals(observacao) ? observacao : "Antecipa��o de Cr�dito"));
				chequehistoricoService.saveOrUpdate(chequehistorico);
			}
		}
	}
	
	/**
	 * Agrupa os "saves or updates" de concilia��o banc�ria.
	 * OBS.: Deve ser colocado dentro de uma transaction para garantir a integridade dos registros.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaohistoricoService#saveOrUpdateNoUseTransaction(Object)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#updateConciliacao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#updateAcao(Movimentacao)
	 * @param movimentacao
	 * @param mh
	 * @author Hugo Ferreira - revis�o das transactions 
	 */
	public void saveOrUpdateConciliacao(Movimentacao movimentacao, Movimentacaohistorico mh) {
		movimentacaohistoricoService.saveOrUpdate(mh);
		if(movimentacao.getHistorico() != null && !"".equals(movimentacao.getHistorico().trim())){
			movimentacaoService.updateHistorico(movimentacao);
		}
		movimentacaoService.updateConciliacao(movimentacao);
		movimentacaoService.updateAcao(movimentacao);
	}
	
	public void updateHistorico(Movimentacao movimentacao) {
		movimentacaoDAO.updateHistorico(movimentacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#carregaMovimentacao
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movimentacao carregaMovimentacao(Movimentacao bean) {
		return movimentacaoDAO.carregaMovimentacao(bean);
	}
	
	public List<Movimentacao> carregaMovimentacao(String whereIn) {
		return movimentacaoDAO.carregaMovimentacao(whereIn);
	}
	
	/**
	 * Carrega a movimenta��o com rateio e seus itens de rateio.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#carregaMovimentacao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio(Rateio)
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacao carregaMovimentacaoComRateio(Movimentacao bean) {
		Movimentacao movimentacao = movimentacaoDAO.carregaMovimentacao(bean);
		/*
		 * Os itens de rateio s�o colocados em queries diferentes por que existe um problema no QueryBuilder. N�o funciona
		 * corretamente se estiverem na mesma query.
		 */
		if(movimentacao.getRateio() != null){
			movimentacao.getRateio().setListaRateioitem(rateioitemService.findByRateio(movimentacao.getRateio()));
		}
		return movimentacao;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findByConta
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findByConta(Conta conta){
		return movimentacaoDAO.findByConta(conta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findByContaTipoOperacao
	 * @param conta
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findByContaTipoOperacao(Conta conta, Tipooperacao tipo,String whereNotIn) {
		return movimentacaoDAO.findByContaTipoOperacao(conta, tipo, whereNotIn);
	}
	
	/**
	 * M�todo que separa as movimenta��es que foram encontradas no arquivo e no sined.
	 * 
	 * @param bean
	 * @param listaConciliacao
	 * @param listaCheque
	 * @author Rodrigo Freitas
	 */
	public void separacaoEncontrada(ArquivoConciliacaoBean bean, List<ConciliacaoBean> listaConciliacao,  List<ConciliacaoBean> listaCheque) {
		List<ConciliacaoBean> listaCredito = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaDebito = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaEncontrada = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaDebitoEncontrado = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaCreditoEncontrado = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaDataPostedPosteriorCredito = new ArrayList<ConciliacaoBean>();
		List<ConciliacaoBean> listaDataPostedPosteriorDebito = new ArrayList<ConciliacaoBean>();
		
		for (ConciliacaoBean conc : listaCheque) {
			if (conc.getDatabanco()!= null && conc.getDtserver() != null && conc.getDatabanco().compareTo(conc.getDtserver()) >= 0) {
				if (conc.getMovimentacao() != null) {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaDataPostedPosteriorCredito.add(conc);
					} else if (conc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
						listaDataPostedPosteriorDebito.add(conc);
					} else {
						listaEncontrada.add(conc);
					}
				} else {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaDataPostedPosteriorCredito.add(conc);
					} else {
						listaDataPostedPosteriorDebito.add(conc);
					}
				}
			} else {
				if (conc.getMovimentacao() != null) {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaCreditoEncontrado.add(conc);
					} else if (conc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
						listaDebitoEncontrado.add(conc);
					} else {
						listaEncontrada.add(conc);
					}
				} else {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaCredito.add(conc);
					} else {
						listaDebito.add(conc);
					}
				}
			}
		}
		
		for (ConciliacaoBean conc : listaConciliacao) {
			if (conc.getDatabanco() != null && conc.getDtserver() != null && conc.getDatabanco().compareTo(conc.getDtserver()) >= 0) {
				if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
					listaDataPostedPosteriorCredito.add(conc);
				} else if (conc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
					listaDataPostedPosteriorDebito.add(conc);
				} else {
					listaEncontrada.add(conc);
				}
			} else {
				if (conc.getMovimentacao() != null) {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaCreditoEncontrado.add(conc);
					} else if (conc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
						listaDebitoEncontrado.add(conc);
					} else {
						listaEncontrada.add(conc);
					}
				} else {
					if (conc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
						listaCredito.add(conc);
					} else {
						listaDebito.add(conc);
					}
				}
			}
		}
		
		bean.setListaCredito(listaCredito);
		bean.setListaDebito(listaDebito);
		bean.setListaEncontrada(listaEncontrada);
		bean.setListaDebitoEncontrado(listaDebitoEncontrado);
		bean.setListaCreditoEncontrado(listaCreditoEncontrado);
		bean.setListaDataPostedPosteriorCredito(listaDataPostedPosteriorCredito);
		bean.setListaDataPostedPosteriorDebito(listaDataPostedPosteriorDebito);
	}
	
	/**
	 * Faz a compara��o da movimenta��o e a transa��o do arquivo pelo valor.
	 * 
	 * @param listaConciliacao
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void comparacaoValor(List<ConciliacaoBean> listaConciliacao,
			List<Movimentacao> listaMovimentacao) {
		for (ConciliacaoBean conc2 : listaConciliacao){
			if(conc2.getFechamentofinanceiro() != null && conc2.getFechamentofinanceiro())
				continue;
			
//			S� ENTRA NO LOOP DE COMPARA��O COM MOVIMETA��ES SE N�O TIVER ACHADO AINDA
			if (conc2.getMovimentacao() == null && (conc2.getAssociada() == null || !conc2.getAssociada())) {
//				FAZ A COMPARA��O SOMENTE POR VALOR
				for (Movimentacao movimentacao : listaMovimentacao) {
					if(movimentacao.getAssociada() == null || !movimentacao.getAssociada()){
						if (movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL) && conc2.getValor().compareTo(movimentacao.getValor()) == 0 && movimentacao.getTipooperacao().equals(conc2.getTipooperacao())) {
							if(conc2.getAssociada() == null || !conc2.getAssociada()){
								conc2.setMovimentacao(movimentacao);
								conc2.setAssociada(true);
								movimentacao.setAssociada(true);
							} else {
								conc2.setMovimentacao(null);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Faz a compara��o da movimenta��o e a transa��o do arquivo pelo valor e pela data.
	 * 
	 * @param listaConciliacao
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void comparacaoDataValor(List<ConciliacaoBean> listaConciliacao,
			List<Movimentacao> listaMovimentacao) {
		for (ConciliacaoBean conc : listaConciliacao) {
			if(conc.getFechamentofinanceiro() != null && conc.getFechamentofinanceiro())
				continue;
			
			for (Movimentacao movimentacao : listaMovimentacao) {
				if(movimentacao.getAssociada() == null || !movimentacao.getAssociada()){
//					FAZ A COMPARA��O PELA DATA E VALOR
					if (movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL) && SinedDateUtils.equalsIgnoreHour(new java.sql.Date(conc.getDatabanco().getTime()), movimentacao.getDtmovimentacao()) && 
							conc.getValor().compareTo(movimentacao.getValor()) == 0 && movimentacao.getTipooperacao().equals(conc.getTipooperacao())) {
						if(conc.getAssociada() == null || !conc.getAssociada()){
							conc.setMovimentacao(movimentacao);
							conc.setAssociada(true);
							movimentacao.setAssociada(true);
						} else {
							conc.setMovimentacao(null);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Pelo tipo cheque faz a compara��o do n�mero do cheque.
	 * 
	 * @param listaCheque
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public List<ConciliacaoBean> comparacaoCheque(List<ConciliacaoBean> listaCheque, List<Movimentacao> listaMovimentacao) {
		List<ConciliacaoBean> lista = new ArrayList<ConciliacaoBean>();
		
		for (ConciliacaoBean cheque : listaCheque) {
			if(cheque.getFechamentofinanceiro() != null && cheque.getFechamentofinanceiro())
				continue;
			
			String semZerosNumero = cheque.getChecknum();
			try{
				semZerosNumero = Integer.parseInt(semZerosNumero) + "";
			} catch (Exception e) {
			}
			for (Movimentacao movimentacao : listaMovimentacao) {
				if(movimentacao.getAssociada() == null || !movimentacao.getAssociada()){
					if (movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL) && 
							movimentacao.getCheque() != null && 
							movimentacao.getCheque().getNumero() != null && 
							(cheque.getChecknum().equals(movimentacao.getCheque().getNumero().toString()) || 
									semZerosNumero.equals(movimentacao.getCheque().getNumero().toString()) )&& 
							movimentacao.getTipooperacao().equals(cheque.getTipooperacao())) {
						if(cheque.getAssociada() == null || !cheque.getAssociada()){
							cheque.setMovimentacao(movimentacao);
							cheque.setAssociada(true);
							movimentacao.setAssociada(true);
						} else {
							cheque.setMovimentacao(null);
						}
					}
				}
			}
		}
		
		for (int i = 0; i < listaCheque.size(); i++) {
			if(listaCheque.get(i).getMovimentacao() != null){
				lista.add(listaCheque.get(i));
				listaCheque.remove(i);
				i--;
			}
		}
		
		return lista;
		
	}
	
	
	
	/**
	 * Retira o valor da string do arquivo.
	 * 
	 * @param conciliacaoBean
	 * @param tramt
	 * @author Rodrigo Freitas
	 * @param achouTipoOperaco 
	 */
	public void processaValor(ConciliacaoBean conciliacaoBean, String tramt, boolean achouTipoOperacao) {
		Double valor;
		String substring = null;
		if (tramt.contains("</TRNAMT>")) {
			substring = tramt.substring(tramt.indexOf(">")+1,tramt.indexOf("</"));
		} else {
			substring = tramt.substring(tramt.indexOf(">")+1,tramt.length());
		}
		
		if(substring != null && !substring.trim().equals("")){
			String valorformatado = "";
			String substringvalor = substring.replace(",", ".");
			String valorsplit[] = substringvalor.split("\\.");
			for(int i = 0; i < valorsplit.length ; i++){
				if(valorsplit.length != i+1){
					valorformatado += valorsplit[i];
				}else{
					valorformatado += "." + valorsplit[i];
				}
			}
			valor = Double.parseDouble(valorformatado);
		} else {
			valor = 0d;
		}
		
		if(!achouTipoOperacao){
			if (valor < 0.0) {
				conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			} else {
				conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			}
		}
		
		if (valor < 0.0) {
			valor = valor*(-1);
		}
		conciliacaoBean.setValor(new Money(valor));
	}
	
	/**
	 * Retira a data do banco que est� na string do arquivo de concilia��o.
	 * 
	 * @param conciliacaoBean
	 * @param dtposted
	 * @param formatter
	 * @throws ParseException
	 * @author Rodrigo Freitas
	 */
	public void processaDatabanco(ConciliacaoBean conciliacaoBean, String dtposted, DateFormat formatter) throws ParseException {
		conciliacaoBean.setDatabanco((java.util.Date)formatter.parse(dtposted.substring(dtposted.indexOf(">")+1, dtposted.indexOf(">")+9)));
		conciliacaoBean.setDatastring(SinedDateUtils.toString(conciliacaoBean.getDatabanco()));
	}
	
	/**
	 * Retira a data do arquivo que est� na string do arquivo de concilia��o.
	 * 
	 * @param conciliacaoBean
	 * @param dtserver
	 * @param formatter
	 * @throws ParseException
	 * @author Jo�o Vitor
	 */
	public void processaDataServer(ConciliacaoBean conciliacaoBean, String dtserver, DateFormat formatter) throws ParseException {
		conciliacaoBean.setDtserver((java.util.Date)formatter.parse(dtserver.substring(dtserver.indexOf(">")+1, dtserver.indexOf(">")+9)));
		conciliacaoBean.setDtserverstring(SinedDateUtils.toString(conciliacaoBean.getDtserver()));
	}
	
	public void processaDataServer(ConciliacaoBean conciliacaoBean) {
		conciliacaoBean.setDtserver(new java.util.Date(System.currentTimeMillis()));
		conciliacaoBean.setDtserverstring(SinedDateUtils.toString(conciliacaoBean.getDtserver()));
	}
	
	/**
	 * De acordo com a string do arquivo de concilia��o ele acha a conta no sistema.
	 * 
	 * @param actid
	 * @param listaConta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Conta> comparacaoContas(String actid, List<Conta> listaConta) {
		List<Conta> contas = new ArrayList<Conta>();		
		for (Conta conta : listaConta) {
			if ((conta.getAgencia()+conta.getDvagencia()+conta.getNumber()+conta.getDvnumber()).equals(actid)) {
				contas.add(conta);
			} else if ((conta.getAgencia()+conta.getNumber()+conta.getDvnumber()).equals(actid)) {
				contas.add(conta);
			} else if ((conta.getAgencia()+conta.getDvagencia()+conta.getNumber()).equals(actid)) {
				contas.add(conta);
			} else if ((conta.getAgencia()+conta.getNumber()).equals(actid)) {
				contas.add(conta);
			}
		}
		return contas;
	}
	
	/**
	 * Faz a compara��o das movimenta��es e logo depois separa as encontradas e n�o encontradas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findByConta
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#comparacaoCheque
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#comparacaoDataValor
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#comparacaoValor
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#separacaoEncontrada
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#removeConciliadas
	 * @param bean
	 * @param listaConciliacao
	 * @param listaCheque
	 * @author Rodrigo Freitas
	 */
	public void comparacaoSeparacao(ArquivoConciliacaoBean bean, List<ConciliacaoBean> listaConciliacao) {
		//		ACHA TODAS AS MOVIMETA��ES DAQUELA CONTA COM SITUA��O NORMAL
				List<Movimentacao> listaMovimentacao = findByConta(bean.getConta());
				
				removeConciliadas(listaConciliacao, listaMovimentacao);
				
				Conta conta = contaService.loadContaWithEmpresa(bean.getConta());
				
				List<Empresa> listaEmpresa = new ArrayList<Empresa>();
				Set<Contaempresa> listaContaempresa = conta.getListaContaempresa();
				for (Contaempresa contaempresa : listaContaempresa) {
					if(contaempresa.getEmpresa() != null) listaEmpresa.add(contaempresa.getEmpresa());
				}
				
				List<Fechamentofinanceiro> listaFechamentofinanceiro = fechamentofinanceiroService.findByEmpresaConta(listaEmpresa, conta);
				if(listaFechamentofinanceiro != null && listaFechamentofinanceiro.size() > 0){
					for (ConciliacaoBean conciliacaoBean : listaConciliacao) {
						if(!fechamentofinanceiroService.isDataValida(listaFechamentofinanceiro, SinedDateUtils.castUtilDateForSqlDate(conciliacaoBean.getDatabanco()))){
							conciliacaoBean.setFechamentofinanceiro(Boolean.TRUE);
						}
					}
				}
				
		//		COMPARA PRIMEIRO OS CHEQUES
				List<ConciliacaoBean> listaCheque = comparacaoCheque(listaConciliacao, listaMovimentacao);
				
				comparacaoDataValor(listaConciliacao, listaMovimentacao);
				
				comparacaoValor(listaConciliacao, listaMovimentacao);
				
				for (int i = 0; i < listaCheque.size(); i++) {
					for (int j = 0; j < listaCheque.size(); j++) {
						if (i==j) {
							continue;
						}
						if (listaCheque.get(i).getMovimentacao() != null && listaCheque.get(j).getMovimentacao() != null) {
							if (listaCheque.get(i).equals(listaCheque.get(j))) {
								listaCheque.get(i).setDeleteMov(true);
								listaCheque.get(j).setDeleteMov(true);
							}
						}
					}
				}
				
				for (ConciliacaoBean conciliacaoBean : listaCheque) {
					if (conciliacaoBean.getDeleteMov() != null && conciliacaoBean.getDeleteMov()) {
						conciliacaoBean.setMovimentacao(null);
					}
				}
				
				for (int i = 0; i < listaConciliacao.size(); i++) {
					for (int j = 0; j < listaConciliacao.size(); j++) {
						if (i==j) {
							continue;
						}
						if (listaConciliacao.get(i).getMovimentacao() != null && listaConciliacao.get(j).getMovimentacao() != null) {
							if (listaConciliacao.get(i).equals(listaConciliacao.get(j))) {
								listaConciliacao.get(i).setDeleteMov(true);
								listaConciliacao.get(j).setDeleteMov(true);
							}
						}
					}
				}
				
				for (ConciliacaoBean conciliacaoBean : listaConciliacao) {
					if (conciliacaoBean.getDeleteMov() != null && conciliacaoBean.getDeleteMov()) {
						conciliacaoBean.setMovimentacao(null);
					}
				}
				
				separacaoEncontrada(bean, listaConciliacao, listaCheque);
	}
	
	/**
	 * Remove as movimenta��es que j� foram conciliadas.
	 * 
	 * @param lista
	 * @param listaMovimentacao
	 * @author Rodrigo Freitas
	 */
	public void removeConciliadas(List<ConciliacaoBean> lista, List<Movimentacao> listaMovimentacao) {
		ConciliacaoBean conc = null;
		List<ConciliacaoBean> listaEncontrado;
		for (int i = 0; i < lista.size(); i++) {
			conc = lista.get(i);
			listaEncontrado = new ArrayList<ConciliacaoBean>();
			for (Movimentacao movimentacao : listaMovimentacao) {
				if (movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CONCILIADA) 
						&& conc.getChecknum().equals(movimentacao.getChecknum())
						&& conc.getFitid().equals(movimentacao.getFitid())
						&& SinedDateUtils.equalsIgnoreHour(new Date(conc.getDatabanco().getTime()), movimentacao.getDtbanco())
						/*&& conc.getValor().getValue().doubleValue() == movimentacao.getValor().getValue().doubleValue()*/) {
					listaEncontrado.add(conc);
				}
			}
			
			if(listaEncontrado.size() == 1){
				lista.remove(i);
				i--;
				continue;
			}
			
			for (Movimentacao movimentacao : listaMovimentacao) {
				if (movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CONCILIADA) 
						&& conc.getChecknum().equals(movimentacao.getChecknum())
						&& SinedDateUtils.equalsIgnoreHour(new Date(conc.getDatabanco().getTime()), movimentacao.getDtbanco())
						&& conc.getFitid().equals(movimentacao.getFitid())
						/*&& conc.getValor().getValue().doubleValue() == movimentacao.getValor().getValue().doubleValue()*/) {
					lista.remove(i);
					i--;
					break;
				}
			}
		}
	}
	
	/**
	 * Salva lista de movimenta��es encontradas no arquivo de concilia��o banc�ria.
	 * 
	 * @see #salvaMovimentacaoConciliacao
	 * @param listaEncontrada
	 * @param observacao
	 * @author Rodrigo Freitas
	 */
	public Boolean salvaEncontrada(List<ConciliacaoBean> listaEncontrada, String observacao) {
		Boolean sucesso = false;
		Movimentacao movimentacao;
		if (SinedUtil.isListNotEmpty(listaEncontrada)) {
			for (ConciliacaoBean conciliacaoBean : listaEncontrada) {
				if (conciliacaoBean.getMovimentacao() != null && conciliacaoBean.getConciliar()!=null && conciliacaoBean.getConciliar()) {
					movimentacao = conciliacaoBean.getMovimentacao();
					movimentacao.setChecknum(conciliacaoBean.getChecknum());
					movimentacao.setFitid(conciliacaoBean.getFitid());
					movimentacao.setDtbanco(new java.sql.Date(conciliacaoBean.getDatabanco().getTime()));
					if (parametrogeralService.buscaValorPorNome(Parametrogeral.HISTORICOCONCILIACAOBANCO).equalsIgnoreCase("TRUE")) {
						movimentacao.setHistorico(conciliacaoBean.getMemo());
					}
					this.salvaMovimentacaoConciliacao(movimentacao,observacao + " (data do arquivo "+conciliacaoBean.getDtserverstring()+")");
					sucesso = true;
				}
			}
		}
		return sucesso;
	}
	
	/**
	 * Salca lista de movimenta��es que n�o foram encontradas e associadas. 
	 * 
	 * @see #salvaMovimentacaoConciliacao
	 * @param lista
	 * @param observacao
	 * @author Rodrigo Freitas
	 */
	public Boolean salvaLista(List<ConciliacaoBean> lista, String observacao) {
		Boolean sucesso = false;
		Movimentacao movimentacao;
		if (SinedUtil.isListNotEmpty(lista)) {
			for (ConciliacaoBean conciliacaoBean : lista) {
				if(conciliacaoBean.getConciliar() != null && conciliacaoBean.getConciliar()){
					if (conciliacaoBean.getMovimentacao() != null && conciliacaoBean.getMovimentacao().getStringMovimentacao() != null) {
						movimentacao = conciliacaoBean.getMovimentacao();
						movimentacao.setChecknum(conciliacaoBean.getChecknum());
						movimentacao.setFitid(conciliacaoBean.getFitid());
						movimentacao.setDtbanco(new java.sql.Date(conciliacaoBean.getDatabanco().getTime()));
						if (parametrogeralService.buscaValorPorNome(Parametrogeral.HISTORICOCONCILIACAOBANCO).equalsIgnoreCase("TRUE")) {
							movimentacao.setHistorico(conciliacaoBean.getMemo());
						}
						movimentacao.setObservacaocopiacheque(conciliacaoBean.getMemo());
						String [] listaMovimentacao = movimentacao.getStringMovimentacao().split(";");
						for (String string : listaMovimentacao) {
							movimentacao.setCdmovimentacao(Integer.parseInt(string));
							this.salvaMovimentacaoConciliacao(movimentacao,observacao  + " (data do arquivo "+conciliacaoBean.getDtserverstring()+")");
						}
						sucesso = true;
					}
				}
			}
		}
		return sucesso;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findByDocumento
	 * @param form
	 * @return
	 */
	public Movimentacao findByDocumento(Documento form) {
		return movimentacaoDAO.findByDocumento(form, null);
	}
	
	public Movimentacao findByDocumento(Documento form, String whereInMovimentacao) {
		return movimentacaoDAO.findByDocumento(form, whereInMovimentacao);
	}

	/**
	 * M�todo de agrupamento das fun��es de salvar concilia��o manual.
	 * Coloca todos os saves em transaction.
	 * 
	 * @see #salvaMovimentacaoConciliacao(Movimentacao, String)
	 * @param bean
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public void doConciliacaoManual(final Movimentacao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				movimentacaoService.salvaMovimentacaoConciliacao(bean, "Concilia��o manual");
				return null;
			}
		});
	}
	
	public void doConciliacaoManualAntecipacaoCredito(final Movimentacao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				movimentacaoService.salvaMovimentacaoConciliacao(bean, "Antecipa��o de cr�dito");
				return null;
			}
		});
	}
	
	/**
	 * M�todo de agrupamento das fun��es de salvar concilia��o autom�tica.
	 * Coloca todos os saves em transaction.
	 * 
	 * @see #salvaEncontrada(List, String)
	 * @see #salvaLista(List, String)
	 * @param bean
	 * @param observacao
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public Boolean doConciliacaoAutomatica(ArquivoConciliacaoBean bean, String observacao) {
		Boolean sucessoEncontrada = movimentacaoService.salvaEncontrada(bean.getListaEncontrada(), observacao);
		Boolean sucessoDebito = movimentacaoService.salvaLista(bean.getListaDebito(), observacao);
		Boolean sucessoCredito = movimentacaoService.salvaLista(bean.getListaCredito(), observacao);
		return sucessoEncontrada || sucessoDebito || sucessoCredito;
	}
	
	/**
	 * M�todo que coloca em uma transaction as a��es de cancelamento de arquivo banc�rio.
	 * 
	 * @see #findMovimentacaoArquivo(String)
	 * @see #cancelarOrigensMovimentacao(Movimentacao, Movimentacaohistorico)
	 * @see br.com.linkcom.sined.geral.service.ArquivobancarioService#updateSituacao(Arquivobancariosituacao, String)
	 * @param whereIn
	 */
	public void doCancelar(final String whereIn) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				List<Movimentacao> listaMovimentacao = movimentacaoService.findMovimentacaoArquivo(whereIn);
				Movimentacaohistorico mh = null;
				
				for (Movimentacao movimentacao : listaMovimentacao) {
					mh = new Movimentacaohistorico();
					mh.setMovimentacao(movimentacao);
					mh.setMovimentacaoacao(Movimentacaoacao.CANCELADA);
					movimentacao.setMovimentacaoacao(Movimentacaoacao.CANCELADA);
					movimentacaoService.cancelarOrigensMovimentacao(movimentacao, mh);			
				}
				
				arquivobancarioService.updateSituacao(Arquivobancariosituacao.CANCELADO,whereIn);
				return null;
			}
		});
	}
	
	/**
	 * Retorna as movimenta��es e seus itens de rateio relacionadas a um determinado projeto.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findByProjeto(Projeto)
	 * 
	 * @param projeto
	 * @return lista de Movimentacao
	 * @author Rodrigo Alvarenga
	 */
	public List<Movimentacao> findByProjeto(Projeto projeto) {
		List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();
		
		Object[] arrayObjeto;		
		Movimentacao movimentacao = new Movimentacao();
		Integer cdMovimentacaoAnterior = -1;		
		Rateio rateio = new Rateio();
		Integer cdRateioAnterior = -1;
		Rateioitem rateioItem;
		List<Rateioitem> listaRateioItem = new ArrayList<Rateioitem>();
		
		Integer cdMovimentacao;
		Date dtMovimentacao;
		Date dtBanco;
		Integer cdRateio;
		Integer cdRateioItem;
		Money valor;
		
		List<Object> listaObjeto = movimentacaoDAO.findByProjeto(projeto);
		if (listaObjeto != null) {
			for (Object objeto : listaObjeto) {
				if (objeto instanceof Object[]) {
					arrayObjeto = (Object[]) objeto;
					cdMovimentacao = arrayObjeto[0] != null ? (Integer) arrayObjeto[0] : null;
					dtMovimentacao = arrayObjeto[1] != null ? (Date) arrayObjeto[1] : null;
					dtBanco = arrayObjeto[2] != null ? (Date) arrayObjeto[2] : null;
					cdRateio = arrayObjeto[3] != null ? (Integer) arrayObjeto[3] : null;
					cdRateioItem = arrayObjeto[4] != null ? (Integer) arrayObjeto[4] : null;
					valor = arrayObjeto[5] != null ? (Money) arrayObjeto[5] : null;
					
					if (!cdMovimentacaoAnterior.equals(cdMovimentacao)) {//Movimenta��o nova
						
						if (!cdMovimentacaoAnterior.equals(-1)) { //Caso n�o seja a primeira movimenta��o, adiciona a anterior na lista.
							listaMovimentacao.add(movimentacao);
						}
						
						movimentacao = new Movimentacao();
						movimentacao.setCdmovimentacao(cdMovimentacao);
						movimentacao.setDtmovimentacao(dtMovimentacao);
						movimentacao.setDtbanco(dtBanco);
					}
					
					if (!cdRateioAnterior.equals(cdRateio)) {//Rateio novo
						rateio = new Rateio();
						rateio.setCdrateio(cdRateio);
						listaRateioItem = new ArrayList<Rateioitem>();
					}
					
					rateioItem = new Rateioitem();
					rateioItem.setCdrateioitem(cdRateioItem);
					rateioItem.setValor(valor);
					listaRateioItem.add(rateioItem);
					rateio.setListaRateioitem(listaRateioItem);
					movimentacao.setRateio(rateio);
					
					cdMovimentacaoAnterior = cdMovimentacao;
					cdRateioAnterior = cdRateio;
				}
			}
			
			//Adiciona o �ltimo registro de movimenta��o na lista
			if (movimentacao != null && movimentacao.getCdmovimentacao() != null) {
				listaMovimentacao.add(movimentacao);
			}
		}
		
		return listaMovimentacao;
	}	
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForAnaliseRecDespesaFlex 
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findForAnaliseRecDespesaFlex(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		return movimentacaoDAO.findForAnaliseRecDespesaFlex(filtro, identificador, max, campo, ordenacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#deleteFromDespesa
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void deleteFromDespesa(String whereIn) {
		movimentacaoDAO.deleteFromDespesa(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#deleteMovimentacao
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void deleteMovimentacao(Movimentacao movimentacao) {
		movimentacaoDAO.deleteMovimentacao(movimentacao);		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#callProcedureAtualizaMovimentacao
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaMovimentacao(Movimentacao movimentacao){
		movimentacaoDAO.callProcedureAtualizaMovimentacao(movimentacao);
	}
	
	public void callProcedureAtualizaMovimentacaoNecessidade(){
		movimentacaoDAO.callProcedureAtualizaMovimentacaoNecessidade();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForChequeMovimentacao
	 * @param whereIn
	 * @author Thiago Clemente
	 */
	public List<Movimentacao> findForChequeMovimentacao(String whereIn, String whereInCheque){
		return movimentacaoDAO.findForChequeMovimentacao(whereIn, whereInCheque);
	}	
	
	private String geraStringVazia(Integer tamanho){
		//String vazia = "";
		StringBuilder vazia = new StringBuilder();
		
		for (int i = 0; i < tamanho; i++) {
			//vazia += " ";
			vazia.append(" ");
		}
		
		//return vazia;
		return new String(vazia);
	}

	/**
	 * Cria o relat�rio de movimenta��o de cheque.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findForChequeMovimentacao
	 * @param request	 
	 * @author Thiago Clemente
	 */
	public ModelAndView createChequeMovimentacaoReport(List<ChequeMovimentacaoBean> listaRelatorio) throws CloneNotSupportedException {
		StringBuilder sb = new StringBuilder();
		
		for (ChequeMovimentacaoBean bean : listaRelatorio) {
			sb.append("\n");
			sb.append(this.geraStringVazia(91));
			sb.append(bean.getValor().toString());
			sb.append("\n");
			sb.append("\n");
			sb.append(this.geraStringVazia(22));
			if(bean.getValorextenso1() != null) sb.append(bean.getValorextenso1().toUpperCase());
			sb.append("\n");
			sb.append(this.geraStringVazia(7));
			if(bean.getValorextenso2() != null) sb.append(bean.getValorextenso2().toUpperCase());
			sb.append("\n");
			sb.append("\n");
			sb.append(this.geraStringVazia(8));
			if(bean.getNominal() != null) sb.append(br.com.linkcom.utils.StringUtils.stringCheia(Util.strings.tiraAcento(bean.getNominal().toUpperCase()), " ", 90, true));
			sb.append("\n");
			sb.append("\n");
			sb.append(this.geraStringVazia(54));
			sb.append(br.com.linkcom.utils.StringUtils.stringCheia(Util.strings.tiraAcento(bean.getCidade().toUpperCase()), " ", 20, true));
			sb.append(this.geraStringVazia(3));
			sb.append(br.com.linkcom.utils.StringUtils.stringCheia(bean.getDia(), "0", 2, false));
			sb.append(this.geraStringVazia(6));
			sb.append(br.com.linkcom.utils.StringUtils.stringCheia(Util.strings.tiraAcento(bean.getMes().toUpperCase()), " ", 15, true));
			sb.append(this.geraStringVazia(12));
			sb.append(bean.getAno());
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
			sb.append("\n");
		}
				
		Resource resource = new Resource("application/remote_printing", "impressaocheque_" + SinedUtil.datePatternForReport() + ".w3erp", sb.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	private static MovimentacaoService instance;
	public static MovimentacaoService getInstance(){
		if(instance == null){
			instance = NeoWeb.getObject(MovimentacaoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#getChequeDocumento
	 * 
	 * @param cdconta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movimentacao getChequeDocumento(Integer cdconta) {
		return movimentacaoDAO.getChequeDocumento(cdconta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateHistoricoWithDescricao
	 *
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void updateHistoricoWithDescricao(Movimentacao movimentacao) {
		movimentacaoDAO.updateHistoricoWithDescricao(movimentacao);
	}
	
	/**
	 * M�todo que cria relat�rio de movimenta��o financeira
	 * 
	 * @param filtro
	 * @return
	 * @Auhtor Tom�s Rabelo
	 */
	public IReport createMovimentacaoReport(MovimentacaoFiltro filtro) {
		Report report = new Report("/financeiro/movimentacaofinanceira");
		Report subreport = new Report("/financeiro/movimentacaofinanceira_subreport");
		report.addSubReport("MOVIMENTACAO_SUBREPORT", subreport);
		report.addParameter("EXIBE_PESSOA", Boolean.TRUE.equals(filtro.getExibirPessoa()));
		
		List<Movimentacao> movimentacoes = findforReportListagem(filtro);
		if(movimentacoes != null && !movimentacoes.isEmpty()){
			for (Movimentacao mov : movimentacoes) {
				if(mov.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
					mov.setValorrateioreportcredito(this.somaRateio(mov.getRateio()));
				} else if (mov.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
					mov.setValorrateioreportdebito(this.somaRateio(mov.getRateio()));
				}
			}
			if(Boolean.TRUE.equals(filtro.getExibirPessoa())){
				preencheNomepessoaByRelatorio(movimentacoes);
			}
			report.setDataSource(movimentacoes);
		}

		return report;
	}
	
	private void preencheNomepessoaByRelatorio(List<Movimentacao> movimentacoes){
		boolean listaRazaosocialCliente = parametrogeralService.getBoolean(Parametrogeral.LISTARRAZAOSOCIALCLIENTE);
		boolean listarRazaosocialAndNomefantasia = parametrogeralService.getBoolean(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA);
		for (Movimentacao mov : movimentacoes) {
			List<String> listaNomes = new ArrayList<String>();
			String nome = "";
			
			if(SinedUtil.isListNotEmpty(mov.getMovimentacaoorigemTaxa())){
				Movimentacaoorigem mo = mov.getMovimentacaoorigemTaxa().get(0);
				if(Util.objects.isPersistent(mo.getMovimentacao())){
					MovimentacaoFiltro filtro = new MovimentacaoFiltro(mo.getMovimentacao().getCdmovimentacao().toString(), true);
					filtro.setExibirPessoa(true);
					List<Movimentacao> listaMovimentacaoTaxa = movimentacaoDAO.findforReportListagem(filtro);
					if(SinedUtil.isListNotEmpty(listaMovimentacaoTaxa) && SinedUtil.isListNotEmpty(listaMovimentacaoTaxa.get(0).getListaMovimentacaoorigem())){
						nome = this.getNomePessoaByMovOrig(listaMovimentacaoTaxa.get(0).getListaMovimentacaoorigem().get(0), listaRazaosocialCliente, listarRazaosocialAndNomefantasia);
						listaNomes.add(nome);					
					}					
				}
			}else if(SinedUtil.isListNotEmpty(mov.getListaMovimentacaoorigem())){
				for(Movimentacaoorigem mo: mov.getListaMovimentacaoorigem()){
					nome = this.getNomePessoaByMovOrig(mo, listaRazaosocialCliente, listarRazaosocialAndNomefantasia);
					
					if(!listaNomes.contains(nome)){
						listaNomes.add(nome);
					}
				}				
			}
			
			mov.setNomepessoa("");
			if(!listaNomes.isEmpty()){
				mov.setNomepessoa(CollectionsUtil.concatenate(listaNomes, ", "));
			}
		}
	}
	
	private Money somaRateio(Rateio rateio) {
		Money total = new Money();
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
		if(listaRateioitem != null){
			for (Rateioitem ri : listaRateioitem) {
				if(ri.getValor() != null){
					total = total.add(ri.getValor());
				}
			}
		}
		return total;
	}
	
	private String getNomePessoaByMovOrig(Movimentacaoorigem mo, boolean listaRazaosocialCliente, boolean listarRazaosocialAndNomefantasia){
		String nome = "";
		if(mo.getDespesaviagemacerto() != null && mo.getDespesaviagemacerto().getColaborador() != null){
			nome = mo.getDespesaviagemacerto().getColaborador().getNome();
		}else if(mo.getDespesaviagemadiantamento() != null && mo.getDespesaviagemadiantamento().getColaborador() != null){
			nome = mo.getDespesaviagemadiantamento().getColaborador().getNome();
		}else if(mo.getAgendamento() != null){
			
			if(Tipopagamento.OUTROS.equals(mo.getAgendamento().getTipopagamento())){
				nome = mo.getAgendamento().getOutrospagamento();
			}else if(mo.getAgendamento().getPessoa() != null){
				Pessoa pessoa = mo.getAgendamento().getPessoa();
				if(Tipopagamento.CLIENTE.equals(mo.getAgendamento().getTipopagamento())){
					String nomePessoa = pessoa.getNome();
					String razaosocial = pessoa.getCliente() != null? Util.strings.emptyIfNull(pessoa.getCliente().getRazaosocial()): "";

					if(listarRazaosocialAndNomefantasia){
						nome = Util.strings.isNotEmpty(razaosocial)? razaosocial + " - " + nomePessoa: nomePessoa;
					}else if(listaRazaosocialCliente){
						nome = StringUtils.isNotBlank(razaosocial)? razaosocial : pessoa.getNome();
					}else{
						nome = pessoa.getNome();
					}
				}else if(Tipopagamento.FORNECEDOR.equals(mo.getAgendamento().getTipopagamento()) || Tipopagamento.COLABORADOR.equals(mo.getAgendamento().getTipopagamento())){
					String nomePessoa = pessoa.getNome();
					String razaosocial = pessoa.getFornecedorBean() != null? Util.strings.emptyIfNull(pessoa.getFornecedorBean().getRazaosocial()): "";

					if(listarRazaosocialAndNomefantasia){
						nome = Util.strings.isNotEmpty(razaosocial)? razaosocial + " - " + nomePessoa: nomePessoa;
					}else{
						nome = pessoa.getNome();
					}
				}
			}
		}else if(mo.getDocumento() != null){
			Documento documento = mo.getDocumento();
			if(Tipopagamento.OUTROS.equals(documento.getTipopagamento())){
				nome = documento.getOutrospagamento();
			}else if(documento.getPessoa() != null){
				if(Tipopagamento.CLIENTE.equals(documento.getTipopagamento())){
					String nomePessoa = documento.getPessoa().getNome();
					String razaosocial = documento.getPessoa().getCliente() != null? Util.strings.emptyIfNull(documento.getPessoa().getCliente().getRazaosocial()): "";

					if(listarRazaosocialAndNomefantasia){
						nome = Util.strings.isNotEmpty(razaosocial)? razaosocial + " - " + nomePessoa: nomePessoa;
					}else if(listaRazaosocialCliente){
						nome = StringUtils.isNotBlank(razaosocial)? razaosocial : documento.getPessoa().getNome();
					}else{
						nome = documento.getPessoa().getNome();
					}
					
				}else if(Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())){
					String nomePessoa = documento.getPessoa().getNome();
					String razaosocial = documento.getPessoa().getFornecedorBean() != null? Util.strings.emptyIfNull(documento.getPessoa().getFornecedorBean().getRazaosocial()): "";

					if(listarRazaosocialAndNomefantasia){
						nome = Util.strings.isNotEmpty(razaosocial)? razaosocial + " - " + nomePessoa: nomePessoa;
					}else{
						nome = documento.getPessoa().getNome();
					}
				}else if(Tipopagamento.COLABORADOR.equals(documento.getTipopagamento())){
					nome = documento.getPessoa().getNome();
				}
			}
		}
		return nome;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacao> findforReportListagem(MovimentacaoFiltro filtro) {
		return movimentacaoDAO.findforReportListagem(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param documentoclasse 
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimaContaPagarReceberBaixada(Documentoclasse documentoclasse) {
		return movimentacaoDAO.getDtUltimaContaPagarReceberBaixada(documentoclasse);
	}
	
	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosMovimentacaoFinanceira(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Date ultimaConciliacao = getDtUltimaConciliacao();
		Integer qtdeNaoConciliados = getQtdeTotalNaoConciliados();
		Integer qtdeTotal = getQtdeTotalMovimentacoes();
		DadosEstatisticosBean bean = new DadosEstatisticosBean(DadosEstatisticosBean.MOVIMENTACAO_FINANCEIRA, ultimoCadastro, qtdeNaoConciliados, qtdeTotal, ultimaConciliacao);
		return bean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalNaoConciliados() {
		return movimentacaoDAO.getQtdeTotalNaoConciliados();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimaConciliacao() {
		return movimentacaoDAO.getDtUltimaConciliacao();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalMovimentacoes() {
		return movimentacaoDAO.getQtdeTotalMovimentacoes();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return movimentacaoDAO.getDtUltimoCadastro();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacao> findMovimentacoesForBuscaGeral(String busca) {
		return movimentacaoDAO.findMovimentacoesForBuscaGeral(busca);
	}
	
	/**
	 * M�todo que cria relat�rio CSV, com os mesmo dados do relat�rio PDF
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource createMovimentacaoCSVReport(MovimentacaoFiltro filtro) {
		List<Movimentacao> movimentacoes = movimentacaoDAO.findforReportListagem(filtro);
		String tituloPessoa = "";
		if(Boolean.TRUE.equals(filtro.getExibirPessoa())){
			preencheNomepessoaByRelatorio(movimentacoes);
			tituloPessoa = "Pessoa;";
		}
		StringBuilder csv = new StringBuilder("V�nculo;"+tituloPessoa+"Data;Hist�rico;Documento;Cheque;O;Situa��o;Valor;Projeto;Centro de Custo;Conta Gerencial;Total\n");
		if(movimentacoes != null && !movimentacoes.isEmpty()){
			Money totalgeralrateio = new Money();
			Money totalgeral = new Money();
			
			for (Movimentacao movimentacao : movimentacoes) {
				csv.append(movimentacao.getConta().getNome()).append(";");
				if(Boolean.TRUE.equals(filtro.getExibirPessoa())){
					csv.append(movimentacao.getNomepessoa() == null ? "" : movimentacao.getNomepessoa()).append(";");
				}
				csv.append(movimentacao.getDataMovimentacao()).append(";");
				csv.append(movimentacao.getHistorico() == null ? "" : movimentacao.getHistorico()).append(";");
				csv.append(movimentacao.getChecknum() == null ? "" : movimentacao.getChecknum()).append(";");
				csv.append(movimentacao.getCheque() == null || movimentacao.getCheque().getNumero() == null ? "" : movimentacao.getCheque().getNumero()).append(";");
				csv.append(movimentacao.getTipooperacao().getNome()).append(";");
				csv.append(movimentacao.getMovimentacaoacao().getNome()).append(";");
				
				Money totalrateio = new Money();
				StringBuilder csvRateio = new StringBuilder();
				
				if(movimentacao.getRateio() != null && movimentacao.getRateio().getListaRateioitem() != null && !movimentacao.getRateio().getListaRateioitem().isEmpty()){
					for (Rateioitem rateioitem : movimentacao.getRateio().getListaRateioitem()) {
						totalrateio = totalrateio.add(rateioitem.getValor());
						
						csvRateio.append(";;;;;;;");
						if(Boolean.TRUE.equals(filtro.getExibirPessoa())){
							csvRateio.append(";");
						}
						csvRateio.append(rateioitem.getValor()).append(";");
						csvRateio.append(rateioitem.getProjeto() != null && rateioitem.getProjeto().getNome() != null ? rateioitem.getProjeto() : "").append(";");
						csvRateio.append(rateioitem.getCentrocusto() != null && rateioitem.getCentrocusto().getNome() != null ? rateioitem.getCentrocusto().getNome() : "").append(";");
						csvRateio.append(rateioitem.getContagerencial() != null && rateioitem.getContagerencial().getNome() != null ? rateioitem.getContagerencial().getNome() : "").append(";\n");
					}
				}
				
				csv.append(totalrateio).append(";;;;");
				csv.append(movimentacao.getValor()).append(";\n");
				csv.append(csvRateio.toString());
				
				if(movimentacao.getTipooperacao() != null){
					if(movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
						totalgeralrateio = totalgeralrateio.add(totalrateio);
						totalgeral = totalgeral.add(movimentacao.getValor());
					} else {
						totalgeralrateio = totalgeralrateio.subtract(totalrateio);
						totalgeral = totalgeral.subtract(movimentacao.getValor());
					}
				}
			}
			
			csv.append(";;;;;;;").append(totalgeralrateio).append(";;;;").append(totalgeral);
		}
		
		Resource resource = new Resource("text/csv", "movimentacao_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#isMovimentacaoDiferenteCreditoNormal(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMovimentacaoDiferenteCreditoNormal(String whereIn) {
		return movimentacaoDAO.isMovimentacaoDiferenteCreditoNormal(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.MovimentacaoDAO#getValorTotalMovimentacoes(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorTotalMovimentacoes(String whereIn) {
		return getValorTotalMovimentacoes(whereIn, null);
	}
	
	public Money getValorTotalMovimentacoes(String whereIn, Conta conta) {
		return movimentacaoDAO.getValorTotalMovimentacoes(whereIn, conta);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#isMovimentacaoDiferenteConciliada(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMovimentacaoDiferenteConciliadaOuCredito(String whereIn) {
		return movimentacaoDAO.isMovimentacaoDiferenteConciliadaOuCredito(whereIn);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForRegistrarDevolucao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findForRegistrarDevolucao(String whereIn) {
		return movimentacaoDAO.findForRegistrarDevolucao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO.findByDocumentoVariasMovimentacoes(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findByDocumentoVariasMovimentacoes(Documento documento) {
		return movimentacaoDAO.findByDocumentoVariasMovimentacoes(documento);
	}
	
	/**
	 * M�todo que busca a movimenta��o com a sua respectiva conta.
	 * 
	 * @param movimentacao
	 * @return
	 */
	public Movimentacao loadWithConta (Movimentacao movimentacao){
		return movimentacaoDAO.loadWithConta(movimentacao);
	}
	
	/**
	 * 
	 * @param listaCddocumentos
	 * @param movimentacao
	 */
	public void salvarVinculosDocumentos (final String[] listaCddocumentos, final Movimentacao movimentacao){
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {			
				for (String cddocumento : listaCddocumentos){
					
					Documento documento = documentoService.load(new Documento(Integer.parseInt(cddocumento)));
					documento.setDocumentoacao(Documentoacao.BAIXADA);
					documentoService.atualizaDocumentoacao(documento);			
					documentoService.callProcedureAtualizaDocumento(documento);
					
					Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
					movimentacaoorigem.setDocumento(documento);
					movimentacaoorigem.setMovimentacao(movimentacao);								
					movimentacaoorigemService.saveOrUpdate(movimentacaoorigem);			
				}	
			return null;
			}});				
	}
	
	/**
	 * M�todo que busca as movimenta��es pelos cdcheque.
	 * @param whereIn
	 * @return
	 * @author Thiers Euller
	 */
	public List<Movimentacao> findByCheque(String whereIn) {
		return movimentacaoDAO.findByCheque(whereIn);
	}
	
	public Movimentacao loadMovimentacaoWithCheque(Movimentacao movimentacao) {
		return movimentacaoDAO.loadMovimentacaoWithCheque(movimentacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#loadUltimaMovimentacaoWithCheque(Cheque cheque)
	*
	* @param cheque
	* @return
	* @since 24/03/2017
	* @author Luiz Fernando
	*/
	public Movimentacao loadUltimaMovimentacaoWithCheque(Cheque cheque) {
		return movimentacaoDAO.loadUltimaMovimentacaoWithCheque(cheque);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findMovimentacaoWithCheque(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 04/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findMovimentacaoWithCheque(String whereIn) {
		return movimentacaoDAO.findMovimentacaoWithCheque(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findOrigemByCheque(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Movimentacao findMovimentacaoByCheque(String whereIn) {
		return movimentacaoDAO.findMovimentacaoByCheque(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		return movimentacaoDAO.verificaListaFechamento(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findByCheque(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findByCheque(Cheque cheque) {
		return movimentacaoDAO.findByCheque(cheque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForGerarmovimentacao(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 * @since 17/09/2013
	 */
	public List<Movimentacao> findForGerarmovimentacao(Cheque cheque) {
		return movimentacaoDAO.findForGerarmovimentacao(cheque);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findByDespesaviagem(Despesaviagem despesaviagem, Tipooperacao tipooperacao)
	 *
	 * @param despesaviagem
	 * @param tipooperacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public List<Movimentacao> findByDespesaviagem(Despesaviagem despesaviagem, Tipooperacao tipooperacao) {
		return movimentacaoDAO.findByDespesaviagem(despesaviagem, tipooperacao);
	}
	
	/**
	 * M�todo que verifica se existe duplicidade no processamento do arquivo de retorno
	 *
	 * @param listaMov
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 * @param listaDocumentoNaoMarcadaByTarifa 
	 */
	public Boolean existDuplicidadeProcessamento(Movimentacao movimentacao, List<ArquivoRetornoCAIXABean> listaDocumentoNaoMarcadaByTarifa) {
		if(movimentacao != null && movimentacao.getDtmovimentacao() != null && 
				movimentacao.getValor() != null && 
				movimentacao.getHistorico() != null &&
				!"".equals(movimentacao.getHistorico())){
			
			//FEITO ISTO PARA AGRUPAR O VALOR DAS TARIFAS NAO SELECIONADAS
			Money valorTotalTarifa = new Money();
			if(listaDocumentoNaoMarcadaByTarifa != null && !listaDocumentoNaoMarcadaByTarifa.isEmpty()){
				for(ArquivoRetornoCAIXABean bean : listaDocumentoNaoMarcadaByTarifa){
					if(bean.getMotivoTarifaDesc() != null && bean.getMotivoTarifaDesc().contains(movimentacao.getHistorico())){  
						if(bean.getDtcredito() != null){
							if(SinedDateUtils.equalsIgnoreHour(bean.getDtcredito(), movimentacao.getDtmovimentacao())){
								if(bean.getValorTarifa() != null)
									valorTotalTarifa = bean.getValorTarifa();
								break;
							}
						}else if(bean.getDtocorrencia() != null){
							if(SinedDateUtils.equalsIgnoreHour(bean.getDtocorrencia(), movimentacao.getDtmovimentacao())){
								if(bean.getValorTarifa() != null)
									valorTotalTarifa = bean.getValorTarifa();
								break;
							}
						}
					}
				}
			}
			
			valorTotalTarifa = movimentacao.getValor().add(valorTotalTarifa);
			
			if(movimentacaoService.existMovimentacaoDataValorHistorico(movimentacao, valorTotalTarifa)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#existMovimentacaoDataValorHistorico(Movimentacao movimentacao)
	 *
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 * @param valorTotalTarifa 
	 */
	public Boolean existMovimentacaoDataValorHistorico(Movimentacao movimentacao, Money valorTotalTarifa) {
		return movimentacaoDAO.existMovimentacaoDataValorHistorico(movimentacao, valorTotalTarifa);
	}
	
	public IReport createArquivoConciliacaoReport(WebRequestContext request) {
		ArquivoConciliacaoBean bean = (ArquivoConciliacaoBean) request.getSession().getAttribute(ArquivoConciliacaoProcess.CONTA_SESSION); 
		Report report = new Report("financeiro/arquivoconciliacao");
		report.setDataSource(bean.getListaRelatorio());
		return report;
	}
	
	/**
	 * Caso a conta importada, do arquivo de retorno CEF, possua tarifa, 
	 * ser� gerada uma nova movimenta��o de d�bito para registrar o valor da tarifa.
	 * @param movimentacao
	 * @author Taidson
	 * @since 17/08/2010
	 */
	public void gerarMovimentacaoTarifa(Movimentacao movimentacao){
		if(movimentacao.getValorTarifa() != null && movimentacao.getValorTarifa().getValue().doubleValue() > 0.0){
			Set<Movimentacaohistorico> lista = new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class);
			
			Movimentacao novaMovimentacao = new Movimentacao();
			
			Rateioitem rateioitem = new Rateioitem();
			
			List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
			Rateio rateio = new Rateio();
			
			Centrocusto centrocusto = new Centrocusto();
			centrocusto.setCdcentrocusto(Integer.parseInt(parametrogeralService.getValorPorNome("CENTROCUSTO_TARIFA_CEF")));
			
			Contagerencial contagerencial = new Contagerencial();
			contagerencial.setCdcontagerencial(Integer.parseInt(parametrogeralService.getValorPorNome("CONTAGERENCIAL_TARIFA_CEF")));
			
			rateioitem.setCentrocusto(centrocusto);
			rateioitem.setContagerencial(contagerencial);
			
			rateioitem.setValor(movimentacao.getValorTarifa());
			listaRateioitem.add(rateioitem);
			rateio.setListaRateioitem(listaRateioitem);
			
			novaMovimentacao.setDtmovimentacao(movimentacao.getDtmovimentacao());
			novaMovimentacao.setConta(movimentacao.getConta());
			novaMovimentacao.setValor(movimentacao.getValorTarifa());
			novaMovimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			novaMovimentacao.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			novaMovimentacao.setRateio(rateio);
			novaMovimentacao.setHistorico(MotivoOcorrenciaTarifa.getDescricaoMotivoTarifa(movimentacao.getMotivoTarifa()));
			Movimentacaohistorico mh = new Movimentacaohistorico();
			mh.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			
			mh.setObservacao(MotivoOcorrenciaTarifa.getDescricaoMotivoTarifa(movimentacao.getMotivoTarifa()));
			mh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			mh.setDtaltera(new Timestamp(System.currentTimeMillis())); 
			
			lista.add(mh);
			novaMovimentacao.setListaMovimentacaohistorico(lista);
			
			if(!movimentacaoService.existDuplicidadeProcessamento(novaMovimentacao, null)){			
				movimentacaoService.saveOrUpdateNoUseTransaction(novaMovimentacao);
				movimentacaoService.callProcedureAtualizaMovimentacao(novaMovimentacao);
			}
		}
	}
	
	public Money calcularMovimentacoesByDocumento(Documento documento) {
		return calcularMovimentacoesByDocumento(documento, null);
	}
	
//	public Money calcularMovimentacoesByPessoa(Cliente cliente){
//		Money valorFinal = new Money(0.0);
//		List<Documento> listDoc = documentoService.findByPessoaDocumentoclasse(cliente, Documentoclasse.OBJ_RECEBER, true);
//		for (Documento documento : listDoc) {
//			valorFinal.add(calcularMovimentacoesByDocumento(documento, null));
//		}
//		return valorFinal;
//	}
	/**
	 * M�todo que busca todos os valores das movimenta��es j� vinculadas ao documento e calcula o que falta ser pago.
	 * 
	 * @param documento
	 * @param dtref 
	 * @return
	 */
	public Money calcularMovimentacoesByDocumento(Documento documento, Date dtref) {		
		Money valorRestante = documento.getValor().add(new Money(taxaService.getValorTaxas(documento, dtref)));
		Long valorMovimentacoesLong = movimentacaoDAO.calcularMovimentacoesByWhereInDocumento(documento.getCddocumento().toString()); 		  
		Money valortotalpago = new Money();
		if (valorMovimentacoesLong != null){
			Money valorMovimentacoes = new Money(valorMovimentacoesLong, true);
			valorRestante = valorRestante.subtract(valorMovimentacoes);
			valortotalpago = valorMovimentacoes;
		}
		
		List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByDocumento(documento);
		if(listaValecompraorigem != null && listaValecompraorigem.size() > 0){
			Money valorTotalValecompra = new Money();
			for (Valecompraorigem valecompraorigem : listaValecompraorigem) {
				if(valecompraorigem.getValecompra() != null && 
						valecompraorigem.getValecompra().getTipooperacao() != null &&
						valecompraorigem.getValecompra().getValor() != null){
					if(valecompraorigem.getValecompra().getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
						valorTotalValecompra = valorTotalValecompra.add(valecompraorigem.getValecompra().getValor());
					} else {
						valorTotalValecompra = valorTotalValecompra.subtract(valecompraorigem.getValecompra().getValor());
					}
				}
			}
			
			valorRestante = valorRestante.add(valorTotalValecompra);
			valortotalpago = valortotalpago.add(valorTotalValecompra);
			documento.setValortotalpago(valortotalpago);
		}
		
		List<HistoricoAntecipacao> listaHistoricoAntecipacao = historicoAntecipacaoService.findByDocumentoReferencia(documento);
		if(SinedUtil.isListNotEmpty(listaHistoricoAntecipacao)){
			Money valorTotalAntecipacao = new Money();
			for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoAntecipacao) {
				if(historicoAntecipacao.getValor() != null && historicoAntecipacao.getValor().getValue().doubleValue() > 0){
					valorTotalAntecipacao = valorTotalAntecipacao.add(historicoAntecipacao.getValor());
				}
			}
			
			if(valorTotalAntecipacao != null && valorTotalAntecipacao.getValue().doubleValue() > 0){
				valorRestante = valorRestante.subtract(valorTotalAntecipacao);
				valortotalpago = valortotalpago.add(valorTotalAntecipacao);
				documento.setValortotalpago(valortotalpago);
			}
		}
		
		return valorRestante;
	}
	
	
	
	public Money calcularMovimentacoesByWhereInDocumento(String whereInDocumento) {		
		Long valorMovimentacoesLong = movimentacaoDAO.calcularMovimentacoesByWhereInDocumento(whereInDocumento); 		  
		if (valorMovimentacoesLong != null){
			return new Money(valorMovimentacoesLong, true);
		} else return new Money();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForVerificarTipooperacaoByCheque(String whereInCheque)
	*
	* @param whereInCheque
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findForVerificarTipooperacaoByCheque(String whereInCheque) {
		return movimentacaoDAO.findForVerificarTipooperacaoByCheque(whereInCheque);
	}
	
	/**
	* M�todo que ajusta a lista de movimenta��o para retornar a ultima movimenta��o do cheque, considerando o tipo de opera��o da primeira movimenta��o
	*
	* @param listaMovimentacao
	* @param listaPrimeiraMovByCheque
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> ajustaListaSaveForRegistrardevolucao(List<Movimentacao> listaMovimentacao, List<Movimentacao> listaPrimeiraMovByCheque) {
		if(SinedUtil.isListEmpty(listaMovimentacao) || listaMovimentacao.size() == 1 || SinedUtil.isListEmpty(listaPrimeiraMovByCheque))
			return listaMovimentacao;
		
		List<Movimentacao> listaAjustada = new ArrayList<Movimentacao>();
		List<Cheque> listaCheque = new ArrayList<Cheque>();
		for(Movimentacao bean : listaMovimentacao){
			if(bean.getCheque() != null && bean.getCheque().getCdcheque() != null && 
					bean.getConta() != null && bean.getConta().getCdconta() != null &&
					bean.getTipooperacao() != null && bean.getTipooperacao().getCdtipooperacao() != null){
				
				if(listaCheque.contains(bean.getCheque())){
					continue;
				}
				
				for(Movimentacao movimentacao : listaPrimeiraMovByCheque){
					if(movimentacao.getCheque() != null && movimentacao.getCheque().equals(bean.getCheque()) &&
							movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().equals(bean.getTipooperacao())){
						listaCheque.add(bean.getCheque());
						listaAjustada.add(bean);
					}
				}
			}
		}
		return listaAjustada;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateMovimentacaoChequeSubstituto(String whereIn, Cheque chequeSubstituto)
	*
	* @param whereIn
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateMovimentacaoChequeSubstituto(String whereIn, Cheque chequeSubstituto) {
		movimentacaoDAO.updateMovimentacaoChequeSubstituto(whereIn, chequeSubstituto);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findForSubistituirByCheque(String whereIn)
	*
	* @param whereInCheque
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findForSubistituirByCheque(String whereInCheque) {
		List<Movimentacao> lista = movimentacaoDAO.findForSubistituirByCheque(whereInCheque);
		if(SinedUtil.isListNotEmpty(lista)){
			String whereInMov = CollectionsUtil.listAndConcatenate(lista, "cdmovimentacao", ",");
			setListaMovimentacaoorigem(lista, movimentacaoorigemService.findForSubstituicaoByMovimentacao(whereInMov));
		}
		return lista;
	}
	
	/**
	* M�todo que seta a lista de movimenta��o origem a sua respectiva movimenta��o
	*
	* @param lista
	* @param listaMovimentacaoorigem
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	private void setListaMovimentacaoorigem(List<Movimentacao> lista, List<Movimentacaoorigem> listaMovimentacaoorigem) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Movimentacao movimentacao : lista){
				movimentacao.setListaMovimentacaoorigem(new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class));
				if(SinedUtil.isListNotEmpty(listaMovimentacaoorigem)){
					if(movimentacao.getCdmovimentacao() != null){
						for (Iterator<Movimentacaoorigem> iterator = listaMovimentacaoorigem.iterator(); iterator.hasNext();) {
							Movimentacaoorigem movimentacaoorigem = (Movimentacaoorigem) iterator.next();
							if(movimentacaoorigem.getMovimentacao() != null && 
									movimentacaoorigem.getMovimentacao().getCdmovimentacao() != null && 
									movimentacao.getCdmovimentacao().equals(movimentacaoorigem.getMovimentacao().getCdmovimentacao())){
								movimentacao.getListaMovimentacaoorigem().add(movimentacaoorigem);
								iterator.remove();
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateSituacaoCanceladaBySubstituicaoCheque(String whereIn)
	*
	* @param whereIn
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateSituacaoCanceladaBySubstituicaoCheque(String whereIn) {
		movimentacaoDAO.updateSituacaoCanceladaBySubstituicaoCheque(whereIn);
	}
	
	/**
	* M�todo que cria uma movimenta��o referente a substituicao de cheque
	*
	* @param listaMovimentacaoSubstituicao
	* @param chequeSubstituto
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public Movimentacao createMovimentacaoBySubstituicaoCheque(List<Movimentacao> listaMovimentacaoSubstituicao, Cheque chequeSubstituto) {
		if(SinedUtil.isListEmpty(listaMovimentacaoSubstituicao) || chequeSubstituto == null)
			return null;
		
		Movimentacao movSubstituicao = listaMovimentacaoSubstituicao.get(0);
		
		Movimentacao movimentacaoNovo = new Movimentacao();
		movimentacaoNovo.setConta(movSubstituicao.getConta());
		movimentacaoNovo.setValor(chequeSubstituto.getValor());
		movimentacaoNovo.setTipooperacao(movSubstituicao.getTipooperacao());
		movimentacaoNovo.setFormapagamento(movSubstituicao.getFormapagamento());	
		movimentacaoNovo.setCheque(chequeSubstituto);
		movimentacaoNovo.setChecknum(movSubstituicao.getChecknum());
		movimentacaoNovo.setHistorico(movSubstituicao.getHistorico());
		movimentacaoNovo.setAux_movimentacao(movSubstituicao.getAux_movimentacao());
		movimentacaoNovo.setOperacaocontabil(movSubstituicao.getOperacaocontabil());
		movimentacaoNovo.setMovimentacaoacao(movSubstituicao.getMovimentacaoacao());
		movimentacaoNovo.setDtmovimentacao(chequeSubstituto.getDtbompara() != null ? 
				chequeSubstituto.getDtbompara() : new Date(System.currentTimeMillis()));
		
		if(movSubstituicao.getRateio() != null && SinedUtil.isListNotEmpty(movSubstituicao.getRateio().getListaRateioitem())){
			Rateio rateioNovo = new Rateio();
			List<Rateioitem> listaRateioitemNovo = new ArrayList<Rateioitem>();
			for(Rateioitem ri : movSubstituicao.getRateio().getListaRateioitem()){
				listaRateioitemNovo.add(new Rateioitem(ri.getContagerencial(),
													ri.getCentrocusto(),
													ri.getProjeto(),
													ri.getValor(),
													ri.getPercentual()));
			}
			rateioNovo.setListaRateioitem(listaRateioitemNovo);
			movimentacaoNovo.setRateio(rateioNovo);
			rateioService.atualizaValorRateio(movimentacaoNovo.getRateio(), chequeSubstituto.getValor());
		}
		
		StringBuilder linkHistorico = new StringBuilder();
		movimentacaoNovo.setListaMovimentacaoorigem(new ArrayList<Movimentacaoorigem>());
		for(Movimentacao mov : listaMovimentacaoSubstituicao){
			if(mov.getCdmovimentacao() != null){
				linkHistorico.append("<a href=\"javascript:visualizaMovimentacao("+ 
						mov.getCdmovimentacao()+");\">"+mov.getCdmovimentacao() +"</a>,");
			}
			if(SinedUtil.isListNotEmpty(mov.getListaMovimentacaoorigem())){
				for(Movimentacaoorigem movOrigem : mov.getListaMovimentacaoorigem()){
					Movimentacaoorigem movimentacaoorigemNova = new Movimentacaoorigem();
					movimentacaoorigemNova.setAgendamento(movOrigem.getAgendamento());
					movimentacaoorigemNova.setConta(movOrigem.getConta());
					movimentacaoorigemNova.setDespesaviagemacerto(movOrigem.getDespesaviagemacerto());
					movimentacaoorigemNova.setDespesaviagemadiantamento(movOrigem.getDespesaviagemadiantamento());
					movimentacaoorigemNova.setDocumento(movOrigem.getDocumento());
					movimentacaoNovo.getListaMovimentacaoorigem().add(movimentacaoorigemNova);
				}
			}
		}
		
		movimentacaoNovo.setListaMovimentacaohistorico(new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class));
		Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
		movimentacaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		movimentacaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		movimentacaohistorico.setMovimentacaoacao(movimentacaoNovo.getMovimentacaoacao());
		movimentacaohistorico.setObservacao("Substitui��o de cheque. Movimenta��es: " + linkHistorico.toString().substring(0, linkHistorico.toString().length()-1));
		movimentacaoNovo.getListaMovimentacaohistorico().add(movimentacaohistorico);		
		
		return movimentacaoNovo;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#existMovimentacaoSituacao(String whereIn, boolean notIn, Movimentacaoacao... movimentacaoacao)
	*
	* @param whereIn
	* @param notIn
	* @param movimentacaoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public Boolean existMovimentacaoSituacao(String whereIn, boolean notIn, Movimentacaoacao... movimentacaoacao) {
		return movimentacaoDAO.existMovimentacaoSituacao(whereIn, notIn, movimentacaoacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#isVinculoTipooperacao(String whereIn, Tipooperacao tipooperacao)
	*
	* @param whereIn
	* @param tipooperacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoTipooperacao(String whereIn, Tipooperacao tipooperacao) {
		return movimentacaoDAO.isVinculoTipooperacao(whereIn, tipooperacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public List<Movimentacao> findWithConta(String whereIn) {
		return movimentacaoDAO.findWithConta(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/08/2015
	 */
	public List<Movimentacao> findWithCheque(String whereIn) {
		return movimentacaoDAO.findWithCheque(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public List<Movimentacao> findForTransferenciaLote(String whereIn) {
		return movimentacaoDAO.findForTransferenciaLote(whereIn);
	}
	

	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#loadWithCheque(Movimentacao movimentacao)
	*
	* @param movimentacao
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	private Movimentacao loadWithCheque(Movimentacao movimentacao) {
		return movimentacaoDAO.loadWithCheque(movimentacao);
	}
	
	/**
	* M�todo que atualiza a situa��o do cheque de acordo com as duas �ltimas movimenta��es
	*
	* @param cheque
	* @param movimentacao
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	public void executarUpdateSituacaoChequeAposCancelamentoMovimentacao(Cheque cheque, Movimentacao movimentacao) {
		if(cheque != null && cheque.getCdcheque() != null && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			List<Movimentacao> listaMovimentacao = buscarMovimentacaoParaExecutarUpdateSituacaoCheque(cheque);
			
			boolean verificarMovimentacoesCanceladas = true;
			if(SinedUtil.isListNotEmpty(listaMovimentacao) && listaMovimentacao.size() > 1){
				Movimentacao movCancelada = null; 
				Movimentacao movAnterior = null;
				for(Movimentacao item : listaMovimentacao){
					if(movCancelada == null){
						movCancelada = item;
					}else if(item.getMovimentacaoacao() != null){
						if(!Movimentacaoacao.CANCELADA.getCdmovimentacaoacao().equals(item.getMovimentacaoacao().getCdmovimentacaoacao())){
							movAnterior = item;
							break;
						}
					}
				}
				
				if(movAnterior != null && movCancelada.getCdmovimentacao() != null && movCancelada.getCdmovimentacao().equals(movimentacao.getCdmovimentacao())){
					if(movAnterior.getMovimentacaoacao() != null){
						Chequesituacao chequesituacao = null;
						if(Movimentacaoacao.NORMAL.getCdmovimentacaoacao().equals(movAnterior.getMovimentacaoacao().getCdmovimentacaoacao())){
							chequesituacao = Chequesituacao.BAIXADO;
							Chequehistorico ch = chequehistoricoService.findHistoricoDevolvido(cheque, movAnterior);
							if(ch != null && ch.getChequesituacao() != null && Chequesituacao.DEVOLVIDO.equals(ch.getChequesituacao())){
								chequesituacao = ch.getChequesituacao();
							}
						}else if(Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao().equals(movAnterior.getMovimentacaoacao().getCdmovimentacaoacao())){
							chequesituacao = Chequesituacao.COMPENSADO;
							Chequehistorico ch = chequehistoricoService.findHistoricoDevolvido(cheque, movAnterior);
							if(ch != null && ch.getChequesituacao() != null && Chequesituacao.DEVOLVIDO.equals(ch.getChequesituacao())){
								chequesituacao = ch.getChequesituacao();
							}
						}
						if(chequesituacao != null){
							verificarMovimentacoesCanceladas = false;
							chequeService.updateSituacao(cheque.getCdcheque().toString(), chequesituacao);
							Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoCancelamentoMovimentacao(cheque, movimentacao);
							if(chequehistorico != null){
								chequehistoricoService.saveOrUpdate(chequehistorico);
							}
						}
					}
				}
			}
			
			if(verificarMovimentacoesCanceladas && !existeMovimentacaoEmAbertoByCheque(cheque.getCdcheque().toString())){
				chequeService.updateSituacao(cheque.getCdcheque().toString(), Chequesituacao.PREVISTO);
				Chequehistorico chequehistorico = chequehistoricoService.criaHistoricoCancelamentoMovimentacao(cheque, movimentacao);
				if(chequehistorico != null){
					chequehistoricoService.saveOrUpdate(chequehistorico);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#buscarMovimentacaoParaExecutarUpdateSituacaoCheque(Cheque cheque)
	*
	* @param cheque
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> buscarMovimentacaoParaExecutarUpdateSituacaoCheque(Cheque cheque) {
		return movimentacaoDAO.buscarMovimentacaoParaExecutarUpdateSituacaoCheque(cheque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param listaValorMovimentacao
	 * @param dtmovimentacao
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean verificaDuplicidade(String whereInValorMovimentacao, Date dtmovimentacao){
		return movimentacaoDAO.verificaDuplicidade(whereInValorMovimentacao, dtmovimentacao);
	}
	
	/**
	* M�todo que calcula o custo operacional
	*
	* @param filtro
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public Money calcularCustooperacional(CustooperacionalFiltro filtro) {
		Money totalMovimentacao = movimentacaoDAO.getValorTotalMovimentacoes(filtro);
		
		if(totalMovimentacao != null && totalMovimentacao.getValue().doubleValue() > 0){
			Double qtde = vendaService.getQtdeProducao(filtro);
			if(qtde != null && qtde > 0){
				return new Money(totalMovimentacao.getValue().doubleValue() / qtde);
			}
		}
		
		return new Money();
	}
	
	/**
	* M�todo que calcula o custo comercial
	*
	* @param filtro
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public Money calcularCustocomercial(CustooperacionalFiltro filtro) {
		Money totalMovimentacao = movimentacaoDAO.getValorTotalMovimentacoes(filtro);
		
		if(totalMovimentacao != null && totalMovimentacao.getValue().doubleValue() > 0){
			Double qtde = vendaService.getValorProducao(filtro);
			if(qtde != null && qtde > 0){
				return new Money(totalMovimentacao.getValue().doubleValue() / qtde);
			}
		}
		
		return new Money();
	}
	
	/**
	 * M�todo que faz a c�pia da movimenta��o
	 *
	 * @param movimentacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/07/2015
	 */
	public Movimentacao criaCopia(Movimentacao movimentacao) {
		Movimentacao movimentacao_nova = new Movimentacao();
		movimentacao_nova.setCheque(movimentacao.getCheque());
		movimentacao_nova.setConta(movimentacao.getConta());
		movimentacao_nova.setEmpresa(movimentacao.getEmpresa());
		movimentacao_nova.setValor(movimentacao.getValor());
		movimentacao_nova.setDtmovimentacao(movimentacao.getDtmovimentacao());
		movimentacao_nova.setMovimentacaoacao(movimentacao.getMovimentacaoacao());
		movimentacao_nova.setFormapagamento(movimentacao.getFormapagamento());
		movimentacao_nova.setHistorico(movimentacao.getHistorico());
		movimentacao_nova.setDtbanco(movimentacao.getDtbanco());
		movimentacao_nova.setChecknum(movimentacao.getChecknum());
		movimentacao_nova.setFitid(movimentacao.getFitid());
		movimentacao_nova.setOperacaocontabil(movimentacao.getOperacaocontabil());
		movimentacao_nova.setMovimentacaoAnterior(movimentacao);
		
		Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
		List<Movimentacaoorigem> listaMovimentacaoorigem = new ArrayList<Movimentacaoorigem>();
		listaMovimentacaoorigem.add(movimentacaoorigem);
		movimentacao_nova.setListaMovimentacaoorigem(listaMovimentacaoorigem);
		
		// LIMPA AS REFER�NCIAS DO RATEIO
		Rateio rateio = movimentacao.getRateio();
		rateioService.limpaReferenciaRateio(rateio);
		movimentacao_nova.setRateio(rateio);
		
		return movimentacao_nova;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findOrigemDevolucao(Movimentacao movimentacao)
	*
	* @param movimentacao
	* @return
	* @since 13/10/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findOrigemDevolucao(Movimentacao movimentacao, Boolean desconsiderarOrigemDocumento) {
		return movimentacaoDAO.findOrigemDevolucao(movimentacao, desconsiderarOrigemDocumento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MovimentacaoService#findMovimentacaoVinculoDevolucao(Movimentacao movimentacao)
	*
	* @param movimentacao
	* @return
	* @since 13/10/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findMovimentacaoVinculoDevolucao(Movimentacao movimentacao) {
		return movimentacaoDAO.findMovimentacaoVinculoDevolucao(movimentacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#findAdiantamentoByDespesaviagem(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 16/02/2016
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findAdiantamentoByDespesaviagem(String whereIn) {
		return movimentacaoDAO.findAdiantamentoByDespesaviagem(whereIn);
	}
	
	/**
	* M�todo que atualiza o valor do rateio da movimenta��o de acordo com os percentuais
	*
	* @param movimentacao
	* @param valor
	* @since 31/10/2016
	* @author Luiz Fernando
	*/
	public void atualizaValorMovimentacaoERateio(Movimentacao movimentacao, Money valor) {
		if(movimentacao != null && movimentacao.getCdmovimentacao() != null && valor != null){
			updateValorMovimentacao(movimentacao, valor);
			Rateio rateio = movimentacao.getRateio();
			if(rateio != null && SinedUtil.isListNotEmpty(rateio.getListaRateioitem())){
				rateioService.atualizaValorRateio(rateio, valor);
				
				Double totalRateioItem = 0d;
				for(Rateioitem rateioitem : rateio.getListaRateioitem()){
					totalRateioItem += SinedUtil.round(rateioitem.getValor().getValue().doubleValue(), 10);
				}
				
				Double diferenca = valor.subtract(new Money(SinedUtil.round(totalRateioItem, 2))).getValue().doubleValue();
				if(diferenca != 0){
					Rateioitem rateioitem = rateio.getListaRateioitem().get(0);
					rateioitem.setValor(rateioitem.getValor().add(new Money(diferenca)));
					double percentualAux = rateioitem.getValor().getValue().doubleValue()/valor.getValue().doubleValue() * 100d;
					percentualAux = SinedUtil.round(percentualAux, 10);
					rateioitem.setPercentual(percentualAux);
				}
				
				for(Rateioitem rateioitem : rateio.getListaRateioitem()){
					rateioitemService.updateValorRateioItem(rateioitem);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#updateValorMovimentacao(Movimentacao movimentacao, Money valor)
	*
	* @param movimentacao
	* @param valor
	* @since 31/10/2016
	* @author Luiz Fernando
	*/
	public void updateValorMovimentacao(Movimentacao movimentacao, Money valor) {
		movimentacaoDAO.updateValorMovimentacao(movimentacao, valor);
	}
	
	public void setAttributeBaixaVinculoDocumento(WebRequestContext request, List<Documento> listaDocumento){
		if(parametrogeralService.getBoolean(Parametrogeral.VALIDAR_VINCULOPROVISIONADO_CONTA)){
			request.setAttribute("VALIDAR_VINCULOPROVISIONADO_CONTA", "true");
			request.setAttribute("whereInVinculoDocumento", SinedUtil.isListNotEmpty(listaDocumento) ? SinedUtil.listAndConcatenate(listaDocumento, "vinculoProvisionado.cdconta", ",") : "");
		}else {
			request.setAttribute("VALIDAR_VINCULOPROVISIONADO_CONTA", "false");
		}
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilTransferencia(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return movimentacaoDAO.findForGerarLancamentoContabilTransferencia(filtro, movimentacaocontabilTipoLancamento); 
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilRetornoBancario(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return movimentacaoDAO.findForGerarLancamentoContabilRetornoBancario(filtro, movimentacaocontabilTipoLancamento);
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Operacaocontabil operacaoContabil, String whereNotIn, String whereNotInRateioitem, boolean considerarRateio){
		return movimentacaoDAO.findForGerarLancamentoContabilContaGerencial(filtro, operacaoContabil, whereNotIn, whereNotInRateioitem, considerarRateio);
	}
	
	public List<Movimentacao> findForContaListagem(String whereInCddocumento){
		if (whereInCddocumento==null || whereInCddocumento.trim().equals("")){
			return new ArrayList<Movimentacao>();
		}else {
			return movimentacaoDAO.findForContaListagem(whereInCddocumento);
		}
	}
	
	public List<Movimentacao> findForGerarArquivoRemessaConfiguravelCheque(GerarArquivoRemessaConfiguravelChequeFiltro filtro){
		return movimentacaoDAO.findForGerarArquivoRemessaConfiguravelCheque(filtro);
	}
	
	public List<MovimentacaoVO> getListaMovimentacaoForRemessaConfiguravel(List<Movimentacao> listaMovimentacao) {
		List<MovimentacaoVO> listaMovimentacaoVO = new ArrayList<MovimentacaoVO>();
		
		if (listaMovimentacao!=null){
			for (Movimentacao movimentacao: listaMovimentacao){
				Cheque cheque = movimentacao.getCheque();
				
				MovimentacaoVO movimentacaoVO = new MovimentacaoVO();
				movimentacaoVO.setDtmovimentacao(movimentacao.getDtmovimentacao());
				movimentacaoVO.setValor(movimentacao.getValor());
				movimentacaoVO.setBordero(movimentacao.getBordero());
				
				if (cheque!=null){
					movimentacaoVO.setChequelinhanumerica(cheque.getLinhanumerica());
					movimentacaoVO.setChequevalor(cheque.getValor());
					movimentacaoVO.setChequenumero(cheque.getNumero());
					movimentacaoVO.setChequecpfcnpj(cheque.getCpfcnpj());
					movimentacaoVO.setChequeemitente(cheque.getEmitente());
					movimentacaoVO.setChequebompara(cheque.getDtbompara());
					movimentacaoVO.setChequebanco(cheque.getBanco());
					movimentacaoVO.setChequeagencia(cheque.getAgencia());
					movimentacaoVO.setChequeconta(cheque.getConta());
				}
				
				listaMovimentacaoVO.add(movimentacaoVO);
			}
		}
		
		return listaMovimentacaoVO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoDAO#existeMovimentacaoEmAbertoByCheque(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 20/03/2017
	* @author Luiz Fernando
	*/
	public boolean existeMovimentacaoEmAbertoByCheque(String whereIn) {
		return movimentacaoDAO.existeMovimentacaoEmAbertoByCheque(whereIn);
	}
	
	public List<Movimentacao> findMovimenacaoTaxa(Movimentacao movimentacao) {
		return movimentacaoDAO.findMovimenacaoTaxa(movimentacao);
	}
	
	public Money getTotalTaxaByMovimentacao(Movimentacao movimentacao){
		Money taxa = new Money();
		if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
			List<Movimentacao> listaMovimentacaoTaxa = movimentacaoService.findMovimenacaoTaxa(movimentacao);
			if(SinedUtil.isListNotEmpty(listaMovimentacaoTaxa)){
				for(Movimentacao movTaxa : listaMovimentacaoTaxa){
					if(movTaxa.getMovimentacaoacao() != null && !Movimentacaoacao.CANCELADA.equals(movTaxa.getMovimentacaoacao()) && movTaxa.getValor() != null &&
						!Boolean.TRUE.equals(movTaxa.getTaxageradaprocessamentocartao())){
						taxa = taxa.add(movTaxa.getValor());
					}
				}
			}
		}
		return taxa;
	}
	
	public boolean verificaSituacao(List<Movimentacao> listaMovimentacao, Movimentacaoacao... movimentacaoacoes){
		for (Movimentacao movimentacao: listaMovimentacao){
			for (Movimentacaoacao movimentacaoacao: movimentacaoacoes){
				boolean achou = false;
				if (movimentacao.getMovimentacaoacao().equals(movimentacaoacao)){
					achou = true;
				}
				
				if (!achou){
					return false;
				}
			}
		}
		
		return true;
	}
	
	public boolean isTransferencia(Movimentacao movimentacao){
		if (movimentacao!=null && movimentacao.getCdmovimentacao()!=null){
			return movimentacaoDAO.isTransferencia(movimentacao);
		}else {
			return false;
		}
	}
	
	public List<Movimentacao> findMovimentacaoRelacionadaTransferencia(Movimentacao movimentacao, boolean todas, boolean cheque){
		if (movimentacao!=null && movimentacao.getCdmovimentacao()!=null){
			return movimentacaoDAO.findMovimentacaoRelacionadaTransferencia(movimentacao, todas, cheque);
		}else {
			return new ArrayList<Movimentacao>();
		}
	}
	
	public void updateDtmovimentacao(Movimentacao movimentacao, Date dtmovimentacao) {
		movimentacaoDAO.updateDtmovimentacao(movimentacao, dtmovimentacao);
	}
	
	public void updateHistorico(Movimentacao movimentacao, String historico) {
		movimentacaoDAO.updateHistorico(movimentacao, historico);
	}
	
	public List<Movimentacao> findByMovimentacaorelacionada(Movimentacao movimentacao) {
		return movimentacaoDAO.findByMovimentacaorelacionada(movimentacao);
	}
	public boolean existeMovimentacaotaxaGeradaNoProcessamentoExtratocartao(Movimentacao movimentacao){
		return movimentacaoDAO.existeMovimentacaotaxaGeradaNoProcessamentoExtratocartao(movimentacao);
	}
	
	public Movimentacao loadForCriaMovimentacaoTaxaProcessamentoextratocartao(Movimentacao movimentacao){
		return movimentacaoDAO.loadForCriaMovimentacaoTaxaProcessamentoextratocartao(movimentacao);
	}
	public void criarAvisoMovimentacao(Motivoaviso m, Date data, Date dateToSearch) {
		List<Movimentacao> movimentacaoList = movimentacaoDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Movimentacao o : movimentacaoList) {
			Aviso aviso = new Aviso("Concilia��o financeira da conta banc�ria n�o realizada", "C�digo do documento: " + o.getCdmovimentacao(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), o.getCdmovimentacao(), o.getEmpresa() != null ? o.getEmpresa() : empresa, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, o.getCdmovimentacao());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, o.getCdmovimentacao(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public List<Movimentacao> findNotNormal(String whereIn){
		return movimentacaoDAO.findNotNormal(whereIn);
	}
	
	public boolean saldoValeCompraNegativoAposCancelamento(String whereInMovimentacao) {
		List<Documento> lista = documentoService.findDocumentoByMovimentacao(whereInMovimentacao, Documentoclasse.OBJ_RECEBER, Tipopagamento.CLIENTE);
		if(SinedUtil.isListNotEmpty(lista)){
			HashMap<Integer, List<Valecompraorigem>> mapPessoaValecompraOrigem = new HashMap<Integer, List<Valecompraorigem>>();
			for(Documento d : lista){
				if(d.getPessoa() != null){
					List<Valecompraorigem> listaValecompraExcluir = valecompraorigemService.findByDocumento(d, Tipooperacao.TIPO_CREDITO);
					if(SinedUtil.isListNotEmpty(listaValecompraExcluir)){
						if(mapPessoaValecompraOrigem.get(d.getPessoa().getCdpessoa()) == null){
							mapPessoaValecompraOrigem.put(d.getPessoa().getCdpessoa(), listaValecompraExcluir);
						}else {
							mapPessoaValecompraOrigem.get(d.getPessoa().getCdpessoa()).addAll(listaValecompraExcluir);
						}
					}
				}
			}
			
			for(Documento d : lista){
				if(d.getPessoa() != null && SinedUtil.isListNotEmpty(mapPessoaValecompraOrigem.get(d.getPessoa().getCdpessoa()))){	
					Money saldoValecompra = valecompraService.getSaldoByCliente(new Cliente(d.getPessoa().getCdpessoa()), CollectionsUtil.listAndConcatenate(mapPessoaValecompraOrigem.get(d.getPessoa().getCdpessoa()), "valecompra.cdvalecompra", ","));
					if(saldoValecompra != null && saldoValecompra.getValue().doubleValue() < 0){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public List<Movimentacao> verificaTipoDocumentoAntecipacaoContaVinculadaBaixada(String ids) {
		return movimentacaoDAO.verificaTipoDocumentoAntecipacaoContaVinculadaBaixada(ids);
	}
	public List<Movimentacao> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto, Date dtInicio,Date dtFim, Empresa empresa) {
		return movimentacaoDAO.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
	}
	public void updateCdRateio(Integer cdMov, Rateio cdrateio) {
		 movimentacaoDAO.updateCdRateio(cdMov,cdrateio);
	}
	public List<Movimentacao> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return movimentacaoDAO.loadForLcdpr(filtro);
	}
	
	public Long countQtdeMovimentacao(Documento documento) {
		return movimentacaoDAO.countQtdeMovimentacao(documento);
	}
	
	public Long countOutrosDocumentosVinculados(String whereInMovimentacao, Documento documento) {
		return movimentacaoDAO.countOutrosDocumentosVinculados(whereInMovimentacao, documento);
	}
	
	public boolean isMovimentacaoVinculadaAMaisDeUmDocumento(Documento documento){
		List<Movimentacao> lista = this.findByDocumentoVariasMovimentacoes(documento);
		boolean exists = false;
		if(SinedUtil.isListNotEmpty(lista)){
			String whereInMovimentacao = CollectionsUtil.listAndConcatenate(lista, "cdmovimentacao", ",");
			return this.countOutrosDocumentosVinculados(whereInMovimentacao, documento) > 0;
		}
		return exists;
	}
	
	public Money buscarGastoEmpresa(RelatorioMargemContribuicaoFiltro filtro) {
		return movimentacaoDAO.buscarGastoEmpresa(filtro);
	}
}