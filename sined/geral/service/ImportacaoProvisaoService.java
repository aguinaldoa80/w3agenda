package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisao;
import br.com.linkcom.sined.geral.dao.ImportacaoProvisaoDAO;

public class ImportacaoProvisaoService extends GenericService<ImportacaoProvisao>{

	private ImportacaoProvisaoDAO importacaoProvisaoDAO;
	
	public void setImportacaoProvisaoDAO(
			ImportacaoProvisaoDAO importacaoProvisaoDAO) {
		this.importacaoProvisaoDAO = importacaoProvisaoDAO;
	}
}
