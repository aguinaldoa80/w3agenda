package br.com.linkcom.sined.geral.service;

import java.util.Arrays;
import java.util.List;

import javax.script.ScriptEngine;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoControleEnum;
import br.com.linkcom.sined.geral.dao.OperacaocontabilDAO;
import br.com.linkcom.sined.geral.dao.OperacaocontabildebitocreditoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OperacaocontabilService extends GenericService<Operacaocontabil>{
	
	public static final String VARIAVEL_HISTORICO_NUMDOCUMENTO = "$NUMDOCUMENTO";
	public static final String VARIAVEL_HISTORICO_DESCRICAO = "$DESCRICAO";
	public static final String VARIAVEL_HISTORICO_PAGAMENTOA = "$PAGAMENTOA";
	public static final String VARIAVEL_HISTORICO_CLIENTE = "$CLIENTE";
	public static final String VARIAVEL_HISTORICO_FORNECEDOR = "$FORNECEDOR";
	public static final String VARIAVEL_HISTORICO_PARCELA = "$PARCELA";
	public static final String VARIAVEL_HISTORICO_CONTAORIGEM = "$CONTAORIGEM";
	public static final String VARIAVEL_HISTORICO_CONTADESTINO = "$CONTADESTINO";
	public static final String VARIAVEL_HISTORICO_DATA = "$DATA";
	public static final String VARIAVEL_HISTORICO_CHEQUE = "$CHEQUE";
	public static final String VARIAVEL_HISTORICO_VENDA = "$VENDA";
	public static final String VARIAVEL_HISTORICO_IDENTIFICADORVENDA = "$IDENTIFICADORVENDA";
	public static final String VARIAVEL_HISTORICO_CODIGO_INTEGRACAO = "$CODIGOINTEGRACAO";
	public static final String VARIAVEL_HISTORICO_COLABORADOR = "$COLABORADOR";
	public static final String VARIAVEL_HISTORICO_EVENTO = "$EVENTO";
	public static final String VARIAVEL_HISTORICO_NUMERO_NOTA = "$NUM_NOTA";
	public static final String VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO = "$VALOR_UTILIZADO_ADIANTAMENTO";
	
	private OperacaocontabilDAO operacaocontabilDAO;
	private OperacaocontabildebitocreditoDAO operacaocontabildebitocreditoDAO;
	
	public void setOperacaocontabilDAO(OperacaocontabilDAO operacaocontabilDAO) {
		this.operacaocontabilDAO = operacaocontabilDAO;
	}

	public void setOperacaocontabildebitocreditoDAO(
			OperacaocontabildebitocreditoDAO operacaocontabildebitocreditoDAO) {
		this.operacaocontabildebitocreditoDAO = operacaocontabildebitocreditoDAO;
	}
	
	public Operacaocontabil load(Object bean, String property){
		return operacaocontabilDAO.load(bean, property);
	}
	
	public List<Operacaocontabil> findForAutocomplete(String q){
		return operacaocontabilDAO.findForAutocomplete(q);
	}
	
	public List<Operacaocontabil> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro){
		return operacaocontabilDAO.findForGerarLancamentoContabil(filtro);
	}
	
	public boolean isPossuiTipoLancamento(Operacaocontabil operacaocontabil){
		return operacaocontabilDAO.isPossuiTipoLancamento(operacaocontabil);
	}

	public String getVariaveisFormula() {
		StringBuilder sb = new StringBuilder();
		for(OperacaocontabildebitocreditoControleEnum value: Arrays.asList(OperacaocontabildebitocreditoControleEnum.values())){
			if(!value.equals(OperacaocontabildebitocreditoControleEnum.VALOR_CALCULADO)){
				sb.append("&nbsp;&nbsp;&nbsp;").append(value.name()).append(" - (").append(value.getNome()).append(")<br>");
			}
		}
		return sb.toString();
	}
	
	public void setMapVariaveisFormula(ScriptEngine engine) {
		for(OperacaocontabildebitocreditoControleEnum value: Arrays.asList(OperacaocontabildebitocreditoControleEnum.values())){
			if(!value.equals(OperacaocontabildebitocreditoControleEnum.VALOR_CALCULADO)){
				engine.put(value.name(), 1);
			}
		}
	}
	
	public List<Operacaocontabil> findForCSV(FiltroListagem filtro) {
		List<Operacaocontabil> lista = operacaocontabilDAO.findForCSV(filtro);
		if(SinedUtil.isListNotEmpty(lista)){
			for (Operacaocontabil operacaocontabil: lista) {
				operacaocontabil.setListaDebito(operacaocontabildebitocreditoDAO.findByOperacaocontabil(operacaocontabil, "operacaocontabildebito"));
				operacaocontabil.setListaCredito(operacaocontabildebitocreditoDAO.findByOperacaocontabil(operacaocontabil, "operacaocontabilcredito"));
			}
		}
		return lista;
	}
}