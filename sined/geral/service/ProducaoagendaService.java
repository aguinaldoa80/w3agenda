package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendaitemadicional;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.dao.ProducaoagendaDAO;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.Materiaprima;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoSobraBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoSobraItemBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoagendaFiltro;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.AnaliseCustoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendaService extends GenericService<Producaoagenda> {

	private ProducaoagendaDAO producaoagendaDAO;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ProducaoordemService producaoordemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ProducaoordemhistoricoService producaoordemhistoricoService;
	private MaterialService materialService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private ProducaoetapaService producaoetapaService;
	private ProducaoagendamaterialmateriaprimaService producaoagendamaterialmateriaprimaService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private MaterialsimilarService materialsimilarService;
	private LotematerialService lotematerialService;
	private ClienteService clienteService;
	private PneuService pneuService;
	private UnidademedidaService unidademedidaService;
	private MaterialproducaoService materialproducaoService;
	private ProducaoetapaitemService producaoetapaitemService;
	private ParametrogeralService parametrogeralService;
	private MaterialunidademedidaService materialunidademedidaService; 
	
	public void setLotematerialService(LotematerialService lotematerialService) {
		this.lotematerialService = lotematerialService;
	}
	public void setProducaoordemhistoricoService(
			ProducaoordemhistoricoService producaoordemhistoricoService) {
		this.producaoordemhistoricoService = producaoordemhistoricoService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setProducaoordemService(
			ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setProducaoagendaDAO(ProducaoagendaDAO producaoagendaDAO) {
		this.producaoagendaDAO = producaoagendaDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	public void setProducaoetapaService(ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setProducaoagendamaterialmateriaprimaService(ProducaoagendamaterialmateriaprimaService producaoagendamaterialmateriaprimaService) {
		this.producaoagendamaterialmateriaprimaService = producaoagendamaterialmateriaprimaService;
	}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setMaterialsimilarService(MaterialsimilarService materialsimilarService) {
		this.materialsimilarService = materialsimilarService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setProducaoetapaitemService(ProducaoetapaitemService producaoetapaitemService) {
		this.producaoetapaitemService = producaoetapaitemService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	
	@Override
	public void saveOrUpdate(final Producaoagenda bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Set<Producaoagendamaterial> listaProducaoagendamaterial = bean.getListaProducaoagendamaterial();
				
				if(bean.getCdproducaoagenda() != null){
					StringBuilder whereInPAMDelete = new StringBuilder();
					if(SinedUtil.isListNotEmpty(listaProducaoagendamaterial)){
						for(Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial){	
							if(producaoagendamaterial.getCdproducaoagendamaterial() != null){
								whereInPAMDelete.append(producaoagendamaterial.getCdproducaoagendamaterial()).append(",");
							}
							if(Hibernate.isInitialized(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) && 
									SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
								for(Producaoagendamaterialmateriaprima pammp : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()){
									if(pammp.getMaterialproducao() != null && pammp.getMaterialproducao().getCdmaterialproducao() == null){
										pammp.setMaterialproducao(null);
									}
								}
							}
						}
					}
					if(whereInPAMDelete.length() > 0){
						List<Producaoagendamaterial> listaPAMDelete = producaoagendamaterialService.findForDelete(bean, whereInPAMDelete.substring(0, whereInPAMDelete.length()-1));
						if(SinedUtil.isListNotEmpty(listaPAMDelete)){
							for(Producaoagendamaterial producaoagendamaterial : listaPAMDelete){
								producaoagendamaterialService.delete(producaoagendamaterial);
							}
						}
					}
				}
				
				saveOrUpdateNoUseTransaction(bean);	
				
				if(SinedUtil.isListNotEmpty(listaProducaoagendamaterial)){
					for(Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial){	
						producaoagendamaterial.setProducaoagenda(bean);
						producaoagendamaterialService.saveOrUpdateNoUseTransaction(producaoagendamaterial);
					}
				}	
				return null;
			}
		});
	}
	
	/**
	* M�todo que carrega a lista de materia prima dos itens da agenda de produ��o
	*
	* @param producaoagenda
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public void carregarProducaoagendamaterialmateriaprima(Producaoagenda producaoagenda){
		if(producaoagenda != null && SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())){
			StringBuilder whereInProducaoagendamaterial = new StringBuilder();
			for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
				if(producaoagendamaterial.getCdproducaoagendamaterial() != null){
					if(!Hibernate.isInitialized(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
						producaoagendamaterial.setListaProducaoagendamaterialmateriaprima(new ArrayList<Producaoagendamaterialmateriaprima>());
					}
					whereInProducaoagendamaterial.append(producaoagendamaterial.getCdproducaoagendamaterial()).append(",");
				}
			}
			if(StringUtils.isNotEmpty(whereInProducaoagendamaterial.toString())){
				List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima = producaoagendamaterialmateriaprimaService.findByProducaoagendamaterial(whereInProducaoagendamaterial.substring(0, whereInProducaoagendamaterial.length()-1));
				if(SinedUtil.isListNotEmpty(listaProducaoagendamaterialmateriaprima)){
					for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
						if(producaoagendamaterial.getCdproducaoagendamaterial() != null){
							for(Producaoagendamaterialmateriaprima item : listaProducaoagendamaterialmateriaprima){
								if(item.getProducaoagendamaterial() != null &&
										item.getProducaoagendamaterial().getCdproducaoagendamaterial() != null &&
										producaoagendamaterial.getCdproducaoagendamaterial().equals(item.getProducaoagendamaterial().getCdproducaoagendamaterial())){
									if(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima() == null){
										producaoagendamaterial.setListaProducaoagendamaterialmateriaprima(new ArrayList<Producaoagendamaterialmateriaprima>());
									}
									producaoagendamaterial.getListaProducaoagendamaterialmateriaprima().add(item);
								}
								
								if(item.getMaterial() != null && item.getLoteestoque() != null){
									List<Lotematerial> listaLotematerial = lotematerialService.findAllByLoteestoqueMaterial(item.getLoteestoque(), item.getMaterial());
									if(listaLotematerial != null && listaLotematerial.size() > 0){
										Lotematerial lotematerial = listaLotematerial.get(0);
										item.getLoteestoque().setValidade(lotematerial.getValidade());
									}
								}
							}
						}
						
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForCsv(ProducaoagendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findForCsv(ProducaoagendaFiltro filtro) {
		return producaoagendaDAO.findForCsv(filtro);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findWithSituacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findWithSituacao(String whereIn) {
		return producaoagendaDAO.findWithSituacao(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForVerificacaoSolicitacaocompra(String whereIn)
	 *
	 * @param whereInProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findForVerificacaoSolicitacaocompra(String whereInProducaoagenda) {
		return producaoagendaDAO.findForVerificacaoSolicitacaocompra(whereInProducaoagenda);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForProducao(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public List<Producaoagenda> findForProducao(String whereIn) {
		return producaoagendaDAO.findForProducao(whereIn);
	}
	
	/**
	 * Cria o arquivo CSV da listagem de Producaoagenda.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public Resource makeCSVListagem(ProducaoagendaFiltro filtro) {
		StringBuilder rel = new StringBuilder();
		
		rel.append("\"ID\"").append(";");
		rel.append("\"CLIENTE\"").append(";");
		rel.append("\"CONTRATO\"").append(";");
		rel.append("\"TIPO DE CONTRATO\"").append(";");
		rel.append("\"PEDIDO DE VENDA\"").append(";");
		rel.append("\"DATA DE ENTREGA\"").append(";");
		rel.append("\"SITUA��O\"").append(";");
		rel.append("\n");
		
		List<Producaoagenda> listaProducaoagenda = this.findForCsv(filtro);
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			rel.append("\"" + producaoagenda.getCdproducaoagenda() + "\";");
			
			if(producaoagenda.getCliente() != null) 
				rel.append("\"" + producaoagenda.getCliente().getNome() + "\";");
			else
				rel.append("\"\";");
			
			if(producaoagenda.getContrato() != null) 
				rel.append("\"" + producaoagenda.getContrato().getDescricao() + "\";");
			else
				rel.append("\"\";");
			
			if(producaoagenda.getContrato() != null && producaoagenda.getContrato().getContratotipo() != null) 
				rel.append("\"" + producaoagenda.getContrato().getContratotipo().getNome() + "\";");
			else
				rel.append("\"\";");
			
			if(producaoagenda.getPedidovenda() != null) 
				rel.append("\"" + producaoagenda.getPedidovenda().getCdpedidovenda())
					.append(" - ")
					.append(SinedDateUtils.toString(producaoagenda.getPedidovenda().getDtpedidovenda()) + "\";");
			else
				rel.append("\"\";");
			
			rel.append("\"" + SinedDateUtils.toString(producaoagenda.getDtentrega(), "dd/MM/yyyy HH:mm") + "\";");
			rel.append("\"" + producaoagenda.getProducaoagendasituacao().getNome().toUpperCase() + "\";");
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","producaoagenda_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		return resource;
	}
	
	public Resource makeCSVListagemAnaliseCusto(List<AnaliseCustoBean> listaAnaliseCusto){
		StringBuilder rel = new StringBuilder();
		
		rel.append("GRUPO").append(";");
		rel.append("C�DIGO").append(";");
		rel.append("MATERIAL").append(";");
		rel.append("QTDE").append(";");
		rel.append("CUSTO MAT�RIA PRIMA").append(";");
		rel.append("CUSTO OPERACIONAL").append(";");
		rel.append("CUSTO COMERCIAL").append(";");
		rel.append("VALOR UNIT�RIO DE CUSTO").append(";");
		rel.append("VALOR UNIT�RIO DE VENDA").append(";");
		rel.append("CUSTO TOTAL").append(";");
		rel.append("TOTAL VENDIDO").append(";");
		rel.append("MARKUP").append(";");
		rel.append("\n");
		
		for(AnaliseCustoBean item : listaAnaliseCusto){
			rel.append(item.getMaterial().getMaterialgrupo().getNome());
			rel.append(";");
			rel.append(item.getMaterial().getIdentificacao());
			rel.append(";");
			rel.append(item.getNomeMaterial());
			rel.append(";");
			rel.append(new DecimalFormat("#,##0.00").format(item.getQtdeVendido()));
			rel.append(";");
			rel.append(item.getCustoMateriaPrima());
			rel.append(";");
			rel.append(new Money(item.getCustoOperacional()));
			rel.append(";");
			rel.append(new Money(item.getCustoComercial()));
			rel.append(";");
			rel.append(item.getValorUnitarioCusto());
			rel.append(";");
			rel.append(item.getValorUnitarioVenda());
			rel.append(";");
			rel.append(item.getCustoTotal());
			rel.append(";");
			rel.append(item.getTotalVendido());
			rel.append(";");
			rel.append(item.getMarkup());
			rel.append(";");
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","analisecusto_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		return resource;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#updateSituacao(Producaoagenda producaoagenda, Producaoagendasituacao producaoagendasituacao)
	 *
	 * @param producaoagenda
	 * @param producaoagendasituacao
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public void updateSituacao(Producaoagenda producaoagenda, Producaoagendasituacao producaoagendasituacao) {
		producaoagendaDAO.updateSituacao(producaoagenda, producaoagendasituacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public void updateSituacao(String whereIn, Producaoagendasituacao producaoagendasituacao) {
		producaoagendaDAO.updateSituacao(whereIn, producaoagendasituacao);
	}

	/**
	 * Cria os links para a Producaoagenda do hist�rico
	 *
	 * @param listaProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public String makeLinkHistoricoProducaoagenda(List<Producaoagenda> listaProducaoagenda) {
		StringBuilder sb = new StringBuilder();
		boolean haveRegistro = false;
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			if(haveRegistro){
				sb.append(", ");
			}
			haveRegistro = true;
			
			sb.append("<a href=\"javascript:visualizarProducaoagenda(")
				.append(producaoagenda.getCdproducaoagenda())
				.append(");\">")
				.append(producaoagenda.getCdproducaoagenda())
				.append("</a>");
		}
		
		return sb.toString();
	}

	/**
	 * Verifica e conclui de acordo com as ordens de produ��o relacionadas a ela.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public void verificaConclusaoProducaoagenda(String whereIn) {
		List<Producaoagenda> listaProducaoagendaConcluir = this.findForConclusao(whereIn);
		for (Producaoagenda producaoagenda : listaProducaoagendaConcluir) {
			boolean concluir = true;
			boolean existeItemConcluido = false;
			
			Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = producaoagenda.getListaProducaoordemmaterialorigem();
			if(listaProducaoordemmaterialorigem != null && listaProducaoordemmaterialorigem.size() > 0){
				for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
					if(producaoordemmaterialorigem.getProducaoordemmaterial() != null && 
							producaoordemmaterialorigem.getProducaoordemmaterial().getProducaoordem() != null &&
							producaoordemmaterialorigem.getProducaoordemmaterial().getProducaoordem().getProducaoordemsituacao() != null){
						if(!producaoordemmaterialorigem.getProducaoordemmaterial().getProducaoordem().getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA) &&
							!producaoordemmaterialorigem.getProducaoordemmaterial().getProducaoordem().getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA)){
							concluir = false;
							break;
						}else if(producaoordemmaterialorigem.getProducaoordemmaterial().getProducaoordem().getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA)){
							existeItemConcluido = true;
						}
					}
				}
			} else concluir = false;
			
			if(concluir && existeItemConcluido){
				this.updateSituacao(producaoagenda, Producaoagendasituacao.CONCLUIDA);
			}
		}
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForConclusao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	private List<Producaoagenda> findForConclusao(String whereIn) {
		return producaoagendaDAO.findForConclusao(whereIn);
	}

	/**
	 * Cancela os agendamentos de produ��o que est�o em aberto.
	 *
	 * @param contrato
	 * @author Rodrigo Freitas
	 * @since 07/03/2013
	 */
	public void cancelaEmEsperaByContrato(Contrato contrato) {
		List<Producaoagenda> listaProducaoagenda = this.findEmEsperaByContrato(contrato);
		
		if(listaProducaoagenda != null){
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setObservacao("Cancelamento do contrato.");
				producaoagendahistorico.setProducaoagenda(producaoagenda);
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
				
				this.updateSituacao(producaoagenda, Producaoagendasituacao.CANCELADA);
			}
		}
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#loadWithoutProducaoagendatipo(Producaoagenda producaoagenda)
	 *
	 * @param producaoagenda
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Producaoagenda loadWithoutProducaoagendatipo(Producaoagenda producaoagenda) {
		return producaoagendaDAO.loadWithoutProducaoagendatipo(producaoagenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findEmEsperaByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/03/2013
	 */
	public List<Producaoagenda> findEmEsperaByContrato(Contrato contrato) {
		return producaoagendaDAO.findEmEsperaByContrato(contrato);
	}
	
	/**
	 * M�todo que retorna a situa��o da agenda de produ��o do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public Producaoagendasituacao getProducaoagendasituacaoByPedidovenda(Pedidovenda pedidovenda) {
		Producaoagendasituacao producaoagendasituacao = null;
		
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			List<Producaoagenda> listaProducaoagenda = findByPedidovenda(pedidovenda);
			if(listaProducaoagenda != null && !listaProducaoagenda.isEmpty()){
				for(Producaoagenda producaoagenda : listaProducaoagenda){
					if(producaoagenda.getProducaoagendasituacao() != null){
						if(producaoagendasituacao == null){
							producaoagendasituacao = producaoagenda.getProducaoagendasituacao();
							if(Producaoagendasituacao.EM_ANDAMENTO.equals(producaoagenda.getProducaoagendasituacao()) || 
									Producaoagendasituacao.EM_ESPERA.equals(producaoagenda.getProducaoagendasituacao()) || 
									Producaoagendasituacao.CHAO_DE_FABRICA.equals(producaoagenda.getProducaoagendasituacao())){
								break;
							}
						}else {
							if(Producaoagendasituacao.EM_ANDAMENTO.equals(producaoagenda.getProducaoagendasituacao()) || 
									Producaoagendasituacao.EM_ESPERA.equals(producaoagenda.getProducaoagendasituacao()) || 
									Producaoagendasituacao.CHAO_DE_FABRICA.equals(producaoagenda.getProducaoagendasituacao())){
								producaoagendasituacao = producaoagenda.getProducaoagendasituacao();
								break;
							}else if(Producaoagendasituacao.CONCLUIDA.equals(producaoagenda.getProducaoagendasituacao())){
								producaoagendasituacao = producaoagenda.getProducaoagendasituacao();
							}
						}
					}
				}
			}
		}
		
		return producaoagendasituacao;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public List<Producaoagenda> findByPedidovenda(Pedidovenda pedidovenda) {
		return producaoagendaDAO.findByPedidovenda(pedidovenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Producaoagenda> findByPedidovenda(String whereIn) {
		return producaoagendaDAO.findByPedidovenda(whereIn);
	}
	
	/**
	 * M�todo que retorna a situa��o da agenda de produ��o da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public Producaoagendasituacao getProducaoagendasituacaoByVenda(Venda venda) {
		Producaoagendasituacao producaoagendasituacao = null;
		
		if(venda != null && venda.getCdvenda() != null){
			List<Producaoagenda> listaProducaoagenda = findByVenda(venda);
			if(listaProducaoagenda != null && !listaProducaoagenda.isEmpty()){
				for(Producaoagenda producaoagenda : listaProducaoagenda){
					if(producaoagenda.getProducaoagendasituacao() != null){
						if(producaoagendasituacao == null){
							producaoagendasituacao = producaoagenda.getProducaoagendasituacao();
						}else if(Producaoagendasituacao.CONCLUIDA.equals(producaoagenda.getProducaoagendasituacao())){
							producaoagendasituacao = producaoagenda.getProducaoagendasituacao();
						}
					}
				}
			}
		}
		
		return producaoagendasituacao;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findByVenda(Venda venda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public List<Producaoagenda> findByVenda(Venda venda) {
		return producaoagendaDAO.findByVenda(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ProducaoagendaService#existProducaoagendaByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 09/04/2014
	 */
	public boolean existProducaoagendaByPedidovenda(Pedidovenda pedidovenda){
		return producaoagendaDAO.existProducaoagendaByPedidovenda(pedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see  br.com.linkcom.sined.geral.service.ProducaoagendaService#findForProduzirByProducaoagenda(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 24/06/2014
	* @author Luiz Fernando
	 * @param whereInMaterialMestre 
	*/
	public List<Producaoagenda> findForProduzirByProducaoagenda(String whereIn, String whereInMaterialMestre) {
		return producaoagendaDAO.findForProduzirByProducaoagenda(whereIn, whereInMaterialMestre);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoagendaByPedidovendaNaoCancelada(String whereIn) {
		return producaoagendaDAO.haveProducaoagendaByPedidovendaNaoCancelada(whereIn);
	}
	
	/**
	 * M�todo que faz o cancelamento de agenda de produ��o
	 *
	 * @param whereInProducaoagenda
	 * @author Rodrigo Freitas
	 * @param request 
	 * @since 29/07/2014
	 */
	public void cancelar(WebRequestContext request, String whereInProducaoagenda, String observacao, boolean cancelarOrdemproducao) {
		String[] idsProducaoagenda = whereInProducaoagenda.split(",");
		this.updateSituacao(whereInProducaoagenda, Producaoagendasituacao.CANCELADA);
		
		for (int i = 0; i < idsProducaoagenda.length; i++) {
			Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
			producaoagendahistorico.setObservacao("Cancelamento" + (observacao != null ? ": " + observacao : ""));
			producaoagendahistorico.setProducaoagenda(new Producaoagenda(Integer.parseInt(idsProducaoagenda[i])));
			producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		}
		
		try{
			if(cancelarOrdemproducao){
				List<Producaoordem> listaProducaoordem = producaoordemService.findByProducaoagenda(whereInProducaoagenda);
				if(listaProducaoordem != null && listaProducaoordem.size() > 0){
					String whereInProducaoordem = CollectionsUtil.listAndConcatenate(listaProducaoordem, "cdproducaoordem", ",");
					producaoordemService.updateSituacao(whereInProducaoordem, Producaoordemsituacao.CANCELADA);
					producaoordemService.registrarHistoricoOrdemCanceladaPedidovenda(whereInProducaoordem);
					
					for (Producaoordem producaoordem : listaProducaoordem) {
						Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
						producaoordemhistorico.setObservacao("Cancelamento da agenda de produ��o" + (observacao != null ? ": " + observacao : ""));
						producaoordemhistorico.setProducaoordem(producaoordem);
						producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(request != null){
				request.addError("Problema no cancelamento de ordem de produ��o: " + e.getMessage());
			}
		}
		
		try{
			List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByProducaoagenda(whereInProducaoagenda);
			if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
				movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(request != null){
				request.addError("Problema no cancelamento de movimenta��o de estoque: " + e.getMessage());
			}
		}
		
		if(request != null){
			if(cancelarOrdemproducao){
				request.addMessage("Processo de cancelamento da(s) agenda(s) de produ��o finalizado com sucesso.");
			}else {
				request.addMessage("A(s) agenda(s) de produ��o (" + whereInProducaoagenda + ") foi(ram) cancelada(s) ap�s a devolu��o de todos os itens da ordem de produ��o.");
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoagendaSituacao(String whereIn, Producaoagendasituacao... producaoagendasituacao) {
		return producaoagendaDAO.haveProducaoagendaSituacao(whereIn, producaoagendasituacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/03/2016
	 */
	public boolean haveProducaoagendaNotInSituacao(String whereIn, Producaoagendasituacao... producaoagendasituacao) {
		return producaoagendaDAO.haveProducaoagendaNotInSituacao(whereIn, producaoagendasituacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#notProducaoagendaSituacao(String whereIn, Producaoagendasituacao producaoagendasituacao)
	*
	* @param whereIn
	* @param producaoagendasituacao
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public boolean notProducaoagendaSituacao(String whereIn, Producaoagendasituacao producaoagendasituacao) {
		return producaoagendaDAO.notProducaoagendaSituacao(whereIn, producaoagendasituacao);
	}
	
	/**
	* M�todo que cria bean com os dados de c�lculo de sobra
	*
	* @param whereIn
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public ProducaoSobraBean getProducaoSobra(String whereIn) {
		ProducaoSobraBean producaoSobraBean = new ProducaoSobraBean();
		if(StringUtils.isNotEmpty(whereIn)){
			List<Producaoagenda> listaProducaoagenda = findForCalcularSobra(whereIn);
			if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
				List<ProducaoSobraItemBean> itens = new ArrayList<ProducaoSobraItemBean>();
				for(Producaoagenda producaoagenda : listaProducaoagenda){
					itens.addAll(getItensProducaoSobra(producaoagenda));
				}
				producaoSobraBean.setListaProducaoSobraItemBean(itens);
			}
		}
		return producaoSobraBean;
	}
	
	/**
	* M�todo que cria os itens do bean com os dados de c�lculo de sobra
	*
	* @param producaoagenda
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public List<ProducaoSobraItemBean> getItensProducaoSobra(Producaoagenda producaoagenda) {
		List<ProducaoSobraItemBean> listaProducaoSobraItemBean = new ArrayList<ProducaoSobraItemBean>();
		if(producaoagenda != null && SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())){
			for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
				if(producaoagendamaterial.getMaterial() != null){
					boolean dimensoesProducao = producaoagendamaterial.getAltura() != null || 
												producaoagendamaterial.getLargura() != null ||
												producaoagendamaterial.getComprimento() != null;
					boolean corteProducao = producaoagendamaterial.getProducaoetapa() != null &&
											producaoagendamaterial.getProducaoetapa().getCorteproducao() != null ?
											producaoagendamaterial.getProducaoetapa().getCorteproducao() : false;
					Double larguraProducao = producaoagendamaterial.getLargura() != null ? producaoagendamaterial.getLargura() : 1d;
					Double alturaProducao = producaoagendamaterial.getAltura() != null ? producaoagendamaterial.getAltura() : 1d;
					Double comprimentoProducao = producaoagendamaterial.getComprimento() != null ? producaoagendamaterial.getComprimento() : 1d;
					Double qtdeProduzir = producaoagendamaterial.getQtde() != null ? producaoagendamaterial.getQtde() : 1d;
					Double b = (larguraProducao * alturaProducao * comprimentoProducao * qtdeProduzir);
					
					if(dimensoesProducao && corteProducao){
						Produto produto = producaoagendamaterial.getMaterial().getMaterialproduto();
						if(produto != null){
							Double largura = produto.getLargura() != null ? produto.getLargura()/1000d : 1d;
							Double altura = produto.getAltura() != null ? produto.getAltura()/1000d : 1d;
							Double comprimento = produto.getComprimento() != null ? produto.getComprimento()/1000d : 1d;
							
							Double a = (largura * altura * comprimento);
							Long c = SinedUtil.roundUp(new BigDecimal(b/a), 0).longValue();
							Double sobra = (c * a) - b;
							
							listaProducaoSobraItemBean.add(new ProducaoSobraItemBean(
									producaoagendamaterial.getMaterial(),
									producaoagendamaterial.getMaterial().getUnidademedida(),
									c,
									b/a,
									sobra,
									producaoagenda,
									producaoagendamaterial,
									producaoetapaService.getUltimaEtapa(producaoagendamaterial.getProducaoetapa())));
						}
					}else if(!corteProducao){
						if(SinedUtil.isListNotEmpty(producaoagendamaterial.getMaterial().getListaProducao())){
							for(Materialproducao materialproducao : producaoagendamaterial.getMaterial().getListaProducao()){
								boolean dimensoesMaterialprima = materialproducao.getAltura() != null || 
															materialproducao.getLargura() != null;
								if(dimensoesMaterialprima){
									Produto produto = materialproducao.getMaterial().getMaterialproduto();
									if(produto != null){
										Double largura = produto.getLargura() != null ? produto.getLargura()/1000 : 1d;
										Double altura = produto.getAltura() != null ? produto.getAltura()/1000 : 1d;
										Double comprimento = produto.getComprimento() != null ? produto.getComprimento()/1000 : 1d;
										
										Double a = (largura * altura * comprimento);
										Long c = SinedUtil.roundUp(new BigDecimal(b/a), 0).longValue();
										Double sobra = (c * a) - b;
										
										listaProducaoSobraItemBean.add(new ProducaoSobraItemBean(
																materialproducao.getMaterial(),
																materialproducao.getMaterial().getUnidademedida(),
																c,
																b/a,
																sobra,
																producaoagenda,
																producaoagendamaterial,
																producaoetapaService.getUltimaEtapa(producaoagendamaterial.getProducaoetapa())));
									}
								}
							}
						}
					}
				}
			}
		}
		return listaProducaoSobraItemBean;
	}
	
	/**
	* M�todo que retorn um whereIn dos materiais da agenda de produ��o
	*
	* @param listaProducaoagenda
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public String getWhereInMaterial(List<Producaoagenda> listaProducaoagenda) {
		StringBuilder whereIn = new StringBuilder();
		
		if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
			for(Producaoagenda producaoagenda : listaProducaoagenda){
				if(producaoagenda != null && SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())){
					for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
						if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getCdmaterial() != null){
							whereIn.append(producaoagendamaterial.getMaterial().getCdmaterial()).append(",");
						}
					}
				}
			}
		}
		return !whereIn.toString().equals("") ? whereIn.substring(0, whereIn.length()-1) : "";
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* Carrega os dados necess�rios para calcular a sobra
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForCalcularSobra(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForCalcularSobra(String whereIn) {
		List<Producaoagenda> listaProducaoagenda = producaoagendaDAO.findForCalcularSobra(whereIn);
		if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
			String whereInMaterial = getWhereInMaterial(listaProducaoagenda);
			if(StringUtils.isNotEmpty(whereInMaterial)){
				List<Material> listaMaterial = materialService.findMaterialComMaterialproducao(whereInMaterial);
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					for(Producaoagenda producaoagenda : listaProducaoagenda){
						if(SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())){
							for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
								if(producaoagendamaterial.getMaterial() != null){
									for(Material material : listaMaterial){
										if(material.equals(producaoagendamaterial.getMaterial())){
											producaoagendamaterial.setMaterial(material);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return listaProducaoagenda;
	}
	
	/**
	* M�todo que registra a quantidade de perda na �ltima ordem de produ��o
	*
	* @param producaoSobraBean
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public void registrarQuantidadePerdaSobraUltimaOrdemproducao(ProducaoSobraBean producaoSobraBean) {
		if(producaoSobraBean != null && SinedUtil.isListNotEmpty(producaoSobraBean.getListaProducaoSobraItemBean())){
			for(ProducaoSobraItemBean producaoSobraItemBean : producaoSobraBean.getListaProducaoSobraItemBean()){
				if(producaoSobraItemBean.getProducaoagenda() != null && producaoSobraItemBean.getProducaoagendamaterial() != null &&
						producaoSobraItemBean.getProducaoagendamaterial().getProducaoetapa() != null && 
						producaoSobraItemBean.getQtdesobra() != null && 
						producaoSobraItemBean.getQtdesobra() > 0){
					Producaoordemmaterial producaoordemmaterial = producaoordemmaterialService.buscarUltimaOrdemproducao(producaoSobraItemBean.getProducaoagenda(), 
																		producaoSobraItemBean.getProducaoetapaitem());
					if(producaoordemmaterial != null && producaoordemmaterial.getCdproducaoordemmaterial() != null){
						producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, producaoSobraItemBean.getQtdesobra());
					}
				}
			}
		}
	}
	
	/**
	* M�todo que registra a entrada no estoque referente a perda
	*
	* @param producaoSobraBean
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public void registrarEntradaPerdaSobraUltimaOrdemproducao(ProducaoSobraBean producaoSobraBean) {
		if(producaoSobraBean != null && producaoSobraBean.getLocalsobra() != null && 
				SinedUtil.isListNotEmpty(producaoSobraBean.getListaProducaoSobraItemBean())){
			HashMap<Producaoagenda, StringBuilder> mapProducaoagendaHistorico = new HashMap<Producaoagenda, StringBuilder>();
			for(ProducaoSobraItemBean producaoSobraItemBean : producaoSobraBean.getListaProducaoSobraItemBean()){
				if(producaoSobraItemBean.getMaterial() != null && producaoSobraItemBean.getQtdesobra() != null && 
						producaoSobraItemBean.getQtdesobra() > 0){
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
					
					Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
					movimentacaoestoque.setMaterial(producaoSobraItemBean.getMaterial());
					movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
					movimentacaoestoque.setLocalarmazenagem(producaoSobraBean.getLocalsobra());
					movimentacaoestoque.setQtde(producaoSobraItemBean.getQtdesobra());
					movimentacaoestoque.setDtmovimentacao(new Date(System.currentTimeMillis()));
					movimentacaoestoque.setObservacao("Sobra apurada em Produ��o");
					
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
					
					StringBuilder historicoPA = new StringBuilder("<a href=\"javascript:visualizaMovimentacaoestoque("+
										movimentacaoestoque.getCdmovimentacaoestoque()+");\">"+
										movimentacaoestoque.getCdmovimentacaoestoque()+"</a>"); 
					if(mapProducaoagendaHistorico.get(producaoSobraItemBean.getProducaoagenda()) == null){
						mapProducaoagendaHistorico.put(producaoSobraItemBean.getProducaoagenda(), historicoPA);
					}else {
						mapProducaoagendaHistorico.get(producaoSobraItemBean.getProducaoagenda()).append(",").append(historicoPA);
					}
				}
			}
			
			if(mapProducaoagendaHistorico.size() > 0){
				for(Producaoagenda producaoagenda : mapProducaoagendaHistorico.keySet()){
					Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
					producaoagendahistorico.setProducaoagenda(producaoagenda);
					producaoagendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					producaoagendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					producaoagendahistorico.setObservacao("Entrada referente a perda: " + mapProducaoagendaHistorico.get(producaoagenda));
					producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
				}
			}
		}
	}
	/**
	* M�todo que cria a lista de materia prima a partir de uma lista de produ��o
	*
	* @param listaProducao
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public List<Materiaprima> createListaMateriaprima(Set<Materialproducao> listaProducao, Pneu pneu, Producaoetapanome producaoetapanome, boolean verificarUnidadePrioritariaProducao) {
		List<Materiaprima> lista = new ArrayList<Materiaprima>();
		
		if(SinedUtil.isListNotEmpty(listaProducao)){
			for (Materialproducao materialproducao : listaProducao) {
				if(producaoetapanome == null || (producaoetapanome.equals(materialproducao.getProducaoetapanome()))){
					if(SinedUtil.isRecapagem() &&
							pneu != null && 
							pneu.getMaterialbanda() != null &&
							materialproducao.getExibirvenda() != null &&
							materialproducao.getExibirvenda()){
						materialproducao.setMaterial(pneu.getMaterialbanda());
						Unidademedida unidademedida = pneu.getMaterialbanda().getUnidademedida();
						if(unidademedida == null){
							Material materialbanda = materialService.load(pneu.getMaterialbanda(), "material.cdmaterial, material.unidademedida");
							if(materialbanda != null && materialbanda.getUnidademedida() != null){
								unidademedida = materialbanda.getUnidademedida();
								if(!Hibernate.isInitialized(unidademedida.getListaUnidademedidaconversao())){
									unidademedida.setListaUnidademedidaconversao(new ArrayList<Unidademedidaconversao>());
								}
							}
						}
						materialproducao.setUnidademedida(unidademedida);
					}
					lista.add(new Materiaprima(materialproducao));
				}
			}
		}
		
		if(verificarUnidadePrioritariaProducao && SinedUtil.isListNotEmpty(lista)){
			for(Materiaprima materiaprima : lista){
				verificaUnidadePrioritariaProducao(materiaprima);
			}
		}
		
		return lista;
	}
	
	private void verificaUnidadePrioritariaProducao(Materiaprima materiaprima) {
		if(materiaprima.getMaterial() != null && materiaprima.getUnidademedida() != null){
			Material material = materialService.findListaMaterialunidademedida(materiaprima.getMaterial());
			Materialunidademedida materialunidademedida = materialunidademedidaService.getMaterialunidademedidaPrioritariaProducao(material);
			
			if(materialunidademedida != null && materialunidademedida.getUnidademedida() != null && !materiaprima.getUnidademedida().equals(materialunidademedida.getUnidademedida())){
				Double fracao1 = unidademedidaService.getFracao(material, materiaprima.getUnidademedida(), materialunidademedida.getUnidademedida());
				Double fracao2 = unidademedidaService.getFracao(material, materialunidademedida.getUnidademedida(), materiaprima.getUnidademedida());		
				
				if(fracao1 != null && fracao2 != null){
					if(materiaprima.getQtdeprevista() != null && materiaprima.getQtdeprevista() > 0){
						materiaprima.setQtdeprevista(SinedUtil.round((materiaprima.getQtdeprevista() * fracao1) / fracao2, 10));
					}
					materiaprima.setFracaounidademedida(material.getFracao());
					materiaprima.setQtdereferenciaunidademedida(material.getQtdereferencia());
					materiaprima.setUnidademedida(materialunidademedida.getUnidademedida());
					materiaprima.setUnidademedidaTrans(materialunidademedida.getUnidademedida());
				}
			}
		}
	}
	/**
	* M�todo que verifica se existe material similar
	*
	* @param lista
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public void verificaTrocaMaterialsimilar(List<Materiaprima> lista) {
		if(lista != null && !lista.isEmpty()){
			for(Materiaprima mp : lista){
				mp.setUnidademedidaAntiga(mp.getUnidademedida());
				if(mp.getMaterial() != null && mp.getMaterial().getCdmaterial() != null){
					mp.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(mp.getMaterial()));
				}
			}
		}		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForMovimentacaoestoque(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 02/04/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForMovimentacaoestoque(String whereIn) {
		return producaoagendaDAO.findForMovimentacaoestoque(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findWithProducaoetapaAndSituacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 14/05/2015
	* @author Jo�o Vitor
	*/
	public List<Producaoagenda> findWithProducaoetapaAndSituacao(String whereIn) {
		return producaoagendaDAO.findWithProducaoetapaAndSituacao(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findForAnaliseCusto(empresa, dtinicio, dtfim)
	 * 
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @author Danilo Guimar�es
	 * @since 22/05/2015
	 */
	public List<Producaoagenda> findForAnaliseCusto(Empresa empresa, Date dtinicio, Date dtfim){
		return producaoagendaDAO.findForAnaliseCusto(empresa, dtinicio, dtfim);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findProducaoagendamateriaprimaByProducaoagenda(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 14/05/2015
	* @author Jo�o Vitor
	*/
	public List<Producaoagenda> findProducaoagendamateriaprimaByProducaoagenda(String whereIn) {
		return producaoagendaDAO.findProducaoagendamateriaprimaByProducaoagenda(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/07/2015
	 */
	public List<Producaoagenda> findByPneu(Pneu pneu) {
		return producaoagendaDAO.findByPneu(pneu, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoagendaService#findByProducaoordem(String whereInProducaoordem)
	*
	* @param whereInProducaoordem
	* @return
	* @since 16/10/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findByProducaoordem(String whereInProducaoordem) {
		return producaoagendaDAO.findByProducaoordem(whereInProducaoordem);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#isMateriaprima(Material material, Producaoagenda producaoagenda)
	*
	* @param material
	* @param producaoagenda
	* @return
	* @since 02/03/2016
	* @author Luiz Fernando
	*/
	public boolean isMateriaprima(Material material, Producaoagenda producaoagenda) {
		return producaoagendaDAO.isMateriaprima(material, producaoagenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendaDAO#findProducaoagendaForEstornoProducaoordem(String whereInProducaoordem)
	*
	* @param whereInProducaoordem
	* @return
	* @since 23/12/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findProducaoagendaForEstornoProducaoordem(String whereInProducaoordem) {
		return producaoagendaDAO.findProducaoagendaForEstornoProducaoordem(whereInProducaoordem);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/03/2016
	 */
	public List<Producaoagenda> findForEnvioChaofabrica(String whereIn) {
		return producaoagendaDAO.findForEnvioChaofabrica(whereIn);
	}
	
	
	
	/**
	* M�todo que converte a unidade de medida na produ��o
	*
	* @param request
	* @param materiaprima
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Materiaprima materiaprima){
		Double qtdeprevista = materiaprima.getQtdeprevista();
		Unidademedida unidademedida = materiaprima.getUnidademedida();
		Unidademedida unidademedidaAntiga = materiaprima.getUnidademedidaAntiga();
		
		Double fatorconversao = null;
		Double qtdereferencia = 1d;
		if(unidademedida != null && unidademedidaAntiga != null && qtdeprevista != null){
			Material material = materialService.findListaMaterialunidademedida(materiaprima.getMaterial());
			
			if(!material.getUnidademedida().equals(unidademedida) && !material.getUnidademedida().equals(unidademedidaAntiga)){
				Double fracao1 = unidademedidaService.getFracao(material, unidademedidaAntiga, material.getUnidademedida());
				Double fracao2 = unidademedidaService.getFracao(material, material.getUnidademedida(), unidademedidaAntiga);
				
				if(fracao1 != null && fracao2 != null){
					qtdeprevista = SinedUtil.round((qtdeprevista * fracao1) / fracao2, 10);
				}
				unidademedidaAntiga = material.getUnidademedida();
			}
			
			Double fracao1 = unidademedidaService.getFracao(material, unidademedidaAntiga, unidademedida);
			Double fracao2 = unidademedidaService.getFracao(material, unidademedida, unidademedidaAntiga);		
			
			if(fracao1 != null && fracao2 != null){
				qtdeprevista = SinedUtil.round((qtdeprevista * fracao1) / fracao2, 10);
				fatorconversao = material.getFracao();
				qtdereferencia = material.getQtdereferencia();
			}
		}
		
		return new JsonModelAndView()
				.addObject("qtdeprevista", qtdeprevista != null ? SinedUtil.descriptionDecimal(qtdeprevista) : "")
				.addObject("fracao", fatorconversao != null ? SinedUtil.descriptionDecimal(fatorconversao) : "")
				.addObject("qtdereferencia", qtdereferencia != null ? SinedUtil.descriptionDecimal(qtdereferencia) : "");
	}
	
	/**
	* M�todo ajax que carrega as unidades de medida do material
	*
	* @param request
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxPreencheComboUnidadeMedida(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Unidademedida> lista = null;
		String cdmaterial = request.getParameter("cdmaterial");
		
		Unidademedida unidademedida = new Unidademedida();
		
		if (StringUtils.isNotBlank(cdmaterial)) {
			unidademedida = materialService.unidadeMedidaMaterial(new Material(Integer.parseInt(cdmaterial))).getUnidademedida();
			lista = unidademedidaService.getUnidademedidaByMaterial(new Material(Integer.parseInt(cdmaterial)));
		}
		
		jsonModelAndView.addObject("unidademedida", unidademedida);
		jsonModelAndView.addObject("lista", lista);
		
		return jsonModelAndView; 
	}
	
	/**
	* M�todo ajax para recalcular as materias-primas
	*
	* @param request
	* @param bean
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Material bean){
		Material material = materialService.loadMaterialComMaterialproducao(bean);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
		if(SinedUtil.isListNotEmpty(bean.getListaMateriaprima())){
			Set<Materialproducao> listaProducao = new ListSet<Materialproducao>(Materialproducao.class);
			for(Materiaprima mp : bean.getListaMateriaprima()){
				if(mp.getMaterial() != null){
					Material materialBean = materialService.load(mp.getMaterial(), "material.cdmaterial, material.identificacao, material.nome");
					mp.getMaterial().setNome(new br.com.linkcom.neo.util.StringUtils().escapeQuotes(materialBean.getNome()));
					mp.getMaterial().setIdentificacao(materialBean.getIdentificacao());
					boolean achou = false;
					boolean considerarMaterialProducaoComFormula = false;
					Materialproducao materialproducaoComFormula = null;
					if((mp.getNaocalcularqtdeprevista() == null || !mp.getNaocalcularqtdeprevista()) && 
						SinedUtil.isListNotEmpty(material.getListaProducao())){
						if(mp.getMaterialproducao() != null && mp.getMaterialproducao().getCdmaterialproducao() != null){
							for(Materialproducao materialproducao : material.getListaProducao()){
								if(materialproducao.getCdmaterialproducao() != null && materialproducao.getCdmaterialproducao().equals(mp.getMaterialproducao().getCdmaterialproducao())){
									achou = true;
									considerarMaterialProducaoComFormula = true;
									materialproducaoComFormula = materialproducao;
									listaProducao.add(materialproducao);
									break;
								}
							}
						}
						if(!achou){
							for(Materialproducao materialproducao : material.getListaProducao()){
								if(materialproducao.getMaterial() != null && materialproducao.getMaterial().equals(mp.getMaterial())){
									achou = true;
									listaProducao.add(materialproducao);
									break;
								}
							}
						}
						if(!achou && bean.getProduto_qtde() != null && bean.getProduto_qtde() > 0){
							mp.setQtdeprevista(mp.getQtdeprevista() / bean.getProduto_qtde());
						}
					} 
					if(!achou){
						Materialproducao materialproducao = new Materialproducao();
						if(considerarMaterialProducaoComFormula){
							materialproducao = materialproducaoComFormula;
						}
						materialproducao.setMaterial(mp.getMaterial());
						materialproducao.setConsumo(mp.getQtdeprevista());
						materialproducao.setNaocalcularqtdeprevista(mp.getNaocalcularqtdeprevista());
						materialproducao.setLoteestoque(mp.getLoteestoque());
						materialproducao.setAgitacao(mp.getAgitacao());
						materialproducao.setQuantidadepercentual(mp.getQuantidadepercentual());
						materialproducao.setUnidademedida(mp.getUnidademedida());
						materialproducao.setFracaounidademedida(mp.getFracaounidademedida());
						materialproducao.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
						materialproducao.setProducaoetapanome(mp.getProducaoetapanome());
						if(mp.getMaterialproducao() != null && mp.getMaterialproducao().getCdmaterialproducao() != null){
							materialproducao.setCdmaterialproducao(mp.getMaterialproducao().getCdmaterialproducao());
						}else if(materialproducaoComFormula != null){
							materialproducao.setCdmaterialproducao(materialproducaoComFormula.getCdmaterialproducao());
						}
						
						listaProducao.add(materialproducao);
					}
				}
			}
			material.setListaProducao(listaProducao);
		}
			
		try{
			material.setProduto_altura(bean.getProduto_altura());
			material.setProduto_largura(bean.getProduto_largura());
			material.setQuantidade(bean.getProduto_qtde());
			material.setProducaoagendatrans(bean.getProducaoagendatrans());
			materialproducaoService.getValorvendaproducao(material);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (Materialproducao materialproducao : material.getListaProducao()) {
			if(materialproducao.getMaterial() != null && (materialproducao.getUnidademedida() == null || materialproducao.getNaocalcularqtdeprevista() == null || !materialproducao.getNaocalcularqtdeprevista())){
				materialproducao.setUnidademedida(materialproducao.getMaterial().getUnidademedida());
			}
			if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && bean.getProduto_qtde() != null){
				if(materialproducao.getNaocalcularqtdeprevista() == null || !materialproducao.getNaocalcularqtdeprevista()){
					materialproducao.setConsumo(materialproducao.getConsumo()* bean.getProduto_qtde());
				}else {
					materialproducao.setConsumo(materialproducao.getConsumo());
				}
			}
		}
		
		Producaoetapanome producaoetapanome = null;
		if(bean.getProducaoetapaitemtrans() != null && bean.getProducaoetapaitemtrans().getCdproducaoetapaitem() != null){
			Producaoetapaitem producaoetapaitem = producaoetapaitemService.load(bean.getProducaoetapaitemtrans());
			if(producaoetapaitem != null){
				producaoetapanome = producaoetapaitem.getProducaoetapanome();
			}
		}
		material.setListaMateriaprima(createListaMateriaprima(material.getListaProducao(), bean.getPneu(), producaoetapanome, true));
		verificaTrocaMaterialsimilar(material.getListaMateriaprima());
		
		if(SinedUtil.isListNotEmpty(material.getListaMateriaprima())){
			for(Materiaprima mp : material.getListaMateriaprima()){
				if(mp.getMaterial() != null && mp.getMaterial().getNome() != null){
					mp.getMaterial().setNome(new br.com.linkcom.neo.util.StringUtils().escapeQuotes(mp.getMaterial().getNome()));
				}
			}
		}
						
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", material);
		
		return json;
	}
	
	/**
	* M�todo que abre a popup para troca de material similar
	*
	* @param request
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirTrocaMaterial(WebRequestContext request){
		Material material = null;
		String cdmaterial = request.getParameter("cdmaterial");
		String index = request.getParameter("index");
		if(cdmaterial != null && !"".equals(cdmaterial) && index != null && !"".equals(index)){
			material = new Material(Integer.parseInt(cdmaterial));
			material = materialService.findForTrocaMaterialsimilar(material);
			materialService.preencherUnidademedidaPrioritariaProducao(material);
			material.setIndex(index);
		}		
		
		request.getServletResponse().setContentType("text/html");
		if(material != null){
			return new ModelAndView("direct:/crud/popup/popUpTrocamaterial", "bean", material);
		}else {
			View.getCurrent().println("<script>alert('O material n�o possui materiais similares.');parent.$.akModalRemove(true);</script>");
			return null;
		}
	}
	
	/**
	* M�todo que cria uma copia da agenda de produ��o
	*
	* @param origem
	* @return
	* @since 07/03/2016
	* @author Luiz Fernando
	*/
	public Producaoagenda criarCopia(Producaoagenda origem) {
		origem = loadForEntrada(origem);
		if(origem.getCliente() != null){
			origem.setCliente(clienteService.load(origem.getCliente(), "cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj"));
		}
		
		Producaoagenda copia = new Producaoagenda();
		copia = origem;
		copia.setCdproducaoagenda(null);
		copia.setPedidovenda(null);
		copia.setVenda(null);
		copia.setContrato(null);
		copia.setProducaoagendasituacao(Producaoagendasituacao.EM_ESPERA);
		copia.setListaProducaoagendahistorico(null);
		copia.setListaProducaoagendamaterialitem(null);
		copia.setListaProducaoordemmaterialorigem(null);
		copia.setListaProducaoagendaitemadicional(null);
		
		if(SinedUtil.isListNotEmpty(copia.getListaProducaoagendamaterial())){
			for(Producaoagendamaterial producaoagendamaterial : copia.getListaProducaoagendamaterial()){
				producaoagendamaterial.setCdproducaoagendamaterial(null);
				producaoagendamaterial.setListaProducaoagendamaterialmateriaprima(new ArrayList<Producaoagendamaterialmateriaprima>());
				if(producaoagendamaterial.getPneu() != null && producaoagendamaterial.getPneu().getCdpneu() != null){
					producaoagendamaterial.setPneu(pneuService.loadForEntrada(producaoagendamaterial.getPneu()));
				}
			}
		}
		
		return copia;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#updateDataCancelamentoMateriaprimaByProducaoagenda(String whereInProducaoagenda, String whereInMaterial)
	*
	* @param whereInProducaoordem
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForConsumoMateriaprima(String whereInProducaoordem) {
		List<Producaoagenda> listaProducaoagenda = producaoagendaDAO.findForConsumoMateriaprima(whereInProducaoordem);
		if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
			return findForProducao(CollectionsUtil.listAndConcatenate(listaProducaoagenda, "cdproducaoagenda", ","));
		}
		return null;
	}
	
	/**
	 * M�todo que seta o atributo dos campos de dimens�es (altura,largura,comprimento)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void setAtributeDimensaoproducao(WebRequestContext request){
		request.setAttribute("DIMENSAOPRODUCAO_1", parametrogeralService.getDimensaovenda(0));
		request.setAttribute("DIMENSAOPRODUCAO_2", parametrogeralService.getDimensaovenda(1));
		request.setAttribute("DIMENSAOPRODUCAO_3", parametrogeralService.getDimensaovenda(2));
	}
	
	public boolean validaObrigatoriedadePneu(Producaoagenda bean, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(SinedUtil.isRecapagem() && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA) &&
				bean != null && SinedUtil.isListNotEmpty(bean.getListaProducaoagendamaterial())){
			for(Producaoagendamaterial item : bean.getListaProducaoagendamaterial()){
				if(item.getPneu() != null){
					if(!pneuService.validaObrigatoriedadePneu(item.getMaterial(), item.getPneu(), request, errors)){
						valido = false;
					}
				}
			}
		}
		return valido;
	}
	
	public void doBaixaEstoqueItemAdicional(Producaoagenda producaoagenda, List<Producaoagendaitemadicional> listaProducaoagendaitemadicional) {
		Integer cdlocalarmazenagem = parametrogeralService.getIntegerNullable(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA);
		Localarmazenagem localarmazenagem_padrao = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		Localarmazenagem localarmazenagem = producaoagenda.getEmpresa() != null ? producaoagenda.getEmpresa().getLocalarmazenagemmateriaprima() : null;
		
		for (Producaoagendaitemadicional producaoagendaitemadicional : listaProducaoagendaitemadicional) {
			doBaixaEstoqueItemAdicional(producaoagenda, producaoagendaitemadicional, localarmazenagem_padrao, localarmazenagem);
		}
	}
	
	
	public void doBaixaEstoqueItemAdicional(Producaoagenda producaoagenda, Producaoagendaitemadicional producaoagendaitemadicional, Localarmazenagem localarmazenagem_padrao, Localarmazenagem localarmazenagem) {
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setProducaoagenda(producaoagenda);
		movimentacaoestoqueorigem.setRegistrarproducao(true);
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setEmpresa(producaoagenda.getEmpresa());
		movimentacaoestoque.setMaterial(producaoagendaitemadicional.getMaterial());
		movimentacaoestoque.setQtde(producaoagendaitemadicional.getQuantidade());
		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
		movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
		movimentacaoestoque.setLocalarmazenagem(localarmazenagem != null ? localarmazenagem : localarmazenagem_padrao);
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		Material mat = materialService.load(producaoagendaitemadicional.getMaterial(), "material.cdmaterial, material.valorcusto");
		if(mat != null) movimentacaoestoque.setValor(mat.getValorcusto());
		
		movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
	}
	
	public Producaoagenda loadProducaoagenda(Producaoagenda producaoagenda){
		return producaoagendaDAO.loadProducaoagenda(producaoagenda);
	}
	
	public void doBaixaEstoqueBanda(Producaoagendamaterial producaoagendamaterial, 
				Material materialbanda, 
				Double qtdeprevistabanda, 
				Unidademedida unidademedidabanda,
				Material materialacompanhabanda,
				Double qtdeprevistaacompanhabanda,
				Unidademedida unidademedidaacompanhabanda) {
		Producaoagenda producaoagenda = loadProducaoagenda(producaoagendamaterial.getProducaoagenda());
		
		Integer cdlocalarmazenagem = parametrogeralService.getIntegerNullable(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA);
		Localarmazenagem localarmazenagem_padrao = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		Localarmazenagem localarmazenagem = producaoagenda.getEmpresa() != null ? producaoagenda.getEmpresa().getLocalarmazenagemmateriaprima() : null;
		
		Producaoordemmaterialmateriaprima producaoordemmaterialmateriaprima = new Producaoordemmaterialmateriaprima();
		producaoordemmaterialmateriaprima.setMaterial(materialbanda);
		producaoordemmaterialmateriaprima.setQtdeprevista(qtdeprevistabanda);
		producaoordemmaterialmateriaprima.setUnidademedida(unidademedidabanda);
		producaoordemService.registrarSaidaMateriaPrimaChaofabrica(producaoordemmaterialmateriaprima, null, null, producaoagenda, producaoagendamaterial, localarmazenagem, localarmazenagem_padrao);
		
		if(materialacompanhabanda != null && qtdeprevistaacompanhabanda != null){
			Producaoordemmaterialmateriaprima producaoordemmaterialmateriaprimaacompanhabanda = new Producaoordemmaterialmateriaprima();
			producaoordemmaterialmateriaprimaacompanhabanda.setMaterial(materialacompanhabanda);
			producaoordemmaterialmateriaprimaacompanhabanda.setQtdeprevista(qtdeprevistaacompanhabanda);
			producaoordemmaterialmateriaprimaacompanhabanda.setUnidademedida(unidademedidaacompanhabanda);
			producaoordemService.registrarSaidaMateriaPrimaChaofabrica(producaoordemmaterialmateriaprimaacompanhabanda, null, null, producaoagenda, producaoagendamaterial, localarmazenagem, localarmazenagem_padrao);
		}
	}

	public List<Producaoagenda> findByPneu(Pneu pneu, Pedidovenda pedidoVenda){
		return producaoagendaDAO.findByPneu(pneu, pedidoVenda);
	}
}
