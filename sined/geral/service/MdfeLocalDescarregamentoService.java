package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeLocalDescarregamento;
import br.com.linkcom.sined.geral.dao.MdfeLocalDescarregamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeLocalDescarregamentoService extends GenericService<MdfeLocalDescarregamento>{

	private MdfeLocalDescarregamentoDAO mdfeLocalDescarregamentoDAO;
	public void setMdfeLocalDescarregamentoDAO(
			MdfeLocalDescarregamentoDAO mdfeLocalDescarregamentoDAO) {
		this.mdfeLocalDescarregamentoDAO = mdfeLocalDescarregamentoDAO;
	}
	
	public List<MdfeLocalDescarregamento> findByMdfe(Mdfe mdfe){
		return mdfeLocalDescarregamentoDAO.findByMdfe(mdfe);
	}
}
