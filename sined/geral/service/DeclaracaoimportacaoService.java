package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.dao.DeclaracaoimportacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DeclaracaoimportacaoService extends GenericService<Declaracaoimportacao> {

	private DeclaracaoimportacaoDAO declaracaoimportacaoDAO;
	
	public void setDeclaracaoimportacaoDAO(
			DeclaracaoimportacaoDAO declaracaoimportacaoDAO) {
		this.declaracaoimportacaoDAO = declaracaoimportacaoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DeclaracaoimportacaoDAO#loadForConsulta(Declaracaoimportacao declaracaoimportacao)
	 *
	 * @param declaracaoimportacao
	 * @return
	 * @since 11/09/2012
	 * @author Rodrigo Freitas
	 */
	public Declaracaoimportacao loadForConsulta(Declaracaoimportacao declaracaoimportacao) {
		return declaracaoimportacaoDAO.loadForConsulta(declaracaoimportacao);
	}

	public void ajustaDIToSave(Declaracaoimportacao bean) {
		if(bean.getNumerodidsida() != null){
			bean.setNumerodidsida(bean.getNumerodidsida().replace("/", "").replace("-", ""));
		}
	}
}
