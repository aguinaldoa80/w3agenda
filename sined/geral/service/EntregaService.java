package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Transient;
import javax.servlet.http.Cookie;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.EntregaDocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamaterialinspecao;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Obslancamentofiscal;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompraorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.ClassificacaoCurvaAbc;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialRateioEstoqueTipoContaGerencial;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.EntregaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Rastro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeDuplicataBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.BaixarEntregaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.BaixarEntregaItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.EmitircurvaabcBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaEntradaEstoque;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaRomaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaSolicitacao;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntrega;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EntregaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.processador.ProcessadorCte;
import br.com.linkcom.sined.util.processador.ProcessadorNfe;
import br.com.linkcom.utils.LkNfeUtil;
import br.com.linkcom.utils.StringUtils;

import com.ibm.icu.text.SimpleDateFormat;

public class EntregaService extends GenericService<Entrega> {

	private EntregaDAO entregaDAO;	
	private EntregapagamentoService entregapagamentoService;
	private EntregamaterialService entregamaterialService;
	private RateioitemService rateioitemService;
	private PrazopagamentoService prazopagamentoService;
	private FornecedorService fornecedorService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private PatrimonioitemService patrimonioitemService;
	private MovpatrimonioService movpatrimonioService;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private MaterialService materialService;
	private OrdemcompraService ordemcompraService;
	private CotacaoService cotacaoService;
	private SolicitacaocompraService solicitacaocompraService;
	private OrdemcomprahistoricoService ordemcomprahistoricoService;
	private DocumentoorigemService documentoorigemService;
	private RateioService rateioService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private MaterialinspecaoitemService materialinspecaoitemService;
	private EntregamaterialinspecaoService inspecaoService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private RomaneioService romaneioService;
	private AvisoService avisoService;
	private CfopService cfopService;
	private UnidademedidaService unidademedidaService;
	private BempatrimonioService bempatrimonioService;
	private EntradafiscalService entradafiscalService;
	private ContapagarService contapagarService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private ArquivoDAO arquivoDAO;
	private VendaService vendaService;
	private OrdemcompraentregaService ordemcompraentregaService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private PessoaquestionarioService pessoaquestionarioService; 
	private RequisicaomaterialService requisicaomaterialService;
	private DocumentoService documentoService;
	private EmpresaService empresaService;
	private EntregadocumentoService entregadocumentoService;
	private ProdutoService produtoService;
	private ParametrogeralService parametrogeralService;
	private EntregadocumentofreteService entregadocumentofreteService;
	private EntregadocumentofreterateioService entregadocumentofreterateioService;
	private ModelodocumentofiscalService modelodocumentofiscalService;
	private AjustefiscalService ajustefiscalService;
	private DocumentotipoService documentotipoService;
	private EntregamaterialloteService entregamaterialloteService;
	private MotivoavisoService motivoavisoService;
	private AvisousuarioService avisoUsuarioService;
	private RomaneioorigemService romaneioorigemService;
	private ManifestoDfeService manifestoDfeService;
	private HistoricooperacaoService historicooperacaoService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
		
	//	Atributos para fazer a busca da nota fiscal com a chave de acesso
	private static final String REGEXP_SPANTAG = "(<span(.*?)>)(((<(.|\n)*?>)*?)(.*?))(</span>)";
	private static final Pattern PATTERN_SPANTAG = Pattern.compile(REGEXP_SPANTAG, Pattern.MULTILINE);
	private static enum TipoConsulta {NFE, CTE};
//	private static final Pattern PATTERN_TABLETAG = Pattern.compile(REGEXP_TABLETAG, Pattern.MULTILINE);
	
	public void setModelodocumentofiscalService(ModelodocumentofiscalService modelodocumentofiscalService) {this.modelodocumentofiscalService = modelodocumentofiscalService;}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {this.ordemcompramaterialService = ordemcompramaterialService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {this.solicitacaocompraService = solicitacaocompraService;}
	public void setCotacaoService(CotacaoService cotacaoService) {this.cotacaoService = cotacaoService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}	
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMovpatrimonioorigemService(MovpatrimonioorigemService movpatrimonioorigemService) {this.movpatrimonioorigemService = movpatrimonioorigemService;}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {this.movpatrimonioService = movpatrimonioService;}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {this.patrimonioitemService = patrimonioitemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setEntregaDAO(EntregaDAO entregaDAO) {this.entregaDAO = entregaDAO;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setEntregapagamentoService(EntregapagamentoService entregapagamentoService) {this.entregapagamentoService = entregapagamentoService;}
	public void setOrdemcomprahistoricoService(OrdemcomprahistoricoService ordemcomprahistoricoService) {this.ordemcomprahistoricoService = ordemcomprahistoricoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setMaterialinspecaoitemService(MaterialinspecaoitemService materialinspecaoitemService) {this.materialinspecaoitemService = materialinspecaoitemService;}
	public void setInspecaoService(EntregamaterialinspecaoService inspecaoService) {this.inspecaoService = inspecaoService;}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;}
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setBempatrimonioService(BempatrimonioService bempatrimonioService) {this.bempatrimonioService = bempatrimonioService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setEntregadocumentohistoricoService(EntregadocumentohistoricoService entregadocumentohistoricoService) {this.entregadocumentohistoricoService = entregadocumentohistoricoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setOrdemcompraentregaService(OrdemcompraentregaService ordemcompraentregaService) {this.ordemcompraentregaService = ordemcompraentregaService;}
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {this.pessoaquestionarioService = pessoaquestionarioService;}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {this.requisicaomaterialService = requisicaomaterialService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setEntregadocumentoService(EntregadocumentoService entregadocumentoService) {this.entregadocumentoService = entregadocumentoService;	}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setEntregadocumentofreteService(EntregadocumentofreteService entregadocumentofreteService) {this.entregadocumentofreteService = entregadocumentofreteService;}
	public void setEntregadocumentofreterateioService(EntregadocumentofreterateioService entregadocumentofreterateioService) {this.entregadocumentofreterateioService = entregadocumentofreterateioService;}
	public void setAjustefiscalService(AjustefiscalService ajustefiscalService) {this.ajustefiscalService = ajustefiscalService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setEntregamaterialloteService(EntregamaterialloteService entregamaterialloteService) {this.entregamaterialloteService = entregamaterialloteService;}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {this.motivoavisoService = motivoavisoService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setRomaneioorigemService(RomaneioorigemService romaneioorigemService) {this.romaneioorigemService = romaneioorigemService;}
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {this.manifestoDfeService = manifestoDfeService;}
	public void setHistoricooperacaoService(HistoricooperacaoService historicooperacaoService) {this.historicooperacaoService = historicooperacaoService;}
	public void setColaboradorFornecedorService(ColaboradorFornecedorService colaboradorFornecedorService) {this.colaboradorFornecedorService = colaboradorFornecedorService;}
	
	@Override
	public void delete(final Entrega bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				Entrega entrega = loadForEntrada(bean);
				
				List<Entregadocumento> listaEntregadocumento = entrega.getListaEntregadocumento();
				for (Entregadocumento entregadocumento : listaEntregadocumento) {
					if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
						Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
						for (Entregamaterial entregamaterial : listaEntregamaterial) {
							entregamaterialService.delete(entregamaterial);
						}						
					}	
					if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
						Set<Entregapagamento> listaEntregapagamento = entregadocumento.getListadocumento();
						for (Entregapagamento entregapagamento : listaEntregapagamento) {
							entregapagamentoService.delete(entregapagamento);
						}						
					}	
					entradafiscalService.delete(entregadocumento);
				}				
				getGenericDAO().delete(entrega);
				
				return null;
			}
		});
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findByOrdemcompra
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findByOrdemcompra(Ordemcompra ordemcompra){
		return entregaDAO.findByOrdemcompra(ordemcompra);
	}
	
	/**
	 * Gera relat�rio com valores iguais o da listagem.
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(EntregaFiltro filtro) {
		Report report = new Report("/suprimento/entrega");
		Report reportlista = new Report("/suprimento/sub_entradafiscal");
		Report reportlistaRateio = new Report("/suprimento/sub_entradafiscal_rateio");
		
		List<Entrega> lista =  entregaDAO.findForPdfListagem(filtro);
		
		String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdentrega", ",");
		lista.removeAll(lista);
		lista.addAll(loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		if(lista != null && lista.size() > 0)
			report.setDataSource(lista);
		
		report.addSubReport("SUB_ENTRADAFISCAL", reportlista);
		report.addSubReport("SUB_ENTRADAFISCAL_RATEIO", reportlistaRateio);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#loadWithSituacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> loadWithSituacao(String whereIn) {
		return entregaDAO.loadWithSituacao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#updateCancelada
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateCancelada(String whereIn) {
		entregaDAO.updateCancelada(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#updateEstorno
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateEstorno(String whereIn) {
		entregaDAO.updateEstorno(whereIn);	
	}
	
	/**
	* Faz refer�ncia ao DAO
	* 
	* @see	br.com.linkcom.sined.geral.dao.EntregaDAO#listEntregaForFaturar(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Jul 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Entrega> listEntregaForFaturar(String whereIn){
		return entregaDAO.listEntregaForFaturar(whereIn);		
	}
	
	/**
	* M�todo para verificar quantidade de entregas selecionadas
	*
	* @param entregasSelecionadas
	* @return
	* @since Jul 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public int verificaQtdeItens(String entregasSelecionadas){
		int qtde = 0;
		for(int i = 0; i < entregasSelecionadas.length(); i++){
			if(entregasSelecionadas.charAt(i) == ',' || (entregasSelecionadas.length()-1) == i){
				qtde += 1;
			}
		}
		return qtde;
	}
	
	/**
	 * Faz o faturamento das entregas selecionadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findForFaturar(String)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#preencheDocumento(Documentotipo, Documento, Entrega, Entregapagamento)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#calculaRateioParaPrimeiraParcela(Entrega, Entregapagamento, Money)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#preencheFinanciamento(Entregapagamento, int)
	 * @see br.com.linkcom.sined.geral.service.RateioService#findRateio(Rateio)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#valorTotalDeCadaContaGerencialDoRateio(Rateio, Set)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#calculaRateioUtilizandoRateioDaOrdemDeCompra(Set, List, HashMap, Entregapagamento, Money)
	 * @param entregasSelecionadas
	 * @param documentotipo
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public Boolean doFaturar(String entregasSelecionadas, Documentotipo documentotipo, HashMap<Integer, String> retornowms) {		
		Boolean faturado = false;
		Double valorTotalEntrega = null;
		Set<Entregapagamento> listaEntregaPagamento = null;
		Documento documento = new Documento();
		Rateio rateio = new Rateio();
		StringBuilder whereInEntregadocumento = new StringBuilder();
		Boolean NOTACOMPRA_GERA_CONTADEFINITIVA = parametrogeralService.getBoolean(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA);
		
		List<Documentoorigem> listaDocumentoorigemAntecipacao = new ArrayList<Documentoorigem>();
		Documentoorigem documentoorigem;
		
		Conta vinculoProvisionado = null;
		if(documentotipo != null && documentotipo.getCddocumentotipo() != null){
			Documentotipo docTipo = documentotipoService.loadWithContadestino(documentotipo);
			if(docTipo != null && docTipo.getContadestino() != null){
				vinculoProvisionado = docTipo.getContadestino();
			}
		}
		
		if(verificaQtdeItens(entregasSelecionadas) > 1){
			List<Entrega> listaEntrega = this.listEntregaForFaturar(entregasSelecionadas);
			int tam = listaEntrega.size();
			int i = 1;
			valorTotalEntrega = 0.0;
			documentoService.defineDocumentoacaoParametroGeral(documento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
			for(Entrega entrega: listaEntrega){				
				if( i == tam){
					valorTotalEntrega += entrega.getValorTotalPagamento().getValue().doubleValue();
					documento.setEmpresa(entrega.getEmpresa());
					documento.setNumero(null);
					documento.setDocumentotipo(documentotipo);
					if(vinculoProvisionado != null){
						documento.setVinculoProvisionado(vinculoProvisionado);
					}
					documento.setDescricao("Conta a pagar referente as entregas: "+ entregasSelecionadas);
					documento.setDtemissao(new Date(System.currentTimeMillis()));
					documento.setDtcompetencia(new Date(System.currentTimeMillis()));
					documento.setDtvencimento(getDtvencimentoMenor(listaEntrega));
					documento.setValor(new Money(valorTotalEntrega));
					documento.setReferencia(Referencia.MES_ANO_CORRENTE);
					documento.setTipopagamento(Tipopagamento.FORNECEDOR);
					documento.setPrazo(null);
					documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
					if(documento.getEmpresa() == null){
						if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(entrega.getEmpresa());
						} else if(entrega.getHaveOrdemcompra()){
							Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
							for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
								if(ordemcompraentrega.getOrdemcompra() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
									documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
									break;
								}
							}
						}
					}
				}
				else{
					valorTotalEntrega += entrega.getValorTotalPagamento().getValue().doubleValue();
					if(documento.getEmpresa() == null){
						if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(entrega.getEmpresa());
						} else if(entrega.getHaveOrdemcompra()){
							Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
							for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
								if(ordemcompraentrega.getOrdemcompra() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
									documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
									break;
								}
							}
						}
					}
				}
				i++;
			}			
						
			Entregapagamento entregapagamentoaux = new Entregapagamento();
			Set<Entregamaterial> listaEntregaMaterialaux = new ListSet<Entregamaterial>(Entregamaterial.class);
			double valorTotalEntregapagamento = 0.0;
			
			boolean existDocumento = false;
			boolean existDocumentoAntecipacao = false;
			for(Entrega e: listaEntrega){
				for(Entregadocumento ed : e.getListaEntregadocumento()){
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
							if(!whereInEntregadocumento.toString().equals("")) whereInEntregadocumento.append(",");
							whereInEntregadocumento.append(ed.getCdentregadocumento());
						}
						if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(ed.getEmpresa());
						}
						documento.setFornecedor(ed.getFornecedor());
						documento.setPessoa(ed.getFornecedor());
						documento.setDescricao(documento.getDescricao() + " - NF " + ed.getNumero());
						
						listaEntregaPagamento = ed.getListadocumento();	
						for(Entregapagamento entregapagamento : listaEntregaPagamento){
							if(entregapagamento.getDocumentoantecipacao() == null){
								valorTotalEntregapagamento += entregapagamento.getValor().getValue().doubleValue();
								existDocumento = true;
							}else {
								documentoorigem = new Documentoorigem();
								documentoorigem.setEntrega(e);
								if(ed.getNumero() != null && !"".equals(ed.getNumero())){
									entregapagamento.getDocumentoantecipacao().setNumero(ed.getNumero());
								}
								documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
								if(!existeDocumentoantecipacaoEntrega(documentoorigem, listaDocumentoorigemAntecipacao)){
									listaDocumentoorigemAntecipacao.add(documentoorigem);
								}
								existDocumentoAntecipacao = true;
								faturado = true;
							}
						}
						
						for(Entregamaterial entregamaterial : ed.getListaEntregamaterial()){
							listaEntregaMaterialaux.add(entregamaterial);
						}
					}
					
					this.preencheListaApropriacaoDocumento(documento, ed.getListaApropriacao());
				}
			}
			
			if(existDocumento){
				if(existDocumentoAntecipacao){
					documento.setValor(new Money(valorTotalEntregapagamento));
				}
				entregapagamentoaux.setValor(new Money(valorTotalEntregapagamento));
				List<Rateioitem> listaRateioitem = this.calculaRateioVariasEntregas(listaEntrega);
				rateioitemService.agruparItensRateio(listaRateioitem);
				rateio.setListaRateioitem(listaRateioitem);
				
				if(rateio != null)
					rateioService.limpaReferenciaRateio(rateio);
				documento.setRateio(rateio);						
				documento.setWhereInEntrega(entregasSelecionadas);
				
				if(documento.getDescricao() != null && documento.getDescricao().length() > 500){
					documento.setDescricao(documento.getDescricao().substring(0, 500));
				}
								
				contapagarService.saveOrUpdate(documento);
				faturado = true;
			}
			
		}else{
			Entrega entrega = this.findForFaturar(entregasSelecionadas);
			List<Documento> financiamentos = new ListSet<Documento>(Documento.class);
	
			valorTotalEntrega = entrega.getValorTotalEntrega().getValue().doubleValue();
			String whereInEd = null;
			if(retornowms != null){
				whereInEd = retornowms.get(entrega.getCdentrega());
			}
			
			boolean existDocumento = false;
			for(Entregadocumento ed : entrega.getListaEntregadocumento()){
				if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
					if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
						if(!whereInEntregadocumento.toString().equals("")) whereInEntregadocumento.append(",");
						whereInEntregadocumento.append(ed.getCdentregadocumento());
					}
					documento = new Documento();
					financiamentos = new ListSet<Documento>(Documento.class);
					
					if(whereInEd == null || "".equals(whereInEd) || !whereInEd.contains(ed.getCdentregadocumento().toString())){
						if(ed.getRateio() != null && ed.getRateio().getCdrateio() != null) {
							ed.setRateio(rateioService.findRateio(ed.getRateio()));
						}
						
						listaEntregaPagamento = ed.getListadocumento();				
						int parcelas = 1;				
						
						for (Entregapagamento entregapagamento : listaEntregaPagamento) {
							if(entregapagamento.getDocumentoantecipacao() == null){
								if(parcelas == 1){
									documentoService.defineDocumentoacaoParametroGeral(documento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
									this.preencheDocumento(documentotipo, documento, ed,entregapagamento);

									if(documento.getVinculoProvisionado() == null){
										if(vinculoProvisionado != null){
											documento.setVinculoProvisionado(vinculoProvisionado);
										}else if(ed.getDocumentotipo() != null && ed.getDocumentotipo().getContadestino() != null){
											documento.setVinculoProvisionado(ed.getDocumentotipo().getContadestino());
										}
									}
									
									rateioitemService.agruparItensRateio(ed.getRateio().getListaRateioitem());
									
									rateio.setListaRateioitem(ed.getRateio().getListaRateioitem());
									rateioService.atualizaValorRateio(rateio, entregapagamento.getValor());
								
								} else{
									Documento financiamento = this.preencheFinanciamento(entregapagamento, parcelas, ed);
									documentoService.defineDocumentoacaoParametroGeral(financiamento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
									financiamentos.add(financiamento);
								}
								
								parcelas++;
								existDocumento = true;
							}else {
								documentoorigem = new Documentoorigem();
								documentoorigem.setEntrega(entrega);
								if(ed.getNumero() != null && !"".equals(ed.getNumero())){
									entregapagamento.getDocumentoantecipacao().setNumero(ed.getNumero());
								}
								documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
								if(!existeDocumentoantecipacaoEntrega(documentoorigem, listaDocumentoorigemAntecipacao)){
									listaDocumentoorigemAntecipacao.add(documentoorigem);
								}
								faturado = true;
							}
						}
						if(existDocumento){
							if(rateio != null)
								rateioService.limpaReferenciaRateio(rateio);
							documento.setRateio(rateio);
							documento.setListaDocumento(financiamentos);
							
							Money valorRateio = new Money();
							
							for (Rateioitem ri : ed.getRateio().getListaRateioitem()) {
								BigDecimal valor = SinedUtil.round(ri.getValor().getValue(), 2);
								valorRateio = valorRateio.add(new Money(valor));
							}
							
							valorRateio = documento.getValor().subtract(valorRateio);
							
							if (!valorRateio.toString().equals("0,00") && !valorRateio.toString().equals("-0,00")) {
								BigDecimal documentoValor = SinedUtil.round(documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).getValor().getValue(), 2);
								BigDecimal rateioValor = SinedUtil.round(valorRateio.getValue(), 2);
								documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).setValor(new Money(documentoValor).add(new Money(rateioValor)));
							}
							
							if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
								documento.setEmpresa(ed.getEmpresa());
							}else if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
								documento.setEmpresa(entrega.getEmpresa());
							} else if(entrega.getHaveOrdemcompra()){
								Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
								for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
									if(ordemcompraentrega.getOrdemcompra() != null && 
											ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
											ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
										documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
										break;
									}
								}
							}
							if(!documento.getIsDescricaoHistorico()){
								String natop = entregadocumentoService.findNatop(entrega.getCdentrega());
								if (natop != null && !"".equals(natop)){
									documento.setDescricao(natop + ", referente a entrega: "+ entregasSelecionadas);
								}else{
									documento.setDescricao("Conta a pagar referente a entrega: "+ entregasSelecionadas + " - NF " + ed.getNumero());			
								}
							}
							documento.setFornecedor(ed.getFornecedor());
							documento.setPessoa(ed.getFornecedor());
							documento.setDtemissao(ed.getDtemissao());
							documento.setFornecedor(ed.getFornecedor());
							documento.setDocumentotipo(ed.getDocumentotipo());
							if(ed.getDocumentotipo() != null)
								documento.setDocumentotipo(ed.getDocumentotipo());
							
							if(documento.getDescricao() != null && documento.getDescricao().length() > 500){
								documento.setDescricao(documento.getDescricao().substring(0, 500));
							}
							
							Date dtCompetencia = null;
							if(ed.getDtentrada() != null) {
								dtCompetencia = ed.getDtentrada();
							} else if(ed.getDtemissao() != null) {
								dtCompetencia = ed.getDtemissao();
							}
							
							documento.setDtcompetencia(dtCompetencia);
							this.preencheListaApropriacaoDocumento(documento, ed.getListaApropriacao());
							
							contapagarService.saveOrUpdate(documento);
							faturado = true;
						}
					}
				}
			}	
		}
		
		if(listaDocumentoorigemAntecipacao != null && !listaDocumentoorigemAntecipacao.isEmpty()){
			for(Documentoorigem doco : listaDocumentoorigemAntecipacao){
				if(doco.getDocumento() != null && doco.getDocumento().getCddocumento() != null && 
						doco.getEntrega() != null && doco.getEntrega().getCdentrega() != null){
					if(doco.getDocumento().getNumero() != null && !"".equals(doco.getDocumento().getNumero())){
						documentoService.updateNumeroByEntradafsical(doco.getDocumento(), doco.getDocumento().getNumero());
					}
					documentoorigemService.saveOrUpdate(doco);
					this.callProcedureAtualizaEntrega(doco.getEntrega());
				}
			}
		}
		
		if(!whereInEntregadocumento.toString().equals("")){
			entradafiscalService.updateSituacaoEntradafiscal(whereInEntregadocumento.toString(), Entregadocumentosituacao.FATURADA);
		}
		
		return faturado;
	}
	
	/**
	 * M�todo que verifica se existe documentoorigem com documento e entrega 
	 *
	 * @param documentoorigem
	 * @param listaDocumentoorigemAntecipacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeDocumentoantecipacaoEntrega(Documentoorigem documentoorigem, List<Documentoorigem> listaDocumentoorigemAntecipacao) {
		Boolean existe = Boolean.FALSE;		
		if(listaDocumentoorigemAntecipacao != null && !listaDocumentoorigemAntecipacao.isEmpty() && 
				documentoorigem != null && documentoorigem.getDocumento() != null && 
				documentoorigem.getDocumento().getCddocumento() != null && 
				documentoorigem.getEntrega() != null && documentoorigem.getEntrega().getCdentrega() != null){
			for(Documentoorigem doco : listaDocumentoorigemAntecipacao){
				if(doco.getDocumento() != null && doco.getDocumento().getCddocumento() != null && 
						doco.getEntrega() != null && doco.getEntrega().getCdentrega() != null)
				if(doco.getDocumento().getCddocumento().equals(documentoorigem.getCddocumentoorigem()) && 
						doco.getEntrega().getCdentrega().equals(documentoorigem.getEntrega().getCdentrega())){
					existe = Boolean.TRUE;
					break;
				}
			}
		}		
		return existe;
	}
	
	/**
	 * M�todo que busca a menor data de vencimento
	 *
	 * @param listaEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Date getDtvencimentoMenor(List<Entrega> listaEntrega) {
		Date dtvencimento = null;
		if(listaEntrega != null && !listaEntrega.isEmpty()){
			for(Entrega e : listaEntrega){
				if(e.getListaEntregadocumento() != null && !e.getListaEntregadocumento().isEmpty()){
					for(Entregadocumento ed : e.getListaEntregadocumento()){
						if(ed.getListadocumento() != null && !ed.getListadocumento().isEmpty()){
							for(Entregapagamento ep : ed.getListadocumento()){
								if(dtvencimento == null){
									dtvencimento = ep.getDtvencimento();
								}else if(ep.getDtvencimento() != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtvencimento, ep.getDtvencimento())){
									dtvencimento = ep.getDtvencimento();
								}
							}
						}
					}
				}
			}
		}
		return dtvencimento;
	}
	
	/**
	* M�todo que calcula rateio para v�rias entregas
	* 
	* #findRateio(Rateio rateio)
	*
	* @param listaEntrega
	* @param entregapagamento2
	* @param valorTotalEntrega
	* @return
	* @since Jul 19, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Rateioitem> calculaRateioVariasEntregas(List<Entrega> listaEntrega){
		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
		for(Entrega entrega : listaEntrega){
			for(Entregadocumento ed : entrega.getListaEntregadocumento()){
				ed.setRateio(rateioService.findRateio(ed.getRateio()));
				itens.addAll(ed.getRateio().getListaRateioitem());
			}
		}			
		
		return itens;
	}
	
	/**
	 * M�todo que calcula o valor total das contas gerenciais a partir da lista de materiais da tela entrega
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadFetch(Material, String)
	 * @param listaEntregamaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	/*private HashMap<Integer, Money> calculaValorTotalContasGerenciais(Set<Entregamaterial> listaEntregamaterial) {
		HashMap<Integer, Money> map = new HashMap<Integer, Money>();
		
		for (Entregamaterial entregamaterial : listaEntregamaterial) {
			if(entregamaterial.getMaterial() != null){
				Material material = materialService.load(entregamaterial.getMaterial(), "material.contagerencial");
				
				if(map.containsKey(material.getContagerencial().getCdcontagerencial())){
					map.put(material.getContagerencial().getCdcontagerencial(), map.get(material.getContagerencial().getCdcontagerencial()).add(entregamaterial.getValortotalmaterial() != null ? entregamaterial.getValortotalmaterial() : new Money(0)));
				} else{
					map.put(material.getContagerencial().getCdcontagerencial(), entregamaterial.getValortotalmaterial() != null ? entregamaterial.getValortotalmaterial() : new Money(0));
				}
			}
		}
		return map;
	}*/
	
	/**
	 * Este m�todo preenche as parcelas que ir�o virar financiamento na conta a pagar
	 * 
	 * @param entregapagamento
	 * @param parcelas
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Documento preencheFinanciamento(Entregapagamento entregapagamento, int parcelas, Entregadocumento entregadocumento) {
		Documento financiamento = new Documento();
		financiamento.setValor(entregapagamento.getValor());
		financiamento.setParcela(new Integer(parcelas));
		financiamento.setDtvencimento(entregapagamento.getDtvencimento());
		financiamento.setDtemissao(entregapagamento.getDtemissao());
		financiamento.setDocumentoacao(Documentoacao.PREVISTA);
		
		if (parametrogeralService.getBoolean("PREENCHE_NUMNF_CAMPO_DOCUMENTO")) {
			financiamento.setNumero(entregadocumento.getNumero() != null ? entregadocumento.getNumero().toString() : null);
		} else {
			financiamento.setNumero(entregapagamento.getNumero() != null ? entregapagamento.getNumero().toString() : null);
		}
		
		return financiamento;
	}
	
	/**
	 * M�todo que calcula o rateio da 1� parcela da conta a pagar que ser� gerada. Calcula com base no rateio da ordem de compra
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#verificaItemRateioIgual(Rateioitem, Rateioitem)
	 * @param listaEntregamaterial
	 * @param listaRateioitem
	 * @param entregapagamento
	 * @param valorTotalEntrega
	 * @return
	 * @author Tom�s Rabelo
	 */
//	private Rateio calculaRateioUtilizandoRateioDaOrdemDeCompra(List<Entregamaterial> listaEntregamaterial, List<Rateioitem> listaRateioitem, Entregapagamento entregapagamento, Double valorTotalEntrega) {
//		Rateio rateio = new Rateio();
//		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
//		
//		for (Entregamaterial entregamaterial : listaEntregamaterial) {
//			for (Rateioitem rateioitem2 : listaRateioitem) {
//
////				if(entregamaterial.getMaterial().getContagerencial().equals(rateioitem2.getContagerencial())){
//					Rateioitem rateioitem = new Rateioitem(
//							rateioitem2.getContagerencial(), rateioitem2.getCentrocusto(), rateioitem2.getProjeto());
//					
////					Money valorTotalContaGerencial = map.get(entregamaterial.getMaterial().getContagerencial().getCdcontagerencial());
////					double percentualMaterialNaContaGerencial = entregamaterial.getValortotalmaterial().getValue().doubleValue() / valorTotalContaGerencial.getValue().doubleValue();
////					double percentualContaGerencialNaEntrega = valorTotalContaGerencial.getValue().doubleValue() / valorTotalEntrega.getValue().doubleValue();
//					
//					double percentualMaterialNaEntrega = entregamaterial.getValortotalmaterial() / valorTotalEntrega;
//					rateioitem.setValor(new Money(SinedUtil.truncate(entregapagamento.getValor().getValue().doubleValue() * percentualMaterialNaEntrega * (rateioitem2.getPercentual() / 100)/**percentualMaterialNaContaGerencial*/)));
//					
//					boolean rateioExistente = false;
//					for (Rateioitem rateioitemaux : itens) {
//						if(rateioitemService.verificaItemRateioIgual(rateioitem, rateioitemaux)){
//							rateioitemaux.setValor(rateioitemaux.getValor().add(rateioitem.getValor()));
//							rateioExistente = true;
//							break;
//						}
//					}
//					if(!rateioExistente){
//						itens.add(rateioitem);
//					}
////					else
////						break;
////				}
//			}
//		}
//		
//		for (Rateioitem rateioitem : itens) {
//			rateioitem.setPercentual(this.calculaPercentual(entregapagamento.getValor(), rateioitem.getValor()));
//		}	
//		
//		rateio.setListaRateioitem(itens);
//		return rateio;
//	}
	
//	/**
//	 * Este m�todo soma o total de cada conta ger�ncial do rateio
//	 * 
//	 * @param rateio2
//	 * @param entregasMaterial
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	private HashMap<Integer, Double> valorTotalDeCadaContaGerencialDoRateio(Rateio rateio2, Set<Entregamaterial> entregasMaterial) {
//		//Calcula o valor total em cada conta ger�ncial
//		HashMap<Integer, Double> map = new HashMap<Integer, Double>();
//		for (Rateioitem rateioitem2 : rateio2.getListaRateioitem()) {
//			for (Entregamaterial entregamaterial : entregasMaterial) {
//				if(rateioitem2.getContagerencial().equals(entregamaterial.getMaterial().getContagerencial())){
//				
//					if(map.containsKey(rateioitem2.getContagerencial().getCdcontagerencial())){
//						map.put(rateioitem2.getContagerencial().getCdcontagerencial(), map.get(rateioitem2.getContagerencial().getCdcontagerencial()) + entregamaterial.getValortotalmaterial());
//					} else{
//						map.put(rateioitem2.getContagerencial().getCdcontagerencial(), entregamaterial.getValortotalmaterial());
//					}
//				}
//			}
//		}
//		return map;
//	}
	/**
	 * Preenche algumas informa��es para a cria��o da conta a pagar.
	 *
	 * @param documentotipo
	 * @param documento
	 * @param entrega
	 * @param entregapagamento
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	private void preencheDocumento(Documentotipo documentotipo, Documento documento, Entregadocumento entregadocumento, Entregapagamento entregapagamento) {
		documento.setEmpresa(entregadocumento.getEmpresa());
		
		if (parametrogeralService.getBoolean("PREENCHE_NUMNF_CAMPO_DOCUMENTO") && entregadocumento.getNumero() != null) {
			documento.setNumero(entregadocumento.getNumero());
		} else {
			documento.setNumero(entregapagamento.getNumero() != null ? entregapagamento.getNumero().toString() : null);
		}
		
		documento.setDocumentotipo(documentotipo);
		documento.setDescricao("Conta a pagar referente a entrega: " + entregadocumento.getEntrega().getCdentrega() + " - NF " + entregadocumento.getNumero());	
		documento.setDtemissao(entregapagamento.getDtemissao());
		documento.setDtvencimento(entregapagamento.getDtvencimento());
		documento.setValor(entregapagamento.getValor());
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		
		documento.setDtemissao(entregadocumento.getDtemissao());
		documento.setEntrega(entregadocumento.getEntrega());
		documento.setTipopagamento(Tipopagamento.FORNECEDOR);
		documento.setFornecedor(entregadocumento.getFornecedor());
		documento.setPessoa(entregadocumento.getFornecedor());
		documento.setPrazo(entregadocumento.getPrazopagamento());
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setIsDescricaoHistorico(historicooperacaoService.addDescricaoEntregaByHistoricooperacao(documento, entregadocumento, entregapagamento));
		
		if(entregadocumento.getListadocumento() != null && entregadocumento.getListadocumento().size() > 1){
			documento.setFinanciamento(Boolean.TRUE);
			documento.setRepeticoes(entregadocumento.getListadocumento().size()-1);
		}
	}
	
	/**
	 * Faz o recalculamento dos percentuais passando a lista de rateio item.
	 * 
	 * @param listaRateioitem
	 * @author Rodrigo Freitas
	 */
	public void reCalculaPercentual(List<Rateioitem> listaRateioitem) {
		Double total = 0.0;
		
		for (Rateioitem rateioitem : listaRateioitem) {
			total += rateioitem.getPercentual();
		}
		
		if (total.doubleValue() != 100) {
			listaRateioitem.get(0).setPercentual((100 - total) + listaRateioitem.get(0).getPercentual());
		}
	}
	
	/**
	 * M�todo que calcula o percentual de dois moneys.
	 * 
	 * @param totalmoney
	 * @param partemoney
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Double calculaPercentual(Money totalmoney, Money partemoney) {
		Double total = totalmoney.getValue().doubleValue();
		Double parte = partemoney.getValue().doubleValue();
		
		return SinedUtil.truncate((parte*100)/total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForFaturar
	 * @param cdentrega
	 * @return
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public Entrega findForFaturar(String cdentrega) {
		return entregaDAO.findForFaturar(cdentrega);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Busca a lista de entregas para faturamento
	*
	* @see	br.com.linkcom.sined.geral.dao.EntregaDAO#findForFaturarList(String whereInCdEntrega)

		
	/**
	 * Seta valores dos pagamentos, no processo de gerar uma entrega a partir de uma ordem de compra.
	 * 
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#loadForEntrada
	 * @param ordemcompra
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Set<Entregapagamento> setaPagamentos(Ordemcompra ordemcompra, Entregadocumento entregadocumento) {
		
		Set<Entregapagamento> listaPagamento = new ListSet<Entregapagamento>(Entregapagamento.class);
		Entregapagamento entregapagamento = null;
		
		Prazopagamento prazopagamento = prazopagamentoService.loadForEntrada(ordemcompra.getPrazopagamento());
		
		Calendar ultimaData = Calendar.getInstance();
		ultimaData.setTimeInMillis(entregadocumento.getDtemissao().getTime());

		Double total = entregadocumento.getValortotaldocumentoreceita().getValue().doubleValue();
		Double parcela = SinedUtil.truncate(total/prazopagamento.getListaPagamentoItem().size());
		Double restante = 0.0;
		Double primeira = 0.0;
		
		if (parcela*prazopagamento.getListaPagamentoItem().size() != total) {
			restante = total - parcela*prazopagamento.getListaPagamentoItem().size();
		}			
		
		int parcelas = 1;
		for (Prazopagamentoitem item : prazopagamento.getListaPagamentoItem()) {
			if (entregapagamento == null) {
				primeira = parcela + restante;
				entregapagamento = new Entregapagamento();
				entregapagamento.setValor(new Money(primeira));
			} else {
				entregapagamento = new Entregapagamento();
				entregapagamento.setValor(new Money(parcela));
			}
			
			
			ultimaData.add(Calendar.DAY_OF_MONTH, item.getDias() != null ? item.getDias() : 0);
			ultimaData.add(Calendar.MONTH, item.getMeses() != null ? item.getMeses() : 0);
			
			entregapagamento.setDtemissao(new Date(System.currentTimeMillis()));
			entregapagamento.setDtvencimento(new Date(ultimaData.getTimeInMillis()));
			entregapagamento.setParcela(new Integer(parcelas++));
			
			listaPagamento.add(entregapagamento);
			ultimaData.setTimeInMillis(entregadocumento.getDtemissao().getTime());
		}
		return listaPagamento;
	}
	
	/**
	 * Seta a lista de material de uma entrega a partir de uma ordem de compra.
	 * 
	 * @see #getQtdeEntregueDoMaterialDaOrdemCompra(Ordemcompra, Material)
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Set<Entregamaterial> setaListaMaterial(Ordemcompra ordemcompra, boolean agrupamento) {
		List<Ordemcompramaterial> lista = ordemcompramaterialService.findByOrdemcompraWherein(ordemcompra.getWhereIn());
		if(ordemcompra.getReceberEntregaByMaterialmestre() != null && ordemcompra.getReceberEntregaByMaterialmestre()){
			lista = ordemcompraService.createListaByGrade(lista);
		}
		
		Set<Entregamaterial> listaMaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
		Entregamaterial entregamaterial = null;
		
		Integer controle = 0;
		for (Ordemcompramaterial ordemcompramaterial : lista) {
			Double qtdPedida = getQtdPedidaRestanteOCM(ordemcompramaterial);
			
			if(qtdPedida <= 0 && ordemcompra.getImportacaoXmlNfeBean() == null){
				continue;
			}
			
			entregamaterial = new Entregamaterial();
			entradafiscalService.setIdentificadorinterno(entregamaterial, controle);
			controle++;
			
			if(ordemcompramaterial.getOrigemMaterialmestregrade() != null && ordemcompramaterial.getOrigemMaterialmestregrade()){
				entregamaterial.setOrigemMaterialmestregrade(ordemcompramaterial.getOrigemMaterialmestregrade());
				entregamaterial.setWhereInOrdemcompramaterial(ordemcompramaterial.getWhereInOrdemcompramaterialItemGrade());
				entregamaterial.setWhereInMaterialItenGrade(ordemcompramaterial.getWhereInMaterialItenGrade());
				entregamaterial.setNomematerialOrdemcompramaterial(ordemcompramaterial.getNomematerialOrdemcompramaterial());
				entregamaterial.setListaOrdemcompramaterialByMaterialmestregrade(ordemcompramaterial.getListaOrdemcompramaterialByMaterialmestregrade());
			}			
			
			entregamaterial.setMaterial(ordemcompramaterial.getMaterial());
			entregamaterial.setQtde(qtdPedida);
			entregamaterial.setValorunitario(ordemcompramaterial.getValor() / (ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia()));
			entregamaterial.setValortotal(ordemcompramaterial.getValor());
			entregamaterial.setValoroutrasdespesas(ordemcompramaterial.getValoroutrasdespesas());
			entregamaterial.setOcm(ordemcompramaterial);
			entregamaterial.setWhereInOrdemcompra(ordemcompramaterial.getOrdemcompra().getCdordemcompra().toString());
			entregamaterial.setOrdemcompra(ordemcompramaterial.getOrdemcompra());
			entregamaterial.setLocalarmazenagem(ordemcompramaterial.getLocalarmazenagem());
			entregamaterial.setProjeto(ordemcompramaterial.getProjeto());
			entregamaterial.setCentrocusto(ordemcompramaterial.getCentrocusto());
			if(ordemcompramaterial.getIpi() != null)
				entregamaterial.setValoripi(new Money(ordemcompramaterial.getIpi()));
			if(ordemcompramaterial.getUnidademedida() != null){
				entregamaterial.setUnidademedidacomercial(ordemcompramaterial.getUnidademedida());
			}
			entregamaterial.setSequenciaitem(controle);
			
			listaMaterial.add(entregamaterial);
		}
		
		listaMaterial = this.agrupaEntregamaterialByMaterial(listaMaterial, agrupamento);
		
		this.addInfSolicitacaocompra(ordemcompra.getWhereIn(), listaMaterial);
		
		return listaMaterial;
	}
	
	public Double getQtdPedidaRestanteOCM(Ordemcompramaterial ordemcompramaterial){
		Double qtdPedida;
		if(ordemcompramaterial.getQtdefrequencia().equals(1.0) && ordemcompramaterial.getCdordemcompramaterial() != null){
			Double quantidadeEntregue = entregamaterialService.getQtdEntregueDoMaterialNaOrdemCompra(ordemcompramaterial);
			if(ordemcompramaterial.getUnidademedida() != null && ordemcompramaterial.getMaterial() != null && 
					ordemcompramaterial.getMaterial().getUnidademedida() != null && 
					!ordemcompramaterial.getMaterial().getUnidademedida().equals(ordemcompramaterial.getUnidademedida())){
				Double fracao = unidademedidaService.getFatorconversao(ordemcompramaterial.getMaterial().getUnidademedida(), quantidadeEntregue, ordemcompramaterial.getUnidademedida(), ordemcompramaterial.getMaterial(), null, 1d);
				if(fracao != null){
					quantidadeEntregue = quantidadeEntregue * fracao;
				}
			}
			qtdPedida = SinedUtil.roundByParametro(ordemcompramaterial.getQtdpedida() - (quantidadeEntregue != null ? quantidadeEntregue : new Double(0.0)));
		} else {
			qtdPedida = ordemcompramaterial.getQtdpedida();
		}
		return qtdPedida;
	}
	
	/**
	 * M�todo que adiciona a informa��o de faturamento no cliente na entrega
	 * @param whereInOrdemcompra
	 * @param listaMaterial
     * @autor Luiz Fernando
	 */
	private void addInfSolicitacaocompra(String whereInOrdemcompra, Set<Entregamaterial> listaMaterial) {
		if(listaMaterial != null && !listaMaterial.isEmpty()){
			List<Ordemcompra> lista = ordemcompraService.findWithOrigem(whereInOrdemcompra);
			
			if(lista != null && !lista.isEmpty()){
				Boolean faturamentocliente = null;
				boolean achou = false;
				for(Entregamaterial entregamaterial : listaMaterial){
					if(entregamaterial.getMaterial() != null){
						faturamentocliente = null;
						for(Ordemcompra ordemcompra : lista){
							if(ordemcompra.getCotacao() != null){
								if(ordemcompra.getCotacao().getListaOrigem() != null && !ordemcompra.getCotacao().getListaOrigem().isEmpty()){
									for(Cotacaoorigem cotacaoorigem : ordemcompra.getCotacao().getListaOrigem()){
										if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getMaterial() != null &&
												cotacaoorigem.getSolicitacaocompra().getMaterial().equals(entregamaterial.getMaterial())){
											faturamentocliente = cotacaoorigem.getSolicitacaocompra().getFaturamentocliente();
											achou = true;
											break;
										}
									}
								}
							}else if(ordemcompra.getListaOrdemcompraorigem() != null && !ordemcompra.getListaOrdemcompraorigem().isEmpty()){
								for(Ordemcompraorigem ordemcompraorigem : ordemcompra.getListaOrdemcompraorigem()){
									if(ordemcompraorigem.getSolicitacaocompra() != null && ordemcompraorigem.getSolicitacaocompra().getMaterial() != null &&
											ordemcompraorigem.getSolicitacaocompra().getMaterial().equals(entregamaterial.getMaterial())){
										faturamentocliente = ordemcompraorigem.getSolicitacaocompra().getFaturamentocliente();
										achou = true;
										break;
									}
								}
							}
						}
						if(achou){
							entregamaterial.setFaturamentocliente(faturamentocliente);
						}
					}
				}
			}
		}		
	}
	
	/**
	 * Agrupa os itens da entrega a partir do material.
	 * E cria a lista de Ordemcompraentregamaterial.
	 *
	 * @param listaMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/11/2012
	 */
	private Set<Entregamaterial> agrupaEntregamaterialByMaterial(Set<Entregamaterial> listaMaterial, boolean agrupamento) {
		Set<Entregamaterial> listaMaterialAgrupada = new ListSet<Entregamaterial>(Entregamaterial.class);
		
		for (Entregamaterial em : listaMaterial) {
			if(em.getOrigemMaterialmestregrade() != null && em.getOrigemMaterialmestregrade() && 
					em.getListaOrdemcompramaterialByMaterialmestregrade() != null && 
					!em.getListaOrdemcompramaterialByMaterialmestregrade().isEmpty()){
				Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
				for(Ordemcompramaterial ocm : em.getListaOrdemcompramaterialByMaterialmestregrade()){
					Ordemcompraentregamaterial ordemcompraentregamaterial = new Ordemcompraentregamaterial();
					ordemcompraentregamaterial.setOrdemcompramaterial(ocm);
					ordemcompraentregamaterial.setQtde(ocm.getQtdpedida());
					listaOrdemcompraentregamaterial.add(ordemcompraentregamaterial);
				}
				em.setListaOrdemcompraentregamaterial(listaOrdemcompraentregamaterial);
				em.setWhereInOrdemcompramaterial(em.getWhereInOrdemcompramaterial());
				if(em.getOrdemcompra() != null && em.getOrdemcompra().getCdordemcompra() != null){
					em.setWhereInOrdemcompra(em.getOrdemcompra().getCdordemcompra().toString());
				}
				listaMaterialAgrupada.add(em);
			}else {
				boolean achouMaterial = false;
				
				Ordemcompraentregamaterial ordemcompraentregamaterial = new Ordemcompraentregamaterial();
				ordemcompraentregamaterial.setOrdemcompramaterial(em.getOcm());
				ordemcompraentregamaterial.setQtde(em.getQtde());
				
				if(agrupamento){
					for (Entregamaterial em2 : listaMaterialAgrupada) {
						if(em2.getMaterial().equals(em.getMaterial()) && em2.getValorunitario().equals(em.getValorunitario())){
							achouMaterial = true;
							
							em2.setQtde(em2.getQtde() + em.getQtde());
							em2.setWhereInOrdemcompramaterial(em2.getWhereInOrdemcompramaterial() + "," + em.getOcm().getCdordemcompramaterial());
							em2.setNomematerialOrdemcompramaterial(em.getMaterial().getCdmaterial() + " - " + em.getMaterial().getNome());
							em2.getListaOrdemcompraentregamaterial().add(ordemcompraentregamaterial);
							
							if(em.getOrdemcompra() != null && em.getOrdemcompra().getCdordemcompra() != null){
								if(org.apache.commons.lang.StringUtils.isNotEmpty(em2.getWhereInOrdemcompra())){
									boolean existeOC = false;
									for(String idOC : em2.getWhereInOrdemcompra().split(",")){
										if(idOC.equals(em.getOrdemcompra().getCdordemcompra().toString())){
											existeOC = true;
											break;
										}
									}
									if(!existeOC){
										em2.setWhereInOrdemcompra(em2.getWhereInOrdemcompra() + "," + em.getOrdemcompra().getCdordemcompra());
									}
								}else {
									em2.setWhereInOrdemcompra(em.getOrdemcompra().getCdordemcompra().toString());
								}
							}
							
							break;
						}
					}
				}
					
				if(!achouMaterial){
					Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
					listaOrdemcompraentregamaterial.add(ordemcompraentregamaterial);
					
					em.setListaOrdemcompraentregamaterial(listaOrdemcompraentregamaterial);
					em.setWhereInOrdemcompramaterial(em.getOcm().getCdordemcompramaterial() + "");
					em.setNomematerialOrdemcompramaterial(em.getOcm().getMaterial().getCdmaterial() + " - " + em.getOcm().getMaterial().getNome());
					if(em.getOrdemcompra() != null && em.getOrdemcompra().getCdordemcompra() != null){
						em.setWhereInOrdemcompra(em.getOrdemcompra().getCdordemcompra().toString());
					}
					
					listaMaterialAgrupada.add(em);
				}
			}
		}
		
		for (Entregamaterial entregamaterial : listaMaterialAgrupada) {
			entregamaterial.setNomematerialOrdemcompramaterial(entregamaterial.getNomematerialOrdemcompramaterial().replaceAll("'", "\\\\'").replaceAll("\"", ""));
		}
		
		return listaMaterialAgrupada;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQtdeEntregueDoMaterialDaOrdemCompra(Ordemcompra ordemcompra, Material material) {
		return entregaDAO.getQtdeEntregueDoMaterialDaOrdemCompra(ordemcompra, material);
	}
	
	public void setaValoresEntregadocumento(Ordemcompra ordemcompra, Entregadocumento entregadocumento) {
		entregadocumento.setValordesconto(ordemcompra.getDesconto());
		entregadocumento.setDtemissao(SinedDateUtils.currentDate());
		entregadocumento.setFornecedor(fornecedorService.load(ordemcompra.getFornecedor()));
		entregadocumento.setValorfrete(ordemcompra.getFrete());
		entregadocumento.setPrazopagamento(ordemcompra.getPrazopagamento());
		entregadocumento.setValor(ordemcompra.getValorprodutos());
		entregadocumento.setDocumentotipo(ordemcompra.getDocumentotipo());
		entregadocumento.setEmpresa(ordemcompra.getEmpresa());
		entregadocumento.setValoroutrasdespesas(ordemcompra.getValorTotalOutrasDespesas());
		entregadocumento.setSimplesremessa(ordemcompra.getFaturamentocliente());
		entregadocumento.setIsReceberEntrega(true);
		
		if(ordemcompra.getWhereIn() != null && !ordemcompra.getWhereIn().equals("")){
			List<Rateio> listaRateio = rateioService.findByOrdemcompra(ordemcompra.getWhereIn());
			Money valortotalrateio = new Money();
			List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
			for (Rateio r : listaRateio) {
				if(r.getListaRateioitem() != null){
					listaRateioitem.addAll(r.getListaRateioitem());
					for(Rateioitem ri : r.getListaRateioitem()){
						if(ri.getValor() != null){
							valortotalrateio = valortotalrateio.add(ri.getValor());
						}
					}
				}
			}
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaRateioitem);
			rateioService.limpaReferenciaRateio(rateio);
			if(valortotalrateio.getValue().doubleValue() > 0)
				rateioService.rateioItensPercentual(rateio, valortotalrateio);
			entregadocumento.setRateio(rateio);
			
		} else if(ordemcompra.getRateio() != null && ordemcompra.getRateio().getCdrateio() != null){
			entregadocumento.setRateio(rateioService.findRateio(ordemcompra.getRateio()));
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForBaixa
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findForBaixa(String whereIn) {
		List<Entrega> lista = entregaDAO.findForBaixa(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Entrega entrega : lista){
				if(entrega != null && entrega.getCdentrega() != null){
					if(SinedUtil.isListNotEmpty(entrega.getListaEntregadocumento())){
						for(Entregadocumento ed : entrega.getListaEntregadocumento()){
							if(ed.getCdentregadocumento() != null){
								entradafiscalService.carregaListaEntregamateriallote(ed);
							}
						}
					}
				}
			}
		}
		return lista;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#updateBaixa
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateBaixa(String whereIn) {
		entregaDAO.updateBaixa(whereIn);
		
		this.callProcedureAtualizaEntrega(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#updateDtBaixa(Date dtbaixa, String whereIn)
	 *
	 * @param dtbaixa
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateDtBaixa(Date dtbaixa, String whereIn) {
		entregaDAO.updateDtBaixa(dtbaixa, whereIn);
		
		this.callProcedureAtualizaEntrega(whereIn);
	}
	
	public void verificaUnidademedidaMaterialDiferenteByEntrega(Movimentacaoestoque movimentacaoestoque, Entregamaterial entregamaterial) {
		if(movimentacaoestoque != null && movimentacaoestoque.getQtde() != null && entregamaterial != null && entregamaterial.getMaterial() != null && 
				entregamaterial.getMaterial().getCdmaterial() != null){
			Material material = materialService.unidadeMedidaMaterial(entregamaterial.getMaterial());
			Unidademedida un = entregamaterial.getUnidademedidacomercial();
			
			if(un != null && material != null && material.getUnidademedida() != null && un.getCdunidademedida() != null && 
					material.getUnidademedida().getCdunidademedida() != null && 
					un.getCdunidademedida() != material.getUnidademedida().getCdunidademedida()){
				if(movimentacaoestoque.getValor() != null){
					Double fracao = unidademedidaService.getFracaoconversaoUnidademedida(material.getUnidademedida(), movimentacaoestoque.getQtde(), un, material, null);
					if(fracao != null){
						movimentacaoestoque.setValor(movimentacaoestoque.getValor()*fracao);
					}
				}
			}
		}
	}
	
	/**
	 * Lan�a uma movimenta��o no estoque.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#saveOrUpdate(Movimentacaoestoque)
	 * @param movimentacaoestoqueorigem
	 * @param item
	 * @author Rodrigo Freitas
	 */
	public void lancarEstoque(Movimentacaoestoqueorigem movimentacaoestoqueorigem, BaixarEntregaItemBean item) {
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();		
		Double qtde = 0.0;
		
		if("DTRECEBIMENTO".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.RECEBIMENTO_MOVIMENTACAO_DATA))){
			movimentacaoestoque.setDtmovimentacao(item.getEntrega().getDtentrega());
		}else{
			movimentacaoestoque.setDtmovimentacao(new Date(System.currentTimeMillis()));
		}
		
		movimentacaoestoque.setEmpresa(item.getEmpresa());
		movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
		movimentacaoestoque.setLoteestoque(item.getLoteestoque());
		movimentacaoestoque.setMaterial(item.getMaterial());
		movimentacaoestoque.setMaterialclasse(item.getMaterialclasse());
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
		movimentacaoestoque.setQtde(item.getQuantidade());
		movimentacaoestoque.setComprimento(item.getComprimento());
//		movimentacaoestoque.setValor(item.getValorunitario() * item.getQuantidade());
		movimentacaoestoque.setValor(SinedUtil.roundByParametro(item.getValorunitario()));
		movimentacaoestoque.setProjeto(item.getProjeto());
				
		if(movimentacaoestoqueorigem.getEntrega() != null){
			List<Entregamaterial> listaEntregamaterial = null;
			if(item.getMaterialmestregrade() != null){
				listaEntregamaterial = entregamaterialService.findForLancarEstoqueConverterUnidademedida(movimentacaoestoqueorigem.getEntrega(), item.getMaterialmestregrade());
			}else {
				listaEntregamaterial = entregamaterialService.findForLancarEstoqueConverterUnidademedida(movimentacaoestoqueorigem.getEntrega(), item.getMaterial());
			}
			if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
				for(Entregamaterial entregamaterial : listaEntregamaterial){	
					if((item.getMaterialmestregrade() != null && entregamaterial.getMaterial().equals(item.getMaterialmestregrade())) || 
							(entregamaterial.getMaterial().equals(movimentacaoestoque.getMaterial()) &&
									(listaEntregamaterial.size() == 1 || 
									 item.getMaterialmestregrade() != null ||
									 item.getEntregamaterial() == null ||
									 item.getEntregamaterial().getCdentregamaterial() == null ||
									 (entregamaterial.getCdentregamaterial() != null &&
										entregamaterial.getCdentregamaterial().equals(item.getEntregamaterial().getCdentregamaterial()))))){
						Double qtdeMestre = null;
						if(entregamaterial.getUnidademedidacomercial() != null && entregamaterial.getMaterial() != null && 
							entregamaterial.getMaterial().getUnidademedida() != null &&
							!entregamaterial.getMaterial().getUnidademedida().equals(entregamaterial.getUnidademedidacomercial())){
							qtde = unidademedidaService.converteQtdeUnidademedida(entregamaterial.getMaterial().getUnidademedida(), 
												item.getQuantidade(),
												entregamaterial.getUnidademedidacomercial(),
												entregamaterial.getMaterial(),null,1.0);
							if(qtde  != null && qtde > 0){
								this.verificaUnidademedidaMaterialDiferenteByEntrega(movimentacaoestoque, entregamaterial);
								movimentacaoestoque.setQtde(qtde);
								if(movimentacaoestoque.getValor() != null){
									Double fracao = unidademedidaService.getFracaoconversaoUnidademedida(entregamaterial.getMaterial().getUnidademedida(), movimentacaoestoque.getQtde(), entregamaterial.getUnidademedidacomercial(), entregamaterial.getMaterial(), null);
									if(fracao != null){
										movimentacaoestoque.setValor(item.getValorunitario()*fracao);
									}
								}
							}
							if(item.getMaterialmestregrade() != null && entregamaterial.getMaterial().equals(item.getMaterialmestregrade())){
								qtdeMestre = unidademedidaService.converteQtdeUnidademedida(entregamaterial.getMaterial().getUnidademedida(), 
													entregamaterial.getQtde(),
													entregamaterial.getUnidademedidacomercial(),
													entregamaterial.getMaterial(),null,1.0);
							}
						}else {
							if(item.getMaterialmestregrade() != null && entregamaterial.getMaterial().equals(item.getMaterialmestregrade())){
								qtdeMestre = entregamaterial.getQtde();
							}
						}
						
						if(movimentacaoestoque.getValor() != null){
							if(qtdeMestre != null && qtdeMestre > 0){
								movimentacaoestoque.setValor(movimentacaoestoqueService.getValorcustoEntrada(movimentacaoestoque.getValor(), qtdeMestre, entregamaterial));
							}else if(entregamaterial.getListaEntregamateriallote() != null &&  entregamaterial.getListaEntregamateriallote().size() > 1){
								movimentacaoestoque.setValor(movimentacaoestoqueService.getValorcustoEntrada(movimentacaoestoque.getValor(), entregamaterial.getQtde(), entregamaterial));
							} else {
								movimentacaoestoque.setValor(movimentacaoestoqueService.getValorcustoEntrada(movimentacaoestoque.getValor(), movimentacaoestoque.getQtde(), entregamaterial));
							}
						}
					}
				}
			}
		}	
		
		Material aux_material = materialService.load(movimentacaoestoque.getMaterial(), "material.cdmaterial, material.unidademedida, material.materialgrupo");
		if(aux_material != null){
			if(aux_material.getUnidademedida() != null){
				movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(movimentacaoestoque.getQtde(), aux_material.getUnidademedida()));
			}
			
			if(aux_material.getMaterialgrupo()!=null && Boolean.TRUE.equals(aux_material.getMaterialgrupo().getAnimal())){
				movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.COMPRA);
			}
		}
		
		if(item.getMaterialOrigem() != null){
			movimentacaoestoque.setMaterialOrigem(item.getMaterialOrigem());
		}
		
		movimentacaoestoque.setPesomedio(item.getEntregamaterial() != null? item.getEntregamaterial().getPesomedio(): null);
		
		movimentacaoestoque.setValor(SinedUtil.roundByParametro(movimentacaoestoque.getValor()));
		
		if(movimentacaoestoque.getQtde() != null && movimentacaoestoque.getQtde() > 0){
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, item.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.ENTRADA_MERCADORIA, item.getCentrocusto(), item.getProjeto(), item.getCfop()));
			}
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
			movimentacaoestoqueService.salvarHisotirico(movimentacaoestoque, "Criado a partir da entrega " 
					+ (item.getEntrega().getCdentrega() != null ? SinedUtil.makeLinkHistorico(item.getEntrega().getCdentrega().toString(), "visualizarEntrega") + ".": "."), MovimentacaoEstoqueAcao.CRIAR);
		}
	}
	
	/**
	 * Lan�ar uma movimenta��o no estoque de um EPI.
	 *
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setarInformacoesEpiProduto
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#saveOrUpdate(Movimentacaoestoque)
	 * @param item
	 * @author Rodrigo Freitas
	 */
	public void lancarEpi(BaixarEntregaItemBean item) {
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setEntrega(item.getEntrega());	
		movimentacaoestoqueorigem.setEntregamaterial(item.getEntregamaterial());
		
		Movimentacaoestoque movimentacaoestoque = this.setarInformacoesEpiProduto(movimentacaoestoqueorigem, item);
		
		movimentacaoestoque.setQtde(item.getQtdepi());
		movimentacaoestoque.setComprimento(item.getComprimento());
		movimentacaoestoque.setValor(SinedUtil.roundByParametro(item.getValorunitario()));
		movimentacaoestoque.setMaterialclasse(Materialclasse.EPI);
		movimentacaoestoque.setProjeto(item.getProjeto());
		movimentacaoestoque.setLoteestoque(item.getLoteestoque());
		
		Material aux_material = materialService.load(movimentacaoestoque.getMaterial(), "material.cdmaterial, material.unidademedida");
		if(aux_material != null && aux_material.getUnidademedida() != null){
			movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(movimentacaoestoque.getQtde(), aux_material.getUnidademedida()));
		}
		
		if(item.getQtdepi() > 0){
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
		}
	}
	
	/**
	 * Lan�ar uma movimenta��o no estoque de um Produto.
	 *	 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setarInformacoesEpiProduto
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#saveOrUpdate(Movimentacaoestoque)
	 * @param item
	 * @author Rodrigo Freitas
	 */
	public void lancarProduto(BaixarEntregaItemBean item) {
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setEntrega(item.getEntrega());
		movimentacaoestoqueorigem.setEntregamaterial(item.getEntregamaterial());
		
		Movimentacaoestoque movimentacaoestoque = this.setarInformacoesEpiProduto(movimentacaoestoqueorigem, item);
		
		movimentacaoestoque.setQtde(item.getQtdproduto());
		movimentacaoestoque.setComprimento(item.getComprimento());
		movimentacaoestoque.setValor(SinedUtil.roundByParametro(item.getValorunitario()));
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
		movimentacaoestoque.setLoteestoque(item.getLoteestoque());
		movimentacaoestoque.setProjeto(item.getProjeto());
		movimentacaoestoque.setPesomedio(item.getEntregamaterial() != null? item.getEntregamaterial().getPesomedio(): null);
		
		Material aux_material = materialService.load(movimentacaoestoque.getMaterial(), "material.cdmaterial, material.unidademedida, material.materialgrupo");
		if(aux_material != null){
			if(aux_material.getUnidademedida() != null){
				movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(movimentacaoestoque.getQtde(), aux_material.getUnidademedida()));
			}
			if(aux_material.getMaterialgrupo()!=null && Boolean.TRUE.equals(aux_material.getMaterialgrupo().getAnimal())){
				movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.COMPRA);
			}
		}
		
		if(movimentacaoestoque.getQtde() > 0){
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, item.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.ENTRADA_MERCADORIA, item.getCentrocusto(), item.getProjeto(), item.getCfop()));
			}
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
			movimentacaoestoqueService.salvarHisotirico(movimentacaoestoque, "Criado a partir da entrega " 
					+ (item.getEntrega().getCdentrega() != null ? SinedUtil.makeLinkHistorico(item.getEntrega().getCdentrega().toString(), "visualizarEntrega") + ".": "."), MovimentacaoEstoqueAcao.CRIAR);
		}		
	}
	
	/**
	 * Setar as informa��es do epi e do produto.
	 *
	 * @param movimentacaoestoqueorigem
	 * @param item
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movimentacaoestoque setarInformacoesEpiProduto(Movimentacaoestoqueorigem movimentacaoestoqueorigem, BaixarEntregaItemBean item) {
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		
		movimentacaoestoque.setDtmovimentacao(new Date(System.currentTimeMillis()));
		movimentacaoestoque.setEmpresa(item.getEmpresa());
		movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
		movimentacaoestoque.setMaterial(item.getMaterial());
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
		movimentacaoestoque.setProjeto(item.getProjeto());
		movimentacaoestoque.setLoteestoque(item.getLoteestoque());
		
		return movimentacaoestoque;
	}
	
	/**
	 * Criar os itens de patrim�nio e lan�ar a movimenta��o deste patrim�nio.
	 *
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#saveOrUpdate(Patrimonioitem)
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioorigemService#saveOrUpdate(Movpatrimonioorigem)
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#saveOrUpdate(Movpatrimonio)
	 * @param listaPatrimonio
	 * @param item
	 * @author Rodrigo Freitas
	 */
	public void lancarPatrimonio(List<Patrimonioitem> listaPatrimonio, BaixarEntregaItemBean item) {
		
		Patrimonioitem patrimonioitem = new Patrimonioitem();
		patrimonioitem.setEmpresa(item.getEmpresa());
		patrimonioitem.setBempatrimonio(new Bempatrimonio(item.getMaterial().getCdmaterial()));
		patrimonioitem.setAtivo(true);
		if(item.getEntregadocumento() != null && item.getEntregadocumento().getDtentrada() != null){
			patrimonioitem.setDtaquisicao(item.getEntregadocumento().getDtentrada());
		}else if(item.getEntrega() != null && item.getEntrega().getDtentrega() != null){
			patrimonioitem.setDtaquisicao(item.getEntrega().getDtentrega());
		}
		
		if (bempatrimonioService.verificaBemPatrimonio(patrimonioitem.getBempatrimonio())){
			throw new SinedException("N�o existe Bem Patrim�nio para fazer a baixa");
		}
		
		patrimonioitemService.saveOrUpdate(patrimonioitem);
		
		Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
		movpatrimonioorigem.setEntrega(item.getEntrega());
		
		movpatrimonioorigemService.saveOrUpdate(movpatrimonioorigem);
		
		Movpatrimonio movpatrimonio = new Movpatrimonio();
		movpatrimonio.setFornecedororigem(item.getFornecedor());
		movpatrimonio.setPatrimonioitem(patrimonioitem);
		movpatrimonio.setDtmovimentacao(new Timestamp(System.currentTimeMillis()));
		movpatrimonio.setLocalarmazenagem(item.getLocalarmazenagem());
		movpatrimonio.setMovpatrimonioorigem(movpatrimonioorigem);
		movpatrimonio.setDepartamento(item.getDepartamento());
		movpatrimonio.setColaborador(item.getColaborador());
		movpatrimonio.setEmpresa(item.getEmpresa());
		
		movpatrimonioService.saveOrUpdate(movpatrimonio);					
		
		listaPatrimonio.add(patrimonioitem);
	}
	
	/**
	 * M�todo que faz o c�lculo das quantidades do material para ser baixado de cada classe.
	 *
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#loadForBaixaEntrega
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadWithLista
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findByCotacaoForBaixaEntrega
	 * @param listaPendentes
	 * @param listaEncontrados
	 * @param itemBean
	 * @param entrega
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void calculoQuantidades(List<BaixarEntregaItemBean> listaPendentes, List<BaixarEntregaItemBean> listaEncontrados,
			BaixarEntregaItemBean itemBean, Entrega entrega, Entregamaterial material) {
		
		if (material.getMaterialclasse() != null) {
			
//			SE O USU�RIO ENCONTRAR A CLASSE DO MATERIAL CADASTRADO NA PR�PRIA ENTREGA
			itemBean.setMaterialclasse(material.getMaterialclasse());
			listaEncontrados.add(itemBean);
			
		} else {
			
//			A MEDIDA QUE N�O FOR ENCONTRANDO A CLASSE DO MATERIAL VAI FAZENDO QUERY, 
//			POIS A QUERY N�O FOI POSS�VEL EM UMA S�
			
//		   ENCONTRA A ORDEM DE COMPRA DA ENTREGA
			Ordemcompra ordemcompra = ordemcompraService.loadForBaixaEntrega(entrega,material.getMaterial());
			
			if (ordemcompra != null && ordemcompra.getCotacao() != null) {
				
//				CARREGA A COTA��O COM SUAS LISTA PARA O C�LCULO
				for(Entregadocumento ed : entrega.getListaEntregadocumento()){
					Cotacao cotacao = cotacaoService.loadWithLista(ordemcompra.getCotacao(), ed.getFornecedor(), entrega.getLocalarmazenagem(), material.getMaterial());
					
					for (Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()) {
						
//					S� SE PODE ENTRAR NO LOOP DOS MATERIAIS SE O FORNECEDOR FOR IGUAL A DA ENTREGA
						if (cf.getFornecedor().getCdpessoa().equals(ed.getFornecedor().getCdpessoa())) {
							
							for (Cotacaofornecedoritem cfi : cf.getListaCotacaofornecedoritem()) {
								
								if (cfi.getMaterial().getCdmaterial().equals(material.getMaterial().getCdmaterial()) && 
										cfi.getLocalarmazenagem().getCdlocalarmazenagem().equals(entrega.getLocalarmazenagem().getCdlocalarmazenagem())) {
									
									if (cfi.getQtdecot().equals(cfi.getQtdesol())) {
										
//									SE A QUANTIDADE COTADA FOR IGUAL A SOLICITADA ENCONTRAR AS SOLICITA��ES PARA PEGAR AS QUANTIDADES
										List<Solicitacaocompra> listaSolicitacao = solicitacaocompraService.findByCotacaoForBaixaEntrega(cotacao, material.getMaterial(), entrega.getLocalarmazenagem());
										
										List<BaixarEntregaItemBean> listaAdicionar = new ArrayList<BaixarEntregaItemBean>();
										Double soma = 0D;
										BaixarEntregaItemBean itemBeanInterno;
										
										for (Solicitacaocompra solicitacaocompra : listaSolicitacao) {
											soma += solicitacaocompra.getQtde();
											itemBeanInterno = new BaixarEntregaItemBean(itemBean);
											itemBeanInterno.setMaterialclasse(solicitacaocompra.getMaterialclasse());
											itemBeanInterno.setQuantidade(solicitacaocompra.getQtde());
											itemBeanInterno.setValorunitario(cfi.getValorunitario());
											listaAdicionar.add(itemBeanInterno);
										}
										
										if (soma.equals(material.getQtde())) {
											listaEncontrados.addAll(listaAdicionar);
										} else {
											listaPendentes.add(itemBean);
										}
										
									} else {
										listaPendentes.add(itemBean);
									}
								}
							}
						} else {
							listaPendentes.add(itemBean);
							break;
						}
					}					
				}
			} else {
				listaPendentes.add(itemBean);
			}
		}
	}
	
	/**
	 * Setar alguns atributos que s�o comuns em todas as classes.
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findClassesWithoutLoad
	 * @param itemBean
	 * @param entrega
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void setarItemDefault(BaixarEntregaItemBean itemBean, Entrega entrega, Entregamaterial material) {
		
		itemBean.setEntregamaterial(material);
		itemBean.setMaterial(material.getMaterial());
		List<Materialclasse> listaClasses = materialService.findClassesWithoutLoad(material.getMaterial());
		
		if (material.getMaterialclassetrans() == null){
			if (listaClasses != null && listaClasses.size() == 1) {
				itemBean.setMaterialclasse(listaClasses.get(0));
			}
		} else {
			itemBean.setMaterialclasse(material.getMaterialclasse());
		}
		
		itemBean.setLocalarmazenagem(material.getLocalarmazenagem() != null ? material.getLocalarmazenagem() : entrega.getLocalarmazenagem());
		itemBean.setQuantidade(material.getQtdeAux() != null ? material.getQtdeAux() : material.getQtde());
		itemBean.setComprimento(material.getComprimento());
		itemBean.setCfop(material.getCfop());
		if(material.getCfop() == null || material.getCfop().getCodigo() == null || 
				(!material.getCfop().getCodigo().equals("1910") && !material.getCfop().getCodigo().equals("2910") )){
			itemBean.setValorunitario(material.getValorunitario());
		}else {
			itemBean.setValorunitario(0d);
		}
		itemBean.setEmpresa(entrega.getEmpresa());
		itemBean.setFornecedor(getFornecedorEmComum(entrega));
		itemBean.setEntrega(entrega);
		itemBean.setProjeto(material.getProjeto());
		itemBean.setCentrocusto(material.getCentrocusto());
		itemBean.setLoteestoque(material.getLoteestoqueAux() != null ? material.getLoteestoqueAux() : material.getLoteestoque());
		itemBean.setUnidademedidacomercial(material.getUnidademedidacomercial());
	}
	
	public Fornecedor getFornecedorEmComum(Entrega e){
		Fornecedor fornecedor = null;
		if(e != null && e.getListaEntregadocumento() != null && !e.getListaEntregadocumento().isEmpty()){
			for(Entregadocumento ed : e.getListaEntregadocumento()){
				if(ed.getFornecedor() != null){
					if(fornecedor == null)
						fornecedor = ed.getFornecedor();
					else if(!ed.getFornecedor().equals(fornecedor)){
						return null;
					}
				}
			}
		}
		return fornecedor;
	}	
	
	@Override
	public void saveOrUpdate(final Entrega bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(bean.getListaEntregadocumento() != null && !bean.getListaEntregadocumento().isEmpty()){
					for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
						if(entregadocumento.getRateio() != null){
							Rateio rateio = entregadocumento.getRateio();
							if (rateio != null) {
								rateioService.limpaReferenciaRateio(rateio);
								rateioService.saveOrUpdateNoUseTransaction(rateio);
								entregadocumento.setRateio(rateio);
							}
						}
					}
				}
				if(bean.getListaSolicitacao() != null && bean.getListaSolicitacao().size() > 0)
					for (Solicitacaocompra solicitacaocompra : bean.getListaSolicitacao()) 
						solicitacaocompraService.doUpdateCompraSolicitacao(solicitacaocompra, Solicitacaocompra.ENTREGA);
				
				//Caso possua ordem de compra e seje a primeira vez que esta gravando, grava hist�rico para ordem de compra
				Set<Ordemcompraentrega> listaOrdemcompraentrega = bean.getListaOrdemcompraentrega();
				List<Ordemcomprahistorico> listaOrdemcomprahistorico = new ArrayList<Ordemcomprahistorico>();
				if(bean.getCdentrega() == null && bean.getHaveOrdemcompra()){
					for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
						listaOrdemcomprahistorico.add(new Ordemcomprahistorico(ordemcompraentrega.getOrdemcompra(), Ordemcompraacao.ENTEGA_RECEBIDA));
					}
				}
				
				boolean isFirstSave = bean.getCdentrega() == null ? true : false;
				
				List<Entregadocumento> listaEntregadocumento = bean.getListaEntregadocumento();
				if(bean.getFaturamentocliente() == null)
					bean.setFaturamentocliente(Boolean.FALSE);
				
				if(bean.getArquivoxml() != null && bean.getArquivoxml().getCdarquivo() != null){
					arquivoDAO.saveFile(bean, "arquivoxml");
				}
				
				saveOrUpdateNoUseTransaction(bean);				
				
				List<Entregamaterial> listaEntregamaterial = new ArrayList<Entregamaterial>();
				for(Entregadocumento entregadocumento : listaEntregadocumento){	
					entregadocumento.setEntrega(bean);
					if(entregadocumento.getEmpresa() == null)
						entregadocumento.setEmpresa(bean.getEmpresa());
					if(entregadocumento.getCdentregadocumento() == null){
						entregadocumento.setEntregadocumentosituacao(Entregadocumentosituacao.REGISTRADA);
					}
					if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getPessoaquestionariotrans() != null){
						pessoaquestionarioService.saveOrUpdateNoUseTransaction(entregadocumento.getFornecedor().getPessoaquestionariotrans());
					}
					
					// Adicionar a lista transiente, caso exista, � lista original para ser salva.
					if(entregadocumento.getListaEntregadocumentoreferenciadoTrans() != null){
						if(entregadocumento.getListaEntregadocumentoreferenciado() == null){
							entregadocumento.setListaEntregadocumentoreferenciado(entregadocumento.getListaEntregadocumentoreferenciadoTrans());
						}else{							
							entregadocumento.getListaEntregadocumentoreferenciado().addAll(entregadocumento.getListaEntregadocumentoreferenciadoTrans());
						}
					}
					
					entradafiscalService.saveOrUpdateNoUseTransaction(entregadocumento);
					if(isFirstSave){
						if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
							for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){ 
								entregamaterialService.saveEntregaMaterialMaisListaInspecao(entregamaterial);
								if(SinedUtil.isListNotEmpty(entregamaterial.getListaEntregamateriallote()) && entregamaterial.getCdentregamaterial() != null){
									for (Entregamateriallote eml : entregamaterial.getListaEntregamateriallote()) {
										eml.setEntregamaterial(entregamaterial);
										entregamaterialloteService.saveOrUpdateNoUseTransaction(eml);
									}
								}
								listaEntregamaterial.add(entregamaterial);
							}
						}
					} else{
						if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
							for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){ 
								entregamaterialService.saveOrUpdateNoUseTransaction(entregamaterial);
								listaEntregamaterial.add(entregamaterial);
							}
						}						
					}
					
					if(SinedUtil.isListNotEmpty(entregadocumento.getListaAjustefiscal())){
						for (Ajustefiscal ajustefiscal : entregadocumento.getListaAjustefiscal()){ 
							ajustefiscalService.saveOrUpdateNoUseTransaction(ajustefiscal);
						}
					}
					
					if (entregadocumento.getListaEntregadocumentofrete()!=null){
						for (Entregadocumentofrete entregadocumentofrete: entregadocumento.getListaEntregadocumentofrete()){
							entregadocumentofrete.setEntregadocumento(entregadocumento);
							entregadocumentofreteService.saveOrUpdateNoUseTransaction(entregadocumentofrete);
						}
					}
					if (entregadocumento.getListaEntregadocumentofreterateio()!=null){
						for (Entregadocumentofreterateio entregadocumentofreterateio: entregadocumento.getListaEntregadocumentofreterateio()){
							entregadocumentofreterateio.setEntregadocumento(entregadocumento);
							entregadocumentofreterateioService.saveOrUpdateNoUseTransaction(entregadocumentofreterateio);
						}
					}
				}
				if(isFirstSave){
					for(Entregadocumento entregadocumento : listaEntregadocumento){
						Entregadocumentohistorico edh = entradafiscalService.criaEntregadocumentohistoricoOrigemEntrega(entregadocumento, bean);
						if(edh != null){	
							entregadocumentohistoricoService.saveOrUpdateNoUseTransaction(edh);
						}					
					}
					
					if(listaOrdemcompraentrega != null && listaOrdemcompraentrega.size() > 0){
						for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
							ordemcompraentrega.setEntrega(bean);
							ordemcompraentregaService.saveOrUpdateNoUseTransaction(ordemcompraentrega);
						}
					}
				}
				
				// Feito so a primeira vez utilizando este primeiro save pois os outros saves iriam apagar a lista.
				if(listaOrdemcomprahistorico != null && listaOrdemcomprahistorico.size() > 0){
					for (Ordemcomprahistorico ordemcomprahistorico : listaOrdemcomprahistorico) {
						ordemcomprahistorico.setObservacao("Entrega <a href=\"#\" onclick=\"redirecionaEntrega("+bean.getCdentrega()+")\">"+bean.getCdentrega()+"</a>");
						ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(ordemcomprahistorico);
					}
				}
				
				if(isFirstSave){
					avisoService.salvarAvisos(montaAvisosEntrega(MotivoavisoEnum.FATURAR_ENTREGA, bean, "Aguardando faturamento"), true);
				}
				
				return status;
			}			
		});
		
		this.callProcedureAtualizaEntrega(bean);
	}
	
	/**
	 * Monta uma lista de avisos gerais
	 * 
	 * @param listaPapeis
	 * @param entrega
	 * @param assunto
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> montaAvisosEntrega(MotivoavisoEnum motivo, Entrega entrega, String assunto) {
		List<Aviso> avisos = new ListSet<Aviso>(Aviso.class);
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		if(motivoAviso != null){
			avisos.add(new Aviso(assunto, "Entrega: "+entrega.getCdentrega(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
					motivoAviso.getUsuario(), AvisoOrigem.ENTREGA, entrega.getCdentrega(), entrega.getEmpresa(), getWhereInProjeto(entrega), motivoAviso));
		}
		
		return avisos;
	}
	
	private String getWhereInProjeto(Entrega entrega) {
		StringBuilder whereInProjeto = new StringBuilder();
		if(entrega != null && SinedUtil.isListNotEmpty(entrega.getListaEntregadocumento())){
			for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
				if(entregadocumento.getRateio() != null && SinedUtil.isListNotEmpty(entregadocumento.getRateio().getListaRateioitem())){
					for(Rateioitem ri : entregadocumento.getRateio().getListaRateioitem()){
						if(ri.getProjeto() != null && ri.getProjeto().getCdprojeto() != null){
							whereInProjeto.append(ri.getProjeto().getCdprojeto().toString()).append(",");
						}
					}
				}
			}
		}
		return whereInProjeto.length() > 0 ? whereInProjeto.substring(0, whereInProjeto.length()-1) : null;
	}
	
	/**
	 * M�todo monta a origem dependendo do valor
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Entrega form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getCdentrega() != null){
			List<Ordemcompraentrega> listaOrdemcompraentrega = ordemcompraentregaService.findByEntrega(form);
			if(listaOrdemcompraentrega != null && listaOrdemcompraentrega.size() > 0){
				for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
					origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ORDEMCOMPRA, "ORDEM DE COMPRA", ordemcompraentrega.getOrdemcompra().getCdordemcompra().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}			
			}
		}
		return listaBean;
	}
	
	/**
	 * M�todo monta a destino dependendo do valor
	 * 
	 * @see #montaDestinoContaPagar(Entrega, List)
	 * @see #montaDestinoMovPatrimonio(Entrega, List)
	 * @see #montaDestinoMovimentacaoEstoque(Entrega, List)
	 * @see #montaDestinoRomaneio(Entrega, List)
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaDestino(Entrega form) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		montaDestinoContaPagar(form, listaBean);
		montaDestinoMovPatrimonio(form, listaBean);
		montaDestinoMovimentacaoEstoque(form, listaBean);
		montaDestinoRomaneio(form, listaBean);
		montaDestinoGeracaoplaqueta(form, listaBean);
		
		return listaBean;
	}
	
	/**
	 * M�todo que acrescenta destinos do romaneio
	 * 
	 * @see br.com.linkcom.sined.geral.service.RomaneioService#getListaRomaneioDaEntrega(Entrega)
	 * @param form
	 * @param listaBean
	 * @author Tom�s Rabelo
	 */
	private void montaDestinoRomaneio(Entrega form,	List<OrigemsuprimentosBean> listaBean) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<Romaneio> listaRomaneios = romaneioService.getListaRomaneioDaEntrega(form);
		
		if(listaRomaneios != null && listaRomaneios.size() > 0)
			for (Romaneio romaneio : listaRomaneios) {
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ROMANEIO, "ROMANEIO", romaneio.getCdromaneio().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
	}
	
	/**
	 * M�todo que acrescenta destinos da movpatrimonio
	 *
	 * @param form
	 * @param listaBean
	 * @author Luiz Fernando
	 */
	private void montaDestinoGeracaoplaqueta(Entrega form,	List<OrigemsuprimentosBean> listaBean) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<Patrimonioitem> listapaPatrimonioitem = patrimonioitemService.findByEntrega(form);
		
		if(listapaPatrimonioitem != null && !listapaPatrimonioitem.isEmpty())
			for (Patrimonioitem patrimonioitem : listapaPatrimonioitem) {
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PATRIMONIOITEM, "ITEM DE PATRIM�NIO", patrimonioitem.getCdpatrimonioitem().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
	}
	
	/**
	 * M�todo que acrescenta destinos da movimenta��o de estoque
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getListaEntradaDaEntrega(Entrega)
	 * @param form
	 * @param listaBean
	 * @author Tom�s Rabelo
	 */
	private void montaDestinoMovimentacaoEstoque(Entrega form, List<OrigemsuprimentosBean> listaBean) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<Movimentacaoestoque> listaProdEPIServico = movimentacaoestoqueService.getListaEntradaDaEntrega(form);
		
		if(listaProdEPIServico != null && listaProdEPIServico.size() > 0)
			for (Movimentacaoestoque movimentacaoestoque : listaProdEPIServico){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTRADASAIDA, "ENTRADA", movimentacaoestoque.getCdmovimentacaoestoque().toString(), null);
				origemsuprimentosBean.setProduto(movimentacaoestoque.getMaterial() != null ? movimentacaoestoque.getMaterial().getNome() : "");
				origemsuprimentosBean.setQtde(movimentacaoestoque.getQtde());
				if(movimentacaoestoque.getLoteestoque() != null){
					movimentacaoestoque.getLoteestoque().montaDescriptionAutocomplete(movimentacaoestoque.getMaterial());
					origemsuprimentosBean.setLote(movimentacaoestoque.getLoteestoque().getDescriptionAutocomplete());
				}
				
				listaBean.add(origemsuprimentosBean);
			}
	}
	
	/**
	 * M�todo que acrescenta destinos da movimenta��o de patrim�nio
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#getListaMovPatrimonioDaEntrega(Entrega)
	 * @param form
	 * @param listaBean
	 * @author Tom�s Rabelo
	 */
	private void montaDestinoMovPatrimonio(Entrega form, List<OrigemsuprimentosBean> listaBean) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<Movpatrimonio> listaPatrimonio = movpatrimonioService.getListaMovPatrimonioDaEntrega(form);
		
		if(listaPatrimonio != null && listaPatrimonio.size() > 0)
			for (Movpatrimonio movpatrimonio : listaPatrimonio){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.MOVPATRIMONIO, "MOVIMENTA��O PATRIM�NIO", movpatrimonio.getCdmovpatrimonio().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
	}
	
	/**
	 * M�todo que acrescenta destinos das contas a pagar geradas
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoorigemService#getListaDocumentosDaEntrega(Entrega) 
	 * @param form
	 * @param listaBean
	 * @author Tom�s Rabelo
	 */
	private void montaDestinoContaPagar(Entrega form, List<OrigemsuprimentosBean> listaBean) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		
		List<Documentoorigem> listaDocumento = documentoorigemService.getListaDocumentosDaEntrega(form);
		form.setListaDocumentoorigem(listaDocumento);
		
		if(form.getListaDocumentoorigem() != null && form.getListaDocumentoorigem().size() > 0)
			for (Documentoorigem documentoorigem : form.getListaDocumentoorigem()){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.DOCUMENTO, "CONTA A PAGAR", documentoorigem.getDocumento().getCddocumento().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
	}
	
	/**
	 * M�todo que gera parcelas para a entrega
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoitemService#findByPrazo(Prazopagamento)
	 * @param entregapagamento
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public List<Entregapagamento> geraParcelamento(Entregapagamento entregapagamento) {
		if(entregapagamento.getDtvencimento() == null) throw new SinedException("O par�metro dtvencimento n�o pode ser null");
		
		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(entregapagamento.getPrazo());
		List<Entregapagamento> listaDoc = new ArrayList<Entregapagamento>();
		Calendar ultimaData = Calendar.getInstance();
		Entregapagamento docTmp = null;
		Money valorPagamento = new Money(0);
		
		Double valorParcela = calculaValorParcela(entregapagamento, listaPrazoItem.size());
		
		Money parcelaArredondada = new Money(valorParcela).round();
		Money valorTotalParcelas = new Money(parcelaArredondada.multiply(new Money(listaPrazoItem.size())));
		
		int parcela = 1;
		for (int i = 0; i < entregapagamento.getRepeticoes(); i++) {
			/*
			 * Se a listaDoc for vazia, pega a dtVencimento de documento, sen�o,
			 * pega a dtVencimento do �ltimo item da lista.
			 */
			long millis = listaDoc.isEmpty() ? entregapagamento.getDtvencimento().getTime() : listaDoc.get(listaDoc.size()-1).getDtvencimento().getTime();
			Prazopagamentoitem prazoItem = null;
			int begin = 0;
			if (listaPrazoItem.size() > 1)
				begin = 0;
			
			for (int j = begin; j < listaPrazoItem.size(); j++) {
				prazoItem = listaPrazoItem.get(j);
				docTmp = new Entregapagamento();
				ultimaData.setTimeInMillis(millis);
				
				ultimaData.add(Calendar.DAY_OF_MONTH, prazoItem.getDias() != null ? prazoItem.getDias() : 0);
				ultimaData.add(Calendar.MONTH, prazoItem.getMeses() != null ? prazoItem.getMeses() : 0);
				
				docTmp.setParcela(parcela++);
				docTmp.setDtvencimento(new Date(ultimaData.getTimeInMillis()));
				docTmp.setNumero(entregapagamento.getNumero());
				docTmp.setValor(new Money(valorParcela));
				
				valorPagamento = valorPagamento.add(docTmp.getValor());
//				if(i == entregapagamento.getRepeticoes()-1){
//					docTmp.setValor(docTmp.getValor().add(entregapagamento.getValor().subtract(valorPagamento)));
//				}
				
				listaDoc.add(docTmp);
			}
			
			//ajusta a ultima parcela, caso a soma do valor das parcelas arredondadas para 2 casa decimais seja diferente do valor total
			if (valorTotalParcelas.toLong() != valorPagamento.toLong()){
				if(valorTotalParcelas.toLong() > valorPagamento.toLong()){
					Money diferenca = new Money (valorTotalParcelas.subtract(valorPagamento));
					parcelaArredondada = parcelaArredondada.subtract(diferenca);					
				}else{
					Money diferenca = new Money (valorPagamento.subtract(valorTotalParcelas));
					parcelaArredondada = parcelaArredondada.add(diferenca);
				}
				listaDoc.get(listaDoc.size() - 1).setValor(parcelaArredondada);
			}
			
		}
		return listaDoc;
	}
	private Double calculaValorParcela(Entregapagamento entregapagamento, Integer numParcelas){
		Double valorParcela = 0.0;
		Boolean possuiJuros = entregapagamento.getPrazo().getParcelasiguaisjuros();
		if( possuiJuros != null && possuiJuros && entregapagamento.getPrazo().getJuros() != null){
			Double juros = (entregapagamento.getPrazo().getJuros()/100)+1;
			Double Sn;
			if(numParcelas < 2){
				if(juros > 1.0){
					Sn = (Math.pow(juros,entregapagamento.getRepeticoes()) -1)/(juros-1);
				}
				else Sn = (double) entregapagamento.getRepeticoes();
				valorParcela = entregapagamento.getValor().getValue().doubleValue() * Math.pow(juros,entregapagamento.getRepeticoes()) / Sn;
				
			}
			else{
				if(juros > 1.0){
					Sn = (Math.pow(juros,numParcelas) -1)/(juros-1);
				}
				else Sn = (double) numParcelas;
				valorParcela = entregapagamento.getValor().getValue().doubleValue() * Math.pow(juros,numParcelas) / Sn;
			}
		}
		else{
			if(numParcelas < 2){
				valorParcela = entregapagamento.getValor().getValue().doubleValue() / entregapagamento.getRepeticoes();
			}
			else{
				valorParcela = entregapagamento.getValor().getValue().doubleValue() / numParcelas;
			}
		}
		return valorParcela;
	}
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#entregaNotBaixada
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean entregaNotBaixada(String whereIn) {
		return entregaDAO.entregaNotBaixada(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> findEntregasDaOrdemCompra(Entrega entrega, Ordemcompra ordemcompra) {
		return entregaDAO.findEntregasDaOrdemCompra(entrega, ordemcompra);
	}
	
	/**
	 * M�todo que cria as lista com os itens de inspe��o de cada material se houver. Caso n�o haja a entrega j� fica como  
	 * inspe��o completa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialinspecaoitemDAO#findItensDosMateriais(String)
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void preparaListaInspecao(Entrega bean) {
		if(bean != null && bean.getListaEntregadocumento() != null && !bean.getListaEntregadocumento().isEmpty()){
			for(Entregadocumento ed : bean.getListaEntregadocumento()){
				if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){	
					String whereIn = CollectionsUtil.listAndConcatenate(ed.getListaEntregamaterial(), "material.cdmaterial", ",");
					List<Materialinspecaoitem> lista = materialinspecaoitemService.findItensDosMateriais(whereIn);
					
					if(lista != null && lista.size() > 0){
						bean.setInspecaocompleta(Boolean.FALSE);
						for (Entregamaterial entregamaterial : ed.getListaEntregamaterial()) {
							List<Entregamaterialinspecao> listaInspecao = new ArrayList<Entregamaterialinspecao>();
							
							for (Materialinspecaoitem materialinspecaoitem : lista) 
								if(entregamaterial.getMaterial().getCdmaterial().equals(materialinspecaoitem.getMaterial().getCdmaterial())){
									
									Entregamaterialinspecao inspecao = new Entregamaterialinspecao(entregamaterial, materialinspecaoitem.getInspecaoitem());
									listaInspecao.add(inspecao);
								}
							
							if(listaInspecao != null && listaInspecao.size() > 0)
								entregamaterial.setListaInspecao(listaInspecao);
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cdentrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Entrega carregaEntregaParaInspecao(String cdentrega) {
		return entregaDAO.carregaEntregaParaInspecao(cdentrega);
	}
	
	/**
	 * M�todo que atualiza os itens da inspe��o do material e caso todos tenham sido preenchidos
	 * atualiza a entrega passando a ter a inspe��o completa.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregamaterialinspecaoService#saveOrUpdateNoUseTransaction(Entregamaterialinspecao)
	 * @see #doUpdateInspecaoEntrega(Entrega)
	 * @param entrega
	 * @author Tom�s Rabelo
	 */
	public void salvaInspecao(final Entrega entrega) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
					for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
						if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
							for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) 
								for (Entregamaterialinspecao inspecao : entregamaterial.getListaInspecao()) 
									inspecaoService.saveOrUpdateNoUseTransaction(inspecao);
						}
					}
					if(entrega.getInspecaocompleta())
						doUpdateInspecaoEntrega(entrega);
				}
				
				return status;
			}
		});
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param entrega
	 * @author Tom�s Rabelo
	 */
	protected void doUpdateInspecaoEntrega(Entrega entrega) {
		entregaDAO.doUpdateInspecaoEntrega(entrega);
	}
	
	/**
	 * M�todo que verifica se todos os radios foram preenchidos na tela
	 * 
	 * @param entrega
	 * @author Tom�s Rabelo
	 */
	public void verificaInspecaoCompleta(Entrega entrega) {
		boolean inspecaoCompleta = true;
		if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
			for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
				if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
					for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){ 
						for (Entregamaterialinspecao inspecao : entregamaterial.getListaInspecao()) 
							if(inspecao.getConforme() == null){
								inspecaoCompleta = false;
								break;
							}
						if(!inspecaoCompleta)
							break;
					}
				}
				if(!inspecaoCompleta)
					break;
			}			
		}
		entrega.setInspecaocompleta(inspecaoCompleta);
	}
	
	/**
	 * M�todo que gera relat�rio contendo informa��es das inspe��es das entregas
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#gerarRelatorioInspecaoEntrega(String)
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorioInspecaoEntrega(String whereIn) {
		Report report = new Report("/suprimento/inspecaoentrega");
		
		Report subreport = new Report("suprimento/sub_inspecaoentregadocumento");
		Report subreport1 = new Report("suprimento/sub_inspecaoentrega");
		Report subreport2 = new Report("suprimento/sub_inspecaoentrega1");
		
		report.addSubReport("SUB_INSPECAOENTREGADOCUMENTO", subreport);
		report.addSubReport("SUB_INSPECAOENTREGA", subreport1);
		report.addSubReport("SUB_INSPECAOENTREGA1", subreport2);
		
		List<Entrega> entregas = entregaDAO.gerarRelatorioInspecaoEntrega(whereIn);
		
		if (entregas.isEmpty()){
			throw new SinedException("N�o existem registros suficientes para gerar o relat�rio.");
		}
		
		report.setDataSource(entregas);
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#callProcedureAtualizaEntrega
	 * @param entrega
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaEntrega(Entrega entrega){
		entregaDAO.callProcedureAtualizaEntrega(entrega);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	private void callProcedureAtualizaEntrega(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Entrega entrega;
			for (int i = 0; i < ids.length; i++) {
				entrega = new Entrega(Integer.parseInt(ids[i].trim()));
				this.callProcedureAtualizaEntrega(entrega);
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#loadWithLista
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> loadWithLista(String whereIn,String orderBy, boolean asc) {
		return entregaDAO.loadWithLista(whereIn,orderBy, asc);
	}
	
	/**
	 * Caso o usu�rio clique no bot�o salvar mais de uma vez, ir� parar no erro e so salvar� o registro uma vez ao inv� de n vezes.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioorigemService#existeMovPatrimonioComOrigemEntrega(Entrega)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService#existeMovEstoqueComOrigemEntrega(Entrega)
	 * @param bean
	 * @param errors
	 * @author Tom�s Rabelo
	 */
	public void validaBaixaEntrega(BaixarEntregaBean bean, BindException errors) {
		if(bean.getListaPendentes() != null && !bean.getListaPendentes().isEmpty()){
			for (BaixarEntregaItemBean baixarEntregaItemBean : bean.getListaPendentes()) {
				if(movpatrimonioorigemService.existeMovPatrimonioComOrigemEntrega(baixarEntregaItemBean.getEntrega())){
					errors.reject("001", "A entrega j� foi baixada");
					break;
				}else if (movimentacaoestoqueorigemService.existeMovEstoqueComOrigemEntrega(baixarEntregaItemBean.getEntrega())){
					errors.reject("001", "A entrega j� foi baixada");
					break;
				}
			}
		}
	}
	
	/**
	 * Este m�todo � chamado toda vez que for salvar uma entrega ou estornar. Caso a entrega perten�a a uma ordem de compra ent�o � refeito o
	 * calculo da entrega na solicita��o de compra
	 * Obs: S�o feitas duas querys pois s�o carregadas v�rias listas. E com uma query s� n�o estava carregando tudo corretamente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadCotacaoParaAjustarPedidoSolicitacaoCompra(Ordemcompra)
	 * @see #getEntregasDaCotacao(Cotacao, Entrega)
	 * @see #calculaTotalMaterialEntregue(List)
	 * @see #buscaSolicitacoesDaEntregaAPartirDoMaterialELocalArmazenagem(Cotacao, Entrega, List, HashMap)
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void preparaAtualizacaoDasSolicitacoes(Entrega bean) {
		if(bean.getHaveOrdemcompra()){
			List<Solicitacaocompra> solicitacaoes = new ArrayList<Solicitacaocompra>();
			
			Set<Ordemcompraentrega> listaOrdemcompraentrega = bean.getListaOrdemcompraentrega();
			for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
				Ordemcompra ordemcompra = ordemcompraentrega.getOrdemcompra();
				if(ordemcompra != null){
					Cotacao cotacao = cotacaoService.loadCotacaoParaAjustarPedidoSolicitacaoCompra(ordemcompra);
					if(cotacao != null){
						List<Entrega> entregasCotacao = this.getEntregasDaCotacao(cotacao, bean);
						HashMap<Integer, Double> map = this.calculaTotalMaterialEntregue(entregasCotacao);
						this.buscaSolicitacoesDaEntregaAPartirDoMaterialELocalArmazenagem(cotacao, bean, solicitacaoes, map);
					}
				}
			}
			
			bean.setListaSolicitacao(solicitacaoes);
		}
	}
	
	/**
	 * Coloca no map o material e a quantidade referente desse material que j� foi entregue
	 * 
	 * @see #adicionaMaterialNoMap(Entrega, HashMap)
	 * @param entregasCotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	private HashMap<Integer, Double> calculaTotalMaterialEntregue(List<Entrega> entregasCotacao) {
		HashMap<Integer, Double> map = new HashMap<Integer, Double>();
		
		for (Entrega entrega : entregasCotacao) {
			for(Entregadocumento ed : entrega.getListaEntregadocumento())
				adicionaMaterialNoMap(ed, map);
		}
		
		return map;
	}
	
	/**
	 * Adiciona material da entrega no map e atualiza quantidade
	 * 
	 * @param entrega
	 * @param map
	 * @author Tom�s Rabelo
	 */
	private void adicionaMaterialNoMap(Entregadocumento entregadocumento, HashMap<Integer, Double> map) {
		for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
			if(map.containsKey(entregamaterial.getMaterial().getCdmaterial())){
				map.put(entregamaterial.getMaterial().getCdmaterial(), map.get(entregamaterial.getMaterial().getCdmaterial()) + entregamaterial.getQtde());
			}else{
				map.put(entregamaterial.getMaterial().getCdmaterial(), entregamaterial.getQtde());	
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cotacao
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> getEntregasDaCotacao(Cotacao cotacao, Entrega entrega) {
		return entregaDAO.getEntregasDaCotacao(cotacao, entrega);
	}
	
	
	/**
	 * M�todo que percorre da entrega, passando pela cota��o at� chegar na solicita��o de compra. Ele busca todas as solicita��es de compra 
	 * que fazem parte da entrega. Est� sendo utilizado de 2 maneiras. Uma ele descobre as solicita��es de compra e seta a entrega para 
	 * atulizar e a outra ele so descobre quais s�o as solicita��es para excluir
	 * 
	 * @see #calculaSolicitacaoEntrega(Integer, Solicitacaocompra, HashMap)
	 * @param cotacao
	 * @param entrega
	 * @param solicitacaoes
	 * @param map
	 * @author Tom�s Rabelo
	 */
	private void buscaSolicitacoesDaEntregaAPartirDoMaterialELocalArmazenagem(Cotacao cotacao, Entrega entrega, List<Solicitacaocompra> solicitacaoes, HashMap<Integer, Double> map) {
		boolean achouMaterial = false;
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
				for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
					if(cotacaofornecedor.getFornecedor().equals(entregadocumento.getFornecedor())){
						for (Entregamaterial entregamaterial :  entregadocumento.getListaEntregamaterial()) {
							achouMaterial = false;
							
							for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
								
								if(cotacaofornecedoritem.getMaterial().equals(entregamaterial.getMaterial()) && 
										cotacaofornecedoritem.getLocalarmazenagem().equals(entrega.getLocalarmazenagem())){
									
									for (Cotacaoorigem cotacaoorigem : cotacao.getListaOrigem()) {
										
										if(cotacaofornecedoritem.getMaterial().equals(cotacaoorigem.getSolicitacaocompra().getMaterial()) && 
												cotacaofornecedoritem.getLocalarmazenagemsolicitacao().equals(cotacaoorigem.getSolicitacaocompra().getLocalarmazenagem())){
											
											calculaSolicitacaoEntrega(entregamaterial.getQtde(), cotacaoorigem.getSolicitacaocompra(), map);
											
											solicitacaoes.add(cotacaoorigem.getSolicitacaocompra());
											achouMaterial = true;
										}
									}
								}
								if(achouMaterial)
									break;
							}
						}
						//Achou fornecedor
						break;
					}					
				}
			}
		}
	}
	
	/**
	 * Calcula o valor que aparecer� na solicita��o de compra. Neste m�todo faz tanto adi��o, subtra��o e remo��o da string entrega.
	 * 
	 * @param qtde
	 * @param solicitacaocompra
	 * @param map
	 * @author Tom�s Rabelo
	 */
	private void calculaSolicitacaoEntrega(Double qtde, Solicitacaocompra solicitacaocompra, HashMap<Integer, Double> map) {
		//Se o map for nulo � pq esta cancelando
		if(map != null){
			if(map.containsKey(solicitacaocompra.getMaterial().getCdmaterial())){
				qtde += map.get(solicitacaocompra.getMaterial().getCdmaterial());
			}
			if(solicitacaocompra.getPedido() != null)
				solicitacaocompra.setEntrega(qtde+"/"+solicitacaocompra.getPedido().split("/")[0]);
		}else{
			if(solicitacaocompra.getEntrega() != null){
				String[] qtdes = solicitacaocompra.getEntrega().split("/");
				if(Double.parseDouble(qtdes[0]) - qtde <= 0){
					solicitacaocompra.setEntrega(null);
				}else{
					solicitacaocompra.setEntrega(Double.parseDouble(qtdes[0]) - qtde+"/"+qtdes[1]);
				}
			}
		}
	}
	
	/**
	 * M�todo chamado ao cancelar entrega. Ele � diferente do inserir e estornar, pois pode-se cancelar v�rias entregas de 1 �nica vez.
	 * 
	 * @see #getEntregasQuePossuemOrdemCompra(List)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadCotacoesParaAjustarPedidoSolicitacaoCompra(String)
	 * @see #buscaSolicitacoesDaEntregaAPartirDoMaterialELocalArmazenagem(Cotacao, Entrega, List, HashMap)
	 * @param itensSelecionados
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesCompraRelacionadasAsEntregas(List<Entrega> listaEntrega) {
		List<Entrega> listaEntregasComOrdensCompra = getEntregasQuePossuemOrdemCompra(listaEntrega);
		List<Solicitacaocompra> solicitacaoes = new ArrayList<Solicitacaocompra>();
		
		if(listaEntregasComOrdensCompra != null && listaEntregasComOrdensCompra.size() > 0){
			List<Ordemcompra> listaOrdemcompra = new ArrayList<Ordemcompra>();
			for (Entrega entrega : listaEntregasComOrdensCompra) {
				for (Ordemcompraentrega ordemcompraentrega : entrega.getListaOrdemcompraentrega()) {
					if(ordemcompraentrega.getOrdemcompra() != null){
						listaOrdemcompra.add(ordemcompraentrega.getOrdemcompra());
					}
				}
			}
			List<Cotacao> cotacoes = cotacaoService.loadCotacoesParaAjustarPedidoSolicitacaoCompra(CollectionsUtil.listAndConcatenate(listaOrdemcompra, "cdordemcompra",","));
			
			if(cotacoes != null && cotacoes.size() > 0)
				for (Entrega entrega : listaEntrega) {
					if(entrega.getHaveOrdemcompra()){
						Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
						for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
							Ordemcompra ordemcompra = ordemcompraentrega.getOrdemcompra();
							
							if(ordemcompra != null && ordemcompra.getCotacao() != null){
								for (Cotacao cotacao : cotacoes) {
									if(ordemcompra.getCotacao().getCdcotacao().equals(cotacao.getCdcotacao())){
										this.buscaSolicitacoesDaEntregaAPartirDoMaterialELocalArmazenagem(cotacao, entrega, solicitacaoes, null);
										break;
									}
								}
							}
						}
					}
				}
		}						
		return solicitacaoes;
	}
	
	/**
	 * Este m�todo retira as entregas que n�o possui nenhuma ordem de compra isso agiliza, pois caso esteje cancelando somente entregas criadas
	 * avulsas n�o � necess�rio fazer query's
	 * 
	 * @param listaEntrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Entrega> getEntregasQuePossuemOrdemCompra(List<Entrega> listaEntrega) {
		List<Entrega> list = new ListSet<Entrega>(Entrega.class);
		for (Entrega entrega : listaEntrega) 
			if(entrega.getHaveOrdemcompra())
				list.add(entrega);
		
		return list;
	}
	
	/**
	 * M�todo que cancela ou estorna o cancelamento das entregas e atualiza a compra das solicita��es referentes a entrega caso haja.
	 * 
	 * @see #updateCancelada(String)
	 * @see #updateEstorno(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#doUpdatePedidoSolicitacao(Solicitacaocompra, Integer)
	 * @see #callProcedureAtualizaEntrega(String)
	 * @param solicitacaoes
	 * @param itensSelecionados
	 * @param situacaosuprimentos
	 * @author Tom�s Rabelo
	 */
	public void cancelaEstornaEntregaAtualizaSolicitacoes(final List<Solicitacaocompra> solicitacaoes, final String itensSelecionados, final Situacaosuprimentos situacaosuprimentos) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(situacaosuprimentos.getCodigo().equals(Situacaosuprimentos.CANCELADA.getCodigo())){
					updateCancelada(itensSelecionados);
				} else{
					updateEstorno(itensSelecionados);
				}
				
				if(solicitacaoes != null && solicitacaoes.size() > 0)
					for (Solicitacaocompra solicitacaocompra : solicitacaoes) 
						solicitacaocompraService.doUpdateCompraSolicitacao(solicitacaocompra, Solicitacaocompra.ENTREGA);
				
				return status;
			}
		});
		
		this.callProcedureAtualizaEntrega(itensSelecionados);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findForAtualizarvalorcustomaterial(String whereInEntrega)
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findForAtualizarvalorcustomaterial(String whereInEntrega){
		return entregaDAO.findForAtualizarvalorcustomaterial(whereInEntrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> getEntregasParaRomaneio(String whereIn) {
		return entregaDAO.getEntregasParaRomaneio(whereIn);
	}
	
	/**
	 * M�todo que verifica se as entregas selecionadas est�o com a situa��o igual a 'Aguardando romaneio'.
	 * 
	 * @param entregas
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean validaRomaneio(List<Entrega> entregas, BindException errors) {
		for (Entrega entrega : entregas) {
			if(entrega.getRomaneio() == null || entrega.getRomaneio()){
				errors.reject("001","As entregas n�o precisam de romaneio.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo verifica se todas as entregas selecionadas possuiem o mesmo local de entrega.
	 * 
	 * @param entregas
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean verificaLocalarmazenagemIguais(List<Entrega> entregas, BindException errors) {
		Localarmazenagem localarmazenagemaux = entregas.get(0).getLocalarmazenagem();
		
		for (Entrega entrega : entregas) {
			if(!entrega.getLocalarmazenagem().getCdlocalarmazenagem().equals(localarmazenagemaux.getCdlocalarmazenagem())){
				errors.reject("003","As entregas devem possuir o mesmo local de entrega.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Localarmazenagem> getLocaisArmazenamentoDasSolicitacoesReferentesAsEntregas(String whereIn) {
		return entregaDAO.getLocaisArmazenamentoDasSolicitacoesReferentesAsEntregas(whereIn);
	}
	
	/**
	 * Cria o relat�rio de Curva ABC.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/10/2012
	 */
	public IReport gerarRelatorioCurvaABC(EmitircurvaabcFiltro filtro) {
		List<EmitircurvaabcBean> listacurvaabc = entregaDAO.listEntregaForCurvaABC(filtro, true);
		return vendaService.makeRelatorioCurvaABCEntrega(filtro, listacurvaabc);
	}
	
	@SuppressWarnings("unchecked")
	public ImportacaoXmlNfeBean getListaImportacao(Arquivo arquivo) throws JDOMException, IOException, ParseException {
		Element root = SinedUtil.getRootElementXML(arquivo.getContent());
		if(root.getName().equals("CompNfse")){
			return this.getListaImportacaoServico(root);
		}
		
		if(!root.getName().equals("nfeProc")){
			if(!root.getName().equals("NFe")){
				throw new SinedException("nfeProc n�o encontrado.");
			}
		}
		
		ImportacaoXmlNfeBean bean = new ImportacaoXmlNfeBean();
		Element nfe = null;
		if(root.getName().equals("nfeProc")){
			nfe = SinedUtil.getChildElement("NFe", root.getContent());
		}else if(root.getName().equals("NFe")){
			nfe = root;
		}
		if(nfe == null) throw new SinedException("NFe n�o encontrado.");
		
		Element infNFe = SinedUtil.getChildElement("infNFe", nfe.getContent());
		if(infNFe == null) throw new SinedException("infNFe n�o encontrado.");
		
//		Attribute versaoAttr = infNFe.getAttribute("versao");
//		if(versaoAttr != null && 
//				versaoAttr.getValue() != null &&
//				!versaoAttr.getValue().trim().equals("2.00")){
//			throw new SinedException("A importa��o n�o pode ser realizada, o formato do XML corresponde a uma vers�o da NF-e n�o suportada pelo sistema. Por favor, tente a importa��o pela chave de acesso.");
//		}
		
		Element ide = SinedUtil.getChildElement("ide", infNFe.getContent());
		if(ide == null) throw new SinedException("ide n�o encontrado.");
		
		// C�digo da UF do emitente
		Element cUF = SinedUtil.getChildElement("cUF", ide.getContent());
		if(cUF == null) throw new SinedException("cUF n�o encontrado.");
		bean.setCuf(Integer.parseInt(cUF.getText())); 
		
		// Natureza de opera��o
		Element natOp = SinedUtil.getChildElement("natOp", ide.getContent());
		if(natOp == null) throw new SinedException("natOp n�o encontrado.");
		bean.setNatop(natOp.getText()); 
		
		// Forma de pagamento
		Element indPag = SinedUtil.getChildElement("indPag", ide.getContent());
//		if(indPag == null) throw new SinedException("indPag n�o encontrado.");
		if(indPag != null){
			bean.setIndpag(Formapagamentonfe.values()[Integer.parseInt(indPag.getText())]);
		}
		
		// C�digo do Modelo do Documento Fiscal
		Element mod = SinedUtil.getChildElement("mod", ide.getContent());
		if(mod == null) throw new SinedException("mod n�o encontrado.");
		bean.setModelodocumentofiscal(modelodocumentofiscalService.loadByCodigo(mod.getText())); 
		
		// S�rie do Documento Fiscal
		Element serie = SinedUtil.getChildElement("serie", ide.getContent());
		if(serie == null) throw new SinedException("serie n�o encontrado.");
		bean.setSerie(Integer.parseInt(serie.getText())); 
		
		// N�mero do Documento Fiscal
		Element nNF = SinedUtil.getChildElement("nNF", ide.getContent());
		if(nNF == null) throw new SinedException("nNF n�o encontrado.");
		bean.setNumero(nNF.getText());
		
		// Data de emiss�o do Documento Fiscal
		Element dEmi = SinedUtil.getChildElement("dEmi", ide.getContent());
		if(dEmi == null){
			Element dhEmi = SinedUtil.getChildElement("dhEmi", ide.getContent());
			if(dhEmi == null) throw new SinedException("dEmi n�o encontrado.");
			bean.setDtemissao(SinedDateUtils.stringToDate(dhEmi.getText().split("T")[0], "yyyy-MM-dd"));
		} else {
			bean.setDtemissao(SinedDateUtils.stringToDate(dEmi.getText(), "yyyy-MM-dd"));
		}
		
		// Data de entrada do Documento Fiscal
		Element dSaiEnt = SinedUtil.getChildElement("dSaiEnt", ide.getContent());
		if(dSaiEnt != null) bean.setDsaient(SinedDateUtils.stringToDate(dSaiEnt.getText(), "yyyy-MM-dd"));
		else {
			Element dhSaiEnt = SinedUtil.getChildElement("dhSaiEnt", ide.getContent());
			if(dhSaiEnt != null) bean.setDsaient(SinedDateUtils.stringToDate(dhSaiEnt.getText().split("T")[0], "yyyy-MM-dd"));
		}
		
		Element emit = SinedUtil.getChildElement("emit", infNFe.getContent());
		if(emit == null) throw new SinedException("emit n�o encontrado.");
		
		// CNPJ do fornecedor
		Element cNPJ = SinedUtil.getChildElement("CNPJ", emit.getContent());
		if(cNPJ != null) bean.setCnpj(new Cnpj(cNPJ.getText()));
		
		// CPF do fornecedor
		Element cPF = SinedUtil.getChildElement("CPF", emit.getContent());
		if(cPF != null) bean.setCpf(new Cpf(cPF.getText()));
		
		// Raz�o social ou nome do Fornecedor
		Element xNome = SinedUtil.getChildElement("xNome", emit.getContent());
		if(xNome == null) throw new SinedException("xNome n�o encontrado.");
		bean.setRazaosocial(xNome.getText());
		
		// Nome fantasia do Fornecedor
		Element xFant = SinedUtil.getChildElement("xFant", emit.getContent());
		if(xFant != null) bean.setNomefantasia(xFant.getText());
		
		// Grupo de endere�o do fornecedor
		Element enderEmit = SinedUtil.getChildElement("enderEmit", emit.getContent());
		if(enderEmit == null) throw new SinedException("enderEmit n�o encontrado.");
		
		// Logradouro do endere�o do fornecedor
		Element xLgr = SinedUtil.getChildElement("xLgr", enderEmit.getContent());
		if(xLgr == null) throw new SinedException("xLgr n�o encontrado.");
		bean.setLogradouro(xLgr.getText());
		
		// N�mero do endere�o do fornecedor
		Element nro = SinedUtil.getChildElement("nro", enderEmit.getContent());
		if(nro == null) throw new SinedException("nro n�o encontrado.");
		bean.setNumeroendereco(nro.getText());
		
		// Complemento do endere�o do fornecedor
		Element xCpl = SinedUtil.getChildElement("xCpl", enderEmit.getContent());
		if(xCpl != null) bean.setComplemento(xCpl.getText());
		
		// Bairro do endere�o do fornecedor
		Element xBairro = SinedUtil.getChildElement("xBairro", enderEmit.getContent());
		if(xBairro == null) throw new SinedException("xBairro n�o encontrado.");
		bean.setBairro(xBairro.getText());
		
		// Munic�pio do fornecedor
		Element cMun = SinedUtil.getChildElement("cMun", enderEmit.getContent());
		if(cMun == null) throw new SinedException("cMun n�o encontrado.");
		bean.setCmunicipio(cMun.getText());
		
		// CEP do fornecedor
		Element cEP = SinedUtil.getChildElement("CEP", enderEmit.getContent());
		if(cEP != null) bean.setCep(new Cep(cEP.getText()));
		
		// Telefone do fornecedor
		Element fone = SinedUtil.getChildElement("fone", enderEmit.getContent());
		if(fone != null) bean.setFone(fone.getText());
		
		// IE do Fornecedor
		Element iE = SinedUtil.getChildElement("IE", emit.getContent());
		if(iE == null) throw new SinedException("IE n�o encontrado.");
		bean.setIe(iE.getText());
		
		// IE ST do Fornecedor
		Element iEST = SinedUtil.getChildElement("IEST", emit.getContent());
		if(iEST != null) bean.setIest(iEST.getText());
		
		// IM do Fornecedor
		Element iM = SinedUtil.getChildElement("IM", emit.getContent());
		if(iM != null) bean.setIm(iM.getText());
		
		// Lista de itens da Nota		
		bean.setListaItens(this.getItensXmlNota(infNFe));
		
		Element total = SinedUtil.getChildElement("total", infNFe.getContent()); 
		if(total == null) throw new SinedException("total n�o encontrado.");
		
		Element iCMSTot = SinedUtil.getChildElement("ICMSTot", total.getContent()); 
		if(iCMSTot == null) throw new SinedException("ICMSTot n�o encontrado.");
		
		// Base de c�lculo do ICMS
		Element vBC = SinedUtil.getChildElement("vBC", iCMSTot.getContent());
//		if(vBC == null) throw new SinedException("vBC n�o encontrado.");
		if(vBC != null){
			bean.setTotalbcicms(new Money(Double.valueOf(vBC.getText())));
		}
		
		// Valor do ICMS
		Element vICMS = SinedUtil.getChildElement("vICMS", iCMSTot.getContent());
//		if(vICMS == null) throw new SinedException("vICMS n�o encontrado.");
		if(vICMS != null) bean.setTotalicms(new Money(Double.valueOf(vICMS.getText())));
		
		// Base de c�lculo do ICMS ST
		Element vBCST = SinedUtil.getChildElement("vBCST", iCMSTot.getContent());
//		if(vBCST == null) throw new SinedException("vBCST n�o encontrado.");
		if(vBCST != null) bean.setTotalbcicmsst(new Money(Double.valueOf(vBCST.getText())));
		
		// Valor do ICMS ST
		Element vST = SinedUtil.getChildElement("vST", iCMSTot.getContent());
		if(vST == null) throw new SinedException("vST n�o encontrado.");
		bean.setTotalicmsst(new Money(Double.valueOf(vST.getText())));
		
		// Valor do frete
		Element vFrete = SinedUtil.getChildElement("vFrete", iCMSTot.getContent());
		if(vFrete == null) throw new SinedException("vFrete n�o encontrado.");
		bean.setTotalfrete(new Money(Double.valueOf(vFrete.getText())));
		
		// Valor do seguro
		Element vSeg = SinedUtil.getChildElement("vSeg", iCMSTot.getContent());
		if(vSeg == null) throw new SinedException("vSeg n�o encontrado.");
		bean.setTotalseguro(new Money(Double.valueOf(vSeg.getText())));
		
		// Valor do desconto
		Element vDesc = SinedUtil.getChildElement("vDesc", iCMSTot.getContent());
		if(vDesc == null) throw new SinedException("vDesc n�o encontrado.");
		bean.setTotaldesconto(new Money(Double.valueOf(vDesc.getText())));
		
		// Valor do IPI
		Element vIPI = SinedUtil.getChildElement("vIPI", iCMSTot.getContent());
		if(vIPI == null) throw new SinedException("vIPI n�o encontrado.");
		bean.setTotalipi(new Money(Double.valueOf(vIPI.getText())));
		
		// Valor do PIS
		Element vPIS = SinedUtil.getChildElement("vPIS", iCMSTot.getContent());
		if(vPIS == null) throw new SinedException("vPIS n�o encontrado.");
		bean.setTotalpis(new Money(Double.valueOf(vPIS.getText())));
		
		// Valor do COFINS
		Element vCOFINS = SinedUtil.getChildElement("vCOFINS", iCMSTot.getContent());
		if(vCOFINS == null) throw new SinedException("vCOFINS n�o encontrado.");
		bean.setTotalcofins(new Money(Double.valueOf(vCOFINS.getText())));
		
		// Valor do FCP
		Element vFCP = SinedUtil.getChildElement("vFCP", iCMSTot.getContent());
		if(vFCP != null) {
			bean.setTotalFcp(new Money(Double.valueOf(vFCP.getText())));
		}
		
		// Valor do FCP ST
		Element vFCPST = SinedUtil.getChildElement("vFCPST", iCMSTot.getContent());
		if(vFCPST != null) {
			bean.setTotalFcpSt(new Money(Double.valueOf(vFCPST.getText())));
		}
		
		// Valor do FCP ST Ret
		Element vFCPSTRet = SinedUtil.getChildElement("vFCPSTRet", iCMSTot.getContent());
		if(vFCPSTRet != null) {
			bean.setTotalFcpStRet(new Money(Double.valueOf(vFCPSTRet.getText())));
		}
		
		// Valor das outras despesas
		Element vOutro = SinedUtil.getChildElement("vOutro", iCMSTot.getContent());
		if(vOutro == null) throw new SinedException("vOutro n�o encontrado.");
		bean.setTotaloutrasdespesas(new Money(Double.valueOf(vOutro.getText())));
		
		
		Element transp = SinedUtil.getChildElement("transp", infNFe.getContent());
		if(transp != null){
			// Valor das outras despesas
			Element modFrete = SinedUtil.getChildElement("modFrete", transp.getContent());
			if(modFrete == null) throw new SinedException("modFrete n�o encontrado.");
			bean.setResponsavelFrete(ResponsavelFrete.getBean(Integer.parseInt(modFrete.getText())));
		}
		
		Element infAdic = SinedUtil.getChildElement("infAdic", infNFe.getContent());
		if(infAdic != null){
			// Informa��o adicional de interesse do Fisco.
			Element infAdFisco = SinedUtil.getChildElement("infAdFisco", infAdic.getContent());
			if(infAdFisco != null) bean.setInfadfisco(infAdFisco.getText());
			
			
			
			// Informa��o adicional de interesse do Contribuinte.
			Element infCpl = SinedUtil.getChildElement("infCpl", infAdic.getContent());
			if(infCpl != null) bean.setInfcpl(infCpl.getText());
		}
		
		Element protNFe = SinedUtil.getChildElement("protNFe", root.getContent());
		if(protNFe != null){			
			// Informa��o adicional de interesse do Fisco.
			Element infProt_Id = SinedUtil.getChildElement("infProt", protNFe.getContent());
			if(infProt_Id != null){				
				// Informa��o adicional de interesse do Contribuinte.
				Element chNfe = SinedUtil.getChildElement("chNFe", infProt_Id.getContent());
				if(chNfe != null) bean.setChaveacesso(chNfe.getText());
			}			
		}
		
		// Lista de itens duplicatas
		bean.setListaDuplicatas(this.getDuplicatasXmlNota(infNFe));
		
		return bean;
	}
	
	/**
	* M�todo que busca as duplicatas no xml
	*
	* @param infNFe
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	private List<ImportacaoXmlNfeDuplicataBean> getDuplicatasXmlNota(Element infNFe) {
		List<ImportacaoXmlNfeDuplicataBean> lista = new ArrayList<ImportacaoXmlNfeDuplicataBean>();
		
		Element cobr = SinedUtil.getChildElement("cobr", infNFe.getContent());
		if(cobr != null){
			List<Element> listaDup = SinedUtil.getListChildElement("dup", cobr.getContent());
			if(SinedUtil.isListNotEmpty(listaDup)){
				ImportacaoXmlNfeDuplicataBean bean;
				Element nDup, nFat, dVenc, vDup;
				
				int sequencial = 0;
				
				for (Element dup : listaDup) {
					bean = new ImportacaoXmlNfeDuplicataBean();
					
					bean.setSequencial(sequencial);
					sequencial++;
					
					String numero = "";
					
					nFat = SinedUtil.getChildElement("nFat", dup.getContent());
					if(nFat != null) numero = nFat.getText();
					
					nDup = SinedUtil.getChildElement("nDup", dup.getContent());
					if(nDup != null) numero = ((org.apache.commons.lang.StringUtils.isNotEmpty(numero)) ? numero : "") + nDup.getText();
					if(numero != null) bean.setNumero(numero);
					
					dVenc = SinedUtil.getChildElement("dVenc", dup.getContent());
					if(dVenc != null){
						try {
							bean.setDtvencimento(SinedDateUtils.stringToDate(dVenc.getText().split("T")[0], "yyyy-MM-dd"));
						} catch (ParseException e) {}
					}
					
					vDup = SinedUtil.getChildElement("vDup", dup.getContent());
					if(vDup != null) bean.setValor(new Money(Double.valueOf(vDup.getText())));
					
					lista.add(bean);
				}
			}
		}
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	private ImportacaoXmlNfeBean getListaImportacaoServico(Element root) {
		ImportacaoXmlNfeBean bean = new ImportacaoXmlNfeBean();
		bean.setModelodocumentofiscal(modelodocumentofiscalService.loadNotafiscalservicoeletronica());
		
		Element nfse = SinedUtil.getChildElement("Nfse", root.getContent());
		if(nfse == null) throw new SinedException("Tag 'Nfse' n�o encontrada.");
		
		Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
		if(infNfse == null) throw new SinedException("Tag 'InfNfse' n�o encontrada.");
		
		Element prestadorServico = SinedUtil.getChildElement("PrestadorServico", infNfse.getContent());
		if(prestadorServico != null){
			Element identificacaoPrestador = SinedUtil.getChildElement("IdentificacaoPrestador", prestadorServico.getContent());
			if(identificacaoPrestador != null){
				Element cnpj = SinedUtil.getChildElement("Cnpj", identificacaoPrestador.getContent());
				if(cnpj != null){
					bean.setCnpj(new Cnpj(cnpj.getText()));
				}
				Element inscricaoMunicipal = SinedUtil.getChildElement("InscricaoMunicipal", identificacaoPrestador.getContent());
				if(inscricaoMunicipal != null){
					bean.setIm(inscricaoMunicipal.getText());
				}
			}
			
			Element razaoSocial = SinedUtil.getChildElement("RazaoSocial", prestadorServico.getContent());
			if(razaoSocial != null){
				bean.setRazaosocial(razaoSocial.getText());
			}
			Element nomeFantasia = SinedUtil.getChildElement("NomeFantasia", prestadorServico.getContent());
			if(nomeFantasia != null){
				bean.setNomefantasia(nomeFantasia.getText());
			}
			Element endereco = SinedUtil.getChildElement("Endereco", prestadorServico.getContent());
			if(endereco != null){
				Element logradouroEndereco = SinedUtil.getChildElement("Endereco", endereco.getContent());
				if(logradouroEndereco != null){
					bean.setLogradouro(logradouroEndereco.getText());
				}
				Element numeroEndereco = SinedUtil.getChildElement("Numero", endereco.getContent());
				if(numeroEndereco != null){
					bean.setNumeroendereco(numeroEndereco.getText());
				}
				Element complementoEndereco = SinedUtil.getChildElement("Complemento", endereco.getContent());
				if(complementoEndereco != null){
					bean.setComplemento(complementoEndereco.getText());
				}
				Element bairroEndereco = SinedUtil.getChildElement("Bairro", endereco.getContent());
				if(bairroEndereco != null){
					bean.setBairro(bairroEndereco.getText());
				}
				Element codigoMunicipioEndereco = SinedUtil.getChildElement("CodigoMunicipio", endereco.getContent());
				if(codigoMunicipioEndereco != null){
					bean.setCmunicipio(codigoMunicipioEndereco.getText());
				}
				Element ufEndereco = SinedUtil.getChildElement("Uf", endereco.getContent());
				if(ufEndereco != null){
					bean.setSiglauf(ufEndereco.getText());
				}
				Element cepEndereco = SinedUtil.getChildElement("Cep", endereco.getContent());
				if(cepEndereco != null){
					bean.setCep(new Cep(cepEndereco.getText()));
				}
			}
		}
		
		Element numero = SinedUtil.getChildElement("Numero", infNfse.getContent());
		if(numero != null){
			bean.setNumero(numero.getText());
		}
		
		Element codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent());
		if(codigoVerificacao != null){
			bean.setChaveacesso(codigoVerificacao.getText());
		}
		
		Element dataEmissao = SinedUtil.getChildElement("DataEmissao", infNfse.getContent());
		if(dataEmissao != null){
			String dataEmissaoStr = dataEmissao.getText();
			if(dataEmissaoStr != null && !dataEmissaoStr.trim().equals("")){
				try {
					java.util.Date data = LkNfeUtil.FORMATADOR_DATA_HORA.parse(dataEmissaoStr.trim());
					bean.setDtemissao(SinedDateUtils.castUtilDateForSqlDate(data));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		
		List<ImportacaoXmlNfeItemBean> listaItens = new ArrayList<ImportacaoXmlNfeItemBean>();
		ImportacaoXmlNfeItemBean it = new ImportacaoXmlNfeItemBean();
		it.setQtde(1d);
		it.setSequencial(0);
		
		Element servico = SinedUtil.getChildElement("Servico", infNfse.getContent());
		if(servico != null){
			Element valores = SinedUtil.getChildElement("Valores", servico.getContent());
			if(valores != null){
				Element valorServicos = SinedUtil.getChildElement("ValorServicos", valores.getContent());
				if(valorServicos != null){
					it.setValorproduto(Double.valueOf(valorServicos.getText()));
					it.setValorunitario(Double.valueOf(valorServicos.getText()));
				}
			}
			Element itemListaServico = SinedUtil.getChildElement("ItemListaServico", servico.getContent());
			if(itemListaServico != null){
				it.setCListServ(itemListaServico.getText());
			}
			Element codigoTributacaoMunicipio = SinedUtil.getChildElement("CodigoTributacaoMunicipio", servico.getContent());
			if(codigoTributacaoMunicipio != null){
				it.setCprod(codigoTributacaoMunicipio.getText());
			}
			Element discriminacao = SinedUtil.getChildElement("Discriminacao", servico.getContent());
			if(discriminacao != null){
				it.setXprod(discriminacao.getText());
			}
		}
		
		listaItens.add(it);
		bean.setListaItens(listaItens);
		
		return bean;
	}
	
	@SuppressWarnings("unchecked")
	private List<ImportacaoXmlNfeItemBean> getItensXmlNota(Element infNFe) {
		
		List<Element> listaDet = SinedUtil.getListChildElement("det", infNFe.getContent());
		
		List<ImportacaoXmlNfeItemBean> lista = new ArrayList<ImportacaoXmlNfeItemBean>();
		ImportacaoXmlNfeItemBean bean;
		Element cProd, xProd, cFOP, qCom, cEAN, nCM, eXTIPI,
				vUnCom, vProd, uCom, iSSQN, cListServ,
				vFrete, vSeg, vDesc, vOutro, 
				prod, imposto, iCMS, iCMS00, iCMS10, iCMS20, 
				iCMS30, iCMS40, iCMS51, iCMS60, iCMS70, iCMS90, 
				iCMSPart, iCMSST, iCMSSN101, iCMSSN102, iCMSSN201,
				iCMSSN202, iCMSSN500, iCMSSN900, vBC, pICMS, vICMS, 
				vBCST, pICMSST, vICMSST, cST, cSOSN, iPI, iPITrib, iPINT,
				pIPI, vIPI, pIS, pISAliq, pISQtde, pISNT, pISOutr, pPIS, vPIS,
				qBCProd, vAliqProd, cOFINS, cOFINSAliq, cOFINSQtde, cOFINSNT, 
				cOFINSOutr, pCOFINS, vCOFINS, vII, vDespAdu, vIOF, iI,
				vBCSTRet, pST, vICMSSTRet,
				vBCFCP, pFCP, vFCP, 
				vBCFCPST, pFCPST, vFCPST, 
				vBCFCPSTRet, pFCPSTRet, vFCPSTRet, 
				vICMSDeson, motDesICMS, orig, nLote, qLote, dFab, dVal, cAgreg;
		
		int sequencial = 0;
		
		for (Element det : listaDet) {
			bean = new ImportacaoXmlNfeItemBean();
			
			bean.setSequencial(sequencial);
			sequencial++;
			
			prod = SinedUtil.getChildElement("prod", det.getContent());
			if(prod == null) throw new SinedException("prod n�o encontrado.");
			
			// C�digo do produto
			cProd = SinedUtil.getChildElement("cProd", prod.getContent());
			if(cProd == null) throw new SinedException("cProd n�o encontrado.");
			bean.setCprod(cProd.getText());
			
			// C�digo EAN do produto
			cEAN = SinedUtil.getChildElement("cEAN", prod.getContent());
			if(cEAN == null) throw new SinedException("cEAN n�o encontrado.");
			bean.setCean(cEAN.getText());
			
			// Descri��o do produto
			xProd = SinedUtil.getChildElement("xProd", prod.getContent());
			if(xProd == null) throw new SinedException("xProd n�o encontrado.");
			bean.setXprod(StringUtils.tiraAcento(xProd.getText()));
			
			// C�digo NCM
			nCM = SinedUtil.getChildElement("NCM", prod.getContent());
			if(nCM != null) bean.setNcm(nCM.getText());
			
			// EX da TIPI
			eXTIPI = SinedUtil.getChildElement("EXTIPI", prod.getContent());
			if(eXTIPI != null) bean.setExtipi(eXTIPI.getText());
			
			// CFOP
			cFOP = SinedUtil.getChildElement("CFOP", prod.getContent());
			if(cFOP == null) throw new SinedException("CFOP n�o encontrado.");
			bean.setCfop(cfopService.findByCodigo(cFOP.getText()));
			
			// Unidade de medida comercial
			uCom = SinedUtil.getChildElement("uCom", prod.getContent());
			if(uCom == null) throw new SinedException("uCom n�o encontrado.");
			bean.setUnidademedidacomercial(uCom.getText());
			
			// Quantidade comercial
			qCom = SinedUtil.getChildElement("qCom", prod.getContent());
			if(qCom == null) throw new SinedException("qCom n�o encontrado.");
			bean.setQtde(Double.valueOf(qCom.getText()));
			
			// Valor unit�rio comercial
			vUnCom = SinedUtil.getChildElement("vUnCom", prod.getContent());
			if(vUnCom == null) throw new SinedException("vUnCom n�o encontrado.");
			bean.setValorunitario(Double.valueOf(vUnCom.getText()));
			
			// Valor bruto do produto
			vProd = SinedUtil.getChildElement("vProd", prod.getContent());
			if(vProd == null) throw new SinedException("vProd n�o encontrado.");
			bean.setValorproduto(Double.valueOf(vProd.getText()));
			
			// Valor do frete
			vFrete = SinedUtil.getChildElement("vFrete", prod.getContent());
			if(vFrete != null) bean.setValorfrete(new Money(Double.valueOf(vFrete.getText())));
			
			// Valor do seguro
			vSeg = SinedUtil.getChildElement("vSeg", prod.getContent());
			if(vSeg != null) bean.setValorseguro(new Money(Double.valueOf(vSeg.getText())));
			
			// Valor do desconto
			vDesc = SinedUtil.getChildElement("vDesc", prod.getContent());
			if(vDesc != null) bean.setValordesconto(new Money(Double.valueOf(vDesc.getText())));
			
			// Outras despesas acess�rias
			vOutro = SinedUtil.getChildElement("vOutro", prod.getContent());
			if(vOutro != null) bean.setValoroutrasdespesas(new Money(Double.valueOf(vOutro.getText())));
			
			List<Element> listaRastroElements = SinedUtil.getListChildElement("rastro", prod.getContent());
			List<Rastro> listaRastro = new ArrayList<Rastro>();
			if(SinedUtil.isListNotEmpty(listaRastroElements)){
				
				for(Element el: listaRastroElements){
					Rastro rastro = new Rastro();
					nLote = SinedUtil.getChildElement("nLote", el.getContent());
					if(nLote != null) rastro.setnLote(nLote.getText());
					
					qLote = SinedUtil.getChildElement("qLote", el.getContent());
					if(qLote != null) rastro.setqLote(Double.valueOf(qLote.getText()));
					
					dFab = SinedUtil.getChildElement("dFab", el.getContent());
					dVal = SinedUtil.getChildElement("dVal", el.getContent());
					
					try {
						if(dFab != null)
							rastro.setdFab(new SimpleDateFormat("yyyy-MM-dd").parse(dFab.getText()));
						if(dVal != null)
							rastro.setdVal(new SimpleDateFormat("yyyy-MM-dd").parse(dVal.getText()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					cAgreg = SinedUtil.getChildElement("cAgreg", el.getContent());
					if(cAgreg != null) rastro.setcAgreg(cAgreg.getText());
					
					listaRastro.add(rastro);
				}
			}
			bean.setListaRastro(listaRastro);
			
			imposto = SinedUtil.getChildElement("imposto", det.getContent());
			if(imposto == null) throw new SinedException("imposto n�o encontrado.");
			
			// ISSQN
			iSSQN = SinedUtil.getChildElement("ISSQN", imposto.getContent()); 
			if(iSSQN != null){
				cListServ = SinedUtil.getChildElement("cListServ", iSSQN.getContent()); 
				if(cListServ != null){
					bean.setCListServ(cListServ.getText());
				}
			}
			
			iCMS = SinedUtil.getChildElement("ICMS", imposto.getContent());
			if(iCMS == null) throw new SinedException("ICMS n�o encontrado.");
			
			iCMS00 = SinedUtil.getChildElement("ICMS00", iCMS.getContent());
			iCMS10 = SinedUtil.getChildElement("ICMS10", iCMS.getContent());
			iCMS20 = SinedUtil.getChildElement("ICMS20", iCMS.getContent());
			iCMS30 = SinedUtil.getChildElement("ICMS30", iCMS.getContent());
			iCMS40 = SinedUtil.getChildElement("ICMS40", iCMS.getContent());
			iCMS51 = SinedUtil.getChildElement("ICMS51", iCMS.getContent());
			iCMS60 = SinedUtil.getChildElement("ICMS60", iCMS.getContent());
			iCMS70 = SinedUtil.getChildElement("ICMS70", iCMS.getContent());
			iCMS90 = SinedUtil.getChildElement("ICMS90", iCMS.getContent());
			iCMSPart = SinedUtil.getChildElement("ICMSPart", iCMS.getContent());
			iCMSST = SinedUtil.getChildElement("ICMSST", iCMS.getContent());
			iCMSSN101 = SinedUtil.getChildElement("ICMSSN101", iCMS.getContent());
			iCMSSN102 = SinedUtil.getChildElement("ICMSSN102", iCMS.getContent());
			iCMSSN201 = SinedUtil.getChildElement("ICMSSN201", iCMS.getContent());
			iCMSSN202 = SinedUtil.getChildElement("ICMSSN202", iCMS.getContent());
			iCMSSN500 = SinedUtil.getChildElement("ICMSSN500", iCMS.getContent());
			iCMSSN900 = SinedUtil.getChildElement("ICMSSN900", iCMS.getContent());
			
			orig = null;
			if(iCMS00 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS00.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS00.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS00.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS00.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS00.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS00.getContent());				
			} else if(iCMS10 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS10.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS10.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS10.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMS10.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMS10.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMS10.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCP
				vBCFCP = SinedUtil.getChildElement("vBCFCP", iCMS10.getContent());
				if (vBCFCP != null) bean.setValorBcFcp(new Money(Double.valueOf(vBCFCP.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS10.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS10.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMS10.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMS10.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMS10.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS10.getContent());
			} else if(iCMS20 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS20.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS20.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS20.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				//vBCFCP
				vBCFCP = SinedUtil.getChildElement("vBCFCP", iCMS20.getContent());
				if (vBCFCP != null) bean.setValorBcFcp(new Money(Double.valueOf(vBCFCP.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS20.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS20.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS20.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS20.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS20.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMS30 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO);
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMS30.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMS30.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMS30.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMS30.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMS30.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMS30.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS30.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS30.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS30.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMS40 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				cST = SinedUtil.getChildElement("CST", iCMS40.getContent());
				if(cST == null)  throw new SinedException("CST n�o encontrado");
				String cstStr = cST.getText();
				if(cstStr.equals("40")){
					bean.setCsticms(Tipocobrancaicms.ISENTA);
				} else if(cstStr.equals("41")){
					bean.setCsticms(Tipocobrancaicms.NAO_TRIBUTADA);
				} else if(cstStr.equals("50")){
					bean.setCsticms(Tipocobrancaicms.SUSPENSAO);
				}
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS40.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS40.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS40.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMS51 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.DIFERIMENTO);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS51.getContent());
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS51.getContent());
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS51.getContent());
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				//vBCFCP
				vBCFCP = SinedUtil.getChildElement("vBCFCP", iCMS51.getContent());
				if (vBCFCP != null) bean.setValorBcFcp(new Money(Double.valueOf(vBCFCP.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS51.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS51.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS51.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS51.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS51.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMS60 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO);
				
				//vBCSTRet
				vBCSTRet = SinedUtil.getChildElement("vBCSTRet", iCMS60.getContent());
				if (vBCSTRet != null) bean.setValorBcStRet(new Money(Double.valueOf(vBCSTRet.getText())));
				
				//pST
				pST = SinedUtil.getChildElement("pST", iCMS60.getContent());
				if(pST != null) bean.setSt(Double.valueOf(pST.getText()));
				
				//vICMSSTRet
				vICMSSTRet = SinedUtil.getChildElement("vICMSSTRet", iCMS60.getContent());
				if(vICMSSTRet != null) bean.setValorIcmsStRet(new Money(Double.valueOf(vICMSSTRet.getText())));
				
				//vBCFCPSTRet
				vBCFCPSTRet = SinedUtil.getChildElement("vBCFCPSTRet", iCMS60.getContent());
				if (vBCFCPSTRet != null) bean.setValorBcFcpStRet(new Money(Double.valueOf(vBCFCPSTRet.getText())));
				
				//pFCPSTRet
				pFCPSTRet = SinedUtil.getChildElement("pFCPSTRet", iCMS60.getContent());
				if(pFCPSTRet != null) bean.setFcpStRet(Double.valueOf(pFCPSTRet.getText()));
				
				//vFCPSTRet
				vFCPSTRet = SinedUtil.getChildElement("vFCPSTRet", iCMS60.getContent());
				if(vFCPSTRet != null) bean.setValorFcpStRet(new Money(Double.valueOf(vFCPSTRet.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS60.getContent());
			} else if(iCMS70 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS70.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS70.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS70.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMS70.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMS70.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMS70.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCP
				vBCFCP = SinedUtil.getChildElement("vBCFCP", iCMS70.getContent());
				if (vBCFCP != null) bean.setValorBcFcp(new Money(Double.valueOf(vBCFCP.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS70.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS70.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMS70.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMS70.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMS70.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS70.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS70.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS70.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMS90 != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.OUTROS);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMS90.getContent());
				if(vBC != null){
					bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				}
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMS90.getContent());
				if(pICMS != null){
					bean.setIcms(Double.valueOf(pICMS.getText()));
				}
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMS90.getContent());
				if(vICMS != null){
					bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				}
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMS90.getContent());
				if(vBCST != null){
					bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				}
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMS90.getContent());
				if(pICMSST  != null){
					bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				}
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMS90.getContent());
				if(vICMSST != null){
					bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				}
				
				//vBCFCP
				vBCFCP = SinedUtil.getChildElement("vBCFCP", iCMS90.getContent());
				if (vBCFCP != null) bean.setValorBcFcp(new Money(Double.valueOf(vBCFCP.getText())));
				
				//pFCP
				pFCP = SinedUtil.getChildElement("pFCP", iCMS90.getContent());
				if(pFCP != null) bean.setFcp(Double.valueOf(pFCP.getText()));
				
				//vFCP
				vFCP = SinedUtil.getChildElement("vFCP", iCMS90.getContent());
				if(vFCP != null) bean.setValorFcp(new Money(Double.valueOf(vFCP.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMS90.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMS90.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMS90.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				vICMSDeson = SinedUtil.getChildElement("vICMSDeson", iCMS90.getContent());
				if(vICMSDeson != null) bean.setValorIcmsDeson(new Money(Double.valueOf(vICMSDeson.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMS90.getContent());
				
				motDesICMS = SinedUtil.getChildElement("motDesICMS", iCMS90.getContent());
				if(motDesICMS != null) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(motDesICMS.getText())) {
						switch (Integer.parseInt(motDesICMS.getText())) {
							case 1: bean.setMotDesICMS(Motivodesoneracaoicms.TAXI);break;
							case 2: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_FISICO);break;
							case 3: bean.setMotDesICMS(Motivodesoneracaoicms.PRODUTOR_AGROPECUARIO);break;
							case 4: bean.setMotDesICMS(Motivodesoneracaoicms.FROTISTA_LOCADORA);break;
							case 5: bean.setMotDesICMS(Motivodesoneracaoicms.DIPLOMATICO_CONSULAR);break;
							case 6: bean.setMotDesICMS(Motivodesoneracaoicms.UTILITARIOS_MOTOCICLETAS);break;
							case 7: bean.setMotDesICMS(Motivodesoneracaoicms.SUFRAMA);break;
							case 8: bean.setMotDesICMS(Motivodesoneracaoicms.OUTROS);break;
							case 9: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_PUBLICO);break;
							case 10: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_CONDUTOR);break;
							case 11: bean.setMotDesICMS(Motivodesoneracaoicms.DEFICIENTE_NAO_CONDUTOR);break;
							case 12: bean.setMotDesICMS(Motivodesoneracaoicms.ORGAO_DESENV_AGROPECUARIO);break;
						}
					}
				}
			} else if(iCMSPart != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				cST = SinedUtil.getChildElement("CST", iCMSPart.getContent());
				if(cST == null)  throw new SinedException("CST n�o encontrado");
				String cstStr = cST.getText();
				if(cstStr.equals("10")){
					bean.setCsticms(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA);
				} else if(cstStr.equals("90")){
					bean.setCsticms(Tipocobrancaicms.OUTROS_PARTILHA);
				}
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMSPart.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMSPart.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMSPart.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMSPart.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMSPart.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMSPart.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSPart.getContent());
			} else if(iCMSST != null){
				
				// C�digo de Situa��o Tribut�ria do ICMS
				bean.setCsticms(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO);
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSST.getContent());
			} else if(iCMSSN101 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				bean.setCsticms(Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO);
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN101.getContent());
			} else if(iCMSSN102 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				cSOSN = SinedUtil.getChildElement("CSOSN", iCMSSN102.getContent());
				if(cSOSN == null)  throw new SinedException("CSOSN n�o encontrado");
				String csosnStr = cSOSN.getText();
				if(csosnStr.equals("102")){
					bean.setCsticms(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA);
				} else if(csosnStr.equals("103")){
					bean.setCsticms(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL);
				} else if(csosnStr.equals("300")){
					bean.setCsticms(Tipocobrancaicms.IMUNE);
				} else if(csosnStr.equals("400")){
					bean.setCsticms(Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL);
				}
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN102.getContent());
			} else if(iCMSSN201 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				bean.setCsticms(Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST);
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMSSN201.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMSSN201.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMSSN201.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMSSN201.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMSSN201.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMSSN201.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN201.getContent());
			} else if(iCMSSN202 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				cSOSN = SinedUtil.getChildElement("CSOSN", iCMSSN202.getContent());
				if(cSOSN == null)  throw new SinedException("CSOSN n�o encontrado");
				String csosnStr = cSOSN.getText();
				if(csosnStr.equals("202")){
					bean.setCsticms(Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST);
				} else if(csosnStr.equals("203")){
					bean.setCsticms(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST);
				} 
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMSSN202.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMSSN202.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMSSN202.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMSSN202.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMSSN202.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMSSN202.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN202.getContent());
			} else if(iCMSSN500 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				bean.setCsticms(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE);
				
				//vBCSTRet
				vBCSTRet = SinedUtil.getChildElement("vBCSTRet", iCMSSN500.getContent());
				if (vBCSTRet != null) bean.setValorBcStRet(new Money(Double.valueOf(vBCSTRet.getText())));
				
				//pST
				pST = SinedUtil.getChildElement("pST", iCMSSN500.getContent());
				if(pST != null) bean.setSt(Double.valueOf(pST.getText()));
				
				//vICMSSTRet
				vICMSSTRet = SinedUtil.getChildElement("vICMSSTRet", iCMSSN500.getContent());
				if(vICMSSTRet != null) bean.setValorIcmsStRet(new Money(Double.valueOf(vICMSSTRet.getText())));
				
				//vBCFCPSTRet
				vBCFCPSTRet = SinedUtil.getChildElement("vBCFCPSTRet", iCMSSN500.getContent());
				if (vBCFCPSTRet != null) bean.setValorBcFcpStRet(new Money(Double.valueOf(vBCFCPSTRet.getText())));
				
				//pFCPSTRet
				pFCPSTRet = SinedUtil.getChildElement("pFCPSTRet", iCMSSN500.getContent());
				if(pFCPSTRet != null) bean.setFcpStRet(Double.valueOf(pFCPSTRet.getText()));
				
				//vFCPSTRet
				vFCPSTRet = SinedUtil.getChildElement("vFCPSTRet", iCMSSN500.getContent());
				if(vFCPSTRet != null) bean.setValorFcpStRet(new Money(Double.valueOf(vFCPSTRet.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN500.getContent());
			} else if(iCMSSN900 != null){
				
				// C�digo de Situa��o da Opera��o - Simples Nacional
				bean.setCsticms(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL);
				
				// Base de c�lculo do ICMS
				vBC = SinedUtil.getChildElement("vBC", iCMSSN900.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcicms(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do ICMS
				pICMS = SinedUtil.getChildElement("pICMS", iCMSSN900.getContent());
//				if(pICMS == null) throw new SinedException("pICMS n�o encontrado");
				if(pICMS != null) bean.setIcms(Double.valueOf(pICMS.getText()));
				
				// Valor do ICMS
				vICMS = SinedUtil.getChildElement("vICMS", iCMSSN900.getContent());
//				if(vICMS == null) throw new SinedException("vICMS n�o encontrado");
				if(vICMS != null) bean.setValoricms(new Money(Double.valueOf(vICMS.getText())));
				
				// Base de c�lculo do ICMS ST
				vBCST = SinedUtil.getChildElement("vBCST", iCMSSN900.getContent());
//				if(vBCST == null) throw new SinedException("vBCST n�o encontrado");
				if(vBCST != null) bean.setValorbcicmsst(new Money(Double.valueOf(vBCST.getText())));
				
				// Al�quota do ICMS ST
				pICMSST = SinedUtil.getChildElement("pICMSST", iCMSSN900.getContent());
//				if(pICMSST == null) throw new SinedException("pICMSST n�o encontrado");
				if(pICMSST != null) bean.setIcmsst(Double.valueOf(pICMSST.getText()));
				
				// Valor do ICMS ST
				vICMSST = SinedUtil.getChildElement("vICMSST", iCMSSN900.getContent());
//				if(vICMSST == null) throw new SinedException("vICMSST n�o encontrado");
				if(vICMSST != null) bean.setValoricmsst(new Money(Double.valueOf(vICMSST.getText())));
				
				//vBCFCPST
				vBCFCPST = SinedUtil.getChildElement("vBCFCPST", iCMSSN900.getContent());
				if (vBCFCPST != null) bean.setValorBcFcpSt(new Money(Double.valueOf(vBCFCPST.getText())));
				
				//pFCPST
				pFCPST = SinedUtil.getChildElement("pFCPST", iCMSSN900.getContent());
				if(pFCPST != null) bean.setFcpSt(Double.valueOf(pFCPST.getText()));
				
				//vFCPST
				vFCPST = SinedUtil.getChildElement("vFCPST", iCMSSN900.getContent());
				if(vFCPST != null) bean.setValorFcpSt(new Money(Double.valueOf(vFCPST.getText())));
				
				// Origem do produto
				orig = SinedUtil.getChildElement("orig", iCMSSN900.getContent());
			}
			
			if(orig != null)
				bean.setOrigemProduto(Origemproduto.getEnum(Integer.parseInt(orig.getText())));
			
			iPI = SinedUtil.getChildElement("IPI", imposto.getContent());
			if(iPI != null) {
			
				iPITrib = SinedUtil.getChildElement("IPITrib", iPI.getContent());
				iPINT = SinedUtil.getChildElement("IPINT", iPI.getContent());
				
				if(iPITrib != null){
					
					// C�digo da situa��o tribut�ria do IPI
					cST = SinedUtil.getChildElement("CST", iPITrib.getContent());
					if(cST == null) throw new SinedException("CST n�o encontrado.");
					String cstStr = cST.getText();
					if(cstStr.equals("00")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO);
					} else if(cstStr.equals("49")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_OUTRAS);
					} else if(cstStr.equals("50")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_TRIBUTABA);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO);
					} else if(cstStr.equals("99")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_OUTRAS);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_OUTRAS);
					} 
					
					// Valor da base de c�lculo do IPI
					vBC = SinedUtil.getChildElement("vBC", iPITrib.getContent());
					if(vBC != null) bean.setValorbcipi(new Money(Double.valueOf(vBC.getText())));
					
					// Al�quota do IPI
					pIPI = SinedUtil.getChildElement("pIPI", iPITrib.getContent());
					if(pIPI != null) bean.setIpi(Double.valueOf(pIPI.getText()));
					
					// Valor do IPI
					vIPI = SinedUtil.getChildElement("vIPI", iPITrib.getContent());
					if(vIPI == null) throw new SinedException("vIPI n�o encontrado");
					bean.setValoripi(new Money(Double.valueOf(vIPI.getText())));
					
				} else if(iPINT != null){
					
					// C�digo da situa��o tribut�ria do IPI
					cST = SinedUtil.getChildElement("CST", iPINT.getContent());
					if(cST == null) throw new SinedException("CST n�o encontrado.");
					String cstStr = cST.getText();
					if(cstStr.equals("01")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_TRIBUTADA_ALIQUOTA_ZERO);
					} else if(cstStr.equals("02")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_ISENTA);
					} else if(cstStr.equals("03")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_NAO_TRIBUTADA);
					} else if(cstStr.equals("04")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_IMUNE);
					} else if(cstStr.equals("05")){
						bean.setCstipi(Tipocobrancaipi.ENTRADA_SUSPENSAO);
					} else if(cstStr.equals("51")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_TRIBUTADA_ALIQUOTA_ZERO);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_TRIBUTADA_ALIQUOTA_ZERO);
					} else if(cstStr.equals("52")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_ISENTA);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_ISENTA);
					} else if(cstStr.equals("53")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_NAO_TRIBUTADA);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_NAO_TRIBUTADA);
					} else if(cstStr.equals("54")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_IMUNE);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_IMUNE);
					} else if(cstStr.equals("55")){
//						bean.setCstipi(Tipocobrancaipi.SAIDA_SUSPENSAO);
						bean.setCstipi(Tipocobrancaipi.ENTRADA_SUSPENSAO);
					} 
					
				}
			} else {
				bean.setCstipi(Tipocobrancaipi.ENTRADA_OUTRAS);
			}
			
			pIS = SinedUtil.getChildElement("PIS", imposto.getContent());
			pISAliq = null;
			pISQtde = null;
			pISNT = null;
			pISOutr = null;
			
			if(pIS != null){
				pISAliq = SinedUtil.getChildElement("PISAliq", pIS.getContent());
				pISQtde = SinedUtil.getChildElement("PISQtde", pIS.getContent());
				pISNT = SinedUtil.getChildElement("PISNT", pIS.getContent());
				pISOutr = SinedUtil.getChildElement("PISOutr", pIS.getContent());
			}
			
			if(pISAliq != null){
				
				// C�digo da situa��o tribut�ria do PIS
				cST = SinedUtil.getChildElement("CST", pISAliq.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				if(cstStr.equals("01")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_01);
				} else if(cstStr.equals("02")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_02);
				}
				
				// Base de c�lculo do PIS
				vBC = SinedUtil.getChildElement("vBC", pISAliq.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbcpis(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do PIS
				pPIS = SinedUtil.getChildElement("pPIS", pISAliq.getContent());
				if(pPIS == null) throw new SinedException("pPIS n�o encontrado");
				bean.setPis(Double.valueOf(pPIS.getText()));
				
				// Valor do PIS
				vPIS = SinedUtil.getChildElement("vPIS", pISAliq.getContent());
				if(vPIS == null) throw new SinedException("vPIS n�o encontrado");
				bean.setValorpis(new Money(Double.valueOf(vPIS.getText())));
				
			} else if(pISQtde != null){
				
				// C�digo da situa��o tribut�ria do PIS
				bean.setCstpis(Tipocobrancapis.OPERACAO_03);
				
				// Quantidade vendida
				qBCProd = SinedUtil.getChildElement("qBCProd", pISQtde.getContent());
				if(qBCProd == null) throw new SinedException("qBCProd n�o encontrado");
				bean.setQtdebcpis(Double.valueOf(qBCProd.getText()));
				
				// Al�quota do PIS (em reais)
				vAliqProd = SinedUtil.getChildElement("vAliqProd", pISQtde.getContent());
				if(vAliqProd == null) throw new SinedException("vAliqProd n�o encontrado");
				bean.setValorunidbcpis(Double.valueOf(vAliqProd.getText()));
				
				// Valor do PIS
				vPIS = SinedUtil.getChildElement("vPIS", pISQtde.getContent());
				if(vPIS == null) throw new SinedException("vPIS n�o encontrado");
				bean.setValorpis(new Money(Double.valueOf(vPIS.getText())));
				
			} else if(pISNT != null){
				
				// C�digo da situa��o tribut�ria do PIS
				cST = SinedUtil.getChildElement("CST", pISNT.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				if(cstStr.equals("04")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_04);
				} else if(cstStr.equals("06")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_06);
				} else if(cstStr.equals("07")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_07);
				} else if(cstStr.equals("08")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_08);
				} else if(cstStr.equals("09")){
					bean.setCstpis(Tipocobrancapis.OPERACAO_09);
				}
				
			} else if(pISOutr != null){
				
				// C�digo da situa��o tribut�ria do PIS
				cST = SinedUtil.getChildElement("CST", pISOutr.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				bean.setCstpis(Tipocobrancapis.getTipocobrancapis(cstStr));
				
				// Base de c�lculo do PIS
				vBC = SinedUtil.getChildElement("vBC", pISOutr.getContent());
				if(vBC != null) bean.setValorbcpis(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do PIS
				pPIS = SinedUtil.getChildElement("pPIS", pISOutr.getContent());
				if(pPIS != null) bean.setPis(Double.valueOf(pPIS.getText()));
				
				// Quantidade vendida
				qBCProd = SinedUtil.getChildElement("qBCProd", pISOutr.getContent());
				if(qBCProd != null) bean.setQtdebcpis(Double.valueOf(qBCProd.getText()));
				
				// Al�quota do PIS (em reais)
				vAliqProd = SinedUtil.getChildElement("vAliqProd", pISOutr.getContent());
				if(vAliqProd != null) bean.setValorunidbcpis(Double.valueOf(vAliqProd.getText()));
				
				// Valor do PIS
				vPIS = SinedUtil.getChildElement("vPIS", pISOutr.getContent());
				if(vPIS == null) throw new SinedException("vPIS n�o encontrado");
				bean.setValorpis(new Money(Double.valueOf(vPIS.getText())));
				
			}
			
			cOFINS = SinedUtil.getChildElement("COFINS", imposto.getContent());
			cOFINSAliq = null;
			cOFINSQtde = null;
			cOFINSNT = null;
			cOFINSOutr = null;
			
			if(cOFINS != null){
				cOFINSAliq = SinedUtil.getChildElement("COFINSAliq", cOFINS.getContent());
				cOFINSQtde = SinedUtil.getChildElement("COFINSQtde", cOFINS.getContent());
				cOFINSNT = SinedUtil.getChildElement("COFINSNT", cOFINS.getContent());
				cOFINSOutr = SinedUtil.getChildElement("COFINSOutr", cOFINS.getContent());
			}
			
			if(cOFINSAliq != null){
				
				// C�digo da situa��o tribut�ria do COFINS
				cST = SinedUtil.getChildElement("CST", cOFINSAliq.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				if(cstStr.equals("01")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_01);
				} else if(cstStr.equals("02")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_02);
				}
				
				// Base de c�lculo do COFINS
				vBC = SinedUtil.getChildElement("vBC", cOFINSAliq.getContent());
//				if(vBC == null) throw new SinedException("vBC n�o encontrado");
				if(vBC != null) bean.setValorbccofins(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do COFINS
				pCOFINS = SinedUtil.getChildElement("pCOFINS", cOFINSAliq.getContent());
				if(pCOFINS == null) throw new SinedException("pCOFINS n�o encontrado");
				bean.setCofins(Double.valueOf(pCOFINS.getText()));
				
				// Valor do COFINS
				vCOFINS = SinedUtil.getChildElement("vCOFINS", cOFINSAliq.getContent());
				if(vCOFINS == null) throw new SinedException("vCOFINS n�o encontrado");
				bean.setValorcofins(new Money(Double.valueOf(vCOFINS.getText())));
				
			} else if(cOFINSQtde != null){
				
				// C�digo da situa��o tribut�ria do COFINS
				bean.setCstcofins(Tipocobrancacofins.OPERACAO_03);
				
				// Quantidade vendida
				qBCProd = SinedUtil.getChildElement("qBCProd", cOFINSQtde.getContent());
				if(qBCProd == null) throw new SinedException("qBCProd n�o encontrado");
				bean.setQtdebccofins(Double.valueOf(qBCProd.getText()));
				
				// Al�quota do COFINS (em reais)
				vAliqProd = SinedUtil.getChildElement("vAliqProd", cOFINSQtde.getContent());
				if(vAliqProd == null) throw new SinedException("vAliqProd n�o encontrado");
				bean.setValorunidbccofins(Double.valueOf(vAliqProd.getText()));
				
				// Valor do COFINS
				vCOFINS = SinedUtil.getChildElement("vCOFINS", cOFINSQtde.getContent());
				if(vCOFINS == null) throw new SinedException("vCOFINS n�o encontrado");
				bean.setValorcofins(new Money(Double.valueOf(vCOFINS.getText())));
				
			} else if(cOFINSNT != null){
				
				// C�digo da situa��o tribut�ria do COFINS
				cST = SinedUtil.getChildElement("CST", cOFINSNT.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				if(cstStr.equals("04")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_04);
				} else if(cstStr.equals("06")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_06);
				} else if(cstStr.equals("07")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_07);
				} else if(cstStr.equals("08")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_08);
				} else if(cstStr.equals("09")){
					bean.setCstcofins(Tipocobrancacofins.OPERACAO_09);
				}
				
			} else if(cOFINSOutr != null){
				
				// C�digo da situa��o tribut�ria do COFINS
				cST = SinedUtil.getChildElement("CST", cOFINSOutr.getContent());
				if(cST == null) throw new SinedException("CST n�o encontrado.");
				String cstStr = cST.getText();
				bean.setCstcofins(Tipocobrancacofins.getTipocobrancacofins(cstStr));
				
				// Base de c�lculo do COFINS
				vBC = SinedUtil.getChildElement("vBC", cOFINSOutr.getContent());
				if(vBC != null) bean.setValorbccofins(new Money(Double.valueOf(vBC.getText())));
				
				// Al�quota do COFINS
				pCOFINS = SinedUtil.getChildElement("pCOFINS", cOFINSOutr.getContent());
				if(pCOFINS != null) bean.setCofins(Double.valueOf(pCOFINS.getText()));
				
				// Quantidade vendida
				qBCProd = SinedUtil.getChildElement("qBCProd", cOFINSOutr.getContent());
				if(qBCProd != null) bean.setQtdebccofins(Double.valueOf(qBCProd.getText()));
				
				// Al�quota do COFINS (em reais)
				vAliqProd = SinedUtil.getChildElement("vAliqProd", cOFINSOutr.getContent());
				if(vAliqProd != null) bean.setValorunidbccofins(Double.valueOf(vAliqProd.getText()));
				
				// Valor do COFINS
				vCOFINS = SinedUtil.getChildElement("vCOFINS", cOFINSOutr.getContent());
				if(vCOFINS == null) throw new SinedException("vCOFINS n�o encontrado");
				bean.setValorcofins(new Money(Double.valueOf(vCOFINS.getText())));
				
			}
			
			iI = SinedUtil.getChildElement("II", imposto.getContent());
			
			if (iI != null){
				// Valor da BC do Imposto de Importa��o 
				vBC = SinedUtil.getChildElement("vBC", iI.getContent());
				if(vBC != null) bean.setValorbcii(new Money(Double.valueOf(vBC.getText())));
				
				// Valor do imposto de importa��o
				vII = SinedUtil.getChildElement("vII", iI.getContent());
				if(vII != null) bean.setValorii(new Money(Double.valueOf(vII.getText())));
				
				// Valor de despesas aduaneiras
				vDespAdu = SinedUtil.getChildElement("vDespAdu", iI.getContent());
				if(vDespAdu != null) bean.setValorDespesasAduaneiras(new Money(Double.valueOf(vDespAdu.getText())));
				
				// Valor do IOF
				vIOF = SinedUtil.getChildElement("vIOF", iI.getContent());
				if(vIOF != null) bean.setIof(new Money(Double.valueOf(vIOF.getText())));
			}
			
			lista.add(bean);
		}
		
		return lista;
	}
	
	
	/**
	 * Gera a planilha excel e salva de acordo com o filtro.
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Thiers Euler
	 */
	public Resource gerarExcelEmitircurvaabc(EmitircurvaabcFiltro filtro, WebRequestContext request) throws IOException{
		
		List<EmitircurvaabcBean> lista = null;
		
		lista = this.listEntregaForCurvaABC(filtro, false);
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));
        
        HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) 6));
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Relat�rio Curva ABC");

		HSSFRow row = null;
		HSSFCell cell = null;
		
		Calendar dtDe = Calendar.getInstance();
		Calendar dtAte = Calendar.getInstance();
		dtDe.setTimeInMillis(filtro.getDtInicio().getTime());
		dtAte.setTimeInMillis(filtro.getDtFim() != null ? filtro.getDtFim().getTime() : new Date(System.currentTimeMillis()).getTime());
		
		Calendar dtMenorCalAux = Calendar.getInstance();
		dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
		
		int meses = adicionaCabecalhosRelatorioExcel(planilha, row, cell, filtro, dtMenorCalAux);
		boolean classificacaovalor = ClassificacaoCurvaAbc.VALOR.equals(filtro.getClassificacao());
		
		criaTabelaComDadosRelatorioExcel(planilha, row, cell, dtMenorCalAux, dtDe, meses, lista, classificacaovalor);
		adicionaTotaisRelatorioExcel(planilha, row, cell, meses, classificacaovalor);
		
		//Desbloqueando o bot�o para gera��o de novo relat�rio
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		
		return wb.getWorkBookResource("curvaabc.xls");
	}
	/**
	 * Preenche os dados na planilha excel.
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param dtMenorCalAux
	 * @param dtDe
	 * @param meses
	 * @param lista
	 * @author Thiers Euler
	 */
	private void criaTabelaComDadosRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, Calendar dtMenorCalAux, Calendar dtDe, int meses, List<EmitircurvaabcBean> lista, boolean classificacaovalor) {
		EmitircurvaabcBean aux;
		Integer rowPai = planilha.getLastRowNum();
		String valorCell;
		Double total = 0d;
		for(EmitircurvaabcBean emitircurvaabcBean : lista){
			for (int k = 4; k <= planilha.getLastRowNum()+1; k++){
				if(planilha.getRow(k)== null){
					rowPai = planilha.getLastRowNum()+1;
					row = planilha.createRow(rowPai);
					cell = row.createCell((short) 0);
					cell.setCellValue(emitircurvaabcBean.getMaterial());
					break;
				}else{
					valorCell = planilha.getRow(k).getCell((short) 0).getStringCellValue();
					if(emitircurvaabcBean.getMaterial().equals(valorCell)){
						rowPai = k;
						break;
					}else{
						continue;
					}
				}
			}
			 
				aux = emitircurvaabcBean;
				dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
				
				total += classificacaovalor ? aux.getValorConsumo() : aux.getQtde();
				
				Integer mes = SinedDateUtils.getDateProperty(aux.getDtinicio(), Calendar.MONTH)+1;
				Integer ano = SinedDateUtils.getDateProperty(aux.getDtinicio(), Calendar.YEAR);
				//Coloca a somat�ria dos valores de acordo com os meses para cada item.
				row = planilha.getRow(rowPai);
				for (int i = 1; i <= meses; i++) {
					HSSFCell cellValor = row.getCell((short) i);
					if(cellValor != null){
						if(aux != null && dtMenorCalAux.get(Calendar.MONTH)+1 == mes && dtMenorCalAux.get(Calendar.YEAR) == ano){
							double valor = cellValor.getNumericCellValue();
							valor += classificacaovalor ? aux.getValorConsumo() : aux.getQtde();
							cellValor.setCellValue(valor);
						}
					}else if (cellValor == null){
						if(aux != null && dtMenorCalAux.get(Calendar.MONTH)+1 == mes && dtMenorCalAux.get(Calendar.YEAR) == ano){
							cellValor = row.createCell((short) i);
							if(classificacaovalor){
								cellValor.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
								cellValor.setCellValue(aux.getValorConsumo());
							}else {
								cellValor.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
								cellValor.setCellValue(aux.getQtde());
							}
						}else{
							cellValor = row.createCell((short) i);
							if(classificacaovalor){
								cellValor.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
								cellValor.setCellValue(0.0);
							}else {
								cellValor.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
								cellValor.setCellValue(0);
							}
						}
					}
					dtMenorCalAux.add(Calendar.MONTH, 1);
				}
				//Coloca os valores totais 
				row = planilha.getRow(rowPai);
				for (int i = 0; i < meses; i++) {
					cell = row.getCell((short) (meses+1));
					if(cell == null){
						cell = row.createCell((short) (meses+1));
						if(classificacaovalor){
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
						}else {
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
						}
						cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1+i)+(rowPai+1)+")");
					}else{
						cell = row.createCell((short) (meses+1));
						if(classificacaovalor){
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
						}else {
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
						}
						cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1)+(rowPai+1) + ":" + SinedExcel.getAlgarismoColuna(1+i)+(rowPai+1)+")");
					}
				}
			}
		}
		
		
//	}
	
	private void adicionaTotaisRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, int meses, boolean classificacaovalor) {
		row = planilha.createRow(planilha.getLastRowNum()+2);
		
		cell = row.createCell((short) (meses));
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
		cell.setCellValue("TOTAL");

		cell = row.createCell((short) (meses+1));
		if(classificacaovalor){
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		}else {
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_RIGHT);
		}
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(meses+1)+(planilha.getFirstRowNum()+6)+":"+SinedExcel.getAlgarismoColuna(meses+1)+(planilha.getLastRowNum())+")");
	}
	
	private int adicionaCabecalhosRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, EmitircurvaabcFiltro filtro, Calendar dtMenorCalAux) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
		row = planilha.createRow(4);
		
		SinedExcel.createHeaderCell(row, 0, "Material");
		
		Date dtInicioAux = SinedDateUtils.firstDateOfMonth(filtro.getDtInicio());
		Date dtFimAux = filtro.getDtFim() != null ? SinedDateUtils.lastDateOfMonth(filtro.getDtFim()) : SinedDateUtils.lastDateOfMonth();
		
		int meses = SinedDateUtils.mesesEntre(dtInicioAux, dtFimAux) + 1;
		
		for (int i = 0; i < meses; i++) {
			cell = row.createCell((short) (i+1));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue(dateFormat.format(dtMenorCalAux.getTime()));
			dtMenorCalAux.add(Calendar.MONTH, 1);
		}
		
		//Cabe�alho totalizador Total
		row = planilha.getRow(4);
		SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "Total");	
		
		return meses;
	}
	
	public List<EmitircurvaabcBean> listEntregaForCurvaABC(EmitircurvaabcFiltro filtro, boolean forPdf) {
		List<EmitircurvaabcBean> listacurvaabc = entregaDAO.listEntregaForCurvaABC(filtro, forPdf);
		if(SinedUtil.isListNotEmpty(listacurvaabc)){
			if (ClassificacaoCurvaAbc.VALOR.equals(filtro.getClassificacao())) {
				Collections.sort(listacurvaabc,
						new Comparator<EmitircurvaabcBean>() {
							public int compare(EmitircurvaabcBean e1,
									EmitircurvaabcBean e2) {
								return e2.getValorConsumo().compareTo(
										e1.getValorConsumo());
							}
						});
			} else if(ClassificacaoCurvaAbc.QUANTIDADE.equals(filtro.getClassificacao())){
				Collections.sort(listacurvaabc,
						new Comparator<EmitircurvaabcBean>() {
							public int compare(EmitircurvaabcBean e1,
									EmitircurvaabcBean e2) {
								return e2.getQtde().compareTo(e1.getQtde());
							}
						});
			}
		}
		return listacurvaabc;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForCsv
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findForCsv(EntregaFiltro filtro){
		return entregaDAO.findForCsv(filtro);
	}
	
	public List<Entrega> findByEntrega(String whereIn){
		return entregaDAO.findByEntrega(whereIn);
	}
	
	public List<Entrega> findForWms(String ids) {
		return entregaDAO.findForWms(ids);
	}
	
//	public String getValorCampoImagem(String valor){
//		valor = valor.replace("<br />", "");
//		if (valor.indexOf("GeraImagem.ashx?text=") > -1){
//			String hash = valor.substring(valor.indexOf("GeraImagem.ashx?text=") + 21, valor.indexOf("\"", valor.indexOf("GeraImagem.ashx?text=")));
//			return this.getUrlBuscaValor(hash);
//		} else {
//			return valor;
//		}
//	}
	
	private TipoConsulta getTipoConsulta(String chaveacesso) throws Exception{
		if(chaveacesso != null && chaveacesso.length() > 22){
			String modelo = chaveacesso.substring(20,22);
			if(modelo.equals("55")){
				return TipoConsulta.NFE;
			} else if(modelo.equals("57")){
				return TipoConsulta.CTE;
			} else{
				throw new Exception("Chave de acesso inv�lida.");
			}
		}
		return null;
	}
	
	/**M�todo que faz a busca da nota fiscal e processa os dados
	 * @author Thiago Augusto
	 * @param chaveacesso
	 * @param html 
	 */
	public ImportacaoXmlNfeBean consultarNotaFiscalEletronica(String chaveacesso, String html){
		ImportacaoXmlNfeBean bean = new ImportacaoXmlNfeBean();
		try {
			TipoConsulta tipoConsulta = getTipoConsulta(chaveacesso);

			if (html != null){
				bean.setTipoconsulta(tipoConsulta.ordinal());
				if(tipoConsulta.equals(TipoConsulta.NFE)){
					ProcessadorNfe pnfe = new ProcessadorNfe();
					//Caso for inserir grupos/campos novos, verifique se tem no WEB-INF/processador/arquivoRegraNfe
					if(pnfe.isLeiauteNovo(html)){
						try{
							bean = pnfe.processarHTML();
						}catch (Exception e) {
							throw e;
						}
					} else{
						pnfe = null;
						
						String divDadosNfe = "ContentPlaceHolder1_tcnConsultaCompleta_tpnNFe";
						String divEmitente = "ContentPlaceHolder1_tcnConsultaCompleta_ptnEmitente";
						String divDestinatario = "ContentPlaceHolder1_tcnConsultaCompleta_tpnDestinatario";
						String divProdutosServicos = "ContentPlaceHolder1_tcnConsultaCompleta_tpnProdutosServicos";
						String divTotais = "ContentPlaceHolder1_tcnConsultaCompleta_tpnTotais";
						String divComercioExterior = "ContentPlaceHolder1_tcnConsultaCompleta_tpnComex";
						String divTransporte = "ContentPlaceHolder1_tcnConsultaCompleta_tpnTransporte";
						String divCobranca = "ContentPlaceHolder1_tcnConsultaCompleta_tpnCobranca";
						String divInfAdicionais = "ContentPlaceHolder1_tcnConsultaCompleta_tpnInfoAdicionais";
					
						int indexOfContent = html.indexOf("ContentPlaceHolder1_tcnConsultaCompleta_body");
						
						if(indexOfContent == -1){
							lancarErroConsultaNotaFiscalEletronica(html);
						}
						
						html = html.substring(indexOfContent);
					
						this.addDadosNfe(bean, html, html.indexOf(divDadosNfe), html.indexOf(divEmitente));
						this.addEmitente(bean, html,  html.indexOf(divEmitente), html.indexOf(divDestinatario));
						this.addDestinatario(bean, html,  html.indexOf(divDestinatario), html.indexOf(divProdutosServicos));
						this.addProdutosServicos(bean, html,  html.indexOf(divProdutosServicos), html.indexOf(divTotais));
						this.addTotais(bean, html,  html.indexOf(divTotais), html.indexOf(divComercioExterior));
						this.addTransporte(bean, html,  html.indexOf(divTransporte), html.indexOf(divCobranca));
						this.addInfAdicionais(bean, html,  html.indexOf(divInfAdicionais), html.length());
					}
					
					bean.setModelodocumentofiscal(modelodocumentofiscalService.loadNotafiscalprodutoeletronica());
				} else if(tipoConsulta.equals(TipoConsulta.CTE)){
					ProcessadorCte pcte = new ProcessadorCte();
					if(pcte.isLeiauteNovo(html)){
						bean = pcte.processarHTML();
					}
					bean.setModelodocumentofiscal(modelodocumentofiscalService.loadConhecimentotransporteeletronico());
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException(e.getMessage());
		}
		
		return bean;
	}
	
	public static void lancarErroConsultaNotaFiscalEletronica(String html) throws Exception{
		int indexOfErroPanel = html.indexOf("ContentPlaceHolder1_bltMensagensErro");
		if(indexOfErroPanel != -1){
			html = html.substring(indexOfErroPanel);
			int indexOfFechamentoErro = html.indexOf("</ul>");
			
			html = html.substring(0, indexOfFechamentoErro);
			
			Pattern pattern = Pattern.compile("<li>(.*)</li>", Pattern.MULTILINE);
			Matcher matcher = pattern.matcher(html);
			
			List<String> listaErro = new ArrayList<String>();
			while(matcher.find()){
				listaErro.add(matcher.group(1));
			}
			
			for (String string : listaErro) {
				NeoWeb.getRequestContext().addError(string);
			}
			throw new Exception("Problema na consulta � nota atrav�s do o site da receita.");
		}
		throw new Exception("Problema na consulta � nota atrav�s do o site da receita, favor verificar se a chave de acesso est� correta ou fa�a o upload do xml para resultados mais satisfat�rios.");
	}
	
	/**
	 * M�todo que adiciona informa��es da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @throws ParseException
	 * @author Luiz Fernando
	 */
	private void addDadosNfe(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) throws ParseException {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			table = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			table = table.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher trmatcher = PATTERN_SPANTAG.matcher(table);
			while (trmatcher.find()) {
				nomeCampo = trmatcher.group(3).replace("<br />", "").trim();
				if (nomeCampo.equals("N�mero")){
					trmatcher.find();
					bean.setNumero(trmatcher.group(3).trim());
				} else if (nomeCampo.equals("S�rie")){
					trmatcher.find();
					bean.setSerie(Integer.parseInt(trmatcher.group(3).trim()));
				} else if (nomeCampo.equals("Data de emiss�o")){
					trmatcher.find();
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
					Date data = new Date(format.parse(trmatcher.group(3)).getTime());
					bean.setDtemissao(data);
				} else if (nomeCampo.equals("Forma de Pagamento")){
					trmatcher.find();
					String [] string = trmatcher.group(3).trim().split(" - ");
					bean.setIndpag(Formapagamentonfe.values()[Integer.parseInt(string[0].trim())]);
				} else if (nomeCampo.equals("Natureza da Opera��o")){
					trmatcher.find();
					bean.setNatop(trmatcher.group(3).trim());
				}
			}
		}
		
	}
	
	/**
	 * M�todo que adiciona informa��es do emitente da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addEmitente(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			table = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			table = table.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher trmatcher = PATTERN_SPANTAG.matcher(table);
			Boolean achoCnpj = Boolean.FALSE;
			Boolean achoFoneFax = Boolean.FALSE;
			Boolean achoIE = Boolean.FALSE;
			while (trmatcher.find()) {
				nomeCampo = trmatcher.group(3).replace("<br />", "").trim();
				if (nomeCampo.equals("Nome / Raz�o Social")){
					trmatcher.find();
					bean.setRazaosocial(trmatcher.group(3).trim());
				} else
				if (nomeCampo.equals("Nome Fantasia")){
					trmatcher.find();
					bean.setNomefantasia(trmatcher.group(3).trim());
				} else
				if (nomeCampo.equals("CNPJ") && !achoCnpj){
					trmatcher.find();
					Cnpj cnpj = new Cnpj(trmatcher.group(3).trim());
					bean.setCnpj(cnpj);
					achoCnpj = Boolean.TRUE;
				}  else if (nomeCampo.replaceAll(" ", "").toUpperCase().equals("Fone/Fax".replaceAll(" ", "").toUpperCase()) && !achoFoneFax){
					trmatcher.find();
					bean.setFone(trmatcher.group(3).trim());
					achoFoneFax = Boolean.TRUE;
				} else if (nomeCampo.equals("Inscri��o Estadual") && !achoIE){
					trmatcher.find();
					bean.setIe(trmatcher.group(3).trim().replace(" ", ""));
					achoIE = Boolean.TRUE;
				}
			}
		}
	}
	
	/**
	 * M�todo que adiciona informa��es do destinat�rio da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addDestinatario(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			table = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			table = table.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher trmatcher = PATTERN_SPANTAG.matcher(table);
			Boolean achoCnpj = Boolean.FALSE;
			while (trmatcher.find()) {
				nomeCampo = trmatcher.group(3).replace("<br />", "").trim();
				if (nomeCampo.equals("CNPJ") && !achoCnpj){
					trmatcher.find();
					Cnpj cnpj = new Cnpj(trmatcher.group(3).trim());
					bean.setEmpresa(empresaService.carregaEmpresaByCnpj(cnpj));
					achoCnpj = Boolean.TRUE;
				}
			}
		}
	}
	
	/**
	 * M�todo que adiciona informa��es dos itens da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addProdutosServicos(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String tabelaProduto = "";
		String nomeCampo = "";
		
		Money basecalculo = null;
		Double aliquota = null;
		List<ImportacaoXmlNfeItemBean> listaItemBean = new ArrayList<ImportacaoXmlNfeItemBean>();
		
		if (posicaoInicial > 0){
			tabelaProduto = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			tabelaProduto = tabelaProduto.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher spanMatcher = PATTERN_SPANTAG.matcher(tabelaProduto);
			ImportacaoXmlNfeItemBean itemBean;
			int sequencial = 0;
			while (spanMatcher.find()) {
				nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
				itemBean = new ImportacaoXmlNfeItemBean();
				
				itemBean.setSequencial(sequencial);
				sequencial++;
				
				while (!nomeCampo.equals("Num.")){
					nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
					if (nomeCampo.equals("Valor(R$)")){
						spanMatcher.find();
						spanMatcher.find();
						
						itemBean.setXprod(spanMatcher.group(3).trim());
						
						spanMatcher.find();
						itemBean.setQtde(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", ".")));
						
						spanMatcher.find();
						itemBean.setUnidademedidacomercial(spanMatcher.group(3).trim());
						
						spanMatcher.find();
						itemBean.setValorproduto(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", ".")));
						
					} else if (nomeCampo.equals("C�digo do Produto")){
						spanMatcher.find();
						itemBean.setCprod(spanMatcher.group(3).trim());
					} else if (nomeCampo.equals("C�digo NCM")){
						spanMatcher.find();
						itemBean.setNcm(spanMatcher.group(3).trim());
					} else if (nomeCampo.equals("CFOP")){
						spanMatcher.find();
						itemBean.setCfop(cfopService.findByCodigo(spanMatcher.group(3).trim()));
					} else if (nomeCampo.equals("C�digo EAN Comercial")){
						spanMatcher.find();
						itemBean.setCean(spanMatcher.group(3).trim());
					} else if (nomeCampo.equals("Unidade Comercial")){
						spanMatcher.find();
						itemBean.setUnidademedidacomercial(spanMatcher.group(3).trim());
					} else if (nomeCampo.equals("Quantidade Comercial")){
						spanMatcher.find();
						itemBean.setQtde(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", ".")));
					} else if (nomeCampo.equals("Valor unit�rio de comercializa��o")){
						spanMatcher.find();
						itemBean.setValorunitario(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", ".")));
					} else if (nomeCampo.equals("Tributa��o do ICMS")){
						spanMatcher.find();
						String [] string = spanMatcher.group(3).trim().split(" - ");
						itemBean.setCsticms(Tipocobrancaicms.getTipocobrancaicms(string[0].trim()));
					} else if (nomeCampo.contains("Base de C�lculo") || nomeCampo.contains("Valor da BC")){
						spanMatcher.find();
						basecalculo = new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", ".")));
						
						spanMatcher.find();
						nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
						if (nomeCampo.contains("Al�quota") || nomeCampo.contains("FCP")){
							spanMatcher.find();
							aliquota = Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."));
						}
						
						spanMatcher.find();						
						nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
						if (nomeCampo.equals("Valor do ICMS ST")){
							if (itemBean.getCsticms().equals("60") || itemBean.getCsticms().equals("500")) {
								itemBean.setValorBcStRet(basecalculo);
								itemBean.setSt(aliquota);
								
								spanMatcher.find();	
								itemBean.setValorIcmsStRet(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
							} else {
								itemBean.setValorbcicmsst(basecalculo);
								itemBean.setIcmsst(aliquota);
								
								spanMatcher.find();	
								itemBean.setValoricmsst(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
							}
						} else if (nomeCampo.equals("Valor do ICMS Normal") || nomeCampo.equals("Valor")){
							itemBean.setValorbcicms(basecalculo);
							itemBean.setIcms(aliquota);
							
							spanMatcher.find();	
							itemBean.setValoricms(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
						} else if (nomeCampo.equals("Valor IPI")){
							itemBean.setValorbcipi(basecalculo);
							itemBean.setIpi(aliquota);
							
							spanMatcher.find();	
							itemBean.setValoripi(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
						} else if (nomeCampo.equals("Valor do Fundo de Combate � Pobreza (FCP)")) {
							itemBean.setValorBcFcp(basecalculo);
							itemBean.setFcp(aliquota);
							
							spanMatcher.find();	
							itemBean.setValorFcp(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
						} else if (nomeCampo.equals("Valor do FCP retido por Substitui��o Tribut�ria")) {
							if (itemBean.getCsticms().equals("60") || itemBean.getCsticms().equals("500")) {
								itemBean.setValorBcFcpStRet(basecalculo);
								itemBean.setFcpStRet(aliquota);
								
								spanMatcher.find();	
								itemBean.setValorFcpStRet(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
							} else {
								itemBean.setValorBcFcpSt(basecalculo);
								itemBean.setFcpSt(aliquota);
								
								spanMatcher.find();	
								itemBean.setValorFcpSt(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
							}
						}
					} else if(nomeCampo.equals("Qtd Total Unidade Padr�o")){
						spanMatcher.find();
						spanMatcher.find();
						nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
						if (nomeCampo.equals("Valor IPI")){
							spanMatcher.find();	
							itemBean.setValoripi(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
						}
					} else if (nomeCampo.equals("Percentual do MVA do ICMS ST")){
						spanMatcher.find();
						itemBean.setValormva(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
					} else if (nomeCampo.equals("CST")){
						spanMatcher.find();
						String [] string = spanMatcher.group(3).split(" - ");
						itemBean.setCstipi(Tipocobrancaipi.getTipocobrancaipi(string[0].trim()));
					} else if (nomeCampo.equals("Valor do Frete")){
						spanMatcher.find();
						itemBean.setValorfrete(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
					} else if (nomeCampo.equals("Valor do Desconto")){
						spanMatcher.find();
						itemBean.setValordesconto(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
					} else if (nomeCampo.equals("Outras despesas acess�rias")){
						spanMatcher.find();
						itemBean.setValoroutrasdespesas(new Money(Double.parseDouble(spanMatcher.group(3).trim().replace(".", "").replace(",", "."))));
					}
					
					if (spanMatcher.find()) {
						nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
					} else {
						break;
					}
					
				} 
				while (!nomeCampo.equals("Unidade Comercial")){
					if(spanMatcher.find()){
						nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
					} else{
						break;
					}
				}
				if(itemBean != null && itemBean.getXprod() != null){
					listaItemBean.add(itemBean);
					itemBean = new ImportacaoXmlNfeItemBean();
				}
			}
			bean.setListaItens(listaItemBean);
		}
		
	}
	
	/**
	 * M�todo que adiciona os totais da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addTotais(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			String tabela = "";
			tabela = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			table = tabela.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher spanMatcher = PATTERN_SPANTAG.matcher(table);
			while (spanMatcher.find()) {
				nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
				if (nomeCampo.equals("Base de C�lculo ICMS")){
					spanMatcher.find();
					bean.setTotalbcicms(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor do ICMS")){
					spanMatcher.find();
					bean.setTotalicms(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Base de C�lculo ICMS ST")){
					spanMatcher.find();
					bean.setTotalbcicmsst(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor do PIS")){
					spanMatcher.find();
					bean.setTotalpis(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor da COFINS")){
					spanMatcher.find();
					bean.setTotalcofins(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor do Frete")){
					spanMatcher.find();
					bean.setTotalfrete(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor do Seguro")){
					spanMatcher.find();
					bean.setTotalseguro(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Outras Despesas Acess�rias")){
					spanMatcher.find();
					bean.setTotaloutrasdespesas(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor Total do IPI")){
					spanMatcher.find();
					bean.setTotalipi(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor Total dos Descontos")){
					spanMatcher.find();
					bean.setTotaldesconto(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor do PIS")){
					spanMatcher.find();
					bean.setTotalpis(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor da COFINS")){
					spanMatcher.find();
					bean.setTotalcofins(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor Total do FCP")) {
					spanMatcher.find();
					bean.setTotalFcp(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor Total do FCP retido por ST")) {
					spanMatcher.find();
					bean.setTotalFcpSt(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				} else if (nomeCampo.equals("Valor Total do FCP retido anteriormente por ST")) {
					spanMatcher.find();
					bean.setTotalFcpStRet(new Money(Double.parseDouble(spanMatcher.group(3).replace(".", "").replace(",", "."))));
				}
			}
		}
		
	}
	
	/**
	 * M�todo que adicona informa��es de transporte da nfe
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addTransporte(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			table = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			table = table.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher spanMatcher = PATTERN_SPANTAG.matcher(table);
			while (spanMatcher.find()) {
				nomeCampo = spanMatcher.group(3).replace("<br />", "").replace("<td valign=\"top\" width=\"33%\"><span class=\"TextoFundoBrancoNegrito\">", "").trim();
				if (nomeCampo.equals("Modalidade do Frete")){
					spanMatcher.find();
					if(!spanMatcher.group(3).trim().equals("")){
						String [] string = spanMatcher.group(3).replaceAll(" ", "").split("-"); 
						ResponsavelFrete responsavelFrete = ResponsavelFrete.getBean(Integer.parseInt(string[0].trim()));
						bean.setResponsavelFrete(responsavelFrete);
					}
				}
				else if (nomeCampo.equals("Raz�o Social / Nome")){
					spanMatcher.find();
					bean.setTransportadora(spanMatcher.group(3).trim());
				}
				else if (nomeCampo.equals("Placa")){
					spanMatcher.find();
					bean.setPlacaveiculo(spanMatcher.group(3).trim());
				}
			}
		}
		
	}
	
	/**
	 * M�todo que adiciona informa��es adicionais da nfe 
	 *
	 * @param bean
	 * @param html
	 * @param posicaoInicial
	 * @param posicaoFinal
	 * @author Luiz Fernando
	 */
	private void addInfAdicionais(ImportacaoXmlNfeBean bean, String html, Integer posicaoInicial, Integer posicaoFinal) {
		String table = "";
		String nomeCampo = "";
		
		if (posicaoInicial > 0){
			String tabela = html.substring(posicaoInicial, posicaoFinal != null && posicaoFinal > 0 ? posicaoFinal : html.length());
			int posInicial = tabela.indexOf("<table");
			int posFinal = tabela.indexOf("</form>", posInicial);
			tabela = tabela.substring(posInicial, posFinal);
			table = table.replaceAll("\\r?\\n", "").replaceAll("\t", "");
			Matcher spanMatcher = PATTERN_SPANTAG.matcher(tabela);
			
			int i = 0;
			while (spanMatcher.find()){
				nomeCampo = spanMatcher.group(3).replace("<br />", "").trim();
				if (nomeCampo.equals("Descri��o")){
					if(i == 0){
						spanMatcher.find();
						bean.setInfcpl(spanMatcher.group(3).trim());
						i++;
					}else {
						spanMatcher.find();
						bean.setInfadfisco(spanMatcher.group(3).trim());
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findRateioOrdemcompraByEntrega(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findRateioOrdemcompraByEntrega(String whereIn) {
		return entregaDAO.findRateioOrdemcompraByEntrega(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findRateioByEntrega(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findRateioByEntrega(String whereIn) {
		return entregaDAO.findRateioByEntrega(whereIn);
	}
	
	/**
	 * M�todo que verirfica se no rateio da ordem de compra existem projetos diferentes
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isProjetosDiferentesRateioOrdemcompra(Ordemcompra ordemcompra) {
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		if(ordemcompra != null && ordemcompra.getRateio() != null && ordemcompra.getRateio().getListaRateioitem() != null && !ordemcompra.getRateio().getListaRateioitem().isEmpty()){
			for(Rateioitem rateioitem : ordemcompra.getRateio().getListaRateioitem()){
				if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
					if(listaProjeto != null && !listaProjeto.isEmpty()){
						for(Projeto projeto : listaProjeto){
							if(!rateioitem.getProjeto().equals(projeto)){
								return Boolean.TRUE;
							}
						}
					}else {
						listaProjeto.add(rateioitem.getProjeto());
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que retorna os projetos com os respectivos percentuais para o calculo da quantidade do material
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> getPercentuaisProjeto(Ordemcompra ordemcompra) {
		List<Projeto> lista = new ArrayList<Projeto>();
		boolean achou = false;
		Projeto projeto;
		if(ordemcompra != null && ordemcompra.getRateio() != null && ordemcompra.getRateio().getListaRateioitem() != null && !ordemcompra.getRateio().getListaRateioitem().isEmpty()){
			for(Rateioitem rateioitem : ordemcompra.getRateio().getListaRateioitem()){
				if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
					if(lista != null && !lista.isEmpty()){
						for(Projeto p : lista){
							if(p.equals(rateioitem.getProjeto())){
								p.setPercentual(p.getPercentual()+rateioitem.getPercentual());
								achou = true;
							}
						}
					}
					
					if(!achou){
						projeto = rateioitem.getProjeto();
						projeto.setPercentual(rateioitem.getPercentual());
						lista.add(projeto);
					}
					achou = false;
					
				}
			}
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findEntregaMenorDataEntradaByMaterial(Material material, String whereIn)
	 *
	 * @param material
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param cdentregadocumento 
	 */
	public String findEntregaMenorDataEntradaByMaterial(Material material, String whereIn, Integer cdentregadocumento) {
		return entregaDAO.findEntregaMenorDataEntradaByMaterial(material, whereIn, cdentregadocumento);
	}
	
	/**
	 * M�todo que calcula a quantidade utilizada ao salvar a notafiscalproduto
	 *
	 * @param item
	 * @author Luiz Fernando
	 */
	public void ajustaQtdeutilizada(Notafiscalprodutoitem item) {
		if(item != null && item.getMaterial() != null){	
			String whereIn;
			Entregadocumento entregadocumento;
			Double qtdenfp = item.getQtde() != null ? item.getQtde() : 1.0;
			
			whereIn = this.findEntregaMenorDataEntradaByMaterial(item.getMaterial(), null, item.getCdentregadocumento());
			if(whereIn != null && !"".equals(whereIn)){
				String[] IdEntrega = whereIn.split(",");
				
				if(IdEntrega != null && IdEntrega.length > 0){
					Double bcicmsst = 0.0;
					Double totalbcicmsst = 0.0;
					Double icmsst = 0.0;
					Double qtdesuficiente = 0.0;
					Double qtdeutilizada;
					Integer cdentregamaterial = null;
					Double qtdematerial = 1.0;
					Double qtdeutilizadamaterial = 0.0;
					
					for(int i = 0; i < IdEntrega.length; i++){
						entregadocumento = entradafiscalService.findEntregadocumentoComSaldo(Integer.parseInt(IdEntrega[i]));
						qtdeutilizada = 0.0;
						if(entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
							for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
								if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getCdmaterial().equals(item.getMaterial().getCdmaterial())){
									cdentregamaterial = entregamaterial.getCdentregamaterial();
									if(entregamaterial.getQtde() != null) qtdematerial = entregamaterial.getQtde();
									if(entregamaterial.getQtdeutilizada() != null) qtdeutilizadamaterial = entregamaterial.getQtdeutilizada();
									qtdesuficiente += (qtdematerial-qtdeutilizadamaterial);
									if(qtdenfp > (qtdematerial-qtdeutilizadamaterial)){
										qtdeutilizada =  (qtdematerial-qtdeutilizadamaterial);
									}else if(qtdenfp <= (qtdematerial-qtdeutilizadamaterial)){
										qtdeutilizada =  qtdenfp;
									}									
									if(entregamaterial.getValorbcicmsst() != null && entregamaterial.getValorbcicmsst().getValue().doubleValue() > 0){
										bcicmsst = entregamaterial.getValorbcicmsst().getValue().doubleValue()/qtdematerial;
										totalbcicmsst += bcicmsst*qtdeutilizada;
									
										if(entregamaterial.getIcmsst() != null){
											icmsst = entregamaterial.getIcmsst()*totalbcicmsst;
											if(icmsst != 0){
												icmsst = icmsst/100;
											}
										}
									}
									break;
								}
							}
							if(cdentregamaterial != null && qtdeutilizada > 0){
								Entregamaterial em = entregamaterialService.findQtdeutilizada(cdentregamaterial);
								if(em != null){
									if(em.getQtdeutilizada() != null)
										qtdeutilizada += em.getQtdeutilizada();									
									entregamaterialService.atualizaQtdeutilizada(cdentregamaterial, qtdeutilizada);
								}
							}
							cdentregamaterial = null;
							qtdeutilizada = 0.0;
							
						}
						if(qtdesuficiente >= qtdenfp){
							break;
						}else {
							qtdenfp = qtdenfp-qtdeutilizada;
						}
					}
				}
			}
		}	
	}
	
	/**
	 * M�todo que c�lcula a qtde entregue do material
	 *
	 * @param entregamaterial
	 * @param listaEntregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQtdeTotalMaterial(Entregamaterial entregamaterial,	List<Entregadocumento> listaEntregadocumento) {
		Double qtdetotal  = 0.0;
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty() && entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getCdmaterial() != null){
			for(Entregadocumento ed : listaEntregadocumento){
				if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
					for(Entregamaterial em : ed.getListaEntregamaterial()){
						if(em.getMaterial() != null && em.getMaterial().getCdmaterial() != null && 
								entregamaterial.getMaterial().getCdmaterial().equals(em.getMaterial().getCdmaterial())){
							qtdetotal += em.getQtde() != null ? em.getQtde() : 1.0;  
						}
					}
					
				}
			}
		}
		return qtdetotal;
	}

	public void salvaArquivo(Entrega bean, Arquivo arquivoxml) {
		if(arquivoxml != null && bean != null){
			bean.setArquivoxml(arquivoxml);
			if(arquivoxml.getCdarquivo() != null){
				this.updateCdarquivo(bean, bean.getArquivoxml());
			}
			arquivoDAO.saveFile(bean, "arquivoxml");
			this.updateCdarquivo(bean, bean.getArquivoxml());
		}
		
	}
	
	public void updateCdarquivo(Entrega entrega, Arquivo arquivoxml) {
		entregaDAO.updateCdarquivo(entrega, arquivoxml);
	}
	
	/**
	 * M�todo que v�lida se existe fornecedor/n�mero cadastrado
	 * caso exista retorna TRUE
	 *
	 * @param entrega
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean validaFornecedornumero(Entrega entrega, Entregadocumento entregadocumento) {
		if(entrega != null && entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
			for(Entregadocumento ed : entrega.getListaEntregadocumento()){
				if(ed.getFornecedor() != null && ed.getNumero() != null && !"".equals(ed.getNumero())){
					if(entradafiscalService.existFornecedorNumero(ed)){
						return true;
					}
				}
			}			
		}else if(entregadocumento != null){
			if(entregadocumento.getFornecedor() != null && entregadocumento.getNumero() != null && !"".equals(entregadocumento.getNumero())){
				if(entradafiscalService.existFornecedorNumero(entregadocumento)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean existeDuplicidadeObslancamentofiscal(Entrega entrega, Entregadocumento entregadocumento) {
		if(entrega != null && entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
			for(Entregadocumento ed : entrega.getListaEntregadocumento()){
				if(SinedUtil.isListNotEmpty(ed.getListaAjustefiscal())){
					if(existeDuplicidadeObslancamentofiscal(ed.getListaAjustefiscal())){
						return true;
					}
				}
			}			
		}else if(entregadocumento != null){
			if(SinedUtil.isListNotEmpty(entregadocumento.getListaAjustefiscal())){
				if(existeDuplicidadeObslancamentofiscal(entregadocumento.getListaAjustefiscal())){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean existeDuplicidadeObslancamentofiscal(Set<Ajustefiscal> listaAjustefiscal) {
		if(SinedUtil.isListNotEmpty(listaAjustefiscal)){
			List<Obslancamentofiscal> listaObslancamentofiscal = new ArrayList<Obslancamentofiscal>();
			for(Ajustefiscal ajustefiscal : listaAjustefiscal){
				if(ajustefiscal.getObslancamentofiscal() != null){
					if(!listaObslancamentofiscal.contains(ajustefiscal.getObslancamentofiscal())){
						listaObslancamentofiscal.add(ajustefiscal.getObslancamentofiscal());
					}else {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * M�todo que retorna os id's das entregadocumento do recebimento separado por v�rgula
	 *
	 * @param listaEntregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public String getWhereInForDeleteEntregadocumento(List<Entregadocumento> listaEntregadocumento) {
		StringBuilder whereIn = new StringBuilder();
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento entregadocumento : listaEntregadocumento){
				if(entregadocumento.getCdentregadocumento() != null){
					if(!"".equals(whereIn.toString()))
						whereIn.append(",");
					
					whereIn.append(entregadocumento.getCdentregadocumento());
				}
			}
		}
		
		return whereIn.toString();
	}
	
	/**
	 * M�todo que verifica se existe projetos diferentes no rateio da entrada fiscal
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isProjetosDiferentesRateioDocumento(Entregadocumento entregadocumento) {
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		if(entregadocumento != null && entregadocumento.getRateio() != null && !entregadocumento.getRateio().getListaRateioitem().isEmpty()){
			for(Rateioitem rateioitem : entregadocumento.getRateio().getListaRateioitem()){
				if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
					if(listaProjeto != null && !listaProjeto.isEmpty()){
						for(Projeto projeto : listaProjeto){
							if(!rateioitem.getProjeto().equals(projeto)){
								return Boolean.TRUE;
							}
						}
					}else {
						listaProjeto.add(rateioitem.getProjeto());
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Retorna os projetos do rateio da entrada fsical 
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> getPercentuaisProjeto(Entregadocumento entregadocumento) {
		List<Projeto> lista = new ArrayList<Projeto>();
		boolean achou = false;
		Projeto projeto;
		if(entregadocumento != null && entregadocumento.getRateio() != null && !entregadocumento.getRateio().getListaRateioitem().isEmpty()){
			for(Rateioitem rateioitem : entregadocumento.getRateio().getListaRateioitem()){
				if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
					if(lista != null && !lista.isEmpty()){
						for(Projeto p : lista){
							if(p.equals(rateioitem.getProjeto())){
								p.setPercentual(p.getPercentual()+rateioitem.getPercentual());
								achou = true;
							}
						}
					}
					
					if(!achou){
						projeto = rateioitem.getProjeto();
						projeto.setPercentual(rateioitem.getPercentual());
						lista.add(projeto);
					}
					achou = false;
					
				}
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que verifica se a entrega precisa de romaneio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#verificaEntregadocumentoRomaneio(Entregadocumento entregadocumento, Localarmazenagem localprincipal)
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public void verificaEntregaRomaneio(Entrega bean) {
		if(bean.getHaveOrdemcompra()){
			bean.setRomaneio(null);
			
			Localarmazenagem localprincipal = bean.getLocalarmazenagem();
			List<Entregadocumento> listaEntregadocumento = bean.getListaEntregadocumento();
			
			for (Entregadocumento entregadocumento : listaEntregadocumento) {
				if(this.verificaEntregadocumentoRomaneio(entregadocumento, localprincipal)){
					bean.setRomaneio(Boolean.FALSE);
					return;
				}
			}
		}
	}
	
	/**
	 * M�todo que verifica se a entregadocumento precisa de romaneio.
	 *
	 * @param entregadocumento
	 * @param localprincipal
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public boolean verificaEntregadocumentoRomaneio(Entregadocumento entregadocumento, Localarmazenagem localprincipal) {
		Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
		if(listaEntregamaterial != null && listaEntregamaterial.size() > 0){
			for (Entregamaterial entregamaterial : listaEntregamaterial) {
				if(entregamaterial.getMaterialclasse() != null && entregamaterial.getMaterialclasse().equals(Materialclasse.SERVICO)) continue;
				
				if(entregamaterial.getQtde() != null && entregamaterial.getQtde() > 0){
					Localarmazenagem localIt = entregamaterial.getLocalarmazenagem();
					if(localIt == null){
						localIt = localprincipal;
					}
					
					Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = entregamaterial.getListaOrdemcompraentregamaterial();
					if(listaOrdemcompraentregamaterial != null && listaOrdemcompraentregamaterial.size() > 0){
						for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
							
							Ordemcompramaterial ordemcompramaterial = ordemcompraentregamaterial.getOrdemcompramaterial();
							List<Solicitacaocompra> listaSolicitacaocompra = new ArrayList<Solicitacaocompra>();
							
							listaSolicitacaocompra.addAll(solicitacaocompraService.findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(ordemcompramaterial));
							listaSolicitacaocompra.addAll(solicitacaocompraService.findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(ordemcompramaterial));
							
							if(listaSolicitacaocompra.size() > 0){
								for (Solicitacaocompra solicitacaocompra : listaSolicitacaocompra) {
									if(solicitacaocompra.getMaterial() != null && 
											entregamaterial.getMaterial() != null && 
											solicitacaocompra.getMaterial().equals(entregamaterial.getMaterial())){
										Localarmazenagem localPedido = solicitacaocompra.getLocalarmazenagem();
										
										if(!localPedido.equals(localIt)){
											return true;
										}
									}
								}
							} else {
								Ordemcompramaterial ordemcompramaterial_aux = ordemcompramaterialService.loadByOrdemcompramaterialForRomaneio(ordemcompramaterial);
								Localarmazenagem localPedido = null;
								
								if(ordemcompramaterial_aux.getLocalarmazenagem() != null){
									localPedido = ordemcompramaterial_aux.getLocalarmazenagem();
								} else if(ordemcompramaterial_aux.getOrdemcompra() != null && 
										ordemcompramaterial_aux.getOrdemcompra().getLocalarmazenagem() != null){
									localPedido = ordemcompramaterial_aux.getOrdemcompra().getLocalarmazenagem();
								}
								
								if(localPedido != null && !localPedido.equals(localIt)){
									return true;
								}
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#updateRomaneio(Entrega entrega, Boolean romaneio)
	 *
	 * @param entrega
	 * @param romaneio
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public void updateRomaneio(Entrega entrega, Boolean romaneio) {
		entregaDAO.updateRomaneio(entrega, romaneio);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findEntregasNecessitamRomaneio(String ids)
	 *
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Entrega> findEntregasNecessitamRomaneio(String ids) {
		return entregaDAO.findEntregasNecessitamRomaneio(ids);
	}
	
	public boolean isRomaneioCompleto(Integer cdentrega){
		BaixarEntregaBean bean = new BaixarEntregaBean();
		bean.setIds(cdentrega.toString());
		bean.setListaEntregaRomaneio(findForValidacaoGerarRomaneio(cdentrega.toString()));

		List<Entrega> listaEntregaRomaneio = findEntregasNecessitamRomaneio(cdentrega.toString());
		if(listaEntregaRomaneio != null && listaEntregaRomaneio.size() > 0){
			bean.setListaEntregaRomaneio(listaEntregaRomaneio);
			
			List<GerarRomaneioBaixarEntregaItemBean> lista = criarListaGerarRomaneio(bean.getListaEntregaRomaneio());
			GerarRomaneioBaixarEntregaBean gerarRomaneioBaixarEntregaBean = new GerarRomaneioBaixarEntregaBean();
			gerarRomaneioBaixarEntregaBean.setIds(cdentrega.toString());
			gerarRomaneioBaixarEntregaBean.setListaEntregaRomaneio(lista);
			
			boolean isDisponivelRomaneio = false;
			for (GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean : lista) {
				if(isDisponivelRomaneio(gerarRomaneioBaixarEntregaItemBean, false, true)){
					isDisponivelRomaneio = true;
					break;
				}
			}
			return !isDisponivelRomaneio;
		}
		
		return false;
	}
		
	public boolean isDisponivelRomaneio(GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean, boolean validaMarcado, boolean somenteValidacao) {
		boolean isDisponivel = false;
		List<Romaneioorigem> listaRomaneioorigem = romaneioorigemService.findForValidaRomaneioEntrega(new Entrega(gerarRomaneioBaixarEntregaItemBean.getCdentrega()));
		if (listaRomaneioorigem != null && listaRomaneioorigem.size() > 0){
			List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio = gerarRomaneioBaixarEntregaItemBean.getListaRomaneio();
			if(listaRomaneio != null && listaRomaneio.size() > 0){
				for(GerarRomaneioBaixarEntregaRomaneio entregadocumento : listaRomaneio){
					if (entregadocumento.getMaterial() != null && (!validaMarcado || Boolean.TRUE.equals(entregadocumento.getMarcado()))){
						entregadocumento.setQtdeEntregue(0.0);
						double qtdeTotalRomaneio = 0d;
						double qtdeDisponivel = entregadocumento.getQtdeOriginal() != null ? entregadocumento.getQtdeOriginal() : entregadocumento.getQtde();
						for(Romaneioorigem romaneioorigem : listaRomaneioorigem){
							if (romaneioorigem.getRomaneio() != null && romaneioorigem.getRomaneio().getListaRomaneioitem() != null){
								for(Romaneioitem romaneioitem : romaneioorigem.getRomaneio().getListaRomaneioitem()){
									if(romaneioitem.getMaterial() != null){
										if(entregadocumento.getMaterial().getCdmaterial().equals(romaneioitem.getMaterial().getCdmaterial())&&
											entregadocumento.getLocalarmazenagemdestino().getCdlocalarmazenagem().equals(romaneioorigem.getRomaneio().getLocalarmazenagemdestino().getCdlocalarmazenagem())&&
											entregadocumento.getLocalarmazenagemorigem().getCdlocalarmazenagem().equals(romaneioorigem.getRomaneio().getLocalarmazenagemorigem().getCdlocalarmazenagem())){
											qtdeDisponivel -= romaneioitem.getQtde();
											qtdeTotalRomaneio += romaneioitem.getQtde();
										}
									}		
								}
							}
						}
						
						if (gerarRomaneioBaixarEntregaItemBean.getListaEntradaestoque() != null ){
							double qtdeRecebida = 0d;
							for (GerarRomaneioBaixarEntregaEntradaEstoque entradaEstoque : gerarRomaneioBaixarEntregaItemBean.getListaEntradaestoque()){
								if(entradaEstoque.getMaterial().getCdmaterial().equals(entregadocumento.getMaterial().getCdmaterial()) && 
										entradaEstoque.getLocalarmazenagem().getCdlocalarmazenagem().equals(entregadocumento.getLocalarmazenagemorigem().getCdlocalarmazenagem())){
									qtdeRecebida += entradaEstoque.getQtde();
								}
							}
							entregadocumento.setQtdeRecebida(qtdeRecebida);
						}
						
						if (qtdeDisponivel > 0.0 && (entregadocumento.getQtdeRecebida() == null || entregadocumento.getQtdeRecebida() > qtdeTotalRomaneio)){
							isDisponivel = true;
						}
						
						if(somenteValidacao) continue;
						
						entregadocumento.setQtdeEntregue(entregadocumento.getQtde()-qtdeDisponivel);
						
						if (gerarRomaneioBaixarEntregaItemBean.getListaEntradaestoque() != null ){
							if(entregadocumento.getZerarQtde() == null || !entregadocumento.getZerarQtde()){
								Double qtde_aux = entregadocumento.getQtdeRecebida() - entregadocumento.getQtdeEntregue();
								if(entregadocumento.getQtde() != null && entregadocumento.getQtde() <= qtde_aux){
									entregadocumento.setQtde(entregadocumento.getQtde());
								}else {
									entregadocumento.setQtde(qtde_aux);
								}
								entregadocumento.setQtdeOriginal(entregadocumento.getQtdeRecebida());
							}
						}
					}
				}
			}
		}
		else{
			isDisponivel = true;
			
			List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio = gerarRomaneioBaixarEntregaItemBean.getListaRomaneio();
			if(listaRomaneio != null && listaRomaneio.size() > 0){
				for(GerarRomaneioBaixarEntregaRomaneio entregadocumento : listaRomaneio){
					if (entregadocumento.getMaterial() != null && (!validaMarcado || Boolean.TRUE.equals(entregadocumento.getMarcado()))){
						if(!somenteValidacao){
							entregadocumento.setQtdeEntregue(0d);
							if (gerarRomaneioBaixarEntregaItemBean.getListaEntradaestoque() != null ){
								for (GerarRomaneioBaixarEntregaEntradaEstoque entradaEstoque : gerarRomaneioBaixarEntregaItemBean.getListaEntradaestoque()){
									if(entradaEstoque.getQtde() != null && entradaEstoque.getMaterial().getCdmaterial().equals(entregadocumento.getMaterial().getCdmaterial()) && !Boolean.TRUE.equals(entradaEstoque.getAdicionado())){
										if(entregadocumento.getQtdeRecebida() == null){
											entregadocumento.setQtdeRecebida(entradaEstoque.getQtde());
										}else {
											entregadocumento.setQtdeRecebida(entregadocumento.getQtdeRecebida() + entradaEstoque.getQtde());
										}
										entradaEstoque.setAdicionado(Boolean.TRUE);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return isDisponivel;
	}
	
	/**
	 * Cria o registro por entrega para a gera��o do romaneio.
	 *
	 * @param listaEntregaRomaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<GerarRomaneioBaixarEntregaItemBean> criarListaGerarRomaneio(List<Entrega> listaEntregaRomaneio) {
		List<GerarRomaneioBaixarEntregaItemBean> lista = new ArrayList<GerarRomaneioBaixarEntregaItemBean>();
		GerarRomaneioBaixarEntregaItemBean itBean;

		for (Entrega entrega : listaEntregaRomaneio) {
			itBean = new GerarRomaneioBaixarEntregaItemBean();
			itBean.setCdentrega(entrega.getCdentrega());
			itBean.setListaSolicitacoes(this.getListaSolicitacoesEntregaRomaneio(entrega));
			itBean.setListaEntradaestoque(this.getListaEntradaEstoqueEntregaRomaneio(entrega));
			itBean.setListaRomaneio(this.getListaRomaneioBaixarEntrega(itBean.getListaSolicitacoes(), itBean.getListaEntradaestoque()));
			
			lista.add(itBean);
		}
		
		return lista;
	}
	
	/**
	 * Gera a lista para a exibi��o na tela para gera��o de romaneios
	 *
	 * @param listaSolicitacoes
	 * @param listaEntradaestoque
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	private List<GerarRomaneioBaixarEntregaRomaneio> getListaRomaneioBaixarEntrega(
			List<GerarRomaneioBaixarEntregaSolicitacao> listaSolicitacoes,
			List<GerarRomaneioBaixarEntregaEntradaEstoque> listaEntradaestoque) {
		
		List<GerarRomaneioBaixarEntregaRomaneio> lista = new ArrayList<GerarRomaneioBaixarEntregaRomaneio>();
		
		List<GerarRomaneioBaixarEntregaEntradaEstoque> listaEntradaestoqueAgrupada = new ArrayList<GerarRomaneioBaixarEntregaEntradaEstoque>();
		HashMap<String, Double> mapMaterialLocalQtde = new HashMap<String, Double>();
		
		for (GerarRomaneioBaixarEntregaEntradaEstoque itEntrada : listaEntradaestoque) {
			boolean achou = false;
			for (GerarRomaneioBaixarEntregaEntradaEstoque itAgrupada : listaEntradaestoqueAgrupada) {
				if(itAgrupada.getLocalarmazenagem().equals(itEntrada.getLocalarmazenagem()) &&
						itAgrupada.getMaterial().equals(itEntrada.getMaterial())){
					itAgrupada.setQtde(itAgrupada.getQtde() + itEntrada.getQtde());
					achou = true;
					
					mapMaterialLocalQtde.put(itAgrupada.getLocalarmazenagem().getCdlocalarmazenagem() + "" + 
							itAgrupada.getMaterial().getCdmaterial(), itAgrupada.getQtde());
				}
			}
			
			if(!achou){
				GerarRomaneioBaixarEntregaEntradaEstoque itNovo = new GerarRomaneioBaixarEntregaEntradaEstoque();
				itNovo.setLocalarmazenagem(itEntrada.getLocalarmazenagem());
				itNovo.setMaterial(itEntrada.getMaterial());
				itNovo.setQtde(itEntrada.getQtde());
				
				mapMaterialLocalQtde.put(itEntrada.getLocalarmazenagem().getCdlocalarmazenagem() + "" + 
						itEntrada.getMaterial().getCdmaterial(), itEntrada.getQtde());
				
				listaEntradaestoqueAgrupada.add(itNovo);
			}
		}
		
		for (GerarRomaneioBaixarEntregaEntradaEstoque itEntrada : listaEntradaestoqueAgrupada) {
			Double qtdeEntrada = itEntrada.getQtde();
			Material materialEntrada = itEntrada.getMaterial();
			Localarmazenagem localEntrada = itEntrada.getLocalarmazenagem();
			
			for (GerarRomaneioBaixarEntregaSolicitacao itSolicitacao : listaSolicitacoes) {
				Material materialSolicitacao = itSolicitacao.getMaterial();
				Localarmazenagem localSolicitacao = itSolicitacao.getLocalarmazenagem();
				
				boolean materialIgual = materialSolicitacao.equals(materialEntrada);
				boolean localarmazenagemIgual = localSolicitacao.equals(localEntrada);
				
				if(materialIgual && localarmazenagemIgual) {
					qtdeEntrada -= itSolicitacao.getQtde();
				}
			}

			for (GerarRomaneioBaixarEntregaSolicitacao itSolicitacao : listaSolicitacoes) {
				if(qtdeEntrada <= 0){
					System.out.println("###material: "+itSolicitacao.getMaterial().getCdmaterial() + " - " + itSolicitacao.getMaterial().getIdentificacao());
					break;
				}
				
				Material materialSolicitacao = itSolicitacao.getMaterial();
				Localarmazenagem localSolicitacao = itSolicitacao.getLocalarmazenagem();
				Materialclasse materialclasseSolicitacao = itSolicitacao.getMaterialclasse();
				
				
				boolean materialIgual = materialSolicitacao.equals(materialEntrada);
				boolean localarmazenagemDiferente = !localSolicitacao.equals(localEntrada);
				
				if(materialIgual && localarmazenagemDiferente){
					boolean achou = false;
					
					Double qtdeSolicitacao = itSolicitacao.getQtde();
					
					for(GerarRomaneioBaixarEntregaRomaneio e : lista){
						if(materialSolicitacao.getCdmaterial().equals(e.getMaterial().getCdmaterial()) &&
							   localEntrada.getCdlocalarmazenagem().equals(e.getLocalarmazenagemorigem().getCdlocalarmazenagem()) &&
							   localSolicitacao.getCdlocalarmazenagem().equals(e.getLocalarmazenagemdestino().getCdlocalarmazenagem())){
							e.setQtde(e.getQtde() + qtdeSolicitacao);
							if(e.getQtdeOriginal() != null){
								e.setQtdeOriginal(e.getQtdeOriginal() + qtdeSolicitacao);
							}
							qtdeEntrada -= qtdeSolicitacao;
							achou = true;
							break;
						}
					}
					
					if(!achou){
						GerarRomaneioBaixarEntregaRomaneio bean = new GerarRomaneioBaixarEntregaRomaneio();
						bean.setMaterial(materialSolicitacao);
						bean.setMaterialclasse(materialclasseSolicitacao);
						bean.setLocalarmazenagemorigem(localEntrada);
						bean.setLocalarmazenagemdestino(localSolicitacao);
						bean.setMarcado(Boolean.TRUE);
						bean.setQtde(qtdeSolicitacao);
						bean.setQtdeOriginal(qtdeSolicitacao);
						
						bean.setQtdeSolicitada(mapMaterialLocalQtde.get(localEntrada.getCdlocalarmazenagem() + "" + 
								materialSolicitacao.getCdmaterial()));
						
						lista.add(bean);
						
						qtdeEntrada -= qtdeSolicitacao;
					}
				}
			}
		}
		
		Collections.sort(lista,new Comparator<GerarRomaneioBaixarEntregaRomaneio>(){
			public int compare(GerarRomaneioBaixarEntregaRomaneio o1, GerarRomaneioBaixarEntregaRomaneio o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		return lista;
	}
	
	/**
	 * Preenche a lista de entradas no estoque dos materiais para a tela de gera��o de romaneio.
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	private List<GerarRomaneioBaixarEntregaEntradaEstoque> getListaEntradaEstoqueEntregaRomaneio(Entrega entrega) {
		List<GerarRomaneioBaixarEntregaEntradaEstoque> lista = new ArrayList<GerarRomaneioBaixarEntregaEntradaEstoque>();
		GerarRomaneioBaixarEntregaEntradaEstoque bean;
		
		List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByEntrega(entrega);
		for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
			bean = new GerarRomaneioBaixarEntregaEntradaEstoque();
			bean.setMaterial(movimentacaoestoque.getMaterial());
			bean.setQtde(movimentacaoestoque.getQtde());
			bean.setLocalarmazenagem(movimentacaoestoque.getLocalarmazenagem());
			bean.setCdmovimentacaoestoque(movimentacaoestoque.getCdmovimentacaoestoque());
			
			lista.add(bean);
		}
		
		Collections.sort(lista,new Comparator<GerarRomaneioBaixarEntregaEntradaEstoque>(){
			public int compare(GerarRomaneioBaixarEntregaEntradaEstoque o1, GerarRomaneioBaixarEntregaEntradaEstoque o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		return lista;
	}
	
	/**
	 * Busca as origens da entrega para a tela de gera��o de romaneio.
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	private List<GerarRomaneioBaixarEntregaSolicitacao> getListaSolicitacoesEntregaRomaneio(Entrega entrega) {
		
		List<GerarRomaneioBaixarEntregaSolicitacao> lista = new ArrayList<GerarRomaneioBaixarEntregaSolicitacao>();
		GerarRomaneioBaixarEntregaSolicitacao bean;
		
		List<Ordemcompramaterial> listaOrdemcompramaterial = ordemcompramaterialService.findByEntrega(entrega);
		for (Ordemcompramaterial ordemcompramaterial : listaOrdemcompramaterial) {
			List<Solicitacaocompra> listaSolicitacaocompra = new ArrayList<Solicitacaocompra>();
			
			//Com Cotacao
			listaSolicitacaocompra.addAll(solicitacaocompraService.findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(ordemcompramaterial));
			//Sem Cotacao
			listaSolicitacaocompra.addAll(solicitacaocompraService.findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(ordemcompramaterial));
			
			if(listaSolicitacaocompra.size() > 0){
				for (Solicitacaocompra solicitacaocompra : listaSolicitacaocompra) {
					bean = new GerarRomaneioBaixarEntregaSolicitacao();
					bean.setMaterial(solicitacaocompra.getMaterial());
					bean.setMaterialclasse(solicitacaocompra.getMaterialclasse());
					bean.setLocalarmazenagem(solicitacaocompra.getLocalarmazenagem());
					bean.setQtde(solicitacaocompra.getQtde());
					bean.setOrigem("Solicita��o de compra " + solicitacaocompra.getCdsolicitacaocompra());
					
					if(!lista.contains(bean)){
						lista.add(bean);
					}
				}
			} else {
				Ordemcompramaterial ordemcompramaterial_aux = ordemcompramaterialService.loadByOrdemcompramaterialForRomaneio(ordemcompramaterial);
				Materialclasse materialclasse = ordemcompramaterialService.getMaterialclasseByOrdemcompramaterial(ordemcompramaterial);
				
				Localarmazenagem localPedido = null;
				
				if(ordemcompramaterial_aux.getLocalarmazenagem() != null){
					localPedido = ordemcompramaterial_aux.getLocalarmazenagem();
				} else if(ordemcompramaterial_aux.getOrdemcompra() != null && 
						ordemcompramaterial_aux.getOrdemcompra().getLocalarmazenagem() != null){
					localPedido = ordemcompramaterial_aux.getOrdemcompra().getLocalarmazenagem();
				}
				
				bean = new GerarRomaneioBaixarEntregaSolicitacao();
				bean.setMaterial(ordemcompramaterial_aux.getMaterial());
				bean.setMaterialclasse(materialclasse);
				bean.setLocalarmazenagem(localPedido);
				bean.setQtde(ordemcompramaterial_aux.getQtdpedida());
				bean.setOrigem("Ordem de compra " + ordemcompramaterial_aux.getOrdemcompra().getCdordemcompra());
				
				if(!lista.contains(bean)){
					lista.add(bean);
				}
			}
		}
		
		Collections.sort(lista,new Comparator<GerarRomaneioBaixarEntregaSolicitacao>(){
			public int compare(GerarRomaneioBaixarEntregaSolicitacao o1, GerarRomaneioBaixarEntregaSolicitacao o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForValidacaoGerarRomaneio(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Entrega> findForValidacaoGerarRomaneio(String whereIn) {
		return entregaDAO.findForValidacaoGerarRomaneio(whereIn);
	}
	
	/**
	 * M�todo que ajusta a quantidade do estoque caso o rateio da ordem de compra tenha projetos diferentes
	 *
	 * @param ids
	 * @param listaitem
	 * @param listaPatrimonio
	 * @param listaResolvidos
	 * @author Luiz Fernando
	 * @param estoquePorProjeto 
	 */
	public List<BaixarEntregaItemBean> ajustarQtdeByRateioOrdemcompraVariosProjetos(List<Entrega> listaEntrega, List<BaixarEntregaItemBean> listaitem, Boolean estoquePorProjeto) {
		List<BaixarEntregaItemBean> listaItembean = new ArrayList<BaixarEntregaItemBean>(); 
		
		BaixarEntregaItemBean beanaux;
		BaixarEntregaItemBean beannovo;
		Double qtde;
		Double qtdetotal;
		
		if(listaEntrega != null && !listaEntrega.isEmpty()){
			for(Entrega entrega : listaEntrega){
				for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
					if(estoquePorProjeto){						
						//verifica se existe projetos diferentes
						//if(entregaService.isProjetosDiferentesRateioDocumento(entregadocumento)){
							List<Projeto> listaProjetos = this.getPercentuaisProjeto(entregadocumento);
							if(listaProjetos != null && !listaProjetos.isEmpty()){
								if(listaitem != null && !listaitem.isEmpty()){
									for(BaixarEntregaItemBean baixarEntregaItemBean : listaitem){
										if(baixarEntregaItemBean.getQuantidade() != null && baixarEntregaItemBean.getEntrega() != null && baixarEntregaItemBean.getEntrega().getCdentrega() != null && 
												baixarEntregaItemBean.getEntrega().getCdentrega().equals(entrega.getCdentrega()) && 
												baixarEntregaItemBean.getEntregadocumento() != null && baixarEntregaItemBean.getEntregadocumento().getCdentregadocumento() != null && 
												baixarEntregaItemBean.getEntregadocumento().getCdentregadocumento().equals(entregadocumento.getCdentregadocumento())){
											beanaux = baixarEntregaItemBean;
											qtdetotal = 0.0;
											//cria novos beans com os percentuais dos projetos
											for(int i = 0; i < listaProjetos.size(); i++){
												beannovo = new BaixarEntregaItemBean(beanaux);
												qtde = beanaux.getQuantidade()*listaProjetos.get(i).getPercentual();
												if(qtde != 0){
													qtdetotal += (qtde/100);
													beannovo.setQuantidade(qtde/100);
													beannovo.setComprimento(beanaux.getComprimento());
													beannovo.setValorunitario(beanaux.getValorunitario());
													beannovo.setProjeto(listaProjetos.get(i));
													beannovo.setCentrocusto(beanaux.getCentrocusto());
													listaItembean.add(beannovo);
												}
											}
											
											//se restar quantidade a ser movimentada, cria-se um novo bean sem projeto
											if(beanaux.getQuantidade()-qtdetotal > 0){
												beannovo = new BaixarEntregaItemBean(beanaux);
												qtde = beanaux.getQuantidade()-qtdetotal;
												if(qtde != 0){
													beannovo.setQuantidade(qtde);
													beannovo.setComprimento(beanaux.getComprimento());
													beannovo.setValorunitario(beanaux.getValorunitario());
													listaItembean.add(beannovo);
												}
											}
										}
									}
								}
							}
//						}
					}
				}
			}
		}
		
		return listaItembean;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#loadWithEmpresa(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public Entrega loadWithEmpresa(Entrega entrega) {
		return entregaDAO.loadWithEmpresa(entrega);
	}
	
	@SuppressWarnings("unchecked")
	public List<Entrega> getUltimascompras(String cdmaterial, String cdempresa, Integer qtdeMeses){
		List<Entrega> lista = new ArrayList<Entrega>();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			if("ORDEM_COMPRA".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.COTACAOPRECOMAXIMO))){
				Entrega entrega;
				List<Ordemcompramaterial> listaMaterial = ordemcompramaterialService.findUltimascomprasByMaterial(
																					Integer.parseInt(cdmaterial), 
																					(cdempresa != null  && !"".equals(cdempresa) ? Integer.parseInt(cdempresa) : null),
																					5);
				if(listaMaterial != null && !listaMaterial.isEmpty()){
					Double precomaximocompra = null;
					if(qtdeMeses != null){
						precomaximocompra = ordemcompramaterialService.getPrecoMaximoCompra(cdmaterial, cdempresa, qtdeMeses);
					}
					for(Ordemcompramaterial ocm : listaMaterial){
						entrega = new Entrega();
						entrega.setUltimacomprafornecedor(ocm.getOrdemcompra().getFornecedor().getNome());
						if(ocm.getOrdemcompra().getDtcriacao() != null){
							entrega.setUltimacompradata(ocm.getOrdemcompra().getDtcriacao());
							entrega.setUltimacompradatastr(format.format(ocm.getOrdemcompra().getDtcriacao()));
						}
						if(ocm.getValor() != null && ocm.getQtdpedida() != null && ocm.getQtdefrequencia() != null){
							entrega.setUltimacompravalorunitario(SinedUtil.round(ocm.getValor()/ocm.getQtdpedida()/ocm.getQtdefrequencia(), 10));
						}
						
						List<Ordemcompraentrega> listaOrdemcompraentrega = new ArrayList<Ordemcompraentrega>();
						Ordemcompraentrega ordemcompraentrega = new Ordemcompraentrega();
						ordemcompraentrega.setOrdemcompra(ocm.getOrdemcompra());
						listaOrdemcompraentrega.add(ordemcompraentrega);
						entrega.setListaOrdemcompraentrega(SinedUtil.listToSet(listaOrdemcompraentrega, Ordemcompraentrega.class));
						entrega.setPrecomaximocompra(precomaximocompra);
						lista.add(entrega);
					}
				}
			}else {
				Entrega entrega;
				List<Entregamaterial> listaMaterial = entregamaterialService.findUltimascomprasByMaterial(
																					Integer.parseInt(cdmaterial), 
																					(cdempresa != null  && !"".equals(cdempresa) ? Integer.parseInt(cdempresa) : null),
																					5);
				if(listaMaterial != null && !listaMaterial.isEmpty()){
					Double precomaximocompra = null;
					if(qtdeMeses != null){
						precomaximocompra = entregamaterialService.getPrecoMaximoCompra(cdmaterial, cdempresa, qtdeMeses);
					}
					for(Entregamaterial em : listaMaterial){
						entrega = new Entrega();
						entrega.setUltimacomprafornecedor(em.getEntregadocumento().getFornecedor().getNome());
						if(em.getEntregadocumento().getDtentrada() != null){
							entrega.setUltimacompradata(em.getEntregadocumento().getDtentrada());
							entrega.setUltimacompradatastr(format.format(em.getEntregadocumento().getDtentrada()));
						}else if(em.getEntregadocumento().getEntrega() != null && em.getEntregadocumento().getEntrega().getDtentrega() != null){
							entrega.setUltimacompradata(em.getEntregadocumento().getEntrega().getDtentrega());
							entrega.setUltimacompradatastr(format.format(em.getEntregadocumento().getEntrega().getDtentrega()));
						}
						entrega.setUltimacompravalorunitario(em.getValorunitario());
						if(em.getEntregadocumento() != null && em.getEntregadocumento().getEntrega() != null && 
								em.getEntregadocumento().getEntrega().getListaOrdemcompraentrega() != null && 
								!em.getEntregadocumento().getEntrega().getListaOrdemcompraentrega().isEmpty()){
							entrega.setListaOrdemcompraentrega(em.getEntregadocumento().getEntrega().getListaOrdemcompraentrega());
						}
						entrega.setPrecomaximocompra(precomaximocompra);
						lista.add(entrega);
					}
				}
			}
		}
		return lista;
	}
	
	public ModelAndView abrirPopupUltimascompras(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		String cdempresa = request.getParameter("cdempresa");
		
		if(cdmaterial == null || "".equals(cdmaterial)){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>parent.$.akModalRemove(true);</script>");
			return null;
		}
		
		String COTACAOMESESPRECOMAXIMO = parametrogeralService.getValorPorNome(Parametrogeral.COTACAOMESESPRECOMAXIMO);
		Integer qtdeMeses = null;
		if(org.apache.commons.lang.StringUtils.isNotBlank(COTACAOMESESPRECOMAXIMO)){
			try {
				qtdeMeses = Integer.parseInt(COTACAOMESESPRECOMAXIMO);
			} catch (Exception e) {}
		}
		List<Entrega> listaEntrega = this.getUltimascompras(cdmaterial, cdempresa, qtdeMeses);
		request.setAttribute("COTACAOMESESPRECOMAXIMO", org.apache.commons.lang.StringUtils.isNotBlank(COTACAOMESESPRECOMAXIMO) ? COTACAOMESESPRECOMAXIMO : null);
		return new ModelAndView("direct:/crud/popup/popupUltimascompras", "entrega", listaEntrega);		
	}
	
	
	/**
	 * M�todo que adiciona informa��es da requisicao de material para a baixa no estoque
	 *
	 * @param lista
	 * @author Luiz Fernando
	 */
	public void adicionarInfRequisicao(List<BaixarEntregaItemBean> lista) {
		if(lista != null && !lista.isEmpty()){
			StringBuilder whereInEntrega = new StringBuilder();
			for(BaixarEntregaItemBean baixarEntregaItemBean : lista){
				if(baixarEntregaItemBean.getEntrega() != null && baixarEntregaItemBean.getEntrega().getCdentrega() != null)
					if(!"".equals(whereInEntrega.toString())) whereInEntrega.append(",");
					whereInEntrega.append(baixarEntregaItemBean.getEntrega().getCdentrega());
			}
			
			if(!"".equals(whereInEntrega.toString())){
				List<Requisicaomaterial> listaRequisicao = requisicaomaterialService.findRequisitanteMaterialByEntrega(whereInEntrega.toString());
				if(listaRequisicao != null && !listaRequisicao.isEmpty()){
					for(BaixarEntregaItemBean baixarEntregaItemBean : lista){
						if(baixarEntregaItemBean.getEntrega() != null && baixarEntregaItemBean.getEntrega().getCdentrega() != null &&
							baixarEntregaItemBean.getMaterial() != null && baixarEntregaItemBean.getMaterial().getCdmaterial() != null){
							for(Requisicaomaterial requisicaomaterial : listaRequisicao){
								if(requisicaomaterial.getEntrega() != null && requisicaomaterial.getEntrega().getCdentrega() != null && 
									requisicaomaterial.getMaterial() != null && requisicaomaterial.getMaterial().getCdmaterial() != null &&
									requisicaomaterial.getEntrega().equals(baixarEntregaItemBean.getEntrega()) &&
									requisicaomaterial.getMaterial().equals(baixarEntregaItemBean.getMaterial())){
									baixarEntregaItemBean.setDepartamento(requisicaomaterial.getDepartamento());
									baixarEntregaItemBean.setColaborador(requisicaomaterial.getColaborador());
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	public TotalizadorEntrega findForListagemTotalizador(EntregaFiltro filtro){
		return entregaDAO.findForListagemTotalizador(filtro);
	}

	
	public List<BaixarEntregaItemBean> montaMaterialitemmestregrade(List<Material> listaMaterialitemgrade) {
		List<BaixarEntregaItemBean> lista = new ArrayList<BaixarEntregaItemBean>();
		
		if(listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty()){
			String calculometroquadrado = parametrogeralService.getValorPorNome(Parametrogeral.CALCULOMETROQUADRADO);
			String whereInItenGrade = CollectionsUtil.listAndConcatenate(listaMaterialitemgrade, "cdmaterial", ",");
			List<Produto> listaProduto = null;
			if(whereInItenGrade != null && !"".equals(whereInItenGrade)){
				listaProduto = produtoService.findWithAlturaComprimentoLargura(whereInItenGrade);
			}
			for(Material material : listaMaterialitemgrade){
				BaixarEntregaItemBean itemBean = new BaixarEntregaItemBean();
				itemBean.setMaterial(material);
				
				Produto produto = produtoService.getProduto(listaProduto, material);
				if(produto != null && (produto.getAltura() != null || produto.getComprimento() != null || produto.getLargura() != null)){
					Double fatorconversao = 1000d;
					if(produto.getFatorconversao() != null) fatorconversao = produto.getFatorconversao();
					if(produto.getComprimento() != null && produto.getComprimento() > 0)
						material.setProduto_comprimento(produto.getComprimento()/fatorconversao);
					if(produto.getAltura() != null && produto.getAltura() > 0)
						material.setProduto_altura(produto.getAltura()/fatorconversao);
					if(produto.getLargura() != null && produto.getLargura() > 0)
						material.setProduto_largura(produto.getLargura()/fatorconversao);
					itemBean.setQuantidade(materialService.getQtdeParamCalculometroquadrado(calculometroquadrado, 
																	material.getProduto_altura(), 
																	material.getProduto_largura(),
																	material.getProduto_comprimento()));
					
					if(itemBean.getQuantidade() != null && itemBean.getQuantidade() > 0){
						if(material.getUnidademedida() != null && material.getUnidademedida().getCasasdecimaisestoque() != null){
							itemBean.setQuantidade(SinedUtil.roundByUnidademedida(itemBean.getQuantidade(), material.getUnidademedida()));
						}else {
							itemBean.setQuantidade(SinedUtil.roundByParametro(itemBean.getQuantidade()));
						}
					}
				}
				lista.add(itemBean);
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que agrupa os itens da grade
	 *
	 * @param listaBaixarEntregaItemBean
	 * @return
	 * @author Luiz Fernando
	 * @since 30/01/2014
	 */
	public List<BaixarEntregaItemBean> getListaCorrigida(List<BaixarEntregaItemBean> listaBaixarEntregaItemBean) {
		List<BaixarEntregaItemBean> lista = new ArrayList<BaixarEntregaItemBean>();
		HashMap<Material, BaixarEntregaItemBean> mapMaterial = new HashMap<Material, BaixarEntregaItemBean>();
		
		if(listaBaixarEntregaItemBean != null && !listaBaixarEntregaItemBean.isEmpty()){
			for(BaixarEntregaItemBean item : listaBaixarEntregaItemBean){
				if(item.getMaterial() != null){
					if(materialService.isControleMaterialgrademestre(item.getMaterial())){
						Material materialWithMestre = materialService.loadWithGrade(item.getMaterial());
						if(materialWithMestre != null && materialWithMestre.getMaterialmestregrade() != null){
							if(mapMaterial.get(materialWithMestre.getMaterialmestregrade()) == null){
								BaixarEntregaItemBean itemMestreGradeForBaixa = new BaixarEntregaItemBean(item);
								itemMestreGradeForBaixa.setMaterial(item.getMaterial().getMaterialmestregrade());
								itemMestreGradeForBaixa.setValorunitario(item.getValorunitario());
								itemMestreGradeForBaixa.setQuantidadePrincipal(item.getQuantidadePrincipal());
								if(item.getQuantidadePrincipal() != null){
									itemMestreGradeForBaixa.setQuantidade(item.getQuantidadePrincipal());
								}else if(item.getQuantidade() != null){
									itemMestreGradeForBaixa.setQuantidade(item.getQuantidade());
								}
								mapMaterial.put(materialWithMestre.getMaterialmestregrade(),itemMestreGradeForBaixa);
							}else {
								BaixarEntregaItemBean itemMestreGradeForBaixa = mapMaterial.get(materialWithMestre.getMaterialmestregrade());
								
								if(item.getQuantidadePrincipal() != null){
									if(itemMestreGradeForBaixa.getQuantidade() != null){
										itemMestreGradeForBaixa.setQuantidade(itemMestreGradeForBaixa.getQuantidade()+item.getQuantidadePrincipal());
									}else {
										itemMestreGradeForBaixa.setQuantidade(item.getQuantidadePrincipal());
									}
								}else if(item.getQuantidade() != null){
									if(itemMestreGradeForBaixa.getQuantidade() != null){
										itemMestreGradeForBaixa.setQuantidade(itemMestreGradeForBaixa.getQuantidade()+item.getQuantidade());
									}else {
										itemMestreGradeForBaixa.setQuantidade(item.getQuantidade());
									}
								}
							}
						}else {
							lista.add(item);
						}
					}else {
						lista.add(item);
					}
				}
			}
			
			if(mapMaterial.size() > 0){
				for(Material material : mapMaterial.keySet()){ 
					lista.add(mapMaterial.get(material));
				}
			}
		}
		return lista;
	}
	
	@Transient
	public String getWhereInEntregadocumento(Entrega entrega){
		StringBuilder s = new StringBuilder();
		if(entrega != null && SinedUtil.isListNotEmpty(entrega.getListaEntregadocumento())){
			for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
				if(entregadocumento.getCdentregadocumento() != null){
					s.append(entregadocumento.getCdentregadocumento()).append(",");
				}
			}
			if(!s.toString().equals("")){
				return s.substring(0, s.length()-1);
			}
		}
		return s.toString();
	}
	
	/**
	* M�todo que verifica se existe entrega com romaneio gerado e n�o existe romaneo em aberto ou baixado vinculado
	*
	* @param whereInRomaneio
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public void verificaRomaneioPendenteAposCancelamento(String whereInRomaneio) {
		if(org.apache.commons.lang.StringUtils.isNotEmpty(whereInRomaneio)){
			List<Entrega> listaEntrega = findEntregaRomaneioGerado(whereInRomaneio);
			if(SinedUtil.isListNotEmpty(listaEntrega)){
				for(Entrega entrega : listaEntrega){
					updateRomaneio(entrega, false);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregaDAO#findEntregaRomaneioGeradoSemRomaneio(String whereInRomaneio)
	*
	* @param whereInRomaneio
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaRomaneioGeradoSemRomaneio(String whereInRomaneio) {
		return entregaDAO.findEntregaRomaneioGeradoSemRomaneio(whereInRomaneio);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregaDAO#findEntregaRomaneioGerado(String whereInRomaneio)
	*
	* @param whereInRomaneio
	* @return
	* @since 18/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaRomaneioGerado(String whereInRomaneio) {
		return entregaDAO.findEntregaRomaneioGerado(whereInRomaneio);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregaDAO#findEntregaDifernteCancelada(String ids, Material material)
	*
	* @param ids
	* @return
	* @since 25/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaDifernteCancelada(String ids, Material material) {
		return entregaDAO.findEntregaDifernteCancelada(ids, material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author C�sar
	 * @since 17/12/2015
	 */
	public List<Entrega> buscarMaterialForPopUpEtiquetaEntrega(String whereIn) {
		return entregaDAO.buscarMaterialForPopUpEtiquetaEntrega(whereIn);
	}
	
	public void validateQtdeEntregue(Entrega bean, BindException errors){
		if(bean.getHaveOrdemcompra() && bean.isValidaQtdeEntrega()){
			if(bean.getListaEntregadocumento() != null && !bean.getListaEntregadocumento().isEmpty()){
				for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
					if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
						for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
							if(entregamaterial.getListaOrdemcompraentregamaterial() != null && entregamaterial.getListaOrdemcompraentregamaterial().size() > 0){
								for (Ordemcompraentregamaterial ordemcompraentregamaterial : entregamaterial.getListaOrdemcompraentregamaterial()) {
									entregamaterial.setOcm(ordemcompraentregamaterial.getOrdemcompramaterial());
									Double restoIt = entregamaterialService.getQtdeByEntregaMaterial(entregamaterial);
									if(restoIt < 0){
										errors.reject("001","A quantidade total entregue do material '"+materialService.load(entregamaterial.getMaterial(),"material.nome").getNome()+"' � maior que a quantidade pedida na ordem de compra.");
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public Entrega criarCopia(Entrega origem){
		if(origem != null && origem.getCdentrega() != null){
			origem = loadForEntrada(origem);
			origem.setListaEntregadocumento(entradafiscalService.carregaListaEntregadocumentoForEntrega(origem));
			origem.setCdentrega(null);
			origem.setAux_entrega(null);
			if(SinedUtil.isListNotEmpty(origem.getListaEntregadocumento())){
				for(Entregadocumento ed : origem.getListaEntregadocumento()){
					ed.setCdentrega(null);
					ed.setCdentregadocumento(null);
					ed.setEntrega(null);
					if(SinedUtil.isListNotEmpty(ed.getListaEntregamaterial())){
						for(Entregamaterial em : ed.getListaEntregamaterial()){
							em.setCdentregamaterial(null);
							em.setEntregadocumento(null);
							em.setListaOrdemcompraentregamaterial(null);
							em.setListaEntregamateriallote(null);
						}
					}
					if(SinedUtil.isListNotEmpty(ed.getListadocumento())){
						for(Entregapagamento ep : ed.getListadocumento()){
							ep.setCdentregapagamento(null);
							ep.setEntregadocumento(null);
						}
					}
					if(ed.getRateio() != null){
						rateioService.limpaReferenciaRateio(ed.getRateio());
					}
				}
			}
		}
		return origem;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.EntregaDAO#haveEntregaSituacao(String whereInEntregadocumento, Situacaosuprimentos... situacoes)
	*
	* @param whereInEntregadocumento
	* @param situacoes
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public boolean haveEntregaSituacao(String whereInEntregadocumento, Situacaosuprimentos... situacoes) {
		return entregaDAO.haveEntregaSituacao(whereInEntregadocumento, situacoes);
	}

	
	public Documento criaFaturaEntregaUnica(String entregasSelecionadas, Documentotipo documentotipo, HashMap<Integer, String> retornowms) {		
		Set<Entregapagamento> listaEntregaPagamento = null;
		Documento documento = new Documento();
		Rateio rateio = new Rateio();
		StringBuilder whereInEntregadocumento = new StringBuilder();
		
		List<Documentoorigem> listaDocumentoorigemAntecipacao = new ArrayList<Documentoorigem>();
		Documentoorigem documentoorigem;
		
		Conta vinculoProvisionado = null;
		if(documentotipo != null && documentotipo.getCddocumentotipo() != null){
			Documentotipo docTipo = documentotipoService.loadWithContadestino(documentotipo);
			if(docTipo != null && docTipo.getContadestino() != null){
				vinculoProvisionado = docTipo.getContadestino();
			}
		}
		Entrega entrega = this.findForFaturar(entregasSelecionadas);
		
		List<Documento> financiamentos = new ListSet<Documento>(Documento.class);

		String whereInEd = null;
		if(retornowms != null){
			whereInEd = retornowms.get(entrega.getCdentrega());
		}
		
		boolean existDocumento = false;
		for(Entregadocumento ed : entrega.getListaEntregadocumento()){
			if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
				if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
					if(!whereInEntregadocumento.toString().equals("")) whereInEntregadocumento.append(",");
					whereInEntregadocumento.append(ed.getCdentregadocumento());
				}
				documento = new Documento();
				financiamentos = new ListSet<Documento>(Documento.class);
				
				if(whereInEd == null || "".equals(whereInEd) || !whereInEd.contains(ed.getCdentregadocumento().toString())){
					if(ed.getRateio() != null && ed.getRateio().getCdrateio() != null)
						ed.setRateio(rateioService.findRateio(ed.getRateio()));
					listaEntregaPagamento = ed.getListadocumento();				
					int parcelas = 1;				
					for (Entregapagamento entregapagamento : listaEntregaPagamento) {
						if(entregapagamento.getDocumentoantecipacao() == null){
								if(parcelas == 1){
									this.preencheDocumento(documentotipo, documento, ed,entregapagamento);

									if(documento.getVinculoProvisionado() == null){
										if(vinculoProvisionado != null){
											documento.setVinculoProvisionado(vinculoProvisionado);
										}else if(ed.getDocumentotipo() != null && ed.getDocumentotipo().getContadestino() != null){
											documento.setVinculoProvisionado(ed.getDocumentotipo().getContadestino());
										}
									}
									
									rateioitemService.agruparItensRateio(ed.getRateio().getListaRateioitem());
									
									rateio.setListaRateioitem(ed.getRateio().getListaRateioitem());
									rateioService.atualizaValorRateio(rateio, entregapagamento.getValor());
								
								} else{
									Documento financiamento = this.preencheFinanciamento(entregapagamento, parcelas, ed);
									financiamentos.add(financiamento);
								}
							
							parcelas++;
							existDocumento = true;
						}else {
							documentoorigem = new Documentoorigem();
							documentoorigem.setEntrega(entrega);
							if(ed.getNumero() != null && !"".equals(ed.getNumero())){
								entregapagamento.getDocumentoantecipacao().setNumero(ed.getNumero());
							}
							documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
							if(!existeDocumentoantecipacaoEntrega(documentoorigem, listaDocumentoorigemAntecipacao)){
								listaDocumentoorigemAntecipacao.add(documentoorigem);
							}
						}
					}
					if(existDocumento){
						if(rateio != null)
							rateioService.limpaReferenciaRateio(rateio);
						documento.setRateio(rateio);
						documento.setListaDocumento(financiamentos);
						Money valorRateio = new Money();
						
						for (Rateioitem ri : ed.getRateio().getListaRateioitem()) {
							BigDecimal valor = SinedUtil.round(ri.getValor().getValue(), 2);
							valorRateio = valorRateio.add(new Money(valor));
						}
						
						valorRateio = documento.getValor().subtract(valorRateio);
						
						if (!valorRateio.toString().equals("0,00") && !valorRateio.toString().equals("-0,00")) {
							BigDecimal documentoValor = SinedUtil.round(documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).getValor().getValue(), 2);
							BigDecimal rateioValor = SinedUtil.round(valorRateio.getValue(), 2);
							documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).setValor(new Money(documentoValor).add(new Money(rateioValor)));
						}
						
						if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(ed.getEmpresa());
						}else if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(entrega.getEmpresa());
						} else if(entrega.getHaveOrdemcompra()){
							Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
							for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
								if(ordemcompraentrega.getOrdemcompra() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
									documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
									break;
								}
							}
						}
						if(!documento.getIsDescricaoHistorico()){
							String natop = entregadocumentoService.findNatop(entrega.getCdentrega());
							if (natop != null && !"".equals(natop)){
								documento.setDescricao(natop + ", referente a entrega: "+ entregasSelecionadas);
							}else{
								documento.setDescricao("Conta a pagar referente a entrega: "+ entregasSelecionadas + " - NF " + ed.getNumero());			
							}		
						}				
						documento.setFornecedor(ed.getFornecedor());
						documento.setPessoa(ed.getFornecedor());
						documento.setDtemissao(ed.getDtemissao());					
						documento.setFornecedor(ed.getFornecedor());
						documento.setDocumentotipo(ed.getDocumentotipo());
						if(ed.getDocumentotipo() != null)
							documento.setDocumentotipo(ed.getDocumentotipo());
						
						//contapagarService.saveOrUpdate(documento);
					}
				}
			}
		}
		return documento;
	}
	public void criarAvisoEntregaSemRegistroFinanceiro(Motivoaviso m, Date data, Date dateToSearch) {
		List<Entrega> entregaList = entregaDAO.findByEntradaSemRegistroFinanceiro(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Entrega e : entregaList) {
			Aviso aviso = new Aviso("Recebimento sem v�nculo a um registro financeiro", "C�digo do recebimento: " + e.getCdentrega(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), e.getCdentrega(), e.getEmpresa() != null ? e.getEmpresa() : empresa, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, e.getCdentrega());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, e.getCdentrega(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public Double getQtdeUnidadeOrdemcompra(Entregamaterial entregamaterial){
		Double qtdesol = entregamaterial.getXmlqtdeinformada();
		Unidademedida unidademedida = entregamaterial.getUnidademedidacomercial();
		Unidademedida unidademedidaAntiga = entregamaterial.getUnidademedidacomercialAnterior();
		
		if(unidademedida != null && unidademedidaAntiga != null && qtdesol != null){
			Material material = materialService.findListaMaterialunidademedida(entregamaterial.getMaterial());
			
			if(!material.getUnidademedida().equals(unidademedida) && !material.getUnidademedida().equals(unidademedidaAntiga)){
				Double fracao1 = unidademedidaService.getFracao(material, unidademedidaAntiga, material.getUnidademedida());
				Double fracao2 = unidademedidaService.getFracao(material, material.getUnidademedida(), unidademedidaAntiga);
				
				if(fracao1 != null && fracao2 != null){
					qtdesol = SinedUtil.round((qtdesol * fracao1) / fracao2, 20);
				}
				unidademedidaAntiga = material.getUnidademedida();
			}
			
			Double fracao1 = unidademedidaService.getFracao(material, unidademedidaAntiga, unidademedida);
			Double fracao2 = unidademedidaService.getFracao(material, unidademedida, unidademedidaAntiga);		
			
			if(fracao1 != null && fracao2 != null){
				qtdesol = SinedUtil.round((qtdesol * fracao1) / fracao2, 20);
			}
		}
		
		return qtdesol;
	}
	
	public ModelAndView ajaxValidacaoChaveAcesso(WebRequestContext request, String chaveacesso) {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String erro = null;
		Boolean confirm = null;
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(chaveacesso)){
			ManifestoDfe manifestoDfe = manifestoDfeService.procurarPorChaveAcessoImportacao(chaveacesso);
			
			if (manifestoDfe != null) {
				if (!LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA.equals(manifestoDfe.getStatusEnum())) {
					erro = "O documento fiscal correspondente a esta chave de acesso est� com a situa��o diferente de 'Autorizado'.";
				}
				
				if (erro == null && manifestoDfe.getArquivoNfeCompletoXml() == null) {
					erro = "O documento fiscal correspondente a esta chave de acesso n�o possui o XML completo dispon�vel. " +
							"Realize a manifesta��o atrav�s da tela 'Documentos Fiscais emitidos por Terceiros' ou realize a entrada atrav�s do XML.";
				}
				
				if (erro == null && (ManifestoDfeSituacaoEnum.DESCONHECIMENTO.equals(manifestoDfe.getManifestoDfeSituacaoEnum()) || 
						ManifestoDfeSituacaoEnum.NAO_REALIZADA.equals(manifestoDfe.getManifestoDfeSituacaoEnum()))) {
					confirm = Boolean.TRUE;
					String mensagem = "";
					if(ManifestoDfeSituacaoEnum.DESCONHECIMENTO.equals(manifestoDfe.getManifestoDfeSituacaoEnum())){
						mensagem = "'Desconhecimento da Opera��o'";
					}else if(ManifestoDfeSituacaoEnum.DESCONHECIMENTO.equals(manifestoDfe.getManifestoDfeSituacaoEnum())){
						mensagem = "'Opera��o n�o realizada'";
					}
					erro = "O documento fiscal correspondente foi manifestado como  " + mensagem + ". Deseja continuar com o registro?";
				}
			} else {
				erro = "O documento fiscal correspondente a esta chave de acesso n�o foi encontrada em nossa base. " +
						"Certifique-se que a nota j� foi sincronizada com o ambiente nacional.";
			}
		}
		
		jsonModelAndView.addObject("erro", erro);
		jsonModelAndView.addObject("confirm", confirm);
		
		return jsonModelAndView;
	}
	
	public void preencheListaApropriacaoDocumento(Documento documento, List<EntregaDocumentoApropriacao> listaEntregaDocumentoApropriacao) {
		if(SinedUtil.isListNotEmpty(listaEntregaDocumentoApropriacao)) {
			List<DocumentoApropriacao> listaDocumentoApropriacao = new ArrayList<DocumentoApropriacao>();
			for(EntregaDocumentoApropriacao item : listaEntregaDocumentoApropriacao) {
				DocumentoApropriacao apropriacao = new DocumentoApropriacao();
				apropriacao.setDocumento(documento);
				apropriacao.setMesAno(item.getMesAno());
				apropriacao.setValor(item.getValor());
				
				listaDocumentoApropriacao.add(apropriacao);
			}
		
			documento.setListaApropriacao(listaDocumentoApropriacao);
		}
	}
	
	public void validarEntregadocumento(Entregadocumento bean, BindException errors) {
		try {
			if(bean.getSimplesremessa() == null || !bean.getSimplesremessa()){
				rateioService.validateRateio(bean.getRateio(), bean.getValor());
			}
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}
		
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaEntregamaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaEntregamaterial((Set<Entregamaterial>) attribute);
		}
		
		if(bean.getListaEntregamaterial() != null && !bean.getListaEntregamaterial().isEmpty()){
			String msgErroCfopescopo = "";
			String nomeMaterial;
			for(Entregamaterial em : bean.getListaEntregamaterial()){
				if(em.getCfop() != null && em.getCfop().getCdcfop() != null){
						em.setCfop(cfopService.carregarCfop(em.getCfop()));
						if(em.getCfop().getCodigo() != null){
//							if("5".equals(em.getCfop().getCodigo().subSequence(0, 1)) || "6".equals(em.getCfop().getCodigo().subSequence(0, 1)) || 
//									"7".equals(em.getCfop().getCodigo().subSequence(0, 1))){
//								errors.reject("001","CFOP informado deve ser do tipo Entrada. " + (em.getMaterial() != null ? "Material " + materialService.load(em.getMaterial(), "material.nome").getNome() : "") + "\n");
//							}
						}
						msgErroCfopescopo = cfopService.verificaCfopescopo(bean.getFornecedor(), bean.getEmpresa(), em.getCfop());
						if(msgErroCfopescopo != null && !"".equals(msgErroCfopescopo)){
							errors.reject("001",msgErroCfopescopo + (em.getMaterial() != null ? " Material " + materialService.load(em.getMaterial(), "material.nome").getNome() : "") + "\n");
						}
				}	
				if(SinedUtil.isRateioMovimentacaoEstoque() && bean.getEntrega() != null && bean.getEntrega().getCdentrega() != null && em.getCentrocusto() == null){
					nomeMaterial = null;
					if(em.getMaterial() != null){
						if(em.getMaterial().getNome() != null){
							nomeMaterial = em.getMaterial().getNome();
						}else {
							nomeMaterial = materialService.loadSined(em.getMaterial(), "material.nome").getNome();
						}
					}
					errors.reject("001", "O campo Centro de custo � obrigat�rio. " + (nomeMaterial != null ? " Material " +  nomeMaterial : "") + "\n");
				}
			}
		}
		if(validaFornecedornumero(null, bean)){
			errors.reject("001", "Existe fornecedor e n�mero j� cadastrado no sistema.");
		}
		if (!entradafiscalService.validaPagamentoAntecipado(bean)){
			errors.reject("001","O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
		}
		
		if(bean.getListaEntregadocumentofrete() != null && bean.getListaEntregadocumentofrete().size() > 0){
			Money valorDocumento = bean.getValor();
			Money valorRateio = new Money(0);
			if (bean.getListaEntregadocumentofreterateio()!=null && !bean.getListaEntregadocumentofreterateio().isEmpty()){
				for (Entregadocumentofreterateio entregadocumentofreterateio: bean.getListaEntregadocumentofreterateio()){
					valorRateio = valorRateio.add(entregadocumentofreterateio.getValor());
				}
			}
			if (valorDocumento.toLong()!=valorRateio.toLong()){
				errors.reject("001", "O valor do rateio n�o pode diferente do valor total da nota.");
			}
		}
			
		if(bean.getFornecedor() != null){
			try {
				colaboradorFornecedorService.validaPermissao(bean.getFornecedor());
			} catch (SinedException e) {
				errors.reject("001",e.getMessage());
			}
		}
	}
}