package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.dao.TipodependenciarecursoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipodependenciarecursoService extends GenericService<Tipodependenciarecurso>{

	private TipodependenciarecursoDAO tipodependenciarecursoDAO;
	
	public void setTipodependenciarecursoDAO(TipodependenciarecursoDAO tipodependenciarecursoDAO) {
		this.tipodependenciarecursoDAO = tipodependenciarecursoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TipodependenciarecursoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	public List<Tipodependenciarecurso> findAllForFlex(){
		return tipodependenciarecursoDAO.findAllForFlex();
	}	
	
}
