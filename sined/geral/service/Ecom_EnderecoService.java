package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ecom_Cliente;
import br.com.linkcom.sined.geral.bean.Ecom_Endereco;
import br.com.linkcom.sined.geral.dao.Ecom_EnderecoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class Ecom_EnderecoService extends GenericService<Ecom_Endereco>{

	private Ecom_EnderecoDAO ecom_EnderecoDAO;
	
	public void setEcom_EnderecoDAO(Ecom_EnderecoDAO ecom_EnderecoDAO) {
		this.ecom_EnderecoDAO = ecom_EnderecoDAO;
	}
	
	public List<Ecom_Endereco> findByCliente(Ecom_Cliente cliente){
		return ecom_EnderecoDAO.findByCliente(cliente);
	}
}
