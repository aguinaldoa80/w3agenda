package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.dao.SindicatoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SindicatoService extends GenericService<Sindicato> {

	private SindicatoDAO sindicatoDAO;
	
	public void setSindicatoDAO(SindicatoDAO sindicatoDAO) {
		this.sindicatoDAO = sindicatoDAO;
	}
	
	/* singleton */
	private static SindicatoService instance;
	public static SindicatoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(SindicatoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o sindicato pelo seu c�digo folha. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SindicatoDAO#findByCodigofolha(Integer codigofolha)
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Sindicato findByCodigofolha(Integer codigofolha) {
		return sindicatoDAO.findByCodigofolha(codigofolha);
	}
}
