package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaArquivo;
import br.com.linkcom.sined.geral.dao.PropostaArquivoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PropostaArquivoService extends GenericService<PropostaArquivo>{
	
	private PropostaArquivoDAO propostaArquivoDAO;
	
	public void setPropostaArquivoDAO(PropostaArquivoDAO propostaArquivoDAO) {
		this.propostaArquivoDAO = propostaArquivoDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 01/02/2012
	 * @param proposta
	 * @return
	 */
	public List<PropostaArquivo> findListByProposta(Proposta proposta){
		return propostaArquivoDAO.findListByProposta(proposta);
	}

}
