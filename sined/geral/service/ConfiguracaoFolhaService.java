package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolha;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ConfiguracaoFolhaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaoFolhaService extends GenericService<ConfiguracaoFolha>{

	private ConfiguracaoFolhaDAO configuracaoFolhaDAO;
	
	public void setConfiguracaoFolhaDAO(
			ConfiguracaoFolhaDAO configuracaoFolhaDAO) {
		this.configuracaoFolhaDAO = configuracaoFolhaDAO;
	}
	
	public ConfiguracaoFolha loadForFechamentoFolha(Empresa empresa){
		return configuracaoFolhaDAO.loadForFechamentoFolha(empresa);
	}	
	
	public boolean existeConfiguracaoFolha(Agendamento agendamento){
		return configuracaoFolhaDAO.existeConfiguracaoFolha(agendamento);
	}
}
