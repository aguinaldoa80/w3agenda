package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominiozona;
import br.com.linkcom.sined.geral.dao.DominiozonaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominiozonaService extends GenericService<Dominiozona>{

	private DominiozonaDAO dominiozonaDAO;
	
	public void setDominiozonaDAO(DominiozonaDAO dominiozonaDAO) {
		this.dominiozonaDAO = dominiozonaDAO;
	}
	
	/**
	 * Retorna lista de dominiozona de acordo com o dominio
	 * @param dominio
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominiozona> findByDominio(Dominio dominio){
		return dominiozonaDAO.findByDominio(dominio);		
	}
	
	/**
	 * Retorna lista de zonas de dom�nio que possuam servi�os do tipo SMTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominiozona> findByServicoCaixaPostal(){
		return dominiozonaDAO.findByServicoCaixaPostal();
	}
}
