package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoHistorico;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.dao.MaterialTabelaPrecoHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialTabelaPrecoHistoricoService extends GenericService<MaterialTabelaPrecoHistorico> {

	private static MaterialTabelaPrecoHistoricoService instance;
	private ArquivoService arquivoService;
	private MaterialTabelaPrecoHistoricoDAO materialTabelaPrecoHistoricoDAO;

	public static MaterialTabelaPrecoHistoricoService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(MaterialTabelaPrecoHistoricoService.class);
		}
		return instance;
	}

	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}

	public void setMaterialTabelaPrecoHistoricoDAO(MaterialTabelaPrecoHistoricoDAO materialTabelaPrecoHistoricoDAO) {
		this.materialTabelaPrecoHistoricoDAO = materialTabelaPrecoHistoricoDAO;
	}

	public void salvar(Materialtabelapreco materialtabelapreco, String observacao, Arquivo arquivoImportacaoProduto) {
		MaterialTabelaPrecoHistorico materialTabelaPrecoHistorico = new MaterialTabelaPrecoHistorico();
		materialTabelaPrecoHistorico.setMaterialtabelapreco(materialtabelapreco);
		materialTabelaPrecoHistorico.setObservacao(observacao);

		if (arquivoImportacaoProduto != null) {
			materialTabelaPrecoHistorico.setArquivoImportacaoProduto(arquivoImportacaoProduto);
			arquivoService.saveFile(materialTabelaPrecoHistorico, "arquivoImportacaoProduto");
		}

		saveOrUpdate(materialTabelaPrecoHistorico);
	}

	public List<MaterialTabelaPrecoHistorico> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		return materialTabelaPrecoHistoricoDAO.findByMaterialtabelapreco(materialtabelapreco);
	}
}
