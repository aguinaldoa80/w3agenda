package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.DespesaviagemHistorico;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.geral.bean.Despesaviagemprojeto;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemdestino;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.DespesaviagemDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.OrigemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DespesaviagemFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DespesaviagemService extends GenericService<Despesaviagem> {

	private DespesaviagemDAO despesaviagemDAO;
	private RateioitemService rateioitemService;
	private EnvioemailService envioemailService;
	private DespesaviagemHistoricoService despesaviagemHistoricoService;
	private RateioService rateioService;
	private DocumentoorigemService documentoorigemService;
	
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setDespesaviagemDAO(DespesaviagemDAO despesaviagemDAO) {
		this.despesaviagemDAO = despesaviagemDAO;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setDespesaviagemHistoricoService(DespesaviagemHistoricoService despesaviagemHistoricoService) {
		this.despesaviagemHistoricoService = despesaviagemHistoricoService;
	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForVerificacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForVerificacao(String whereIn) {
		return despesaviagemDAO.findForVerificacao(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#updateSituacao
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Situacaodespesaviagem situacaodespesaviagem) {
		despesaviagemDAO.updateSituacao(whereIn, situacaodespesaviagem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForRelizacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForRealizacao(String whereIn) {
		return despesaviagemDAO.findForRealizacao(whereIn); 
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#updateDtretornorealizado
	 * @param despesaviagem
	 * @author Rodrigo Freitas
	 */
	public void updateDtretornorealizado(Despesaviagem despesaviagem) {
		despesaviagemDAO.updateDtretornorealizado(despesaviagem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#loadWithColaborador
	 * @param despesaviagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Despesaviagem loadWithColaborador(Despesaviagem despesaviagem) {
		return despesaviagemDAO.loadWithColaborador(despesaviagem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#calculaDiferencaAcerto
	 * @param despesaviagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Double calculaDiferencaAcerto(Despesaviagem despesaviagem) {
		return despesaviagemDAO.calculaDiferencaAcerto(despesaviagem);
	}
	
	public Boolean podeEstornar(String whereIn) {
		return despesaviagemDAO.podeEstornar(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#buildAdiantamentos
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<OrigemBean> buildAdiantamentos(Despesaviagem form, String whereIn) {
		return despesaviagemDAO.buildAdiantamentos(form, whereIn);
	}

	public List<OrigemBean> buildAcertos(Despesaviagem form) {
		return despesaviagemDAO.buildAcertos(form);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#getValorTotalAdiantamentos(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param tipooperacao 
	 */
	public Money getValorTotalAdiantamentos(String whereIn, Tipooperacao tipooperacao) {
		return despesaviagemDAO.getValorTotalAdiantamentos(whereIn, tipooperacao);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.DespesaviagemDAO#getValorTotalAcertos(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param tipooperacao 
	 */
	public Money getValorTotalAcertos(String whereIn, Tipooperacao tipooperacao) {
		return despesaviagemDAO.getValorTotalAcertos(whereIn, tipooperacao);
	}
	
	/**
	 * Retorno o total de acertos e adiantamentos de acordo com o par�metro
	 * 
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#getValorTotalAdiantamentos(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#getValorTotalAcertos(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorTotalAdiantamentosAcertos(String whereIn, Tipooperacao tipooperacao){
		Money total = new Money();
		Money totaladiantamentos = getValorTotalAdiantamentos(whereIn, tipooperacao);
		Money totalacertos = getValorTotalAcertos(whereIn, tipooperacao);
		if(totaladiantamentos != null)
			total = total.add(totaladiantamentos);
		if(totalacertos != null)
			total = total.add(totalacertos);
		return total;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForAcerto(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForAcerto(String whereIn) {
		return despesaviagemDAO.findForAcerto(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#loadForAtualizarRateio(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public Despesaviagem loadForAtualizarRateio(String whereIn) {
		return despesaviagemDAO.loadForAtualizarRateio(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#loadForAtualizarRateioByDespesaviagem(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> loadForAtualizarRateioByDespesaviagem(String whereIn) {
		return despesaviagemDAO.loadForAtualizarRateioByDespesaviagem(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/11/2012
	 */
	public Despesaviagem findForAdiatamento(String whereIn) {
		return despesaviagemDAO.findForAdiatamento(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForAdiatamentoByDespesaviagem(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForAdiatamentoByDespesaviagem(String whereIn) {
		return despesaviagemDAO.findForAdiatamentoByDespesaviagem(whereIn);
	}

	public Rateio criaRateioForAtualizarvalores(Money valorDocumento, List<Despesaviagem> listaDespesaviagem, Boolean adiantamento, Boolean credito) {
		Rateio rateio = new Rateio(); 
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			Money totalrealizado = new Money();
			for (Despesaviagem despesaviagem : listaDespesaviagem) {
				totalrealizado = totalrealizado.add(despesaviagem.getTotalrealizado());
			}
			for (Despesaviagem despesaviagem : listaDespesaviagem) {
				Empresa empresa = despesaviagem.getEmpresa();
				Contagerencial contagerencial = null;
				Centrocusto centrocusto = null;
				if(empresa != null){
					contagerencial = adiantamento != null && adiantamento ? empresa.getContagerencialdebitoadiantamento() : (credito != null && credito ? empresa.getContagerencialcredito() : empresa.getContagerencialdebito());
					centrocusto = empresa.getCentrocustodespesaviagem();
				}
				
				if(adiantamento != null && adiantamento){
					List<Despesaviagemprojeto> listaDespesaviagemprojeto = despesaviagem.getListaDespesaviagemprojeto();
					if(listaDespesaviagemprojeto != null && listaDespesaviagemprojeto.size() > 0){
						for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
							Money valorIt = valorDocumento.multiply(despesaviagemprojeto.getPercentual().divide(new Money(100d)));
							listaRateioitem.add(new Rateioitem(contagerencial, getCentrocustoRateio(despesaviagemprojeto.getProjeto(), centrocusto), despesaviagemprojeto.getProjeto(), valorIt, 0d));
						}
					} else {
						listaRateioitem.add(new Rateioitem(contagerencial, centrocusto, null, valorDocumento, 0d));
					}
				} else {
					if(credito != null && credito){
						List<Despesaviagemprojeto> listaDespesaviagemprojeto = despesaviagem.getListaDespesaviagemprojeto();
						if(listaDespesaviagemprojeto != null && listaDespesaviagemprojeto.size() > 0){
							for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
								Money valorIt = valorDocumento.multiply(despesaviagemprojeto.getPercentual().divide(new Money(100d)));
								listaRateioitem.add(new Rateioitem(contagerencial, getCentrocustoRateio(despesaviagemprojeto.getProjeto(), centrocusto), despesaviagemprojeto.getProjeto(), valorIt, 0d));
							}
						} else {
							listaRateioitem.add(new Rateioitem(contagerencial, centrocusto, null, valorDocumento, 0d));
						}
					} else {
						List<Despesaviagemitem> listaDespesaviagemitem = despesaviagem.getListaDespesaviagemitem();
						List<Despesaviagemprojeto> listaDespesaviagemprojeto = despesaviagem.getListaDespesaviagemprojeto();
						if(listaDespesaviagemitem != null && listaDespesaviagemitem.size() > 0){
							for (Despesaviagemitem despesaviagemitem : listaDespesaviagemitem) {
								Contagerencial contagerencialIt = 
										despesaviagemitem.getDespesaviagemtipo() != null && 
										despesaviagemitem.getDespesaviagemtipo().getContagerencial() != null ? 
										despesaviagemitem.getDespesaviagemtipo().getContagerencial() : 
										contagerencial;
										
								if(despesaviagemitem.getValorrealizado() != null && totalrealizado != null && totalrealizado.compareTo(new Money()) != 0){
									Money percentIt = despesaviagemitem.getValorrealizado().divide(totalrealizado);
									if(listaDespesaviagemprojeto != null && listaDespesaviagemprojeto.size() > 0){
										for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
											Money valorIt = valorDocumento.multiply(despesaviagemprojeto.getPercentual().divide(new Money(100d))).multiply(percentIt);
											listaRateioitem.add(new Rateioitem(contagerencialIt, getCentrocustoRateio(despesaviagemprojeto.getProjeto(), centrocusto), despesaviagemprojeto.getProjeto(), valorIt, 0d));
										}
									} else {
										Money valorIt = valorDocumento.multiply(percentIt);
										listaRateioitem.add(new Rateioitem(contagerencialIt, centrocusto, null, valorIt, 0d));
									}
								}
							}
						} else {
							if(listaDespesaviagemprojeto != null && listaDespesaviagemprojeto.size() > 0){
								for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
									Money valorIt = valorDocumento.multiply(despesaviagemprojeto.getPercentual().divide(new Money(100d)));
									listaRateioitem.add(new Rateioitem(contagerencial, getCentrocustoRateio(despesaviagemprojeto.getProjeto(), centrocusto), despesaviagemprojeto.getProjeto(), valorIt, 0d));
								}
							} else {
								listaRateioitem.add(new Rateioitem(contagerencial, centrocusto, null, valorDocumento, 0d));
							}
						}
					}
				}
				
				
			}
		}
		
		
		if(listaRateioitem.size() > 1){
			rateioitemService.agruparItensRateio(listaRateioitem);
		}
		rateioService.ajustaPercentualRateioitem(valorDocumento, listaRateioitem);
		rateio.setListaRateioitem(listaRateioitem);
		return rateio;
	}
	
	/**
	* M�todo que retorna o centro de custo do projeto caso o centro de custo do par�metro seja nulo
	*
	* @param projeto
	* @param centrocusto
	* @return
	* @since 07/03/2016
	* @author Luiz Fernando
	*/
	private Centrocusto getCentrocustoRateio(Projeto projeto, Centrocusto centrocusto) {
		return centrocusto != null || projeto == null ? centrocusto : projeto.getCentrocusto();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForGerarAcerto(String whereInDespesaviagem)
	 *
	 * @param whereInDespesaviagem
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForGerarAcerto(String whereInDespesaviagem) {
		return despesaviagemDAO.findForGerarAcerto(whereInDespesaviagem);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForGerarreceitalote(String whereInProjeto)
	 *
	 * @param whereInProjeto
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForGerarreceitalote(String whereInProjeto) {
		return despesaviagemDAO.findForGerarreceitalote(whereInProjeto);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#getTotalDespesaviagemByColaborador(Empresa empresa, Colaborador colaborador, Date dataGerada)
	 *
	 * @param empresa
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getTotalDespesaviagemByColaborador(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2) {
		return despesaviagemDAO.getTotalDespesaviagemByColaborador(empresa, colaborador, dtreferencia1, dtreferencia2);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param listaDespesaviagemprojeto
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> getProjetos(List<Despesaviagemprojeto> listaDespesaviagemprojeto) {
		List<Projeto> projetos = new ListSet<Projeto>(Projeto.class);
		for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
			if(despesaviagemprojeto.getProjeto() != null && despesaviagemprojeto.getProjeto().getCdprojeto()!= null)
				projetos.add(despesaviagemprojeto.getProjeto());
		}
		
		return projetos;
	}
	
	public List<Despesaviagem> findForCSV(DespesaviagemFiltro filtro) {
		return despesaviagemDAO.findForCSV(filtro);
	}
	
	public void enviaEmailAutorizacao(String whereIn) {

		List<Despesaviagem> lista = despesaviagemDAO.findForEnviarEmail(whereIn);

		for (Despesaviagem despesaviagem : lista) {
			if (despesaviagem.getEmpresa() != null) {
				if (despesaviagem.getEmpresa().getEmailfinanceiro() != null	&& despesaviagem.getEmpresa().getEmail() != null) {
					try {
						if (GenericValidator.isEmail(despesaviagem.getEmpresa().getEmailfinanceiro())
								&& GenericValidator.isEmail(despesaviagem.getEmpresa().getEmail())) {

							String assunto = "Despesa de viagem "+ despesaviagem.getCddespesaviagem();
							String mensagem = "A despesa de viagem "+ despesaviagem.getCddespesaviagem()+ " foi autorizada. Consulte no W3ERP para maiores informa��es.";

							Envioemail envioemail = envioemailService.registrarEnvio(despesaviagem.getEmpresa().getEmailfinanceiro(), assunto,mensagem, Envioemailtipo.COMUNICADO);

							EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
							email.setFrom(despesaviagem.getEmpresa().getEmail());
							email.setSubject(assunto);
							email.setTo(despesaviagem.getEmpresa().getEmailfinanceiro());
							email.addHtmlText(mensagem+ EmailUtil.getHtmlConfirmacaoEmail(envioemail, despesaviagem.getEmpresa().getEmail()));
							email.sendMessage();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param itensSelecionados
	 * @return
	 */
	public List<Despesaviagem> findForEmitirFichaDespesaViagem(String whereIn) {
		return despesaviagemDAO.findForEmitirFichaDespesaViagem(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 */
	public List<Despesaviagem> loadWithLista(String whereIn, String orderBy, boolean asc){
		return despesaviagemDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Preenche os totais da tela de despesa viagem
	 * @param filtro
	 */
	public void preencheTotais(DespesaviagemFiltro filtro) {
		List<Despesaviagem> lista = despesaviagemDAO.findForPreencherTotal(filtro);
		
		Money valorPrevisto = new Money();
		Money valorRealizado = new Money();
		
		for(Despesaviagem despesaViagem : lista){
			if(despesaViagem.getTotalprevisto() != null){
				valorPrevisto = valorPrevisto.add(despesaViagem.getTotalprevisto());
			}
			
			if(despesaViagem.getTotalrealizado() != null){
				valorRealizado = valorRealizado.add(despesaViagem.getTotalrealizado());
			}
		}
		
		filtro.setTotalPrevisto(valorPrevisto);
		filtro.setTotalRealizado(valorRealizado);
	}
	
	/**
	 * Salva o historico de altera��es das despesasViagens
	 * @param whereIn
	 * @param autorizada
	 */
	public void salvaHistorico(String whereIn, Despesaviagemacao acao, String observacao) {
		DespesaviagemHistorico historico = new DespesaviagemHistorico();
		historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		historico.setUsuario(SinedUtil.getUsuarioLogado());
		historico.setAcao(acao);
		historico.setObservacao(observacao);
		
		String[] array = whereIn.split(",");
		for(int i=0; i<array.length; i++){
			historico.setCddespesaviagemhistorico(null);
			historico.setDespesaviagem(new Despesaviagem(Integer.parseInt(array[i])));
			despesaviagemHistoricoService.saveOrUpdate(historico);
		}
	}
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForReembolso(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public List<Despesaviagem> findForReembolso(String whereIn) {
		return despesaviagemDAO.findForReembolso(whereIn);
	}
	
	/**
	* M�todo que cria o documento de reembolso de acordo com as despesas de viagem
	* @param listaDespesaviagem
	* @return
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public Documento criaDocumentoReemboso(List<Despesaviagem> listaDespesaviagem) {
		Documento documento = new Documento();
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		
		if(SinedUtil.isListNotEmpty(listaDespesaviagem)){
			String whereIn = CollectionsUtil.listAndConcatenate(listaDespesaviagem, "cddespesaviagem", ",");
			StringBuilder observacao = new StringBuilder();
			Money valorTotalRealizado = new Money();
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			for(Despesaviagem despesaviagem : listaDespesaviagem){
				if(despesaviagem.getEmpresa() != null){
					documento.setEmpresa(despesaviagem.getEmpresa());
				}
				if(Despesaviagemdestino.CLIENTE.equals(despesaviagem.getDespesaviagemdestino())){
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					if(despesaviagem.getCliente() != null){
						documento.setCliente(despesaviagem.getCliente());
					}
				}else if(Despesaviagemdestino.FORNECEDOR.equals(despesaviagem.getDespesaviagemdestino())){
					documento.setTipopagamento(Tipopagamento.FORNECEDOR);
					if(despesaviagem.getFornecedor() != null){
						documento.setFornecedor(despesaviagem.getFornecedor());
					}
				}
				
				valorTotalRealizado = valorTotalRealizado.add(despesaviagem.getTotalrealizado());
				
				observacao.append(despesaviagem.getCddespesaviagem() + " - Data da viagem: " + 
							(despesaviagem.getDtsaida() != null ? format.format(despesaviagem.getDtsaida()) : "") + " - " + 
							(despesaviagem.getDtretornorealizado() != null ? format.format(despesaviagem.getDtretornorealizado()) : ""));
				observacao.append("\n");
				
				if(SinedUtil.isListNotEmpty(despesaviagem.getListaDespesaviagemitem())){
					for(Despesaviagemitem despesaviagemitem : despesaviagem.getListaDespesaviagemitem()){
						if(despesaviagemitem.getDespesaviagemtipo() != null){
							observacao.append(despesaviagemitem.getDespesaviagemtipo().getNome())
									  .append(" - ")
									  .append(despesaviagemitem.getDocumento() != null ? despesaviagemitem.getDocumento() : "")
									  .append(" - ")
									  .append(despesaviagemitem.getValorrealizado() != null ? despesaviagemitem.getValorrealizado() : "")
									  .append("\n");
						}
					}
				}
				
			}
			
			String[] split = whereIn.split(",");
			String whereInDescricao = CollectionsUtil.concatenate(Arrays.asList(split), ", ");
			
			documento.setDescricao("Reembolso referente as despesas de viagem " + whereInDescricao);
			documento.setDtemissao(new Date(System.currentTimeMillis()));
			documento.setValor(valorTotalRealizado);
			documento.setDocumentoacao(Documentoacao.PREVISTA);
			documento.setObservacao(observacao.toString());
			documento.setWhereInDespesaviagemReembolso(whereIn);
			documento.setNumero(whereInDescricao);
		}
		
		return documento;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#updateReembolso(String whereInDespesaviagemReembolso, boolean reembolsogerado)
	*
	* @param whereInDespesaviagemReembolso
	* @param reembolsogerado
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public void updateReembolso(String whereInDespesaviagemReembolso, boolean reembolsogerado) {
		despesaviagemDAO.updateReembolso(whereInDespesaviagemReembolso, reembolsogerado);
	}
	
	/**
	 * @param list
	 * @param listaAdiantamento
	 * @since 21/08/2015
	 * @author Andrey Leonardo
	 */
	public void calculaTotalAdiantamento(List<Despesaviagem> list, List<OrigemBean> listaAdiantamento){		
		for(OrigemBean adiantamento : listaAdiantamento){
			for(Despesaviagem despesa : list){
				if(adiantamento.getIdInteger().equals(despesa.getCddespesaviagem())){
					Money totalAdiantamento = despesa.getTotaladiantamento();
					if(totalAdiantamento == null){
						totalAdiantamento = adiantamento.getValorMoney();
					}else{
						totalAdiantamento = totalAdiantamento.add(adiantamento.getValorMoney());
					}
					despesa.setTotaladiantamento(totalAdiantamento);
					break;
				}
			}
		}
	}
	
	/**
	* M�todo que registra no hist�rico da despesa de viagem o cancelamento do documento
	*
	* @param whereInDocumento
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public void verificaDespesaviagemAposCancelamentoDocumento(String whereInDocumento) {
		if(StringUtils.isNotBlank(whereInDocumento)){
			List<Documentoorigem> listaDocumentoorigem = documentoorigemService.findDocumentoWithDespesaviagem(whereInDocumento);
			if(SinedUtil.isListNotEmpty(listaDocumentoorigem)){
				List<Despesaviagem> listaDespesaviagem = new ArrayList<Despesaviagem>();
				Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
				Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
				String funcao;
				for(Documentoorigem docorigem : listaDocumentoorigem){
					Despesaviagem despesaviagem = null;
					if(docorigem.getDespesaviagemacerto() != null){
						despesaviagem = docorigem.getDespesaviagemacerto();
					}else if(docorigem.getDespesaviagemadiantamento() != null){
						despesaviagem = docorigem.getDespesaviagemadiantamento();
					}
					
					if(despesaviagem != null){
						funcao = Documentoclasse.OBJ_PAGAR.equals(docorigem.getDocumento().getDocumentoclasse()) ? "visualizaContapagar" : "visualizaContareceber";
						DespesaviagemHistorico dvh = new DespesaviagemHistorico();
						dvh.setDespesaviagem(despesaviagem);
						dvh.setDtaltera(dtaltera);
						dvh.setUsuario(usuarioLogado);
						dvh.setObservacao("Conta cancelada: " + "<a href=\"javascript:" + funcao + "("+ 
								docorigem.getDocumento().getCddocumento() +");\">"+docorigem.getDocumento().getCddocumento()+"</a>.");
						despesaviagemHistoricoService.saveOrUpdate(dvh);
						
						if(!listaDespesaviagem.contains(despesaviagem)){
							listaDespesaviagem.add(despesaviagem);
						}
					}
				}
				
				verificaSituacaoDespesaviagem(listaDespesaviagem);
			}
		}
	}
	
	/**
	* M�todo que verifica e atualiza a despesa de viagem com a situa��o anterior
	*
	* @param listaDespesaviagem
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public void verificaSituacaoDespesaviagem(List<Despesaviagem> listaDespesaviagem){
		if(SinedUtil.isListNotEmpty(listaDespesaviagem)){
			listaDespesaviagem = findForVerificacaoSituacaoAnterior(CollectionsUtil.listAndConcatenate(listaDespesaviagem, "cddespesaviagem", ","));
			if(SinedUtil.isListNotEmpty(listaDespesaviagem)){
				Situacaodespesaviagem situacaodespesaviagem;
				for(Despesaviagem despesaviagem : listaDespesaviagem){
					situacaodespesaviagem = null;
					if(SinedUtil.isListNotEmpty(despesaviagem.getListaDespesaviagemitem())){
						for(Despesaviagemitem despesaviagemitem : despesaviagem.getListaDespesaviagemitem()){
							if(despesaviagemitem.getValorrealizado() != null && despesaviagemitem.getValorrealizado().getValue().doubleValue() > 0){
								situacaodespesaviagem = Situacaodespesaviagem.REALIZADA;
								break;
							}
						}
						if(situacaodespesaviagem == null){
							if(SinedUtil.isListNotEmpty(despesaviagem.getListaDespesaviagemitem())){
								for(DespesaviagemHistorico despesaviagemHistorico : despesaviagem.getListaDespesaviagemHistorico()){
									if(Despesaviagemacao.AUTORIZADA.equals(despesaviagemHistorico.getAcao())){
										situacaodespesaviagem = Situacaodespesaviagem.AUTORIZADA;
										break;
									}
								}
							}
						}
						if(situacaodespesaviagem == null){
							situacaodespesaviagem = Situacaodespesaviagem.EM_ABERTO;
						}
						updateSituacao(despesaviagem.getCddespesaviagem().toString(), situacaodespesaviagem);
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DespesaviagemDAO#findForVerificacaoSituacaoAnterior(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public List<Despesaviagem> findForVerificacaoSituacaoAnterior(String whereIn) {
		return despesaviagemDAO.findForVerificacaoSituacaoAnterior(whereIn);
	}
}
