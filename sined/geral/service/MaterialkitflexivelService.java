package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialkitflexivel;
import br.com.linkcom.sined.geral.bean.Materialkitflexivelformula;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.MaterialVO;
import br.com.linkcom.sined.geral.dao.MaterialkitflexivelDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialkitflexivelService extends GenericService<Materialkitflexivel> {

	private MaterialkitflexivelDAO materialkitflexivelDAO;
	private ProdutoService produtoService;
	
	public void setMaterialkitflexivelDAO(MaterialkitflexivelDAO materialkitflexivelDAO) {
		this.materialkitflexivelDAO = materialkitflexivelDAO;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}

	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		materialkitflexivelDAO.deleteWhereNotInMaterial(material, whereIn);
	}
	
	/* singleton */
	private static MaterialkitflexivelService instance;
	public static MaterialkitflexivelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialkitflexivelService.class);
		}
		return instance;
	}

	public void calculaQuantidadeKitflexivel(Material material) throws ScriptException{
		if(material.getListaMaterialkitflexivel() != null){
			List<String> idsMateriais = new ArrayList<String>();
			Pattern pattern_material = Pattern.compile("formula\\.[a-zA-Z]+Material\\(([0-9]+)\\)");
			
			for (Materialkitflexivel mkf : material.getListaMaterialkitflexivel()) {
				if(mkf.getListaMaterialkitflexivelformula() != null){
					for (Materialkitflexivelformula mkff : mkf.getListaMaterialkitflexivelformula()) {
						Matcher m_material = pattern_material.matcher(mkff.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
					}
				}
			}
			
			FormulaVO formulaVO = new FormulaVO();
			
			if(idsMateriais.size() > 0){
				List<Produto> listaMaterial = produtoService.findForCalculoFormula(CollectionsUtil.concatenate(idsMateriais, ","));
				for (Produto p : listaMaterial) {
					MaterialVO materialVO = new MaterialVO();
					materialVO.setCdmaterial(p.getCdmaterial());
					materialVO.setAltura(p.getAltura() != null ? p.getAltura() : 0.0);
					materialVO.setLargura(p.getLargura() != null ? p.getLargura() : 0.0);
					materialVO.setPeso(p.getPeso() != null ? p.getPeso() : 0.0);
					formulaVO.addMaterial(materialVO);
				}
			}
			
			for (Materialkitflexivel mkf : material.getListaMaterialkitflexivel()) {
				MaterialVO materialVO = new MaterialVO();
				if(mkf.getMaterialkit() != null){
					materialVO.setCdmaterial(mkf.getMaterialkit().getCdmaterial());
				}
				formulaVO.addMaterialProducao(materialVO);
			}
			
			if(material.getQuantidade() != null)
				formulaVO.setQuantidadeMaterialVenda(material.getQuantidade());
			if(material.getProduto_altura() != null)
				formulaVO.setAlturaMaterialVenda(material.getProduto_altura());
			if(material.getProduto_largura() != null)
				formulaVO.setLarguraMaterialVenda(material.getProduto_largura());
			
			for (Materialkitflexivel mkf : material.getListaMaterialkitflexivel()) {
				List<Materialkitflexivelformula> listaMaterialkitflexivelformula = mkf.getListaMaterialkitflexivelformula();
				if(listaMaterialkitflexivelformula != null && listaMaterialkitflexivelformula.size() > 0){
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine engine = manager.getEngineByName("JavaScript");
					
					engine.put("formula", formulaVO);
					
					Collections.sort(listaMaterialkitflexivelformula, new Comparator<Materialkitflexivelformula>(){
						public int compare(Materialkitflexivelformula o1, Materialkitflexivelformula o2) {
							return o1.getOrdem().compareTo(o2.getOrdem());
						}
					});
					
					Double consumo = 0.0;
					
					for (Materialkitflexivelformula materialkitflexivelformula : listaMaterialkitflexivelformula) {
						Object obj = engine.eval(materialkitflexivelformula.getFormula());

						Double resultado = 0d;
						if(obj != null){
							String resultadoStr = obj.toString();
							resultado = new Double(resultadoStr);
						}
						
						engine.put(materialkitflexivelformula.getIdentificador(), resultado);
						consumo = resultado;
					}
					
					mkf.setQuantidade(consumo);
				}
			}
			
		}
	}
	
	public Double getValorvendakitflexivel(Material material) throws ScriptException {
		if(material.getListaMaterialkitflexivel() != null){
			this.calculaQuantidadeKitflexivel(material);
			
			Double valorconsumoTotal = 0.0;
			
			for (Materialkitflexivel mkf : material.getListaMaterialkitflexivel()) {
				if(mkf.getQuantidade() == null) mkf.setQuantidade(0d);
				
				Double valorconsumo = (mkf.getValorvenda() != null ? mkf.getValorvenda().getValue().doubleValue() : 0.0) * (mkf.getQuantidade() != null ? mkf.getQuantidade() : 0.0);
				
				if(material.getQuantidade() != null && material.getQuantidade() > 0 && 
						mkf.getListaMaterialkitflexivelformula() == null || 
						mkf.getListaMaterialkitflexivelformula().isEmpty()){
					valorconsumo = valorconsumo * material.getQuantidade();
				}
				mkf.setValorvenda(new Money(valorconsumo));
				valorconsumoTotal += valorconsumo;
			}
			
			if(material.getFrete() != null){
				Double fretePercentual = material.getFrete().getValue().doubleValue();
				if(fretePercentual > 0){
					Double freteValor = (fretePercentual * valorconsumoTotal) / 100d;
					valorconsumoTotal += freteValor;
				}
			}
			
			return SinedUtil.roundByParametro(valorconsumoTotal);
		}
		
		return null;
	}
}
