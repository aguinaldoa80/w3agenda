package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.gnre.CampoExtra;
import br.com.linkcom.gnre.Destinatario;
import br.com.linkcom.gnre.Emitente;
import br.com.linkcom.gnre.GNRE;
import br.com.linkcom.gnre.Item;
import br.com.linkcom.gnre.LoteGNRE;
import br.com.linkcom.gnre.Referencia;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivognre;
import br.com.linkcom.sined.geral.bean.Arquivognreitem;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognrerateio;
import br.com.linkcom.sined.geral.bean.Configuracaognreuf;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Gnre;
import br.com.linkcom.sined.geral.bean.Gnrehistorico;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.GnreAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreCodigoReceitaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreDocumentoOrigemTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.GnreDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GnreService extends GenericService<Gnre> {

	private GnreDAO gnreDAO;
	private ConfiguracaognreService configuracaognreService;
	private ConfiguracaognreufService configuracaognreufService;
	private ArquivonfnotaService arquivonfnotaService;
	private ArquivognreService arquivognreService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private RateioService rateioService;
	private ContapagarService contapagarService;
	private GnrehistoricoService gnrehistoricoService;
	private NotaHistoricoService notaHistoricoService;
	private DocumentoorigemService documentoorigemService;
	
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setGnrehistoricoService(
			GnrehistoricoService gnrehistoricoService) {
		this.gnrehistoricoService = gnrehistoricoService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setConfiguracaognreufService(
			ConfiguracaognreufService configuracaognreufService) {
		this.configuracaognreufService = configuracaognreufService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setArquivognreService(ArquivognreService arquivognreService) {
		this.arquivognreService = arquivognreService;
	}
	public void setGnreDAO(GnreDAO gnreDAO) {
		this.gnreDAO = gnreDAO;
	}
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setConfiguracaognreService(
			ConfiguracaognreService configuracaognreService) {
		this.configuracaognreService = configuracaognreService;
	}
	
	public int gerarByNota(List<Notafiscalproduto> listaNota) {
		
		List<Gnre> listaGnre = new ArrayList<Gnre>();
		List<String> ufs = new ArrayList<String>();
		Map<Empresa, List<Configuracaognre>> mapConfiguracaoByEmpresa = new HashMap<Empresa, List<Configuracaognre>>();
		int sucesso = 0;
		for (Notafiscalproduto notafiscalproduto : listaNota) {
			Endereco enderecoCliente = notafiscalproduto.getEnderecoCliente();
			Municipio municipio = enderecoCliente.getMunicipio();
			Uf uf = municipio.getUf();
			if(!ufs.contains(uf.getSigla())) ufs.add(uf.getSigla());
			if(org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "ES") || org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "SP")){
				continue;
			}
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
			if (arquivonfnota == null){
				NeoWeb.getRequestContext().addError("Lote de NF-e n�o encontrado. " + new StringUtils().getLabelAndText("Nota: ", notafiscalproduto.getNumero()));
				continue;
			}
			
			List<Configuracaognre> listaConfiguracao = null;
			Empresa empresa = notafiscalproduto.getEmpresa();
			if(mapConfiguracaoByEmpresa.containsKey(empresa)){
				listaConfiguracao = mapConfiguracaoByEmpresa.get(empresa);
			} else {
				listaConfiguracao = configuracaognreService.findByEmpresa(empresa);
				for (Configuracaognre configuracaognre : listaConfiguracao) {
					configuracaognre.setListaConfiguracaognreuf(configuracaognreufService.findByConfiguracaognre(configuracaognre));
				}
			}
			
			if(listaConfiguracao != null && listaConfiguracao.size() > 0){
				for (Configuracaognre configuracaognre : listaConfiguracao) {
					if(this.hasGnreByNotaConfiguracao(notafiscalproduto, configuracaognre)){
						continue;
					}
					
					Configuracaognreuf configuracaognreuf = null;
					List<Configuracaognreuf> listaConfiguracaognreuf = configuracaognre.getListaConfiguracaognreuf();
					for (Configuracaognreuf configuracaognreufIt : listaConfiguracaognreuf) {
						if(configuracaognreufIt.getUf().getCduf().equals(uf.getCduf())){
							configuracaognreuf = configuracaognreufIt;
							break;
						}
					}
					if(configuracaognreuf == null) continue;
					
					Localdestinonfe localdestinonfe = notafiscalproduto.getLocaldestinonfe();
					Cliente cliente = notafiscalproduto.getCliente();
					Boolean contribuinteICMS = cliente.getContribuinteICMS();
					Operacaonfe operacaonfe = notafiscalproduto.getOperacaonfe();
					
					GnreCodigoReceitaEnum codigoreceita = configuracaognre.getCodigoreceita();
					
					Gnre gnre = new Gnre();
					gnre.setConfiguracaognre(configuracaognre);
					gnre.setSituacao(GnreSituacao.EM_ABERTA);
					gnre.setEmpresa(empresa);
					gnre.setCodigoreceita(codigoreceita);
					gnre.setCodigodetalhamento(configuracaognreuf.getCodigodetalhamento());
					gnre.setCliente(cliente);
					gnre.setMunicipioCliente(municipio);
					
					Timestamp datahorasaidaentrada = notafiscalproduto.getDatahorasaidaentrada() != null ? notafiscalproduto.getDatahorasaidaentrada() : SinedDateUtils.currentTimestamp();
					
					Date dtpagamento = null;
					if(configuracaognreuf.getConsiderardtpagamento() != null){
						Integer diasdtpagamento = configuracaognreuf.getDiasdtpagamento();
						if(diasdtpagamento == null) diasdtpagamento = 0;
						switch (configuracaognreuf.getConsiderardtvencimento()) {
							case DATA_ATUAL:
								dtpagamento = SinedDateUtils.addDiasUteisData(SinedDateUtils.currentDate(), configuracaognreuf.getDiasdtpagamento());
								break;
							case DATA_SAIDA:
								dtpagamento = SinedDateUtils.addDiasUteisData(new Date(datahorasaidaentrada.getTime()), configuracaognreuf.getDiasdtpagamento());
								break;
							case DATA_EMISSAO:
								dtpagamento = SinedDateUtils.addDiasUteisData(notafiscalproduto.getDtEmissao(), configuracaognreuf.getDiasdtpagamento());
								break;
	
							default:
								break;
						}
					}
					
					Date dtvencimento = null;
					if(configuracaognreuf.getConsiderardtvencimento() != null){
						Integer diasdtvencimento = configuracaognreuf.getDiasdtvencimento();
						if(diasdtvencimento == null) diasdtvencimento = 0;
						switch (configuracaognreuf.getConsiderardtvencimento()) {
						case DATA_ATUAL:
							dtvencimento = SinedDateUtils.addDiasUteisData(SinedDateUtils.currentDate(), configuracaognreuf.getDiasdtvencimento());
							break;
						case DATA_SAIDA:
							dtvencimento = SinedDateUtils.addDiasUteisData(new Date(datahorasaidaentrada.getTime()), configuracaognreuf.getDiasdtvencimento());
							break;
						case DATA_EMISSAO:
							dtvencimento = SinedDateUtils.addDiasUteisData(notafiscalproduto.getDtEmissao(), configuracaognreuf.getDiasdtvencimento());
							break;
							
						default:
							break;
						}
					}
							
					gnre.setDtpagamento(dtpagamento);
					gnre.setDtvencimento(dtvencimento);
					gnre.setUf(uf);
					gnre.setNota(notafiscalproduto);
					gnre.setChaveacesso(arquivonfnota.getChaveacesso());
					gnre.setDtemissaodocumento(notafiscalproduto.getDtEmissao());
					gnre.setNumerodocumento(notafiscalproduto.getNumero());
					gnre.setDtsaidadocumento(notafiscalproduto.getDatahorasaidaentrada() != null ? new Date(notafiscalproduto.getDatahorasaidaentrada().getTime()) : null);
					gnre.setInfocomplementardocumento(notafiscalproduto.getInfoadicionalcontrib());
					
					switch (codigoreceita) {
						case COD_100099:
							if(localdestinonfe.equals(Localdestinonfe.INTERESTADUAL) && 
								contribuinteICMS != null && 
								contribuinteICMS &&
								notafiscalproduto.getValoricmsst() != null &&
								notafiscalproduto.getValoricmsst().getValue().doubleValue() > 0){
								gnre.setValor(notafiscalproduto.getValoricmsst());
								if(org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "RJ")){
									gnre.setValorfcp(notafiscalproduto.getValorfcp());
								}
							} else continue;
							break;
							
						case COD_100102:
							if(operacaonfe.equals(Operacaonfe.CONSUMIDOR_FINAL) &&
									localdestinonfe.equals(Localdestinonfe.INTERESTADUAL) && 
									(contribuinteICMS == null || !contribuinteICMS) &&
									notafiscalproduto.getValoricmsdestinatario() != null &&
									notafiscalproduto.getValoricmsdestinatario().getValue().doubleValue() > 0){
								gnre.setValor(notafiscalproduto.getValoricmsdestinatario());
								if(org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "RJ")){
									gnre.setValorfcp(notafiscalproduto.getValorfcp());
								}
							} else continue;
							break;
							
						case COD_100080:
							if(operacaonfe.equals(Operacaonfe.CONSUMIDOR_FINAL) &&
									localdestinonfe.equals(Localdestinonfe.INTERESTADUAL) && 
									contribuinteICMS != null && 
									contribuinteICMS &&
									org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "MT") &&
									notafiscalproduto.getValorfcp() != null &&
									notafiscalproduto.getValorfcp().getValue().doubleValue() > 0){
								gnre.setValor(notafiscalproduto.getValorfcp());
							} else continue;
							break;
							
						case COD_100129:
							if(operacaonfe.equals(Operacaonfe.CONSUMIDOR_FINAL) &&
									localdestinonfe.equals(Localdestinonfe.INTERESTADUAL) && 
									!org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "RJ") && 
									!(contribuinteICMS != null &&  contribuinteICMS && org.apache.commons.lang.StringUtils.equals(uf.getSigla(), "MT")) && 
									notafiscalproduto.getValorfcp() != null && 
									notafiscalproduto.getValorfcp().getValue().doubleValue() > 0){
								gnre.setValor(notafiscalproduto.getValorfcp());
							} else continue; 
							break;
	
						default: 
							continue;
					}
					
					listaGnre.add(gnre);
				}
			}
		}
		
		for (Gnre gnre : listaGnre) {
			this.saveOrUpdate(gnre);
			
			Gnrehistorico gnrehistorico = new Gnrehistorico();
			gnrehistorico.setAcao(GnreAcaoEnum.CRIADA);
			gnrehistorico.setGnre(gnre);
			gnrehistoricoService.saveOrUpdate(gnrehistorico);
			
			if(gnre.getNota() != null && gnre.getNota().getCdNota() != null){
				NotaHistorico notaHistorico = new NotaHistorico();
				notaHistorico.setNota(gnre.getNota());
				notaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
				notaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				notaHistorico.setObservacao(new StringBuilder().append("Registro de GNRE criado: <a href=\"/w3erp/fiscal/crud/Gnre?ACAO=consultar&cdgnre=").append(gnre.getCdgnre()).append("\">").append(gnre.getCdgnre()).append("</a>").toString());
				notaHistorico.setNotaStatus(NotaStatus.GNRE_GERADA);
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
			
			sucesso++;
		}
		
		return sucesso;
	}
	
	public boolean hasGnreByNotaConfiguracao(Notafiscalproduto notafiscalproduto, Configuracaognre configuracaognre) {
		return gnreDAO.hasGnreByNotaConfiguracao(notafiscalproduto, configuracaognre);
	}
	
	public List<Gnre> findForXML(String whereIn) {
		return gnreDAO.findForXML(whereIn);
	}
	
	public void gerarXML(List<Gnre> listaGnre) {
		if(listaGnre != null && listaGnre.size() > 0){
			List<LoteGNRE> listaLoteGNRE = new ArrayList<LoteGNRE>();
			LoteGNRE loteGNRE = new LoteGNRE();
			for (int i = 0; i < listaGnre.size(); i++) {
				if(i != 0 && i % 20 == 0){
					listaLoteGNRE.add(loteGNRE);
					loteGNRE = new LoteGNRE();
				}
				
				Gnre gnre = listaGnre.get(i);
				Cliente cliente = gnre.getCliente();
				Uf uf = gnre.getUf();
				Configuracaognre configuracaognre = gnre.getConfiguracaognre();
				Empresa empresa = gnre.getEmpresa();
				Endereco endereco = empresa.getEndereco();
				
				Configuracaognreuf configuracaognreuf = null;
				List<Configuracaognreuf> listaConfiguracaognreuf = configuracaognre.getListaConfiguracaognreuf();
				for (Configuracaognreuf configuracaognreufIt : listaConfiguracaognreuf) {
					if(configuracaognreufIt.getUf().getCduf().equals(uf.getCduf())){
						configuracaognreuf = configuracaognreufIt;
						break;
					}
				}
				if(configuracaognreuf == null) continue;
				
				Emitente emitente = new Emitente();
				emitente.setCnpj(empresa.getCnpj().getValue());
				if(configuracaognreuf.getIeremetente() != null && 
						configuracaognreuf.getIeremetente() && 
						endereco.getMunicipio().getUf().getCduf().equals(uf.getCduf())){
					emitente.setInscricaoestadual(empresa.getInscricaoestadual());
				}
				emitente.setRazaosocial(empresa.getRazaosocialOuNome());
				emitente.setEndereco(endereco.getLogradouroCompletoComBairroSemMunicipio());
				emitente.setMunicipio(endereco.getMunicipio().getCdibge().substring(2));
				emitente.setCep(endereco.getCep().getValue());
				emitente.setUf(endereco.getMunicipio().getUf().getSigla());
				emitente.setTelefone(StringUtils.soNumero(empresa.getTelefone().getTelefone()));
				
				Destinatario destinatario = null;
				if(configuracaognreuf.getDestinatario() != null && configuracaognreuf.getDestinatario()){
					if(cliente != null){
						destinatario = new Destinatario();
						destinatario.setCnpj(cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null);
						destinatario.setCpf(cliente.getCpf() != null ? cliente.getCpf().getValue() : null);
						if(configuracaognreuf.getIedestinatario() != null && 
								configuracaognreuf.getIedestinatario()){
							destinatario.setInscricaoestadual(cliente.getInscricaoestadual());
						}
						destinatario.setMunicipio(gnre.getMunicipioCliente() != null ? gnre.getMunicipioCliente().getCdibge().substring(2) : null);
						destinatario.setRazaoSocial(cliente.getRazaoOrNomeByTipopessoa());
					}
				}
				
				Referencia referencia = null;
				if(configuracaognreuf.getReferencia() != null && configuracaognreuf.getReferencia()){
					referencia = new Referencia();
					referencia.setPeriodo(configuracaognreuf != null && configuracaognreuf.getCodigoperiodoreferencia() != null ? configuracaognreuf.getCodigoperiodoreferencia().getValue() : null);
					referencia.setMes(SinedDateUtils.toString(gnre.getDtemissaodocumento(), "MM"));
					referencia.setAno(SinedDateUtils.toString(gnre.getDtemissaodocumento(), "yyyy"));
					referencia.setParcela("1");
				}
				
				List<CampoExtra> listaCampoExtra = new ArrayList<CampoExtra>();
				if(configuracaognreuf.getChaveacesso() != null && configuracaognreuf.getChaveacesso()){
					CampoExtra campoExtra = new CampoExtra();
					campoExtra.setCodigo(configuracaognreuf.getCodigochaveacesso());
					campoExtra.setTipo("T");
					campoExtra.setValor(gnre.getChaveacesso());
					listaCampoExtra.add(campoExtra);
				}
				if(configuracaognreuf.getDtsaida() != null && configuracaognreuf.getDtsaida()){
					CampoExtra campoExtra = new CampoExtra();
					campoExtra.setCodigo(configuracaognreuf.getCodigodtsaida());
					campoExtra.setTipo("D");
					campoExtra.setValor(SinedDateUtils.toString(gnre.getDtsaidadocumento(), "yyyy-MM-dd"));
					listaCampoExtra.add(campoExtra);
				}
				if(configuracaognreuf.getDtemissao() != null && configuracaognreuf.getDtemissao()){
					CampoExtra campoExtra = new CampoExtra();
					campoExtra.setCodigo(configuracaognreuf.getCodigodtemissao());
					campoExtra.setTipo("D");
					campoExtra.setValor(SinedDateUtils.toString(gnre.getDtemissaodocumento(), "yyyy-MM-dd"));
					listaCampoExtra.add(campoExtra);
				}
				String infocomplementardocumento = gnre.getInfocomplementardocumento();
				String infocomplementar = null;
				String infocomplementar2 = null;
				if(configuracaognreuf.getMaximoinfocomplementar() != null && configuracaognreuf.getMaximoinfocomplementar() > 0){
					if(infocomplementardocumento.length() > configuracaognreuf.getMaximoinfocomplementar()){
						infocomplementar = infocomplementardocumento.substring(0, configuracaognreuf.getMaximoinfocomplementar());
						if(configuracaognreuf.getMaximoinfocomplementar2() != null && configuracaognreuf.getMaximoinfocomplementar2() > 0){
							if(infocomplementardocumento.length() > (configuracaognreuf.getMaximoinfocomplementar() + configuracaognreuf.getMaximoinfocomplementar2())){
								infocomplementar2 = infocomplementardocumento.substring(configuracaognreuf.getMaximoinfocomplementar(), configuracaognreuf.getMaximoinfocomplementar() + configuracaognreuf.getMaximoinfocomplementar2());
							} else {
								infocomplementar2 = infocomplementardocumento.substring(configuracaognreuf.getMaximoinfocomplementar(), infocomplementardocumento.length());
							}
						}
					} else {
						infocomplementar = infocomplementardocumento;
					}
				}
				
				if(configuracaognreuf.getInfocomplementar() != null && configuracaognreuf.getInfocomplementar() && infocomplementar != null){
					CampoExtra campoExtra = new CampoExtra();
					campoExtra.setCodigo(configuracaognreuf.getCodigoinfocomplementar());
					campoExtra.setTipo("T");
					campoExtra.setValor(infocomplementar);
					listaCampoExtra.add(campoExtra);
				}
				if(configuracaognreuf.getInfocomplementar2() != null && configuracaognreuf.getInfocomplementar2() && infocomplementar2 != null){
					CampoExtra campoExtra = new CampoExtra();
					campoExtra.setCodigo(configuracaognreuf.getCodigoinfocomplementar2());
					campoExtra.setTipo("T");
					campoExtra.setValor(infocomplementar2);
					listaCampoExtra.add(campoExtra);
				}
				
				List<Item> listaItem = new ArrayList<Item>();
				Item item = new Item();
				item.setDataVencimento(gnre.getDtvencimento());
				item.setDestinatario(destinatario);
				item.setReceita(gnre.getCodigoreceita().getCodigo());
				item.setDetalhamentoReceita(gnre.getCodigodetalhamento());
				item.setListaCampoExtra(listaCampoExtra);
				item.setReferencia(referencia);
				item.setValor(gnre.getValor().doubleValue());
				item.setValorFCP(gnre.getValorfcp() != null ? gnre.getValorfcp().doubleValue() : null);
				
				if(configuracaognreuf.getDocumentoorigem() != null && configuracaognreuf.getDocumentoorigem()){
					if(configuracaognreuf.getDocumentoorigemtipo() != null && 
							configuracaognreuf.getDocumentoorigemtipo().equals(GnreDocumentoOrigemTipoEnum.NUMERO) &&
							org.apache.commons.lang.StringUtils.isNotBlank(gnre.getNumerodocumento())){
						item.setTipoDocumentoOrigem(configuracaognreuf.getCodigodocumentoorigem());
						item.setDocumentoOrigem(gnre.getNumerodocumento());
					} else if(configuracaognreuf.getDocumentoorigemtipo() != null && 
							configuracaognreuf.getDocumentoorigemtipo().equals(GnreDocumentoOrigemTipoEnum.CHAVE_ACESSO) &&
							org.apache.commons.lang.StringUtils.isNotBlank(gnre.getChaveacesso())){
						item.setTipoDocumentoOrigem(configuracaognreuf.getCodigodocumentoorigem());
						item.setDocumentoOrigem(gnre.getChaveacesso());
					}
				}
				
				listaItem.add(item);
				
				GNRE gnreXmlBean = new GNRE();
				gnreXmlBean.setVersao(configuracaognreuf.getVersao().getNome());
				gnreXmlBean.setUfFavorecida(uf.getSigla());
				gnreXmlBean.setTipoGnre("0");
				gnreXmlBean.setEmitente(emitente);
				gnreXmlBean.setListaItem(listaItem);
				gnreXmlBean.setDataPagamento(gnre.getDtpagamento());
				
				loteGNRE.addGNRE(gnreXmlBean, gnre.getCdgnre());
			}
			if(loteGNRE != null && loteGNRE.getListaGNRE() != null && loteGNRE.getListaGNRE().size() > 0){
				listaLoteGNRE.add(loteGNRE);
			}
			
			for (LoteGNRE loteXml : listaLoteGNRE) {
				List<Arquivognreitem> listaArquivognreitem = new ArrayList<Arquivognreitem>();
				List<Integer> listaIds = loteXml.getListaIds();
				for (Integer id : listaIds) {
					Gnre gnre = new Gnre(id);
					
					Arquivognreitem arquivognreitem = new Arquivognreitem();
					arquivognreitem.setGnre(gnre);
					listaArquivognreitem.add(arquivognreitem);
					
					this.updateSituacao(gnre, GnreSituacao.GERADA);
					
					Gnrehistorico gnrehistorico = new Gnrehistorico();
					gnrehistorico.setAcao(GnreAcaoEnum.XML_GERADO);
					gnrehistorico.setGnre(gnre);
					gnrehistorico.setObservacao("Lote de GNRE <a href= '/"+ NeoWeb.getApplicationName() + "/fiscal/crud/Gnre?ACAO=consultar&cdgnre=" + gnre.getCdgnre() + "'> " + gnre.getCdgnre() + " </a>");
					gnrehistoricoService.saveOrUpdate(gnrehistorico);
				}
				
				Arquivo arquivoxml = new Arquivo(loteXml.toString().getBytes(), "gnre_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				
				Arquivognre arquivognre = new Arquivognre();
				arquivognre.setArquivoxml(arquivoxml);
				arquivoDAO.saveFile(arquivognre, "arquivoxml");
				arquivoService.saveOrUpdate(arquivoxml);
				
				arquivognre.setListaArquivognreitem(listaArquivognreitem);
				arquivognreService.saveOrUpdate(arquivognre);
				
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), arquivoxml.getCdarquivo());
				NeoWeb.getRequestContext().addMessage("Lote " + arquivognre.getCdarquivognre() + " gerado para fazer o download <a href='" + SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + arquivoxml.getCdarquivo() + "'>clique aqui</a>.");
			}
			
		} else throw new SinedException("Nenhuma GNRE encontrada.");
	}
	
	public void updateSituacao(Gnre gnre, GnreSituacao situacao) {
		gnreDAO.updateSituacao(gnre, situacao);
	}
	
	
	public void gerarContapagar(List<Gnre> listaGnre) {
		if(listaGnre != null && listaGnre.size() > 0){
			List<Documento> listaDocumento = new ArrayList<Documento>();
			for (Gnre gnre : listaGnre) {
				Configuracaognre configuracaognre = gnre.getConfiguracaognre();
				
				Documento documento = new Documento();
				documento.setEmpresa(gnre.getEmpresa());
				documento.setNumero(null);
				documento.setDocumentotipo(configuracaognre.getDocumentotipo());
				documento.setDtcompetencia(SinedDateUtils.currentDate());
				documento.setDescricao("Referente a GNRE " + gnre.getCdgnre());
				documento.setDtvencimento(gnre.getDtvencimento());
				documento.setDtemissao(SinedDateUtils.currentDate());
				documento.setValor(gnre.getValor().add(gnre.getValorfcp()));
				documento.setReferencia(br.com.linkcom.sined.geral.bean.Referencia.MES_ANO_CORRENTE);
				documento.setTipopagamento(Tipopagamento.FORNECEDOR);
				documento.setPrazo(null);
				documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
				documento.setDocumentoacao(Documentoacao.PREVISTA);
				documento.setFornecedor(configuracaognre.getFornecedor());
				documento.setPessoa(configuracaognre.getFornecedor());
				documento.setWhereInGnre(gnre.getCdgnre() + "");
				
				Rateio rateio = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				List<Configuracaognrerateio> listaConfiguracaognrerateio = configuracaognre.getListaConfiguracaognrerateio();
				for (Configuracaognrerateio configuracaognrerateio : listaConfiguracaognrerateio) {
					Rateioitem rateioitem = new Rateioitem();
					rateioitem.setContagerencial(configuracaognrerateio.getContagerencial());
					rateioitem.setCentrocusto(configuracaognrerateio.getCentrocusto());
					rateioitem.setProjeto(configuracaognrerateio.getProjeto());
					rateioitem.setPercentual(configuracaognrerateio.getPercentual());
					listaRateioitem.add(rateioitem);
				}
				rateio.setListaRateioitem(listaRateioitem);
				rateioService.atualizaValorRateio(rateio, documento.getValor());
				documento.setRateio(rateio);
				listaDocumento.add(documento);
			}
			
			for (Documento documento : listaDocumento) {
				String whereInGnre = documento.getWhereInGnre();
				contapagarService.saveOrUpdate(documento);
				
				String[] ids = whereInGnre.split(",");
				for (String id : ids) {
					Gnre gnre = new Gnre(Integer.parseInt(id));
					Gnrehistorico gnrehistorico = new Gnrehistorico();
					gnrehistorico.setAcao(GnreAcaoEnum.CONTA_PAGAR_GERADA);
					gnrehistorico.setGnre(gnre);
					gnrehistorico.setObservacao("Conta a pagar <a href= '/"+ NeoWeb.getApplicationName() + "/financeiro/crud/Contapagar?ACAO=consultar&cddocumento=" + documento.getCddocumento() + "'> " + documento.getCddocumento() + " </a>");
					gnrehistoricoService.saveOrUpdate(gnrehistorico);
					
					this.updateSituacao(gnre, GnreSituacao.FINALIZADA);
				}
				
			}
			
			NeoWeb.getRequestContext().addMessage("Conta(s) a pagar gerada(s) com sucesso.");
		} else throw new SinedException("Nenhuma GNRE encontrada.");
	}
	
	public List<Gnre> findForGerarContaAPagar(String whereIn) {
		return gnreDAO.findForGerarContaAPagar(whereIn);
	}
	
	public List<Gnre> findForCancelar(String whereIn) {
		return gnreDAO.findForCancelar(whereIn);
	}
	
	public void cancelar(List<Gnre> listaGnre) {
		if(listaGnre != null && listaGnre.size() > 0){
			for (Gnre gnre : listaGnre) {
				Gnrehistorico gnrehistorico = new Gnrehistorico();
				gnrehistorico.setAcao(GnreAcaoEnum.CANCELADA);
				gnrehistorico.setGnre(gnre);
				gnrehistoricoService.saveOrUpdate(gnrehistorico);
				
				this.updateSituacao(gnre, GnreSituacao.CANCELADA);
			}
		}
	}
	
	public void estornar(List<Gnre> listaGnre) {
		if(listaGnre != null && listaGnre.size() > 0){
			for (Gnre gnre : listaGnre) {
				Gnrehistorico gnrehistorico = new Gnrehistorico();
				gnrehistorico.setAcao(GnreAcaoEnum.ESTORNADA);
				gnrehistorico.setGnre(gnre);
				gnrehistoricoService.saveOrUpdate(gnrehistorico);
				
				this.updateSituacao(gnre, GnreSituacao.EM_ABERTA);
			}
		}
	}
	
	public List<Gnre> findByNota(String whereInNota) {
		return gnreDAO.findByNota(whereInNota);
	}
	
	public void verificaGnreAposCancelamentoDocumento(String whereInDocumento) {
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInDocumento)){
			List<Documentoorigem> listaDocumentoorigem = documentoorigemService.findDocumentoWithGnre(whereInDocumento);
			if(SinedUtil.isListNotEmpty(listaDocumentoorigem)){
				for(Documentoorigem docorigem : listaDocumentoorigem){
					Gnre gnre = docorigem.getGnre();
					if(gnre != null){
						Gnrehistorico gnrehistorico = new Gnrehistorico();
						gnrehistorico.setAcao(GnreAcaoEnum.CONTA_PAGAR_CANCELADA);
						gnrehistorico.setGnre(gnre);
						gnrehistoricoService.saveOrUpdate(gnrehistorico);
						
						this.updateSituacao(gnre, GnreSituacao.GERADA);
					}
				}
			}
		}
	}

}
