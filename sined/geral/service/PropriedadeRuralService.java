package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.geral.bean.ParticipantePropriedadeRural;
import br.com.linkcom.sined.geral.bean.PropriedadeRural;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTerceiros;
import br.com.linkcom.sined.geral.dao.PropriedadeRuralDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

public class PropriedadeRuralService extends GenericService<PropriedadeRural> {
	private PropriedadeRuralDAO propriedadeRuralDAO;
	private EmpresaproprietarioService empresaproprietarioService;
	
	public void setPropriedadeRuralDAO(PropriedadeRuralDAO propriedadeRuralDAO) {this.propriedadeRuralDAO = propriedadeRuralDAO;}
	public void setEmpresaproprietarioService(EmpresaproprietarioService empresaproprietarioService) {this.empresaproprietarioService = empresaproprietarioService;}
	
	public void validateListaParticipantes(BindException errors, Collection<ParticipantePropriedadeRural> listaParticipantes, TipoTerceiros tipoTerceiros, Empresa empresa){
		Empresaproprietario empresaproprietario = empresaproprietarioService.findColaboradorPrincipal(empresa.getCdpessoa());
		Boolean proprietarioPresente = false;
		
		if(SinedUtil.isListEmpty(listaParticipantes)) {
			return;
		}
		
		Double somaPercentualParticipantes = 0D;
		for(ParticipantePropriedadeRural participante : listaParticipantes) {
			somaPercentualParticipantes += participante.getPercentual();
			if(participante.getColaborador().getCdpessoa().equals(empresaproprietario.getColaborador().getCdpessoa())){
				proprietarioPresente = true;
			}
		}
		if(!proprietarioPresente){
			errors.reject("001", "O propriet�rio " + empresaproprietario.getColaborador().getNome() + ", propriet�rio da fazenda " + 
									empresaproprietario.getEmpresa().getNomefantasia() + ", deve estar obrigat�riamente entre os participantes.");
		}
		
		if(tipoTerceiros != null 
				&& tipoTerceiros.equals(TipoTerceiros.NAO_SE_APLICA) 
				&& (listaParticipantes.size() > 1 || (listaParticipantes.size() == 1 && somaPercentualParticipantes != 100D))) {
			errors.reject("001", "Caso o Tipo de Terceiros for \"N�o se aplica\", ser� poss�vel cadastrar apenas um participante, com o percentual de participa��o em 100% obrigatoriamente.");
		} else if(somaPercentualParticipantes != 100D) {
			errors.reject("001", "A soma dos percentuais de participa��o n�o pode ser diferente de 100%");
		}
	}
	
	public Integer getCodigoImovelUltimaPropriedadeRural(Integer cdpessoa) {
		Integer codigoImovel = propriedadeRuralDAO.getCodigoImovelUltimaPropriedadeRural(cdpessoa);
		return codigoImovel != null ? codigoImovel : 0;
	}
	
	public Boolean existeCodigoImovelCadastrado(Integer codigoImovel, Integer cdPropriedadeRural, Integer cdpessoa) {
		return propriedadeRuralDAO.existeCodigoImovelCadastrado(codigoImovel, cdPropriedadeRural, cdpessoa);
	}

	public List<PropriedadeRural> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return propriedadeRuralDAO.loadForLcdpr(filtro);
	}

	public Boolean havePrincipal(Integer cdpessoa) {
		return propriedadeRuralDAO.havePrincipal(cdpessoa);
	}

	public PropriedadeRural loadPrincipal(Integer cdpessoa) {
		return propriedadeRuralDAO.loadPrincipal(cdpessoa);
	}

	public void updatePrincipal(Integer cdPropriedadeRural) {
		propriedadeRuralDAO.updatePrincipal(cdPropriedadeRural);
	}
	
}
