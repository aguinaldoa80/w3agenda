package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.dao.NotaVendaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaVendaService extends GenericService<NotaVenda>{

	private NotaVendaDAO notaVendaDAO;
	private VendahistoricoService vendahistoricoService;
	
	public void setNotaVendaDAO(NotaVendaDAO notaVendaDAO) {
		this.notaVendaDAO = notaVendaDAO;
	}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 05/01/2012
	 * @param nota
	 * @return
	 */
	public List<NotaVenda> findByNota(String whereIn){
		return notaVendaDAO.findByNota(whereIn); 
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaVendaDAO#findByNota(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaVenda> findByNota(Nota nota){
		return notaVendaDAO.findByNota(nota); 
	}
	
	public Boolean existNotaVenda(Nota nota) {
		return notaVendaDAO.existNotaVenda(nota);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaVendaDAO#existNotaEmitida(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existNotaEmitida(Venda venda, NotaTipo notaTipo) {
		return notaVendaDAO.existNotaEmitida(venda, notaTipo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.NotaVendaService#existeNotaEmitida(Expedicao expedicao, NotaTipo notaTipo)
	*
	* @param expedicao
	* @param notaTipo
	* @return
	* @since 18/01/2017
	* @author Luiz Fernando
	*/
	public Boolean existeNotaEmitida(Expedicao expedicao, NotaTipo notaTipo) {
		return notaVendaDAO.existeNotaEmitida(expedicao, notaTipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaVendaDAO#existNotaEmitida(Documento documento, NotaTipo notaTipo)
	 *
	 * @param documento
	 * @param notaTipo
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existNotaEmitida(Documento documento, NotaTipo notaTipo) {
		return notaVendaDAO.existNotaEmitida(documento, notaTipo);
	}
	
	/**
	 * 
	 * @param venda
	 * @return
	 */
	public List<NotaVenda> findByVenda(Venda venda){
		return this.findByVenda(venda, true);
	}
	
	public List<NotaVenda> findByVenda(Venda venda, boolean includeDenegada){
		return notaVendaDAO.findByVenda(venda, includeDenegada);
	}
	
	/**
	 * M�todo que atualiza o hist�rico da venda com o novo n�mero da nota
	 * @param bean
	 * @param numeroAnteriorNota
	 * @param servico
	 */
	public void atualizarNumeroHistoricoVenda(Nota bean, String numeroAnteriorNota, Boolean servico, Boolean nfe) {
		if(bean != null && bean.getCdNota() != null){
			List<NotaVenda> listaNotaVenda = findByNota(bean);
			if(listaNotaVenda != null && !listaNotaVenda.isEmpty()){
				for(NotaVenda notaVenda : listaNotaVenda){
					if(notaVenda.getVenda() != null && notaVenda.getVenda().getCdvenda() != null){
						Vendahistorico vendahistorico = new Vendahistorico();
						vendahistorico.setVenda(notaVenda.getVenda());
						vendahistorico.setAcao("Altera��o na Nota");
						vendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
						vendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						vendahistorico.setObservacao("N�mero da nota atualizado. Visualizar nota: <a href=\"javascript:visualiza" + (nfe ? "Nota" : "NFCe") + (servico ? "Servico" : "") + "("+bean.getCdNota()+");\">"+(bean.getNumero() != null ? bean.getNumero() : bean.getCdNota() )+"</a>.");
						vendahistoricoService.saveOrUpdate(vendahistorico);
					}
				}
			}
		}
		
	}
	
	public boolean isNotaIndustrializacaoRetorno(Nota nota){
		return notaVendaDAO.isNotaIndustrializacaoRetorno(nota);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaVendaDAO#existeNotaEmitida(Venda venda)
	*
	* @param venda
	* @return
	* @since 07/10/2015
	* @author Luiz Fernando
	*/
	public Boolean existeNotaEmitida(Venda venda, boolean desmarkAsReader) {
		return notaVendaDAO.existeNotaEmitida(venda, desmarkAsReader);
	}
	
	public Boolean existeNotaEmitida(Venda venda) {
		return notaVendaDAO.existeNotaEmitida(venda, false);
	}
	
	public Boolean existeNotaEmitida(Venda venda, String whereInExpedicao) {
		return notaVendaDAO.existeNotaEmitida(venda, whereInExpedicao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaVendaDAO#existeNFsENFe(Venda venda)
	*
	* @param venda
	* @return
	* @since 03/12/2015
	* @author Luiz Fernando
	*/
	public Boolean existeNFsENFe(Venda venda) {
		return notaVendaDAO.existeNFsENFe(venda);
	}
	
	public String getWhereInVenda(Notafiscalproduto nota) {
		if(nota.getCdvendaassociada() != null){
			return nota.getCdvendaassociada().toString();
		}
		return notaVendaDAO.getWhereInVenda(nota);
	}
	
	public String getWhereInVenda(NotaFiscalServico nota) {
		if(nota.getCdvendaassociada() != null){
			return nota.getCdvendaassociada().toString();
		}
		return notaVendaDAO.getWhereInVenda(nota);
	}
}
