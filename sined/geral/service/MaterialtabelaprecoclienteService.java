package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.dao.MaterialtabelaprecoclienteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecocliente.MaterialtabelaprecoclienteRESTModel;

public class MaterialtabelaprecoclienteService extends GenericService<Materialtabelaprecocliente> {

	private MaterialtabelaprecoclienteDAO materialtabelaprecoclienteDAO;
	
	public void setMaterialtabelaprecoclienteDAO(MaterialtabelaprecoclienteDAO materialtabelaprecoclienteDAO) {
		this.materialtabelaprecoclienteDAO = materialtabelaprecoclienteDAO;
	}

	/* singleton */
	private static MaterialtabelaprecoclienteService instance;
	public static MaterialtabelaprecoclienteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialtabelaprecoclienteService.class);
		}
		return instance;
	}
	
	public List<MaterialtabelaprecoclienteRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialtabelaprecoclienteRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialtabelaprecoclienteRESTModel> lista = new ArrayList<MaterialtabelaprecoclienteRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialtabelaprecocliente bean : materialtabelaprecoclienteDAO.findForAndroid(whereIn))
				lista.add(new MaterialtabelaprecoclienteRESTModel(bean));
		}
		
		return lista;
	}
	
	public List<Materialtabelaprecocliente> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco){
		return materialtabelaprecoclienteDAO.findByMaterialtabelapreco(materialtabelapreco);
	}
}
