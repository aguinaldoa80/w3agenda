package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.dao.MaterialFaixaMarkupDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MaterialFaixaMarkupWSBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialFaixaMarkupService extends GenericService<MaterialFaixaMarkup>{

	private MaterialFaixaMarkupDAO materialFaixaMarkupDAO;
	private MaterialService materialService;
	
	public void setMaterialFaixaMarkupDAO(
			MaterialFaixaMarkupDAO materialFaixaMarkupDAO) {
		this.materialFaixaMarkupDAO = materialFaixaMarkupDAO;
	}
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public List<MaterialFaixaMarkup> findByMaterial(Material material){
		List<MaterialFaixaMarkup> lista = materialFaixaMarkupDAO.findByMaterial(material);
		this.preencheValorCusto(lista);
		return lista;
	}
	
	public List<MaterialFaixaMarkup> findForVenda(Material material, Unidademedida unidademedida, Empresa empresa){
		return materialFaixaMarkupDAO.findForVenda(material, unidademedida, empresa);
	}
	
	public ModelAndView findJsonFaixaMarkup(Material material, Unidademedida unidademedida, Empresa empresa){
		ModelAndView json = new JsonModelAndView();
		List<MaterialFaixaMarkup> lista = this.findForVenda(material, unidademedida, empresa);
		this.preencheValorCusto(lista);
		List<String> listaStr = new ArrayList<String>();
		if(SinedUtil.isListNotEmpty(lista)){
			for(MaterialFaixaMarkup materialFaixaMarkup: lista){
				listaStr.add("<b>"+materialFaixaMarkup.getFaixaMarkupNome().getNome()+"</b> - A partir de R$ "+materialFaixaMarkup.getValorVenda().toString().replace(".", ","));
			}
		}
		return json.addObject("lista", listaStr);
	}
	
	public List<MaterialFaixaMarkupWSBean> findFaixaMarkupWSVenda(Material material, Unidademedida unidademedida, Empresa empresa){
		List<MaterialFaixaMarkup> lista = this.findForVenda(material, unidademedida, empresa);
		
		List<MaterialFaixaMarkupWSBean> listaRetorno = new ArrayList<MaterialFaixaMarkupWSBean>();
		if(SinedUtil.isListNotEmpty(lista)){
			this.preencheValorCusto(lista);
			for(MaterialFaixaMarkup materialFaixaMarkup: lista){
				MaterialFaixaMarkupWSBean bean = new MaterialFaixaMarkupWSBean();
				bean.setId(Util.strings.toStringIdStyled(materialFaixaMarkup));
				if(Util.objects.isPersistent(materialFaixaMarkup.getFaixaMarkupNome())){
					bean.setCor(materialFaixaMarkup.getFaixaMarkupNome().getCor());
					bean.setValue(materialFaixaMarkup.getFaixaMarkupNome().getNome());
				}
				bean.setValorVenda(materialFaixaMarkup.getValorVenda());
				
				listaRetorno.add(bean);
			}
		}
		return listaRetorno;
	}
	
	public List<MaterialFaixaMarkup> findOrdenado(Material material, Unidademedida unidademedida, Empresa empresa){
		return materialFaixaMarkupDAO.findOrdenado(material, unidademedida, empresa, true);
	}
	
	public List<MaterialFaixaMarkup> findOrdenado(Material material, Unidademedida unidademedida, Empresa empresa, Boolean desc){
		return materialFaixaMarkupDAO.findOrdenado(material, unidademedida, empresa, desc);
	}
	
	public List<MaterialFaixaMarkup> findOrderByValorVenda(Material material, Unidademedida unidademedida, Empresa empresa, Boolean desc){
		return materialFaixaMarkupDAO.findOrderByValorVenda(material, unidademedida, empresa, desc);
	}
	
	public void preencheValorCusto(List<MaterialFaixaMarkup> lista){
		if(SinedUtil.isListNotEmpty(lista)){
			for(MaterialFaixaMarkup faixa: lista){
				this.preencheValorCusto(faixa);
			}
		}
	}
	
	public void preencheValorCusto(MaterialFaixaMarkup bean){
		Double valorCusto = materialService.getValorCustoAtual(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa());
		if(valorCusto == null){
			valorCusto = 0D;
		}
		bean.setValorCusto(valorCusto);
	}
	
	public MaterialFaixaMarkup loadFaixaAtual(Material material, Unidademedida unidademedida, Empresa empresa, Double markup){
		if(markup != null){
			List<MaterialFaixaMarkup> lista = this.findOrdenado(material, unidademedida, empresa);
			//Double markupFormatado = new Double(new Money(markup).toString().replace(".", "").replace(",", "."));
			Double markupFormatado = markup;
			
			this.preencheValorCusto(lista);
			if(SinedUtil.isListNotEmpty(lista)){
				for(MaterialFaixaMarkup faixa: lista){
					Double markup2 = SinedUtil.round(BigDecimal.valueOf(faixa.getMarkup()), 10).doubleValue();
					if(faixa.getMarkup() != null && markupFormatado >= markup2) {
						return faixa;
					}
				}
			}
		}
		return null;
	}
	
	public MaterialFaixaMarkup loadFaixaAtualByValorVenda(Material material, Unidademedida unidademedida, Empresa empresa, Double valorVenda){
		if(valorVenda != null){
			List<MaterialFaixaMarkup> lista = this.findOrderByValorVenda(material, unidademedida, empresa, true);
			this.preencheValorCusto(lista);
			if(SinedUtil.isListNotEmpty(lista)){
				for(MaterialFaixaMarkup faixa: lista){
					Double valorVendaLimite = SinedUtil.round(faixa.getValorVenda(), 10).doubleValue();
					if(valorVenda >= valorVendaLimite) {
						return faixa;
					}
				}
			}
		}
		return null;
	}
	
	public MaterialFaixaMarkup getMarkupValorMinimo(Material material, Unidademedida unidadeMedida, Empresa empresa){
		return materialFaixaMarkupDAO.getMarkupValorMinimo(material, unidadeMedida, empresa);
	}
	
	public MaterialFaixaMarkup getMarkupValorIdeal(Material material, Unidademedida unidadeMedida, Empresa empresa){
		return materialFaixaMarkupDAO.getMarkupValorIdeal(material, unidadeMedida, empresa);
	}
	
	public List<MaterialFaixaMarkup> findMarkupValorMinimo(String whereInMaterial, Empresa empresa){
		return materialFaixaMarkupDAO.findMarkupValorMinimo(whereInMaterial, empresa);
	}
	
	public List<MaterialFaixaMarkup> findMarkupValorIdeal(String whereInMaterial, Empresa empresa){
		return materialFaixaMarkupDAO.findMarkupValorIdeal(whereInMaterial, empresa);
	}
	
	public Double calcularPercentualMarkupPorValorVenda(Double valorCusto, Double valorVenda) {
		if(valorCusto != null && valorVenda != null && valorCusto > 0) {
			Double custo = valorCusto; 
			Double venda = valorVenda;
			
			return (venda - custo) / custo * 100d;
		}
		return null;
	}
	
	public Double calcularValorVendaPorMarkup(Double valorCusto, Double percentualMarkup) {
		if(valorCusto != null && percentualMarkup != null) {
			Double custo = valorCusto;
			Double valorVenda = SinedUtil.round(custo * (percentualMarkup / 100 + 1), 2);
			
			return valorVenda;
		}
		return null;
	}
	
	public void updatePercentualMarkup(MaterialFaixaMarkup faixa, Double markup) {
		materialFaixaMarkupDAO.updatePercentualMarkup(faixa, markup);
	}
	
	public void updateValorVenda(MaterialFaixaMarkup faixa, Double valorVenda) {
		materialFaixaMarkupDAO.updateValorVenda(faixa, valorVenda);
	}
	

	/* singleton */
	private static MaterialFaixaMarkupService instance;
	public static MaterialFaixaMarkupService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialFaixaMarkupService.class);
		}
		return instance;
	}
}
