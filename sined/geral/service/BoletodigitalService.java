package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Boletodigital;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.W3erpBankStatusIntegracao;
import br.com.linkcom.sined.geral.dao.BoletodigitalDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRetornoAcaoEnum;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRetornoDocumentoBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BoletodigitalService extends GenericService<Boletodigital> {
	
	private BoletodigitalDAO boletodigitalDAO;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ContareceberService contareceberService;
	private DocumentoService documentoService;
	private ContratoService contratoService;
	private MovimentacaoService movimentacaoService;
	private RateioService rateioService;
	private ContapagarService contapagarService;
	private NotaDocumentoService notaDocumentoService;
	
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setBoletodigitalDAO(BoletodigitalDAO boletodigitalDAO) {
		this.boletodigitalDAO = boletodigitalDAO;
	}
	
	public void gerarRemessa(BoletoDigitalRemessaBean filtro) {
		String[] ids = filtro.getCds().split(",");
		for (String id : ids) {
			Documento documento = new Documento(Integer.parseInt(id));
			Boletodigital boletodigital = this.loadByDocumento(documento);
			if(boletodigital == null){
				boletodigital = new Boletodigital();
			}
			boletodigital.setDocumento(documento);
			boletodigital.setStatusintegracao(W3erpBankStatusIntegracao.SINCRONIZAR);
			this.saveOrUpdate(boletodigital);
		}
	}

	public Boletodigital loadByDocumento(Documento documento) {
		return boletodigitalDAO.loadByDocumento(documento);
	}

	public BoletoDigitalRetornoBean processarRetorno(BoletoDigitalRetornoBean bean) {
		List<BoletoDigitalRetornoDocumentoBean> listaDocumento = new ArrayList<BoletoDigitalRetornoDocumentoBean>();
		
		Money valorTotalPago = new Money();
		
		List<Boletodigital> listaBaixados = this.findForRetornoBaixadas(bean);
		for (Boletodigital boletodigital : listaBaixados) {
			BoletoDigitalRetornoDocumentoBean it = new BoletoDigitalRetornoDocumentoBean();
			it.setMarcado(Boolean.TRUE);
			it.setAcao(BoletoDigitalRetornoAcaoEnum.BAIXAR);
			it.setDocumento(boletodigital.getDocumento());
			it.setDtcredito(boletodigital.getPagamento());
			it.setDtvencimento(boletodigital.getVencimento() != null ? boletodigital.getVencimento() : boletodigital.getDocumento().getDtvencimento());
			it.setNossonumero(boletodigital.getNossonumero());
			it.setUsoempresa(boletodigital.getDocumento().getCddocumento() + "");
			it.setValor(boletodigital.getValorliquidado() != null ? new Money(boletodigital.getValorliquidado()) : new Money());
			it.setValorpago(boletodigital.getValorpago() != null ? new Money(boletodigital.getValorpago()) : new Money());
			listaDocumento.add(it);
			valorTotalPago = valorTotalPago.add(it.getValorpago());
		}
		
		
//		List<Boletodigital> listaCanceladas = this.findForRetornoCanceladas();
//		for (Boletodigital boletodigital : listaBaixados) {
//			BoletoDigitalRetornoDocumentoBean it = new BoletoDigitalRetornoDocumentoBean();
//			it.setMarcado(Boolean.TRUE);
//			it.setAcao(BoletoDigitalRetornoAcaoEnum.CANCELAR);
//			it.setDocumento(boletodigital.getDocumento());
//			it.setDtvencimento(boletodigital.getVencimento() != null ? boletodigital.getVencimento() : boletodigital.getDocumento().getDtvencimento());
//			it.setNossonumero(boletodigital.getNossonumero());
//			it.setUsoempresa(boletodigital.getDocumento().getCddocumento() + "");
//			listaDocumento.add(it);
//		}
		
		Collections.sort(listaDocumento, new Comparator<BoletoDigitalRetornoDocumentoBean>() {
			@Override
			public int compare(BoletoDigitalRetornoDocumentoBean o1, BoletoDigitalRetornoDocumentoBean o2) {
				int compareVencimento = o1.getDocumento().getDtvencimento().compareTo(o2.getDocumento().getDtvencimento());
				if(compareVencimento == 0){
					return o1.getDocumento().getCddocumento().compareTo(o2.getDocumento().getCddocumento());
				}
				return compareVencimento;
			}
		});
				
		bean.setValorTotalPago(valorTotalPago);
		bean.setListaDocumento(listaDocumento);
		return bean;
	}

	private List<Boletodigital> findForRetornoBaixadas(BoletoDigitalRetornoBean bean) {
		return boletodigitalDAO.findForRetornoBaixadas(bean);
	}

	public void processarDocumentos(BoletoDigitalRetornoBean bean) {
		if (bean != null && bean.getListaDocumento() != null){			
			
			List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();	
			List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
			
			for (BoletoDigitalRetornoDocumentoBean retornoBean : bean.getListaDocumento()) {
				if(retornoBean.getMarcado() == null || !retornoBean.getMarcado()){
					continue;
				}
				if (retornoBean.getDocumento() == null){
					retornoBean.addMensagem("Nenhum documento vinculado para ser processado.");
					continue;
				}
				
				//Verifica��o de data limite do ultimo fechamento.
				Documento aux = new Documento();
				aux.setWhereIn(retornoBean.getDocumento().getCddocumento().toString());
				if(fechamentofinanceiroService.verificaListaFechamento(aux)){
					retornoBean.addMensagem("A data de vencimento deste documento refere-se a um per�odo j� fechado.");
					continue;
				}
				
				retornoBean.setDocumento(contareceberService.loadForEntradaWithDadosPessoa(retornoBean.getDocumento()));
				
				//Grava o nosso n�mero na tabela documento
				if (retornoBean.getDocumento() != null && retornoBean.getNossonumero() != null){
					Documento documento = retornoBean.getDocumento();					
					documento.setNossonumero(retornoBean.getNossonumero());
					documentoService.updateNossoNumero(documento);
				}		
				
				if (BoletoDigitalRetornoAcaoEnum.BAIXAR.equals(retornoBean.getAcao())){
					Movimentacao movimentacao = this.doBaixar(bean, retornoBean);
					if (movimentacao != null){
						listaMovimentacao.add(movimentacao);
						listaDocumentoForComissionamento.add(new Documento(retornoBean.getDocumento().getCddocumento()));
					}
				}else {
					retornoBean.setMensagem("A��o n�o identificada.");
				}		
			}
			
			//Atualiza os documentos processados
			for (BoletoDigitalRetornoDocumentoBean retornoBean : bean.getListaDocumento()) {
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getCddocumento() != null && 
						retornoBean.getMarcado() != null && retornoBean.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(retornoBean.getDocumento());
				}
			}
			
			//Atualiza as movimenta��es processadas			
			for (Movimentacao movimentacao2 : listaMovimentacao) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			try {
				if(SinedUtil.isListNotEmpty(listaDocumentoForComissionamento)){
					contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private Movimentacao doBaixar(BoletoDigitalRetornoBean bean, BoletoDigitalRetornoDocumentoBean retornoBean){
		Movimentacao movimentacao = null;
		
		try {
			Rateio rateio = null;
			List<Documento> listaDocumento = new ArrayList<Documento>();
			Documento documento = retornoBean.getDocumento();
			
			if (Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) ||
					Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao())){
			
				if(documento.getValor().getValue().doubleValue() == 0d){
					rateio = contareceberService.atualizaValorRateio(documento, retornoBean.getValor());
					documento = contareceberService.loadForEntradaWithDadosPessoa(retornoBean.getDocumento());
				} else {
					rateio = rateioService.findByDocumento(documento);
					rateioService.atualizaValorRateio(rateio, retornoBean.getValor());
				}							
	
				documento.setValoratual(retornoBean.getValor());
				listaDocumento.add(documento);
			}
			
			if(listaDocumento.size() == 0){
				retornoBean.addMensagem("Baixa n�o realizada. Conta com situa��o diferente de 'Prevista' ou 'Definitiva'.");
				return null;
			}
			
			Money vlrPago = retornoBean.getValorpago() != null ? retornoBean.getValorpago() : retornoBean.getValor();
			
			rateioService.atualizaRateioWithDescontoAndMulta(rateio, new Money(), new Money(), new Money(), retornoBean.getValor(), vlrPago, Tipooperacao.TIPO_CREDITO);	
			
			BaixarContaBean baixarContaBean = new BaixarContaBean();
			baixarContaBean.setListaDocumento(listaDocumento);
			baixarContaBean.setRateio(rateio);
			baixarContaBean.setDtpagamento(retornoBean.getDtcredito());
			baixarContaBean.setDtcredito(retornoBean.getDtcredito());
			baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
			baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
			baixarContaBean.setVinculo(bean.getConta());
			baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			baixarContaBean.setChecknum(retornoBean.getNossonumero());
			baixarContaBean.setFromRetorno(true);
			baixarContaBean.setValorTotal(retornoBean.getValorpago() != null ? retornoBean.getValorpago().getValue().doubleValue() : retornoBean.getValor().getValue().doubleValue());
			baixarContaBean.setMensagem(retornoBean.getMensagem());
			
			movimentacao = documentoService.doBaixarConta(null, baixarContaBean);						
							
			notaDocumentoService.liberaNotaDocumentoWebservice(NeoWeb.getRequestContext(), baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
			documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);	
			
			if (movimentacao == null) {
				retornoBean.addMensagem("Baixa n�o efetuada");
			} else {
				retornoBean.addMensagem("Baixa efetuada");
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			retornoBean.addMensagem("Baixa n�o efetuada: " + e.getMessage());
		}
			
		return movimentacao;
	}

}
