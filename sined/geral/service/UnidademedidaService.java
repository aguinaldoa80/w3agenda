package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialunidademedidaVO;
import br.com.linkcom.sined.geral.dao.UnidademedidaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.UnidadeMedidaOfflineJSON;
import br.com.linkcom.sined.util.rest.android.unidademedida.UnidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.unidademedida.UnidadeMedidaW3producaoRESTModel;

public class UnidademedidaService extends GenericService<Unidademedida> {

	private UnidademedidaDAO unidademedidaDAO;
	private MaterialService materialService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private MaterialunidademedidaService materialunidademedidaService;
	
	public void setUnidademedidaDAO(UnidademedidaDAO unidademedidaDAO) {
		this.unidademedidaDAO = unidademedidaDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	
	/* singleton */
	private static UnidademedidaService instance;
	public static UnidademedidaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UnidademedidaService.class);
		}
		return instance;
	}
	
	/**
	* M�todo que valida se as convers�es est�o iguais entre as unidades de medida 
	*
	* @param unidademedida
	* @param errors
	* @return
	* @since 12/04/2016
	* @author Luiz Fernando
	*/
	public boolean validaConversoesUnidade(Unidademedida unidademedida, BindException errors){
		if(SinedUtil.isListNotEmpty(unidademedida.getListaUnidademedidaconversao())){
			List<Unidademedidaconversao> list;
			for(Unidademedidaconversao unidademedidaconversao : unidademedida.getListaUnidademedidaconversao()){
				
				Double fracao = unidademedidaconversao.getFracao();
				Double qtdereferencia = unidademedidaconversao.getQtdereferencia();
				
				if(unidademedida.getCdunidademedida() != null){
					list = unidademedidaconversaoService.findForValidacaoConversao(unidademedida, unidademedidaconversao.getUnidademedidarelacionada());
					if(SinedUtil.isListNotEmpty(list)){
						for(Unidademedidaconversao item : list){
							if((item.getFracao() == null || item.getFracao() == 0) ||
									(item.getQtdereferencia() == null || item.getQtdereferencia() == 0)){
								errors.reject("001", "N�o � permitido cadastrar a fra��o e/ou qtde refer�ncia com valores zerados. Unidade de medida com o erro: " + item.getUnidademedidarelacionada().getNome());
								return true;
							}
							
							Double fracao2 = item.getFracao();
							Double qtdereferencia2 = item.getQtdereferencia();
							
							Double fracaoQtdereferencia = (qtdereferencia2 / fracao2);
							if(unidademedidaconversao.getFracaoQtdereferencia().compareTo(fracaoQtdereferencia) != 0 && 
									SinedUtil.round(unidademedidaconversao.getFracaoQtdereferencia(), 2).compareTo(SinedUtil.round(fracaoQtdereferencia, 2)) != 0){
								errors.reject("001", "Existe convers�o divergente relacionado a unidade de medida " + item.getUnidademedidarelacionada().getNome()+ ". Unidade de medida com valor divergente: " + item.getUnidademedida().getNome());
								return true;
							}
						}
					}
				}
				
				
				for(Unidademedidaconversao unidademedidaconversao2 : unidademedida.getListaUnidademedidaconversao()){
					if(!unidademedidaconversao2.getUnidademedidarelacionada().equals(unidademedidaconversao.getUnidademedidarelacionada())){
						list = unidademedidaconversaoService.findForValidacaoConversao(unidademedidaconversao2.getUnidademedidarelacionada(), unidademedidaconversao.getUnidademedidarelacionada());
						if(SinedUtil.isListNotEmpty(list)){
							for(Unidademedidaconversao item : list){
								if((item.getFracao() == null || item.getFracao() == 0) ||
										(item.getQtdereferencia() == null || item.getQtdereferencia() == 0)){
									errors.reject("001", "N�o � permitido cadastrar a fra��o e/ou qtde refer�ncia com valores zerados. Unidade de medida com o erro: " + item.getUnidademedidarelacionada().getNome());
									return true;
								}
								
								Double fracao2 = unidademedidaconversao2.getFracao();
								Double qtdereferencia2 = unidademedidaconversao2.getQtdereferencia();
								
								Double fracaoQtdereferencia = (qtdereferencia * fracao2) / (fracao * qtdereferencia2); 
								if(item.getFracaoQtdereferencia().compareTo(fracaoQtdereferencia) != 0 &&
										SinedUtil.round(item.getFracaoQtdereferencia(), 2).compareTo(SinedUtil.round(fracaoQtdereferencia, 2)) != 0){
									errors.reject("001", "Existe convers�o divergente relacionado a unidade de medida " + item.getUnidademedidarelacionada().getNome() + ". Unidade de medida com valor divergente: " + item.getUnidademedida().getNome());
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Unidademedida> findAllForFlex(){
		return unidademedidaDAO.findAllForFlex();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#findBySimbolo
	 * @param unidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Unidademedida findBySimbolo(String unidade) {
		return unidademedidaDAO.findBySimbolo(unidade);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Unidademedida findByMaterial(Material material) {
		return unidademedidaDAO.findByMaterial(material);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#unidadeAndUnidadeRelacionada
	 *
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 */
	public List<Unidademedida> unidadeAndUnidadeRelacionada(Unidademedida unidademedida){
		return unidademedidaDAO.unidadeAndUnidadeRelacionada(unidademedida);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#findForSpedReg0190
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Unidademedida> findForSpedReg0190(String whereIn) {
		return unidademedidaDAO.findForSpedReg0190(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO.findForSpedPiscofinsReg0190(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedida> findForSpedPiscofinsReg0190(String whereIn) {
		return unidademedidaDAO.findForSpedPiscofinsReg0190(whereIn);
	}	
	
	/**
	 * M�todo que converte a quantidade de acordo com a unidade de medida relacionada
	 *
	 * @param unidademedida
	 * @param qtde
	 * @param unidademedidarelacionada
	 * @param material
	 * @param fatorconversao
	 * @return
	 * @author Luiz Fernando
	 */
	public Double converteQtdeUnidademedida(Unidademedida unidademedida, Double qtde, Unidademedida unidademedidarelacionada, Material material, Double fatorconversao, Double qtdereferencia){
		if(unidademedida != null && unidademedida.getCdunidademedida() != null && unidademedidarelacionada != null && qtde != null){
			Unidademedida umRelacionada = this.findForConverter(unidademedidarelacionada);
			Unidademedida umEntrada = this.findForConverter(unidademedida);	
			
			if(material != null && material.getCdmaterial() != null){
				Material materialaux = materialService.findListaMaterialunidademedida(material);
				if(materialaux != null && materialaux.getListaMaterialunidademedida() != null && !materialaux.getListaMaterialunidademedida().isEmpty()){
					for(Materialunidademedida materialunidademedida : materialaux.getListaMaterialunidademedida()){
						if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().equals(umRelacionada)){
							if(fatorconversao != null && fatorconversao != 0){
								return SinedUtil.roundByUnidademedida(qtde / (fatorconversao / (qtdereferencia != null ? qtdereferencia : 1.0)), unidademedida);
							}else if(materialunidademedida.getFracao() != null && materialunidademedida.getFracao() != 0){
								 return SinedUtil.roundByUnidademedida(qtde / materialunidademedida.getFracaoQtdereferencia(), unidademedida);
							}
						}
					}
				}
			}
			if(umEntrada != null && umEntrada.getListaUnidademedidaconversao() != null && !umEntrada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umEntrada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(umRelacionada)){
						if(fatorconversao != null && fatorconversao != 0){
							return SinedUtil.roundByUnidademedida(qtde / (fatorconversao / (qtdereferencia != null ? qtdereferencia : 1.0)), umRelacionada);
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return SinedUtil.roundByUnidademedida(qtde / unidademedidaconversao.getFracaoQtdereferencia(), umRelacionada);
						}
					}
				}
			}
			if(umRelacionada != null && umRelacionada.getListaUnidademedidaconversao() != null && !umRelacionada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umRelacionada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(unidademedida)){
						if(fatorconversao != null && fatorconversao != 0){
							return SinedUtil.roundByUnidademedida(qtde / (fatorconversao / (qtdereferencia != null ? qtdereferencia : 1.0)), umRelacionada);
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return SinedUtil.roundByUnidademedida(qtde / unidademedidaconversao.getFracaoQtdereferencia(), umRelacionada);
						}
					}
				}
			}
			
			return qtde;
		}
		
		return 0.0;
	}
	
	public Double getFracaoconversaoUnidademedida(Unidademedida unidademedida, Double qtde, Unidademedida unidademedidarelacionada, Material material, Double fatorconversao){
		if(unidademedida != null && unidademedida.getCdunidademedida() != null && unidademedidarelacionada != null && qtde != null){
			Unidademedida umEntrada = this.findForConverter(unidademedida);	
			
			if(material != null && material.getCdmaterial() != null){
				Material materialaux = materialService.findListaMaterialunidademedida(material);
				if(materialaux != null && materialaux.getListaMaterialunidademedida() != null && !materialaux.getListaMaterialunidademedida().isEmpty()){
					for(Materialunidademedida materialunidademedida : materialaux.getListaMaterialunidademedida()){
						if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().equals(unidademedidarelacionada)){
							if(fatorconversao != null && fatorconversao != 0){
								return fatorconversao;
							}else if(materialunidademedida.getFracao() != null && materialunidademedida.getFracao() != 0){
								 return materialunidademedida.getFracaoQtdereferencia();
							}
						}
					}
				}
			}
			if(umEntrada != null && umEntrada.getListaUnidademedidaconversao() != null && !umEntrada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umEntrada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(unidademedidarelacionada)){
						if(fatorconversao != null && fatorconversao != 0){
							return fatorconversao;
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return unidademedidaconversao.getFracaoQtdereferencia();
						}
					}
				}
			}
			Unidademedida umRelacionada = this.findForConverter(unidademedidarelacionada);
			if(umRelacionada != null && umRelacionada.getListaUnidademedidaconversao() != null && !umRelacionada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umRelacionada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(unidademedida)){
						if(fatorconversao != null && fatorconversao != 0){
							return fatorconversao;
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return unidademedidaconversao.getFracaoQtdereferencia();
						}
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * M�todo que retorna o fator de convers�o utilizado na venda ou do material
	 *
	 * @param unidademedida
	 * @param qtde
	 * @param unidademedidarelacionada
	 * @param material
	 * @param fatorconversao
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getFatorconversao(Unidademedida unidademedida, Double qtde, Unidademedida unidademedidarelacionada, Material material, Double fatorconversao, Double qtdereferencia){
		if(unidademedida != null && unidademedida.getCdunidademedida() != null && unidademedidarelacionada != null && qtde != null){
			Unidademedida umEntrada = this.findForConverter(unidademedida);	
			
			if(material != null && material.getCdmaterial() != null){
				Material materialaux = materialService.findListaMaterialunidademedida(material);
				if(materialaux != null && materialaux.getListaMaterialunidademedida() != null && !materialaux.getListaMaterialunidademedida().isEmpty()){
					for(Materialunidademedida materialunidademedida : materialaux.getListaMaterialunidademedida()){
						if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().equals(unidademedidarelacionada)){
							if(fatorconversao != null && fatorconversao != 0){
								return (fatorconversao / (qtdereferencia != null ? qtdereferencia : 1.0));
							}else if(materialunidademedida.getFracao() != null && materialunidademedida.getFracao() != 0){
								 return materialunidademedida.getFracaoQtdereferencia();
							}
						}
					}
				}
			}
			if(umEntrada != null && umEntrada.getListaUnidademedidaconversao() != null && !umEntrada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umEntrada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(unidademedidarelacionada)){
						if(fatorconversao != null && fatorconversao != 0){
							return fatorconversao;
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return unidademedidaconversao.getFracaoQtdereferencia();
						}
					}
				}
			}
			Unidademedida umRelacionada = this.findForConverter(unidademedidarelacionada);
			if(umRelacionada != null && umRelacionada.getListaUnidademedidaconversao() != null && !umRelacionada.getListaUnidademedidaconversao().isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : umRelacionada.getListaUnidademedidaconversao()){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && unidademedidaconversao.getUnidademedidarelacionada().equals(unidademedida)){
						if(fatorconversao != null && fatorconversao != 0){
							return fatorconversao;
						}else if(unidademedidaconversao.getFracao() != null && unidademedidaconversao.getFracao() != 0){
							 return unidademedidaconversao.getFracaoQtdereferencia();
						}
					}
				}
			}
		}
		
		return null;
	}

	private Unidademedida findForConverter(Unidademedida unidademedida) {
		return unidademedidaDAO.findForConverter(unidademedida);
	}
	
	public List<Unidademedida> findUnidademedidarelacionado(Material material){
		List<Unidademedida> lista = new ArrayList<Unidademedida>();
		Integer cdunidademedida = materialService.getIdUnidadeMedida(material);
		Unidademedida unidademedida = new Unidademedida();
		unidademedida.setCdunidademedida(cdunidademedida);
		lista =  unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(unidademedida, material);		
		return lista;
	}

	public List<UnidadeMedidaOfflineJSON> findForPVOffline() {
		List<Unidademedida> listaUnidademedida = unidademedidaDAO.findForPVOffline();
		
		List<UnidadeMedidaOfflineJSON> _lista = new ArrayList<UnidadeMedidaOfflineJSON>();
		if(listaUnidademedida != null && !listaUnidademedida.isEmpty()){
			for(Unidademedida unidademedida : listaUnidademedida){
				_lista.add(new UnidadeMedidaOfflineJSON(unidademedida));
			}
		}
		
		return _lista;
	}
	
	public List<UnidademedidaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<UnidademedidaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<UnidademedidaRESTModel> lista = new ArrayList<UnidademedidaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Unidademedida um : unidademedidaDAO.findForAndroid(whereIn))
				lista.add(new UnidademedidaRESTModel(um));
		}
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#findForImportarproducao()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedida> findForImportarproducao() {
		return unidademedidaDAO.findForImportarproducao();
	}
	
	public List<Unidademedida> getUnidademedidaByMaterial(Material bean) {
		List<Unidademedida> unidadesMedida = new ArrayList<Unidademedida>();

		if(Util.objects.isNotPersistent(bean)){
			return new ArrayList<Unidademedida>();
		}
		bean = materialService.unidadeMedidaMaterial(bean);			
		if (bean != null){
			List<Unidademedida> lista = this.unidadeAndUnidadeRelacionada(bean.getUnidademedida());
			Material material = materialService.findListaMaterialunidademedida(bean);
			
			List<Unidademedida> unidadesMedidaAll = new ArrayList<Unidademedida>();
			unidadesMedidaAll.add(bean.getUnidademedida());
			
			if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
				for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
					if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null){
						materialunidademedida.getUnidademedida().setFracao(materialunidademedida.getFracao() != null ? materialunidademedida.getFracao() : null);
						materialunidademedida.getUnidademedida().setQtdereferencia(materialunidademedida.getQtdereferencia() != null ? materialunidademedida.getQtdereferencia() : null);
						lista.add(materialunidademedida.getUnidademedida());
					}
				}
			}
			
			if(lista != null && lista.size() > 0){
				for (Unidademedida item : lista) {
					if(item.getListaUnidademedidaconversao() != null && item.getListaUnidademedidaconversao().size() > 0){
						for (Unidademedidaconversao item2 : item.getListaUnidademedidaconversao()) {
							if(item2.getUnidademedida() != null){
								item2.getUnidademedida().setFracao(item2.getFracao() != null ? item2.getFracao() : null);
								item2.getUnidademedida().setQtdereferencia(item2.getQtdereferencia() != null ? item2.getQtdereferencia() : null);
								unidadesMedidaAll.add(item2.getUnidademedida());
							}
							if(item2.getUnidademedidarelacionada() != null){
								item2.getUnidademedidarelacionada().setFracao(item2.getFracao() != null ? item2.getFracao() : null);
								item2.getUnidademedidarelacionada().setQtdereferencia(item2.getQtdereferencia() != null ? item2.getQtdereferencia() : null);
								unidadesMedidaAll.add(item2.getUnidademedidarelacionada());
							}
						}
					}
					unidadesMedidaAll.add(item);
				}
			}
			if(unidadesMedidaAll != null && unidadesMedidaAll.size() > 0){
				for (Unidademedida unidademedida : unidadesMedidaAll) {
					if(unidadesMedida == null || unidadesMedida.size() == 0 || !unidadesMedida.contains(unidademedida)){
						unidadesMedida.add(unidademedida);
					}
				}
			}
		}
		return unidadesMedida;
	}
	
	
	//m�todo utilizado no item da nota de produto e na devolu��o de material
	public Double getFracaoConversaoUnidademedida(Material material, Unidademedida unidademedida) {
		Set<Materialunidademedida> listaMaterialunidademedida = material.getListaMaterialunidademedida();
		Double fracao = null;
		
		if(material != null){
			if(material.getUnidademedida() != null && material.getUnidademedida().equals(unidademedida)){
				fracao = 1d;
			} else if(listaMaterialunidademedida != null && !listaMaterialunidademedida.isEmpty()){
				for(Materialunidademedida materialunidademedida : listaMaterialunidademedida){
					if(materialunidademedida.getUnidademedida() != null && 
						materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
						materialunidademedida.getFracao() != null){
						
						if(materialunidademedida.getUnidademedida().equals(unidademedida)){
							fracao = materialunidademedida.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}else {
				List<Unidademedida> lista = this.unidadeAndUnidadeRelacionada(material.getUnidademedida());
				if(lista != null && lista.size() > 0){
					boolean achou = false;
					for (Unidademedida item : lista) {
						if(item.getListaUnidademedidaconversao() != null && item.getListaUnidademedidaconversao().size() > 0){
							for (Unidademedidaconversao item2 : item.getListaUnidademedidaconversao()) {
								if(item2.getUnidademedida() != null && 
										item2.getUnidademedida().getCdunidademedida() != null &&
										item2.getFracao() != null){
									if(item2.getUnidademedida().equals(unidademedida)){
										fracao = item2.getFracaoQtdereferencia();
										achou = true;
										break;
									}
								}
							}
							if(achou){
								break;
							}
						}
					}
				}
			}
		}
		
		return fracao;
	}

	//m�todo utilizado no item da nota de produto e na devolu��o de material
	public MaterialunidademedidaVO getFracaoConversaoUnidademedidaForVO(Material material, Unidademedida unidademedida) {
		MaterialunidademedidaVO mumVO = new MaterialunidademedidaVO();
		Set<Materialunidademedida> listaMaterialunidademedida = material.getListaMaterialunidademedida();
		
		if(material != null){
			if(material.getUnidademedida() != null && material.getUnidademedida().equals(unidademedida)){
				mumVO.setFracao(1d);
				mumVO.setQtdereferencia(1d);
				mumVO.setFatorconversao(1d);	
			} else if(listaMaterialunidademedida != null && !listaMaterialunidademedida.isEmpty()){
				for(Materialunidademedida materialunidademedida : listaMaterialunidademedida){
					if(materialunidademedida.getUnidademedida() != null && 
						materialunidademedida.getUnidademedida().getCdunidademedida() != null &&materialunidademedida.getFracao() != null){
						
						if(materialunidademedida.getUnidademedida().equals(unidademedida)){
							mumVO.setFracao(materialunidademedida.getFracaoQtdereferencia());
							mumVO.setQtdereferencia(materialunidademedida.getQtdereferencia());
							mumVO.setFatorconversao(materialunidademedida.getFracao());							
							break;
						}
					}
				}
			}
		}
		
		return mumVO;
	}
	
	
	public List<Unidademedida> setListUnidademedidaWithConversao(List<Unidademedida> lista) {
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		
		if(lista!=null && !lista.isEmpty()){
			for (Unidademedida unidademedida : lista){
				listaUnidademedida.add(unidademedida);
				if(unidademedida.getListaUnidademedidaconversao()!=null && !unidademedida.getListaUnidademedidaconversao().isEmpty()){
					for (Unidademedidaconversao unidademedidaconversao : unidademedida.getListaUnidademedidaconversao()){
						Unidademedida unidadeconversao = unidademedidaconversao.getUnidademedidarelacionada();
						if(unidadeconversao!=null && unidadeconversao.getCdunidademedida()!=null){
							listaUnidademedida.add(unidadeconversao);	
						}
					}
				}
			}
		}
		
		return listaUnidademedida;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param tarefa
	 * @return
	 * @author Lucas Costa
	 */
	public Unidademedida findByTarefa(Tarefa tarefa) {
		return unidademedidaDAO.findByTarefa(tarefa);
	}
	
	/**
	* M�todo que converte a quantidade de acordo com a unidade de medida anterior
	*
	* @param material
	* @param unidademedida
	* @param unidademedidaAnterior
	* @param qtde
	* @return
	* @since 09/10/2015
	* @author Luiz Fernando
	*/
	public Double getQtdeConvertida(Material material, Unidademedida unidademedida, Unidademedida unidademedidaAnterior, Double qtde, boolean arredondar){
		if(material != null && unidademedida != null && unidademedidaAnterior != null && qtde != null && 
				!unidademedida.equals(unidademedidaAnterior)){
			Material materialUnidade = materialService.findListaMaterialunidademedida(material);
			
			Unidademedida unidademedidaAntiga = new Unidademedida(unidademedidaAnterior.getCdunidademedida());
			
			if(!materialUnidade.getUnidademedida().equals(unidademedida) && !materialUnidade.getUnidademedida().equals(unidademedidaAntiga)){
				Double fracao1 = getFracao(materialUnidade, unidademedidaAntiga, materialUnidade.getUnidademedida());
				Double fracao2 = getFracao(materialUnidade, materialUnidade.getUnidademedida(), unidademedidaAntiga);
				
				if(fracao1 != null && fracao2 != null){
					if(arredondar){
						qtde = SinedUtil.round((qtde * fracao1) / fracao2, 10);						
					}else{
						qtde = (qtde * fracao1) / fracao2;	
					}
				}
				unidademedidaAntiga = materialUnidade.getUnidademedida();
			}
			
			Double fracao1 = getFracao(materialUnidade, unidademedidaAntiga, unidademedida);
			Double fracao2 = getFracao(materialUnidade, unidademedida, unidademedidaAntiga);		
			
			if(fracao1 != null && fracao2 != null){
				if(arredondar){
					qtde = SinedUtil.round((qtde * fracao1) / fracao2, 10);					
				}else{
					qtde = (qtde * fracao1) / fracao2;					
				}
			}
		}
		
		return qtde;
	}
	
	public Double getQtdeConvertida(Material material, Unidademedida unidademedida, Unidademedida unidademedidaAnterior, Double qtde){
		return this.getQtdeConvertida(material, unidademedida, unidademedidaAnterior, qtde, true);
	}
	
	public List<UnidadeMedidaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo que forma a lista de unidademedida para sincroniza��o com W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<UnidadeMedidaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<UnidadeMedidaW3producaoRESTModel> lista = new ArrayList<UnidadeMedidaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Unidademedida um : unidademedidaDAO.findForW3Producao(whereIn))
				lista.add(new UnidadeMedidaW3producaoRESTModel(um));
		}
		
		return lista;
	}
	
	public Double getFracao(Material material, Unidademedida unidademedida, Unidademedida unidademedidaNova) {
		Set<Materialunidademedida> listaMaterialunidademedida = material.getListaMaterialunidademedida();
		Double fracao = null;
		
		if(material != null){
			boolean achou = false;
			if(material.getUnidademedida() != null && material.getUnidademedida().equals(unidademedida)){
				fracao = 1d;
				material.setFracao(1d);
				material.setQtdereferencia(1d);
				achou = true;
			} else if(listaMaterialunidademedida != null && !listaMaterialunidademedida.isEmpty()){
				for(Materialunidademedida materialunidademedida : listaMaterialunidademedida){
					if(materialunidademedida.getUnidademedida() != null && 
						materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
						materialunidademedida.getFracao() != null){
						
						if(materialunidademedida.getUnidademedida().equals(unidademedida)){
							if(unidademedidaNova != null && material.getUnidademedida().equals(unidademedidaNova)){
								fracao = materialunidademedida.getQtdereferencia() / materialunidademedida.getFracao();
								material.setFracao(materialunidademedida.getQtdereferencia());
								material.setQtdereferencia(materialunidademedida.getFracao());
							}else {
								fracao = materialunidademedida.getFracaoQtdereferencia();
								material.setFracao(materialunidademedida.getFracao());
								material.setQtdereferencia(materialunidademedida.getQtdereferencia());
							}
//							fracao = materialunidademedida.getFracaoQtdereferencia();
//							material.setFracao(materialunidademedida.getFracao());
//							material.setQtdereferencia(materialunidademedida.getQtdereferencia());
							achou = true;
							break;
						}
					}
				}
			}
			if(!achou) {
				List<Unidademedida> lista = this.unidadeAndUnidadeRelacionada(unidademedida);
				if(lista != null && lista.size() > 0){
					achou = false;
					for (Unidademedida item : lista) {
						if(item.getListaUnidademedidaconversao() != null && item.getListaUnidademedidaconversao().size() > 0){
							for (Unidademedidaconversao item2 : item.getListaUnidademedidaconversao()) {
								if(item2.getUnidademedida() != null && 
										item2.getUnidademedida().getCdunidademedida() != null &&
										item2.getFracao() != null){
									if(item2.getUnidademedidarelacionada().equals(unidademedida) && item.equals(unidademedidaNova) || 
											item2.getUnidademedidarelacionada().equals(unidademedidaNova) && item.equals(unidademedida)){
										achou = true;
										if(unidademedidaNova != null && item2.getUnidademedida().equals(unidademedidaNova)){
											fracao = item2.getQtdereferencia() / item2.getFracao();
											material.setFracao(item2.getQtdereferencia());
											material.setQtdereferencia(item2.getFracao());
										}else {
											fracao = item2.getFracaoQtdereferencia();
											material.setFracao(item2.getFracao());
											material.setQtdereferencia(item2.getQtdereferencia());
										}
										break;
									}
								}
							}
							if(achou){
								break;
							}
						}
					}
				}
			}
		}
		
		return fracao;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.UnidademedidaDAO#getQtdeUnidadePrincipal(Material material, Unidademedida unidademedida, Double qtde)
	*
	* @param material
	* @param unidademedida
	* @param qtde
	* @return
	* @since 24/10/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeUnidadePrincipal(Material material, Unidademedida unidademedida, Double qtde){
		return unidademedidaDAO.getQtdeUnidadePrincipal(material, unidademedida, qtde);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.UnidademedidaDAO#findForSagefiscal(String whereIn)
	*
	* @param listaSimboloUnidademedida
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Unidademedida> findForSagefiscal(List<String> listaSimboloUnidademedida) {
		return unidademedidaDAO.findForSagefiscal(listaSimboloUnidademedida);
	}
	
	public boolean existeMaterialComUnidademedidaVendida(Unidademedida unVenda, Materialrelacionado materialrelacionado) {
		boolean existeUnidademedida = false;
		if(unVenda != null && materialrelacionado != null && materialrelacionado.getMaterialpromocao() != null){
			Material material = materialService.loadMaterialunidademedida(materialrelacionado.getMaterialpromocao());
			
			if(material.getUnidademedida() != null){
				if(unVenda.equals(material.getUnidademedida())){
					existeUnidademedida = true;
				}else {
					if(SinedUtil.isListNotEmpty(material.getListaMaterialunidademedida())){
						for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
							if(unVenda.equals(materialunidademedida.getUnidademedida())){
								existeUnidademedida = true;
								break;
							}
						}
					}
					if(!existeUnidademedida && SinedUtil.isListNotEmpty(material.getUnidademedida().getListaUnidademedidaconversao())){
						for(Unidademedidaconversao unidademedidaconversao : material.getUnidademedida().getListaUnidademedidaconversao()){
							if(unVenda.equals(unidademedidaconversao.getUnidademedida())){
								existeUnidademedida = false;
								break;
							}
						}
					}
				}
			}
		}
		return existeUnidademedida;
	}
	
	public List<Unidademedida> findByConversaoUnidade(Material material, String stCopiar) {
		List<Unidademedida> lista = new ArrayList<Unidademedida>();
		if(Util.objects.isPersistent(material)){
			List<Materialunidademedida> listaMaterialUnidade = materialunidademedidaService.findByMaterial(material);
			for(Materialunidademedida mu: listaMaterialUnidade){
				lista.add(mu.getUnidademedida());
			}
			if(Util.objects.isPersistent(material.getUnidademedida())){
				lista.add(this.load(material.getUnidademedida()));
			}
		}else if(StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			Material materialTrans = new Material (material.getCdmaterialtrans());
			
			List<Materialunidademedida> listaMaterialUnidade = materialunidademedidaService.findByMaterial(materialTrans);
			for(Materialunidademedida mu: listaMaterialUnidade){
				lista.add(mu.getUnidademedida());
			}
			if(Util.objects.isPersistent(material.getUnidademedida())){
				lista.add(this.load(material.getUnidademedida()));
			}
		}
		return lista;
	}

}
