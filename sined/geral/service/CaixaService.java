package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.dao.CaixaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CaixaService extends GenericService<Conta> {

	private CaixaDAO caixaDAO;

	public void setCaixaDAO(CaixaDAO caixaDAO) {
		this.caixaDAO = caixaDAO;
	}


	/**
	 * M�todo para verificar se existe caixa cadastrada no banco com a mesma descri��o.
	 *
	 * @see #countCaixaByDescricao(Conta)
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean existeNomeCaixa(Conta bean){
		Integer count = countCaixaByDescricao(bean);
		return count != 0;
	}
	
	/**
	 * M�todo pra obter o n�mero de caixas com a mesma descri��o.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CaixaDAO#countCaixaByDescricao(Caixa)
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Integer countCaixaByDescricao(Conta bean){
		return caixaDAO.countCaixaByDescricao(bean);
	}


	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CaixaDAO#isCaixaAndConciliacaoAuto
	 *
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isCaixaAndConciliacaoAuto(Conta conta) {
		return caixaDAO.isCaixaAndConciliacaoAuto(conta);
	}

}
