package br.com.linkcom.sined.geral.service;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolha;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolhaEvento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoFolha;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.geral.bean.state.AgendamentoAcao;
import br.com.linkcom.sined.geral.dao.FechamentoFolhaDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FechamentoFolhaService extends GenericService<Fechamentofinanceiro> {
	
	private FechamentoFolhaDAO fechamentoFolhaDAO;
	private ProvisaoService provisaoService;
	private AgendamentoService agendamentoService;
	private RateioService rateioService;
	private AgendamentoHistoricoService agendamentoHistoricoService;

	private ColaboradordespesaService colaboradorDespesaService;
	
	public void setFechamentoFolhaDAO(FechamentoFolhaDAO fechamentoFolhaDAO) {
		this.fechamentoFolhaDAO = fechamentoFolhaDAO;
	}
	public void setProvisaoService(ProvisaoService provisaoService) {
		this.provisaoService = provisaoService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setColaboradorDespesaService(
			ColaboradordespesaService colaboradorDespesaService) {
		this.colaboradorDespesaService = colaboradorDespesaService;
	}
	public void setAgendamentoHistoricoService(
			AgendamentoHistoricoService agendamentoHistoricoService) {
		this.agendamentoHistoricoService = agendamentoHistoricoService;
	}
	
	public boolean atualizarAgendamentoAposFechamentoFolha(FechamentoFolha fechamentoFolha, ConfiguracaoFolha configuracaoFolha) {
		boolean agendamentoAtualizado = false;
		StringBuilder whereInEventoDespesaColaborador = new StringBuilder();
		StringBuilder whereInEventoProvisionamento = new StringBuilder();
		if(configuracaoFolha != null && configuracaoFolha.getAgendamento() != null && SinedUtil.isListNotEmpty(configuracaoFolha.getListaEvento())){
			for(ConfiguracaoFolhaEvento configFolhaEvento : configuracaoFolha.getListaEvento()){
				if(Boolean.TRUE.equals(configFolhaEvento.getEvento().getProvisionamento())){
					if(whereInEventoProvisionamento.length()!=0){
						whereInEventoProvisionamento.append(", " + configFolhaEvento.getEvento().getCdEventoPagamento().toString());						
					}else{
						whereInEventoProvisionamento.append(configFolhaEvento.getEvento().getCdEventoPagamento().toString());						
					}
				}else{
					if(whereInEventoDespesaColaborador.length()!=0){
						whereInEventoDespesaColaborador.append(", " + configFolhaEvento.getEvento().getCdEventoPagamento().toString());						
					}else{
						whereInEventoDespesaColaborador.append(configFolhaEvento.getEvento().getCdEventoPagamento().toString());						
					}
				}
			}
			Money valorTotalProvisionamento = provisaoService.calcularValorTotalFechamentoFolha(configuracaoFolha.getAgendamento().getEmpresa(), whereInEventoProvisionamento.toString(), null, fechamentoFolha.getDtFim());
			Money valorTotalDespesaColaborador = colaboradorDespesaService.calcularValorTotalDespesaColaborador(configuracaoFolha.getAgendamento().getEmpresa(), whereInEventoDespesaColaborador.toString(), fechamentoFolha.getDtInicio(), fechamentoFolha.getDtFim());
			Money valorTotal = valorTotalProvisionamento.add(valorTotalDespesaColaborador);
			
			if(valorTotal == null || valorTotal.getValue().doubleValue() < 0) valorTotal = new Money();
			Agendamento agendamentoAntigo = agendamentoService.findValorAgendamento(configuracaoFolha.getAgendamento());
			agendamentoService.updateValor(configuracaoFolha.getAgendamento(), valorTotal);
			agendamentoHistoricoService.salvarHistoricoAgendamento(configuracaoFolha.getAgendamento(), new AgendamentoAcao(AgendamentoAcao.ALTERADO), "Altera��o de valor por Fechamento de Folha. Valor anterior: R$" + agendamentoAntigo.getValor());
			if(configuracaoFolha.getAgendamento().getRateio() != null && SinedUtil.isListNotEmpty(configuracaoFolha.getAgendamento().getRateio().getListaRateioitem())){
				rateioService.atualizaValorRateio(configuracaoFolha.getAgendamento().getRateio(), valorTotal);
				rateioService.saveOrUpdate(configuracaoFolha.getAgendamento().getRateio());
			}
			
			Date dtproximo = configuracaoFolha.getAgendamento().getDtproximo();
			Date dtFim = fechamentoFolha.getDtFim() != null ? SinedDateUtils.addMesData(fechamentoFolha.getDtFim(), 1) : null;
			
			
			if(dtproximo != null && dtFim != null && SinedDateUtils.getMes(dtproximo) == SinedDateUtils.getMes(dtFim) && 
					SinedDateUtils.getAno(dtproximo) == SinedDateUtils.getAno(dtFim)){
				agendamentoService.updateDtproximo(configuracaoFolha.getAgendamento(), SinedDateUtils.addMesData(dtproximo, 1));
			}
			agendamentoAtualizado = true;
		}
		return agendamentoAtualizado;
	}
	
	public boolean existeFechamentoPeriodo(FechamentoFolha bean) {
		return fechamentoFolhaDAO.existeFechamentoPeriodo(bean);
	}
	
	public boolean existeFechamentoPeriodo(Empresa empresa, Date data) {
		return fechamentoFolhaDAO.existeFechamentoPeriodo(empresa, data);
	}
}