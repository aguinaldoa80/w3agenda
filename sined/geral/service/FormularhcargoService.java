package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhcargo;
import br.com.linkcom.sined.geral.dao.FormularhcargoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormularhcargoService extends GenericService<Formularhcargo>{

	private FormularhcargoDAO formularhcargoDAO;
	
	public void setFormularhcargoDAO(FormularhcargoDAO formularhcargoDAO) {
		this.formularhcargoDAO = formularhcargoDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 20/03/2012
	 * @param whereIn
	 * @param cargos
	 * @return
	 */
	public List<Formularhcargo> validaCargo(String whereIn, String cargos){
		return formularhcargoDAO.validaCargo(whereIn, cargos);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 20/03/2012
	 * @param formularh
	 * @return
	 */
	public List<Formularhcargo> getListaFormulaRhCargoByFormulaRh(Formularh formularh){
		return formularhcargoDAO.getListaFormulaRhCargoByFormulaRh(formularh);
	}
}
