package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominioemailusuario;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.DominioemailusuarioDAO;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioemailusuarioFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominioemailusuarioService extends GenericService<Dominioemailusuario>{

	private DominioemailusuarioDAO dominioemailusuarioDAO;
	
	public void setDominioemailusuarioDAO(DominioemailusuarioDAO dominioemailusuarioDAO) {
		this.dominioemailusuarioDAO = dominioemailusuarioDAO;
	}
	
	/**
	 * Retorna lista de acordo com o filtro
	 * @param filtro
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominioemailusuario> findByFiltro(DominioemailusuarioFiltro filtro){
		return dominioemailusuarioDAO.findByFiltro(filtro);
	}
	
	@Override
	public void saveOrUpdate(final Dominioemailusuario bean) {
		boolean novoRegistro = false;
		Dominioemailusuario dominioemailusuarioAnterior = null;
		if(bean.getCddominioemailusuario() == null){
			novoRegistro = true;
		}else{
			dominioemailusuarioAnterior = load(bean);
			bean.setTemporario(dominioemailusuarioAnterior.getDiretorio());
		}
		super.saveOrUpdate(bean);
		
		SinedUtil.executaBriefCase(Servicoservidortipo.EMAIL, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCddominioemailusuario());
		updateCampoTemporario(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void updateCampoTemporario(Dominioemailusuario bean) {
		dominioemailusuarioDAO.updateCampoTemporario(bean);
	}

	@Override
	public void delete(Dominioemailusuario bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.EMAIL, AcaoBriefCase.REMOVER, bean.getCddominioemailusuario());
		super.delete(bean);
	}

	public int countContratomaterial(Integer cddominioemailusuario, Contratomaterial contratomaterial) {
		return dominioemailusuarioDAO.countContratomaterial(cddominioemailusuario, contratomaterial);
	}

}
