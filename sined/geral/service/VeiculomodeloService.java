package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.dao.VeiculomodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculomodeloService extends GenericService<Veiculomodelo> {

	private VeiculomodeloDAO veiculomodeloDAO;
	
	public void setVeiculomodeloDAO(VeiculomodeloDAO veiculomodeloDAO) {
		this.veiculomodeloDAO = veiculomodeloDAO;
	}

	public List<Veiculomodelo> findByTipocontrole(Tipocontroleveiculo tipocontroleveiculo) {
		return veiculomodeloDAO.findByTipocontrole(tipocontroleveiculo);
	}
	
	public Veiculomodelo loadWithCategoria(Veiculomodelo veiculomodelo) {
		return veiculomodeloDAO.loadWithCategoria(veiculomodelo);
	}
}
