package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.dao.VeiculousotipoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculousotipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculousotipoService extends GenericService<Veiculousotipo> {

	private VeiculousotipoDAO veiculousotipoDAO;
	
	public void setVeiculousotipoDAO(VeiculousotipoDAO veiculousotipoDAO) {
		this.veiculousotipoDAO = veiculousotipoDAO;
	}
	
	@Override
	public void saveOrUpdate(final Veiculousotipo bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(bean.getEscolta())
					veiculousotipoDAO.removeEscoltaVeiculoUsoTipo();
				if(bean.getNormal())
					veiculousotipoDAO.removeNormalVeiculoUsoTipo();
				
				saveOrUpdateNoUseTransaction(bean);
				return status;
			}
		});
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculousotipo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isVeiculoUsoTipoEscolta(Veiculousotipo veiculousotipo) {
		return veiculousotipoDAO.isVeiculoUsoTipoEscolta(veiculousotipo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculousotipo veiculoUsoTipoEscolta() {
		return veiculousotipoDAO.veiculoUsoTipoEscolta();
	}

	/**
	 * M�todo que faz listagem padr�o do flex
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculousotipo> findForListagemFlex(VeiculousotipoFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		return veiculousotipoDAO.findForListagem(filtro).list();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculousotipo veiculoUsoTipoNormal() {
		return veiculousotipoDAO.veiculoUsoTipoNormal();
	}
	/**
	 * M�todo para verificar se existe pelo menos um tipo de uso Normal
	 * 
	 * @return boolean
	 * @author Fernando Boldrini
	 */
	public Boolean findTipoUsoNormal() {
		if(veiculousotipoDAO.veiculoUsoTipoNormal() != null)
			return true;
		return false;
		
	}
}
