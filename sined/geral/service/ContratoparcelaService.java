package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.geral.dao.ContratoparcelaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratoparcelaService extends GenericService<Contratoparcela> {

	private ContratoparcelaDAO contratoparcelaDAO;
	
	public void setContratoparcelaDAO(ContratoparcelaDAO contratoparcelaDAO) {
		this.contratoparcelaDAO = contratoparcelaDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contratoparcela> findByContrato(Contrato contrato) {
		return contratoparcelaDAO.findByContrato(contrato);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contratoparcela
	 * @Author Tom�s Rabelo
	 */
	public void updateParcelaCobrada(Contrato contrato) {
		contratoparcelaDAO.updateParcelaCobrada(contrato);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateValorParcela
	 * 
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 */
	public void updateValorParcela(Contratoparcela contratoparcela) {
		contratoparcelaDAO.updateValorParcela(contratoparcela);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateParcelaCobrada
	 * 
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 */
	public void updateParcelaCobrada(Contratoparcela contratoparcela){
		contratoparcelaDAO.updateParcelaCobrada(contratoparcela);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateParcelaNaoCobrada(Contratoparcela contratoparcela)
	*
	* @param contratoparcela
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public void updateParcelaNaoCobrada(Contratoparcela contratoparcela){
		contratoparcelaDAO.updateParcelaNaoCobrada(contratoparcela);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateDtVencimento
	 * 
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 */
	public void updateDtVencimento(Contratoparcela contratoparcela){
		contratoparcelaDAO.updateDtVencimento(contratoparcela);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateRenovacao(Contratoparcela contratoparcela)
	 *
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 * @since 05/10/2012
	 */
	public void updateRenovacao(Contratoparcela contratoparcela) {
		contratoparcelaDAO.updateRenovacao(contratoparcela);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratoparcelaDAO#updateDesmarcarParcelacobrada(Contratoparcela contratoparcela, Boolean cobrada)
	 *
	 * @param contratoparcela
	 * @param cobrada
	 * @author Luiz Fernando
	 */
	public void updateDesmarcarParcelacobrada(Contratoparcela contratoparcela, Boolean cobrada) {
		contratoparcelaDAO.updateDesmarcarParcelacobrada(contratoparcela, cobrada);		
	}

}
