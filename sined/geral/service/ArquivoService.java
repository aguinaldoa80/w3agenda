package br.com.linkcom.sined.geral.service;

import java.awt.Image;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivoService extends GenericService<Arquivo> {

	protected ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public <E extends File> E loadWithContents(E bean) {
		return arquivoDAO.loadWithContents(bean);
	}

	public void fillWithContents(File bean) {
		arquivoDAO.fillWithContents(bean);
	}
	
	public void delete(Arquivo bean){
		arquivoDAO.delete(bean);
	}
	
	public Image loadAsImage(File bean) {
		return arquivoDAO.loadAsImage(bean);
	}	
	
	/* singleton */
	private static ArquivoService instance;
	public static ArquivoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivoService.class);
		}
		return instance;
	}

	public void deleteByIds(String whereIn) {
		arquivoDAO.deleteByIds(whereIn);
	}

	public void saveFile(Object bean, String filePropertyName) {
		arquivoDAO.saveFile(bean, filePropertyName);		
	}
	
	public void save(File arquivoNovo, File arquivoVelho) {
		arquivoDAO.save(arquivoNovo, arquivoVelho);		
	}
}
