package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.dao.OtrPneuTipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrPneuTipoService extends GenericService<OtrPneuTipo> {
	
	/* singleton */
	private static OtrPneuTipoService instance;
	public static OtrPneuTipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(OtrPneuTipoService.class);
		}
		return instance;
	}
	
	protected OtrPneuTipoDAO otrPneuTipoDao;

	public void setOtrPneuTipoDao(OtrPneuTipoDAO otrPneuTipoDao) {
		this.otrPneuTipoDao = otrPneuTipoDao;
	}
	
	
	public List<OtrPneuTipo> findAutocomplete(String nome){
		return otrPneuTipoDao.findAutocomplete(nome);
		
	}

}
