package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.VendaSincronizacaoFlex;
import br.com.linkcom.sined.geral.dao.VendaSincronizacaoFlexDAO;

public class VendaSincronizacaoFlexService extends GenericService<VendaSincronizacaoFlex>{
	
	protected VendaSincronizacaoFlexDAO vendaSincronizacaoFlexDAO;
	
	public void setVendaSincronizacaoFlexDAO(VendaSincronizacaoFlexDAO vendaSincronizacaoFlexDAO) {
		this.vendaSincronizacaoFlexDAO = vendaSincronizacaoFlexDAO;
	}
	
	/* singleton */
	private static VendaSincronizacaoFlexService instance;
	public static VendaSincronizacaoFlexService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendaSincronizacaoFlexService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @return
	 */
	public List<VendaSincronizacaoFlex> findForIntegracaoFlex() {
		return vendaSincronizacaoFlexDAO.findForIntegracaoFlex();
	}

	public void atualizaDtSincronizacao(VendaSincronizacaoFlex vendaFlex) {
		vendaSincronizacaoFlexDAO.atualizaDtSincronizacao(vendaFlex);	
	}
}
