package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Memorandoexportacao;
import br.com.linkcom.sined.geral.bean.Memorandoexportacaonotafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.geral.dao.MemorandoexportacaoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.ArquivoW3erpUtil;

public class MemorandoexportacaoService extends GenericService<Memorandoexportacao>{
	private MemorandoexportacaoDAO memorandoexportacaoDAO;
	private ArquivonfnotaService arquivonfnotaService;
	private EntregadocumentofreteService entregadocumentofreteService;
	

	public void setMemorandoexportacaoDAO(
			MemorandoexportacaoDAO memorandoexportacaoDAO) {
		this.memorandoexportacaoDAO = memorandoexportacaoDAO;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setEntregadocumentofreteService(
			EntregadocumentofreteService entregadocumentofreteService) {
		this.entregadocumentofreteService = entregadocumentofreteService;
	}

	public List<Memorandoexportacao> loadByNotafiscalproduto(String whereIn){
		return memorandoexportacaoDAO.loadByNotafiscalproduto(whereIn);
	}	
	
	/**
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	@SuppressWarnings("unchecked")
	public List<Memorandoexportacao> loadForMemorando(String whereIn){
		List<Memorandoexportacao> listaMemorandoexportacao = memorandoexportacaoDAO.loadForMemorando(whereIn);
		
		for(Memorandoexportacao memorando : listaMemorandoexportacao){
			for(Memorandoexportacaonotafiscalprodutoitem memorandoprodutoitem : memorando.getListaitens()){
				for(Notafiscalprodutoitementregamaterial notaentregamaterial : memorandoprodutoitem.getNotafiscalprodutoitem().getListaNotaprodutoitementregamaterial()){
					Entregadocumento entregadocumento = notaentregamaterial.getEntregamaterial().getEntregadocumento();
					List<Entregadocumentofrete> listaentregadocumentofrete = entregadocumentofreteService.findByEntregadocumentoForMemorandoexportacao(entregadocumento);
					entregadocumento.setListaEntregadocumentofrete(SinedUtil.listToSet(listaentregadocumentofrete, listaentregadocumentofrete.getClass()));					
				}
			}
		}
		return listaMemorandoexportacao;
	}
	
	public List<Memorandoexportacao> loadForDeleteMemorandoexportacao(String whereIn){
		return memorandoexportacaoDAO.loadForDeleteMemorandoexportacao(whereIn);
	}
	
	
	/**
	 * 
	 * @param filtro
	 * @return
	 * @author Andrey Leonardo
	 */
	public String createReportMatricial(List<Memorandoexportacao> listaMemorandoexportacao, Colaborador representante) {
		StringBuilder relatorioMatricial = new StringBuilder();
		int page = 1;
		for(Memorandoexportacao memorandoexportacao : listaMemorandoexportacao){
			int i;			
			Notafiscalproduto notafiscalproduto = memorandoexportacao.getNotafiscalproduto();
			Fornecedor remetente = memorandoexportacao.getRemetente();
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
			Fornecedor transportador = notafiscalproduto.getTransportador();
			
			String representanteNome = representante.getNome();
			
			String dataEmissao = memorandoexportacao.getDtemissao() != null ? SinedDateUtils.toString(memorandoexportacao.getDtemissao()) : SinedDateUtils.toString(SinedDateUtils.currentDate());
			
			String numeroMemorando = memorandoexportacao.getNumero().toString();
			
			Endereco endereco;
			//EXPORTADOR
			endereco = notafiscalproduto.getEmpresa().getEndereco();
			String empresaRazaoSocial = notafiscalproduto.getEmpresa() != null ? notafiscalproduto.getEmpresa().getRazaosocial() : "";
			String empresaEndereco =  endereco != null ? endereco.getLogradouroCompletoComBairro() : "";
			String empresaIE = notafiscalproduto.getEmpresa() != null ? notafiscalproduto.getEmpresa().getInscricaoestadual() : "";
			String empresaCnpj = notafiscalproduto.getEmpresa() != null ? notafiscalproduto.getEmpresa().getCpfCnpjProprietarioPrincipalOuEmpresa() : "";
			
			//DADOS DA EXPORTA��O
			String chaveAcessoNotaFiscalSaida = "";
			if(arquivonfnota != null && arquivonfnota.getChaveacesso() != null){
				chaveAcessoNotaFiscalSaida = arquivonfnota.getChaveacesso();
			}
			
			String chaveAcessoDocumentosReferenciados = "";
			if (notafiscalproduto.getListaReferenciada()!=null && !notafiscalproduto.getListaReferenciada().isEmpty()){
				//Neste caso 1 para 1
				chaveAcessoDocumentosReferenciados = notafiscalproduto.getListaReferenciada().get(0).getChaveacesso();				
			}
			
			String declaracaoExportacao = memorandoexportacao.getDeclaracaoexportacao();
			String registroExportacao = memorandoexportacao.getRegistroexportacao().toString();
			String conhecimentoEmbarque = memorandoexportacao.getConhecimentoembarque();
			String estadoProdutor = remetente.getEnderecoSiglaEstado() != null ? remetente.getEnderecoSiglaEstado() : "";
			String paisDestino = notafiscalproduto.getEnderecoCliente()!=null && notafiscalproduto.getEnderecoCliente().getPais() != null ? notafiscalproduto.getEnderecoCliente().getPais().getNome() : " ";
			String dtDeclaracaoExportacao = SinedDateUtils.toString(memorandoexportacao.getDtdeclaracaoexportacao(), "dd/MM/yy");
			String dtConhecimentoEmbarque = SinedDateUtils.toString(memorandoexportacao.getDtconhecimentoembarque(), "dd/MM/yy");
			String dtRegistroExportacao = SinedDateUtils.toString(memorandoexportacao.getDtregistroexportacao(), "dd/MM/yy");
			
			//PRODUTOS EXPORTADOS
			List<Memorandoexportacaonotafiscalprodutoitem> listaItensExportados = memorandoexportacao.getListaitens();
			
			//REMETENTE
			endereco = remetente.getEndereco();
			String fornecedorRazaoSocial = remetente.getRazaosocial() != null ? remetente.getRazaosocial() : "";
			String fornecedorEndereco = endereco != null ? endereco.getLogradouroCompletoComBairro() : "";
			String fornecedorIE = remetente.getInscricaoestadual() != null ? remetente.getInscricaoestadual() : "";
			String fornecedorCnpj = remetente.getCnpj().toString() != null ? remetente.getCnpj().toString() : "";
			
			//TRANSPORTADOR
			String transportadorRazaoSocial = "";
			String transportadorEndereco = "";
			String transportadorIE = "";
			String transportadorCnpj = "";
			if(transportador != null){
				endereco = notafiscalproduto.getEnderecotransportador();
				transportadorRazaoSocial = transportador.getRazaosocial() != null ? transportador.getRazaosocial() : "";
				transportadorEndereco = endereco != null ? endereco.getLogradouroCompletoComBairro() : "";
				transportadorIE = transportador.getInscricaoestadual() !=null ? transportador.getInscricaoestadual() : "";
				transportadorCnpj = transportador.getCnpj() !=null ? transportador.getCnpj().toString() : "";
			}
//			LINHA 1 - 4
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4);
			
//			LINHA 5
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 94);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, numeroMemorando, 16, ' ', false); // N�MERO DO MEMORANDO			
			
//			LINHA 5 - 7
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 3);
			
//			LINHA 8 - 10
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 19);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, empresaRazaoSocial, 100, ' ', true); // RAZ�O SOCIAL EXPORTADOR
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 17);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, empresaEndereco, 100, ' ', true); // ENDERE�O EXPORTADOR
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 20);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, empresaIE, 15, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 72);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, empresaCnpj, 20, ' ', true);
			
//			LINHA 11 - 12
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 3);
			
//			LINHA 13 - 18
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 48);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, chaveAcessoDocumentosReferenciados, 44, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 33);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, declaracaoExportacao, 20, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 36);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, dtDeclaracaoExportacao, 10, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 31);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, registroExportacao, 20, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 38);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, dtRegistroExportacao, 10, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 34);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, conhecimentoEmbarque, 20, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 35);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, dtConhecimentoEmbarque, 10, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 34);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, estadoProdutor, 2, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 34);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, paisDestino, 50, ' ', true);			
			
//			LINHA 19 - 21
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4);
			
//			LINHA 22 - 31
			i=0;
			
			for(Memorandoexportacaonotafiscalprodutoitem memorandoprodutoitem : listaItensExportados){
				if(i<=10){
					Notafiscalprodutoitem produtoitem = memorandoprodutoitem.getNotafiscalprodutoitem();				
					String qtde = produtoitem.getQtde() != null ? SinedUtil.descriptionDecimal(produtoitem.getQtde()) : "";
					String unidadeMedida = produtoitem.getUnidademedida() != null ? produtoitem.getUnidademedida().getSimbolo() : "";
					String ncm = produtoitem.getNcmcompleto() != null ? produtoitem.getNcmcompleto() : "";
					String descricao = produtoitem.getMaterial() != null ? produtoitem.getMaterial().getNome() : "";
					String valorUnitario = produtoitem.getValorunitario() != null ? SinedUtil.descriptionDecimal(produtoitem.getValorunitario(), true) : "";
					String valorTotal = produtoitem.getValorbruto() != null ? produtoitem.getValorbruto().toString() : "";
					
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, qtde, 9, ' ', true);
					
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, unidadeMedida, 7, ' ', true);
					
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, ncm, 22, ' ', true);
					
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 2);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, descricao, 53, ' ', true);
									
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, valorUnitario, 13, ' ', false);
									
					ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
					ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, valorTotal, 12, ' ', false);
					ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
					i++;
				}else{
					break;
				}
			}
			if(i<=10){
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 11-i);
			}
			
//			LINHA 32 - 34
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
			
//			LINHA 35 - 37
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 19);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, fornecedorRazaoSocial, 100, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 17);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, fornecedorEndereco, 100, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 20);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, fornecedorIE, 15, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 55);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, fornecedorCnpj, 20, ' ', true);			
			
//			LINHA 38 - 39
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 3);
			
//			LINHA 40 - 44
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 53);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, chaveAcessoNotaFiscalSaida, 44, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 5);
			
//			LINHA 45 - 49
			i=0;
			List<Entregamaterial> listaentregamaterial = new ArrayList<Entregamaterial>();
			for(Memorandoexportacaonotafiscalprodutoitem memorandoprodutoitem : listaItensExportados){
				for(Notafiscalprodutoitementregamaterial notaitementregamaterial : 
					memorandoprodutoitem.getNotafiscalprodutoitem().getListaNotaprodutoitementregamaterial()){					
					if(i<=4 && !listaentregamaterial.contains(notaitementregamaterial.getEntregamaterial())){
						Entregamaterial entregamaterial = notaitementregamaterial.getEntregamaterial();
						String notaFiscal = entregamaterial.getEntregadocumento() != null ? entregamaterial.getEntregadocumento().getNumero() : "";
						String modelo = entregamaterial.getEntregadocumento().getModelodocumentofiscal() != null ? entregamaterial.getEntregadocumento().getModelodocumentofiscal().getCodigo() : "";
						String serie = entregamaterial.getEntregadocumento().getSerie() != null ? entregamaterial.getEntregadocumento().getSerie() : "";
						String data = SinedDateUtils.toString(entregamaterial.getEntregadocumento().getDtemissao(), "dd/MM/yy");
						String qtde = SinedUtil.descriptionDecimal(entregamaterial.getQtde());
						String unidadeMedida = entregamaterial.getMaterial().getUnidademedida() != null ? entregamaterial.getMaterial().getUnidademedida().getSimbolo() : "";
						String ncm = entregamaterial.getMaterial() != null ? entregamaterial.getMaterial().getNcmcompleto(): "";
						String descricao = entregamaterial.getMaterial() != null ? entregamaterial.getMaterial().getNome() : "";
											
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 6);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, notaFiscal, 6, '0', false);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 11);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, modelo, 1, ' ', true);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 9);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, serie, 5, ' ', true);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, data, 11, ' ', false);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 3);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, qtde, 7, ' ', false);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, unidadeMedida, 2, ' ', true);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, ncm, 10, ' ', true);
						
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, descricao, 45, ' ', true);
						ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
						i++;
						listaentregamaterial.add(entregamaterial);
					}else{
						break;
					}
				}
			}			
			if(i<=4){
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 5-i);
			}
			
//			LINHA 50 - 51
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
			
//			LINHA 52 - 55
			i = 0;
			List<Entregadocumento> listadocumentofrete = new ArrayList<Entregadocumento>();
			for(Memorandoexportacaonotafiscalprodutoitem memorandoprodutoitem : listaItensExportados){
				for(Notafiscalprodutoitementregamaterial notaitementregamaterial : 
					memorandoprodutoitem.getNotafiscalprodutoitem().getListaNotaprodutoitementregamaterial()){
					Entregadocumento entregadocumento = notaitementregamaterial.getEntregamaterial().getEntregadocumento();
					for(Entregadocumentofrete entregadocumentofrete : entregadocumento.getListaEntregadocumentofrete()){
						if(i<=3 && !listadocumentofrete.contains(entregadocumentofrete.getEntregadocumentovinculo())){
							Entregadocumento conhecimentotransporte = entregadocumentofrete.getEntregadocumentovinculo();
							
							String numero = conhecimentotransporte.getNumero() != null ? conhecimentotransporte.getNumero() : "";
							String modelo = conhecimentotransporte.getModelodocumentofiscal() != null ? conhecimentotransporte.getModelodocumentofiscal().getCodigo() : "";
							String serie = conhecimentotransporte.getSerie() != null ? conhecimentotransporte.getSerie() : "";
							String data = SinedDateUtils.toString(conhecimentotransporte.getDtemissao(), "dd/MM/yyyy");
							
							ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 17);
							ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, numero, 22, ' ', true);
							
							ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 19);
							ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, modelo, 2, ' ', true);
							
							ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 30);
							ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, serie, 3, ' ', true);
							
							ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 30);
							ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, data, 12, ' ', true);
							ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
							i++;
							listadocumentofrete.add(conhecimentotransporte);
						}else{
							break;
						}
							
					}
				}				
			}
			if(i<=3){
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4-i);
			}
			
//			LINHA 56 - 57
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
			
//			LINHA 58 - 60
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 19);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, transportadorRazaoSocial, 100, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 17);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, transportadorEndereco, 100, ' ', true);
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 20);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, transportadorIE, 15, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 55);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, transportadorCnpj, 20, ' ', true);
			
//			LINHA 61 - 63
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 3);
			
//			LINHA 64
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 13);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, representanteNome, 47, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 20);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, dataEmissao, 11, ' ', true);
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 46);
			if(page < listaMemorandoexportacao.size()){
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4);
			}
			page++;
		}
			
		return relatorioMatricial.toString();		
	}	
	
	/**
	 * @param notafiscalproduto
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isExisteMemorandoexportacao(Notafiscalproduto notafiscalproduto){
		return memorandoexportacaoDAO.isExisteMemorandoexportacao(notafiscalproduto);
	}
}
