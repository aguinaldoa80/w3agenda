package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.PropostaCaixaItem;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.dao.PropostaCaixaItemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PropostaCaixaItemService extends GenericService<PropostaCaixaItem>{
	
	private PropostaCaixaItemDAO propostacaixaItemDAO;
	
	public void setPropostacaixaItemDAO(
			PropostaCaixaItemDAO propostacaixaItemDAO) {
		this.propostacaixaItemDAO = propostacaixaItemDAO;
	}
	
	/**M�todo com refer�ncia na classe DAO.
	 * @author Thiago Augusto
	 * @param item
	 * @return
	 */
	public List<PropostaCaixaItem> findByItem(Propostacaixa item){
		return propostacaixaItemDAO.findByItem(item);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @param bean
	 * @return
	 */
	public List<PropostaCaixaItem> findListByPropostaCaixa(Propostacaixa bean){
		return propostacaixaItemDAO.findListByPropostaCaixa(bean);
	}
}
