package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.geral.dao.DocumentoconfirmacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoconfirmacaoService extends GenericService<Documentoconfirmacao> {
	
	private DocumentoconfirmacaoDAO documentoconfirmacaoDAO;
	
	public void setDocumentoconfirmacaoDAO(DocumentoconfirmacaoDAO documentoconfirmacaoDAO) {this.documentoconfirmacaoDAO = documentoconfirmacaoDAO;}
	
	public List<Documentoconfirmacao> findByDocumentos(List<Documento> listaDocumento){
		return documentoconfirmacaoDAO.findByDocumentos(listaDocumento);
	}
	
}
