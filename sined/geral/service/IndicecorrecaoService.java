package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Indicecorrecaovalor;
import br.com.linkcom.sined.geral.dao.IndicecorrecaoDAO;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.IndicecorrecaoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.indicecorrecao.IndicecorrecaoRESTModel;

public class IndicecorrecaoService extends GenericService<Indicecorrecao> {

	private IndicecorrecaoDAO indicecorrecaoDAO;
	
	public void setIndicecorrecaoDAO(IndicecorrecaoDAO indicecorrecaoDAO) {
		this.indicecorrecaoDAO = indicecorrecaoDAO;
	}
	

	/* singleton */
	private static IndicecorrecaoService instance;
	public static IndicecorrecaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(IndicecorrecaoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que calcula o �ndice de corre��o dado um valor uma dt de vencimento e dt referencia
	 * 
	 * @see br.com.linkcom.sined.geral.service.IndicecorrecaovalorService#findByIndiceCorrecao(Indicecorrecao)
	 * @param indicecorrecao
	 * @param valor
	 * @param dtvencimento
	 * @param dtReferencia
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Money calculaIndiceCorrecao(Indicecorrecao indicecorrecao, Money valor, Date dtvencimento, Date dtReferencia) {
		if(indicecorrecao != null && indicecorrecao.getCdindicecorrecao() != null){
			indicecorrecao = carregaIndiceCorrecao(indicecorrecao);
			//Se for true = acumulado, false = mensal
			if(indicecorrecao.getTipocalculo()){
				return calculaValorAcumulado(indicecorrecao, valor, dtvencimento, dtReferencia);
			}else{
				return calculaValorMensal(indicecorrecao, valor, dtvencimento);
			}
		}
		return valor;
	}
	
	/**
	 * M�todo que calcula o �ndice de corre��o para an�lise de compra
	 *
	 * @param indicecorrecao
	 * @param valor
	 * @param dtvencimento
	 * @param dtReferencia
	 * @return
	 * @author Luiz Fernando
	 */
	public Money calculaIndiceCorrecaoAnalisecompra(Indicecorrecao indicecorrecao, Money valor, Date dtvencimento, Date dtReferencia) {
		if(indicecorrecao != null){
			//Se for true = acumulado, false = mensal
			if(indicecorrecao.getTipocalculo()){
				return calculaValorAcumulado(indicecorrecao, valor, dtvencimento, dtReferencia);
			}else{
				return calculaValorMensal(indicecorrecao, valor, dtvencimento);
			}
		}
		return valor;
	}
	
	/**
	 * M�todo com ref�rencia no DAO
	 * 
	 * @param indicecorrecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Indicecorrecao carregaIndiceCorrecao(Indicecorrecao indicecorrecao) {
		return indicecorrecaoDAO.carregaIndiceCorrecao(indicecorrecao);
	}
	
	/**
	 * M�todo que calcula o valor de acordo com o tipo de c�lculo mensal
	 * 
	 * @param indicecorrecao
	 * @param valorAux
	 * @param dtvencimento
	 * @author Tom�s Rabelo
	 */
	private Money calculaValorMensal(Indicecorrecao indicecorrecao, Money valorAux, Date dtvencimento) {
		Calendar dtVencimentoAux = Calendar.getInstance();
		dtVencimentoAux.setTimeInMillis(dtvencimento.getTime());
		
		for (Indicecorrecaovalor indicecorrecaovalor : indicecorrecao.getListaIndicecorrecaovalor()) {
			Calendar dataIndiceCorrecaoValor = Calendar.getInstance();
			dataIndiceCorrecaoValor.setTimeInMillis(indicecorrecaovalor.getMesano().getTime());
			
			if(dtVencimentoAux.get(Calendar.MONTH) == dataIndiceCorrecaoValor.get(Calendar.MONTH) && 
			   dtVencimentoAux.get(Calendar.YEAR) == dataIndiceCorrecaoValor.get(Calendar.YEAR)){
				valorAux = valorAux.add(new Money(valorAux.getValue().doubleValue() * (indicecorrecaovalor.getPercentual() / 100)));
				break;
			}
		}
		return valorAux;
	}
	
	/**
	 * M�todo que calcula o valor de acordo com o tipo de c�lculo acumulado
	 * 
	 * ANTE��O: Altera��es devem ser realizadas tamb�m no seguinte m�todo do W3Cliente: PendenciaFinanceiraProcess.calculaValorAcumulado
	 * 
	 * @param indicecorrecao
	 * @param valorAux
	 * @param dtvencimento
	 * @param dtReferencia
	 * @author Tom�s Rabelo
	 */
	private Money calculaValorAcumulado(Indicecorrecao indicecorrecao, Money valorAux, Date dtvencimento, Date dtReferencia) {
		
		Calendar dtVencimentoAux = Calendar.getInstance();
		Calendar dtReferenciaAux = Calendar.getInstance();
		
		dtVencimentoAux.setTimeInMillis(dtvencimento.getTime());
		dtVencimentoAux.set(Calendar.DAY_OF_MONTH, 1);
		
		dtReferenciaAux.setTimeInMillis(dtReferencia.getTime());
		dtReferenciaAux.set(Calendar.DAY_OF_MONTH, 1);
		
		int meses = SinedDateUtils.mesesEntre(dtReferenciaAux, dtVencimentoAux);
		if(meses > 0){
			for (int i = 0; i < meses; i++) {
				for (Indicecorrecaovalor indicecorrecaovalor : indicecorrecao.getListaIndicecorrecaovalor()) {
					Calendar dataIndiceCorrecaoValor = Calendar.getInstance();
					dataIndiceCorrecaoValor.setTimeInMillis(indicecorrecaovalor.getMesano().getTime());
					
					if(dtReferenciaAux.get(Calendar.MONTH) == dataIndiceCorrecaoValor.get(Calendar.MONTH) && 
					   dtReferenciaAux.get(Calendar.YEAR) == dataIndiceCorrecaoValor.get(Calendar.YEAR)){
						valorAux = valorAux.add(new Money(valorAux.getValue().doubleValue() * (indicecorrecaovalor.getPercentual() / 100)));
						break;
					}
				}
				dtReferenciaAux.add(Calendar.MONTH, 1);
			}
		}
		return valorAux;
	}

	public List<IndicecorrecaoOfflineJSON> findForPVOffline() {
		List<IndicecorrecaoOfflineJSON> lista = new ArrayList<IndicecorrecaoOfflineJSON>();
		for(Indicecorrecao ic : indicecorrecaoDAO.findForPVOffline())
			lista.add(new IndicecorrecaoOfflineJSON(ic));

		return lista;
	}
	
	public List<IndicecorrecaoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<IndicecorrecaoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<IndicecorrecaoRESTModel> lista = new ArrayList<IndicecorrecaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Indicecorrecao bean : indicecorrecaoDAO.findForAndroid(whereIn))
				lista.add(new IndicecorrecaoRESTModel(bean));
		}
		
		return lista;
	}
	
	public List<Indicecorrecao> findForVenda() {
		return indicecorrecaoDAO.findForVenda();
	}
	
	public List<GenericBean> findForWSVenda() {
		return ObjectUtils.translateEntityListToGenericBeanList(this.findForVenda());
	}
}
