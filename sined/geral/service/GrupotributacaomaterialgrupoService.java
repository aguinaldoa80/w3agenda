package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaomaterialgrupo;
import br.com.linkcom.sined.geral.dao.GrupotributacaomaterialgrupoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaomaterialgrupoService extends GenericService<Grupotributacaomaterialgrupo>{

	private GrupotributacaomaterialgrupoDAO grupotributacaomaterialgrupoDAO;
	
	public void setGrupotributacaomaterialgrupoDAO(GrupotributacaomaterialgrupoDAO grupotributacaomaterialgrupoDAO) {
		this.grupotributacaomaterialgrupoDAO = grupotributacaomaterialgrupoDAO;
	}

	public List<Grupotributacaomaterialgrupo> findByGrupotributacao(Grupotributacao grupotributacao){
		return grupotributacaomaterialgrupoDAO.findByGrupotributacao(grupotributacao);
	}
}
