package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentosincronizacao;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamaterialsincronizacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Materialdevolucaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialDevolucaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialdevolucaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.MaterialdevolucaoItemReportBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.MaterialdevolucaoQueryBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.MaterialdevolucaoReportBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

public class MaterialdevolucaoService extends GenericService<Materialdevolucao> {

	private MaterialdevolucaoDAO materialdevolucaoDAO;
	private EntregadocumentosincronizacaoService entregadocumentosincronizacaoService;
	private VendaService vendaService;
	private EmpresaService empresaService;
	private MaterialdevolucaohistoricoService materialdevolucaohistoricoService; 
	private EntradafiscalService entradafiscalService;
	private ParametrogeralService parametroGeralService;
	
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {this.parametroGeralService = parametroGeralService;}
	public void setMaterialdevolucaoDAO(MaterialdevolucaoDAO materialdevolucaoDAO) {
		this.materialdevolucaoDAO = materialdevolucaoDAO;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setEntregadocumentosincronizacaoService(EntregadocumentosincronizacaoService entregadocumentosincronizacaoService) {
		this.entregadocumentosincronizacaoService = entregadocumentosincronizacaoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMaterialdevolucaohistoricoService(MaterialdevolucaohistoricoService materialdevolucaohistoricoService) {
		this.materialdevolucaohistoricoService = materialdevolucaohistoricoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	
	/**
	 * M�todo que cria um Materialdevolucao
	 *
	 * @param bean
	 * @param itemBean
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialdevolucao criarMaterialdevolucao(VendaDevolucaoBean bean, VendamaterialDevolucaoBean itemBean, Movimentacaoestoque movimentacaoestoque) {
		Materialdevolucao materialdevolucao = new Materialdevolucao();
		
		if(parametroGeralService.getBoolean(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE)){
			
		}
		
		materialdevolucao.setMaterial(itemBean.getMaterial());
		materialdevolucao.setVenda(bean.getVenda());
		materialdevolucao.setPedidovenda(bean.getPedidovenda());
		materialdevolucao.setVendamaterial(itemBean.getVendamaterial());
		materialdevolucao.setPedidovendamaterial(itemBean.getPedidovendamaterial());
		materialdevolucao.setPreco(itemBean.getPreco());
		materialdevolucao.setQtdevendida(itemBean.getQuantidade());
		materialdevolucao.setUnidademedidavenda(itemBean.getUnidademedida());
		materialdevolucao.setUnidademedida(itemBean.getUnidademedidaDevolucao());
		materialdevolucao.setQtdedevolvida(itemBean.getQtdedevolvida());
	
		materialdevolucao.setPlacaveiculo(bean.getPlacaveiculo());
		materialdevolucao.setNumeronotadevolucao(bean.getNumero());
		materialdevolucao.setLocalarmazenagem(bean.getLocalarmazenagem());
		
		List<Materialdevolucaohistorico> listaMaterialdevolucaohistorico = new ArrayList<Materialdevolucaohistorico>();
		
		Materialdevolucaohistorico materialdevolucaohistorico = new Materialdevolucaohistorico();
		materialdevolucaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		materialdevolucaohistorico.setUsuario(SinedUtil.getUsuarioLogado());
		materialdevolucaohistorico.setAcao("Devolu��o");
		
		if(movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
			materialdevolucao.setMovimentacaoestoque(movimentacaoestoque);
			materialdevolucaohistorico.setObservacao("Entrada no estoque <br><a href=/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" 
					+ movimentacaoestoque.getCdmovimentacaoestoque() 
					+ ">"
					+ movimentacaoestoque.getCdmovimentacaoestoque() + "</a>");
		}
		
		listaMaterialdevolucaohistorico.add(materialdevolucaohistorico);		
		materialdevolucao.setListaMaterialdevolucaohistorico(listaMaterialdevolucaohistorico);
		
		return materialdevolucao;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#getQtdeJaDevolvida(Venda venda, Material material)
	 *
	 * @param venda
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQtdeJaDevolvida(Venda venda, Material material, Vendamaterial vendamaterial) {
		return getQtdeJaDevolvida(venda, material, vendamaterial, true);
	}
	
	public Double getQtdeJaDevolvidaByPedidoVenda(Pedidovendamaterial pedidovendamaterial) {
		return materialdevolucaoDAO.getQtdeJaDevolvida(null, null, null, pedidovendamaterial, true);
	}
	
	public Double getQtdeJaDevolvida(Venda venda, Material material, Vendamaterial vendamaterial, boolean converterParaUnidadePrincipal) {
		return materialdevolucaoDAO.getQtdeJaDevolvida(venda, material, vendamaterial, null, converterParaUnidadePrincipal);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#findWithQtdeJaDevolvida(Venda venda, Material material, Vendamaterial vendamaterial, boolean converterParaUnidadePrincipal)
	*
	* @param venda
	* @return
	* @since 02/12/2016
	* @author Luiz Fernando
	*/
	public List<Materialdevolucao> findWithQtdeJaDevolvida(Venda venda, boolean converterParaUnidadePrincipal, String whereInCdvenda) {
		return materialdevolucaoDAO.findWithQtdeJaDevolvida(venda, null, null, converterParaUnidadePrincipal, whereInCdvenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#getQtdeJaDevolvida(Pedidovenda pedidovenda, Material material, Pedidovendamaterial pedidovendamaterial)
	*
	* @param pedidovenda
	* @param material
	* @param pedidovendamaterial
	* @return
	* @since 12/02/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeJaDevolvida(Pedidovenda pedidovenda, Material material, Pedidovendamaterial pedidovendamaterial) {
		return materialdevolucaoDAO.getQtdeJaDevolvida(pedidovenda, material, pedidovendamaterial);
	}
	
	public boolean existeDevolucaoUnidadeDiferente(Venda venda,	Material material, Vendamaterial vendamaterial,	Unidademedida unidademedida) {
		return materialdevolucaoDAO.existeDevolucaoUnidadeDiferente(venda, material, vendamaterial, unidademedida);
	}
	
	public boolean existeDevolucaoUnidadeDiferente(Pedidovenda pedidovenda, Material material, Pedidovendamaterial pedidovendamaterial,	Unidademedida unidademedida) {
		return materialdevolucaoDAO.existeDevolucaoUnidadeDiferente(pedidovenda, material, pedidovendamaterial, unidademedida);
	}

	/**
	 * M�todo para lan�ar a devolu��o ap�s a confirma��o do wms
	 *
	 * @param cdsincronizacao
	 * @author Luiz Fernando
	 */
	public void lancarDevolucaoComConfirmacaoWms(Integer cdsincronizacao) {
		Entregadocumentosincronizacao eds = entregadocumentosincronizacaoService.findForDevolucaoMaterial(cdsincronizacao);
		if(eds != null && eds.getListaEntregamaterialsincronizacao() != null && 
				!eds.getListaEntregamaterialsincronizacao().isEmpty()){
			Venda venda = eds.getVenda();
			Pedidovenda pedidovenda = eds.getPedidovenda();
			VendaDevolucaoBean vendaDevolucaoBean = venda != null ? new VendaDevolucaoBean(venda) : new VendaDevolucaoBean(pedidovenda);
			VendamaterialDevolucaoBean vendamaterialDevolucaoBean;
			
			for(Entregamaterialsincronizacao entregamaterialsincronizacao : eds.getListaEntregamaterialsincronizacao()){
				vendamaterialDevolucaoBean = new VendamaterialDevolucaoBean();
				
				vendamaterialDevolucaoBean.setMaterial(entregamaterialsincronizacao.getMaterial());
				if(entregamaterialsincronizacao.getMaterialdevolucao() != null){ 
					if(entregamaterialsincronizacao.getMaterialdevolucao().getQtdedevolvida() != null){
						vendamaterialDevolucaoBean.setQtdedevolvida(entregamaterialsincronizacao.getMaterialdevolucao().getQtdedevolvida());
					}
					if(entregamaterialsincronizacao.getMaterialdevolucao().getVendamaterial() != null &&
							entregamaterialsincronizacao.getMaterialdevolucao().getVendamaterial().getLoteestoque() != null){
						vendamaterialDevolucaoBean.setLoteestoque(entregamaterialsincronizacao.getMaterialdevolucao().getVendamaterial().getLoteestoque());
					}
				}
				if(venda != null){ 
					if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
						for(Vendamaterial vm : venda.getListavendamaterial()){
							if(vm.getMaterial() != null && entregamaterialsincronizacao.getMaterial() != null && 
									entregamaterialsincronizacao.getMaterial().equals(vm.getMaterial())){
								vendamaterialDevolucaoBean.setQuantidade(vm.getQuantidade());
								break;
							}
						}
					}
					venda.setPlacaveiculo(eds.getNotaEntradaPlacaVeiculo());
				}
				vendamaterialDevolucaoBean.setPreco(entregamaterialsincronizacao.getEntregamaterialValorUnitario());
				Movimentacaoestoque movimentacaoestoque = vendaService.lancarDevolucaoEstoque(vendaDevolucaoBean, vendamaterialDevolucaoBean);
				
				if(entregamaterialsincronizacao.getMaterialdevolucao() != null && 
						entregamaterialsincronizacao.getMaterialdevolucao().getCdmaterialdevolucao() != null && 
						movimentacaoestoque != null && 
						movimentacaoestoque.getCdmovimentacaoestoque() != null){
					Materialdevolucaohistorico materialdevolucaohistorico = new Materialdevolucaohistorico();
					materialdevolucaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					materialdevolucaohistorico.setUsuario(SinedUtil.getUsuarioLogado());
					materialdevolucaohistorico.setAcao("Devolu��o");
					materialdevolucaohistorico.setMaterialdevolucao(entregamaterialsincronizacao.getMaterialdevolucao());
					if(movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
						materialdevolucaohistorico.setObservacao("Entrada no estoque <br><a href=/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" 
								+ movimentacaoestoque.getCdmovimentacaoestoque() 
								+ ">"
								+ movimentacaoestoque.getCdmovimentacaoestoque() + "</a>");
					}
					materialdevolucaohistoricoService.saveOrUpdate(materialdevolucaohistorico);
				}
				entregadocumentosincronizacaoService.updateEntregadocumentosincronizacaoDevolucaoefetuada(eds);
			}
		}
	}
	
	/**
	 * M�todo para gerar comprovante de devolu��o de material
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @throws Exception 
	 */
	public Resource gerarComprovante(String whereIn) throws Exception {
		
		MergeReport mergeReport = new MergeReport("comprovante_devolucao_material_" + SinedUtil.datePatternForReport() + ".pdf");
		
		List<MaterialdevolucaoQueryBean> listaQueryBean = this.findForComprovantedevolucao(whereIn);
		List<MaterialdevolucaoReportBean> lista = this.agrupaBeanPorVenda(listaQueryBean);
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Empresa empresaPrincipal = empresaService.loadArquivoPrincipal();
		
		for (MaterialdevolucaoReportBean itReport : lista) {
			Empresa empresa = itReport.getCdempresa() != null ? empresaService.loadComArquivo(new Empresa(itReport.getCdempresa())) : null;
			
			Report report = new Report("/faturamento/comprovantedevolucao");
			
			report.setDataSource(itReport.getListaItens());
			
			report.addParameter("CLIENTE_NOME", itReport.getCliente_nome());
			report.addParameter("VENDA_NUMERO", itReport.getVenda_numero());
			report.addParameter("USUARIO_NOME", usuarioLogado.getNome());
			report.addParameter("DATA_DEVOLUCAO", itReport.getData_devolucao());
			report.addParameter("VALOR_TOTAL_DEVOLVIDO", itReport.getValor_total_devolvido());
			report.addParameter("TITULO", "COMPROVANTE DE DEVOLU��O DE MATERIAL");
			report.addParameter("LOGO", SinedUtil.getLogo(empresa != null ? empresa : empresaPrincipal));
			report.addParameter("EMPRESA", empresa != null ? empresa.getNomefantasia() : empresaPrincipal.getNomefantasia());
			report.addParameter("USUARIO", usuarioLogado.getNome());
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			mergeReport.addReport(report);
		}
		
		
		
		
//		Venda venda = vendaService.findForComprovantedevolucao(filtro.getCdvenda());
//		List<Materialdevolucao> listaMaterialdevolucao = findForComprovantedevolucao(filtro.getWhereIn());
//		
//		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
//		Vendamaterial vendamaterial;
//		Double totaldevolvido = 0.0;
//		
//		if(listaMaterialdevolucao != null && !listaMaterialdevolucao.isEmpty()){
//			for(Materialdevolucao materialdevolucao : listaMaterialdevolucao){
//				totaldevolvido += (materialdevolucao.getQtdedevolvida() * materialdevolucao.getPreco());
//				
//				vendamaterial = new Vendamaterial();
//				if(materialdevolucao.getMaterial() != null){
//					vendamaterial.setMaterial(materialdevolucao.getMaterial().copiaMaterial());
//					if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getNome() != null && materialdevolucao.getVendamaterial() != null){
//						vendamaterial.getMaterial().setNome(vendamaterial.getMaterial().getNome() + " " + vendaService.getAlturaLarguraComprimentoConcatenados(materialdevolucao.getVendamaterial(), vendamaterial.getMaterial()));
//					}
//				}
//				vendamaterial.setQtdedevolvida(materialdevolucao.getQtdedevolvida());
//				vendamaterial.setPreco(materialdevolucao.getPreco());
//				listaVendamaterial.add(vendamaterial);
//			}
//		}
//		
		return mergeReport.generateResource();
	}
	
	private List<MaterialdevolucaoReportBean> agrupaBeanPorVenda(List<MaterialdevolucaoQueryBean> listaQueryBean) {
		List<MaterialdevolucaoReportBean> lista = new ArrayList<MaterialdevolucaoReportBean>();
		for (MaterialdevolucaoQueryBean itQuery : listaQueryBean) {
			MaterialdevolucaoItemReportBean itBean = new MaterialdevolucaoItemReportBean(itQuery);
			
			boolean achou = false;
			for (MaterialdevolucaoReportBean itReport : lista) {
				boolean vendaIgual = (itReport.getVenda_numero() == null && itQuery.getVenda_numero() == null) || 
										(itReport.getVenda_numero() != null && itQuery.getVenda_numero() != null && itReport.getVenda_numero().equals(itQuery.getVenda_numero()));
				if(vendaIgual){
					achou = true;
					itReport.getListaItens().add(itBean);
				}
			}
			
			if(!achou){
				lista.add(new MaterialdevolucaoReportBean(itQuery, itBean));
			}
		}
		return lista;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#findForComprovantedevolucao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<MaterialdevolucaoQueryBean> findForComprovantedevolucao(String whereIn) {		
		return materialdevolucaoDAO.findForComprovantedevolucao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @since 19/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Materialdevolucao> findForEntrega(String whereIn) {
		return materialdevolucaoDAO.findForEntrega(whereIn);
	}
	
	
	/**
	 * Preeche o recebimento a partir das devolu��es
	 *
	 * @param listaDevolucao
	 * @return
	 * @since 19/09/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Entregadocumento preecheEntregadocumento(List<Materialdevolucao> listaDevolucao) {
		Entregadocumento entregadocumento = new Entregadocumento();
		entregadocumento.setSimplesremessa(true);
		
		List<Entregamaterial> listaEntregamaterial = new ArrayList<Entregamaterial>();
		Entregamaterial entregamaterial;
		
		Integer controle = 0;
		for (Materialdevolucao materialdevolucao : listaDevolucao) {
			entregamaterial = new Entregamaterial();
			entradafiscalService.setIdentificadorinterno(entregamaterial, controle);
			controle++;
			
			entregamaterial.setMaterial(materialdevolucao.getMaterial());
			entregamaterial.setValorunitario(materialdevolucao.getPreco());
			entregamaterial.setQtde(materialdevolucao.getQtdedevolvida());
			entregamaterial.setLocalarmazenagem(materialdevolucao.getLocalarmazenagem());
			entregamaterial.setUnidademedidacomercial(materialdevolucao.getUnidademedida());
			if(materialdevolucao.getVendamaterial() != null && materialdevolucao.getVendamaterial().getLoteestoque() != null){
				entregamaterial.setLoteestoque(materialdevolucao.getVendamaterial().getLoteestoque());
			}else if(materialdevolucao.getPedidovendamaterial() != null && materialdevolucao.getPedidovendamaterial().getLoteestoque() != null){
				entregamaterial.setLoteestoque(materialdevolucao.getPedidovendamaterial().getLoteestoque());
			}
			entregadocumento.setNumero(materialdevolucao.getNumeronotadevolucao());
			
			listaEntregamaterial.add(entregamaterial);
		}
		
		entregadocumento.setListaEntregamaterial(SinedUtil.listToSet(listaEntregamaterial, Entregamaterial.class));
		
		return entregadocumento;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#existDevolucaoByVenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existDevolucaoByVenda(Venda venda) {
		return materialdevolucaoDAO.existDevolucaoByVenda(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#findForCsv(MaterialdevolucaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialdevolucao> findForCsv(MaterialdevolucaoFiltro filtro) {
		return materialdevolucaoDAO.findForCsv(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#getTotalQtdeDevolvidaListagem(MaterialdevolucaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getTotalQtdeDevolvidaListagem(MaterialdevolucaoFiltro filtro){
		return materialdevolucaoDAO.getTotalQtdeDevolvidaListagem(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialdevolucaoDAO#getValortotalDevolvidaListagemVenda(String whereInVenda)
	 *
	 * @param whereInVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValortotalDevolvidaListagemVenda(String whereInVenda) {
		return materialdevolucaoDAO.getValortotalDevolvidaListagemVenda(whereInVenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param vendaWhereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/01/2015
	 */
	public List<Materialdevolucao> getListaMaterialdevolucaoByVendaSemConversaoUnidadeprincipal(String vendaWhereIn) {
		return materialdevolucaoDAO.getListaMaterialdevolucaoByVendaSemConversaoUnidadeprincipal(vendaWhereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.service.MaterialdevolucaoService#updateDtcancelamento(String whereIn)
	*
	* @param whereIn
	* @since 20/04/2016
	* @author Luiz Fernando
	*/
	public void updateDtcancelamento(String whereIn) {
		materialdevolucaoDAO.updateDtcancelamento(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialdevolucaoService#findForCancelamento(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 20/04/2016
	* @author Luiz Fernando
	*/
	public List<Materialdevolucao> findForCancelamento(String whereIn) {
		return materialdevolucaoDAO.findForCancelamento(whereIn);
	}
	
	public List<Materialdevolucao> findForCancelamento(Material material, Venda venda, Vendamaterial vendamaterial) {
		return materialdevolucaoDAO.findForCancelamento(material, venda, vendamaterial);
	}
}
