package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.dao.MateriallegendaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MateriallegendaService extends GenericService<Materiallegenda>{

	private MateriallegendaDAO materiallegendaDAO;
	
	public void setMateriallegendaDAO(MateriallegendaDAO materiallegendaDAO) {
		this.materiallegendaDAO = materiallegendaDAO;
	}
	
	/* singleton */
	private static MateriallegendaService instance;
	public static MateriallegendaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MateriallegendaService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/02/2014
	 */
	public List<Materiallegenda> findByMaterial(String whereInMaterial) {
		return materiallegendaDAO.findByMaterial(whereInMaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param ordem
	 * @param cdmateriallegenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/04/2014
	 */
	public boolean haveOrdem(Integer ordem, Integer cdmateriallegenda) {
		return materiallegendaDAO.haveOrdem(ordem, cdmateriallegenda);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInMaterial
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Materiallegenda> findByMaterialtipo(String whereInMaterialTipo){
		return materiallegendaDAO.findByMaterialtipo(whereInMaterialTipo);
	}
}
