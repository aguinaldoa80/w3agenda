package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoautorizacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoautorizacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoautorizacaoService extends GenericService<Notafiscalprodutoautorizacao>{

	private NotafiscalprodutoautorizacaoDAO notafiscalprodutoautorizacaoDAO;
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setNotafiscalprodutoautorizacaoDAO(
			NotafiscalprodutoautorizacaoDAO notafiscalprodutoautorizacaoDAO) {
		this.notafiscalprodutoautorizacaoDAO = notafiscalprodutoautorizacaoDAO;
	}
	
	public List<Notafiscalprodutoautorizacao> findByNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoautorizacaoDAO.findByNotafiscalproduto(notafiscalproduto);
	}

	public void preencheAbaAutorizacaoByParametro(Notafiscalproduto notafiscalproduto) {
		List<Notafiscalprodutoautorizacao> listaNotafiscalprodutoautorizacao = new ListSet<Notafiscalprodutoautorizacao>(Notafiscalprodutoautorizacao.class);
		String parametro = parametrogeralService.buscaValorPorNome(Parametrogeral.NOTAFISCAL_AUTORIZACAO_PADRAO);
		if(parametro != null && !parametro.trim().equals("")){
			notafiscalproduto.setCadastrarautorizacaoadicional(Boolean.TRUE);
			String[] array = parametro.split(";");
			for (String cpfcnpj : array) {
				try{
					String numeros = StringUtils.soNumero(cpfcnpj);
					if(numeros != null && numeros.length() == 11){
						Notafiscalprodutoautorizacao notafiscalprodutoautorizacao = new Notafiscalprodutoautorizacao();
						notafiscalprodutoautorizacao.setTipopessoa(Tipopessoa.PESSOA_FISICA);
						notafiscalprodutoautorizacao.setCpf(new Cpf(numeros));
						listaNotafiscalprodutoautorizacao.add(notafiscalprodutoautorizacao);
					}
					if(numeros != null && numeros.length() == 14){
						Notafiscalprodutoautorizacao notafiscalprodutoautorizacao = new Notafiscalprodutoautorizacao();
						notafiscalprodutoautorizacao.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
						notafiscalprodutoautorizacao.setCnpj(new Cnpj(numeros));
						listaNotafiscalprodutoautorizacao.add(notafiscalprodutoautorizacao);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		notafiscalproduto.setListaAutorizacao(listaNotafiscalprodutoautorizacao);
	}
	
}