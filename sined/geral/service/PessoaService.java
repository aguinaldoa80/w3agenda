package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import br.com.linkcom.buscacpf.BuscaCPFAPI;
import br.com.linkcom.empresaapi.EmpresaAPI;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.PessoaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ParticipanteSpedBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.ReceitaFederalProcess;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoaService extends GenericService<Pessoa> {
	
	private PessoaDAO pessoaDAO;
	private EnderecoService enderecoService;
	
	public void setPessoaDAO(PessoaDAO pessoaDAO) {
		this.pessoaDAO = pessoaDAO;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#carregaPessoa(Pessoa)
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Pessoa carregaPessoa(Pessoa bean){
		return pessoaDAO.carregaPessoa(bean);
	}
	
	/**
	 * M�todo para realizar ajustes no bean antes de salvar.
	 * 
	 * @param pessoa
	 * @author Fl�vio Tavares
	 */
	public void ajustaPessoaToSave(Pessoa pessoa){
		
		/**
		 * Ajuste em dado bancario. Insere o dado bancario do objeto pessoa e joga dentro de uma lista.
		 */
		Set<Dadobancario> listaDadobancario = null;
		Dadobancario dadobancario = pessoa.getDadobancario();
		if(dadobancario != null && dadobancario.mainFieldsNotNull()){
			listaDadobancario = new ListSet<Dadobancario>(Dadobancario.class);
			listaDadobancario.add(dadobancario);
		}
		pessoa.setListaDadobancario(listaDadobancario);
		if(pessoa.getListaEndereco() != null && pessoa.getListaEndereco().size() > 0){
			for (Endereco item : pessoa.getListaEndereco()) {
				if(item.getBairro() != null) item.setBairro(item.getBairro().toUpperCase());
				if(item.getCaixapostal() != null) item.setCaixapostal(item.getCaixapostal().toUpperCase());
				if(item.getLogradouro() != null) item.setLogradouro(item.getLogradouro().toUpperCase());
				if(item.getComplemento() != null) item.setComplemento(item.getComplemento().toUpperCase());
				if(item.getNumero() != null) item.setNumero(item.getNumero().toUpperCase());
			}
		}
		if(pessoa.getEndereco() != null){
			if(pessoa.getEndereco().getBairro() != null)
				pessoa.getEndereco().setBairro(pessoa.getEndereco().getBairro().toUpperCase());
			if(pessoa.getEndereco().getCaixapostal() != null)
				pessoa.getEndereco().setCaixapostal(pessoa.getEndereco().getCaixapostal().toUpperCase());
			if(pessoa.getEndereco().getLogradouro() != null)
				pessoa.getEndereco().setLogradouro(pessoa.getEndereco().getLogradouro().toUpperCase());
			if(pessoa.getEndereco().getComplemento() != null)
				pessoa.getEndereco().setComplemento(pessoa.getEndereco().getComplemento().toUpperCase());
			if(pessoa.getEndereco().getNumero() != null)
				pessoa.getEndereco().setNumero(pessoa.getEndereco().getNumero().toUpperCase());
		}
	}
	
	/**
	 * M�todo para realizar ajustes no bean antes de exibir na tela.
	 * 
	 * @param pessoa
	 * @author Fl�vio Tavares
	 */
	public void ajustaPessoaToLoad(Pessoa pessoa){
		
		/**
		 * Ajuste em dado bancario. Carrega o dado bancario da lista e joga em um objeto.
		 */
		if(SinedUtil.isListNotEmpty(pessoa.getListaDadobancario())){
			Dadobancario dadobancario = pessoa.getListaDadobancario().iterator().next();
			pessoa.setDadobancario(dadobancario);
		}
		
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#carregaDadosPessoa(Pessoa)
	 * @param pessoa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Pessoa carregaDadosPessoa(Pessoa pessoa){
		return pessoaDAO.carregaDadosPessoa(pessoa);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#carregaEmailPessoa(Pessoa)
	 * @param pessoa
	 * @return
	 * @author Rafael Salvio
	 */
	public Pessoa carregaEmailPessoa(Pessoa pessoa){
		return pessoaDAO.carregaEmailPessoa(pessoa);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#findPessoaByCpf(Cpf)
	 * @param cpf
	 * @return 
	 * @author Jo�o Paulo Zica
	 */
	public Pessoa findPessoaByCpf(Cpf cpf) {
		return pessoaDAO.findPessoaByCpf(cpf);
	}
	
	/**
	 * Obter pessoa pelo CNPJ.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#findPessoaByCnpj(Cnpj)
	 * @param cnpj
	 * @return
	 * @author Flavio
	 */
	public Pessoa findPessoaByCnpj(Cnpj cnpj){
		return pessoaDAO.findPessoaByCnpj(cnpj);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#findPessoaByCodigo(Integer)
	 * @param cod
	 * @return Pessoa
	 * @author Jo�o Paulo Zica
	 */
	public Pessoa findPessoaByCodigo(Integer cod) {
		return pessoaDAO.findPessoaByCodigo(cod);
	}
	
	
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Informa se o CPF j� est� cadastrado.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#cpfJaCadastrado(Cpf)
	 * @param cpf
	 * @return <i>true<i> se o CPF j� estiver cadastrado
	 * @return <i>false</i> se o CPF ainda n�o estiver cadastrado
	 * @author Hugo Ferreira
	 */
	public Boolean cpfJaCadastrado(Cpf cpf) {
		return pessoaDAO.cpfJaCadastrado(cpf);
	}
	
	/* singleton */
	private static PessoaService instance;
	public static PessoaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PessoaService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#updateEmail
	 * 
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void updateEmail(Pessoa bean) {
		pessoaDAO.updateEmail(bean);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#findForSpedReg0150
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<ParticipanteSpedBean> findForSpedReg0150(String whereIn) {
		return pessoaDAO.findForSpedReg0150(whereIn);
	}
	
	public void verificaNomeCPFCNPJ(String cpfcnpj){
		View.getCurrent().println("var nome = '" + this.buscarPessoa(cpfcnpj) + "';");
	}
	
	public void verificaSimples(String cnpj){
		View.getCurrent().println("var simples = " + this.buscarSimples(cnpj) + ";");
	}
	
	public String buscarPessoa(String id){
		try {
			if(id != null && id.length() == 14){
				return EmpresaAPI.getInstance(ReceitaFederalProcess.CHAVE_EMPRESAAPI).getRazaosocialByCNPJ(id);
			} else {
				return BuscaCPFAPI.getInstance().getNomeByCPF(id);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean buscarSimples(String cnpj){
		try {
			return EmpresaAPI.getInstance(ReceitaFederalProcess.CHAVE_EMPRESAAPI).isSimplesByCNPJ(cnpj);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#findPessoaByNome(String nome)
	 *
	 * @param nome
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Pessoa findPessoaByNome(String nome) {
		return pessoaDAO.findPessoaByNome(nome);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param pessoa
	 * @return
	 */
	public Pessoa carregaPessoaForContato(Pessoa pessoa) {
		return pessoaDAO.carregaPessoaForContato(pessoa);
	}

	public String findNomeByCdpessoa(Integer cdpessoa) {
		return pessoaDAO.findNomeByCdpessoa(cdpessoa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PessoaDAO#loadWithTelefone(Pessoa pessoa
	*
	* @param pessoa
	* @return
	* @since 03/10/2016
	* @author Luiz Fernando
	*/
	public Pessoa loadWithTelefone(Pessoa pessoa) {
		return pessoaDAO.loadWithTelefone(pessoa);
	}
	
	public Pessoa loadWithEndereco(Pessoa pessoa) {
		return pessoaDAO.loadWithEndereco(pessoa);
	}
	
	public List<Pessoa> findAutocompleteProcessoReferenciado(String nome){
		return pessoaDAO.findAutocompleteProcessoReferenciado(nome, SinedUtil.getNotaFiscalProdutoParametroRequisicaoAutocomplete(), SinedUtil.getEntradaFiscalParametroRequisicaoAutocomplete());
	}
	
	@SuppressWarnings("unchecked")
	public void setListaEnderecoForEntrada(Pessoa pessoa){
		if(pessoa != null && pessoa.getCdpessoa() != null){
			List<Endereco> listaEndereco = enderecoService.findByPessoaWhereNotIn(pessoa.getCdpessoa(), SinedUtil.isListNotEmpty(pessoa.getListaEndereco()) ? CollectionsUtil.listAndConcatenate(pessoa.getListaEndereco(), "cdendereco", ",") : null);
			if(SinedUtil.isListNotEmpty(listaEndereco)){
				if(SinedUtil.isListNotEmpty(pessoa.getListaEndereco())){
					pessoa.getListaEndereco().addAll(listaEndereco);
				}else {
					pessoa.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
				}
			}
		}
	}
}
