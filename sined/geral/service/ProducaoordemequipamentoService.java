package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemequipamento;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.dao.ProducaoordemequipamentoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemequipamentoService extends GenericService<Producaoordemequipamento> {

	private ProducaoordemequipamentoDAO producaoordemequipamentoDAO;
	private MaterialService materialService;

	public void setProducaoordemequipamentoDAO(ProducaoordemequipamentoDAO producaoordemequipamentoDAO) {
		this.producaoordemequipamentoDAO = producaoordemequipamentoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemequipamentoDA#.findByProducaoordem(Producaoordem producaoordem)
	*
	* @param producaoordem
	* @return
	* @since 24/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemequipamento> findByProducaoordem(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null)
			throw new SinedException("Ordem de produ��o n�o pode ser nula");
		
		return findByProducaoordem(producaoordem.getCdproducaoordem().toString());
	}
	
	public List<Producaoordemequipamento> findByProducaoordem(String whereIn, Boolean patrimonioitemNulo) {
		return producaoordemequipamentoDAO.findByProducaoordem(whereIn, patrimonioitemNulo);
	}
	
	public List<Producaoordemequipamento> findByProducaoordem(String whereIn) {
		return findByProducaoordem(whereIn, null);
	}

	/**
	* M�todo que preenche a lista de equipamentos da ordem de produ��o. 
	*
	* @param producaoordem
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void preencherListaEquipamentos(Producaoordem producaoordem) {
		if(producaoordem != null && SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
			String whereIn = CollectionsUtil.listAndConcatenate(producaoordem.getListaProducaoordemmaterial(), "material.cdmaterial", ",");
			if(StringUtils.isNotBlank(whereIn)){
				List<Material> listaMaterial = materialService.findForProducaoordemEquipamentos(whereIn);
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					Set<Producaoordemequipamento> listaEquipamento = SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemequipamento()) ? 
							producaoordem.getListaProducaoordemequipamento() : new ListSet<Producaoordemequipamento>(Producaoordemequipamento.class);
					
					Integer i = 1;
					for(Material material : listaMaterial){
						if(SinedUtil.isListNotEmpty(material.getListaProducao())){
							Set<Producaoordemmaterial> listaPO = getListaProducaoordemmaterial(material, producaoordem.getListaProducaoordemmaterial());
							if(SinedUtil.isListNotEmpty(listaPO)){
								for(Producaoordemmaterial producaoordemmaterial : listaPO){
									if(StringUtils.isBlank(producaoordemmaterial.getIdentificadorinterno())){
										producaoordemmaterial.setIdentificadorinterno(System.currentTimeMillis() + i.toString() + material.getCdmaterial().toString());
									}
									for(Materialproducao materialproducao : material.getListaProducao()){
										if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getPatrimonio() != null && 
												materialproducao.getMaterial().getPatrimonio() &&
												(materialproducao.getProducaoetapanome() == null || 
												 producaoordemmaterial.getProducaoetapaitem() == null ||
												 materialproducao.getProducaoetapanome().equals(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome())) &&
												!existeEquipamento(materialproducao.getMaterial(), producaoordemmaterial, listaEquipamento)){
											listaEquipamento.add(new Producaoordemequipamento(materialproducao.getMaterial(), producaoordemmaterial, producaoordemmaterial.getIdentificadorinterno()));
										}
									}
									i++;
								}
							}
						}
					}
					
					producaoordem.setListaProducaoordemequipamento(listaEquipamento);
				}
			}
		}
	}
	
	/**
	* M�todo que verifica se existe equipamento j� inclu�do na lista
	*
	* @param material
	* @param producaoordemmaterial
	* @param listaEquipamento
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	private boolean existeEquipamento(Material material, Producaoordemmaterial producaoordemmaterial, Set<Producaoordemequipamento> listaEquipamento) {
		if(material != null && producaoordemmaterial != null && StringUtils.isNotBlank(producaoordemmaterial.getIdentificadorinterno()) && SinedUtil.isListNotEmpty(listaEquipamento)){
			for(Producaoordemequipamento producaoordemequipamento : listaEquipamento){
				if(producaoordemmaterial.getIdentificadorinterno().equals(producaoordemequipamento.getIdentificadorinterno()) &&
						material.equals(producaoordemequipamento.getMaterial())){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo que retorna os itens da ordem de produ��o referente ao material
	*
	* @param material
	* @param listaProducaoordemmaterial
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	private Set<Producaoordemmaterial> getListaProducaoordemmaterial(Material material, Set<Producaoordemmaterial> listaProducaoordemmaterial) {
		Set<Producaoordemmaterial> lista = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
		if(material != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
			for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
				if(producaoordemmaterial.getMaterial() != null && producaoordemmaterial.getMaterial().equals(material)){
					lista.add(producaoordemmaterial);
				}
			}
		}
		return lista;
	}
	
	/**
	* M�todo que retorna o itenm da ordem de produ��o de acordo com o identificador
	*
	* @param listaProducaoordemmaterial
	* @param identificadorinterno
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	private Producaoordemmaterial getProducaoordemmaterial(Set<Producaoordemmaterial> listaProducaoordemmaterial, String identificadorinterno) {
		if(StringUtils.isNotBlank(identificadorinterno) && SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
			for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
				if(identificadorinterno.equals(producaoordemmaterial.getIdentificadorinterno())){
					return producaoordemmaterial;
				}
			}
		}
		return null;
	}
	
	/**
	* M�todo que faz o update do vinculo entrega os itens da ordem de produ��o com os itens de equipamentos
	* @param producaoordem
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void updateVinculoProducaoordemmaterialEquipamento(Producaoordem producaoordem) {
		if(producaoordem != null && SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial()) &&
			SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemequipamento())){
			for(Producaoordemequipamento poe : producaoordem.getListaProducaoordemequipamento()){
				if(StringUtils.isNotBlank(poe.getIdentificadorinterno()) && poe.getProducaoordemmaterial() == null){
					Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterial(producaoordem.getListaProducaoordemmaterial(), poe.getIdentificadorinterno());
					if(producaoordemmaterial != null && producaoordemmaterial.getCdproducaoordemmaterial() != null){
						poe.setProducaoordemmaterial(producaoordemmaterial);
						updateVinculoProducaoordemmaterial(poe, producaoordemmaterial);
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemequipamentoDAO#updateVinculoProducaoordemmaterial(Producaoordemequipamento producaoordemequipamento, Producaoordemmaterial producaoordemmaterial)
	*
	* @param producaoordemequipamento
	* @param producaoordemmaterial
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void updateVinculoProducaoordemmaterial(Producaoordemequipamento producaoordemequipamento, Producaoordemmaterial producaoordemmaterial){
		producaoordemequipamentoDAO.updateVinculoProducaoordemmaterial(producaoordemequipamento, producaoordemmaterial);
	}
	
	/**
	* M�todo que salva os itens de patrimonio selecionados no registro de produ��o
	*
	* @param lista
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void salvarPatrimonioitemEquipamento(Set<Producaoordemequipamento> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Producaoordemequipamento poe : lista){
				if(poe.getCdproducaoordemequipamento() != null && poe.getPatrimonioitem() != null){
					updatePatrimonioitem(poe, poe.getPatrimonioitem());
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemequipamentoDAO#updatePatrimonioitem(Producaoordemequipamento producaoordemequipamento, Patrimonioitem patrimonioitem)
	*
	* @param producaoordemequipamento
	* @param patrimonioitem
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void updatePatrimonioitem(Producaoordemequipamento producaoordemequipamento, Patrimonioitem patrimonioitem){
		producaoordemequipamentoDAO.updatePatrimonioitem(producaoordemequipamento, patrimonioitem);
	}
	
	/**
	* M�todo que faz o update do item de patrimonio ou insere um novo registro de equipamento na ordem de produ��o
	*
	* @param lista
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void registrarEquipamento(Set<Producaoordemequipamento> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Producaoordemequipamento poe : lista){
				if(poe.getCdproducaoordemequipamento() != null && poe.getPatrimonioitem() != null){
					updatePatrimonioitem(poe, poe.getPatrimonioitem());
				}else if(poe.getCdproducaoordemequipamento() == null && poe.getProducaoordem() != null){
					saveOrUpdate(poe);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemequipamentoDAO#findForDelete(Producaoordem producaoordem, String whereIn)
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemequipamento> findForDelete(Producaoordem producaoordem, String whereIn) {
		return producaoordemequipamentoDAO.findForDelete(producaoordem, whereIn);
	}
}
