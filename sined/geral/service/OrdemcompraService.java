package br.com.linkcom.sined.geral.service;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.APROVADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.EM_ABERTO;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ENTREGA_PARCIAL;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.PEDIDO_ENVIADO;

import java.awt.Image;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.bean.Fluxocaixaorigem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcaracteristica;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupousuario;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompraorigem;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaopedidosolicitacaocompra;
import br.com.linkcom.sined.geral.bean.view.Vanalisecompra;
import br.com.linkcom.sined.geral.dao.OrdemcompraDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AvaliacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AvaliacaoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemCompraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemCompraItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraContatoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraEmpresaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraFornecedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraMaterialBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.AnaliseCompraBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.ImportacaoDadosOrdemcompraBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.AnalisecompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraemissaoFiltroReportTemplate;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.CotacaoMapaAux;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionarioquestao;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.SolicitarrestanteBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.SolicitarrestanteItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.OrdemcompraemissaoTemplateReport;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

public class OrdemcompraService extends GenericService<Ordemcompra> {

	private OrdemcompraDAO ordemcompraDAO;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private OrdemcomprahistoricoService ordemcomprahistoricoService;
	private EntregaService entregaService;
	private CotacaoService cotacaoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private FluxocaixaService fluxocaixaService;
	private FluxocaixaTipoService fluxocaixaTipoService; 
	private MaterialService materialService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private CotacaoorigemService cotacaoorigemService;
	private ContatoService contatoService;
	private SolicitacaocompraService solicitacaocompraService;
	private AvisoService avisoService;
	private PapelService papelService;
	private MaterialgrupoService materialgrupoService;
	private UsuarioService usuarioService;
	private ContagerencialService contagerencialService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ParametrogeralService parametrogeralService;
	private PessoaService pessoaService;
	private LocalarmazenagemService localarmazenagemService;
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	private ProdutoService produtoService;
	private OrdemcompraentregaService ordemcompraentregaService;
	private RequisicaomaterialService requisicaomaterialService;
	private ArquivoService arquivoService;
	private IndicecorrecaoService indicecorrecaoService;
	private UnidademedidaService unidademedidaService;
	private OrdemcomprafornecimentocontratoService ordemcomprafornecimentocontratoService;
	private DocumentotipoService documentotipoService;
	private FornecedorService fornecedorService;
	private OrdemcompraarquivoService ordemcompraarquivoService;
	private PessoaquestionarioService pessoaquestionarioService;
	private OrdemcompraemissaoTemplateReport ordemcompraemissaoTemplateReport;
	private EntregamaterialService entregamaterialService;
	private OrdemcompraorigemService ordemcompraorigemService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private SolicitacaocompraordemcompramaterialService solicitacaocompraordemcompramaterialService;
	private MotivoavisoService motivoavisoService;
	private AvisousuarioService avisoUsuarioService;
	
	public void setOrdemcompraemissaoTemplateReport(OrdemcompraemissaoTemplateReport ordemcompraemissaoTemplateReport) {this.ordemcompraemissaoTemplateReport = ordemcompraemissaoTemplateReport;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {this.requisicaomaterialService = requisicaomaterialService;}
	public void setOrdemcompraentregaService(OrdemcompraentregaService ordemcompraentregaService) {this.ordemcompraentregaService = ordemcompraentregaService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {this.materialgrupoService = materialgrupoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setOrdemcompraDAO(OrdemcompraDAO ordemcompraDAO) {this.ordemcompraDAO = ordemcompraDAO;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setOrdemcomprahistoricoService(OrdemcomprahistoricoService ordemcomprahistoricoService) {this.ordemcomprahistoricoService = ordemcomprahistoricoService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setCotacaoService(CotacaoService cotacaoService) {this.cotacaoService = cotacaoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setFluxocaixaService(FluxocaixaService fluxocaixaService) {this.fluxocaixaService = fluxocaixaService;}
	public void setFluxocaixaTipoService(FluxocaixaTipoService fluxocaixaTipoService) {this.fluxocaixaTipoService = fluxocaixaTipoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {this.ordemcompramaterialService = ordemcompramaterialService;}
	public void setCotacaoorigemService(CotacaoorigemService cotacaoorigemService) {this.cotacaoorigemService = cotacaoorigemService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {this.solicitacaocompraService = solicitacaocompraService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setPapelService(PapelService papelService) {this.papelService = papelService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {this.envioemailitemService = envioemailitemService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {this.indicecorrecaoService = indicecorrecaoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setOrdemcomprafornecimentocontratoService(OrdemcomprafornecimentocontratoService ordemcomprafornecimentocontratoService) {
		this.ordemcomprafornecimentocontratoService = ordemcomprafornecimentocontratoService;
	}
	public void setOrdemcompraarquivoService(OrdemcompraarquivoService ordemcompraarquivoService) {this.ordemcompraarquivoService = ordemcompraarquivoService;}
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {this.pessoaquestionarioService = pessoaquestionarioService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setOrdemcompraorigemService(OrdemcompraorigemService ordemcompraorigemService) {this.ordemcompraorigemService = ordemcompraorigemService;}
	public void setPlanejamentorecursogeralService(PlanejamentorecursogeralService planejamentorecursogeralService) {this.planejamentorecursogeralService = planejamentorecursogeralService;}
	public void setSolicitacaocompraordemcompramaterialService(SolicitacaocompraordemcompramaterialService solicitacaocompraordemcompramaterialService) {
		this.solicitacaocompraordemcompramaterialService = solicitacaocompraordemcompramaterialService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {this.motivoavisoService = motivoavisoService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	
	/**
	 * Este m�todo agrupa as cota��es da seguinte maneira. Fornecedor + local de entrega iguais 
	 * tornam mesma ordem de compra.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoorigemService#findSolicitacoesByCotacao(Cotacao)
	 * @see #criaOrdemCompra(Integer, Cotacaofornecedor, Cotacao, Map, List)
	 * @param request
	 * @param cotacao
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public List<Ordemcompra> geraOrdemCompra(Cotacao cotacao) {
		List<Ordemcompra> listaOrdensCompra = new ListSet<Ordemcompra>(Ordemcompra.class);
		List<Cotacaoorigem> listaSolicitacoes = cotacaoorigemService.findSolicitacoesByCotacao(cotacao);
		List<Requisicaomaterial> listaRequisicaomaterial = requisicaomaterialService.findByCotacaoWithArquivo(cotacao);
		
		Empresa empresaUnicaentrega = null; 
		if(cotacao != null && cotacao.getCdcotacao() != null){
			Cotacao cotacaoEmpresaUnica = cotacaoService.loadWithEmpresa(cotacao);
			if(cotacaoEmpresaUnica != null && cotacaoEmpresaUnica.getEmpresa() != null){
				empresaUnicaentrega = cotacaoEmpresaUnica.getEmpresa();
			}
		}
		for (Cotacaofornecedor cotacaofornecedor: cotacao.getListaCotacaofornecedor()) {
			
			//Agrupa materiais aos locais armazenagem com mesmo fornecedor
			Map<Integer, Set<Cotacaofornecedoritem>> map = new HashMap<Integer, Set<Cotacaofornecedoritem>>();
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
				if(cotacaofornecedoritem.getChecado()){
					if(map.containsKey(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem())){
						map.get(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem()).add(cotacaofornecedoritem);
					} else{
						Set<Cotacaofornecedoritem> list = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
						list.add(cotacaofornecedoritem);
						map.put(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem(),list);
					}
				}
			}
			
			//Monta a Ordem de compra com os itens
			if(!map.isEmpty()){
				for (Integer cdlocalArmazenagem : map.keySet()) {
					listaOrdensCompra.addAll(criaOrdemCompra(cdlocalArmazenagem, cotacaofornecedor, cotacao, map, listaSolicitacoes, empresaUnicaentrega));
				}
			} 
		}

		if(listaOrdensCompra == null || listaOrdensCompra.size() == 0)
			throw new SinedException("Nenhum item da cota��o foi selecionado.");
		
		for (Ordemcompra ordemcompra : listaOrdensCompra) {
			Set<Ordemcompraarquivo> listaArquivo = new ListSet<Ordemcompraarquivo>(Ordemcompraarquivo.class);
			List<Ordemcompramaterial> listaMaterial = ordemcompra.getListaMaterial();
			
			if(listaMaterial != null && listaMaterial.size() > 0){
				for (Ordemcompramaterial ocm : listaMaterial) {
					if(ocm.getMaterial() != null){
						for (Requisicaomaterial rm : listaRequisicaomaterial) {
							if(rm.getArquivo() != null && rm.getArquivo().getCdarquivo() != null && 
									rm.getMaterial() != null && rm.getMaterial().equals(ocm.getMaterial())){
								try{
									Arquivo arquivocarregado = arquivoService.loadWithContents(rm.getArquivo());
									arquivocarregado.setCdarquivo(null);
									
									Ordemcompraarquivo ordemcompraarquivo = new Ordemcompraarquivo();
									ordemcompraarquivo.setArquivo(arquivocarregado);
									
									listaArquivo.add(ordemcompraarquivo);
								} catch (Exception e) {
									e.printStackTrace();
									NeoWeb.getRequestContext().addError(e.getMessage());
								}
							}
						}
					}
				}
//				this.calculaRateioAPartirDaCotacaoForGerarordemcompra(ordemcompra);
				this.calculaRateio(ordemcompra);
			}
			
			ordemcompra.setListaArquivosordemcompra(listaArquivo);
		}
		
		return listaOrdensCompra;
	}

	/**
	 * Cria a ordem de compra com seus respectivos itens
	 * 
	 * @param cdlocalArmazenagem
	 * @param cotacaofornecedor
	 * @param cotacao
	 * @param map
	 * @param listaSolicitacoes
	 * @return
	 * @author Tom�s Rabelo	 
	 * @param listaSolicitacoes 
	 * @param listaRequisicaomaterial 
	 */
	private Set<Ordemcompra> criaOrdemCompra(Integer cdlocalArmazenagem,	Cotacaofornecedor cotacaofornecedor, Cotacao cotacao,
			Map<Integer, Set<Cotacaofornecedoritem>> map, List<Cotacaoorigem> listaSolicitacoes, Empresa empresaUnicaentrega) {
		
		Set<Ordemcompra> list = new ListSet<Ordemcompra>(Ordemcompra.class);
		Boolean faturamentocliente = Boolean.FALSE;
		
		/*Roda duas vezes uma verificando se existe faturamento para cliente e outra n�o.
		 * Caso n�o adicione nenhum material simplesmente descarta a ordem de compra.
		 */
		for (int i = 0; i < 2; i++) {
			
			Ordemcompra ordemcompra = new Ordemcompra();
			
			ordemcompra.setListaMaterial(new ListSet<Ordemcompramaterial>(Ordemcompramaterial.class));
			ordemcompra.setFornecedor(cotacaofornecedor.getFornecedor());
			ordemcompra.setContato(cotacaofornecedor.getContato());
	
			Localarmazenagem localarmazenagem = new Localarmazenagem(cdlocalArmazenagem);
			ordemcompra.setLocalarmazenagem(localarmazenagem);
			
			ordemcompra.setPrazopagamento(cotacaofornecedor.getPrazopagamento());
			
			ordemcompra.setDesconto(cotacaofornecedor.getDesconto());
			ordemcompra.setFrete(new Money());
			
			ordemcompra.setObservacao(cotacaofornecedor.getObservacao());
			ordemcompra.setCotacao(cotacao);
			
			ordemcompra.setRateioverificado(Boolean.FALSE);
			ordemcompra.setFaturamentocliente(faturamentocliente);
			
			//Busca a menos data da entrega encontrada nos fornecedores item
			Date proximaEntrega = null;
			Double valortotaldescontomaterial = 0.0;
			
			Collection<Set<Cotacaofornecedoritem>> listaMatForn = map.values();
			for (Set<Cotacaofornecedoritem> set : listaMatForn) {
				for (Cotacaofornecedoritem cotacaofornecedoritem2 : set) {
					boolean somarDescontoFrete = true;
					//Verifica se o localarmazenagem bate com a key
					if(cotacaofornecedoritem2.getLocalarmazenagem().getCdlocalarmazenagem().equals(cdlocalArmazenagem)){
						if(listaSolicitacoes != null && listaSolicitacoes.size() > 0){
							for (Cotacaoorigem cotacaoorigem : listaSolicitacoes) {
								/*Busca as solicita��es do material. Isso � feito pois mais de uma solicita��o pode gerar 1 cotacaofornecedoritem.
								 * � neste passo que separa o material dos que ser�o faturamento do cliente e os n�o
								 */
								if(cotacaoorigem.getSolicitacaocompra().getMaterial().equals(cotacaofornecedoritem2.getMaterial()) && 
								   cotacaoorigem.getSolicitacaocompra().getFaturamentocliente().equals(faturamentocliente) &&
								   cotacaoorigem.getSolicitacaocompra().getFrequencia().equals(cotacaofornecedoritem2.getFrequencia()) &&
								   cotacaoorigem.getSolicitacaocompra().getQtdefrequencia().equals(cotacaofornecedoritem2.getQtdefrequencia())){
									
									Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial(
											cotacaofornecedoritem2.getMaterial(), 0.0, 0.0, cotacaofornecedoritem2.getIcmsissincluso(),
											cotacaofornecedoritem2.getIcmsiss(), cotacaofornecedoritem2.getIpiincluso(), cotacaofornecedoritem2.getIpi(), 
											cotacaofornecedoritem2.getFrequencia(), cotacaofornecedoritem2.getQtdefrequencia(), cotacaofornecedoritem2.getUnidademedida());
									
									if(proximaEntrega == null && cotacaofornecedoritem2.getDtentrega() != null){
										proximaEntrega = cotacaofornecedoritem2.getDtentrega();
									} else if(cotacaofornecedoritem2.getDtentrega() != null){
										if(proximaEntrega.after(cotacaofornecedoritem2.getDtentrega()))
											proximaEntrega = cotacaofornecedoritem2.getDtentrega();
									}
									
									if(ordemcompramaterial.getObservacao() != null){
										ordemcompramaterial.setObservacao(ordemcompramaterial.getObservacao() + ". " + cotacaofornecedoritem2.getObservacao());
									}else {
										ordemcompramaterial.setObservacao(cotacaofornecedoritem2.getObservacao());
									}
									
									ordemcompramaterial.setAltura(cotacaoorigem.getSolicitacaocompra().getAltura());
									ordemcompramaterial.setComprimento(cotacaoorigem.getSolicitacaocompra().getComprimento());
									ordemcompramaterial.setLargura(cotacaoorigem.getSolicitacaocompra().getLargura());
									ordemcompramaterial.setPesototal(cotacaoorigem.getSolicitacaocompra().getPesototal());
									ordemcompramaterial.setQtdvolume(cotacaoorigem.getSolicitacaocompra().getQtdvolume());
									ordemcompramaterial.setDtentrega(cotacaofornecedoritem2.getDtentrega());
									ordemcompramaterial.setProjeto(cotacaoorigem.getSolicitacaocompra().getProjeto());
									ordemcompramaterial.setCentrocusto(cotacaoorigem.getSolicitacaocompra().getCentrocusto());
									if(cotacaoorigem.getSolicitacaocompra().getContagerencial() != null){
										ordemcompramaterial.setContagerencial(cotacaoorigem.getSolicitacaocompra().getContagerencial());
									}else {
										ordemcompramaterial.setContagerencial(cotacaoorigem.getSolicitacaocompra().getMaterial().getContagerencial());
									}
									double percentual = this.getQtdeConvercaoSolicitacaocompra(cotacaoorigem.getSolicitacaocompra(), cotacaofornecedoritem2) / cotacaofornecedoritem2.getQtdesol().doubleValue();
									double valor = cotacaofornecedoritem2.getQtdecot().doubleValue() * percentual;
									ordemcompramaterial.setQtdpedida(valor + ordemcompramaterial.getQtdpedida());
									
									addSolicitacaocompraOrdemcompramaterial(ordemcompramaterial, cotacaoorigem.getSolicitacaocompra(), ordemcompramaterial.getQtdpedida());
									
									Double valorUnitarioForOrdemcompra = cotacaofornecedoritem2.getValorunitarioForOrdemcompra();
									ordemcompramaterial.setValor(ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia() * (valorUnitarioForOrdemcompra != null ? valorUnitarioForOrdemcompra : 0d));
									
									ordemcompramaterial.setDescontotrans(cotacaofornecedoritem2.getDesconto()); 
									
									if(cotacaofornecedoritem2.getIcmsiss() != null)
										ordemcompramaterial.setIcmsiss(ordemcompramaterial.getValor().doubleValue() * cotacaofornecedoritem2.getIcmsiss().doubleValue() / 100);
									if(cotacaofornecedoritem2.getIpi() != null)
										ordemcompramaterial.setIpi(ordemcompramaterial.getValor().doubleValue() * cotacaofornecedoritem2.getIpi().doubleValue() / 100);
									
									if(ordemcompramaterial.getUnidademedida() == null && cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getUnidademedida() != null){
										ordemcompramaterial.setUnidademedida(cotacaoorigem.getSolicitacaocompra().getUnidademedida());
									}
									if(ordemcompramaterial.getLocalarmazenagem() == null){
										if(cotacao.getConsiderarlocalunicoentrega() != null && cotacao.getConsiderarlocalunicoentrega() &&
												cotacaofornecedoritem2.getLocalarmazenagem() != null){
											ordemcompramaterial.setLocalarmazenagem(cotacaofornecedoritem2.getLocalarmazenagem());
										}else if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getLocalarmazenagem() != null){
											ordemcompramaterial.setLocalarmazenagem(cotacaoorigem.getSolicitacaocompra().getLocalarmazenagem());
										}
									}
									
									if(ordemcompramaterial.getQtdpedida() > 0){
										ordemcompra.getListaMaterial().add(ordemcompramaterial);
										if(ordemcompramaterial.getDescontotrans() != null && somarDescontoFrete){
											valortotaldescontomaterial += ordemcompramaterial.getDescontotrans().getValue().doubleValue();
										}
										
										if(somarDescontoFrete){
											ordemcompra.setFrete(ordemcompra.getFrete().add(new Money(cotacaofornecedoritem2.getFrete() != null ? cotacaofornecedoritem2.getFrete() : 0.0)));
										}
										somarDescontoFrete = false;
									}
								}
								if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getEmpresa() != null){
									ordemcompra.setEmpresa(cotacaoorigem.getSolicitacaocompra().getEmpresa());
								}
							}
						} else {
							Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial(
									cotacaofornecedoritem2.getMaterial(), 0.0, 0.0, cotacaofornecedoritem2.getIcmsissincluso(),
									cotacaofornecedoritem2.getIcmsiss(), cotacaofornecedoritem2.getIpiincluso(), cotacaofornecedoritem2.getIpi(), 
									cotacaofornecedoritem2.getFrequencia(), cotacaofornecedoritem2.getQtdefrequencia(), cotacaofornecedoritem2.getUnidademedida());
							
							if(proximaEntrega == null && cotacaofornecedoritem2.getDtentrega() != null){
								proximaEntrega = cotacaofornecedoritem2.getDtentrega();
							} else if(cotacaofornecedoritem2.getDtentrega() != null){
								if(proximaEntrega.after(cotacaofornecedoritem2.getDtentrega()))
									proximaEntrega = cotacaofornecedoritem2.getDtentrega();
							}
							ordemcompramaterial.setDtentrega(cotacaofornecedoritem2.getDtentrega());
							ordemcompramaterial.setQtdpedida(cotacaofornecedoritem2.getQtdecot().doubleValue() + ordemcompramaterial.getQtdpedida());
							ordemcompramaterial.setValor(ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia() * cotacaofornecedoritem2.getValorunitarioForOrdemcompra());
							
							ordemcompramaterial.setDescontotrans(cotacaofornecedoritem2.getDesconto());
							ordemcompramaterial.setObservacao(cotacaofornecedoritem2.getObservacao());
							
							if(ordemcompramaterial.getIcmsiss() != null)
								ordemcompramaterial.setIcmsiss(ordemcompramaterial.getValor().doubleValue() * ordemcompramaterial.getIcmsiss().doubleValue() / 100);
							if(ordemcompramaterial.getIpi() != null)
								ordemcompramaterial.setIpi(ordemcompramaterial.getValor().doubleValue() * ordemcompramaterial.getIpi().doubleValue() / 100);
							
							i = 1;
							
							if(ordemcompramaterial.getQtdpedida() > 0){
								ordemcompra.getListaMaterial().add(ordemcompramaterial);
								if(ordemcompramaterial.getDescontotrans() != null)
									valortotaldescontomaterial += ordemcompramaterial.getDescontotrans().getValue().doubleValue();
								
								ordemcompra.setFrete(ordemcompra.getFrete().add(new Money(cotacaofornecedoritem2.getFrete() != null ? cotacaofornecedoritem2.getFrete() : 0.0)));
							}
						}
						
						
					}
				}
			}
			
			ordemcompra.setDtproximaentrega(proximaEntrega);
			if(ordemcompra.getDesconto() != null)
				valortotaldescontomaterial += ordemcompra.getDesconto().getValue().doubleValue();
			ordemcompra.setDesconto(new Money(valortotaldescontomaterial));
			
			if(empresaUnicaentrega != null){
				ordemcompra.setEmpresa(empresaUnicaentrega);
			}
			
			if(ordemcompra.getListaMaterial() != null && ordemcompra.getListaMaterial().size() > 0){
				ordemcompra.setListaMaterial(this.agrupaItensOrdemCompra(ordemcompra.getListaMaterial()));
				list.add(ordemcompra);
			}
			
			if(!faturamentocliente)
				faturamentocliente = Boolean.TRUE;
		}
		return list;
	}


	/**
	* M�todo que adicione uma solicita��o de compra no item da ordem de compra
	*
	* @param ordemcompramaterial
	* @param solicitacaocompra
	* @param qtde
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	private void addSolicitacaocompraOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial, Solicitacaocompra solicitacaocompra, Double qtde) {
		if(ordemcompramaterial != null && solicitacaocompra != null){
			Set<Solicitacaocompraordemcompramaterial> lista = ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial();
			if(lista == null){
				lista = new ListSet<Solicitacaocompraordemcompramaterial>(Solicitacaocompraordemcompramaterial.class);
			}
			
			boolean adicionar = true;
			for(Solicitacaocompraordemcompramaterial solicitacaocompraordemcompramaterial : lista){
				if(solicitacaocompra.equals(solicitacaocompraordemcompramaterial.getSolicitacaocompra())){
					adicionar = false;
					break;
				}
			}
			
			if(adicionar){
				Solicitacaocompraordemcompramaterial item = new Solicitacaocompraordemcompramaterial();
				item.setSolicitacaocompra(solicitacaocompra);
				item.setQtde(qtde);
				lista.add(item);
			}
			ordemcompramaterial.setListaSolicitacaocompraordemcompramaterial(lista);
		}
	}
	
	/**
	 * M�todo que converte a quantidade da solicita��o de compra para o calculo da qtde da ordem de compra.
	 * Obs: apenas � feito se a undiade da solicita��o for diferente da cota��o
	 *
	 * @param solicitacaocompra
	 * @param cotacaofornecedoritem2
	 * @return
	 * @author Luiz Fernando
	 */
	private Double getQtdeConvercaoSolicitacaocompra(Solicitacaocompra solicitacaocompra, Cotacaofornecedoritem cotacaofornecedoritem2) {
		Double qtde = solicitacaocompra.getQtde();
		if(solicitacaocompra != null && solicitacaocompra.getMaterial() != null && 
				solicitacaocompra.getQtde() != null && cotacaofornecedoritem2.getUnidademedida() != null && 
				solicitacaocompra.getUnidademedida() != null && 
				!solicitacaocompra.getUnidademedida().equals(cotacaofornecedoritem2.getUnidademedida())){
			qtde = unidademedidaService.getQtdeConvertida(solicitacaocompra.getMaterial(), cotacaofornecedoritem2.getUnidademedida(), solicitacaocompra.getUnidademedida(), qtde);
		}
		return qtde;
	}
	/**
	 * Faz o agrupamento dos itens da Ordem de compra.
	 *
	 * @param listaMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	private List<Ordemcompramaterial> agrupaItensOrdemCompra(List<Ordemcompramaterial> listaMaterial) {
		List<Ordemcompramaterial> listaAgrupada = new ArrayList<Ordemcompramaterial>();
		
		for (Ordemcompramaterial om : listaMaterial) {
			boolean achou = false;
			for (Ordemcompramaterial omAgrupada : listaAgrupada) {
				boolean materialIgual = om.getMaterial().equals(omAgrupada.getMaterial());
				boolean contagerencialIgual = (om.getContagerencial() != null && omAgrupada.getContagerencial() != null && om.getContagerencial().equals(omAgrupada.getContagerencial())) || (om.getContagerencial() == null && omAgrupada.getContagerencial() == null);
				boolean centrocustoIgual = (om.getCentrocusto() != null && omAgrupada.getCentrocusto() != null && om.getCentrocusto().equals(omAgrupada.getCentrocusto())) || (om.getCentrocusto() == null && omAgrupada.getCentrocusto() == null);
				boolean projetoIgual = (om.getProjeto() != null && omAgrupada.getProjeto() != null && om.getProjeto().equals(omAgrupada.getProjeto())) || (om.getProjeto() == null && omAgrupada.getProjeto() == null);
				boolean localIgual = (om.getLocalarmazenagem() != null && omAgrupada.getLocalarmazenagem() != null && om.getLocalarmazenagem().equals(omAgrupada.getLocalarmazenagem())) || (om.getLocalarmazenagem() == null && omAgrupada.getLocalarmazenagem() == null);
				boolean frequenciaIgual = (om.getFrequencia() != null && omAgrupada.getFrequencia() != null && om.getFrequencia().equals(omAgrupada.getFrequencia())) || (om.getFrequencia() == null && omAgrupada.getFrequencia() == null);
				boolean qtdeFrequenciaIgual = (om.getQtdefrequencia() != null && omAgrupada.getQtdefrequencia() != null && om.getQtdefrequencia().equals(omAgrupada.getQtdefrequencia())) || (om.getQtdefrequencia() == null && omAgrupada.getQtdefrequencia() == null);
				boolean valorunitarioIgual = (om.getValorunitario() != null && omAgrupada.getValorunitario() != null && om.getValorunitario().equals(omAgrupada.getValorunitario())) || (om.getValorunitario() == null && omAgrupada.getValorunitario() == null);
				
				if(materialIgual && contagerencialIgual && centrocustoIgual && projetoIgual && localIgual && frequenciaIgual && qtdeFrequenciaIgual && valorunitarioIgual && !parametrogeralService.getBoolean("DESAGRUPAR_ITENS_COTACAO_ORDEMCOMPRA")){
					omAgrupada.setQtdpedida((omAgrupada.getQtdpedida() != null ? omAgrupada.getQtdpedida() : 0d) + (om.getQtdpedida() != null ? om.getQtdpedida() : 0d));
					omAgrupada.setValor((omAgrupada.getValor() != null ? omAgrupada.getValor() : 0d) + (om.getValor() != null ? om.getValor() : 0d));
					omAgrupada.setIpi((omAgrupada.getIpi() != null ? omAgrupada.getIpi() : 0d) + (om.getIpi() != null ? om.getIpi() : 0d));
					omAgrupada.setIcmsiss((omAgrupada.getIcmsiss() != null ? omAgrupada.getIcmsiss() : 0d) + (om.getIcmsiss() != null ? om.getIcmsiss() : 0d));
					omAgrupada.setValoroutrasdespesas((omAgrupada.getValoroutrasdespesas() != null ? omAgrupada.getValoroutrasdespesas() : new Money()).add((om.getValoroutrasdespesas() != null ? om.getValoroutrasdespesas() : new Money())));
					
					if(om.getObservacao() != null){
						if(omAgrupada.getObservacao() != null){
							if(!omAgrupada.getObservacao().contains(om.getObservacao())){
								omAgrupada.setObservacao(omAgrupada.getObservacao() + ". " + om.getObservacao());
							}
						}else {
							omAgrupada.setObservacao(om.getObservacao());
						}
					}
					
					if(SinedUtil.isListNotEmpty(om.getListaSolicitacaocompraordemcompramaterial())){
						for(Solicitacaocompraordemcompramaterial solicitacaocompraordemcompramaterial : om.getListaSolicitacaocompraordemcompramaterial()){
							addSolicitacaocompraOrdemcompramaterial(omAgrupada, solicitacaocompraordemcompramaterial.getSolicitacaocompra(), om.getQtdpedida());
						}
					}
					
					achou = true;
					break;
				}
			}
			
			if(!achou){
				listaAgrupada.add(om);
			}
		}
		
		return listaAgrupada;
	}
	
	/**
	 * M�todo que salva as ordens de compras geradas a partir de uma cota��o.
	 * Al�m de salvar concatena os ids das ordens de compras geradas e coloca no 
	 * filtro.
	 * 
	 * @see #saveOrUpdateNoUseTransaction(Ordemcompra)
	 * @see br.com.linkcom.sined.geral.service.OrdemcomprahistoricoService#saveOrUpdateNoUseTransaction(Ordemcomprahistorico)
	 * @see #montaAvisosOrdemCompra(List, Set, String)
	 * @see br.com.linkcom.sined.geral.service.AvisoService#saveOrUpdateNoUseTransaction(Aviso)
	 * @param form
	 * @param listaPapeis 
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public String salvaOrdensCompra(final List<Ordemcompra> ordensCompra) {
		Object cds = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				StringBuilder cds = new StringBuilder();
				for (Ordemcompra ordemcompra : ordensCompra){
					boolean existeItemSemCentrocusto = false;
					if(ordemcompra.getRateio() != null && SinedUtil.isListNotEmpty(ordemcompra.getRateio().getListaRateioitem())){
						for(Rateioitem rateioitem : ordemcompra.getRateio().getListaRateioitem()){
							if(rateioitem.getCentrocusto() == null){
								existeItemSemCentrocusto = true;
								break;
							}
						}
					}
					if(existeItemSemCentrocusto) {
						ordemcompra.setRateio(null);
					}else {
						rateioService.saveOrUpdate(ordemcompra.getRateio());
					}
					saveOrUpdateNoUseTransaction(ordemcompra);
					cds.append(ordemcompra.getCdordemcompra()+",");
					
					ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(
							new Ordemcomprahistorico(ordemcompra, Ordemcompraacao.CRIADA, 
													"Cota��o <a href=\"#\" onclick=\"redirecionaCotacao("+ordemcompra.getCotacao().getCdcotacao()+")\">"+ordemcompra.getCotacao().getCdcotacao()+"</a>"));
					
					if(ordemcompra.getListaMaterial() != null){
						for (Ordemcompramaterial ocm : ordemcompra.getListaMaterial()) {
							if(ocm.getCdordemcompramaterial() != null){
								Set<Solicitacaocompraordemcompramaterial> listaSCOCM = ocm.getListaSolicitacaocompraordemcompramaterial();
								if(SinedUtil.isListNotEmpty(listaSCOCM)){
									for (Solicitacaocompraordemcompramaterial scocm : listaSCOCM) {
										scocm.setOrdemcompramaterial(ocm);
										solicitacaocompraordemcompramaterialService.saveOrUpdateNoUseTransaction(scocm);
									}
								}
							}
						}
					}
				}
				
				List<Aviso> listaAvisos = new ListSet<Aviso>(Aviso.class);
				montaAvisosOrdemCompra(listaAvisos, MotivoavisoEnum.AUTORIZAR_ORDEMCOMPRA, ordensCompra, "Aguardando autoriza��o");
				avisoService.salvarAvisos(listaAvisos, true);
				
				if(cds.toString().length() > 0)
					cds.delete(cds.toString().length()-1, cds.toString().length());
					
				return cds.toString();
			}
		});
		
		// ATUALIZA AS TABELAS AUCXILIARES DE ORDEM DE COMPRA
		this.callProcedureAtualizaOrdemcompra(cds.toString());
		
		return cds.toString();
	}
	
	/**
	 * Monta uma lista de avisos gerais
	 * 
	 * @param listaAvisos
	 * @param listaPapeis
	 * @param ordemcompra
	 * @param assunto
	 * @author Tom�s Rabelo
	 */
	public void montaAvisosOrdemCompra(List<Aviso> listaAvisos, MotivoavisoEnum motivo, Ordemcompra ordemcompra, String assunto) {
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		if(motivoAviso != null){
			listaAvisos.add(new Aviso(assunto, "Ordem de compra: "+ordemcompra.getCdordemcompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
					motivoAviso.getUsuario(), AvisoOrigem.ORDEM_DE_COMPRA, ordemcompra.getCdordemcompra(), ordemcompra.getEmpresa(), getWhereInProjeto(ordemcompra), 
					motivoAviso));
		}
	}
	
	private String getWhereInProjeto(Ordemcompra ordemcompra) {
		StringBuilder whereInProjeto = new StringBuilder();
		if(ordemcompra != null && ordemcompra.getRateio() != null && SinedUtil.isListNotEmpty(ordemcompra.getRateio().getListaRateioitem())){
			for(Rateioitem ri : ordemcompra.getRateio().getListaRateioitem()){
				if(ri.getProjeto() != null && ri.getProjeto().getCdprojeto() != null){
					whereInProjeto.append(ri.getProjeto().getCdprojeto().toString()).append(",");
				}
			}
		}
		return whereInProjeto.length() > 0 ? whereInProjeto.substring(0, whereInProjeto.length()-1) : null;
	}

	/**
	 * Monta uma lista de avisos gerais
	 * 
	 * @param listaPapeis
	 * @param ordensCompra
	 * @param assunto
	 * @return
	 * @author Tom�s Rabelo
	 * @param listaAvisos 
	 */
	public void montaAvisosOrdemCompra(List<Aviso> listaAvisos, MotivoavisoEnum motivo, List<Ordemcompra> ordensCompra, String assunto) {
		for (Ordemcompra ordemcompra : ordensCompra){ 
			montaAvisosOrdemCompra(listaAvisos, motivo, ordemcompra, assunto);
		}
	}
		
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findOrdens
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdens(String whereIn) {
		return ordemcompraDAO.findOrdens(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoOrdens(String whereIn, Situacaosuprimentos situacao) {
		ordemcompraDAO.doUpdateSituacaoOrdens(whereIn, situacao);
	}

	/**
	 * Gera relat�rio com valores iguais o da listagem.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForListagem
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public IReport gerarRelatorio(OrdemcompraFiltro filtro) {
		Report report = new Report("/suprimento/ordemcompra");
		
		filtro.setPageSize(Integer.MAX_VALUE);
		
		ListagemResult<Ordemcompra> ordens = ordemcompraDAO.findForListagem(filtro);
		
		if(ordens != null && ordens.list().size() > 0){
			List<Ordemcompra> lista = ordens.list();
			List<Ordemcompramaterial> findByOrdemcompra;
			Double saldo;
			Money valorTotal = new Money();
			for (Ordemcompra o : lista) {
				
				o.setListaOrdemcompraentrega(SinedUtil.listToSet(ordemcompraentregaService.findByOrdemcompra(o), Ordemcompraentrega.class));
				o.getPrazopagamento().setListaPagamentoItem(prazopagamentoitemService.findByPrazo(o.getPrazopagamento()));
				findByOrdemcompra = ordemcompramaterialService.findByOrdemcompra(o);
				o.setListaMaterial(findByOrdemcompra);
				
				saldo = o.getValorTotalOrdemCompraReport();
				valorTotal = valorTotal.add(new Money(saldo)); 
				if(o.getListaOrdemcompraentrega() != null && o.getListaOrdemcompraentrega().size() > 0){
					for (Ordemcompraentrega ordemcompraentrega : o.getListaOrdemcompraentrega()) {
						if(ordemcompraentrega.getEntrega() != null){
							saldo -= ordemcompraentrega.getEntrega().getValorTotalEntrega().getValue().doubleValue();
						}
					}
				}
				o.setSaldo(saldo);
				
			}
			
			report.setDataSource(ordens.list());
			report.addParameter("valorTotal", valorTotal);
		}
		return report;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findOrdensCompra(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 10/02/2016
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findOrdensCompra(String whereIn){
		List<Ordemcompra> lista = ordemcompraDAO.findOrdensCompra(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			List<Material> listaMaterial = new ArrayList<Material>();
			for(Ordemcompra ordemcompra : lista){
				if(SinedUtil.isListNotEmpty(ordemcompra.getListaMaterial())){
					for(Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()){
						if(ordemcompramaterial.getMaterial() != null){
							ordemcompramaterial.getMaterial().setListaCaracteristica(new ListSet<Materialcaracteristica>(Materialcaracteristica.class));
							if(!listaMaterial.contains(ordemcompramaterial.getMaterial())){
								listaMaterial.add(ordemcompramaterial.getMaterial());
							}
						}
					}
				}
			}
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				List<Material> listaMterialComCategoria = materialService.findWithCaracteristica(CollectionsUtil.listAndConcatenate(listaMaterial, "cdmaterial", ","));
				if(SinedUtil.isListEmpty(listaMterialComCategoria)){
					for(Material material : listaMterialComCategoria){
						if(SinedUtil.isListNotEmpty(material.getListaCaracteristica())){
							for(Material materialOC : listaMaterial){
								materialOC.setListaCaracteristica(material.getListaCaracteristica());
							}
						}
					}
				}
			}
		}
		return lista;
	}

	/**
	 * Gera relat�rio de emiss�o de ordem de compra, para o usu�rio enviar para os
	 * respectivos fornecedores
	 * 
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#carregaRubricaUsuario(Usuario)
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadWithContents(br.com.linkcom.neo.types.File)
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadAsImage(br.com.linkcom.neo.types.File)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findOrdensCompra(String)
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#carregaEnderecoEmpresa(Empresa)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompramaterialService#findByOrdemcompra(Ordemcompra)
	 * @see br.com.linkcom.sined.geral.service.MaterialgrupoService#makeStringObservacao(List)
	 * @see #buscaEnderecoFornecedor(Fornecedor)
	 * @see #buscaTelefoneContatoFornecedor(Contato)
	 * @see #buscaProjetosContasgerenciaisRateioOrdemCompra(Rateio)
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public Resource emitirOrdemCompra(String whereIn) throws Exception {
		MergeReport mergeReport = new MergeReport("ordemcompra_"+SinedUtil.datePatternForReport()+".pdf");
		Date dtemissao = new Date(System.currentTimeMillis());
		
		List<Ordemcompra> ordens = findOrdensCompra(whereIn);
		if(ordens != null && ordens.size() > 0){
			
			for (Ordemcompra item : ordens) {
				if(item.getFornecedor() != null && item.getFornecedor().getCdpessoa() != null){
					item.setFornecedor(fornecedorService.carregaFornecedor(item.getFornecedor()));
				}
				if(item.getContato() != null && item.getContato().getCdpessoa() != null){
					item.setContato(contatoService.carregaContato(item.getContato()));
				}
				if(item.getFornecedoroptriangular() != null && item.getFornecedoroptriangular().getCdpessoa() != null){
					item.setFornecedoroptriangular(fornecedorService.carregaFornecedor(item.getFornecedoroptriangular()));
					if(item.getEnderecoentrega() != null && item.getEnderecoentrega().getCdendereco() != null){
						item.setEnderecoentrega(enderecoService.loadEndereco(item.getEnderecoentrega()));
					}
				}
				if(item.getEmpresa() != null && item.getEmpresa().getCdpessoa() != null){
					item.setEmpresa(empresaService.loadWithEnderecoTelefone(item.getEmpresa()));
				}
				if(item.getTransportador() != null && item.getTransportador().getCdpessoa() != null){
					item.setTransportador(fornecedorService.carregaFornecedor(item.getTransportador()));
				}
				for (Ordemcompramaterial ocm : item.getListaMaterial()) {
					if(ocm.getUnidademedida() != null){
                        ocm.getMaterial().setUnidademedidaString(ocm.getUnidademedida().getSimbolo());
						ocm.setUnidademedida_simbolo(ocm.getUnidademedida().getSimbolo());
					}else if(ocm.getMaterial().getUnidademedida() != null) {
						ocm.setUnidademedida_simbolo(ocm.getMaterial().getUnidademedida().getSimbolo());
					}
				}
				
			}
		}
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		Endereco enderecoEmpresa = null;
		Endereco enderecoEmpresaPrincipal = null;
		if(empresaPrincipal != null)
			enderecoEmpresaPrincipal = enderecoService.carregaEnderecoAtivoEmpresa(empresaPrincipal);
		
		for (Ordemcompra ordemcompra : ordens) {
			empresaService.setInformacoesPropriedadeRural(ordemcompra.getEmpresa());
			
			String telefone = "";
			List<Ordemcompramaterial> list = new ArrayList<Ordemcompramaterial>();
			list.addAll(ordemcompra.getListaMaterial());
			Produto produto; 
			for (Ordemcompramaterial ordemcompramaterial : list) {
				produto = new Produto();
				produto = produtoService.carregaProduto(ordemcompramaterial.getMaterial());
				ordemcompramaterial.getMaterial().setProduto_largura(produto != null ? produto.getLargura() : null);
				ordemcompramaterial.getMaterial().setProduto_comprimento(produto != null ? produto.getComprimento() : null);
				
				if(ordemcompramaterial.getQtdvolume() != null && ordemcompramaterial.getQtdvolume() > 0d){
					ordemcompramaterial.getMaterial().setNome(ordemcompramaterial.getMaterial().getNome() + "\nQtde. Volume(s): " + SinedUtil.descriptionDecimal(ordemcompramaterial.getQtdvolume()));
				}
			}
			
			Collections.sort(list,new Comparator<Ordemcompramaterial>(){
				public int compare(Ordemcompramaterial o1, Ordemcompramaterial o2) {
					return o1.getCdordemcompramaterial().compareTo(o2.getCdordemcompramaterial());
				}
			});
			
			String modelo = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.LAYOUT_EMISSAO_ORDEMCOMPRA);
			
			if (parametrogeralService.getBoolean(Parametrogeral.ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC)) {
				ordemcompramaterialService.getCodigoAlternativo(ordemcompra.getListaMaterial(), ordemcompra.getFornecedor());
			}
			
			ordemcompra.setListaMaterialTrans(list);
			
			buscaEnderecoFornecedor(ordemcompra.getFornecedor());
			buscaTelefoneContatoFornecedor(ordemcompra.getContato());
			
			ordemcompra.setObservacaoGrupomaterial(materialgrupoService.makeStringObservacao(ordemcompramaterialService.findByOrdemcompra(ordemcompra)));
			
			List<Ordemcomprahistorico> listaHistorico = new ArrayList<Ordemcomprahistorico>();
			listaHistorico = ordemcomprahistoricoService.getListaOrdemCompraHistoricoForReport(ordemcompra);
			
			Report report = new Report("/suprimento/emissaoordemcompra_" + modelo);
			Report subreport1 = new Report("suprimento/sub_emissaoordemcompra_assinatura_" + (modelo.equals("paisagem2") ? "paisagem" : modelo));
			Report subreport3 = new Report("suprimento/sub_emissaoordemcompra_inspecao");
			
			Ordemcomprahistorico ordemcomprahistorico = ordemcomprahistoricoService.getPrimeiroHistorico(ordemcompra);
			
			String dtcriacao = SinedDateUtils.toString(ordemcompra.getDtcriacao());
			if(ordemcomprahistorico != null){
				dtcriacao = SinedDateUtils.toString(ordemcomprahistorico.getDtaltera());
			}
			
			String dtcriacaoOc = SinedDateUtils.toString(ordemcompra.getDtcriacao(), "yy");
			if(ordemcomprahistorico != null){
				dtcriacaoOc = SinedDateUtils.toString(ordemcomprahistorico.getDtaltera(), "yy");
			}
			
			report.addParameter("DTEMISSAO", (ordemcompra.getDtcriacao() != null ? ordemcompra.getDtcriacao() : dtemissao));
			report.addParameter("DTCRIACAO", dtcriacao);
			report.addParameter("DTCRIACAOOC", dtcriacaoOc);
			
			for (Ordemcomprahistorico och : listaHistorico) {
				if (och.getCdusuarioaltera() != null){
					Usuario usuario = new Usuario();
					usuario.setCdpessoa(och.getCdusuarioaltera());
					if (och.getOrdemcompraacao().equals(Ordemcompraacao.CRIADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						subreport1.addParameter("CRIADA", usuario.getNome().toUpperCase());
					} else if (och.getOrdemcompraacao().equals(Ordemcompraacao.AUTORIZADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						subreport1.addParameter("AUTORIZADA", usuario.getNome().toUpperCase());
					} else if (och.getOrdemcompraacao().equals(Ordemcompraacao.APROVADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						subreport1.addParameter("APROVADA", usuario.getNome().toUpperCase());
					}
				}
			}
			
			report.addSubReport("SUB_EMISSAOORDEMCOMPRA", subreport1);
			report.addSubReport("SUB_EMISSAOORDEMCOMPRA_INSPECAO", subreport3);
			
			Report subreport2 = new Report("suprimento/sub_emissaoordemcompra_" + (modelo.equals("paisagem2") ? "paisagem" : modelo));
			
			report.addParameter("CODIGO_ALTERNATIVO_DO_FORNECEDOR_OC", parametrogeralService.getBoolean(Parametrogeral.ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC));
			report.addParameter("OBSERVACAO", ordemcompra.getObservacao());
			report.addParameter("EXIBIR_INSPECAO", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INSPECAOORDEMCOMPRA)));
			report.addParameter("INSPECAO", Util.strings.emptyIfNull(ordemcompra.getInspecao()));
			report.addParameter("OBSERVACAOGRUPOMATERIAL", ordemcompra.getObservacaoGrupomaterial());
			report.addParameter("CDORDEMCOMPRA", ordemcompra.getCdordemcompra());
			report.addParameter("CONTATO", ordemcompra.getContato());
			report.addParameter("FORNECEDOR", ordemcompra.getFornecedor());
			
			if(ordemcompra.getFornecedoroptriangular() != null){
				Localarmazenagem la = new Localarmazenagem();
				StringBuilder nome = new StringBuilder();
				nome.append(ordemcompra.getFornecedoroptriangular().getRazaosocial() != null ? ordemcompra.getFornecedoroptriangular().getRazaosocial() + " ": "")
					.append(ordemcompra.getFornecedoroptriangular().getCpfOuCnpj() != null ? ordemcompra.getFornecedoroptriangular().getCpfOuCnpj() : "");
					
				la.setNome(nome.toString());
				la.setEndereco(ordemcompra.getEnderecoentrega());
				ordemcompra.setLocalarmazenagem(la);
			}else{
				ordemcompra.setLocalarmazenagem(localarmazenagemService.loadForEntrada(ordemcompra.getLocalarmazenagem()));
			}
			if(modelo.equals("paisagem") || modelo.equals("paisagem2")){
				report.addParameter("LOCALARMAZENAGEM", ordemcompra.getLocalarmazenagem());
			}else{
				subreport2.addParameter("LOCALARMAZENAGEM", ordemcompra.getLocalarmazenagem());
			}
			report.addParameter("TRANSPORTADOR", ordemcompra.getTransportador() != null && ordemcompra.getTransportador().getNome() != null ? ordemcompra.getTransportador().getNome() : "");
			report.addParameter("TRANSPORTADORTELEFONE", ordemcompra.getTransportador() != null && ordemcompra.getTransportador().getTelefone() != null ? ordemcompra.getTransportador().getTelefone().getTelefone() : "");
			
			subreport2.addParameter("FRETE", ordemcompra.getFrete());
			subreport2.addParameter("DESCONTO", ordemcompra.getDesconto());
			subreport2.addParameter("PRAZOPAGAMENTO", ordemcompra.getPrazopagamento());
			subreport2.addParameter("DTPROXIMAENTREGA", ordemcompra.getDtproximaentrega());
			subreport2.addParameter("TIPOFRETE", ordemcompra.getTipofrete());
			subreport2.addParameter("GARANTIA", ordemcompra.getGarantia());
			subreport2.addParameter("DOCUMENTOTIPO", ordemcompra.getDocumentotipo());
			subreport2.addParameter("ICMSST", new Double(ordemcompra.getValoricmsst() != null ? ordemcompra.getValoricmsst().getValue().doubleValue() : 0d));
			subreport2.addParameter("TOTALOUTRASDESPESAS", ordemcompra.getValorTotalOutrasDespesas());
			
			report.addSubReport("SUB_EMISSAOORDEMCOMPRA_" + (modelo.equals("paisagem2") ? "paisagem" : modelo).toUpperCase(), subreport2);
			
			report.setDataSource(ordemcompra.getListaMaterialTrans());
			
			Cliente cliente = null;
			if(ordemcompra.getFaturamentocliente() != null && ordemcompra.getFaturamentocliente() && ordemcompra.getRateio() != null &&
					ordemcompra.getRateio().getCdrateio() != null){
				List<Rateioitem> itensRateio = rateioitemService.findByRateioWithProjetoCliente(ordemcompra.getRateio());
				Projeto projeto = null;
				if(itensRateio != null && !itensRateio.isEmpty()){
					for(Rateioitem rateioitem : itensRateio){
						if(rateioitem.getProjeto() != null){
							if(projeto == null){
								projeto = rateioitem.getProjeto();
							}else if(!projeto.equals(rateioitem.getProjeto())){
								projeto = null;
								break;
							}
						}
					}
					if(projeto != null && projeto.getCliente() != null){
						cliente = projeto.getCliente();
					}
				}
			}
			
			Map<String, String> map = buscaProjetosContasgerenciaisRateioOrdemCompra(ordemcompra.getRateio());
			if(map != null){
				report.addParameter("PROJETOS", map.get("PROJETOS"));
				report.addParameter("CONTASGERENCIAIS", map.get("CONTASGERENCIAIS"));
			}
			report.addParameter("TITULO", "ORDEM DE COMPRA");
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			Image imageRubricaElaboracao = null;
			Usuario usuarioElaboracao = null;
			Ordemcomprahistorico oh1 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.CRIADA);
			
			
			Image imageRubricaAutorizacao = null;
			Usuario usuarioAutorizacao = null;
			Ordemcomprahistorico oh2 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.AUTORIZADA);
			
			try{
				if(oh1 != null && oh1.getCdusuarioaltera() != null){
					usuarioElaboracao = usuarioService.carregaRubricaUsuario(new Usuario(oh1.getCdusuarioaltera()));
					if(usuarioElaboracao != null && usuarioElaboracao.getRubrica() != null){
						Arquivo rubrica = usuarioElaboracao.getRubrica();
						rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
						imageRubricaElaboracao = ArquivoService.getInstance().loadAsImage(rubrica);
					}
				}
				
				if(oh2 != null && oh2.getCdusuarioaltera() != null){
					usuarioAutorizacao = usuarioService.carregaRubricaUsuario(new Usuario(oh2.getCdusuarioaltera()));
					if(usuarioAutorizacao != null && usuarioAutorizacao.getRubrica() != null){
						Arquivo rubrica = usuarioAutorizacao.getRubrica();
						rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
						imageRubricaAutorizacao = ArquivoService.getInstance().loadAsImage(rubrica);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			report.addParameter("USUARIO_RUBRICA_ELABORACAO", imageRubricaElaboracao);
			report.addParameter("USUARIO_RUBRICA_AUTORIZACAO", imageRubricaAutorizacao);
			
			if(new SinedUtil().needAprovacao()){
				Image imageRubricaAprovada = null;
				Usuario usuarioAprovada = null;
				Ordemcomprahistorico oh3 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.APROVADA);
				
				try{
					if(oh3 != null && oh3.getCdusuarioaltera() != null){
						usuarioAprovada = usuarioService.carregaRubricaUsuario(new Usuario(oh3.getCdusuarioaltera()));
						if(usuarioAprovada != null && usuarioAprovada.getRubrica() != null){
							Arquivo rubrica = usuarioAprovada.getRubrica();
							rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
							imageRubricaAprovada = ArquivoService.getInstance().loadAsImage(rubrica);
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				report.addParameter("USUARIO_RUBRICA_APROVADA", imageRubricaAprovada);
				
				report.addParameter("NEED_APROVACAO", Boolean.TRUE);
			} else {
				report.addParameter("NEED_APROVACAO", Boolean.FALSE);
			}
			
			Empresa empresaDe = null;
			Endereco enderecoDe = null;
			String telefoneEmpresaClienteDE = "";
			if(ordemcompra.getEmpresa() != null){
				
				enderecoEmpresa = enderecoService.carregaEnderecoAtivoEmpresa(ordemcompra.getEmpresa());
				
				if(!SinedUtil.isAmbienteDesenvolvimento()){
					Image image = SinedUtil.getLogo(ordemcompra.getEmpresa());
					report.addParameter("LOGO", image);										
				}
				
				report.addParameter("EMPRESA", ordemcompra.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
				report.addParameter("CPFCNPJ", ordemcompra.getEmpresa().getCpfCnpjProprietarioPrincipalOuEmpresa());
				report.addParameter("LABEL_CPFCNPJ", Tipopessoa.PESSOA_FISICA.equals(ordemcompra.getEmpresa().getTipopessoa())? "CPF:": "CNPJ:");
				report.addParameter("EMAIL", ordemcompra.getEmpresa().getEmailordemcompra() != null && !ordemcompra.getEmpresa().getEmailordemcompra().equals("") ? ordemcompra.getEmpresa().getEmailordemcompra() : ordemcompra.getEmpresa().getEmail());
				report.addParameter("INSCRICAOESTADUAL", ordemcompra.getEmpresa().getInscricaoestadual());
				if(ordemcompra.getEmpresa().getListaTelefone() !=null || !ordemcompra.getEmpresa().getListaTelefone().isEmpty()){
					telefone =  telefone + CollectionsUtil.listAndConcatenate( ordemcompra.getEmpresa().getListaTelefone(), "telefone", " / ");
				}
				report.addParameter("EMPRESAFONE", telefone);
				if(enderecoEmpresa != null)
					report.addParameter("endereco", enderecoEmpresa);
				
				empresaDe = empresaService.loadEmpresa(ordemcompra.getEmpresa());
				enderecoDe = enderecoEmpresa;
			} else if(empresaPrincipal != null){
				
				Image image = SinedUtil.getLogo(empresaPrincipal);
				report.addParameter("LOGO", image);
				report.addParameter("EMPRESA", empresaPrincipal.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
				report.addParameter("CPFCNPJ", empresaPrincipal.getCpfCnpjProprietarioPrincipalOuEmpresa());
				report.addParameter("LABEL_CPFCNPJ", Tipopessoa.PESSOA_FISICA.equals(empresaPrincipal.getTipopessoa())? "CPF:": "CNPJ:");
				report.addParameter("EMAIL", empresaPrincipal.getEmailordemcompra() != null && !empresaPrincipal.getEmailordemcompra().equals("") ? empresaPrincipal.getEmailordemcompra() : empresaPrincipal.getEmail());
				report.addParameter("INSCRICAOESTADUAL", empresaPrincipal.getInscricaoestadual());
				if(empresaPrincipal.getListaTelefone() !=null || !empresaPrincipal.getListaTelefone().isEmpty()){
					telefone =  telefone + CollectionsUtil.listAndConcatenate( empresaPrincipal.getListaTelefone(), "telefone", " / ");
					telefoneEmpresaClienteDE = telefone;
				}
				report.addParameter("EMPRESAFONE", telefone);
				if(enderecoEmpresaPrincipal != null)
					report.addParameter("endereco", enderecoEmpresaPrincipal);
				
				empresaDe = empresaPrincipal;
				enderecoDe = enderecoEmpresaPrincipal;
			}
			
			if(cliente != null){
				enderecoDe = cliente.getEndereco();
				
				report.addParameter("EMPRESACLIENTEDE", cliente.getNome());
				report.addParameter("CPFCNPJDE", cliente.getCpfOuCnpj());
				report.addParameter("LABEL_CPFCNPJDE", Tipopessoa.PESSOA_FISICA.equals(cliente.getTipopessoa())? "CPF:": "CNPJ:");
				report.addParameter("EMAILDE", cliente.getEmail() != null && !cliente.getEmail().equals("") ? cliente.getEmail() : null);
				report.addParameter("INSCRICAOESTADUALDE", cliente.getInscricaoestadual());
				if(cliente.getListaTelefone() !=null || !cliente.getListaTelefone().isEmpty()){
					telefoneEmpresaClienteDE =  CollectionsUtil.listAndConcatenate( cliente.getListaTelefone(), "telefone", " / ");
				}
				report.addParameter("EMPRESAFONEDE", telefoneEmpresaClienteDE);
				if(enderecoDe != null)
					report.addParameter("enderecoDE", enderecoDe);
			}else if(empresaDe != null) {
				report.addParameter("EMPRESACLIENTEDE", empresaDe.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
				report.addParameter("CPFCNPJDE", empresaDe.getCpfCnpjProprietarioPrincipalOuEmpresa());
				report.addParameter("LABEL_CPFCNPJDE", Tipopessoa.PESSOA_FISICA.equals(empresaDe.getTipopessoa())? "CPF:": "CNPJ:");
				report.addParameter("EMAILDE", empresaDe.getEmailordemcompra() != null && !empresaDe.getEmailordemcompra().equals("") ? empresaDe.getEmailordemcompra() : empresaDe.getEmail());
				report.addParameter("INSCRICAOESTADUALDE", empresaDe.getInscricaoestadual());
				if(empresaDe.getListaTelefone() !=null || !empresaDe.getListaTelefone().isEmpty()){
					telefoneEmpresaClienteDE =  CollectionsUtil.listAndConcatenate( empresaDe.getListaTelefone(), "telefone", " / ");
				}
				report.addParameter("EMPRESAFONEDE", telefoneEmpresaClienteDE);
				if(enderecoDe != null)
					report.addParameter("enderecoDE", enderecoDe);
			}
			
			mergeReport.addReport(report);
		}

		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que busca itens do rateio da ordem de compra e concatena os nomes dos projetos
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio(Rateio)
	 * @param rateio
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Map<String, String> buscaProjetosContasgerenciaisRateioOrdemCompra(Rateio rateio) {
		
		if(rateio == null)
			return null;
		
		List<Rateioitem> itensRateio = rateioitemService.findByRateio(rateio);
		if(itensRateio == null || itensRateio.size() == 0)
			return null;
		
		Locale locale = new Locale("pt", "BR");
		NumberFormat format = NumberFormat.getInstance(locale);
		
		Map<Projeto, Double> mapPrj = new HashMap<Projeto, Double>();
		Map<Contagerencial, Double> mapCg = new HashMap<Contagerencial, Double>();
		Map<Contagerencial, Money> mapCgValor = new HashMap<Contagerencial, Money>();
		
		Projeto projeto;
		Contagerencial contagerencial;	
		Double percent, aux;
		Money valor, auxVlr;
		for (Rateioitem ri : itensRateio) {
			projeto = ri.getProjeto();
			percent = ri.getPercentual();
			valor = ri.getValor();
			contagerencial = contagerencialService.findPaiNivel1(ri.getContagerencial());
			
			if(projeto != null){
				if(mapPrj.containsKey(projeto)){
					aux = mapPrj.get(projeto);
					aux += percent;
					mapPrj.put(projeto, aux);
				} else {
					mapPrj.put(projeto, percent);
				}
			}
			
			if(contagerencial != null){
				if(mapCg.containsKey(contagerencial)){
					aux = mapCg.get(contagerencial);
					aux += percent;
					mapCg.put(contagerencial, aux);
					
					auxVlr = mapCgValor.get(contagerencial);
					auxVlr = auxVlr.add(valor);
					mapCgValor.put(contagerencial, auxVlr);
				} else {
					mapCg.put(contagerencial, percent);
					mapCgValor.put(contagerencial, valor);
				}
			}
			
			projeto = null;
			percent = null;
			contagerencial = null;
			aux = null;
		}
		
		StringBuilder strPrj = new StringBuilder();
		List<Entry<Projeto, Double>> listPrj = new ArrayList<Entry<Projeto,Double>>(mapPrj.entrySet());
		Collections.sort(listPrj,new Comparator<Entry<Projeto, Double>>(){
			public int compare(Entry<Projeto, Double> o1, Entry<Projeto, Double> o2) {
				return o1.getKey().getNome().compareTo(o2.getKey().getNome());
			}
		});
		
		for (Entry<Projeto, Double> entry : listPrj) {
			strPrj.append(entry.getKey().getNome()+" ("+format.format(entry.getValue())+"%)\n");
		}
		
		if(!strPrj.toString().equals("")){
			strPrj.delete(strPrj.toString().length()-1, strPrj.toString().length());
			
			if(listPrj.size() == 1){
				strPrj.delete(strPrj.toString().length()-6, strPrj.toString().length());
			}
		}
		
		StringBuilder strCg = new StringBuilder();
		List<Entry<Contagerencial, Double>> listCg = new ArrayList<Entry<Contagerencial,Double>>(mapCg.entrySet());
		Collections.sort(listCg,new Comparator<Entry<Contagerencial, Double>>(){
			public int compare(Entry<Contagerencial, Double> o1, Entry<Contagerencial, Double> o2) {
				return o1.getKey().getNome().compareTo(o2.getKey().getNome());
			}
		});
		
		for (Entry<Contagerencial, Double> entry : listCg) {
			strCg.append(entry.getKey().getNome()+" ("+format.format(entry.getValue())+"%) - R$ " + mapCgValor.get(entry.getKey()).toString() + "\n");
		}
		
		if(!strCg.toString().equals("")){
			strCg.delete(strCg.toString().length()-1, strCg.toString().length());
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("PROJETOS", strPrj.toString());
		map.put("CONTASGERENCIAIS", strCg.toString());
		
		return map;
	}
	
	/**
	 * M�todo que busca o telefone e fax do contato. Busca pelo telefone principal se n�o houver pega o primeiro
	 * que achar se ser fax.
	 * 
	 * @param contato
	 * @author Tom�s Rabelo
	 */
	private void buscaTelefoneContatoFornecedor(Contato contato) {
		//Busca telefones do contato
		if(contato != null){
			if(contato.getListaTelefone() != null && contato.getListaTelefone().size() > 0){
				for (Telefone telefone : contato.getListaTelefone()) {
					boolean achouTelPrincipal = false;
					if(contato.getFax() != null && achouTelPrincipal)
						break;
					
					if(!telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.FAX)){
						if(telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL))
							achouTelPrincipal = true;
						
						contato.setTelefonePrincipal(telefone);
					} else{
						contato.setFax(telefone);
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que busca o endere�o do fornecedor. Busca pelo primeiro que achar, pois se houver um �nico so poder� haver 1
	 * 
	 * @param fornecedor
	 * @author Tom�s Rabelo
	 */
	private void buscaEnderecoFornecedor(Fornecedor fornecedor) {
		//Busca o primeiro endere�o do fornecedor
		if(fornecedor.getListaEndereco() != null && fornecedor.getListaEndereco().size() > 0){
			for (Endereco endereco : fornecedor.getListaEndereco()) {
				fornecedor.setEnderecoPrincipal(endereco);
				break;
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#loadForBaixaEntrega
	 * @param entrega
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcompra loadForBaixaEntrega(Entrega entrega, Material material) {
		return ordemcompraDAO.loadForBaixaEntrega(entrega,material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#loadWithLista
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcompra loadWithLista(Ordemcompra ordemcompra){
		return ordemcompraDAO.loadWithLista(ordemcompra);
	}
	
	@Override
	public void delete(Ordemcompra bean) {
		bean = this.loadForEntrada(bean);
		Cotacao cotacao = bean.getCotacao();
		super.delete(bean);
		if(cotacao != null && cotacao.getCdcotacao() != null)
			cotacaoService.callProcedureAtualizaCotacao(cotacao);
	}
	
	@Override
	public void saveOrUpdate(final Ordemcompra bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				rateioService.saveOrUpdateNoUseTransaction(bean.getRateio());
				
				saveOrUpdateNoUseTransaction(bean);
				
				if(bean.getListaMaterial() != null){
					for (Ordemcompramaterial ocm : bean.getListaMaterial()) {
						if(ocm.getCdordemcompramaterial() != null){
							Set<Solicitacaocompraordemcompramaterial> listaSCOCM = ocm.getListaSolicitacaocompraordemcompramaterial();
							if(SinedUtil.isListNotEmpty(listaSCOCM)){
								for (Solicitacaocompraordemcompramaterial scocm : listaSCOCM) {
									scocm.setOrdemcompramaterial(ocm);
									solicitacaocompraordemcompramaterialService.saveOrUpdateNoUseTransaction(scocm);
								}
							}
						}
					}
				}
				
				if(bean.getCdordemcompra() == null){
					List<Aviso> listaAvisos = new ListSet<Aviso>(Aviso.class);
					montaAvisosOrdemCompra(listaAvisos, MotivoavisoEnum.AUTORIZAR_ORDEMCOMPRA, bean, "Aguardando autoriza��o");
					avisoService.salvarAvisos(listaAvisos, true);
				}
				
				if(bean.getOrdemcomprahistorico() !=  null)
					ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(bean.getOrdemcomprahistorico());
				
				return status;
			}
		});
		
		this.callProcedureAtualizaOrdemcompra(bean);
	}
	
	/**
	 * M�todo que salva hist�rico da ordem de compra e tamb�m da update na situa��o da ordem de compra
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#doUpdatePedidoSolicitacao(Solicitacaocompra, Situacaosuprimentos)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#setNullFluxocaixa(Integer)
	 * @see br.com.linkcom.sined.geral.service.FluxocaixaService#delete(Fluxocaixa)
	 * @see br.com.linkcom.sined.geral.service.OrdemcomprahistoricoService#saveOrUpdateNoUseTransaction
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#doUpdateSituacaoOrdens(String, Situacaosuprimentos)
	 * @see br.com.linkcom.sined.geral.service.AvisoService#saveOrUpdateNoUseTransaction(Aviso)
	 * @param ordemcomprahistorico
	 * @param acao
	 * @param solicitacoesOrdemCompra 
	 * @author Tom�s Rabelo
	 * @param listaAvisos 
	 * @param listaPapeis 
	 */
	public void saveHistoricoEUpdateSituacao(final Ordemcomprahistorico ordemcomprahistorico, final Ordemcompraacao acao, final Situacaosuprimentos situacaosuprimentos, final List<Solicitacaocompra> solicitacoesOrdemCompra, final List<Aviso> listaAvisos) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String[] ids = ordemcomprahistorico.getIds().split(",");
				
				if(solicitacoesOrdemCompra != null && solicitacoesOrdemCompra.size() > 0)
					for (Solicitacaocompra solicitacaocompra : solicitacoesOrdemCompra) 
						solicitacaocompraService.doUpdateCompraSolicitacao(solicitacaocompra, Solicitacaocompra.PEDIDO);
				
				if(situacaosuprimentos.equals(Situacaosuprimentos.ESTORNAR)){
					List<Ordemcompra> lista = findWithFluxocaixa(ordemcomprahistorico.getIds());
					for (Ordemcompra ordemcompra : lista) {
						if(ordemcompra != null && ordemcompra.getListaFluxocaixaorigem() != null && !ordemcompra.getListaFluxocaixaorigem().isEmpty()){
//							setNullFluxocaixa(ordemcompra.getCdordemcompra());
							for (Fluxocaixaorigem fluxocaixaorigem : ordemcompra.getListaFluxocaixaorigem()) {
								fluxocaixaService.delete(fluxocaixaorigem.getFluxocaixa());
								rateioService.delete(fluxocaixaorigem.getFluxocaixa().getRateio());
							}
						}
					}	
				}
				
				for (int i = 0; i < ids.length; i++) {
					Ordemcomprahistorico historico = new Ordemcomprahistorico(new Ordemcompra(new Integer(ids[i])),
							acao, ordemcomprahistorico.getObservacao());
				
					ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(historico);
				}
				
				ordemcompraDAO.doUpdateSituacaoOrdens(ordemcomprahistorico.getIds(), situacaosuprimentos);
				
				if(listaAvisos != null && listaAvisos.size() > 0){
					avisoService.salvarAvisos(listaAvisos, true);
				}
					
						
				return status;
			}
		});
		
		this.callProcedureAtualizaOrdemcompra(ordemcomprahistorico.getIds());
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#setNullFluxocaixa
	 * @param cdordemcompra
	 * @author Rodrigo Freitas
	 
	private void setNullFluxocaixa(Integer cdordemcompra) {
		ordemcompraDAO.setNullFluxocaixa(cdordemcompra);
	}*/

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findWithFluxocaixa
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Ordemcompra> findWithFluxocaixa(String ids) {
		return ordemcompraDAO.findWithFluxocaixa(ids);
	}
	/**
	 * M�todo monta a origem dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Ordemcompra form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getCotacao() != null){
			if(form.getCotacao().getCdcotacao() != null){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.COTACAO, "COTA��O", form.getCotacao().getCdcotacao().toString(), null);
				
				List<Cotacaoorigem> listaCotacaoorigem = cotacaoorigemService.findSolicitacoesByCotacao(form.getCotacao());
				if(listaCotacaoorigem != null && !listaCotacaoorigem.isEmpty()){
					for(Cotacaoorigem cotacaoorigem : listaCotacaoorigem){
						if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getEmpresa() != null && 
								cotacaoorigem.getSolicitacaocompra().getEmpresa().getCdpessoa() != null){
							form.setEmpresacalculorateio(cotacaoorigem.getSolicitacaocompra().getEmpresa());
						}
					}
				}
				listaBean.add(origemsuprimentosBean);
			}
		} else if(form.getListaOrdemcompraorigem() != null && !form.getListaOrdemcompraorigem().isEmpty()){
			for(Ordemcompraorigem ordemcompraorigem : form.getListaOrdemcompraorigem()){
				if(ordemcompraorigem.getSolicitacaocompra() != null && ordemcompraorigem.getSolicitacaocompra().getCdsolicitacaocompra() != null){
					origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.SOLICITACAOCOMPRA, "SOLICITA��O DE COMPRA", ordemcompraorigem.getSolicitacaocompra().getCdsolicitacaocompra().toString(), ordemcompraorigem.getSolicitacaocompra().getIdentificadorOuCdsolicitacaocompra());
					listaBean.add(origemsuprimentosBean);
				}else if(ordemcompraorigem.getPlanejamento() != null && ordemcompraorigem.getPlanejamento().getCdplanejamento() != null){
					origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PLANEJAMENTO, "PLANEJAMENTO", ordemcompraorigem.getPlanejamento().getCdplanejamento().toString(), ordemcompraorigem.getPlanejamento().getDescricao());
					listaBean.add(origemsuprimentosBean);
				}
			}
		}else {
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.CADASTRO, "CADASTRO");
			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo monta a destino dependendo do valor
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findByOrdemcompra(Ordemcompra)
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaDestino(Ordemcompra form) {
		List<Entrega> listaEntrega = entregaService.findByOrdemcompra(form);
		List<OrigemsuprimentosBean> listaBean = null;
		
		if(listaEntrega != null && listaEntrega.size() > 0){
			listaBean = new ArrayList<OrigemsuprimentosBean>();
			for (Entrega entrega : listaEntrega) {
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTREGA, "ENTREGA", entrega.getCdentrega().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo que tenta montar rateio para ordem de compra a partir da cota��o
	 * 
	 * @see br.com.linkcom.sined.geral.service.cotacaoService#getCotacaoDaOrdemCompraParaRateio(ordemcompra)
	 * @see #valorTotalDeCadaMaterial(Set, Set)	
	 * @see #calculaRateioUtilizandoCotacao(HashMap, Cotacao, Set, Money)
	 * @see br.com.linkcom.sined.geral.service.RateioService#findRateio(Rateio)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @param ordemcompra
	 * @author Tom�s T. Rabelo
	 */
	public void calculaRateioAPartirDaCotacao(Ordemcompra ordemcompra) {
		
		Rateio rateio = null;
		Double valorTotalOrdemCompra = ordemcompra.getValorTotalOrdemCompra();
		
		if((ordemcompra.getCotacao() != null && ordemcompra.getCotacao().getCdcotacao() != null) && (ordemcompra.getRateio() == null || ordemcompra.getRateio().getCdrateio() == null)){
			Cotacao cotacao = cotacaoService.getCotacaoDaOrdemCompraParaRateio(ordemcompra);
			
			HashMap<Integer, Double> map = valorTotalDeCadaMaterial(cotacao.getListaOrigem(), ordemcompra.getListaMaterial());
			
			rateio = calculaRateioUtilizandoCotacao(map, cotacao, ordemcompra.getListaMaterial(), new Money(valorTotalOrdemCompra));
			
			ordemcompra.setRateio(rateio);
		} else{
			
			if(ordemcompra.getRateio() != null && ordemcompra.getRateio().getCdrateio() != null){
				//Foi criado uma query porque na query da entrada n�o esta buscando os valores corretos da lista
				rateio = rateioService.findRateio(ordemcompra.getRateio());
				ordemcompra.setRateio(rateio);
			}
		}
		
		rateioService.calculaValoresRateio(rateio, new Money(valorTotalOrdemCompra));
	}
	
	/**
	 * M�todo que calcula o rateio utilizando a origem (cota��o)
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#obtemCentrocuso(Cotacaoorigem)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#obtemProjeto(Cotacaoorigem)
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#verificaItemRateioIgual(Rateioitem, Rateioitem)
	 * @param map
	 * @param cotacao
	 * @param listaMaterialOrdemCompra
	 * @param valorTotalOrdemCompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Rateio calculaRateioUtilizandoCotacao(HashMap<Integer, Double> map, Cotacao cotacao, List<Ordemcompramaterial> listaMaterialOrdemCompra, Money valorTotalOrdemCompra) {
		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
		
		for (Cotacaoorigem cotacaoorigem : cotacao.getListaOrigem()) {
			Solicitacaocompra solicitacaocompra = cotacaoorigem.getSolicitacaocompra();
			
			for (Ordemcompramaterial ordemcompramaterial : listaMaterialOrdemCompra) {
				if(ordemcompramaterial.getMaterial().equals(solicitacaocompra.getMaterial())){
					Centrocusto centrocusto = obtemCentrocuso(cotacaoorigem);
					Projeto projeto = obtemProjeto(cotacaoorigem);
					
					Double valorTotalDoMaterial = map.get(ordemcompramaterial.getMaterial().getCdmaterial());
					
					double percentualMaterial = solicitacaocompra.getQtde().doubleValue() / valorTotalDoMaterial;
					
					Money valorRateioitem = new Money(SinedUtil.truncate(ordemcompramaterial.getValor() * percentualMaterial));
					
//					Rateioitem rateioitem = new Rateioitem(cotacaoorigem.getSolicitacaocompra().getMaterial().getContagerencial(),
//														   centrocusto,
//														   projeto,
//														   valorRateioitem);
					Rateioitem rateioitem = new Rateioitem();
					if(cotacaoorigem.getSolicitacaocompra().getContagerencial() != null){
						rateioitem = new Rateioitem(cotacaoorigem.getSolicitacaocompra().getContagerencial(),
								   centrocusto,
								   projeto,
								   valorRateioitem);
					}else {
						rateioitem = new Rateioitem(cotacaoorigem.getSolicitacaocompra().getMaterial().getContagerencial(),
								   centrocusto,
								   projeto,
								   valorRateioitem);
					}
					
					boolean rateioExistente = false;
					for (Rateioitem rateioitemaux : itens) 
						if(rateioitemService.verificaItemRateioIgual(rateioitem, rateioitemaux)){
							rateioitemaux.setValor(rateioitemaux.getValor().add(rateioitem.getValor()));
							rateioExistente = true;
							break;
						}

					if(!rateioExistente)
						itens.add(rateioitem);
				}
			}
		}
		
		for (Rateioitem rateioitem : itens) {
			rateioitem.setPercentual(this.calculaPercentual(valorTotalOrdemCompra, rateioitem.getValor()));
		}	
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(itens);
		return rateio;
	}
	
//	/**
//	 * M�todo que tenta calcular o rateio ao gerar ordem de compra a partir da cota��o 
//	 * (isto � feito para salvar o rateio no momento de salvar a ordem de compra a partir da cota��o)
//	 *
//	 * @param ordemcompra
//	 * @author Luiz Fernando
//	 */
//	public void calculaRateioAPartirDaCotacaoForGerarordemcompra(Ordemcompra ordemcompra) {
//		if((ordemcompra.getCotacao() != null && ordemcompra.getCotacao().getCdcotacao() != null) && (ordemcompra.getRateio() == null || ordemcompra.getRateio().getCdrateio() == null)){
//			Rateio rateio = null;
//			Double valorTotalOrdemCompra = ordemcompra.getValorTotalOrdemCompra();
//			Cotacao cotacao = cotacaoService.getCotacaoDaOrdemCompraParaRateio(ordemcompra.getCotacao());
//			
//			HashMap<Integer, Double> map = valorTotalDeCadaMaterial(cotacao.getListaOrigem(), ordemcompra.getListaMaterial());
//			
//			rateio = calculaRateioUtilizandoCotacao(map, cotacao, ordemcompra.getListaMaterial(), new Money(valorTotalOrdemCompra));
//			
//			if(rateio != null && rateio.getListaRateioitem() != null && !rateio.getListaRateioitem().isEmpty()){
//				boolean centrocustovazio = false;
//				for(Rateioitem rateioitem : rateio.getListaRateioitem()){
//					if(rateioitem.getCentrocusto() == null || rateioitem.getCentrocusto().getCdcentrocusto() == null){
//						centrocustovazio = true;
//						break;
//					}
//				}
//				if(!centrocustovazio){
//					ordemcompra.setRateio(rateio);
//					rateioService.calculaValoresRateio(rateio, new Money(valorTotalOrdemCompra));
//				}
//			}
//		} 		
//	}
	
	/**
	 * M�todo que calcula o percentual de dois moneys.
	 * 
	 * @param totalmoney
	 * @param partemoney
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Double calculaPercentual(Money totalmoney, Money partemoney) {
		Double total = totalmoney.getValue().doubleValue();
		Double parte = partemoney.getValue().doubleValue();
		
		return SinedUtil.truncate((parte*100)/total);
	}
	
	/**
	 * M�todo que acumula a qtde total de material de cada conta gerencial pedida na solicita��o de compra
	 * 
	 * @param listaOrigem
	 * @param listaMaterialOrdemCompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	private HashMap<Integer, Double> valorTotalDeCadaMaterial(Set<Cotacaoorigem> listaOrigem, List<Ordemcompramaterial> listaMaterialOrdemCompra) {
		HashMap<Integer, Double> map = new HashMap<Integer, Double>();
		for (Cotacaoorigem cotacaoorigem : listaOrigem) {
			Material material = cotacaoorigem.getSolicitacaocompra().getMaterial();
			for (Ordemcompramaterial ordemcompramaterial : listaMaterialOrdemCompra) {
				if(material.equals(ordemcompramaterial.getMaterial())){
					if(map.containsKey(material.getCdmaterial())){
						map.put(material.getCdmaterial(), map.get(material.getCdmaterial()) + ordemcompramaterial.getQtdpedida());
					} else{
						map.put(material.getCdmaterial(), ordemcompramaterial.getQtdpedida());
					}
				}
			}
		}
		
		return map;
	}
	/**
	 * Busca projeto da solicita��o de compra. Se n�o achar tenta buscar da requisi��o de material
	 * 
	 * @param cotacaoorigem
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Projeto obtemProjeto(Cotacaoorigem cotacaoorigem) {
		if(cotacaoorigem.getSolicitacaocompra().getProjeto() != null){
			return cotacaoorigem.getSolicitacaocompra().getProjeto();
		} else if(cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial() != null){
			if(cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial().getProjeto() != null){
				return cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial().getProjeto();
			}
		}
		return null;
	}
	/**
	 * Busca centro de custo da solicita��o de compra. Se n�o achar tenta buscar da requisi��o de material
	 * 
	 * @param cotacaoorigem
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Centrocusto obtemCentrocuso(Cotacaoorigem cotacaoorigem) {
		if(cotacaoorigem.getSolicitacaocompra().getCentrocusto() != null){
			return cotacaoorigem.getSolicitacaocompra().getCentrocusto();
		} else if(cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial() != null){
			if(cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial().getCentrocusto() != null){
				return cotacaoorigem.getSolicitacaocompra().getSolicitacaocompraorigem().getRequisicaomaterial().getCentrocusto();
			}
		}
		return null;
	}
	
	/**
	 * M�todo que gera um fluxo de caixa para cada ordem de compra e muda sua situa��o para autorizada
	 * 
	 * @see br.com.linkcom.sined.geral.service.FluxocaixaTipoService#getFluxoCaixaTipoOrdemCompra()
	 * @see br.com.linkcom.sined.geral.service.FluxocaixaTipoService#saveOrUpdateNoUseTransaction(FluxocaixaTipo)
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoService#getPrazoPagamentoUnico()
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoService#saveOrUpdateNoUseTransaction(Prazopagamento)
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio(Rateio)
	 * @see br.com.linkcom.sined.geral.service.FluxocaixaService#saveOrUpdateNoUseTransaction(Fluxocaixa)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#doUpdateAutorizadaGerandoFluxoCaixaOrdens(List)
	 * @see br.com.linkcom.sined.geral.service.AvisoService#saveOrUpdateNoUseTransaction(Aviso)
	 * @param ordens
	 * @author Tom�s T. Rabelo
	 * @param listaAvisos 
	 */
	public String doUpdateAutorizadaGerandoFluxoCaixaOrdens(final List<Ordemcompra> ordens, final List<Aviso> listaAvisos) {
		Object ids = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String msg = "/Erro:";
				FluxocaixaTipo fluxocaixaTipo = fluxocaixaTipoService.getFluxoCaixaTipoOrdemCompra();
				if(fluxocaixaTipo == null){
					fluxocaixaTipo = new FluxocaixaTipo("Ordem de compra", "OC");
					fluxocaixaTipoService.saveOrUpdateNoUseTransaction(fluxocaixaTipo);
				}
				
//				Prazopagamento prazopagamento = prazopagamentoService.getPrazoPagamentoUnico();
//				if(prazopagamento == null){
//					List<Prazopagamentoitem> list = new ArrayList<Prazopagamentoitem>();
//					Prazopagamentoitem prazopagamentoitem = new Prazopagamentoitem(1, 0, 0);
//					list.add(prazopagamentoitem);
//					
//					prazopagamento = new Prazopagamento("�nica", Boolean.TRUE, list);
//					
//					prazopagamentoService.saveOrUpdateNoUseTransaction(prazopagamento);
//				}
//				prazopagamento = null;
				for (Ordemcompra ordemcompra : ordens) {
					List<Fluxocaixa> fluxos = fluxocaixaService.criaFluxoCaixasOrdemCompra(ordemcompra, fluxocaixaTipo);
					
					for (Fluxocaixa fluxocaixa : fluxos) 
						fluxocaixaService.saveOrUpdateNoUseTransaction(fluxocaixa);
					
					doUpdateOrdemCompraAutorizar(ordemcompra);
					ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(new Ordemcomprahistorico(ordemcompra, Ordemcompraacao.AUTORIZADA));
				}
				
				if(listaAvisos != null && listaAvisos.size() > 0){
					avisoService.salvarAvisos(listaAvisos, true);
				}
					
				
//				try {
//					enviaEmailGerencia(ordens);
//				} catch (Exception e) {
//					e.printStackTrace();
//					msg += "E-mail n�o enviado.";
//				}
				
				return CollectionsUtil.listAndConcatenate(ordens, "cdordemcompra", ",")+msg;
			}

//			/**
//			 * M�todo que envia email, caso necessite de aprova��o e tenha sido cadastrados emails no parametro geral do sistema.
//			 * 
//			 * @param ordens
//			 * @throws Exception
//			 * @author Tom�s Rabelo
//			 */
//			private void enviaEmailGerencia(List<Ordemcompra> ordens) throws Exception {
//				if(new SinedUtil().needAprovacao()){
//					String emails = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAEMAILORDEMDECOMPRA);
//					Empresa empresa = empresaService.loadPrincipal();
//					
//					if(emails != null && !emails.equals("")){
//						
//						for (Ordemcompra ordemcompra : ordens) {
//
//							String assunto = "Aprova��o da OC "+ordemcompra.getCdordemcompra();
//							String mensagem = "<b>Aviso do sistema:</b> A ordem de compra "
//											+ ordemcompra.getCdordemcompra()
//											+ " necessita de aprova��o. <BR><BR>"
//											+ "<a href=\""
//											+ SinedUtil.getUrlWithContext()
//											+ "/suprimento/crud/Ordemcompra?ACAO=consultar&cdordemcompra="
//											+ ordemcompra.getCdordemcompra()
//											+ "\">Ordem de compra</a>";
//
//							Envioemail envioemail = envioemailService.registrarEnvio(
//									empresa.getEmail(), 
//									assunto, 
//									mensagem, 
//									Envioemailtipo.AVISO_APROVACAO_ORDEM_COMPRA);
//							
//							String[] emailsAux = emails.split(";");
//							for (String destinatario : emailsAux){
//							
//								Envioemailitem envioemailitem = new Envioemailitem();
//								envioemailitem.setEnvioemail(envioemail);
//								envioemailitem.setEmail(destinatario);
//								envioemailitem.setPessoa(empresa);
//								envioemailitem.setNomecontato(empresa.getNome());
//								envioemailitemService.saveOrUpdate(envioemailitem);
//								
//								EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
//								email.setFrom(empresa.getEmail());
//								email.setSubject(assunto);
//								email.setTo(destinatario);
//								email.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinatario));
//								email.sendMessage();
//							}
//						}
//					}
//				}
//			}
		});
		
		this.callProcedureAtualizaOrdemcompra(ids.toString().split("/")[0]);
		return ids.toString().split("/")[1];
	}
	
	private void doUpdateAprovada(final List<Ordemcompra> listaOrdemcompra){
		Object ids = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Ordemcompra ordemcompra : listaOrdemcompra) {
					doUpdateOrdemCompraAprovar(ordemcompra);
					ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(new Ordemcomprahistorico(ordemcompra, Ordemcompraacao.APROVADA));
				}
				
				return CollectionsUtil.listAndConcatenate(listaOrdemcompra, "cdordemcompra", ",");
			}
		});
		
		this.callProcedureAtualizaOrdemcompra(ids.toString());
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @author Tom�s T. Rabelo
	 */
	private void doUpdateOrdemCompraAutorizar(Ordemcompra ordemcompra) {
		ordemcompraDAO.doUpdateOrdemCompraAutorizar(ordemcompra);
	}
	
	private void doUpdateOrdemCompraAprovar(Ordemcompra ordemcompra) {
		ordemcompraDAO.doUpdateOrdemCompraAprovar(ordemcompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public List<Ordemcompra> findOrdensParaAutorizar(String whereIn) {
		return ordemcompraDAO.findOrdensParaAutorizar(whereIn);
	}
	
	/**
	 * M�todo que faz a valida��o do rateio quando o checkbox � selecionado
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#getValorTotalContasGerenciais(Money, List)
	 * @see br.com.linkcom.sined.geral.service.MaterialService#load(Material, String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#getCodigosContasGerenciasDoRateio(HashMap)
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findContasGerenciais(String)
	 * @param bean
	 * @param errors
	 * @author Tom�s Rabelo
	 */
	public void validaRateio(Ordemcompra bean, BindException errors) {
		Money valorTotalRateio = new Money(0);
		
		for (Rateioitem rateioitem : bean.getRateio().getListaRateioitem()) {
			valorTotalRateio = valorTotalRateio.add(rateioitem.getValor());
		}
		
		if(!errors.hasErrors()){
			BigDecimal valorTotalOrdemCompraFloor = SinedUtil.roundFloor(new BigDecimal(bean.getValorTotalOrdemCompra()), 2);
			BigDecimal valorTotalOrdemCompra = SinedUtil.round(new BigDecimal(bean.getValorTotalOrdemCompra()), 2);
			BigDecimal valorTotalOrdemCompraUp = SinedUtil.roundUp(new BigDecimal(bean.getValorTotalOrdemCompra()), 2); 
			
			BigDecimal valorTotalRateioBigDecimal = valorTotalRateio.getValue();
			
			if(valorTotalOrdemCompraFloor.compareTo(valorTotalRateioBigDecimal) != 0 &&
					valorTotalOrdemCompra.compareTo(valorTotalRateioBigDecimal) != 0 &&
					valorTotalOrdemCompraUp.compareTo(valorTotalRateioBigDecimal) != 0){
				errors.reject("003", "O valor do rateio n�o � igual ao valor total da ordem de compra.");
			}
		}
	}
	
//	/**
//	 * M�todo que calcula o valor total de cada conta gerencial do rateio
//	 * 
//	 * @param valorTotalRateio
//	 * @param listaRateioitem
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	private HashMap<Integer, Money> getValorTotalContasGerenciais(Money valorTotalRateio, List<Rateioitem> listaRateioitem) {
//		HashMap<Integer, Money> map = new HashMap<Integer, Money>();
//		for (Rateioitem rateioitem : listaRateioitem) {
//			valorTotalRateio = valorTotalRateio.add(rateioitem.getValor());
//			
//			if(map.containsKey(rateioitem.getContagerencial().getCdcontagerencial())){
//				map.put(rateioitem.getContagerencial().getCdcontagerencial(), map.get(rateioitem.getContagerencial().getCdcontagerencial()).add(rateioitem.getValor()));
//			} else{
//				map.put(rateioitem.getContagerencial().getCdcontagerencial(), rateioitem.getValor());
//			}
//			
//		}
//		return map;
//	}
//	
//	/**
//	 * M�todo que cria um whereIn da contas gerenciais
//	 * 
//	 * @param map
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	private String getCodigosContasGerenciasDoRateio(HashMap<Integer, Money> map) {
//		StringBuilder codigos = new StringBuilder();
//		for (Integer cdcontagerencial : map.keySet())
//			codigos.append(cdcontagerencial+",");
//		
//		return codigos.delete(codigos.length()-1, codigos.length()).toString();
//	}
	
	/**
	 * M�todo calcula rateio quando o usu�rio clica em calcular rateio. 
	 * Obs: s� � possivel se a ordem de compra for gerada manualmente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#load(Material, String)
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void calculaRateio(Ordemcompra bean) {
		HashMap<String, Money> map = new HashMap<String, Money>();
		HashMap<Integer, Money> mapimpostos = new HashMap<Integer, Money>();
		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
		
		Empresa empresa = null;
		Contagerencial contagerencialicms = null;
		Contagerencial contagerencialicmsst = null;
		Contagerencial contagerencialipi = null;
		Contagerencial contagerencialiss = null;
		Contagerencial contagerencialfrete = null;
		
		if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
			empresa = empresaService.loadComContagerenciaiscompra(bean.getEmpresa());
		}else if(bean.getEmpresacalculorateio() != null && bean.getEmpresacalculorateio().getCdpessoa() != null){
			empresa = empresaService.loadComContagerenciaiscompra(bean.getEmpresacalculorateio());
		}
		
		Double valortotalipi = 0.0;
		Double valortotaliss = 0.0;
		Double valortotalicms = 0.0;
		
		Double qtdetotal = 0.0;
		if(bean.getDesconto() != null && bean.getDesconto().getValue().doubleValue() > 0){
			for (Ordemcompramaterial ordemcompramaterial : bean.getListaMaterial()) {
				if(ordemcompramaterial.getQtdpedida() != null && ordemcompramaterial.getQtdpedida() > 0){
					qtdetotal += ordemcompramaterial.getQtdpedida();
				}
			}
		}
		Contagerencial contagerencial = null;
		Centrocusto centrocusto = null;
		Projeto projeto = null;		
		for (Ordemcompramaterial ordemcompramaterial : bean.getListaMaterial()) {
			if(ordemcompramaterial.getMaterial() != null){
				contagerencial = null;
				centrocusto = ordemcompramaterial.getCentrocusto();
				projeto = ordemcompramaterial.getProjeto();
				
				Material m = materialService.load(ordemcompramaterial.getMaterial(), "material.produto, material.servico");
				if(ordemcompramaterial.getContagerencial() != null && ordemcompramaterial.getContagerencial().getCdcontagerencial() != null)
					contagerencial = ordemcompramaterial.getContagerencial();
				else{
					Material material = materialService.load(ordemcompramaterial.getMaterial(), "material.contagerencial");
					if(material != null){
						contagerencial = material.getContagerencial();
					}else{
						throw new SinedException("O material "+ ordemcompramaterial.getMaterial().getNome() + " n�o possui conta gerencial.");
					}
				}
				
				String whereInContagerencialCentrocustoProjeto = getwhereInContagerencialCentrocustoProjeto(contagerencial, centrocusto, projeto);
				
				Double valor = ordemcompramaterial.getValor();
				if(qtdetotal > 0 && ordemcompramaterial.getQtdpedida() != null){
					valor  = valor - (ordemcompramaterial.getQtdpedida() * bean.getDesconto().getValue().doubleValue() / qtdetotal);
				}
				if(map.containsKey(whereInContagerencialCentrocustoProjeto)){
					map.put(whereInContagerencialCentrocustoProjeto, map.get(whereInContagerencialCentrocustoProjeto).add(valor != null ? new Money(valor) : new Money(0)));
				} else{
					map.put(whereInContagerencialCentrocustoProjeto, valor != null ? new Money(valor) : new Money(0));
				}				
				
				if(ordemcompramaterial.getIpi() != null && ordemcompramaterial.getIpi() > 0 && ordemcompramaterial.getIpiincluso() != null && 
						!ordemcompramaterial.getIpiincluso()){
					valortotalipi += ordemcompramaterial.getIpi();
				}
				
				if(m != null && ordemcompramaterial.getIcmsiss() != null && ordemcompramaterial.getIcmsiss() > 0 && ordemcompramaterial.getIcmsissincluso() != null && 
						!ordemcompramaterial.getIcmsissincluso()){
					if(m.getServico() != null && m.getServico()){
						valortotaliss += ordemcompramaterial.getIcmsiss();
					}else if(m.getProduto() != null && m.getProduto()){
						valortotalicms += ordemcompramaterial.getIcmsiss();
					}
				}
			}
		}
		
		if(empresa != null){
			contagerencialicms = empresa.getContagerencialicms();
			contagerencialicmsst = empresa.getContagerencialicmsst();
			contagerencialipi = empresa.getContagerencialipi();
			contagerencialiss = empresa.getContagerencialiss();
			contagerencialfrete = empresa.getContagerencialfrete();
			
			if(contagerencialicms != null && contagerencialicms.getCdcontagerencial() != null && 
					valortotalicms != null && valortotalicms > 0){
				if(mapimpostos.containsKey(contagerencialicms.getCdcontagerencial())){
					mapimpostos.put(contagerencialicms.getCdcontagerencial(), mapimpostos.get(contagerencialicms.getCdcontagerencial()).add(new Money(valortotalicms)));
				} else{
					mapimpostos.put(contagerencialicms.getCdcontagerencial(), (new Money(valortotalicms)));
				}
			}
			if(contagerencialicmsst != null && contagerencialicmsst.getCdcontagerencial() != null && 
					bean.getValoricmsst() != null && bean.getValoricmsst().getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialicmsst.getCdcontagerencial())){
					mapimpostos.put(contagerencialicmsst.getCdcontagerencial(), mapimpostos.get(contagerencialicmsst.getCdcontagerencial()).add(bean.getValoricmsst()));
				} else{
					mapimpostos.put(contagerencialicmsst.getCdcontagerencial(), bean.getValoricmsst());
				}
			}
			if(contagerencialipi != null && contagerencialipi.getCdcontagerencial() != null && 
					valortotalipi != null && valortotalipi > 0){
				if(mapimpostos.containsKey(contagerencialipi.getCdcontagerencial())){
					mapimpostos.put(contagerencialipi.getCdcontagerencial(), mapimpostos.get(contagerencialipi.getCdcontagerencial()).add(new Money(valortotalipi)));
				} else{
					mapimpostos.put(contagerencialipi.getCdcontagerencial(), new Money(valortotalipi));
				}
			}
			if(contagerencialiss != null && contagerencialiss.getCdcontagerencial() != null && 
					valortotaliss != null && valortotaliss > 0){
				if(mapimpostos.containsKey(contagerencialiss.getCdcontagerencial())){
					mapimpostos.put(contagerencialiss.getCdcontagerencial(), mapimpostos.get(contagerencialiss.getCdcontagerencial()).add(new Money(valortotaliss)));
				} else{
					mapimpostos.put(contagerencialiss.getCdcontagerencial(), new Money(valortotaliss));
				}
			}
			
			if(contagerencialfrete != null && contagerencialfrete.getCdcontagerencial() != null && 
					bean.getFrete() != null && bean.getFrete().getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialfrete.getCdcontagerencial())){
					mapimpostos.put(contagerencialfrete.getCdcontagerencial(), mapimpostos.get(contagerencialfrete.getCdcontagerencial()).add(bean.getFrete()));
				} else{
					mapimpostos.put(contagerencialfrete.getCdcontagerencial(), bean.getFrete());
				}
			}
		}
		
		for (String whereInContagerencialCentrocustoProjeto : map.keySet()) {
			Money valorContaGerencial = map.get(whereInContagerencialCentrocustoProjeto);
			
			String[] idsContagerencialCentrocustoProjeto = whereInContagerencialCentrocustoProjeto.split(",");
			
			contagerencial = null;
			centrocusto = null;
			projeto = null;
			if(!idsContagerencialCentrocustoProjeto[0].equals("<null>")) contagerencial = new Contagerencial(Integer.parseInt(idsContagerencialCentrocustoProjeto[0]));
			if(!idsContagerencialCentrocustoProjeto[1].equals("<null>")) centrocusto = new Centrocusto(Integer.parseInt(idsContagerencialCentrocustoProjeto[1]));
			if(!idsContagerencialCentrocustoProjeto[2].equals("<null>")) projeto = new Projeto(Integer.parseInt(idsContagerencialCentrocustoProjeto[2]));
			
			Rateioitem rateioitem = new Rateioitem(
					contagerencial, centrocusto, projeto, valorContaGerencial, 
					SinedUtil.truncate(bean.getValorTotalOrdemCompra() == 0 ? 0 : valorContaGerencial.getValue().doubleValue() /bean.getValorTotalOrdemCompra() * 100));
			
			itens.add(rateioitem);
		}
		
		for (Integer cg : mapimpostos.keySet()) {
			Money valorContaGerencial = mapimpostos.get(cg);
			
			Rateioitem rateioitem = new Rateioitem(
					new Contagerencial(cg), centrocusto, null, valorContaGerencial, 
					SinedUtil.truncate(bean.getValorTotalOrdemCompra() == 0 ? 0 : valorContaGerencial.getValue().doubleValue() / bean.getValorTotalOrdemCompra() * 100));
			
			itens.add(rateioitem);
		}
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(itens);
		bean.setRateio(rateio);
		rateioService.calculaValoresRateio(rateio, new Money(bean.getValorTotalOrdemCompra()));
	}
	
	/**
	 * M�todo que cria um whereIn dos ids de Conta gerencial, centro de custo e projeto
	 *
	 * @param contagerencial
	 * @param centrocusto
	 * @param projeto
	 * @return
	 * @author Luiz Fernando
	 */
	public String getwhereInContagerencialCentrocustoProjeto(Contagerencial contagerencial, Centrocusto centrocusto, Projeto projeto) {
		StringBuilder ids = new StringBuilder();
		
		if(contagerencial != null && contagerencial.getCdcontagerencial() != null){
			ids.append(contagerencial.getCdcontagerencial());
		}else {
			ids.append("<null>");
		}
		ids.append(",");
		if(centrocusto != null && centrocusto.getCdcentrocusto() != null){
			ids.append(centrocusto.getCdcentrocusto());
		}else {
			ids.append("<null>");
		}
		ids.append(",");
		if(projeto != null && projeto.getCdprojeto() != null){
			ids.append(projeto.getCdprojeto());
		}else {
			ids.append("<null>");
		}
		return ids.toString();
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Ordemcompra findOrdemCompraParaEnviarPedido(String whereIn) {
		return ordemcompraDAO.findOrdemCompraParaEnviarPedido(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#callProcedureAtualizaOrdemcompra
	 * @param ordemcompra
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaOrdemcompra(Ordemcompra ordemcompra){
		ordemcompraDAO.callProcedureAtualizaOrdemcompra(ordemcompra);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	private void callProcedureAtualizaOrdemcompra(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Ordemcompra ordemcompra;
			for (int i = 0; i < ids.length; i++) {
				try {
					ordemcompra = new Ordemcompra(Integer.parseInt(ids[i].trim()));
					this.callProcedureAtualizaOrdemcompra(ordemcompra);
				} catch (Exception e) {
					System.out.println("*** whereInOrdemCompra - callProcedureAtualizaOrdemcompra : " + whereIn);
					throw new SinedException(e);
				}
			}
		}
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) orden(s) para autorizada
	 * 
	 * @see #findOrdensParaAutorizar(String)
	 * @see br.com.linkcom.sined.geral.service.PapelService#carregaPapeisPermissaoAcao(String)
	 * @see #montaAvisosOrdemCompra(List, List, List, String)
	 * @see #doUpdateAutorizadaGerandoFluxoCaixaOrdens(List)
	 * 
	 * @param whereIn
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean autorizar(String whereIn, BindException errors) {
		List<Ordemcompra> ordens = findOrdensParaAutorizar(whereIn);
		
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(AUTORIZADA) || 
					ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(APROVADA)){
				errors.reject("001", "Ordem de compra j� est� autorizada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(PEDIDO_ENVIADO) || 
					  ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(ENTREGA_PARCIAL)){
				errors.reject("002", "Ordem de compra j� foi autorizada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("003", "Autoriza��o n�o permitida. A requisi��o est� baixada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("004", "Autoriza��o n�o permitida. Ordem de compra est� cancelada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO) &&
					 (ordemcompra.getRateioverificado() == null || !ordemcompra.getRateioverificado())){
				errors.reject("005", "O rateio da ordem de compra deve ser verificado.");
				return false;
			}
		}
		
		List<Aviso> listaAvisos = new ListSet<Aviso>(Aviso.class);
		montaAvisosOrdemCompra(listaAvisos, MotivoavisoEnum.ENVIAR_PEDIDO_COMPRA, ordens, "Aguardando envio.");
		
		String msg = doUpdateAutorizadaGerandoFluxoCaixaOrdens(ordens, listaAvisos);
		if(msg != null && msg.length() > 5)
			errors.reject("006", msg.substring(5, msg.length()));
		return true;
	}
	
	public boolean aprovar(String whereIn, BindException errors) {
		List<Ordemcompra> ordens = findOrdensParaAutorizar(whereIn);
		
		Boolean permiteAutorizarOrdemcompra = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_ORDEMCOMPRA");
		
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(APROVADA)){
				errors.reject("001", "Ordem de compra j� est� aprovada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(PEDIDO_ENVIADO) || 
					ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(ENTREGA_PARCIAL)){
				errors.reject("002", "Ordem de compra j� foi aprovada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("003", "Autoriza��o n�o permitida. A requisi��o est� baixada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("004", "Aprova��o n�o permitida. Ordem de compra est� cancelada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO)){
				if(!permiteAutorizarOrdemcompra){
					errors.reject("005", "A ordem de compra tem que ser autorizada.");
					return false;
				}
			}
		}
		
		doUpdateAprovada(ordens);
		return true;
	}
	
	/**
	 * M�todo muda a situa��o da(s) orden(s) para cancelado
	 * 
	 * @param ordens
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean cancelar(List<Ordemcompra> ordens, BindException errors) {
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(AUTORIZADA)){
				errors.reject("006", "Cancelamento n�o permitido. Ordem de compra est� autorizada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(PEDIDO_ENVIADO)){
				errors.reject("007", "Cancelamento n�o permitido. Ordem de compra est� com pedido enviado.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(ENTREGA_PARCIAL)){
				errors.reject("008", "Cancelamento n�o permitido. Ordem de compra est� em entrega.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("009", "Cancelamento n�o permitido. Ordem de compra est� baixada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("010", "Ordem de compra j� est� cancelada.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) orden(s) para em aberto
	 * 
	 * @param ordens
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean estornar(List<Ordemcompra> ordens, BindException errors) {
		HashMap<Projeto, List<Ordemcompramaterial>> mapProjetoOrdemcompramaterial = new HashMap<Projeto, List<Ordemcompramaterial>>();
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO)){
				errors.reject("011", "Ordem de compra j� est� estornada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(ENTREGA_PARCIAL)){
				errors.reject("012", "Estorno n�o permitido. Ordem de compra est� em entrega parcial.");
				return false;
			}  else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA) && 
					   ordemcompra.getDtbaixa() == null){
				errors.reject("013", "Estorno n�o permitido. A compra est� baixada.");
				return false;
			}
			
			if(SinedUtil.isListNotEmpty(ordemcompra.getListaMaterial())){
				for(Ordemcompramaterial ocm : ordemcompra.getListaMaterial()){
					if(ocm.getProjeto() != null){
						ocm.setOrdemcompra(ordemcompra);
						if(mapProjetoOrdemcompramaterial.get(ocm.getProjeto()) == null){
							mapProjetoOrdemcompramaterial.put(ocm.getProjeto(), new ArrayList<Ordemcompramaterial>());
						}
						mapProjetoOrdemcompramaterial.get(ocm.getProjeto()).add(ocm);
					}
				}
			}
		}
		
		if(mapProjetoOrdemcompramaterial != null && mapProjetoOrdemcompramaterial.size() > 0){
			for(Projeto projeto : mapProjetoOrdemcompramaterial.keySet()){
				validaQtdeCompraMaiorQuePlanejamento(mapProjetoOrdemcompramaterial.get(projeto), errors, null);
				if(errors.hasErrors()){
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) orden(s) para baixada
	 * 
	 * @see #saveHistoricoEUpdateSituacao(Ordemcomprahistorico, Ordemcompraacao, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param ordens
	 * @param whereIn
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean baixar(List<Ordemcompra> ordens, String whereIn,	BindException errors) {
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(AUTORIZADA)){
				errors.reject("014", "Baixa n�o permitida. Ordem de compra est� autorizada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(PEDIDO_ENVIADO)){
				errors.reject("015", "Para baixar, a ordem de compra deve estar com todos os pedidos recebidos.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("016", "Ordem de compra j� est� baixada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("017", "Baixa n�o permitida. Ordem de compra est� cancelada.");
				return false;
			}
		}
		
		saveHistoricoEUpdateSituacao(new Ordemcomprahistorico(whereIn), Ordemcompraacao.BAIXADA, BAIXADA, null, null);
		return true;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) orden(s) para pedido enviado
	 * 
	 * @param ordemcompra
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean enviarPedido(Ordemcompra ordemcompra, BindException errors) {
		boolean needAprovacao = new SinedUtil().needAprovacao();
		
		Boolean permiteAutorizarOrdemcompra = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_ORDEMCOMPRA");
		Boolean permiteAprovarOrdemcompra = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "APROVAR_ORDEMCOMPRA");
		
		if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO)){
			if(needAprovacao){
				if(!permiteAprovarOrdemcompra){
					errors.reject("018", "Para enviar pedido, a ordem de compra deve estar aprovada.");
					return false;
				}else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO) &&
						 (ordemcompra.getRateioverificado() == null || !ordemcompra.getRateioverificado())){
					errors.reject("005", "O rateio da ordem de compra deve ser verificado.");
					return false;
				}
			} else {
				if(!permiteAutorizarOrdemcompra){
					errors.reject("018", "Para enviar pedido, a ordem de compra deve estar autorizada.");
					return false;
				}else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO) &&
						 (ordemcompra.getRateioverificado() == null || !ordemcompra.getRateioverificado())){
					errors.reject("005", "O rateio da ordem de compra deve ser verificado.");
					return false;
				}
			}
		} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(PEDIDO_ENVIADO) || 
				  ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(ENTREGA_PARCIAL)){
			errors.reject("019", "Ordem de compra com pedido j� enviado.");
			return false;
		} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
			errors.reject("020", "Envio de pedido n�o permitido. Ordem de compra est� baixada.");
			return false;
		} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
			errors.reject("021", "Envio de pedido n�o permitido. Ordem de compra est� cancelada.");
			return false;
		} else if(needAprovacao && ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(AUTORIZADA)){
			if(!permiteAutorizarOrdemcompra){
				errors.reject("025", "Para enviar pedido, a ordem de compra deve estar aprovada.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo que valida e gera uma entrega de uma ordem de compra.
	 * 
	 * @param ordens
	 * @param whereIn
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean receberEntrega(List<Ordemcompra> ordens, String whereIn,	BindException errors) {
		Fornecedor fornecedor = null;
		Empresa empresa = null;
		
		for (Ordemcompra ordemcompra : ordens){
			if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(EM_ABERTO) || 
			   ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(AUTORIZADA)){
				errors.reject("022", "Para receber entrega, a ordem de compra deve estar com o pedido enviado.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("023", "Recebimento de entrega n�o permitido. Ordem de compra est� baixada.");
				return false;
			} else if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("024", "Recebimento de entrega n�o permitido. Ordem de compra est� cancelada.");
				return false;
			}
			
			if(fornecedor == null && empresa == null){
				fornecedor = ordemcompra.getFornecedor();
				empresa = ordemcompra.getEmpresa();
			} else if(!fornecedor.equals(ordemcompra.getFornecedor())){
				errors.reject("026", "Recebimento de entrega n�o permitido. As ordens de compra tem que ter o mesmo fornecedor.");
				return false;
			} else {
				boolean empresaIgual = (empresa == null && ordemcompra.getEmpresa() == null) || 
										(empresa != null && ordemcompra.getEmpresa() != null && empresa.equals(ordemcompra.getEmpresa()));
				if(!empresaIgual){
					errors.reject("027", "Recebimento de entrega n�o permitido. As ordens de compra tem que ter a mesma empresa.");
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo que envia email, muda situa��o da ordem de compra para 'pedido enviado'.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContatoService#retiraContatoComEmailDuplicados(List)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#emitirOrdemCompra(String)
	 * @see #enviaPedido(WebRequestContext, Ordemcompra)
	 * @param ordemcompra
	 * @author Tom�s Rabelo
	 */
	public void enviaPedidoComEmail(Ordemcompra ordemcompra) throws Exception {
		List<Contato> contatos = contatoService.retiraContatoComEmailDuplicados(ordemcompra.getListaDestinatarios());
		
		Envioemail envioemail = envioemailService.registrarEnvio(ordemcompra.getRemetente(), ordemcompra.getAssunto(), 
				ordemcompra.getEmail(), Envioemailtipo.PEDIDO_ORDEM_COMPRA);
		
		String modelo = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.LAYOUT_EMISSAO_ORDEMCOMPRA);
		
		Resource report = null;
		
		if(ordemcompra.getEnviarordemcompra() != null && ordemcompra.getEnviarordemcompra()){
			if("configuravel".equalsIgnoreCase(modelo)){
				OrdemcompraemissaoFiltroReportTemplate filtro = new OrdemcompraemissaoFiltroReportTemplate(); 
				WebRequestContext request = NeoWeb.getRequestContext();
				request.setAttribute("selectedItensEnvioOrdemcompra", ordemcompra.getCdordemcompra().toString());
				
				report = ordemcompraemissaoTemplateReport.getPdfResource(request, filtro);
			} else {
				report = this.emitirOrdemCompra(ordemcompra.getCdordemcompra().toString());
			}
		}

		for (Contato contato : contatos){ 
		
			Envioemailitem envioemailitem = new Envioemailitem();
			envioemailitem.setEnvioemail(envioemail);
			envioemailitem.setPessoa(contato.getCdpessoa() == null ? ordemcompra.getFornecedor() : contato);
			envioemailitem.setEmail(contato.getEmailcontato());
			envioemailitem.setNomecontato(contato.getNome());
			envioemailitemService.saveOrUpdate(envioemailitem);
			
			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			email.setFrom(ordemcompra.getRemetente());
			email.setSubject(ordemcompra.getAssunto());
			email.setTo(contato.getEmailcontato());
			email.setEmailItemId(envioemailitem.getCdenvioemailitem());
			String emailHtml = ordemcompra.getEmail();
			if(emailHtml != null) emailHtml = emailHtml.replace("\n", "<br>");
			
			email.addHtmlText(emailHtml + EmailUtil.getHtmlConfirmacaoEmail(envioemail, contato.getEmailcontato()) + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
			
			Integer contentId = 1;
			if(report != null){
				email.attachFileUsingByteArray(report.getContents(), report.getFileName(), "application/pdf", contentId.toString());
				contentId++;
			}
			
			if(ordemcompra.getListaArquivosordemcompra() != null){
				for (Ordemcompraarquivo ordemcompraarquivo : ordemcompra.getListaArquivosordemcompra()) {
					if(ordemcompraarquivo.getCdordemcompraarquivo() != null){
						if(!(ordemcompraarquivo.getArquivo() != null && ordemcompraarquivo.getArquivo().getContent() != null && ordemcompraarquivo.getArquivo().getContent().length > 0)){
							ordemcompraarquivo = ordemcompraarquivoService.loadForEnvioPedidoCompra(ordemcompraarquivo);
							ordemcompraarquivo.setArquivo(arquivoService.loadWithContents(ordemcompraarquivo.getArquivo()));
						}
					}
					
					if(ordemcompraarquivo.getArquivo() != null && ordemcompraarquivo.getArquivo().getContent() != null && ordemcompraarquivo.getArquivo().getContent().length > 0){
						email.attachFileUsingByteArray(ordemcompraarquivo.getArquivo().getContent(), ordemcompraarquivo.getArquivo().getNome(), ordemcompraarquivo.getArquivo().getTipoconteudo(), contentId.toString());
						contentId++;
					}
				}
			}
			
			email.sendMessage();
		}
	}
	
	/**
	 * Este m�todo busca as solicita��es de compra relacionadas a ordem de compra. Funciona da seguinte maneira:
	 * A partir do material na ordem de compra, pega qual material est� em quest�o atrav�s do localarmazenagem e material.
	 * Depois volta mais um nivel chegando na solicita��o de compra e a compara��o � do mesmo jeito por�m utilizando o localarmazenagem "antigo".
	 * Existe um map que calcula a quantidade do material que foi pedido. Pois podem existir o mesmo material em outras ordens de compra da mesma 
	 * cota��o.
	 * 
	 * Obs: existem 2 querys para fazer este processo pois elas buscam muitas listas. Em uma query so n�o estava trazendo as listas corretamente. 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadCotacaoParaAjustarPedidoSolicitacaoCompra(Ordemcompra)
	 * @see #getOrdensCompraDaCotacao(Cotacao)
	 * @see #calculaTotalMaterialPedido(List)
	 * @see #buscaSolicitacoesDaOrdemDeCompraAPartirDoMaterialELocalArmazenagem(Cotacao, Ordemcompra, List, HashMap)
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesCompraRelacionadasAOrdemCompra(Ordemcompra ordemcompra) {
		List<Solicitacaocompra> listaSolicitacoesPedido = new ArrayList<Solicitacaocompra>();
		Cotacao cotacao = cotacaoService.loadCotacaoParaAjustarPedidoSolicitacaoCompra(ordemcompra);
		
		if(cotacao != null){
			List<Ordemcompra> ordensCompraCotacao = getOrdensCompraDaCotacao(cotacao, ordemcompra);
			
			//Key id material, value qtde pedida
			HashMap<Integer, Double> map = calculaTotalMaterialPedido(ordensCompraCotacao);
			
			for (Ordemcompra ordemcompra2 : ordensCompraCotacao) {
				
				if(ordemcompra2.getCdordemcompra().equals(ordemcompra.getCdordemcompra())){
					buscaSolicitacoesDaOrdemDeCompraAPartirDoMaterialELocalArmazenagem(cotacao, ordemcompra2, listaSolicitacoesPedido, map);
					break;
				}
			}
		}
		return listaSolicitacoesPedido;
	}
	
	/**
	 * M�todo que percorre da ordem de compra, passando pela cota��o at� chegar na solicita��o de compra. Ele busca todas as solicita��es de compra 
	 * que fazem parte de uma ordem de compra. Esta sendo utilizado de 2 maneiras. Uma ele descobre as solicita��es de compra e seta o pedido para 
	 * atulizar e a outra ele so descobre quais s�o as solicita��es para excluir
	 * 
	 * @param cotacao
	 * @param ordemcompra2
	 * @param listaSolicitacoesPedido
	 * @param map
	 * @author Tom�s Rabelo
	 */
	public void buscaSolicitacoesDaOrdemDeCompraAPartirDoMaterialELocalArmazenagem(Cotacao cotacao, Ordemcompra ordemcompra2, List<Solicitacaocompra> listaSolicitacoesPedido, HashMap<Integer, Double> map) {
		boolean achouMaterial = false;
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			
			if(cotacaofornecedor.getFornecedor().equals(ordemcompra2.getFornecedor())){
				for (Ordemcompramaterial ordemcompramaterial :  ordemcompra2.getListaMaterial()) {
					achouMaterial = false;
					
					for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
						
						if(cotacaofornecedoritem.getMaterial().equals(ordemcompramaterial.getMaterial()) && 
						   cotacaofornecedoritem.getLocalarmazenagem().equals(ordemcompra2.getLocalarmazenagem())){
							
							for (Cotacaoorigem cotacaoorigem : cotacao.getListaOrigem()) {
							
								if(cotacaofornecedoritem.getMaterial().equals(cotacaoorigem.getSolicitacaocompra().getMaterial()) && 
								   cotacaofornecedoritem.getLocalarmazenagemsolicitacao().equals(cotacaoorigem.getSolicitacaocompra().getLocalarmazenagem())){
									
									if(map != null){
										cotacaoorigem.getSolicitacaocompra().setPedido(map.get(cotacaofornecedoritem.getMaterial().getCdmaterial())+"/"+cotacaofornecedoritem.getQtdesol());
									}else{
										verificaSeExcluiPedidoDaSolicitacaoOuSubtraiQtde(cotacaoorigem.getSolicitacaocompra(), ordemcompramaterial.getQtdpedida());
									}
									
									listaSolicitacoesPedido.add(cotacaoorigem.getSolicitacaocompra());
									achouMaterial = true;
								}
							}
						}
						if(achouMaterial)
							break;
					}
				}
				//Achou fornecedor
				break;
			}
		}
	}
	
	/**
	 * Entra neste m�todo quando esta estornando um pedido enviado. Aqui verifica se ir� subtrair a qtde do pedido referente as ordens de compra
	 * ou ir� excluir o pedido da solicita��o.
	 * 
	 * @param solicitacaocompra
	 * @param QtdePedidaOrdemCompra
	 * @author Tom�s Rabelo
	 */
	private void verificaSeExcluiPedidoDaSolicitacaoOuSubtraiQtde(Solicitacaocompra solicitacaocompra, Double QtdePedidaOrdemCompra) {
		if(solicitacaocompra.getPedido() != null){
			String[] qtdes = solicitacaocompra.getPedido().split("/");
			if(Double.parseDouble(qtdes[0]) - QtdePedidaOrdemCompra <= 0){
				solicitacaocompra.setPedido(null);
			}else{
				solicitacaocompra.setPedido(Double.parseDouble(qtdes[0]) - QtdePedidaOrdemCompra+"/"+qtdes[1]);
			}
		}
	}
	
	/**
	 * M�todo que calcula a quantidade que foi solicitada de um material
	 * 
	 * @param ordensCompraCotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	private HashMap<Integer, Double> calculaTotalMaterialPedido(List<Ordemcompra> ordensCompraCotacao) {
		HashMap<Integer, Double> map = new HashMap<Integer, Double>();
		
		for (Ordemcompra ordemcompra : ordensCompraCotacao) {
			for (Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()) {
				if(map.containsKey(ordemcompramaterial.getMaterial().getCdmaterial())){
					map.put(ordemcompramaterial.getMaterial().getCdmaterial(), map.get(ordemcompramaterial.getMaterial().getCdmaterial()) + ordemcompramaterial.getQtdpedida());
				}else{
					map.put(ordemcompramaterial.getMaterial().getCdmaterial(), ordemcompramaterial.getQtdpedida());	
				}
			}
		}
		
		return map;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 * @param ordemcompra 
	 */
	public List<Ordemcompra> getOrdensCompraDaCotacao(Cotacao cotacao, Ordemcompra ordemcompra) {
		return ordemcompraDAO.getOrdensCompraDaCotacao(cotacao, ordemcompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForEdicaoCotacao(Cotacao cotacao)
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForEdicaoCotacao(Cotacao cotacao) {
		return ordemcompraDAO.findForEdicaoCotacao(cotacao);
	}
	
	/**
	 * M�todo que busca as solicita��es de compra relacionadas a v�rias ordens de compra
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadCotacoesParaAjustarPedidoSolicitacaoCompra(String)
	 * @see #buscaSolicitacoesDaOrdemDeCompraAPartirDoMaterialELocalArmazenagem(Cotacao, Ordemcompra, List, HashMap) 
	 * @param ordens
	 * @param whereInOrdemCompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesCompraRelacionadasAsOrdensCompras(List<Ordemcompra> ordens, String whereInOrdemCompra) {
		List<Cotacao> cotacoes = cotacaoService.loadCotacoesParaAjustarPedidoSolicitacaoCompra(whereInOrdemCompra);
		List<Solicitacaocompra> solicitacaoes = new ArrayList<Solicitacaocompra>();
		
		if(cotacoes != null && cotacoes.size() > 0){
			for (Ordemcompra ordemcompra : ordens) {
				if(ordemcompra.getCotacao() != null){
					for (Cotacao cotacao : cotacoes) {
						if(ordemcompra.getCotacao().getCdcotacao().equals(cotacao.getCdcotacao())){
							buscaSolicitacoesDaOrdemDeCompraAPartirDoMaterialELocalArmazenagem(cotacao, ordemcompra, solicitacaoes, null);
							break;
						}
					}
				}
			}
		}
		return solicitacaoes;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdensParaEstorno(String whereIn) {
		return ordemcompraDAO.findOrdensParaEstorno(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean verificaSeEntregaPrecisaRomaneio(Entregadocumento bean, Ordemcompra ordemcompra) {
		return ordemcompraDAO.verificaSeEntregaPrecisaRomaneio(bean, ordemcompra);
	}
	
	public List<AnaliseCompraBean> getDadosForAnaliseCompra(AnalisecompraFiltro filtro){
		List<AnaliseCompraBean> lista = ordemcompraDAO.getDadosForAnaliseCompra(filtro);
		if(lista != null && !lista.isEmpty()){
			String whereInMaterial = getWhereInMaterial(lista);
			if(whereInMaterial != null && !whereInMaterial.equals("")){
				List<Vanalisecompra> listaVanalisecompra = ordemcompraDAO.findForAnaliseCompra(filtro, whereInMaterial);
				if(listaVanalisecompra != null && !listaVanalisecompra.isEmpty()){
					for(AnaliseCompraBean bean : lista){
						if(bean.getCdmaterial() != null){
							Vanalisecompra vanalisecompra = getBeanValormelhorpreco(bean.getCdmaterial(), bean.getCdpessoa(), listaVanalisecompra);
							if(vanalisecompra != null){
								bean.setValorultimacompra(vanalisecompra.getValorultimacompra());
								bean.setValormelhorpreco(vanalisecompra.getValorunitario());
								bean.setDatacompramelhorpreco(vanalisecompra.getDtcriacaoordemcompra());
								bean.setValormelhorprecofornecedor(vanalisecompra.getValorunitarioFornecedor());
							}
							
						}
					}
				}
				
			}
		}
		return lista;
	}
	
	private Vanalisecompra getBeanValormelhorpreco(Integer cdmaterial, Integer cdfornecedor, List<Vanalisecompra> listaVanalisecompra) {
		Money valorultimacompra = null;
		Date dataUltimacompra = null;
		Money menorValorMaterial = null;
		Money menorValorFornecedor = null;
		Date datacriacaoordemcompra = null;
		if(listaVanalisecompra != null && !listaVanalisecompra.isEmpty() && cdmaterial != null){
			for(Vanalisecompra bean : listaVanalisecompra){
				if(bean.getCdmaterial() != null && bean.getCdmaterial().equals(cdmaterial) && bean.getValorunitario() != null){
					if(bean.getDataEntradaOuEntrega() != null && bean.getValorunitario() != null){
						if(dataUltimacompra == null){
							valorultimacompra = bean.getValorunitario();
							dataUltimacompra = bean.getDataEntradaOuEntrega();
						}else if(SinedDateUtils.beforeOrEqualIgnoreHour(dataUltimacompra, bean.getDataEntradaOuEntrega())){
							valorultimacompra = bean.getValorunitario();
							dataUltimacompra = bean.getDataEntradaOuEntrega();
						}
					}
					if(menorValorMaterial != null){
						if(bean.getValorunitario().getValue().doubleValue() < menorValorMaterial.getValue().doubleValue()){
							menorValorMaterial = bean.getValorunitario();
							datacriacaoordemcompra = bean.getDtcriacaoordemcompra();
						}
					}else {
						menorValorMaterial = bean.getValorunitario();
						datacriacaoordemcompra = bean.getDtcriacaoordemcompra();
					}
					if(cdfornecedor != null && bean.getCdfornecedor() != null && bean.getCdfornecedor().equals(cdfornecedor)){
						if(menorValorFornecedor != null){
							if(bean.getValorunitario().getValue().doubleValue() < menorValorFornecedor.getValue().doubleValue()){
								menorValorFornecedor = bean.getValorunitario();
							}
						}else {
							menorValorFornecedor = bean.getValorunitario();
						}
					}
				}
			}
		}
		return new Vanalisecompra(cdmaterial, cdfornecedor, valorultimacompra, menorValorMaterial, menorValorFornecedor, datacriacaoordemcompra);
	}
	
	private String getWhereInMaterial(List<AnaliseCompraBean> lista) {
		StringBuilder whereIn = new StringBuilder();
		if(lista != null && !lista.isEmpty()){
			for(AnaliseCompraBean bean : lista){
				if(bean.getCdmaterial() != null){
					if(!whereIn.toString().contains(bean.getCdmaterial().toString())){
						whereIn.append(bean.getCdmaterial()).append(",");
					}
				}
			}
		}
		return !whereIn.toString().equals("") ? whereIn.toString().substring(0, whereIn.toString().length()-1) : "";
	}
	/**
	 * M�todo que gera relat�rio de analise de compra
	 * 
	 * @param filtro
	 * @return
	 */
	public IReport gerarAnaliseCompraRelatorio(AnalisecompraFiltro filtro) {
		Report report = new Report("/suprimento/analisecompra");
		
		List<AnaliseCompraBean> list = getDadosForAnaliseCompra(filtro);
		
		Indicecorrecao indicecorrecao = null;
		if(filtro.getIndicecorrecao() != null && filtro.getIndicecorrecao().getCdindicecorrecao() != null){
			indicecorrecao = indicecorrecaoService.carregaIndiceCorrecao(filtro.getIndicecorrecao());
		}
		if(indicecorrecao != null){
			for(AnaliseCompraBean bean : list){
				//Se for true = acumulado, false = mensal
				if(indicecorrecao.getTipocalculo() != null && indicecorrecao.getTipocalculo()){
					bean.setValormelhorpreco(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValormelhorpreco(), bean.getDataentrega(), bean.getDatacompramelhorpreco()));
				}else {
					bean.setValormelhorpreco(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValormelhorpreco(), bean.getDatacompra(), bean.getDatacompra()));
				}
				if(bean.getValorultimacompra() != null && bean.getValorultimacompra().getValue().doubleValue() > 0){
					if(bean.getValormelhorpreco() != null && bean.getValormelhorpreco().getValue().doubleValue() > 0){
						bean.setIndicepreco((bean.getValorultimacompra().divide(bean.getValormelhorpreco()).getValue().doubleValue()-1)*100);
					}
					if(indicecorrecao.getTipocalculo() != null && indicecorrecao.getTipocalculo()){
						bean.setValorultimacompra(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValorultimacompra(), bean.getDataentrega(), bean.getDatacompramelhorpreco()));
					}else {
						bean.setValorultimacompra(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValorultimacompra(), bean.getDatacompra(), bean.getDatacompra()));
					}
					
				}
				if(bean.getValormelhorprecofornecedor() != null && bean.getValormelhorprecofornecedor().getValue().doubleValue() > 0){
					if(bean.getValormelhorpreco() != null && bean.getValormelhorpreco().getValue().doubleValue() > 0){
						bean.setIndicepreco((bean.getValormelhorprecofornecedor().divide(bean.getValormelhorpreco()).getValue().doubleValue()-1)*100);
					}
					if(indicecorrecao.getTipocalculo() != null && indicecorrecao.getTipocalculo()){
						bean.setValorultimacompra(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValormelhorprecofornecedor(), bean.getDataentrega(), bean.getDatacompramelhorpreco()));
					}else {
						bean.setValorultimacompra(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValormelhorprecofornecedor(), bean.getDatacompra(), bean.getDatacompra()));
					}
				}
			}
		}else {
			for(AnaliseCompraBean bean : list){
				if(bean.getValorultimacompra() != null && bean.getValorultimacompra().getValue().doubleValue() > 0){
					if(bean.getValormelhorpreco() != null && bean.getValormelhorpreco().getValue().doubleValue() > 0){
						bean.setIndicepreco((bean.getValorultimacompra().divide(bean.getValormelhorpreco()).getValue().doubleValue()-1)*100);
					}
				}
			}
		}
		
		
		if(filtro.getSomentemenorpreco() != null && filtro.getSomentemenorpreco()){
			for (Iterator<AnaliseCompraBean> iterator = list.iterator(); iterator.hasNext();) {
				AnaliseCompraBean analiseCompraBean = (AnaliseCompraBean) iterator.next();
				for (AnaliseCompraBean it: list) {
					if(it.getCdmaterial().equals(analiseCompraBean.getCdmaterial()) &&
							it.getCdpessoa().equals(analiseCompraBean.getCdpessoa()) &&
							!it.equals(analiseCompraBean) &&
							analiseCompraBean.getValorunitario().compareTo(it.getValorunitario()) <= 0){
						iterator.remove();
						break;
					}
				}
			}
		}
		
//		if(filtro.getMelhorcompra() != null && filtro.getMelhorcompra()){
//			for (Iterator<AnaliseCompraBean> iterator = list.iterator(); iterator.hasNext();) {
//				AnaliseCompraBean analiseCompraBean = (AnaliseCompraBean) iterator.next();
//				for (AnaliseCompraBean it: list) {
//					if(it.getCdmaterial().equals(analiseCompraBean.getCdmaterial()) &&
//							it.getCdpessoa().equals(analiseCompraBean.getCdpessoa()) &&
//							!it.equals(analiseCompraBean) &&
//							analiseCompraBean.getValormelhorpreco() != null &&
//							it.getValormelhorpreco() != null &&
//							analiseCompraBean.getValormelhorpreco().compareTo(it.getValormelhorpreco()) <= 0){
//						iterator.remove();
//						break;
//					}
//				}
//			}
//		}
		if(filtro.getIndice() != null){
			for (Iterator<AnaliseCompraBean> iterator = list.iterator(); iterator.hasNext();) {
				AnaliseCompraBean analiseCompraBean = (AnaliseCompraBean) iterator.next();
				if("true".equalsIgnoreCase(filtro.getIndice())){
					//Somente �ndice positivo
					if(analiseCompraBean.getIndicepreco() != null && analiseCompraBean.getIndicepreco() < 0)
						iterator.remove();
				}else {
					//Somente �ndice Negativo
					if(analiseCompraBean.getIndicepreco() != null && analiseCompraBean.getIndicepreco() >= 0)
						iterator.remove();
				}
			}
		}
		
		if(list != null && list.size() > 0){
			report.setDataSource(list);
		}
		
		return report;
	}
	
	/**
	 * M�todo que gera relat�rio de analise de compra sint�tico
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarAnaliseCompraSinteticoRelatorio(AnalisecompraFiltro filtro) {
		Report report = new Report("/suprimento/analisecomprasintetico");
		
		List<AnaliseCompraBean> list = ordemcompraDAO.getDadosForAnaliseCompraSintetico(filtro);
		List<AnaliseCompraBean> listAjustada = new ArrayList<AnaliseCompraBean>();
		
		if(list != null && !list.isEmpty()){
			HashMap<Integer, Money> mapMaterialValorultimacompra = new HashMap<Integer, Money>();
			HashMap<Integer, String> mapMaterialNome = new HashMap<Integer, String>();
			HashMap<Integer, Integer> mapMaterialQtdeTotalOrdemcompra = new HashMap<Integer, Integer>();
			HashMap<Integer, Integer> mapMaterialQtdeValorAbaixoUltimacompra = new HashMap<Integer, Integer>();
			
			if(filtro.getIndicecorrecao() != null && filtro.getIndicecorrecao().getCdindicecorrecao() != null){
				Indicecorrecao indicecorrecao = indicecorrecaoService.carregaIndiceCorrecao(filtro.getIndicecorrecao());
				if(indicecorrecao != null){
					for(AnaliseCompraBean bean : list){
						bean.setValorunitario(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValorunitario(), new Date(System.currentTimeMillis()), bean.getDatacompra()));
						if(bean.getValorultimacompra() != null && bean.getValorultimacompra().getValue().doubleValue() > 0){
							bean.setValorultimacompra(indicecorrecaoService.calculaIndiceCorrecaoAnalisecompra(indicecorrecao, bean.getValorultimacompra(), new Date(System.currentTimeMillis()), bean.getDatacompra()));
						}
					}
				}
			}
			
			for(AnaliseCompraBean bean : list){
				if(bean.getValorultimacompra() != null && bean.getValorultimacompra().getValue().doubleValue() > 0){
					mapMaterialValorultimacompra.put(bean.getCdmaterial(), bean.getValorultimacompra());
				}
			}
			
			Money valorultimacompra = null;
			for(AnaliseCompraBean bean : list){
				valorultimacompra = mapMaterialValorultimacompra.get(bean.getCdmaterial());
				if(bean.getValorunitario() != null && valorultimacompra != null && 
						bean.getValorunitario().getValue().doubleValue() < valorultimacompra.getValue().doubleValue() ){
					if(mapMaterialQtdeValorAbaixoUltimacompra.get(bean.getCdmaterial()) != null){
						mapMaterialQtdeValorAbaixoUltimacompra.put(bean.getCdmaterial(), 
								(mapMaterialQtdeValorAbaixoUltimacompra.get(bean.getCdmaterial())+1));
					}else {
						mapMaterialQtdeValorAbaixoUltimacompra.put(bean.getCdmaterial(), 1);
					}
				}
				if(mapMaterialQtdeTotalOrdemcompra.get(bean.getCdmaterial()) != null){
					mapMaterialQtdeTotalOrdemcompra.put(bean.getCdmaterial(), mapMaterialQtdeTotalOrdemcompra.get(bean.getCdmaterial()) + 1);
				}else {
					mapMaterialQtdeTotalOrdemcompra.put(bean.getCdmaterial(), 1);
				}
				mapMaterialNome.put(bean.getCdmaterial(), bean.getMaterial());
			}
			
			AnaliseCompraBean analisecomprabean;
			for(Integer cdmaterial : mapMaterialNome.keySet()){
				analisecomprabean = new AnaliseCompraBean();
				analisecomprabean.setMaterial(mapMaterialNome.get(cdmaterial));
				if(mapMaterialQtdeValorAbaixoUltimacompra.get(cdmaterial) == null)
					analisecomprabean.setQtdeocmenorultimovalor(0);
				else
					analisecomprabean.setQtdeocmenorultimovalor(mapMaterialQtdeValorAbaixoUltimacompra.get(cdmaterial));
				analisecomprabean.setQtdetotaloc(mapMaterialQtdeTotalOrdemcompra.get(cdmaterial));
				
				listAjustada.add(analisecomprabean);
			}
		}
		
		if(listAjustada != null && listAjustada.size() > 0){
			report.setDataSource(listAjustada);
		}
		
		return report;
	}
	
	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosOrdemCompra(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Integer qtdeEmAberto = getQtdeEmAberto();
		Integer qtdeBaixada = getQtdeBaixadas();
		Integer total = getQtdeTotalOrdensCompra();
		return new DadosEstatisticosBean(DadosEstatisticosBean.ORDEM_COMPRA, ultimoCadastro, qtdeEmAberto, qtdeBaixada, total);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalOrdensCompra() {
		return ordemcompraDAO.getQtdeTotalOrdensCompra();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeBaixadas() {
		return ordemcompraDAO.getQtdeBaixadas();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeEmAberto() {
		return ordemcompraDAO.getQtdeEmAberto();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAutorizadas() {
		return ordemcompraDAO.getQtdeAutorizadas();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdePedidoEnviado() {
		return ordemcompraDAO.getQtdePedidoEnviado();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return ordemcompraDAO.getDtUltimoCadastro();
	}
	
	
	
	/**
	 * Popula o arquivo CSV
	 * 
	 * @author Taidson Santos
	 * @since 22/03/2010
	 */
	private void adicionaRowArquivoCSV(StringBuilder csv, Ordemcompra ordemcompra) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Pessoa comprador = pessoaService.load(new Pessoa(ordemcompra.getCdusuarioaltera()));
		
		Double qtdetotal = 0.0;
		for (Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()) {
			if(ordemcompramaterial.getQtdpedida() != null)
				qtdetotal += ordemcompramaterial.getQtdpedida();
		}
		
		String nomeFornecedor = "";
		if(ordemcompra.getFornecedor() != null && ordemcompra.getFornecedor().getNome() != null)
			nomeFornecedor = ordemcompra.getFornecedor().getNome();
		
		String cotacao = "";
		if(ordemcompra.getCotacao() != null && ordemcompra.getCotacao().getCdcotacao() != null){
			cotacao = ordemcompra.getCotacao().getCdcotacao().toString();
		}
		for (Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()) {
			csv.append(ordemcompra.getCdordemcompra()).append(";");
			
			String cdsolicitacaocompra = "";
			String solicitantes = "";
			String centroscusto = "";
			String projetos = "";
			if(ordemcompra.getCotacao() != null && ordemcompra.getCotacao().getListaOrigem() != null && !ordemcompra.getCotacao().getListaOrigem().isEmpty()){
				for (Cotacaoorigem cotacaoorigem : ordemcompra.getCotacao().getListaOrigem()) {
					if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getMaterial() != null && cotacaoorigem.getSolicitacaocompra().getMaterial().getCdmaterial() != null && cotacaoorigem.getSolicitacaocompra().getMaterial().equals(ordemcompramaterial.getMaterial())){
						if(cotacaoorigem.getSolicitacaocompra().getIdentificador() != null){
							cdsolicitacaocompra += cotacaoorigem.getSolicitacaocompra().getIdentificador() +", ";
						}else{
							cdsolicitacaocompra += cotacaoorigem.getSolicitacaocompra().getCdsolicitacaocompra() +", ";
						}
						if(solicitantes.indexOf(cotacaoorigem.getSolicitacaocompra().getColaborador().getNome()) == -1)
							solicitantes += cotacaoorigem.getSolicitacaocompra().getColaborador().getNome()+", ";
						if(centroscusto.indexOf(cotacaoorigem.getSolicitacaocompra().getCentrocusto().getNome()) == -1)
							centroscusto += cotacaoorigem.getSolicitacaocompra().getCentrocusto().getNome()+", ";
					}
				}
				if(cdsolicitacaocompra.lastIndexOf(",") != -1)
					cdsolicitacaocompra = cdsolicitacaocompra.substring(0, cdsolicitacaocompra.length()-2);
				if(solicitantes.lastIndexOf(",") != -1)
					solicitantes = solicitantes.substring(0, solicitantes.length()-2);
				if(centroscusto.lastIndexOf(",") != -1)
					centroscusto = centroscusto.substring(0, centroscusto.length()-2);
			}
			
			csv.append(cdsolicitacaocompra).append(";")
			.append(nomeFornecedor).append(";")
			.append(cotacao).append(";")
			.append(ordemcompramaterial.getMaterial().getIdentificacao() != null ? ordemcompramaterial.getMaterial().getIdentificacao() : "").append(";")
			.append(ordemcompramaterial.getMaterial().getNome()).append(";")
			.append(ordemcompramaterial.getUnidademedida() != null && ordemcompramaterial.getUnidademedida().getNome() != null ? ordemcompramaterial.getUnidademedida().getNome() : "").append(";")
			.append(ordemcompramaterial.getQtdpedida() != null ? SinedUtil.descriptionDecimal(ordemcompramaterial.getQtdpedida()) : "").append(";")
			.append(ordemcompramaterial.getMaterial().getMaterialgrupo().getNome()).append(";")
			.append(centroscusto).append(";")
			.append(comprador.getNome()).append(";");
			if (ordemcompramaterial.getOrdemcompra() != null && ordemcompramaterial.getOrdemcompra().getRateio() != null && ordemcompramaterial.getOrdemcompra().getRateio().getListaRateioitem() != null){
				projetos = SinedUtil.listAndConcatenate(ordemcompramaterial.getOrdemcompra().getRateio().getListaRateioitem(), "projeto.nome", ", ");
//				for (Rateioitem ri : ordemcompramaterial.getOrdemcompra().getRateio().getListaRateioitem()) {
//					if (ri.getProjeto() != null){
//						projeto
//					}
//				}
			}
			csv.append(projetos).append(";");
			csv.append(solicitantes).append(";")
			.append(ordemcompra.getDtcriacao() != null ? dateFormat.format(ordemcompra.getDtcriacao()) : "").append(";")
			.append(ordemcompra.getDtaprova() != null ? dateFormat.format(ordemcompra.getDtaprova()) : "").append(";");
			
			Double qtdeMateriaisEntregues = 0d;
			Money totalMateriaisEntregues = new Money(0);
			
			Set<Ordemcompraentrega> listaOrdemcompraentrega = ordemcompra.getListaOrdemcompraentrega();
			if(listaOrdemcompraentrega != null && listaOrdemcompraentrega.size() > 0){
				int i = 0;
				
				for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
					Entrega entrega = ordemcompraentrega.getEntrega();
					if(entrega != null && entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
						for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
							if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
								for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
									if(entregamaterial.getMaterial().equals(ordemcompramaterial.getMaterial())){
										if(i==0 && entrega.getDtentrega() != null)
											csv.append(dateFormat.format(entrega.getDtentrega()));
										i++;
										qtdeMateriaisEntregues += entregamaterial.getQtde();
										totalMateriaisEntregues = totalMateriaisEntregues.add(new Money(entregamaterial.getQtde()*entregamaterial.getValorunitario()));
									}
								}
							}
						}
					}
				}
				
			}
			
			Double pendente = ordemcompramaterial.getQtdpedida()-qtdeMateriaisEntregues;
			csv
			.append(";")
			.append(ordemcompramaterial.getQtdpedida() != null ? SinedUtil.descriptionDecimal(ordemcompramaterial.getQtdpedida()) : "").append(";")
			.append(ordemcompramaterial.getValor() != null ? SinedUtil.descriptionDecimal(ordemcompramaterial.getValor()) : "").append(";");
			
			if(ordemcompra.getDesconto() != null && qtdetotal > 0 && ordemcompramaterial.getQtdpedida() != null){
				csv.append(new Money(ordemcompra.getDesconto().getValue().doubleValue()/qtdetotal*ordemcompramaterial.getQtdpedida()));
			}
			
			csv
			.append(";")
			.append(SinedUtil.descriptionDecimal(qtdeMateriaisEntregues)).append(";")
			.append(SinedUtil.descriptionDecimal(totalMateriaisEntregues.getValue().doubleValue())).append(";")
			.append(SinedUtil.descriptionDecimal(pendente)).append(";")
			.append(pendente <= 0.0 ? 0 : SinedUtil.descriptionDecimal(new Money(ordemcompramaterial.getValor()/ordemcompramaterial.getQtdpedida()*pendente).getValue().doubleValue())).append(";")
			.append(ordemcompra.getObservacao() != null ? ordemcompra.getObservacao() : "").append(";")
			.append(ordemcompra.getObservacoesinternas() != null ? ordemcompra.getObservacoesinternas() : "").append(";");

			
			csv.append(ordemcompramaterial.getIcmsiss()!= null ? SinedUtil.descriptionDecimal(ordemcompramaterial.getIcmsiss()) : "").append(";");
			csv.append(ordemcompramaterial.getIcmsissincluso() != null && ordemcompramaterial.getIcmsissincluso() ? "Sim" : "N�o").append(";");
			csv.append(ordemcompramaterial.getIpi()!= null ? SinedUtil.descriptionDecimal(ordemcompramaterial.getIpi()) : "").append(";");
			csv.append(ordemcompramaterial.getIpiincluso() != null && ordemcompramaterial.getIpiincluso() ? "Sim" : "N�o").append(";\n");
		}
		
		
		
		/*DecimalFormat formatadorDecimal = new DecimalFormat("#,##0.00");
		Double valor = 0.0;
		for (Ordemcompramaterial vr : ordemcompra.getListaMaterial()) {
			valor += vr.getValor(); 
			
		}
		ordemcompra.setValorTotalCSV(valor);
		if(ordemcompra.getAux_ordemcompra().getEntregapercentual() == null){
			ordemcompra.getAux_ordemcompra().setEntregapercentual(0);
		}
		if(ordemcompra.getRateioverificado() != null && ordemcompra.getRateioverificado()){
			ordemcompra.setRateioCSV("Verificado");
		}else{ 
			ordemcompra.setRateioCSV("Pendente");
		}
		csv.append("\n");
		csv.append(ordemcompra.getCdordemcompra()+ ";");
		csv.append(ordemcompra.getFornecedor().getNome() + " ("+
				   ordemcompra.getAux_ordemcompra().getEntregapercentual()+"%)" + ";");
		csv.append(ordemcompra.getMateriais()+";");
		if(ordemcompra.getLocalarmazenagem() != null){
			csv.append(ordemcompra.getLocalarmazenagem().getNome()+";");
		}else {csv.append(""+";");}
		csv.append(ordemcompra.getDtcriacao()+";");
		csv.append(ordemcompra.getEntregasListagemCSV()+";");
		csv.append(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos()+";");
		csv.append(ordemcompra.getRateioCSV()+";");
		if(ordemcompra.getDtsPagamentoPrevisto() != null){
			csv.append(ordemcompra.getDtsPagamentoPrevisto()+";");
		}else{csv.append(""+";");}
		csv.append(formatadorDecimal.format(ordemcompra.getValorTotalCSV()) + ";");
		csv.append(formatadorDecimal.format(ordemcompra.getSaldo()) + ";");
		 */
	}
	
	/**
	 * M�todo que cria arquivo CSV de acordo com resultado da tela de listagem de ordem de compra.
	 * @author Taidson Santos
	 * @since 22/03/2010
	 */
	@SuppressWarnings("unchecked")
	public Resource preparaArquivoOrdemCompraCSV(OrdemcompraFiltro filtro) throws Exception {
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Ordemcompra> lista = findForListagem(filtro).list();
		/*
		List<Ordemcompramaterial> findByOrdemcompra;
		Double saldo;
		for (Ordemcompra o : lista) {
			o.setListaEntregas(entregaService.findByOrdemcompra(o));
			o.getPrazopagamento().setListaPagamentoItem(prazopagamentoitemService.findByPrazo(o.getPrazopagamento()));
			
			findByOrdemcompra = ordemcompramaterialService.findByOrdemcompra(o);
			o.setListaMaterial(SinedUtil.listToSet(findByOrdemcompra, Ordemcompramaterial.class));
			
			saldo = o.getValorTotalOrdemCompraReport();
			if(o.getListaEntregas() != null && o.getListaEntregas().size() > 0){
				for (Entrega e : o.getListaEntregas()) {
					saldo -= e.getValorTotalEntrega().getValue().doubleValue();
				}
			}
			o.setSaldo(saldo);
		}
		*/
		StringBuilder csv = new StringBuilder();
		csv.append("Compra;Solicita��o compra;Fornecedor;Cota��o;Identificador;Materiais e Servi�os;Unidade de medida;Quantidade;Grupo material;Centro de custos;Comprador;Projeto;Solicitante;Dt. emiss�o;Dt. aprova��o.;Dt. �ltima entrega;Total;;;Entrega;;Pendente;;Observa��o;Observa��o Interna; ICMS/ISS; Incluso?; IPI; Incluso?\n"); 
		csv.append(";;;;;;;;;;;;;;;Quantidade;Valor;Desconto;Quantidade;Valor;Quantidade;Valor;\n");
		Ordemcompra ordemcompraaux;
		for (Ordemcompra ordemcompra : lista) {
			ordemcompra.setListaMaterial(ordemcompramaterialService.findByOrdemcompra(ordemcompra));
			ordemcompra.setListaOrdemcompraentrega(SinedUtil.listToSet(ordemcompraentregaService.findByOrdemcompra(ordemcompra), Ordemcompraentrega.class));
			ordemcompraaux = loadWithCotacao(ordemcompra);
			if(ordemcompraaux != null)
				ordemcompra.setCotacao(ordemcompraaux.getCotacao());
			adicionaRowArquivoCSV(csv, ordemcompra);
		}
		Resource resource = new Resource("text/csv", "exportarordemcompra_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo chamado via flex para gerar ordem de compra
	 * 
	 * @param cdcotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String gerarOrdemCompraFlex(CotacaoMapaAux mapacotacao){
		for (Cotacaofornecedor cotacaofornecedor : mapacotacao.getListaFornecedores()){
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()){
				cotacaofornecedoritem.setValor(cotacaofornecedoritem.getCorvalor().getValor().getValue().doubleValue());
				cotacaofornecedoritem.setCotacaofornecedor(cotacaofornecedor);
			}
		}
		return gerarOrdemCompra(new Cotacao(mapacotacao.getCdcotacao(), mapacotacao.getListaFornecedores()));
	}
	
	/**
	 * M�todo que gera ordem de compra a partir da cota��o, agora � chamado tanto do Java como do FLEX
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String gerarOrdemCompra(Cotacao cotacao) {
		List<Ordemcompra> ordensCompra = geraOrdemCompra(cotacao);
		
		List<Ordemcompramaterial> listaMaterial;
		for (Ordemcompra ordemcompra : ordensCompra) {
			listaMaterial = ordemcompra.getListaMaterial();
			for (Ordemcompramaterial ocm : listaMaterial) {
				if(ocm.getContagerencial() == null){
					ocm.setContagerencial(materialService.findWithContagerencial(ocm.getMaterial()).getContagerencial());
				}
			}
		}
		
		return salvaOrdensCompra(ordensCompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForEnvioemailcompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForEnvioemailcompra(String whereIn) {
		return ordemcompraDAO.findForEnvioemailcompra(whereIn);
	}
	
	/**
	 * M�todo que envia email de acordo com o processo de compra
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Luiz Fernando
	 */
	public void enviarEmailOrdemcompra(String whereIn, Situacaosuprimentos situacao, String notWhereInCdusuario) {
		if(whereIn != null && !"".equals(whereIn)){
			List<Ordemcompra> listaOrdemcompra = this.findForEnvioemailcompra(whereIn);
			StringBuilder whereInProjetos;
			String assunto = "";
			String parametrogeralaprovar = parametrogeralService.getValorPorNome(Parametrogeral.APROVAR_ORDEM_DE_COMPRA);
			if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
				assunto = "Ordem de compra Autorizada";
			}else if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
				assunto = "Ordem de compra criada";
			}else if(situacao.equals(Situacaosuprimentos.APROVADA)){
				assunto = "Ordem de compra Aprovada";
			}
			if(listaOrdemcompra != null && !listaOrdemcompra.isEmpty()){
				StringBuilder whereInCdusuario;
				for(Ordemcompra ordemcompra : listaOrdemcompra){
					whereInProjetos = new StringBuilder();
					if(ordemcompra.getRateio() != null && ordemcompra.getRateio().getListaRateioitem() != null && 
							!ordemcompra.getRateio().getListaRateioitem().isEmpty()){
						for(Rateioitem rateioitem : ordemcompra.getRateio().getListaRateioitem()){
							if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
								if(!"".equals(whereInProjetos.toString())) whereInProjetos.append(",");
								whereInProjetos.append(rateioitem.getProjeto().getCdprojeto());
							}
						}
					}
					
					whereInCdusuario = new StringBuilder();
					if(situacao.equals(Situacaosuprimentos.AUTORIZADA) || situacao.equals(Situacaosuprimentos.APROVADA)){
						if((parametrogeralaprovar == null || !"TRUE".equalsIgnoreCase(parametrogeralaprovar)) || situacao.equals(Situacaosuprimentos.APROVADA)){
							if(ordemcompra.getListaMaterial() != null && !ordemcompra.getListaMaterial().isEmpty()){
								for(Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()){
									if(ordemcompramaterial.getMaterial() != null &&  ordemcompramaterial.getMaterial().getMaterialgrupo() != null && 
											ordemcompramaterial.getMaterial().getMaterialgrupo().getListaMaterialgrupousuario() != null &&
											!ordemcompramaterial.getMaterial().getMaterialgrupo().getListaMaterialgrupousuario().isEmpty()){
										for(Materialgrupousuario materialgrupousuario : ordemcompramaterial.getMaterial().getMaterialgrupo().getListaMaterialgrupousuario()){
											if(materialgrupousuario.getUsuario() != null && materialgrupousuario.getUsuario().getCdpessoa() != null){
												if(!"".equals(whereInCdusuario.toString())) whereInCdusuario.append(",");
												whereInCdusuario.append(materialgrupousuario.getUsuario().getCdpessoa());
											}
										}
									}
								}
							}
						}
					}
					
					List<Usuario> listaUsuario = null;
					if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
						if(parametrogeralaprovar != null && "TRUE".equalsIgnoreCase(parametrogeralaprovar)){
							listaUsuario = usuarioService.findForEnvioemailcompra(Acao.APROVAR_ORDEMCOMPRA, ordemcompra.getEmpresa(), !"".equals(whereInProjetos) ? whereInProjetos.toString() : null, null, notWhereInCdusuario != null && !notWhereInCdusuario.equals("") ? notWhereInCdusuario : null);
						}else {
							listaUsuario = usuarioService.findForEnvioemailcompra(Acao.ENVIAR_PEDIDO_ORDEMCOMPRA, ordemcompra.getEmpresa(), !"".equals(whereInProjetos) ? whereInProjetos.toString() : null, !"".equals(whereInCdusuario) ? whereInCdusuario.toString() : null, notWhereInCdusuario != null && !notWhereInCdusuario.equals("") ? notWhereInCdusuario : null);
						}
					}else if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.AUTORIZAR_ORDEMCOMPRA, ordemcompra.getEmpresa(), !"".equals(whereInProjetos) ? whereInProjetos.toString() : null, null, notWhereInCdusuario != null && !notWhereInCdusuario.equals("") ? notWhereInCdusuario : null);
					}else if(situacao.equals(Situacaosuprimentos.APROVADA)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.ENVIAR_PEDIDO_ORDEMCOMPRA, ordemcompra.getEmpresa(), !"".equals(whereInProjetos) ? whereInProjetos.toString() : null, !"".equals(whereInCdusuario) ? whereInCdusuario.toString() : null, notWhereInCdusuario != null && !notWhereInCdusuario.equals("") ? notWhereInCdusuario : null);
					}
					
					if(listaUsuario != null && !listaUsuario.isEmpty()){
						for(Usuario usuario : listaUsuario){
							if(usuario.getEmail() != null && !"".equals(usuario.getEmail())){
								EmailManager email;							
								try {
									email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom("w3erp@w3erp.com.br")
										.setSubject(assunto)
										.setTo(usuario.getEmail());
									
									if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
										assunto = "Ordem de compra Autorizada";
										email.addHtmlText("Aviso do sistema: A ordem de compra " + ordemcompra.getCdordemcompra() +" foi autorizada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext() + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}else if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
										email.addHtmlText("Aviso do sistema: A ordem de compra " + ordemcompra.getCdordemcompra() +" foi criada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext() + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}else if(situacao.equals(Situacaosuprimentos.APROVADA)){
										email.addHtmlText("Aviso do sistema: A ordem de compra " + ordemcompra.getCdordemcompra() +" foi aprovada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext() + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}
									
									email.sendMessage();								
								} catch (Exception e) {
									
								}
							}
						}
					}
				}
			}
		}	
	}
	
	/**
	 * Preenche a ordem de compra com as dados de solicita��o de compra
	 *
	 * @param ordemcompra
	 * @param whereInSolicitacoes
	 * @author Luiz Fernando
	 */
	public void criaOrdemcompraBySolicitacao(Ordemcompra ordemcompra, String whereInSolicitacoes) {
		List<Solicitacaocompra> listaSolicitacao = solicitacaocompraService.findForOrdemcompra(whereInSolicitacoes);
		List<Requisicaomaterial> listaRequisicaomaterial = requisicaomaterialService.findBySolicitacaocompraWithArquivo(whereInSolicitacoes);
		
		if(listaSolicitacao != null && !listaSolicitacao.isEmpty()){
			solicitacaocompraService.ajustaQtdeUnidademedida(listaSolicitacao);
			ordemcompra.setWhereInSolicitacaocompra(whereInSolicitacoes);
			List<Ordemcompramaterial> listaOrdemcompramaterial = new ArrayList<Ordemcompramaterial>();
			Ordemcompramaterial ordemcompramaterial;
			for(Solicitacaocompra solicitacaocompra : listaSolicitacao){
				if(ordemcompra.getEmpresa() == null && solicitacaocompra.getEmpresa() != null)
					ordemcompra.setEmpresa(solicitacaocompra.getEmpresa());
				if(ordemcompra.getLocalarmazenagem() == null)
					ordemcompra.setLocalarmazenagem(solicitacaocompra.getLocalarmazenagem());
				if(ordemcompra.getFaturamentocliente() == null)
					ordemcompra.setFaturamentocliente(solicitacaocompra.getFaturamentocliente());
				
				ordemcompramaterial = new Ordemcompramaterial();
				ordemcompramaterial.setMaterial(solicitacaocompra.getMaterial());
				if(solicitacaocompra.getContagerencial() != null){
					ordemcompramaterial.setContagerencial(solicitacaocompra.getContagerencial());
				}else {
					ordemcompramaterial.setContagerencial(solicitacaocompra.getMaterial().getContagerencial());
				}
				ordemcompramaterial.setQtdpedida(solicitacaocompra.getQtde());
				ordemcompramaterial.setQtdefrequencia(solicitacaocompra.getQtdefrequencia());
				if(solicitacaocompra.getUnidademedida() != null)
					ordemcompramaterial.setUnidademedida(solicitacaocompra.getUnidademedida());
				else if(solicitacaocompra.getMaterial().getUnidademedida() != null){
					ordemcompramaterial.setUnidademedida(solicitacaocompra.getMaterial().getUnidademedida());
				}
				
				addSolicitacaocompraOrdemcompramaterial(ordemcompramaterial, solicitacaocompra, ordemcompramaterial.getQtdpedida());
				
				ordemcompramaterial.setFrequencia(solicitacaocompra.getFrequencia());
				ordemcompramaterial.setCentrocusto(solicitacaocompra.getCentrocusto());
				ordemcompramaterial.setProjeto(solicitacaocompra.getProjeto());
				ordemcompramaterial.setLocalarmazenagem(solicitacaocompra.getLocalarmazenagem());
				ordemcompramaterial.setDtentrega(solicitacaocompra.getDataEntrega());
				ordemcompramaterial.setAltura(solicitacaocompra.getAltura());
				ordemcompramaterial.setComprimento(solicitacaocompra.getComprimento());
				ordemcompramaterial.setLargura(solicitacaocompra.getLargura());
				ordemcompramaterial.setPesototal(solicitacaocompra.getPesototal());
				ordemcompramaterial.setQtdvolume(solicitacaocompra.getQtdvolume());
				ordemcompramaterial.setObservacao(solicitacaocompra.getObservacao());
				
				if(solicitacaocompra.getFaturamentocliente() != null && solicitacaocompra.getFaturamentocliente()){
					ordemcompra.setFaturamentocliente(solicitacaocompra.getFaturamentocliente());
				}
				
				listaOrdemcompramaterial.add(ordemcompramaterial);				
			}
			ordemcompra.setListaMaterial(this.agrupaItensOrdemCompra(listaOrdemcompramaterial));
//			ordemcompra.setObservacao(observacao);
			if(ordemcompra.getListaMaterial() != null && ordemcompra.getListaMaterial().size() > 0)
				this.calculaRateio(ordemcompra);
		}
		
		Set<Ordemcompraarquivo> listaArquivo = new ListSet<Ordemcompraarquivo>(Ordemcompraarquivo.class);
		for (Requisicaomaterial rm : listaRequisicaomaterial) {
			if(rm.getArquivo() != null && rm.getArquivo().getCdarquivo() != null){
				Arquivo arquivocarregado = arquivoService.loadWithContents(rm.getArquivo());
//				arquivocarregado.setCdarquivo(null);
				
				Ordemcompraarquivo ordemcompraarquivo = new Ordemcompraarquivo();
				ordemcompraarquivo.setArquivo(arquivocarregado);
				
				listaArquivo.add(ordemcompraarquivo);
			}
		}
		ordemcompra.setListaArquivosordemcompra(listaArquivo);
	}
	
	/**
	 * Cria c�pia da ordem de compra
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompra criarCopia(Ordemcompra origem) {
		origem = loadForEntrada(origem);
		
		Ordemcompra copia = new Ordemcompra();
		copia.setFornecedor(origem.getFornecedor());
		copia.setContato(origem.getContato());
		copia.setLocalarmazenagem(origem.getLocalarmazenagem());
		copia.setEmpresa(origem.getEmpresa());
		copia.setDocumentotipo(origem.getDocumentotipo());
		copia.setDtcriacao(new Date(System.currentTimeMillis()));
		copia.setObservacao(origem.getObservacao());
		copia.setObservacoesinternas(origem.getObservacoesinternas());
		copia.setValoricmsst(origem.getValoricmsst());
		copia.setDesconto(origem.getDesconto());
		copia.setFrete(origem.getFrete());
		copia.setPorcentagemfrete(origem.getPorcentagemfrete());
		copia.setTipofrete(origem.getTipofrete());
		copia.setFornecedor(origem.getFornecedor());
		copia.setGarantia(origem.getGarantia());
		copia.setPrazopagamento(origem.getPrazopagamento());
		copia.setFaturamentocliente(origem.getFaturamentocliente());
		
		copia.setListaMaterial(this.copiaListaMaterial(origem.getListaMaterial()));
		
		if(origem.getRateio() != null && origem.getRateio().getCdrateio() != null){
			Rateio rateio = rateioService.findRateio(origem.getRateio());
			if(rateio != null){
				copia.setRateio(rateio);
				rateioService.limpaReferenciaRateio(copia.getRateio());
			}
		}
		
		return copia;
	}
	
	/**
	 * M�todo que faz a copia da lista de material da ordem de compra
	 *
	 * @param listaMaterial
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Ordemcompramaterial> copiaListaMaterial(List<Ordemcompramaterial> listaMaterial) {
		List<Ordemcompramaterial> listaNova = new ArrayList<Ordemcompramaterial>();
		
		Double valorunitario;
		Double qtdepedida;
		if(listaMaterial != null && !listaMaterial.isEmpty()){
			Ordemcompramaterial ordemcompramaterial;
			for(Ordemcompramaterial ocm : listaMaterial){
				ordemcompramaterial = new Ordemcompramaterial();
				ordemcompramaterial.setMaterial(ocm.getMaterial());
				ordemcompramaterial.setContagerencial(ocm.getContagerencial());
				ordemcompramaterial.setQtdpedida(ocm.getQtdpedida());
				ordemcompramaterial.setUnidademedida(ocm.getUnidademedida());
				ordemcompramaterial.setFrequencia(ocm.getFrequencia());
				ordemcompramaterial.setQtdefrequencia(ocm.getQtdefrequencia());
				ordemcompramaterial.setContagerencial(ocm.getContagerencial());
				ordemcompramaterial.setCentrocusto(ocm.getCentrocusto());
				ordemcompramaterial.setProjeto(ocm.getProjeto());
				
				if(ocm.getQtdpedida() != null && ocm.getQtdefrequencia() != null && 
						ocm.getValor() != null){
					qtdepedida = ocm.getQtdpedida() * ocm.getQtdefrequencia();
					valorunitario = ocm.getValor() / qtdepedida;
					ordemcompramaterial.setValorunitario(valorunitario);
				}
				ordemcompramaterial.setValor(ocm.getValor());
				ordemcompramaterial.setDtentrega(ocm.getDtentrega());
				ordemcompramaterial.setIcmsiss(ocm.getIcmsiss());
				ordemcompramaterial.setIcmsissincluso(ocm.getIcmsissincluso());
				ordemcompramaterial.setIpi(ocm.getIpi());
				ordemcompramaterial.setIpiincluso(ocm.getIpiincluso());
				
				listaNova.add(ordemcompramaterial);
			}
		}
		return listaNova;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#loadWithCotacao(Ordemcompra ordemcompra)
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompra loadWithCotacao(Ordemcompra ordemcompra) {
		return ordemcompraDAO.loadWithCotacao(ordemcompra);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Ordemcompra> findOrdensForEntrada(String whereIn) {
		return ordemcompraDAO.findOrdensForEntrada(whereIn);
	}
	
	public List<Ordemcompra> findByItens(String whereIn) {
		return ordemcompraDAO.findByItens(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#podeEditar(Ordemcompra ordemcompra)
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean podeEditar(Ordemcompra ordemcompra){
		return ordemcompraDAO.podeEditar(ordemcompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findByDocumento(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findByDocumento(Documento documento) {
		return ordemcompraDAO.findByDocumento(documento);
	}
	
	/**
	 * M�todo que cria ordem de compra a partir do arquivo csv
	 *
	 * @param arquivo
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompra criaOrdemcompraByImportacao(Arquivo arquivo, Fornecedor fornecedor) {
		Ordemcompra ordemcompra = new Ordemcompra();
		ordemcompra.setFornecedor(fornecedor);
		List<Ordemcompramaterial> listaItens = new ArrayList<Ordemcompramaterial>();
		Ordemcompramaterial ordemcompramaterial;
		Material material;
		Unidademedida unidademedida;
		List<String> listaErro = new ArrayList<String>();
		if(arquivo != null){
			String strFile = new String(arquivo.getContent());
			String[] linhas = strFile.split("\\r?\\n");
			
			for (int i = 1; i < linhas.length; i++) {
				if(linhas[i] != null && !linhas[i].equals("")){
					String[] campos = linhas[i].split(";");
					
					ImportacaoDadosOrdemcompraBean bean = new ImportacaoDadosOrdemcompraBean();
					
					try{
						bean.setLinha(i+1);
						if(campos.length > 0){
						
							if(campos.length > 0) bean.setIdentificador(campos[0]);
							if(campos.length > 1) bean.setQuantidadeStr(campos[1]);
							if(campos.length > 2) bean.setUnidademedidaStr(campos[2]);
							if(campos.length > 3) bean.setValorunitarioStr(campos[3]);
							if(campos.length > 4) bean.setPercentualIcmsissStr(campos[4]);
							if(campos.length > 5) bean.setIcmsissStr(campos[5]);
							if(campos.length > 6) bean.setPercentualIpiStr(campos[6]);
							if(campos.length > 7) bean.setIpiStr(campos[7]);
							
							if(bean.getIdentificador() == null || bean.getIdentificador().equals("")){
								listaErro.add("O campo Identificador do material n�o foi preenchido. Linha: " + (i+1));
							} else {
								material = materialService.getMaterialByIdentificacao(bean.getIdentificador());
								if(material != null){
									ordemcompramaterial = new Ordemcompramaterial();
									ordemcompramaterial.setMaterial(material);
									ordemcompramaterial.setUnidademedida(material.getUnidademedida());
									ordemcompramaterial.setContagerencial(material.getContagerencial());
									ordemcompramaterial.setQtdefrequencia(1.0);
									ordemcompramaterial.setFrequencia(new Frequencia(1));
									ordemcompramaterial.setFrequenciatrans(new Frequencia(1));
									if(bean.getQuantidadeStr() != null && !bean.getQuantidadeStr().equals("")){
										try{
											Double qtde = Double.parseDouble(bean.getQuantidadeStr());
											ordemcompramaterial.setQtdpedida(qtde);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getUnidademedidaStr() != null && !bean.getUnidademedidaStr().equals("")){
										try{
											unidademedida = unidademedidaService.findBySimbolo(bean.getUnidademedidaStr().trim());
											if(unidademedida != null)
												ordemcompramaterial.setUnidademedida(unidademedida);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getValorunitarioStr() != null && !bean.getValorunitarioStr().equals("")){
										try{
											Double valorunitario = Double.valueOf(bean.getValorunitarioStr().replace(",", "."));
											ordemcompramaterial.setValorunitario(valorunitario);
											ordemcompramaterial.setValor(valorunitario*ordemcompramaterial.getQtdpedida());
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getPercentualIcmsissStr() != null && !bean.getPercentualIcmsissStr().equals("")){
										try{
											Double percentualicmsiss = Double.valueOf(bean.getPercentualIcmsissStr().replace(",", "."));
											ordemcompramaterial.setPercentualicmsiss(percentualicmsiss);
											if(ordemcompramaterial.getValor() != null){
												ordemcompramaterial.setIcmsiss((ordemcompramaterial.getValor() * percentualicmsiss) / 100);
												ordemcompramaterial.setIcmsiss(SinedUtil.round(ordemcompramaterial.getIcmsiss(), 2));
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getIcmsissStr() != null && bean.getIcmsissStr().equals("S")){
										try{
											ordemcompramaterial.setIcmsissincluso(true);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getPercentualIpiStr() != null && !bean.getPercentualIpiStr().equals("")){
										try{
											Double percentualipi = Double.valueOf(bean.getPercentualIpiStr().replace(",", "."));
											ordemcompramaterial.setPercentualipi(percentualipi);
											if(ordemcompramaterial.getValor() != null){
												ordemcompramaterial.setIpi((ordemcompramaterial.getValor() * percentualipi) / 100);
												ordemcompramaterial.setIpi(SinedUtil.round(ordemcompramaterial.getIpi(), 2));
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getIpiStr() != null && bean.getIpiStr().equals("S")){
										try{
											ordemcompramaterial.setIpiincluso(true);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(ordemcompramaterial.getIcmsissincluso() == null){
										ordemcompramaterial.setIcmsissincluso(false);
									}
									if(ordemcompramaterial.getIpiincluso() == null){
										ordemcompramaterial.setIpiincluso(false);
									}
									listaItens.add(ordemcompramaterial);
								}else {
									listaErro.add("O material n�o foi encontrado. Identificador: " + bean.getIdentificador());
								}
							}
						}
					} catch (Exception e) {
						listaErro.add(e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}
		
		if(listaErro.size() > 0)
			ordemcompra.setListaErros(listaErro);
		
		ordemcompra.setListaMaterial(listaItens);
		this.calculaRateio(ordemcompra);
		return ordemcompra;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForGerarfornecimento(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForGerarfornecimento(String whereIn) {
		return ordemcompraDAO.findForGerarfornecimento(whereIn);
	}
	
	/**
	 * M�todo que atualiza a ordem de compra (campo dtbaixa) ap�s gerar contrato de fornecimento
	 *
	 * @param whereIn
	 * @param fornecimentocontrato
	 * @author Luiz Fernando
	 */
	public void baixarAposGerarfornecimento(String whereIn, Fornecimentocontrato fornecimentocontrato) {
		if(whereIn != null && !"".equals(whereIn) && fornecimentocontrato != null && fornecimentocontrato.getCdfornecimentocontrato() != null){
			List<Ordemcompra> listaOrdemcompra = new ArrayList<Ordemcompra>();
			for(String id : whereIn.split(",")){
				listaOrdemcompra.add(new Ordemcompra(Integer.parseInt(id)));
			}
			ordemcomprafornecimentocontratoService.salvaOrdemcomprafornecimentocontrato(listaOrdemcompra, fornecimentocontrato);
			this.updateDtbaixa(whereIn, new Date(System.currentTimeMillis()));
			Ordemcomprahistorico ordemcomprahistorico = new Ordemcomprahistorico(whereIn);
			ordemcomprahistorico.setObservacao("Gerado contrato de fornecimento: <a href=\"javascript:visualizarFornecimentocontrato(" + fornecimentocontrato.getCdfornecimentocontrato() +")\">" + fornecimentocontrato.getCdfornecimentocontrato() + "</a>");
			this.saveHistoricoEUpdateSituacao(ordemcomprahistorico, Ordemcompraacao.BAIXADA, BAIXADA, null, null);
		}
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#updateDtbaixa(String whereIn, Date dtbaixa)
	 *
	 * @param whereIn
	 * @param dtbaixa
	 * @author Luiz Fernando
	 */
	public void updateDtbaixa(String whereIn, Date dtbaixa) {
		ordemcompraDAO.updateDtbaixa(whereIn, dtbaixa);
	}
	
	public List<Ordemcompra> findWithOrigem(String whereInOrdemcompra) {
		return ordemcompraDAO.findWithOrigem(whereInOrdemcompra);
	}
	
	/**
	 * M�todo que cria o documento a apartir da ordem de compra
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public Documento criarAntecipacaoOrdemcompra(WebRequestContext request) {
		String whereInOrdemcompra = request.getParameter("whereInOrdemcompra");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String controller = "/suprimento/crud/Ordemcompra" + (entrada ? "?ACAO=consultar&cdordemcompra=" + whereInOrdemcompra : "");
		Documento documento = new Documento();
		
		if(whereInOrdemcompra != null && !whereInOrdemcompra.equals("")){
			documento.setController(controller);
			documento.setWhereInOrdemcompra(whereInOrdemcompra);
			Ordemcompra ordemcompra = this.loadForGerarAntecipacao(new Ordemcompra(Integer.parseInt(whereInOrdemcompra)));
			
			documento.setEmpresa(ordemcompra.getEmpresa());
			documento.setFornecedor(ordemcompra.getFornecedor());
			documento.setDtemissao(new Date(System.currentTimeMillis()));
			documento.setDtvencimento(new Date(System.currentTimeMillis()));
			documento.setValor(ordemcompra.getValorTotalOrdemCompraMoney());
			documento.setPrazo(ordemcompra.getPrazopagamento());
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
			documento.setDescricao("Conta a pagar referente a OC: " + whereInOrdemcompra);
			
			List<Documentotipo> listaDocumentotipo = documentotipoService.findAntecipacao();
			if(listaDocumentotipo != null && !listaDocumentotipo.isEmpty()){
				documento.setDocumentotipo(listaDocumentotipo.get(0));
			}
			
			if(ordemcompra.getPrazopagamento() != null && ordemcompra.getPrazopagamento().getCdprazopagamento() != null){
				this.addParcelasForDocumento(documento);
			}
			
			if((ordemcompra.getRateio() == null || ordemcompra.getRateio().getCdrateio() == null || ordemcompra.getRateio().getListaRateioitem() == null ||
					ordemcompra.getRateio().getListaRateioitem().isEmpty()) &&
					(ordemcompra.getRateioverificado() == null || !ordemcompra.getRateioverificado())){
				this.calculaRateioAPartirDaCotacao(ordemcompra);
			}else {
				ordemcompra.setRateio(rateioService.findRateio(ordemcompra.getRateio()));
			}
			
			documento.setRateio(ordemcompra.getRateio());
			rateioService.limpaReferenciaRateio(documento.getRateio());
			rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
		}		
		
		return documento;
	}
	
	/**
	 * M�todo que adiciona as parcelas ao documento
	 *
	 * @param documento
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	private void addParcelasForDocumento(Documento documento) {
		documento.setRepeticoes(1);
		
		Entregapagamento entregapagamento = new Entregapagamento();
		entregapagamento.setDtvencimento(documento.getDtvencimento());
		entregapagamento.setPrazo(documento.getPrazo());
		entregapagamento.setRepeticoes(documento.getRepeticoes());
		entregapagamento.setValor(documento.getValor());
		
		List<Entregapagamento> listaEntregapagamento = entregaService.geraParcelamento(entregapagamento);
		
		if(listaEntregapagamento != null && !listaEntregapagamento.isEmpty()){
			Entregapagamento ep;
			Documento doc;
			List<Documento> listaDoc = new ArrayList<Documento>();
			for(int i = 0; i < listaEntregapagamento.size(); i++){
				ep = listaEntregapagamento.get(i);
				if(i == 0){
					documento.setValor(ep.getValor());
					documento.setDtvencimento(ep.getDtvencimento());
				}else {
					documento.setFinanciamento(true);
					
					doc =  new Documento();
					doc.setParcela(i+1);
					doc.setDtvencimento(ep.getDtvencimento());
					doc.setValor(ep.getValor());
					doc.setDocumentoacao(Documentoacao.DEFINITIVA);
					
					listaDoc.add(doc);
				}
			}
			
			if(listaDoc != null && !listaDoc.isEmpty()){
				documento.setListaDocumento(listaDoc);
			}else {
				documento.setPrazo(null);
				documento.setRepeticoes(null);
			}
		}
	}
	
	public Ordemcompra loadForGerarAntecipacao(Ordemcompra ordemcompra) {
		return ordemcompraDAO.loadForGerarAntecipacao(ordemcompra);
	}
	
	public Situacaopedidosolicitacaocompra getSituacaoOrdemcompraByPedidovenda(Pedidovenda pedidovenda) {
		Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra = null;
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			Double qtdesolicitacao = this.getQteOrdemByPedidovenda(pedidovenda);
			if(qtdesolicitacao > 0){
				Double qtdebaixada = this.getQteOrdemBaixadaByPedidovenda(pedidovenda);
				if(qtdebaixada < qtdesolicitacao){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_PARCIAL;
				}else if(qtdebaixada.compareTo(qtdesolicitacao) == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_REALIZADA;
				}
			}
		}
		return situacaopedidosolicitacaocompra;
	}
	
	public Situacaopedidosolicitacaocompra getSituacaoOrdemcompraByVendaorcamento(Vendaorcamento vendaorcamento) {
		Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra = null;
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null){
			Double qtdesolicitacao = this.getQteOrdemByOrcamento(vendaorcamento);
			if(qtdesolicitacao > 0){
				Double qtdebaixada = this.getQteOrdemBaixadaByOrcamento(vendaorcamento);
				if(qtdebaixada < qtdesolicitacao){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_PARCIAL;
				}else if(qtdebaixada.compareTo(qtdesolicitacao) == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_REALIZADA;
				}
			}
		}
		return situacaopedidosolicitacaocompra;
	}
	
	public Double getQteOrdemBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		return ordemcompraDAO.getQteOrdemBaixadaByPedidovenda(pedidovenda);
	}
	
	public Double getQteOrdemBaixadaByOrcamento(Vendaorcamento vendaorcamento) {
		return ordemcompraDAO.getQteOrdemBaixadaByOrcamento(vendaorcamento);
	}
	
	public Double getQteOrdemByPedidovenda(Pedidovenda pedidovenda) {
		return ordemcompraDAO.getQteOrdemByPedidovenda(pedidovenda);
	}
	
	public Double getQteOrdemByOrcamento(Vendaorcamento vendaorcamento) {
		return ordemcompraDAO.getQteOrdemByOrcamento(vendaorcamento);
	}
	
	public List<Ordemcompra> findOrdemCompraWithEmpresa(String whereIn) {
		return ordemcompraDAO.findOrdemCompraWithEmpresa(whereIn);
	}
	public LinkedList<OrdemCompraBean> gerarOrdemCompraBean(String itensSelecionados) {

		LinkedList<OrdemCompraBean> listaRelatorio = new LinkedList<OrdemCompraBean>();
		
		Date dtemissao = new Date(System.currentTimeMillis());
		DateFormat anoFormat = new SimpleDateFormat("yy");
		boolean exibirInspecao = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INSPECAOORDEMCOMPRA));
		
		List<Ordemcompra> ordens = findOrdensCompra(itensSelecionados);
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		Endereco enderecoEmpresaPrincipal = null;
		if(empresaPrincipal != null)
			enderecoEmpresaPrincipal = enderecoService.carregaEnderecoEmpresa(empresaPrincipal);
		
		for (Ordemcompra ordemcompra : ordens) {
			if(ordemcompra.getFornecedor() != null && ordemcompra.getFornecedor().getCdpessoa() != null){
				ordemcompra.setFornecedor(fornecedorService.carregaFornecedor(ordemcompra.getFornecedor()));
				buscaEnderecoFornecedor(ordemcompra.getFornecedor());
			}
			if(ordemcompra.getContato() != null && ordemcompra.getContato().getCdpessoa() != null){
				ordemcompra.setContato(contatoService.carregaContato(ordemcompra.getContato()));
				buscaTelefoneContatoFornecedor(ordemcompra.getContato());
			}
			if(ordemcompra.getFornecedoroptriangular() != null && ordemcompra.getFornecedoroptriangular().getCdpessoa() != null){
				ordemcompra.setFornecedoroptriangular(fornecedorService.carregaFornecedor(ordemcompra.getFornecedoroptriangular()));
				if(ordemcompra.getEnderecoentrega() != null && ordemcompra.getEnderecoentrega().getCdendereco() != null){
					ordemcompra.setEnderecoentrega(enderecoService.loadEndereco(ordemcompra.getEnderecoentrega()));
				}
			}
			if(ordemcompra.getEmpresa() != null && ordemcompra.getEmpresa().getCdpessoa() != null){
				ordemcompra.setEmpresa(empresaService.loadWithEnderecoTelefone(ordemcompra.getEmpresa()));
			}
			if(ordemcompra.getTransportador() != null && ordemcompra.getTransportador().getCdpessoa() != null){
				ordemcompra.setTransportador(fornecedorService.carregaFornecedor(ordemcompra.getTransportador()));
			}
			for (Ordemcompramaterial ocm : ordemcompra.getListaMaterial()) {
				if(ocm.getUnidademedida() != null){
					ocm.getMaterial().setUnidademedidaString(ocm.getUnidademedida().getSimbolo());
				}
			}

			OrdemCompraBean bean =  new OrdemCompraBean();
			
			if(ordemcompra.getPessoaquestionario()!= null && ordemcompra.getPessoaquestionario().getCdpessoaquestionario()!= null){
				AvaliacaoBean avaliacaoBean = new AvaliacaoBean();
				LinkedList<AvaliacaoItemBean> avaliacaoItemBeans = new LinkedList<AvaliacaoItemBean>();
				ordemcompra.setPessoaquestionario(pessoaquestionarioService.findPessoaQuestinario(ordemcompra.getPessoaquestionario()));
				avaliacaoBean.setPontuacao(ordemcompra.getPessoaquestionario().getPontuacao() != null ? ordemcompra.getPessoaquestionario().getPontuacao() : 0);
				for (Pessoaquestionarioquestao pessoaquestionarioquestao : ordemcompra.getPessoaquestionario().getListaPessoaQuestionarioQuestao()) {
					AvaliacaoItemBean avaliacaoItemBean = new AvaliacaoItemBean();
					avaliacaoItemBean.setPergunta(pessoaquestionarioquestao.getQuestionarioquestao().getQuestao());
					if (pessoaquestionarioquestao.getQuestionarioquestao().getTiporesposta().getDesricao().equalsIgnoreCase("Sim/N�o")){ 
						avaliacaoItemBean.setResposta(pessoaquestionarioquestao.getSimString());
					} else {
						avaliacaoItemBean.setResposta(pessoaquestionarioquestao.getResposta());
					}
					avaliacaoItemBean.setPontuacao(pessoaquestionarioquestao.getPontuacao() != null ? pessoaquestionarioquestao.getPontuacao() : 0);
					avaliacaoItemBeans.add(avaliacaoItemBean);
				}
				avaliacaoBean.setAvaliacaoItemBeans(avaliacaoItemBeans);
				bean.setAvaliacaoBean(avaliacaoBean);
			}
			
			List<Ordemcompramaterial> list = new ArrayList<Ordemcompramaterial>();
			list.addAll(ordemcompra.getListaMaterial());
			Produto produto; 
			for (Ordemcompramaterial ordemcompramaterial : list) {
				produto = new Produto();
				produto = produtoService.carregaProduto(ordemcompramaterial.getMaterial());
				ordemcompramaterial.getMaterial().setProduto_largura(produto != null ? produto.getLargura() : null);
				ordemcompramaterial.getMaterial().setProduto_comprimento(produto != null ? produto.getComprimento() : null);
				ordemcompramaterial.getMaterial().setProduto_altura(produto != null ? produto.getAltura() : null);
				
				if(ordemcompramaterial.getQtdvolume() != null && ordemcompramaterial.getQtdvolume() > 0d){
					ordemcompramaterial.getMaterial().setNome(ordemcompramaterial.getMaterial().getNome() + "\nQtde. Volume(s): " + SinedUtil.descriptionDecimal(ordemcompramaterial.getQtdvolume()));
				}
			}
			
			Collections.sort(list,new Comparator<Ordemcompramaterial>(){
				public int compare(Ordemcompramaterial o1, Ordemcompramaterial o2) {
					return o1.getCdordemcompramaterial().compareTo(o2.getCdordemcompramaterial());
				}
			});
			
			ordemcompra.setListaMaterialTrans(list);
			
			for (Ordemcompramaterial item : list){
				
				OrdemcompraMaterialBean materialBean = new OrdemcompraMaterialBean();
				materialBean.setNome(item.getMaterial().getNome());
				materialBean.setDescricao(item.getMaterial().getAutocompleteDescription());
				materialBean.setUnidade_medida(item.getMaterial().getUnidademedida().getSimbolo());
				materialBean.setLargura(item.getMaterial().getProduto_largura());
				materialBean.setComprimento(item.getMaterial().getProduto_comprimento());
				materialBean.setAltura(item.getMaterial().getProduto_altura());

				OrdemCompraItemBean itemBean = new OrdemCompraItemBean();
				itemBean.setQtde_pedida(item.getQtdpedida());
				itemBean.setMaterial(materialBean);
				itemBean.setValor(item.getValor());
				itemBean.setIcms_iss(item.getIcmsiss());
				itemBean.setIpi(item.getIpi());
				itemBean.setIcms_iss_incluso(item.getIcmsissincluso());
				itemBean.setIpi_incluso(item.getIpiincluso());
				itemBean.setFrequencia(item.getFrequenciaStr());
				itemBean.setQtde_frequencia(item.getQtdefrequencia());
				itemBean.setConta_gerencial(item.getContagerencial().getNome());
				itemBean.setObservacao(item.getObservacao());
				itemBean.setUnidade_medida(item.getUnidademedida() != null ? item.getUnidademedida().getSimbolo() : "");
				itemBean.setObservacao(item.getObservacao());
				
				bean.getItens().add(itemBean);

			}
			
			bean.setNumero(ordemcompra.getCdordemcompra());
			bean.setAno(ordemcompra.getDtcriacao() != null ? anoFormat.format(ordemcompra.getDtcriacao()) : anoFormat.format(dtemissao));
			bean.setObservacao(ordemcompra.getObservacao());
			bean.setExibir_inspecao(exibirInspecao);
			bean.setInspecao(Util.strings.emptyIfNull(ordemcompra.getInspecao()));
			bean.setObservacao_grupo_material(materialgrupoService.makeStringObservacao(ordemcompramaterialService.findByOrdemcompra(ordemcompra)));
			bean.setTransportador(ordemcompra.getTransportador() != null && ordemcompra.getTransportador().getNome() != null ? ordemcompra.getTransportador().getNome() : "");
			bean.setTransportador_telefone(ordemcompra.getTransportador() != null && ordemcompra.getTransportador().getTelefone() != null ? ordemcompra.getTransportador().getTelefone().getTelefone() : "");
			bean.setFrete(ordemcompra.getFrete());
			bean.setDesconto(ordemcompra.getDesconto());
			bean.setPrazo_pagamento(ordemcompra.getPrazopagamento().getNome());
			bean.setIcms_st(new Double(ordemcompra.getValoricmsst() != null ? ordemcompra.getValoricmsst().getValue().doubleValue() : 0d));
			bean.setData_emissao(ordemcompra.getDtcriacao() != null ? SinedDateUtils.toString(ordemcompra.getDtcriacao()) : SinedDateUtils.toString(dtemissao));
			bean.setTotal(new Money(ordemcompra.getValorTotalOrdemCompraReport()));
			bean.setTotal_item(new Money(ordemcompra.getValorTotalOrdemCompra()));

			if (ordemcompra.getDtproximaentrega() != null)
				bean.setProxima_entrega(SinedDateUtils.toString(ordemcompra.getDtproximaentrega()));

			if (ordemcompra.getTipofrete() != null)
				bean.setTipo_frete(ordemcompra.getTipofrete().getDescricao());

			if (ordemcompra.getGarantia() != null)
				bean.setGarantia(ordemcompra.getGarantia().getDescricao());

			if (ordemcompra.getDocumentotipo() != null)
				bean.setTipo_documento(ordemcompra.getDocumentotipo().getNome());

			Ordemcomprahistorico ordemcomprahistorico = ordemcomprahistoricoService.getPrimeiroHistorico(ordemcompra);
			if(ordemcomprahistorico != null)
				bean.setData_criacao(SinedDateUtils.toString(ordemcomprahistorico.getDtaltera()));
			else
				bean.setData_criacao(SinedDateUtils.toString(ordemcompra.getDtcriacao()));
			
			
			List<Ordemcomprahistorico> listaHistorico = ordemcomprahistoricoService.getListaOrdemCompraHistoricoForReport(ordemcompra);
			for (Ordemcomprahistorico och : listaHistorico) {
				if (och.getCdusuarioaltera() != null){
					Usuario usuario = new Usuario();
					usuario.setCdpessoa(och.getCdusuarioaltera());
					if (och.getOrdemcompraacao().equals(Ordemcompraacao.CRIADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						bean.setCriada_por(usuario.getNome().toUpperCase());
					} else if (och.getOrdemcompraacao().equals(Ordemcompraacao.AUTORIZADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						bean.setAutorizada_por(usuario.getNome().toUpperCase());
					} else if (och.getOrdemcompraacao().equals(Ordemcompraacao.APROVADA)){
						usuario = usuarioService.carregaUsuario(usuario);
						bean.setAprovada_por(usuario.getNome().toUpperCase());
					}
				}
			}
			
			OrdemcompraContatoBean contatoBean = new OrdemcompraContatoBean();
			if (ordemcompra.getContato() != null){
				contatoBean.setNome(ordemcompra.getContato().getNome());
				if (ordemcompra.getContato().getTelefone() != null)
					contatoBean.setTelefone(ordemcompra.getContato().getTelefone().getTelefone());
				if (ordemcompra.getContato().getFax() != null)
					contatoBean.setFax(ordemcompra.getContato().getFax().getTelefone());
				contatoBean.setEmail(ordemcompra.getContato().getEmailcontato());
			}
			bean.setContato(contatoBean);
			
			OrdemcompraFornecedorBean fornecedorBean = new OrdemcompraFornecedorBean();
			if (ordemcompra.getFornecedor() != null){
				fornecedorBean.setNome(ordemcompra.getFornecedor().getNome());
				fornecedorBean.setRazao_social(ordemcompra.getFornecedor().getRazaosocial());
				fornecedorBean.setCpf_cnpj(ordemcompra.getFornecedor().getCpfOuCnpj());
				Endereco endereco = ordemcompra.getFornecedor().getEnderecoPrincipal();
				if (endereco != null){
					fornecedorBean.setLogradouro(endereco.getLogradouroCompleto());
					fornecedorBean.setBairro(endereco.getBairro());
					if (endereco.getCep() != null)
						fornecedorBean.setCep(endereco.getCep().getValue());
					if (endereco.getMunicipio() != null){
						fornecedorBean.setMunicipio(endereco.getMunicipio().getNome());
						fornecedorBean.setUf(endereco.getMunicipio().getUf().getSigla());
					}
				}
				Dadobancario dadobancario = ordemcompra.getFornecedor().getDadobancario();
				if (dadobancario != null){
					fornecedorBean.setBanco(dadobancario.getBanco() != null ? dadobancario.getBanco().getNome() : "");
					if (dadobancario.getTipoconta() != null)
						fornecedorBean.setTipoconta(dadobancario.getTipoconta().getNome());
					fornecedorBean.setOperacao(dadobancario.getOperacao());
					if(Util.strings.isEmpty(dadobancario.getDvagencia()))
						fornecedorBean.setAgencia(dadobancario.getAgencia());
					else
						fornecedorBean.setAgencia(dadobancario.getAgencia() + "-" + dadobancario.getDvagencia());
					if (Util.strings.isEmpty(dadobancario.getDvconta()))
						fornecedorBean.setConta(dadobancario.getConta());
					else
						fornecedorBean.setConta(dadobancario.getConta() + "-" + dadobancario.getDvconta());
				}
			}
			bean.setFornecedor(fornecedorBean);
			
			OrdemcompraFornecedorBean fornecedoroptriangularBean = new OrdemcompraFornecedorBean();
			if (ordemcompra.getFornecedoroptriangular() != null){
				fornecedoroptriangularBean.setNome(ordemcompra.getFornecedoroptriangular().getNome());
				fornecedoroptriangularBean.setRazao_social(ordemcompra.getFornecedoroptriangular().getRazaosocial());
				fornecedoroptriangularBean.setCpf_cnpj(ordemcompra.getFornecedoroptriangular().getCpfOuCnpj());
				Endereco endereco = ordemcompra.getEnderecoentrega();
				if (endereco != null){
					fornecedoroptriangularBean.setLogradouro(endereco.getLogradouroCompleto());
					fornecedoroptriangularBean.setBairro(endereco.getBairro());
					if (endereco.getCep() != null)
						fornecedoroptriangularBean.setCep(endereco.getCep().getValue());
					if (endereco.getMunicipio() != null){
						fornecedoroptriangularBean.setMunicipio(endereco.getMunicipio().getNome());
						fornecedoroptriangularBean.setUf(endereco.getMunicipio().getUf().getSigla());
					}
				}
			}
			bean.setFornecedoroptriangular(fornecedoroptriangularBean);
			
			Localarmazenagem localarmazenagem = localarmazenagemService.loadForEntrada(ordemcompra.getLocalarmazenagem());
			bean.setLocal_armazenagem_nome(localarmazenagem.getNome());
			bean.setLocal_armazenagem_endereco(localarmazenagem.getEnderecoCompleto());
			
			
			Cliente cliente = null;
			if(ordemcompra.getFaturamentocliente() != null && ordemcompra.getFaturamentocliente() && ordemcompra.getRateio() != null &&
					ordemcompra.getRateio().getCdrateio() != null){
				List<Rateioitem> itensRateio = rateioitemService.findByRateioWithProjetoCliente(ordemcompra.getRateio());
				Projeto projeto = null;
				if(itensRateio != null && !itensRateio.isEmpty()){
					for(Rateioitem rateioitem : itensRateio){
						if(rateioitem.getProjeto() != null){
							if(projeto == null){
								projeto = rateioitem.getProjeto();
							}else if(!projeto.equals(rateioitem.getProjeto())){
								projeto = null;
								break;
							}
						}
					}
					if(projeto != null && projeto.getCliente() != null){
						cliente = projeto.getCliente();
					}
				}
			}
			
			Map<String, String> map = buscaProjetosContasgerenciaisRateioOrdemCompra(ordemcompra.getRateio());
			if(map != null){
				bean.setProjetos(map.get("PROJETOS"));
				bean.setContas_gerenciais(map.get("CONTASGERENCIAIS"));
			}
			
			
			Ordemcomprahistorico oh1 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.CRIADA);
			Ordemcomprahistorico oh2 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.AUTORIZADA);
			
			try{
				if(oh1 != null && oh1.getCdusuarioaltera() != null){
					Usuario usuarioElaboracao = usuarioService.carregaRubricaUsuario(new Usuario(oh1.getCdusuarioaltera()));
					if(usuarioElaboracao != null && usuarioElaboracao.getRubrica() != null){
						Arquivo rubrica = usuarioElaboracao.getRubrica();
						bean.setCriada_por_rubrica(SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + rubrica.getCdarquivo());
					}
				}
				
				if(oh2 != null && oh2.getCdusuarioaltera() != null){
					Usuario usuarioAutorizacao = usuarioService.carregaRubricaUsuario(new Usuario(oh2.getCdusuarioaltera()));
					if(usuarioAutorizacao != null && usuarioAutorizacao.getRubrica() != null){
						Arquivo rubrica = usuarioAutorizacao.getRubrica();
						bean.setAutorizada_por_rubrica(SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + rubrica.getCdarquivo());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(new SinedUtil().needAprovacao()){
				Ordemcomprahistorico oh3 = ordemcomprahistoricoService.getUsuarioLastAction(ordemcompra, Ordemcompraacao.APROVADA);
				try{
					if(oh3 != null && oh3.getCdusuarioaltera() != null){
						Usuario usuarioAprovada = usuarioService.carregaRubricaUsuario(new Usuario(oh3.getCdusuarioaltera()));
						if(usuarioAprovada != null && usuarioAprovada.getRubrica() != null){
							Arquivo rubrica = usuarioAprovada.getRubrica();
							bean.setAprovada_por_rubrica(SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + rubrica.getCdarquivo());
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
				bean.setExige_aprovacao(true);
			} else {
				bean.setExige_aprovacao(false);
			}
			
			Empresa empresa = null;
			Endereco enderecoEmpresa = null;
			
			Empresa empresaDe = null;
			Endereco enderecoDe = null;
			
			if(ordemcompra.getEmpresa() != null){
				empresa = ordemcompra.getEmpresa();
				enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(ordemcompra.getEmpresa());

				empresaDe = ordemcompra.getEmpresa();
				enderecoDe = enderecoEmpresa;
			} else if(empresaPrincipal != null){
				empresa = empresaPrincipal;
				enderecoEmpresa = enderecoEmpresaPrincipal;
				
				empresaDe = empresaPrincipal;
				enderecoDe = enderecoEmpresaPrincipal;
			}
			
			OrdemcompraClienteBean clienteBean =  new OrdemcompraClienteBean();
			
			if(cliente != null){
				enderecoDe = cliente.getEndereco();
				
				clienteBean.setNome(cliente.getNome());
				clienteBean.setCpf_cnpj(cliente.getCpfOuCnpj());
				clienteBean.setEmail(cliente.getEmail() != null && !cliente.getEmail().equals("") ? cliente.getEmail() : null);
				clienteBean.setInscricao_estadual(cliente.getInscricaoestadual());
				if(cliente.getListaTelefone() !=null || !cliente.getListaTelefone().isEmpty()){
					clienteBean.setTelefone(CollectionsUtil.listAndConcatenate( cliente.getListaTelefone(), "telefone", " / "));
				}
				
			}else if(empresaDe != null) {
				clienteBean.setNome(empresaDe.getRazaosocialOuNome());
				clienteBean.setCpf_cnpj(empresaDe.getCpfOuCnpj());
				clienteBean.setEmail(empresaDe.getEmailordemcompra() != null && !empresaDe.getEmailordemcompra().equals("") ? empresaDe.getEmailordemcompra() : empresaDe.getEmail());
				clienteBean.setInscricao_estadual(empresaDe.getInscricaoestadual());
				if(empresaDe.getListaTelefone() !=null || !empresaDe.getListaTelefone().isEmpty()){
					clienteBean.setTelefone(CollectionsUtil.listAndConcatenate( empresaDe.getListaTelefone(), "telefone", " / "));
				}
			}

			if(enderecoDe != null){
				clienteBean.setLogradouro(enderecoDe.getLogradouroCompleto());
				clienteBean.setBairro(enderecoDe.getBairro());
				if (enderecoDe.getCep() != null)
					clienteBean.setCep(enderecoDe.getCep().getValue());
				if (enderecoDe.getMunicipio() != null){
					clienteBean.setMunicipio(enderecoDe.getMunicipio().getNome());
					clienteBean.setUf(enderecoDe.getMunicipio().getUf().getSigla());
				}
			}
			
			bean.setCliente(clienteBean);

			OrdemcompraEmpresaBean empresaBean = new OrdemcompraEmpresaBean();
			if (empresa != null){
				empresaBean.setNome(empresa.getRazaosocialOuNome());
				empresaBean.setCpf_cnpj(empresa.getCpfOuCnpj());
				empresaBean.setEmail(empresa.getEmailordemcompra() != null && !empresa.getEmailordemcompra().equals("") ? empresa.getEmailordemcompra() : empresa.getEmail());
				empresaBean.setInscricao_estadual(empresa.getInscricaoestadual());
				if(empresa.getListaTelefone() !=null || !empresa.getListaTelefone().isEmpty()){
					empresaBean.setTelefone(CollectionsUtil.listAndConcatenate( empresa.getListaTelefone(), "telefone", " / "));
				}
				if(enderecoEmpresa != null){
					empresaBean.setLogradouro(enderecoEmpresa.getLogradouroCompleto());
					empresaBean.setBairro(enderecoEmpresa.getBairro());
					if (enderecoEmpresa.getCep() != null)
						empresaBean.setCep(enderecoEmpresa.getCep().getValue());
					if (enderecoEmpresa.getMunicipio() != null){
						empresaBean.setMunicipio(enderecoEmpresa.getMunicipio().getNome());
						empresaBean.setUf(enderecoEmpresa.getMunicipio().getUf().getSigla());
					}
				}
				
			}
			bean.setEmpresa(empresaBean);
			
			listaRelatorio.add(bean);
		}

		return listaRelatorio;
	}
	
	public void salvarCompraantecipada(Ordemcompra ordemcompra) {
		ordemcompraDAO.salvarCompraantecipada(ordemcompra);
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#existItemGrade(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public Boolean existItemGrade(String whereIn) {
		return ordemcompraDAO.existItemGrade(whereIn);
	}
	
	public List<Ordemcompramaterial> createListaByGrade(Set<Ordemcompramaterial> listaMateriais) {
		if(listaMateriais == null || listaMateriais.isEmpty())
			return null;
		if(listaMateriais.isEmpty())
			return new ArrayList<Ordemcompramaterial>();
		
		List<Ordemcompramaterial> lista = new ArrayList<Ordemcompramaterial>();
		lista.addAll(listaMateriais);
		return createListaByGrade(lista);
	}
	
	/**
	 * M�todo que cria a lista de Ordemcompramaterial apenas com materiais que n�o s�o itens da grade 
	 * (material que n�o � de grade / material mestre de grade)
	 *
	 * @param listaMateriais
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public List<Ordemcompramaterial> createListaByGrade(List<Ordemcompramaterial> listaMateriais) {
		if(listaMateriais == null || listaMateriais.isEmpty())
			return listaMateriais;
		
		List<Ordemcompramaterial> listaNova = new ArrayList<Ordemcompramaterial>();
		List<Ordemcompramaterial> listaMetreGrade = new ArrayList<Ordemcompramaterial>();
		for(Ordemcompramaterial ocm : listaMateriais){
			if(ocm.getMaterial() == null || ocm.getMaterial().getMaterialmestregrade() == null){
				listaNova.add(ocm);
			}else {
				boolean adicionar = true;
				if(listaMetreGrade != null && !listaMetreGrade.isEmpty()){
					for(Ordemcompramaterial ocmMestregrade : listaMetreGrade){
						if(ocmMestregrade.getMaterial().equals(ocm.getMaterial().getMaterialmestregrade())){
							adicionar = false;
							if(ocmMestregrade.getQtdpedida() == null) ocmMestregrade.setQtdpedida(0d);
							if(ocm.getQtdpedida() == null) ocm.setQtdpedida(0d);
							if(ocm.getQtdpedida() == null) ocm.setQtdpedida(0d);
							if(ocmMestregrade.getValor() == null) ocmMestregrade.setValor(0d);
							if(ocmMestregrade.getListaOrdemcompramaterialByMaterialmestregrade() == null)
								ocmMestregrade.setListaOrdemcompramaterialByMaterialmestregrade(new ArrayList<Ordemcompramaterial>());
							
							if(ocm.getValor() != null){
								ocmMestregrade.setValor(ocmMestregrade.getValor()+ocm.getValor());
							}
							ocmMestregrade.setQtdpedida(ocmMestregrade.getQtdpedida()+ocm.getQtdpedida());
							if(ocmMestregrade.getWhereInOrdemcompramaterialItemGrade() == null || 
									"".equals(ocmMestregrade.getWhereInOrdemcompramaterialItemGrade())){
								ocmMestregrade.setWhereInOrdemcompramaterialItemGrade(ocm.getCdordemcompramaterial().toString());
							}else {
								ocmMestregrade.setWhereInOrdemcompramaterialItemGrade(ocmMestregrade.getWhereInOrdemcompramaterialItemGrade()
										+ "," + ocm.getCdordemcompramaterial());
							}
							if(ocmMestregrade.getWhereInMaterialItenGrade() == null || 
									"".equals(ocmMestregrade.getWhereInMaterialItenGrade())){
								ocmMestregrade.setWhereInMaterialItenGrade(ocm.getMaterial().getCdmaterial().toString());
							}else {
								ocmMestregrade.setWhereInMaterialItenGrade(ocmMestregrade.getWhereInMaterialItenGrade()
										+ "," + ocm.getMaterial().getCdmaterial());
							}
							ocmMestregrade.getListaOrdemcompramaterialByMaterialmestregrade().add(ocm);
						}
					}
				}
				if(adicionar){
					Ordemcompramaterial ocmMestregrade = new Ordemcompramaterial(
							ocm.getMaterial().getMaterialmestregrade(), 
							ocm.getQtdpedida(), 
							ocm.getValor(), 
							ocm.getIcmsissincluso(), 
							ocm.getIcmsiss(), 
							ocm.getIpiincluso(), 
							ocm.getIpi(), 
							ocm.getFrequencia(), 
							ocm.getQtdefrequencia(), 
							ocm.getUnidademedida());
					
					ocmMestregrade.setOrigemMaterialmestregrade(true);
					ocmMestregrade.setOrdemcompra(ocm.getOrdemcompra());
					ocmMestregrade.setWhereInOrdemcompramaterialItemGrade(ocm.getCdordemcompramaterial().toString());
					ocmMestregrade.setWhereInMaterialItenGrade(ocm.getMaterial().getCdmaterial().toString());
					ocmMestregrade.setNomematerialOrdemcompramaterial(
							ocm.getMaterial().getMaterialmestregrade().getCdmaterial() + " - " + 
							ocm.getMaterial().getMaterialmestregrade().getNome());
					
					List<Ordemcompramaterial> listaOCM = new ArrayList<Ordemcompramaterial>();
					listaOCM.add(ocm);
					ocmMestregrade.setListaOrdemcompramaterialByMaterialmestregrade(listaOCM);
					
					listaMetreGrade.add(ocmMestregrade);
				}
			}
		}
		
		listaNova.addAll(listaMetreGrade);
		return listaNova;
	}
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Jo�o Vitor
	 * @since 14/10/2014
	 */
	public void vinculaPessoaQuestionarioAOrdemCompra(Ordemcompra ordemcompra){
		ordemcompraDAO.vinculaPessoaQuestionarioAOrdemCompra(ordemcompra);
	}
	
	/**
	 * M�todo que adiciona o material a lista de ordem de compra
	 *
	 * @param listaOrdemcompramaterial
	 * @param ordemcompramaterial
	 * @author Lucas Costa
	 */
	public void addMateriaisAgendaproducao(List<Ordemcompramaterial> listaOrdemcompramaterial, Ordemcompramaterial ordemcompramaterial) {
		if(ordemcompramaterial.getQtdpedida() > 0){
			boolean adicionar = true;
			if(listaOrdemcompramaterial.size() > 0){
				for(Ordemcompramaterial om : listaOrdemcompramaterial){
					if(om.getMaterial().equals(ordemcompramaterial.getMaterial())){
						adicionar = false;
						om.setQtdpedida(om.getQtdpedida() + ordemcompramaterial.getQtdpedida());
						break;
					}
				}
			}
			if(adicionar)
				listaOrdemcompramaterial.add(ordemcompramaterial);
		}
	}
	
	public void gerarFluxoCaixaOrdens(List<Ordemcompra> listaOrdemcompra) {
		if(SinedUtil.isListNotEmpty(listaOrdemcompra)){
			final List<Ordemcompra> ordens = findOrdensParaAutorizar(CollectionsUtil.listAndConcatenate(listaOrdemcompra, "cdordemcompra", ","));
			if(SinedUtil.isListNotEmpty(ordens)){
				getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
					public Object doInTransaction(TransactionStatus status) {
						String msg = "/Erro:";
						FluxocaixaTipo fluxocaixaTipo = fluxocaixaTipoService.getFluxoCaixaTipoOrdemCompra();
						if(fluxocaixaTipo == null){
							fluxocaixaTipo = new FluxocaixaTipo("Ordem de compra", "OC");
							fluxocaixaTipoService.saveOrUpdateNoUseTransaction(fluxocaixaTipo);
						}
						
						for (Ordemcompra ordemcompra : ordens) {
							if(fluxocaixaService.existeFluxocaixa(ordemcompra))
								continue;
							
							List<Fluxocaixa> fluxos = fluxocaixaService.criaFluxoCaixasOrdemCompra(ordemcompra, fluxocaixaTipo);
							
							for (Fluxocaixa fluxocaixa : fluxos) 
								fluxocaixaService.saveOrUpdateNoUseTransaction(fluxocaixa);
							
						}				
						return msg;
					}
				});
			}
		}
	}
	
	public List<Ordemcompra> buscarOrdemcompraCriarFluxocaixa(Date dtInicio, Date dtFim, String whereIn) {
		return ordemcompraDAO.buscarOrdemcompraCriarFluxocaixa(dtInicio, dtFim, whereIn);
	}
	
	public Ordemcompra findOrdemcompraAvaliacao(Ordemcompra ordemcompra){
		return ordemcompraDAO.findOrdemcompraAvaliacao(ordemcompra);
	}
	
	public Ordemcompra findFornecedorForOrdemCompra(Ordemcompra ordemcompra){
		return ordemcompraDAO.findFornecedorForOrdemCompra(ordemcompra);
	}
	
	public void deleteVinculoPessoaQuestionarioAOrdemCompra(Ordemcompra ordemcompra){
		ordemcompraDAO.deleteVinculoPessoaQuestionarioAOrdemCompra(ordemcompra);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForBaixar(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findForBaixar(String whereIn){
		return ordemcompraDAO.findForBaixar(whereIn);
	}
	
	/**
	* M�todo que verifica se existe itens restantes na ordem de compra
	*
	* @param whereIn
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public boolean existeItensRestantes(String whereIn) {
		if(StringUtils.isNotEmpty(whereIn)){
			List<Ordemcompra> lista = findForBaixar(whereIn);
			if(SinedUtil.isListNotEmpty(lista)){
				for(Ordemcompra oc : lista){
					if(SinedUtil.isListNotEmpty(oc.getListaMaterial())){
						for(Ordemcompramaterial ocm : oc.getListaMaterial()){
							if(ocm.getQtdpedida() != null && ocm.getMaterial() != null && ocm.getMaterial().getUnidademedida() != null){
								Double qtde = unidademedidaService.converteQtdeUnidademedida(ocm.getMaterial().getUnidademedida(), 
										ocm.getQtdpedida(), ocm.getUnidademedida(), ocm.getMaterial(), null, null);
								if(qtde == null || qtde == 0d){
									qtde = ocm.getQtdpedida();
								}
								if(qtde != null && qtde > 0){
									Double qtdeTotalEntrega = 0d;
									if(SinedUtil.isListNotEmpty(ocm.getListaOrdemcompraentregamaterial())){
										for(Ordemcompraentregamaterial ocem : ocm.getListaOrdemcompraentregamaterial()){
											if(ocem.getQtde() != null && ocem.getEntregamaterial() != null && ocem.getEntregamaterial().getUnidademedidacomercial() != null &&
													ocem.getEntregamaterial().getEntregadocumento() != null && 
													ocem.getEntregamaterial().getEntregadocumento().getEntrega() != null && 
													ocem.getEntregamaterial().getEntregadocumento().getEntrega().getAux_entrega() != null &&
													!Situacaosuprimentos.CANCELADA.equals(ocem.getEntregamaterial().getEntregadocumento().getEntrega().getAux_entrega().getSituacaosuprimentos())){
												Double qtdeEntrega = unidademedidaService.converteQtdeUnidademedida(ocm.getMaterial().getUnidademedida(), 
														ocem.getQtde(), ocem.getEntregamaterial().getUnidademedidacomercial(), ocm.getMaterial(), null, null);
												if(qtdeEntrega == null || qtdeEntrega == 0d){
													qtdeEntrega = ocem.getQtde();
												}
												if(qtdeEntrega != null){
													qtdeTotalEntrega += qtdeEntrega;
												}
											}
										}
									}
									if(qtde > qtdeTotalEntrega){
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#existeSituacaoDiferente(String whereIn, Situacaosuprimentos situacaosuprimentos)
	*
	* @param whereIn
	* @param situacaosuprimentos
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public Boolean existeSituacaoDiferente(String whereIn, Situacaosuprimentos situacaosuprimentos) {
		return ordemcompraDAO.existeSituacaoDiferente(whereIn, situacaosuprimentos);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findForSolicitarrestante(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findForSolicitarrestante(String whereIn) {
		return ordemcompraDAO.findForSolicitarrestante(whereIn);
	}
	
	/**
	* M�todo que criar o bean SolicitarrestanteBean com os materiais com quantidade restante
	*
	* @param listaOrdemcompra
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public SolicitarrestanteBean criaSolicitarrestanteBean(List<Ordemcompra> listaOrdemcompra) {
		SolicitarrestanteBean bean = new SolicitarrestanteBean();
		bean.setListaSolicitarrestanteItemBean(new ArrayList<SolicitarrestanteItemBean>());
		if(SinedUtil.isListNotEmpty(listaOrdemcompra)){
			boolean isFornecedorDiferente = false;
			boolean isContatoDiferente = false;
			boolean isLocalarmazenagemDiferente = false;
			boolean isEmpresaDiferente = false;
			boolean isDocumentotipoDiferente = false;
			boolean isPrazopagamentoDiferente = false;
			boolean isTransportadorDiferente = false;
			boolean isGarantiaDiferente = false;
			boolean isTipofreteDiferente = false;
			
			for(Ordemcompra oc : listaOrdemcompra){
				if(SinedUtil.isListNotEmpty(oc.getListaMaterial())){
					int contIdentificador = 0;
					for(Ordemcompramaterial ocm : oc.getListaMaterial()){
						Double qtdeEntregue = entregamaterialService.getQtdEntregueDoMaterialNaOrdemCompra(ocm);
						if(ocm.getUnidademedida() != null && ocm.getMaterial() != null && 
								ocm.getMaterial().getUnidademedida() != null && 
								!ocm.getMaterial().getUnidademedida().equals(ocm.getUnidademedida())){
							Double fracao = unidademedidaService.getFatorconversao(ocm.getMaterial().getUnidademedida(), qtdeEntregue, ocm.getUnidademedida(), ocm.getMaterial(), null, 1d);
							if(fracao != null){
								qtdeEntregue = qtdeEntregue * fracao;
							}
						}
						ocm.setQtdrestante(SinedUtil.roundByParametro((ocm.getQtdpedida() * ocm.getQtdefrequencia()) - (qtdeEntregue != null ? qtdeEntregue : 0.0)));
						
						if(ocm.getQtdrestante() != null && ocm.getQtdrestante() > 0){
							SolicitarrestanteItemBean itemBean = new SolicitarrestanteItemBean();
							itemBean.setChecado(true);
							itemBean.setMaterial(ocm.getMaterial());
							itemBean.setLocalarmazenagem(ocm.getLocalarmazenagem());
							
							itemBean.setObservacao(ocm.getObservacao());
							itemBean.setProjeto(ocm.getProjeto());
							
							Materialclasse materialclasse = Materialclasse.PRODUTO;
							if(ocm.getMaterial().getProduto() != null && ocm.getMaterial().getProduto()){
								materialclasse = Materialclasse.PRODUTO;	
							}else if(ocm.getMaterial().getServico() != null && ocm.getMaterial().getServico()){
								materialclasse = Materialclasse.SERVICO;	
							}else if(ocm.getMaterial().getPatrimonio() != null && ocm.getMaterial().getPatrimonio()){
								materialclasse = Materialclasse.PATRIMONIO;	
							}else {
								materialclasse = Materialclasse.EPI;	
							}
							itemBean.setMaterialclasse(materialclasse);
							
							if(ocm.getUnidademedida() != null){
								itemBean.setUnidademedida(ocm.getUnidademedida());
							}else {
								itemBean.setUnidademedida(ocm.getMaterial().getUnidademedida());
							}
							itemBean.setCentrocusto(ocm.getCentrocusto());
							itemBean.setContagerencial(ocm.getContagerencial());
							itemBean.setQuantidade(ocm.getQtdrestante());
							
						
							
							Double qtdepedida = ocm.getQtdpedida() * ocm.getQtdefrequencia();
							Double valorunitario = ocm.getValor() / qtdepedida;
							itemBean.setValorunitario(SinedUtil.round(valorunitario, 10));
							itemBean.setValortotal(SinedUtil.roundByParametro(itemBean.getQuantidade()*itemBean.getValorunitario()));
							
							itemBean.setWhereInOrdemcompra(oc.getCdordemcompra().toString());
							
							List<Solicitacaocompra> listaSolicitacaocompra = new ArrayList<Solicitacaocompra>();
							if(oc.getCotacao() != null){
								if(SinedUtil.isListNotEmpty(oc.getCotacao().getListaCotacaofornecedor())){
									for(Cotacaofornecedor cf : oc.getCotacao().getListaCotacaofornecedor()){
										if(cf.getFornecedor() != null && cf.getFornecedor().equals(oc.getFornecedor())){
											if(SinedUtil.isListNotEmpty(cf.getListaCotacaofornecedoritem())){
												for(Cotacaofornecedoritem cfi : cf.getListaCotacaofornecedoritem()){
													if(cfi.getMaterial() != null && cfi.getMaterial().equals(ocm.getMaterial()) &&
															SinedUtil.isListNotEmpty(cfi.getListaCotacaofornecedoritemsolicitacaocompra())){
														for(Cotacaofornecedoritemsolicitacaocompra cfisc : cfi.getListaCotacaofornecedoritemsolicitacaocompra()){
															if(cfisc.getSolicitacaocompra() != null && 
																	!listaSolicitacaocompra.contains(cfisc.getSolicitacaocompra())){
																listaSolicitacaocompra.add(cfisc.getSolicitacaocompra());
															}
														}
													}
												}
											}
											break;
										}
									}
								}
							}else if(SinedUtil.isListNotEmpty(oc.getListaOrdemcompraorigem())){
								for(Ordemcompraorigem oco : oc.getListaOrdemcompraorigem()){
									if(oco.getSolicitacaocompra() != null && oco.getSolicitacaocompra().getMaterial() != null && 
											oco.getSolicitacaocompra().getMaterial().equals(ocm.getMaterial()) &&
											!listaSolicitacaocompra.contains(oco.getSolicitacaocompra())){
										listaSolicitacaocompra.add(oco.getSolicitacaocompra());
									}
								}
							}
							
							itemBean.setListaSolicitacaocompra(listaSolicitacaocompra);
							itemBean.setIdentificador(System.currentTimeMillis() + contIdentificador);
							contIdentificador++;
							bean.getListaSolicitarrestanteItemBean().add(itemBean);
							
							if(bean.getFornecedor() != null && oc.getFornecedor() != null && 
									!bean.getFornecedor().equals(oc.getFornecedor())){
								isFornecedorDiferente = true;
							}else {
								bean.setFornecedor(oc.getFornecedor());
							}
							if(bean.getContato() != null && oc.getContato() != null && 
									!bean.getContato().equals(oc.getContato())){
								isContatoDiferente = true;
							}else {
								bean.setContato(oc.getContato());
							}
							if(bean.getLocalarmazenagem() != null && oc.getLocalarmazenagem() != null && 
									!bean.getLocalarmazenagem().equals(oc.getLocalarmazenagem())){
								isLocalarmazenagemDiferente = true;
							}else {
								bean.setLocalarmazenagem(oc.getLocalarmazenagem());
							}
							if(bean.getEmpresa() != null && oc.getEmpresa() != null && 
									!bean.getEmpresa().equals(oc.getEmpresa())){
								isEmpresaDiferente = true;
							}else {
								bean.setEmpresa(oc.getEmpresa());
							}
							if(bean.getDocumentotipo() != null && oc.getDocumentotipo() != null && 
									!bean.getDocumentotipo().equals(oc.getDocumentotipo())){
								isDocumentotipoDiferente = true;
							}else {
								bean.setDocumentotipo(oc.getDocumentotipo());
							}
							if(bean.getTransportador() != null && oc.getTransportador() != null && 
									!bean.getTransportador().equals(oc.getTransportador())){
								isTransportadorDiferente = true;
							}else {
								bean.setTransportador(oc.getTransportador());
							}
							if(bean.getGarantia() != null && oc.getGarantia() != null && 
									!bean.getGarantia().equals(oc.getGarantia())){
								isGarantiaDiferente = true;
							}else {
								bean.setGarantia(oc.getGarantia());
							}
							if(bean.getPrazopagamento() != null && oc.getPrazopagamento() != null && 
									!bean.getPrazopagamento().equals(oc.getPrazopagamento())){
								isPrazopagamentoDiferente = true;
							}else {
								bean.setPrazopagamento(oc.getPrazopagamento());
							}
							if(bean.getTipofrete() != null && oc.getTipofrete() != null && 
									!bean.getTipofrete().equals(oc.getTipofrete())){
								isTipofreteDiferente = true;
							}else {
								bean.setTipofrete(oc.getTipofrete());
							}
							bean.setDtcriacao(oc.getDtcriacao());
						}
					}
				}
			}
			
			if(isFornecedorDiferente){bean.setFornecedor(null);}
			if(isContatoDiferente){bean.setContato(null);}
			if(isLocalarmazenagemDiferente){bean.setLocalarmazenagem(null);}
			if(isEmpresaDiferente){bean.setEmpresa(null);}
			if(isDocumentotipoDiferente){bean.setDocumentotipo(null);}
			if(isPrazopagamentoDiferente){bean.setPrazopagamento(null);}
			if(isTransportadorDiferente){bean.setTransportador(null);}
			if(isGarantiaDiferente){bean.setGarantia(null);}
			if(isTipofreteDiferente){bean.setTipofrete(null);}
		}
		
		return bean;
	}
	
	/**
	* M�todo que salva as solicita��es criadas na solicita��o de quantidade restante
	*
	* @param listaSC
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public String salvaSolicitacoes(final List<Solicitacaocompra> listaSC) {
		List<Papel> listaPapeis = papelService.carregaPapeisPermissaoAcao("AUTORIZAR_SOLICITACAOCOMPRA");
		solicitacaocompraService.salvaSolicitacaoEmMassa(listaSC, listaPapeis, false);
		return SinedUtil.listAndConcatenate(listaSC, "cdsolicitacaocompra", ",");
	}
	
	/**
	* M�todo que cria solicita��es de compra a partir do bean solicitarrestanteBean
	*
	* @param solicitarrestanteBean
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> criaSolicitacaocompraBySolicitarrestanteBean(SolicitarrestanteBean solicitarrestanteBean) {
		List<Solicitacaocompra> listaSolicitacaocompra = new ArrayList<Solicitacaocompra>();
		Timestamp dtAtual = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat format = new SimpleDateFormat("yy");
		java.util.Date data = new java.util.Date();
		
		for(SolicitarrestanteItemBean itemBean : solicitarrestanteBean.getListaSolicitarrestanteItemBean()){
			if(itemBean.getChecado() != null && itemBean.getChecado()){
				Solicitacaocompra scNova = new Solicitacaocompra();
				scNova.setDtlimite(solicitarrestanteBean.getDtnecessidade());
				scNova.setMaterial(itemBean.getMaterial());
				scNova.setMaterialclasse(itemBean.getMaterialclasse());
				scNova.setUnidademedida(itemBean.getUnidademedida());
				Double qtde = itemBean.getQuantidade();
				if(qtde != null){
					if(itemBean.getUnidademedida() != null && itemBean.getMaterial() != null && 
							itemBean.getMaterial().getUnidademedida() != null && 
							!itemBean.getMaterial().getUnidademedida().equals(itemBean.getUnidademedida())){
						Double fracao = unidademedidaService.getFatorconversao(itemBean.getUnidademedida(), qtde, itemBean.getMaterial().getUnidademedida(), itemBean.getMaterial(), null, 1d);
						if(fracao != null){
							qtde = qtde * fracao;
						}
					}
				}
				scNova.setQtde(itemBean.getQuantidade());
				scNova.setQtdefrequencia(1d);
				scNova.setFrequencia(new Frequencia(Frequencia.UNICA));
				scNova.setFaturamentocliente(false);
				scNova.setCentrocusto(itemBean.getCentrocusto());
				scNova.setContagerencial(itemBean.getContagerencial());
				scNova.setDepartamento(solicitarrestanteBean.getDepartamento());
				scNova.setEmpresa(solicitarrestanteBean.getEmpresa());
				scNova.setColaborador(solicitarrestanteBean.getColaborador());
				scNova.setLocalarmazenagem(solicitarrestanteBean.getLocalarmazenagem());
				
				Solicitacaocompraorigem sco = new Solicitacaocompraorigem();
				scNova.setSolicitacaocompraorigem(sco);
				
				Solicitacaocompra scBanco = itemBean.getSolicitacaocompra() != null ? solicitacaocompraService.loadForEntrada(itemBean.getSolicitacaocompra()) : null;
				Requisicaomaterial requisicaomaterial = null;
				if(scBanco != null){
					scNova.setIdentificador(scBanco.getIdentificador());
					scNova.setDtsolicitacao(new java.sql.Date(dtAtual.getTime()));
					scNova.setEmpresa(scBanco.getEmpresa());
					scNova.setProjeto(scBanco.getProjeto());
					scNova.setDepartamento(scBanco.getDepartamento());
					scNova.setColaborador(scBanco.getColaborador());
					scNova.setLocalarmazenagem(scBanco.getLocalarmazenagem());
					scNova.setCentrocusto(scBanco.getCentrocusto());
					scNova.setContagerencial(scBanco.getContagerencial());
					
					if(scBanco.getSolicitacaocompraorigem() != null && scBanco.getSolicitacaocompraorigem().getRequisicaomaterial() != null){
						requisicaomaterial = scBanco.getSolicitacaocompraorigem().getRequisicaomaterial();
						sco.setRequisicaomaterial(scBanco.getSolicitacaocompraorigem().getRequisicaomaterial());
					}
					sco.setSolicitacaocompra(scBanco);
					
				}else {
					Integer identificador = solicitacaocompraService.getUltimoIdentificador();
					scNova.setIdentificador(new Integer(identificador != null ? (identificador + 1) : 0));
				}
				if(scNova.getDescricao() == null || scNova.getDescricao().trim().equals("")){
					scNova.setDescricao(scNova.getIdentificador()+"/"+format.format(data));
				}
				
				Solicitacaocomprahistorico sch = new Solicitacaocomprahistorico();
				sch.setSolicitacaocompra(scNova);
				sch.setSolicitacaocompraacao(Solicitacaocompraacao.CRIADA);
				sch.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				sch.setDtaltera(dtAtual);
				sch.setObservacao("Criado a partir da a��o Solicitar restante da Ordem de compra " +
						SinedUtil.makeLinkHistorico(itemBean.getWhereInOrdemcompra(), "visualizarOrdemcompra") + 
						(requisicaomaterial != null ? (" e vinculado a requisi��o " +  SinedUtil.makeLinkHistorico(requisicaomaterial.getCdrequisicaomaterial().toString(), requisicaomaterial.getIdentificadorOuCdrequisicaomaterial(), "visualizarRequisicaomaterial")) : ""));
				scNova.setSolicitacaocomprahistorico(sch);
				
				listaSolicitacaocompra.add(scNova);
			}
		}
		
		return listaSolicitacaocompra;
	}
	
	/**
	* M�todo que cria hist�rico e efetua a baixa das ordens de compra
	*
	* @param solicitarrestanteBean
	* @param whereInSolicitacao
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public void baixarEcriarHistoricoSolicitarrestante(SolicitarrestanteBean solicitarrestanteBean,	String whereInSolicitacao) {
		List<String> listaIdOrdemcompra = new ArrayList<String>();
		for(SolicitarrestanteItemBean itemBean : solicitarrestanteBean.getListaSolicitarrestanteItemBean()){
			if(itemBean.getChecado() != null && itemBean.getChecado() && StringUtils.isNotEmpty(itemBean.getWhereInOrdemcompra())){
				for(String idOC : itemBean.getWhereInOrdemcompra().split(",")){
					if(!listaIdOrdemcompra.contains(idOC)){
						listaIdOrdemcompra.add(idOC);
					}
				}
				
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaIdOrdemcompra)){
			final String whereInOC = CollectionsUtil.concatenate(listaIdOrdemcompra, ",");
			final String links = solicitacaocompraService.makeLinkHistoricoSolicitacaocompra(solicitacaocompraService.findWithIdentificador(whereInSolicitacao));
			final Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
			final Integer cdusuario = SinedUtil.getUsuarioLogado().getCdpessoa();
			
			final List<String> listaFinalIdOrdemcompra = listaIdOrdemcompra;
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					for(String id : listaFinalIdOrdemcompra){
						
						Ordemcomprahistorico och = new Ordemcomprahistorico();
						och.setOrdemcompra(new Ordemcompra(Integer.parseInt(id)));
						och.setDtaltera(dataAtual);
						och.setCdusuarioaltera(cdusuario);
						och.setOrdemcompraacao(Ordemcompraacao.BAIXADA);
						och.setObservacao("Baixado a partir do processo Solicitar Restante que deu origem a(s) solicita��o(�es): " + links);
						ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(och);
					}
					
					ordemcompraDAO.doUpdateSituacaoOrdens(whereInOC, Situacaosuprimentos.BAIXADA);
							
					return status;
				}
			});
			
			this.callProcedureAtualizaOrdemcompra(whereInOC);
		}
	}
	
	/**
	* M�todo que cria a ordem de compra de acordo com os itens restantes
	*
	* @param solicitarrestanteBean
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public Ordemcompra criaOrdemcompraBySolicitarrestanteBean(SolicitarrestanteBean solicitarrestanteBean) {
		Ordemcompra form = new Ordemcompra();
	
		String sugerir = parametrogeralService.buscaValorPorNome(Parametrogeral.SUGERIR_OBSERVACAO_E_PROJETO_SOLICITARRESTANTE);
		List<Ordemcompra> listaOrigem = new ArrayList<Ordemcompra>();
		if("TRUE".equalsIgnoreCase(sugerir) && solicitarrestanteBean.getPedidoorigem() != null){
			listaOrigem = ordemcompraDAO.findOrigem(solicitarrestanteBean.getPedidoorigem());
			if(listaOrigem.size() == 1){
				form.setObservacoesinternas(listaOrigem.get(0).getObservacoesinternas());
				form.setObservacao(listaOrigem.get(0).getObservacao());
			}else if (!listaOrigem.isEmpty() && listaOrigem!=null){
				for (Ordemcompra ordemcompra : listaOrigem) {
					if(listaOrigem.get(0).getObservacoesinternas().equals(ordemcompra.getObservacoesinternas())
							&& listaOrigem.get(0).getObservacao().equals(ordemcompra.getObservacao())){
						form.setObservacoesinternas(listaOrigem.get(0).getObservacoesinternas());
						form.setObservacao(listaOrigem.get(0).getObservacao());
					}else{
						form.setObservacoesinternas("");
						form.setObservacao("");
						break;
					}
				}
			}		
		}
		
		List<Ordemcompramaterial> listaOrdemcompramaterial = new ArrayList<Ordemcompramaterial>();
		List<String> listaIdOrdemcompra = new ArrayList<String>();
		for(SolicitarrestanteItemBean itemBean : solicitarrestanteBean.getListaSolicitarrestanteItemBean()){
			if(itemBean.getChecado() != null && itemBean.getChecado()){
				Ordemcompramaterial ocmNovo = new Ordemcompramaterial();
				ocmNovo.setMaterial(itemBean.getMaterial());
				ocmNovo.setUnidademedida(itemBean.getUnidademedida());
				ocmNovo.setLocalarmazenagem(itemBean.getLocalarmazenagem());
				ocmNovo.setContagerencial(itemBean.getContagerencial());
				ocmNovo.setCentrocusto(itemBean.getCentrocusto());
				ocmNovo.setQtdefrequencia(1d);
				ocmNovo.setQtdpedida(itemBean.getQuantidade());
				ocmNovo.setValor(itemBean.getValortotal());
				ocmNovo.setFrequencia(new Frequencia(Frequencia.UNICA));
				ocmNovo.setIcmsissincluso(false);
				ocmNovo.setIpiincluso(false);
				if("TRUE".equalsIgnoreCase(sugerir) && solicitarrestanteBean.getPedidoorigem() != null){
					ocmNovo.setProjeto(itemBean.getProjeto());
					ocmNovo.setObservacao(itemBean.getObservacao());
				}	
				if(ocmNovo.getQtdpedida() != null && ocmNovo.getValor() != null){
					ocmNovo.setValorunitario(ocmNovo.getValor() / ocmNovo.getQtdpedida());
				}
				listaOrdemcompramaterial.add(ocmNovo);
				
				if(StringUtils.isNotEmpty(itemBean.getWhereInOrdemcompra())){
					for(String idOC : itemBean.getWhereInOrdemcompra().split(",")){
						if(!listaIdOrdemcompra.contains(idOC)){
							listaIdOrdemcompra.add(idOC);
						}
					}
				}
			}
		}
		
		form.setFornecedor(solicitarrestanteBean.getFornecedor());
		form.setContato(solicitarrestanteBean.getContato());
		form.setLocalarmazenagem(solicitarrestanteBean.getLocalarmazenagem());
		form.setEmpresa(solicitarrestanteBean.getEmpresa());
		form.setDocumentotipo(solicitarrestanteBean.getDocumentotipo());
		form.setTransportador(solicitarrestanteBean.getTransportador());
		form.setGarantia(solicitarrestanteBean.getGarantia());
		form.setPrazopagamento(solicitarrestanteBean.getPrazopagamento());
		form.setDtcriacao(solicitarrestanteBean.getDtcriacao());
		form.setTipofrete(solicitarrestanteBean.getTipofrete());
		form.setListaMaterial(listaOrdemcompramaterial);
		form.setWhereInOCSolicitarrestante(CollectionsUtil.concatenate(listaIdOrdemcompra, ","));
		
		calculaRateio(form);
		return form;
	}
	
	/**
	* M�todo que faz a baixa da ordem de compra e registra o hist�rico
	*
	* @param bean
	* @param whereInOCSolicitarrestante
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public void baixarEcriarHistoricoSolicitarrestante(final Ordemcompra bean, final String whereInOCSolicitarrestante) {
		if(StringUtils.isNotEmpty(whereInOCSolicitarrestante)){
			final String links = SinedUtil.makeLinkHistorico(whereInOCSolicitarrestante, "visualizarOrdem");
			final Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
			final Integer cdusuario = SinedUtil.getUsuarioLogado().getCdpessoa();
			
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					List<Ordemcompra> listaOrdemcompra = findWithOrigem(whereInOCSolicitarrestante);
					StringBuilder historicoCotacao = new StringBuilder();
					StringBuilder historicoSolicitacaocompra = new StringBuilder();
					
					if(SinedUtil.isListNotEmpty(listaOrdemcompra)){
						for(Ordemcompra ordemcompra : listaOrdemcompra){
							if(ordemcompra.getCotacao() != null){
								updateVinculoCotacao(bean, ordemcompra.getCotacao());
								if(historicoCotacao.length() == 0 ){
									historicoCotacao.append("<br>Vinculado a cota��o ");
								}else {
									historicoCotacao.append(",");
								}
								historicoCotacao.append(SinedUtil.makeLinkHistorico(ordemcompra.getCotacao().getCdcotacao().toString(), "visualizarCotacao"));
								
							}else if(SinedUtil.isListNotEmpty(ordemcompra.getListaOrdemcompraorigem())){
								List<Solicitacaocompra> listaSolicitacaocompra = new ArrayList<Solicitacaocompra>();
								for(Ordemcompraorigem ordemcompraorigem : ordemcompra.getListaOrdemcompraorigem()){
									ordemcompraorigem.setCdordemcompraorigem(null);
									ordemcompraorigem.setOrdemcompra(bean);
									ordemcompraorigemService.saveOrUpdateNoUseTransaction(ordemcompraorigem);
									
									if(ordemcompraorigem.getSolicitacaocompra() != null && 
											!listaSolicitacaocompra.contains(ordemcompraorigem.getSolicitacaocompra())){
										listaSolicitacaocompra.add(ordemcompraorigem.getSolicitacaocompra());
									}
								}
								
								String linkSC = null;
								if(SinedUtil.isListNotEmpty(listaSolicitacaocompra)){
									linkSC = solicitacaocompraService.makeLinkHistoricoSolicitacaocompra(listaSolicitacaocompra);
								}
								if(historicoSolicitacaocompra.length() == 0 ){
									historicoSolicitacaocompra.append("<br>Vinculado a(s) solicita��o(�es) ");
								}else {
									historicoSolicitacaocompra.append(",");
								}
								historicoSolicitacaocompra.append(linkSC);
								
							}
						}
						
						Ordemcomprahistorico och = new Ordemcomprahistorico();
						och.setOrdemcompra(bean);
						och.setDtaltera(dataAtual);
						och.setCdusuarioaltera(cdusuario);
						och.setOrdemcompraacao(Ordemcompraacao.CRIADA);
						och.setObservacao("Criado a partir da ordem de compra " + links + " atrav�s " +
								"da a��o Solicitar Restante. " + historicoCotacao.toString() + historicoSolicitacaocompra.toString());
						ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(och);
					}
					
					for(String id : whereInOCSolicitarrestante.split(",")){
						Ordemcomprahistorico och = new Ordemcomprahistorico();
						och.setOrdemcompra(new Ordemcompra(Integer.parseInt(id)));
						och.setDtaltera(dataAtual);
						och.setCdusuarioaltera(cdusuario);
						och.setOrdemcompraacao(Ordemcompraacao.BAIXADA);
						och.setObservacao("Baixado a partir do processo Solicitar Restante que deu origem a ordem de compra " + SinedUtil.makeLinkHistorico(bean.getCdordemcompra().toString(), "visualizarOrdem"));
						ordemcomprahistoricoService.saveOrUpdateNoUseTransaction(och);
					}

					ordemcompraDAO.doUpdateSituacaoOrdens(whereInOCSolicitarrestante, Situacaosuprimentos.BAIXADA);
					
					return status;
				}
			});
			
			this.callProcedureAtualizaOrdemcompra(whereInOCSolicitarrestante);
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#updateVinculoCotacao(Ordemcompra ordemcompra, Cotacao cotacao)
	*
	* @param ordemcompra
	* @param cotacao
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public void updateVinculoCotacao(Ordemcompra ordemcompra, Cotacao cotacao) {
		ordemcompraDAO.updateVinculoCotacao(ordemcompra, cotacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#findOrdemcompraEmAberto(String ids, Material material)
	*
	* @param ids
	* @return
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findOrdemcompraEmAberto(String ids, Material material) {
		return ordemcompraDAO.findOrdemcompraEmAberto(ids, material);
	}
	
	/**
	* M�todo que retorna as listas de ordemcompramaterial das ordens de compra 
	*
	* @param listaOrdemcompra
	* @return
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompramaterial> getListaOrdemcompramaterial(List<Ordemcompra> listaOrdemcompra) {
		List<Ordemcompramaterial> listaOCM = new ArrayList<Ordemcompramaterial>();
		if(SinedUtil.isListNotEmpty(listaOrdemcompra)){
			for(Ordemcompra ordemcompra : listaOrdemcompra){
				if(SinedUtil.isListNotEmpty(ordemcompra.getListaMaterial())){
					listaOCM.addAll(ordemcompra.getListaMaterial());
				}
			}
		}
		return listaOCM;
	}
	
	public void validaQtdeCompraMaiorQuePlanejamento(Ordemcompra bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaMaterial())){
			validaQtdeCompraMaiorQuePlanejamento(bean.getListaMaterial(), errors, bean.getWhereInSolicitacaocompra());
		}
	}
	
	/**
	* M�todo que valida se a quantidade solicitada � maior que a quantidade do planejamento
	*
	* @param bean
	* @param errors
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public void validaQtdeCompraMaiorQuePlanejamento(List<Ordemcompramaterial> lista, BindException errors, String whereInSolicitacaocompra) {
		if(SinedUtil.isListNotEmpty(lista) && "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARCOMPRAPROJETO))){
	 		String whereInMaterial = CollectionsUtil.listAndConcatenate(lista, "material.cdmaterial", ",");
	 		StringBuilder whereInOrdemcompramaterial = new StringBuilder();
	 		StringBuilder whereInProjeto = new StringBuilder();
	 		String whereIn = null;
	 		
	 		List<Ordemcompramaterial> listaOrdemcompramaterial = new ArrayList<Ordemcompramaterial>();
	 		for(Ordemcompramaterial ordemcompramaterial : lista){
	 			if(ordemcompramaterial.getCdordemcompramaterial() != null) whereInOrdemcompramaterial.append(ordemcompramaterial.getCdordemcompramaterial()).append(",");
	 			if(ordemcompramaterial.getProjeto() != null){ 
	 				whereInProjeto.append(ordemcompramaterial.getProjeto().getCdprojeto()).append(",");
	 				listaOrdemcompramaterial.add(ordemcompramaterial);
	 			}
	 		}
	 		
	 		if(StringUtils.isNotBlank(whereInProjeto.toString()) && SinedUtil.isListNotEmpty(listaOrdemcompramaterial)){
		 		List<Planejamentorecursogeral> listaPlanejamentorecursogeral = planejamentorecursogeralService.findForBloquearQtdeCompra(whereInProjeto.substring(0, whereInProjeto.length()-1), whereInMaterial);
		 		if(listaPlanejamentorecursogeral == null || listaPlanejamentorecursogeral.size() == 0){
		 			StringBuilder materiais = new StringBuilder();
		 			for(Ordemcompramaterial ordemcompramaterial : listaOrdemcompramaterial){
		 				if(ordemcompramaterial.getMaterial() != null && ordemcompramaterial.getMaterial().getNome() != null){
		 					materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(ordemcompramaterial.getMaterial().getNome());
		 				}
		 			}
		 			errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
		 		}else {
		 			List<Material> listaMaterialEncontrado = new ArrayList<Material>();
		 			StringBuilder materiais = new StringBuilder();
		 			for(Ordemcompramaterial ordemcompramaterial : listaOrdemcompramaterial){
		 				if(ordemcompramaterial.getMaterial() != null){
		 					boolean materialEncontrado = false;
		 					for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
		 						if(prg.getMaterial() != null && prg.getMaterial().equals(ordemcompramaterial.getMaterial())){
		 							materialEncontrado = true;
		 							if(!listaMaterialEncontrado.contains(ordemcompramaterial.getMaterial())){
		 								listaMaterialEncontrado.add(ordemcompramaterial.getMaterial());
		 							}
		 							break;
		 						}
		 					}
		 					if(!materialEncontrado){
		 						materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(ordemcompramaterial.getMaterial().getNome());
		 					}
		 				}
		 			}
		 			if(StringUtils.isNotBlank(materiais.toString())){
		 				errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
		 			}
		 			
	 				if(StringUtils.isNotBlank(whereInOrdemcompramaterial.toString())){
	 					whereIn = whereInOrdemcompramaterial.substring(0, whereInOrdemcompramaterial.length()-1);
	 				}
	 				
	 				List<Ordemcompramaterial> listaAgrupada = agruparItensValidacaoPlanejamento(listaOrdemcompramaterial);
	 				List<Solicitacaocompra> listaSC = solicitacaocompraService.findForBloquearQtdeCompra(whereInProjeto.substring(0, whereInProjeto.length()-1), whereInMaterial, whereInSolicitacaocompra, whereIn);
	 				
	 				StringBuilder menagem =  new StringBuilder();
	 				for(Ordemcompramaterial ordemcompramaterial : listaAgrupada){
	 		 			if(ordemcompramaterial.getQtdpedida() != null && ordemcompramaterial.getMaterial() != null && listaMaterialEncontrado.contains(ordemcompramaterial.getMaterial())){
	 		 				Double qtdecomprada = 0d;
	 		 				Double qtdePrevista = 0d;
	 		 				if(SinedUtil.isListNotEmpty(listaSC)){
		 		 				for(Solicitacaocompra sc : listaSC){
	 								if(sc.getQtde() != null && sc.getMaterial() != null && sc.getMaterial().equals(ordemcompramaterial.getMaterial()) && 
	 										sc.getProjeto() != null && sc.getProjeto().equals(ordemcompramaterial.getProjeto())){
	 									qtdecomprada = sc.getQtde();
	 									break;
	 								}
		 		 				}
	 						}
	 		 				
	 		 				for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
								if(prg.getQtde() != null && prg.getMaterial() != null && prg.getMaterial().equals(ordemcompramaterial.getMaterial()) && 
										prg.getPlanejamento() != null && prg.getPlanejamento().getProjeto() != null &&
										prg.getPlanejamento().getProjeto().equals(ordemcompramaterial.getProjeto())){
									qtdePrevista += prg.getQtde();
								}
	 		 				}
	 		 				
	 		 				Double qtdeAcumulada = qtdecomprada + ordemcompramaterial.getQtdpedida();
	 		 				if(qtdeAcumulada > qtdePrevista){
	 		 					menagem.append("<br>&nbsp;&nbsp;&nbsp;" + ordemcompramaterial.getMaterial().getNome() + " - Qtde Prevista: " + qtdePrevista + " - Qtde Acumulada: " + qtdeAcumulada);
	 		 				}
	 		 			}
	 		 		}
	 				if(StringUtils.isNotBlank(menagem.toString())){
	 					errors.reject("001", "A quantidade de material comprado ou solicitado extrapola a quantidade prevista no Planejamento." + menagem);
	 				}
	 			}
	 		}
 		}
	}
	
	/**
	* M�todo que agrupa os itens da ordem de compra de acordo com o material e projeto
	*
	* @param listaOrdemcompramaterial
	* @return
	* @since 13/05/2016
	* @author Luiz Fernando
	*/
	private List<Ordemcompramaterial> agruparItensValidacaoPlanejamento(List<Ordemcompramaterial> listaOrdemcompramaterial) {
		List<Ordemcompramaterial> listaAgrupada = new ArrayList<Ordemcompramaterial>();
		if(SinedUtil.isListNotEmpty(listaOrdemcompramaterial)){
			for(Ordemcompramaterial ocm : listaOrdemcompramaterial){
				if(ocm.getQtdpedida() != null && ocm.getMaterial() != null && ocm.getProjeto() != null &&
 		 					(ocm.getOrdemcompra() == null ||
 							 ocm.getOrdemcompra().getAux_ordemcompra() == null ||
 							 ocm.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos() == null ||
							 !Situacaosuprimentos.BAIXADA.equals(ocm.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos()))){
					boolean adicionar = true;
					for(Ordemcompramaterial itemAgrupado : listaAgrupada){
						if(ocm.getMaterial().equals(itemAgrupado.getMaterial()) && ocm.getProjeto().equals(itemAgrupado.getProjeto())){
							itemAgrupado.setQtdpedida(itemAgrupado.getQtdpedida() + ocm.getQtdpedida());
							adicionar = false;
						}
					}
					if(adicionar){
						Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial();
						ordemcompramaterial.setMaterial(ocm.getMaterial());
						ordemcompramaterial.setQtdpedida(ocm.getQtdpedida());
						ordemcompramaterial.setProjeto(ocm.getProjeto());
						listaAgrupada.add(ordemcompramaterial);
					}
				}
			}
		}
		return listaAgrupada;
	}
	
	/**
	* M�todo que carrega a lista de Solicitacaocompraordemcompramaterial referente a ordem de compra
	*
	* @param ordemcompra
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public void carregaListaSolicitacaocompraordemcompramaterial(Ordemcompra ordemcompra) {
		if(ordemcompra != null && SinedUtil.isListNotEmpty(ordemcompra.getListaMaterial())){
			String whereIn = CollectionsUtil.listAndConcatenate(ordemcompra.getListaMaterial(), "cdordemcompramaterial", ",");
			if(StringUtils.isNotBlank(whereIn)){
				List<Solicitacaocompraordemcompramaterial> lista = solicitacaocompraordemcompramaterialService.findByOrdemcompramaterial(whereIn);
				if(SinedUtil.isListNotEmpty(lista)){
					for(Solicitacaocompraordemcompramaterial scocm : lista){
						for(Ordemcompramaterial ocm : ordemcompra.getListaMaterial()){
							if(ocm.getCdordemcompramaterial() != null && scocm.getOrdemcompramaterial() != null && 
									ocm.getCdordemcompramaterial().equals(scocm.getOrdemcompramaterial().getCdordemcompramaterial())){
								if(ocm.getListaSolicitacaocompraordemcompramaterial() == null){
									ocm.setListaSolicitacaocompraordemcompramaterial(new ListSet<Solicitacaocompraordemcompramaterial>(Solicitacaocompraordemcompramaterial.class));
								}
								ocm.getListaSolicitacaocompraordemcompramaterial().add(scocm);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo que calcula a quantidade restante dos itens da ordem de compra
	*
	* @param ordemcompra
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public void calculaQtdeRestanteOrdemcompra(Ordemcompra ordemcompra) {
		if(SinedUtil.isListNotEmpty(ordemcompra.getListaMaterial())){
			for (Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()) {
				Double resto = 0d;
				
				if(SinedUtil.isListNotEmpty(ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial())){
					for (Solicitacaocompraordemcompramaterial solicitacaocompraordemcompramaterial : ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial()) {
						if(solicitacaocompraordemcompramaterial.getOrdemcompramaterial() != null && solicitacaocompraordemcompramaterial.getOrdemcompramaterial().getUnidademedida() == null){
							Ordemcompramaterial ocm = ordemcompramaterialService.loadWithUnidademedida(solicitacaocompraordemcompramaterial.getOrdemcompramaterial());
							if(ocm != null){
								solicitacaocompraordemcompramaterial.getOrdemcompramaterial().setUnidademedida(ocm.getUnidademedida());
							}
						}
						ordemcompramaterial.setSolicitacaocompra(solicitacaocompraordemcompramaterial.getSolicitacaocompra());
						Double restoIt = ordemcompramaterialService.getQtdeByOrdemcompramaterial(ordemcompramaterial);
						if(restoIt != null && ordemcompramaterial.getUnidademedida() != null && ordemcompramaterial.getMaterial() != null && 
								ordemcompramaterial.getMaterial().getUnidademedida() != null && 
								!ordemcompramaterial.getMaterial().getUnidademedida().equals(ordemcompramaterial.getUnidademedida())){
							Double fracao = unidademedidaService.getFatorconversao(ordemcompramaterial.getMaterial().getUnidademedida(), restoIt, ordemcompramaterial.getUnidademedida(), ordemcompramaterial.getMaterial(), null, 1d);
							if(fracao != null){
								restoIt = restoIt * fracao;
							}
						}
						if(restoIt != null && solicitacaocompraordemcompramaterial.getQtde() != null){
							restoIt = restoIt - solicitacaocompraordemcompramaterial.getQtde();
						}
						solicitacaocompraordemcompramaterial.setResto(restoIt);
						resto += restoIt;
					}
				}
				
				ordemcompramaterial.setResto(resto);
			}
		}	
	}
	
	public void criarAvisoOrdemCompraAtrasada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Ordemcompra> ordemCompraList = ordemcompraDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		for (Ordemcompra o : ordemCompraList) {
			Aviso aviso = new Aviso("Ordem de compra com atraso na entrega", "C�digo da ordem de compra: " + o.getCdordemcompra(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), o.getCdordemcompra(), o.getEmpresa() != null ? o.getEmpresa() : empresaPrincipal, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, o.getCdordemcompra());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, o.getCdordemcompra(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public void criarAvisoEnvioOrdemCompra(Motivoaviso m, Date data, Date dateToSearch) {
		List<Ordemcompra> ordemCompraList = ordemcompraDAO.findByEnviarPedidoCompra(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		for (Ordemcompra o : ordemCompraList) {
			Aviso aviso = new Aviso("Ordem de compra com atraso na entrega", "C�digo da ordem de compra: " + o.getCdordemcompra(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), o.getCdordemcompra(), o.getEmpresa() != null ? o.getEmpresa() : empresaPrincipal, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, o.getCdordemcompra());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, o.getCdordemcompra(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	/**
	 * Busca todas as ordems de compras relacionadas a uma entrega.
	 * @param entrega
	 * @return
	 */
	public List<Ordemcompra> getOrdemcompraPorEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega() == null)
			throw new IllegalArgumentException("O par�metro entrega n�o pode ser nulo");
		return ordemcompraDAO.getOrdemcompraPorEntrega(entrega);
	}
}
