package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.MovimentacaoEstoqueHistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.dao.MovimentacaoEstoqueHistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoEstoqueHistoricoService extends GenericService<MovimentacaoEstoqueHistorico> {
	
	MovimentacaoEstoqueHistoricoDAO movimentacaoEstoqueHistoricoDAO;
	
	public void setMovimentacaoEstoqueHistoricoDAO(MovimentacaoEstoqueHistoricoDAO movimentacaoEstoqueHistoricoDAO) {
		this.movimentacaoEstoqueHistoricoDAO = movimentacaoEstoqueHistoricoDAO;
	}
	
	private static MovimentacaoEstoqueHistoricoService instance;
	public static MovimentacaoEstoqueHistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MovimentacaoEstoqueHistoricoService.class);
		}
		return instance;
	}
	
	public void preencherHistorico(Movimentacaoestoque movimentacaoestoque, MovimentacaoEstoqueAcao acao, String observacao, Boolean useTransaction) {
		MovimentacaoEstoqueHistorico movimentacaoEstoqueHistorico = new MovimentacaoEstoqueHistorico();
		try {
			movimentacaoEstoqueHistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		} catch (Exception e) {}
		movimentacaoEstoqueHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		movimentacaoEstoqueHistorico.setMovimentacaoEstoque(movimentacaoestoque);
		movimentacaoEstoqueHistorico.setMovimentacaoEstoqueAcao(acao);
		movimentacaoEstoqueHistorico.setObservacao(observacao);
		if(Boolean.TRUE.equals(useTransaction)){
			saveOrUpdate(movimentacaoEstoqueHistorico);			
		} else {
			saveOrUpdateNoUseTransaction(movimentacaoEstoqueHistorico);
		}
	}

	public List<MovimentacaoEstoqueHistorico> findByMovimentacaoEstoque(Movimentacaoestoque movimentacaoEstoque) {
		return movimentacaoEstoqueHistoricoDAO.findByMovimentacaoEstoque(movimentacaoEstoque);
	}
}
