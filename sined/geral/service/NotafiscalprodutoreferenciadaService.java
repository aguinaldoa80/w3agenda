package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoreferenciada;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoreferenciadaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoreferenciadaService extends GenericService<Notafiscalprodutoreferenciada> {

	private NotafiscalprodutoreferenciadaDAO notafiscalprodutoreferenciadaDAO;
	
	public void setNotafiscalprodutoreferenciadaDAO(
			NotafiscalprodutoreferenciadaDAO notafiscalprodutoreferenciadaDAO) {
		this.notafiscalprodutoreferenciadaDAO = notafiscalprodutoreferenciadaDAO;
	}
	
	public List<Notafiscalprodutoreferenciada> findByNotafiscalproduto(Notafiscalproduto nota) {
		return notafiscalprodutoreferenciadaDAO.findByNotafiscalproduto(nota);
	}
	
}
