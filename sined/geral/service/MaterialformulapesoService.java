package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulapeso;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso.TipoMaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso.VendaVO;
import br.com.linkcom.sined.geral.dao.MaterialformulapesoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialformulapesoService extends GenericService<Materialformulapeso>{

	private MaterialtipoService materialtipoService;
	private MaterialformulapesoDAO materialformulapesoDAO;
	
	public void setMaterialformulapesoDAO(
			MaterialformulapesoDAO materialformulapesoDAO) {
		this.materialformulapesoDAO = materialformulapesoDAO;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	
	/**
	 * Calcula o peso do material
	 *
	 * @param material
	 * @return
	 * @throws ScriptException
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public Double calculaPesoMaterial(Material material) throws ScriptException {
		Double peso = 0d;
		
		if(material != null){
			if(material.getListaMaterialformulapeso() != null && 
				material.getListaMaterialformulapeso().size() > 0){
				List<Materialformulapeso> listaMaterialformulapeso = material.getListaMaterialformulapeso();
				
				for (Materialformulapeso materialformulapeso : listaMaterialformulapeso) {
					if(materialformulapeso.getOrdem() == null ||
							materialformulapeso.getIdentificador() == null ||
							materialformulapeso.getFormula() == null){
						return 0d;
					}
				}
				
				MaterialVO materialVO = new MaterialVO();
				if(material.getProduto_altura() != null){
					materialVO.setAltura(material.getProduto_altura());
				}
				if(material.getProduto_comprimento() != null){
					materialVO.setComprimento(material.getProduto_comprimento());
				}
				if(material.getProduto_largura() != null){
					materialVO.setLargura(material.getProduto_largura());
				}
				if(material.getQtdvolume() != null){
					materialVO.setQtdvolume(material.getQtdvolume());
				}
				if(material.getPesobruto() != null){
					materialVO.setPesobruto(material.getPesobruto());
				}
				
				TipoMaterialVO tipoMaterialVO = new TipoMaterialVO();
				Materialtipo materialtipo = material.getMaterialtipo();
				if(materialtipo != null && materialtipo.getCdmaterialtipo() != null){
					if(materialtipo.getPesoespecifico() == null){
						materialtipo = materialtipoService.load(materialtipo, "materialtipo.cdmaterialtipo, materialtipo.pesoespecifico");
					}
					if(materialtipo.getPesoespecifico() != null){
						tipoMaterialVO.setPesoEspecifico(materialtipo.getPesoespecifico());
					}
				}
				
				VendaVO vendaVO = new VendaVO();
				vendaVO.setUnidademedida(material.getVendasimbolounidademedida());
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				engine.put("material", materialVO);
				engine.put("tipoMaterial", tipoMaterialVO);
				engine.put("venda", vendaVO);
				
				
				Collections.sort(listaMaterialformulapeso, new Comparator<Materialformulapeso>(){
					public int compare(Materialformulapeso o1, Materialformulapeso o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Materialformulapeso materialformulapeso : listaMaterialformulapeso) {
					Object obj = engine.eval(materialformulapeso.getFormula());
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(materialformulapeso.getIdentificador(), resultado);
					peso = resultado;
				}
			}  else if(material.getPeso() != null){
				peso = material.getPeso();
			}
		}
		
		return peso;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialformulapesoDAO#findByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public List<Materialformulapeso> findByMaterial(Material material) {
		return materialformulapesoDAO.findByMaterial(material);
	}
	

}
