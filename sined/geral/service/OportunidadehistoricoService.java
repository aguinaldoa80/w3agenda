package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.dao.OportunidadehistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OportunidadehistoricoService extends GenericService<Oportunidadehistorico>{
	
	protected OportunidadehistoricoDAO oportunidadehistoricoDAO;
	
	public void setOportunidadehistoricoDAO(OportunidadehistoricoDAO oportunidadehistoricoDAO) {
		this.oportunidadehistoricoDAO = oportunidadehistoricoDAO;
	}
	
	public List<Oportunidadehistorico> findByOportunidade(Oportunidade oportunidade){
		return oportunidadehistoricoDAO.findByOportunidade(oportunidade, null, null);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * @param oportunidadehistorico
	 * @author Taidson
	 * @since 10/09/2010
	 */
	public void insertHistoricoForAgendamento(Oportunidadehistorico oportunidadehistorico) {
		oportunidadehistoricoDAO.insertHistoricoForAgendamento(oportunidadehistorico);
	}
	
	public List<Oportunidadehistorico> findByOportunidade(Oportunidade oportunidade, Atividadetipo atividadetipo){
		return oportunidadehistoricoDAO.findByOportunidade(oportunidade, atividadetipo, null);
	}
	
	public List<Oportunidadehistorico> findByOportunidade(Oportunidade oportunidade, Atividadetipo atividadetipo, Empresa empresa){
		return oportunidadehistoricoDAO.findByOportunidade(oportunidade, atividadetipo, empresa);
	}
}
