package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorbeneficio;
import br.com.linkcom.sined.geral.dao.ColaboradorbeneficioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorbeneficioService extends GenericService<Colaboradorbeneficio>{
	
	private ColaboradorbeneficioDAO colaboradorbeneficioDAO;
	private static ColaboradorbeneficioService instance;
	
	public void setColaboradorbeneficioDAO(ColaboradorbeneficioDAO colaboradorbeneficioDAO) {
		this.colaboradorbeneficioDAO = colaboradorbeneficioDAO;
	}
	
	public static ColaboradorbeneficioService getInstance() {
		if (instance==null){
			instance = Neo.getObject(ColaboradorbeneficioService.class);
		}
		return instance;
	}
	
	public List<Colaboradorbeneficio> findByColaborador(Colaborador colaborador){
		return colaboradorbeneficioDAO.findByColaborador(colaborador);		
	}
}