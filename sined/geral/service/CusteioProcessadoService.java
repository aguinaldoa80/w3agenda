package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.CusteioHistorico;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.CusteioTipoDocumento;
import br.com.linkcom.sined.geral.bean.SituacaoCusteio;
import br.com.linkcom.sined.geral.bean.TipoDocumentoCusteioEnum;
import br.com.linkcom.sined.geral.dao.CusteioProcessadoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CusteioProcessadoService extends GenericService<CusteioProcessado> {

	
	private static CusteioProcessadoService instance;
	private CusteioProcessadoDAO custeioProcessadoDAO;
	private CusteioHistoricoService custeioHistoricoService;
	private CusteioTipoDocumentoService custeioTipoDocumentoService;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private MovimentacaoestoqueService movimentacaoEstoqueService;
	
	public void setMovimentacaoEstoqueService(MovimentacaoestoqueService movimentacaoEstoqueService) {this.movimentacaoEstoqueService = movimentacaoEstoqueService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setCusteioTipoDocumentoService(CusteioTipoDocumentoService custeioTipoDocumentoService) {this.custeioTipoDocumentoService = custeioTipoDocumentoService;}
	public void setCusteioHistoricoService(CusteioHistoricoService custeioHistoricoService) {this.custeioHistoricoService = custeioHistoricoService;}
	public void setCusteioProcessadoDAO(CusteioProcessadoDAO custeioProcessadoDAO) {this.custeioProcessadoDAO = custeioProcessadoDAO;}
	public static CusteioProcessadoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CusteioProcessadoService.class);
		}
		return instance;
	}
	
	@Override
	public CusteioProcessado loadForEntrada(CusteioProcessado bean) {
		bean = super.loadForEntrada(bean);
		bean.setListaCusteioHistorico(custeioHistoricoService.findByCusteioProcessado(bean));
		bean.setListaCusteioTipoDocumento(custeioTipoDocumentoService.findByCusteioProcessado(bean));
		return bean;
	}
	public CusteioProcessado buscarCusteioPorId(Integer id) {
		return custeioProcessadoDAO.buscarCusteioPorId(id);
	}
	public void estornarCusteioProcessado(CusteioProcessado custeioProcessado) {
		String obs = custeioProcessado.getObservacaohistorico();
		custeioProcessado = this.buscarCusteioPorId(custeioProcessado.getCdCusteioProcessado());
		List<CusteioTipoDocumento> listaDocumentos = custeioProcessado.getListaCusteioTipoDocumento();
		for (CusteioTipoDocumento custeioDocumento : listaDocumentos) {
			if(TipoDocumentoCusteioEnum.FINANCEIRA.equals(custeioDocumento.getTipoDocumentoCusteioEnum())){
				movimentacaoService.updateCdRateio(custeioDocumento.getCodObjetoOrigem(), custeioDocumento.getCodRateioOrigem());
			}else if(TipoDocumentoCusteioEnum.ESTOQUE.equals(custeioDocumento.getTipoDocumentoCusteioEnum())){
				movimentacaoEstoqueService.updateCdRateio(custeioDocumento.getCodObjetoOrigem(), custeioDocumento.getCodRateioOrigem().getCdrateio());
			}else{
				documentoService.updateCdRateio(custeioDocumento.getCodObjetoOrigem(), custeioDocumento.getCodRateioOrigem().getCdrateio());
			}
		}
		custeioProcessadoDAO.updateSituacao(custeioProcessado,SituacaoCusteio.ESTORNADO);
		this.registrarHistorico(custeioProcessado,obs,SituacaoCusteio.ESTORNADO);
	}

	
	public void registrarHistorico(CusteioProcessado custeioProcessado,String observacao,SituacaoCusteio situacao){
		CusteioHistorico historico = new CusteioHistorico();
		historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		historico.setDtaltera(SinedDateUtils.currentTimestamp());
		historico.setObservacao(observacao);
		historico.setSituacaoCusteio(situacao);
		historico.setCusteioProcessado(custeioProcessado);
		custeioHistoricoService.saveOrUpdate(historico);
	}
	
}
