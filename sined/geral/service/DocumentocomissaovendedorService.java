package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.DocumentocomissaovendedorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentocomissaovendedorService  extends GenericService<Documentocomissao>{
	
	private DocumentocomissaovendedorDAO documentocomissaovendedorDAO;
	private ContratoService contratoService;
	private VendaService vendaService;
	private PedidovendaService pedidovendaService;
	private VendamaterialService vendamaterialService;
	private VendapagamentoService vendapagamentoService;
	
	public void setDocumentocomissaovendedorDAO(DocumentocomissaovendedorDAO documentocomissaovendedorDAO) {
		this.documentocomissaovendedorDAO = documentocomissaovendedorDAO;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	
	public void alterarDocumentoComissaoVendedor (Documentocomissao documentocomissao){
		documentocomissaovendedorDAO.alterarDocumentoComissaoVendedor ( documentocomissao);
	}

	/**
	* M�todo que ajusta o documentocomissao para salvar. Atualiza o valor que estiver nulo (percentual ou valor)
	*
	* @param documentocomissao
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public void ajusteSave(Documentocomissao documentocomissao) {
		if(documentocomissao != null){
			if((documentocomissao.getValorcomissao() == null && documentocomissao.getPercentualcomissao() != null) ||
				(documentocomissao.getValorcomissao() != null && documentocomissao.getPercentualcomissao() == null)){
				Money valorBase = null;
				Integer qtdeParcela = null;
				if(documentocomissao.getContrato() != null && documentocomissao.getContrato().getCdcontrato() != null){
					List<Contrato> listaContrato = contratoService.findForCobranca(documentocomissao.getContrato().getCdcontrato().toString());
					if(SinedUtil.isListNotEmpty(listaContrato)){
						valorBase = listaContrato.get(0).getValorContrato();
					}
				}else if(documentocomissao.getVenda() != null && documentocomissao.getVenda().getCdvenda() != null){
					Venda venda = vendaService.loadForEntrada(documentocomissao.getVenda());
					if(venda != null){
						venda.setListavendamaterial(vendamaterialService.findByVenda(venda));
						venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
						valorBase = venda.getTotalvenda();
						if(venda.getListavendapagamento() != null && venda.getListavendapagamento().size() > 0){
							qtdeParcela = venda.getListavendapagamento().size();
						}
					}
				}else if(documentocomissao.getPedidovenda() != null && documentocomissao.getPedidovenda().getCdpedidovenda() != null){
					Pedidovenda pedidovenda = pedidovendaService.loadForEntrada(documentocomissao.getPedidovenda());
					if(pedidovenda != null){
						valorBase = pedidovenda.getTotalvenda();
						if(pedidovenda.getListaPedidovendapagamento() != null && pedidovenda.getListaPedidovendapagamento().size() > 0){
							qtdeParcela = pedidovenda.getListaPedidovendapagamento().size();
						}
					}
				}
				if(valorBase != null){
					if(qtdeParcela != null && qtdeParcela > 1){
						valorBase = valorBase.divide(new Money(qtdeParcela));
					}
					if(documentocomissao.getValorcomissao() == null && documentocomissao.getPercentualcomissao() != null){
						documentocomissao.setValorcomissao(valorBase.multiply(documentocomissao.getPercentualcomissao()).divide(new Money(100)));
					}else if(documentocomissao.getValorcomissao() != null && documentocomissao.getPercentualcomissao() == null){
						documentocomissao.setPercentualcomissao(documentocomissao.getValorcomissao().multiply(new Money(100)).divide(valorBase));
					}
				}
			}
		}
	}
	
	/**
	* M�todo que atualiza o valor ou percentual da comiss�o
	*
	* @see br.com.linkcom.sined.geral.service.DocumentocomissaovendedorService.ajusteSave(Documentocomissao documentocomissao)
	* @see br.com.linkcom.sined.geral.service.DocumentocomissaovendedorService#alterarDocumentoComissaoVendedor(Documentocomissao documentocomissao)
	* 
	* @param valor
	* @param percentual
	* @param whereIn
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public void atualizarValorPercentual(Money valor, Money percentual, String whereIn) {
		if(StringUtils.isNotBlank(whereIn) && (valor != null || percentual != null)){
			List<Documentocomissao> lista = findForAtualizarValorPercentual(whereIn);
			if(SinedUtil.isListNotEmpty(lista)){
				for(Documentocomissao documentocomissao : lista){
					if(documentocomissao.getVenda() != null || documentocomissao.getPedidovenda() != null || 
							documentocomissao.getContrato() != null){
						documentocomissao.setPercentualcomissao(percentual);
						documentocomissao.setValorcomissao(valor);
						ajusteSave(documentocomissao);
						alterarDocumentoComissaoVendedor (documentocomissao);
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendedorDAO#findForAtualizarValorPercentual(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public List<Documentocomissao> findForAtualizarValorPercentual(String whereIn) {
		return documentocomissaovendedorDAO.findForAtualizarValorPercentual(whereIn);
	}
	
}
