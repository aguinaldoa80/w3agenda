package br.com.linkcom.sined.geral.service;

import java.util.HashMap;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Sincronizacaotabelaandroid;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaandroid;
import br.com.linkcom.sined.geral.dao.SincronizacaotabelaandroidDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.downloadbean.DownloadBeanRESTModel;
import br.com.linkcom.sined.util.rest.android.downloadbean.DownloadBeanRESTWSBean;

public class SincronizacaotabelaandroidService extends GenericService<Sincronizacaotabelaandroid> {

	private SincronizacaotabelaandroidDAO sincronizacaotabelaandroidDAO;
	
	public void setSincronizacaotabelaandroidDAO(SincronizacaotabelaandroidDAO sincronizacaotabelaandroidDAO) {
		this.sincronizacaotabelaandroidDAO = sincronizacaotabelaandroidDAO;
	}

	public HashMap<Tabelaandroid, String> getMaptabela(DownloadBeanRESTWSBean command, DownloadBeanRESTModel model, Boolean excluido) {
		HashMap<Tabelaandroid, StringBuilder> mapTabelaAux = new HashMap<Tabelaandroid, StringBuilder>();
		List<Sincronizacaotabelaandroid> lista = findForAndroid(command, excluido, null);
		
		if(SinedUtil.isListNotEmpty(lista)){
			for(Sincronizacaotabelaandroid bean : lista){
				if(bean.getTabelaandroid() != null && bean.getTabelaid() != null){
					System.out.println(bean.getTabelaandroid());
					if(mapTabelaAux.get(bean.getTabelaandroid()) == null){
						mapTabelaAux.put(bean.getTabelaandroid(), new StringBuilder(bean.getTabelaid().toString()));
					}else {
						mapTabelaAux.put(bean.getTabelaandroid(), mapTabelaAux.get(bean.getTabelaandroid()).append(",").append(bean.getTabelaid().toString()));
					}
				}
				if(bean.getDtalteracao() != null && model != null){
					model.setDataUltimaSincronizacao(bean.getDtalteracao().getTime());
				}
			}
		}
		
		if(model != null){
			lista = findForAndroid(command, excluido, model.getDataUltimaSincronizacao());
			
			if(SinedUtil.isListNotEmpty(lista)){
				for(Sincronizacaotabelaandroid bean : lista){
					if(bean.getTabelaandroid() != null && bean.getTabelaid() != null){
						System.out.println(bean.getTabelaandroid());
						if(mapTabelaAux.get(bean.getTabelaandroid()) == null){
							mapTabelaAux.put(bean.getTabelaandroid(), new StringBuilder(bean.getTabelaid().toString()));
						}else {
							mapTabelaAux.put(bean.getTabelaandroid(), mapTabelaAux.get(bean.getTabelaandroid()).append(",").append(bean.getTabelaid().toString()));
						}
					}
					if(bean.getDtalteracao() != null && model != null){
						model.setDataUltimaSincronizacao(bean.getDtalteracao().getTime());
					}
				}
			}
		}
		
		HashMap<Tabelaandroid, String> mapTabela = new HashMap<Tabelaandroid, String>();
		if(mapTabelaAux.size() > 0){
			for(Tabelaandroid value : mapTabelaAux.keySet()){
				mapTabela.put(value, mapTabelaAux.get(value).toString());
			}
		}
		return mapTabela;
	}

	private List<Sincronizacaotabelaandroid> findForAndroid(DownloadBeanRESTWSBean command, Boolean excluido, Long data) {
		return sincronizacaotabelaandroidDAO.findForAndroid(command, excluido, data);
	}
}
