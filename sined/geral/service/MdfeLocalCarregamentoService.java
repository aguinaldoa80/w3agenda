package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeLocalCarregamento;
import br.com.linkcom.sined.geral.dao.MdfeLocalCarregamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeLocalCarregamentoService extends GenericService<MdfeLocalCarregamento>{

	private MdfeLocalCarregamentoDAO mdfeLocalCarregamentoDAO;
	public void setMdfeLocalCarregamentoDAO(
			MdfeLocalCarregamentoDAO mdfeLocalCarregamentoDAO) {
		this.mdfeLocalCarregamentoDAO = mdfeLocalCarregamentoDAO;
	}
	
	public List<MdfeLocalCarregamento> findByMdfe(Mdfe mdfe){
		return mdfeLocalCarregamentoDAO.findByMdfe(mdfe);
	}
}
