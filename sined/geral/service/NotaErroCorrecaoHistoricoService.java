package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.NotaErroCorrecaoHistorico;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.dao.NotaErroCorrecaoHistoricoDAO;

public class NotaErroCorrecaoHistoricoService extends GenericService<NotaErroCorrecaoHistorico> {

	public NotaErroCorrecaoHistoricoDAO notaErroCorrecaoHistoricoDAO;
	
	public void setNotaErroCorrecaoHistoricoDAO(
			NotaErroCorrecaoHistoricoDAO notaErroCorrecaoHistoricoDAO) {
		this.notaErroCorrecaoHistoricoDAO = notaErroCorrecaoHistoricoDAO;
	}

	public List<NotaErroCorrecaoHistorico> findByNotaerrocorrecaoHistorico(Notaerrocorrecao notaerrocorrecao) {
		return notaErroCorrecaoHistoricoDAO.findByNotaerrocorrecaoHistorico(notaerrocorrecao);
	}
	
}