package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.dao.UsuariopapelDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class UsuariopapelService extends GenericService<Usuariopapel> {

	private UsuarioService usuarioService;
	private UsuariopapelDAO usuariopapelDAO;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setUsuariopapelDAO(UsuariopapelDAO usuariopapelDAO) {
		this.usuariopapelDAO = usuariopapelDAO;
	}
	
	/**
	 * M�todo para carregar os pape�s selecionados para o usu�rio
	 * 
	 * @param usuario
	 * @return
	 * @author Jo�o Paulo Zica
	 * @author Hugo Ferreira
	 */
	public List<Papel> carregaPapel(Usuario usuario){
		List<Usuariopapel> listaUsuariopapel = null;
		List<Papel> listaPapel = new ArrayList<Papel>();
		if (usuario.getListaUsuariopapel() == null || usuario.getListaUsuariopapel().isEmpty() && usuario.getCdpessoa() != null) {
			/* 
			 * Adicionado em 24/03/2008 por Hugo Ferreira
			 * esta condi��o acontece ao editar um usu�rio, colocar um e-mail j� cadastrado no sistema.
			 * caso n�o seja feito isto, a lista de papel do usu�rio retorna vazia para a tela. */
			Usuario usuarioTmp = usuarioService.carregaUsuario(usuario);
			listaUsuariopapel = usuarioTmp.getListaUsuariopapel();
		} else {
			listaUsuariopapel = usuario.getListaUsuariopapel();
		}
		for (Usuariopapel usuariopapel : listaUsuariopapel) {
			listaPapel.add(usuariopapel.getPapel());
		}
		return listaPapel;
	}	

	/* singleton */
	private static UsuariopapelService instance;
	public static UsuariopapelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuariopapelService.class);
		}
		return instance;
	}

	public List<Usuariopapel> findByUsuarioLogado() {
		return usuariopapelDAO.findByUsuario(SinedUtil.getUsuarioLogado());
	}

	public List<Usuariopapel> findByPapeis(List<Papel> listaPapel) {
		return usuariopapelDAO.findByPapeis(listaPapel);
	}
	
	public List<Usuariopapel> findByPapeisTodos(List<Papel> listaPapel) {
		return usuariopapelDAO.findByPapeisTodos(listaPapel);
	}
}
