package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.GeraMovimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Rateio;

@Bean
public class GerarMovimentacaoService {

	/**
	 * M�todo para criar um bean <code>GeraMovimentacao</code> com dados de uma conta.
	 * 
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public GeraMovimentacao createBeanGeraMovimentacao(Conta conta){
		GeraMovimentacao gera = new GeraMovimentacao(true);
		
		Movimentacao origem = gera.getMovimentacaoorigem();
		Movimentacao destino = gera.getMovimentacaodestino();
		
		origem.setHistorico(conta.getDescricao());
		
		destino.setConta(conta);
		Money saldoatual = new Money(Math.abs(conta.getVconta().getSaldoatual().getValue().doubleValue()));
		
		origem.setValor(saldoatual);
		destino.setValor(saldoatual);
		
		Rateio rateio = gera.getRateio();
		rateio.setValoraux(saldoatual.toString());
		rateio.setValortotaux(saldoatual.toString());
		
		rateio = gera.getRateio2();
		rateio.setValoraux(saldoatual.toString());
		rateio.setValortotaux(saldoatual.toString());
		
		return gera;
	}
}
