package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Beneficio;
import br.com.linkcom.sined.geral.bean.Beneficiovalor;
import br.com.linkcom.sined.geral.dao.BeneficioDAO;
import br.com.linkcom.sined.geral.dao.BeneficiovalorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BeneficioService extends GenericService<Beneficio>{

	private static BeneficioService instance;
	
	private BeneficioDAO beneficioDAO;
	private BeneficiovalorDAO beneficiovalorDAO;
	
	public void setBeneficioDAO(BeneficioDAO beneficioDAO) {
		this.beneficioDAO = beneficioDAO;
	}
	public void setBeneficiovalorDAO(BeneficiovalorDAO beneficiovalorDAO) {
		this.beneficiovalorDAO = beneficiovalorDAO;
	}
	
	public static BeneficioService getInstance() {
		if (instance==null){
			instance = Neo.getObject(BeneficioService.class);
		}
		return instance;
	}

	public void processarListagem(ListagemResult<Beneficio> listagemResult) {
		List<Beneficio> listaBeneficio = listagemResult.list();
		String whereIn = "-1";
		
		for (Beneficio beneficio: listaBeneficio){
			whereIn += ", " + beneficio.getCdbeneficio();
		}
		
		Map<Integer, List<Beneficiovalor>> mapa = new HashMap<Integer, List<Beneficiovalor>>();
		List<Beneficiovalor> listaBeneficiovalor = beneficiovalorDAO.findByCdsbeneficio(whereIn);
		
		for (Beneficiovalor beneficiovalor: listaBeneficiovalor){
			List<Beneficiovalor> listaAux = mapa.get(beneficiovalor.getBeneficio().getCdbeneficio());
			if (listaAux==null){
				listaAux = new ArrayList<Beneficiovalor>();
			}
			listaAux.add(beneficiovalor);
			mapa.put(beneficiovalor.getBeneficio().getCdbeneficio(), listaAux);
		}
		
		for (Beneficio beneficio: listaBeneficio){
			beneficio.setListaBeneficiovalor(mapa.get(beneficio.getCdbeneficio()));
		}
	}
	
	@Override
	protected ListagemResult<Beneficio> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Beneficio> listagemResult = beneficioDAO.findForExportacao(filtro);
		processarListagem(listagemResult);
		
		return listagemResult;
	}
}