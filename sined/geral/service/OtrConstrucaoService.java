package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrConstrucao;
import br.com.linkcom.sined.geral.dao.OtrConstrucaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrConstrucaoService extends GenericService<OtrConstrucao>{
	protected OtrConstrucaoDAO otrConstrucaoDao;

	public void setOtrConstrucaoDao(OtrConstrucaoDAO otrConstrucaoDao) {
		this.otrConstrucaoDao = otrConstrucaoDao;
	}
	
	
	public List<OtrConstrucao> findConstrucao(){
		return otrConstrucaoDao.findConstrucao();
	}
}
