package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.dao.PrazopagamentoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.PrazopagamentoitemOfflineJSON;
import br.com.linkcom.sined.util.rest.android.prazopagamentoitem.PrazopagamentoitemRESTModel;

public class PrazopagamentoitemService extends GenericService<Prazopagamentoitem> {
	private PrazopagamentoitemDAO prazopagamentoitemDAO;
	
	public void setPrazopagamentoitemDAO(PrazopagamentoitemDAO prazopagamentoitemDAO) {
		this.prazopagamentoitemDAO = prazopagamentoitemDAO;
	}
	
	/* singleton */
	private static PrazopagamentoitemService instance;
	public static PrazopagamentoitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PrazopagamentoitemService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO que fornece os itens de um prazo de pagamento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PrazopagamentoitemDAO#findByPrazo(Prazopagamento)
	 * @param prazo
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Prazopagamentoitem> findByPrazo(Prazopagamento prazo) {
		return prazopagamentoitemDAO.findByPrazo(prazo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param prazo
	 * @return
	 * @author Ramon Brazil
	 */
	public Long countParcelas(Prazopagamento prazo) {
		return prazopagamentoitemDAO.countParcelas(prazo);
	}
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param prazo
	 * @param parcela
	 * @return
	 * @author Ramon Brazil
	 */
	public Prazopagamentoitem findByPrazoOfParcela(Prazopagamento prazo, Integer parcela) {
		return prazopagamentoitemDAO.findByPrazoOfParcela(prazo, parcela);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param prazo
	 * @param parcela
	 * @return
	 * @author Taidson
	 * @since 28/06/2010
	 */
	public Prazopagamentoitem obtemDadosParcela(Prazopagamento prazo, Integer parcela){
		return prazopagamentoitemDAO.obtemDadosParcela(prazo, parcela);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param prazopagamento
	 * @return
	 * @author Taidson
	 * @since 28/06/201
	 */
	public Integer qtdeParcelasPrazoPagamento(Prazopagamento prazopagamento){
		return prazopagamentoitemDAO.qtdeParcelasPrazoPagamento(prazopagamento);
	}

	
	public List<PrazopagamentoitemOfflineJSON> findForPVOffline() {
		List<PrazopagamentoitemOfflineJSON> lista = new ArrayList<PrazopagamentoitemOfflineJSON>();
		for(Prazopagamentoitem ppi : prazopagamentoitemDAO.findForPVOffline())
			lista.add(new PrazopagamentoitemOfflineJSON(ppi));
		
		return lista;
	}

	public List<PrazopagamentoitemRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	public List<PrazopagamentoitemRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PrazopagamentoitemRESTModel> lista = new ArrayList<PrazopagamentoitemRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Prazopagamentoitem bean : prazopagamentoitemDAO.findForAndroid(whereIn))
				lista.add(new PrazopagamentoitemRESTModel(bean));
		}
		
		return lista;
	}
	
}
