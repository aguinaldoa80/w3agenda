package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.geral.dao.OperacaoContabilTipoLancamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OperacaoContabilTipoLancamentoService extends GenericService<OperacaoContabilTipoLancamento> {
	private OperacaoContabilTipoLancamentoDAO operacaoContabilTipoLancamentoDAO;
	
	public void setOperacaoContabilTipoLancamentoDAO(OperacaoContabilTipoLancamentoDAO operacaoContabilTipoLancamentoDAO) {this.operacaoContabilTipoLancamentoDAO = operacaoContabilTipoLancamentoDAO;}
		
	public List<OperacaoContabilTipoLancamento> buscarOrdenados(String whereIn) {
		return operacaoContabilTipoLancamentoDAO.buscarOrdenados(whereIn);
	}
}
