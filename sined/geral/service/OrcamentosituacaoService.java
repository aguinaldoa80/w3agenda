package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.geral.dao.OrcamentosituacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrcamentosituacaoService extends GenericService<Orcamentosituacao>{

	private OrcamentosituacaoDAO orcamentosituacaoDAO;
		
	public void setOrcamentosituacaoDAO(OrcamentosituacaoDAO orcamentosituacaoDAO) {
		this.orcamentosituacaoDAO = orcamentosituacaoDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentosituacaoDAO#findForComboFlex()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Orcamentosituacao> findForComboFlex() {
		return orcamentosituacaoDAO.findForComboFlex(); 
	}
}
