package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Contigencianfe;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontigencia;
import br.com.linkcom.sined.geral.dao.ContigencianfeDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContigencianfeService extends GenericService<Contigencianfe>{
	
	private ContigencianfeDAO contigencianfeDAO;
	
	public void setContigencianfeDAO(ContigencianfeDAO contigencianfeDAO) {
		this.contigencianfeDAO = contigencianfeDAO;
	}

	/**
	 * M�todo que retorna o objeto preenchido de cotig�ncia geral ou local.
	 *
	 * @param uf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	public Contigencianfe getContigenciaAtual(Uf uf) {
		List<Contigencianfe> listaContigenciaLocal = this.findAtivosByUf(uf);
		if(listaContigenciaLocal != null && listaContigenciaLocal.size() > 0){
			return listaContigenciaLocal.get(0);
		}
		
		try{
			List<Contigencianfe> listaContigenciaGeral = this.getContigenciaInW3controle(uf);
			if(listaContigenciaGeral != null && listaContigenciaGeral.size() > 0){
				return listaContigenciaGeral.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * M�todo que busca no W3Controle as contig�ncias ativadas.
	 *
	 * @param uf
	 * @return
	 * @throws NamingException
	 * @throws IOException
	 * @throws JDOMException
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	private List<Contigencianfe> getContigenciaInW3controle(Uf uf) throws NamingException, IOException, JDOMException {
		URL url = montaURLRequisicao();
		String data = "uf=" + uf.getSigla();
		
		URLConnection conn = url.openConnection(); 
		conn.setDoOutput(true); 
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
		wr.write(data); 
		wr.flush(); 
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		String line; 
		String xml = ""; 
		
		while ((line = rd.readLine()) != null) { xml += line; } 
		
		wr.close(); 
		rd.close(); 
		
		return verificaXmlContigencia(xml);
	}
	
	/**
	 * Recebe a string com o XML e transforma em uma lista de objetos de Contigencianfe
	 *
	 * @param xml
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	@SuppressWarnings("unchecked")
	private List<Contigencianfe> verificaXmlContigencia(String xml) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(Util.strings.tiraAcento(xml).getBytes());
		
		if(rootElement != null && rootElement.getContent().size() > 0){
			List<Contigencianfe> listaContigencia = new ArrayList<Contigencianfe>();
			List<Element> listaContigenciaElement = SinedUtil.getListChildElement("Contigencia", rootElement.getContent());
			
			for (Element contigenciaElement : listaContigenciaElement) {
				Element tipoElement = SinedUtil.getChildElement("Tipo", contigenciaElement.getContent());
				Element dataentradaElement = SinedUtil.getChildElement("Dataentrada", contigenciaElement.getContent());
				Element justificativaElement = SinedUtil.getChildElement("Justificativa", contigenciaElement.getContent());
				
				Contigencianfe contigencianfe = new Contigencianfe();
				contigencianfe.setTipocontigencia(Tipocontigencia.valueOf(SinedUtil.getElementString(tipoElement)));
				contigencianfe.setDtentrada(SinedUtil.getElementTimestamp(dataentradaElement));
				contigencianfe.setJustificativa(SinedUtil.getElementString(justificativaElement));
				listaContigencia.add(contigencianfe);
			}
			
			return listaContigencia;
		}
		
		return null;
	}

	/**
	 * M�todo que monta a URL para consulta ao W3Controle para Contig�ncia NF-e
	 *
	 * @return
	 * @throws MalformedURLException
	 * @throws NamingException
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	private URL montaURLRequisicao() throws MalformedURLException, NamingException{
		String urlString = "linkcom.w3erp.com.br";
		
		if(SinedUtil.isAmbienteDesenvolvimento()){
			urlString = InitialContext.doLookup("CONTIGENCIA_URL");
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o de contig�ncia.");
			}
		}
		
		return new URL("http://" + urlString + "/w3controle/pub/process/Contigencianfegeral");
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param uf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	private List<Contigencianfe> findAtivosByUf(Uf uf) {
		return contigencianfeDAO.findAtivosByUf(uf);
	}
	
}
