package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Sessao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.SessaoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SessaoService extends GenericService<Sessao> {

	private SessaoDAO sessaoDAO;
	
	public void setSessaoDAO(SessaoDAO sessaoDAO) {
		this.sessaoDAO = sessaoDAO;
	}
	
	public void makeLogin(Usuario usuario){
		Sessao sessao = new Sessao();
		sessao.setDatahora(new Timestamp(System.currentTimeMillis()));
		sessao.setEntrada(true);
		
		String ipAddress = NeoWeb.getRequestContext().getServletRequest().getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
		    ipAddress = NeoWeb.getRequestContext().getServletRequest().getRemoteAddr();
		}
		sessao.setIp(ipAddress);
		
		sessao.setSid(NeoWeb.getRequestContext().getSession().getId());
		sessao.setUseragent(NeoWeb.getRequestContext().getServletRequest().getHeader("User-Agent"));
		sessao.setUsuario(usuario);
		sessao.setResolucao(NeoWeb.getRequestContext().getParameter("resolucao"));
		
		sessaoDAO.insertSessao(sessao);
		
		String urlLogando = "";
		try{
			urlLogando = "(" + SinedUtil.getUrlWithoutContext() + ") ";
		} catch (Exception e) {
		}
		
		System.out.println(urlLogando + usuario.getCdpessoa() + " - " + usuario.getNome() + " logando...");
	}
	
	
	private static SessaoService instance;
	public static SessaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(SessaoService.class);
		}
		return instance;
	}

	public Boolean getUsuarioJaAbriuPopupAviso() {
		return sessaoDAO.getUsuarioJaAbriuPopupAviso();
	}

	public void updatePopupAviso() {
		Sessao sessao = sessaoDAO.findByUsuario();
		sessaoDAO.updatePopupAviso(sessao);
	}
}
