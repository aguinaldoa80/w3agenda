package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Indicerecursogeral;
import br.com.linkcom.sined.geral.bean.Indicerecursohumano;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Tarefasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporecursogeral;
import br.com.linkcom.sined.geral.dao.TarefaDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ImportarTarefasPlanejamentoBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration.RelComparativoIndiceSituacao;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.TarefaFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RelComparativoIndice;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RelComparativoTarefas;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoTarefaFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.ComparativoIndiceReportFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.ComparativoTarefaReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.Apontamentotarefaatraso;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class TarefaService extends GenericService<Tarefa>{

	public static final String WHERENOTIN_TAREFA_SESSION = "wherenotin_tarefa_session";
	
	private TarefaService tarefaService;
	private TarefaDAO tarefaDAO;
	private IndiceService indiceService;
	private ProducaodiariaService producaodiariaService;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	private CalendarioService calendarioService;
	private TarefarecursogeralService tarefarecursogeralService;
	private TarefarequisicaoService tarefarequisicaoService;
	private UnidademedidaService unidademedidaService;
		
	public void setTarefarecursogeralService(TarefarecursogeralService tarefarecursogeralService) {this.tarefarecursogeralService = tarefarecursogeralService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setTarefaService(TarefaService tarefaService) {this.tarefaService = tarefaService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {this.planejamentoService = planejamentoService;	}
	public void setTarefarequisicaoService(TarefarequisicaoService tarefarequisicaoService) {this.tarefarequisicaoService = tarefarequisicaoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	
	/* singleton */
	private static TarefaService instance;
	public static TarefaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TarefaService.class);
		}
		return instance;
	}
	
	private List<Tarefa>listagemTarefas(TarefaFiltro filtro) {
		return tarefaDAO.listagemTarefas(filtro);
	}

	public IReport createTarefaReport(TarefaFiltro filtro) {
		Report report = new Report("/projeto/tarefa");
	
		report.setDataSource(tarefaService.listagemTarefas(filtro));
		
		Projeto projeto = null;
		if(filtro.getProjeto() != null){
			projeto = projetoService.load(filtro.getProjeto(), "projeto.cdprojeto, projeto.nome");
		}
		Planejamento planejamento = null;
		if(filtro.getPlanejamento() != null){
			planejamento = planejamentoService.load(filtro.getPlanejamento(), "planejamento.cdplanejamento, planejamento.descricao");
		}
		report.addParameter("PROJETO", projeto);
		report.addParameter("PLANEJAMENTO", planejamento);
		report.addSubReport("SUBTAREFA", new Report("/projeto/subTarefa"));
		report.addSubReport("SUBTAREFARH", new Report("/projeto/subTarefaRH"));

		return report;
	}
	
	public void setIndiceService(IndiceService indiceService) {
		this.indiceService = indiceService;
	}
	public void setTarefaDAO(TarefaDAO tarefaDAO) {
		this.tarefaDAO = tarefaDAO;
	}
	public void setProducaodiariaService(
			ProducaodiariaService producaodiariaService) {
		this.producaodiariaService = producaodiariaService;
	}
	
	public void atualizaSituacaoTarefas(String whereIn) {
		List<Tarefa> listaTarefa = this.findSituacaoTarefas(whereIn);
		this.atualizaTarefas(listaTarefa);
	}
	
	public List<Tarefa> findSituacaoTarefas(String whereIn){
		return tarefaDAO.findSituacaoTarefas(whereIn);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Atualiza a situa��o das tarefas que est�o desatualizadas
	* Obs: este m�todo � chamado toda vez que � logado um usu�rio
	* 
	* @see	br.com.linkcom.sined.geral.dao.TarefaDAO#findSituacaoTodasTarefas()
	* @see	br.com.linkcom.sined.geral.dao.TarefaDAO#updateSituacao(Tarefasituacao tarefasituacao, Tarefa tarefa)
	*
	* @since Aug 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaSituacaoTodasTarefas(){
		List<Tarefa> listaTarefa = this.findSituacaoTodasTarefas();
		this.atualizaTarefas(listaTarefa);
	}
	
	private void atualizaTarefas(List<Tarefa> listaTarefa) {
		List<Tarefa> listaNaoSalva = new ArrayList<Tarefa>();
		if(listaTarefa != null && !listaTarefa.isEmpty()){
			for(Tarefa tarefa : listaTarefa){
				if(tarefa.getDtfim() == null){
					Date dtfim = tarefa.getDtfimCalculada();
					if(dtfim != null){
						updateDatafim(dtfim, tarefa);
						tarefa.setDtfim(dtfim);
					}
				}
				tarefa.setTarefasituacao(this.verificaSitucaotarefa(tarefa));
				listaNaoSalva.add(tarefa);
			}
		}
		
		if(listaNaoSalva != null && !listaNaoSalva.isEmpty()){
			for(Tarefa tarefa : listaNaoSalva){
				tarefaDAO.updateSituacao(tarefa.getTarefasituacao(), tarefa);
			}
		}
	}
	
	/**
	* M�todo que atualiza o dtfim das tarefas, caso o dtfim seja nulo
	*
	* @since 30/06/2014
	* @author Luiz Fernando
	*/
	public void atualizaDtfimTarefas() {
		List<Tarefa> listaTarefa = this.findForAtualizarDtfimTarefa();
		if(listaTarefa != null && !listaTarefa.isEmpty()){
			for(Tarefa tarefa : listaTarefa){
				if(tarefa.getDtfim() == null){
					Date dtfim = tarefa.getDtfimCalculada();
					if(dtfim != null){
						updateDatafim(dtfim, tarefa);
						tarefa.setDtfim(dtfim);
					}
				}
			}
		}
	}
	
	/**
	* M�todo que verifica e retorna a situa��o da tarefa
	*
	* @param bean
	* @return
	* @since Sep 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tarefasituacao verificaSitucaotarefa(Tarefa bean){		
		Tarefasituacao tarefasituacao = null;
		if(bean.getTarefasituacao() != null && bean.getTarefasituacao().equals(Tarefasituacao.CONCLUIDA)) {
			bean.setTarefasituacao(Tarefasituacao.CONCLUIDA);
			return Tarefasituacao.CONCLUIDA;
		}
		
		Date dataAtual = new Date(System.currentTimeMillis());
		if(bean.getDtinicio() != null && SinedDateUtils.beforeIgnoreHour(dataAtual, bean.getDtinicio())){
			tarefasituacao = Tarefasituacao.EM_ESPERA;				
		} else if(bean.getDtinicio() != null && SinedDateUtils.equalsIgnoreHour(dataAtual, bean.getDtinicio())){
			tarefasituacao = Tarefasituacao.EM_ANDAMENTO;				
		} else if(bean.getDtinicio() != null && SinedDateUtils.afterOrEqualsIgnoreHour(dataAtual, bean.getDtinicio()) && bean.getDtfim() != null &&
				SinedDateUtils.afterOrEqualsIgnoreHour(bean.getDtfim(), dataAtual)){
			tarefasituacao = Tarefasituacao.EM_ANDAMENTO;				
		} else if(bean.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(), dataAtual) ){
			tarefasituacao = Tarefasituacao.ATRASADA;									
		} else {
			tarefasituacao = Tarefasituacao.EM_ESPERA;
		}
		bean.setTarefasituacao(tarefasituacao);
		return tarefasituacao;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#findAllMenosTarefa
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefa> findAllMenosTarefa(Tarefa form) {
		return tarefaDAO.findAllMenosTarefa(form);
	}
	
	/**
	 * Cria o relat�rio comparativo de composi��o.
	 *
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 */
	public IReport createReport(ComparativoIndiceReportFiltro filtro) {		
		Report report = new Report("/projeto/comparativoIndice");
		List<RelComparativoIndice> listaRelBase = this.findForReport(filtro.getPlanejamentobase());
		List<RelComparativoIndice> listaRelFinal = this.findForReport(filtro.getPlanejamentofinal());
		
		boolean achouItem = false;
		for (RelComparativoIndice ci : listaRelBase) {
			if(ci.getQtdeIndice() > 0 && ci.getQtdeRealizada() > 0 && (ci.getQtdeTarefa() / ci.getQtdeRealizada()) > 0){
				ci.setQtdeItemRazao((ci.getQtdeItemPrevista().doubleValue() / ci.getQtdeIndice().doubleValue()) / (ci.getQtdeTarefa().doubleValue() / ci.getQtdeRealizada().doubleValue()));
			} else {
				ci.setQtdeItemRazao(0.0);
			}
			ci.setEncontrado(false);
		}
		
		RelComparativoIndiceSituacao situacao = null;
		for (RelComparativoIndice ci : listaRelFinal) {
			ci.setEncontrado(false);
			if(ci.getQtdeIndice() > 0 && ci.getQtdeRealizada() > 0 && (ci.getQtdeTarefa() / ci.getQtdeRealizada()) > 0){
				ci.setQtdeItemRazao((ci.getQtdeItemPrevista().doubleValue() / ci.getQtdeIndice().doubleValue()) / (ci.getQtdeTarefa().doubleValue() / ci.getQtdeRealizada().doubleValue()));
			} else {
				ci.setQtdeItemRazao(0.0);
			}
			
			if(listaRelBase != null && !listaRelBase.isEmpty()){
				achouItem = false;
				for (RelComparativoIndice ciBase : listaRelBase) {
					if((ciBase.getEncontrado() == null || !ciBase.getEncontrado()) && ciBase.getItem() != null && ciBase.getItem().equals(ci.getItem())){
						achouItem = true;
						ciBase.setEncontrado(true);
						ci.setEncontrado(true);
						
						ci.setQtdeItemPrevistaAnterior(ciBase.getQtdeItemPrevista());
						ci.setQtdeTarefaAnterior(ciBase.getQtdeTarefa());
						ci.setQtdeIndiceAnterior(ciBase.getQtdeIndice());
						ci.setQtdeRealizadaAnterior(ciBase.getQtdeRealizada());
						
						
						if(ci.getQtdeItemPrevista() != null && !ci.getQtdeItemPrevista().equals(ciBase.getQtdeItemPrevista())){
							situacao = RelComparativoIndiceSituacao.ALTERADO;
						}
						
						if(ci.getQtdeTarefa() != null && !ci.getQtdeTarefa().equals(ciBase.getQtdeTarefa())){
							situacao = RelComparativoIndiceSituacao.ALTERADO;
						}
						
						if(situacao == null){
							situacao = RelComparativoIndiceSituacao.MANTIDO;
						}
						
					}
				}
				if(achouItem){
					if(situacao == null){
						situacao = RelComparativoIndiceSituacao.EXCLUIDO;
					}
				}
				ci.setSituacao(situacao);
			}
		}
		
		if(listaRelBase != null && !listaRelBase.isEmpty()){
			for (RelComparativoIndice ciBase : listaRelBase) {
				if(ciBase.getEncontrado() == null || !ciBase.getEncontrado()){
					listaRelFinal.add(ciBase);
				}
			}
		}
		
		report.setDataSource(listaRelFinal);
		return report;
	}

	/**
	 * M�todo para construir o relat�rio de desempenho de Tarefas
	 *
	 * @author Andr� Brunelli
	 * @param filtro
	 * @return
	 */
	public IReport createReportTarefas(ComparativoTarefaReportFiltro filtro) {
		Report report = new Report("/projeto/comparativoTarefas");
		List<RelComparativoTarefas> listaRel = this.findForReportTarefas(filtro);
		
		for (RelComparativoTarefas ct : listaRel) {
			if(ct.getQtdeItemRealizada() > 0){
				ct.setQtdeItemRazao((ct.getQtdeItemRealizada().doubleValue() / ct.getQtdeItemPrevista().doubleValue()) * 100);
			}
		}
		
		report.setDataSource(listaRel);
		return report;
	}
	
	/**
	 * 
	 * M�todo de referencia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#findForReportTarefas(ComparativoTarefaReportFiltro)
	 * @param filtro
	 * @return
	 * @author Andr� Brunelli
	 */
	private List<RelComparativoTarefas> findForReportTarefas(ComparativoTarefaReportFiltro filtro) {
		return tarefaDAO.findForReportTarefas(filtro);
	}
	
	/**
	 * 
	 * Metodo de referencia ao DAO.
	 * Montagem do relatorio
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#findForReport(ComparativoIndiceReportFiltro)
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 */
	public List<RelComparativoIndice> findForReport(Planejamento planejamento){		
		return tarefaDAO.findForReport(planejamento);
	}
	
	/**
	 * M�todo para organizar os ids das tarefas da tela em flex.
	 *
	 * @param listaTarefa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefa> atualizaIdsFlex(List<Tarefa> listaTarefa){
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		atualizaIdListaFlex(listaTarefa, 0, map);
		modificaPredecessora(listaTarefa, map);
		return listaTarefa;
	}
	
	/**
	 * Modifica os idds da predecessora que teve seu id modificado.
	 *
	 * @param listaTarefa
	 * @param map
	 * @author Rodrigo Freitas
	 */
	private void modificaPredecessora(List<Tarefa> listaTarefa, Map<Integer, Integer> map) {
		Integer result = null;
		for (Tarefa tarefa : listaTarefa) {
			if (tarefa.getPredecessoras() != null && !tarefa.getPredecessoras().equals("")) {
				String[] array = tarefa.getPredecessoras().split(";");
				for (int i = 0; i < array.length; i++) {
					result = map.get(Integer.parseInt(array[i]));
					if (result != null) {
						array[i] = result.toString();
					}
				}
				tarefa.setPredecessoras(joinArray(array, ";"));
			}
			if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
				modificaPredecessora(tarefa.getListaTarefaFilha(), map);
			}
		}		
	}
	
	/**
	 * Junta um array de string em apenas uma string com o separador.
	 *
	 * @param array
	 * @param separator
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String joinArray(String[] array, String separator){
		String string = "";
		for (int i = 0; i < array.length; i++) {
			string += array[i] + separator;
		}
		return string.substring(0,string.length() - separator.length());
	}

	/**
	 * Atualiza o id da lista na tela em flex.
	 *
	 * @param listaTarefa
	 * @param id
	 * @param map
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Integer atualizaIdListaFlex(List<Tarefa> listaTarefa, Integer id, Map<Integer, Integer> map){
		for (Tarefa tarefa : listaTarefa) {
			map.put(tarefa.getId(), id);
			tarefa.setId(id);
			id++;
			if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
				id = atualizaIdListaFlex(tarefa.getListaTarefaFilha(), id, map);
			}
		}
		return id;
	}
	
	/**
	 * Carrega as listas de indices e transforma em tarefa para uso na tela em flex.
	 *
	 * @param indice2
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Tarefa carregaListasIndiceFlex(Indice indice2){
		Indice indice = indiceService.findWithListasFlex(indice2);
		
		double num = indice2.getQtde()/indice.getQtde();
		Tarefa tarefa = new Tarefa();
		transformIndiceTarefa(indice, num, tarefa);
		
		return tarefa;
	}
	
	/**
	 * Transforma as lista de RG e RH de �ndice para uma Tarefa.
	 *
	 * @param indice
	 * @param num
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	private void transformIndiceTarefa(Indice indice, double num, Tarefa tarefa) {
		List<Tarefarecursogeral> listaTarefaGeral = new ArrayList<Tarefarecursogeral>();
		Tarefarecursogeral tarefarecursogeral = null;
		if(indice.getListaIndicerecursogeral() != null){
			for (Indicerecursogeral ig : indice.getListaIndicerecursogeral()) {
				tarefarecursogeral = new Tarefarecursogeral();
				if(ig.getMaterial() != null){
					tarefarecursogeral.setTiporecursogeral(Tiporecursogeral.MATERIAL);
					tarefarecursogeral.setMaterial(ig.getMaterial());
				} else {
					tarefarecursogeral.setTiporecursogeral(Tiporecursogeral.OUTRO);
					tarefarecursogeral.setUnidademedida(ig.getUnidademedida());
					tarefarecursogeral.setOutro(ig.getOutro());
				}
				tarefarecursogeral.setQtde(num*ig.getQtde());
				listaTarefaGeral.add(tarefarecursogeral);
			}
		}
		
		List<Tarefarecursohumano> listaTarefaHumano = new ArrayList<Tarefarecursohumano>();
		Tarefarecursohumano tarefarecursohumano = null;
		if (indice.getListaIndicerecursohumano() != null) {
			for (Indicerecursohumano ih : indice.getListaIndicerecursohumano() ) {
				tarefarecursohumano = new Tarefarecursohumano();
				tarefarecursohumano.setCargo(ih.getCargo());
				tarefarecursohumano.setQtde(num*ih.getQtde());
				listaTarefaHumano.add(tarefarecursohumano);
			}
		}
		
		tarefa.setListaTarefarecursogeral(SinedUtil.listToSet(listaTarefaGeral, Tarefarecursogeral.class));
		tarefa.setListaTarefarecursohumano(SinedUtil.listToSet(listaTarefaHumano, Tarefarecursohumano.class));
		if (tarefa.getUnidademedida()==null){
			tarefa.setUnidademedida(indice.getUnidademedida());
		}
	}

	/**
	 * Salvar a lista da tela em flex no BD, e retorna a lista atualizada.
	 *
	 * @param listaTarefa
	 * @param sair
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefa> sincronizaListaFlex(List<Tarefa> listaTarefa, Boolean sair, Planejamento planejamento){
		NeoWeb.getRequestContext().getSession().removeAttribute(WHERENOTIN_TAREFA_SESSION);
		

		if(listaTarefa.get(0).getListaTarefaFilha() != null && listaTarefa.get(0).getListaTarefaFilha().size() > 0){
			atualizaIdsFlex(listaTarefa);
			salvaListaFilha(listaTarefa.get(0).getListaTarefaFilha(), null, planejamento);
		} else {
//			DELETAR TODAS AS TAREFAS QUE EXISTEM DO PLANEJAMENTO
			deleteWhereNotIn("0", planejamento);
		}
	
		
		String string = NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION) != null ? NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION).toString() : null;
		if(string != null && !string.equals("")){
			this.atualizaSituacaoTarefas(string);
			this.deleteWhereNotIn(string, planejamento);
		}
		if(sair != null && sair){
			return null;
		} else {
			return getListaTarefaFlex(planejamento);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#deleteWhereNotIn
	 * @param string
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	private void deleteWhereNotIn(String string, Planejamento planejamento) {
		tarefaDAO.deleteWhereNotIn(string, planejamento);
	}
	
	/**
	 * Salva a lista de tarefas filhas para a tela em flex.
	 *
	 * @param listaTarefaFilha
	 * @param pai
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	private void salvaListaFilha(List<Tarefa> listaTarefaFilha, Tarefa pai, Planejamento planejamento) {
		if(listaTarefaFilha != null){
			String string = null;
			for (Tarefa tarefa : listaTarefaFilha) {
				if (tarefa.getCdtarefa().equals(-1)) {
					tarefa.setCdtarefa(null);
				}
				
				if(tarefa.getCdtarefa() != null){
					Tarefa aux = this.load(tarefa, "tarefa.cdtarefa, tarefa.tarefasituacao");
					if(aux != null && aux.getTarefasituacao() != null){
						tarefa.setTarefasituacao(aux.getTarefasituacao());
					}
				}
				
				tarefa.setPlanejamento(planejamento);
				tarefa.setTarefapai(pai);
				if(tarefa.getListaTarefarecursogeral() != null){
					for (Tarefarecursogeral geral : tarefa.getListaTarefarecursogeral()) {
						if (geral.getCdtarefarecursogeral().equals(0)) {
							geral.setCdtarefarecursogeral(null);
						}
					}
				}
				if(tarefa.getListaTarefarecursohumano() != null){
					for (Tarefarecursohumano humano : tarefa.getListaTarefarecursohumano()) {
						if (humano.getCdtarefarecursohumano().equals(0)) {
							humano.setCdtarefarecursohumano(null);
						}
					}
				}
				saveOrUpdate(tarefa);
				string = NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION) != null ? NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION).toString() : null;
				if (string == null) {
					string = tarefa.getCdtarefa().toString();
				} else {
					string += ","+tarefa.getCdtarefa().toString();
				}
				NeoWeb.getRequestContext().getSession().setAttribute(WHERENOTIN_TAREFA_SESSION, string);
				if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
					salvaListaFilha(tarefa.getListaTarefaFilha(), tarefa, planejamento);
				}
				
			}
		}
	}
	
	/**
	 * Retorna a lista j� formatada para a tela em flex.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefaService#getListaTarefaFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefa> getListaDPFlex(Planejamento planejamento){
		return getListaTarefaFlex(planejamento);
	}
	
	/**
	 * Retorna a lista de tarefas preenchidas.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#preencheListaPlanejamentoRecGeral
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#preencheListaPlanejamentoRecHumano
	 * @see br.com.linkcom.sined.geral.service.TarefaService#getListaFilha
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Tarefa> getListaTarefaFlex(Planejamento planejamento) {
		Tarefa tarefa = new Tarefa();
		tarefa.setCdtarefa(planejamento.getCdplanejamento());
		tarefa.setId(0);
		tarefa.setDescricao(planejamento.getDescricao());
		
		tarefa.setListaTarefaFilha(getArvoreTarefa(planejamento));
		
		Date dataInicio = null;
		Date dataFim = null;
		
		for (Tarefa t : tarefa.getListaTarefaFilha()) {
			t.setTarefapai(tarefa);
			
			if(dataInicio == null || (t.getDtinicio() != null && SinedDateUtils.beforeIgnoreHour(t.getDtinicio(), dataInicio))){
				dataInicio = t.getDtinicio();
			}
			if(dataFim == null || (t.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(dataFim, t.getDtfim()))){
				dataFim = t.getDtfim();
			}
		}
		tarefa.setDtinicio(dataInicio);
		tarefa.setDtfim(dataFim);
		
		Planejamento p1 = planejamentoService.loadWithProjto(planejamento);
		
		Calendario calendario = calendarioService.getByProjeto(p1.getProjeto());
		tarefa.setDuracao(SinedDateUtils.diferencaDias(dataInicio, dataFim, calendario) + 1);
		
//		tarefa.setListaTarefaFilha(getListaFilha(tarefa, planejamento, true));
		
		List<Tarefa> listaDP = new ArrayList<Tarefa>();
		listaDP.add(tarefa);
		
		this.atualizaIdsFlex(listaDP);
				
		return listaDP;
	}
	
	private List<Tarefa> getArvoreTarefa(Planejamento planejamento) {
		
		Projeto projeto = planejamento.getProjeto() != null && planejamento.getProjeto().getCdprojeto() != null ? 
				planejamento.getProjeto() : 
				planejamentoService.loadWithProjto(planejamento).getProjeto();
				
		Calendario calendario = calendarioService.getByProjeto(projeto);
		
		List<Tarefa> listaTodasTarefas = this.findAllTarefas(planejamento);
		for (Tarefa tarefa : listaTodasTarefas) {
			if(tarefa.getDtinicio() != null && tarefa.getDuracao() != null){
				tarefa.setDtfim(SinedDateUtils.incrementaDia(tarefa.getDtinicio(), tarefa.getDuracao()-1, calendario));
			}
			verificaOrigemRecursogeral(tarefa);
		}
		
		List<Tarefa> listaRaiz = this.extractRaiz(listaTodasTarefas);
		
		for (Tarefa tRaiz : listaRaiz) {
			tRaiz.setListaTarefaFilha(this.extractFilhas(tRaiz, listaTodasTarefas));
			if(calendario != null){
				tRaiz.setHaveCalendario(Boolean.TRUE);
			} else {
				tRaiz.setHaveCalendario(Boolean.FALSE);
			}
		}
		
		return listaRaiz;
	}
	
	private List<Tarefa> extractFilhas(Tarefa raiz, List<Tarefa> listaTodasTarefas) {
		List<Tarefa> listaFilhas = new ArrayList<Tarefa>();
		
		for (Tarefa t : listaTodasTarefas) {
			if(t.getTarefapai() != null && t.getTarefapai().equals(raiz)){
				t.setTarefapai(raiz);
				listaFilhas.add(t);
			}
		}
		
		for (Tarefa f : listaFilhas) {
			f.setListaTarefaFilha(this.extractFilhas(f, listaTodasTarefas));
		}
		
		return listaFilhas;
	}
	
	private List<Tarefa> extractRaiz(List<Tarefa> listaTodasTarefas) {
		List<Tarefa> listaRaiz = new ArrayList<Tarefa>();
		for (Tarefa t : listaTodasTarefas) {
			if(t.getTarefapai() == null || t.getTarefapai().getCdtarefa() == null){
				listaRaiz.add(t);
			}
		}
		
		return listaRaiz;
	}
	
	private List<Tarefa> findAllTarefas(Planejamento planejamento) {
		return tarefaDAO.findAllTarefas(planejamento);
	}
	
//	/**
//	 * Carrega a lista de tarefas filhas.
//	 *
//	 * @see br.com.linkcom.sined.geral.service.TarefaService#listaWithPai
//	 * @param tarefa
//	 * @param planejamento
//	 * @param plan
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	private List<Tarefa> getListaFilha(Tarefa tarefa, Planejamento planejamento, Boolean plan) {
//		List<Tarefa> lista = null;
//		if(plan) lista = this.listaWithPai(null, planejamento);
//		else lista = this.listaWithPai(tarefa, planejamento);
//		for (Tarefa t2 : lista) {
//			t2.setTarefapai(tarefa);
//			t2.setListaTarefaFilha(getListaFilha(t2, planejamento, false));
//		}
//		return lista;
//	}
//	
//	/**
//	 * Faz refer�ncia ao DAO.
//	 *
//	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#listaWithPai
//	 * @param tarefa
//	 * @param planejamento
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	private List<Tarefa> listaWithPai(Tarefa tarefa, Planejamento planejamento) {
//		return tarefaDAO.listaWithPai(tarefa, planejamento);
//	}
	
	/**
	 * Monta a hierarquia das tarefas de um determinado planejamento.
	 * A tarefa inicial � sempre o pr�prio planejamento.
	 * 
	 * @see br.com.linkcom.sined.util.SinedDateUtils#calculaDiferencaDias(java.sql.Date, java.sql.Date)
	 * @see br.com.linkcom.sined.geral.service.TarefaService#getListaFilha(Tarefa, Planejamento, Boolean)
	 * 
	 * @param planejamento
	 * @return tarefa
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Tarefa montaHierarquiaCompleta(Planejamento planejamento) {
		Tarefa tarefa = new Tarefa();
		tarefa.setCdtarefa(planejamento.getCdplanejamento());
		tarefa.setId(0);
		tarefa.setDescricao(planejamento.getDescricao());
		tarefa.setDtinicio(planejamento.getDtinicio());
		tarefa.setDtfim(planejamento.getDtfim());
		tarefa.setDuracao(new Long(SinedDateUtils.calculaDiferencaDias(planejamento.getDtinicio(), planejamento.getDtfim())+1).intValue());
		tarefa.setListaTarefaFilha(getArvoreTarefa(planejamento));
//		tarefa.setListaTarefaFilha(getListaFilha(tarefa, planejamento, true));
		return tarefa;
	}
	
	/**
	 * Retorna uma lista de tarefas folhas de um determinado planejamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaService#montaHierarquiaCompleta(Planejamento)
	 * @see br.com.linkcom.sined.geral.service.TarefaService#findFolhas(Tarefa, List)
	 * 
	 * @param planejamento
	 * @return lista de Tarefa
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Tarefa> findTarefasFolha(Planejamento planejamento) {
		Tarefa tarefaInicial = montaHierarquiaCompleta(planejamento);
		List<Tarefa> listaTarefaFolha = new ArrayList<Tarefa>();
		
		findFolhas(tarefaInicial, listaTarefaFolha);
		
		return listaTarefaFolha;
	}
	
	/**
	 * M�todo recursivo que percorre as tarefas filhas de uma tarefa.
	 * Cada tarefa filha que n�o possuir nenhum filho, esta � adicionada no par�metro listaTarefaFolha
	 * 
	 * @param listaTarefaFolha
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void findFolhas(Tarefa tarefa, List<Tarefa> listaTarefaFolha) {
		Boolean temFihos = false;
		
		if (tarefa != null) {
			if (tarefa.getListaTarefaFilha() != null) {
				for (Tarefa tarefaFilha : tarefa.getListaTarefaFilha()) {
					findFolhas(tarefaFilha, listaTarefaFolha);
					temFihos = true;
				}
			}
		}
		if (!temFihos) {
			listaTarefaFolha.add(tarefa);
		}
	}
	
	/**
	 * Dada uma lista de tarefas, calcula o peso de cada uma baseado na quantidade de dias de dura��o.
	 * 
	 * @param listaTarefa
	 * @return listaTarefa com o campo peso preenchido
	 * @throws SinedException - caso alguma das tarefas n�o possua dura��o
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Tarefa> calculaPesoTarefa(List<Tarefa> listaTarefa) {
		Integer duracaoTotal = 0;
		
		if (listaTarefa != null) {
			//Soma a dura��o de todas as tarefas
			for (Tarefa tarefa : listaTarefa) {
				if (tarefa.getDuracao() == null) {
					throw new SinedException("N�o pode haver tarefa com dura��o nula no c�lculo de peso das tarefas.");
				}
				duracaoTotal += tarefa.getDuracao();
			}
			//Obt�m-se o peso de cada tarefa baseado na dura��o da mesma em rela��o � dura��o total
			//O peso varia de 0 a 1
			for (Tarefa tarefa : listaTarefa) {
				tarefa.setPeso(tarefa.getDuracao().doubleValue() / duracaoTotal);
			}			
		}
		
		return listaTarefa;
	}		
	
	/**
	 * Executa um save or update, <b>sem transaction</b>, para cada item da lista.
	 * 
	 * @see #saveOrUpdateNoUseTransaction(Tarefa)
	 * @param listaTarefa
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateListNoUseTransaction(Collection<Tarefa> listaTarefa){
		if(SinedUtil.isListNotEmpty(listaTarefa)){
			for (Tarefa tarefa : listaTarefa) {
				this.saveOrUpdateNoUseTransaction(tarefa);
			}
		}
	}
	
	/**
	 * M�todo que cria arquivo CSV de tarefa para ser importado no project. Cria j� formatado
	 * 
	 * @param tarefa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource preparaArquivoTarefaCSV(String whereIn) {
		List<Tarefa> lista = tarefaDAO.carregaTarefas(whereIn);
		if(lista == null || lista.size() == 0)
			throw new SinedException("N�o existe nenhuma tarefa com o filtro especificado.");
		
		StringBuilder csv = new StringBuilder();
		int i = 1;
		for (Tarefa tarefa : lista) {
			if(i == 1)
				csv.append("Projeto;Nome;Dura��o;In�cio;T�rmino;Predecessoras;N�vel da estrutura de t�picos");
			
			/*Se tiver pai procura o pai selecionado para cria nivel de estrutura se nao achar
			 ele adiciona na lista como se fosse um pai*/
			if(tarefa.getTarefapai() != null && tarefa.getTarefapai().getCdtarefa() != null){
				if(lista.contains(tarefa.getTarefapai())){
					for (Tarefa tarefaPai : lista) {
						if(tarefaPai.equals(tarefa.getTarefapai())){
							tarefaPai.setNivelEstrutura(tarefaPai.getNivelEstrutura()+1);
							tarefa.setNivelEstrutura(tarefaPai.getNivelEstrutura());
							adicionaRowArquivoCSV(csv, tarefa);
							break;
						}
					}
					continue;
				} 
			}
			adicionaRowArquivoCSV(csv, tarefa);
			
			i++;
		}
		
		Resource resource = new Resource("text/csv", "exportartarefa_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	
	/**
	 * Adiciona os dados de uma fileira do arquivo CSV para o project
	 * 
	 * @param csv
	 * @param tarefa
	 * @author Tom�s Rabelo
	 */
	private void adicionaRowArquivoCSV(StringBuilder csv, Tarefa tarefa) {
		csv.append("\n");
		csv.append(tarefa.getPlanejamento().getProjeto().getNome()+";");
		csv.append(tarefa.getDescricao()+";");
		csv.append(tarefa.getDuracao()+";");
		csv.append(tarefa.getDtinicio() != null ? new SimpleDateFormat("dd/MM/yyyy").format(tarefa.getDtinicio())+";" : ";");
		csv.append(tarefa.getDtfim() != null ? new SimpleDateFormat("dd/MM/yyyy").format(tarefa.getDtfim())+";" : ";");
		csv.append(tarefa.getPredecessoras() != null ? tarefa.getPredecessoras()+";" : ";");
		csv.append(tarefa.getNivelEstrutura());
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param tarefa
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Double getQtdePrevistaTarefa(Tarefa tarefa) {
		return tarefaDAO.getQtdePrevistaTarefa(tarefa);
	}
	
	/**
	 * M�todo que cria relat�rio Excel da acompanhamento de tarefas
	 * 
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Tom�s Rabelo
	 */
	public Resource preparaArquivoAcompanhamentoTarefaExcel(AcompanhamentoTarefaFiltro filtro) throws IOException{
		List<Tarefa> tarefas = findAllTarefas(filtro.getProjeto(), filtro.getPlanejamento());
		Projeto projeto = projetoService.load(filtro.getProjeto());
		Planejamento planejamento = planejamentoService.load(filtro.getPlanejamento());
		this.atualizaIdsFlex(tarefas);
		
		Calendario calendario = calendarioService.getByProjeto(filtro.getProjeto());
		
		SinedExcel wb = new SinedExcel();
		if(tarefas != null && !tarefas.isEmpty()){
			for (Tarefa tarefa : tarefas) {
				if(calendario != null)
					tarefa.setHaveCalendario(Boolean.TRUE);
				else 
					tarefa.setHaveCalendario(Boolean.FALSE);
			}
			
			
			HSSFSheet planilha = wb.createSheet("Planilha");
	        planilha.setDefaultColumnWidth((short) 20);
	        
			HSSFRow headerRow = planilha.createRow((short) 0);
			planilha.addMergedRegion(new Region(0, (short) 0, 3, (short) 6));
			
			HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
			cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
			cellHeaderTitle.setCellValue("Relat�rio de acompanhamento de tarefas" + "  -  " + "Projeto: " +projeto.getNome() + "\nPlanejamento: " + planejamento.getDescricao());
			
	        HSSFRow row = null;
			HSSFCell cell = null;
			
			planilha.createRow(5);
			row = planilha.createRow(4);
			
			planilha.addMergedRegion(new Region(4, (short) 0, 5, (short) 0));
			SinedExcel.createHeaderCell(row, 0, "Id");
			planilha.addMergedRegion(new Region(4, (short) 1, 5, (short) 1));
			SinedExcel.createHeaderCell(row, 1, "Tarefas");
			planilha.addMergedRegion(new Region(4, (short) 2, 5, (short) 2));
			SinedExcel.createHeaderCell(row, 2, "U.M.");
			planilha.addMergedRegion(new Region(4, (short) 3, 5, (short) 3));
			SinedExcel.createHeaderCell(row, 3, "Qtde.");
			planilha.addMergedRegion(new Region(4, (short) 4, 5, (short) 4));
			SinedExcel.createHeaderCell(row, 4, "Pre�o total");
			
			//Adiciona as bordas nos cabe�alhos
			row = planilha.getRow(5);
			SinedExcel.createHeaderCell(row, 0, "");
			SinedExcel.createHeaderCell(row, 1, "");
			SinedExcel.createHeaderCell(row, 2, "");
			SinedExcel.createHeaderCell(row, 3, "");
			SinedExcel.createHeaderCell(row, 4, "");
			
			//Busca maiores e menores datas das tarefas e cria o cabe�alho din�mico
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
			Date menorDataTarefa = getMenorDataTarefaPaiProjetoPlanejamento(filtro.getProjeto(), filtro.getPlanejamento());
			Date maiorDataTarefa = getMaiorDataTarefaPaiProjetoPlanejamento(filtro.getProjeto(), filtro.getPlanejamento());
			Date menorDataProducaoDiaria = producaodiariaService.getMenorDataProducaoDiariaProjetoPlanejamento(filtro.getProjeto(), filtro.getPlanejamento());
			Date maiorDataProducaoDiaria = producaodiariaService.getMaiorDataProducaoDiariaProjetoPlanejamento(filtro.getProjeto(), filtro.getPlanejamento());

			Calendar dtMenorAux = Calendar.getInstance();
			Calendar dtMaiorAux = Calendar.getInstance();
			Calendar dtProducao = Calendar.getInstance();
			Calendar dtPaiInicio = Calendar.getInstance();
			Calendar dtPaiFim = Calendar.getInstance();
			
			if(menorDataTarefa == null) throw new SinedException("Nenhuma tarefa cadastrada para este planejamento.");
			
			if(menorDataProducaoDiaria != null && menorDataProducaoDiaria.before(menorDataTarefa)){
				dtMenorAux.setTimeInMillis(menorDataProducaoDiaria.getTime());
			}else{
				dtMenorAux.setTimeInMillis(menorDataTarefa.getTime());
			}

			if(maiorDataProducaoDiaria != null && maiorDataProducaoDiaria.after(maiorDataTarefa)){
				dtMaiorAux.setTimeInMillis(maiorDataProducaoDiaria.getTime());
			}else{
				dtMaiorAux.setTimeInMillis(maiorDataTarefa.getTime());
			}
			dtMaiorAux.set(Calendar.DAY_OF_MONTH, 1);
			dtMenorAux.set(Calendar.DAY_OF_MONTH, 1);
			int meses = SinedDateUtils.diferencaMeses(new Date(dtMaiorAux.getTimeInMillis()), new Date(dtMenorAux.getTimeInMillis()))+2;
			int iCelula = 0;
			Calendar dtMenorCalAux = Calendar.getInstance();
			dtMenorCalAux.setTimeInMillis(dtMenorAux.getTimeInMillis());
			for (int i = 1; i < meses; i++) {
				row = planilha.getRow(4);
				iCelula = i*2;
				planilha.addMergedRegion(new Region(4, (short) (3+iCelula), 4, (short) (4+iCelula)));
				cell = row.createCell((short) (3+iCelula));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(dateFormat.format(dtMenorCalAux.getTime()));
				//Adiciona bordar
				cell = row.createCell((short) (4+iCelula));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				
				row = planilha.getRow(5);
				cell = row.createCell((short) (3+iCelula));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue("Previsto");
				cell = row.createCell((short) (4+iCelula));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue("Realizado");
				dtMenorCalAux.add(Calendar.MONTH, 1);
			}
			
			//Cabe�alho totalizador Resumo total
			row = planilha.getRow(4);
			planilha.addMergedRegion(new Region(4, (short) (row.getLastCellNum()+1), 4, (short) (row.getLastCellNum()+2)));
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "Resumo total");
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "");
			
			row = planilha.getRow(5);
			cell = row.createCell((short) (row.getLastCellNum()+1));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue("Previsto");
			cell = row.createCell((short) (row.getLastCellNum()+1));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue("Realizado");
			
			/*
			 * Come�o l�gica. Consiste de achar os pais raizes. 
			 * Chama o m�todo recursivo adicionaSubTarefas at� chegar nas folhas
			 */ 
			Integer rowPai = 0;
			List<Integer> listaPosicaoPais = new ArrayList<Integer>();
			for (Tarefa tarefaPai : tarefas) {
				if(tarefaPai.getTarefapai() == null || tarefaPai.getTarefapai().getCdtarefa() == null){
					rowPai = planilha.getLastRowNum()+1;
					row = planilha.createRow(rowPai);
					tarefaPai.setRowPai(rowPai);
					listaPosicaoPais.add(rowPai);
					
					cell = row.createCell((short) 0);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
					cell.setCellValue(tarefaPai.getId() != null ? tarefaPai.getId() : 0);
					cell = row.createCell((short) 1);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					cell.setCellValue(tarefaPai.getDescricao());
					cell = row.createCell((short) 2);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					cell.setCellValue(tarefaPai.getUnidademedida() != null ? tarefaPai.getUnidademedida().getSimbolo() : "");
					cell = row.createCell((short) 3);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
					cell.setCellValue(tarefaPai.getQtde());
					
					dtPaiInicio.setTime(tarefaPai.getDtinicio());
					dtPaiFim.setTime(tarefaPai.getDtinicio());
					dtPaiFim.add(Calendar.DAY_OF_MONTH, tarefaPai.getDuracao());
					
					adicionaSubTarefas(tarefas, tarefaPai, planilha, row, cell, meses, dtMenorAux, menorDataTarefa, dtProducao);
					
					row = planilha.getRow(rowPai);
					
					boolean existFilhos = existFilhos(tarefas, tarefaPai);
					//Custo total = 0 caso n�o tenha filhos 
					if(row.getCell((short) 4) == null && existFilhos){
						cell = row.createCell((short) 4);
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
						cell.setCellValue(0.0);	
						
						//Totalizador Raiz
						StringBuilder totalPrevistos = new StringBuilder("");
						StringBuilder totalRealizados = new StringBuilder("");
						for (int i = 1; i < meses*2-1; i++) {
							//Previsto e Realizado
							if(row.getCell((short) (4+i)) == null){
								cell = row.createCell((short) (4+i));
								cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
								cell.setCellValue(0.0);	
							}else{
								cell = row.getCell((short) (4+i));
								cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
								if(i % 2 == 0)
									totalRealizados.append(SinedExcel.getAlgarismoColuna(4+i)+(rowPai+1)).append(";");
								else
									totalPrevistos.append(SinedExcel.getAlgarismoColuna(4+i)+(rowPai+1)).append(";");
							}
						}
						
						cell = row.createCell((short)(row.getLastCellNum()+1));
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
						if(totalPrevistos.length() > 0){
							cell.setCellFormula("AVERAGE("+totalPrevistos.delete(totalPrevistos.length()-1, totalPrevistos.length()).toString()+")");
						}else{
							cell.setCellValue(0.0);
						}
						
						cell = row.createCell((short)(row.getLastCellNum()+1));
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
						if(totalRealizados.length() > 0){
							cell.setCellFormula("AVERAGE("+totalRealizados.delete(totalRealizados.length()-1, totalRealizados.length()).toString()+")");
						}else{
							cell.setCellValue(0.0);
						}
					}else if(!existFilhos){
						Calendar dtTarefaInicio = Calendar.getInstance();
						if(tarefaPai.getDtinicio() != null){
							dtTarefaInicio.setTime(tarefaPai.getDtinicio());	
						}else{
							throw new SinedException("Existem tarefas sem data de in�cio. Esse campo � obrigat�rio.");
						}
						
						Calendar dtTarefaFim = Calendar.getInstance();
						dtTarefaFim.setTime(tarefaPai.getDtinicio());
						dtTarefaFim.add(Calendar.DAY_OF_MONTH, tarefaPai.getDuracao());
						
						cell = row.createCell((short) (0));
						cell.setCellValue(tarefaPai.getId());
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
						cell = row.createCell((short) (1));
						cell.setCellValue(tarefaPai.getDescricao());
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
						cell = row.createCell((short) (2));
						cell.setCellValue(tarefaPai.getUnidademedida() != null ? tarefaPai.getUnidademedida().getSimbolo() : "");
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
						cell = row.createCell((short) (3));
						cell.setCellValue(tarefaPai.getQtde());
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
						cell = row.createCell((short) (4));
						cell.setCellValue(tarefaPai.getCustototal() != null ? tarefaPai.getCustototal() : 0.0);
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
						
						adicionaInfExcel(cell, dtMenorAux, meses, planilha, row, rowPai, tarefaPai, menorDataTarefa, dtProducao, dtTarefaInicio, dtTarefaFim, null);
					}
					
					row = planilha.getRow(rowPai);
					
					
					
				}
			}
			
			//Cria totalizadores inferiores
			row = planilha.createRow(planilha.getLastRowNum()+1);
			planilha.addMergedRegion(new Region((planilha.getLastRowNum()), (short) 0, (planilha.getLastRowNum()), (short) 3));
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
			cell.setCellValue("Total R$");
			SinedExcel.createHeaderCell(row, 1, "");
			SinedExcel.createHeaderCell(row, 2, "");
			SinedExcel.createHeaderCell(row, 3, "");
			SinedExcel.createHeaderCell(row, 4, "");
			
			StringBuilder somaPais = new StringBuilder("AVERAGE(");
			for (Integer posicaoPai : listaPosicaoPais) 
				somaPais.append(SinedExcel.getAlgarismoColuna(4)+(posicaoPai+1)).append(";");
			cell = row.createCell((short) 4);
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
			if(!somaPais.toString().equals("AVERAGE(")){
				cell.setCellFormula(somaPais.substring(0, somaPais.length()-1)+")");
			}else{
				cell.setCellValue(0.0);
			}

			//Cria totalizador inferior percentual
			row = planilha.createRow(planilha.getLastRowNum()+1);
			planilha.addMergedRegion(new Region((planilha.getLastRowNum()), (short) 0, (planilha.getLastRowNum()), (short) 4));
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
			cell.setCellValue("Total em percentual");
			for (int i = 0; i < meses*2+1; i++) {
				somaPais = new StringBuilder("AVERAGE(");
				for (Integer posicaoMeses : listaPosicaoPais) 
					somaPais.append(SinedExcel.getAlgarismoColuna(4+i)+(posicaoMeses+1)).append(";");
				
				cell = row.createCell((short) (4+i));
				cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
				if(!somaPais.toString().equals("AVERAGE(")){
					cell.setCellFormula(somaPais.substring(0, somaPais.length()-1)+")");
				}else{
					cell.setCellValue(0.0);
				}
			}
			SinedExcel.createHeaderCell(row, 1, "");
			SinedExcel.createHeaderCell(row, 2, "");
			SinedExcel.createHeaderCell(row, 3, "");
			SinedExcel.createHeaderCell(row, 4, "");
		}
		
		return wb.getWorkBookResource("acompanhamentotarefa.xls");
	}
	
	/**
	 * M�todo que verifica se a tarefa cont�m filhos
	 *
	 * @param tarefas
	 * @param tarefaPai
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean existFilhos(List<Tarefa> tarefas, Tarefa tarefaPai) {
		for (Tarefa tarefaFilha : tarefas) {
			if(tarefaFilha.getTarefapai() != null && tarefaFilha.getTarefapai().getCdtarefa() != null && tarefaFilha.getTarefapai().equals(tarefaPai)){
				return true;
			}
		}
		return false;
	}
	/**
	 * M�todo recursivo � chamado at� chegar nas folhas da �rvore. Quando chegar no final volta criando totalizadores dos pais
	 * 
	 * @param tarefas
	 * @param tarefaPai
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param meses
	 * @param dtMenorAux
	 * @param menorData
	 * @param dtProducao
	 * @auhtor Tom�s Rabelo
	 */
	private void adicionaSubTarefas(List<Tarefa> tarefas, Tarefa tarefaPai, HSSFSheet planilha, HSSFRow row, HSSFCell cell, int meses, Calendar dtMenorAux, Date menorData, Calendar dtProducao) {
		Integer rowPai = 0;
		for (Tarefa tarefaFilha : tarefas) {
			if(tarefaFilha.getTarefapai() != null && tarefaFilha.getTarefapai().getCdtarefa() != null && tarefaFilha.getTarefapai().equals(tarefaPai)){
				rowPai = planilha.getLastRowNum()+1;
				row = planilha.createRow(rowPai);
				tarefaFilha.setRowPai(rowPai);
				
				Calendar dtTarefaInicio = Calendar.getInstance();
				if(tarefaFilha.getDtinicio() != null){
					dtTarefaInicio.setTime(tarefaFilha.getDtinicio());	
				}else{
					throw new SinedException("Existem tarefas sem data de in�cio. Esse campo � obrigat�rio.");
				}
				
				Calendar dtTarefaFim = Calendar.getInstance();
				dtTarefaFim.setTime(tarefaFilha.getDtinicio());
				dtTarefaFim.add(Calendar.DAY_OF_MONTH, tarefaFilha.getDuracao());
				
				cell = row.createCell((short) (0));
				cell.setCellValue(tarefaFilha.getId());
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell = row.createCell((short) (1));
				cell.setCellValue(tarefaFilha.getDescricao());
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
				cell = row.createCell((short) (2));
				cell.setCellValue(tarefaFilha.getUnidademedida() != null ? tarefaFilha.getUnidademedida().getSimbolo() : "");
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
				cell = row.createCell((short) (3));
				cell.setCellValue(tarefaFilha.getQtde());
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell = row.createCell((short) (4));
				cell.setCellValue(tarefaFilha.getCustototal() != null ? tarefaFilha.getCustototal() : 0.0);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);

				//Chamada recursiva
				adicionaSubTarefas(tarefas, tarefaFilha, planilha, row, cell, meses, dtMenorAux, menorData, dtProducao);
				
				adicionaInfExcel(cell, dtMenorAux, meses, planilha, row, rowPai, tarefaFilha, menorData, dtProducao, dtTarefaInicio, dtTarefaFim, tarefaPai);
			}
		}
	}
	
	/**
	 * M�todo que adiciona informa��es da tarefa filho e da tarefa que n�o tem filhos
	 *
	 * @param cell
	 * @param dtMenorAux
	 * @param meses
	 * @param planilha
	 * @param row
	 * @param rowPai
	 * @param tarefaFilha
	 * @param menorData
	 * @param dtProducao
	 * @param dtTarefaInicio
	 * @param dtTarefaFim
	 * @param tarefaPai
	 * @author Luiz Fernando
	 */
	private void adicionaInfExcel(HSSFCell cell, Calendar dtMenorAux, int meses, HSSFSheet planilha, HSSFRow row, Integer rowPai, Tarefa tarefaFilha, Date menorData, Calendar dtProducao, Calendar dtTarefaInicio, Calendar dtTarefaFim, Tarefa tarefaPai){
		//Totalizadores das linhas. Guardam as refer�ncias das colunas
		StringBuilder totalPrevistos = new StringBuilder("");
		StringBuilder totalRealizados = new StringBuilder("");
		Calendar dtMenorCalcAux = Calendar.getInstance();
		dtMenorCalcAux.setTimeInMillis(dtMenorAux.getTimeInMillis());
		for (int i = 0; i < meses*2-2; i++) {
			row = planilha.getRow(rowPai);
			double realizado = 0.0;
			
			//Se for a vez de realizado e tiver lan�amento de produ��o realiza o c�lculo
			if(i % 2 != 0 && (tarefaFilha.getListaProducaodiaria() != null || !tarefaFilha.getListaProducaodiaria().isEmpty())){
				for (Producaodiaria producaodiaria : tarefaFilha.getListaProducaodiaria()) {
					dtProducao.setTimeInMillis(producaodiaria.getDtproducao().getTime());
					if(dtMenorCalcAux.get(Calendar.MONTH) == dtProducao.get(Calendar.MONTH) && 
					   dtMenorCalcAux.get(Calendar.YEAR) == dtProducao.get(Calendar.YEAR)){
							realizado += producaodiaria.getQtde();
					}
				}
			}
			
			dtMenorCalcAux.set(Calendar.DAY_OF_MONTH, 1);
			dtTarefaInicio.set(Calendar.DAY_OF_MONTH, 1);
			dtTarefaFim.set(Calendar.DAY_OF_MONTH, 1);
			//Adiciona os valores de previsto e realizado
			if(row.getCell((short) (5+i)) == null){
				cell = row.createCell((short) (5+i));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
				if(i % 2 == 0){
					if(dtMenorCalcAux.getTimeInMillis() >= dtTarefaInicio.getTimeInMillis() && dtMenorCalcAux.getTimeInMillis() <= dtTarefaFim.getTimeInMillis()){
						if(tarefaFilha.getQtde() != null && tarefaFilha.getQtde() > 0){
							cell.setCellValue(SinedUtil.round(tarefaFilha.getQtdMensal() / tarefaFilha.getQtde() , 2));
						}else{
							cell.setCellValue(0.0);
						}
					}
					else
						cell.setCellValue(0.0);
				}else{
					cell.setCellValue(realizado == 0.0 ? 0.0 : (SinedUtil.round(realizado / tarefaFilha.getQtdMensal(), 2)));
					dtMenorCalcAux.add(Calendar.MONTH, 1);
				}
			}
			
			if(i % 2 == 0)
				totalPrevistos.append(SinedExcel.getAlgarismoColuna(5+i)+(rowPai+1)).append(";");
			else
				totalRealizados.append(SinedExcel.getAlgarismoColuna(5+i)+(rowPai+1)).append(";");
			
			//Recupera fileira do paiz�o e adiciona m�dia	
			if(tarefaPai != null){
				row = planilha.getRow(tarefaPai.getRowPai());
				
				cell = row.getCell((short) (5+i));
				if(cell == null){
					cell = row.createCell((short) (5+i));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
					cell.setCellFormula("AVERAGE("+SinedExcel.getAlgarismoColuna(5+i)+(rowPai+1)+")");
				}else{
					String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(5+i)+(rowPai+1)+")";
					cell = row.createCell((short) (5+i));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
					cell.setCellFormula(formula);
				}
			}
		}

		//Totalizador da fileira
		row = planilha.getRow(rowPai);
		cell = row.createCell((short)(row.getLastCellNum()+1));
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		if(totalPrevistos.length() > 0){
			cell.setCellFormula("AVERAGE("+totalPrevistos.delete(totalPrevistos.length()-1, totalPrevistos.length()).toString()+")");
		}else{
			cell.setCellValue(0.0);
		}
		
		cell = row.createCell((short)(row.getLastCellNum()+1));
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		if(totalRealizados.length() > 0){
			cell.setCellFormula("AVERAGE("+totalRealizados.delete(totalRealizados.length()-1, totalRealizados.length()).toString()+")");
		}else{
			cell.setCellValue(0.0);
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Tarefa> loadTarefasFilhas(Projeto projeto, Planejamento planejamento) {
		return tarefaDAO.loadTarefasFilhas(projeto, planejamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 */
	public List<Tarefa> findAllTarefas(Projeto projeto, Planejamento planejamento) {
		return tarefaDAO.findAllTarefas(projeto, planejamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMenorDataTarefaPaiProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return tarefaDAO.getMenorDataTarefaPaiProjetoPlanejamento(projeto, planejamento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMaiorDataTarefaPaiProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return tarefaDAO.getMaiorDataTarefaPaiProjetoPlanejamento(projeto, planejamento);
	}
	
	public Solicitacaocompra gerarSolicitacaocompra(Solicitacaocompra form, String whereIn) {
		List<Tarefarecursogeral> listaRG = tarefarecursogeralService.findByTarefa(whereIn);
		List<Solicitacaocompra> listaSolicitacao = new ArrayList<Solicitacaocompra>();
		Solicitacaocompra sol = null;
		
		for (Tarefarecursogeral tg : listaRG) {
			sol = new Solicitacaocompra();
			
			sol.setMaterial(tg.getMaterial());
			sol.setQtde(new Double(tg.getQtde()));
			sol.setDtlimite(tg.getTarefa().getDtinicio());
			sol.setTarefa(tg.getTarefa());
			sol.setCdtarefarecursogeral(tg.getCdtarefarecursogeral());
			
			sol.setGerado(tg.getListaSolicitacaocompraorigem() != null && tg.getListaSolicitacaocompraorigem().size() > 0);
			
			listaSolicitacao.add(sol);
		}	
		
		form.setSolicitacoes(listaSolicitacao);
		return form; 
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* carrega a lista de tarefa com a situa��o de acordo com os par�metros passados
	* 
	* @see	br.com.linkcom.sined.geral.dao.TarefaDAO#findSituacaoForTarefa(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findSituacaoForTarefa(String whereIn) {
		return tarefaDAO.findSituacaoForTarefa(whereIn);
	}
	
	/**
	* M�todo com refer�ncia ao DAO 
	* Atualiza a situa��o da Tarefa
	*
	* @see	br.com.linkcom.sined.geral.dao.TarefaDAO#updateSituacao(Tarefasituacao tarefasituacao, Tarefa tarefa)
	*
	* @param tarefasituacao
	* @param tarefa
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateSituacao(Tarefasituacao tarefasituacao, Tarefa tarefa){
		tarefaDAO.updateSituacao(tarefasituacao, tarefa);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Carrega a lista de tarefa com a situa��o
	*
	* @see	br.com.linkcom.sined.geral.dao.TarefaDAO#findSituacaoTodasTarefas()
	*
	* @return
	* @since Aug 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findSituacaoTodasTarefas(){
		return tarefaDAO.findSituacaoTodasTarefas();
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Retorna a lista de Tarefas que tem o mesmo colaborador alocado na mesma data de acordo com os par�metros
	* 
	* @see br.com.linkcom.sined.geral.dao.TarefaDAO#findTarefasConflitantesDataColaborador(Colaborador colaborador)
	* 
	* @return
	* @since Aug 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findTarefasConflitantesDataColaborador(Colaborador colaborador, Date dtinicio){
		return tarefaDAO.findTarefasConflitantesDataColaborador(colaborador, dtinicio);
	}
	
	/**
	* M�todo que busca as tarefas para verificar apontamentos n�o lan�ados por colaboradores
	* retorna uma lista de Apontamentotarefaatraso (bean auxiliar)
	*
	* @return
	* @since Aug 16, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Apontamentotarefaatraso> buscaTarefaVerificarAtrasoApontamento(){
		return tarefaDAO.buscaTarefaVerificarAtrasoApontamento();
	}
	
	/**
	 * 
	 * M�todo que cria o report de checklist.
	 *
	 * @name createReportTarefaChecklist
	 * @param filtro
	 * @return
	 * @return IReport
	 * @author Thiago Augusto
	 * @date 25/06/2012
	 *
	 */
	public IReport createReportTarefaChecklist(String whereIn){
		Report report = new Report("/projeto/checklist");
		List<Tarefa> listaTarefas = this.findListTarefaForChecklistReport(whereIn);
		for (Tarefa tarefa : listaTarefas) {
			if (tarefa.getDtinicio() != null && tarefa.getDuracao() != null && !tarefa.getDuracao().equals("")){
				tarefa.setDtfim(SinedDateUtils.addDiasData(tarefa.getDtinicio(), tarefa.getDuracao()));
			}
			tarefa.setListaTarefarequisicao(tarefarequisicaoService.findByTarefa(tarefa));
			if (tarefa.getListaTarefarequisicao() != null && !tarefa.getListaTarefarequisicao().isEmpty()){
				tarefa.setColaboradorResponsavel(SinedUtil.listAndConcatenate(tarefa.getListaTarefarequisicao(), "requisicao.colaboradorresponsavel.nome", ", "));
			} else {
				tarefa.setColaboradorResponsavel("");
			}
		}
		report.setDataSource(listaTarefas);
		return report;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListTarefaForChecklistReport
	 * @param filtro
	 * @return
	 * @return List<Tarefa>
	 * @author Thiago Augusto
	 * @date 25/06/2012
	 *
	 */
	public List<Tarefa> findListTarefaForChecklistReport(String whereIn){
		return tarefaDAO.findListTarefaForChecklistReport(whereIn);
	}
	
	public void updateDatainicio(Date data, Tarefa tarefa) {
		tarefaDAO.updateDatainicio(data, tarefa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TarefaDAO#updateDatafim(Date data, Tarefa tarefa) 
	* 
	* @param data
	* @param tarefa
	* @since 26/06/2014
	* @author Luiz Fernando
	*/
	public void updateDatafim(Date data, Tarefa tarefa) {
		tarefaDAO.updateDatafim(data, tarefa);
	}
	
	/**
	 * 
	 * @param bytes
	 * @author Thiago Clemente
	 * @throws Exception
	 * 
	 */
	public List<ImportarTarefasPlanejamentoBean> validarImportarTarefas(byte[] bytes) throws Exception {
		 
		List<ImportarTarefasPlanejamentoBean> listaBean = new ArrayList<ImportarTarefasPlanejamentoBean>();
		ImportarTarefasPlanejamentoBean bean;
		
		String linhaStr;
		int linha = 1;
		
		BufferedReader bufferedReader = new BufferedReader(new StringReader(new String(bytes, "LATIN1")));
//		Pattern pattern = Pattern.compile("(.*;)(.*;)(.*;)(.*;)(.*;)(.*;?)");
		
		while ((linhaStr=bufferedReader.readLine())!=null){
			
			if (linha>1 && !linhaStr.trim().equals("")){
				
				
				bean = new ImportarTarefasPlanejamentoBean();
				
				String[] linhaSplit = linhaStr.split("\\;");
				
				if (linhaSplit.length<6){
					linhaSplit = Arrays.copyOf(linhaSplit, 6);
				}else if (linhaSplit.length>7){
					throw new SinedException("Total de colunas inv�lidas. Linha: " + linha);
				}				
				
				try {
					bean.setId(new Integer(linhaSplit[0]));
				}catch (Exception e) {
					throw new SinedException("Identificador inv�lido. Linha: " + linha);
				}
				
				try {
					bean.setIdPai(new Integer(linhaSplit[1]));
				}catch (Exception e) {
				}
				
				bean.setDescricao(linhaSplit[2]);
				bean.setUnidade(linhaSplit[3]);
				
				try {
					bean.setQtde(new Double(linhaSplit[4]));
				}catch (Exception e) {
					throw new SinedException("Quantidade � obrigat�ria. Linha: " + linha);
				}
				
				bean.setComposicao(linhaSplit[5]);
				
				//Valida��es
				if ((linha-1)==1 && bean.getId()!=1){
					throw new SinedException("Identificador da primeira tarefa tem que ser 1.");
				}
				
				if ((linha-1)!=bean.getId()){
					throw new SinedException("O Identificador tem que ser incremental. Linha: " + linha);
				}
				
				if (bean.getIdPai()!=null && bean.getId()==bean.getIdPai()){
					throw new SinedException("O Identificador da tarefa pai n�o pode ser o pr�prio Identificador da tarefa.");
				}
				
				if (bean.getDescricao()==null || bean.getDescricao().trim().equals("")){
					throw new SinedException("Descri��o � obrigat�rio. Linha: " + linha);
				}
				
				listaBean.add(bean);
			}
			
			linha++;
		}
		
		return listaBean;
	}
	
	/**
	 * Valida o arquivo para ver se tem relacionamento de pai recursivo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#verificaPai
	 * @param lista
	 * @author Rodrigo Freitas
	 */
	public void validarArquivoImportarTarefas(List<ImportarTarefasPlanejamentoBean> listaBean) {
		for (ImportarTarefasPlanejamentoBean bean: listaBean) {
			if (bean.getIdPai()!=null && verificarPaiImportarTarefas(bean.getId(), bean.getIdPai(), listaBean)){
				throw new SinedException("Relacionamento de tarefa pai recursivo.");
			}
		}
	}
	
	/**
	 * Verifica se a tarefa � de um relacionamento recursivo.
	 *
	 * @param id1
	 * @param id2
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Boolean verificarPaiImportarTarefas(int id1, int id2, List<ImportarTarefasPlanejamentoBean> listaBean){
		
		ImportarTarefasPlanejamentoBean bean = listaBean.get(id2-1);
		
		if (bean.getId()==id1) {
			return true;
		}else if (bean.getIdPai()!=null){
			return verificarPaiImportarTarefas(id1, bean.getIdPai(), listaBean);
		}else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param listaBean
	 * @param planejamento
	 * @author Thiago Clemente
	 * 
	 */
	public List<Tarefa> getListaTarefa(List<ImportarTarefasPlanejamentoBean> listaBean, Planejamento planejamento){
		
		List<Tarefa> listaTarefa = new ArrayList<Tarefa>();
		Map<String, Unidademedida> mapaUM = new HashMap<String, Unidademedida>();
		Tarefa tarefa;
		Indice indice;
		
		for (Unidademedida unidademedida: unidademedidaService.findAll()){
			if (unidademedida.getSimbolo()!=null && !unidademedida.getSimbolo().trim().equals("")){
				mapaUM.put(Util.strings.tiraAcento(unidademedida.getSimbolo()).toUpperCase().trim(), unidademedida);
			}
		}
		
		for (ImportarTarefasPlanejamentoBean bean: listaBean){
			
			tarefa = new Tarefa();
			tarefa.setTarefasituacao(Tarefasituacao.EM_ESPERA);
			tarefa.setPlanejamento(planejamento);
			tarefa.setIdPai(bean.getIdPai());
			tarefa.setDescricao(bean.getDescricao());
			tarefa.setQtde(bean.getQtde());
			
			if (bean.getUnidade()!=null && !bean.getUnidade().trim().equals("")){
				tarefa.setUnidademedida(mapaUM.get(Util.strings.tiraAcento(bean.getUnidade()).toUpperCase().trim()));
			}
			
			if (bean.getComposicao()!=null && !bean.getComposicao().trim().equals("")){
				indice = indiceService.findForImportacaoTarefa(bean.getComposicao());
				if (indice==null){
					throw new SinedException("N�o encontrado a composi��o: " + bean.getComposicao());
				}
				tarefa.setIndice(indice);
				transformIndiceTarefa(indice, tarefa.getQtde()/indice.getQtde(), tarefa);
			}
			
			listaTarefa.add(tarefa);
		}
		
		for (Tarefa tarefa2: listaTarefa) {
			if (tarefa2.getIdPai()!=null){
				tarefa2.setTarefapai(listaTarefa.get(tarefa2.getIdPai().intValue()-1));
			}
		}
		
		return listaTarefa;
	}
	
	/**
	 * Salva a lista de tarefa de um or�amento.
	 *
	 * @param listaTarefa
	 * @author Thiago Clemente
	 * 
	 */
	public void saveListaTarefa(final List<Tarefa> listaTarefa) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Tarefa tarefa: listaTarefa) {					
					salvaTarefa(tarefa);
				}
				
				return null;
			}
		});
	}
	
	/**
	 * Salva a tarefa, mas antes verifica de existe uma tarefa pai n�o salva.
	 *
	 * @param tarefaorcamento
	 * @author Thiago Clemente
	 * 
	 */
	private void salvaTarefa(Tarefa tarefa){
		if (tarefa.getTarefapai()!=null && tarefa.getTarefapai().getCdtarefa()==null){
			salvaTarefa(tarefa.getTarefapai());
		}
		saveOrUpdateNoUseTransaction(tarefa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2013
	 */
	public List<Tarefa> findForRomaneio(String whereIn) {
		return tarefaDAO.findForRomaneio(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.TarefaDAO#findForAtualizarDtfimTarefa()
	* 
	* @return
	* @since 30/06/2014
	* @author Luiz Fernando
	*/
	public List<Tarefa> findForAtualizarDtfimTarefa() {
		return tarefaDAO.findForAtualizarDtfimTarefa();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TarefaDAO#findForVerificarAlocacaoTarefa(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 20/03/2015
	* @author Luiz Fernando
	*/
	public List<Tarefa> findForVerificarAlocacaoTarefa(String whereIn) {
		return tarefaDAO.findForVerificarAlocacaoTarefa(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TarefaDAO#findForGerarOrdemServico(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 24/03/2015
	* @author Jo�o Vitor
	*/
	public List<Tarefa> findForGerarOrdemServico(String whereIn) {
		return tarefaDAO.findForGerarOrdemServico(whereIn);
	}
	
	/**
	* M�todo que verifica se existe solicita��o de compra vinculado ao recurso geral
	*
	* @param tarefa
	* @since 11/06/2015
	* @author Luiz Fernando
	*/
	public void verificaOrigemRecursogeral(Tarefa tarefa) {
		tarefa.setExisteRecursogeralComOrigem(false);
		if(tarefa != null && Hibernate.isInitialized(tarefa.getListaTarefarecursogeral()) && SinedUtil.isListNotEmpty(tarefa.getListaTarefarecursogeral())){
			for(Tarefarecursogeral tarefarecursogeral : tarefa.getListaTarefarecursogeral()){
				if(Hibernate.isInitialized(tarefarecursogeral.getListaSolicitacaocompraorigem()) && SinedUtil.isListNotEmpty(tarefarecursogeral.getListaSolicitacaocompraorigem())){
					tarefa.setExisteRecursogeralComOrigem(true);
					tarefarecursogeral.setExisteRecursogeralComOrigem(true);
				}else {
					tarefarecursogeral.setExisteRecursogeralComOrigem(false);
				}
			}
		}
	}
}