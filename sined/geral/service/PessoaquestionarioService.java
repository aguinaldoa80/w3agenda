package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.PessoaquestionarioDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoaquestionarioService extends GenericService<Pessoaquestionario>{
	
	private PessoaquestionarioDAO pessoaquestionarioDAO;
	
	public void setPessoaquestionarioDAO(
			PessoaquestionarioDAO pessoaquestionarioDAO) {
		this.pessoaquestionarioDAO = pessoaquestionarioDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public List<Pessoaquestionario> carregarPessoaQuestinario(Fornecedor bean){
		return pessoaquestionarioDAO.carregarPessoaQuestinario(bean);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 07/03/2012
	 * @param whereIn
	 * @param fornecedor
	 */
	public void excluirDaListaPessoa(String whereIn, Fornecedor fornecedor){
		pessoaquestionarioDAO.excluirDaListaPessoa(whereIn, fornecedor);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 09/03/2012
	 * @param fornecedor
	 */
	public void excluirListaCompletaPessoa(Fornecedor fornecedor){
		pessoaquestionarioDAO.excluirListaCompletaPessoa(fornecedor);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<Pessoaquestionario> findForFornecedorCSVListagem(String whereIn, FornecedorFiltro filtro){
		return pessoaquestionarioDAO.findForFornecedorCSVListagem(whereIn, filtro);
	}
	
	/**
	 * M�todo que retorna um question�rio de acordo com o cdpessoaquestionario informado
	 * 
	 * @param pessoaquestionario
	 * @author Jo�o Vitor
	 * 
	 */
	public Pessoaquestionario findPessoaQuestinario(Pessoaquestionario pessoaquestionario){
		return pessoaquestionarioDAO.findPessoaQuestionario(pessoaquestionario);
	}
	
	public List<Pessoaquestionario> carregarPessoaQuestionario(Pessoa bean){
		return pessoaquestionarioDAO.carregarPessoaQuestionario(bean);
	}
	
	public List<Pessoaquestionario> carregarPessoaquestionarioByInteracao(Pessoa bean){
		return pessoaquestionarioDAO.carregarPessoaquestionarioByInteracao(bean);
	}
	
	public List<Pessoaquestionario> carregarPessoaquestionarioByAgendaInteracao(Agendainteracao agendainteracao){
		return pessoaquestionarioDAO.carregarPessoaquestionarioByAgendaInteracao(agendainteracao);
	}
}
