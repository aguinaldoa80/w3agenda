package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.dao.CodigocnaeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CodigocnaeService extends GenericService<Codigocnae> {

	private CodigocnaeDAO codigocnaeDAO;
	
	public void setCodigocnaeDAO(CodigocnaeDAO codigocnaeDAO) {this.codigocnaeDAO = codigocnaeDAO;}

	/**
	 * M�todo que retorna o regime de tributa��o pelo codigo
	 *
	 * @param codigocnaeStr
	 * @return
	 * @author Luiz Fernando
	 */
	public Codigocnae getCodigocnaeForImportacaoxml(String codigocnaeStr) {
		Codigocnae codigocnae = null;
		List<Codigocnae> listaCodigocnae = this.findByCodigocnae(codigocnaeStr);
		if(listaCodigocnae != null && listaCodigocnae.size() > 0)
			codigocnae = listaCodigocnae.get(0);
		return codigocnae;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CodigocnaeDAO#findByCodigocnae(String cnae)
	 *
	 * @param cnae
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Codigocnae> findByCodigocnae(String cnae) {
		return codigocnaeDAO.findByCodigocnae(cnae);
	}

	public List<Codigocnae> findByCnae(String cnae) {
		return codigocnaeDAO.findByCnae(cnae);
	}
}
