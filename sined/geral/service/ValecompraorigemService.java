package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.dao.ValecompraorigemDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ValecompraorigemService extends GenericService<Valecompraorigem> {

	private ValecompraorigemDAO valecompraorigemDAO;
	
	public void setValecompraorigemDAO(ValecompraorigemDAO valecompraorigemDAO) {
		this.valecompraorigemDAO = valecompraorigemDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public List<Valecompraorigem> findByDocumento(Documento documento) {
		return findByDocumento(documento, null);
	}
	
	public List<Valecompraorigem> findByDocumento(Documento documento, Tipooperacao tipooperacao) {
		return valecompraorigemDAO.findByWhereInCddocumento(documento.getCddocumento().toString(), tipooperacao);
	}
	
	public List<Valecompraorigem> findByWhereInCddocumento(String whereInCddocumento) {
		return valecompraorigemDAO.findByWhereInCddocumento(whereInCddocumento, null);
	}
	
	/**
	* M�todo que verifica se o documento tem vale compra
	*
	* @param documento
	* @return
	* @since 16/12/2015
	* @author Luiz Fernando
	*/
	public Boolean existeValecompra(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo");
		
		return existeValecompra(documento.getCddocumento().toString());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param whereIn
	* @return
	* @since 18/12/2015
	* @author Luiz Fernando
	*/
	public Boolean existeValecompra(String whereIn) {
		return valecompraorigemDAO.existeValecompra(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ValecompraorigemDAO#calcularValecompraByWhereInDocumento(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 18/12/2015
	* @author Luiz Fernando
	*/
	public Money calcularValecompraByWhereInDocumento(String whereIn) {
		return valecompraorigemDAO.calcularValecompraByWhereInDocumento(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInValecompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public List<Valecompraorigem> findByValecompra(String whereInValecompra) {
		return valecompraorigemDAO.findByValecompra(whereInValecompra);
	}
	
}