package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.dao.RomaneioitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RomaneioitemService extends GenericService<Romaneioitem> {

	private RomaneioitemDAO romaneioitemDAO;
	
	public void setRomaneioitemDAO(RomaneioitemDAO romaneioitemDAO) {
		this.romaneioitemDAO = romaneioitemDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public Double getQtdeRomaneioByRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		return romaneioitemDAO.getQtdeRomaneioByRequisicaomaterial(requisicaomaterial);
	}

}
