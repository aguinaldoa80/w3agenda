package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.lkbanco.boleto.BarCode2of5;
import br.com.linkcom.lkbanco.boleto.BoletoBancario;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoDoBrasil;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoDoBrasil240_conv7pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoDoBrasil_conv6pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoDoBrasil_conv7pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoReal;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoRegionalBrasilia;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoSantander;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBancoob;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBanrisul;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioBradesco;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum11pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum11posComRegistro;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum16pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSIGCB;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSIGCB_ComRegistro;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSINCO;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCecred;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioDigitalBs2;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioHSBC;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioItau;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioMercantil;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioRural;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioSafra;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioSicred;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioUnibanco;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioUniprime;
import br.com.linkcom.lkbanco.filereturn.util.EDIHeaderLoteRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIHeaderRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIHeaderRetorno400;
import br.com.linkcom.lkbanco.filereturn.util.EDIHeaderRetornoBB240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroDetalheTRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroDetalheTRetornoBB240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroDetalheURetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetorno400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoBB400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoItau400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoRural400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoSantander400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetorno400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoBB240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoBB400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoItau400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoRural400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoSantander400;
import br.com.linkcom.lkbanco.filereturn.util.EDITrailerLoteRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDITrailerRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDITrailerRetorno400;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Boleto;
import br.com.linkcom.sined.geral.bean.Boletodigital;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaohistorico;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletocliente;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentonegociado;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ContareceberDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobrancaDocumento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.util.rest.TLSSocketConnectionFactory;
import br.com.linkcom.utils.Modulos;
import br.com.linkcom.utils.StringUtils;

public class ContareceberService extends GenericService<Documento> {

	private DocumentoService documentoService;
	private DocumentoorigemService documentoorigemService;
	private DespesaviagemService despesaviagemService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private ColaboradorService colaboradorService;
	private ContareceberDAO contareceberDAO;
	private EmpresaService empresaService;
	private MovimentacaoService movimentacaoService;
	private DocumentohistoricoService documentohistoricoService;
	private EnderecoService enderecoService;
	private ContratoService contratoService;
	private ContratohistoricoService contratohistoricoService;
	private TaxaitemService taxaitemService;
	private BoletoService boletoService;
	private RateioService rateioService;
	private AvisoService avisoService;
	private PessoacategoriaService pessoacategoriaService;
	private ContaService contaService;
	private EnvioemailService envioemailService;
	private ParametrogeralService parametrogeralService;
	private FaturamentohistoricoService faturamentohistoricoService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService;
	private ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService;
	private VendaService vendaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private DocumentotipoService documentotipoService;
	private RateioitemService rateioitemService;
	private NotaService notaService;
	private ContacorrenteService contacorrenteService;
	private TipotaxaService tipotaxaService;
	private AvisousuarioService avisoUsuarioService;
	private ReportTemplateService reportTemplateService;
	
	public void setContratofaturalocacaodocumentoService(ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService) {this.contratofaturalocacaodocumentoService = contratofaturalocacaodocumentoService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}
	public void setFaturamentohistoricoService(FaturamentohistoricoService faturamentohistoricoService) {this.faturamentohistoricoService = faturamentohistoricoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setContareceberDAO(ContareceberDAO contareceberDAO) {this.contareceberDAO = contareceberDAO;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setTaxaitemService(TaxaitemService taxaitemService) {this.taxaitemService = taxaitemService;}
	public void setBoletoService(BoletoService boletoService) {this.boletoService = boletoService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {this.pessoacategoriaService = pessoacategoriaService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContratofaturalocacaohistoricoService(ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService) {this.contratofaturalocacaohistoricoService = contratofaturalocacaohistoricoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	
	/* singleton */
	private static ContareceberService instance;

	public static ContareceberService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(ContareceberService.class);
		}
		return instance;
	}
	
	public Documento loadForEntradaWithDadosPessoa(Documento documento){
		Documento doc = this.loadForEntrada(documento);
		documentoService.carregaDadosPessoaDocumentoeForEntrada(doc);
		return doc;
	}

	@Override
	public void saveOrUpdate(Documento bean) {
		boolean financiado = bean.getFinanciamento() != null
				&& bean.getListaDocumento() != null
				&& bean.getListaDocumento().size() != 0
				&& bean.getCddocumento() == null;
		
		Parcelamento parcelamento = null;
		if (financiado) {
			parcelamento = documentoService.salvaDocumentoFinanciado(bean);
		} else {
			documentoService.atualizaHistoricoDocumento(bean, Boolean.TRUE);

			Boolean adiantamentoDespesa = null;
			String whereInDespesaviagem = null;
			if(bean.getWhereInDespesaviagem() != null && !bean.getWhereInDespesaviagem().equals("")) {
				whereInDespesaviagem = bean.getWhereInDespesaviagem();
				adiantamentoDespesa = bean.getAdiatamentodespesa();
			}
			
			String whereInDespesaviagemReembolso = null;
			if(bean.getWhereInDespesaviagemReembolso() != null && !bean.getWhereInDespesaviagemReembolso().equals("")) {
				whereInDespesaviagemReembolso = bean.getWhereInDespesaviagemReembolso();
			}
			
			if(bean.getConta() != null){
				Conta conta = contaService.carregaConta(bean.getConta());
				
				if (conta.getBanco() != null && conta.getBanco().getNumero() != null && conta.getBanco().getNumero() == 341){ //Ita�
					if (conta.getNossonumerointervalo() != null && conta.getNossonumerointervalo() && bean.getNossonumero() == null)
						bean.setNossonumero(StringUtils.stringCheia(getNextNossoNumero(conta).toString(), "0", 8, false));				
				}
				
				//Se n�o informar a carteira, o sistema preenche com a carteira padr�o.
				if (bean.getContacarteira() == null && conta.getContacarteira() != null && 
						conta.getContacarteira().getCdcontacarteira() != null){
					bean.setContacarteira(conta.getContacarteira());
				}
				
			}			
			
			super.saveOrUpdate(bean);

			if(whereInDespesaviagem != null){
				String[] idsDespesas = whereInDespesaviagem.split(",");
				for (int i = 0; i < idsDespesas.length; i++) {
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					if (adiantamentoDespesa) {
						documentoorigem.setDespesaviagemadiantamento(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
					} else {
						documentoorigem.setDespesaviagemacerto(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
						despesaviagemService.updateSituacao(idsDespesas[i], Situacaodespesaviagem.BAIXADA);
					}
					documentoorigemService.saveOrUpdate(documentoorigem);
				}
			}
			
			if(whereInDespesaviagemReembolso != null){
				despesaviagemService.updateReembolso(whereInDespesaviagemReembolso, true);
				
				StringBuilder linkDespesaviagemReembolso = new StringBuilder();
				String[] idsDespesas = whereInDespesaviagemReembolso.split(",");
				for (int i = 0; i < idsDespesas.length; i++) {
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setDespesaviagemreembolso(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
					documentoorigemService.saveOrUpdate(documentoorigem);
					
					linkDespesaviagemReembolso.append("<a href=\"javascript:visualizarDespesaviagem("+idsDespesas[i]+")\">"+idsDespesas[i]+"</a>,");
				}
				
				Documentohistorico documentohistorico = new Documentohistorico();
				documentohistorico.setDocumento(bean);
				documentohistorico.setDocumentoacao(bean.getDocumentoacao());
				documentohistorico.setObservacao("Origem do reembolso gerado das despesas de viagem: " + linkDespesaviagemReembolso.substring(0, linkDespesaviagemReembolso.length()-1));
				documentohistoricoService.saveOrUpdate(documentohistorico);
			}
		}
		documentoService.callProcedureAtualizaDocumento(bean);
		
		boolean criarVinculoFaturaDocumento = true;
		//Caso o processo de gera��o de uma nova conta a receber se originou pelo crud de contrato
		String nomeProcessoFluxoAnterior = bean.getNomeProcessoFluxoAnterior();
		if(nomeProcessoFluxoAnterior != null && bean.getContrato() != null){
			
			Contrato contrato = contratoService.findForCobranca(bean.getContrato().getCdcontrato().toString()).get(0);
			
			if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
				contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			}
			
			Documentoorigem documentoorigem = new Documentoorigem();
			documentoorigem.setContrato(contrato);
			documentoorigem.setDocumento(bean);
			documentoorigemService.saveOrUpdate(documentoorigem);
			
			List<Integer> listaParcelaInteger = null;
			if(parcelamento != null){
				Set<Parcela> listaParcela = parcelamento.getListaParcela();
				listaParcelaInteger = new ArrayList<Integer>();
				for (Parcela parcela : listaParcela) {
					documentoorigem = new Documentoorigem();
					documentoorigem.setContrato(contrato);
					documentoorigem.setDocumento(parcela.getDocumento());
					documentoorigemService.saveOrUpdate(documentoorigem);
					
					listaParcelaInteger.add(parcela.getDocumento().getCddocumento());
				}
			}
			
			Contratohistorico historico = new Contratohistorico();
			historico.setAcao(Contratoacao.COSOLIDADO);
			historico.setContrato(contrato);
			historico.setUsuario((Usuario)NeoWeb.getUser());
			historico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			
			StringBuilder sb = new StringBuilder();
			if(listaParcelaInteger != null) {
				sb.append("Gerada a(s) conta(s) ").append("<a href=\"javascript:visualizarContaReceber(").append(bean.getCddocumento()).append(")\">").append(bean.getCddocumento()).append("</a>");
				
				listaParcelaInteger.remove(bean.getCddocumento());
				for (Integer cd : listaParcelaInteger) {
					sb.append(", <a href=\"javascript:visualizarContaReceber(");
					sb.append(cd);
					sb.append(")\">");
					sb.append(cd);
					sb.append("</a>");
				}
			} else {
				sb.append("Gerada a conta ").append("<a href=\"javascript:visualizarContaReceber(").append(bean.getCddocumento()).append(")\">").append(bean.getCddocumento()).append("</a>");
			}
			sb.append(".");
			historico.setObservacao(sb.toString());
			
			historico = contratoService.criaHistoricoOSContratoAndUpdateRequisicaoestado(contrato, historico);
			
			contratohistoricoService.saveOrUpdate(historico);
			
			faturamentohistoricoService.saveHistoricoFaturamento(contrato, bean);
			
			if(bean.getFaturarlocacao() != null && bean.getFaturarlocacao()){
				Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.criaContratofaturalocacaoByContrato(contrato, bean.getValor(), false);
				contratofaturalocacaoService.gerarNumeroSequencial(contratofaturalocacao);
				documentoService.updateMesangem1And2NFDocumentoVenda(bean, "Referente a fatura: " + contratofaturalocacao.getNumero().toString(), null);
				documentoService.updateNumerosNFDocumento(bean.getCddocumento().toString(), contratofaturalocacao.getNumero().toString());
				contratofaturalocacaoService.saveFromFaturamento(contratofaturalocacao, bean);
				
				Contratofaturalocacaodocumento contratofaturalocacaodocumento = new Contratofaturalocacaodocumento();
				contratofaturalocacaodocumento.setDocumento(bean);
				contratofaturalocacaodocumento.setContratofaturalocacao(contratofaturalocacao);
				contratofaturalocacaodocumentoService.saveOrUpdate(contratofaturalocacaodocumento);
				criarVinculoFaturaDocumento = false;
			}
			
			if(contrato.getAcrescimoproximofaturamento() != null && contrato.getAcrescimoproximofaturamento()){
				contratoService.updateZeraAcrescimo(contrato);
			}
			contratoService.atualizaParcelaCobradaFaturamento(contrato);
			contratoService.atualizaDtProximoVencimentoFaturamento(contrato);
			contratoService.atualizaValorRateioFaturamento(contrato);
			
			if(bean.getFinalizarContrato() != null && bean.getFinalizarContrato()){
				contrato.setDtfim(SinedDateUtils.currentDate());
				contratoService.atualizaDataFimContrato(contrato);
				contratoService.atualizaDataConclusaoContrato(contrato);
			}
			contratoService.updateAux(contrato.getCdcontrato());
			
			List<Documento> listaContaReceber = new ArrayList<Documento>();
			bean.setContrato(contrato);
			listaContaReceber.add(bean);
			contratoService.verificaComissionamentoDocumento(listaContaReceber, false);
			
			
		}
		
		if (Boolean.TRUE.equals(bean.getGerarreceitafaturalocacao())){
			Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.findForGerarReceita(bean.getCdcontratofaturalocacao().toString()).get(0);
			contratofaturalocacaoService.criaDocumentoFaturaContratoLocacao(bean, contratofaturalocacao, null, false, false);
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setDocumento(bean);
			documentohistorico.setDocumentoacao(bean.getDocumentoacao());
			documentohistorico.setObservacao("Fatura de loca��o: <a href=\"javascript:visualizarContratofaturalocacao("+contratofaturalocacao.getCdcontratofaturalocacao()+")\">"+contratofaturalocacao.getNumero()+"</a>.");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			if(criarVinculoFaturaDocumento){
				Contratofaturalocacaodocumento contratofaturalocacaodocumento = new Contratofaturalocacaodocumento();
				contratofaturalocacaodocumento.setDocumento(bean);
				contratofaturalocacaodocumento.setContratofaturalocacao(contratofaturalocacao);
				contratofaturalocacaodocumentoService.saveOrUpdate(contratofaturalocacaodocumento);
			}
			
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.FATURADA);
			contratofaturalocacaohistorico.setContratofaturalocacao(contratofaturalocacao);
			String observacao = "Conta a receber: <a href=\"javascript:visualizarContareceber("+bean.getCddocumento()+")\">"+bean.getCddocumento()+"</a>. ";
			contratofaturalocacaohistorico.setObservacao(observacao);
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
			
			contratofaturalocacaoService.updateSituacao(bean.getCdcontratofaturalocacao().toString(), Contratofaturalocacaosituacao.FATURADA);
		}
	}

	/**
	 * Faz o merge de todos os relat�rios de conta a receber.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public Resource createBoleto(WebRequestContext request, String whereIn) throws Exception {

		if(whereIn == null) whereIn = SinedUtil.getItensSelecionados(request);
		
		List<String> listaErro = new ArrayList<String>();
		
		// VAI ENTRAR NESSE IF QUANDO VIR DA TELA DE VENDA
		// O WHEREIN NESSE CASSO SERIAM AS VENDA
		if(request.getParameter("fromVenda") != null && request.getParameter("fromVenda").equals("true")){
			if(vendaService.existeVendaRepresentacao(whereIn)){ 
				throw new SinedException("N�o � poss�vel utilizar as a��es de Gerar nota, Gerar boleto e Negociar de venda de representa��o.");
			}
			
			if(vendaService.existeVendaSemDocumento(whereIn)){ 
				throw new SinedException("N�o foi(ram) gerada(s) contas a receber a partir de venda.");
			}
			
			List<Documento> listadocumento = this.findByVendasForBoleto(whereIn);
			if(listadocumento == null || listadocumento.size() == 0) {
				throw new SinedException("Nenhum documento encontrado a partir dessa venda.");
			}
			whereIn = CollectionsUtil.listAndConcatenate(listadocumento, "cddocumento", ",");
		}

		if (documentoService.isDocumentoNotDefitivaNotPrevista(whereIn)) {
			listaErro.add("S� pode imprimir boleto de uma conta a receber que estiver na situa��o prevista ou definitiva.");
		}
		
		//List<Documento> docs = documentoService.findDocumentoContaNotGeraBoleto(whereIn);
		/*if (documentoService.isDocumentoContaNotGeraBoleto(whereIn)) {
			listaErro.add("S� pode imprimir boleto de conta banc�ria que esteja marcada para gerar boleto.");
		}*/
		
		if (documentoService.existDocumentoContaNotNossonumero(whereIn)) {
			listaErro.add("Arquivo de retorno de confirma��o de entrada n�o processada.<br>S� pode imprimir boleto de conta banc�ria com o campo NOSSO NUMERO GERADO PELO BANCO marcado, se o documento tiver o nosso n�mero preenchido.");
		}
		
		if (documentoService.isPossuiEmissaoBoletoBloqueado(whereIn)){
			listaErro.add("N�o foi poss�vel emitir o boleto! Conta possui arquivo de retorno gerado e configurado para bloquear emiss�o de boletos.");
		}
		
		if(listaErro.size() > 0){
//			for (String erro : listaErro) {
//				request.addError(erro);
//			}
//			throw new SinedException("Erro na gera��o de boleto.");
			throw new SinedException(CollectionsUtil.concatenate(listaErro, "<br/>"));
		}

		List<Documento> listaDocumento = this.findForBoleto(whereIn);
		List<Documento> listaDocumentosGerarBoleto = new ArrayList<Documento>();
		//Filtra apenas documentos que podem gerar boleto
		for(Documento doc: listaDocumento){
			if(doc.getContacarteira() == null || !Boolean.TRUE.equals(doc.getContacarteira().getNaogerarboleto())){
				listaDocumentosGerarBoleto.add(doc);
			}
		}
		listaDocumento = listaDocumentosGerarBoleto;
		List<BoletoBean> lista = new ArrayList<BoletoBean>();
		
		boolean forcarAtualizacao = false;
		String paramForcarAtualizacao = request.getParameter("forcarAtualizacao");
		if(paramForcarAtualizacao != null && !paramForcarAtualizacao.equals("") && paramForcarAtualizacao.toUpperCase().equals("TRUE")){
			forcarAtualizacao = true;
		}
		
		HashMap<MessageType, String> mapaValidacaoSequenciais = documentoService.validaLimiteSequencialNossoNum(listaDocumento, null);
		if(mapaValidacaoSequenciais.containsKey(MessageType.ERROR)){
			throw new SinedException(mapaValidacaoSequenciais.get(MessageType.ERROR));
		}
		
		for (Documento documento : listaDocumento) {
			try{
				lista.add(boletoDocumento(documento, forcarAtualizacao));
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add("Erro na gera��o do boleto da conta " + documento.getCddocumento() + ": " + e.getMessage());
			}
		}
		
		if(listaErro.size() > 0){
//			for (String erro : listaErro) {
//				request.addError(erro);
//			}
			//J� existe a mensagem de "Erro na gera��o do boleto" acima
//			throw new SinedException("Erro na gera��o de boleto.");
			throw new SinedException(CollectionsUtil.concatenate(listaErro, "<br/>"));
		}
		
		documentoService.gerarHistoricoBoletoPrimeiraVez(listaDocumento);
		
		String fileName = "boleto_" + SinedUtil.datePatternForReport() + ".pdf";
		MergeReport mergeReport = new MergeReport(fileName);
		if(!lista.isEmpty()){
			Report report = new Report("/financeiro/relBoleto");
			if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
				report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
			}
			report.setDataSource(lista);
			mergeReport.addReport(report);
		}

		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/08/2014
	 */
	public List<Documento> findByVendasForBoleto(String whereIn) {
		return contareceberDAO.findByVendasForBoleto(whereIn);
	}
	
	public List<Documento> findByVendasForNegociar(String whereIn) {
		return contareceberDAO.findByVendasForNegociar(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findByVendas(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 10/04/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findByVendas(String whereIn) {
		return contareceberDAO.findByVendas(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Documento> findByPedidovenda(String whereIn) {
		return contareceberDAO.findByPedidovenda(whereIn);
	}
	
	@SuppressWarnings("unchecked")
	public BoletoBean boletoDocumento(Documento documento, boolean forcaAtualizacao) {
		
		if (documento.getConta() == null) {
			throw new SinedException("A conta a receber " + documento.getCddocumento() + " n�o possui conta banc�ria cadastrada.");
		}
		
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		
		Date vencimentoOriginal = documento.getDtvencimento();
		Date vencimento = documento.getDtvencimento();
		
		String msg1 = "";
		String msg2 = "";
		String outrosAcrescimos = "";
		boolean taxaBoleto = false;
		
		Cliente cliente = null;
		if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
			cliente = clienteService.carregarDadosCliente(new Cliente(documento.getPessoa().getCdpessoa()));
		}
		String msgcedenteStr = documento.getInformacoescedente() != null ? documento.getInformacoescedente() + "\n" : "";
		Money valorboleto = documento.getValor();		
		Date atual = new Date(System.currentTimeMillis());		
		
		//utilizado para deduzir no valor do documento ap�s gerar o c�digo de barras e a imagem com o valor correto
		Money valortaxaboleto = null;
		
		if(documento.getTaxa() != null){
			List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
			listaTaxaItem = taxaitemService.findTaxasItens(documento.getTaxa());
			Money valorAux;		
			int difdias;
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				Conta contabancaria = documento.getConta() != null ? contaService.load(documento.getConta(), "conta.cdconta, conta.banco") : null;
				String numero_banco = contabancaria != null && contabancaria.getBanco() != null && contabancaria.getBanco().getNumero() != null ? contabancaria.getBanco().getNumero().toString() : null;
					
				for (Taxaitem item : listaTaxaItem) {
					
					if(item.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem() || forcaAtualizacao){
							if(item.isPercentual()){
								if("756".equals(numero_banco)){
									valorAux = new Money(new Double(SinedUtil.truncateFormat(documento.getValor().multiply(item.getValor().divide(new Money(100d))).getValue().doubleValue(), 2)));
								}else {
									valorAux = documento.getValor().multiply(item.getValor().divide(new Money(100d)));
								}
							} else {
								valorAux = item.getValor();
							}
							
							valorboleto = valorboleto.add(valorAux);
							msgcedenteStr += "Valor atualizado com multa de R$ " + valorAux.toString() + ".\n";
							vencimento = atual;
						}
						
						if(forcaAtualizacao){
							String msgMulta = item.getTipotaxa().getNome() + 
												" de " +
												(item.isPercentual() ? "" : "R$") +
												item.getValor().toString() +
												(item.isPercentual() ? "%" : "") +
												" ap�s " +
												SinedDateUtils.toString(vencimento) + ". ";
							
							if((msg1.length() + msgMulta.length()) <= 80){
								msg1 += msgMulta;
							} else {
								msg2 += msgMulta;
							}
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem() || forcaAtualizacao){
							if(item.isPercentual()){
								valorAux = taxaitemService.calcula_juros_dia(documento.getValor(), item.getValor(), documento.getConta());
							} else {
								valorAux = item.getValor();
							}
							
							difdias = SinedDateUtils.diferencaDias(atual,item.getDtlimite());
							if(item.isPercentual() && "756".equals(numero_banco)){
								valorAux = new Money(new Double(SinedUtil.truncateFormat(valorAux.multiply(new Money(new Double(difdias))).getValue().doubleValue(), 2)));
							}else {
								valorAux = valorAux.multiply(new Money(new Double(difdias)));
							}
							valorboleto = valorboleto.add(valorAux);
							msgcedenteStr += "Valor atualizado com juros de R$ " + valorAux.toString() + ".\n";
							vencimento = atual;
						}
						
						if(forcaAtualizacao){
							String msgJuros = item.getTipotaxa().getNome() + 
												" de " +
												(item.isPercentual() ? "" : "R$") +
												item.getValor().toString() +
												(item.isPercentual() ? "%" : "") +
												" ap�s " +
												SinedDateUtils.toString(vencimento) + ". ";
							
							if((msg1.length() + msgJuros.length()) <= 80){
								msg1 += msgJuros;
							} else {
								msg2 += msgJuros;
							}
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.JUROS_MES) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem() || forcaAtualizacao){
							if(item.isPercentual()){
								difdias = SinedDateUtils.diferencaDias(atual,item.getDtlimite());
								valorAux = taxaitemService.calcula_juros_mes(documento.getValor(), item.getValor(), difdias, documento.getConta());
								valorboleto = valorboleto.add(valorAux);
								msgcedenteStr += "Valor atualizado com juros de R$ " + valorAux.toString() + ".\n";
								vencimento = atual;
							}
						}
						
						if(forcaAtualizacao){
							String msgJuros = item.getTipotaxa().getNome() + 
												" de " +
												(item.isPercentual() ? "" : "R$") +
												item.getValor().toString() +
												(item.isPercentual() ? "%" : "") +
												" ap�s " +
												SinedDateUtils.toString(vencimento) + ". ";
							
							if((msg1.length() + msgJuros.length()) <= 80){
								msg1 += msgJuros;
							} else {
								msg2 += msgJuros;
							}
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.DESAGIO) && SinedDateUtils.afterIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = documento.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
							
							difdias = SinedDateUtils.diferencaDias(item.getDtlimite(), atual);
							valorAux = valorAux.multiply(new Money(new Double(difdias)));
							valorboleto = valorboleto.subtract(valorAux);
							msgcedenteStr += "Valor atualizado com des�gio de R$ " + valorAux.toString() + ".\n";
							//vencimento = atual;
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.DESCONTO) && SinedDateUtils.afterIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = documento.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
							
							valorboleto = valorboleto.subtract(valorAux);
							msgcedenteStr += "Valor atualizado com desconto de R$ " + valorAux.toString() + ".\n";
							//vencimento = atual;
						}
					}
					
					if (item.getTipotaxa().equals(Tipotaxa.TAXABOLETO)){
						taxaBoleto = true;
						if( SinedDateUtils.beforeOrEqualIgnoreHour(item.getDtlimite(), new Date(System.currentTimeMillis()))){
							if (cliente != null ){
								if (cliente.getNaoconsiderartaxaboleto() == null || !cliente.getNaoconsiderartaxaboleto()){
									if(cliente.getDiscriminartaxaboleto() != null && cliente.getDiscriminartaxaboleto()){
										outrosAcrescimos = item.getValor().toString();
										valorboleto = valorboleto.add(item.getValor());
										valortaxaboleto = item.getValor();
									}else {
										valorboleto = valorboleto.add(item.getValor());
									}
								}
							} else {
								valorboleto = valorboleto.add(item.getValor());
							}							
						}
					}
					
					if (item.getTipotaxa().equals(Tipotaxa.TAXAMOVIMENTO)){
						if(item.getDtlimite() != null && SinedDateUtils.beforeOrEqualIgnoreHour(item.getDtlimite(), documento.getDtemissao())){
							if(item.isPercentual()){
								valorboleto = valorboleto.subtract(documento.getValor().multiply(item.getValor().divide(new Money(100d))));
							} else {
								valorboleto = valorboleto.subtract(item.getValor());
							}
						}
					}
				}
				
				valorboleto = taxaitemService.getValorTaxaBaixaDesconto(valorboleto, SinedUtil.listToSet(listaTaxaItem, Taxaitem.class), documento.getDtemissao());
				valorboleto = taxaitemService.getValorTaxaBaixaAcrescimo(valorboleto, SinedUtil.listToSet(listaTaxaItem, Taxaitem.class), new Date(System.currentTimeMillis()));
			} 
		} 
		
		if (!taxaBoleto){
			if (cliente != null ){
				if (cliente.getNaoconsiderartaxaboleto() == null || !cliente.getNaoconsiderartaxaboleto()){
					if(cliente.getDiscriminartaxaboleto() != null && cliente.getDiscriminartaxaboleto()){
						outrosAcrescimos = documento.getConta().getTaxaboleto().toString();
						valorboleto = valorboleto.add(documento.getConta().getTaxaboleto());
						valortaxaboleto = documento.getConta().getTaxaboleto();
					}else {
						valorboleto = valorboleto.add(documento.getConta().getTaxaboleto());
					}
				}
			} else {
				valorboleto = valorboleto.add(documento.getConta().getTaxaboleto());
			}
		}
				
		if(documento.getPessoa() != null){
			msgcedenteStr += this.incluirAviso(documento.getPessoa());
		}
		
		documento.setValorboleto(valorboleto);
		documento.setDtvencimento(vencimento);
		
		try{
			if(forcaAtualizacao){
				documento.setMensagem2(msg1);
				documento.setMensagem3(msg2);
			}
			BoletoBean bean = geraBoleto(documento);
			
			if(valortaxaboleto != null && valortaxaboleto.getValue().doubleValue() > 0){
				bean.setValorDocumento(documento.getValorboleto().subtract(valortaxaboleto).toString());
			}
//			if(forcaAtualizacao){
//				bean.setInstrucoes3(msg1);
//				bean.setInstrucoes4(msg2);
//			}
				
			if (!"".equals(outrosAcrescimos))
				bean.setOutrosAcrescimos(outrosAcrescimos);
			
			bean.setDtVencimento(formatador.format(vencimento));
			bean.setMsgcedente(msgcedenteStr);
			bean.setLogo(SinedUtil.getLogo(documento.getEmpresa()));
			
			documento.setDtvencimento(vencimentoOriginal);
			
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			
			if("Corrigir a quantidade de caracteres do Nosso N�mero (quantidade permitida s�o 7 + DV)".equals(e.getMessage())){
				throw new SinedException(e.getMessage());
			}else {
				throw new SinedException("favor conferir os dados da conta banc�ria.");
			}
		}
	}

	/**
	 * Adiciona avisos do cliente �s mensagens do boleto.
	 * @param pessoa
	 * @return
	 * @author Taidson
	 * @since 16/11/2010
	 */
	public String incluirAviso(Pessoa pessoa){
		Cliente cliente = new Cliente(pessoa.getCdpessoa());
		String aviso = "";
		String whereIn = "";
		List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
		List<Integer> listaCdcategoria = null;
		
		if(listaPessoacategoria != null && listaPessoacategoria.size() > 0){
			listaCdcategoria = new ArrayList<Integer>();
			for (Pessoacategoria item : listaPessoacategoria) {
				listaCdcategoria.add(item.getCategoria().getCdcategoria());
			}
			whereIn = org.apache.commons.lang.StringUtils.join(listaCdcategoria.iterator(), ",");
		}
		List<Aviso> listaAviso = avisoService.avisoBoleto(cliente, whereIn);
		if(listaAviso != null && listaAviso.size() > 0){
			for (Aviso item : listaAviso) {
				aviso += item.getComplemento() +"\n" ;
			}
		}
		return aviso;
	}
	
	
	public Report createCarne(WebRequestContext request) throws Exception {
		Report report = new Report("/financeiro/emitircarne");

		String whereIn = SinedUtil.getItensSelecionados(request);
		String strSemCpfCnpj = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.BOLETOSEMCPFCNPJ);
		if (documentoService.isDocumentoNotDefitivaNotPrevista(whereIn)) {
			throw new SinedException("S� pode imprimir boleto de uma conta a receber que estiver na situa��o prevista ou definitiva.");
		}

		String viascarne = parametrogeralService.getValorPorNome(Parametrogeral.VIAS_CARNE);
		if(viascarne != null && "3".equals(viascarne)){
			report = new Report("/financeiro/emitircarne3vias");
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.LAYOUT_CARNE))
			report = new Report("/financeiro/emitircarneRetrato3vias");
		
		List<Documento> listaDocumento = this.findForBoleto(whereIn);
		List<BoletoBean> lista = new ArrayList<BoletoBean>();
		
		for (Documento documento : listaDocumento) {
			documento.setEmitirCarne(Boolean.TRUE);
			BoletoBean  boletoBean = boletoDocumento(documento, false);
			if (strSemCpfCnpj.equals("TRUE")){
				boletoBean.setCpfCnpj(null);
			}
			lista.add(boletoBean);
		}
		report.setDataSource(lista);
		return report;
	}

	/**
	 * Cria um relat�rio de boleto.
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public BoletoBean geraBoleto(Documento documento) {
		
		
		BoletoBancario bb = null;
		String cdBanco = "";
		boolean fatorvencimentozerado = documento.getContacarteira().getFatorvencimentozerado() != null ? documento.getContacarteira().getFatorvencimentozerado() : false;
		String parametroSantander = parametrogeralService.buscaValorPorNome(Parametrogeral.NOVO_BOLETO_SANTANDER);
//		String paramBoletoCaixa = ParametrogeralService.getInstance().getValorPorNome("LAYOUT_BOLETO_CAIXA");
		String strSemCpfCnpj = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.BOLETOSEMCPFCNPJ);
		
		//
		bb = criaBoletoBancario(documento, cdBanco, fatorvencimentozerado, parametroSantander);
		if(bb != null){
			cdBanco = bb.getCdBanco();
		}

		Image image = null;
		try {
			String url = null;
			if (!parametroSantander.equals("TRUE") && documento.getConta().getBanco().getNumero().equals(33)){
				url = SinedUtil.getUrlWithContext() + "/imagens/financeiro/relatorio/" + documento.getConta().getBanco().getNumero() + "_old.gif";
			}else{
				url = SinedUtil.getUrlWithContext() + "/imagens/financeiro/relatorio/" + documento.getConta().getBanco().getNumero() + ".gif";
			}
			
			SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Content-type", "image/gif");
			HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpGet);
			HttpEntity entity = response.getEntity();
			image = ImageIO.read(entity.getContent());
		} catch (Exception e) {
//			System.out.println("Problemas no carregamento da Logomarca do Banco " + documento.getConta().getBanco().getNumero());
			image = null;
		}

		BoletoBean boletoBean = new BoletoBean();
		boletoBean.setLogoBanco(image);
		boletoBean.setCodigoBanco(cdBanco);
		if("422-7".equals(cdBanco)){
			boletoBean.setNomeBanco("Banco Safra S.A");
		}
		if(documento.getConta() != null && documento.getConta().getLocalpagamento() != null && !"".equals(documento.getConta().getLocalpagamento()))
			boletoBean.setLocalPagamento(documento.getConta().getLocalpagamento());
		else
			boletoBean.setLocalPagamento(fatorvencimentozerado ? "PAG�VEL EM QUALQUER AG�NCIA BANC�RIA" : bb.getLOCAL_PAGAMENTO());
		boletoBean.setDtVencimento(new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()));
		
		Empresa empresa = documento.getEmpresa();
		if(documento.getConta() != null && documento.getConta().getListaContaempresa() != null && !documento.getConta().getListaContaempresa().isEmpty()){
			if(documento.getConta().getListaContaempresa().size() == 1 || empresa == null){
				empresa = documento.getConta().getEmpresa();
			}
		}
		
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		Empresa empresaTitular = empresa;
		if(empresa != null && documento.getConta().getEmpresatitular() != null && 
				!empresa.equals(documento.getConta().getEmpresatitular())){
			empresaTitular = documento.getConta().getEmpresatitular();
			empresaService.setInformacoesPropriedadeRural(empresaTitular);
			
			boletoBean.setAvalistasacador(empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
			boletoBean.setCpfCnpjAvalistasacador(empresa.getCpfCnpjProprietarioPrincipalOuEmpresa());
		}
		
		// Caso seja emiss�o de boleto, cedente recebe campo 'titular'
		if(documento.getEmitirCarne() != null && documento.getEmitirCarne()){
			String nomeCedente = null;
			if(documento.getConta().getTitular() != null && !"".equals(documento.getConta().getTitular())){
				nomeCedente = documento.getConta().getTitular();				
			}
			else if(empresaTitular != null && empresaTitular.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() != null){
				nomeCedente = empresaTitular.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa();		
			}
			boletoBean.setCedente(nomeCedente);
		}
		// Caso seja emiss�o de boleto
		else {
			String cpfcnpj = null;
			String enderecoStr = null;
			String nomeCedente = null;
			
			if(empresaTitular != null){
				if(empresaTitular.getCpfCnpj() != null){
					cpfcnpj = empresaTitular.getCpfCnpjProprietarioPrincipalOuEmpresa();
					boletoBean.setCpfCnpjCedente(cpfcnpj);
				}
				if(SinedUtil.isListNotEmpty(empresaTitular.getListaEndereco())){
					Endereco enderecoBoleto = null;
					for(Endereco endereco : empresaTitular.getListaEndereco()){
						if(endereco.getEnderecotipo() != null && !Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
							enderecoBoleto = endereco;
							break;
						}
					}
					if(enderecoBoleto == null){
						enderecoBoleto = empresaTitular.getListaEndereco().iterator().next();
					}
					Endereco endereco = enderecoService.loadEndereco(enderecoBoleto);
					enderecoStr = endereco.getLogradouroCompletoComBairroCEP();
					boletoBean.setEnderecoCedente(enderecoStr);
					if(endereco.getCep()!=null){
						boletoBean.setCepCedente(endereco.getCep().getValue());
					}
				}
			}
			if(documento.getConta().getTitular() != null && !"".equals(documento.getConta().getTitular())){
				nomeCedente = documento.getConta().getTitular();				
			}
			else if(empresaTitular != null && empresaTitular.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() != null){
				nomeCedente = empresaTitular.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa();		
			}
			else{
				nomeCedente = "";
			}
				
			boletoBean.setCedenteComEndereco(nomeCedente + (!strSemCpfCnpj.equals("TRUE") ? ("\nCPF/CNPJ: "+ Util.strings.emptyIfNull(cpfcnpj)) : "") + (enderecoStr != null ? ("\n"+enderecoStr) : ""));
			boletoBean.setCedente(nomeCedente);
		}
		boletoBean.setAgCdCedente(bb.getAgCdCedente());
		boletoBean.setDtDocumento(new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtemissao()));

		// Caso seja emiss�o de carn� e documento referente a uma venda, 'numdocumento' recebe 'cddocumento(cdvenda/N�Parcela)'
		if(documento.getEmitirCarne() != null && documento.getEmitirCarne()){
			String cdvenda = null;
			String numparcela = null;
			if(documento.getListaDocumentoOrigem() != null && !documento.getListaDocumentoOrigem().isEmpty()){
				Documentoorigem documentoorigem = documento.getListaDocumentoOrigem().get(0);
				if(documentoorigem.getVenda() != null && documentoorigem.getVenda().getCdvenda() != null)
					cdvenda = documentoorigem.getVenda().getCdvenda().toString();
			}
			if(documento.getListaParcela() != null && !documento.getListaParcela().isEmpty()){
				Parcela parcela = documento.getListaParcela().iterator().next();
				if(parcela != null && parcela.getOrdem() != null)
					numparcela = parcela.getOrdem().toString();
			}
			
			if(numparcela != null && cdvenda != null)
				boletoBean.setNumDocumento(documento.getCddocumento().toString()+"("+cdvenda+"/"+numparcela+")");
			else
				boletoBean.setNumDocumento(documento.getCddocumento().toString());
		}
		// Caso seja emiss�o de boleto, 'numdocumento' recebe apenas 'cddocumento'
		else{
			if(documento.getNumero() != null && !"".equals(documento.getNumero())){
				boletoBean.setNumDocumento(documento.getNumero());
			}else {
				boletoBean.setNumDocumento(documento.getCddocumento().toString());
			}
		}
		
		boletoBean.setEspDocumento(documento.getContacarteira().getEspeciedocumento() != null && !documento.getContacarteira().getEspeciedocumento().equals("") ? documento.getContacarteira().getEspeciedocumento() : bb.getEspDocumento());
		boletoBean.setAceite(bb.getAceite());
		boletoBean.setDtProcessamento(new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtemissao()));
		boletoBean.setNossoNumero(bb.getNossoNumero());
		boletoBean.setUsoBanco("");
		boletoBean.setCarteira(bb.getCarteira());
		boletoBean.setEspecie("R$");
		boletoBean.setValorDocumento(documento.getValorboleto().toString());
		StringBuilder instrucoes = new StringBuilder();
		
		instrucoes.append(documento.getMensagem1()!=null ? documento.getMensagem1()+"\n" : "");
		instrucoes.append(documento.getMensagem2()!=null ? documento.getMensagem2()+"\n" : "");
		instrucoes.append(documento.getMensagem3()!=null ? documento.getMensagem3()+"\n" : "");
		instrucoes.append(documento.getMensagem4()!=null ? documento.getMensagem4()+"\n" : "");
		instrucoes.append(documento.getMensagem5()!=null ? documento.getMensagem5()+"\n" : "");
		instrucoes.append(documento.getMensagem6()!=null ? documento.getMensagem6()+"\n" : "");
		
		if(parametrogeralService.getBoolean(Parametrogeral.CONCATENAR_OBSERVACAO_CONTARECEBER_NO_BOLETO)){
			String aux = (documento.getDescricao()!=null ? documento.getDescricao() + " " : "")+(documento.getObservacao() !=null ? documento.getObservacao() : "");
			aux = aux.replaceAll("\n", " ");
			aux = aux.replaceAll("\r", " ");
			instrucoes.append(!aux.isEmpty() ? aux : "");
		}
		
		boletoBean.setInstrucoes1(instrucoes.toString());
		boletoBean.setDesconto("");
		boletoBean.setOutrasDeducoes("");
		boletoBean.setMoraMulta("");
		boletoBean.setOutrosAcrescimos("");
		
		String nomeSacado = null;
		String cpfcnpj = null;
		
		Endereco endObj = documento.getEndereco();
		if(endObj != null && endObj.getCdendereco() != null){
			endObj = enderecoService.loadEndereco(endObj);
		} else endObj = null;
		
		if (documento.getTipopagamento().equals(Tipopagamento.CLIENTE)) {
			Cliente cliente = clienteService.loadForBoleto(documento.getPessoa().getCdpessoa());
			nomeSacado = cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals("") ? cliente.getRazaosocial() : cliente.getNome();
			if(endObj == null) endObj = cliente.getEndereco();
			cpfcnpj = cliente.getCpfCnpj();
			boletoBean.setClienteIdentificador(cliente.getIdentificador() != null ? cliente.getIdentificador() : "");
		} else if (documento.getTipopagamento().equals(Tipopagamento.FORNECEDOR)) {
			Fornecedor fornecedor = fornecedorService.loadForBoleto(documento.getPessoa().getCdpessoa());
			nomeSacado = fornecedor.getRazaosocial();
			if(endObj == null) endObj = fornecedor.getEndereco();
			cpfcnpj = fornecedor.getCpfCnpj();
		} else if (documento.getTipopagamento().equals(Tipopagamento.COLABORADOR)) {
			Colaborador colaborador = colaboradorService.loadForBoleto(documento.getPessoa().getCdpessoa());
			nomeSacado = colaborador.getNome();
			if(endObj == null) endObj = colaborador.getEndereco();
			cpfcnpj = colaborador.getCpfCnpj();
		}
		if (strSemCpfCnpj.equals("TRUE"))
			cpfcnpj = null;
		
		
		//Verifica se h� ids dos contratos
		if(documento.getListaDocumentoOrigem() != null && !documento.getListaDocumentoOrigem().isEmpty()){
			String idsContratos = "";
			for (Documentoorigem documentoorigem : documento.getListaDocumentoOrigem()) {
				if(documentoorigem.getContrato() != null && documentoorigem.getContrato().getCdcontrato() != null && documentoorigem.getContrato().getDtcancelamento() == null){
					idsContratos += documentoorigem.getContrato().getCdcontrato()+", ";
				}
			}
			if(idsContratos != null && !idsContratos.equals("")){
				boletoBean.setContratoId(idsContratos.substring(0, idsContratos.length()-2));
			}
		}
		
		if (documento.getContacarteira() != null && (documento.getContacarteira().getAprovadoproducao() == null || !documento.getContacarteira().getAprovadoproducao())) {
			boletoBean.setMarcadagua("Em Homologa��o");
		}

		boletoBean.setNomeSacado(nomeSacado);
		boletoBean.setEnderecoBairroSacado(endObj != null ? endObj.getLogradouroCompletoComBairroSemMunicipio() : "");
		boletoBean.setCidadeEstadoCepSacado((endObj != null && endObj.getMunicipio() != null ? endObj.getMunicipio().getNome() + " - " + endObj.getMunicipio().getUf().getSigla() : "") + " "+ (endObj != null && endObj.getCep() != null ? endObj.getCep().toString() : ""));
		boletoBean.setCaixaPostalSacado(endObj != null ? endObj.getCaixapostal() : "");
		boletoBean.setCepPagador(endObj != null && endObj.getCep() != null? endObj.getCep().getValue(): "");
		boletoBean.setCdBaixa(bb.getNossoNumero());
		boletoBean.setCpfCnpj(cpfcnpj);
		boletoBean.setNumCodBarras(bb.GeraSequenciaSuperior());
		boletoBean.setNumCodBarrasSemFormatacao(bb.GeraSequenciaInferior());
		
		// Gera imagem do codigo de barra e passa para o relatorio
		BarCode2of5 bc = new BarCode2of5();
		bc.setSize(351, 32);
		Image img = bc.createImage(bb.GeraSequenciaInferior());

		boletoBean.setCodigoBarra(img);
		
		return boletoBean;
	}
	
	public BoletoBancario criaBoletoBancario(Documento documento, String cdBanco, boolean fatorvencimentozerado, String parametroSantander){
		BoletoBancario bb = null;
		String dvnumero = documento.getConta().getDvnumero() != null ? documento.getConta().getDvnumero() : "";
		String numero = documento.getConta().getNumero() != null ? documento.getConta().getNumero() : "";
		
		if (documento.getConta().getBanco().getNumero().equals(104)) { // Caixa Econ�mica Federal
			cdBanco = "104-0";

			try {
				if (BoletoBancarioCaixaSIGCB_ComRegistro.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())) {
					// SIGCB CR
					bb = new BoletoBancarioCaixaSIGCB_ComRegistro(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
							numero.replaceAll("[\\.-]","").substring(0, 3), 										/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(numero.replaceAll("[\\.-]", "").substring(3), "0", 9, false),	/* N�mero da Conta do Cedente 				*/
							new Integer(8), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							documento.getContacarteira().getConvenio(), 																	/* C�digo do Cliente (Com DV) 				*/
							new Integer(6), 																								/* Tamanho do C�digo do Cliente (Sem DV)	*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(15), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
							documento.getContacarteira().getCarteira(), 																	/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N",	/* Aceite 									*/
							documento.getContacarteira().getIdemissaoboletoimpresso());																/*Id emiss�o de boleto caixa */											
				} else if(BoletoBancarioCaixaSIGCB.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())){
					// SIGCB
					bb = new BoletoBancarioCaixaSIGCB(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
							numero.replaceAll("[\\.-]","").substring(0, 3), 										/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(numero.replaceAll("[\\.-]", "").substring(3), "0", 9, false),	/* N�mero da Conta do Cedente 				*/
							new Integer(8), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							documento.getContacarteira().getConvenio(), 																	/* C�digo do Cliente (Com DV) 				*/
							new Integer(6), 																								/* Tamanho do C�digo do Cliente (Sem DV)	*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(15), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
							documento.getContacarteira().getCarteira(), 																	/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N",  /* Aceite 									*/
							documento.getContacarteira().getIdemissaoboletoimpresso());																/*Id emiss�o de boleto caixa */
				} else if(BoletoBancarioCaixaSINCO.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())){
					// SINCO
					bb = new BoletoBancarioCaixaSINCO(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(),																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia				*/
							numero.replaceAll("[\\.-]","").substring(0, 3), 										/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(numero.replaceAll("[\\.-]", "").substring(3), "0", 9, false),	/* N�mero da Conta do Cedente 				*/
							new Integer(8), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							documento.getContacarteira().getConvenio(), 																			/* C�digo do Cliente (Com DV) 				*/
							new Integer(6),	 																								/* Tamanho do C�digo do Cliente (Sem DV) 	*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(17), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(),												/* Valor do Boleto 							*/
							"01", 																											/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N");																											/* Aceite 									*/
				} else if(BoletoBancarioCaixaSICOB_nossoNum16pos.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())){
					String numeroConta = numero.replaceAll("[\\.-]", "") + dvnumero.replaceAll("[\\.-]", "");
					String operacao = "870";
					if(documento.getContacarteira().getCarteira() != null && 
							org.apache.commons.lang.StringUtils.isNotEmpty(documento.getContacarteira().getOperacao())){
						operacao = documento.getContacarteira().getOperacao();
					}
					
					//SICOB NOSSO N�MERO 16 POSI��ES
					bb = new BoletoBancarioCaixaSICOB_nossoNum16pos(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
							operacao, 																											/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(numeroConta, "0", 6, false),															/* N�mero da Conta do Cedente 				*/
							new Integer(5), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(15), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
							"SR", 																											/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N");					
				} else if(BoletoBancarioCaixaSICOB_nossoNum11posComRegistro.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())){
					String numeroConta = numero.replaceAll("[\\.-]", "") + dvnumero.replaceAll("[\\.-]", "");
					String operacao = "870";
					if(documento.getContacarteira().getCarteira() != null && 
							org.apache.commons.lang.StringUtils.isNotEmpty(documento.getContacarteira().getOperacao())){
						operacao = documento.getContacarteira().getOperacao();
					}
					
					//SICOB NOSSO N�MERO 11 POSI��ES COM REGISTRO
					bb = new BoletoBancarioCaixaSICOB_nossoNum11posComRegistro(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
							operacao, 																											/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(numeroConta, "0", 9, false),															/* N�mero da Conta do Cedente 				*/
							new Integer(8), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(10), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
							documento.getContacarteira().getCarteira(), 																											/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N");
				} else if(BoletoBancarioCaixaSICOB_nossoNum11pos.class.equals(documento.getContacarteira().getBoletocobranca().getLeiauteboleto())){
//					String numeroConta = documento.getConta().getNumero().replaceAll("[\\.-]", "") + documento.getConta().getDvnumero().replaceAll("[\\.-]", "");
					String operacao = "870";
					if(documento.getContacarteira().getCarteira() != null && 
							org.apache.commons.lang.StringUtils.isNotEmpty(documento.getContacarteira().getOperacao())){
						operacao = documento.getContacarteira().getOperacao();
					}
					
					//SICOB NOSSO N�MERO 11 POSI��ES
					bb = new BoletoBancarioCaixaSICOB_nossoNum11pos(
							cdBanco, 																										/* N�mero do Banco 							*/
							documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
							new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
							operacao, 																											/* Opera��o C�digo Cedente 					*/
							StringUtils.stringCheia(documento.getContacarteira().getConvenio(), "0", 9, false),															/* N�mero da Conta do Cedente 				*/
							new Integer(8), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
							fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
							(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																			/* Nosso N�mero 							*/
							new Integer(10), 																								/* Tamanho do Nosso N�mero 					*/
							documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
							"SR", 																											/* Carteira 								*/
							"DM", 																											/* Esp�cie Documento 						*/
							documento.getContacarteira().getAceito() != null && !"".equals(documento.getContacarteira().getAceito()) ? documento.getContacarteira().getAceito() : "N");
				} else{
					throw new Exception("Tipo de boleto de cobran�a n�o definido para o documento " + documento.getCddocumento());
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
				throw new SinedException("Identificador da Empresa no Banco inv�lido.");
			} catch (Exception e) {
				throw new SinedException(e.getMessage());
			} 
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(218)) { // Bom sucesso (DIGITAL)
			Set<Boletodigital> listaBoletodigital = documento.getListaBoletodigital();
			Boletodigital boletodigital = listaBoletodigital != null && listaBoletodigital.size() > 0 ? listaBoletodigital.iterator().next() : null;
			cdBanco = "218-6";
			bb = new BoletoBancarioDigitalBs2(
					cdBanco, 
					documento.getConta().getAgencia().substring(0, 4),
					new Integer(4), 
					StringUtils.stringCheia(numero + dvnumero, "0", 7, false), 
					new Integer(6), 
					new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 
					boletodigital != null ? boletodigital.getNossonumero() : "", 
					new Integer(11), 
					documento.getValorboleto().getValue().toString(), 
					boletodigital != null ? boletodigital.getCarteira() : "", 
					documento.getContacarteira().getEspeciedocumento(), 
					documento.getContacarteira().getAceito(), 
					boletodigital != null ? boletodigital.getLinhadigitavel() : "", 
					boletodigital != null ? boletodigital.getCodigodebarras() : "");
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(341)) { // Ita�
			cdBanco = "341-7";
//			documento.getConta().setNumero(documento.getConta().getNumero() + documento.getConta().getDvnumero());
			bb = new BoletoBancarioItau(
					cdBanco, 																								/* N�mero do Banco 						*/
					documento.getConta().getAgencia().substring(0, 4), 														/* N�mero da Ag�ncia 					*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia 		*/
					StringUtils.stringCheia(numero + dvnumero, "0", 6, false), 		/* N�mero da Conta do Cedente 			*/
					new Integer(5), 																						/* Tamanho do N�mero da Conta sem o DV 	*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 		*/
					documento.getNossonumero() != null ? documento.getNossonumero() : documento.getCddocumento().toString(), 																	/* Nosso N�mero 						*/
					new Integer(8), 																						/* Tamanho do Nosso N�mero 				*/
					documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 						*/
					documento.getContacarteira().getCarteira(), 	/* Carteira 							*/
					documento.getConta().getLocalpagamento(),
					documento.getContacarteira().getEspeciedocumento(), 																									/* Esp�cie Documento 					*/
					documento.getContacarteira().getAceito(), 																									/* Aceite 								*/
					documento.getContacarteira().getEscritural());	/*Carteira escritural*/
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(748)) { // Sicred
			cdBanco = "748-X";
			String variacaocarteira = documento.getContacarteira().getVariacaocarteira();
			if(variacaocarteira == null || variacaocarteira.equals("")){
				variacaocarteira = "3";
			}
			bb = new BoletoBancarioSicred(
					cdBanco, 																								/* N�mero do Banco 						*/
					documento.getConta().getAgencia().substring(0, 4), 														/* N�mero da Ag�ncia 					*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia 		*/
					StringUtils.stringCheia(numero, "0", 5, false), 		/* N�mero da Conta do Cedente 			*/
					new Integer(5), 																						/* Tamanho do N�mero da Conta sem o DV 	*/
					documento.getContacarteira().getConvenio(),
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 		*/
					documento.getNossonumero() != null ? StringUtils.stringCheia(documento.getNossonumero(), "0", 9, false) : documento.getCddocumento().toString(), 																	/* Nosso N�mero 						*/
					new Integer(5), 																						/* Tamanho do Nosso N�mero 				*/
					documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 						*/
					documento.getContacarteira().getCarteira(), 															/* Carteira 							*/
					variacaocarteira,
					documento.getConta().getLocalpagamento(),
					documento.getContacarteira().getEspeciedocumento(),														/* Esp�cie Documento 					*/
					documento.getContacarteira().getAceito()); 
		}

		else if (documento.getConta().getBanco().getNumero().equals(409)) { // Unibanco
			cdBanco = "409-0";
			bb = new BoletoBancarioUnibanco(
					cdBanco, 																								/* N�mero do Banco 							*/
					documento.getConta().getAgencia(), 																		/* N�mero da Ag�ncia 						*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia 			*/
					numero, 																		/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
					documento.getContacarteira().getConvenio(), 																	/* C�digo do Cliente (Com DV) 				*/
					new Integer(7), 																						/* Tamanho do C�digo do Cliente (Com DV) 	*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
					new Integer(14), 																						/* Tamanho do Nosso N�mero 					*/
					documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 							*/
					"ESP", 																									/* Carteira  								*/
					"DM", 																									/* Esp�cie Documento 						*/
					"N"); 																									/* Aceite 									*/
		}

		else if (documento.getConta().getBanco().getNumero().equals(237)) { // Bradesco
			cdBanco = "237-2";
			if(documento.getContacarteira().getEspeciedocumento() == null || documento.getContacarteira().getEspeciedocumento().equals("")){
				documento.getContacarteira().setEspeciedocumento("RC");
			}
			if(documento.getContacarteira().getAceito() == null || documento.getContacarteira().getAceito().equals("")){
				documento.getContacarteira().setAceito("N");
			}
//			documento.getConta().setNumero(documento.getConta().getNumero() + documento.getConta().getDvnumero());
			bb = new BoletoBancarioBradesco(
					cdBanco, 																							/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), /* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																					/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero + dvnumero, "0", 8, false), 	/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																					/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()),/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																/* Nosso N�mero 							*/
					new Integer(11), 																					/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(),													/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 																/* Carteira									*/
					documento.getContacarteira().getEspeciedocumento(), 																								/* Esp�cie Documento	 					*/
					documento.getContacarteira().getAceito()); 																								/* Aceite 									*/
		}

		else if (documento.getConta().getBanco().getNumero().equals(1)) { // Banco do Brasil
			cdBanco = "001-9";
			
			if(documento.getContacarteira().getConvenio().length() == 6 && (documento.getContacarteira().getCarteira().equals("18") || documento.getContacarteira().getCarteira().equals("17"))){
				bb = new BoletoBancarioBancoDoBrasil_conv6pos(
						cdBanco, 																											/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																									/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(numero + dvnumero, "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
						new Integer(8), 																									/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 				/* Data de Vencimento do Boleto 			*/
						(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																				/* Nosso N�mero 							*/
						new Integer(5), 																									/* Tamanho do Nosso N�mero sem o DV 		*/
						documento.getValorboleto().getValue().toString(),													/* Valor do Boleto 							*/
						documento.getContacarteira().getCarteira(), 																												/* Carteira 								*/
						"R$", 																												/* Esp�cie Documento 						*/
						"N", 																												/* Aceite 									*/
						documento.getContacarteira().getConvenio());																				/* Convenio 								*/
			} else if(documento.getContacarteira().getConvenio().length() == 7 && documento.getContacarteira().getCarteira().equals("18")){
				bb = new BoletoBancarioBancoDoBrasil_conv7pos(
						cdBanco, 																											/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																									/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(numero + dvnumero, "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
						new Integer(8), 																									/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 				/* Data de Vencimento do Boleto 			*/
						(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																				/* Nosso N�mero 							*/
						new Integer(10), 																									/* Tamanho do Nosso N�mero sem o DV 		*/
						documento.getValorboleto().getValue().toString(),													/* Valor do Boleto 							*/
						documento.getContacarteira().getCarteira(), 																												/* Carteira 								*/
						"R$", 																												/* Esp�cie Documento 						*/
						"N", 																												/* Aceite 									*/
						documento.getContacarteira().getConvenio(),
						documento.getConta().getLocalpagamento());	
			} else if(documento.getContacarteira().getConvenio().length() == 7 && documento.getContacarteira().getCarteira().equals("17")){
				bb = new BoletoBancarioBancoDoBrasil240_conv7pos(
						cdBanco, 																											/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																									/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(numero + dvnumero, "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
						new Integer(8), 																									/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 				/* Data de Vencimento do Boleto 			*/
						(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																				/* Nosso N�mero 							*/
						new Integer(10), 																									/* Tamanho do Nosso N�mero sem o DV 		*/
						documento.getValorboleto().getValue().toString(),													/* Valor do Boleto 							*/
						documento.getContacarteira().getCarteira(), 																												/* Carteira 								*/
						"DM", 																												/* Esp�cie Documento 						*/
						"N", 																												/* Aceite 									*/
						documento.getContacarteira().getConvenio());				
			} else { 
				//nesse caso a agencia e conta devem ser preenchidas com o d�gito e sem o h�fen nos campos agencia e conta, desconsiderando os campos dos d�gitos
				bb = new BoletoBancarioBancoDoBrasil(
						cdBanco, 																								/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 5, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
						new Integer(8), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
						(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
						new Integer(11), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
						documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 							*/
						"11", 																									/* Carteira 								*/
						"DM", 																									/* Esp�cie Documento 						*/
						"N", 																									/* Aceite 									*/
						documento.getContacarteira().getConvenio()); 																	/* Convenio 								*/
			}
		}

		else if (documento.getConta().getBanco().getNumero().equals(356)) { // Banco Real
			cdBanco = "356-5";
			bb = new BoletoBancarioBancoReal(
					cdBanco, 																								/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 7, false), 		/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
					new Integer(7), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 							*/
					"20", 																									/* Carteira 								*/
					"DM", 																									/* Esp�cie Documento	 					*/
					"N"); 																									/* Aceite 									*/
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(33)) { // Santander (Antigo Real)
			if (!parametroSantander.equals("TRUE")){	//ANTIGO BOLETO SANTANDER
				cdBanco = "033-7";
				bb = new BoletoBancarioBancoReal(
						cdBanco, 																								/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 7, false), 		/* N�mero da Conta do Cedente 				*/
						new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
						(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
						new Integer(7), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
						documento.getValorboleto().getValue().toString(),														/* Valor do Boleto 							*/
						"20", 																									/* Carteira 								*/
						"DM", 																									/* Esp�cie Documento	 					*/
						"N"); 																									/* Aceite 									*/
			} else { 			//NOVO BOLETO SANTANDER
				cdBanco = "033-7";
				bb = new BoletoBancarioBancoSantander(
						cdBanco, 																								/* N�mero do Banco 							*/
						StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
						new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
						StringUtils.stringCheia(documento.getContacarteira().getConvenio().replaceAll("[\\.-]", ""), "0", 7, false), 	/* N�mero da Conta do Cedente 				*/
						new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
						fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
						geraNovoNumeroComDv((documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString())), 											/* Nosso N�mero 							*/
						new Integer(14), 																						/* Tamanho do Nosso N�mero com o DV 		*/
						documento.getValorboleto().getValue().toString(),														/* Valor do Boleto 							*/
						documento.getContacarteira().getCarteira(), 																	/* Carteira									*/
						documento.getContacarteira().getEspeciedocumento(), 															/* Esp�cie Documento	 					*/
						documento.getContacarteira().getAceito());
			}
		}

		else if (documento.getConta().getBanco().getNumero().equals(389)) { // Mercantil
			cdBanco = "389-1";
			bb = new BoletoBancarioMercantil(
					cdBanco, 																								/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
					new Integer(9), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()),	/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
					new Integer(10), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(), 									/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 																	/* Carteira 								*/
					"R$", 																									/* Esp�cie Documento 						*/
					"S"); 																									/* Aceite 									*/
		}
		
//		else if (documento.getConta().getBanco().getNumero().equals(237)) { // Rural
//			cdBanco = "237-2";
//			bb = new BoletoBancarioMercantil(
//					cdBanco, 																								/* N�mero do Banco 							*/
//					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
//					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
//					StringUtils.stringCheia(documento.getConta().getNumero().replaceAll("[\\.-]", ""), "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
//					new Integer(9), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
//					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()),	/* Data de Vencimento do Boleto 			*/
//					documento.getCddocumento().toString(), 																	/* Nosso N�mero 							*/
//					new Integer(10), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
//					documento.getValorboleto().getValue().toString(), 									/* Valor do Boleto 							*/
//					documento.getConta().getCarteira(), 																	/* Carteira 								*/
//					"R$", 																									/* Esp�cie Documento 						*/
//					"S"); 																									/* Aceite 									*/
//		}

		else if (documento.getConta().getBanco().getNumero().equals(756)) { // Bancoob
			cdBanco = "756-0";
			String nossoNumero = documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString();
			if(nossoNumero != null && nossoNumero.length() > 7){
				//throw new SinedException("Corrigir a quantidade de caracteres do Nosso N�mero (quantidade permitida s�o 7 + DV)");
			}
			bb = new BoletoBancarioBancoob(
					cdBanco, 																								/* N�mero do Banco							*/
					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(documento.getContacarteira().getConvenio().replaceAll("[\\.-]", ""), "0", 7, false), 	/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/	
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
					nossoNumero, 																							/* Nosso N�mero 							*/
					new Integer(8), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(), 														/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 															/* Carteira 								*/
					documento.getContacarteira().getEspeciedocumento(),														/* Esp�cie Documento 						*/
					documento.getContacarteira().getAceito(),																/* Aceite 									*/
					documento.getContacarteira().getTipocobranca()); 														/* Modalidade								*/
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(399)) { // HSBC
			cdBanco = "399-9";
			String nossonumero = documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString();
			bb = new BoletoBancarioHSBC(
					cdBanco, 																								/* N�mero do Banco							*/
					StringUtils.stringCheia(documento.getConta().getAgencia(), "0", 4, false), 								/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero + dvnumero, "0", 7, false), 		/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																						/* Tamanho do N�mero da Conta sem o DV 		*/	
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 			*/
					nossonumero, 																							/* Nosso N�mero 							*/
					new Integer(10), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(), 														/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 															/* Carteira									*/
					documento.getContacarteira().getEspeciedocumento(), 													/* Esp�cie Documento	 					*/
					documento.getContacarteira().getAceito(),																/* Aceite 									*/
					documento.getContacarteira().getConvenio()); 															/* Conv�nio 									*/
		}
		
//		BOLETO DO BANCO RURAL
		else if (documento.getConta().getBanco().getNumero().equals(453)){
			cdBanco = "453-7";
			bb = new BoletoBancarioRural(
					cdBanco, 																								/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 	/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 9, false), 		/* N�mero da Conta do Cedente 				*/
					new Integer(9), 																						/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()),	/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																	/* Nosso N�mero 							*/
					new Integer(10), 																						/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(), 														/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 																	/* Carteira 								*/
					"DM", 																									/* Esp�cie Documento 						*/
					"N");  																									/* Aceite 									*/
		}
		
//		BOLETO DO CECRED
		else if (documento.getConta().getBanco().getNumero().equals(85)){
			cdBanco = "085-1";
			bb = new BoletoBancarioCecred(
					cdBanco, 																											/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), 	/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																									/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero + dvnumero, "0", 9, false), 															/* N�mero da Conta do Cedente 				*/
					new Integer(8), 																									/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 				/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), /* Nosso N�mero 							*/
					new Integer(17), 																									/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(),																	/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 																		/* Carteira 								*/
					documento.getContacarteira().getEspeciedocumento(), 																/* Esp�cie Documento 						*/
					documento.getContacarteira().getAceito(), 																			/* Aceite 									*/
					documento.getContacarteira().getConvenio());																		/* Conv�nio									*/
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(41)){ //Banrisul
			cdBanco = "041-8";
			bb = new BoletoBancarioBanrisul(
					cdBanco, 																																				/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia().replaceAll("[\\.-]", ""), "0", 4, false), 													/* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																																		/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 7, true, false), 																				/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																																		/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 													/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()),	/* Nosso N�mero 							*/
					new Integer(8), 																																		/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(),																										/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 															/* Carteira									*/
					documento.getContacarteira().getEspeciedocumento(), 													/* Esp�cie Documento	 					*/
					documento.getContacarteira().getAceito(),																											/* Conv�nio									*/
					numero);																								/* N�mero da Conta do Cedente 				*/
		}
		/*BANCO REGIONAL DE BRAS�LIA*/
		else if (documento.getConta().getBanco().getNumero().equals(70)) { // Banco Regional de Bras�lia
			cdBanco = "070-1";
			bb = new BoletoBancarioBancoRegionalBrasilia(
					cdBanco, 																										/* N�mero do Banco 							*/
					documento.getConta().getAgencia(), 																				/* N�mero da Ag�ncia 						*/
					new Integer(4), 																								/* Tamanho do N�mero da Ag�ncia 			*/
					numero.replaceAll("[\\.-]","").substring(0, 3), 																/* Opera��o C�digo Cedente 					*/
					StringUtils.stringCheia(numero.replaceAll("[\\.-]", ""), "0", 10, false),										/* N�mero da Conta do Cedente 				*/
					new Integer(10), 																								/* Tamanho do N�mero da Conta sem o DV 		*/
					StringUtils.stringCheia(documento.getContacarteira().getConvenio(), "0", 10, false), 							/* C�digo do Cliente (Com DV) 				*/
					new Integer(6), 																								/* Tamanho do C�digo do Cliente (Sem DV)	*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 			/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 		/* Nosso N�mero 							*/
					new Integer(12), 																								/* Tamanho do Nosso N�mero 					*/
					documento.getValorboleto().getValue().toString(), 																/* Valor do Boleto 							*/
					documento.getContacarteira().getTipocobranca(),																	/* Modalidade de cobranca 					*/
					"000",																											/* Incremento do campo livre				*/
					documento.getContacarteira().getCarteira(), 																	/* Carteira									*/
					documento.getContacarteira().getEspeciedocumento(), 															/* Esp�cie Documento	 					*/
					documento.getContacarteira().getAceito());																		/* Aceite									*/
																													
		}
		
		else if (documento.getConta().getBanco().getNumero().equals(422)) { // Banco Safra
			cdBanco = "422-7";
			bb = new BoletoBancarioSafra(
					cdBanco, 																								/* N�mero do Banco 						*/
					documento.getConta().getAgencia(), 														/* N�mero da Ag�ncia 					*/
					new Integer(4), 																						/* Tamanho do N�mero da Ag�ncia 		*/
					StringUtils.stringCheia(numero + dvnumero, "0", 9, false), 		/* N�mero da Conta do Cedente 			*/
					new Integer(9), 																						/* Tamanho do N�mero da Conta sem o DV 	*/
					documento.getContacarteira().getConvenio(),
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()), 	/* Data de Vencimento do Boleto 		*/
					documento.getNossonumero() != null ? StringUtils.stringCheia(documento.getNossonumero(), "0", 9, false) : documento.getCddocumento().toString(), 																	/* Nosso N�mero 						*/
					new Integer(9), 																						/* Tamanho do Nosso N�mero 				*/
					documento.getValorboleto().getValue().toString(),										/* Valor do Boleto 						*/
					documento.getContacarteira().getCarteira(), 															/* Carteira 							*/
					"",
					documento.getConta().getLocalpagamento(),
					documento.getContacarteira().getEspeciedocumento(),														/* Esp�cie Documento 					*/
					documento.getContacarteira().getAceito()); 
																													
		}

		else if (documento.getConta().getBanco().getNumero().equals(84)) { // Uniprime
			cdBanco = "084-1";
			if(documento.getContacarteira().getAceito() == null || documento.getContacarteira().getAceito().equals("")){
				documento.getContacarteira().setAceito("N");
			}
			bb = new BoletoBancarioUniprime(
					cdBanco, 																							/* N�mero do Banco 							*/
					StringUtils.stringCheia(documento.getConta().getAgencia() + documento.getConta().getDvagencia(), "0", 5, false), /* N�mero da Ag�ncia do Cedente 			*/
					new Integer(4), 																					/* Tamanho do N�mero da Ag�ncia sem o DV 	*/
					StringUtils.stringCheia(numero + dvnumero, "0", 8, false), 	/* N�mero da Conta do Cedente 				*/
					new Integer(7), 																					/* Tamanho do N�mero da Conta sem o DV 		*/
					fatorvencimentozerado ? "" : new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()),/* Data de Vencimento do Boleto 			*/
					(documento.getNossonumero() != null && !"".equals(documento.getNossonumero()) ? documento.getNossonumero() : documento.getCddocumento().toString()), 																/* Nosso N�mero 							*/
					new Integer(11), 																					/* Tamanho do Nosso N�mero sem o DV 		*/
					documento.getValorboleto().getValue().toString(),													/* Valor do Boleto 							*/
					documento.getContacarteira().getCarteira(), 																/* Carteira									*/
					documento.getContacarteira().getEspeciedocumento(), 																								/* Esp�cie Documento	 					*/
					documento.getContacarteira().getAceito()); 																								/* Aceite 									*/
		}
		
		else {
			throw new SinedException("C�digo do Banco inv�lido => " + documento.getConta().getBanco().getNumero());
		}
		
		return bb;
	}

	/**
	 * Faz refer�ncia a outro service.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findForBoleto
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Documento> findForBoleto(String whereIn) {
		return documentoService.findForBoleto(whereIn);
	}

	public EDIRetorno240 processaArquivoRetornoCAIXASINCO240(Arquivo arquivo)
			throws Exception {
		String arquivoStr = new String(arquivo.getContent());

		String[] linhas = arquivoStr.split("\\r?\\n");

		EDIRetorno240 retorno240 = new EDIRetorno240();
		String cdtiporegistro, linha, cdsegmento;
		EDIRegistroDetalheTRetorno240 detalheT = new EDIRegistroDetalheTRetorno240();

		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(7, 8);

			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetorno240 header = new EDIHeaderRetorno240(linha);
				retorno240.setHeaderRetorno240(header);
			} else if (cdtiporegistro.equals("1")) { // Header do Lote
				@SuppressWarnings("unused")
				EDIHeaderLoteRetorno240 header = new EDIHeaderLoteRetorno240(
						linha);
			} else if (cdtiporegistro.equals("3")) { // Detalhe
				cdsegmento = linha.substring(13, 14);

				if (cdsegmento.equals("T")) { // Segmento T
					detalheT = new EDIRegistroDetalheTRetorno240(linha);
				} else if (cdsegmento.equals("U")) { // Segmento U
					EDIRegistroDetalheURetorno240 detalheU = new EDIRegistroDetalheURetorno240(linha);
					
					detalheT.setValorPrincipal(detalheU.getValorpago());
					detalheT.setDtCredito(detalheU.getDtcredito());
					detalheT.setDtOcorrencia(detalheU.getDtocorrencia());
					detalheT.setDtDebitoTarifa(detalheU.getDtdebitotarifa());

					retorno240.getRegistroDetalheTRetorno240().add(detalheT);
				} else {
					throw new SinedException(
							"Identificador de Segmento inv�lido!");
				}
			} else if (cdtiporegistro.equals("5")) { // Trailer do Lote
				@SuppressWarnings("unused")
				EDITrailerLoteRetorno240 trailer = new EDITrailerLoteRetorno240(
						linha);
			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
				@SuppressWarnings("unused")
				EDITrailerRetorno240 trailer = new EDITrailerRetorno240(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}

		return retorno240;
	}

	public EDIRetorno400 processaArquivoRetornoBradesco400(Arquivo arquivo)
			throws Exception {
		String arquivoStr = new String(arquivo.getContent());

		String[] linhas = arquivoStr.split("\\r?\\n");

		EDIRetorno400 retorno400 = new EDIRetorno400();
		String cdtiporegistro, linha, cdagencia = null, cdconta = null;
		EDIRegistroRetorno400 detalhe = new EDIRegistroRetorno400();

		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(0, 1);

			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetorno400 header = new EDIHeaderRetorno400(linha);
				retorno400.setHeaderRetorno400(header);
			} else if (cdtiporegistro.equals("1")) { // Detalhe

				detalhe = new EDIRegistroRetorno400(linha);
				retorno400.getRegistroRetorno400().add(detalhe);

				if (cdagencia == null && cdconta == null) {
					cdagencia = linha.substring(24, 29);
					cdconta = linha.substring(29, 36);

					retorno400.setAgencia(cdagencia);
					retorno400.setConta(cdconta);
				}

			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
				@SuppressWarnings("unused")
				EDITrailerRetorno400 trailer = new EDITrailerRetorno400(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}

		return retorno400;
	}
	
	public EDIRetornoSantander400 processaArquivoRetornoSantander400(Arquivo arquivo) throws Exception {
		String arquivoStr = new String(arquivo.getContent());
		
		String[] linhas = arquivoStr.split("\\r?\\n");
		
		EDIRetornoSantander400 retorno400 = new EDIRetornoSantander400();
		String cdtiporegistro, linha, cdagencia = null, cdconta = null;
		EDIRegistroRetornoSantander400 detalhe = new EDIRegistroRetornoSantander400();
		
		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(0, 1);
			
			if (cdtiporegistro.equals("0")) { // Header do Arquivo
//				EDIHeaderRetorno400 header = new EDIHeaderRetorno400(linha);
//				retorno400.setHeaderRetorno400(header);
			} else if (cdtiporegistro.equals("1")) { // Detalhe
				
				detalhe = new EDIRegistroRetornoSantander400(linha);
				retorno400.getRegistroRetornoSantander400().add(detalhe);
				
				if (cdagencia == null && cdconta == null) {
					cdagencia = linha.substring(17,21);
					cdconta = linha.substring(21,29);
					
					retorno400.setAgencia(cdagencia);
					retorno400.setConta(cdconta);
				}
				
			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
//				@SuppressWarnings("unused")
//				EDITrailerRetorno400 trailer = new EDITrailerRetorno400(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}
		
		return retorno400;
	}
	
	public EDIRetornoItau400 processaArquivoRetornoItau400(Arquivo arquivo)
			throws Exception {
		String arquivoStr = new String(arquivo.getContent());

		String[] linhas = arquivoStr.split("\\r?\\n");

		EDIRetornoItau400 retornoItau400 = new EDIRetornoItau400();
		String cdtiporegistro, linha, cdagencia = null, cdconta = null;
		EDIRegistroRetornoItau400 detalhe = new EDIRegistroRetornoItau400();

		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(0, 1);

			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetorno400 header = new EDIHeaderRetorno400(linha);
				retornoItau400.setHeaderRetorno400(header);
			} else if (cdtiporegistro.equals("1")) { // Detalhe

				detalhe = new EDIRegistroRetornoItau400(linha);				
				retornoItau400.getRegistroRetornoItau400().add(detalhe);

				if (cdagencia == null && cdconta == null) {
					cdagencia = linha.substring(17, 21);
					cdconta = linha.substring(23, 28);

					retornoItau400.setAgencia(cdagencia);
					retornoItau400.setConta(cdconta);
				}

			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
				@SuppressWarnings("unused")
				EDITrailerRetorno400 trailer = new EDITrailerRetorno400(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}

		return retornoItau400;
	}	
	
	public EDIRetornoBB240 processaArquivoRetornoBancoBrasil240(Arquivo arquivo) throws Exception {
		String arquivoStr = new String(arquivo.getContent());
		
		String[] linhas = arquivoStr.split("\\r?\\n");
		
		EDIRetornoBB240 retornoBB240 = new EDIRetornoBB240();
		String cdtiporegistro, linha, cdsegmento;
		EDIRegistroDetalheTRetornoBB240 detalheT = new EDIRegistroDetalheTRetornoBB240();
		
		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(7, 8);
		
			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetornoBB240 header = new EDIHeaderRetornoBB240(linha);
				retornoBB240.setHeaderRetornoBB240(header);
			} else if (cdtiporegistro.equals("1")) { // Header do Lote
				@SuppressWarnings("unused")
				EDIHeaderLoteRetorno240 header = new EDIHeaderLoteRetorno240(
						linha);
			} else if (cdtiporegistro.equals("3")) { // Detalhe
				cdsegmento = linha.substring(13, 14);
		
				if (cdsegmento.equals("T")) { // Segmento T
					detalheT = new EDIRegistroDetalheTRetornoBB240(linha);
				} else if (cdsegmento.equals("U")) { // Segmento U
					EDIRegistroDetalheURetorno240 detalheU = new EDIRegistroDetalheURetorno240(
							linha);
					detalheT.setValorPrincipal(detalheU.getValorpago());
					detalheT.setDtCredito(detalheU.getDtcredito());
					detalheT.setDtOcorrencia(detalheU.getDtocorrencia());
		
					retornoBB240.getRegistroDetalheTRetornoBB240().add(detalheT);
				} else {
					throw new SinedException(
							"Identificador de Segmento inv�lido!");
				}
			} else if (cdtiporegistro.equals("5")) { // Trailer do Lote
				@SuppressWarnings("unused")
				EDITrailerLoteRetorno240 trailer = new EDITrailerLoteRetorno240(
						linha);
			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
				@SuppressWarnings("unused")
				EDITrailerRetorno240 trailer = new EDITrailerRetorno240(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}
		
		return retornoBB240;
	}
	
	public EDIRetornoBB400 processaArquivoRetornoBancoBrasil400(Arquivo arquivo)	throws Exception {
		String arquivoStr = new String(arquivo.getContent());
		
		String[] linhas = arquivoStr.split("\\r?\\n");
		
		EDIRetornoBB400 retornoBB400 = new EDIRetornoBB400();
		String cdtiporegistro, linha, cdagencia = null, cdconta = null, convenio = null;
		EDIRegistroRetornoBB400 detalhe = new EDIRegistroRetornoBB400();
		
		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(0, 1);
		
			if (cdtiporegistro.equals("5"))
				continue;
			
			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetorno400 header = new EDIHeaderRetorno400(linha);
				retornoBB400.setHeaderRetorno400(header);
			} else if (cdtiporegistro.equals("7")) { // Detalhe
		
				detalhe = new EDIRegistroRetornoBB400(linha);				
				retornoBB400.getRegistroRetornoBB400().add(detalhe);
		
				if (cdagencia == null && cdconta == null && convenio == null) {
					cdagencia = linha.substring(17, 21);
					cdconta = linha.substring(22, 30);
					convenio = linha.substring(31, 38);
		
					retornoBB400.setAgencia(cdagencia);
					retornoBB400.setConta(cdconta);
					retornoBB400.setConvenio(convenio);
				}
		
			} else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
				@SuppressWarnings("unused")
				EDITrailerRetorno400 trailer = new EDITrailerRetorno400(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}
		
		return retornoBB400;
	}	
	

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#loadForRetorno
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento loadForRetorno(Documento documento) {
		return contareceberDAO.loadForRetorno(documento);
	}
	
	public Documento loadForRetorno(String documentoNumero, boolean buscaNossonumero, boolean buscaNumero, boolean nossoNumeroNulo, Money valorDocBusca) {
		return contareceberDAO.loadForRetorno(documentoNumero, buscaNossonumero, buscaNumero, nossoNumeroNulo, valorDocBusca);
	}

	/**
	 * M�todo que envia boleta para cliente
	 * 
	 * @param documento
	 * @param report
	 * @throws Exception 
	 * @Author Tom�s Rabelo
	 */
	public void enviaBoleto(Documento documento, Report report, String remetente, String nomeArquivo, Contato contato, EmailCobranca emailCobranca) throws Exception {
		Empresa empresa = documento.getEmpresa() != null && documento.getEmpresa().getCdpessoa() != null ? documento.getEmpresa() : empresaService.loadPrincipal();
		empresaService.setInformacoesPropriedadeRural(empresa, true);
		
		String template = null;
		ReportTemplateBean cdreporttemplate = empresa.getCdreporttemplateboleto();
		String assunto = "Envio de boleto";
		StringBuilder urlComCaptcha = new StringBuilder();
		StringBuilder urlDireto = new StringBuilder();
		
		urlComCaptcha.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(documento.getCddocumento())
			.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
		
		urlDireto.append(SinedUtil.getUrlWithContext()).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(documento.getCddocumento())
		.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
		
		if (cdreporttemplate != null && documento.getEmpresa() != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
			ReportTemplateBean reportTemplate = reportTemplateService.loadForEntrada(cdreporttemplate);
			assunto = reportTemplate.getAssunto();
			template = reportTemplate.getLeiaute();
			template = template.replace("{nomeCliente}", (documento != null && documento.getPessoa() != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getPessoa().getNome())) ? documento.getPessoa().getNome() : "");
			template = template.replace("{emailCliente}", (documento != null && documento.getPessoa() != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getPessoa().getEmail())) ? documento.getPessoa().getEmail() : "");
			template = template.replace("{nomeContatoCliente}", (documento != null && documento.getPessoa() != null && documento.getPessoa().getCliente() != null && documento.getPessoa().getCliente().getContatoResponsavel() != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getPessoa().getCliente().getContatoResponsavel().getNome())) ? documento.getPessoa().getCliente().getContatoResponsavel().getNome() : "");
			template = template.replace("{emailContatoCliente}", (documento != null && documento.getPessoa() != null && documento.getPessoa().getCliente() != null && documento.getPessoa().getCliente().getContatoResponsavel() != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getPessoa().getCliente().getContatoResponsavel().getEmailcontato())) ? documento.getPessoa().getCliente().getContatoResponsavel().getEmailcontato() : "");
			
			template = template.replace("{descricaoContaReceber}", (documento != null && documento != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getDescricao())) ? documento.getDescricao() : "");
			template = template.replace("{vencimentoContaReceber}", (documento.getDtvencimento() != null) ? SinedDateUtils.toString(documento.getDtvencimento()) : "");
			template = template.replace("{numeroDocumento}", (documento != null && documento.getCddocumento() != null && org.apache.commons.lang.StringUtils.isNotEmpty(documento.getCddocumento().toString())) ? documento.getCddocumento().toString() : "");
			template = template.replace("{linkDireto}", (urlDireto != null && org.apache.commons.lang.StringUtils.isNotEmpty(urlDireto.toString())) ? urlDireto.toString() : "");
			template = template.replace("{linkComCaptcha}", (urlComCaptcha != null && org.apache.commons.lang.StringUtils.isNotEmpty(urlComCaptcha.toString())) ? urlComCaptcha.toString() : "");
			template = template.replace("{valorOriginal}", (documento != null && documento.getValor() != null) ? documento.getValor().toString() : "");
			template = template.replace("{valorAtual}", (documento != null && documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null) ? documento.getAux_documento().getValoratual().toString() : "");
			
			template = template.replace("{nomeEmpresa}", (empresa != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getNomeProprietarioPrincipalOuEmpresa())) ? empresa.getNomeProprietarioPrincipalOuEmpresa() : "");
			template = template.replace("{emailEmpresa}", (empresa != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getEmail())) ? empresa.getEmail() : "");
			template = template.replace("{telefoneEmpresa}", (empresa != null && empresa.getTelefone() != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getTelefone().getTelefone())) ? empresa.getTelefone().getTelefone() : "");
			template = template.replace("{textoautenticidadeboleto}", (empresa != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getTextoautenticidadeboleto())) ? empresa.getTextoautenticidadeboleto() : "");
			template = template.replace("{complementoTextoBoleto}", (empresa != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getTextoboleto())) ? empresa.getTextoboleto() : "");
		} else if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(documento.getEmpresa().getFormaEnvioBoleto())) { 
			TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioBoletoEmailAnexo.tpl");
			try {
				template = templateAux
								.assign("cliente", documento.getPessoa().getNome())
								.assign("descricao", documento.getDescricao() != null ? documento.getDescricao().trim() : "")
								.assign("valor", documento.getAux_documento().getValoratual().toString())
								.assign("dtvencimento", new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()))
								.assign("empresa", empresa.getNomeProprietarioPrincipalOuEmpresa())
								.assign("complemento", empresa.getTextoboleto() != null ? empresa.getTextoboleto().replaceAll("\n", "<BR>") : "")
								.getTemplate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
			TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioBoletoEmail.tpl");
			try {
				template = templateAux
								.assign("cliente", documento.getPessoa().getNome())
								.assign("descricao", documento.getDescricao() != null ? documento.getDescricao().trim() : "")
								.assign("valor", documento.getAux_documento().getValoratual().toString())
								.assign("dtvencimento", new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()))
								.assign("empresa", empresa.getNomeProprietarioPrincipalOuEmpresa())
								.assign("complemento", empresa.getTextoboleto() != null ? empresa.getTextoboleto().replaceAll("\n", "<BR>") : "")
								.assign("link", urlComCaptcha.toString())
								.getTemplate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioBoletoEmail.tpl");
			try {
				template = templateAux
								.assign("cliente", documento.getPessoa().getNome())
								.assign("descricao", documento.getDescricao() != null ? documento.getDescricao().trim() : "")
								.assign("valor", documento.getAux_documento().getValoratual().toString())
								.assign("dtvencimento", new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento()))
								.assign("empresa", empresa.getNomeProprietarioPrincipalOuEmpresa())
								.assign("complemento", empresa.getTextoboleto() != null ? empresa.getTextoboleto().replaceAll("\n", "<BR>") : "")
								.assign("link", urlComCaptcha.toString())
								.getTemplate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		String fileName = nomeArquivo != null && !nomeArquivo.equals("") ? nomeArquivo : "boleto_" + SinedUtil.datePatternForReport() + ".pdf";
		MergeReport mergeReport = new MergeReport(fileName);
		mergeReport.addReport(report);
		
		if(documento.getPessoa() != null && documento.getPessoa().getNome() != null && !"".equals(documento.getPessoa().getNome())){
			assunto += " - " + documento.getPessoa().getNome();
		}

		String remetenteEmail = "";
		
		Parametrogeral parametrogeral = parametrogeralService.findByNome("REPLY_TO_ENVIO_BOLETO");
		if (parametrogeral != null && parametrogeral.getValor() != null && !parametrogeral.getValor().equals("")){
			remetenteEmail = parametrogeral.getValor();
		} else if(remetente != null && !remetente.trim().equals("")){
			remetenteEmail = remetente;
		}else if(org.apache.commons.lang.StringUtils.isNotBlank(empresa.getEmailfinanceiro())){
			remetenteEmail = empresa.getEmailfinanceiro();
		} else {
			remetenteEmail = empresa.getEmail();
		}
		
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		String nomeRemetente = "";
		String enderecoEmail = "";
		Boolean novoEnvio = false;
		if(emailCobranca == null){
			if(contato != null){
				emailCobranca = new EmailCobranca(contato.getEmailcontato(), contato.getCdpessoa());
				
				List<EmailCobrancaDocumento> listaEmailCobrancaDocumento = new ArrayList<EmailCobrancaDocumento>();
				listaEmailCobrancaDocumento.add(new EmailCobrancaDocumento(documento));
				emailCobranca.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
				
				nomeRemetente = contato.getNome();
			}else{
				emailCobranca = new EmailCobranca(documento.getPessoa().getEmail(), documento.getPessoa().getCdpessoa());
				
				List<EmailCobrancaDocumento> listaEmailCobrancaDocumento = new ArrayList<EmailCobrancaDocumento>();
				listaEmailCobrancaDocumento.add(new EmailCobrancaDocumento(documento));
				emailCobranca.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
				
				nomeRemetente = documento.getPessoa().getNome();
			}
			EmailCobrancaService.getInstance().saveOrUpdate(emailCobranca);				
			novoEnvio = true;				
		}else
			nomeRemetente = emailCobranca.getPessoa().getNome();
		
		enderecoEmail = emailCobranca.getEmail();
		email.setEmailId(emailCobranca.getCdemailcobranca());
		
		Envioemail envioemail = envioemailService.registrarEnvio(remetenteEmail, assunto, template, Envioemailtipo.BOLETO,				
				documento.getPessoa(), enderecoEmail, nomeRemetente, email);
		
		if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
			try {			
				email.setFrom(remetenteEmail)
				.setSubject(assunto)
				.setTo(enderecoEmail)
				.addHtmlText(template+ EmailUtil
						.getHtmlConfirmacaoEmail(envioemail,enderecoEmail))
				.attachFileUsingByteArray(mergeReport
						.generateResource()
						.getContents(), fileName,"application/pdf", "1")
				.sendMessage();
			
			} catch (Exception e) {			
				if(novoEnvio){
					EmailCobrancaService.getInstance().delete(emailCobranca);
				}
				throw e;
			}
		} else {
			try {			
				email.setFrom(remetenteEmail)
				.setSubject(assunto)
				.setTo(enderecoEmail)
				.addHtmlText(template+ EmailUtil
						.getHtmlConfirmacaoEmail(envioemail,enderecoEmail))
				.sendMessage();
			
			} catch (Exception e) {			
				if(novoEnvio){
					EmailCobrancaService.getInstance().delete(emailCobranca);
				}
				throw e;
			}
		}
		
		if(emailCobranca !=null)
			EmailCobrancaService.getInstance().updateEmailCobranca(emailCobranca);		
	}
	
	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosContaReceber() {
		Date ultimocadastro = documentoService
				.getDtUltimoCadastro(Documentoclasse.OBJ_RECEBER);
		Date ultimaBaixa = movimentacaoService
				.getDtUltimaContaPagarReceberBaixada(Documentoclasse.OBJ_RECEBER);
		Integer atrasadas = documentoService
				.getQtdeAtrasadas(Documentoclasse.OBJ_RECEBER);
		Integer total = documentoService
				.getQtdeTotalContas(Documentoclasse.OBJ_RECEBER);
		return new DadosEstatisticosBean(DadosEstatisticosBean.CONTA_RECEBER,
				ultimocadastro, ultimaBaixa, atrasadas, total);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documento> findContasReceberForBuscaGeral(String busca) {
		return contareceberDAO.findContasReceberForBuscaGeral(busca);
	}

	/**
	 * M�todo de acesso ao DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Documento> findContas(String whereIn) {
		return contareceberDAO.findContas(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findContasOrigemVendas(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findContasOrigemVendas(String whereIn) {
		return contareceberDAO.findContasOrigemVendas(whereIn);
	}

	/**
	 * Retorna uma lista com os documentos que foram gerados durante a
	 * negocia��o da conta passada como par�metro
	 * 
	 * @param conta
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Documento> findParcelasNegociacao(Documento conta) {
		List<Documentohistorico> historico = documentohistoricoService
				.carregaHistorico(conta);
		String parcelas = "";
		for (Documentohistorico documentohistorico : historico) {
			String observacao = documentohistorico.getObservacao();
			if (observacao != null && observacao.contains("visualizaContaReceber")) {
				Pattern p = Pattern.compile("visualizaContaReceber\\((.*?)\\)");
				Matcher m = p.matcher(observacao);
				while (m.find()) {
					if (!parcelas.equals(""))
						parcelas += ",";
					parcelas += m.group(1);
				}
				break;
			}
		}
		if (!parcelas.equals(""))
			return documentoService.carregaDocumentos(parcelas);
		else
			return null;
	}

	/**
	 * M�todo que carrega lista de endere�o da pessoa de acordo com o tipo de pagamento
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Endereco> carregaEnderecosAPartirTipoPagamento(Documento documento) {
		List<Endereco> list = null;
		if(documento.getTipopagamento().equals(Tipopagamento.CLIENTE) || 
				documento.getTipopagamento().equals(Tipopagamento.FORNECEDOR) ||
				documento.getTipopagamento().equals(Tipopagamento.COLABORADOR))
			list = enderecoService.carregarListaEndereco(documento.getPessoa());
			
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Endereco enderecoSugerido(List<Endereco> listaEnderecos, boolean contaReceber) {
		Endereco endereco = null;
		
		if (contaReceber){
			if (listaEnderecos!=null && !listaEnderecos.isEmpty()){
				ArrayList<Endereco> listaClone = (ArrayList<Endereco>) ((ArrayList<Endereco>) listaEnderecos).clone();
				for (Endereco enderecoAux: listaClone) {
					if (enderecoAux.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
						enderecoAux.setOrdem(1);
					}else if (enderecoAux.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						enderecoAux.setOrdem(2);
					}else if (enderecoAux.getEnderecotipo().equals(Enderecotipo.UNICO)){
						enderecoAux.setOrdem(3);
					}else {
						enderecoAux.setOrdem(4);
					}
				}
				Collections.sort(listaClone, new BeanComparator("ordem"));
				endereco = listaClone.get(0);
			}
		}else {
			if(listaEnderecos != null && !listaEnderecos.isEmpty()){
				for (Endereco enderecoAux : listaEnderecos) {
					if(enderecoAux.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
						endereco = enderecoAux;
						break;
					}else if(enderecoAux.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
						endereco = enderecoAux;
					}else if(enderecoAux.getEnderecotipo().equals(Enderecotipo.UNICO) && endereco == null){
						endereco = enderecoAux;
					}
				}
			}
		}
		
		return endereco;
	}
	
	public Endereco enderecoSugerido(List<Endereco> listaEnderecos) {
		return enderecoSugerido(listaEnderecos, false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean clientePossuiContasAtrasadas(Cliente cliente) {
		return contareceberDAO.clientePossuiContasAtrasadas(cliente);
	}

	/**
	 * Faz refer�ncia ao DAO
	 * @param documento
	 * @param pessoa
	 * @author Taidson
	 * @since 20/07/2010
	 */
	public void mudarCliente(Documento documento, Pessoa pessoa) {
		contareceberDAO.mudarCliente(documento, pessoa);
	}
	
	public Documento findUltimaReceita(Contrato contrato) {
		return contareceberDAO.findUltimaReceita(contrato);
	}
	
	/**
	 * M�todo que gera o HTML utilizando o layout do boleto selecionado
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 * @param boleto 
	 * @throws IOException 
	 */
	public String criaBoletoHTML(String whereIn, Boleto boleto) throws IOException {
		StringBuilder html = new StringBuilder("");
		List<Documento> listaDocumento = this.findForBoleto(whereIn);
		boleto = boletoService.loadForEntrada(boleto);
		for (Documento documento : listaDocumento) {
			BoletoBean boletoBean = boletoDocumento(documento, false);
			html.append(boletoService.generateTemplate(boleto, boletoBean)).append("<BR><BR>");
		}
		return html.toString();
	}
	
	public Rateio atualizaValorRateio(Documento doc, Money valor) {
		this.atualizaValor(doc, valor);
		
		Rateio rateio = rateioService.findByDocumento(doc);
		rateioService.atualizaValorRateio(rateio, valor);
		rateioService.saveOrUpdate(rateio);
		return rateio;
	}
	
	private void atualizaValor(Documento doc, Money valor) {
		contareceberDAO.atualizaValor(doc, valor);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param cdconta
	 * @return
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public boolean verficaContaBaixadaDefinitiva(Integer cdconta) {
		return contareceberDAO.verficaContaBaixadaDefinitiva(cdconta);
	}
	
//	/**
//	 * M�todo que incrementa o n� do documento caso haja incrementador cadastrado na empresa
//	 * 
//	 * @param documento
//	 * @author Tom�s Rabelo
//	 */
//	public void incrementaNumDocumento(Documento documento) {
//		documento.setEmpresa(empresaService.findByDocumento(documento));
//		if(documento.getEmpresa() != null && documento.getEmpresa().getCdpessoa() != null){
//			Integer proxNum = empresaService.carregaProxNumContaReceber(documento.getEmpresa());
//			if(proxNum != null){
//				updateNumeroContaReceber(documento, proxNum);
//				proxNum++;
//				empresaService.updateProximoNumContaReceber(documento.getEmpresa(), proxNum);
//			}
//		}	
//	}
	
//	/**
//	 * M�todo que incrementa o n� do documento caso haja incrementador cadastrado na empresa
//	 * 
//	 * @param listaDocumento
//	 * @author Tom�s Rabelo
//	 */
//	public void incrementaNumDocumento(List<Documento> listaDocumento) {
//		for (Documento documento : listaDocumento) {
//			incrementaNumDocumento(documento);
//		}
//	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param documento
	 * @param proxNum
	 * @author Tom�s Rabelo
	 */
	public void updateNumeroContaReceber(Documento documento, Integer proxNum) {
		contareceberDAO.updateNumeroContaReceber(documento, proxNum);
	}
	
	public Long getNextNossoNumero(Conta conta){
		return contareceberDAO.getNextNossoNumero(conta);
	}
	
	
	/**
	 * 
	 * M�todo que recebe o nosso n�mero e retorna o nosso n�mero com o DV calculado
	 *
	 *@author Thiago Augusto
	 *@date 26/03/2012
	 * @param nossoNumero
	 * @return
	 */
	public String geraNovoNumeroComDv(String nossoNumero){
		nossoNumero = StringUtils.stringCheia(nossoNumero, "0", 12, false);
		String retorno = nossoNumero + " ";
		//char [] stringAux = nossoNumero.toCharArray();
		//int multiply = 2;
		//int resultado = 0;
		
		/*
		for (char c : stringAux) {
			if(multiply == 10)
				multiply = 2;
			String valor = String.valueOf(c);
			resultado += (Integer.parseInt(valor) * multiply);
			multiply ++;
		}
		resultado = 11 - (resultado % 11);
		return retorno + resultado;
		*/
		return retorno + Modulos.modulo11(nossoNumero, 6);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#isPendenciaFinanceiraByCpfCnpj(String cpfcnpj)
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isPendenciaFinanceiraByCpfCnpj(VerificaPendenciaFinanceira bean){
		return contareceberDAO.isPendenciaFinanceiraByCpfCnpj(bean);
	}
	
	public List<Documento> getPendenciaFinanceiraByCpfCnpj(String cnpj, String cpf, Contagerencial contagerencial, Projeto projetoW3erp, Projeto projetoSindis, Projeto projetoSupergov){
		return contareceberDAO.getPendenciaFinanceiraByCpfCnpj(cnpj, cpf, contagerencial, projetoW3erp, projetoSindis, projetoSupergov);
	}
	
	/**
	 * Processamento do arquivo retorno Banco Rural 400
	 * @param arquivo
	 * @return
	 * @throws Exception
	 * @author Thiers Euller
	 */
	public EDIRetornoRural400 processaArquivoRetornoRural400(Arquivo arquivo) throws Exception {
		String arquivoStr = new String(arquivo.getContent());
		
		String[] linhas = arquivoStr.split("\\r?\\n");
		
		EDIRetornoRural400 retorno400 = new EDIRetornoRural400();
		String cdtiporegistro, linha, cdagencia = null, cdconta = null;
		EDIRegistroRetornoRural400 detalhe = new EDIRegistroRetornoRural400();
		
		for (int i = 0; i < linhas.length; i++) {
			linha = linhas[i];
			cdtiporegistro = linha.substring(0, 1);
			
			if (cdtiporegistro.equals("0")) { // Header do Arquivo
				EDIHeaderRetorno400 header = new EDIHeaderRetorno400(linha);
				retorno400.setHeaderRetorno400(header);
			} else if (cdtiporegistro.equals("1")) { // Detalhe
				
				detalhe = new EDIRegistroRetornoRural400(linha);
				retorno400.getRegistroRetornoRural400().add(detalhe);
				
				if (cdagencia == null && cdconta == null) {
					cdagencia = linha.substring(17,21);
					cdconta = linha.substring(23,29);
					
					retorno400.setAgencia(cdagencia);
					retorno400.setConta(cdconta);
				}
				
			} else if (cdtiporegistro.equals("2")) { //Segunda linha do Detalhe
			
			}else if (cdtiporegistro.equals("9")) { // Trailer do Arquivo
//				@SuppressWarnings("unused")
//				EDITrailerRetorno400 trailer = new EDITrailerRetorno400(linha);
			} else {
				throw new SinedException("Identificador de Registro inv�lido!");
			}
		}
		
		return retorno400;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#atualizaContaNegociada(Documento documento, Documentonegociado documentonegociado)
	 *
	 * @param bean
	 * @return
	 * @author Rafael Salvio
	 */
	public void atualizaContaNegociada(Documento documento, Documentonegociado documentonegociado){
		contareceberDAO.atualizaContaNegociada(documento, documentonegociado);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForRetornoConfiguravel(String nossonumero)
	 *
	 * @param nossonumero
	 * @return
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForRetornoConfiguravel(String nossonumero) {
		return contareceberDAO.findForRetornoConfiguravel(nossonumero);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForRetornoConfiguravel(String nossonumero, Conta conta)
	 *
	 * @param nossonumero
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRetornoConfiguravel(String nossonumero, Conta conta) {
		return contareceberDAO.findForRetornoConfiguravel(nossonumero, conta);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContareceberService#getValorDebitoContasEmAbertoByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorDebitoContasEmAbertoByCliente(Cliente cliente, boolean contasAtrasadas){
		Money valor = this.getValorRestanteContasBaixadasParcialmente(cliente, contasAtrasadas);
		return valor.add(contareceberDAO.getValorDebitoContasComValorTotalEmAbertoByCliente(cliente, contasAtrasadas));
	}
	
	public Money getValorDebitoContasEmAbertoByCliente(Cliente cliente){
		return getValorDebitoContasEmAbertoByCliente(cliente, false);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte)
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		return contareceberDAO.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContareceberService#findForRecalculoComissaoComNota(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte)
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoComNota(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		return contareceberDAO.findForRecalculoComissaoComNota(whereIn, dtrecalculoDe, dtrecalculoAte);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContareceberService#findForRecalculoComissaoDocumentoBaixado(String whereInCddocumento)
	 *
	 * @param whereInCddocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoDocumentoBaixado(String whereInCddocumento) {
		return contareceberDAO.findForRecalculoComissaoDocumentoBaixado(whereInCddocumento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForRecalculoComissaoDocumentoBaixadoComNota(String whereInCddocumento)
	 *
	 * @param whereInCddocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoDocumentoBaixadoComNota(String whereInCddocumento) {
		return contareceberDAO.findForRecalculoComissaoDocumentoBaixadoComNota(whereInCddocumento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForPermitirAssociar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForPermitirAssociar(String whereIn){
		return contareceberDAO.findForPermitirAssociar(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#verificaPendenciaFinanceiraByClientes(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Object[]> verificaPendenciaFinanceiraByClientes(String whereIn){
		return contareceberDAO.verificaPendenciaFinanceiraByClientes(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#loadWithValoratual(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Documento loadWithValoratual(Documento documento){
		return contareceberDAO.loadWithValoratual(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContareceberDAO#findForValecompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForValecompra(String whereIn){
		return contareceberDAO.findForValecompra(whereIn);
	}
	
	/**
	 * M�todo que cria vale compra
	 *
	 * @param listaDocumento
	 * @param valortotalmovimentacao
	 * @param dtpagamento 
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Valecompra> criaValecompraByCliente(List<Documento> listaDocumento, Double valortotalmovimentacao, Double valortotalrestante, Date dtpagamento, List<Documento> listaDocumentoForValecompra, boolean fromArquivoRetorno) {
		List<Valecompra> listaValecompra = new ArrayList<Valecompra>();
		Map<Integer, Money> mapaValorTitulo = new HashMap<Integer, Money>();
//		Map<Integer, Money> mapaValorPago = new HashMap<Integer, Money>();
//		Map<Integer, Money> mapaValorJurosMulta = new HashMap<Integer, Money>();
//		Map<Integer, Money> mapaValorDesconto = new HashMap<Integer, Money>();
		Map<Integer, Money> mapaValorPagoCalculado = new HashMap<Integer, Money>();
		
		if(listaDocumento != null && !listaDocumento.isEmpty()){
			StringBuilder whereInDocumento = new StringBuilder();
			for(Documento documento : listaDocumento){
				if(documento.getCddocumento() != null){
					if(!"".equals(whereInDocumento.toString())) whereInDocumento.append(",");
					whereInDocumento.append(documento.getCddocumento());
					
					if (fromArquivoRetorno){
						mapaValorTitulo.put(documento.getCddocumento(), documento.getValorTituloValeCompra());
//						mapaValorPago.put(documento.getCddocumento(), documento.getValorPagoValeCompra());
//						mapaValorJurosMulta.put(documento.getCddocumento(), SinedUtil.zeroIfNull(documento.getValorJurosMultaValeCompra()));
//						mapaValorDesconto.put(documento.getCddocumento(), SinedUtil.zeroIfNull(documento.getValorDescontoOutroDescontoValeCompra()));
						mapaValorPagoCalculado.put(documento.getCddocumento(), SinedUtil.zeroIfNull(documento.getValorPagoValeCompra()).subtract(documento.getValorJurosMultaValeCompra()).add(documento.getValorDescontoOutroDescontoValeCompra()));
					}
				}				
			}
			
			if(!"".equals(whereInDocumento.toString())){
				List<Documento> lista = this.findForValecompra(whereInDocumento.toString());
				if(lista != null && !lista.isEmpty()){
					if(dtpagamento != null){
						lista = documentoService.calculaDocumentoValorByDataRef(lista, dtpagamento, SinedUtil.isListNotEmpty(listaDocumentoForValecompra) ? listaDocumentoForValecompra : listaDocumento, listaDocumento.size() == 1 );
					}
					Double valortotaldocumentos = 0.0;
					Double valortotalpago = 0.0;
					Double valortotaldocumentosNaoCliente = 0.0;
					for(Documento d : lista){
						if(dtpagamento != null && d.getValoratualbaixaSemTaxa() != null){
							valortotaldocumentos += d.getValoratualbaixaSemTaxa().getValue().doubleValue();
							if(!Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
								valortotaldocumentosNaoCliente += d.getValoratualbaixaSemTaxa().getValue().doubleValue();	
							}
						}else if(dtpagamento != null && d.getValoratual() != null){
							valortotaldocumentos += d.getValoratual().getValue().doubleValue();
							if(!Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
								valortotaldocumentosNaoCliente += d.getValoratual().getValue().doubleValue();	
							}
						}else if(fromArquivoRetorno && d.getAux_documento() != null && d.getAux_documento().getValoratual() != null){
//							valortotaldocumentos += SinedUtil.zeroIfNull(mapaValorTitulo.get(d.getCddocumento())).getValue().doubleValue();
//							valortotaldocumentos += SinedUtil.zeroIfNull(mapaValorJurosMulta.get(d.getCddocumento())).getValue().doubleValue();
//							valortotaldocumentos -= SinedUtil.zeroIfNull(mapaValorDesconto.get(d.getCddocumento())).getValue().doubleValue();
							valortotaldocumentos += SinedUtil.zeroIfNull(mapaValorTitulo.get(d.getCddocumento())).getValue().doubleValue();
							valortotalpago += SinedUtil.zeroIfNull(mapaValorPagoCalculado.get(d.getCddocumento())).getValue().doubleValue();
							if(!Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
								valortotaldocumentosNaoCliente += d.getAux_documento().getValoratual().getValue().doubleValue();	
							}
						}else if(!fromArquivoRetorno && d.getAux_documento() != null && d.getAux_documento().getValoratual() != null){
							valortotaldocumentos += d.getAux_documento().getValoratual().getValue().doubleValue();
							if(!Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
								valortotaldocumentosNaoCliente += d.getAux_documento().getValoratual().getValue().doubleValue();	
							}
						}
					}
					
					Double valorrestante;
					
					if (fromArquivoRetorno){
						valorrestante = SinedUtil.round(valortotaldocumentos, 2) - SinedUtil.round(valortotalpago, 2);
					}else {
						valorrestante = SinedUtil.round((valortotalrestante != null ? valortotalrestante : valortotaldocumentos), 2) - valortotalmovimentacao;
					}
					
					if(valorrestante < 0){
						valorrestante = valorrestante*-1;
						Integer qtdetotaldocumentos = lista.size();
						Double valorTotalCredito = 0d;
						for(Documento documento : lista){
							Money valorValeCompraArquivoRetorno = new Money();
							Double percentual = 0.0;
							
							if(Tipopagamento.CLIENTE.equals(documento.getTipopagamento()) && documento.getPessoa() != null){ 
								if(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null){
									if (fromArquivoRetorno){
										valorValeCompraArquivoRetorno = SinedUtil.zeroIfNull(mapaValorPagoCalculado.get(documento.getCddocumento())).subtract(SinedUtil.zeroIfNull(mapaValorTitulo.get(documento.getCddocumento()))); 
									}else {
										if(qtdetotaldocumentos == 1){
											percentual  = 100.0;
										}else {
											percentual = documento.getAux_documento().getValoratual().getValue().doubleValue() * 100.0 / (valortotalmovimentacao - valortotaldocumentosNaoCliente - valorrestante);
										}
									}
									
									if(percentual > 0 || valorValeCompraArquivoRetorno.toLong()>0){
										Valecompra valecompra = new Valecompra();
										valecompra.setData(SinedDateUtils.currentDate());
										valecompra.setCliente(new Cliente(documento.getPessoa().getCdpessoa(), documento.getPessoa().getNome()));
										valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
										
										if (percentual > 0){
											valecompra.setValor(new Money(valorrestante*percentual/100));
										}else if (valorValeCompraArquivoRetorno.toLong() > 0){
											valecompra.setValor(new Money(valorValeCompraArquivoRetorno));											
										}else {
											continue;
										}
										
										if (fromArquivoRetorno){
											valecompra.setIdentificacao("Referente ao valor restante da baixa da conta " + documento.getCddocumento());
										}else {
											valecompra.setIdentificacao("Referente ao valor restante da baixa da(s) conta(s) " + whereInDocumento);
										}
										
										if (valecompra.getValor().toLong()<=0L){
											continue;
										}
										
										valorTotalCredito += SinedUtil.round(valecompra.getValor().getValue().doubleValue(), 2);
										
										Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class); 
										Valecompraorigem valecompraorigem = new Valecompraorigem();
										valecompraorigem.setDocumento(documento);
										listaValecompraorigem.add(valecompraorigem);
										
										valecompra.setListaValecompraorigem(listaValecompraorigem);
										
										listaValecompra.add(valecompra);
									}
								}
							}
						}
						
						if(!fromArquivoRetorno && valorTotalCredito - valorrestante >= 0 && listaValecompra.size() > 0){
							Valecompra valecompra = listaValecompra.get(listaValecompra.size()-1);
							Money valor = valecompra.getValor();
							valor = valor.subtract(new Money(valorTotalCredito - valorrestante));
							valecompra.setValor(valor);
						}
					}
				}
			}
		}
		
		return listaValecompra;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @param param
	 * @return
	 * @author Lucas Costa
	 */
	public List<Documento> findForAutoCompleteDocComVendedor(String param) {
		return contareceberDAO.findForAutoCompleteDocComVendedor(param);
	}

	
	/**
	 * Caso a conta a receber seja estornada, retorna o valor original.
	 *
	 * @param documento, valorPrevista, descriacao
	 * @author Lucas Costa
	 * @since 17/11/2014
	 */
	public void voltaValorIndiceCorrecao(Documento documento, Money valorPrevista, String descriacao){
		contareceberDAO.voltaValorIndiceCorrecao(documento, valorPrevista, descriacao);
	}

	/**
	 * M�todo com refer�ncia no DAO.
	 * Salva a descri��o e o valor do documento antes da aplica��o do indice de corre��o
	 * @param documento
	 * @author Lucas Costa
	 */
	public void updateDadosAntesIndeceCorrecao (Documento documento){
		contareceberDAO.updateDadosAntesIndeceCorrecao(documento);
	}
	
	/**
	 * 
	 * M�todo que seta um ano a mais na data.
	 *
	 *@author Thiago Augusto
	 *@date 15/03/2012
	 * @param date
	 * @return
	 */
	public Date getAdicionarAnoData(Date date){
		
		Calendar calendar = Calendar.getInstance();  
		calendar.setTime(date);  
		calendar.add(Calendar.MONTH, 12);  
		
		Date retorno = new Date(calendar.getTime().getTime());
		
		return retorno;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @author Jo�o Vitor
	 */
	public Documento findForAssociacaoArquivoConciliacaoOperadora(String whereIn){
		return contareceberDAO.findForAssociacaoArquivoConciliacaoOperadora(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @author Jo�o Vitor
	 */
	public List<Documento> findListaDocumentosForAssociacaoArquivoOperadora(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		return contareceberDAO.findListaDocumentosForAssociacaoArquivoOperadora(arquivoconciliacaooperadora);
	}
	
	public ModelAndView retornaJsonParaValidarEmissaoDeBoletos(String whereIn){
		if(whereIn!=""){
			List<Documento> docs = documentoService.findDocumentoContaNotGeraBoleto(whereIn);
			if(docs != null && docs.size() > 0){
				Boolean nenhumDocPodeGerarBoleto = (whereIn.split(",").length == docs.size());

				return new JsonModelAndView().addObject("qtdeBoletosNaoGerar", docs.size())
										.addObject("nenhumDocPodeGerarBoleto", nenhumDocPodeGerarBoleto)
										 .addObject("possuiBoletosNaoGerar", true);
			}			
		}
		JsonModelAndView retorno = new JsonModelAndView();
		List<Documento> docs = documentoService.findForBoleto(whereIn);
		HashMap<MessageType, String> retornoValidaLimiteNossoNum = documentoService.validaLimiteSequencialNossoNum(docs, null);
		if(!retornoValidaLimiteNossoNum.isEmpty()){
			if(retornoValidaLimiteNossoNum.containsKey(MessageType.WARN)){
				retorno.addObject("alertanossonumero", retornoValidaLimiteNossoNum.get(MessageType.WARN));
			}
		}
		retorno.addObject("qtdeBoletosNaoGerar", 0)
		 .addObject("nenhumDocPodeGerarBoleto", false)
		 .addObject("possuiBoletosNaoGerar", false);
 
		return retorno;
	}
	
	public List<Documento> findByNota(String cdnota){
		return contareceberDAO.findByNota(cdnota);
	}
	
	public Documento gerarReceitaNota(String whereIn) {
		List<NotaFiscalServico> lista = notaFiscalServicoService.loadForReceita(whereIn);
		
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		Documento documento = new Documento();
		Money valortotal = new Money();
		StringBuilder msg1 = new StringBuilder();
		int i = 0;
		
		Documentotipo documentotipoNota = notaService.getDocumentotipo(lista);
		List<Endereco> listaEnderecoNota = new ArrayList<Endereco>();
		for(NotaFiscalServico nfs : lista){
			
			if(nfs.getEnderecoCliente() != null && !listaEnderecoNota.contains(nfs.getEnderecoCliente())){
				listaEnderecoNota.add(nfs.getEnderecoCliente());
			}
			
			notaFiscalServicoService.setPrazogapamentoUnico(nfs);
			if(i == (lista.size()-1)){
				if(documentotipoNota != null){
					documento.setDocumentotipo(documentotipoNota); 
				}else {
					documento.setDocumentotipo(documentotipo);
				}
				
				documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				documento.setDocumentoacao(Documentoacao.DEFINITIVA);
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.setReferencia(Referencia.MES_ANO_CORRENTE);
				
				documento.setPessoa(nfs.getCliente());
				documento.setCliente(nfs.getCliente());
				documento.setDtemissao(notaFiscalServicoService.getDtemissaoMenor(lista));
				documento.setDtvencimento(documento.getDtemissao());
				if(lista.size() == 1)
					documento.setNumero(notaFiscalServicoService.formataNumNfse(nfs.getNumero()));
				
				valortotal = valortotal.add(nfs.getValorNota());
				documento.setValor(valortotal);
				
				
				documento.setEmpresa(nfs.getEmpresa());
				if(!msg1.toString().equals("")) msg1.append(",");
				msg1.append((nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));
				documento.setMensagem1("Referente a(s) NF(s): " + msg1.toString());
				
				if(lista.size() > 1){
					notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, "Presta��o de Servi�os");
				} else {
					notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, (nfs.getListaItens().size() > 1 ? "Presta��o de Servi�os" : null));
				}
				documento.setAcaohistorico(Documentoacao.DEFINITIVA);
				if(lista.size() > 1){
					StringBuilder obs = new StringBuilder();
					for(NotaFiscalServico nota : lista){
						obs.append("<a href=\"javascript:visualizarNotaFiscalServico("+nota.getCdNota()+")\">"+(nota.getNumero() != null ? nota.getNumero() : "sem n�mero")+"</a>,");
					}
					documento.setObservacaoHistorico("Origem da(s) nota(s): " + obs.substring(0, obs.length()-1));
				}else {
					documento.setObservacaoHistorico("Origem da nota <a href=\"javascript:visualizarNotaFiscalServico("+nfs.getCdNota()+")\">"+(nfs.getNumero() != null ? nfs.getNumero() : "sem n�mero")+"</a>.");
				}				
				Documentohistorico documentohistorico = new Documentohistorico(documento);
				
				List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
				listaHistorico.add(documentohistorico);
				
				documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
				
				documento.setRateio(new Rateio());
				
				List<Notafiscalservicoduplicata> listaDuplicata = notaFiscalServicoService.getDuplicatasAgrupadas(lista);
				if(SinedUtil.isListNotEmpty(listaDuplicata)){
					boolean considerarDiaUtil = nfs.getPrazopagamentofatura() != null && 
					nfs.getPrazopagamentofatura().getDataparceladiautil() != null &&
					nfs.getPrazopagamentofatura().getDataparceladiautil();
					if(nfs.getContaboleto() != null) documento.setConta(nfs.getContaboleto());
					if(nfs.getContacarteira() != null) documento.setContacarteira(nfs.getContacarteira());
					
					if(nfs.getPrazopagamentofatura() != null){
						documento.setPrazo(nfs.getPrazopagamentofatura());
						documento.setFinanciamento(listaDuplicata.size() > 1 ? Boolean.TRUE : Boolean.FALSE);
						documento.setRepeticoes(listaDuplicata.size()-1);
					}
					documentoService.setDuplicatasDocumento(nfs, listaDuplicata, documento, considerarDiaUtil, lista.size() == 1, lista);
				}
				
				if (lista.size()==1){
					if (nfs.getContaboleto()!=null){
						documento.setConta(contaService.loadWithTaxa(nfs.getContaboleto()));
					}
					if (nfs.getDocumentotipo()!=null){
						documento.setDocumentotipo(documentotipoService.load(nfs.getDocumentotipo()));
					}
				}
				
			}else {
				valortotal = valortotal.add(nfs.getValorNota());
				if(!msg1.toString().equals("")) msg1.append(",");
				msg1.append((nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));
				i++;
			}
		}
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		Date dtproximovencimento = null;
		for(NotaFiscalServico nfs : lista){
			if(SinedUtil.isListNotEmpty(nfs.getListaNotaContrato())){
				for (NotaContrato c : nfs.getListaNotaContrato()) {
					if(c.getContrato() != null && c.getContrato().getDtproximovencimento() != null){
						if(dtproximovencimento == null){
							if(documento.getDtemissao() == null || SinedDateUtils.beforeIgnoreHour(documento.getDtemissao(), c.getContrato().getDtproximovencimento())){
								dtproximovencimento = c.getContrato().getDtproximovencimento();
							}
						}else {
							if(SinedDateUtils.afterIgnoreHour(dtproximovencimento, c.getContrato().getDtproximovencimento())){
								if(documento.getDtemissao() == null || SinedDateUtils.beforeIgnoreHour(documento.getDtemissao(), c.getContrato().getDtproximovencimento())){
									dtproximovencimento = c.getContrato().getDtproximovencimento();
								}
							}
						}
					}
					if(c.getContrato().getRateio() != null && c.getContrato().getRateio().getCdrateio() != null){
						List<Rateioitem> listaRi = rateioitemService.findByRateio(c.getContrato().getRateio()); 
						if(listaRi != null && !listaRi.isEmpty()){
							Rateio rateioAux = c.getContrato().getRateio();
							rateioAux.setListaRateioitem(listaRi);
							rateioService.atualizaValorRateio(rateioAux, nfs.getValorNota());
							listaRateioitem.addAll(rateioAux.getListaRateioitem());
						}
					}
					
					contratoService.preencherTaxasContrato(listaTaxaitem, c.getContrato());
					
					if (Boolean.TRUE.equals(c.getContrato().getReajuste()) && documento.getContrato()==null){
						documento.setContrato(c.getContrato());
						documento.setMensagem6(c.getContrato().getMensagemBoletoReajuste());
					}
				}
			}else if(SinedUtil.isListNotEmpty(nfs.getListaNotavenda())){
				listaRateioitem.addAll(notaFiscalServicoService.getListaRateioitemFaturamento(nfs));
			} 
		}
		if(documento.getContrato() == null)
			documento.setEndereco(documento.getCliente().getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO));
		else
			documento.setEndereco(contratoService.selecionarEnderecoAlternativoDeCobranca(documento.getContrato()).getEnderecoPrioritario());
		
		if(documento.getEndereco() == null && listaEnderecoNota.size() == 1){
			documento.setEndereco(listaEnderecoNota.get(0));
		}
		if(dtproximovencimento != null && documento.getDtvencimento() == null){
			documento.setDtvencimento(dtproximovencimento);
		}
		
		if(documento.getConta() != null){
			contacorrenteService.preencherTaxasByConta(listaTaxaitem, documento.getConta());
		}
		
		rateio.setListaRateioitem(listaRateioitem);
		documento.setRateio(rateio);
		rateioitemService.agruparItensRateio(documento.getRateio().getListaRateioitem());
		rateioService.limpaReferenciaRateio(rateio);
		rateioService.calculaValorRateioitem(documento.getValor(), listaRateioitem);
		rateioService.ajustaDiferencaRateio(documento.getValor(), listaRateioitem);
		if(documento.getDtvencimento() != null && SinedUtil.isListNotEmpty(listaTaxaitem)){
			for (Taxaitem taxaitem : listaTaxaitem) {
				if(taxaitem.getDias() != null){
					if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
						taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), -taxaitem.getDias()));
					}else {
						taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), taxaitem.getDias()));
					}
				}
			}
		}
		
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		documento.setTaxa(taxa);
		documentoService.addTaxaMovimentoTipoDocumento(documento);
		
		if(SinedUtil.isListNotEmpty(taxa.getListaTaxaitem())){
			
			StringBuilder msg2 = new StringBuilder();
			StringBuilder msg3 = new StringBuilder();
			String preposicao = "";
			String msg = "";
			
			
			if(documento.getMensagem2() != null)
				msg2.append(documento.getMensagem2());
			if(documento.getMensagem3() != null)
				msg3.append(documento.getMensagem3());
			
			for(Taxaitem txitem : taxa.getListaTaxaitem()){
				if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
					if(tipotaxaService.isApos(txitem.getTipotaxa())){
						preposicao = " ap�s ";
					}else {
						preposicao = " at� ";
					}
					msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + SinedDateUtils.toString(txitem.getDtlimite()) : "") + ". ";
					if((msg2.length() + msg.length()) <= 80){
						msg2.append(msg);
					}else if((msg3.length() + msg.length()) <= 80){
						msg3.append(msg);
					}
				}
			}
			if(!msg2.toString().equals("")){
				documento.setMensagem2(msg2.toString());
			}
			if(!msg3.toString().equals("")){
				documento.setMensagem3(msg3.toString());
			}
		}
		
		return documento;
	}
	public void criarAvisoContaReceberAtrasado(Motivoaviso m, Date data, Date dateToSearch) {
		List<Documento> documentoList = contareceberDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		for (Documento d : documentoList) {
			Aviso aviso = new Aviso("Conta a receber vencida", (!org.apache.commons.lang.StringUtils.isEmpty(d.getNumero()) ? "N�mero da conta a receber: " + d.getNumero() : "C�digo da conta a receber: " + d.getCddocumento()), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), d.getCddocumento(), d.getEmpresa() != null ? d.getEmpresa() : empresaPrincipal, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, d.getCddocumento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, d.getCddocumento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public void salvarConta(Documento documento){
		super.saveOrUpdate(documento);
	}
	
	public List<Documento> findForAntecipacao(String whereIn, Boolean antecipado) {
		Documentoacao[] acoes = null;
		return findForAntecipacao(whereIn, antecipado, acoes);
	}

	public List<Documento> findForAntecipacao(String whereIn, Boolean antecipado, Documentoacao... acoes) {
		return contareceberDAO.findForAntecipacao(whereIn, antecipado, acoes);
	}
	
	public void modificarAntecipadoDaConta(List<Documento> contas, boolean antecipar){
		for(Documento conta: contas){
			conta.setAntecipado(antecipar);
			conta.setDtantecipacao(new Date(System.currentTimeMillis()));
			Documentohistorico historico = new Documentohistorico(); 
			documentohistoricoService.gerarHistoricoDocumento(historico, antecipar ? Documentoacao.ANTECIPACAO : Documentoacao.CANCELAMENTO_DE_ANTECIPACAO, conta);
			historico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			documentohistoricoService.saveOrUpdate(historico);
			updateCampoAntecipado(conta);
		}
	}
	
	public void updateCampoAntecipado(Documento conta) {
		contareceberDAO.updateCampoAntecipado(conta);
	}
	
	public List<Documento> findForEnvioautomaticoAgendadoBoleto(Documentoenvioboleto documentoenvioboleto){
		String whereInCliente = null;
		if(SinedUtil.isListNotEmpty(documentoenvioboleto.getListaCliente())){
			List<Cliente> listaCliente = new ArrayList<Cliente>();
			for(Documentoenvioboletocliente documentoenviocliente: documentoenvioboleto.getListaCliente()){
				if(!listaCliente.contains(documentoenviocliente.getCliente())){
					listaCliente.add(documentoenviocliente.getCliente());
				}
			}
			whereInCliente = CollectionsUtil.listAndConcatenate(listaCliente, "cdpessoa", ",");
		}

		return contareceberDAO.findForEnvioautomaticoAgendadoBoleto(documentoenvioboleto, whereInCliente);
	}
	
	public Money getValorRestanteContasBaixadasParcialmente(Cliente cliente, Boolean contasAtrasadas){
		Money valorRestante = new Money(0);
		Date dataAtual = new Date(System.currentTimeMillis());
		List<Documento> listaDocumento = documentoService.findByDocumentoacao(Documentoacao.BAIXADA_PARCIAL, cliente, contasAtrasadas);
		for(Documento doc: listaDocumento){
			valorRestante = valorRestante.add(movimentacaoService.calcularMovimentacoesByDocumento(doc, dataAtual));
		}
		return valorRestante;
	}
	
	public Money getValorRestanteContasBaixadasParcialmente(Cliente cliente){
		return getValorRestanteContasBaixadasParcialmente(cliente, false);
	}
	
	public List<Documento> findSituacaoDocumentoByPedidovenda(Pedidovenda pedidovenda) {
		return contareceberDAO.findSituacaoDocumentoByPedidovenda(pedidovenda);
	}
	
	public void cancelaDocumentosVinculadoPedidoVendaCancelado(List<Documento> listaContaReceber) {
        String whereInDocumento = CollectionsUtil.listAndConcatenate(listaContaReceber, "cddocumento", ",");
        listaContaReceber = documentoService.carregaDocumentos(whereInDocumento);
        if(SinedUtil.isListNotEmpty(listaContaReceber)){
            for (Documento documento : listaContaReceber) {
                if(!Documentoacao.CANCELADA.equals(documento.getDocumentoacao())){
                    contareceberDAO.cancelaContaReceberVinculadaPedidoVendaCancelado(documento);
                }
            }
        }
        listaContaReceber = documentoService.carregaDocumentos(listaContaReceber);
		Documentohistorico dh;
        for (Documento documento : listaContaReceber) {
			dh = new Documentohistorico();
			dh.setObservacao("CANCELAMENTO AUTOM�TICO CONFORME PAR�METRO DE CONFIGURA��O");
			
			dh = documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.CANCELADA, documento);
			documentohistoricoService.saveOrUpdate(dh);
        }
	}
}
