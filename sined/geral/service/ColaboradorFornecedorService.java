package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorFornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ColaboradorFornecedorDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorFornecedorService extends GenericService<ColaboradorFornecedor> {

	private ColaboradorFornecedorDAO colaboradorFornecedorDAO;
	private ColaboradorService colaboradorService;
	private UsuarioService usuarioService;
	
	public void setColaboradorFornecedorDAO (ColaboradorFornecedorDAO colaboradorFornecedorDAO){
		this.colaboradorFornecedorDAO = colaboradorFornecedorDAO;
	}
	
	public void setColaboradorService (ColaboradorService colaboradorService){
		this.colaboradorService = colaboradorService;
	}
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ColaboradorFornecedorDAO#findByFornecedor(Fornecedor fornecedor)
	*
	* @param fornecedor
	* @return
	* @since 07/03/2016
	* @author Luiz Fernando
	*/
	public List<ColaboradorFornecedor> findByFornecedor(Fornecedor fornecedor){
		return colaboradorFornecedorDAO.findByFornecedor(fornecedor);
	}

	/**
	 * M�todo com referencia no DAO
	 * @param fornecedor, colaborador
	 * @return
	 * @author Lucas Costa
	 */
	public Boolean isFornecedorVinculadoColaborador (Fornecedor fornecedor, Colaborador colaborador){
		return colaboradorFornecedorDAO.isFornecedorVinculadoColaborador(fornecedor, colaborador);
	}
	
	/**
	 * M�todo que valida se o usuario logado est� vinculado ao fornecedor, caso contrario gera exception
	 * @param form
	 * @author Lucas Costa
	 */
	public void validaPermissao (Fornecedor form){
		if(form.getCdpessoa() != null){
			Usuario usuario = SinedUtil.getUsuarioLogado();
			usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.restricaoFornecedorColaborador");
			if(usuario.getRestricaoFornecedorColaborador() != null && usuario.getRestricaoFornecedorColaborador()){
				Colaborador colaborador = colaboradorService.findColaboradorusuario(usuario.getCdpessoa());
				if (colaborador != null && !this.isFornecedorVinculadoColaborador(form, colaborador)){
					throw new SinedException("Opera��o cancelada. O usu�rio logado n�o est� vinculado ao fornecedor "+form.getNome()+".");
				}
			}
		}
	}

	/**
	 * M�todo com referencia no DAO
	 * @return
	 * @author Lucas Costa
	 */
	public List<ColaboradorFornecedor> findByUsuarioLogado(){
		Colaborador colaborador = colaboradorService.findColaboradorusuario(SinedUtil.getUsuarioLogado().getCdpessoa());
		return colaboradorFornecedorDAO.findByUsuarioLogado(colaborador);		
	}
	
	/**
	 * M�todo com referencia no DAO
	 * @return
	 * @author Lucas Costa
	 */
	public List<ColaboradorFornecedor> findNaoVinculadosByUsuarioLogado(){
		Colaborador colaborador = colaboradorService.findColaboradorusuario(SinedUtil.getUsuarioLogado().getCdpessoa());
		return colaboradorFornecedorDAO.findNaoVinculadosByUsuarioLogado(colaborador);		
	}
	
	/**
	 * M�todo com referencia no DAO
	 * @return
	 * @author Lucas Costa
	 */
	public List<String> findNomeFornecedorNaoVinculados(){
		Colaborador colaborador = colaboradorService.findColaboradorusuario(SinedUtil.getUsuarioLogado().getCdpessoa());
		return colaboradorFornecedorDAO.findNomeFornecedorNaoVinculados(colaborador);		
	}
}
