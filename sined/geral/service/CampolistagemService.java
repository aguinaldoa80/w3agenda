package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Campolistagem;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.dao.CampolistagemDAO;

public class CampolistagemService extends GenericService<Campolistagem>{
	
	private CampolistagemDAO campolistagemDAO;
	
	public void setCampolistagemDAO(CampolistagemDAO campolistagemDAO) {this.campolistagemDAO = campolistagemDAO;}
	
	/* singleton */
	private static CampolistagemService instance;
	public static CampolistagemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CampolistagemService.class);
		}
		return instance;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * verifica se existe campos da listagem cadastrados para a tela
	 *
	 * @param tela
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeCamposCadastrados(Tela tela){
		return campolistagemDAO.existeCamposCadastrados(tela);
	}
}
