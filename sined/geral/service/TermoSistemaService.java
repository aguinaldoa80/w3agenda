package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.TermoSistema;
import br.com.linkcom.sined.geral.dao.TermoSistemaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TermoSistemaService extends GenericService<TermoSistema>{

	protected TermoSistemaDAO termoSistemaDAO;
	public void setTermoSistemaDAO(TermoSistemaDAO termoSistemaDAO) {
		this.termoSistemaDAO = termoSistemaDAO;
	}
	
	/* singleton */
	private static TermoSistemaService instance;
	public static TermoSistemaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TermoSistemaService.class);
		}
		return instance;
	}
}
