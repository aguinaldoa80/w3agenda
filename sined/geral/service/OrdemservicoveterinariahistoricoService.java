package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.Date;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariahistorico;
import br.com.linkcom.sined.geral.dao.OrdemservicoveterinariahistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemservicoveterinariahistoricoService extends GenericService<Ordemservicoveterinariahistorico> {	
	
	private OrdemservicoveterinariahistoricoDAO ordemservicoveterinariahistoricoDAO;
	
	public void setOrdemservicoveterinariahistoricoDAO(OrdemservicoveterinariahistoricoDAO ordemservicoveterinariahistoricoDAO) {	this.ordemservicoveterinariahistoricoDAO = ordemservicoveterinariahistoricoDAO;	}

	/**
	 * M�todo que persiste o Hist�rico de Faturamento para Lista OrdemServicoVeterinaria 
	 *
	 * @param whereIn OrdemServicoVeterinaria
	 * @param cdvenda
	 * @author Marcos Lisboa
	 */
	public void saveHistoricoOSVenda(String whereInOSV, Integer cdvenda) {
		for (String strIdOS : whereInOSV.split(",")) {
			Ordemservicoveterinariahistorico historico = new Ordemservicoveterinariahistorico();
			historico.setOrdemservicoveterinaria(new Ordemservicoveterinaria(Integer.parseInt(strIdOS)));
			historico.setData(new Timestamp(new Date().getTime()));
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			historico.setObservacao("Faturamento: <a href=\"javascript:visualizaVenda(" + cdvenda + ");\">" +  cdvenda + "</a> ");
			ordemservicoveterinariahistoricoDAO.saveOrUpdate(historico);
		}
	}
	
	public void saveHistoricoOSPedidovenda(String whereInOSV, Integer cdvenda) {
		for (String strIdOS : whereInOSV.split(",")) {
			Ordemservicoveterinariahistorico historico = new Ordemservicoveterinariahistorico();
			historico.setOrdemservicoveterinaria(new Ordemservicoveterinaria(Integer.parseInt(strIdOS)));
			historico.setData(new Timestamp(new Date().getTime()));
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			historico.setObservacao("Faturamento: <a href=\"javascript:visualizaPedidovenda(" + cdvenda + ");\">" +  cdvenda + "</a> ");
			ordemservicoveterinariahistoricoDAO.saveOrUpdate(historico);
		}
	}
	
}