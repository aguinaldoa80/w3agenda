package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.MargemContribuicao;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.MargemContribuicaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoNotaVendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoNotaVendaItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.FormaAgrupamentoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.OrdenacaoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.SemaforoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;

public class MargemContribuicaoService extends GenericService<MargemContribuicao> {

	private EmpresaService empresaService;
	private ClienteService clienteService;
	private CategoriaService categoriaService;
	private FornecedorService fornecedorService;
	private MaterialService materialService;
	private MaterialgrupoService materialGrupoService;
	private MaterialtipoService materialTipoService;
	private MaterialcategoriaService materialcategoriaService;
	private ContapagarService contaPagarService;
	private MovimentacaoService movimentacaoService;
	private MargemContribuicaoDAO margemContribuicaoDAO;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	
	public void setMaterialTipoService(MaterialtipoService materialTipoService) {
		this.materialTipoService = materialTipoService;
	}
	
	public void setMaterialGrupoService(MaterialgrupoService materialGrupoService) {
		this.materialGrupoService = materialGrupoService;
	}
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	public void setContaPagarService(ContapagarService contaPagarService) {
		this.contaPagarService = contaPagarService;
	}
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	public void setMargemContribuicaoDAO(MargemContribuicaoDAO margemContribuicaoDAO) {
		this.margemContribuicaoDAO = margemContribuicaoDAO;
	}
	
	public boolean validaPadrao(MargemContribuicao bean) {
		return margemContribuicaoDAO.validaPadrao(bean);
	}
	
	public IReport gerarRelatorioPdf(RelatorioMargemContribuicaoFiltro filtro, List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean) {
		Report report = new Report("/faturamento/margemContribuicao");
		
		if (filtro.getEmpresa() != null) {
			report.addParameter("nomeEmpresa", empresaService.load(filtro.getEmpresa(), "empresa.nome, empresa.razaosocial").getRazaosocialOuNome());
		} else {
			report.addParameter("nomeEmpresa", "");
		}
		
		if (filtro.getCliente() != null) {
			report.addParameter("nomeCliente", clienteService.load(filtro.getCliente(), "cliente.nome, cliente.razaosocial, cliente.tipopessoa").getRazaoOrNomeByTipopessoa());
		} else {
			report.addParameter("nomeCliente", "");
		}
		
		if (filtro.getMargemContribuicao() != null) {
			report.addParameter("nomeMargemContribuicao", margemContribuicaoDAO.load(filtro.getMargemContribuicao(), "margemContribuicao.nome").getNome());
		} else {
			report.addParameter("nomeMargemContribuicao", "");
		}
		
		if (filtro.getCategoria() != null) {
			report.addParameter("nomeCategoria", categoriaService.load(filtro.getCategoria(), "categoria.nome").getNome());
		} else {
			report.addParameter("nomeCategoria", "");
		}
		
		if (filtro.getDtInicio() != null) {
			report.addParameter("dtInicio", SinedDateUtils.toString(filtro.getDtInicio()));
		} else {
			report.addParameter("dtInicio", "");
		}
		
		if (filtro.getDtFim() != null) {
			report.addParameter("dtFim", SinedDateUtils.toString(filtro.getDtFim()));
		} else {
			report.addParameter("dtFim", "");
		}
		
		if (filtro.getFornecedor() != null) {
			report.addParameter("nomeFabricante", fornecedorService.load(filtro.getFornecedor(), "fornecedor.nome, fornecedor.razaosocial, fornecedor.tipopessoa").getRazaoSocialOrNomeByTipopessoa());
		} else {
			report.addParameter("nomeFabricante", "");
		}
		
		if (filtro.getMaterial() != null) {
			report.addParameter("nomeMaterial", materialService.load(filtro.getMaterial(), "material.nome").getNome());
		} else {
			report.addParameter("nomeMaterial", "");
		}
		
		if (filtro.getMaterialGrupo() != null) {
			report.addParameter("nomeGrupoMaterial", materialGrupoService.load(filtro.getMaterialGrupo(), "materialgrupo.nome").getNome());
		} else {
			report.addParameter("nomeGrupoMaterial", "");
		}
		
		if (filtro.getMaterialTipo() != null) {
			report.addParameter("nomeMaterialTipo", materialTipoService.load(filtro.getMaterialTipo(), "materialtipo.nome").getNome());
		} else {
			report.addParameter("nomeMaterialTipo", "");
		}
		
		if (filtro.getMaterialCategoria() != null) {
			report.addParameter("nomeMaterialCategoria", materialcategoriaService.load(filtro.getMaterialCategoria(), "materialcategoria.descricao").getDescricao());
		} else {
			report.addParameter("nomeMaterialCategoria", "");
		}
		
		if (filtro.getOrigemString() != null) {
			report.addParameter("nomeOrigem", filtro.getOrigemString());
		} else {
			report.addParameter("nomeOrigem", "");
		}
		
		if (filtro.getSemaforoEnum() != null) {
			report.addParameter("nomeSemaforo", filtro.getSemaforoEnum().getDescricao());
		} else {
			report.addParameter("nomeSemaforo", "");
		}
		
		if (filtro.getMargem() != null) {
			report.addParameter("nomeMargem", filtro.getMargemString());
		} else {
			report.addParameter("nomeMargem", "");
		}
		
		if (filtro.getValorMargemInicio() != null) {
			report.addParameter("valorMargemInicio", filtro.getValorMargemInicio());
		} else {
			report.addParameter("valorMargemInicio", new Money());
		}
		
		if (filtro.getValorMargemFim() != null) {
			report.addParameter("valorMargemFim", filtro.getValorMargemFim());
		} else {
			report.addParameter("valorMargemFim", new Money());
		}
		
		if (filtro.getPercentualMargemInicio() != null) {
			report.addParameter("percentualMargemInicio", new Money(filtro.getPercentualMargemInicio()));
		} else {
			report.addParameter("percentualMargemInicio", new Money());
		}
		
		if (filtro.getPercentualMargemFim() != null) {
			report.addParameter("percentualMargemFim", new Money(filtro.getPercentualMargemFim()));
		} else {
			report.addParameter("percentualMargemFim", new Money());
		}
		
		if (filtro.getOrdenacaoEnum() != null) {
			report.addParameter("nomeOrdenacao", filtro.getOrdenacaoEnum().getDescricao());
		} else {
			report.addParameter("nomeOrdenacao", "");
		}
		
		if (filtro.getFormaAgrupamentoEnum() != null) {
			report.addParameter("nomeFormaAgrupamento", filtro.getFormaAgrupamentoEnum().getDescricao().toUpperCase());
		} else {
			report.addParameter("nomeFormaAgrupamento", "PRODUTO");
		}
		
		if (listaRelatorioMargemContribuicaoBean != null && listaRelatorioMargemContribuicaoBean.size() > 0) {
			if (listaRelatorioMargemContribuicaoBean.get(0).getValorGastosEmpresa() != null) {
				report.addParameter("valorGastosEmpresa", listaRelatorioMargemContribuicaoBean.get(0).getValorGastosEmpresa());
			} else {
				report.addParameter("valorGastosEmpresa", "");
			}
			
			if (listaRelatorioMargemContribuicaoBean.get(0).getMargemContribuicaoTotal() != null) {
				report.addParameter("margemContribuicaoTotal", listaRelatorioMargemContribuicaoBean.get(0).getMargemContribuicaoTotal());
			} else {
				report.addParameter("margemContribuicaoTotal", "");
			}
			
			if (listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrioOperacional() != null) {
				report.addParameter("pontoEquilibrioOperacional", listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrioOperacional());
			} else {
				report.addParameter("pontoEquilibrioOperacional", "");
			}
			
			if(listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrio() != null){
				report.addParameter("pontoEquilibrio", listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrio());
			}else {
				report.addParameter("pontoEquilibrio", "");
			}
		}
		
		report.setDataSource(listaRelatorioMargemContribuicaoBean);
		
		return report;
	}

	public Resource gerarRelatorioCsv(List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean, RelatorioMargemContribuicaoFiltro filtro) throws IOException {
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 35);
        
        HSSFRow row = null;
		HSSFCell cell = null;
        
		planilha.setColumnWidth((short) 0, (short) (300*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
		planilha.setColumnWidth((short) 2, (short) (150*35));
		planilha.setColumnWidth((short) 3, (short) (150*35));
		planilha.setColumnWidth((short) 4, (short) (150*35));
		planilha.setColumnWidth((short) 5, (short) (150*35));
		planilha.setColumnWidth((short) 6, (short) (150*35));
		planilha.setColumnWidth((short) 7, (short) (150*35));
		planilha.setColumnWidth((short) 8, (short) (150*35));
		
		row = planilha.createRow((short) 0);
		
		cell = row.createCell((short) 0);
		cell.setCellValue(filtro.getFormaAgrupamentoEnum().getDescricao().toUpperCase());
		
		cell = row.createCell((short) 1);
		cell.setCellValue("FAT. BRUTO");
		
		cell = row.createCell((short) 2);
		cell.setCellValue("(-) DED. VENDAS");
		
		cell = row.createCell((short) 3);
		cell.setCellValue("RECEITA LIQ.");
		
		cell = row.createCell((short) 4);
		cell.setCellValue("(-) CMV");
		
		cell = row.createCell((short) 5);
		cell.setCellValue("RESULTADO BRUTO");
		
		cell = row.createCell((short) 6);
		cell.setCellValue("(-) DESP. COM VENDA");
		
		cell = row.createCell((short) 7);
		cell.setCellValue("(R$) MARGEM");
		
		cell = row.createCell((short) 8);
		cell.setCellValue("(%) MARGEM");
		
		if (listaRelatorioMargemContribuicaoBean != null && listaRelatorioMargemContribuicaoBean.size() > 0) {
			for (int i = 0; i < listaRelatorioMargemContribuicaoBean.size(); i++) {
				int j = i + 1;
				row = planilha.createRow((short) j);
				
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getDescricao());
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getFaturamentoBruto().toString());
				
				cell = row.createCell((short) 2);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getDeducao().toString());
				
				cell = row.createCell((short) 3);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getReceitaLiquida().toString());
				
				cell = row.createCell((short) 4);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getCmv().toString());
				
				cell = row.createCell((short) 5);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getResultadoBruto().toString());
				
				cell = row.createCell((short) 6);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getDespesas().toString());
				
				cell = row.createCell((short) 7);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getValorMargem().toString());
				
				cell = row.createCell((short) 8);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(i).getPercentualMargem().toString());
			}
		}
		
		row = planilha.createRow((short) listaRelatorioMargemContribuicaoBean.size() + 3);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("GASTO DA EMPRESA");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(0).getValorGastosEmpresa().toString());
		
		row = planilha.createRow((short) listaRelatorioMargemContribuicaoBean.size() + 4);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("MARGEM DE CONTRIBUI��O TOTAL");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(0).getMargemContribuicaoTotal().toString());
		
		row = planilha.createRow((short) listaRelatorioMargemContribuicaoBean.size() + 5);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("PONTO DE EQUIL�BRIO OPERACIONAL");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrioOperacional().toString());
		
		row = planilha.createRow((short) listaRelatorioMargemContribuicaoBean.size() + 6);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("PONTO DE EQUIL�BRIO");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(listaRelatorioMargemContribuicaoBean.get(0).getPontoEquilibrio().toString());	
		
		return wb.getWorkBookResource("margemContribuicao_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	public List<RelatorioMargemContribuicaoBean> createListaRelatorioMargemContribuicaoBean(RelatorioMargemContribuicaoFiltro filtro, List<Venda> listaVenda, List<Notafiscalproduto> listaNotaFiscalProduto) {
		List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean = new ArrayList<RelatorioMargemContribuicaoBean>();
		HashMap<String, RelatorioMargemContribuicaoBean> hashMap = new HashMap<String, RelatorioMargemContribuicaoBean>();
		FormaAgrupamentoEnum formaAgrupamentoEnum = filtro.getFormaAgrupamentoEnum();
		MargemContribuicao margemContribuicao = margemContribuicaoDAO.loadByMargemContribuicao(filtro.getMargemContribuicao());
		
		if (listaVenda != null && listaVenda.size() > 0) {
			for (Venda venda : listaVenda) {
				for (Vendamaterial vendaMaterial : venda.getListavendamaterial()) {
					vendaMaterial.setVenda(venda);
					
					RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean = new RelatorioMargemContribuicaoBean();
					
					relatorioMargemContribuicaoBean.addFaturamentoBruto(vendaMaterial.getValorBruto());
					
					if(Boolean.TRUE.equals(venda.getBonificacao())){
						relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValorBruto());
					}
					
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValoricms());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValoricmsst());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValorfcp());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValorfcpst());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValoripi());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValoricmsdestinatario());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValoricmsremetente());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValordifal());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getValorfcpdestinatario());
					
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getDesconto());
					relatorioMargemContribuicaoBean.addDeducao(vendaMaterial.getDescontoVendaPorporcional());
					
					Money devolucao = new Money();
					if(SinedUtil.isListNotEmpty(vendaMaterial.getListaMaterialdevolucao())){
						for (Materialdevolucao materialdevolucao : vendaMaterial.getListaMaterialdevolucao()) {
							devolucao = devolucao.add(new Money(materialdevolucao.getPreco()).multiply(materialdevolucao.getQtdedevolvida()));
						}						
					}
					relatorioMargemContribuicaoBean.addDeducao(devolucao);
					
					relatorioMargemContribuicaoBean.addCmv(vendaMaterial.getCustooperacional() != null ? new Money(vendaMaterial.getCustooperacional().doubleValue()) : null);
					
					if(vendaMaterial.getValorcustomaterial() != null){
						relatorioMargemContribuicaoBean.addCmv(new Money(vendaMaterial.getValorcustomaterial()));						
					}else if(vendaMaterial.getMaterial().getValorcusto() != null){
						relatorioMargemContribuicaoBean.addCmv(new Money(vendaMaterial.getMaterial().getValorcusto()));
					}else {
						relatorioMargemContribuicaoBean.addCmv(null);
					}
					
					if (margemContribuicao.getMediaComissaoMaterial() != null && margemContribuicao.getMediaComissaoMaterial() > 0d) {
						relatorioMargemContribuicaoBean.addDespesas(vendaMaterial.getValorBruto().multiply(new Money(margemContribuicao.getMediaComissaoMaterial() / 100)));
					} else {
						relatorioMargemContribuicaoBean.addDespesas(vendaMaterial.getValorComissao());
					}
					
					
					relatorioMargemContribuicaoBean.addDespesas(venda.getTaxavenda().divide(new Double(venda.getListavendamaterial().size())));
					
					relatorioMargemContribuicaoBean.addDespesas(vendaMaterial.getFreteVendaPorporcional());
					
					if (BooleanUtils.isTrue(filtro.getMostrarTela())) {
						relatorioMargemContribuicaoBean.addListaVenda(venda);
					}
					
					if (hashMap.containsKey(this.getObjectKey(formaAgrupamentoEnum, venda, vendaMaterial))) {
						RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBeanFound = hashMap.get(this.getObjectKey(formaAgrupamentoEnum, venda, vendaMaterial));
						
						updateRelatorioMargemContribuicaoBean(relatorioMargemContribuicaoBean, relatorioMargemContribuicaoBeanFound);
					} else {
						hashMap.put(this.getObjectKey(formaAgrupamentoEnum, venda, vendaMaterial), relatorioMargemContribuicaoBean);
					}
				}
			}
		}
		
		if (listaNotaFiscalProduto != null && listaNotaFiscalProduto.size() > 0) {
			for (Notafiscalproduto notaFiscalProduto : listaNotaFiscalProduto) {
				for (Notafiscalprodutoitem notaFiscalProdutoItem : notaFiscalProduto.getListaItens()) {
					RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean = new RelatorioMargemContribuicaoBean();
					
					relatorioMargemContribuicaoBean.addFaturamentoBruto(notaFiscalProdutoItem.getValorbruto());
					
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoricms());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoricmsst());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoricmsstremetente());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoricmsstdestinatario());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorfcp());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorfcpst());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorIcmsPropSub());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorIcmsEfetiva());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorcreditoicms());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoripi());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorpis());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorcofins());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoriss());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorcsll());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorir());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorinss());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValoriof());
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValorii());
					
					relatorioMargemContribuicaoBean.addDeducao(notaFiscalProdutoItem.getValordesconto());
					
					if(notaFiscalProdutoItem.getValorcustomaterial() != null){
						relatorioMargemContribuicaoBean.addCmv(new Money(notaFiscalProdutoItem.getValorcustomaterial()));
					}else if(notaFiscalProdutoItem.getMaterial().getValorcusto() != null){
						relatorioMargemContribuicaoBean.addCmv(new Money(notaFiscalProdutoItem.getMaterial().getValorcusto()));						
					}else{
						relatorioMargemContribuicaoBean.addCmv(null);
					}
					
					if (margemContribuicao.getMediaComissaoMaterial() != null && margemContribuicao.getMediaComissaoMaterial() > 0d) {
						relatorioMargemContribuicaoBean.addDespesas(notaFiscalProdutoItem.getValorbruto().multiply(new Money(margemContribuicao.getMediaComissaoMaterial() / 100)));
					} else {
						relatorioMargemContribuicaoBean.addDespesas(notaFiscalProdutoItem.getValorComissao());
					}
					
					relatorioMargemContribuicaoBean.addDespesas(notaFiscalProduto.getTaxaVenda().divide(new Double(notaFiscalProduto.getListaItens().size())));
					
					relatorioMargemContribuicaoBean.addDespesas(notaFiscalProdutoItem.getValorfrete());
					relatorioMargemContribuicaoBean.addDespesas(notaFiscalProdutoItem.getOutrasdespesas());
					relatorioMargemContribuicaoBean.addDespesas(notaFiscalProdutoItem.getValorseguro());
					
					if (BooleanUtils.isTrue(filtro.getMostrarTela())) {
						relatorioMargemContribuicaoBean.addListaNota(notaFiscalProduto);
					}
					
					if (hashMap.containsKey(this.getObjectKey(formaAgrupamentoEnum, notaFiscalProduto, notaFiscalProdutoItem))) {
						RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBeanFound = hashMap.get(this.getObjectKey(formaAgrupamentoEnum, notaFiscalProduto, notaFiscalProdutoItem));
						
						updateRelatorioMargemContribuicaoBean(relatorioMargemContribuicaoBean, relatorioMargemContribuicaoBeanFound);
					} else {
						hashMap.put(this.getObjectKey(formaAgrupamentoEnum, notaFiscalProduto, notaFiscalProdutoItem), relatorioMargemContribuicaoBean);
					}
				}
			}
		}
		
		if (hashMap != null && hashMap.size() > 0) {
			RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBeanTotal = new RelatorioMargemContribuicaoBean();
			relatorioMargemContribuicaoBeanTotal.setDescricao("TOTAIS GERAIS");
			
			for (Map.Entry<String, RelatorioMargemContribuicaoBean> pair : hashMap.entrySet()) {
				RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean = pair.getValue();
				
				Money receitaLiquida = relatorioMargemContribuicaoBean.getFaturamentoBruto().subtract(relatorioMargemContribuicaoBean.getDeducao());
				Money resultadoBruto = receitaLiquida.subtract(relatorioMargemContribuicaoBean.getCmv());
				Money valorMargem = resultadoBruto.subtract(relatorioMargemContribuicaoBean.getDespesas());
				
				Money percentualMargem = new Money();
				if(receitaLiquida.getValue().doubleValue() > 0)
				 percentualMargem = valorMargem.divide(receitaLiquida).multiply(new Money(100));
				
				if (BooleanUtils.isTrue(filtro.getMargem())) {
					if (filtro.getValorMargemInicio() != null && filtro.getValorMargemInicio().getValue().doubleValue() != 0d &&
							valorMargem.getValue().doubleValue() < filtro.getValorMargemInicio().getValue().doubleValue()) {
						continue;
					}
					
					if (filtro.getValorMargemFim() != null && filtro.getValorMargemFim().getValue().doubleValue() != 0d &&
							valorMargem.getValue().doubleValue() > filtro.getValorMargemFim().getValue().doubleValue()) {
						continue;
					}
				} else if (BooleanUtils.isFalse(filtro.getMargem())) {
					if (filtro.getPercentualMargemInicio() != null && filtro.getPercentualMargemInicio() != 0d &&
							percentualMargem.getValue().doubleValue() < filtro.getPercentualMargemInicio()) {
						continue;
					}
					
					if (filtro.getPercentualMargemFim() != null && filtro.getPercentualMargemFim() != 0d &&
							percentualMargem.getValue().doubleValue() > filtro.getPercentualMargemFim()) {
						continue;
					}
				}
				
				relatorioMargemContribuicaoBean.setDescricao(pair.getKey());
				relatorioMargemContribuicaoBean.setReceitaLiquida(receitaLiquida);
				relatorioMargemContribuicaoBean.setResultadoBruto(resultadoBruto);
				relatorioMargemContribuicaoBean.setValorMargem(valorMargem);
				relatorioMargemContribuicaoBean.setPercentualMargem(percentualMargem);
				
				listaRelatorioMargemContribuicaoBean.add(relatorioMargemContribuicaoBean);
				
				relatorioMargemContribuicaoBeanTotal.addFaturamentoBruto(relatorioMargemContribuicaoBean.getFaturamentoBruto());
				relatorioMargemContribuicaoBeanTotal.addDeducao(relatorioMargemContribuicaoBean.getDeducao());
				relatorioMargemContribuicaoBeanTotal.addReceitaLiquida(relatorioMargemContribuicaoBean.getReceitaLiquida());
				relatorioMargemContribuicaoBeanTotal.addCmv(relatorioMargemContribuicaoBean.getCmv());
				relatorioMargemContribuicaoBeanTotal.addDespesas(relatorioMargemContribuicaoBean.getDespesas());
				relatorioMargemContribuicaoBeanTotal.addValorMargem(relatorioMargemContribuicaoBean.getValorMargem());
				relatorioMargemContribuicaoBeanTotal.addPercentualMargem(relatorioMargemContribuicaoBean.getPercentualMargem());
				relatorioMargemContribuicaoBeanTotal.addResultadoBruto(relatorioMargemContribuicaoBean.getResultadoBruto());
			}
			
			Money valorGastosEmpresa = new Money();
			
			if (margemContribuicao.getGastosEmpresa() != null && margemContribuicao.getGastosEmpresa()) {
				valorGastosEmpresa = margemContribuicao.getValorGastosEmpresa();
			} else {
				valorGastosEmpresa = this.buscarValorGastosEmpresa(filtro);
			}
			
			Money valorPontoEquilibrio = new Money();
			
			if (margemContribuicao.getCalcularPontoEquilibrio() != null && margemContribuicao.getCalcularPontoEquilibrio() && relatorioMargemContribuicaoBeanTotal.getValorMargem() != null && relatorioMargemContribuicaoBeanTotal.getValorMargem().getValue().doubleValue() > 0) {
				valorPontoEquilibrio = valorGastosEmpresa.divide(relatorioMargemContribuicaoBeanTotal.getValorMargem());
			} else if(margemContribuicao.getValorPontoEquilibrio() != null){
				valorPontoEquilibrio = new Money(margemContribuicao.getValorPontoEquilibrio());
			}
			
			Money margemContribuicaoTotal = new Money();
			if(relatorioMargemContribuicaoBeanTotal.getValorMargem() != null && relatorioMargemContribuicaoBeanTotal.getReceitaLiquida() != null){
				margemContribuicaoTotal = relatorioMargemContribuicaoBeanTotal.getValorMargem().divide(relatorioMargemContribuicaoBeanTotal.getReceitaLiquida()).multiply(100.0d);				
			}
			Double margemContribuicaoDivide100 = SinedUtil.round(margemContribuicaoTotal.getValue().doubleValue(), 2) /100d;
			Money pontoEquilibrioOperacional = new Money();
			if(margemContribuicaoDivide100 > 0){
				pontoEquilibrioOperacional = valorGastosEmpresa.divide(margemContribuicaoDivide100);				
			}
			Money valorPontoEquilibrioAmarelo = valorPontoEquilibrio.add(margemContribuicao.getValorPontoEquilibrioAmarelo());
			
			for (RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean : listaRelatorioMargemContribuicaoBean) {
				if (SinedUtil.round(relatorioMargemContribuicaoBean.getPercentualMargem().getValue().doubleValue(), 2) < valorPontoEquilibrio.getValue().doubleValue()) {
					relatorioMargemContribuicaoBean.setSemaforo(SemaforoEnum.VERMELHO.getDescricao());
				} else if (SinedUtil.round(relatorioMargemContribuicaoBean.getPercentualMargem().getValue().doubleValue(),2) >= valorPontoEquilibrio.getValue().doubleValue() && 
						SinedUtil.round(relatorioMargemContribuicaoBean.getPercentualMargem().getValue().doubleValue(), 2) <= valorPontoEquilibrioAmarelo.getValue().doubleValue()) {
					relatorioMargemContribuicaoBean.setSemaforo(SemaforoEnum.AMARELO.getDescricao());
				} else {
					relatorioMargemContribuicaoBean.setSemaforo(SemaforoEnum.VERDE.getDescricao());
				}
				
				if (relatorioMargemContribuicaoBean.getListaNota() != null && relatorioMargemContribuicaoBean.getListaNota().size() > 0) {
					relatorioMargemContribuicaoBean.setIdsNota(SinedUtil.listAndConcatenate(relatorioMargemContribuicaoBean.getListaNota(), "cdNota", ","));
				}
				if (relatorioMargemContribuicaoBean.getListaVenda() != null && relatorioMargemContribuicaoBean.getListaVenda().size() > 0) {
					relatorioMargemContribuicaoBean.setIdsVenda(SinedUtil.listAndConcatenate(relatorioMargemContribuicaoBean.getListaVenda(), "cdvenda", ","));
				}
			}
			
			List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBeanFiltroSemaforo = new ArrayList<RelatorioMargemContribuicaoBean>();
			
			if (filtro.getSemaforoEnum() != null) {
				relatorioMargemContribuicaoBeanTotal = new RelatorioMargemContribuicaoBean();
				relatorioMargemContribuicaoBeanTotal.setDescricao("TOTAIS GERAIS");
				
				for (RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean : listaRelatorioMargemContribuicaoBean) {
					if (relatorioMargemContribuicaoBean.getSemaforo().equals(filtro.getSemaforoEnum().getDescricao())) {
						relatorioMargemContribuicaoBeanTotal.addFaturamentoBruto(relatorioMargemContribuicaoBean.getFaturamentoBruto());
						relatorioMargemContribuicaoBeanTotal.addDeducao(relatorioMargemContribuicaoBean.getDeducao());
						relatorioMargemContribuicaoBeanTotal.addReceitaLiquida(relatorioMargemContribuicaoBean.getReceitaLiquida());
						relatorioMargemContribuicaoBeanTotal.addCmv(relatorioMargemContribuicaoBean.getCmv());
						relatorioMargemContribuicaoBeanTotal.addDespesas(relatorioMargemContribuicaoBean.getDespesas());
						relatorioMargemContribuicaoBeanTotal.addValorMargem(relatorioMargemContribuicaoBean.getValorMargem());
						relatorioMargemContribuicaoBeanTotal.addPercentualMargem(relatorioMargemContribuicaoBean.getPercentualMargem());
						relatorioMargemContribuicaoBeanTotal.addResultadoBruto(relatorioMargemContribuicaoBean.getResultadoBruto());
						
						listaRelatorioMargemContribuicaoBeanFiltroSemaforo.add(relatorioMargemContribuicaoBean);
					}
				}
				
				listaRelatorioMargemContribuicaoBean = listaRelatorioMargemContribuicaoBeanFiltroSemaforo;
			}
			
			if (filtro.getOrdenacaoEnum() == null) {
				filtro.setOrdenacaoEnum(OrdenacaoEnum.MARGEM);
			}
			
			final OrdenacaoEnum ordenacaoEnum = filtro.getOrdenacaoEnum() != null ? filtro.getOrdenacaoEnum() : OrdenacaoEnum.MARGEM;
			
			Collections.sort(listaRelatorioMargemContribuicaoBean, new Comparator<RelatorioMargemContribuicaoBean>() {
	
				@Override
				public int compare(RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean1, RelatorioMargemContribuicaoBean relatorioMargemContribuicaoBean2) {
					if (OrdenacaoEnum.SEMAFORO.equals(ordenacaoEnum)) {
						String semaforo1 = "", semaforo2 = "";
						
						if (SemaforoEnum.VERDE.getDescricao().equals(relatorioMargemContribuicaoBean1.getSemaforo())) {
							semaforo1 = "a";
						} else if (SemaforoEnum.AMARELO.getDescricao().equals(relatorioMargemContribuicaoBean1.getSemaforo())) {
							semaforo1 = "b";
						} else if (SemaforoEnum.VERMELHO.getDescricao().equals(relatorioMargemContribuicaoBean1.getSemaforo())) {
							semaforo1 = "c";
						}
						
						if (SemaforoEnum.VERDE.getDescricao().equals(relatorioMargemContribuicaoBean2.getSemaforo())) {
							semaforo2 = "a";
						} else if (SemaforoEnum.AMARELO.getDescricao().equals(relatorioMargemContribuicaoBean2.getSemaforo())) {
							semaforo2 = "b";
						} else if (SemaforoEnum.VERMELHO.getDescricao().equals(relatorioMargemContribuicaoBean2.getSemaforo())) {
							semaforo2 = "c";
						}
						
		    			return semaforo1.compareTo(semaforo2);
		    		} else if (OrdenacaoEnum.MARGEM.equals(ordenacaoEnum)) {
		    			return relatorioMargemContribuicaoBean1.getValorMargem().compareTo(relatorioMargemContribuicaoBean2.getValorMargem());
		    		} else {
		    			return relatorioMargemContribuicaoBean1.getFaturamentoBruto().compareTo(relatorioMargemContribuicaoBean2.getFaturamentoBruto());
		    		}
				}
			});
			
			if (listaRelatorioMargemContribuicaoBean.size() > 0) {
				relatorioMargemContribuicaoBeanTotal.setPercentualMargem(relatorioMargemContribuicaoBeanTotal.getPercentualMargem().divide(new Money(listaRelatorioMargemContribuicaoBean.size())));
			} else {
				relatorioMargemContribuicaoBeanTotal.setPercentualMargem(new Money());
			}
			
			if(SinedUtil.isListNotEmpty(listaRelatorioMargemContribuicaoBean)){
				listaRelatorioMargemContribuicaoBean.add(relatorioMargemContribuicaoBeanTotal);
				
				listaRelatorioMargemContribuicaoBean.get(0).setValorGastosEmpresa(valorGastosEmpresa);
				listaRelatorioMargemContribuicaoBean.get(0).setMargemContribuicaoTotal(margemContribuicaoTotal);
				listaRelatorioMargemContribuicaoBean.get(0).setPontoEquilibrioOperacional(pontoEquilibrioOperacional);
				listaRelatorioMargemContribuicaoBean.get(0).setPontoEquilibrio(valorPontoEquilibrio);
			}
			
		}
		
		return listaRelatorioMargemContribuicaoBean;
	}
	
	private String getObjectKey(FormaAgrupamentoEnum formaAgrupamentoEnum, Venda venda, Vendamaterial vendaMaterial) {
		String key = "";
		
		if (FormaAgrupamentoEnum.PRODUTO.equals(formaAgrupamentoEnum)) {
			if (vendaMaterial.getMaterial() != null) {
				key = vendaMaterial.getMaterial().getNome();
			}
		} else if (FormaAgrupamentoEnum.GRUPO_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (vendaMaterial.getMaterial() != null && vendaMaterial.getMaterial().getMaterialgrupo() != null) {
				key = vendaMaterial.getMaterial().getMaterialgrupo().getNome();
			}
		} else if (FormaAgrupamentoEnum.CATEGORIA_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (vendaMaterial.getMaterial() != null && vendaMaterial.getMaterial().getMaterialcategoria() != null) {
				key = vendaMaterial.getMaterial().getMaterialcategoria().getDescricao();
			}
		} else if (FormaAgrupamentoEnum.TIPO_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (vendaMaterial.getMaterial() != null && vendaMaterial.getMaterial().getMaterialtipo() != null) {
				key = vendaMaterial.getMaterial().getMaterialtipo().getNome();
			}
		} else if (FormaAgrupamentoEnum.MUNICIPIO.equals(formaAgrupamentoEnum)) {
			if (venda.getEndereco() != null && venda.getEndereco().getMunicipio() != null) {
				key = venda.getEndereco().getMunicipio().getNome();
			}else {
				Endereco endereco = venda.getCliente().getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO, Enderecotipo.COBRANCA, Enderecotipo.ENTREGA, Enderecotipo.CORRESPONDENCIA, Enderecotipo.INSTALACAO, Enderecotipo.OUTRO);
				if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null){
					key = endereco.getMunicipio().getNome();
				}
				
				if(StringUtils.isEmpty(key)){
					key = "SEM MUNIC�PIO";					
				}
			}
		} else if (FormaAgrupamentoEnum.VENDEDOR.equals(formaAgrupamentoEnum)) {
			if (venda.getColaborador() != null) {
				key = venda.getColaborador().getNome();
			}
		} else if (FormaAgrupamentoEnum.CLIENTE.equals(formaAgrupamentoEnum)) {
			if (venda.getCliente() != null) {
				key = venda.getCliente().getRazaoOrNomeByTipopessoa();
			}
		} else if (FormaAgrupamentoEnum.FABRICANTE.equals(formaAgrupamentoEnum)) {
			if (SinedUtil.isListNotEmpty(vendaMaterial.getMaterial().getListaFornecedor())) {
				if(vendaMaterial.getMaterial().getListaFornecedor().size() > 1){
					key = "FABRICANTES DIVERSOS";
				}else {
					for (Materialfornecedor materialfornecedor : vendaMaterial.getMaterial().getListaFornecedor()) {
						key = materialfornecedor.getFornecedor().getRazaoSocialOrNomeByTipopessoa();						
					}
				}
			}
		}
		
		if (StringUtils.isEmpty(key)) {
			key = "N�O IDENTIFICADO";
		}
		
		return key;
	}
	
	private String getObjectKey(FormaAgrupamentoEnum formaAgrupamentoEnum, Notafiscalproduto notaFiscalProduto, Notafiscalprodutoitem notaFiscalProdutoItem) {
		String key = "";
		
		if (FormaAgrupamentoEnum.PRODUTO.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProdutoItem.getMaterial() != null) {
				key = notaFiscalProdutoItem.getMaterial().getNome();
			}
		} else if (FormaAgrupamentoEnum.GRUPO_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProdutoItem.getMaterial() != null && notaFiscalProdutoItem.getMaterial().getMaterialgrupo() != null) {
				key = notaFiscalProdutoItem.getMaterial().getMaterialgrupo().getNome();
			}
		} else if (FormaAgrupamentoEnum.CATEGORIA_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProdutoItem.getMaterial() != null && notaFiscalProdutoItem.getMaterial().getMaterialcategoria() != null) {
				key = notaFiscalProdutoItem.getMaterial().getMaterialcategoria().getDescricao();
			}
		} else if (FormaAgrupamentoEnum.TIPO_MATERIAL.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProdutoItem.getMaterial() != null && notaFiscalProdutoItem.getMaterial().getMaterialtipo() != null) {
				key = notaFiscalProdutoItem.getMaterial().getMaterialtipo().getNome();
			}
		} else if (FormaAgrupamentoEnum.MUNICIPIO.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProduto.getEnderecoCliente() != null && notaFiscalProduto.getEnderecoCliente().getMunicipio() != null) {
				key = notaFiscalProduto.getEnderecoCliente().getMunicipio().getNome();
			}else {
				key = "SEM MUNIC�PIO";
			}
		} else if (FormaAgrupamentoEnum.VENDEDOR.equals(formaAgrupamentoEnum)) {
			for (NotaVenda notaVenda : notaFiscalProduto.getListaNotavenda()) {
				if (notaVenda.getVenda() != null) {
					if (notaVenda.getVenda().getColaborador() != null && StringUtils.isNotEmpty(notaVenda.getVenda().getColaborador().getNome())) {
						if (StringUtils.isEmpty(key)) {
							key = notaVenda.getVenda().getColaborador().getNome();
						} else if (!key.equals(notaVenda.getVenda().getColaborador().getNome())) {
							key = "";
							break;
						}
					}
				}
			}
		} else if (FormaAgrupamentoEnum.CLIENTE.equals(formaAgrupamentoEnum)) {
			if (notaFiscalProduto.getCliente() != null) {
				key = notaFiscalProduto.getCliente().getRazaoOrNomeByTipopessoa();
			}
		} else if (FormaAgrupamentoEnum.FABRICANTE.equals(formaAgrupamentoEnum)) {
			if(SinedUtil.isListNotEmpty(notaFiscalProdutoItem.getMaterial().getListaFornecedor())){
				if(notaFiscalProdutoItem.getMaterial().getListaFornecedor().size() > 1){
					key = "FABRICANTES DIVERSOS";
				}else {
					for (Materialfornecedor materialfornecedor : notaFiscalProdutoItem.getMaterial().getListaFornecedor()) {
						key = materialfornecedor.getFornecedor().getRazaoSocialOrNomeByTipopessoa();						
					}
				}
			}
		}
		
		if (StringUtils.isEmpty(key)) {
			key = "N�O IDENTIFICADO";
		}
		
		return key;
	}
	
	public List<MargemContribuicao> findByEmpresa(Empresa empresa) {
		if (empresa == null) {
			return margemContribuicaoDAO.findAll();
		}
		
		return margemContribuicaoDAO.findByEmpresa(empresa);
	}
	
	private void updateRelatorioMargemContribuicaoBean(RelatorioMargemContribuicaoBean newBean, RelatorioMargemContribuicaoBean foundBean) {
		foundBean.setFaturamentoBruto(foundBean.getFaturamentoBruto() != null ? foundBean.getFaturamentoBruto().add(newBean.getFaturamentoBruto()) : newBean.getFaturamentoBruto());
		foundBean.setDeducao(foundBean.getDeducao() != null ? foundBean.getDeducao().add(newBean.getDeducao()) : newBean.getDeducao());
		foundBean.setCmv(foundBean.getCmv() != null ? foundBean.getCmv().add(newBean.getCmv()) : newBean.getCmv());
		foundBean.setDespesas(foundBean.getDespesas() != null ? foundBean.getDespesas().add(newBean.getDespesas()) : newBean.getDespesas());
		
		if (newBean.getListaNota() != null && newBean.getListaNota().size() > 0) {
			foundBean.addListaNota(newBean.getListaNota().get(0));
		}
		
		if (newBean.getListaVenda() != null && newBean.getListaVenda().size() > 0) {
			foundBean.addListaVenda(newBean.getListaVenda().get(0));
		}
	}
	
	private Money buscarValorGastosEmpresa(RelatorioMargemContribuicaoFiltro filtro) {
		Money valorGastoEmpresaContaPagar = contaPagarService.buscarGastoEmpresa(filtro);
		Money valorGastoEmpresaMovimentacao = movimentacaoService.buscarGastoEmpresa(filtro);
		
		return valorGastoEmpresaContaPagar.add(valorGastoEmpresaMovimentacao);
	}

	public List<RelatorioMargemContribuicaoNotaVendaBean> createLista(List<Venda> listaVenda, List<Notafiscalproduto> listaNota) {
		List<RelatorioMargemContribuicaoNotaVendaBean> listaRelatorioMargemContribuicaoNotaVendaBean = new ArrayList<RelatorioMargemContribuicaoNotaVendaBean>();
		
		if (listaVenda != null && listaVenda.size() > 0) {
			for (Venda venda : listaVenda) {
				RelatorioMargemContribuicaoNotaVendaBean relatorioMargemContribuicaoNotaVendaBean = new RelatorioMargemContribuicaoNotaVendaBean();
				
				relatorioMargemContribuicaoNotaVendaBean.setVenda(venda);
				
				relatorioMargemContribuicaoNotaVendaBean.setDescricao("VENDA");
				relatorioMargemContribuicaoNotaVendaBean.setNumero(venda.getCdvenda() != null ? venda.getCdvenda().toString() : "");
				relatorioMargemContribuicaoNotaVendaBean.setClienteNome(venda.getCliente().getRazaoOrNomeByTipopessoa() != null ? venda.getCliente().getRazaoOrNomeByTipopessoa() : "");
				relatorioMargemContribuicaoNotaVendaBean.setIdsVendaMaterial(SinedUtil.listAndConcatenate(venda.getListavendamaterial(), "cdvendamaterial", ","));
				
				for (Vendamaterial vendaMaterial : venda.getListavendamaterial()) {
					relatorioMargemContribuicaoNotaVendaBean.addValorBruto(vendaMaterial.getValorBruto());
				}
				
				listaRelatorioMargemContribuicaoNotaVendaBean.add(relatorioMargemContribuicaoNotaVendaBean);
			}
		}
		
		if (listaNota != null && listaNota.size() > 0) {
			for (Notafiscalproduto notaFiscalProduto : listaNota) {
				RelatorioMargemContribuicaoNotaVendaBean relatorioMargemContribuicaoNotaVendaBean = new RelatorioMargemContribuicaoNotaVendaBean();
				
				relatorioMargemContribuicaoNotaVendaBean.setNotafiscalproduto(notaFiscalProduto);
				
				relatorioMargemContribuicaoNotaVendaBean.setDescricao("NOTA");
				relatorioMargemContribuicaoNotaVendaBean.setNumero(notaFiscalProduto.getNumero() != null ? notaFiscalProduto.getNumero() : "");
				relatorioMargemContribuicaoNotaVendaBean.setClienteNome(notaFiscalProduto.getCliente().getRazaoOrNomeByTipopessoa() != null ? notaFiscalProduto.getCliente().getRazaoOrNomeByTipopessoa() : "");
				relatorioMargemContribuicaoNotaVendaBean.setValorBruto(notaFiscalProduto.getValorBruto());
				relatorioMargemContribuicaoNotaVendaBean.setIdsNotaItens(SinedUtil.listAndConcatenate(notaFiscalProduto.getListaItens(), "cdnotafiscalprodutoitem", ","));
				
				listaRelatorioMargemContribuicaoNotaVendaBean.add(relatorioMargemContribuicaoNotaVendaBean);
			}
		}
		
		return listaRelatorioMargemContribuicaoNotaVendaBean;
	}

	public List<RelatorioMargemContribuicaoNotaVendaItemBean> createListaItens(List<Notafiscalproduto> listaNotafiscalproduto, List<Venda> listaVenda) {
		List<RelatorioMargemContribuicaoNotaVendaItemBean> listaRelatorioMargemContribuicaoNotaVendaItemBean = new ArrayList<RelatorioMargemContribuicaoNotaVendaItemBean>();
		
		if (listaVenda != null && listaVenda.size() > 0) {
			for (Venda venda : listaVenda) {
		        for (Vendamaterial vendaMaterial : venda.getListavendamaterial()) {
		        	vendaMaterial.setVenda(venda);
		        	
		            RelatorioMargemContribuicaoNotaVendaItemBean relatorioMargemContribuicaoNotaVendaItemBean = new RelatorioMargemContribuicaoNotaVendaItemBean();
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.setNome(vendaMaterial.getMaterial() != null ? vendaMaterial.getMaterial().getNome() : "");
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addValorBruto(vendaMaterial.getValorBruto());
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addDesconto(vendaMaterial.getDescontoVendaPorporcional());
		            relatorioMargemContribuicaoNotaVendaItemBean.addDesconto(vendaMaterial.getDesconto());
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValoricms());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValoricmsst());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValorfcp());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValorfcpst());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValoripi());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValoricmsdestinatario());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValoricmsremetente());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValordifal());
		            relatorioMargemContribuicaoNotaVendaItemBean.addImposto(vendaMaterial.getValorfcpdestinatario());
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addTaxaFinanceira(venda.getTaxavenda().divide(new Double(venda.getListavendamaterial().size())));  
		            
		            if(vendaMaterial.getValorcustomaterial() != null){
		            	relatorioMargemContribuicaoNotaVendaItemBean.addCmv(new Money(vendaMaterial.getValorcustomaterial()));						
					}else if(vendaMaterial.getMaterial().getValorcusto() != null){
						relatorioMargemContribuicaoNotaVendaItemBean.addCmv(new Money(vendaMaterial.getMaterial().getValorcusto()));
					}else {
						relatorioMargemContribuicaoNotaVendaItemBean.addCmv(null);
					}

		            relatorioMargemContribuicaoNotaVendaItemBean.addCmv(vendaMaterial.getCustooperacional() != null ? new Money(vendaMaterial.getCustooperacional().doubleValue()) : null);
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addComissao(vendaMaterial.getValorComissao());
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addFrete(vendaMaterial.getFreteVendaPorporcional());
		            
		            relatorioMargemContribuicaoNotaVendaItemBean.addMargem(relatorioMargemContribuicaoNotaVendaItemBean.getValorBruto()
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getDesconto())
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getImposto())
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getTaxaFinanceira())
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getCmv())
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getComissao())
		                    .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getFrete()));
		            
		            listaRelatorioMargemContribuicaoNotaVendaItemBean.add(relatorioMargemContribuicaoNotaVendaItemBean);
		        }
			}
		}
		
		if (listaNotafiscalproduto != null && listaNotafiscalproduto.size() > 0) {
			for (Notafiscalproduto notafiscalproduto : listaNotafiscalproduto) {
				for (Notafiscalprodutoitem notaFiscalProdutoItem : notafiscalproduto.getListaItens()) {
					RelatorioMargemContribuicaoNotaVendaItemBean relatorioMargemContribuicaoNotaVendaItemBean = new RelatorioMargemContribuicaoNotaVendaItemBean();
					
			        relatorioMargemContribuicaoNotaVendaItemBean.setNome(notaFiscalProdutoItem.getMaterial() != null ? notaFiscalProdutoItem.getMaterial().getNome() : "");
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addValorBruto(notaFiscalProdutoItem.getValorbruto());
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addDesconto(notaFiscalProdutoItem.getValordesconto());
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoricms());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoricmsst());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoricmsstremetente());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoricmsstdestinatario());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorfcp());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorfcpst());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorIcmsPropSub());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorIcmsEfetiva());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorcreditoicms());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoripi());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorpis());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorcofins());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoriss());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorcsll());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorir());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorinss());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValoriof());
			        relatorioMargemContribuicaoNotaVendaItemBean.addImposto(notaFiscalProdutoItem.getValorii());
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addTaxaFinanceira(notafiscalproduto.getTaxaVenda().divide(new Double(notafiscalproduto.getListaItens().size())));
			        
			        if(notaFiscalProdutoItem.getValorcustomaterial() != null){
			        	relatorioMargemContribuicaoNotaVendaItemBean.addCmv(new Money(notaFiscalProdutoItem.getValorcustomaterial()));
					}else if(notaFiscalProdutoItem.getMaterial().getValorcusto() != null){
						relatorioMargemContribuicaoNotaVendaItemBean.addCmv(new Money(notaFiscalProdutoItem.getMaterial().getValorcusto()));						
					}else{
						relatorioMargemContribuicaoNotaVendaItemBean.addCmv(null);
					}
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addComissao(notaFiscalProdutoItem.getValorComissao());
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addFrete(notaFiscalProdutoItem.getValorfrete());
			        
			        relatorioMargemContribuicaoNotaVendaItemBean.addMargem(relatorioMargemContribuicaoNotaVendaItemBean.getValorBruto()
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getDesconto())
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getImposto())
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getTaxaFinanceira())
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getCmv())
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getComissao())
			                .subtract(relatorioMargemContribuicaoNotaVendaItemBean.getFrete()));
			        
			        listaRelatorioMargemContribuicaoNotaVendaItemBean.add(relatorioMargemContribuicaoNotaVendaItemBean);
				}
			}
		}
		
		return listaRelatorioMargemContribuicaoNotaVendaItemBean;
	}

	public MargemContribuicao findPrincipal() {
		return margemContribuicaoDAO.findPrincipal();
	}
}
