package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notaromaneio;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.dao.NotaromaneioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaromaneioService extends GenericService<Notaromaneio> {
	
	private NotaromaneioDAO notaromaneioDAO;
	
	
	public void setNotaromaneioDAO(NotaromaneioDAO notaromaneioDAO){
		this.notaromaneioDAO = notaromaneioDAO;
	}
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VendaDAO#findByRomaneio (Romaneio romaneio)
	 *
	 * @param romaneio
	 * @return
	 * @author Lucas Costa
	 * @since 24/07/2014
	 */
	public List<Notaromaneio> findByRomaneio (Romaneio romaneio){
		return notaromaneioDAO.findByRomaneio (romaneio);
	}
}
