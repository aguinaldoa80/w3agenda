package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicohistorico;
import br.com.linkcom.sined.geral.bean.Contratojuridicoparcela;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Processojuridicoandamento;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.bean.Processojuridicoquestionario;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Contratojuridicoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Processojuridicoclienteenvolvido;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoenvolvido;
import br.com.linkcom.sined.geral.dao.ProcessojuridicoDAO;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoFiltro;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.FichaProcessojuridicoBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.FichaProcessojuridicoClienteBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.FichaProcessojuridicoParteContrariaBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.QuantitativoProcessojuridicoBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.QuantitativoProcessojuridicoSituacaoBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.filter.AgrupamentoEnum;
import br.com.linkcom.sined.modulo.juridico.controller.report.filter.QuantitativoProcessojuridicoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class ProcessojuridicoService extends GenericService<Processojuridico>{
	
	private ProcessojuridicoDAO processojuridicoDAO;
	private ProcessojuridicoinstanciaService processojuridicoinstanciaService;
	private ProcessojuridicosituacaoService processojuridicosituacaoService;
	private ContratojuridicoService contratojuridicoService;
	private ProcessojuridicoclienteService processojuridicoclienteService;
	private ProcessojuridicoquestionarioService processojuridicoquestionarioService;
	private ContratojuridicohistoricoService contratojuridicohistoricoService;
	private ProcessojuridicoandamentoService processojuridicoandamentoService;
	
	public void setContratojuridicohistoricoService(ContratojuridicohistoricoService contratojuridicohistoricoService) {this.contratojuridicohistoricoService = contratojuridicohistoricoService;}
	public void setContratojuridicoService(ContratojuridicoService contratojuridicoService) {this.contratojuridicoService = contratojuridicoService;}
	public void setProcessojuridicoDAO(ProcessojuridicoDAO processojuridicoDAO) {this.processojuridicoDAO = processojuridicoDAO;}
	public void setProcessojuridicoinstanciaService(ProcessojuridicoinstanciaService processojuridicoinstanciaService) {this.processojuridicoinstanciaService = processojuridicoinstanciaService;}
	public void setProcessojuridicosituacaoService(ProcessojuridicosituacaoService processojuridicosituacaoService) {this.processojuridicosituacaoService = processojuridicosituacaoService;}
	public void setProcessojuridicoclienteService(ProcessojuridicoclienteService processojuridicoclienteService) {this.processojuridicoclienteService = processojuridicoclienteService;}
	public void setProcessojuridicoquestionarioService(ProcessojuridicoquestionarioService processojuridicoquestionarioService) {this.processojuridicoquestionarioService = processojuridicoquestionarioService;}
	public void setProcessojuridicoandamentoService (ProcessojuridicoandamentoService processojuridicoandamentoService){this.processojuridicoandamentoService = processojuridicoandamentoService;}
	/**
	 * Faz refer�ncia a DAO.
	 * @author Taidson Santos
	 * @since 16/03/2010
	 * @param request
	 */
	public List<Processojuridico> populaCombo (Processojuridico processojuridico){
		return processojuridicoDAO.populaCombo(processojuridico);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProcessojuridicoDAO#findByCliente
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Processojuridico> findByCliente(Cliente cliente){
		return processojuridicoDAO.findByCliente(cliente);
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem de acordo com o filtro
	 * 
	 * @param filtro
	 * @auhtor Thiago Clemente
	 * 
	 */
	public IReport gerarRelatorioListagem(ProcessojuridicoFiltro filtro) {
		
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Processojuridico> listaListagemResult = processojuridicoDAO.findForListagem(filtro);
		List<Processojuridico> listaProcessojuridico = listaListagemResult.list();
		
		for (Processojuridico processojuridico : listaProcessojuridico) {
			processojuridico.setListaProcessojuridicoinstancia(processojuridicoinstanciaService.listaDeInstancias(processojuridico.getCdprocessojuridico()));
			if (processojuridico.getListaProcessojuridicoinstancia().size() > 0){
				Processojuridicoinstancia processojuridicoinstancia = new Processojuridicoinstancia();
				int maior = 0;
				for (Processojuridicoinstancia lista : processojuridico.getListaProcessojuridicoinstancia()) {
					if(lista.getProcessojuridicoinstanciatipo().getCdprocessojuridicoinstanciatipo() > maior){
						maior = lista.getProcessojuridicoinstanciatipo().getCdprocessojuridicoinstanciatipo();
						processojuridicoinstancia.setCdprocessojuridicoinstancia(lista.getCdprocessojuridicoinstancia());
					}
				}
				List<Processojuridicoandamento> listaProcessojuridicoandamento = processojuridicoandamentoService.loadAndamento(processojuridicoinstancia);
				java.util.Date maiorData =  new Date(0);
				for (Processojuridicoandamento lista : listaProcessojuridicoandamento) {
					if (maiorData.before(lista.getData())){
						maiorData = lista.getData();
						processojuridico.setDescAndamentoReport(lista.getAndamento());
					}
				}
			}
		}
		
		Report report = new Report("/juridico/processojuridico");
		report.setDataSource(listaProcessojuridico);
		
		return report;
	}
	
	public List<Processojuridico> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return processojuridicoDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * @throws Exception
	 * 
	 */
	public Resource getResourceQuantitativoProcessojuridico(QuantitativoProcessojuridicoFiltro filtro) throws Exception {
		
		List<Processojuridicosituacao> listaProcessojuridicosituacao = processojuridicosituacaoService.findAll("processojuridicosituacao.nome");
		int tamanhoListaSituacao = listaProcessojuridicosituacao.size();
		
		SinedExcel sinedExcel = new SinedExcel();
		
		HSSFSheet planilha = sinedExcel.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));
        
        HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) (tamanhoListaSituacao + 1)));
		
		HSSFCell HeaderTitleCell = headerRow.createCell((short) 0);
		HeaderTitleCell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		HeaderTitleCell.setCellValue("Quantitativo dos Processos Jur�dicos");
		
		List<QuantitativoProcessojuridicoBean> listaBean = new ArrayList<QuantitativoProcessojuridicoBean>();
		if (AgrupamentoEnum.CLIENTE.equals(filtro.getAgrupamento())){
			listaBean = processojuridicoDAO.findForQuantitativoProcessoJuridicoCliente(filtro);
		}else if (AgrupamentoEnum.PARTE_CONTRARIA.equals(filtro.getAgrupamento())){
			listaBean = processojuridicoDAO.findForQuantitativoProcessoJuridicoParteContraria(filtro);
		}else if (AgrupamentoEnum.AREA.equals(filtro.getAgrupamento())){
			listaBean = processojuridicoDAO.findForQuantitativoProcessoJuridicoArea(filtro);
		}else if (AgrupamentoEnum.ADVOGADO.equals(filtro.getAgrupamento())){
			listaBean = processojuridicoDAO.findForQuantitativoProcessoJuridicoAdvogado(filtro);
		}
		
		Collections.sort(listaBean);
		
		int tamanhoListaBean = listaBean.size();
		int i = 0;
		int j = 3;
		int k = 3;
		
		HSSFRow row = planilha.createRow((short) j++);
		HSSFCell cell = row.createCell((short) i++);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue(filtro.getAgrupamento().getNome());
		
		for (Processojuridicosituacao processojuridicosituacao: listaProcessojuridicosituacao){
			cell = row.createCell((short) i++);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
			cell.setCellValue(processojuridicosituacao.getNome());
		}
		
		cell = row.createCell((short) i++);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("TOTAL");
		
		for (QuantitativoProcessojuridicoBean bean: listaBean){
			i = 0;
			row = planilha.createRow(j++);
			cell = row.createCell((short) i++);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue(bean.getNome());			
			List<QuantitativoProcessojuridicoSituacaoBean> listaSituacaoBean = bean.getListaSituacaoBean();
			for (Processojuridicosituacao processojuridicosituacao: listaProcessojuridicosituacao){
				QuantitativoProcessojuridicoSituacaoBean situacaoBean = getSituacaoBean(listaSituacaoBean, processojuridicosituacao);
				cell = row.createCell((short) i++);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellValue(situacaoBean.getTotal());
			}
			cell = row.createCell((short) i++);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellFormula("SUM(" + SinedExcel.getAlgarismoColuna(1) + j + ":" + SinedExcel.getAlgarismoColuna(tamanhoListaSituacao) + j + ")");
		}
		
		if (!listaBean.isEmpty()){
			i = 0;
			row = planilha.createRow(j++);
			cell = row.createCell((short) i++);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("TOTAL");
			
			for (k=1; k<=tamanhoListaSituacao+1;k++){
				cell = row.createCell((short) i++);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellFormula("SUM(" + SinedExcel.getAlgarismoColuna(k) + 5 + ":" + SinedExcel.getAlgarismoColuna(k) + (tamanhoListaBean + 4) + ")");
			}
		}
		
		return sinedExcel.getWorkBookResource("quantitativo_processo_juridico.xls");
	}
	
	private QuantitativoProcessojuridicoSituacaoBean getSituacaoBean(List<QuantitativoProcessojuridicoSituacaoBean> listaSituacaoBean, Processojuridicosituacao processojuridicosituacao){
		
		for (QuantitativoProcessojuridicoSituacaoBean situacaoBean: listaSituacaoBean){
			if (situacaoBean.getProcessojuridicosituacao().equals(processojuridicosituacao)){
				return situacaoBean;
			}
		}
		
		QuantitativoProcessojuridicoSituacaoBean situacaoBean = new QuantitativoProcessojuridicoSituacaoBean();
		situacaoBean.setProcessojuridicosituacao(processojuridicosituacao);
		situacaoBean.setTotal(0);
		
		return situacaoBean;
	}
	
	public List<Processojuridico> findAutocomplete(String q){
		return processojuridicoDAO.findAutocomplete(q);
	}
	
	public List<Processojuridico> findForGeracaoContrato(String whereIn) {
		return processojuridicoDAO.findForGeracaoContrato(whereIn);
	}
	
	public void gerarContrato(Processojuridico pj, Empresa e) {
		
		Set<Processojuridicocliente> listaProcessojuridicocliente = pj.getListaProcessojuridicocliente();
		List<Date> datas;
		Date dataAux;
		Contratojuridicoparcela parcela;
		List<Contratojuridicoparcela> listaParcela;
		Rateio rateio;
		Rateioitem rateioitem;
		List<Rateioitem> listaRateioitem;
		Contratojuridicohistorico cjh;
		StringBuilder sb;
		for (Processojuridicocliente pjc : listaProcessojuridicocliente) {
			
			if(pjc.getEnvolvido().equals(Tipoenvolvido.PARTE_CONTRARIA) || pjc.getCliente() == null || pjc.getCliente().getCdpessoa() == null)
				continue;
			
			Contratojuridico cj = new Contratojuridico();
			
			cj.setCliente(pjc.getCliente());
			cj.setProcessojuridico(pj);
			cj.setDescricao(pj.getArea().getDescricao() + " - " + pj.getAreatipo().getDescricao());
			cj.setEmpresa(e);
			cj.setDtinicio(pj.getData());
			cj.setValor(pj.getValorcontrato());
			cj.setValorcausa(pj.getValorcausa());
			cj.setTipohonorario(pj.getTipohonorario());
			cj.setHonorario(pj.getHonorario());
			cj.setPrazopagamento(pj.getPrazopagamento());
			
			datas = new ArrayList<Date>();
			for (Prazopagamentoitem ppitem : cj.getPrazopagamento().getListaPagamentoItem()) {
				dataAux = null;
				
				if(ppitem.getDias() != null)
					dataAux = SinedDateUtils.addDiasData(cj.getDtinicio(), ppitem.getDias());
				if(ppitem.getMeses() != null)
					dataAux = SinedDateUtils.addMesData(cj.getDtinicio(), ppitem.getMeses());
				if(dataAux != null)
					datas.add(dataAux);
			}
			
			listaParcela = new ArrayList<Contratojuridicoparcela>();
			for (Date dtvencimento : datas) {
				parcela = new Contratojuridicoparcela();
				parcela.setDtvencimento(dtvencimento);
				parcela.setValor(cj.getValor().divide(new Money(datas.size())));
				
				listaParcela.add(parcela);
			}
			cj.setListaContratojuridicoparcela(listaParcela);
			
			rateioitem = new Rateioitem();
			rateioitem.setProjeto(pj.getArea().getProjeto());
			rateioitem.setCentrocusto(pj.getArea().getCentrocusto());
			rateioitem.setContagerencial(pj.getArea().getContagerencial());
			rateioitem.setPercentual(100d);
			rateioitem.setValor(cj.getValor());
			
			listaRateioitem = new ArrayList<Rateioitem>();
			listaRateioitem.add(rateioitem);
			
			rateio = new Rateio();
			rateio.setListaRateioitem(listaRateioitem);
			
			cj.setRateio(rateio);
			
			contratojuridicoService.saveOrUpdate(cj);
			
			cjh = new Contratojuridicohistorico();
			cjh.setContratojuridico(cj);
			cjh.setAcao(Contratojuridicoacao.CRIADO);
			
			sb = new StringBuilder();
			sb.append("Contrato criado a partir do processo jur�dico <a href=\"javascript:visualizarProcessoJuridico(");
			sb.append(pj.getCdprocessojuridico());
			sb.append(");\">");
			sb.append(pj.getCdprocessojuridico());
			sb.append("</a>.");
			
			cjh.setObservacao(sb.toString());
			
			contratojuridicohistoricoService.saveOrUpdate(cjh);
		}
	}
	
	/**
	 * M�todo que gera relat�rio de ficha de processo jurid�co
	 * 
	 * @param whereIn
	 * @auhtor Thiago Clemente
	 * 
	 */
	public IReport gerarRelatorioFichaProcessoJuridico(String whereIn) {
		
		List<FichaProcessojuridicoBean> listaRelatorio = new ArrayList<FichaProcessojuridicoBean>();
		List<Processojuridico> listaProcessoJuridico = processojuridicoDAO.findForFichaProcessoJuridico(whereIn);
		List<Processojuridicocliente> listaProcessojuridicocliente = processojuridicoclienteService.findForFichaProcessoJuridico(whereIn);
		List<Processojuridicoquestionario> listaProcessojuridicoquestionario = processojuridicoquestionarioService.findForFichaProcessoJuridico(whereIn);		
		
		montaRelatorioFichaProcessoJuridico(listaRelatorio, listaProcessoJuridico, listaProcessojuridicocliente, listaProcessojuridicoquestionario);
		
		Report subReportCliente = new Report("/juridico/fichaprocessojuridico_sub_cliente");
		Report subReportParteContraria = new Report("/juridico/fichaprocessojuridico_sub_partecontraria");
		Report subReportQuestionario = new Report("/juridico/fichaprocessojuridico_sub_questionario");
		Report report = new Report("/juridico/fichaprocessojuridico");
		report.addSubReport("SUBREPORT_CLIENTE", subReportCliente);		
		report.addSubReport("SUBREPORT_PARTECONTRARIA", subReportParteContraria);		
		report.addSubReport("SUBREPORT_QUESTIONARIO", subReportQuestionario);		
		report.setDataSource(listaRelatorio);
		
		return report;
	}
	
	/**
	 * 
	 * @param listaRelatorio
	 * @param listaProcessoJuridico
	 * @param listaProcessojuridicocliente
	 * @param listaProcessojuridicoquestionario
	 * @auhtor Thiago Clemente
	 * 
	 */
	private void montaRelatorioFichaProcessoJuridico(List<FichaProcessojuridicoBean> listaRelatorio,
														List<Processojuridico> listaProcessoJuridico,
														List<Processojuridicocliente> listaProcessojuridicocliente,
														List<Processojuridicoquestionario> listaProcessojuridicoquestionario){
		
		
		Map<Integer, List<FichaProcessojuridicoClienteBean>> mapaCliente = new HashMap<Integer, List<FichaProcessojuridicoClienteBean>>();
		Map<Integer, List<FichaProcessojuridicoParteContrariaBean>> mapaParteContraria = new HashMap<Integer, List<FichaProcessojuridicoParteContrariaBean>>();
		Map<Integer, List<Processojuridicoquestionario>> mapaQuestionario = new HashMap<Integer, List<Processojuridicoquestionario>>();
		List<FichaProcessojuridicoClienteBean> listaFichaProcessojuridicoClienteBeanAux;
		List<FichaProcessojuridicoParteContrariaBean> listaFichaProcessojuridicoParteContrariaBeanAux;
		List<Processojuridicoquestionario> listaProcessojuridicoquestionarioAux;
		Integer cdprocessojuridico;
		
		for (Processojuridicocliente processojuridicocliente: listaProcessojuridicocliente){
			if (processojuridicocliente.getEnvolvido().equals(Processojuridicoclienteenvolvido.CLIENTE) && processojuridicocliente.getCliente()!=null){
				cdprocessojuridico = processojuridicocliente.getProcessojuridico().getCdprocessojuridico();
				listaFichaProcessojuridicoClienteBeanAux = mapaCliente.get(cdprocessojuridico);
				if (listaFichaProcessojuridicoClienteBeanAux==null){
					listaFichaProcessojuridicoClienteBeanAux = new ArrayList<FichaProcessojuridicoClienteBean>();
				}
				listaFichaProcessojuridicoClienteBeanAux.add(new FichaProcessojuridicoClienteBean(processojuridicocliente));
				mapaCliente.put(cdprocessojuridico, listaFichaProcessojuridicoClienteBeanAux);
			}else if (processojuridicocliente.getEnvolvido().equals(Processojuridicoclienteenvolvido.PARTE_CONTRARIA) && processojuridicocliente.getPartecontraria()!=null){
				cdprocessojuridico = processojuridicocliente.getProcessojuridico().getCdprocessojuridico();
				listaFichaProcessojuridicoParteContrariaBeanAux = mapaParteContraria.get(cdprocessojuridico);
				if (listaFichaProcessojuridicoParteContrariaBeanAux==null){
					listaFichaProcessojuridicoParteContrariaBeanAux = new ArrayList<FichaProcessojuridicoParteContrariaBean>();
				}
				listaFichaProcessojuridicoParteContrariaBeanAux.add(new FichaProcessojuridicoParteContrariaBean(processojuridicocliente));
				mapaParteContraria.put(cdprocessojuridico, listaFichaProcessojuridicoParteContrariaBeanAux);
			}
		}		
		
		for (Processojuridicoquestionario processojuridicoquestionario: listaProcessojuridicoquestionario){
			cdprocessojuridico = processojuridicoquestionario.getProcessojuridico().getCdprocessojuridico();
			listaProcessojuridicoquestionarioAux = mapaQuestionario.get(cdprocessojuridico);
			if (listaProcessojuridicoquestionarioAux==null){
				listaProcessojuridicoquestionarioAux = new ArrayList<Processojuridicoquestionario>();
			}
			listaProcessojuridicoquestionarioAux.add(processojuridicoquestionario);
			mapaQuestionario.put(cdprocessojuridico, listaProcessojuridicoquestionarioAux);
		}		
		
		FichaProcessojuridicoBean bean = null;
		
		for (Processojuridico processojuridico: listaProcessoJuridico){
			
			List<FichaProcessojuridicoClienteBean> listaClienteBean = mapaCliente.get(processojuridico.getCdprocessojuridico());
			List<FichaProcessojuridicoParteContrariaBean> listaParteContrariaBean = mapaParteContraria.get(processojuridico.getCdprocessojuridico());
			List<Processojuridicoquestionario> listaQuestionario = mapaQuestionario.get(processojuridico.getCdprocessojuridico());
			
			if (listaClienteBean!=null){
				Collections.sort(listaClienteBean);
			}else {
				listaClienteBean = new ArrayList<FichaProcessojuridicoClienteBean>();
				listaClienteBean.add(new FichaProcessojuridicoClienteBean());
			}
			
			if (listaParteContrariaBean!=null){
				Collections.sort(listaParteContrariaBean);
			}else {
				listaParteContrariaBean = new ArrayList<FichaProcessojuridicoParteContrariaBean>();
				listaParteContrariaBean.add(new FichaProcessojuridicoParteContrariaBean());
			}
			
			if (listaQuestionario!=null){
				Collections.sort(listaQuestionario);
			}else {
				listaQuestionario = new ArrayList<Processojuridicoquestionario>();
				listaQuestionario.add(new Processojuridicoquestionario());
			}
			
			bean = new FichaProcessojuridicoBean();
			bean.setCdprocessojuridico(processojuridico.getCdprocessojuridico());
			bean.setArea(processojuridico.getArea());
			bean.setAreatipo(processojuridico.getAreatipo());
			bean.setAdvogado(processojuridico.getAdvogado());
			bean.setSintese(processojuridico.getSintese());
			bean.setListaClienteBean(listaClienteBean);
			bean.setListaParteContrariaBean(listaParteContrariaBean);
			bean.setListaProcessojuridicoquestionario(listaQuestionario);
			listaRelatorio.add(bean);
		}
	}
}