package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorstatusfolha;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorstatusfolhaService extends GenericService<Colaboradorstatusfolha> {	
	
	/* singleton */
	private static ColaboradorstatusfolhaService instance;
	public static ColaboradorstatusfolhaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorstatusfolhaService.class);
		}
		return instance;
	}
	
}
