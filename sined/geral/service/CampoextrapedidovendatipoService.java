package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.dao.CampoextrapedidovendatipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CampoextrapedidovendatipoService  extends GenericService<Campoextrapedidovendatipo>{

	private CampoextrapedidovendatipoDAO campoextrapedidovendatipoDAO;
	
	public void setCampoextrapedidovendatipoDAO(CampoextrapedidovendatipoDAO campoextrapedidovendatipoDAO) {
		this.campoextrapedidovendatipoDAO = campoextrapedidovendatipoDAO;
	}
	
	/**
	 * Busca todos os campos extras de um determinado {@link Pedidovendatipo}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Campoextrapedidovendatipo> findByTipo(Pedidovendatipo pedidovendatipo) {

		return campoextrapedidovendatipoDAO.findByTipo(pedidovendatipo);
	}

}
