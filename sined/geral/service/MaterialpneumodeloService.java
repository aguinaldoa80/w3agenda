package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialpneumodelo;
import br.com.linkcom.sined.geral.dao.MaterialpneumodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.materialpneumodelo.MaterialPneuModeloW3producaoRESTModel;

public class MaterialpneumodeloService extends GenericService<Materialpneumodelo> {
	
	private MaterialpneumodeloDAO materialpneumodeloDAO;
	
	public void setMaterialpneumodeloDAO(
			MaterialpneumodeloDAO materialpneumodeloDAO) {
		this.materialpneumodeloDAO = materialpneumodeloDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2016
	 */
	public List<Materialpneumodelo> findByMaterial(Material material){
		return materialpneumodeloDAO.findByMaterial(material);
	}
	
	/**
	 * Monta a lista de marterialpneumodelo para a sincronizacao com o W3Producao
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2016
	 */
	public List<MaterialPneuModeloW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de marterialpneumodelo para a sincronizacao com o W3Producao
	 * 
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2016
	 */
	public List<MaterialPneuModeloW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialPneuModeloW3producaoRESTModel> lista = new ArrayList<MaterialPneuModeloW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialpneumodelo pm : materialpneumodeloDAO.findForW3Producao(whereIn))
				lista.add(new MaterialPneuModeloW3producaoRESTModel(pm));
		}
		
		return lista;
	}

}
