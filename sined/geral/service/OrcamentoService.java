package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.BdiForm;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Composicaomaoobraformula;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Customaoobraitem;
import br.com.linkcom.sined.geral.bean.Customaoobraitemcargo;
import br.com.linkcom.sined.geral.bean.Dependenciacargo;
import br.com.linkcom.sined.geral.bean.Dependenciacargocomposicao;
import br.com.linkcom.sined.geral.bean.Dependenciafaixa;
import br.com.linkcom.sined.geral.bean.Dependenciafaixacomposicao;
import br.com.linkcom.sined.geral.bean.Divisaotempo;
import br.com.linkcom.sined.geral.bean.Fatormdo;
import br.com.linkcom.sined.geral.bean.Fatormdoitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Orcamentomaterialitem;
import br.com.linkcom.sined.geral.bean.Orcamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Planejamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.geral.bean.Relacaocargo;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorg;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorh;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.auxiliar.OrcamentoTelaEntrada;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.dao.OrcamentoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoHumanoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.RelacaoCargoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiItemForm;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiTributoForm;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportacaoOrcamentoBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoFinanceiroBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoFinanceiroFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class OrcamentoService extends GenericService<Orcamento>{
	
	private OrcamentoDAO orcamentoDAO;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private ComposicaoorcamentoService composicaoorcamentoService;
	private OrcamentomaterialService orcamentomaterialService;
	private TarefaorcamentoService tarefaorcamentoService;
	private RelacaocargoService relacaocargoService;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	private TarefaService tarefaService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private PlanejamentorecursohumanoService planejamentorecursohumanoService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private RecursocomposicaoService recursocomposicaoService;
	private ContagerencialService contagerencialService;
	private ProjetodespesaService projetodespesaService;
	private FatormdoService fatormdoService;
	private CustomaoobraitemService customaoobraitemService;
	private ClienteService clienteService;
	private OportunidadeService oportunidadeService;
	private BdicalculotipoService bdicalculotipoService;
	private OrcamentosituacaoService orcamentosituacaoService;
	private UfService ufService;
	private ComposicaomaoobraService composicaomaoobraService;
	private BdiService bdiService;
	private ProjetotipoService projetotipoService;
	
	public void setFatormdoService(FatormdoService fatormdoService) {this.fatormdoService = fatormdoService;}
	public void setProjetodespesaService(ProjetodespesaService projetodespesaService) {this.projetodespesaService = projetodespesaService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setRecursocomposicaoService(RecursocomposicaoService recursocomposicaoService) {this.recursocomposicaoService = recursocomposicaoService;}
	public void setPlanejamentorecursogeralService(PlanejamentorecursogeralService planejamentorecursogeralService) {this.planejamentorecursogeralService = planejamentorecursogeralService;}
	public void setPlanejamentorecursohumanoService(PlanejamentorecursohumanoService planejamentorecursohumanoService) {this.planejamentorecursohumanoService = planejamentorecursohumanoService;}
	public void setOrcamentorecursohumanoService(OrcamentorecursohumanoService orcamentorecursohumanoService) {this.orcamentorecursohumanoService = orcamentorecursohumanoService;}
	public void setTarefaService(TarefaService tarefaService) {this.tarefaService = tarefaService;}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {this.planejamentoService = planejamentoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}	
	public void setRelacaocargoService(RelacaocargoService relacaocargoService) {this.relacaocargoService = relacaocargoService;}
	public void setTarefaorcamentoService(TarefaorcamentoService tarefaorcamentoService) {this.tarefaorcamentoService = tarefaorcamentoService;}
	public void setOrcamentomaterialService(OrcamentomaterialService orcamentomaterialService) {this.orcamentomaterialService = orcamentomaterialService;}
	public void setComposicaoorcamentoService(ComposicaoorcamentoService composicaoorcamentoService) {this.composicaoorcamentoService = composicaoorcamentoService;}
	public void setOrcamentoDAO(OrcamentoDAO orcamentoDAO) {this.orcamentoDAO = orcamentoDAO;}
	public void setPeriodoorcamentocargoService(PeriodoorcamentocargoService periodoorcamentocargoService) {this.periodoorcamentocargoService = periodoorcamentocargoService;}
	public void setCustomaoobraitemService(CustomaoobraitemService customaoobraitemService) {this.customaoobraitemService = customaoobraitemService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setBdicalculotipoService(BdicalculotipoService bdicalculotipoService) {this.bdicalculotipoService = bdicalculotipoService;}
	public void setOrcamentosituacaoService(OrcamentosituacaoService orcamentosituacaoService) {this.orcamentosituacaoService = orcamentosituacaoService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setComposicaomaoobraService(ComposicaomaoobraService composicaomaoobraService) {this.composicaomaoobraService = composicaomaoobraService;}
	public void setBdiService(BdiService bdiService) {this.bdiService = bdiService;}
	public void setProjetotipoService(ProjetotipoService projetotipoService) {this.projetotipoService = projetotipoService;}
	
	/* 
	 * Deixar sobrescrito mesmo sem nenhuma implementa��o para ser usado
	 * na parte em flex da aplica��o.
	 */
	@Override
	public Orcamento loadForEntrada(Orcamento bean) {
		return super.loadForEntrada(bean);
	}
	
	public OrcamentoTelaEntrada loadForEntradaFlex(Orcamento bean) {
		Orcamento orcamento = super.loadForEntrada(bean);
		return getInfTelaEntradaFlex(orcamento);
	}
	
	public OrcamentoTelaEntrada getInfTelaEntradaFlex(Orcamento orcamento) {
		OrcamentoTelaEntrada orcamentoTelaEntrada = new OrcamentoTelaEntrada();
		
		orcamentoTelaEntrada.setOrcamento(orcamento);
		orcamentoTelaEntrada.setListaCliente(clienteService.findForComboFlex());
		orcamentoTelaEntrada.setListaProjetotipo(projetotipoService.findForComboFlex());
		orcamentoTelaEntrada.setListaOportunidade(oportunidadeService.findForComboFlex());
		orcamentoTelaEntrada.setListaBdicalculotipo(bdicalculotipoService.findForComboFlex());
		orcamentoTelaEntrada.setListaOrcamentosituacao(orcamentosituacaoService.findForComboFlex());
		orcamentoTelaEntrada.setListaUf(ufService.findForComboFlex());
		orcamentoTelaEntrada.setListaContagerencial(contagerencialService.findDebitoForFlex());
		
		return orcamentoTelaEntrada;
	}
	
	/**
	 * Salva o objeto do tipo Orcamento no banco de dados e recalcula o histograma,
	 * caso o par�metro recalcularHistograma seja verdadeiro
	 *
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#atualizaValoresHistograma(Orcamento)
	 * 
	 * @param orcamento
	 * @param recalcularHistograma
	 * @return
	 * 
	 * @author Rodrigo Alvarenga
	 */		
	public void saveOrcamentoFlex(Orcamento orcamento, Boolean recalcularHistograma) {
		this.saveOrUpdate(orcamento);
		
		if (recalcularHistograma) {
			if(orcamento.getCalcularhistograma()){
				periodoorcamentocargoService.atualizaValoresHistograma(orcamento);
			} else {
				periodoorcamentocargoService.deleteByOrcamento(orcamento);
				orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoSemHistogramaFlex(orcamento);
				recursocomposicaoService.atualizaOrcamentoRecursoGeralSemHistogramaFlex(orcamento);
			}
		}
	}
	
	/* 
	 * Deixar sobrescrito mesmo sem nenhuma implementa��o para ser usado
	 * na parte em flex da aplica��o.
	 */
	@Override
	public void saveOrUpdate(Orcamento bean) {
		if(bean != null){
			if(bean.getCdorcamento() != null && bean.getCdorcamento().equals(0)){
				bean.setCdorcamento(null);
			}
			if(bean.getOportunidade() != null && bean.getOportunidade().getCdoportunidade().equals(0)){
				bean.setOportunidade(null);
			}
			super.saveOrUpdate(bean);
		}
	}

	/**
	 * Retorna uma lista de or�amentos de um determinado cliente
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#findByClienteForFlex
	 * @param cliente
	 * @return lista de Orcamento
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Orcamento> findByClienteForFlex(Cliente cliente) {
		return orcamentoDAO.findByClienteForFlex(cliente);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#findForListagemFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamento> findForListagemFlex() {
		return this.findForListagemFlex(new OrcamentoFiltro());
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#findForListagemFlex
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamento> findForListagemFlex(OrcamentoFiltro filtro) {
		return orcamentoDAO.findForListagemFlex(filtro);
	}
	
	/**
	 * Deleta o orcamento.
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteFlex(Orcamento orcamento){
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		try {
			delete(orcamento);
		} catch (DataAccessException da) {
			throw new SinedException("Or�amento n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
		} catch (Exception e) {
			throw new SinedException(e.getMessage());
		}
	}
	
	/**
	 * Copia o or�amento j� salvando no banco.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#salvarOrcamento
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaComposicao
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaListaMaterial
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaTarefa
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaRelacao
	 * 
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String copiarOrcamento(Orcamento orcamento){
		try{
			if(orcamento == null){
				throw new SinedException("Or�amento n�o pode ser nulo.");
			}
			
			List<Composicaoorcamento> listaComposicao = orcamento.getListaCopiarComposicao();
			List<Orcamentomaterial> listaOrcamentomaterial = orcamento.getListaCopiarListaMaterial();
			Boolean tarefa = orcamento.getCopiartarefa();
			Boolean relacao = orcamento.getCopiarrelacao();
			Boolean manterprecoorcamento = orcamento.getManterprecoorcamento();
			Integer cdorcamentoantigo = orcamento.getCdorcamentoantigo();
			
			this.salvarOrcamento(orcamento);
			
			if(listaComposicao != null && listaComposicao.size() > 0){
				this.copiaComposicao(listaComposicao, orcamento);
			}
			
			if(listaOrcamentomaterial != null && listaOrcamentomaterial.size() > 0){
				this.copiaListaMaterial(listaOrcamentomaterial, orcamento);
			}
			
			if(tarefa != null && tarefa){
				this.copiaTarefa(new Orcamento(cdorcamentoantigo), orcamento);
				this.copiaRecursoHumano(new Orcamento(cdorcamentoantigo), orcamento);
				if(manterprecoorcamento){
					this.copiaRecursoGeral(new Orcamento(cdorcamentoantigo), orcamento);
				}
			}
			
			if(relacao != null && relacao){
				this.copiaRelacao(new Orcamento(cdorcamentoantigo), orcamento);
			}
			
			if(orcamento.getCopiarcustomaoobraitem() != null && orcamento.getCopiarcustomaoobraitem()){
				try {
					this.copiaCustomaoobraitem(new Orcamento(cdorcamentoantigo), orcamento);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if(orcamento.getCopiarcomposicaomaoobra() != null && orcamento.getCopiarcomposicaomaoobra()){
				this.copiaComposicaomaoobra(new Orcamento(cdorcamentoantigo), orcamento);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}

	private void copiaRecursoHumano(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Orcamentorecursohumano> lista = orcamentorecursohumanoService.findForListagemFlex(new OrcamentoRecursoHumanoFiltro(orcamentoAntigo));
		
		for (Orcamentorecursohumano orcamentorecursohumano : lista) {
			Fatormdo fatormdo = orcamentorecursohumano.getFatormdo();
			if(fatormdo != null && fatormdo.getFormula() != null){
				fatormdo.setCdfatormdo(null);
				fatormdo.setOrcamento(orcamentoNovo);
				
				if(fatormdo.getListaFatormdoitem() != null){
					for (Fatormdoitem fatormdoitem : fatormdo.getListaFatormdoitem()) {
						fatormdoitem.setCdfatormdoitem(null);
					}
				}
				
				fatormdoService.saveOrUpdate(fatormdo);
				orcamentorecursohumano.setFatormdo(fatormdo);
			}
			
			orcamentorecursohumano.setCdorcamentorecursohumano(null);
			orcamentorecursohumano.setOrcamento(orcamentoNovo);
			orcamentorecursohumanoService.saveOrUpdate(orcamentorecursohumano);
		}
	}
	
	private void copiaRecursoGeral(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Recursocomposicao> lista = recursocomposicaoService.findRecursoDiretoByOrcamento(orcamentoAntigo);
		
		for (Recursocomposicao recursocomposicao : lista) {
			recursocomposicao.setCdrecursocomposicao(null);
			recursocomposicao.setOrcamento(orcamentoNovo);
			
			recursocomposicaoService.saveOrUpdate(recursocomposicao);
		}
	}
	
	/**
	* M�todo que copia o item de custo de m�o de obra do or�amento selecionado para o novo or�amento
	*
	* @param orcamentoAntigo
	* @param orcamentoNovo
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	private void copiaCustomaoobraitem(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Customaoobraitem> lista = customaoobraitemService.buscarCustomaoobraitemParaCopia(orcamentoAntigo);
		
		if(SinedUtil.isListNotEmpty(lista)){
			for (Customaoobraitem customaoobraitem : lista) {
				customaoobraitem.setCdcustomaoobraitem(null);
				customaoobraitem.setOrcamento(orcamentoNovo);
				
				if(SinedUtil.isListNotEmpty(customaoobraitem.getListaCustomaoobraitemcargo())){
					for(Customaoobraitemcargo customaoobraitemcargo : customaoobraitem.getListaCustomaoobraitemcargo()){
						customaoobraitemcargo.setCdcustomaoobraitemcargo(null);
						customaoobraitemcargo.setCustomaoobraitem(customaoobraitem);
					}
				}
				customaoobraitemService.saveOrUpdate(customaoobraitem);
			}
		}
	}
	
	/**
	* M�todo que copia a composi��o de m�o de obra do or�amento selecionado para o novo or�amento 
	*
	* @param orcamentoAntigo
	* @param orcamentoNovo
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	private void copiaComposicaomaoobra(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Composicaomaoobra> lista = composicaomaoobraService.buscarComposicaomaoobraParaCopia(orcamentoAntigo);
		
		if(SinedUtil.isListNotEmpty(lista)){
			for (Composicaomaoobra composicaomaoobra : lista) {
				composicaomaoobra.setCdcomposicaomaoobra(null);
				composicaomaoobra.setOrcamento(orcamentoNovo);
				
				if(SinedUtil.isListNotEmpty(composicaomaoobra.getListaComposicaomaoobraformula())){
					for(Composicaomaoobraformula composicaomaoobraformula : composicaomaoobra.getListaComposicaomaoobraformula()){
						composicaomaoobraformula.setCdcomposicaomaoobraformula(null);
						composicaomaoobraformula.setComposicaomaoobra(composicaomaoobra);
					}
				}
				
				composicaomaoobraService.saveOrUpdate(composicaomaoobra);
			}
		}
	}
	
	/**
	 * Copia as rela��es entre cargos de um or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RelacaocargoService#findForListagemFlex
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdate
	 *
	 * @param orcamentoAntigo
	 * @param orcamentoNovo
	 * @author Rodrigo Freitas
	 */
	private void copiaRelacao(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Relacaocargo> listaRelacao = relacaocargoService.findForListagemFlex(new RelacaoCargoFiltro(orcamentoAntigo));
		
		for (Relacaocargo rc : listaRelacao) {
			rc.setCdrelacaocargo(null);
			
			if(rc.getListaDependenciacargo() != null && rc.getListaDependenciacargo().size() > 0){
				for (Dependenciacargo dc : rc.getListaDependenciacargo()) {
					dc.setCddependenciacargo(null);
				}
			}
			
			if(rc.getListaDependenciafaixa() != null && rc.getListaDependenciafaixa().size() > 0){
				for (Dependenciafaixa df : rc.getListaDependenciafaixa()) {
					df.setCddependenciafaixa(null);
				}
			}
			rc.setOrcamento(orcamentoNovo);
			
			relacaocargoService.saveOrUpdate(rc);
		}
	}
	
	/**
	 * Copia as tarefas do or�amento antigo que foi passado por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#getListaDPFlex
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#retiraCodTarefasFilhas
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#sincronizaListaFlex
	 * 
	 * @param orcamentoAntigo
	 * @param orcamentoNovo
	 * @author Rodrigo Freitas
	 */
	private void copiaTarefa(Orcamento orcamentoAntigo, Orcamento orcamentoNovo) {
		List<Tarefaorcamento> listaTarefa = tarefaorcamentoService.getListaDPFlex(orcamentoAntigo);
		
		retiraCodTarefasFilhas(listaTarefa.get(0).getListaTarefaFilha(), orcamentoNovo);
		
		tarefaorcamentoService.sincronizaListaFlex(listaTarefa, true, orcamentoNovo, false);		
	}
	
	/**
	 * Retira o c�digo e coloca -1 nas tarefas e chama recurivamente para as filhas daquela tarefa tamb�m.
	 *
	 * @param lista
	 * @param orcamentoNovo
	 * @author Rodrigo Freitas
	 */
	private void retiraCodTarefasFilhas(List<Tarefaorcamento> lista,Orcamento orcamentoNovo) {
		
		for (Tarefaorcamento to : lista) {
			to.setCdtarefaorcamento(-1);
			to.setOrcamento(orcamentoNovo);
			
			if(to.getListaTarefaorcamentorg() != null){
				for (Tarefaorcamentorg rg : to.getListaTarefaorcamentorg()) {
					rg.setCdtarefaorcamentorg(null);
				}
			}
			
			if(to.getListaTarefaorcamentorh() != null){
				for (Tarefaorcamentorh rh : to.getListaTarefaorcamentorh()) {
					rh.setCdtarefaorcamentorh(null);
				}
			}
						
			if(to.getListaTarefaFilha() != null && to.getListaTarefaFilha().size() > 0){
				retiraCodTarefasFilhas(to.getListaTarefaFilha(), orcamentoNovo);
			}
		}
		
	}
	
	/**
	 * Copia a(s) lista(s) de material que estavam no or�amento antigo.
	 *
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#findForCopiaOrcamento
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#saveOrUpdate
	 *
	 * @param listaOrcamentomaterial
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	private void copiaListaMaterial(List<Orcamentomaterial> listaOrcamentomaterial, Orcamento orcamento) {
		String whereIn = CollectionsUtil.listAndConcatenate(listaOrcamentomaterial, "cdorcamentomaterial", ",");
		
		listaOrcamentomaterial = orcamentomaterialService.findForCopiaOrcamento(whereIn);
		
		for (Orcamentomaterial om : listaOrcamentomaterial) {
			om.setCdorcamentomaterial(null);
			if(om.getListaOrcamentomaterialitem() != null){
				for (Orcamentomaterialitem it : om.getListaOrcamentomaterialitem()) {
					it.setCdorcamentomaterialitem(null);
				}
			}
			om.setOrcamento(orcamento);
			
			orcamentomaterialService.saveOrUpdate(om);
		}
		
	}
	
	/**
	 * Copia a lista de composi��o que est� no or�amento antigo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#findForCopiaOrcamento
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#saveOrUpdate
	 *
	 * @param listaComposicao
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	private void copiaComposicao(List<Composicaoorcamento> listaComposicao, Orcamento orcamento) {
		String whereIn = CollectionsUtil.listAndConcatenate(listaComposicao, "cdcomposicaoorcamento", ",");
		
		listaComposicao = composicaoorcamentoService.findForCopiaOrcamento(whereIn);
		
		for (Composicaoorcamento co : listaComposicao) {
			co.setCdcomposicaoorcamento(null);
			if(co.getListaRecursocomposicao() != null){
				for (Recursocomposicao rc : co.getListaRecursocomposicao()) {
					rc.setCdrecursocomposicao(null);
					if(rc.getListaDependenciacargocomposicao() != null){
						for (Dependenciacargocomposicao dc : rc.getListaDependenciacargocomposicao()) {
							dc.setCddependenciacargocomposicao(null);
						}
					}
					if(rc.getListaDependenciafaixacomposicao()!= null){
						for (Dependenciafaixacomposicao dc : rc.getListaDependenciafaixacomposicao()) {
							dc.setCddependenciafaixacomposicao(null);
						}
					}
				}
			}
			co.setOrcamento(orcamento);
			
			composicaoorcamentoService.saveOrUpdate(co);
		}
	}

	/**
	 * Salva o or�amento.
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	private void salvarOrcamento(Orcamento orcamento) {
		orcamento.setCdorcamento(null);
		orcamento.setOrcamentosituacao(Orcamentosituacao.EM_ABERTO);
		this.saveOrUpdate(orcamento);
	}
	
	/**
	 * Executa a importa��o do or�amento para projeto.
	 *
	 * @see br.com.linkcom.sined.modulo.projeto.controller.process.bean#ImportacaoOrcamentoBean
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaProjeto
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiaPlanejamento
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiarTarefasProjeto
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiarModProjeto
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiarMoiProjeto
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiarRecursoProjeto
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#copiarDespesasProjeto
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String importaOrcamento(final ImportacaoOrcamentoBean bean){
		try{
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					
					Projeto prj = copiaProjeto(bean);
					Planejamento plan = copiaPlanejamento(bean, prj);
					
					Orcamento orcamento = new Orcamento(bean.getCdorcamento());
					
					if(bean.getEapTarefa()){
						copiarTarefasProjeto(orcamento, plan, bean.getMatTarefa(), bean.getModTarefa());
					}
					
					if(bean.getMod() || bean.getMoi()){
						copiarMDOProjeto(orcamento, plan, bean.getMod(), bean.getMoi());
					}
					
					if(bean.getListaComposicao() != null && bean.getListaComposicao().size() > 0){
						copiarRecursoProjeto(orcamento, plan, bean.getListaComposicao());
					}
					
					copiarDespesasProjeto(orcamento, prj, bean.getDespesasMateriais(), bean.getDespesasMod(), bean.getDespesasMoi());
					
					return null;
				}
			});
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PLANEJAMENTO_DESCRICAO")) {
				return "Nome do planejamento j� cadastrado no sistema.";
			} else if (DatabaseError.isKeyPresent(e, "idx_projeto_nome")) {
				return "Nome do projeto j� cadastrado no sistema.";
			} else {
				return e.getMessage();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		
		return null;
	}
	
	/**
	 * Cria as despesas do projeto a partir dos dados do or�amento.
	 *
	 * @param orcamento
	 * @param prj
	 * @param despesasMateriais
	 * @param despesasMod
	 * @param despesasMoi
	 * @author Rodrigo Freitas
	 * @since 17/01/2013
	 */
	private void copiarDespesasProjeto(Orcamento orcamento, Projeto prj, Boolean despesasMateriais, Boolean despesasMod, Boolean despesasMoi) {
		List<Projetodespesa> listaProjetodespesa = new ArrayList<Projetodespesa>();
		if(despesasMateriais){
			List<Projetodespesa> listaPD = this.buscarDespesasMateriais(orcamento);
			if(SinedUtil.isListNotEmpty(listaPD)){
				listaProjetodespesa.addAll(listaPD);
			}
		}
		
		if(despesasMod){
			List<Projetodespesa> listaPD = this.getDespesaMod(orcamento);
			if(SinedUtil.isListNotEmpty(listaPD)){
				for(Projetodespesa projetodespesa : listaPD){
					projetodespesa.setProjeto(prj);
				}
				listaProjetodespesa.addAll(listaPD);
			}
		}
		
		if(despesasMoi){
			List<Projetodespesa> listaPD = this.getDespesaMoi(orcamento);
			if(SinedUtil.isListNotEmpty(listaPD)){
				for(Projetodespesa projetodespesa : listaPD){
					projetodespesa.setProjeto(prj);
				}
				listaProjetodespesa.addAll(listaPD);
			}
		}
		
		BdiForm bdiForm = bdiService.getByOrcamentoFlex(orcamento);
		if(bdiForm != null){
			Money precocusto = bdiForm.getPrecoCusto() != null ? bdiForm.getPrecoCusto() : new Money();
			if(SinedUtil.isListNotEmpty(bdiForm.getListaBdiItemForm())){
				for(BdiItemForm bdiItemForm : bdiForm.getListaBdiItemForm()){
					addProjetodespesaBdiItem(bdiItemForm, listaProjetodespesa, prj, precocusto);
				}
			}
			if(SinedUtil.isListNotEmpty(bdiForm.getListaBdiTributoForm())){
				for(BdiTributoForm bdiTributoForm : bdiForm.getListaBdiTributoForm()){
					addProjetodespesaBdiTributo(bdiTributoForm, listaProjetodespesa, prj, precocusto);
				}
			}
		}
		
		List<Projetodespesa> listaPDCustomaoobraitem = copiarCustomaoobraitem(orcamento, prj);
		if(SinedUtil.isListNotEmpty(listaPDCustomaoobraitem)){
			listaProjetodespesa.addAll(listaPDCustomaoobraitem);
		}
		
		if(SinedUtil.isListNotEmpty(listaProjetodespesa)){
			projetodespesaService.agruparListaPD(listaProjetodespesa);
			this.saveListaProjetoDespesa(prj, listaProjetodespesa);
		}
	}
	
	/**
	* M�todo que adicionar uma despesa de projeto de acordo com o item do bdi
	*
	* @param bdiItemForm
	* @param lista
	* @param prj
	* @param precocusto
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	private void addProjetodespesaBdiItem(BdiItemForm bdiItemForm, List<Projetodespesa> lista, Projeto prj, Money precocusto) {
		if (bdiItemForm.getListaBdiItemFormFilho() != null && !bdiItemForm.getListaBdiItemFormFilho().isEmpty()) {
			for (BdiItemForm filho : bdiItemForm.getListaBdiItemFormFilho()) {
				addProjetodespesaBdiItem(filho, lista, prj, precocusto);		
			}
		}else if(bdiItemForm.getContagerencial() != null){
			Projetodespesa projetodespesa = new Projetodespesa();
			projetodespesa.setProjeto(prj);
			projetodespesa.setContagerencial(bdiItemForm.getContagerencial());
			if(bdiItemForm.getValor() != null){
				projetodespesa.setValortotal(new Money(bdiItemForm.getValor()*precocusto.getValue().doubleValue()/100));
			}else {
				projetodespesa.setValortotal(new Money());
			}
			lista.add(projetodespesa);
		}
	}
	
	/**
	* M�todo que adicionar uma despesa de projeto de acordo com o tributo do bdi
	*
	* @param bdiTributoForm
	* @param lista
	* @param prj
	* @param precocusto
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	private void addProjetodespesaBdiTributo(BdiTributoForm bdiTributoForm, List<Projetodespesa> lista, Projeto prj, Money precocusto) {
		if (bdiTributoForm.getListaBdiTributoFormFilho() != null && !bdiTributoForm.getListaBdiTributoFormFilho().isEmpty()) {
			for (BdiTributoForm filho : bdiTributoForm.getListaBdiTributoFormFilho()) {
				addProjetodespesaBdiTributo(filho, lista, prj, precocusto);		
			}
		}else if(bdiTributoForm.getContagerencial() != null){
			Projetodespesa projetodespesa = new Projetodespesa();
			projetodespesa.setProjeto(prj);
			projetodespesa.setContagerencial(bdiTributoForm.getContagerencial());
			if(bdiTributoForm.getValor() != null){
				projetodespesa.setValortotal(new Money(bdiTributoForm.getValor()*precocusto.getValue().doubleValue()/100));
			}else {
				projetodespesa.setValortotal(new Money());
			}
			lista.add(projetodespesa);
		}
	}
	
	/**
	 * Calcula despesas de MOI
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/01/2013
	 */
	private List<Projetodespesa> getDespesaMoi(Orcamento orcamento) {
		Orcamento orcamento_new = this.load(orcamento, "orcamento.cdorcamento, orcamento.contagerencialmoi");
		List<Projetodespesa> listaProjetodespesa = new ArrayList<Projetodespesa>();
		
		if(existeItemCustoEComposicaoMO(orcamento)){
			Orcamento orcamento_aux = new Orcamento(orcamento.getCdorcamento());
			if(orcamento_new != null)
				orcamento_aux.setContagerencialmoi(orcamento_new.getContagerencialmoi());
			listaProjetodespesa =  orcamentorecursohumanoService.calculaCustoTotalQtdeMDOContagerencial(orcamento_aux, Tipocargo.MOI);
		}else if(orcamento_new != null && orcamento_new.getContagerencialmod() != null){
			Double[] resultados = orcamentorecursohumanoService.calculaCustoTotalQtdeMDO(orcamento_new, Tipocargo.MOI);
			
			Money valortotal = new Money(resultados[0]);
			Double qtdetotal = resultados[1];
			
			Money valorhora = qtdetotal > 0 ? valortotal.divide(new Money(qtdetotal)) : new Money();

			listaProjetodespesa.add(new Projetodespesa(orcamento_new.getContagerencialmod(), valortotal, valorhora));
		}
		
		return listaProjetodespesa;
	}
	
	private List<Projetodespesa> getDespesaMod(Orcamento orcamento) {
		Orcamento orcamento_new = this.load(orcamento, "orcamento.cdorcamento, orcamento.contagerencialmod");
		List<Projetodespesa> listaProjetodespesa = new ArrayList<Projetodespesa>();
		
		if(existeItemCustoEComposicaoMO(orcamento)){
			Orcamento orcamento_aux = new Orcamento(orcamento.getCdorcamento());
			if(orcamento_new != null)
				orcamento_aux.setContagerencialmod(orcamento_new.getContagerencialmod());
			listaProjetodespesa =  orcamentorecursohumanoService.calculaCustoTotalQtdeMDOContagerencial(orcamento_aux, Tipocargo.MOD);
		}else if(orcamento_new != null && orcamento_new.getContagerencialmod() != null){
			Double[] resultados = orcamentorecursohumanoService.calculaCustoTotalQtdeMDO(orcamento_new, Tipocargo.MOD);
			
			Money valortotal = new Money(resultados[0]);
			Double qtdetotal = resultados[1];
			
			Money valorhora = qtdetotal > 0 ? valortotal.divide(new Money(qtdetotal)) : new Money();

			listaProjetodespesa.add(new Projetodespesa(orcamento_new.getContagerencialmod(), valortotal, valorhora));
		}
		
		return listaProjetodespesa;
	}
	
	/**
	 * Busca as despesas do or�amento dos materiais.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/01/2013
	 */
	private List<Projetodespesa> buscarDespesasMateriais(Orcamento orcamento) {
		return orcamentoDAO.buscarDespesasMateriais(orcamento);
	}
	
	/**
	 * Salva a lista de projetodespesa.
	 *
	 * @param prj
	 * @param listaProjetodespesa
	 * @author Rodrigo Freitas
	 * @since 17/01/2013
	 */
	private void saveListaProjetoDespesa(Projeto prj, List<Projetodespesa> listaProjetodespesa) {
		if(listaProjetodespesa != null && !listaProjetodespesa.isEmpty()){
			for (Projetodespesa projetodespesa : listaProjetodespesa) {
				if(projetodespesa.getContagerencial() != null){
					projetodespesa.setProjeto(prj);
					projetodespesaService.saveOrUpdate(projetodespesa);
				}
			}
		}
	}
	
	/**
	 * Copia o projeto do or�amento e salva no banco.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Projeto copiaProjeto(ImportacaoOrcamentoBean bean) {
		
		Projeto prj = new Projeto();
		
		prj.setNome(bean.getDescricaoProjeto());
		prj.setProjetotipo(bean.getProjetotipoProjeto());
		prj.setSigla(bean.getSiglaProjeto());
		prj.setDtprojeto(bean.getDtProjeto());
		prj.setDtfimprojeto(bean.getDtfimProjeto());
		prj.setMunicipio(bean.getMunicipioProjeto());
		prj.setEndereco(bean.getEnderecoProjeto());
		prj.setColaborador(bean.getResponsavelProjeto());
		prj.setCliente(bean.getClienteProjeto());
		prj.setSituacao(Situacaoprojeto.EM_ESPERA);
		prj.setEmpresa(bean.getEmpresaProjeto());
		prj.setCentrocusto(bean.getCentrocustoProjeto());
		
		Orcamento orcamento = loadForEntrada(new Orcamento(bean.getCdorcamento()));
		prj.setOportunidade(orcamento.getOportunidade());
		
		projetoService.saveOrUpdateNoUseTransaction(prj);
		
		return prj;
	}
	
	/**
	 * Copia o planejamento do or�amneto e salva no banco.
	 *
	 * @param bean
	 * @param prj
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Planejamento copiaPlanejamento(ImportacaoOrcamentoBean bean, Projeto prj) {
		
		Planejamento plan = new Planejamento();
		
		plan.setProjeto(prj);
		plan.setDivisaotempo(Divisaotempo.SEMANA);
		plan.setPlanejamentosituacao(Planejamentosituacao.EM_ABERTO);
		
		plan.setOrcamento(new Orcamento(bean.getCdorcamento()));
		plan.setDescricao(bean.getDescricaoPlanej());
		plan.setVersao(Integer.parseInt(bean.getVersaoPlanej()));
		plan.setTamanhotempo(Integer.parseInt(bean.getTamanhotempoPlanej()));
		plan.setDtinicio(bean.getDtinicioPlanej());
		plan.setDtfim(bean.getDtfimPlanej());
		if(bean.getCustoPlanej() != null && !bean.getCustoPlanej().equals("")) plan.setCusto(new Money(Double.parseDouble(bean.getCustoPlanej().replace(",", "."))));
		
		planejamentoService.saveOrUpdateNoUseTransaction(plan);
		
		return plan;
	}
	
	/**
	 * Copiar recursos gerais de um or�amento para o planejamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#findByOrcamentoForFlex
	 *
	 * @param orcamento
	 * @param plan
	 * @param listaComposicao
	 * @author Rodrigo Freitas
	 */
	private void copiarRecursoProjeto(Orcamento orcamento, Planejamento plan, List<Composicaoorcamento> listaComposicao) {
		Planejamentorecursogeral prg;
		listaComposicao = composicaoorcamentoService.findByOrcamentoForFlex(orcamento, null);
		
		for (Composicaoorcamento co : listaComposicao) {
			for (Recursocomposicao rc : co.getListaRecursocomposicao()) {
				if(rc.getMaterial() != null){
					prg = new Planejamentorecursogeral();
					
					prg.setPlanejamento(plan);
					
					prg.setMaterial(rc.getMaterial());
					prg.setQtde(rc.getQuantidadecalculada()*rc.getNumocorrencia());
					
					planejamentorecursogeralService.saveOrUpdateNoUseTransaction(prg);
				}
			}
		}
	}
	
	/**
	 * Copiar os recursos humanos de um or�amento para o planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#findByOrcamento
	 *
	 * @param orcamento
	 * @param plan
	 * @param mod
	 * @param moi
	 * @author Rodrigo Freitas
	 */
	private void copiarMDOProjeto(Orcamento orcamento, Planejamento plan, Boolean mod, Boolean moi) {
		List<Orcamentorecursohumano> lista = orcamentorecursohumanoService.findByOrcamento(orcamento, mod, moi);
		Planejamentorecursohumano prh;
		
		for (Orcamentorecursohumano orh : lista) {
			prh = new Planejamentorecursohumano();
			
			prh.setCargo(orh.getCargo());
			prh.setQtde(new Double(orh.getTotalhoras()));
			
			prh.setPlanejamento(plan);
			
			planejamentorecursohumanoService.saveOrUpdateNoUseTransaction(prh);
		}
	}
	
	/**
	 * Copia as tarefas do or�amento para o planejamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#findByOrcamento
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#geraListaRG
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#geraListaRH
	 * 
	 * @param orcamento
	 * @param plan
	 * @param rg
	 * @param rh
	 * @author Rodrigo Freitas
	 */
	private void copiarTarefasProjeto(Orcamento orcamento, Planejamento plan, Boolean rg, Boolean rh) {
		
		List<Tarefaorcamento> listaTarefa = tarefaorcamentoService.findByOrcamento(orcamento); 
		Tarefa tp;
		List<Tarefa> lista = new ArrayList<Tarefa>();
		
		Map<Integer, Tarefa> mapCodigo = new HashMap<Integer, Tarefa>();
		
		for (Tarefaorcamento to : listaTarefa) {
			tp = new Tarefa();
			
			tp.setDescricao(to.getDescricao());
			tp.setDtfim(to.getDtfim());
			tp.setDtinicio(to.getDtinicio());
			tp.setDuracao(to.getDuracao());
			tp.setId(to.getId());
			tp.setIndice(to.getIndice());
			tp.setUnidademedida(to.getIndice() != null ? to.getIndice().getUnidademedida() : null);
			tp.setPredecessoras(to.getPredecessoras());
			tp.setQtde(to.getQtde());
			tp.setCustototal(to.getCustototal());
			
			if(rg){
				tp.setListaTarefarecursogeral(new ListSet<Tarefarecursogeral>(Tarefarecursogeral.class,
						this.geraListaRG(to.getListaTarefaorcamentorg())));
			}
			
			if(rh){
				tp.setListaTarefarecursohumano(new ListSet<Tarefarecursohumano>(Tarefarecursohumano.class,
						this.geraListaRH(to.getListaTarefaorcamentorh())));
			}
			
			tp.setPlanejamento(plan);
			tarefaService.verificaSitucaotarefa(tp);
			
			tarefaService.saveOrUpdateNoUseTransaction(tp);
			
			tp.setCdtarefapai(to.getTarefapai() != null ? to.getTarefapai().getCdtarefaorcamento() : null);
			mapCodigo.put(to.getCdtarefaorcamento(), tp);
			
			lista.add(tp);
		}
		
		for (Tarefa tarefa : lista) {
			if(tarefa.getCdtarefapai() != null){
				tarefa.setTarefapai(mapCodigo.get(tarefa.getCdtarefapai()));
			}
			
			tarefaService.saveOrUpdateNoUseTransaction(tarefa);		
		}
		
	}
	
	private List<Projetodespesa> copiarCustomaoobraitem(Orcamento orcamento, Projeto projeto) {
		List<Projetodespesa> listaProjetodespesa = new ArrayList<Projetodespesa>();
		if(orcamento != null && orcamento.getCdorcamento() != null && projeto != null && projeto.getCdprojeto() != null){
			List<Customaoobraitem> listaCustomaoobraitem = customaoobraitemService.findForImportarOrcamento(orcamento);
			if(SinedUtil.isListNotEmpty(listaCustomaoobraitem)){
				for(Customaoobraitem customaoobraitem : listaCustomaoobraitem){
					if(customaoobraitem.getContagerencial() != null){
						Projetodespesa projetodespesa = new Projetodespesa();
						projetodespesa.setProjeto(projeto);
						projetodespesa.setContagerencial(customaoobraitem.getContagerencial());
						projetodespesa.setValortotal(customaoobraitem.getCustovenda());
						listaProjetodespesa.add(projetodespesa);
					}
				}
			}
		}
		return listaProjetodespesa;
	}
	
	/**
	 * Gera a lista de <code>Tarefarecursohumano</code> a partir de uma lista <code>Tarefaorcamentorh</code>
	 *
	 * @param listaTarefaorcamentorh
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Tarefarecursohumano> geraListaRH(List<Tarefaorcamentorh> listaTarefaorcamentorh) {
		List<Tarefarecursohumano> lista = new ArrayList<Tarefarecursohumano>();
		Tarefarecursohumano trh;
		
		for (Tarefaorcamentorh tarefaorcamentorh : listaTarefaorcamentorh) {
			trh = new Tarefarecursohumano();
			
			trh.setQtde(tarefaorcamentorh.getQuantidade());
			trh.setCargo(tarefaorcamentorh.getCargo());
			
			lista.add(trh);
		}
		return lista;
	}
	
	/**
	 * Gera a lista de <code>Tarefarecursogeral</code> a partir de uma lista <code>Tarefaorcamentorg</code>
	 *
	 * @param listaTarefaorcamentorg
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Tarefarecursogeral> geraListaRG(List<Tarefaorcamentorg> listaTarefaorcamentorg) {
		List<Tarefarecursogeral> lista = new ArrayList<Tarefarecursogeral>();
		Tarefarecursogeral trg;
		
		for (Tarefaorcamentorg tarefaorcamentorg : listaTarefaorcamentorg) {
			trg = new Tarefarecursogeral();
			
			trg.setQtde(tarefaorcamentorg.getQuantidade());
			trg.setMaterial(tarefaorcamentorg.getMaterial());
			trg.setUnidademedida(tarefaorcamentorg.getUnidademedida());
			trg.setOutro(tarefaorcamentorg.getOutro());
			trg.setTiporecursogeral(tarefaorcamentorg.getTiporecursogeral());
			
			lista.add(trg);
		}
		return lista;
	}
	
	/**
	 * M�todo para verificar qual aplica��o est� sendo acessada, para a parte em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean getUrlImage(){
		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
		String url = servletRequest.getRequestURL().toString();
		
		String[] urlDividida = url.split("/");
		
		String servidor = urlDividida[2];
		
		urlDividida = servidor.split(".");
				
		if(urlDividida.length==0){
			urlDividida = servidor.split(":");
			if(urlDividida.length>0){				
				servidor = urlDividida[0];
			}
//			if(!servidor.toUpperCase().equals("LOCALHOST")){
			if(!"www.sined.com.br".equals(servidor.toLowerCase())){
				return false;
			}
		} else if(urlDividida.length > 1 && urlDividida[1] != null && urlDividida[1].toUpperCase().equals("W3ERP")){
			return false;
		}
		
		return true;
	}	
	
	/**
	 * M�todo que gera relat�rio para project das tarefas do or�amento
	 * 
	 * @see TarefaorcamentoService#findByOrcamento(Orcamento)
	 * @see #adicionaRowArquivoCSV(StringBuilder, Tarefaorcamento)
	 * @param orcamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource preparaArquivoTarefaCSV(Orcamento orcamento) {
		List<Tarefaorcamento> lista = tarefaorcamentoService.findByOrcamento(orcamento);
		
		StringBuilder csv = new StringBuilder();
		if(lista == null || lista.size() == 0)
			return new Resource("text/csv", "exportartarefaorcamento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		
		int i = 1;
		for (Tarefaorcamento tarefa : lista) {
			if(i == 1)
				csv.append("Nome;Dura��o;In�cio;T�rmino;Predecessoras;N�vel da estrutura de t�picos");
			
			/*Se tiver pai procura o pai selecionado para cria nivel de estrutura se nao achar
			 ele adiciona na lista como se fosse um pai*/
			if(tarefa.getTarefapai() != null && tarefa.getTarefapai().getCdtarefaorcamento() != null){
				if(lista.contains(tarefa.getTarefapai())){
					for (Tarefaorcamento tarefaPai : lista) {
						if(tarefaPai.equals(tarefa.getTarefapai())){
							tarefaPai.setNivelEstrutura(tarefaPai.getNivelEstrutura()+1);
							tarefa.setNivelEstrutura(tarefaPai.getNivelEstrutura());
							adicionaRowArquivoCSV(csv, tarefa);
							break;
						}
					}
					continue;
				} 
			}
			adicionaRowArquivoCSV(csv, tarefa);
			
			i++;
		}
		
		return new Resource("text/csv", "exportartarefaorcamento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
	}
	
	/**
	 * M�todo que seta valores de cada fileira da tarefa
	 * 
	 * @param csv
	 * @param tarefa
	 * @author Tom�s Rabelo
	 */
	private void adicionaRowArquivoCSV(StringBuilder csv, Tarefaorcamento tarefa) {
		csv.append("\n");
		csv.append(tarefa.getDescricao() != null ?tarefa.getDescricao()+";" : ";");
		csv.append(tarefa.getDuracao() != null ? tarefa.getDuracao()+";" : ";");
		csv.append(tarefa.getDtinicio() != null ? new SimpleDateFormat("dd/MM/yyyy").format(tarefa.getDtinicio())+";" : ";");
		csv.append(tarefa.getDtfim() != null ? new SimpleDateFormat("dd/MM/yyyy").format(tarefa.getDtfim())+";" : ";");
		csv.append(tarefa.getPredecessoras() != null ? tarefa.getPredecessoras()+";" : ";");
		csv.append(tarefa.getNivelEstrutura());
	}
	
	/**
	 * M�todo que verifica se o or�amento possui tarefas
	 * 
	 * @param orcamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean orcamentoPossuiTarefa(Orcamento orcamento){
		return tarefaorcamentoService.orcamentoPossuiTarefa(orcamento);
	}
	
	/**
	 * Cria o relat�rio de acompanhamento financeiro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#findForAcompanhamentoFinanceiro
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createReportAcompanhamentoFinanceiro(AcompanhamentoFinanceiroFiltro filtro) {
		Report report = new Report("/projeto/acompanhamentoFinanceiro");
		
		Planejamento planejamento = planejamentoService.loadWithOrcamento(filtro.getPlanejamento());
		
		if(planejamento != null) {
			List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
			
			// BUSCA A LISTA DE DESPESA DO PROJETO
			List<Projetodespesa> listaProjetodespesa = projetodespesaService.findByProjetoData(planejamento.getProjeto(), null);
			for (Projetodespesa projetodespesa : listaProjetodespesa) {
				lista.add(new AcompanhamentoFinanceiroBean(projetodespesa.getContagerencial(), projetodespesa.getValortotal().getValue().doubleValue(), 0d));
			}
			
//			Orcamento orcamento = planejamento.getOrcamento();
//			if(orcamento != null){
//				// RECURSO GERAL DA TAREFA
//				mergeLista(this.calculaRecursoGeralTarefa(orcamento), lista); 
//				
//				// RECURSO GERAL DA COMPOSI��O
//				mergeLista(this.calculaRecursoGeralCamposicao(orcamento), lista); 
//				
//				// RECURSO HUMANO DIRETO
//				mergeLista(this.calculaRecursoHumanoDireto(orcamento), lista);
//				
//				// RECURSO HUMANO INDIRETO
//				mergeLista(this.calculaRecursoHumanoIndireto(orcamento), lista);
//				
//				// TAREFAS SEM RECURSOS
//				mergeLista(this.calculaTarefasSemRecursos(orcamento), lista); 
//			}
			
			// REALIZADO
			this.calculaRealizado(planejamento, lista); 
			
			Collections.sort(lista, new Comparator<AcompanhamentoFinanceiroBean>(){
				public int compare(AcompanhamentoFinanceiroBean o1, AcompanhamentoFinanceiroBean o2) {
					return o1.getContagerencial().getNome().compareTo(o2.getContagerencial().getNome());
				}
			});
			
			double total = 0d;
			for (AcompanhamentoFinanceiroBean bean : lista) {
				bean.setSaldo(bean.getOrcado() - bean.getRealizado());
				total += bean.getOrcado();
			}
			
			double percent = 0d;
			for (AcompanhamentoFinanceiroBean bean : lista) {
				percent += (bean.getOrcado() * 100.0) / total;
				bean.setAcumulado(percent);
			}
			
			report.setDataSource(lista);
		}
		
		return report;
	}
	
//	private void mergeLista(List<AcompanhamentoFinanceiroBean> listaAux, List<AcompanhamentoFinanceiroBean> lista) {
//		
//		int idx;
//		AcompanhamentoFinanceiroBean bAux;
//		for (AcompanhamentoFinanceiroBean b1 : listaAux) {
//			if(lista.contains(b1)){
//				idx = lista.indexOf(b1);
//				bAux = lista.get(idx);
//				
//				bAux.setOrcado(bAux.getOrcado() + b1.getOrcado());
//				bAux.setRealizado(bAux.getRealizado() + b1.getRealizado());
//				
//				lista.set(idx, bAux);
//			} else {
//				lista.add(b1);
//			}
//		}
//		
//	}
	
	private List<AcompanhamentoFinanceiroBean> calculaRealizado(Planejamento planejamento, List<AcompanhamentoFinanceiroBean> listaOrcado) {
		List<AcompanhamentoFinanceiroBean> lista = orcamentoDAO.findForAcompanhamentoFinanceiro(planejamento);
		
		Contagerencial contagerencial;
		for (AcompanhamentoFinanceiroBean b1 : lista) {
			contagerencial = contagerencialService.loadWithIdentificador(b1.getContagerencial());
			
			for (AcompanhamentoFinanceiroBean b2 : listaOrcado) {
				if(contagerencial.getVcontagerencial().getIdentificador().startsWith(b2.getContagerencial().getVcontagerencial().getIdentificador())){
					b2.setRealizado(b2.getRealizado() + b1.getRealizado());
				}
			}
		}
		
		return lista;
	}
	
//	private List<AcompanhamentoFinanceiroBean> calculaTarefasSemRecursos(Orcamento orcamento) {
//		
//		List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
//		AcompanhamentoFinanceiroBean bean;
//		
//		List<Tarefaorcamento> listaTarefa = tarefaorcamentoService.findByOrcamentoSemRecursos(orcamento);
//		
//		for (Tarefaorcamento tarefaorcamento : listaTarefa) {
//			if(tarefaorcamento.getCustototal() != null && tarefaorcamento.getContagerencial() != null){
//				bean = new AcompanhamentoFinanceiroBean(
//						tarefaorcamento.getContagerencial(),
//						tarefaorcamento.getCustototal(),
//						0d
//				);
//				
//				lista.add(bean);
//			}
//		}
//		
//		return lista;
//	}
//	
//	private List<AcompanhamentoFinanceiroBean> calculaRecursoHumanoIndireto(Orcamento orcamento) {
//		List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
//		AcompanhamentoFinanceiroBean bean;
//		
//		if(orcamento.getContagerencialmoi() != null){
//			bean = new AcompanhamentoFinanceiroBean(
//					orcamento.getContagerencialmoi(),
//					orcamentorecursohumanoService.calculaCustoTotalMOI(orcamento),
//					0d
//			);
//			
//			lista.add(bean);
//		}
//		
//		return lista;
//	}
//	
//	private List<AcompanhamentoFinanceiroBean> calculaRecursoHumanoDireto(Orcamento orcamento) {
//		List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
//		AcompanhamentoFinanceiroBean bean;
//		
//		if(orcamento.getContagerencialmod() != null){
//			bean = new AcompanhamentoFinanceiroBean(
//					orcamento.getContagerencialmod(),
//					orcamentorecursohumanoService.calculaCustoTotalMOD(orcamento),
//					0d
//			);
//			
//			lista.add(bean);
//		} else {
//			
//			OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
//			filtro.setOrcamento(orcamento);
//			filtro.setTipoCargo(Tipocargo.MOD);
//			
//			List<Tarefaorcamentorh> listaRH = tarefaorcamentorhService.findByOrcamento(orcamento);
//			List<Orcamentorecursohumano> listaOrcamentoRH = orcamentorecursohumanoService.findRecursoHumano(filtro);
//			
//			double valor;
//			boolean find; 
//			for (Tarefaorcamentorh trh : listaRH) {
//				if(trh.getCargo() != null){
//					valor = 0d;
//					find = false;
//					for (Orcamentorecursohumano orh : listaOrcamentoRH) {
//						if(orh.getCargo() != null && orh.getCargo().equals(trh.getCargo()) && 
//								orh.getCustohora() != null && orh.getCustohora().getValue().doubleValue() > 0){
//							if(orh.getFatormdo() != null && orh.getFatormdo().getTotal() > 0){
//								valor = orh.getCustohora().getValue().doubleValue() * orh.getFatormdo().getTotal();
//							} else {
//								valor = orh.getCustohora().getValue().doubleValue();
//							}
//							find = true;
//							break;
//						}
//					}
//					
//					if(!find && trh.getCargo().getCustohora() != null){
//						valor = trh.getCargo().getCustohora().getValue().doubleValue();
//					}
//					
//					bean = new AcompanhamentoFinanceiroBean(
//							trh.getTarefaorcamento().getContagerencial(),
//							valor,
//							0d
//					);
//					
//					lista.add(bean);
//				}
//			}
//		}
//		
//		return lista;
//	}
//	
//	private List<AcompanhamentoFinanceiroBean> calculaRecursoGeralCamposicao(Orcamento orcamento) {
//		List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
//		AcompanhamentoFinanceiroBean bean;
//		
//		List<Composicaoorcamento> listaComposicao = composicaoorcamentoService.findByOrcamento(orcamento);
//		for (Composicaoorcamento composicaoorcamento : listaComposicao) {
//			if(composicaoorcamento.getContagerencial() != null){
//				bean = new AcompanhamentoFinanceiroBean(
//						composicaoorcamento.getContagerencial(),
//						composicaoorcamentoService.calculaCustoTotalComposicao(composicaoorcamento),
//						0d
//				);
//				
//				lista.add(bean);
//			}
//		}
//		
//		return lista;
//	}
//	
//	private List<AcompanhamentoFinanceiroBean> calculaRecursoGeralTarefa(Orcamento orcamento) {
//		
//		List<Recursocomposicao> listaRC = recursocomposicaoService.findRecursoDiretoByOrcamento(orcamento);
//		List<Tarefaorcamentorg> listaRG = tarefaorcamentorgService.findByOrcamento(orcamento);
//		
//		double valor;
//		boolean find;
//		
//		List<AcompanhamentoFinanceiroBean> lista = new ArrayList<AcompanhamentoFinanceiroBean>();
//		AcompanhamentoFinanceiroBean bean;
//		
//		for (Tarefaorcamentorg trg : listaRG) {
//			valor = 0d;
//			find = false;
//			for (Recursocomposicao rc : listaRC) {
//				if(
//					((trg.getMaterial() != null && rc.getMaterial() != null && rc.getMaterial().equals(trg.getMaterial())) ||
//					(trg.getMaterial() == null && rc.getMaterial() == null && rc.getNome() != null && trg.getOutro() != null && rc.getNome().toUpperCase().equals(trg.getOutro().toUpperCase())))
//					&& rc.getCustounitario() != null
//					) {
//					find = true;
//					valor = rc.getCustounitario().getValue().doubleValue();
//					break;
//				}
//			}
//			
//			if(!find && trg.getMaterial() != null && trg.getMaterial().getValorcusto() != null){
//				valor = trg.getMaterial().getValorcusto();
//			}
//			
//			bean = new AcompanhamentoFinanceiroBean(
//					trg.getTarefaorcamento().getContagerencial(),
//					valor*trg.getQuantidade(),
//					0d
//			);
//			
//			lista.add(bean);
//		}
//		
//		return lista;
//	}
	
	public String verificaContagerencialOrcamento(Contagerencial contagerencialmod, Contagerencial contagerencialmoi, Orcamento orcamento){
		StringBuilder sb = new StringBuilder();
		
		List<Contagerencial> listaTarefa = contagerencialService.findInTarefaOrcamento(orcamento);
		List<Contagerencial> listaComposicao = contagerencialService.findInComposicao(orcamento);
		String identificadorMOD = null, identificadorMOI = null;
		
		if(contagerencialmod != null){
			contagerencialmod = contagerencialService.loadWithIdentificador(contagerencialmod);
			identificadorMOD = contagerencialmod.getVcontagerencial().getIdentificador();
			
			for (Contagerencial cgT : listaComposicao) {
				if(!identificadorMOD.equals(cgT.getVcontagerencial().getIdentificador())){
					if(identificadorMOD.startsWith(cgT.getVcontagerencial().getIdentificador())){
						sb.append("- A Conta Gerencial MOD � filha de Conta Gerencial em Composi��o.\n");
					}
					
					if(cgT.getVcontagerencial().getIdentificador().startsWith(identificadorMOD)){
						sb.append("- A Conta Gerencial MOD � pai de Conta Gerencial em Composi��o.\n");
					}
				}
			}
			
			for (Contagerencial cgT : listaTarefa) {
				if(!identificadorMOD.equals(cgT.getVcontagerencial().getIdentificador())){
					if(identificadorMOD.startsWith(cgT.getVcontagerencial().getIdentificador())){
						sb.append("- A Conta Gerencial MOD � filha de Conta Gerencial em Tarefa do Or�amento.\n");
					}
					
					if(cgT.getVcontagerencial().getIdentificador().startsWith(identificadorMOD)){
						sb.append("- A Conta Gerencial MOD � pai de Conta Gerencial em Tarefa do Or�amento.\n");
					}
				}
			}
		}
		
		if(contagerencialmoi != null){
			contagerencialmoi = contagerencialService.loadWithIdentificador(contagerencialmoi);
			identificadorMOI = contagerencialmoi.getVcontagerencial().getIdentificador();
			
			for (Contagerencial cgT : listaComposicao) {
				if(!identificadorMOI.equals(cgT.getVcontagerencial().getIdentificador())){
					if(identificadorMOI.startsWith(cgT.getVcontagerencial().getIdentificador())){
						sb.append("- A Conta Gerencial MOI � filha de Conta Gerencial em Composi��o.\n");
					}
					
					if(cgT.getVcontagerencial().getIdentificador().startsWith(identificadorMOI)){
						sb.append("- A Conta Gerencial MOI � pai de Conta Gerencial em Composi��o.\n");
					}
				}
			}
			
			for (Contagerencial cgT : listaTarefa) {
				if(!identificadorMOI.equals(cgT.getVcontagerencial().getIdentificador())){
					if(identificadorMOI.startsWith(cgT.getVcontagerencial().getIdentificador())){
						sb.append("- A Conta Gerencial MOI � filha de Conta Gerencial em Tarefa do Or�amento.\n");
					}
					
					if(cgT.getVcontagerencial().getIdentificador().startsWith(identificadorMOI)){
						sb.append("- A Conta Gerencial MOI � pai de Conta Gerencial em Tarefa do Or�amento.\n");
					}
				}
			}
		}
		
		if(identificadorMOD != null && identificadorMOI != null && !identificadorMOD.equals(identificadorMOI)){
			if(identificadorMOD.startsWith(identificadorMOI)){
				sb.append("- A Conta Gerencial MOD � filha da Conta Gerencial MOI.\n");
			}
			
			if(identificadorMOI.startsWith(identificadorMOD)){
				sb.append("- A Conta Gerencial MOI � filha da Conta Gerencial MOD.\n");
			}
		}
		
		String string = sb.toString();
		return string == null || string.equals("") ? null : string + "(Favor corrigir os cadastros)";
	}
	
	public String verificaContagerencialComposicao(Contagerencial contagerencial, Composicaoorcamento composicaoorcamento, Orcamento orcamento){
		StringBuilder sb = new StringBuilder();
		
		List<Contagerencial> listaTarefa = contagerencialService.findInTarefaOrcamento(orcamento);
		List<Contagerencial> listaComposicao = contagerencialService.findInComposicao(orcamento, composicaoorcamento);
		orcamento = this.loadWithContagerencial(orcamento);
		
		contagerencial = contagerencialService.loadWithIdentificador(contagerencial);
		String ident = contagerencial.getVcontagerencial().getIdentificador();
		
		if(orcamento.getContagerencialmod() != null){
			String identMOD = orcamento.getContagerencialmod().getVcontagerencial().getIdentificador();
			
			if(!ident.equals(identMOD)){
				if(ident.startsWith(identMOD)){
					sb.append("- A Conta Gerencial da Composi��o � filha da Conta Gerencial MOD.\n");
				}
				
				if(identMOD.startsWith(ident)){
					sb.append("- A Conta Gerencial da Composi��o � pai da Conta Gerencial MOD.\n");
				}
			}
		}
		
		if(orcamento.getContagerencialmoi() != null){
			String identMOI = orcamento.getContagerencialmoi().getVcontagerencial().getIdentificador();
			
			if(!ident.equals(identMOI)){
				if(ident.startsWith(identMOI)){
					sb.append("- A Conta Gerencial da Composi��o � filha da Conta Gerencial MOI.\n");
				}
				
				if(identMOI.startsWith(ident)){
					sb.append("- A Conta Gerencial da Composi��o � pai da Conta Gerencial MOI.\n");
				}
			}
		}
		
		for (Contagerencial cgT : listaComposicao) {
			if(!ident.equals(cgT.getVcontagerencial().getIdentificador())){
				if(ident.startsWith(cgT.getVcontagerencial().getIdentificador())){
					sb.append("- A Conta Gerencial da Composi��o � filha de Conta Gerencial em outra Composi��o.\n");
				}
				
				if(cgT.getVcontagerencial().getIdentificador().startsWith(ident)){
					sb.append("- A Conta Gerencial da Composi��o � pai de Conta Gerencial em outra Composi��o.\n");
				}
			}
		}
		
		for (Contagerencial cgT : listaTarefa) {
			if(!ident.equals(cgT.getVcontagerencial().getIdentificador())){
				if(ident.startsWith(cgT.getVcontagerencial().getIdentificador())){
					sb.append("- A Conta Gerencial da Composi��o � filha de Conta Gerencial em Tarefa do Or�amento.\n");
				}
				
				if(cgT.getVcontagerencial().getIdentificador().startsWith(ident)){
					sb.append("- A Conta Gerencial da Composi��o � pai de Conta Gerencial em Tarefa do Or�amento.\n");
				}
			}
		}
		
		String string = sb.toString();
		return string == null || string.equals("") ? null : string + "(Favor corrigir os cadastros)";
	}
	
	private Orcamento loadWithContagerencial(Orcamento orcamento) {
		return orcamentoDAO.loadWithContagerencial(orcamento);
	}
	
	public String verificaContagerencialTarefa(Contagerencial contagerencial, Tarefaorcamento tarefaorcamento, Orcamento orcamento){
		StringBuilder sb = new StringBuilder();
		
		List<Contagerencial> listaTarefa = contagerencialService.findInTarefaOrcamento(orcamento, tarefaorcamento);
		List<Contagerencial> listaComposicao = contagerencialService.findInComposicao(orcamento);
		orcamento = this.loadWithContagerencial(orcamento);
		
		contagerencial = contagerencialService.loadWithIdentificador(contagerencial);
		String ident = contagerencial.getVcontagerencial().getIdentificador();
		
		if(orcamento.getContagerencialmod() != null){
			String identMOD = orcamento.getContagerencialmod().getVcontagerencial().getIdentificador();
			
			if(!ident.equals(identMOD)){
				if(ident.startsWith(identMOD)){
					sb.append("- A Conta Gerencial da Tarefa � filha da Conta Gerencial MOD.\n");
				}
				
				if(identMOD.startsWith(ident)){
					sb.append("- A Conta Gerencial da Tarefa � pai da Conta Gerencial MOD.\n");
				}
			}
		}
		
		if(orcamento.getContagerencialmoi() != null){
			String identMOI = orcamento.getContagerencialmoi().getVcontagerencial().getIdentificador();
			
			if(!ident.equals(identMOI)){
				if(ident.startsWith(identMOI)){
					sb.append("- A Conta Gerencial da Tarefa � filha da Conta Gerencial MOI.\n");
				}
				
				if(identMOI.startsWith(ident)){
					sb.append("- A Conta Gerencial da Tarefa � pai da Conta Gerencial MOI.\n");
				}
			}
		}
		
		for (Contagerencial cgT : listaComposicao) {
			if(!ident.equals(cgT.getVcontagerencial().getIdentificador())){
				if(ident.startsWith(cgT.getVcontagerencial().getIdentificador())){
					sb.append("- A Conta Gerencial da Tarefa � filha de Conta Gerencial em Composi��o.\n");
				}
				
				if(cgT.getVcontagerencial().getIdentificador().startsWith(ident)){
					sb.append("- A Conta Gerencial da Tarefa � pai de Conta Gerencial em Composi��o.\n");
				}
			}
		}
		
		for (Contagerencial cgT : listaTarefa) {
			if(!ident.equals(cgT.getVcontagerencial().getIdentificador())){
				if(ident.startsWith(cgT.getVcontagerencial().getIdentificador())){
					sb.append("- A Conta Gerencial da Tarefa � filha de Conta Gerencial em outra Tarefa do Or�amento.\n");
				}
				
				if(cgT.getVcontagerencial().getIdentificador().startsWith(ident)){
					sb.append("- A Conta Gerencial da Tarefa � pai de Conta Gerencial em outra Tarefa do Or�amento.\n");
				}
			}
		}
		
		String string = sb.toString();
		return string == null || string.equals("") ? null : string + "(Favor corrigir os cadastros)";
	}
	
	/**
	 * Retorna ao flex se h� necessidade de recalcular os valores de m�o-de-obra.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#existeHistograma
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean necessidadeCalculo(Orcamento orcamento){
		if(orcamento.getCalcularhistograma()){
			return periodoorcamentocargoService.existeHistograma(orcamento);
		} else {
			return Boolean.TRUE;
		}
	}
	
	/**
	 * M�todo para concluir o or�amento
	 * 
	 * @see	br.com.linkcom.sined.geral.service.OrcamentoService#salvaSituacaoorcamento(Orcamento orcamento)
	 *
	 * @param orcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public String concluirOrcamento(Orcamento orcamento){
		StringBuilder msg = new StringBuilder();
		if(orcamento == null || orcamento.getCdorcamento() == null){
			msg.append("Nenhum or�amento selecionado.\n");
		}else if(orcamento.getOrcamentosituacao() != null && orcamento.getOrcamentosituacao().equals(Orcamentosituacao.CONCLUIDO)){
			msg.append("O or�amento j� est� conclu�do.");
		}else{
			try {
				salvaSituacaoorcamento(orcamento);
				msg.append("Or�amento(s) conclu�do(s) com sucesso.");
			} catch (Exception e) {
				msg.append(e.getMessage());
			}
		}
		
		return msg.toString();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#salvaSituacaoorcamento(Orcamento orcamento)
	 *
	 * @param orcamento
	 * @author Luiz Fernando
	 */
	public void salvaSituacaoorcamento(Orcamento orcamento){
		orcamentoDAO.salvaSituacaoorcamento(orcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#isCalcularhistogramaOrcamento(Orcamento orcamento)
	 *
	 * @param orcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isCalcularhistogramaOrcamento(Orcamento orcamento){
		return orcamentoDAO.isCalcularhistogramaOrcamento(orcamento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#loadWithCliente(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 05/08/2014
	* @author Luiz Fernando
	*/
	public Orcamento loadWithCliente(Orcamento orcamento) {
		return orcamentoDAO.loadWithCliente(orcamento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrcamentoDAO#existeItemCustoEComposicaoMO(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 22/06/2015
	* @author Luiz Fernando
	*/
	public boolean existeItemCustoEComposicaoMO(Orcamento orcamento) {
		return orcamentoDAO.existeItemCustoEComposicaoMO(orcamento);
	}
}
