package br.com.linkcom.sined.geral.service;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Expedicaoproducao;
import br.com.linkcom.sined.geral.bean.Expedicaoproducaohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExpedicaoproducaohistoricoService extends GenericService<Expedicaoproducaohistorico> {

	public void saveHistorico(String whereIn, String obs, Expedicaoacao expedicaoacao, HashMap<Integer, StringBuilder> mapExpedicaoMovimentacaoestoque) {
		String[] ids = whereIn.split(",");
		Expedicaoproducaohistorico expedicaoproducaohistorico;
		for (int i = 0; i < ids.length; i++) {
			Integer idExpedicaoproducao = Integer.parseInt(ids[i]);
			expedicaoproducaohistorico = new Expedicaoproducaohistorico();
			expedicaoproducaohistorico.setExpedicaoproducao(new Expedicaoproducao(idExpedicaoproducao));
			expedicaoproducaohistorico.setExpedicaoacao(expedicaoacao);
			expedicaoproducaohistorico.setObservacao(obs);
			
			if(mapExpedicaoMovimentacaoestoque != null && 
					mapExpedicaoMovimentacaoestoque.get(idExpedicaoproducao) != null && 
					!"".equals(mapExpedicaoMovimentacaoestoque.get(idExpedicaoproducao).toString())){
				String linkMovimentacaoestoque = "Entrada/Sa�da: " + mapExpedicaoMovimentacaoestoque.get(idExpedicaoproducao).toString();
				if(StringUtils.isNotEmpty(expedicaoproducaohistorico.getObservacao())){
					expedicaoproducaohistorico.setObservacao(expedicaoproducaohistorico.getObservacao() 
							+ ". " + linkMovimentacaoestoque);
				}else {
					expedicaoproducaohistorico.setObservacao(linkMovimentacaoestoque);
				}
			}
			
			this.saveOrUpdate(expedicaoproducaohistorico);
		}
	}
	
	
}
