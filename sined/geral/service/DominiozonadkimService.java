package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Dominiozonadkim;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominiozonadkimService extends GenericService<Dominiozonadkim>{

	@Override
	public void saveOrUpdate(Dominiozonadkim bean) {
		boolean novoRegistro = false;
		Dominiozonadkim dominioAnterior = null;
		if(bean.getCddominiozonadkim() == null){
			novoRegistro = true;
		}else{
			dominioAnterior = load(bean);
		}
		
		//Muda a data da vers�o e incrementa o contador.
		if(dominioAnterior != null){
			bean.setVersao(SinedUtil.mudaVersaoBriefCase(bean.getVersao()));
		}else{
			bean.setVersao(SinedUtil.criaVersaoBriefCase());
		}
		
		super.saveOrUpdate(bean);
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCddominiozonadkim());
	}
	
	@Override
	public void delete(Dominiozonadkim bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, AcaoBriefCase.REMOVER, bean.getCddominiozonadkim());
		super.delete(bean);
	}
	
}
