package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeNfe;
import br.com.linkcom.sined.geral.dao.MdfeNfeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeNfeService extends GenericService<MdfeNfe>{

	private MdfeNfeDAO mdfeNfeDAO;
	public void setMdfeNfeDAO(MdfeNfeDAO mdfeNfeDAO) {
		this.mdfeNfeDAO = mdfeNfeDAO;
	}
	
	
	public List<MdfeNfe> findByMdfe(Mdfe mdfe){
		return mdfeNfeDAO.findByMdfe(mdfe);
	}
}
