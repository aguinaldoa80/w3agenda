package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.axis.AxisFault;
import org.apache.axis.Message;
import org.apache.axis.SimpleTargetedChain;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.configuration.BasicClientConfig;
import org.apache.axis.configuration.SimpleProvider;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.RPCElement;
import org.apache.axis.transport.http.CommonsHTTPSender;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.lknfe.xml.nfe.CancNFe;
import br.com.linkcom.lknfe.xml.nfe.ConsReciNFe;
import br.com.linkcom.lknfe.xml.nfe.InfCanc;
import br.com.linkcom.lknfe.xml.nfse.abrasf.CancelarNfseEnvio;
import br.com.linkcom.lknfe.xml.nfse.abrasf.ConstrucaoCivil;
import br.com.linkcom.lknfe.xml.nfse.abrasf.ConsultarLoteRpsEnvio;
import br.com.linkcom.lknfe.xml.nfse.abrasf.ConsultarSituacaoLoteRpsEnvio;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Contato;
import br.com.linkcom.lknfe.xml.nfse.abrasf.CpfCnpj;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Endereco;
import br.com.linkcom.lknfe.xml.nfse.abrasf.EnviarLoteRpsEnvio;
import br.com.linkcom.lknfe.xml.nfse.abrasf.IdentificacaoNfse;
import br.com.linkcom.lknfe.xml.nfse.abrasf.IdentificacaoRps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.IdentificacaoTomador;
import br.com.linkcom.lknfe.xml.nfse.abrasf.InfPedidoCancelamento;
import br.com.linkcom.lknfe.xml.nfse.abrasf.InfRps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Itensservico;
import br.com.linkcom.lknfe.xml.nfse.abrasf.ListaRps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.LoteRps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Pedido;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Prestador;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Rps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Servico;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Tomador;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Valores;
import br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.InfDeclaracaoPrestacaoServico;
import br.com.linkcom.lknfe.xml.nfse.assessorpublico.Identificacao;
import br.com.linkcom.lknfe.xml.nfse.assessorpublico.NfseCancelamento;
import br.com.linkcom.lknfe.xml.nfse.assessorpublico.NfseConsulta;
import br.com.linkcom.lknfe.xml.nfse.assessorpublico.Notas;
import br.com.linkcom.lknfe.xml.nfse.datapublic.Registro5;
import br.com.linkcom.lknfe.xml.nfse.eel2.ListaServico;
import br.com.linkcom.lknfe.xml.nfse.eliss.DadosPrestador;
import br.com.linkcom.lknfe.xml.nfse.eliss.DadosTomador;
import br.com.linkcom.lknfe.xml.nfse.eliss.IdentificacaoPrestador;
import br.com.linkcom.lknfe.xml.nfse.eliss.Servicos;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Login;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Reg20;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Reg20Item;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Reg30;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Reg30Item;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Reg90;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.SDTRPS;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Sdt_processarpsin;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.Ws_nfePROCESSARPS;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.txt.LoteRPS;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.txt.Registro10;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.txt.Registro20;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.txt.Registro30;
import br.com.linkcom.lknfe.xml.nfse.etransparencia.txt.Registro90;
import br.com.linkcom.lknfe.xml.nfse.fiscalweb.Lista;
import br.com.linkcom.lknfe.xml.nfse.fiscalweb.Nf;
import br.com.linkcom.lknfe.xml.nfse.fiscalweb.Nfse;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaIndItemServico;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaIndItemServicoLista;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaIndRps;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaInfEmp;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaNfe;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaNfeLista;
import br.com.linkcom.lknfe.xml.nfse.governa.v1.GovernaTotalizacaoArquivo;
import br.com.linkcom.lknfe.xml.nfse.issmap.NfseIssMapCartaCancelamento;
import br.com.linkcom.lknfe.xml.nfse.issmap.NfseIssMapCriptografia;
import br.com.linkcom.lknfe.xml.nfse.issmap.NfseIssMapRps;
import br.com.linkcom.lknfe.xml.nfse.nfeletronica.LoteNfeletronica;
import br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro3;
import br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro4;
import br.com.linkcom.lknfe.xml.nfse.obaratec.CancelamentoNfseObaratec;
import br.com.linkcom.lknfe.xml.nfse.obaratec.CancelamentoNfseObaratecNota;
import br.com.linkcom.lknfe.xml.nfse.obaratec.NfseObaratec;
import br.com.linkcom.lknfe.xml.nfse.obaratec.NfseObaratecNota;
import br.com.linkcom.lknfe.xml.nfse.obaratec.NfseObaratecNotaTomador;
import br.com.linkcom.lknfe.xml.nfse.prefeiturabrumadinho.LoteBrumadinho;
import br.com.linkcom.lknfe.xml.nfse.prefeiturabrumadinho.Registro1;
import br.com.linkcom.lknfe.xml.nfse.prefeiturabrumadinho.Registro2;
import br.com.linkcom.lknfe.xml.nfse.prefeiturabrumadinho.Registro9;
import br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Cabecalho;
import br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.ListaItem;
import br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Lote;
import br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.ReqConsultaLoteRPS;
import br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.ReqEnvioLoteRPS;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Deducao;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Deducoes;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Emissao;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.GovDigital;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Item;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Itens;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Listadeducao;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Listaitem;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Listanfe;
import br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Nfe;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Fatura;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Nfd;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.NfdCancelamento;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Tbfatura;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Tbnfd;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Tbservico;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.CPFCNPJ;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.ChaveNFe;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.ChaveRPS;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.Detalhe;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.ListaDetalhe;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.ListaRPS;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.PedidoCancelamentoNFe;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.PedidoCancelamentoNFeCabecalho;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.PedidoEnvioLoteRPS;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.PedidoEnvioLoteRPSCabecalho;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.RPS;
import br.com.linkcom.lknfe.xml.nfse.prescon.Dadosprestador;
import br.com.linkcom.lknfe.xml.nfse.prescon.Dadosservico;
import br.com.linkcom.lknfe.xml.nfse.prescon.Dadostomador;
import br.com.linkcom.lknfe.xml.nfse.prescon.Detalheservico;
import br.com.linkcom.lknfe.xml.nfse.prescon.Notafiscal;
import br.com.linkcom.lknfe.xml.nfse.sipeg.ConsultarNfseRpsEnvio;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerro;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerrocancelamento;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEnvioNFCliente;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTributacaoIssETransparencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivonfDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.AtualizacaoLoteNfseBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailPessoaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailResultadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.CancelamentoNfseBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobrancaDocumento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.NotaFiscalServicoTemplateReport;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MarcarArquivonfBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.utils.LkNfeUtil;
import br.com.linkcom.utils.NFCeUtil;

public class ArquivonfService extends GenericService<Arquivonf> {
	
	private static final DateFormat FORMAT_DATETIME = new SimpleDateFormat("yyyy-MM-dd//HH:mm:ss");
	private static final DateFormat FORMAT_DATETIME_SMARAPD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final DateFormat FORMAT_DATETIME_PARACATU = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static final DateFormat FORMAT_DATETIME_SIGBANCOS = new SimpleDateFormat("dd/MM/yyyy");
	private static final DateFormat FORMAT_DATETIME_EEL = new SimpleDateFormat("yyyy-MM-dd");
	

	private ArquivonfDAO arquivonfDAO;
	private NotaService notaService;
	private ArquivonfnotaService arquivonfnotaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private NotaDocumentoService notaDocumentoService;
	private ArquivonfnotaerroService arquivonfnotaerroService;
	private NotaHistoricoService notaHistoricoService;
	private ArquivonfnotaerrocancelamentoService arquivonfnotaerrocancelamentoService;
	private VendaService vendaService;
	private ConfiguracaonfeService configuracaonfeService;
	private ParametrogeralService parametrogeralService;
	private DocumentoService documentoService;
	private RequisicaoService requisicaoService;
	private GrupotributacaoimpostoService grupotributacaoimpostoService;
	private FaixaimpostoService faixaimpostoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private EnvioemailService envioemailService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private UsuarioService usuarioService;
	private NotaVendaService notaVendaService;
	private VendapagamentoService vendapagamentoService;
	private ExpedicaoService expedicaoService;
	private ContareceberService contareceberService;
	private DocumentohistoricoService documentohistoricoService;
	private ReportTemplateService reporttemplateService;
	private NotaFiscalServicoTemplateReport notaFiscalServicoTemplateReport;
	private ContatoService contatoService;
	
	public void setVendapagamentoService(
			VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	public void setNotaVendaService(NotaVendaService notaVendaService) {
		this.notaVendaService = notaVendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setFaixaimpostoService(FaixaimpostoService faixaimpostoService) {
		this.faixaimpostoService = faixaimpostoService;
	}
	public void setGrupotributacaoimpostoService(
			GrupotributacaoimpostoService grupotributacaoimpostoService) {
		this.grupotributacaoimpostoService = grupotributacaoimpostoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setArquivonfnotaerrocancelamentoService(
			ArquivonfnotaerrocancelamentoService arquivonfnotaerrocancelamentoService) {
		this.arquivonfnotaerrocancelamentoService = arquivonfnotaerrocancelamentoService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setArquivonfnotaerroService(
			ArquivonfnotaerroService arquivonfnotaerroService) {
		this.arquivonfnotaerroService = arquivonfnotaerroService;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setArquivonfDAO(ArquivonfDAO arquivonfDAO) {
		this.arquivonfDAO = arquivonfDAO;
	}
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setReporttemplateService(
			ReportTemplateService reporttemplateService) {
		this.reporttemplateService = reporttemplateService;
	}
	public void setNotaFiscalServicoTemplateReport(
			NotaFiscalServicoTemplateReport notaFiscalServicoTemplateReport) {
		this.notaFiscalServicoTemplateReport = notaFiscalServicoTemplateReport;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#findArquivonfNovosForEmissao(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @since 09/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findArquivonfNovosForEmissao(Empresa empresa){
		return arquivonfDAO.findArquivonfNovosForEmissao(empresa);
	}
	
	public List<Arquivonf> findArquivonfNovosForConsulta(Empresa empresa){
		return arquivonfDAO.findArquivonfNovosForConsulta(empresa);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#findGerados()
	 * 
	 * @return
	 * @since 01/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findGerados() {
		return arquivonfDAO.findGerados();
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#findEnviados()
	 *
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findEnviados() {
		return arquivonfDAO.findEnviados();
	}

	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#marcarArquivonf(MarcarArquivonfBean bean)
	 *
	 * @param bean
	 * @return
	 * @since 06/02/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarArquivonf(Integer cdarquivonf, String flag) {
		return arquivonfDAO.marcarArquivonf(cdarquivonf.toString(), flag);
	}
	
	/**
	 * Faz referência ao DAO.
	 *
	 * @param whereIn
	 * @param flag
	 * @return
	 * @since 09/08/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarArquivonf(String whereIn, String flag){
		return arquivonfDAO.marcarArquivonf(whereIn, flag);
	}
	
	/**
	 * Faz referência ao DAO.
	 *
	 * @param whereIn
	 * @param flag
	 * @since 09/08/2012
	 * @author Rodrigo Freitas
	 */
	public void desmarcarArquivonf(String whereIn, String flag) {
		arquivonfDAO.desmarcarArquivonf(whereIn, flag);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#desmarcarArquivonf(MarcarArquivonfBean bean)
	 *
	 * @param bean
	 * @return
	 * @since 06/02/2012
	 * @author Rodrigo Freitas
	 */
	public void desmarcarArquivonf(Integer cdarquivonf, String flag) {
		arquivonfDAO.desmarcarArquivonf(cdarquivonf.toString(), flag);
	}

	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoxmlassinado(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlassinado(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoxmlassinado(arquivonf);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoretornoenvio(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoenvio(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoretornoenvio(arquivonf);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoxmlconsultasituacao(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlconsultasituacao(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoxmlconsultasituacao(arquivonf);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoxmlconsultalote(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlconsultalote(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoxmlconsultalote(arquivonf);
	}

	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#findAssinados()
	 * 
	 * @return
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findAssinados() {
		return arquivonfDAO.findAssinados();
	}
	
	/**
	 * Processa o retorno do envio do Sefaz.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateReciboDatarecebimento(Arquivonf arquivonf, String numerorecibo, Timestamp dtrecebimento)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#findByArquivonf(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @param content
	 * @param prefixowebservice
	 * @throws ParseException
	 * @throws JDOMException
	 * @throws IOException
	 * @since 21/03/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioSefaz(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws ParseException, JDOMException, IOException{
		Element retEnviNFe = SinedUtil.getRootElementXML(content);
		
		Element cStat = SinedUtil.getChildElement("cStat", retEnviNFe.getContent());
		String cStatStr = cStat != null ? cStat.getValue() : null;
		
		if(cStatStr != null && cStatStr.equals("103")){
			Element dhRecbto = SinedUtil.getChildElement("dhRecbto", retEnviNFe.getContent());
			Element infRec = SinedUtil.getChildElement("infRec", retEnviNFe.getContent());
			Element nRec = SinedUtil.getChildElement("nRec", infRec.getContent());
			
			String nRecStr = nRec.getValue();
			String dhRecbtoStr = dhRecbto.getValue();
			
			java.util.Date data = LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr);
			Timestamp dtrecebimento = new Timestamp(data.getTime());
			
			this.updateReciboDatarecebimento(arquivonf, nRecStr, dtrecebimento);
			this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_ENVIADA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NF-e enviado.", null);
			}
			
			
			
			ConsReciNFe consReciNFe = this.createXmlConsultaLoteNfe(nRecStr, configuracaonfe);
			String stringXmlConsultaLote = Util.strings.tiraAcento(consReciNFe.toString());
			Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
			
			arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
			arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
			
			this.updateArquivoxmlconsultalote(arquivonf);
			
		} else {
			
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				if(!(NotaStatus.NFE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) 
						|| NotaStatus.NFE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())
						|| NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus())
						|| NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus()))){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NF-e rejeitado.", null);
				}
			}
			
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retEnviNFe.getContent());
			String xMotivoStr = xMotivo != null ? xMotivo.getValue() : null;
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xMotivoStr);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
		}
	}

	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateReciboDatarecebimento(Arquivonf arquivonf, String numerorecibo, Timestamp dtrecebimento)
	 *
	 * @param arquivonf
	 * @param numerorecibo
	 * @param dtrecebimento
	 * @since 21/03/2012
	 * @author Rodrigo Freitas
	 */
	private void updateReciboDatarecebimento(Arquivonf arquivonf, String numerorecibo, Timestamp dtrecebimento) {
		arquivonfDAO.updateReciboDatarecebimento(arquivonf, numerorecibo, dtrecebimento);
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioMemory(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		Element statusElt = SinedUtil.getChildElement("status", rootElement.getContent());
		Element protocoloElt = SinedUtil.getChildElement("protocolo", rootElement.getContent());
		
		String protocoloStr = protocoloElt != null ? protocoloElt.getTextTrim() : null;
		String statusStr = statusElt != null ? statusElt.getTextTrim() : null;
		
		if(statusStr != null && statusStr.equalsIgnoreCase("TRUE") && protocoloStr != null){
			this.updateProtocolo(arquivonf, protocoloStr);
			this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
			}
			
			Arquivo arquivoXmlConsultaLote = new Arquivo(protocoloStr.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
			arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
			arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
			this.updateArquivoxmlconsultalote(arquivonf);	
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			Element errosElt = SinedUtil.getChildElement("erros", rootElement.getContent());
			if(errosElt != null){
				List<Element> listaErrosElt = SinedUtil.getListChildElement("erro", errosElt.getContent());
				for (Element erroElt : listaErrosElt) {
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem(erroElt.getTextTrim());
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
				}
			}
			
			Element avisosElt = SinedUtil.getChildElement("avisos", rootElement.getContent());
			if(avisosElt != null){
				List<Element> listaAvisosElt = SinedUtil.getListChildElement("aviso", avisosElt.getContent());
				for (Element avisoElt : listaAvisosElt) {
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem(avisoElt.getTextTrim());
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
				}
			}
		}
	}
	
	/**
	 * Processa o retorno do envio da Prefeitura de Belo Horizonte.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateProtocolo(Arquivonf arquivonf, String protocoloStr)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#findByArquivonf(Arquivonf arquivonf)
	 * @see br.com.linkcom.sined.geral.service.NotaService#alterarStatusAcao(Nota nota, String observacao, boolean cancelarDocumentos)
	 *
	 * @param arquivonf
	 * @param content
	 * @throws JDOMException
	 * @throws IOException
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 * @param configuracaonfe 
	 */
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioBhiss(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws JDOMException, IOException {
		System.out.println(new String(content));
		Element rootElement = SinedUtil.getRootElementXML(content);
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		Element protocolo = SinedUtil.getChildElement("Protocolo", rootElement.getContent());
		if(protocolo == null){
			protocolo = SinedUtil.getChildElement("numeroProtocolo", rootElement.getContent());
		}
		
		String protocoloStr = null;
		if(protocolo != null){
			protocoloStr = protocolo.getValue();
		}
		
		if (org.apache.commons.lang.StringUtils.isEmpty(protocoloStr) && 
				(Prefixowebservice.ARACAJUISS_HOM.equals(prefixowebservice) || Prefixowebservice.ARACAJUISS_PROD.equals(prefixowebservice) ||
						Prefixowebservice.SIMOESFILHOISS_NOVO_HOM.equals(prefixowebservice) || Prefixowebservice.SIMOESFILHOISS_NOVO_PROD.equals(prefixowebservice) ||
						Prefixowebservice.CHAPECO_HOM.equals(prefixowebservice) || Prefixowebservice.CHAPECO_PROD.equals(prefixowebservice) ||
						Prefixowebservice.GOIANIA_HOM.equals(prefixowebservice) || Prefixowebservice.GOIANIA_PROD.equals(prefixowebservice))) {
			Element rootElementSucesso = SinedUtil.getChildElement("ListaNfse", rootElement.getContent());
			
			if (rootElementSucesso != null) {
				protocoloStr = "0";
				rootElement = rootElementSucesso;
			}
		}
		
		if (protocoloStr != null && !protocoloStr.equals("")) {
			this.updateProtocolo(arquivonf, protocoloStr);
			
			if(prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_HOM) || 
					prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.OUROPRETO_HOM) || 
					prefixowebservice.equals(Prefixowebservice.OUROPRETO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) || 
					prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARACAJUISS_HOM) || 
					prefixowebservice.equals(Prefixowebservice.ARACAJUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_HOM) || 
					prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ERECHIM_HOM) || 
					prefixowebservice.equals(Prefixowebservice.ERECHIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) ||
                    prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.AVARE_HOM) || 
					prefixowebservice.equals(Prefixowebservice.AVARE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BOA_VISTA_HOM) || 
                    prefixowebservice.equals(Prefixowebservice.BOA_VISTA_PROD) ||
//                    prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) || 
//                    prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD) ||
                    prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_HOM) || 
                    prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_PROD) ||
                    prefixowebservice.equals(Prefixowebservice.CHAPECO_HOM) || 
                    prefixowebservice.equals(Prefixowebservice.CHAPECO_PROD) ||
                    prefixowebservice.equals(Prefixowebservice.GOIANIA_HOM) || 
                    prefixowebservice.equals(Prefixowebservice.GOIANIA_PROD)) {
				processaRetornoConsultaBhiss(arquivonf, new String(content), prefixowebservice);
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
				}
				
				Empresa empresa = empresaService.loadByArquivonf(arquivonf);
				
				if(!prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.CURVELO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.CURVELO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) &&
                        !prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) &&
                        !prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.BOA_VISTA_HOM) &&
                        !prefixowebservice.equals(Prefixowebservice.BOA_VISTA_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.IVOTI_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.IVOTI_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.JUAZEIRO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.JUAZEIRO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.CASTELO_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.CASTELO_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.PETROLINA_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.PETROLINA_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.PETROLINA_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.PETROLINA_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD)) {
					
					ConsultarSituacaoLoteRpsEnvio consultarSituacaoLoteRpsEnvio = this.createXmlConsultaSituacaoLoteNfse(empresa, protocoloStr, configuracaonfe);
					String stringXmlConsultaSituacao = Util.strings.tiraAcento(consultarSituacaoLoteRpsEnvio.toString());
					if(prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) ||
							prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD)){
						stringXmlConsultaSituacao = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + stringXmlConsultaSituacao;
					}
					Arquivo arquivoXmlConsultaSituacao = new Arquivo(stringXmlConsultaSituacao.getBytes(), "nfconsultasituacao_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
					arquivonf.setArquivoxmlconsultasituacao(arquivoXmlConsultaSituacao);
					arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultasituacao");
					arquivoService.saveOrUpdate(arquivoXmlConsultaSituacao);
					this.updateArquivoxmlconsultasituacao(arquivonf);	
				}
				
				String stringXmlConsultaLote = null;
				
				if(prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD)){
					ConsultarNfseRpsEnvio consultarNfseRpsEnvio = this.createXmlConsultaNfse(listan, empresa, protocoloStr, configuracaonfe);
					stringXmlConsultaLote = Util.strings.tiraAcento(consultarNfseRpsEnvio.toString());
				}else {
					ConsultarLoteRpsEnvio consultarLoteRpsEnvio = this.createXmlConsultaLoteNfse(empresa, protocoloStr, configuracaonfe);
					stringXmlConsultaLote = Util.strings.tiraAcento(consultarLoteRpsEnvio.toString());
					if(prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) ||
							prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD)){
						stringXmlConsultaLote = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + stringXmlConsultaLote;
					}
				}
				
				Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
				arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
				arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
				this.updateArquivoxmlconsultalote(arquivonf);	
			}
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
			if (listaMensagemRetorno != null && listaMensagemRetorno.getContent() != null && listaMensagemRetorno.getContent().size() <= 0) {
				listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetornoLote", rootElement.getContent());
			}
			
			if (listaMensagemRetorno == null) {
				listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetornoLote", rootElement.getContent());
            }
			
			if(listaMensagemRetorno != null){
				List<Element> lista = listaMensagemRetorno.getContent();
				Element msg, correcao, codigo, mensagemRetorno;
				Object object;
				Arquivonfnotaerro arquivonfnotaerro;
				for (int i = 0; i < lista.size(); i++) {
					object = lista.get(i);
					if(object instanceof Element){
						mensagemRetorno = (Element)object;
						
						msg = null;
						correcao = null;
						codigo = null;
						
						msg = SinedUtil.getChildElement("Mensagem", mensagemRetorno.getContent());
						correcao = SinedUtil.getChildElement("Correcao", mensagemRetorno.getContent());
						codigo = SinedUtil.getChildElement("Codigo", mensagemRetorno.getContent());
						
						if(msg == null) msg = mensagemRetorno;
						
						if(msg != null){
							arquivonfnotaerro = new Arquivonfnotaerro();
							
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setCodigo(codigo != null && codigo.getValue() != null ? codigo.getValue() : null);
							arquivonfnotaerro.setMensagem(msg.getValue());
							arquivonfnotaerro.setCorrecao(correcao != null && correcao.getValue() != null ? correcao.getValue() : null);
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
						}
					}
				}
			} else {
				List<Element> lista = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
				if(lista != null && lista.size() > 0){
					for (int i = 0; i < lista.size(); i++) {
						Object object = lista.get(i);
						if(object instanceof Element){
							Element msg = (Element)object;
							if(msg.getName().equals("mensagens")){
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(msg.getValue());
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioIiBrasil(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws UnsupportedEncodingException, JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		if (rootElement != null) {
			Element informacoesNfse = SinedUtil.getChildElement("InformacoesNfse", rootElement.getContent());
			
			if (informacoesNfse != null) {
				Element numeroRpsElement, codigoVerificacaoElement;
				String numeroRps = "", codigoVerificacao = "";
				
				numeroRpsElement = SinedUtil.getChildElement("NumeroRps", informacoesNfse.getContent());
				
				if (numeroRpsElement != null) {
					numeroRps = numeroRpsElement.getValue();
				}
				
				codigoVerificacaoElement = SinedUtil.getChildElement("CodigoVerificacao", informacoesNfse.getContent());
				
				if (codigoVerificacaoElement != null) {
					codigoVerificacao = codigoVerificacaoElement.getValue();
				}
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				
				if (listan != null && listan.size() > 0) {
					Arquivonfnota arquivonfnota = listan.get(0);
					
					arquivonfnotaService.updateNumeroCodigo(arquivonfnota, StringUtils.soNumero(numeroRps), codigoVerificacao, null, null);
					
					if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || 
							notaService.isNotaPossuiReceita(arquivonfnota.getNota(), true)){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
					} else {
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
					}
					
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
				}
			
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				
				for (Arquivonfnota arquivonfnota : listan) {
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
				}
				
				Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
				
				if (listaMensagemRetorno != null) {
					List<Element> lista = SinedUtil.getListChildElement("MensagemRetorno", listaMensagemRetorno.getContent());
					
					Element msg, correcao, codigo;
					
					if (lista != null && lista.size() > 0) {
						Arquivonfnotaerro arquivonfnotaerro;
						
						for (Element e : lista) {
							msg = SinedUtil.getChildElement("Mensagem", e.getContent());
							correcao = SinedUtil.getChildElement("Correcao", e.getContent());
							codigo = SinedUtil.getChildElement("Codigo", e.getContent());
							
							arquivonfnotaerro = new Arquivonfnotaerro();
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							if (msg != null) {
								arquivonfnotaerro.setCodigo(codigo != null && codigo.getValue() != null ? codigo.getValue() : null);
								arquivonfnotaerro.setMensagem(msg.getValue());
								arquivonfnotaerro.setCorrecao(correcao != null && correcao.getValue() != null ? correcao.getValue() : null);
							} else {
								arquivonfnotaerro.setMensagem("Erro ao enviar NFS-e.");
							}
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
						}
					} else {
						Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
						
						arquivonfnotaerro.setArquivonf(arquivonf);
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						arquivonfnotaerro.setMensagem("Erro ao enviar NFS-e.");
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
				} else {
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					arquivonfnotaerro.setMensagem("Erro ao enviar NFS-e.");
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void atualizaLoteMi6(WebRequestContext request, Arquivonf arquivonf) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		List<Arquivonfnota> listaArquivonfnota = arquivonf.getListaArquivonfnota();
		
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			if(arquivonfnota.getArquivoxml() != null){
				Element rootElement = SinedUtil.getRootElementXML(arquivonfnota.getArquivoxml().getContent());
				
				Element nfse = SinedUtil.getChildElement("Nfse", rootElement.getContent());
				Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
				
				Element numeroElement = SinedUtil.getChildElement("Numero", infNfse.getContent());
				Element codigoVerificacaoElement = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent()); 
				Element dataEmissaoElement = SinedUtil.getChildElement("DataEmissao", infNfse.getContent()); 
				
				String numeroNfse = numeroElement.getText();
				String dataEmissaoStr = dataEmissaoElement.getText();
				String codigoVerificacao = codigoVerificacaoElement.getText();
				
				dataEmissaoStr = dataEmissaoStr.replaceAll("T", "//");
				Date dataEmissao = FORMAT_DATETIME.parse(dataEmissaoStr);
				
				arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao, new Timestamp(dataEmissao.getTime()), dataEmissao);
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
				}
				
				if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(arquivonfnota.getNota())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
				} else {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
				}
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
				
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
				
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				
				request.addMessage("Nota atualizada com sucesso.");
			}
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void atualizaLoteProcessadoErroProduto(WebRequestContext request, Arquivonf arquivonf) throws JDOMException, IOException, ParseException {
		List<Arquivonfnota> listaArquivonfnota = arquivonf.getListaArquivonfnota();
		
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			if(arquivonfnota.getArquivoxml() != null){
				Element rootElement = SinedUtil.getRootElementXML(arquivonfnota.getArquivoxml().getContent());
				
				Element protNFe = SinedUtil.getChildElement("protNFe", rootElement.getContent());
				Element infProt = SinedUtil.getChildElement("infProt", protNFe.getContent());
				
				Element xMotivo = SinedUtil.getChildElement("xMotivo", infProt.getContent());
				
				Element dhRecbto = SinedUtil.getChildElement("dhRecbto", infProt.getContent());
				String dhRecbtoStr = dhRecbto.getValue();
				Timestamp dataRec = new Timestamp(LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr).getTime());
				
				Element nProt = SinedUtil.getChildElement("nProt", infProt.getContent());
				String nProtStr = nProt.getValue();
				
				arquivonfnotaService.updateArquivonfnota(arquivonfnota, dataRec, nProtStr);
				
				if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(arquivonfnota.getNota())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_LIQUIDADA);
				}else {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_EMITIDA);
				}
				notaService.alterarStatusAcao(arquivonfnota.getNota(), xMotivo.getText() + " (Atualizado manualmente)", null);
				
				Arquivo arquivoxml = new Arquivo(arquivonfnota.getArquivoxml().getContent(), nProtStr + "-procNFe.xml", "text/xml");
				arquivonfnota.setArquivoxml(arquivoxml);
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
				
				request.addMessage("Nota atualizada com sucesso.");
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void atualizaLoteProcessadoErro(WebRequestContext request, Arquivonf arquivonf, byte[] content) throws JDOMException, IOException, ParseException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		List<AtualizacaoLoteNfseBean> listaAtualizacaoLoteNfseBean = new ArrayList<AtualizacaoLoteNfseBean>();
		AtualizacaoLoteNfseBean atualizacaoLoteNfseBean;
		
		arquivonf = this.loadForEntrada(arquivonf);
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		if(configuracaonfe != null && configuracaonfe.getPrefixowebservice() != null && configuracaonfe.getPrefixowebservice().getPrefixo().equals("smarapd")){
			List<Element> listaNfdok = SinedUtil.getListChildElement("nfdok", rootElement.getContent());
			if(listaNfdok != null && listaNfdok.size() > 0){
				for (Element nfdok : listaNfdok) {
					Element newDataSet = SinedUtil.getChildElement("NewDataSet", nfdok.getContent());
					Element nota_fiscal = SinedUtil.getChildElement("NOTA_FISCAL", newDataSet.getContent());
					
					
					Element numeroNota = SinedUtil.getChildElement("NumeroNota", nota_fiscal.getContent());
					Element numeroRps = SinedUtil.getChildElement("NumeroRps", nota_fiscal.getContent());
					Element dataEmissao = SinedUtil.getChildElement("DataEmissao", nota_fiscal.getContent());
					Element chaveValidacao = SinedUtil.getChildElement("ChaveValidacao", nota_fiscal.getContent());
					
					atualizacaoLoteNfseBean = new AtualizacaoLoteNfseBean();
					atualizacaoLoteNfseBean.setNumero_rps(numeroRps.getText());
					atualizacaoLoteNfseBean.setNumero_nfse(numeroNota.getText());
					atualizacaoLoteNfseBean.setCodigoverificacao(chaveValidacao.getText());
					atualizacaoLoteNfseBean.setDataemissao(new Timestamp(FORMAT_DATETIME_SMARAPD.parse(dataEmissao.getText()).getTime()));
					atualizacaoLoteNfseBean.setCompetencia(atualizacaoLoteNfseBean.getDataemissao());
					
					listaAtualizacaoLoteNfseBean.add(atualizacaoLoteNfseBean);
				}
			}
		} else if(configuracaonfe != null && configuracaonfe.getPrefixowebservice() != null && configuracaonfe.getPrefixowebservice().getPrefixo().equals("dsfnetiss")){
			List<Element> listaNotaFiscal = SinedUtil.getListChildElement("NOTA_FISCAL", rootElement.getContent());
			for (Element nota_fiscal : listaNotaFiscal) {
				Element numeroNota = SinedUtil.getChildElement("NUM_NOTA", nota_fiscal.getContent());
				Element numeroRps = SinedUtil.getChildElement("RPS_NUM", nota_fiscal.getContent());
				Element dataEmissao = SinedUtil.getChildElement("DATA_HORA_EMISSAO", nota_fiscal.getContent());
				Element chaveValidacao = SinedUtil.getChildElement("CODIGO_VERIFICACAO", nota_fiscal.getContent());
				
				atualizacaoLoteNfseBean = new AtualizacaoLoteNfseBean();
				atualizacaoLoteNfseBean.setNumero_rps(numeroRps.getText());
				atualizacaoLoteNfseBean.setNumero_nfse(numeroNota.getText());
				atualizacaoLoteNfseBean.setCodigoverificacao(chaveValidacao.getText());
				atualizacaoLoteNfseBean.setDataemissao(new Timestamp(FORMAT_DATETIME_PARACATU.parse(dataEmissao.getText()).getTime()));
				atualizacaoLoteNfseBean.setCompetencia(atualizacaoLoteNfseBean.getDataemissao());
				
				listaAtualizacaoLoteNfseBean.add(atualizacaoLoteNfseBean);
			}
			
		} else {
			Element listaNfse = SinedUtil.getChildElement("ListaNfse", rootElement.getContent());
			List<Element> listaNfseGinfes = SinedUtil.getListChildElement("Nfse", rootElement.getContent());
			if(listaNfse == null && (listaNfseGinfes == null || listaNfseGinfes.size() == 0)){
				if(rootElement.getName().equals("ConsultarLoteRpsResponse")){
					Element consultarLoteRpsResultElt = SinedUtil.getChildElement("ConsultarLoteRpsResult", rootElement.getContent());
					if(consultarLoteRpsResultElt != null){
						listaNfse = SinedUtil.getChildElement("ListaNfse", consultarLoteRpsResultElt.getContent());
					}
				}
				if(listaNfse == null) listaNfse = rootElement;
			}
			
			if(rootElement.getName().equals("CompNfse")){
				getDadosNotaInCompNfse(listaAtualizacaoLoteNfseBean, rootElement);
			} else if(listaNfse != null || configuracaonfe.getPrefixowebservice().getPrefixo().equals("eel2")){
				List<Element> listaCompNfse = null;
				if(listaNfse != null){
					listaCompNfse = SinedUtil.getListChildElement("CompNfse", listaNfse.getContent());
				}
				if(listaCompNfse != null && listaCompNfse.size() > 0){
					for (Element compNfse : listaCompNfse) {
						getDadosNotaInCompNfse(listaAtualizacaoLoteNfseBean, compNfse);
					}
				} else {
					List<Element> listaNfseElt = null;
					if(listaNfse == null && configuracaonfe.getPrefixowebservice().getPrefixo().equals("eel2")){
						listaNfseElt = SinedUtil.getListChildElement("Nfse", rootElement.getContent());
					}else {
						SinedUtil.getListChildElement("Nfse", listaNfse.getContent());
					}
					if(listaNfseElt != null && listaNfseElt.size() > 0){
						
						for (Element compNfse : listaNfseElt) {
							Element nfse = null;
							if(!configuracaonfe.getPrefixowebservice().getPrefixo().equals("eel2")){
								SinedUtil.getChildElement("Nfse", compNfse.getContent());
							}else {
								nfse = compNfse;
							}
							if(nfse == null) throw new SinedException("Não foi encontrada a tag 'Nfse' no XML");
							
							Element identificacaoNfse = SinedUtil.getChildElement("IdentificacaoNfse", nfse.getContent());
							if(identificacaoNfse == null) throw new SinedException("Não foi encontrada a tag 'IdentificacaoNfse' no XML");
							
							Element numero_Nfse = SinedUtil.getChildElement("Numero", identificacaoNfse.getContent());
							if(numero_Nfse == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
							
							Element codigoVerificacao = null;
							Element competencia = null;
							Element identificacaoRps = null;
							Element numero_Rps = null;
							
							if(!configuracaonfe.getPrefixowebservice().getPrefixo().equals("eel2")){
								codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", identificacaoNfse.getContent());
								if(codigoVerificacao == null) throw new SinedException("Não foi encontrada a tag 'CodigoVerificacao' no XML");
							
								competencia = SinedUtil.getChildElement("Competencia", nfse.getContent());
								if(competencia == null) throw new SinedException("Não foi encontrada a tag 'Competencia' no XML");
								
								identificacaoRps = SinedUtil.getChildElement("IdentificacaoRps", nfse.getContent());
								if(identificacaoRps == null) throw new SinedException("Não foi encontrada a tag 'IdentificacaoRps' no XML");
								
								numero_Rps = SinedUtil.getChildElement("Numero", identificacaoRps.getContent());
								if(numero_Rps == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
							}else {
								numero_Rps = SinedUtil.getChildElement("NumeroRps", identificacaoNfse.getContent());
								if(numero_Rps == null) throw new SinedException("Não foi encontrada a tag 'NumeroRps' no XML");
							}
							
							Element dataEmissao = SinedUtil.getChildElement("DataEmissao", nfse.getContent());
							if(dataEmissao == null) throw new SinedException("Não foi encontrada a tag 'DataEmissao' no XML");
							
							
							atualizacaoLoteNfseBean = new AtualizacaoLoteNfseBean();
							atualizacaoLoteNfseBean.setNumero_rps(numero_Rps.getText());
							atualizacaoLoteNfseBean.setNumero_nfse(numero_Nfse.getText());
							
							if(codigoVerificacao != null){
								atualizacaoLoteNfseBean.setCodigoverificacao(codigoVerificacao.getText());
							}
							
							String dataEmissaoStr = dataEmissao.getText().replaceAll("T", "//");
							atualizacaoLoteNfseBean.setDataemissao(new Timestamp(FORMAT_DATETIME.parse(dataEmissaoStr).getTime()));
							
							if(competencia != null){
								String competenciaStr = competencia.getText();
								if(competenciaStr.length() > 10){
									competenciaStr = competenciaStr.substring(0, 10);
									atualizacaoLoteNfseBean.setCompetencia(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(competenciaStr).getTime()));
								} else {
									atualizacaoLoteNfseBean.setCompetencia(atualizacaoLoteNfseBean.getDataemissao());
								}
							}
							
							listaAtualizacaoLoteNfseBean.add(atualizacaoLoteNfseBean);
						}
						
					} else throw new SinedException("Não foi encontrada a tag 'CompNfse' nem 'Nfse' no XML");
				} 
			} else if(listaNfseGinfes != null && listaNfseGinfes.size() > 0){
				
				for (Element nfse : listaNfseGinfes) {
					Element identificacaoRps = SinedUtil.getChildElement("IdentificacaoRps", nfse.getContent());
					if(identificacaoRps == null) continue;
					
					Element identificacaoNfse = SinedUtil.getChildElement("IdentificacaoNfse", nfse.getContent());
					if(identificacaoNfse == null) throw new SinedException("Não foi encontrada a tag 'IdentificacaoNfse' no XML");
					
					Element numeroRps = SinedUtil.getChildElement("Numero", identificacaoRps.getContent());
					if(numeroRps == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
					
					Element numeroNfse = SinedUtil.getChildElement("Numero", identificacaoNfse.getContent());
					if(numeroNfse == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
					
					Element codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", identificacaoNfse.getContent());
					if(codigoVerificacao == null) throw new SinedException("Não foi encontrada a tag 'CodigoVerificacao' no XML");
					
					Element dataEmissao = SinedUtil.getChildElement("DataEmissao", nfse.getContent());
					if(dataEmissao == null) throw new SinedException("Não foi encontrada a tag 'DataEmissao' no XML");
					
					Element competencia = SinedUtil.getChildElement("Competencia", nfse.getContent());
					if(competencia == null) throw new SinedException("Não foi encontrada a tag 'Competencia' no XML");
					
					atualizacaoLoteNfseBean = new AtualizacaoLoteNfseBean();
					atualizacaoLoteNfseBean.setNumero_rps(numeroRps.getText());
					atualizacaoLoteNfseBean.setNumero_nfse(numeroNfse.getText());
					atualizacaoLoteNfseBean.setCodigoverificacao(codigoVerificacao.getText());
					
					String dataEmissaoStr = dataEmissao.getText().replaceAll("T", "//");
					atualizacaoLoteNfseBean.setDataemissao(new Timestamp(FORMAT_DATETIME.parse(dataEmissaoStr).getTime()));
					
					String competenciaStr = competencia.getText();
					if(competenciaStr.length() > 10){
						competenciaStr = competenciaStr.substring(0, 10);
						atualizacaoLoteNfseBean.setCompetencia(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(competenciaStr).getTime()));
					} else {
						atualizacaoLoteNfseBean.setCompetencia(atualizacaoLoteNfseBean.getDataemissao());
					}
					
					listaAtualizacaoLoteNfseBean.add(atualizacaoLoteNfseBean);
				}
				
				
			} else throw new SinedException("Não foi encontrada a tag 'ListaNfse' nem 'Nfse' no XML");
		}
		
		
		if(listaAtualizacaoLoteNfseBean != null && listaAtualizacaoLoteNfseBean.size() > 0){
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByArquivonf(arquivonf); 
			
			boolean atualizadanota = false;
			
			for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
				if(arquivonfnota != null && arquivonfnota.getNota() != null && arquivonfnota.getNota().getNumero() != null){
					String numeroNota = arquivonfnota.getNota().getNumero();
					
					for (AtualizacaoLoteNfseBean bean : listaAtualizacaoLoteNfseBean) {
						if(bean.getNumero_rps().equals(numeroNota)){
							arquivonfnotaService.updateNumeroCodigo(
												arquivonfnota, 
												bean.getNumero_nfse(), 
												bean.getCodigoverificacao(), 
												bean.getDataemissao(), 
												bean.getCompetencia());
							
							if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(arquivonfnota.getNota())){
								arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
							} else {
								arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
							}
							notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso. (Atualizado manualmente)", null);
							
							request.addMessage("Nota " + numeroNota + " atualizada com sucesso.");
							atualizadanota = true;
						}
					}
				}
				
			}
			
			if(atualizadanota){
				// ATUALIZA O ARQUIVO XML
				Arquivo arquivoretornoconsulta = new Arquivo(content, "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivonf.setArquivoretornoconsulta(arquivoretornoconsulta);
				arquivoDAO.saveFile(arquivonf, "arquivoretornoconsulta");
				arquivoService.saveOrUpdate(arquivoretornoconsulta);
				this.updateArquivoretornoconsulta(arquivonf);
				
				request.addMessage("Arquivo de retorno de consulta do lote salvo com sucesso.");
				
				// ATUALIZA A SITUAÇÃO DO LOTE
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				
				request.addMessage("Situação do lote atualizada com sucesso.");
				
				// ATUALIZA PROTOCOLO
				if(arquivonf.getProtocoloatualizacao() != null && !arquivonf.getProtocoloatualizacao().equals("")){
					this.updateProtocolo(arquivonf, arquivonf.getProtocoloatualizacao());
					
					request.addMessage("Protocolo do lote atualizado com sucesso.");
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void getDadosNotaInCompNfse(List<AtualizacaoLoteNfseBean> listaAtualizacaoLoteNfseBean, Element compNfse) throws ParseException {
		AtualizacaoLoteNfseBean atualizacaoLoteNfseBean;
		Element nfse = SinedUtil.getChildElement("Nfse", compNfse.getContent());
		if(nfse == null) {
			Element tcCompNfse = SinedUtil.getChildElement("tcCompNfse", compNfse.getContent());
			if(tcCompNfse != null){
				nfse = SinedUtil.getChildElement("Nfse", tcCompNfse.getContent());
			}
			
			if(nfse == null) throw new SinedException("Não foi encontrada a tag 'Nfse' no XML");
		}
		
		Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
		if(infNfse == null) throw new SinedException("Não foi encontrada a tag 'InfNfse' no XML");
		
		Element declaracaoPrestacaoServico = SinedUtil.getChildElement("DeclaracaoPrestacaoServico", infNfse.getContent());
		
		Element numero_Nfse = SinedUtil.getChildElement("Numero", infNfse.getContent());
		if(numero_Nfse == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
		
		Element codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent());
		if(codigoVerificacao == null){
			codigoVerificacao = SinedUtil.getChildElement("CodigoControle", infNfse.getContent());
		}
		if(codigoVerificacao == null) throw new SinedException("Não foi encontrada a tag 'CodigoVerificacao' no XML");
		
		Element dataEmissao = SinedUtil.getChildElement("DataEmissao", infNfse.getContent());
		if(dataEmissao == null) throw new SinedException("Não foi encontrada a tag 'DataEmissao' no XML");
		
		Element infDeclaracaoPrestacaoServico = null;
		if (declaracaoPrestacaoServico!=null){
			infDeclaracaoPrestacaoServico = SinedUtil.getChildElement("InfDeclaracaoPrestacaoServico", declaracaoPrestacaoServico.getContent());
		}
		
		Element competencia = SinedUtil.getChildElement("Competencia", infNfse.getContent());
		if(competencia == null){
			if(declaracaoPrestacaoServico != null){
				 competencia = SinedUtil.getChildElement("Competencia", declaracaoPrestacaoServico.getContent());
				 if(competencia == null && infDeclaracaoPrestacaoServico!=null){
					 competencia = SinedUtil.getChildElement("Competencia", infDeclaracaoPrestacaoServico.getContent());
				 }
			}
			if(competencia == null)	throw new SinedException("Não foi encontrada a tag 'Competencia' no XML");
		}
		
		Element identificacaoRps = SinedUtil.getChildElement("IdentificacaoRps", infNfse.getContent());
		if(identificacaoRps == null) {
			if(declaracaoPrestacaoServico != null){
				Element rps = SinedUtil.getChildElement("Rps", declaracaoPrestacaoServico.getContent());
				if (rps==null && infDeclaracaoPrestacaoServico!=null){
					rps = SinedUtil.getChildElement("Rps", infDeclaracaoPrestacaoServico.getContent());
				}
				if(rps != null){
					identificacaoRps = SinedUtil.getChildElement("IdentificacaoRps", rps.getContent());
				}
			}
			if(identificacaoRps == null) throw new SinedException("Não foi encontrada a tag 'IdentificacaoRps' no XML");
		}
		
		Element numero_Rps = SinedUtil.getChildElement("Numero", identificacaoRps.getContent());
		if(numero_Rps == null) throw new SinedException("Não foi encontrada a tag 'Numero' no XML");
		
		atualizacaoLoteNfseBean = new AtualizacaoLoteNfseBean();
		atualizacaoLoteNfseBean.setNumero_rps(numero_Rps.getText());
		atualizacaoLoteNfseBean.setNumero_nfse(numero_Nfse.getText());
		atualizacaoLoteNfseBean.setCodigoverificacao(codigoVerificacao.getText());
		
		String dataEmissaoStr = dataEmissao.getText().replaceAll("T", "//");
		atualizacaoLoteNfseBean.setDataemissao(new Timestamp(FORMAT_DATETIME.parse(dataEmissaoStr).getTime()));
		
		String competenciaStr = competencia.getText();
		if(competenciaStr.length() > 10){
			competenciaStr = competenciaStr.substring(0, 10);
			atualizacaoLoteNfseBean.setCompetencia(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(competenciaStr).getTime()));
		} else {
			atualizacaoLoteNfseBean.setCompetencia(atualizacaoLoteNfseBean.getDataemissao());
		}
		
		listaAtualizacaoLoteNfseBean.add(atualizacaoLoteNfseBean);
	}
	
	public void processaRetornoEnvioETransparencia(Arquivonf arquivonf, byte[] content) throws UnsupportedEncodingException, JDOMException, IOException, ParseException{
		String stringArquivo = new String(content);
		String[] linhas = stringArquivo.split("\\r?\\n");
		
		Map<String, String> mapaNumNfse = new HashMap<String, String>();
		Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
		Map<String, String> mapaDtemissao = new HashMap<String, String>();
		Map<String, String> mapaDtcompetencia = new HashMap<String, String>();
		
		for (String linha : linhas) {
			String[] campo = linha.split("\\|");
			String tiporegistro = campo[0];
			if(tiporegistro.equals("20")){
				String numNf = SinedUtil.retiraZeroEsquerda(campo[2]);
				String dtEmiNf = campo[4];
				String dtHrGerNf = campo[5];
				String codVerNf = campo[6];
				String numRps = SinedUtil.retiraZeroEsquerda(campo[7]);
				
				mapaNumNfse.put(numRps, numNf);
				mapaCodVerificacao.put(numRps, codVerNf);
				mapaDtemissao.put(numRps, dtHrGerNf);
				mapaDtcompetencia.put(numRps, dtEmiNf);
			}
		}
		
		List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
		for (Arquivonfnota arquivonfnota : listan) {
			String numeroNfse = mapaNumNfse.get(arquivonfnota.getNota().getNumero());
			String codigoVerificacao = mapaCodVerificacao.get(arquivonfnota.getNota().getNumero());
			String dtEmissaoStr = mapaDtemissao.get(arquivonfnota.getNota().getNumero());
			String dtCompetenciaStr = mapaDtcompetencia.get(arquivonfnota.getNota().getNumero());
			
			Date dataEmissao = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dtEmissaoStr);
			Date dataCompetencia = new SimpleDateFormat("dd/MM/yyyy").parse(dtCompetenciaStr);
			
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
				documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
				try {
					vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			arquivonfnotaService.updateNumeroCodigo(arquivonfnota, StringUtils.soNumero(numeroNfse), codigoVerificacao, new Timestamp(dataEmissao.getTime()), dataCompetencia);
			
			if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || 
					notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
			} else {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
			}
			notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
		}
		
		this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		
		Arquivo arquivoXmlRetornoEnvio = new Arquivo(content, "LOTE-RETORNO_" + SinedUtil.datePatternForReport() + ".TXT", "text/txt");
		
		arquivonf.setArquivoretornoenvio(arquivoXmlRetornoEnvio);
		arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
		
		arquivoService.saveOrUpdate(arquivoXmlRetornoEnvio);
		
		this.updateArquivoretornoenvio(arquivonf);	
		
		String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
		try {
			if(paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")){
				this.enviarNFCliente(CollectionsUtil.listAndConcatenate(listan, "nota.cdNota", ","), TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioLavrasiss(Arquivonf arquivonf, byte[] content) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		Element emissao = SinedUtil.getChildElement("emissao", rootElement.getContent());
		if(emissao != null){
			List<Element> listaNfe = SinedUtil.getListChildElement("nf-e", emissao.getContent());
			if(listaNfe != null && listaNfe.size() > 0){
				Map<String, List<String>> mapErros = new HashMap<String, List<String>>();
				Map<String, String> mapaNumNfse = new HashMap<String, String>();
				Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
				
				for (Element nfe : listaNfe) {
					Element correlacao = SinedUtil.getChildElement("correlacao", nfe.getContent());
					if(correlacao == null) throw new SinedException("Não foi encontrada a tag 'correlacao' no XML");
					String numeroNota = correlacao.getText();
					
					Element erros = SinedUtil.getChildElement("erros", nfe.getContent());
					if(erros != null){
						List<String> listaErroMsg = new ArrayList<String>();
						List<Element> listaErro = SinedUtil.getListChildElement("erro", erros.getContent());
						for (Element erro : listaErro) {
							listaErroMsg.add(erro.getText());
						}
						
						mapErros.put(numeroNota, listaErroMsg);
					} else {
						Element senha = SinedUtil.getChildElement("senha", nfe.getContent());
						Element numero = SinedUtil.getChildElement("numero", nfe.getContent());
						
						if(senha == null) throw new SinedException("Não foi encontrada a tag 'senha' no XML");
						if(numero == null) throw new SinedException("Não foi encontrada a tag 'numero' no XML");
						
						mapaNumNfse.put(numeroNota, numero.getText());
						mapaCodVerificacao.put(numeroNota, senha.getText());
					}
				}
				
				boolean contemErro = mapErros.size() > 0;
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					if(contemErro){
						if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
							notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
						}
						
						List<String> listaErros = mapErros.get(arquivonfnota.getNota().getNumero());
						if(listaErros != null && listaErros.size() > 0){
							for (String erroMsg : listaErros) {
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonfnota(arquivonfnota);
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(erroMsg);
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						}
					} else {
						String numeroNfse = mapaNumNfse.get(arquivonfnota.getNota().getNumero());
						String codigoVerificacao = mapaCodVerificacao.get(arquivonfnota.getNota().getNumero());
						
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
							documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
							try {
								vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao);
						
						if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(arquivonfnota.getNota())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
						} else {
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
						}
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
					}
				}

				Arquivo arquivoXmlRetornoEnvio = new Arquivo(content, "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				
				arquivonf.setArquivoretornoenvio(arquivoXmlRetornoEnvio);
				arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
				
				arquivoService.saveOrUpdate(arquivoXmlRetornoEnvio);
				
				this.updateArquivoretornoenvio(arquivonf);	
				
				if(contemErro){
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				} else {
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				}
				
			} else throw new SinedException("Não foi encontrada nenhuma tag 'nf-e' no XML.");
		} else throw new SinedException("Não foi encontrada nenhuma tag 'emissao' no XML.");
	}
	
	public void processaRetornoEnvioMarabaIss(Arquivonf arquivonf, byte[] content) throws JDOMException, IOException, ParseException {
		String stringArquivo = new String(content);
		List<String> listaNFE2 = new ArrayList<String>();
		String[] arrayNFE2 = stringArquivo.split("\\r?\\n");
		
		if (arrayNFE2 != null && arrayNFE2.length > 0) {
			for (String linha : arrayNFE2) {
				if (linha.contains("NFE2")) {
					listaNFE2.add(linha);
				} else if (linha.contains("RET1")) {
					listaNFE2.add(linha);
				}
			}
			
			List<Arquivonfnota> listaArquivoNfNota = arquivonfnotaService.findByArquivonf(arquivonf);
			Boolean isProcessadoComSucesso = Boolean.FALSE;
			
			for (String nfe2 : listaNFE2) {
				String numeroNota = nfe2.contains("NFE2") ? Integer.parseInt(nfe2.substring(11, 20)) + "" : Integer.parseInt(nfe2.substring(20, 30)) + "";
				String numeroRps = nfe2.contains("NFE2") ? Integer.parseInt(nfe2.substring(20, 30)) + "" : Integer.parseInt(nfe2.substring(11, 20)) + "";
				String codigoVerificacao = nfe2.substring(30, 40);
				String dataEmissaoStr = nfe2.contains("NFE2") ? nfe2.substring(41, 49) : nfe2.substring(40, 48);
				Date dataEmissao = org.apache.commons.lang.StringUtils.isNotEmpty(dataEmissaoStr) ? new SimpleDateFormat("yyyyMMdd").parse(dataEmissaoStr) : null;
				for (Arquivonfnota arquivonfnota : listaArquivoNfNota) {
					if (org.apache.commons.lang.StringUtils.isNotEmpty(numeroRps) && numeroRps.equals(arquivonfnota.getNota().getNumero())) {
						arquivonfnotaService.updateNumeroCodigo(arquivonfnota, StringUtils.soNumero(numeroNota), codigoVerificacao, new Timestamp(dataEmissao.getTime()), null);
						
						if (notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || 
								notaService.isNotaPossuiReceita(arquivonfnota.getNota())) {
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
						} else {
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
						}
						
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
						
						isProcessadoComSucesso = Boolean.TRUE;
					}
				}
			}
			
			if (isProcessadoComSucesso) {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				
				Arquivo arquivoXmlRetornoEnvio = new Arquivo(content, "LOTE-RETORNO_" + SinedUtil.datePatternForReport() + ".TXT", "text/txt");
				
				arquivonf.setArquivoretornoenvio(arquivoXmlRetornoEnvio);
				arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
				
				arquivoService.saveOrUpdate(arquivoXmlRetornoEnvio);
				
				this.updateArquivoretornoenvio(arquivonf);	
				
				String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
				
				try {
					if (paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")) {
						this.enviarNFCliente(CollectionsUtil.listAndConcatenate(listaArquivoNfNota, "nota.cdNota", ","), TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			throw new SinedException("Erro ao importar NFS-e.");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioSmarapd(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws JDOMException, IOException {
		String xml = new String(content);
		
		if(xml.startsWith("<?xml")){
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element recibo = SinedUtil.getChildElement("recibo", rootElement.getContent());
			if(recibo != null){
				Element codrecibo = SinedUtil.getChildElement("codrecibo", recibo.getContent());
				if(codrecibo != null){
					String numeroLoteStr = codrecibo.getText();
					
					this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
					this.updateProtocolo(arquivonf, numeroLoteStr);
					
					List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
					for (Arquivonfnota arquivonfnota : listan) {
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
					}
					
					xml = xml.replaceAll("<\\?.+\\?>", "");
					String stringXmlConsultaLote = Util.strings.tiraAcento("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xml);
					Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
					arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
					arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
					arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
					this.updateArquivoxmlconsultalote(arquivonf);
				}
			}
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xml);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioCampinasiss(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws JDOMException, IOException {
		String xml = new String(content);
		
		if(xml.startsWith("<?xml")){
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			boolean sucesso = false;
			String numeroLoteStr = "";
			Element cabecalhoElement = SinedUtil.getChildElement("Cabecalho", rootElement.getContent());
			if(cabecalhoElement != null){
				Element numeroLoteElement = SinedUtil.getChildElement("NumeroLote", cabecalhoElement.getContent());
				if(numeroLoteElement != null){
					numeroLoteStr = numeroLoteElement.getText();
					this.updateProtocolo(arquivonf, numeroLoteStr);
				}
				
				Element sucessoElement = SinedUtil.getChildElement("Sucesso", cabecalhoElement.getContent());
				if(sucessoElement != null){
					String sucessoStr = sucessoElement.getText();
					sucesso = sucessoStr != null && sucessoStr.trim().toLowerCase().equals("true");
				}
			}
		
			if(sucesso){
				this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
				}
				
				Empresa empresa = empresaService.loadByArquivonf(arquivonf);
				ReqConsultaLoteRPS reqConsultaLoteRPS = this.createXmlConsultaLoteNfseCampinas(empresa, numeroLoteStr, configuracaonfe);
			
				String stringXmlConsultaLote = Util.strings.tiraAcento(reqConsultaLoteRPS.toString());
				Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
				arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
				arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
				this.updateArquivoxmlconsultalote(arquivonf);	
				
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
				}
				
				Element errosElement = SinedUtil.getChildElement("Erros", rootElement.getContent());
				if(errosElement != null){
					List<Element> listaErroElement = SinedUtil.getListChildElement("Erro", errosElement.getContent());
					if(listaErroElement != null){
						for (Element erroElement : listaErroElement) {
							String descricao = null;
							
							Element descricaoElement = SinedUtil.getChildElement("Descricao", erroElement.getContent());
							if(descricaoElement != null){
								descricao = descricaoElement.getText();
							}
							
							if(descricao != null){
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(descricao);
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						}
					}
				}
			}
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
			}
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xml);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
		}
	}
	
	private ReqConsultaLoteRPS createXmlConsultaLoteNfseCampinas(Empresa empresa, String numeroLoteStr, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixo = configuracaonfe.getPrefixowebservice();
		
		ReqConsultaLoteRPS reqConsultaLoteRPS = new ReqConsultaLoteRPS();
		
		Cabecalho cabecalho = new Cabecalho(false);
		reqConsultaLoteRPS.setCabecalho(cabecalho);
		
		if(prefixo.equals(Prefixowebservice.CAMPINASISS_HOM) || prefixo.equals(Prefixowebservice.CAMPINASISS_PROD)){		
			cabecalho.setCodCidade("6291");
		} else if(prefixo.equals(Prefixowebservice.SOROCABAISS_HOM) || prefixo.equals(Prefixowebservice.SOROCABAISS_PROD)){		
			cabecalho.setCodCidade("7145");
		} else throw new SinedException("Não foi possível obter o prefixo da configuração.");
		cabecalho.setcPFCNPJRemetente(empresa != null ? empresa.getCnpj().getValue() : null);
		cabecalho.setNumeroLote(numeroLoteStr);
		cabecalho.setVersao("1");
		
		return reqConsultaLoteRPS;
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioSpiss(Arquivonf arquivonf, byte[] content) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		Element cabecalho = SinedUtil.getChildElement("Cabecalho", rootElement.getContent());
		
		if(cabecalho != null){
			Element sucesso = SinedUtil.getChildElement("Sucesso", cabecalho.getContent());
			if(sucesso != null && sucesso.getText() != null && sucesso.getText().toUpperCase().equals("TRUE")){
				
				List<Element> listaChaveNFeRPS = SinedUtil.getListChildElement("ChaveNFeRPS", rootElement.getContent());
				if(listaChaveNFeRPS != null && listaChaveNFeRPS.size() > 0){
					Map<String, String> mapaNumNfse = new HashMap<String, String>();
					Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
					
					for (Element chaveNFeRPS : listaChaveNFeRPS) {
						Element chaveNFe = SinedUtil.getChildElement("ChaveNFe", chaveNFeRPS.getContent());
						Element numeroNFe = SinedUtil.getChildElement("NumeroNFe", chaveNFe.getContent());
						Element codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", chaveNFe.getContent());
						
						Element chaveRPS = SinedUtil.getChildElement("ChaveRPS", chaveNFeRPS.getContent());
						Element numeroRPS = SinedUtil.getChildElement("NumeroRPS", chaveRPS.getContent());
						
						String numNota = numeroRPS.getText();
						String numNfse = numeroNFe.getText();
						String codVerificacao = codigoVerificacao.getText();
						
						mapaNumNfse.put(numNota, numNfse);
						mapaCodVerificacao.put(numNota, codVerificacao);
					}
					
					String numeroNfse, codigoVerificacao;
					List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
					for (Arquivonfnota arquivonfnota : listan) {
						String numeroNota = SinedUtil.retiraZeroEsquerda(arquivonfnota.getNota().getNumero());
						numeroNfse = mapaNumNfse.get(numeroNota);
						codigoVerificacao = mapaCodVerificacao.get(numeroNota);
						
						if(numeroNfse != null && codigoVerificacao != null){
							if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
								documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
								try {
									vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							
							arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao);
							
							if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
								arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
							} else {
								arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
							}
							notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
						}
					}
					
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				} else {
					
					
					List<Element> listaAlerta = SinedUtil.getListChildElement("Alerta", rootElement.getContent());
					if(listaAlerta != null && listaAlerta.size() > 0){
						for (Element alerta : listaAlerta) {
							Element descricao = SinedUtil.getChildElement("Descricao", alerta.getContent());
							if(descricao != null){
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(descricao.getValue());
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						}
						
						List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
						for (Arquivonfnota arquivonfnota : listan) {
							if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
								arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
								notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
							}
						}
						
						this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
					}
				}
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
				
				List<Element> erros = SinedUtil.getListChildElement("Erro", rootElement.getContent());
				if(erros != null){
					Arquivonfnotaerro arquivonfnotaerro;
					Element descricao;
					for (Element erro : erros) {
						descricao = SinedUtil.getChildElement("Descricao", erro.getContent());
						if(descricao != null){
							arquivonfnotaerro = new Arquivonfnotaerro();
							
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setMensagem(descricao.getValue());
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao)
	 *
	 * @param arquivonf
	 * @param situacao
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao) {
		arquivonfDAO.updateSituacao(arquivonf, situacao);
	}
	
	public void updateEnviando(String whereIn, Boolean enviando) {
		arquivonfDAO.updateEnviando(whereIn, enviando);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateProtocolo(Arquivonf arquivonf, String protocoloStr)
	 *
	 * @param arquivonf
	 * @param protocoloStr
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	private void updateProtocolo(Arquivonf arquivonf, String protocoloStr) {
		arquivonfDAO.updateProtocolo(arquivonf, protocoloStr);
	}
	
    @SuppressWarnings("unchecked")
    public void processaRetornoEnvioEel2(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws UnsupportedEncodingException, JDOMException, IOException {
        Element rootElement = SinedUtil.getRootElementXML(content);
        
        if (rootElement != null) {
        	Element numeroProtocoloElement = SinedUtil.getChildElement("numeroProtocolo", rootElement.getContent());
        	
            if (numeroProtocoloElement != null) {
            	String xmlConsulta = Util.strings.tiraAcento(numeroProtocoloElement.getValue());
            	
            	Arquivo arquivoXmlConsultaLote = new Arquivo(xmlConsulta.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
            	
				arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
				arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
				arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
				
				this.updateArquivoxmlconsultalote(arquivonf);
            	this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
				}
            } else {
                this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
                
                List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
                
                for (Arquivonfnota arquivonfnota : listan) {
                    if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
                        arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
                        notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
                    }
                }
                
                List<Element> lista = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
                
                if (lista != null && lista.size() > 0) {
                    Arquivonfnotaerro arquivonfnotaerro;
                    
                    for (Element e : lista) {
                        arquivonfnotaerro = new Arquivonfnotaerro();
                        arquivonfnotaerro.setArquivonf(arquivonf);
                        arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
                        
                        if (org.apache.commons.lang.StringUtils.isNotEmpty(e.getValue())) {
                            arquivonfnotaerro.setMensagem(e.getValue());
                        } else {
                            arquivonfnotaerro.setMensagem("Erro ao enviar NFS-e.");
                        }
                        
                        arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
                    }
                } else {
                    Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
                    
                    arquivonfnotaerro.setArquivonf(arquivonf);
                    arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
                    arquivonfnotaerro.setMensagem("Erro ao enviar NFS-e.");
                    
                    arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public Boolean processaRetornoConsultaEel2(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) throws UnsupportedEncodingException, JDOMException, IOException {
        try {
            Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
            
            if (rootElement != null) {
            	Element notasFiscaisElement = SinedUtil.getChildElement("notasFiscais", rootElement.getContent());
            	if(notasFiscaisElement == null){
            		notasFiscaisElement = SinedUtil.getChildElement("nfeRpsNotaFiscal", rootElement.getContent());
            	}
            	
                if (notasFiscaisElement != null) {
                	Date dataEmissao;
                	
                	Element dataProcessamentoElement = SinedUtil.getChildElement("dataProcessamento", notasFiscaisElement.getContent());
                	Element idNotaElement = SinedUtil.getChildElement("idNota", notasFiscaisElement.getContent());
                	Element numeroElement = SinedUtil.getChildElement("numero", notasFiscaisElement.getContent());
                	
                	String dtEmissaoStr = dataProcessamentoElement.getValue();
                	dtEmissaoStr = dtEmissaoStr.replaceAll("T", "//");
                	
        			try {
        				dataEmissao = FORMAT_DATETIME.parse(dtEmissaoStr);
        			} catch (Exception e) {
        				dataEmissao = SinedDateUtils.currentDate();
        			}
                	
                	List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
                	
                	arquivonfnotaService.updateNumeroCodigo(listan.get(0), StringUtils.soNumero(numeroElement.getValue()), idNotaElement.getValue(), new Timestamp(dataEmissao.getTime()), null);
                	
                	if(notaDocumentoService.isNotaWebservice(listan.get(0).getNota()) || 
        					notaService.isNotaPossuiReceita(listan.get(0).getNota(), true)){
                		listan.get(0).getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
        			} else {
        				listan.get(0).getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
        			}
                	
                	
                	notaService.alterarStatusAcao(listan.get(0).getNota(), "Arquivo NFS-e processado com sucesso.", null);                	
                	
        			this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
                } else {
                	Boolean emProcessamento = Boolean.FALSE;
                	
                    List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
                    List<Arquivonfnotaerro> listaErro = new ArrayList<Arquivonfnotaerro>();
                    List<Element> lista = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
                    
                    if (lista != null && lista.size() > 0) {
                        Arquivonfnotaerro arquivonfnotaerro;
                        
                        for (Element e : lista) {
                        	if ("EL68".contains(e.getValue()) || "EL47".contains(e.getValue())) {
                        		emProcessamento = Boolean.TRUE;
                        		
                        		listaErro = new ArrayList<Arquivonfnotaerro>();
                        		
                        		arquivonfnotaerro = new Arquivonfnotaerro();
        						
        						arquivonfnotaerro.setArquivonf(arquivonf);
        						arquivonfnotaerro.setMensagem(e.getValue());
        						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
        						
        						listaErro.add(arquivonfnotaerro);
                        		
                        		break;
                        	}
                        	
                            arquivonfnotaerro = new Arquivonfnotaerro();
                            arquivonfnotaerro.setArquivonf(arquivonf);
                            arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
                            
                            if (org.apache.commons.lang.StringUtils.isNotEmpty(e.getValue())) {
                                arquivonfnotaerro.setMensagem(e.getValue());
                            } else {
                                arquivonfnotaerro.setMensagem("Erro ao consultar NFS-e.");
                            }
                            
                            listaErro.add(arquivonfnotaerro);
                        }
                    } else {
                        Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
                        
                        arquivonfnotaerro.setArquivonf(arquivonf);
                        arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
                        arquivonfnotaerro.setMensagem("Erro ao consultar NFS-e.");
                        
                        arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
                    }
                    
                    for (Arquivonfnotaerro arquivonfnotaerro : listaErro) {
                		arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
                	}
                    
                    if (emProcessamento) {
                    	throw new SinedException("Lote ainda em processamento");
                    } else {
                    	this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
                        
                        for (Arquivonfnota arquivonfnota : listan) {
                            if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
                                arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
                                notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
                            }
                        }
                    }
                }
            }
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void processaRetornoCancelamentoEel2(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
        try {
        	try {
        		System.out.println("xml de cancelamento castelo: " + new String(content));
        		//TODO remover apos testes castelo luiz.silva.codigo
			} catch (Exception e) {
			}
        	
            Element rootElement = SinedUtil.getRootElementXML(content);
            
            arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
            
            if (rootElement != null) {
            	Element nfeRpsNotaFiscalElement = SinedUtil.getChildElement("nfeRpsNotaFiscal", rootElement.getContent());
            	
                if (nfeRpsNotaFiscalElement != null) {
                	
        			Nota nota = arquivonfnota.getNota();
                	String obs = "Cancelamento na Receita";
                	
    				nota.setNotaStatus(NotaStatus.CANCELADA);
    				notaService.alterarStatusAcao(nota, obs, null);
    				
    				arquivonfnotaService.updateCancelado(arquivonfnota);
                } else {
                    List<Element> lista = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
                    
                    if (lista != null && lista.size() > 0) {
                        for (Element erro : lista) {
                            Nota nota = arquivonfnota.getNota();
                            NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
                            
                            Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
                            arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
                            arquivonfnotaerrocancelamento.setMensagem(erro != null ? erro.getValue() : "Motivo não identificado.");
                            
                            arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
                            
                            if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
                                String obs = "Cancelamento na Receita não efetuado.";
                                nota.setNotaStatus(lastSituacao);
                                notaService.alterarStatusAcao(nota, obs, null);
                            }
                        }
                    }
                }
            } else {
            	Nota nota = arquivonfnota.getNota();
                NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
                
                Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
                arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
                arquivonfnotaerrocancelamento.setMensagem("Motivo não identificado.");
                
                arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
                
                if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
                    String obs = "Cancelamento na Receita não efetuado.";
                    nota.setNotaStatus(lastSituacao);
                    notaService.alterarStatusAcao(nota, obs, null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public EnviarLoteRpsEnvio createXmlEnviarLoteNfseEel2(List<NotaFiscalServico> listaNota, Empresa empresa, WebRequestContext request, Configuracaonfe configuracaonfe) {
        Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
        String webservice = prefixowebservice.getPrefixo();
        br.com.linkcom.sined.geral.bean.Endereco enderecoEmpresa = empresa.getEndereco();
        
        EnviarLoteRpsEnvio enviarLoteRpsEnvio = new EnviarLoteRpsEnvio(webservice);
        LoteRps loteRps = new LoteRps(webservice);
        
        CpfCnpj cpfCnpj = new CpfCnpj(webservice);
        cpfCnpj.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
        
        br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador prestador = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador(webservice);
        prestador.setCpfCnpj(cpfCnpj);
        prestador.setIndicadorCpfCnpj(2);
        prestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
        
        if(empresa.getCnpj() == null){
            throw new SinedException("Favor cadastrar CNPJ da empresa " + empresa.getRazaosocialOuNome());
        }
        
        if(configuracaonfe.getInscricaomunicipal() == null){
            throw new SinedException("Favor cadastrar Insc. Municipal da configuração " + configuracaonfe.getDescricao());
        }
        
        Integer proxNum = configuracaonfeService.getNextNumLoteNfse(configuracaonfe);
        String serie = configuracaonfe.getSerienfse();
        
        loteRps.setId(StringUtils.stringCheia(proxNum.toString() + serie, "0", 13, false));
        
        loteRps.setNumeroLote(proxNum);
        loteRps.setQuantidadeRps(listaNota.size());
        loteRps.setPrestador(prestador);
        
        ListaRps listaRps = new ListaRps(webservice);
        List<br.com.linkcom.lknfe.xml.nfse.eel2.Rps> rpsList = new ArrayList<br.com.linkcom.lknfe.xml.nfse.eel2.Rps>();
        br.com.linkcom.lknfe.xml.nfse.eel2.Rps rps;
        
        for (NotaFiscalServico nfs : listaNota) {
            rps = new br.com.linkcom.lknfe.xml.nfse.eel2.Rps();
            
            String numeroRps = StringUtils.soNumero(nfs.getNumero());
            String id = StringUtils.stringCheia(proxNum.toString() + numeroRps + serie, "0", 13, false);
            
            rps.setId(id);
            
            if (enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null && enderecoEmpresa.getMunicipio().getCdibge() != null &&
                    nfs.getCliente() != null && nfs.getCliente().getEndereco() != null && nfs.getCliente().getEndereco().getMunicipio() != null &&
                    nfs.getCliente().getEndereco().getMunicipio().getCdibge() != null &&
                    enderecoEmpresa.getMunicipio().getCdibge().equals(nfs.getCliente().getEndereco().getMunicipio().getCdibge())) {
                rps.setLocalPrestacao(2);
            } else {
                rps.setLocalPrestacao(1);
            }
            
            if (nfs.getIncideiss() != null && nfs.getIncideiss()) {
                rps.setIssRetido(2);
            } else {
                rps.setIssRetido(1);
            }
            
            rps.setDataEmissao(nfs.getDtEmissao());
            
            IdentificacaoRps identificacaoRps = new IdentificacaoRps();
            
            identificacaoRps.setNumero(numeroRps);
            identificacaoRps.setSerie(configuracaonfe.getSerienfse());
            identificacaoRps.setTipo("1");
            
            rps.setIdentificacaoRps(identificacaoRps);
            
            br.com.linkcom.lknfe.xml.nfse.eel2.DadosPrestador dadosPrestador = new br.com.linkcom.lknfe.xml.nfse.eel2.DadosPrestador();
            
            dadosPrestador.setPrestador(prestador);
            dadosPrestador.setRazaoSocial(empresa.getRazaosocial());
            dadosPrestador.setNomeFantasia(empresa.getNomefantasia());
            dadosPrestador.setIncentivadorCultural(configuracaonfe.getIncentivadorcultural() != null && configuracaonfe.getIncentivadorcultural() ? 1 : 2);
            dadosPrestador.setOptanteSimplesNacional(configuracaonfe.getOptantesimples() != null && configuracaonfe.getOptantesimples() ? 1 : 2);
            dadosPrestador.setNaturezaOperacao(nfs.getNaturezaoperacao() != null ? Integer.parseInt(nfs.getNaturezaoperacao().getCodigonfse()) : 0);
            dadosPrestador.setRegimeEspecialTributacao(nfs.getRegimetributacao() != null ? Integer.parseInt(nfs.getRegimetributacao().getCodigonfse()) : 0);            
            
            br.com.linkcom.lknfe.xml.nfse.eel2.Endereco enderecoPrestador = new br.com.linkcom.lknfe.xml.nfse.eel2.Endereco();
            
            enderecoPrestador.setLogradouro(enderecoEmpresa.getLogradouro());
            enderecoPrestador.setLogradouroNumero(enderecoEmpresa.getNumero());
            enderecoPrestador.setLogradouroComplemento(enderecoEmpresa.getComplemento());
            enderecoPrestador.setBairro(enderecoEmpresa.getBairro());
            enderecoPrestador.setCodigoMunicipio(enderecoEmpresa.getMunicipio() != null ? enderecoEmpresa.getMunicipio().getCdibge() : null);
            enderecoPrestador.setMunicipio(enderecoEmpresa.getMunicipio() != null ? enderecoEmpresa.getMunicipio().getNome() : null);
            enderecoPrestador.setUf(enderecoEmpresa.getMunicipio() != null && enderecoEmpresa.getMunicipio().getUf() != null ? enderecoEmpresa.getMunicipio().getUf().getSigla() : null);
            enderecoPrestador.setCep(enderecoEmpresa.getCep() != null ? enderecoEmpresa.getCep().getValue() : null);
            
            dadosPrestador.setEndereco(enderecoPrestador);
            
            Contato contatoPrestador = new Contato(webservice);
            
            contatoPrestador.setEmail(empresa.getEmail() != null ? empresa.getEmail() : "");
            contatoPrestador.setTelefone(configuracaonfe.getTelefone() != null ? configuracaonfe.getTelefone() : "");
            
            dadosPrestador.setContato(contatoPrestador);
            
            rps.setDadosPrestador(dadosPrestador);
            
            if (nfs.getCliente() != null) {
                Cliente clienteTomador = nfs.getCliente();
                br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = nfs.getEnderecoCliente(); 
                
                br.com.linkcom.lknfe.xml.nfse.eel2.DadosTomador dadosTomador = new br.com.linkcom.lknfe.xml.nfse.eel2.DadosTomador();
                IdentificacaoTomador identificacaoTomador = new IdentificacaoTomador(webservice);
                CpfCnpj cpfCnpjTomador = new CpfCnpj(webservice);
                
                if (Tipopessoa.PESSOA_FISICA.equals(clienteTomador.getTipopessoa())) {
                    identificacaoTomador.setIndicacaoCpfCnpj(1);
                    cpfCnpjTomador.setCpf(StringUtils.soNumero(clienteTomador.getCpf().getValue()));
                } else if (Tipopessoa.PESSOA_JURIDICA.equals(clienteTomador.getTipopessoa())) {
                    identificacaoTomador.setIndicacaoCpfCnpj(2);
                    cpfCnpjTomador.setCnpj(StringUtils.soNumero(clienteTomador.getCnpj().getValue()));
                } else {
                    identificacaoTomador.setIndicacaoCpfCnpj(3);
                }
                
                identificacaoTomador.setCpfCnpj(cpfCnpjTomador);
                identificacaoTomador.setInscricaoMunicipal(clienteTomador.getInscricaomunicipal());
                
                dadosTomador.setIdentificacaoTomador(identificacaoTomador);
                dadosTomador.setRazaoSocial(clienteTomador.getRazaosocial());
                dadosTomador.setNomeFantasia(clienteTomador.getNome());
                
                br.com.linkcom.lknfe.xml.nfse.eel2.Endereco enderecoTomador = new br.com.linkcom.lknfe.xml.nfse.eel2.Endereco();
                
                enderecoTomador.setLogradouro(enderecoCliente.getLogradouro());
                enderecoTomador.setLogradouroNumero(enderecoCliente.getNumero());
                enderecoTomador.setLogradouroComplemento(enderecoCliente.getComplemento());
                enderecoTomador.setBairro(enderecoCliente.getBairro());
                enderecoTomador.setCodigoMunicipio(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getCdibge() : null);
                enderecoTomador.setMunicipio(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getNome() : null);
                enderecoTomador.setUf(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : null);
                enderecoTomador.setCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
                
                dadosTomador.setEndereco(enderecoTomador);
                
                Contato contatoTomador = new Contato(webservice);
                
                contatoTomador.setEmail(clienteTomador.getEmail() != null ? clienteTomador.getEmail() : "");
                contatoTomador.setTelefone(nfs.getTelefoneCliente() != null ? StringUtils.soNumero(nfs.getTelefoneCliente()) : "");
                
                dadosTomador.setContato(contatoTomador);
                
                rps.setDadosTomador(dadosTomador);
            }
            
            ListaServico listaServico = new ListaServico();
            List<br.com.linkcom.lknfe.xml.nfse.eel2.Servico> servicoList = new ArrayList<br.com.linkcom.lknfe.xml.nfse.eel2.Servico>();
            br.com.linkcom.lknfe.xml.nfse.eel2.Servico servico;
            
            notaFiscalServicoService.setValorIssqnProporcionalItem(nfs);
            for (NotaFiscalServicoItem nfsi : nfs.getListaItens()) {
            	
            	if(parametrogeralService.getBoolean(Parametrogeral.VALIDAR_VALOR_SERVICOS_NFSE)){
            		if(nfsi.getTotalParcial() == null || nfsi.getTotalParcial().getValue().doubleValue() == 0d)
            			continue;
            	}
                servico = new br.com.linkcom.lknfe.xml.nfse.eel2.Servico();
                
                servico.setCodigoCnae(nfs.getCodigocnaeBean() != null ? nfs.getCodigocnaeBean().getCnae() : null);
                servico.setCodigoServico116(nfs.getItemlistaservicoBean() != null ? nfs.getItemlistaservicoBean().getCodigo() : null);
                servico.setCodigoServicoMunicipal(nfs.getItemlistaservicoBean() != null ? nfs.getItemlistaservicoBean().getCodigo() : null);
                servico.setQuantidade(nfsi.getQtde());
                servico.setUnidade(nfsi.getMaterial() != null && nfsi.getMaterial().getUnidademedida() != null ? nfsi.getMaterial().getUnidademedida().getSimbolo() : "UN");
                
                String descricao = this.preencheDescricaoItemNfs(nfsi, configuracaonfe);        		
        		servico.setDescricao(descricao);                
                
                servico.setAliquota(nfs.getIss() != null ? nfs.getIss() / 100 : null);
                
                nfsi.setPrecoTotal(new Money(nfsi.getPrecoUnitario() * nfsi.getQtde()));
                
                if(nfsi.getDesconto() != null){
                    nfsi.setPrecoTotal(nfsi.getPrecoTotal().subtract(nfsi.getDesconto()));
                }
                
                servico.setValorServico(nfsi.getPrecoUnitario() != null ? nfsi.getPrecoUnitario() : null);
                servico.setValorDesconto(nfsi.getDesconto() != null ? nfsi.getDesconto().getValue().doubleValue() : null);
                servico.setValorIssqn(nfsi.getValorIss() != null ? nfsi.getValorIss().getValue().doubleValue() : null);
                
                servicoList.add(servico);
            }
            
            listaServico.setListaServico(servicoList);
            
            rps.setListaServico(listaServico);
            
            Valores valores = new Valores(webservice);
            
            valores.setValorServicos(nfs.getValorBruto() != null ? nfs.getValorBruto().getValue().doubleValue() : null);
            valores.setValorDeducoes(nfs.getDeducao() != null ? nfs.getDeducao().getValue().doubleValue() : null);
            valores.setValorPis(nfs.getValorPis() != null ? nfs.getValorPis().getValue().doubleValue() : null);
            valores.setValorCofins(nfs.getValorCofins() != null ? nfs.getValorCofins().getValue().doubleValue() : null);
            valores.setValorInss(nfs.getValorInss() != null ? nfs.getValorInss().getValue().doubleValue() : null);
            valores.setValorIr(nfs.getValorIr() != null ? nfs.getValorIr().getValue().doubleValue() : null);
            valores.setValorCsll(nfs.getValorCsll() != null ? nfs.getValorCsll().getValue().doubleValue() : null);
            valores.setValorIss(nfs.getValorIss() != null ? nfs.getValorIss().getValue().doubleValue() : null);
            valores.setOutrasRetencoes(nfs.getOutrasretencoes() != null ? nfs.getOutrasretencoes().getValue().doubleValue() : null);
            valores.setValorLiquidoNfse(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue() : null);
            valores.setValorIssRetido(nfs.getIncideiss() != null && nfs.getIncideiss() && nfs.getValorIss() != null ? nfs.getValorIss().getValue().doubleValue() : null);
            
            rps.setValores(valores);
            
            rps.setObservacao(nfs.getInfocomplementar());
            rps.setStatus(1);
            
            rpsList.add(rps);
        }
        
        listaRps.setListaRpsEel2(rpsList);
        loteRps.setListaRps(listaRps);
        
        enviarLoteRpsEnvio.setLoteRps(loteRps);
        
        return enviarLoteRpsEnvio;
    }
	
	public EnviarLoteRpsEnvio createXmlEnviarLoteNfseIssonline(List<NotaFiscalServico> listaNota, Empresa empresa, WebRequestContext request, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		String webservice = prefixowebservice.getPrefixo();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		String xmlns = "";
		if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_HOM){
			xmlns = "http://nfejf.portalfacil.com.br/homologacao/schema/nfse_v202.xsd";
		} else if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_PROD){
			xmlns = "http://nfejf.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		}  else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_HOM) {
			xmlns = "http://homologacaosetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_PROD) {
			xmlns = "http://nfsesetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		}
		
		EnviarLoteRpsEnvio enviarLoteRpsEnvio = new EnviarLoteRpsEnvio(webservice, xmlns);
		
		if (webservice != null && "sigep".equals(webservice)) {
			if (org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getLoginparacatu())) {
				throw new SinedException("Favor cadastrar o usuário da configuração " + configuracaonfe.getDescricao());
			}
			
			if (org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getSenhaparacatu())) {
				throw new SinedException("Favor cadastrar a senha da configuração " + configuracaonfe.getDescricao());
			}
			
			if (org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getToken())) {
				throw new SinedException("Favor cadastrar a chave da configuração " + configuracaonfe.getDescricao());
			}
			
			enviarLoteRpsEnvio.setUsuario(configuracaonfe.getLoginparacatu());
			enviarLoteRpsEnvio.setSenha(configuracaonfe.getSenhaparacatu());
			enviarLoteRpsEnvio.setChave(configuracaonfe.getToken());
		}
		
		LoteRps loteRps = new LoteRps(webservice);
		enviarLoteRpsEnvio.setLoteRps(loteRps);
		
		Integer proxNum = configuracaonfeService.getNextNumLoteNfse(configuracaonfe);
		loteRps.setNumeroLote(proxNum);
		
		if(empresa.getCnpj() == null){
			throw new SinedException("Favor cadastrar CNPJ da empresa " + empresa.getRazaosocialOuNome());
		}
		
		if(configuracaonfe.getInscricaomunicipal() == null){
			throw new SinedException("Favor cadastrar Insc. Municipal da configuração " + configuracaonfe.getDescricao());
		}
		
		loteRps.setId(proxNum.toString());
		
		if (webservice != null && (webservice.equals("eel") || webservice.equals("abaco"))) {
			br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador prestadorLoteRps = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador(webservice);
			loteRps.setPrestador(prestadorLoteRps);
			
			CpfCnpj cpfCnpjPrestadorLoteRps = new CpfCnpj(webservice);
			prestadorLoteRps.setCpfCnpj(cpfCnpjPrestadorLoteRps);
			
			if (Prefixowebservice.PETROLINA_HOM.equals(prefixowebservice) || Prefixowebservice.PETROLINA_PROD.equals(prefixowebservice)) {
				cpfCnpjPrestadorLoteRps.setCnpj(empresa.getCnpj() != null ? empresa.getCnpj().toString() : null);
			} else {
				cpfCnpjPrestadorLoteRps.setCnpj(empresa.getCnpj() != null ? StringUtils.soNumero(empresa.getCnpj().getValue()) : null);
			}
			
			cpfCnpjPrestadorLoteRps.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
			
			prestadorLoteRps.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		} else {
			loteRps.setCnpj(empresa.getCnpj() != null ? StringUtils.soNumero(empresa.getCnpj().getValue()) : null);
			loteRps.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		}
		
		loteRps.setQuantidadeRps(listaNota.size());
		
		ListaRps listaRps = new ListaRps(webservice);
		loteRps.setListaRps(listaRps);
		
		List<Rps> lista = listaRps.getListaRps();
		Rps rps;
		br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Rps rpsIssonline;
		InfDeclaracaoPrestacaoServico infDeclaracaoPrestacaoServico;
		IdentificacaoRps identificacaoRps;
		br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Servico servico;
		br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Valores valores;
		String strDiscriminacao;
		StringBuilder discriminacao;
		br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador prestador;
		Tomador tomador;
		IdentificacaoTomador identificacaoTomador;
		CpfCnpj cpfCnpj;
		CpfCnpj cpfCnpjPrestador;
		Endereco endereco;
		br.com.linkcom.sined.geral.bean.Endereco enderecoCliente;
		
		for (NotaFiscalServico nfs : listaNota) {
			rps = new Rps(webservice);
			infDeclaracaoPrestacaoServico = new InfDeclaracaoPrestacaoServico(webservice);
			rps.setInfDeclaracaoPrestacaoServico(infDeclaracaoPrestacaoServico);
			
			rpsIssonline = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Rps(webservice);
			infDeclaracaoPrestacaoServico.setRps(rpsIssonline);
			
			identificacaoRps = new IdentificacaoRps(webservice);
			rpsIssonline.setIdentificacaoRps(identificacaoRps);
			
			String numeroRps = StringUtils.soNumero(nfs.getNumero());
			String id = proxNum.toString() + numeroRps;
			identificacaoRps.setNumero(numeroRps);
			if(configuracaonfe.getSerienfse() != null && !configuracaonfe.getSerienfse().equals("")){
				identificacaoRps.setSerie(configuracaonfe.getSerienfse());
			} else {
				identificacaoRps.setSerie("A");
			}
			
			if (webservice != null && "sigep".equals(webservice) && configuracaonfe.getSerienfse() != null && !configuracaonfe.getSerienfse().equals("")) {
				identificacaoRps.setTipo(configuracaonfe.getSerienfse());
			} else {
				identificacaoRps.setTipo("1");
			}
			
			rpsIssonline.setDataEmissao(nfs.getDtEmissao());
			
			if (webservice != null && "sigep".equals(webservice)) {
				rpsIssonline.setStatus("CO");
			} else {
				rpsIssonline.setStatus("1");
			}
			
			rpsIssonline.setId(id);
			
			if (webservice != null && webservice.equals("fiorilli")) {
				infDeclaracaoPrestacaoServico.setId(empresa.getEnderecoNumeroEstado() + "" + 
						(nfs.getDtEmissao() != null ? new SimpleDateFormat("yy").format(nfs.getDtEmissao()) : "") + "" + 
						(nfs.getDtEmissao() != null ? StringUtils.stringCheia(new Integer(SinedDateUtils.getMes(nfs.getDtEmissao())).toString(), "0", 2, false) : "")+ "" + 
						(empresa.getCnpj() != null ? StringUtils.soNumero(empresa.getCnpj().getValue()) : "") + "" + 
						(configuracaonfe.getInscricaomunicipal() != null ? StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()), "0", 5, false) : "") + "" + 
						(nfs.getNumero() != null ? StringUtils.stringCheia(nfs.getNumero(), "0", 9, false) : "") + "" + 
						(nfs.getNumero() != null ? StringUtils.stringCheia(nfs.getNumero(), "0", 9, false) : ""));
			} else {
				infDeclaracaoPrestacaoServico.setId(proxNum.toString());
			}
			
			infDeclaracaoPrestacaoServico.setInformacoesComplementares(org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getInfocomplementar()) ? nfs.getInfocomplementar() : null);
			infDeclaracaoPrestacaoServico.setCompetencia(nfs.getDtEmissao());
			infDeclaracaoPrestacaoServico.setRegimeEspecialTributacao(nfs.getRegimetributacao() != null ? nfs.getRegimetributacao().getCodigonfse() : null);
			infDeclaracaoPrestacaoServico.setOptanteSimplesNacional(configuracaonfe.getOptantesimples() != null ? configuracaonfe.getOptantesimples() : false);
			infDeclaracaoPrestacaoServico.setIncentivoFiscal(configuracaonfe.getIncentivadorcultural() != null ? configuracaonfe.getIncentivadorcultural() : false);
			infDeclaracaoPrestacaoServico.setProducao(ambienteProducao);
			
			servico = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Servico(webservice);
			infDeclaracaoPrestacaoServico.setServico(servico);
			
			valores = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Valores(webservice);
			servico.setValores(valores);
			
			valores.setValorServicos(nfs.getValorBruto().getValue().doubleValue());
			valores.setValorDeducoes(nfs.getDeducao() != null ? nfs.getDeducao().getValue().doubleValue() : null);
			valores.setDescontoCondicionado(nfs.getDescontocondicionado() != null ? nfs.getDescontocondicionado().getValue().doubleValue() : null);
			valores.setDescontoIncondicionado(nfs.getDescontoincondicionado() != null ? nfs.getDescontoincondicionado().getValue().doubleValue() : null);
			valores.setOutrasRetencoes(nfs.getOutrasretencoes() != null ? nfs.getOutrasretencoes().getValue().doubleValue() : null);
			
			if(nfs.getIss() != null && nfs.getIss() > 0){
				valores.setAliquota(SinedUtil.round(nfs.getIss(), 4));
			}
			
			Money valorIss = nfs.getValorIss();
			if(valorIss != null) valores.setValorIss(valorIss.getValue().doubleValue());
			
			if(nfs.getIncidepis() != null && nfs.getIncidepis() && nfs.getValorPis() != null){
				double valorPis = nfs.getValorPis().getValue().doubleValue();
				valores.setValorPis(valorPis);
			}
			
			if(nfs.getIncidecofins() != null && nfs.getIncidecofins() && nfs.getValorCofins() != null){
				double valorCofins = nfs.getValorCofins().getValue().doubleValue();
				valores.setValorCofins(valorCofins);
			}
			
			if(nfs.getIncideinss() != null && nfs.getIncideinss() && nfs.getValorInss() != null){
				double valorInss = nfs.getValorInss().getValue().doubleValue();
				valores.setValorInss(valorInss);
			}
			
			if(nfs.getIncideir() != null && nfs.getIncideir() && nfs.getValorIr() != null){
				double valorIr = nfs.getValorIr().getValue().doubleValue();
				valores.setValorIr(valorIr);
			}
			
			if(nfs.getIncidecsll() != null && nfs.getIncidecsll() && nfs.getValorCsll() != null){
				double valorCsll = nfs.getValorCsll().getValue().doubleValue();
				valores.setValorCsll(valorCsll);
			}
			
			if(nfs.getIncideiss() != null && nfs.getIncideiss()){
				servico.setIssRetido(true);
				if(prefixowebservice != Prefixowebservice.GOIANIA_PROD && prefixowebservice != Prefixowebservice.GOIANIA_HOM){
					servico.setResponsavelRetencao(1);
				}
			} else {
				servico.setIssRetido(false);
			}
			
			if(nfs.getCodigocnaeBean() != null) servico.setCodigoCnae(StringUtils.soNumero(nfs.getCodigocnaeBean().getCnae()));
			if(nfs.getItemlistaservicoBean() != null){
				if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_HOM || prefixowebservice == Prefixowebservice.JUIZDEFORAISS_PROD
						|| prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_HOM || prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_PROD 
						|| prefixowebservice == Prefixowebservice.AVARE_HOM || prefixowebservice == Prefixowebservice.AVARE_PROD 
						|| prefixowebservice == Prefixowebservice.AVARE_NOVO_HOM || prefixowebservice == Prefixowebservice.AVARE_NOVO_PROD
						|| prefixowebservice == Prefixowebservice.SAOJOSEDALAPA_NOVO_HOM || prefixowebservice == Prefixowebservice.SAOJOSEDALAPA_NOVO_PROD
                        || prefixowebservice == Prefixowebservice.IPATINGA_HOM || prefixowebservice == Prefixowebservice.IPATINGA_PROD
                        || prefixowebservice == Prefixowebservice.LIMEIRA_NOVO_HOM || prefixowebservice == Prefixowebservice.LIMEIRA_NOVO_PROD
                        || prefixowebservice == Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM || prefixowebservice == Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD
                        || prefixowebservice == Prefixowebservice.CASTELO_HOM || prefixowebservice == Prefixowebservice.CASTELO_PROD
                        || prefixowebservice == Prefixowebservice.PETROLINA_HOM || prefixowebservice == Prefixowebservice.PETROLINA_PROD
                        || prefixowebservice == Prefixowebservice.NOVALIMAISS_HOM || prefixowebservice == Prefixowebservice.NOVALIMAISS_PROD
                        || prefixowebservice == Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM || prefixowebservice == Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD
                        || prefixowebservice == Prefixowebservice.ITUMBIARA_NOVO_HOM || prefixowebservice == Prefixowebservice.ITUMBIARA_NOVO_PROD
                        || prefixowebservice == Prefixowebservice.ITUMBIARA_PROD || prefixowebservice == Prefixowebservice.ITUMBIARA_HOM) {
					servico.setItemListaServico(nfs.getItemlistaservicoBean().getCodigo());
				}else {
					servico.setItemListaServico(StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo()));
				}
				
			}
			
			if(nfs.getCodigotributacao() != null) {
				if(prefixowebservice == Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM || prefixowebservice == Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD
                        || prefixowebservice == Prefixowebservice.ITUMBIARA_NOVO_HOM || prefixowebservice == Prefixowebservice.ITUMBIARA_NOVO_PROD) {
					servico.setCodigoTributacaoMunicipio(nfs.getCodigotributacao().getCodigo());
				} else {
					servico.setCodigoTributacaoMunicipio(StringUtils.soNumero(nfs.getCodigotributacao().getCodigo()));
				}
			}
			if (prefixowebservice == Prefixowebservice.GOIANIA_HOM || prefixowebservice == Prefixowebservice.GOIANIA_PROD) {
				servico.setCodigoPais("1058");
				servico.setCodigoMunicipio(nfs.getMunicipioissqn().getCodigogoiania());
			} else {
				servico.setCodigoMunicipio(nfs.getMunicipioissqn().getCdibge());
			}
			
			if(configuracaonfe.getExigibilidadeisslavras() == null){
				throw new SinedException("Cadastrar a exibilidade ISS na configuração de NFS-e.");
			}
			servico.setExigibilidadeISS(configuracaonfe.getExigibilidadeisslavras().getValue() + 1);
			
			discriminacao = new StringBuilder();
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
			}
			
			strDiscriminacao = discriminacao.toString();
			
			if (strDiscriminacao.length() > 1) {
				strDiscriminacao = strDiscriminacao.substring(0, strDiscriminacao.length() - 1);
			}
			
			servico.setDiscriminacao(strDiscriminacao.length() > 2000 ? addScape(strDiscriminacao.substring(0, 2000)) : addScape(strDiscriminacao));
			
			prestador = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador(webservice);
			infDeclaracaoPrestacaoServico.setPrestador(prestador);
			
			cpfCnpjPrestador = new CpfCnpj(webservice);
			prestador.setCpfCnpj(cpfCnpjPrestador);
			
			cpfCnpjPrestador.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
			cpfCnpjPrestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
			
			prestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
			
			if(!webservice.equals("mi6") && !webservice.equals("portalfacil") && !webservice.equals("webiss2") && !webservice.equals("fiorilli")  
					&& !webservice.equals("gissonline") && !webservice.equals("prodeb") && !webservice.equals("sigbancos") && !webservice.equals("saatri")
					&& !webservice.equals("manaus") && !webservice.equals("goianiaiss") && !webservice.equals("iibrasil") && !webservice.equals("eel") && !webservice.equals("abaco")
					&& !webservice.equals("joaopessoaiss") && !webservice.equals("sigep") && !webservice.equals("centi")) {
				if(org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getSenha()) ||
						org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getFrasesecreta())){
					throw new SinedException("Cadastrar a senha e frase secreta na configuração de NFS-e.");
				}
				
				prestador.setSenha(configuracaonfe.getSenha());
				prestador.setFrasesecreta(configuracaonfe.getFrasesecreta());
			}
			
			tomador = new Tomador(webservice);
			infDeclaracaoPrestacaoServico.setTomadorServico(tomador);
			
			if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
				tomador.setRazaoSocial(addScape(nfs.getNomefantasiaCliente()));
			}else if(nfs.getCliente().getRazaosocial() != null && !nfs.getCliente().getRazaosocial().trim().equals(""))
				tomador.setRazaoSocial(addScape(nfs.getCliente().getRazaosocial()));
			else 
				tomador.setRazaoSocial(addScape(nfs.getCliente().getNome().trim()));
			
			identificacaoTomador = new IdentificacaoTomador(webservice);
			tomador.setIdentificacaoTomador(identificacaoTomador);
			
			cpfCnpj = new CpfCnpj(webservice);
			identificacaoTomador.setCpfCnpj(cpfCnpj);
			
			if(nfs.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				if(nfs.getCliente().getCpf() == null) throw new SinedException("Cadastrar o CPF do cliente " + nfs.getCliente().getNome());
				cpfCnpj.setCpf(StringUtils.soNumero(nfs.getCliente().getCpf().getValue()));
			} else {
				if(nfs.getCliente().getCnpj() == null) throw new SinedException("Cadastrar o CNPJ do cliente " + nfs.getCliente().getNome());
				cpfCnpj.setCnpj(StringUtils.soNumero(nfs.getCliente().getCnpj().getValue()));
				if(nfs.getInscricaomunicipal() != null)
					identificacaoTomador.setInscricaoMunicipal(!nfs.getInscricaomunicipal().trim().equals("") ? nfs.getInscricaomunicipal() : null);
				else
					identificacaoTomador.setInscricaoMunicipal(nfs.getCliente().getInscricaomunicipal());
			}
			
			enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());

			String erroEnderecoCliente = "";
			
			if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) erroEnderecoCliente += "Logradouro do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")) erroEnderecoCliente += "Bairro do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getCep() == null || StringUtils.soNumero(enderecoCliente.getCep().getValue()).equals("")) erroEnderecoCliente += "CEP do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")) erroEnderecoCliente += "Número do endereço do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getMunicipio() == null) erroEnderecoCliente += "Município do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			
			if(!erroEnderecoCliente.equals("")){
				throw new SinedException(erroEnderecoCliente);
			}
			
			endereco = new Endereco(webservice);
			tomador.setEndereco(endereco);
			
			endereco.setEndereco(addScape(enderecoCliente.getLogradouro()));
			endereco.setNumero(enderecoCliente.getNumero());
			endereco.setComplemento(addScape(enderecoCliente.getComplemento()));
			endereco.setBairro(addScape(enderecoCliente.getBairro()));
			if (prefixowebservice == Prefixowebservice.GOIANIA_HOM || prefixowebservice == Prefixowebservice.GOIANIA_PROD) {
				endereco.setCodigoMunicipio(enderecoCliente.getMunicipio().getCodigogoiania());
				
				endereco.setEndereco(removerCaracterEspecialEspacoBranco(endereco.getEndereco()));
				endereco.setComplemento(removerCaracterEspecialEspacoBranco(endereco.getComplemento()));
				endereco.setBairro(removerCaracterEspecialEspacoBranco(endereco.getBairro()));
			} else {
				endereco.setCodigoMunicipio(enderecoCliente.getMunicipio().getCdibge());
			}
			endereco.setUf(enderecoCliente.getMunicipio().getUf().getSigla());
			endereco.setCodigoPais("1058");
			endereco.setCep(StringUtils.soNumero(enderecoCliente.getCep().getValue()));
			
			String emailcliente = nfs.getCliente().getEmail();
			String telefonecliente = null;

			if (nfs.getTelefoneCliente() != null && !"".equals(nfs.getTelefoneCliente())) {
				telefonecliente = StringUtils.soNumero(nfs.getTelefoneCliente());
			} else if (nfs.getCliente().getListaTelefone() != null && nfs.getCliente().getListaTelefone().size() > 0) {
				telefonecliente = StringUtils.soNumero(nfs.getCliente().getListaTelefone().iterator().next().getTelefone());
			}
			
			if (webservice != null && "sigep".equals(webservice)) {
				Contato contato = new Contato(webservice);
				
				if (telefonecliente.startsWith("0")) {
					contato.setDdd(telefonecliente.substring(0, 3));
					contato.setTelefone(telefonecliente.substring(3));
				} else {
					contato.setDdd("0" + telefonecliente.substring(0, 2));
					contato.setTelefone(telefonecliente.substring(2));
				}
				
				contato.setTipoTelefone("RE");
				contato.setEmail(addScape(emailcliente));
				
				tomador.setContato(contato);
			} else {
				if ((emailcliente != null && !emailcliente.equals("")) || (telefonecliente != null && !telefonecliente.equals(""))) {
					Contato contato = new Contato(webservice);
	
					if (emailcliente != null && !emailcliente.equals(""))
						contato.setEmail(addScape(emailcliente));
	
					if (telefonecliente != null && !telefonecliente.equals("")) {
						contato.setTelefone(telefonecliente);
					}
	
					tomador.setContato(contato);
				}		
			}
			
			lista.add(rps);
		}
		
		return enviarLoteRpsEnvio;
	}
	
	private String removerCaracterEspecialEspacoBranco(String endereco) {
		if(endereco == null) return endereco;
		return endereco.trim().replace("?", "");
	}
	
	private br.com.linkcom.lknfe.xml.nfse.eliss.LoteRps createXmlEnviarLoteNfseElIss(List<NotaFiscalServico> listaNota, Empresa empresa, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		br.com.linkcom.lknfe.xml.nfse.eliss.LoteRps loteRps = new br.com.linkcom.lknfe.xml.nfse.eliss.LoteRps();
		
		String cnpjPrestadorTeste = "23596650000147";
		String imPrestadorTeste = "0010949";
		
		String cnpjTomadorTeste = "05120082000121";
		String imTomadorTeste = "0009720";
		
		br.com.linkcom.sined.geral.bean.Endereco enderecoBDPrestador = configuracaonfe.getEndereco();
		Municipio municipioBDPrestador = enderecoBDPrestador.getMunicipio();
		Uf ufBDPrestador = municipioBDPrestador.getUf();
		
		IdentificacaoPrestador identificacaoPrestador = new IdentificacaoPrestador();
		identificacaoPrestador.setIndicacaoCpfCnpj(2); // PADRÃO CNPJ
		if(ambienteProducao){
			identificacaoPrestador.setCpfCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
			identificacaoPrestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		} else {
			identificacaoPrestador.setCpfCnpj(cnpjPrestadorTeste);
			identificacaoPrestador.setInscricaoMunicipal(imPrestadorTeste);
		}
		
		br.com.linkcom.lknfe.xml.nfse.eliss.Endereco enderecoPrestador = new br.com.linkcom.lknfe.xml.nfse.eliss.Endereco();
		enderecoPrestador.setLogradouro(enderecoBDPrestador.getLogradouro());
		enderecoPrestador.setLogradouroNumero(enderecoBDPrestador.getNumero());
		enderecoPrestador.setLogradouroComplemento(enderecoBDPrestador.getComplemento());
		enderecoPrestador.setBairro(enderecoBDPrestador.getBairro());
		enderecoPrestador.setCodigoMunicipio(municipioBDPrestador.getCdibge());
		enderecoPrestador.setMunicipio(municipioBDPrestador.getNome());
		enderecoPrestador.setUf(ufBDPrestador.getSigla());
		enderecoPrestador.setCep(enderecoBDPrestador.getCep() != null ? enderecoBDPrestador.getCep().getValue() : null);
		
		br.com.linkcom.lknfe.xml.nfse.eliss.Contato contatoPrestador = new br.com.linkcom.lknfe.xml.nfse.eliss.Contato();
		contatoPrestador.setEmail(empresa.getEmail());
		contatoPrestador.setTelefone(configuracaonfe.getTelefone());
		
		String idLote = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date(System.currentTimeMillis()));
		Integer proxNum = configuracaonfeService.getNextNumLoteNfse(configuracaonfe);
		
		loteRps.setId(idLote);
		loteRps.setNumeroLote(proxNum);
		loteRps.setQuantidadeRps(listaNota.size());
		loteRps.setIdentificacaoPrestador(identificacaoPrestador);
		
		br.com.linkcom.lknfe.xml.nfse.eliss.ListaRps listaRps = new br.com.linkcom.lknfe.xml.nfse.eliss.ListaRps();
		loteRps.setListaRps(listaRps);
		
		
		List<br.com.linkcom.lknfe.xml.nfse.eliss.Rps> lista = listaRps.getListaRps();
		for (NotaFiscalServico nfs : listaNota) {
			br.com.linkcom.lknfe.xml.nfse.eliss.Rps rps = new br.com.linkcom.lknfe.xml.nfse.eliss.Rps();
			
			String idRps = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date(System.currentTimeMillis()));
			
			Integer localPrestacao = 2; // NO MUNICÍPIO
			if(nfs.getMunicipioissqn() != null && !nfs.getMunicipioissqn().equals(municipioBDPrestador)){
				localPrestacao = 1; // FORA DO MUNICÍPIO
			}
			
			rps.setId(idRps);
			rps.setLocalPrestacao(localPrestacao);
			rps.setIssRetido(nfs.getIncideiss() != null && nfs.getIncideiss() ? 2 : 1);
			rps.setDataEmissao(nfs.getDtEmissao());
			rps.setObervacao(nfs.getDadosAdicionais());
			rps.setStatus(1);
			
			br.com.linkcom.lknfe.xml.nfse.eliss.IdentificacaoRps identificacaoRps = new br.com.linkcom.lknfe.xml.nfse.eliss.IdentificacaoRps();
			rps.setIdentificacaoRps(identificacaoRps);
			
			identificacaoRps.setNumero(StringUtils.soNumero(nfs.getNumero()));
			identificacaoRps.setSerie(configuracaonfe.getSerienfse());
			identificacaoRps.setTipo(1);
			
			DadosPrestador dadosPrestador = new DadosPrestador();
			rps.setDadosPrestador(dadosPrestador);
			
			dadosPrestador.setIdentificacaoPrestador(identificacaoPrestador);
			dadosPrestador.setRazaoSocial(empresa.getRazaosocialOuNome());
			dadosPrestador.setNomeFantasia(empresa.getNomefantasia());
			if(ambienteProducao){
				dadosPrestador.setIncentivadorCultural(configuracaonfe.getIncentivadorcultural() != null && configuracaonfe.getIncentivadorcultural() ? 1 : 2);
				dadosPrestador.setOptanteSimplesNacional(configuracaonfe.getOptantesimples() != null && configuracaonfe.getOptantesimples() ? 1 : 2);
			} else {
				dadosPrestador.setIncentivadorCultural(2);
				dadosPrestador.setOptanteSimplesNacional(2);
			}
			try {
				if(ambienteProducao){
					dadosPrestador.setNaturezaOperacao(nfs.getNaturezaoperacao() != null ? Integer.parseInt(nfs.getNaturezaoperacao().getCodigonfse()) : 0);
				} else {
					dadosPrestador.setNaturezaOperacao(6);
				}
			} catch (Exception e) {
				dadosPrestador.setNaturezaOperacao(0);
			}
			try {
				if(ambienteProducao){
					dadosPrestador.setRegimeEspecialTributacao(nfs.getRegimetributacao() != null ? Integer.parseInt(nfs.getRegimetributacao().getCodigonfse()) : 0);
				} else {
					dadosPrestador.setRegimeEspecialTributacao(8);
				}
			} catch (Exception e) {
				dadosPrestador.setRegimeEspecialTributacao(0);
			}
			
			dadosPrestador.setEndereco(enderecoPrestador);
			dadosPrestador.setContato(contatoPrestador);
			
			
			DadosTomador dadosTomador = new DadosTomador();
			rps.setDadosTomador(dadosTomador);
			
			Cliente cliente = nfs.getCliente();
			
			br.com.linkcom.lknfe.xml.nfse.eliss.IdentificacaoTomador identificacaoTomador = new br.com.linkcom.lknfe.xml.nfse.eliss.IdentificacaoTomador();
			dadosTomador.setIdentificacaoTomador(identificacaoTomador);
			
			if(ambienteProducao){
				identificacaoTomador.setCpfCnpj(StringUtils.soNumero(cliente.getCpfcnpj()));
				identificacaoTomador.setIndicacaoCpfCnpj(cliente.getTipopessoa().ordinal() + 1);
				identificacaoTomador.setInscricaoMunicipal(cliente.getInscricaomunicipal());
			} else {
				identificacaoTomador.setCpfCnpj(cnpjTomadorTeste);
				identificacaoTomador.setIndicacaoCpfCnpj(2);
				identificacaoTomador.setInscricaoMunicipal(imTomadorTeste);
			}
			
			dadosTomador.setRazaoSocial(cliente.getRazaosocial() != null && cliente.getRazaosocial().trim().equals("") ? cliente.getRazaosocial() : cliente.getNome());
			dadosTomador.setNomeFantasia(cliente.getNome());
			
			br.com.linkcom.sined.geral.bean.Endereco enderecoBDCliente = nfs.getEnderecoCliente();
			if(enderecoBDCliente != null){
				enderecoBDCliente = enderecoService.loadEndereco(enderecoBDCliente);
				
				br.com.linkcom.lknfe.xml.nfse.eliss.Endereco enderecoTomador = new br.com.linkcom.lknfe.xml.nfse.eliss.Endereco();
				dadosTomador.setEndereco(enderecoTomador);
				
				Municipio municipioBDCliente = enderecoBDCliente.getMunicipio();
				Uf ufBDCliente = municipioBDCliente != null ? municipioBDCliente.getUf() : null;
				
				enderecoTomador.setLogradouro(enderecoBDCliente.getLogradouro());
				enderecoTomador.setLogradouroNumero(enderecoBDCliente.getNumero());
				enderecoTomador.setLogradouroComplemento(enderecoBDCliente.getComplemento());
				enderecoTomador.setBairro(enderecoBDCliente.getBairro());
				enderecoTomador.setCodigoMunicipio(municipioBDCliente != null ? municipioBDCliente.getCdibge() : null);
				enderecoTomador.setMunicipio(municipioBDCliente != null ? municipioBDCliente.getNome() : null);
				enderecoTomador.setUf(ufBDCliente != null ? ufBDCliente.getSigla() : null);
				enderecoTomador.setCep(enderecoBDCliente.getCep() != null ? enderecoBDCliente.getCep().getValue() : null);
			}
			
			br.com.linkcom.lknfe.xml.nfse.eliss.Contato contatoCliente = new br.com.linkcom.lknfe.xml.nfse.eliss.Contato();
			dadosTomador.setContato(contatoCliente);
			
			String emailcliente = cliente.getEmail();
			String telefonecliente = null;

			if (nfs.getTelefoneCliente() != null && !"".equals(nfs.getTelefoneCliente())) {
				telefonecliente = StringUtils.soNumero(nfs.getTelefoneCliente());
			} else if (nfs.getCliente().getListaTelefone() != null && nfs.getCliente().getListaTelefone().size() > 0) {
				telefonecliente = StringUtils.soNumero(nfs.getCliente().getListaTelefone().iterator().next().getTelefone());
			}
			contatoCliente.setEmail(emailcliente);
			contatoCliente.setTelefone(telefonecliente);
			
			Servicos servicos = new Servicos();
			rps.setServicos(servicos);
			
			br.com.linkcom.lknfe.xml.nfse.eliss.Servico servico = new br.com.linkcom.lknfe.xml.nfse.eliss.Servico();
			servicos.getListaServico().add(servico);
			
			if(nfs.getItemlistaservicoBean() != null) servico.setCodigoServico116(StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo()));
			if(nfs.getCodigocnaeBean() != null) servico.setCodigoCnae(StringUtils.soNumero(nfs.getCodigocnaeBean().getCnae()));
			if(nfs.getCodigotributacao() != null) servico.setCodigoServicoMunicipal(nfs.getCodigotributacao().getCodigo());
			servico.setQuantidade(1);
			servico.setUnidade("UN");
			
			StringBuilder discriminacao = new StringBuilder();
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
			}
			servico.setDescricao(discriminacao.toString());
			
			Double aliquotaIss = nfs.getIss();
			Money valorIss = nfs.getValorIss();
			Double valorServico = nfs.getValorBruto() != null ? nfs.getValorBruto().getValue().doubleValue() : null;
			
			servico.setAliquota(aliquotaIss != null ? aliquotaIss/100.0 : null);
			servico.setValorServico(valorServico);
			servico.setValorIssqn(valorIss != null ? valorIss.getValue().doubleValue() : null);
			
			br.com.linkcom.lknfe.xml.nfse.eliss.Valores valores = new br.com.linkcom.lknfe.xml.nfse.eliss.Valores();
			rps.setValores(valores);
			
			valores.setValorServicos(valorServico);
			valores.setValorDeducoes(nfs.getDeducao() != null ? nfs.getDeducao().getValue().doubleValue() : null);
			
			if(nfs.getIncidepis() != null && nfs.getIncidepis() && nfs.getValorPis() != null){
				double valorPis = nfs.getValorPis().getValue().doubleValue();
				valores.setValorPis(valorPis);
			}
			
			if(nfs.getIncidecofins() != null && nfs.getIncidecofins() && nfs.getValorCofins() != null){
				double valorCofins = nfs.getValorCofins().getValue().doubleValue();
				valores.setValorCofins(valorCofins);
			}
			
			if(nfs.getIncideinss() != null && nfs.getIncideinss() && nfs.getValorInss() != null){
				double valorInss = nfs.getValorInss().getValue().doubleValue();
				valores.setValorInss(valorInss);
			}
			
			if(nfs.getIncideir() != null && nfs.getIncideir() && nfs.getValorIr() != null){
				double valorIr = nfs.getValorIr().getValue().doubleValue();
				valores.setValorIr(valorIr);
			}
			
			if(nfs.getIncidecsll() != null && nfs.getIncidecsll() && nfs.getValorCsll() != null){
				double valorCsll = nfs.getValorCsll().getValue().doubleValue();
				valores.setValorCsll(valorCsll);
			}
			
			valores.setValorIss(valorIss != null ? valorIss.getValue().doubleValue() : null);
			valores.setValorOutrasRetencoes(nfs.getOutrasretencoes() != null ? nfs.getOutrasretencoes().getValue().doubleValue() : null);
			
			Money valorNotaLiquido = nfs.getValorNota();
			if(valorNotaLiquido != null) {
				valores.setValorLiquidoNfse(valorNotaLiquido.getValue().doubleValue());
			}
			if(nfs.getIncideiss() != null && nfs.getIncideiss()){
				valores.setValorIssRetido(valorIss != null ? valorIss.getValue().doubleValue() : null);
			}
			
			Double descontoCondicionado = nfs.getDescontocondicionado() != null ? nfs.getDescontocondicionado().getValue().doubleValue() : 0d;
			Double descontoIncondicionado = nfs.getDescontoincondicionado() != null ? nfs.getDescontoincondicionado().getValue().doubleValue() : 0d;
			valores.setOutrosDescontos(descontoCondicionado + descontoIncondicionado);
			
			lista.add(rps);
		}
		
		return loteRps;
	}
	
	/**
	 * Cria o XMl de envio de uma NF de Serviço Eletrônica.
	 *
	 * @param whereIn
	 * @param empresa
	 * @param request
	 * @param webservice
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public EnviarLoteRpsEnvio createXmlEnviarLoteNfse(List<NotaFiscalServico> listaNota, Empresa empresa, WebRequestContext request, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		String webservice = prefixowebservice.getPrefixo();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		String xmlns = "";
		
		if(prefixowebservice == Prefixowebservice.IPATINGA_HOM) {
            xmlns = "http://nfeipatinga.portalfacil.com.br/homologacao/schema/nfse_v01.xsd";
        } else if(prefixowebservice == Prefixowebservice.IPATINGA_PROD) {
            xmlns = "http://nfeipatinga.portalfacil.com.br/nfseserv/schema/nfse_v01.xsd";
        }
		
		EnviarLoteRpsEnvio enviarLoteRpsEnvio = new EnviarLoteRpsEnvio(webservice, xmlns);
		
		LoteRps loteRps = new LoteRps(webservice);
		enviarLoteRpsEnvio.setLoteRps(loteRps);
		
		Integer proxNum = configuracaonfeService.getNextNumLoteNfse(configuracaonfe);
		loteRps.setNumeroLote(proxNum);
		
		if(empresa.getCnpj() == null){
			throw new SinedException("Favor cadastrar CNPJ da empresa " + empresa.getRazaosocialOuNome());
		}
		
		if(configuracaonfe.getInscricaomunicipal() == null){
			throw new SinedException("Favor cadastrar Insc. Municipal da configuração " + configuracaonfe.getDescricao());
		}
		
		String cnpj = StringUtils.soNumero(empresa.getCnpj().getValue());
		String inscricaoMunicipal = configuracaonfe.getInscricaomunicipal();
		String id = "Lote" + cnpj + inscricaoMunicipal;
		
		if(webservice.equals("fisslex")){
			id = id + proxNum;
		} else if (webservice.equals("tecnosistemas")) {
			id = "1" + SinedDateUtils.getAno(SinedDateUtils.currentDate()) + cnpj + StringUtils.stringCheia(proxNum.toString(), "0", 16, false);
		} else if (webservice.equals("manaus") && loteRps.getNumeroLote() != null) {
			id = loteRps.getNumeroLote().toString();
		}
		
		if (prefixowebservice == Prefixowebservice.IPATINGA_HOM || prefixowebservice == Prefixowebservice.IPATINGA_PROD) {
			id = cnpj + inscricaoMunicipal;
		}
		
		loteRps.setId(id);
		loteRps.setCodMunicipio(configuracaonfe.getEndereco().getMunicipio().getCdibge());
		loteRps.setCnpj(cnpj);
		loteRps.setInscricaoMunicipal(inscricaoMunicipal);
		loteRps.setQuantidadeRps(listaNota.size());
		
		String paramAnotacaocorpoContrato = parametrogeralService.getValorPorNome(Parametrogeral.CONTRATO_ANOTACAOCORPO_CONCATENAR);
		boolean concatenarAnotacaocorpo = paramAnotacaocorpoContrato != null && paramAnotacaocorpoContrato.toUpperCase().trim().equals("TRUE");
		
		ListaRps listaRps = new ListaRps(webservice);
		loteRps.setListaRps(listaRps);
		
		List<Rps> lista = listaRps.getListaRps();
		Rps rps;
		InfRps infRps;
		IdentificacaoRps identificacaoRps;
		Servico servico;
		Valores valores;
		String strDiscriminacao;
		StringBuilder discriminacao;
		Prestador prestador;
		Tomador tomador;
		IdentificacaoTomador identificacaoTomador;
		CpfCnpj cpfCnpj;
		Endereco endereco;
		br.com.linkcom.sined.geral.bean.Endereco enderecoCliente;
		int i = 0;
		
		for (NotaFiscalServico nfs : listaNota) {
			rps = new Rps(webservice);
			infRps = new InfRps(webservice);
			rps.setInfRps(infRps);
			
			i++;
			infRps.setId("Rps" + i + "11");
			
			if(webservice.equals("memory")) {
				infRps.setId(i + "");
			} else if (webservice.equals("tecnosistemas")) {
				infRps.setId("1" + cnpj + StringUtils.stringCheia(StringUtils.soNumero(nfs.getNumero()), "0", 16, false));
			} else if (webservice.equals("manaus")) {
				infRps.setId(StringUtils.soNumero(nfs.getNumero()));
			} else if (webservice.equals("publica")) {
				infRps.setId("assinar");
			}
			
			if (prefixowebservice == Prefixowebservice.IPATINGA_HOM || prefixowebservice == Prefixowebservice.IPATINGA_PROD) {
				infRps.setId("rps" + StringUtils.soNumero(nfs.getNumero()) + configuracaonfe.getSerienfse());
			}
			
			identificacaoRps = new IdentificacaoRps(webservice);
			infRps.setIdentificacaoRps(identificacaoRps);
			
			identificacaoRps.setNumero(StringUtils.soNumero(nfs.getNumero()));
			if(configuracaonfe.getSerienfse() != null && !configuracaonfe.getSerienfse().equals("")){
				identificacaoRps.setSerie(configuracaonfe.getSerienfse());
			} else {
				identificacaoRps.setSerie("A");
			}
			identificacaoRps.setTipo("1");
			
			infRps.setDataEmissao(nfs.getDtEmissao());
			infRps.setNaturezaOperacao(nfs.getNaturezaoperacao().getCodigonfse() != null ? nfs.getNaturezaoperacao().getCodigonfse() : "");
			infRps.setRegimeEspecialTributacao(nfs.getRegimetributacao() != null ? nfs.getRegimetributacao().getCodigonfse() : null);
			infRps.setOptanteSimplesNacional(configuracaonfe.getOptantesimples() != null ? configuracaonfe.getOptantesimples() : false);
			infRps.setIncentivadorCultural(configuracaonfe.getIncentivadorcultural() != null ? configuracaonfe.getIncentivadorcultural() : false);
			if (webservice != null && webservice.equals("webiss")) {
				infRps.setOutrasInformacoes(nfs.getDadosAdicionais());
			}
			infRps.setStatus(1);
			
			servico = new Servico(webservice);
			infRps.setServico(servico);
			
			if (Prefixowebservice.CAMBUI_HOM.equals(prefixowebservice) || Prefixowebservice.CAMBUI_PROD.equals(prefixowebservice) ||
					Prefixowebservice.JOAO_MONLEVADE_NOVO_HOM.equals(prefixowebservice) || Prefixowebservice.JOAO_MONLEVADE_NOVO_PROD.equals(prefixowebservice)) {
				valores = new Valores(webservice, "cambui");
			} else {
				valores = new Valores(webservice);
			}
			
			servico.setValores(valores);
			
			valores.setDescontoCondicionado(nfs.getDescontocondicionado() != null ? nfs.getDescontocondicionado().getValue().doubleValue() : null);
			valores.setDescontoIncondicionado(nfs.getDescontoincondicionado() != null ? nfs.getDescontoincondicionado().getValue().doubleValue() : null);
			if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JABOTICABALISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JABOTICABALISS_PROD) ||
					((nfs.getOutrasretencoes() == null || nfs.getOutrasretencoes().getValue().doubleValue() == 0) &&
							(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.RIOCLAROISS_HOM) ||
							configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.RIOCLAROISS_PROD)))){
				valores.setOutrasRetencoes(null);
			}else {
				valores.setOutrasRetencoes(nfs.getOutrasretencoes() != null ? nfs.getOutrasretencoes().getValue().doubleValue() : null);
			}
			
			if (nfs.getIncentivoFiscalIss() != null && nfs.getIncentivoFiscalIss() > 0) {
				Double valorDeducoes = nfs.getDeducao() != null ? SinedUtil.round(nfs.getDeducao().getValue().doubleValue(), 2) : 0;
				Double valorIncentivoFiscalIss = (nfs.getBasecalculo().getValue().doubleValue() * nfs.getIncentivoFiscalIss()) / 100;
								
				valores.setValorDeducoes(SinedUtil.round(valorDeducoes + valorIncentivoFiscalIss, 2));
				valores.setBaseCalculo(nfs.getBasecalculoiss() != null ? nfs.getBasecalculoiss().getValue().doubleValue() : nfs.getValorBruto().getValue().doubleValue());
			} else {
				valores.setValorDeducoes(nfs.getDeducao() != null ? nfs.getDeducao().getValue().doubleValue() : null);
				valores.setBaseCalculo(nfs.getBasecalculo() != null ? nfs.getBasecalculo().getValue().doubleValue() : nfs.getValorBruto().getValue().doubleValue());
			}
			
			valores.setValorServicos(nfs.getValorBruto().getValue().doubleValue());
			
			if(!webservice.equals("aparecidaiss") &&
					!webservice.equals("simpliss") &&
					!webservice.equals("govdigital") &&
					!webservice.equals("memory")){
				if(nfs.getIss() != null && nfs.getIss() > 0){
					valores.setAliquota(SinedUtil.round(nfs.getIss()/100d, 4));
				}
			} else {
				if(nfs.getIss() != null && nfs.getIss() > 0){
					valores.setAliquota(SinedUtil.round(nfs.getIss(), 4));
				}
			}
			
			Money valorIss = nfs.getValorIss();
			if(valorIss != null){
				if(webservice.equals("ginfesiss") && 
						!prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD)){
					BigDecimal valorIssBigDecimal = valorIss.getValue();
					valorIss = new Money(SinedUtil.round(valorIssBigDecimal, 2));
				}
				valores.setValorIss(valorIss.getValue().doubleValue());
			}
			
			Money valorNotaLiquido = null;
			if(webservice.equals("ginfesiss")){
				valorNotaLiquido = nfs.getValorNotaGinfes();
			} else {
				valorNotaLiquido = nfs.getValorNota();
			}
			if(valorNotaLiquido != null) valores.setValorLiquidoNfse(valorNotaLiquido.getValue().doubleValue());
			
			if(nfs.getIncidepis() != null && nfs.getIncidepis() && (nfs.getValorPis() != null || nfs.getValorPisRetido() != null)){
				double valorPis = nfs.getValorPis().getValue().doubleValue();
				if(nfs.getValorPisRetido() != null && nfs.getValorPisRetido().getValue().doubleValue() > 0){
					valorPis = nfs.getValorPisRetido().getValue().doubleValue();
				}
				valores.setValorPis(valorPis);
			}
			
			if(nfs.getIncidecofins() != null && nfs.getIncidecofins() && (nfs.getValorCofins() != null || nfs.getValorCofinsRetido() != null)){
				double valorCofins = nfs.getValorCofins().getValue().doubleValue();
				if(nfs.getValorCofinsRetido() != null && nfs.getValorCofinsRetido().getValue().doubleValue() > 0){
					valorCofins = nfs.getValorCofinsRetido().getValue().doubleValue();
				}
				valores.setValorCofins(valorCofins);
			}
			
			if(nfs.getIncideinss() != null && nfs.getIncideinss() && nfs.getValorInss() != null){
				double valorInss = nfs.getValorInss().getValue().doubleValue();
				valores.setValorInss(valorInss);
			}
			
			if(nfs.getIncideir() != null && nfs.getIncideir() && nfs.getValorIr() != null){
				double valorIr = nfs.getValorIr().getValue().doubleValue();
				valores.setValorIr(valorIr);
			}
			
			if(nfs.getIncidecsll() != null && nfs.getIncidecsll() && nfs.getValorCsll() != null){
				double valorCsll = nfs.getValorCsll().getValue().doubleValue();
				valores.setValorCsll(valorCsll);
			}
			
			if(nfs.getIncideiss() == null || !nfs.getIncideiss()){
				valores.setIssRetido(false);
				if(webservice.equals("ginfesiss") && 
						!prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) &&
						!prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) &&
						nfs.getRegimetributacao() != null && 
						(nfs.getRegimetributacao().equals(Regimetributacao.ME_EPP_SIMPLES_NACIONAL) || nfs.getRegimetributacao().equals(Regimetributacao.MEI_SIMPLES_NACIONAL))) {
					
					if((!prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) &&
						!prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD)) || !Codigoregimetributario.NORMAL.equals(empresa.getCrt())){
						valores.setValorIss(0.0);
					}
				}
			} else {
				valores.setIssRetido(true);
				if(valorIss != null) valores.setValorIssRetido(valorIss.getValue().doubleValue());
				if(webservice.equals("webiss") && 
						(prefixowebservice.equals(Prefixowebservice.BARBACENAISS_PROD) ||
						 prefixowebservice.equals(Prefixowebservice.BARBACENAISS_HOM) ||
						 prefixowebservice.equals(Prefixowebservice.UBERABA_PROD) ||
						 prefixowebservice.equals(Prefixowebservice.UBERABA_HOM))) {
					valores.setValorIss(0.0);
				}
			}
			
//			if((configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) ||
//					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD)) &&
//					nfs.getNaturezaoperacao() != null && nfs.getNaturezaoperacao().equals(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO)){
//				valores.setIssRetido(true);
//				if(valorIss != null) valores.setValorIss(valorIss.getValue().doubleValue());
//			}
			
			if(nfs.getItemlistaservicoBean() != null) servico.setItemListaServico(nfs.getItemlistaservicoBean().getCodigo());

			if(nfs.getCodigocnaeBean() != null) servico.setCodigoCnae(StringUtils.soNumero(nfs.getCodigocnaeBean().getCnae()));
			
			if(webservice.equals("ginfesiss") || webservice.equals("flyenota") || webservice.equals("manaus")) {
				if(nfs.getItemlistaservicoBean() != null) servico.setItemListaServico(StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo()));
				if(nfs.getCodigotributacao() != null){
					if(webservice.equals("manaus")){
						servico.setCodigoTributacaoMunicipio(StringUtils.soNumero(nfs.getCodigotributacao().getCodigo()));
					}else {
						servico.setCodigoTributacaoMunicipio(nfs.getCodigotributacao().getCodigo());
					}
				}
			} else {
				if(webservice.equals("webiss") || webservice.equals("recifeiss")){
					if(nfs.getItemlistaservicoBean() != null) {
						String codigo = StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo());
						if(codigo.length() == 3) codigo = "0" + codigo;
						servico.setItemListaServico(codigo);
					}
				} else if(webservice.equals("aparecidaiss")){
					if(nfs.getItemlistaservicoBean() != null) {
						String codigo = StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo());
						servico.setItemListaServico(codigo);
					}
				} else {
					if(nfs.getItemlistaservicoBean() != null) servico.setItemListaServico(nfs.getItemlistaservicoBean().getCodigo());
				}
				
				if(webservice.equals("memory")){
					if(nfs.getCodigotributacao() != null) servico.setCodigoTributacaoMunicipio(nfs.getCodigotributacao().getCodigo());
				} else {
					if(nfs.getCodigotributacao() != null) servico.setCodigoTributacaoMunicipio(StringUtils.soNumero(nfs.getCodigotributacao().getCodigo()));
				}
			}
			
			if(webservice.equals("aparecidaiss") && !ambienteProducao){
				servico.setCodigoMunicipio("999");
			} else {
				servico.setCodigoMunicipio(nfs.getMunicipioissqn().getCdibge());
				if(webservice.equals("tecnosistemas")){
					servico.setCodigoPais("1058");
					if(nfs.getMunicipioissqn() != null) servico.setMunicipioIncidencia(nfs.getMunicipioissqn().getCdibge());
				}
			}
			
			discriminacao = new StringBuilder();
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
			}
			if(concatenarAnotacaocorpo && nfs.getInfocomplementar() != null && !nfs.getInfocomplementar().trim().equals("")){
				discriminacao.append("- ").append(nfs.getInfocomplementar());
			}
			
			if ((Prefixowebservice.UBERABA_HOM.equals(prefixowebservice) || Prefixowebservice.UBERABA_PROD.equals(prefixowebservice)) && 
					org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getDadosAdicionais())) {
				discriminacao.append("- ").append(nfs.getDadosAdicionais());
			}
			
			strDiscriminacao = discriminacao.toString();
			servico.setDiscriminacao(strDiscriminacao.length() > 2000 ? addScape(strDiscriminacao.substring(0, 2000)) : addScape(strDiscriminacao));
			
			if(webservice.equals("simpliss")){
				List<Itensservico> listaItensservico = new ArrayList<Itensservico>();
				
				for (NotaFiscalServicoItem it : nfs.getListaItens()) {
					Itensservico itensservico = new Itensservico();
					
					if (it.getDescricao() != null && !it.getDescricao().equals("")) {
						itensservico.setDescricao(it.getDescricao());
					} else {
						if (it.getMaterial() != null && it.getMaterial().getNome() != null && !it.getMaterial().getNome().equals("")) {
							itensservico.setDescricao(it.getMaterial().getNome());
						}
					}
					
					itensservico.setQuantidade(it.getQtde());
					itensservico.setValorUnitario(it.getPrecoUnitario());
					listaItensservico.add(itensservico);
				}
				
				servico.setListaItensservico(listaItensservico);
			}
			
			prestador = new Prestador(webservice, true, false, false);
			infRps.setPrestador(prestador);
			
			prestador.setCnpj(cnpj);
			prestador.setInscricaoMunicipal(inscricaoMunicipal);
			
			
			tomador = new Tomador(webservice);
			infRps.setTomador(tomador);
			
			if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
				tomador.setRazaoSocial(addScape(nfs.getNomefantasiaCliente()));
			}else if(nfs.getCliente().getRazaosocial() != null && !nfs.getCliente().getRazaosocial().trim().equals(""))
				tomador.setRazaoSocial(addScape(nfs.getCliente().getRazaosocial()));
			else 
				tomador.setRazaoSocial(addScape(nfs.getCliente().getNome().trim()));
			
			identificacaoTomador = new IdentificacaoTomador(webservice);
			tomador.setIdentificacaoTomador(identificacaoTomador);
			
			cpfCnpj = new CpfCnpj(webservice);
			identificacaoTomador.setCpfCnpj(cpfCnpj);
			
			if(nfs.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				if(nfs.getCliente().getCpf() == null) throw new SinedException("Cadastrar o CPF do cliente " + nfs.getCliente().getNome());
				cpfCnpj.setCpf(StringUtils.soNumero(nfs.getCliente().getCpf().getValue()));
			} else {
				if(nfs.getCliente().getCnpj() == null) throw new SinedException("Cadastrar o CNPJ do cliente " + nfs.getCliente().getNome());
				cpfCnpj.setCnpj(StringUtils.soNumero(nfs.getCliente().getCnpj().getValue()));
				if(nfs.getInscricaomunicipal() != null)
					identificacaoTomador.setInscricaoMunicipal(!nfs.getInscricaomunicipal().trim().equals("") ? nfs.getInscricaomunicipal() : null);
				else
					identificacaoTomador.setInscricaoMunicipal(nfs.getCliente().getInscricaomunicipal());
			}
			
			enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());

			String erroEnderecoCliente = "";
			
			if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) erroEnderecoCliente += "Logradouro do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")) erroEnderecoCliente += "Bairro do cliente " + nfs.getCliente().getNome() + " não informadoo.<BR>";
			if(enderecoCliente.getCep() == null || StringUtils.soNumero(enderecoCliente.getCep().getValue()).equals("")) erroEnderecoCliente += "CEP do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")) erroEnderecoCliente += "Número do endereço do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getMunicipio() == null) erroEnderecoCliente += "Município do cliente " + nfs.getCliente().getNome() + " não informado.<BR>";
			
			if(!erroEnderecoCliente.equals("")){
				throw new SinedException(erroEnderecoCliente);
			}
			
			endereco = new Endereco(webservice);
			tomador.setEndereco(endereco);
			
			endereco.setEndereco(addScape(enderecoCliente.getLogradouro()));
			endereco.setNumero(enderecoCliente.getNumero());
			endereco.setComplemento(addScape(enderecoCliente.getComplemento()));
			endereco.setBairro(addScape(enderecoCliente.getBairro()));
			endereco.setCodigoMunicipio(enderecoCliente.getMunicipio().getCdibge());
			endereco.setUf(enderecoCliente.getMunicipio().getUf().getSigla());
			endereco.setCep(StringUtils.soNumero(enderecoCliente.getCep().getValue()));
			if(webservice.equals("tecnosistemas")){
				endereco.setCodigoPais("1058");
			}
			String emailcliente = nfs.getCliente().getEmail();
			String telefonecliente = null;

			if (nfs.getTelefoneCliente() != null && !"".equals(nfs.getTelefoneCliente())) {
				telefonecliente = StringUtils.soNumero(nfs.getTelefoneCliente());
			} else if (nfs.getCliente().getListaTelefone() != null && nfs.getCliente().getListaTelefone().size() > 0) {
				telefonecliente = StringUtils.soNumero(nfs.getCliente().getListaTelefone().iterator().next().getTelefone());
			}

			if ((emailcliente != null && !emailcliente.equals("")) || (telefonecliente != null && !telefonecliente.equals(""))) {
				Contato contato = new Contato(webservice);

				if (emailcliente != null && !emailcliente.equals(""))
					contato.setEmail(addScape(emailcliente));

				if (telefonecliente != null && !telefonecliente.equals("")) {
					if(webservice.equals("simpliss") || webservice.equals("fisslex")){
						contato.setTelefone(telefonecliente);
					}else if(webservice.equals("ginfesiss") || webservice.equals("bhiss") || webservice.equals("memory") || webservice.equals("salvadoriss") || webservice.equals("eireli")) {
						if (telefonecliente.length() > 11){
							throw new SinedException("O campo telefone do cliente " + nfs.getCliente().getNome() +  " não pode ter mais que 11 dígitos.");
						}
						contato.setTelefone(telefonecliente);
					} else {
						if (telefonecliente.length() == 10 || (telefonecliente.length() == 11 && telefonecliente.startsWith("0")) || 
								(telefonecliente.length() == 12 && telefonecliente.startsWith("0")) || (webservice.equals("webiss") && telefonecliente.length() == 11) ) {
							if ("curitibaiss".equals(webservice) && telefonecliente.startsWith("0")) {
								contato.setTelefone(telefonecliente.substring(1, telefonecliente.length() - 1));
							} else {
								contato.setTelefone(telefonecliente);
							}
						} else {
							throw new SinedException("O campo telefone do cliente " + nfs.getCliente().getNome() +  " deve conter dez dígitos numéricos sem espacos ou 11 dígitos numéricos com o primeiro dígito igual a zero.");
						}
					}
				}

				tomador.setContato(contato);
			}		
			
			
			if(nfs.getProjeto() != null && nfs.getProjeto().getArt() != null && !"".equals(nfs.getProjeto().getArt()) && nfs.getProjeto().getCei() != null && !"".equals(nfs.getProjeto().getCei())){
				ConstrucaoCivil construcaoCivil = new ConstrucaoCivil();
				infRps.setConstrucaoCivil(construcaoCivil);
				
				construcaoCivil.setCodigoObra(StringUtils.soNumero(nfs.getProjeto().getCei()));
				construcaoCivil.setArt(StringUtils.soNumero(nfs.getProjeto().getArt()));
			}
			
			lista.add(rps);
		}
		
		return enviarLoteRpsEnvio;
	}
	
	public String createXmlEnviarNfseGoverna(List<NotaFiscalServico> listaNota, Empresa empresa, Configuracaonfe configuracaonfe, String numeroNfse) {
		Integer quantidadeRps = 0;
		Integer numSeq = 0;
		
		GovernaNfeLista governaNfeLista = new GovernaNfeLista();
		
		GovernaInfEmp governaInfEmp = new GovernaInfEmp();
		
		numSeq++;
		governaInfEmp.setNumSeq(numSeq.toString());
		governaInfEmp.setCodCadBic(configuracaonfe.getInscricaomunicipal());
		governaInfEmp.setVrsLeiArq("5");
		
		governaNfeLista.setGovernaInfEmp(governaInfEmp);
		
		for (NotaFiscalServico nfs : listaNota) {
			quantidadeRps++;
			
			GovernaNfe governaNfe = new GovernaNfe();
			
			GovernaIndRps governaIndRps = new GovernaIndRps();
			
			numSeq++;
			governaIndRps.setNumSeq(numSeq.toString());
			governaIndRps.setVrsLeiImp("5");
			governaIndRps.setNumNot(org.apache.commons.lang.StringUtils.isNotEmpty(numeroNfse) ? numeroNfse : "0");
			governaIndRps.setNumRps(org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getNumero()) ? StringUtils.soNumero(nfs.getNumero()) : "0");
			governaIndRps.setCodVer(" ");
			governaIndRps.setMunSvc(nfs.getMunicipioissqn() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getMunicipioissqn().getNome()) ? nfs.getMunicipioissqn().getNome() : " ");
			governaIndRps.setuFMunSvc(nfs.getMunicipioissqn() != null && nfs.getMunicipioissqn().getUf() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getMunicipioissqn().getUf().getSigla()) ? nfs.getMunicipioissqn().getUf().getSigla().toString() : " ");
			governaIndRps.setNumNotSbt("0");
			governaIndRps.setMesCpt(nfs.getDtEmissao() != null ? String.valueOf(SinedDateUtils.getMes(nfs.getDtEmissao()) + 1) : "0");
			governaIndRps.setAnoCpt(nfs.getDtEmissao() != null ? String.valueOf(SinedDateUtils.getAno(nfs.getDtEmissao())) : "0");
			governaIndRps.setDatEmsRps(nfs.getDtEmissao());
			governaIndRps.setNatOpe(nfs.getNaturezaoperacao() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getNaturezaoperacao().getCodigonfse()) ? nfs.getNaturezaoperacao().getCodigonfse() : "0");
			
			governaIndRps.setNomTmd(nfs.getCliente() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getCliente().getRazaoOrNomeByTipopessoa()) ? nfs.getCliente().getRazaoOrNomeByTipopessoa() : " ");
			governaIndRps.setNumDocTmd(nfs.getCliente() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getCliente().getCpfOrCnpjByTipopessoa()) ? nfs.getCliente().getCpfOrCnpjByTipopessoa() : " ");
			governaIndRps.setInsEstTmd(nfs.getCliente() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getCliente().getInscricaoestadual()) ? nfs.getCliente().getInscricaoestadual() : " ");
			governaIndRps.setInsMunTmd(nfs.getCliente() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getCliente().getInscricaomunicipal()) ? nfs.getCliente().getInscricaomunicipal() : " ");
			
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());
			
			governaIndRps.setDesEndTmd(enderecoCliente != null && org.apache.commons.lang.StringUtils.isNotEmpty(enderecoCliente.getLogradouro()) ? enderecoCliente.getLogradouro() : " ");
			governaIndRps.setNomBaiTmd(enderecoCliente != null && org.apache.commons.lang.StringUtils.isNotEmpty(enderecoCliente.getBairro()) ? enderecoCliente.getBairro() : " ");
			governaIndRps.setNomMunTmd(enderecoCliente != null && enderecoCliente.getMunicipio() != null && org.apache.commons.lang.StringUtils.isNotEmpty(enderecoCliente.getMunicipio().getNome()) ? enderecoCliente.getMunicipio().getNome() : " ");
			governaIndRps.setuFMunTmd(enderecoCliente != null && enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null && org.apache.commons.lang.StringUtils.isNotEmpty(enderecoCliente.getMunicipio().getUf().getSigla()) ? enderecoCliente.getMunicipio().getUf().getSigla() : " ");
			governaIndRps.setcEPTmd(enderecoCliente != null && enderecoCliente.getCep() != null && org.apache.commons.lang.StringUtils.isNotEmpty(enderecoCliente.getCep().getValue()) ? enderecoCliente.getCep().getValue() : "0");
			
			governaIndRps.setEmlTmd(nfs.getCliente() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getCliente().getEmail()) ? nfs.getCliente().getEmail() : " ");
			
			Money desconto = nfs.getDescontocondicionado() != null ? nfs.getDescontocondicionado() : new Money(0);
			desconto = nfs.getDescontoincondicionado() != null ? desconto.add(nfs.getDescontoincondicionado()) : desconto;
			
			governaIndRps.setVlrDsc(desconto != null ? StringUtils.soNumero(desconto.toString()) : "0");
			governaIndRps.setVlrDed(nfs.getDeducao() != null ? StringUtils.soNumero(nfs.getDeducao().toString()) : "0");
			governaIndRps.setCodLstSvc(nfs.getItemlistaservicoBean() != null && org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getItemlistaservicoBean().getCodigo()) ? StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo()) : "0");
			governaIndRps.setPerAlq(nfs.getIss() != null ? StringUtils.soNumero(LkNfeUtil.impressaoValorDuasCasasPontoNullZero(nfs.getIss()).toString()) : "0");
			governaIndRps.setiSSRtn(nfs.getIncideiss() != null && nfs.getIncideiss() ? "S" : "N");
			governaIndRps.setVlrPIS(nfs.getValorPis() != null ? StringUtils.soNumero(nfs.getValorPis().toString()) : "0");
			governaIndRps.setVlrCOFINS(nfs.getValorCofins() != null ? StringUtils.soNumero(nfs.getValorCofins().toString()) : "0");
			governaIndRps.setVlrIR(nfs.getValorIr() != null ? StringUtils.soNumero(nfs.getValorIr().toString()) : "0");
			governaIndRps.setVlrCSLL(nfs.getValorCsll() != null ? StringUtils.soNumero(nfs.getValorCsll().toString()) : "0");
			governaIndRps.setVlrINSS(nfs.getValorInss() != null ? StringUtils.soNumero(nfs.getValorInss().toString()) : "0");
			
			String observacao = org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getDadosAdicionais()) ? nfs.getDadosAdicionais() : "";
			observacao += org.apache.commons.lang.StringUtils.isNotEmpty(observacao) ? "" : " ";
			observacao += org.apache.commons.lang.StringUtils.isNotEmpty(nfs.getInfocomplementar()) ? nfs.getInfocomplementar() : "";
			
			governaIndRps.setObs(observacao);
			
			governaNfe.setGovernaIndRps(governaIndRps);
			
			GovernaIndItemServicoLista governaListaIndItemServico = new GovernaIndItemServicoLista();
			
			if (nfs.getListaItens() != null && nfs.getListaItens().size() > 0) {
				Integer seqItem = 0;
				
				for (NotaFiscalServicoItem nfsi : nfs.getListaItens()) {
					GovernaIndItemServico governaIndItemServico = new GovernaIndItemServico();
					
					numSeq++;
					governaIndItemServico.setNumSeq(numSeq.toString());
					
					seqItem++;
					governaIndItemServico.setSeqItem(seqItem.toString());
					
					String strDiscriminacao = this.preencheDescricaoItemNfs(nfsi, configuracaonfe);
					
					if (strDiscriminacao.length() > 1) {
						strDiscriminacao = strDiscriminacao.substring(0, strDiscriminacao.length() - 1);
					} else {
						strDiscriminacao = " ";
					}
					
					governaIndItemServico.setDisSvc(strDiscriminacao);
					governaIndItemServico.setQdeSvc(nfsi.getQtde() != null ? StringUtils.soNumero(((Integer) nfsi.getQtde().intValue()).toString()) : "0");
					governaIndItemServico.setVlrUnt(nfsi.getPrecoUnitario() != null ? StringUtils.soNumero(LkNfeUtil.impressaoValorDuasCasasPontoNullZero(nfsi.getPrecoUnitario()).toString()) : "0");
					
					governaListaIndItemServico.addGovernaIndItemServico(governaIndItemServico);
				}
			}
			
			governaNfe.setGovernaListaIndItemServico(governaListaIndItemServico);
			
			governaNfeLista.addGovernaNfe(governaNfe);
		}
		
		GovernaTotalizacaoArquivo governaTotalizacaoArquivo = new GovernaTotalizacaoArquivo();
		
		numSeq++;
		governaTotalizacaoArquivo.setNumSeq(numSeq.toString());
		governaTotalizacaoArquivo.setNumRPS2(quantidadeRps.toString());
		
		governaNfeLista.setGovernaTotalizacaoArquivo(governaTotalizacaoArquivo);
		
		return governaNfeLista.toString();
	}
	
	public String createXmlCancelamentoNfseGoverna(Arquivonfnota arquivonfnota, Empresa empresa, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNotaFiscalServico = notaFiscalServicoService.findForNFe(arquivonfnota.getNota().getCdNota().toString());
		
		return this.createXmlEnviarNfseGoverna(listaNotaFiscalServico, empresa, configuracaonfe, arquivonfnota.getNumeronfse());
	}
	
	public String addScape(String string){
		if(string == null) return null;
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			switch (string.charAt(i)) {
			case '"' :
//				stringBuilder.append("&quot;");
				break;
			case '\'' :
//				stringBuilder.append("&#39;");
				break;
			case '<' :
//				stringBuilder.append("&lt;");
				break;
			case '>' :
//				stringBuilder.append("&gt;");
				break;
			case '&' :
				stringBuilder.append("&amp;");
				break;
			default :
				stringBuilder.append(string.charAt(i));
			}
		}
		
		return stringBuilder.toString();
	}
	
	private LoteRPS createXmlEnviarLoteEtransparencia(String in, Empresa empresa, Configuracaonfe configuracaonfe){
		String codigoContribuinte = configuracaonfe.getLoginparacatu();
		String codigoUsuario = configuracaonfe.getSenhaparacatu();
		if(org.apache.commons.lang.StringUtils.isBlank(codigoContribuinte) || org.apache.commons.lang.StringUtils.isBlank(codigoUsuario)){
			throw new SinedException("O código do contribuinte e o código do usuário são obrigatórios no cadastro da configuraração de NFS-e.");
		}
		
		TipoTributacaoIssETransparencia tipotributacaoissetransparencia = configuracaonfe.getTipotributacaoissetransparencia();
		if(tipotributacaoissetransparencia == null){
			throw new SinedException("O tipo de tributação é obrigatório no cadastro da configuraração de NFS-e.");
		}
		
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		
		java.sql.Date dTIni = null;
		java.sql.Date dTFin = null;
		for (NotaFiscalServico nf : listaNota) {
			java.sql.Date dtEmissao = nf.getDtEmissao();
			if(dTIni == null || dTIni.getTime() > dtEmissao.getTime()) dTIni = dtEmissao;
			if(dTFin == null || dTFin.getTime() < dtEmissao.getTime()) dTFin = dtEmissao;
			
			if(nf.getItemlistaservicoBean() == null){
				throw new SinedException("Item da lista de serviço obrigatório.");
			}
		}
		
		LoteRPS loteRPS = new LoteRPS();
		Registro10 registro10 = new Registro10();
		loteRPS.setRegistro10(registro10);
		
		Ws_nfePROCESSARPS ws_nfePROCESSARPS = new Ws_nfePROCESSARPS();
		
		Sdt_processarpsin sdt_processarpsin = new Sdt_processarpsin();
		ws_nfePROCESSARPS.setSdt_processarpsin(sdt_processarpsin);
		
		Login login = new Login();
		SDTRPS sdtrps = new SDTRPS();
		sdt_processarpsin.setLogin(login);
		sdt_processarpsin.setSdtrps(sdtrps);
		
		login.setCodigoContribuinte(codigoContribuinte);
		login.setCodigoUsuario(codigoUsuario);
		
		java.sql.Date currentDate = SinedDateUtils.currentDate();
		Integer ano = SinedDateUtils.getAno(currentDate);
		Integer mes = SinedDateUtils.getMes(currentDate) + 1;
		
		sdtrps.setAno(ano);
		sdtrps.setMes(mes);
		sdtrps.setcPFCNPJ(empresa.getCnpj().getValue());
		sdtrps.setdTIni(dTIni);
		sdtrps.setdTFin(dTFin);
		sdtrps.setTipoTrib(tipotributacaoissetransparencia.getValue());
		sdtrps.setDtAdesSN(configuracaonfe.getDtadesaosimples());
		sdtrps.setAlqIssSN_IP(configuracaonfe.getAliqiss_snip());
		sdtrps.setVersao("2.00");
		
		registro10.setCpfcnpjprestador(empresa.getCnpj().getValue());
		registro10.setDtinicio(dTIni);
		registro10.setDtfim(dTFin);
		registro10.setTipotributacao(tipotributacaoissetransparencia.getValue());
		registro10.setDtadesaosimples(configuracaonfe.getDtadesaosimples());
		registro10.setAliquotasimples(configuracaonfe.getAliqiss_snip());
		registro10.setVersao("2.00");
		
		
		Integer qtdReg20Item = 0;
		Integer qtdReg30 = 0;
		
		Double totalValorNFS = 0d;
		Double totalValorISS = 0d;
		Double totalValorDed = 0d;
		Double totalValorIssRetTom = 0d;
		Double totalValorTributos = 0d;
		
		Reg20 reg20 = new Reg20();
		sdtrps.setReg20(reg20);
		
		for (NotaFiscalServico nf : listaNota) {
			Registro20 registro20 = new Registro20();
			Reg20Item reg20Item = new Reg20Item();
			
			reg20Item.setTipoNFS("RPS");
			reg20Item.setNumRps(nf.getNumero());
			reg20Item.setSerRps(configuracaonfe.getSerienfse());
			reg20Item.setDtEmi(nf.getDtEmissao());
			
			registro20.setTipoNFS("RPS");
			registro20.setNumRps(nf.getNumero());
			registro20.setSerRps(configuracaonfe.getSerienfse());
			registro20.setDtEmi(nf.getDtEmissao());
			
			boolean incideIss = nf.getIncideiss() != null && nf.getIncideiss();
			reg20Item.setRetFonte(incideIss);
			registro20.setRetFonte(incideIss);
			
			Itemlistaservico itemlistaservicoBean = nf.getItemlistaservicoBean();
			reg20Item.setCodSrv(itemlistaservicoBean.getCodigo());
			registro20.setCodSrv(itemlistaservicoBean.getCodigo());
			
			StringBuilder descricaoServ = new StringBuilder(); 
			List<NotaFiscalServicoItem> listaItens = nf.getListaItens();
			if(listaItens != null && listaItens.size() > 0){
				for (NotaFiscalServicoItem it : listaItens) {
					descricaoServ.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
				}
			}
			String descricaoServString = descricaoServ.toString().trim();
			reg20Item.setDiscrSrv(descricaoServString.length() > 4000 ? "Serviços diversos" : descricaoServString);
			registro20.setDiscrSrv(descricaoServString.length() > 4000 ? "Serviços diversos" : descricaoServString);
			
			Double valorNota = nf.getValorNota().getValue().doubleValue();
			totalValorNFS += valorNota;
					
			reg20Item.setVlNFS(valorNota);
			reg20Item.setVlDed(null);
			reg20Item.setDiscrDed(null);
			reg20Item.setVlBasCalc(nf.getBasecalculoiss() != null ? nf.getBasecalculoiss().getValue().doubleValue() : (nf.getBasecalculo() != null ? nf.getBasecalculo().getValue().doubleValue() : 0d));
			
			registro20.setVlNFS(valorNota);
			registro20.setVlDed(null);
			registro20.setDiscrDed(null);
			registro20.setVlBasCalc(nf.getBasecalculoiss() != null ? nf.getBasecalculoiss().getValue().doubleValue() : (nf.getBasecalculo() != null ? nf.getBasecalculo().getValue().doubleValue() : 0d));
			
			Double valorIss = nf.getValoriss().getValue().doubleValue();
			Double iss = nf.getIss();
			
			totalValorISS += valorIss;
			totalValorIssRetTom += incideIss ? valorIss : 0d;
			
			reg20Item.setAlqIss(iss);
			reg20Item.setVlIss(valorIss);
			reg20Item.setVlIssRet(incideIss ? valorIss : 0d);
			
			registro20.setAlqIss(iss);
			registro20.setVlIss(valorIss);
			registro20.setVlIssRet(incideIss ? valorIss : 0d);
			
			if(nf.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				if(nf.getCliente().getCpf() == null) throw new SinedException("Cadastrar o CPF do cliente " + nf.getCliente().getNome());
				reg20Item.setCpfCnpTom(StringUtils.soNumero(nf.getCliente().getCpf().getValue()));
				registro20.setCpfCnpTom(StringUtils.soNumero(nf.getCliente().getCpf().getValue()));
			} else {
				if(nf.getCliente().getCnpj() == null) throw new SinedException("Cadastrar o CNPJ do cliente " + nf.getCliente().getNome());
				
				reg20Item.setCpfCnpTom(StringUtils.soNumero(nf.getCliente().getCnpj().getValue()));
				registro20.setCpfCnpTom(StringUtils.soNumero(nf.getCliente().getCnpj().getValue()));
				if(nf.getInscricaomunicipal() != null){
					reg20Item.setInscricaoMunicipal(!nf.getInscricaomunicipal().trim().equals("") ? nf.getInscricaomunicipal() : null);
					registro20.setInscricaoMunicipal(!nf.getInscricaomunicipal().trim().equals("") ? nf.getInscricaomunicipal() : null);
				} else {
					reg20Item.setInscricaoMunicipal(nf.getCliente().getInscricaomunicipal());
					registro20.setInscricaoMunicipal(nf.getCliente().getInscricaomunicipal());
				}
			}
			
			if(nf.getNomefantasiaCliente() != null && !"".equals(nf.getNomefantasiaCliente())){
				reg20Item.setRazSocTom(addScape(nf.getNomefantasiaCliente()));
				registro20.setRazSocTom(addScape(nf.getNomefantasiaCliente()));
			}else if(nf.getCliente().getRazaosocial() != null && !nf.getCliente().getRazaosocial().trim().equals("")){
				reg20Item.setRazSocTom(addScape(nf.getCliente().getRazaosocial()));
				registro20.setRazSocTom(addScape(nf.getCliente().getRazaosocial()));
			}else{ 
				reg20Item.setRazSocTom(addScape(nf.getCliente().getNome().trim()));
				registro20.setRazSocTom(addScape(nf.getCliente().getNome().trim()));
			}
			
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
			
			String erroEnderecoCliente = "";

			if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) erroEnderecoCliente += "Logradouro do cliente " + nf.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")) erroEnderecoCliente += "Bairro do cliente " + nf.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getCep() == null || StringUtils.soNumero(enderecoCliente.getCep().getValue()).equals("")) erroEnderecoCliente += "CEP do cliente " + nf.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")) erroEnderecoCliente += "Número do endereço do cliente " + nf.getCliente().getNome() + " não informado.<BR>";
			if(enderecoCliente.getMunicipio() == null) erroEnderecoCliente += "Município do cliente " + nf.getCliente().getNome() + " não informado.<BR>";
			
			if(!erroEnderecoCliente.equals("")){
				throw new SinedException(erroEnderecoCliente);
			}
			
			reg20Item.setTipoLogtom(enderecoCliente.getLogradouro().split(" ")[0]);
			reg20Item.setLogTom(addScape(enderecoCliente.getLogradouro()));
			reg20Item.setNumEndTom(enderecoCliente.getNumero());
			reg20Item.setComplEndTom(addScape(enderecoCliente.getComplemento()));
			reg20Item.setBairroTom(addScape(enderecoCliente.getBairro()));
			reg20Item.setMunTom(enderecoCliente.getMunicipio().getNome());
			reg20Item.setSiglaUFTom(enderecoCliente.getMunicipio().getUf().getSigla());
			reg20Item.setCepTom(StringUtils.soNumero(enderecoCliente.getCep().getValue()));
			
			registro20.setTipoLogtom(enderecoCliente.getLogradouro().split(" ")[0]);
			registro20.setLogTom(addScape(enderecoCliente.getLogradouro()));
			registro20.setNumEndTom(enderecoCliente.getNumero());
			registro20.setComplEndTom(addScape(enderecoCliente.getComplemento()));
			registro20.setBairroTom(addScape(enderecoCliente.getBairro()));
			registro20.setMunTom(enderecoCliente.getMunicipio().getNome());
			registro20.setSiglaUFTom(enderecoCliente.getMunicipio().getUf().getSigla());
			registro20.setCepTom(StringUtils.soNumero(enderecoCliente.getCep().getValue()));
			
			String emailcliente = nf.getCliente().getEmail();
			String telefonecliente = null;

			if (nf.getTelefoneCliente() != null && !"".equals(nf.getTelefoneCliente())) {
				telefonecliente = StringUtils.soNumero(nf.getTelefoneCliente());
			} else if (nf.getCliente().getListaTelefone() != null && nf.getCliente().getListaTelefone().size() > 0) {
				telefonecliente = StringUtils.soNumero(nf.getCliente().getListaTelefone().iterator().next().getTelefone());
			}
			
			reg20Item.setTelefone(telefonecliente);
			reg20Item.setEmail1(emailcliente);
//			reg20Item.setEmail2(email2);
//			reg20Item.setEmail3(email3);
			
			registro20.setTelefone(telefonecliente);
			registro20.setEmail1(emailcliente);
//			registro20.setEmail2(email2);
//			registro20.setEmail3(email3);
			
			br.com.linkcom.sined.geral.bean.Endereco endereco = enderecoService.loadEndereco(configuracaonfe.getEndereco());
			
			reg20Item.setTipoLogLocPre(endereco.getLogradouro().split(" ")[0]);
			reg20Item.setLogLocPre(addScape(endereco.getLogradouro()));
			reg20Item.setNumEndLocPre(endereco.getNumero());
			reg20Item.setComplEndLocPre(addScape(endereco.getComplemento()));
			reg20Item.setBairroLocPre(addScape(endereco.getBairro()));
			reg20Item.setMunLocPre(endereco.getMunicipio().getNome());
			reg20Item.setSiglaUFLocpre(endereco.getMunicipio().getUf().getSigla());
			reg20Item.setCepLocPre(StringUtils.soNumero(endereco.getCep().getValue()));
			
			registro20.setTipoLogLocPre(endereco.getLogradouro().split(" ")[0]);
			registro20.setLogLocPre(addScape(endereco.getLogradouro()));
			registro20.setNumEndLocPre(endereco.getNumero());
			registro20.setComplEndLocPre(addScape(endereco.getComplemento()));
			registro20.setBairroLocPre(addScape(endereco.getBairro()));
			registro20.setMunLocPre(endereco.getMunicipio().getNome());
			registro20.setSiglaUFLocpre(endereco.getMunicipio().getUf().getSigla());
			registro20.setCepLocPre(StringUtils.soNumero(endereco.getCep().getValue()));

			boolean incidecofins = nf.getIncidecofins() != null && nf.getIncidecofins(); 
			boolean incidecsll = nf.getIncidecsll() != null && nf.getIncidecsll(); 
			boolean incideinss = nf.getIncideinss() != null && nf.getIncideinss(); 
			boolean incideir = nf.getIncideir() != null && nf.getIncideir(); 
			boolean incideiss = nf.getIncideiss() != null && nf.getIncideiss(); 
			boolean incidepis = nf.getIncidepis() != null && nf.getIncidepis(); 
			
			Reg30 reg30 = new Reg30();
			reg20Item.setReg30(reg30);
			
			if(incidecofins || incidecsll || incideinss || incideir || incideiss || incidepis){
				
				if(incidecofins){
					Double valorCofins = nf.getValorCofins().getValue().doubleValue();
					totalValorTributos += valorCofins;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("COFINS");
					reg30Item.setTributoAliquota(nf.getCofins());
					reg30Item.setTributoValor(valorCofins);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("COFINS");
					registro30.setTributoAliquota(nf.getCofins());
					registro30.setTributoValor(valorCofins);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
				if(incidecsll){
					Double valorCsll = nf.getValorCsll().getValue().doubleValue();
					totalValorTributos += valorCsll;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("CSLL");
					reg30Item.setTributoAliquota(nf.getCsll());
					reg30Item.setTributoValor(valorCsll);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("CSLL");
					registro30.setTributoAliquota(nf.getCsll());
					registro30.setTributoValor(valorCsll);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
				if(incideinss){
					Double valorInss = nf.getValorInss().getValue().doubleValue();
					totalValorTributos += valorInss;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("INSS");
					reg30Item.setTributoAliquota(nf.getInss());
					reg30Item.setTributoValor(valorInss);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("INSS");
					registro30.setTributoAliquota(nf.getInss());
					registro30.setTributoValor(valorInss);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
				if(incideir){
					Double valorIr = nf.getValorIr().getValue().doubleValue();
					totalValorTributos += valorIr;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("IR");
					reg30Item.setTributoAliquota(nf.getIr());
					reg30Item.setTributoValor(valorIr);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("IR");
					registro30.setTributoAliquota(nf.getIr());
					registro30.setTributoValor(valorIr);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
				if(incideiss){
					totalValorTributos += valorIss;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("ISS");
					reg30Item.setTributoAliquota(iss);
					reg30Item.setTributoValor(valorIss);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("ISS");
					registro30.setTributoAliquota(iss);
					registro30.setTributoValor(valorIss);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
				if(incidepis){
					Double valorPis = nf.getValorPis().getValue().doubleValue();
					totalValorTributos += valorPis;
					
					Reg30Item reg30Item = new Reg30Item();
					reg30Item.setTributoSigla("PIS");
					reg30Item.setTributoAliquota(nf.getPis());
					reg30Item.setTributoValor(valorPis);
					reg30.addReg30Item(reg30Item);
					
					Registro30 registro30 = new Registro30();
					registro30.setTributoSigla("PIS");
					registro30.setTributoAliquota(nf.getPis());
					registro30.setTributoValor(valorPis);
					registro20.addRegistro30(registro30);
					
					qtdReg30++;
				}
				
			}
			
			loteRPS.addRegistro20(registro20);
			reg20.addReg20Item(reg20Item);
			qtdReg20Item++;
		}
		
		Registro90 registro90 = new Registro90();
		loteRPS.setRegistro90(registro90);
		
		Reg90 reg90 = new Reg90();
		sdtrps.setReg90(reg90);
		
		reg90.setQtdRegNormal(qtdReg20Item);
		reg90.setValorNFS(totalValorNFS);
		reg90.setValorISS(totalValorISS);
		reg90.setValorDed(totalValorDed);
		reg90.setValorIssRetTom(totalValorIssRetTom);
		reg90.setQtdReg30(qtdReg30);
		reg90.setValorTributos(totalValorTributos);
		
		registro90.setQtdRegNormal(qtdReg20Item);
		registro90.setValorNFS(totalValorNFS);
		registro90.setValorISS(totalValorISS);
		registro90.setValorDed(totalValorDed);
		registro90.setValorIssRetTom(totalValorIssRetTom);
		registro90.setQtdReg30(qtdReg30);
		registro90.setValorTributos(totalValorTributos);
		
		return loteRPS;
	}
	
	private LoteNfeletronica createXmlEnviarLoteNfseNfeletronica(String in, Configuracaonfe configuracaonfe) {
		LoteNfeletronica loteNfeletronica = new LoteNfeletronica();
		
		int numeroregistros = 0;
		java.sql.Date datainicial = null;
		java.sql.Date datafinal = null;
		
		Municipio municipioconfiguracao = configuracaonfe.getEndereco().getMunicipio();
		
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		for (NotaFiscalServico nf : listaNota) {
			String referencia = "LK" + nf.getCdNota();
			java.sql.Date dataemissao = nf.getDtEmissao();
			Double valortotalservicos = nf.getValorBruto().getValue().doubleValue();
			Double aliquotaiss = nf.getIss();
			
			Money valorIss = nf.getValorIss();
			Money valorIr = nf.getValorIr();
			Money valorPis = nf.getValorPis();
			Money valorPisRetido = nf.getValorPisRetido();
			Money valorCofins = nf.getValorCofins();
			Money valorCofinsRetido = nf.getValorCofinsRetido();
			Money valorCsll = nf.getValorCsll();
			Money valorInss = nf.getValorInss();
			
			Boolean incideiss = nf.getIncideiss();
			Boolean incideir = nf.getIncideir();
			Boolean incidepis = nf.getIncidepis();
			Boolean incidecofins = nf.getIncidecofins();
			Boolean incidecsll = nf.getIncidecsll();
			Boolean incideinss = nf.getIncideinss();
			
			
			Cliente cliente = nf.getCliente();
			String cpfcnpj = cliente.getCpfOuCnpjValue();
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
			
			if(datainicial == null || SinedDateUtils.beforeIgnoreHour(dataemissao, datainicial)){
				datainicial = dataemissao;
			}
			if(datafinal == null || SinedDateUtils.afterIgnoreHour(dataemissao, datafinal)){
				datafinal = dataemissao;
			}
			
			String codigoservico = null;
			Codigotributacao codigotributacao = nf.getCodigotributacao();
			if(codigotributacao != null){
				codigoservico = codigotributacao.getCodigo();
			}
			
			br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro2 registro2 = new br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro2();
			registro2.setReferencia(referencia);
			registro2.setDataemissao(dataemissao);
			registro2.setLocalprestacaoservico(municipioconfiguracao.equals(nf.getMunicipioissqn()) ? 1 : 2);
			registro2.setValortotalservicos(valortotalservicos);
			registro2.setCodigoservico(codigoservico);
			registro2.setAliquotaiss(aliquotaiss);
			registro2.setValorissretido(incideiss != null && incideiss && valorIss != null ? valorIss.getValue().doubleValue() : 0.0);
			registro2.setValorirrfretido(incideir != null && incideir && valorIr != null ? valorIr.getValue().doubleValue() : 0.0);
			registro2.setValorcsllretido(incidecsll != null && incidecsll && valorCsll != null ? valorCsll.getValue().doubleValue() : 0.0);
			registro2.setValorinssretido(incideinss != null && incideinss && valorInss != null ? valorInss.getValue().doubleValue() : 0.0);
			
			if(valorPisRetido != null && valorPisRetido.getValue().doubleValue() > 0){
				registro2.setValorpisretido(valorPisRetido.getValue().doubleValue());
			} else {
				registro2.setValorpisretido(incidepis != null && incidepis && valorPis != null ? valorPis.getValue().doubleValue() : 0.0);
			}
			
			if(valorCofinsRetido != null && valorCofinsRetido.getValue().doubleValue() > 0){
				registro2.setValorcofinsretido(valorCofinsRetido.getValue().doubleValue());
			} else {
				registro2.setValorcofinsretido(incidecofins != null && incidecofins && valorCofins != null ? valorCofins.getValue().doubleValue() : 0.0);
			}
			
			registro2.setCnpjtomador(cpfcnpj);
			registro2.setDatavencimento(nf.getDtVencimento());
			
			Integer countParcelaVenda = vendapagamentoService.countParcelaVendaByNota(nf);
			registro2.setCodigovencimento(countParcelaVenda > 0 ? countParcelaVenda : 1);
			
			String infocomplementar = nf.getInfocomplementar();
			registro2.setInstrucoespagamento(infocomplementar != null ? infocomplementar.replaceAll("\r?\n", " ") : null);
			
			loteNfeletronica.addRegistro2(registro2); numeroregistros++;
			
			for (NotaFiscalServicoItem it : nf.getListaItens()) {
				Registro3 registro3 = new Registro3();
				registro3.setReferencia(referencia);
				registro3.setQuantidade(it.getQtde());
				registro3.setUnidade("UN");
				registro3.setValorunitario(it.getPrecoUnitario());
				
				String descricao = this.preencheDescricaoItemNfs(it, configuracaonfe);
				descricao = descricao.substring(0, descricao.length()-1);
				
				registro3.setDescricao(descricao != null ? descricao.replaceAll("\r?\n", " ") : null);
				loteNfeletronica.addRegistro3(registro3); numeroregistros++;
			}
			
			Registro4 registro4 = new Registro4();
			registro4.setCpfcnpj(cpfcnpj);
			registro4.setReferencia(referencia);
			registro4.setNometomador(cliente.getNome());
			registro4.setInscricaomunicipal(cliente.getInscricaomunicipal());
			registro4.setInscricaoestadualourg(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) ? cliente.getInscricaoestadual() : cliente.getRg());
			registro4.setEndereco(enderecoCliente.getLogradouroCompletoComBairroSemMunicipio());
			registro4.setMunicipio(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getNome() : null);
			registro4.setUf(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : null);
			registro4.setCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
			registro4.setEmailtomador(cliente.getEmail());
			registro4.setEnderecocobranca(null);
			loteNfeletronica.addRegistro4(registro4); numeroregistros++;
			
		}
		
		br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro1 registro1 = new br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro1();
		registro1.setCcm(configuracaonfe.getInscricaomunicipal());
		registro1.setDatainicial(datainicial);
		registro1.setDatafinal(datafinal);
		loteNfeletronica.setRegistro1(registro1);
		
		br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro9 registro9 = new br.com.linkcom.lknfe.xml.nfse.nfeletronica.Registro9();
		registro9.setNumeroregistros(numeroregistros);
		loteNfeletronica.setRegistro9(registro9);
		
		return loteNfeletronica;
	}
	
	private LoteBrumadinho createXmlEnviarLoteNfseBrumadinho(String in, Configuracaonfe configuracaonfe) {
		LoteBrumadinho loteBrumadinho = new LoteBrumadinho();
		
		Integer numerolinhasdetalhe = 0;
		Double valortotalservicos = 0.0;
		Double valortotalbc = 0.0;
		
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		for (NotaFiscalServico nf : listaNota) {
			Cliente cliente = nf.getCliente();
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
			
			Registro2 registro2 = new Registro2();
			Double valorbc = nf.getBasecalculo().getValue().doubleValue();
			Double valorservicos = nf.getValorBruto().getValue().doubleValue();
			
			valortotalservicos += valorservicos;
			valortotalbc += valorbc;
			numerolinhasdetalhe++;
			
			Itemlistaservico itemlistaservicoBean = nf.getItemlistaservicoBean();
			if(itemlistaservicoBean != null){
				String codigoservico = itemlistaservicoBean.getCodigo();
				codigoservico = StringUtils.soNumero(codigoservico);
				registro2.setCodigoservico(codigoservico);
			}
			registro2.setIdentificadorsistemalegado(nf.getNumero());
			registro2.setValorbc(valorbc);
			registro2.setValorservicos(valorservicos);
			registro2.setSituacaonotafiscal(nf.getSituacaoissbrumadinho() != null ? nf.getSituacaoissbrumadinho().getSimbolo() : null);
			
			StringBuilder discriminacao = new StringBuilder();
			for (NotaFiscalServicoItem it : nf.getListaItens()) {
				discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
			}
			String strDiscriminacao = discriminacao.toString();
			strDiscriminacao = strDiscriminacao.replaceAll("\n", "");
			registro2.setDiscriminacaoservicos(strDiscriminacao.length() > 1000 ? addScape(strDiscriminacao.substring(0, 1000)) : addScape(strDiscriminacao));
			
			if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				registro2.setCpfcnpjtomador(cliente.getCpf().getValue());
			} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
				registro2.setCpfcnpjtomador(cliente.getCnpj().getValue());
			}
			
			if(nf.getNomefantasiaCliente() != null && !"".equals(nf.getNomefantasiaCliente())){
				registro2.setNometomador(addScape(nf.getNomefantasiaCliente()));
			}else if(cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals(""))
				registro2.setNometomador(addScape(cliente.getRazaosocial()));
			else
				registro2.setNometomador(addScape(cliente.getNome()));
			
			registro2.setImtomador(cliente.getInscricaomunicipal());
			registro2.setEmailtomador(cliente.getEmail());
			
			registro2.setEnderecotomador(enderecoCliente.getLogradouro());
			registro2.setComplementotomador(enderecoCliente.getComplemento());
			registro2.setNumerotomador(enderecoCliente.getNumero());
			registro2.setBairrotomador(enderecoCliente.getBairro());
			registro2.setCidadetomador(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getNome() : null);
			registro2.setUftomador(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : null);
			registro2.setCeptomador(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
			
			loteBrumadinho.addRegistro2(registro2);
		}
		
		Registro1 registro1 = new Registro1();
		registro1.setDataarquivo(SinedDateUtils.currentDate());
		registro1.setInscricaoprestador(configuracaonfe.getInscricaomunicipal());
		
		Registro9 registro9 = new Registro9();
		registro9.setNumerolinhasdetalhe(numerolinhasdetalhe);
		registro9.setValortotalservicos(valortotalservicos);
		registro9.setValortotalbc(valortotalbc);
		
		loteBrumadinho.setRegistro1(registro1);
		loteBrumadinho.setRegistro9(registro9);
		return loteBrumadinho;
	}
	
	private br.com.linkcom.lknfe.xml.nfse.prescon.Nfe createXmlEnviarLoteNfseVinhedo(String in, Configuracaonfe configuracaonfe) {
		br.com.linkcom.lknfe.xml.nfse.prescon.Nfe nfe = new br.com.linkcom.lknfe.xml.nfse.prescon.Nfe();

		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		for (NotaFiscalServico nfs : listaNota) {
			Notafiscal notafiscal = new Notafiscal();
			
			Dadosprestador dadosprestador = new Dadosprestador();
			notafiscal.setDadosprestador(dadosprestador);
			
			Dadostomador dadostomador = new Dadostomador();
			notafiscal.setDadostomador(dadostomador);
			
			Dadosservico dadosservico = new Dadosservico();
			notafiscal.setDadosservico(dadosservico);
			
			Detalheservico detalheservico = new Detalheservico();
			notafiscal.setDetalheservico(detalheservico);
			
			dadosprestador.setIm(StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()), "0", 9, false));
			dadosprestador.setNumeronota(StringUtils.stringCheia(StringUtils.soNumero(nfs.getNumero()), "0", 6, false));
			dadosprestador.setDataemissao(nfs.getDtEmissao());
			
			Cliente cliente = nfs.getCliente();
			if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				dadostomador.setTipodoc("F");
				dadostomador.setDocumento(cliente.getCpf().getValue());
			} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
				dadostomador.setTipodoc("J");
				dadostomador.setDocumento(cliente.getCnpj().getValue());
				dadostomador.setIe(cliente.getInscricaoestadual() != null && !cliente.getInscricaoestadual().trim().equals("") ? cliente.getInscricaoestadual() : null);
			}
			
			if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
				dadostomador.setNometomador(addScape(nfs.getNomefantasiaCliente()));
			} else if(cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals("")){
				dadostomador.setNometomador(addScape(cliente.getRazaosocial()));
			} else {
				dadostomador.setNometomador(addScape(cliente.getNome()));
			}
			dadostomador.setEmail(cliente.getEmail() != null && !cliente.getEmail().trim().equals("") ? cliente.getEmail() : "NT");
			
			if(nfs.getEnderecoCliente() != null){
				br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());
				if(enderecoCliente != null){
					dadostomador.setLogradouro(enderecoCliente.getLogradouro());
					dadostomador.setNumero(StringUtils.stringCheia(StringUtils.soNumero(enderecoCliente.getNumero()), "0", 4, false));
					dadostomador.setBairro(enderecoCliente.getBairro());
					dadostomador.setUf(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : "");
					dadostomador.setCidade(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getNome() : "");
					dadostomador.setCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : "");
				}
			}
			
			br.com.linkcom.sined.geral.bean.Endereco endereco = configuracaonfe.getEndereco();
			if(endereco != null){
				dadosservico.setPais("Brasil");
				dadosservico.setLogradouro(endereco.getLogradouro());
				dadosservico.setNumero(StringUtils.stringCheia(StringUtils.soNumero(endereco.getNumero()), "0", 4, false));
				dadosservico.setBairro(endereco.getBairro());
				dadosservico.setUf(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : "");
				dadosservico.setCidade(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "");
				dadosservico.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : "");
			}
			
			StringBuilder discriminacao = new StringBuilder();
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
			}
			detalheservico.setDescricao(addScape(discriminacao.toString()));
			detalheservico.setValor(nfs.getValorTotalServicos().getValue().doubleValue());
			
			String codigo = nfs.getCodigotributacao() != null ? nfs.getCodigotributacao().getCodigo() : "";
			detalheservico.setCodigo(StringUtils.stringCheia(StringUtils.soNumero(codigo), "0", 4, false));
			
			Double desconto = 0d;
			if(nfs.getDescontocondicionado() != null){
				desconto += nfs.getDescontocondicionado().getValue().doubleValue();
			}
			if(nfs.getDescontoincondicionado() != null){
				desconto += nfs.getDescontoincondicionado().getValue().doubleValue();
			}
			detalheservico.setDesconto(desconto);
			
			Money valorInss = nfs.getValorInss();
			Money valorIr = nfs.getValorIr();
			Money valorCsll = nfs.getValorCsll();
			Money valorCofins = nfs.getValorCofins();
			Money valorPis = nfs.getValorPis();
			
			detalheservico.setInss(valorInss != null ? valorInss.getValue().doubleValue() : 0d);
			detalheservico.setIr(valorIr != null ? valorIr.getValue().doubleValue() : 0d);
			detalheservico.setCsll(valorCsll != null ? valorCsll.getValue().doubleValue() : 0d);
			detalheservico.setCofins(valorCofins != null ? valorCofins.getValue().doubleValue() : 0d);
			detalheservico.setPispasep(valorPis != null ? valorPis.getValue().doubleValue() : 0d);
			detalheservico.setOutra(nfs.getOutrasretencoes() != null ? nfs.getOutrasretencoes().getValue().doubleValue() : 0d);
			detalheservico.setIssretido(nfs.getIncideiss() != null && nfs.getIncideiss() ? 1 : 0);
			detalheservico.setOutromunicipio(0);
			detalheservico.setObs(nfs.getInfocomplementar() != null && !nfs.getInfocomplementar().trim().equals("") ? nfs.getInfocomplementar() : "");
			
			nfe.addNotafiscal(notafiscal);
		}
		return nfe;
	}
	
	private Tbnfd createXmlEnviarLoteNfseSmarapd(String in, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		if(listaNota == null || listaNota.size() > 1) throw new SinedException("No XML da prefeitura de Paracatu só pode uma NF.");
		
		NotaFiscalServico nfs = listaNota.get(0);
		
		Tbnfd tbnfd = new Tbnfd();
		
		Nfd nfd = new Nfd();
		tbnfd.setNfd(nfd);
		
		if(configuracaonfe.getEnviarnumeronf() != null && configuracaonfe.getEnviarnumeronf()){
			nfd.setNumeronfd(nfs.getNumero());
		} else {
			nfd.setNumeronfd("0");
		}
		nfd.setCodseriedocumento("7");
		
		Naturezaoperacao naturezaoperacao = nfs.getNaturezaoperacao();
		if(naturezaoperacao != null && naturezaoperacao.getCodigonfse() != null){
			nfd.setCodnaturezaoperacao(naturezaoperacao.getCodigonfse());
		}
		nfd.setCodigocidade("3");
		nfd.setInscricaomunicipalemissor(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()));
		nfd.setDataemissao(SinedDateUtils.currentDate());
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(nfs.getDadosAdicionais())){
			nfd.setObservacao(nfs.getDadosAdicionais().length() > 110 ? nfs.getDadosAdicionais().substring(0, 110) : nfs.getDadosAdicionais());
		}
		
		Cliente cliente = nfs.getCliente();
		
		if(cliente != null){
			if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
				nfd.setRazaotomador(addScape(nfs.getNomefantasiaCliente()));
			} else if(cliente.getRazaosocial() != null && !cliente.getRazaosocial().trim().equals("")){
				nfd.setRazaotomador(addScape(cliente.getRazaosocial().trim()));
			} else {
				nfd.setRazaotomador(addScape(cliente.getNome().trim()));
			}
			nfd.setNomefantasiatomador(addScape(cliente.getNome().trim()));
			nfd.setEmailtomador(cliente.getEmail());
			
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());
			
			boolean exterior = false;
			if(enderecoCliente.getPais() != null && 
					enderecoCliente.getPais().getNome() != null &&
					!enderecoCliente.getPais().getNome().trim().toUpperCase().equals("BRASIL")){
				exterior = true;
				nfd.setTppessoa("O");
			}
			
			if(!exterior && cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				if(cliente.getCpf() == null) throw new SinedException("Cadastrar o CPF do cliente " + cliente.getNome());
				nfd.setCpfcnpjtomador(StringUtils.soNumero(cliente.getCpf().getValue()));
				nfd.setTppessoa("F");
			} else {
				if(!exterior){
					if(cliente.getCnpj() == null) throw new SinedException("Cadastrar o CNPJ do cliente " + cliente.getNome());
					nfd.setCpfcnpjtomador(StringUtils.soNumero(cliente.getCnpj().getValue()));
					nfd.setTppessoa("J");
				}
				
				if(nfs.getInscricaomunicipal() != null && !nfs.getInscricaomunicipal().trim().equals("")){
					nfd.setInscricaomunicipaltomador(StringUtils.soNumero(nfs.getInscricaomunicipal()));
				} else if(cliente.getInscricaomunicipal() != null && !cliente.getInscricaomunicipal().trim().equals("")){
					nfd.setInscricaomunicipaltomador(StringUtils.soNumero(cliente.getInscricaomunicipal()));
				}
				
				if(cliente.getInscricaoestadual() != null){
					nfd.setInscricaoestadualtomador(cliente.getInscricaoestadual());
				}
			}
			
			String erroEnderecoCliente = "";
		
			if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) erroEnderecoCliente += "Logradouro do cliente " + cliente.getNome() + " não informado.<BR>";
			if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")) erroEnderecoCliente += "Bairro do cliente " + cliente.getNome() + " não informado.<BR>";
			if(!exterior && (enderecoCliente.getCep() == null || StringUtils.soNumero(enderecoCliente.getCep().getValue()).equals(""))) erroEnderecoCliente += "CEP do cliente " + cliente.getNome() + " não informado.<BR>";
			if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")) erroEnderecoCliente += "Número do endereço do cliente " + cliente.getNome() + " não informado.<BR>";
			if(enderecoCliente.getMunicipio() == null) erroEnderecoCliente += "Município do cliente " + cliente.getNome() + " não informado.<BR>";
		
			if(!erroEnderecoCliente.equals("")){
				throw new SinedException(erroEnderecoCliente);
			}
			
			
			
			nfd.setEnderecotomador(addScape(enderecoCliente.getLogradouroNumeroComplemento()));
			nfd.setBairrotomador(addScape(enderecoCliente.getBairro()));
			nfd.setCeptomador(enderecoCliente.getCep() != null ? StringUtils.soNumero(enderecoCliente.getCep().getValue()) : null);
			nfd.setCidadetomador(enderecoCliente.getMunicipio().getNome());
			nfd.setEstadotomador(enderecoCliente.getMunicipio().getUf().getSigla());
			nfd.setPaistomador(enderecoCliente.getPais() != null ? enderecoCliente.getPais().getNome() : "Brasil");
			
			String telefonecliente = null;

			if (nfs.getTelefoneCliente() != null && !"".equals(nfs.getTelefoneCliente())) {
				telefonecliente = StringUtils.soNumero(nfs.getTelefoneCliente());
			} else if (cliente.getListaTelefone() != null && cliente.getListaTelefone().size() > 0) {
				telefonecliente = StringUtils.soNumero(cliente.getListaTelefone().iterator().next().getTelefone());
			}
			nfd.setFonetomador(telefonecliente);
//			nfd.setFaxtomador(faxtomador);
		}
		
		Money valorLiquidoNota = nfs.getValorNota();
		
		Tbfatura tbfatura = new Tbfatura();
		Fatura fatura;
		if(SinedUtil.isListNotEmpty(nfs.getListaDuplicata())){
			for(Notafiscalservicoduplicata duplicata : nfs.getListaDuplicata()){
				fatura = new Fatura();
				fatura.setNumfatura(duplicata.getNumero());
				fatura.setVencimentofatura(duplicata.getDtvencimento());
				fatura.setValorfatura(duplicata.getValor() != null ? duplicata.getValor().getValue().doubleValue() : 0d);
				
				tbfatura.addFatura(fatura);
			}
		}else {
			fatura = new Fatura();
			fatura.setNumfatura("1");
			fatura.setVencimentofatura(nfs.getDtVencimento() != null ? nfs.getDtVencimento() : SinedDateUtils.currentDate());
			fatura.setValorfatura(valorLiquidoNota != null ? valorLiquidoNota.getValue().doubleValue() : 0d);
			
			tbfatura.addFatura(fatura);
		}
		nfd.setTbfatura(tbfatura);
		
		Tbservico tbservico = new Tbservico();
		
		boolean impostoretido = nfs.getIncideiss() != null && nfs.getIncideiss();
		Double aliquota = nfs.getIss() != null ? nfs.getIss() : 0d;
		String codatividade = nfs.getCodigotributacao() != null ? nfs.getCodigotributacao().getCodigo() : null;
		
		List<NotaFiscalServicoItem> listaItens = nfs.getListaItens();
		for (NotaFiscalServicoItem it : listaItens) {
			br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Servico servico = new br.com.linkcom.lknfe.xml.nfse.prefeituraserra.Servico();
			servico.setQuantidade(it.getQtde());
			String descricao = this.preencheDescricaoItemNfs(it, configuracaonfe);
			servico.setDescricao(descricao.substring(0, descricao.length()-1));			
			servico.setValorunitario(it.getPrecoUnitario());
			
			servico.setCodatividade(codatividade);
			servico.setAliquota(aliquota);
			servico.setImpostoretido(impostoretido);
			
			tbservico.addServico(servico);
		}
		nfd.setTbservico(tbservico);
		
		if(nfs.getIncidepis() != null && nfs.getIncidepis() && (nfs.getValorPis() != null || nfs.getValorPisRetido() != null)){
			double valorPis = nfs.getValorPis().getValue().doubleValue();
			if(nfs.getValorPisRetido() != null && nfs.getValorPisRetido().getValue().doubleValue() > 0){
				valorPis = nfs.getValorPisRetido().getValue().doubleValue();
			}
			nfd.setPis(valorPis);
		}
		
		if(nfs.getIncidecofins() != null && nfs.getIncidecofins() && (nfs.getValorCofins() != null || nfs.getValorCofinsRetido() != null)){
			double valorCofins = nfs.getValorCofins().getValue().doubleValue();
			if(nfs.getValorCofinsRetido() != null && nfs.getValorCofinsRetido().getValue().doubleValue() > 0){
				valorCofins = nfs.getValorCofinsRetido().getValue().doubleValue();
			}
			nfd.setCofins(valorCofins);
		}
		
		if(nfs.getIncideinss() != null && nfs.getIncideinss() && nfs.getValorInss() != null){
			double valorInss = nfs.getValorInss().getValue().doubleValue();
			nfd.setInss(valorInss);
		}
		
		if(nfs.getIncideir() != null && nfs.getIncideir() && nfs.getValorIr() != null){
			double valorIr = nfs.getValorIr().getValue().doubleValue();
			nfd.setIrrf(valorIr);
		}
		
		if(nfs.getIncidecsll() != null && nfs.getIncidecsll() && nfs.getValorCsll() != null){
			double valorCsll = nfs.getValorCsll().getValue().doubleValue();
			nfd.setCsll(valorCsll);
		}
		
		nfd.setNumerort(nfs.getNumero());
		nfd.setCodigoseriert("7");
		nfd.setDataemissaort(nfs.getDtEmissao());
		
		return tbnfd;
	}
	
	private Nfse createXmlEnviarLoteNfseParacatu(String in, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		if(listaNota == null || listaNota.size() > 1) throw new SinedException("No XML da prefeitura de Paracatu só pode uma NF.");
		
		NotaFiscalServico notaFiscalServico = listaNota.get(0);
		empresaService.setInformacoesPropriedadeRural(notaFiscalServico.getEmpresa());
		
		Nfse nfse = new Nfse();
		
		Nf nf = new Nf();
		nfse.setNf(nf);
		
		nf.setValor_total(notaFiscalServico.getValorTotalServicos().getValue().doubleValue());
		nf.setValor_desconto(notaFiscalServico.getDescontocondicionado() != null ? notaFiscalServico.getDescontocondicionado().getValue().doubleValue() : 0.0);
		nf.setValor_ir(notaFiscalServico.getValorIr() != null ? notaFiscalServico.getValorIr().getValue().doubleValue() : 0.0);
		nf.setValor_inss(notaFiscalServico.getValorInss() != null ? notaFiscalServico.getValorInss().getValue().doubleValue() : 0.0);
		nf.setValor_contribuicao_social(notaFiscalServico.getValorCsll() != null ? notaFiscalServico.getValorCsll().getValue().doubleValue() : 0.0);
		nf.setValor_rps(0.0);
		nf.setValor_pis(notaFiscalServico.getValorPis() != null ? notaFiscalServico.getValorPis().getValue().doubleValue() : 0.0);
		nf.setValor_cofins(notaFiscalServico.getValorCofins() != null ? notaFiscalServico.getValorCofins().getValue().doubleValue() : 0.0);
		nf.setObservacao(notaFiscalServico.getDadosAdicionais());
		
		br.com.linkcom.lknfe.xml.nfse.fiscalweb.Prestador prestador = new br.com.linkcom.lknfe.xml.nfse.fiscalweb.Prestador();
		nfse.setPrestador(prestador);
		
		prestador.setCpfcnpj(notaFiscalServico.getEmpresa().getCpfOuCnpjValueProprietarioPrincipalOuEmpresa());
		prestador.setCidade(configuracaonfe.getEndereco().getMunicipio().getCodigotom());
				
		br.com.linkcom.lknfe.xml.nfse.fiscalweb.Tomador tomador = new br.com.linkcom.lknfe.xml.nfse.fiscalweb.Tomador();
		nfse.setTomador(tomador);
		
		Cliente cliente = notaFiscalServico.getCliente();
		br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(notaFiscalServico.getEnderecoCliente());
		
		if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
			tomador.setTipo("F");
			tomador.setCpfcnpj(cliente.getCpf().getValue());
		} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
			tomador.setTipo("J");
			tomador.setCpfcnpj(cliente.getCnpj().getValue());
		}
		
		tomador.setIe(cliente.getInscricaoestadual());
		
		if(notaFiscalServico.getNomefantasiaCliente() != null && !"".equals(notaFiscalServico.getNomefantasiaCliente())){
			tomador.setNome_razao_social(addScape(notaFiscalServico.getNomefantasiaCliente()));
		}else if(cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals(""))
			tomador.setNome_razao_social(addScape(cliente.getRazaosocial()));
		else
			tomador.setNome_razao_social(addScape(cliente.getNome()));
		
		tomador.setLogradouro(enderecoCliente.getLogradouro());
		tomador.setEmail(cliente.getEmail());
		tomador.setNumero_residencia(enderecoCliente.getNumero());
		tomador.setComplemento(enderecoCliente.getComplemento());
		tomador.setPonto_referencia(enderecoCliente.getPontoreferencia());
		tomador.setBairro(enderecoCliente.getBairro());
		tomador.setCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
		
		br.com.linkcom.lknfe.xml.nfse.fiscalweb.Itens itens = new br.com.linkcom.lknfe.xml.nfse.fiscalweb.Itens();
		nfse.setItens(itens);
		
		Lista lista = new Lista();
		itens.setLista(lista);
		
		Naturezaoperacao naturezaoperacao = notaFiscalServico.getNaturezaoperacao();
		if(naturezaoperacao != null && naturezaoperacao.equals(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO)){
			lista.setTributa_municipio_prestador("N");
		} else {
			lista.setTributa_municipio_prestador("S");
		}
		
		lista.setCodigo_local_prestacao_servico(notaFiscalServico.getMunicipioissqn() != null ? notaFiscalServico.getMunicipioissqn().getCdsiafi() : "");
		lista.setUnidade_codigo("1");
		lista.setUnidade_quantidade(1d);
		lista.setUnidade_valor_unitario(notaFiscalServico.getValorTotalServicos().getValue().doubleValue());
		lista.setCodigo_item_lista_servico(notaFiscalServico.getItemlistaservicoBean() != null ? StringUtils.soNumero(notaFiscalServico.getItemlistaservicoBean().getCodigo()) : "");
		lista.setAliquota_item_lista_servico(notaFiscalServico.getIss());
		lista.setSituacao_tributaria(notaFiscalServico.getSituacaoissparacatu() != null ? (notaFiscalServico.getSituacaoissparacatu().getCdnfe() + "") : "");
		lista.setValor_tributavel(notaFiscalServico.getBasecalculoiss() != null ? notaFiscalServico.getBasecalculoiss().getValue().doubleValue() : null);
		lista.setValor_deducao(notaFiscalServico.getDeducao().getValue().doubleValue());
		
		if(notaFiscalServico.getIncideiss() != null && notaFiscalServico.getIncideiss())
			lista.setValor_issrf(notaFiscalServico.getValorIss().getValue().doubleValue());
		
		StringBuilder discriminacao = new StringBuilder();
		for (NotaFiscalServicoItem it : notaFiscalServico.getListaItens()) {
			String descricao = this.preencheDescricaoItemNfs(it, configuracaonfe);
			discriminacao.append(descricao);
		}
		String strDiscriminacao = discriminacao.toString();
		lista.setDescritivo(strDiscriminacao.length() > 2000 ? addScape(strDiscriminacao.substring(0, 2000)) : addScape(strDiscriminacao));
		
		return nfse;
	}
	
	/**
	 * Cria o XMl de envio de uma NF de Serviço Eletrônica da prefeitura de Lavras.
	 *
	 * @param whereIn
	 * @param empresa
	 * @return
	 * @since 22/08/2012
	 * @author Rodrigo Freitas
	 * @param configuracaonfe 
	 */
	private GovDigital createXmlEnviarLoteNfseLavras(String whereIn, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(whereIn);
		
		GovDigital govDigital = new GovDigital();
		
		Emissao emissao = new Emissao();
		govDigital.setEmissao(emissao);
		
		Listanfe listanfe = new Listanfe();
		emissao.setListanfe(listanfe);
		
		StringBuilder sbErro = new StringBuilder();
		
		for (NotaFiscalServico nfs: listaNota) {
			Nfe nfe = new Nfe();
			
			if(configuracaonfe.getExigibilidadeisslavras() == null) sbErro.append("O campo Exigibilidade ISS da configuração não preenchido.<BR>");
			if(nfs.getMunicipioissqn() == null) sbErro.append("O campo Município da incidência do ISSQN da nota " + nfs.getNumero() + " não preenchido.<BR>");
			if(nfs.getCodigotributacao() == null) sbErro.append("O campo Código de tributação da nota " + nfs.getNumero() + " não preenchido.<BR>");
			
			if(sbErro.length() > 0) continue;
			
			nfe.setCorrelacao(nfs.getNumero());
			nfe.setPrestacao(nfs.getDtEmissao());
			nfe.setExigibilidade(configuracaonfe.getExigibilidadeisslavras().getValue() + 1);
			nfe.setRetido(nfs.getIncideiss() != null && nfs.getIncideiss() ? 1 : 2);
			
			if(nfs.getIss() != null)
				nfe.setAliquota(nfs.getIss()/100d);
			
			nfe.setMunicipioIncidencia(nfs.getMunicipioissqn().getCdibge());
			nfe.setAtividade(nfs.getCodigotributacao().getCodigo());
			nfe.setObs(nfs.getDadosAdicionais());
			
			br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Tomador tomador = new br.com.linkcom.lknfe.xml.nfse.prefeituralavras.Tomador();
			nfe.setTomador(tomador);
			
			String documento = null;
			String nome = null;
			
			if(nfs.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				documento = nfs.getCliente().getCpf().getValue();
			} else {
				documento = nfs.getCliente().getCnpj().getValue();
			}
			
			if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
				nome = addScape(nfs.getNomefantasiaCliente());
			}else if(nfs.getCliente().getRazaosocial() != null && !nfs.getCliente().getRazaosocial().equals(""))
				nome = addScape(nfs.getCliente().getRazaosocial());
			else
				nome = addScape(nfs.getCliente().getNome());
			
			tomador.setNome(nome);
			tomador.setDocumento(documento);
			tomador.setEmail(nfs.getCliente().getEmail());
			
			if(nfs.getCliente().getInscricaoestadual() != null &&
					!nfs.getCliente().getInscricaoestadual().trim().equalsIgnoreCase("") &&
					!nfs.getCliente().getInscricaoestadual().trim().equalsIgnoreCase("ISENTO")){
				tomador.setInscEst(nfs.getCliente().getInscricaoestadual());
			}
			
			Telefone telBean = nfs.getCliente().getTelefone();
			tomador.setTelefone(telBean != null ? telBean.getTelefone() : null);
			
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());

			tomador.setLogradouro(enderecoCliente.getLogradouro());
			tomador.setNumero(enderecoCliente.getNumero());
			tomador.setComplemento(enderecoCliente.getComplemento());
			tomador.setCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
			tomador.setBairro(enderecoCliente.getBairro());
			tomador.setMunicipio(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getNome() : null);
			tomador.setEstado(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : null);
			tomador.setPais(enderecoCliente.getPais() != null ? enderecoCliente.getPais().getNome() : null);
			
			
			if(nfs.getListaItens() != null && nfs.getListaItens().size() > 0){
				Itens itens = new Itens();
				nfe.setItens(itens);
				
				Listaitem listaitem = new Listaitem();
				itens.setListaitem(listaitem);
				
				for (NotaFiscalServicoItem notaFiscalServicoItem : nfs.getListaItens()) {
					Item item = new Item();	
					String descricao = this.preencheDescricaoItemNfs(notaFiscalServicoItem, configuracaonfe);
					item.setDescricao(descricao.substring(0, descricao.length()-1));
					
					item.setValor(notaFiscalServicoItem.getTotalParcial().getValue().doubleValue());
	
					listaitem.addItem(item);
				}
			}
			
			boolean deduzirINSS = nfs.getIncideinss() != null && nfs.getIncideinss() && nfs.getInss() != null && nfs.getInss() > 0;
			boolean deduzirPIS = nfs.getIncidepis() != null && nfs.getIncidepis() && nfs.getPis() != null && nfs.getPis() > 0;
			boolean deduzirCOFINS = nfs.getIncidecofins() != null && nfs.getIncidecofins() && nfs.getCofins() != null && nfs.getCofins() > 0;
			boolean deduzirIRRF = nfs.getIncideir() != null && nfs.getIncideir() && nfs.getIr() != null && nfs.getIr() > 0;
			boolean deduzirCSLL = nfs.getIncidecsll() != null && nfs.getIncidecsll() && nfs.getCsll() != null && nfs.getCsll() > 0;
			
			boolean deduzirDESCONTO_CONDICIONAL = nfs.getDescontocondicionado() != null && nfs.getDescontocondicionado().getValue().doubleValue() > 0;
			boolean deduzirDESCONTO_INCONDICIONAL = nfs.getDescontoincondicionado() != null && nfs.getDescontoincondicionado().getValue().doubleValue() > 0;
			boolean deduzirDEDUCOES = nfs.getDeducao() != null && nfs.getDeducao().getValue().doubleValue() > 0;
			boolean deduzirOUTRAS_RETENCOES = nfs.getOutrasretencoes() != null && nfs.getOutrasretencoes().getValue().doubleValue() > 0;
			
			if(deduzirINSS || deduzirPIS || deduzirCOFINS || deduzirIRRF || 
				deduzirCSLL || deduzirDESCONTO_CONDICIONAL || deduzirDESCONTO_INCONDICIONAL || 
				deduzirDEDUCOES || deduzirOUTRAS_RETENCOES){
				
				Deducoes deducoes = new Deducoes();
				nfe.setDeducoes(deducoes);
				
				Listadeducao listadeducao = new Listadeducao();
				deducoes.setListadeducao(listadeducao);
				
				if(deduzirINSS){
					Deducao deducao = new Deducao();
					deducao.setCodigo("INSS");
					deducao.setValor(nfs.getValorInss().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirPIS){
					Deducao deducao = new Deducao();
					deducao.setCodigo("PIS");
					deducao.setValor(nfs.getValorPis().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirCOFINS){
					Deducao deducao = new Deducao();
					deducao.setCodigo("COFINS");
					deducao.setValor(nfs.getValorCofins().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirIRRF){
					Deducao deducao = new Deducao();
					deducao.setCodigo("IRRF");
					deducao.setValor(nfs.getValorIr().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirCSLL){
					Deducao deducao = new Deducao();
					deducao.setCodigo("CSLL");
					deducao.setValor(nfs.getValorCsll().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirDESCONTO_CONDICIONAL){
					Deducao deducao = new Deducao();
					deducao.setCodigo("DESCONTO CONDICIONAL");
					deducao.setValor(nfs.getDescontocondicionado().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirDESCONTO_INCONDICIONAL){
					Deducao deducao = new Deducao();
					deducao.setCodigo("DESCONTO INCONDICIONAL");
					deducao.setValor(nfs.getDescontoincondicionado().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirDEDUCOES){
					Deducao deducao = new Deducao();
					deducao.setCodigo("DEDUCOES");
					deducao.setValor(nfs.getDeducao().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
				
				if(deduzirOUTRAS_RETENCOES){
					Deducao deducao = new Deducao();
					deducao.setCodigo("OUTRAS RETENCOES");
					deducao.setValor(nfs.getOutrasretencoes().getValue().doubleValue());
					listadeducao.addDeducao(deducao);
				}
			}
			
			listanfe.addNfe(nfe);
		}
		
		if(sbErro.length() > 0){
			throw new SinedException(sbErro.toString());
		}
		
		return govDigital;
	}

	/**
	 * Cria o XMl de envio de uma NF de Serviço Eletrônica da prefeitura de São Paulo.
	 *
	 * @param whereIn
	 * @param empresa
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public PedidoEnvioLoteRPS createXmlEnviarLoteNfseSp(String whereIn, Empresa empresa, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(whereIn);
		
		PedidoEnvioLoteRPS pedidoEnvioLoteRPS = new PedidoEnvioLoteRPS(prefixowebservice.getPrefixo());
		
		PedidoEnvioLoteRPSCabecalho cabecalho = new PedidoEnvioLoteRPSCabecalho();
		pedidoEnvioLoteRPS.setCabecalho(cabecalho);
		
		cabecalho.setVersao(1);
		
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		CPFCNPJ cpfcnpjRemetente = new CPFCNPJ("Remetente");
		cpfcnpjRemetente.setCnpj(empresa != null ? empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() : null);
		cabecalho.setCpfcnpjremetente(cpfcnpjRemetente);
		
		cabecalho.setTransacao(Boolean.TRUE);
		cabecalho.setQtdRPS(listaNota != null ? listaNota.size() : 0);
		this.setResumoXmlEnvioSp(cabecalho, listaNota);
		
		ListaRPS listaRPS = new ListaRPS();
		pedidoEnvioLoteRPS.setListaRPS(listaRPS);
		
		if(listaNota != null && listaNota.size() > 0){
			RPS rps;
			ChaveRPS chaveRPS;
			CPFCNPJ cpfcnpjTomador;
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente;
			br.com.linkcom.lknfe.xml.nfse.prefeiturasp.Endereco endereco;
			StringBuilder discriminacao;
			String strDiscriminacao;
			for (NotaFiscalServico nf : listaNota) {
				rps = new RPS();
				
				rps.setAssinatura(this.makeAssinaturaXmlEnvioSp(nf, configuracaonfe));
				
				chaveRPS = new ChaveRPS();
				rps.setChaveRPS(chaveRPS);
				
				chaveRPS.setInscricaoPrestador(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()));
				try {
					chaveRPS.setNumerorps(Integer.parseInt(nf.getNumero()));
				} catch (Exception e) {
					throw new SinedException("Erro na conversão do número da nota.");
				}
				chaveRPS.setSerierps(configuracaonfe.getSerienfse());
				
				rps.setTipoRPS("RPS");
				rps.setDataEmissao(nf.getDtEmissao());
				rps.setStatusRPS("N");
				
				if(nf.getNaturezaoperacao() != null){
					if(nf.getNaturezaoperacao().equals(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO)) rps.setTributacaoRPS("T");
					else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO)) rps.setTributacaoRPS("F");
					else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.ISENCAO)) rps.setTributacaoRPS("I");
					else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.EXIGIBILIDADE_SUSPENSA_JUDICIAL)) rps.setTributacaoRPS("J");
					else rps.setTributacaoRPS("T");
				} else rps.setTributacaoRPS("T");
				
				rps.setValorServicos(SinedUtil.round(nf.getValorTotalServicos().getValue().doubleValue(), 2));
				
				if(nf.getDeducao() != null) rps.setValorDeducoes(nf.getDeducao().getValue().doubleValue());
				else rps.setValorDeducoes(0d);
				
				if(nf.getValorPis() != null) rps.setValorPIS(nf.getValorPis().getValue().doubleValue());
				else rps.setValorPIS(0d);
				
				if(nf.getValorCofins() != null) rps.setValorCOFINS(nf.getValorCofins().getValue().doubleValue());
				else rps.setValorCOFINS(0d);
				
				if(nf.getValorInss() != null) rps.setValorINSS(nf.getValorInss().getValue().doubleValue());
				else rps.setValorINSS(0d);
				
				if(nf.getValorIr() != null) rps.setValorIR(nf.getValorIr().getValue().doubleValue());
				else rps.setValorIR(0d);
				
				if(nf.getValorCsll() != null) rps.setValorCSLL(nf.getValorCsll().getValue().doubleValue());
				else rps.setValorCSLL(0d);
				
				if(nf.getCodigotributacao() != null){
					String codigo = nf.getCodigotributacao().getCodigo();
					rps.setCodigoServicos(StringUtils.stringCheia(codigo, "0", 5, false));
				}
				
				if(nf.getIss() != null) rps.setAliquotaServicos(nf.getIss()/100d);
				else rps.setAliquotaServicos(0d);
				
				if(configuracaonfe.getIssretidopelotomador() != null && configuracaonfe.getIssretidopelotomador()){
					rps.setIssRetido(true);
				} else rps.setIssRetido(nf.getIncideiss() != null && nf.getIncideiss());
				
				cpfcnpjTomador = new CPFCNPJ("Tomador");
				rps.setCpfcnpjTomador(cpfcnpjTomador);
				
				if(nf.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
					cpfcnpjTomador.setCpf(nf.getCliente().getCpf().getValue());
				} else {
					cpfcnpjTomador.setCnpj(nf.getCliente().getCnpj().getValue());
				}
				
				enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
				
				if(enderecoCliente != null && 
						enderecoCliente.getMunicipio() != null &&
						enderecoCliente.getMunicipio().getCdibge() != null &&
						enderecoCliente.getMunicipio().getCdibge().equals("3550308")){
					if(nf.getInscricaomunicipal() != null)
						rps.setInscricaoMunicipalTomador(br.com.linkcom.utils.StringUtils.soNumero(!nf.getInscricaomunicipal().trim().equals("") ? nf.getInscricaomunicipal() : null));
					else if(nf.getCliente().getInscricaomunicipal() != null){
						rps.setInscricaoMunicipalTomador(br.com.linkcom.utils.StringUtils.soNumero(nf.getCliente().getInscricaomunicipal()));
					}else 
						rps.setInscricaoMunicipalTomador(null);
				} else {
					rps.setInscricaoMunicipalTomador(null);
				}
				
				rps.setInscricaoEstadualTomador(null);
				
				if(nf.getNomefantasiaCliente() != null && !"".equals(nf.getNomefantasiaCliente()))
					rps.setRazaoSocialTomador(addScape(nf.getNomefantasiaCliente()));
				else if(nf.getCliente().getRazaosocial() != null && !nf.getCliente().getRazaosocial().equals(""))
					rps.setRazaoSocialTomador(addScape(nf.getCliente().getRazaosocial()));
				else
					rps.setRazaoSocialTomador(addScape(nf.getCliente().getNome()));	
				
				if(enderecoCliente != null){
					endereco = new br.com.linkcom.lknfe.xml.nfse.prefeiturasp.Endereco();
					rps.setEnderecoTomador(endereco);
					
					String tipoLogradouro = enderecoCliente.getLogradouro().split(" ")[0];
					endereco.setTipoLogradouro(tipoLogradouro != null && tipoLogradouro.length() > 3 ? tipoLogradouro.substring(0, 3) : tipoLogradouro);
					endereco.setLogradouro(enderecoCliente.getLogradouro().substring(enderecoCliente.getLogradouro().indexOf(" ") + 1));
					endereco.setNumeroEndereco(enderecoCliente.getNumero());
					endereco.setComplementoEndereco(enderecoCliente.getComplemento());
					endereco.setBairro(enderecoCliente.getBairro());
					if(enderecoCliente.getMunicipio() != null){
						endereco.setCidade(enderecoCliente.getMunicipio().getCdibge());
						endereco.setUf(enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla() : null);
					}
					endereco.setCep(enderecoCliente.getCep().getValue());
				}
				
				rps.setEmailTomador(nf.getCliente().getEmail());

				discriminacao = new StringBuilder();
				for (NotaFiscalServicoItem it : nf.getListaItens()) {
					discriminacao.append(this.preencheDescricaoItemNfs(it, configuracaonfe));
				}
				
				if(nf.getGrupotributacao() != null){
					Codigotributacao codigotributacao = nf.getCodigotributacao();
					
					List<Grupotributacaoimposto> listaGrupotributacaoimposto = grupotributacaoimpostoService.findByGrupotributacao(nf.getGrupotributacao());
					for (Grupotributacaoimposto gti : listaGrupotributacaoimposto){
						Municipio municipio = null;
						if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
							if (nf.getEnderecoCliente() != null && nf.getEnderecoCliente().getMunicipio() != null)
								municipio = nf.getEnderecoCliente().getMunicipio();
							else if (nf.getCliente() != null && nf.getCliente().getEndereco() != null)
								municipio = nf.getCliente().getEndereco().getMunicipio();
						} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
							if(nf.getMunicipioissqn() != null)
								municipio = nf.getMunicipioissqn(); 
							else if (nf.getEmpresa() != null && nf.getEmpresa().getEndereco() != null)
								municipio = nf.getEmpresa().getEndereco().getMunicipio(); 
						}
		
						Faixaimposto fi = faixaimpostoService.findByControleMunicipio(empresa, nf.getCliente(), gti.getControle(), municipio, codigotributacao);
						
						if(fi != null && fi.getExibiremissaonfse() != null && fi.getExibiremissaonfse()){
							Money basecalculo = null;
							Money valorimposto = null;
							
							if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
								basecalculo = nf.getBasecalculoicms();
								valorimposto = nf.getValorIcms();
							} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
								basecalculo = nf.getBasecalculoiss();
								valorimposto = nf.getValorIss();
							} else if (Faixaimpostocontrole.IR.equals(gti.getControle())){
								basecalculo = nf.getBasecalculoir();
								valorimposto = nf.getValorIr();
							} else if (Faixaimpostocontrole.INSS.equals(gti.getControle())){
								basecalculo = nf.getBasecalculoinss();
								valorimposto = nf.getValorInss();
							} else if (Faixaimpostocontrole.CSLL.equals(gti.getControle())){
								basecalculo = nf.getBasecalculocsll();
								valorimposto = nf.getValorCsll();
							} else if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
								basecalculo = nf.getBasecalculopis();
								valorimposto = nf.getValorPis();
							} else if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
								basecalculo = nf.getBasecalculocofins();
								valorimposto = nf.getValorCofins();
							}
							
							discriminacao.append("\nBase de cálculo do ").append(gti.getControle().toString()).append(": R$ ").append(basecalculo != null ? basecalculo : new Money()).append(" ");
							discriminacao.append("Valor do ").append(gti.getControle().toString()).append(": R$ ").append(valorimposto != null ? valorimposto : new Money());
						}
					}
				}
				
				strDiscriminacao = discriminacao.toString();
				rps.setDiscriminacao(strDiscriminacao.length() > 2000 ? addScape(strDiscriminacao.substring(0, 2000)) : addScape(strDiscriminacao));
				
				listaRPS.addRPS(rps);
			}
		}
		
		return pedidoEnvioLoteRPS;
	}
	
	
	private String makeAssinaturaXmlEnvioSp(NotaFiscalServico nf, Configuracaonfe configuracaonfe) {
		if(nf.getNumero() == null || nf.getNumero().equals("")) throw new SinedException("Número não cadastrado.");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()), "0", 8, false));
		sb.append(StringUtils.stringCheia(configuracaonfe.getSerienfse(), " ", 5, true));
		sb.append(StringUtils.stringCheia(StringUtils.soNumero(nf.getNumero()), "0", 12, false));
		sb.append(new SimpleDateFormat("yyyyMMdd").format(nf.getDtEmissao()));
		
		if(nf.getNaturezaoperacao() != null){
			if(nf.getNaturezaoperacao().equals(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO)) sb.append("T");
			else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO)) sb.append("F");
			else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.ISENCAO)) sb.append("I");
			else if(nf.getNaturezaoperacao().equals(Naturezaoperacao.EXIGIBILIDADE_SUSPENSA_JUDICIAL)) sb.append("J");
			else sb.append("T");
		} else sb.append("T");
		
		sb.append("N");
		
		if(configuracaonfe.getIssretidopelotomador() != null && configuracaonfe.getIssretidopelotomador()){
			sb.append("S");
		} else if(nf.getIncideiss() != null && nf.getIncideiss()) {
			sb.append("S");
		} else {
			sb.append("N");
		}
		
		sb.append(StringUtils.stringCheia(nf.getValorTotalServicos().toLong() + "", "0", 15, false));
		
		if(nf.getDeducao() != null) sb.append(StringUtils.stringCheia(nf.getDeducao().toLong() + "", "0", 15, false));
		else sb.append(StringUtils.stringCheia("0", "0", 15, false));
		
		if(nf.getCodigotributacao() != null){
			sb.append(StringUtils.stringCheia(nf.getCodigotributacao().getCodigo(), "0", 5, false));
		} else {
			sb.append(StringUtils.stringCheia("", "0", 5, false));
		}
		
		if(nf.getCliente() != null){
			if(nf.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				sb.append("1");
				sb.append(StringUtils.stringCheia(nf.getCliente().getCpf().getValue(), "0", 14, false));
			} else {
				sb.append("2");
				sb.append(StringUtils.stringCheia(nf.getCliente().getCnpj().getValue(), "0", 14, false));
			}
		} else {
			sb.append("3");
			sb.append(StringUtils.stringCheia("0", "0", 14, false));
		}
		
		return sb.toString();
	}
	
	private void setResumoXmlEnvioSp(PedidoEnvioLoteRPSCabecalho cabecalho, List<NotaFiscalServico> listaNota){
		java.sql.Date dtinicio = null, dtfim = null;
		Double valorTotalServicos = 0d, valorTotalDeducoes = 0d;
		
		if(listaNota != null && listaNota.size() > 0){
			for (NotaFiscalServico nf : listaNota) {
				if(dtinicio == null || SinedDateUtils.beforeIgnoreHour(nf.getDtEmissao(), dtinicio)){
					dtinicio = nf.getDtEmissao();
				}
				if(dtfim == null || SinedDateUtils.afterIgnoreHour(nf.getDtEmissao(), dtfim)){
					dtfim = nf.getDtEmissao();
				}
				valorTotalServicos += nf.getValorTotalServicos().getValue().doubleValue();
				valorTotalDeducoes += nf.getDeducao() != null ? nf.getDeducao().getValue().doubleValue() : 0d; 
			}
		}
		cabecalho.setDtinicio(dtinicio);
		cabecalho.setDtfim(dtfim);
		cabecalho.setValorTotalServicos(valorTotalServicos);
		cabecalho.setValorTotalDeducoes(valorTotalDeducoes > 0 ? valorTotalDeducoes : null);
	}
	
	private String makeAssinaturaXmlEnvioCampinas(NotaFiscalServico nf, Configuracaonfe configuracaonfe) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()), "0", 11, false));
		sb.append(StringUtils.stringCheia(configuracaonfe.getSerienfse(), " ", 5, true));
		sb.append(StringUtils.stringCheia(StringUtils.soNumero(nf.getNumero()), "0", 12, false));
		sb.append(new SimpleDateFormat("yyyyMMdd").format(nf.getDtEmissao()));
		
		if(nf.getRegimetributacao() != null){
			sb.append(StringUtils.stringCheia(nf.getRegimetributacao().getCodigonfse(), " ", 2, true));
		} else {
			sb.append("  ");
		}
		sb.append("N");
		
		if(nf.getIncideiss() != null && nf.getIncideiss()) sb.append("S");
		else sb.append("N");
		
		sb.append(StringUtils.stringCheia(nf.getValorTotalServicos().toLong() + "", "0", 15, false));
		
		if(nf.getDeducao() != null) sb.append(StringUtils.stringCheia(nf.getDeducao().toLong() + "", "0", 15, false));
		else sb.append(StringUtils.stringCheia("0", "0", 15, false));
		
		if(nf.getCodigotributacao() != null){
			sb.append(StringUtils.stringCheia(StringUtils.soNumero(nf.getCodigotributacao().getCodigo()), "0", 10, false));
		} else {
			sb.append(StringUtils.stringCheia("", "0", 10, false));
		}
		
		if(nf.getCliente() != null){
			if(nf.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				sb.append(StringUtils.stringCheia(nf.getCliente().getCpf().getValue(), "0", 14, false));
			} else {
				sb.append(StringUtils.stringCheia(nf.getCliente().getCnpj().getValue(), "0", 14, false));
			}
		} else {
			sb.append(StringUtils.stringCheia("0", "0", 14, false));
		}
		
		return sb.toString();
	}
	
	private void setResumoXmlEnvioCampinas(Cabecalho cabecalho, List<NotaFiscalServico> listaNota){
		java.sql.Date dtinicio = null, dtfim = null;
		Double valorTotalServicos = 0d, valorTotalDeducoes = 0d;
		
		if(listaNota != null && listaNota.size() > 0){
			for (NotaFiscalServico nf : listaNota) {
				if(dtinicio == null || SinedDateUtils.beforeIgnoreHour(nf.getDtEmissao(), dtinicio)){
					dtinicio = nf.getDtEmissao();
				}
				if(dtfim == null || SinedDateUtils.afterIgnoreHour(nf.getDtEmissao(), dtfim)){
					dtfim = nf.getDtEmissao();
				}
				valorTotalServicos += nf.getValorTotalServicos().getValue().doubleValue();
				valorTotalDeducoes += nf.getDeducao() != null ? nf.getDeducao().getValue().doubleValue() : 0d; 
			}
		}
		cabecalho.setDtInicio(dtinicio);
		cabecalho.setDtFim(dtfim);
		cabecalho.setValorTotalServicos(valorTotalServicos);
		cabecalho.setValorTotalDeducoes(valorTotalDeducoes);
	}
	
	public ReqEnvioLoteRPS createXmlEnviarLoteNfseCampinas(String whereIn, Empresa empresa, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(whereIn);
		
		ReqEnvioLoteRPS reqEnvioLoteRPS = new ReqEnvioLoteRPS();
		
		Cabecalho cabecalho = new Cabecalho(true);
		reqEnvioLoteRPS.setCabecalho(cabecalho);
		
		Prefixowebservice prefixo = configuracaonfe.getPrefixowebservice();
		
		if(prefixo.equals(Prefixowebservice.CAMPINASISS_HOM) || prefixo.equals(Prefixowebservice.CAMPINASISS_PROD)){		
			cabecalho.setCodCidade("6291");
		} else if(prefixo.equals(Prefixowebservice.SOROCABAISS_HOM) || prefixo.equals(Prefixowebservice.SOROCABAISS_PROD)){		
			cabecalho.setCodCidade("7145");
		} else throw new SinedException("Não foi possível obter o prefixo da configuração.");
		
		cabecalho.setcPFCNPJRemetente(empresa != null ? empresa.getCnpj().getValue() : null);
		cabecalho.setRazaoSocialRemetente(empresa != null ? empresa.getRazaosocialOuNome() : null);
		cabecalho.setTransacao(Boolean.TRUE);
		cabecalho.setQtdRPS(listaNota != null ? listaNota.size() : 0);
		this.setResumoXmlEnvioCampinas(cabecalho, listaNota);
		cabecalho.setVersao("1");
		cabecalho.setMetodoEnvio("WS");
		
		Lote lote = new Lote();
		reqEnvioLoteRPS.setLote(lote);
		
		br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.ListaRPS listaRPS = new br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.ListaRPS();
		lote.setListaRPS(listaRPS);
		lote.setId("lote:UNICO");
		
		if(listaNota != null && listaNota.size() > 0){
			br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.RPS rps;
			Cliente cliente;
			br.com.linkcom.sined.geral.bean.Endereco enderecoCliente;
			for (NotaFiscalServico nf : listaNota) {
				rps = new br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.RPS();
				
				cliente = nf.getCliente();
				
				rps.setId("rps:" + nf.getNumero());
				rps.setAssinatura(this.makeAssinaturaXmlEnvioCampinas(nf, configuracaonfe));
				rps.setInscricaoMunicipalPrestador(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()));
				rps.setRazaoSocialPrestador(empresa != null ? empresa.getNome() : null);
				rps.setTipoRPS("RPS");
				rps.setSerieRPS(configuracaonfe.getSerienfse());
				rps.setNumeroRPS(nf.getNumero());
				rps.setDataEmissaoRPS(nf.getDtEmissao());
				rps.setSituacaoRPS("N");
				rps.setSeriePrestacao("99");
				
				// TOMADOR
				rps.setInscricaoMunicipalTomador(cliente.getInscricaomunicipal() != null ? cliente.getInscricaomunicipal() : "");
				rps.setcPFCNPJTomador(StringUtils.soNumero(cliente.getCpfOuCnpj()));
				
				if(nf.getNomefantasiaCliente() != null && !"".equals(nf.getNomefantasiaCliente()))
					rps.setRazaoSocialTomador(addScape(nf.getNomefantasiaCliente()));
				else if(cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals(""))
					rps.setRazaoSocialTomador(addScape(cliente.getRazaosocial()));
				else
					rps.setRazaoSocialTomador(addScape(cliente.getNome()));	
				
				enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
				
				if(enderecoCliente != null){
					rps.setTipoLogradouroTomador(enderecoCliente.getLogradouro().split(" ")[0]);
					rps.setLogradouroTomador(enderecoCliente.getLogradouro().substring(enderecoCliente.getLogradouro().indexOf(" ") + 1));
					rps.setNumeroEnderecoTomador(enderecoCliente.getNumero());
					rps.setComplementoEnderecoTomador(enderecoCliente.getComplemento());
					rps.setTipoBairroTomador("Bairro");
					rps.setBairroTomador(enderecoCliente.getBairro());
					if(enderecoCliente.getMunicipio() != null){
						rps.setCidadeTomador(enderecoCliente.getMunicipio().getCdsiafi());
						rps.setCidadeTomadorDescricao(enderecoCliente.getMunicipio().getNome());
					}
					if(enderecoCliente.getCep() != null) rps.setcEPTomador(enderecoCliente.getCep().getValue());
				}
				
				rps.setEmailTomador(cliente.getEmail()!= null ? cliente.getEmail() : "");
				
				if(nf.getCodigotributacao() != null){
					rps.setCodigoAtividade(nf.getCodigotributacao().getCodigo());
				}
				
				if(nf.getIss() != null) rps.setAliquotaAtividade(nf.getIss());
				else rps.setAliquotaAtividade(0d);
				
				if(nf.getIncideiss() != null && nf.getIncideiss()){
					rps.setTipoRecolhimento("R");
				} else {
					rps.setTipoRecolhimento("A");
				}
				
				if(nf.getMunicipioissqn() != null){
					rps.setMunicipioPrestacao(nf.getMunicipioissqn().getCdsiafi());
					rps.setMunicipioPrestacaoDescricao(nf.getMunicipioissqn().getNome());
				}
				
				if(nf.getNaturezaoperacao() != null){
					rps.setOperacao(nf.getNaturezaoperacao().getCodigonfse());
				}
				
				if(nf.getRegimetributacao() != null){
					rps.setTributacao(nf.getRegimetributacao().getCodigonfse());
				}
				
				if(Boolean.TRUE.equals(nf.getIncidepis()) && nf.getValorPis() != null) rps.setValorPIS(nf.getValorPis().getValue().doubleValue());
				else rps.setValorPIS(0d);
				
				if(Boolean.TRUE.equals(nf.getIncidecofins()) && nf.getValorCofins() != null) rps.setValorCOFINS(nf.getValorCofins().getValue().doubleValue());
				else rps.setValorCOFINS(0d);
				
				if(Boolean.TRUE.equals(nf.getIncideinss()) && nf.getValorInss() != null) rps.setValorINSS(nf.getValorInss().getValue().doubleValue());
				else rps.setValorINSS(0d);
				
				if(Boolean.TRUE.equals(nf.getIncideir()) && nf.getValorIr() != null) rps.setValorIR(nf.getValorIr().getValue().doubleValue());
				else rps.setValorIR(0d);
				
				if(Boolean.TRUE.equals(nf.getIncidecsll()) && nf.getValorCsll() != null) rps.setValorCSLL(nf.getValorCsll().getValue().doubleValue());
				else rps.setValorCSLL(0d);
				
				if(nf.getPis() != null) rps.setAliquotaPIS(nf.getPis());
				else rps.setAliquotaPIS(0d);
				
				if(nf.getCofins() != null) rps.setAliquotaCOFINS(nf.getCofins());
				else rps.setAliquotaCOFINS(0d);
				
				if(nf.getInss() != null) rps.setAliquotaINSS(nf.getInss());
				else rps.setAliquotaINSS(0d);
				
				if(nf.getIr() != null) rps.setAliquotaIR(nf.getIr());
				else rps.setAliquotaIR(0d);
				
				if(nf.getCsll() != null) rps.setAliquotaCSLL(nf.getCsll());
				else rps.setAliquotaCSLL(0d);
				
				if(nf.getDadosAdicionais() != null){
					rps.setDescricaoRPS(nf.getDadosAdicionais());
				}
				
				br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Itens itens = new br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Itens();
				rps.setItens(itens);
				
				ListaItem listaItem = new ListaItem();
				itens.setListaItem(listaItem);
				
				for (NotaFiscalServicoItem it : nf.getListaItens()) {
					br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Item item = new br.com.linkcom.lknfe.xml.nfse.prefeituracampinas.Item();
					
					String descricao = this.preencheDescricaoItemNfs(it, configuracaonfe);
					item.setDiscriminacaoServico(descricao.substring(0, descricao.length()-1));
					item.setQuantidade(it.getQtde());
					item.setValorUnitario(it.getPrecoUnitario());
					item.setValorTotal(it.getTotalParcial().getValue().doubleValue());
					
					listaItem.addItem(item);
				}
				
				listaRPS.addRPS(rps);
			}
		}
		
		return reqEnvioLoteRPS;
	}
	
	/**
	 * Cria o XML de consulta de situação do lote de NF de Serviço Eletrônica.
	 *
	 * @param empresa
	 * @param numeroProtocolo
	 * @param configuracaonfe
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public ConsultarSituacaoLoteRpsEnvio createXmlConsultaSituacaoLoteNfse(Empresa empresa, String numeroProtocolo, Configuracaonfe configuracaonfe){
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		ConsultarSituacaoLoteRpsEnvio consultarSituacaoLoteRpsEnvio = new ConsultarSituacaoLoteRpsEnvio(prefixowebservice.getPrefixo());
		
		Prestador prestador = new Prestador(prefixowebservice.getPrefixo(), false, false, true);
		consultarSituacaoLoteRpsEnvio.setPrestador(prestador);
		
		prestador.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
		prestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		
		consultarSituacaoLoteRpsEnvio.setProtocolo(numeroProtocolo);
		
		return consultarSituacaoLoteRpsEnvio;
	}
	
	public ConsultarNfseRpsEnvio createXmlConsultaNfse(List<Arquivonfnota> listaArquivonfnota, Empresa empresa, String numeroProtocolo, Configuracaonfe configuracaonfe){
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		ConsultarNfseRpsEnvio consultarNfseRpsEnvio = new ConsultarNfseRpsEnvio(prefixowebservice.getPrefixo());
		
		CpfCnpj cpfCnpj = new CpfCnpj(prefixowebservice.getPrefixo());
        cpfCnpj.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
        
		br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador prestador = new br.com.linkcom.lknfe.xml.nfse.abrasf.issonline.Prestador(prefixowebservice.getPrefixo());
		prestador.setCpfCnpj(cpfCnpj);
		prestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		consultarNfseRpsEnvio.setPrestador(prestador);
		
		IdentificacaoRps identificacaoRps = new IdentificacaoRps(prefixowebservice.getPrefixo());
		
		if(SinedUtil.isListNotEmpty(listaArquivonfnota)){
			for(Arquivonfnota arquivonfnota : listaArquivonfnota){
				if(arquivonfnota.getNota() != null){
					identificacaoRps.setNumero(StringUtils.soNumero(arquivonfnota.getNota().getNumero()));
					break;
				}
			}
		}
		if(configuracaonfe.getSerienfse() != null && !configuracaonfe.getSerienfse().equals("")){
			identificacaoRps.setSerie(configuracaonfe.getSerienfse());
		} else {
			identificacaoRps.setSerie("A");
		}
		identificacaoRps.setTipo("1");
		consultarNfseRpsEnvio.setIdentificacaoRps(identificacaoRps);
		
		consultarNfseRpsEnvio.setUsuario(configuracaonfe.getLoginparacatu());
		consultarNfseRpsEnvio.setSenha(configuracaonfe.getSenhaparacatu());
		consultarNfseRpsEnvio.setChavePrivada(configuracaonfe.getToken());
		
		return consultarNfseRpsEnvio;
	}
	
	/**
	 * Cria o XML de consulta do lote de NF de Serviço Eletrônica.
	 *
	 * @param empresa
	 * @param numeroProtocolo
	 * @param webservice
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public ConsultarLoteRpsEnvio createXmlConsultaLoteNfse(Empresa empresa, String numeroProtocolo, Configuracaonfe configuracaonfe){
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		String xmlns = "";
		if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_HOM){
			xmlns = "http://nfejf.portalfacil.com.br/homologacao/schema/nfse_v202.xsd";
		} else if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_PROD){
			xmlns = "http://nfejf.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_HOM) {
			xmlns = "http://homologacaosetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_PROD) {
			xmlns = "http://nfsesetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if(prefixowebservice == Prefixowebservice.IPATINGA_HOM) {
            xmlns = "http://nfe.ipatinga.mg.gov.br/homologacao/schema/nfse_v01.xsd";
        } else if(prefixowebservice == Prefixowebservice.IPATINGA_PROD) {
            xmlns = "http://nfeipatinga.portalfacil.com.br/nfseserv/schema/nfse_v01.xsd";
        }
		
		ConsultarLoteRpsEnvio consultarLoteRpsEnvio = new ConsultarLoteRpsEnvio(prefixowebservice.getPrefixo(), xmlns);
		
		Prestador prestador = new Prestador(prefixowebservice.getPrefixo(), false, false, true);
		consultarLoteRpsEnvio.setPrestador(prestador);
		
		prestador.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
		prestador.setInscricaoMunicipal(configuracaonfe.getInscricaomunicipal());
		
		consultarLoteRpsEnvio.setProtocolo(numeroProtocolo);
		
		return consultarLoteRpsEnvio;
	}
	
	/**
	 * Cria o XML de consulta do lote de NF de Produto EletrÃ´nica.
	 *
	 * @param nRecStr
	 * @param oficial
	 * @return
	 * @since 21/03/2012
	 * @author Rodrigo Freitas
	 */
	private ConsReciNFe createXmlConsultaLoteNfe(String nRecStr, Configuracaonfe configuracaonfe) {
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		boolean oficial = prefixowebservice.name().endsWith("PROD");
		
		ConsReciNFe consReciNFe = new ConsReciNFe();
		consReciNFe.setVersao("4.00");
		consReciNFe.setTpAmb(oficial);
		consReciNFe.setnRec(nRecStr);
		
		return consReciNFe;
	}
	
	/**
	 * Cria o XML de cancelamento da NF de Serviço Eletrônica de São Paulo.
	 *
	 * @param numeronfse
	 * @param empresa
	 * @param webservice
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/10/2015
	 */
	public PedidoCancelamentoNFe createXmlCancelamentoNfseSp(String numeronfse, Empresa empresa, String webservice) {
		PedidoCancelamentoNFe pedidoCancelamentoNFe = new PedidoCancelamentoNFe(webservice);
		
		PedidoCancelamentoNFeCabecalho cabecalho = new PedidoCancelamentoNFeCabecalho();
		pedidoCancelamentoNFe.setCabecalho(cabecalho);
		
		CPFCNPJ cpfcnpjRemetente = new CPFCNPJ("Remetente");
		cpfcnpjRemetente.setCnpj(empresa != null ? empresa.getCnpj().getValue() : null);
		cabecalho.setCpfcnpjremetente(cpfcnpjRemetente);
		
		cabecalho.setTransacao(Boolean.TRUE);
		cabecalho.setVersao(1);
		
		ListaDetalhe listaDetalhe = new ListaDetalhe();
		pedidoCancelamentoNFe.setListaDetalhe(listaDetalhe);
		
		Detalhe detalhe = new Detalhe();
		listaDetalhe.addDetalhe(detalhe);
		
		ChaveNFe chaveNFe = new ChaveNFe();
		detalhe.setChaveNFe(chaveNFe);
		
		chaveNFe.setInscricaoPrestador(empresa.getInscricaomunicipal());
		chaveNFe.setNumeronfe(Integer.parseInt(StringUtils.soNumero(numeronfse)));
		
		String assinaturaCancelamento = StringUtils.stringCheia(empresa.getInscricaomunicipal(), "0", 8, false) + StringUtils.stringCheia(numeronfse, "0", 12, false);
		detalhe.setAssinaturaCancelamento(assinaturaCancelamento);
		
		return pedidoCancelamentoNFe;
	}
	
	/**
	 * Cria o XML de cancelamento da NF de Serviço Eletrônica do fornecedor Smarpd
	 *
	 * @param numeronfse
	 * @param empresa
	 * @param motivo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/10/2016
	 */
	public NfdCancelamento createXmlCancelamentoNfseSmarpd(String numeronfse, Configuracaonfe configuracaonfe, String motivo) {
		NfdCancelamento nfdCancelamento = new NfdCancelamento();
		
		nfdCancelamento.setDaracancelamento(SinedDateUtils.currentDate());
		nfdCancelamento.setInscricaomunicipalemissor(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()));
		nfdCancelamento.setMotivocancelamento(motivo);
		nfdCancelamento.setNumeronf(numeronfse);
		
		return nfdCancelamento;
	}
	
	/**
	 * Cria o XML de cancelamento da NF de Serviço Eletrônica.
	 *
	 * @param numeronfse
	 * @param empresa
	 * @return
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 * @param bean 
	 */
	public CancelarNfseEnvio createXmlCancelamentoNfse(String numeronfse, String codigoVerificacao, Empresa empresa, Prefixowebservice prefixowebservice, String sufixo_webservice, Configuracaonfe configuracaonfe, CancelamentoNfseBean bean) {
		String webservice = prefixowebservice.getPrefixo();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		String xmlns = "";
		if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_HOM){
			xmlns = "http://nfejf.portalfacil.com.br/homologacao/schema/nfse_v202.xsd";
		} else if(prefixowebservice == Prefixowebservice.JUIZDEFORAISS_PROD){
			xmlns = "http://nfejf.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_HOM) {
			xmlns = "http://homologacaosetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if (prefixowebservice == Prefixowebservice.SETELAGOAS_NOVO_PROD) {
			xmlns = "http://nfsesetelagoas.portalfacil.com.br/nfseserv/schema/nfse_v202.xsd";
		} else if(prefixowebservice == Prefixowebservice.IPATINGA_HOM) {
            xmlns = "http://nfeipatinga.portalfacil.com.br/homologacao/schema/nfse_v01.xsd";
        } else if(prefixowebservice == Prefixowebservice.IPATINGA_PROD) {
            xmlns = "http://nfeipatinga.portalfacil.com.br/nfseserv/schema/nfse_v01.xsd";
        }
		
		if (Prefixowebservice.JUAZEIRO_HOM.equals(prefixowebservice) || Prefixowebservice.JUAZEIRO_PROD.equals(prefixowebservice)) {
			numeronfse = numeronfse.substring(4, numeronfse.length());
			numeronfse = Integer.valueOf(numeronfse).toString();
		}
		
		CancelarNfseEnvio cancelarNfseEnvio = new CancelarNfseEnvio(webservice, sufixo_webservice, xmlns);
		
		Prestador prestador = new Prestador(webservice, false, true, false);
		cancelarNfseEnvio.setPrestador(prestador);
		
		prestador.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
		prestador.setInscricaoMunicipal(empresa.getInscricaomunicipal());
		
		cancelarNfseEnvio.setNumeroNfse(numeronfse);
		cancelarNfseEnvio.setChave(configuracaonfe.getToken());
		cancelarNfseEnvio.setUsuario(configuracaonfe.getLoginparacatu());
		cancelarNfseEnvio.setSenha(configuracaonfe.getSenhaparacatu());
		
		Pedido pedido = new Pedido(webservice);
		cancelarNfseEnvio.setPedido(pedido);
		
		InfPedidoCancelamento infPedidoCancelamento = new InfPedidoCancelamento(webservice);
		pedido.setInfPedidoCancelamento(infPedidoCancelamento);
		
		infPedidoCancelamento.setId("unico");
		
		if (prefixowebservice == Prefixowebservice.CHAPECO_HOM || prefixowebservice == Prefixowebservice.CHAPECO_PROD) {
			infPedidoCancelamento.setCodigocancelamento("C001");
		}else if (prefixowebservice == Prefixowebservice.ITUMBIARA_HOM || prefixowebservice == Prefixowebservice.ITUMBIARA_PROD) {
			infPedidoCancelamento.setCodigocancelamento("EE");
			infPedidoCancelamento.setDescricaoCancelamento(bean.getMotivo());
		} else {
			infPedidoCancelamento.setCodigocancelamento("2");
		}
		
		IdentificacaoNfse identificacaoNfse = new IdentificacaoNfse(webservice);
		infPedidoCancelamento.setIdentificacaoNfse(identificacaoNfse);
		
		identificacaoNfse.setNumero(numeronfse);
		identificacaoNfse.setCnpj(StringUtils.soNumero(empresa.getCnpj().getValue()));
		identificacaoNfse.setInscricaomunicipal(configuracaonfe.getInscricaomunicipal());
		identificacaoNfse.setCodigomunicipio(empresa.getEndereco().getMunicipio().getCdibge());
		identificacaoNfse.setCodigoverificacao(codigoVerificacao);
		
		return cancelarNfseEnvio;
	}
	
	/**
	 * Cria o XML para o cancelamento de NF-e
	 *
	 * @param chaveacesso
	 * @param protocolonfe
	 * @param motivo
	 * @param prefixowebservice
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public CancNFe createXmlCancelamentoNfe(String chaveacesso, String protocolonfe, String motivo, Prefixowebservice prefixowebservice) {
		CancNFe cancNFe = new CancNFe();
		
		cancNFe.setVersao("2.00");
		
		InfCanc infCanc = new InfCanc();
		cancNFe.setInfCanc(infCanc);
		
		infCanc.setId("ID" + chaveacesso);
		infCanc.setxServ("CANCELAR");
		infCanc.setChNFe(chaveacesso);
		infCanc.setnProt(protocolonfe);
		infCanc.setxJust(motivo);
		
		int tpAmb = 1;
		if(prefixowebservice.name().endsWith("_HOM")){
			tpAmb = 2;
		}
		infCanc.setTpAmb(tpAmb);
		
		return cancNFe;
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoretornoconsulta(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoconsulta(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoretornoconsulta(arquivonf);
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#updateArquivoretornoconsultasituacao(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoconsultasituacao(Arquivonf arquivonf) {
		arquivonfDAO.updateArquivoretornoconsultasituacao(arquivonf);
	}
	
	/**
	 * Processa o retorno da consulta de NF-e do Sefaz
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#findByChaveAcesso(Arquivonf arquivonf, String chNFe)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#updateArquivonfnota(Arquivonfnota arquivonfnota, Timestamp dataRec, String nProtStr)
	 * @see br.com.linkcom.sined.geral.service.NotaService#alterarStatusAcao(Nota nota, String observacao, boolean cancelarDocumentos)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao)
	 *
	 * @param arquivonf
	 * @param xml
	 * @param prefixowebservice
	 * @throws ParseException
	 * @throws JDOMException
	 * @throws IOException
	 * @since 22/03/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaSefaz(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) throws ParseException, JDOMException, IOException{
		String strNovo = new String(Util.strings.tiraAcento(xml).getBytes(),"UTF-8");
		Element retConsReciNFe = SinedUtil.getRootElementXML(strNovo.getBytes());
		
		Element cStat = SinedUtil.getChildElement("cStat", retConsReciNFe.getContent());
		String cStatStr = cStat != null ? cStat.getValue() : "";
		
		if(cStatStr.equals("104")){
			List<Element> listaProtNFe = SinedUtil.getListChildElement("protNFe", retConsReciNFe.getContent());
			
			processaRetornoConsultaSefazLoteSucesso(listaProtNFe, arquivonf);
			
			return true;
		}else if(cStatStr.equals("105")){
			
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retConsReciNFe.getContent());
			String xMotivoStr = xMotivo.getValue();
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xMotivoStr);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			
			return false;
		}else {
			List<Arquivonfnota> listaArquivoNfNota = arquivonfnotaService.findByArquivonf(arquivonf);
			
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retConsReciNFe.getContent());
			String xMotivoStr = "";
			if(xMotivo != null){
				xMotivoStr = xMotivo.getValue();
			}
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xMotivoStr);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			
			for (Arquivonfnota arquivoNfNota : listaArquivoNfNota) {
				if(!NotaStatus.NFE_EMITIDA.equals(arquivoNfNota.getNota().getNotaStatus()) && !NotaStatus.NFE_LIQUIDADA.equals(arquivoNfNota.getNota().getNotaStatus())){
					arquivoNfNota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivoNfNota.getNota(), xMotivoStr, null);
				}
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
			}
			
			return true;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoConsultaSefazLoteSucesso(List<Element> listaProtNFe, Arquivonf arquivonf) throws ParseException {
		boolean sucesso = true;
		
		String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
		List<String> ids = new ArrayList<String>();
		for (Element protNFe : listaProtNFe) {
			Element infProt = SinedUtil.getChildElement("infProt", protNFe.getContent());
			
			Element cStat_NFe = SinedUtil.getChildElement("cStat", infProt.getContent());
			String cStatStr_NFe = cStat_NFe.getValue();
			
			Element xMotivo_NFe = SinedUtil.getChildElement("xMotivo", infProt.getContent());
			String xMotivoStr_NFe = xMotivo_NFe.getValue();
			
			Element chNFe = SinedUtil.getChildElement("chNFe", infProt.getContent());
			String chNFeStr = chNFe.getValue();
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByChaveAcesso(arquivonf, chNFeStr);
			
			if(cStatStr_NFe.equals("100") || cStatStr_NFe.equals("110") || cStatStr_NFe.equals("301") || cStatStr_NFe.equals("302")){
				Element dhRecbto = SinedUtil.getChildElement("dhRecbto", infProt.getContent());
				String dhRecbtoStr = dhRecbto != null ? dhRecbto.getValue() : null;
				Timestamp dataRec = null;
				if(dhRecbtoStr != null){
					dataRec = new Timestamp(LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr).getTime());
				} else {
					dataRec = new Timestamp(System.currentTimeMillis());
				}
				
				Element nProt = SinedUtil.getChildElement("nProt", infProt.getContent());
				String nProtStr = nProt != null ? nProt.getValue() : "";
				
				if(arquivonfnota != null){
					boolean denegada = false;
					arquivonfnotaService.updateArquivonfnota(arquivonfnota, dataRec, nProtStr);
					boolean notaJaEmitida = false;
					if(cStatStr_NFe.equals("110") || cStatStr_NFe.equals("301") || cStatStr_NFe.equals("302")){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_DENEGADA);
						denegada = true;
					} else if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || 
							notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
						if(!NotaStatus.NFE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_LIQUIDADA);
						}else {
							notaJaEmitida = true;
						}
					} else {
						if(!NotaStatus.NFE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFE_EMITIDA);
						}else {
							notaJaEmitida = true;
						}
					}
					if(!notaJaEmitida){
						notaService.alterarStatusAcao(arquivonfnota.getNota(), xMotivoStr_NFe, null);
					}
					try{
						if(denegada){
							if(parametrogeralService.getBoolean(Parametrogeral.CANCELARVENDA_NFDENEGADA)){
								arquivonfnota.getNota().setCancelarvenda(Boolean.TRUE);
								arquivonfnota.getNota().setDenegada(denegada);
							}
							vendaService.estornarVendaByNota(arquivonfnota.getNota());
							expedicaoService.verificaSituacaoExpedicaoAposCancelamentoNota(arquivonfnota.getNota());
						} else {
							vendaService.movimentacaoEstoqueByEmissaoNota(arquivonfnota.getNota());
							documentoService.updateInfDocumentoByNota(arquivonfnota, chNFeStr);
							if(!notaJaEmitida){
								ids.add(arquivonfnota.getNota().getCdNota().toString());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			} else {
				Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
				if(arquivonfnota != null){
					arquivonfnotaerro.setArquivonfnota(arquivonfnota);
					
					if(arquivonfnota.getNota() != null){
						if(!NotaStatus.NFE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
							notaService.alterarStatusAcao(arquivonfnota.getNota(), xMotivoStr_NFe, null);
						}
					}
				} else {
					arquivonfnotaerro.setArquivonf(arquivonf);
				}
				
				arquivonfnotaerro.setCodigo(cStatStr_NFe);
				arquivonfnotaerro.setMensagem(xMotivoStr_NFe);
				arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
				arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
				
				sucesso = false;
			}
		}
		
		Arquivonfsituacao situacao = Arquivonfsituacao.PROCESSADO_COM_SUCESSO;
		if(!sucesso){
			situacao = Arquivonfsituacao.PROCESSADO_COM_ERRO;
		} 
		
		this.updateSituacao(arquivonf, situacao);
		
		if(ids.size() > 0){
			try{
				if(paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")){
					boolean isNfe = (arquivonf.getConfiguracaonfe() != null && 
									arquivonf.getConfiguracaonfe().getTipoconfiguracaonfe() != null && 
									arquivonf.getConfiguracaonfe().getTipoconfiguracaonfe().equals(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO)) ||
									arquivonf.getConfiguracaonfe() == null ||
									arquivonf.getConfiguracaonfe().getTipoconfiguracaonfe() == null;
					this.enviarNFCliente(CollectionsUtil.concatenate(ids, ","), isNfe, TipoEnvioNFCliente.PRODUTO_PDF, TipoEnvioNFCliente.PRODUTO_XML);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaMemory(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
			
			Element erros = SinedUtil.getChildElement("erros", rootElement.getContent());
			if(erros != null){
				List<Element> listaErro = SinedUtil.getListChildElement("erro", erros.getContent());
				for (Element erro : listaErro) {
					String erroStr = erro.getTextTrim();
					
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem(erroStr);
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
				}
			}
			
			Element avisos = SinedUtil.getChildElement("avisos", rootElement.getContent());
			if(avisos != null){
				List<Element> listaAviso = SinedUtil.getListChildElement("aviso", avisos.getContent());
				for (Element aviso : listaAviso) {
					String avisoStr = aviso.getTextTrim();
					
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem(avisoStr);
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					
					if(avisoStr.contains("na fila de processamento")){
						throw new SinedException("Lote ainda em processamento");
					}
				}
			}
			
			boolean haveNota = false;
			Element notas = SinedUtil.getChildElement("notas", rootElement.getContent());
			if(notas != null){
				List<Element> listaNota = SinedUtil.getListChildElement("nota", notas.getContent());
				if(listaNota != null && listaNota.size() > 0){ 
					haveNota = true;
					
					Map<String, String> mapaNumNfse = new HashMap<String, String>();
					Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
					Map<String, String> mapaDtemissao = new HashMap<String, String>();
					Map<String, String> mapaDtcompetencia = new HashMap<String, String>();
					Map<String, String> mapaDtEmissaoRPS = new HashMap<String, String>();
					
					for (Element nota : listaNota) {
						Element xmlElement = SinedUtil.getChildElement("xml", nota.getContent());
						String xmlString = StringEscapeUtils.unescapeXml(xmlElement.getTextTrim());
						Element rootElementNota = SinedUtil.getRootElementXML(xmlString.getBytes());
						
						Element nfse = SinedUtil.getChildElement("Nfse", rootElementNota.getContent());
						Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
						
						Element numNfse = SinedUtil.getChildElement("Numero", infNfse.getContent());
						Element codVerificacao = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent());
						Element dtEmissao = SinedUtil.getChildElement("DataEmissao", infNfse.getContent());
						Element dtCompetencia = SinedUtil.getChildElement("Competencia", infNfse.getContent());
						Element dtEmissaoRPS = SinedUtil.getChildElement("DataEmissaoRps", infNfse.getContent());
						Element numRPS = SinedUtil.getChildElement("NumeroRps", infNfse.getContent());
						
						mapaNumNfse.put(numRPS.getValue(), numNfse.getValue());
						mapaCodVerificacao.put(numRPS.getValue(), codVerificacao.getValue());
						mapaDtemissao.put(numRPS.getValue(), dtEmissao.getValue());
						mapaDtEmissaoRPS.put(numRPS.getValue(), dtEmissaoRPS.getValue());
						mapaDtcompetencia.put(numRPS.getValue(), dtCompetencia.getValue());
					}
					
					this.processNfseSuccess(arquivonf, mapaNumNfse, mapaCodVerificacao, mapaDtemissao, mapaDtcompetencia, mapaDtEmissaoRPS);
				}
			}
			
			if(!haveNota){
				this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NF-e rejeitado.", null);
					}
				}
			}
			
			return true;
		} catch (SinedException e) {
			return false;		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaSmarapd(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
		
			Element nfdok = SinedUtil.getChildElement("nfdok", rootElement.getContent());
			if(nfdok != null){
				Element newDataSet = SinedUtil.getChildElement("NewDataSet", nfdok.getContent());
				if(newDataSet != null){
					Element nota_fiscal = SinedUtil.getChildElement("NOTA_FISCAL", newDataSet.getContent());
					if(nota_fiscal != null){
						Element numeroNota = SinedUtil.getChildElement("NumeroNota", nota_fiscal.getContent());
						Element numeroRps = SinedUtil.getChildElement("NumeroRps", nota_fiscal.getContent());
						Element chaveValidacao = SinedUtil.getChildElement("ChaveValidacao", nota_fiscal.getContent());
						
						if(numeroNota != null && numeroRps != null && chaveValidacao != null){
							String numeroNfse = numeroNota.getTextTrim();
							String codigoVerificacao = chaveValidacao.getTextTrim();
							
							List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
							for (Arquivonfnota arquivonfnota : listan) {
								if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
									documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
									try {
										vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								
								arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao);
								
								if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
									if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
										arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
										notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
									}
								} else {
									if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
										arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
										notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
									}
								}
							}
							
							this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
							String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
							try {
								if (paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")) {
									this.enviarNFCliente(CollectionsUtil.listAndConcatenate(listan, "nota.cdNota", ","), TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							return true;
						}
					}
				}
			}
		
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NF-e rejeitado.", null);
			}
			
			return true;
		} catch (SinedException e) {
			return false;		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaCampinas(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			System.out.println("XML ############ " + xml);
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
			
			boolean sucesso = false;
			Element cabecalhoElement = SinedUtil.getChildElement("Cabecalho", rootElement.getContent());
			if(cabecalhoElement != null){
				Element sucessoElement = SinedUtil.getChildElement("Sucesso", cabecalhoElement.getContent());
				if(sucessoElement != null){
					String sucessoStr = sucessoElement.getText();
					sucesso = sucessoStr != null && sucessoStr.trim().toLowerCase().equals("true");
				}
			}
			
			
			if(sucesso){
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
			
				Element listaNFSe = SinedUtil.getChildElement("ListaNFSe", rootElement.getContent());
				if(listaNFSe != null){
					Map<String, String> mapaNumNfse = new HashMap<String, String>();
					Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
					
					List<Element> listaConsultaNFSe = SinedUtil.getListChildElement("ConsultaNFSe", listaNFSe.getContent());
					
					for (Element consultaNFSe : listaConsultaNFSe) {
						Element numeroNFe = SinedUtil.getChildElement("NumeroNFe", consultaNFSe.getContent());
						Element codigoVerificacao = SinedUtil.getChildElement("CodigoVerificacao", consultaNFSe.getContent());
						Element numeroRPS = SinedUtil.getChildElement("NumeroRPS", consultaNFSe.getContent());
						
						String numNota = numeroRPS.getText();
						String numNfse = numeroNFe.getText();
						String codVerificacao = codigoVerificacao.getText();
						
						mapaNumNfse.put(numNota, numNfse);
						mapaCodVerificacao.put(numNota, codVerificacao);
					}
					
					String numeroNfse, codigoVerificacao;
					List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
					for (Arquivonfnota arquivonfnota : listan) {
						numeroNfse = mapaNumNfse.get(arquivonfnota.getNota().getNumero());
						codigoVerificacao = mapaCodVerificacao.get(arquivonfnota.getNota().getNumero());
						
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
							documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
							try {
								vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						
						arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao);
						
						if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) ||
								notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
						} else {
							arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
						}
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
					}
				}
			} else {
				Element errosElement = SinedUtil.getChildElement("Erros", rootElement.getContent());
				Element alertasElement = SinedUtil.getChildElement("Alertas", rootElement.getContent());
				if(alertasElement != null && alertasElement.getContent().size() > 0){
					List<Element> listaAlertaElement = SinedUtil.getListChildElement("Alerta", alertasElement.getContent());
					for (Element alertaElement : listaAlertaElement) {
						Element codigoElement = SinedUtil.getChildElement("Codigo", alertaElement.getContent());
						if(codigoElement != null && codigoElement.getText() != null && codigoElement.getText().trim().equals("203")){
							return false;
						}
					}
				}
				
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
				}
				
				
				if(errosElement != null){
					List<Element> listaErroElement = SinedUtil.getListChildElement("Erro", errosElement.getContent());
					if(listaErroElement != null){
						for (Element erroElement : listaErroElement) {
							String descricao = null;
							
							Element descricaoElement = SinedUtil.getChildElement("Descricao", erroElement.getContent());
							if(descricaoElement != null){
								descricao = descricaoElement.getText();
							}
							
							if(descricao != null){
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(descricao);
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						}
					}
				}
			}
			
			return true;
		} catch (SinedException e) {
			e.printStackTrace();
			return false;		
		} catch (Exception e) {
			System.out.println("XML ############ " + xml);
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Processa o retorno da consulta do lote
	 *
	 * @param arquivonf
	 * @param content
	 * @param prefixowebservice
	 * @since 23/02/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaBhiss(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
			
			String prefixo = prefixowebservice.getPrefixo();
			if(prefixo.equals("issonline") || prefixo.equals("supernova")){
				Element situacaoElement = SinedUtil.getChildElement("Situacao", rootElement.getContent());
				if(situacaoElement != null && situacaoElement.getText() != null && situacaoElement.getText().trim().equals("2")){
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem("Lote ainda em processamento");
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					
					throw new SinedException("Lote ainda em processamento");
				}
				

				if(situacaoElement != null && situacaoElement.getText() != null && situacaoElement.getText().trim().equals("3")){
					Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
					if(listaMensagemRetorno != null){
						List<Element> listaMensagemIssonlineElement = SinedUtil.getListChildElement("Mensagem", listaMensagemRetorno.getContent());
						if(listaMensagemIssonlineElement != null){
							for (Element msg : listaMensagemIssonlineElement) {
								if(msg.getText() != null && msg.getText().toUpperCase().indexOf("FAVOR AGUARDAR PROCESSAMENTO DA(S) NOTA(S) GERADA(S) PELO PROTOCOLO") != -1){
									Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
									
									arquivonfnotaerro.setArquivonf(arquivonf);
									arquivonfnotaerro.setMensagem(msg.getText());
									arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
									
									arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
									
									throw new SinedException("Lote ainda em processamento");
								}
							}
						}
					}
				}
			}
			
			Element listaNfse = SinedUtil.getChildElement("ListaNfse", rootElement.getContent());
			
			if ((prefixo.equals("sigbancos") && !"ListaNfse".equals(listaNfse.getName())) || 
					prefixo.equals("sigep") || 
					(prefixo.equals("manaus") && "CompNfse".equals(rootElement.getName())) ||
					(prefixo.equals("bhiss") && "ConsultarNfseRpsResposta".equals(rootElement.getName()))) {
				listaNfse = rootElement;
			}
			
			Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetornoLote", rootElement.getContent());
			
			if(listaMensagemRetorno == null || (listaMensagemRetorno != null && listaMensagemRetorno.getContent() != null && listaMensagemRetorno.getContent().size() <= 0)){
				listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
			}
			if(listaMensagemRetorno == null || (listaMensagemRetorno != null && listaMensagemRetorno.getContent() != null && listaMensagemRetorno.getContent().size() <= 0)){
				listaMensagemRetorno = SinedUtil.getChildElement("Listamensagemretorno", rootElement.getContent());
			}
			List<Element> mensagens = null;
			if(listaMensagemRetorno == null || (listaMensagemRetorno != null && listaMensagemRetorno.getContent() != null && listaMensagemRetorno.getContent().size() <= 0)){
				mensagens = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
			}
			if(prefixowebservice.getPrefixo().equals("fisslex")){
				Element consultarloterpsrespostaElement = SinedUtil.getChildElement("Consultarloterpsresposta", rootElement.getContent());
				String listaNfseStr = consultarloterpsrespostaElement.getValue();
				listaNfse = SinedUtil.getRootElementXML(listaNfseStr.getBytes());
			}
			
			if(listaNfse != null && listaNfse.getContent() != null && listaNfse.getContent().size() > 0){
				Map<String, String> mapaNumNfse = new HashMap<String, String>();
				Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
				Map<String, String> mapaDtemissao = new HashMap<String, String>();
				Map<String, String> mapaDtcompetencia = new HashMap<String, String>();
				Map<String, String> mapaDtEmissaoRPS = new HashMap<String, String>();
				
				List<Element> listaCompNfse = listaNfse.getContent();
				Element nfse, infNfse, numNfse, codVerificacao, idRps, num, dtEmissao, dtCompetencia, dtEmissaoRPS;
				for (Object obj : listaCompNfse) {
					if(obj instanceof Element){
						Element compNfse = (Element) obj;
						
						if (compNfse != null && ("Signature".equals(compNfse.getName()) || "ListaMensagemRetorno".equals(compNfse.getName()) || "ListaMensagemAlertaRetorno".equals(compNfse.getName()))) {
							continue;
						}
						
						nfse = SinedUtil.getChildElement("Nfse", compNfse.getContent());
						if(nfse == null){
							Element tcCompNfse = SinedUtil.getChildElement("tcCompNfse", compNfse.getContent());
							if (tcCompNfse != null) {
								nfse = SinedUtil.getChildElement("Nfse", tcCompNfse.getContent());
							} else if ((prefixo.equals("sigbancos") || prefixo.equals("manaus")) && compNfse != null && "Nfse".equals(compNfse.getName())) {
								nfse = compNfse;
							}
						}
						infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
						
						numNfse = SinedUtil.getChildElement("Numero", infNfse.getContent());
						codVerificacao = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent());
						dtEmissao = SinedUtil.getChildElement("DataEmissao", infNfse.getContent());
						dtCompetencia = SinedUtil.getChildElement("Competencia", infNfse.getContent());
						dtEmissaoRPS = SinedUtil.getChildElement("DataEmissaoRps", infNfse.getContent());
						
						idRps = SinedUtil.getChildElement("IdentificacaoRps", infNfse.getContent());
						
						if(idRps == null){
							Element declaracaoPrestacaoServico = SinedUtil.getChildElement("DeclaracaoPrestacaoServico", infNfse.getContent());
							if(declaracaoPrestacaoServico != null){
								Element rps = SinedUtil.getChildElement("Rps", declaracaoPrestacaoServico.getContent());
								if(rps == null){
									Element infDeclaracaoPrestacaoServico = SinedUtil.getChildElement("InfDeclaracaoPrestacaoServico", declaracaoPrestacaoServico.getContent());
									if(infDeclaracaoPrestacaoServico != null){
										rps = SinedUtil.getChildElement("Rps", infDeclaracaoPrestacaoServico.getContent());
									}
								}
								if(rps != null){
									idRps = SinedUtil.getChildElement("IdentificacaoRps", rps.getContent());
								}
								if(idRps == null){
									idRps = SinedUtil.getChildElement("IdentificacaoRps", declaracaoPrestacaoServico.getContent());
								}
							}
						}
						num = SinedUtil.getChildElement("Numero", idRps.getContent());
						
						mapaNumNfse.put(num.getValue(), numNfse.getValue());
						mapaCodVerificacao.put(num.getValue(), codVerificacao.getValue());
						mapaDtemissao.put(num.getValue(), dtEmissao.getValue());
						if(dtEmissaoRPS != null) mapaDtEmissaoRPS.put(num.getValue(), dtEmissaoRPS.getValue());
						if(dtCompetencia != null){
							mapaDtcompetencia.put(num.getValue(), dtCompetencia.getValue());
						} else {
							mapaDtcompetencia.put(num.getValue(), dtEmissao.getValue());
						}
					}
				}
				
				this.processNfseSuccess(arquivonf, mapaNumNfse, mapaCodVerificacao, mapaDtemissao, mapaDtcompetencia, mapaDtEmissaoRPS);
			} else if(listaMensagemRetorno != null || mensagens != null){
				Map<String, List<String>> mapaMensagens = new HashMap<String, List<String>>();
				Element mensagemElt, identRpsElt, numeroElt, correcaoElt, codigoElt;
				Arquivonfnotaerro arquivonfnotaerro;
				List<String> listaMsg;
				Boolean emProcessamento = Boolean.FALSE;
				
				Map<String, String> map = new HashMap<String, String>();
				Map<String, String> mapMensagemCodigo = new HashMap<String, String>();
				
				if(listaMensagemRetorno != null){
					List<Element> listaMensagemIssonlineElement = SinedUtil.getListChildElement("Mensagem", listaMensagemRetorno.getContent());
					for (Element msg : listaMensagemIssonlineElement) {
						arquivonfnotaerro = new Arquivonfnotaerro();
						
						arquivonfnotaerro.setArquivonf(arquivonf);
						arquivonfnotaerro.setMensagem(msg.getText());
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
					
					List<Element> listaMensagemElement = SinedUtil.getListChildElement("MensagemRetorno", listaMensagemRetorno.getContent());
					if(listaMensagemElement == null || listaMensagemElement.size() == 0){
						listaMensagemElement = SinedUtil.getListChildElement("tcMensagemRetorno", listaMensagemRetorno.getContent());
					}
					for (Element msg : listaMensagemElement) {
						identRpsElt = SinedUtil.getChildElement("IdentificacaoRps", msg.getContent());
						
						if(identRpsElt != null){
							numeroElt = SinedUtil.getChildElement("Numero", identRpsElt.getContent());
							mensagemElt = SinedUtil.getChildElement("Mensagem", msg.getContent());
							codigoElt = SinedUtil.getChildElement("Codigo", msg.getContent());
							if(numeroElt == null){
								numeroElt = SinedUtil.getChildElement("NumeroRps", identRpsElt.getContent());
							}
							
							listaMsg = mapaMensagens.get(numeroElt.getValue());
							if(listaMsg == null){
								listaMsg = new ArrayList<String>();
							}
							listaMsg.add(mensagemElt.getValue());
							
							mapaMensagens.put(numeroElt.getValue(), listaMsg);
							mapMensagemCodigo.put(mensagemElt.getValue(), codigoElt != null ? codigoElt.getValue() : null);
						} else {
							codigoElt = SinedUtil.getChildElement("Codigo", msg.getContent());
							mensagemElt = SinedUtil.getChildElement("Mensagem", msg.getContent());
							if(codigoElt == null) continue;
							
							correcaoElt = SinedUtil.getChildElement("Correcao", msg.getContent());
							
							if(codigoElt != null && 
									(
										(codigoElt.getValue().equals("E178") && prefixo.equals("saatri")) || 
										(codigoElt.getValue().equals("0") && prefixo.equals("bhiss")) ||
										(codigoElt.getValue().equals("86") && prefixo.equals("bhiss")) ||
										(codigoElt.getValue().equals("180") && prefixo.equals("bhiss")) ||
										codigoElt.getValue().equals("99999")
									)
								){
								arquivonfnotaerro = new Arquivonfnotaerro();
								
								String msgErro = "";
								if(mensagemElt != null && org.apache.commons.lang.StringUtils.isNotBlank(mensagemElt.getTextTrim())){
									msgErro = " (" + mensagemElt.getTextTrim() + ")";
								}
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem("Lote ainda em processamento." + msgErro);
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
								
								throw new SinedException("Lote ainda em processamento");
							} else if ("E179".equals(codigoElt.getValue()) && "eireli".equals(prefixo)) {
								emProcessamento = Boolean.TRUE;
							}
							
							map.put(mensagemElt.getValue(), correcaoElt != null ? correcaoElt.getValue() : "");
							mapMensagemCodigo.put(mensagemElt.getValue(), codigoElt != null ? codigoElt.getValue() : null);
						}
					}
				}
				
				if(mensagens != null){
					for (Element elt : mensagens) {
						arquivonfnotaerro = new Arquivonfnotaerro();
						
						arquivonfnotaerro.setArquivonf(arquivonf);
						arquivonfnotaerro.setMensagem(elt.getText());
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
				}
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				List<Integer> idsNotasSemErro = new ArrayList<Integer>();
				for (Arquivonfnota arquivonfnota : listan) {
					if(mapaMensagens.size() > 0){
						try {
							listaMsg = mapaMensagens.get(arquivonfnota.getNota().getNumero());
							
							if (listaMsg == null) {
								listaMsg = mapaMensagens.get(Integer.parseInt(arquivonfnota.getNota().getNumero()) + "");
							}
						} catch (Exception e) {
							listaMsg = null;
						}
						
						if(listaMsg != null){
							for (String string : listaMsg) {
								arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonfnota(arquivonfnota);
								arquivonfnotaerro.setMensagem(string);
								arquivonfnotaerro.setCodigo(mapMensagemCodigo.get(string));
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
							}
						} else {
							idsNotasSemErro.add(arquivonfnota.getNota().getCdNota());
						}
					}
					if (emProcessamento) {
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Lote ainda em processamento. Tente novamente mais tarde.", null);
					} else if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com erro.", null);
					}
				}
				
				if(map.size() > 0){
					Set<Entry<String, String>> entrySet = map.entrySet();
					for (Entry<String, String> entry : entrySet) {
						arquivonfnotaerro = new Arquivonfnotaerro();
						
						arquivonfnotaerro.setArquivonf(arquivonf);
						if(entry.getKey() != null){
							arquivonfnotaerro.setMensagem(entry.getKey().length() > 1000 ? entry.getKey().substring(0, 999) : entry.getKey());
						}
						arquivonfnotaerro.setCodigo(mapMensagemCodigo.get(entry.getKey()));
						arquivonfnotaerro.setCorrecao(entry.getValue());
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
				}
				
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				if(idsNotasSemErro != null && idsNotasSemErro.size() > 0){
					arquivonf = this.loadForEntrada(arquivonf);
					this.createArquivonfByNota(null, CollectionsUtil.concatenate(idsNotasSemErro, ","), arquivonf.getEmpresa(), arquivonf.getConfiguracaonfe());
				}
			} else return false;
			
			return true;
		} catch (SinedException e) {
			return false;		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	private void processNfseSuccess(Arquivonf arquivonf,
			Map<String, String> mapaNumNfse,
			Map<String, String> mapaCodVerificacao,
			Map<String, String> mapaDtemissao,
			Map<String, String> mapaDtcompetencia,
			Map<String, String> mapaDtEmissaoRPS) throws ParseException {
		List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
		String numeroNfse, codigoVerificacao, dtEmissaoStr, dtCompetenciaStr, dtEmissaoRPSStr;
		Date dataEmissao, dataCompetencia;
		
		String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
		for (Arquivonfnota arquivonfnota : listan) {
			numeroNfse = mapaNumNfse.get(arquivonfnota.getNota().getNumero());
			if(org.apache.commons.lang.StringUtils.isBlank(numeroNfse)) continue;
			
			codigoVerificacao = mapaCodVerificacao.get(arquivonfnota.getNota().getNumero());
			dtEmissaoStr = mapaDtemissao.get(arquivonfnota.getNota().getNumero());
			dtCompetenciaStr = mapaDtcompetencia.get(arquivonfnota.getNota().getNumero());
			dtEmissaoRPSStr = mapaDtEmissaoRPS.get(arquivonfnota.getNota().getNumero());
			
			dtEmissaoStr = dtEmissaoStr.replaceAll("T", "//");
			try{
				dataEmissao = FORMAT_DATETIME.parse(dtEmissaoStr);
			} catch (Exception e) {
				try {
					dataEmissao = FORMAT_DATETIME_PARACATU.parse(dtEmissaoStr);
				} catch (Exception ex) {
					try {
						dataEmissao = FORMAT_DATETIME_SIGBANCOS.parse(dtEmissaoStr);
					} catch (Exception e2) {
						dataEmissao = FORMAT_DATETIME_EEL.parse(dtEmissaoStr);
					} 
				}
			}
			
			if(dtCompetenciaStr.length() >= 10){
				dtCompetenciaStr = dtCompetenciaStr.substring(0, 10);
				try {
					dataCompetencia = new SimpleDateFormat("yyyy-MM-dd").parse(dtCompetenciaStr);
				} catch (Exception e) {
					dataCompetencia = new SimpleDateFormat("dd/MM/yyyy").parse(dtCompetenciaStr);
				}
			} else if(dtEmissaoRPSStr != null && dtEmissaoRPSStr.length() >= 10){
				dtEmissaoRPSStr = dtEmissaoRPSStr.substring(0, 10);
				try{
					dataCompetencia = new SimpleDateFormat("yyyy-MM-dd").parse(dtEmissaoRPSStr);
				} catch (Exception e) {
					dataCompetencia = new SimpleDateFormat("dd/MM/yyyy").parse(dtEmissaoRPSStr);
				}
			} else {
				dataCompetencia = dataEmissao;
			}
			
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
				documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
				try {
					vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			arquivonfnotaService.updateNumeroCodigo(arquivonfnota, StringUtils.soNumero(numeroNfse), codigoVerificacao, new Timestamp(dataEmissao.getTime()), dataCompetencia);
			
			if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || 
					notaService.isNotaPossuiReceita(arquivonfnota.getNota(), true)){
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
			} else {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
			}
			notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
		}
		
		this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		
		try{
			if(paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")){
				this.enviarNFCliente(CollectionsUtil.listAndConcatenate(listan, "nota.cdNota", ","), TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Processa o retorno da consulta da situação do lote
	 *
	 * @param arquivonf
	 * @param content
	 * @param prefixowebservice
	 * @since 23/02/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsultaSituacaoBhiss(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());

			if(prefixowebservice.getPrefixo().equals("fisslex")){
				rootElement = SinedUtil.getChildElement("Consultarsituacaoloterpsresposta", rootElement.getContent());
			}
			
			Element situacao = SinedUtil.getChildElement("Situacao", rootElement.getContent());
			if(situacao == null){
				situacao = SinedUtil.getChildElement("situacaoLoteRps", rootElement.getContent());
			}
			if (situacao != null) {
				String situacaoString = situacao.getValue();
				if (situacaoString.equals("2")) {
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem("Lote ainda não processado.");
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					
					MarcarArquivonfBean bean = new MarcarArquivonfBean();
					bean.setFlag("consultando");
					bean.setCdarquivonf(arquivonf.getCdarquivonf());
					this.desmarcarArquivonf(bean.getCdarquivonf(), bean.getFlag());
					
					throw new SinedException("Lote ainda não processado.");
				} else if (situacaoString.equals("1")) {
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem("Lote ainda não recebido.");
					arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);			
					
					MarcarArquivonfBean bean = new MarcarArquivonfBean();
					bean.setFlag("consultando");
					bean.setCdarquivonf(arquivonf.getCdarquivonf());
					this.desmarcarArquivonf(bean.getCdarquivonf(), bean.getFlag());
					
					throw new SinedException("Lote ainda não recebido.");
				} else if (situacaoString.equals("0")) {
					Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
					if(listaMensagemRetorno != null){
						List<Element> lista = SinedUtil.getListChildElement("MensagemRetorno", listaMensagemRetorno.getContent());
						if(lista != null && lista.size() > 0){
							for (Element mensagemRetorno : lista) {
								Element mensagem = SinedUtil.getChildElement("Mensagem", mensagemRetorno.getContent());
								Element correcao = SinedUtil.getChildElement("Correcao", mensagemRetorno.getContent());
								
								Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
								
								arquivonfnotaerro.setArquivonf(arquivonf);
								arquivonfnotaerro.setMensagem(mensagem.getText());
								arquivonfnotaerro.setCorrecao(correcao.getText());
								arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
								
								arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);						
							}
						}
					}
				}
			} else {
				Element listaMensagemRetorno = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
				if(listaMensagemRetorno != null){
					List<Element> lista = SinedUtil.getListChildElement("MensagemRetorno", listaMensagemRetorno.getContent());
					if(lista != null && lista.size() > 0){
						for (Element mensagemRetorno : lista) {
							Element mensagem = SinedUtil.getChildElement("Mensagem", mensagemRetorno.getContent());
							Element correcao = SinedUtil.getChildElement("Correcao", mensagemRetorno.getContent());
							
							Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
							
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setMensagem(mensagem.getText());
							arquivonfnotaerro.setCorrecao(correcao.getText());
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);						
						}
					}
				}
			}
			
			return true;
		} catch (Exception e) {
//			e.printStackTrace();
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamentoSp(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice){
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element cabecalhoElt = SinedUtil.getChildElement("Cabecalho", rootElement.getContent());
			if(cabecalhoElt != null){
				Element sucessoElt = SinedUtil.getChildElement("Sucesso", cabecalhoElt.getContent());
				if(sucessoElt != null){
					arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
					Nota nota = arquivonfnota.getNota();
					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
					
					String sucessoStr = sucessoElt.getTextTrim();
					if(sucessoStr != null && sucessoStr.trim().toUpperCase().equals("TRUE")){
						String obs = "Cancelamento na Receita";
						nota.setNotaStatus(NotaStatus.CANCELADA);
						notaService.alterarStatusAcao(nota, obs, null);
						
						arquivonfnotaService.updateCancelado(arquivonfnota);
						
						if(nota.getCdNota() != null){
							requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
							
							List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
							List<Venda> listaVenda = new ArrayList<Venda>();
							for (NotaVenda nv : listaNotaVenda) {
								if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
									listaVenda.add(nv.getVenda());
								}
							}
							
							String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
							if (!whereIn.equals("")){
								vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
							}
							
							notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
						}
					} else {
						List<Element> listaErro = SinedUtil.getListChildElement("Erro", rootElement.getContent());
						for (Element erroElt : listaErro) {
							Element descricaoElt = SinedUtil.getChildElement("Descricao", erroElt.getContent());
							
							Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
							arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
							arquivonfnotaerrocancelamento.setMensagem(descricaoElt != null ? descricaoElt.getText() : "Motivo não identificado.");
							
							arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
						}
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado.";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
					}
					
				} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'Sucesso'.");
			} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'Cabecalho'.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void processaRetornoCancelamentoSmarpd(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice){
		try {
			String mensagem = new String(content);
			
			arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
			Nota nota = arquivonfnota.getNota();
			NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
			
			if(mensagem.trim().equalsIgnoreCase("Nota fiscal cancelada com sucesso.") || mensagem.trim().startsWith("Erro: Essa nota fiscal ja foi cancelada!")){
				String obs = "Cancelamento na Receita";
				nota.setNotaStatus(NotaStatus.CANCELADA);
				notaService.alterarStatusAcao(nota, obs, null);
				
				arquivonfnotaService.updateCancelado(arquivonfnota);
				
				if(nota.getCdNota() != null){
					requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
					
					List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
					List<Venda> listaVenda = new ArrayList<Venda>();
					for (NotaVenda nv : listaNotaVenda) {
						if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
							listaVenda.add(nv.getVenda());
						}
					}
					
					String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
					if (!whereIn.equals("")){
						vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
					}
					
					notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
				}
			} else {
				Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
				arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
				arquivonfnotaerrocancelamento.setMensagem(mensagem);
				
				arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
				
				if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
					String obs = "Cancelamento na Receita não efetuado. " + mensagem;
					nota.setNotaStatus(lastSituacao);
					notaService.alterarStatusAcao(nota, obs, null);
				}
			}
			
			
			
//			Element cabecalhoElt = SinedUtil.getChildElement("Cabecalho", rootElement.getContent());
//			if(cabecalhoElt != null){
//				Element sucessoElt = SinedUtil.getChildElement("Sucesso", cabecalhoElt.getContent());
//				if(sucessoElt != null){
//					arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
//					Nota nota = arquivonfnota.getNota();
//					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
//					
//					String sucessoStr = sucessoElt.getTextTrim();
//					if(sucessoStr != null && sucessoStr.trim().toUpperCase().equals("TRUE")){
//					} else {
//						List<Element> listaErro = SinedUtil.getListChildElement("Erro", rootElement.getContent());
//						for (Element erroElt : listaErro) {
//							Element descricaoElt = SinedUtil.getChildElement("Descricao", erroElt.getContent());
//							
//							Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
//							arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
//							arquivonfnotaerrocancelamento.setMensagem(descricaoElt != null ? descricaoElt.getText() : "Motivo não identificado.");
//							
//							arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
//						}
//						
//						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
//							String obs = "Cancelamento na Receita não efetuado.";
//							nota.setNotaStatus(lastSituacao);
//							notaService.alterarStatusAcao(nota, obs, null);
//						}
//					}
//					
//				} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'Sucesso'.");
//			} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'Cabecalho'.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamentoBhiss(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element listaMensagemRetornoElt = SinedUtil.getChildElement("ListaMensagemRetorno", rootElement.getContent());
			Element retCancelamentoElt = SinedUtil.getChildElement("RetCancelamento", rootElement.getContent());
			
			//GINFES V2
			List<Element> mensagemRetornoElt = SinedUtil.getListChildElement("MensagemRetorno", rootElement.getContent());
			Element sucessoElt = SinedUtil.getChildElement("Sucesso", rootElement.getContent());
			
			// CONTAGEM
			Element cancelamentoElt = SinedUtil.getChildElement("Cancelamento", rootElement.getContent());
			
			arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
			Nota nota = arquivonfnota.getNota();
			NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
			
			if((listaMensagemRetornoElt != null && listaMensagemRetornoElt.getContent() != null && listaMensagemRetornoElt.getContent().size() > 0) || (mensagemRetornoElt != null && mensagemRetornoElt.size() > 0)){
				List<Element> lista = null;
				if(mensagemRetornoElt != null && mensagemRetornoElt.size() > 0){
					lista = mensagemRetornoElt;
				} else {
					lista = listaMensagemRetornoElt.getContent();
				}
				boolean cancelar = false;
				for (Object obj : lista) {
					if(obj != null && obj instanceof Element){
						Element mensagemRetorno = (Element) obj;
						Element msg =  SinedUtil.getChildElement("Mensagem",mensagemRetorno.getContent());
						if(msg != null){
							String strMsg = Util.strings.emptyIfNull(msg.getValue()).trim();
							if("Essa NFS-e já está cancelada".equalsIgnoreCase(strMsg)){
								cancelar = true;
							}else if(org.apache.commons.lang.StringUtils.isNotBlank(strMsg)){
								Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
								arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
								arquivonfnotaerrocancelamento.setMensagem(strMsg);
								
								arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
							}
						}
					}
				}
				if(cancelar){
					boolean notaJaCancelada = notaService.haveNFWithStatus(nota.getCdNota().toString(), NotaStatus.CANCELADA);
					if(!notaJaCancelada){
						String obs = "Cancelamento na Receita";
						nota.setNotaStatus(NotaStatus.CANCELADA);
						notaService.alterarStatusAcao(nota, obs, null);
						
						arquivonfnotaService.updateCancelado(arquivonfnota);
						
						if(nota.getCdNota() != null){
							requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
							
							List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
							List<Venda> listaVenda = new ArrayList<Venda>();
							for (NotaVenda nv : listaNotaVenda) {
								if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
									listaVenda.add(nv.getVenda());
								}
							}
							
							String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
							if (!whereIn.equals("")){
								vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
							}
							
							notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
						}
					}
				}else{
					if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
						String obs = "Cancelamento na Receita não efetuado.";
						nota.setNotaStatus(lastSituacao);
						notaService.alterarStatusAcao(nota, obs, null);
					}
				}
				
			} else if(retCancelamentoElt != null){
				
				String obs = "Cancelamento na Receita";
				nota.setNotaStatus(NotaStatus.CANCELADA);
				notaService.alterarStatusAcao(nota, obs, null);
				
				arquivonfnotaService.updateCancelado(arquivonfnota);
				
				if(nota.getCdNota() != null){
					requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
					
					List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
					List<Venda> listaVenda = new ArrayList<Venda>();
					for (NotaVenda nv : listaNotaVenda) {
						if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
							listaVenda.add(nv.getVenda());
						}
					}
					
					String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
					if (!whereIn.equals("")){
						vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
					}
					
					notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
				}
				
			} else if(cancelamentoElt != null){
				Element confirmacaoElt = SinedUtil.getChildElement("Confirmacao", cancelamentoElt.getContent());;
				if(confirmacaoElt != null){
					String obs = "Cancelamento na Receita";
					nota.setNotaStatus(NotaStatus.CANCELADA);
					notaService.alterarStatusAcao(nota, obs, null);
					
					arquivonfnotaService.updateCancelado(arquivonfnota);
					
					if(nota.getCdNota() != null){
						requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
						
						List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
						List<Venda> listaVenda = new ArrayList<Venda>();
						for (NotaVenda nv : listaNotaVenda) {
							if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
								listaVenda.add(nv.getVenda());
							}
						}
						
						String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
						if (!whereIn.equals("")){
							vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
						}
						
						notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
					}
				}
			} else if(sucessoElt != null){	
				String sucessoStr = sucessoElt.getText();
				if(sucessoStr != null && "TRUE".equals(sucessoStr.trim().toUpperCase())){
					String obs = "Cancelamento na Receita";
					nota.setNotaStatus(NotaStatus.CANCELADA);
					notaService.alterarStatusAcao(nota, obs, null);
					
					arquivonfnotaService.updateCancelado(arquivonfnota);
					
					if(nota.getCdNota() != null){
						requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
						
						List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
						List<Venda> listaVenda = new ArrayList<Venda>();
						for (NotaVenda nv : listaNotaVenda) {
							if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
								listaVenda.add(nv.getVenda());
							}
						}
						
						String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
						if (!whereIn.equals("")){
							vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
						}
						
						notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
					}
				} else {
					if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
						String obs = "Cancelamento na Receita não efetuado.";
						nota.setNotaStatus(lastSituacao);
						notaService.alterarStatusAcao(nota, obs, null);
					}
				}
			} else throw new SinedException("Erro no processamento do retorno do cancelamento.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamentoSefaz(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
		try{
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element infCancElt = SinedUtil.getChildElement("infCanc", rootElement.getContent());
			if(infCancElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			Element cStatElt = SinedUtil.getChildElement("cStat", infCancElt.getContent());
			if(cStatElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
			Nota nota = arquivonfnota.getNota();
			
			String cStat = cStatElt.getText();
			if(cStat != null && cStat.trim().equals("101")){
				executarCancelamentoRetornoSefaz(nota, arquivonfnota);
			} else if (cStat != null && cStat.trim().equals("155")) {
				Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infCancElt.getContent());
				
				executarCancelamentoRetornoSefaz(nota, arquivonfnota, xMotivoElt.getText());
			} else {
				Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infCancElt.getContent());
				
				Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
				arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
				arquivonfnotaerrocancelamento.setMensagem(xMotivoElt.getText());
				
				arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
				
				if("573 - Rejeicao: Duplicidade de Evento (tpEvento + chNFe + nSeqEvento)".equalsIgnoreCase(arquivonfnotaerrocancelamento.getMensagem())){
					executarCancelamentoRetornoSefaz(nota, arquivonfnota);
				}else if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA);
					String obs = "Cancelamento na Receita não efetuado.";
					if(lastSituacao == null) lastSituacao = NotaStatus.NFE_EMITIDA;
					nota.setNotaStatus(lastSituacao);
					notaService.alterarStatusAcao(nota, obs, null);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void executarCancelamentoRetornoSefaz(Nota nota, Arquivonfnota arquivonfnota) {
		String obs = "Cancelamento na Receita";
		nota.setNotaStatus(NotaStatus.CANCELADA);
		notaService.alterarStatusAcao(nota, obs, null);
		
		arquivonfnotaService.updateCancelado(arquivonfnota);
		
		vendaService.estornarVendaByNota(nota);
		expedicaoService.verificaSituacaoExpedicaoAposCancelamentoNota(nota);
	}
	
	private void executarCancelamentoRetornoSefaz(Nota nota, Arquivonfnota arquivonfnota, String observacao) {
		String obs = observacao;
		
		nota.setNotaStatus(NotaStatus.CANCELADA);
		notaService.alterarStatusAcao(nota, obs, null);
		
		arquivonfnotaService.updateCancelado(arquivonfnota);
		
		vendaService.estornarVendaByNota(nota);
		expedicaoService.verificaSituacaoExpedicaoAposCancelamentoNota(nota);
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamentoPorEventoSefaz(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
		try{
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element cStatElt = SinedUtil.getChildElement("cStat", rootElement.getContent());
			if(cStatElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
			Nota nota = arquivonfnota.getNota();
			
			String cStatPrincipal = cStatElt.getText();
			if(cStatPrincipal != null && (cStatPrincipal.trim().equals("128"))){
				
				Element retEventoElt = SinedUtil.getChildElement("retEvento", rootElement.getContent());
				if(retEventoElt == null) throw new SinedException("Erro no processamento do retorno.");
				
				Element infEventoElt = SinedUtil.getChildElement("infEvento", retEventoElt.getContent());
				if(infEventoElt == null) throw new SinedException("Erro no processamento do retorno.");
				
				Element cStatElt2 = SinedUtil.getChildElement("cStat", infEventoElt.getContent());
				if(cStatElt2 == null) throw new SinedException("Erro no processamento do retorno.");
				
				String cStat = cStatElt2.getText();
				
				if(cStat.equals("135") || cStat.equals("136") || cStat.equals("155")){
					String obs = "Cancelamento na Receita";
					nota.setNotaStatus(NotaStatus.CANCELADA);
					
					if (cStat.equals("155")) {
						Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infEventoElt.getContent());
						
						obs = xMotivoElt.getText();
					}
					
					notaService.alterarStatusAcao(nota, obs, null);
					
					arquivonfnotaService.updateCancelado(arquivonfnota);
					
					vendaService.estornarVendaByNota(nota);
					expedicaoService.verificaSituacaoExpedicaoAposCancelamentoNota(nota);
					movimentacaoestoqueService.cancelarByNota(nota.getCdNota().toString());
				} else {
					Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infEventoElt.getContent());
					
					Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
					arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
					arquivonfnotaerrocancelamento.setMensagem((cStat != null ? cStat + " - ": "") + xMotivoElt.getText());
					
					arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
					
					if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
						NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA);
						String obs = "Cancelamento na Receita não efetuado.";
						nota.setNotaStatus(lastSituacao);
						notaService.alterarStatusAcao(nota, obs, null);					
					}
				}
			} else {
				Element xMotivoElt = SinedUtil.getChildElement("xMotivo", rootElement.getContent());
				
				Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
				arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
				arquivonfnotaerrocancelamento.setMensagem((cStatPrincipal != null ? cStatPrincipal + " - ": "") + xMotivoElt.getText());
				
				arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
				
				if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA);
					String obs = "Cancelamento na Receita não efetuado.";
					nota.setNotaStatus(lastSituacao);
					notaService.alterarStatusAcao(nota, obs, null);					
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Faz o processo de criação de Arquivonf a partir de um WhereIn de NFS.
	 *
	 * @param request
	 * @param whereIn
	 * @throws UnsupportedEncodingException
	 * @since 28/03/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void createArquivonfByNota(WebRequestContext request, String whereIn, Empresa empresa, Configuracaonfe configuracaonfe) throws UnsupportedEncodingException {
		empresa = empresaService.loadForEntrada(empresa);
		configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe);
		String numeroLote = null;
		
		if(Boolean.TRUE.equals(configuracaonfe.getAtualizacaodataemissao()) || Boolean.TRUE.equals(configuracaonfe.getAtualizacaodatavencimento())){
			int mesAtual = SinedDateUtils.getDateProperty(SinedDateUtils.currentDate(), Calendar.MONTH);
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForAtualizacaoData(whereIn);
			for (NotaFiscalServico nfs : listaNota) {
				Date dtemissao = nfs.getDtEmissao();
				Date dtvencimento = nfs.getDtVencimento();
				
				boolean atualizaVencimento = false;
				boolean atualizaEmissao = false;
				
				if(dtvencimento == null){
					atualizaVencimento = true;
				} else {
					int mesVencimento = SinedDateUtils.getDateProperty(dtvencimento, Calendar.MONTH);
					if(mesVencimento != mesAtual){
						atualizaVencimento = true;
					}
				}
				
				if(dtemissao == null){
					atualizaEmissao = true;
				} else {
					int mesEmissao = SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH);
					if(mesEmissao != mesAtual){
						atualizaEmissao = true;
					}
				}
				
				if(Boolean.TRUE.equals(configuracaonfe.getAtualizacaodataemissao()) && atualizaEmissao){
					notaFiscalServicoService.updateDataemissaoAtual(nfs);
				}
				if(Boolean.TRUE.equals(configuracaonfe.getAtualizacaodatavencimento()) && atualizaVencimento){
					notaFiscalServicoService.updateDatavencimentoAtual(nfs);
				}
			}
		}
		
		Map<String, String> mapArquivos = new HashMap<String, String>();
		Map<String, String> mapLote = new HashMap<String, String>();
		List<String> listaWhereInNotas = new ArrayList<String>();
		String[] ids = whereIn.split(",");
		
		Arrays.sort(ids, new Comparator<String>() {
		    @Override
		    public int compare(String s1, String s2) {
		    	Integer i1 = Integer.parseInt(s1);
		    	Integer i2 = Integer.parseInt(s2);
		        return i1.compareTo(i2);
		    }
		});
		
		int nota_por_lote = 5;
		try{
			nota_por_lote = Integer.parseInt(parametrogeralService.getValorPorNome(Parametrogeral.NUMERO_NOTAS_LOTE_NFE));
		} catch (Exception e) {}
		
		boolean prescon = false;
		boolean brumadinho = false;
		boolean tinus = false;
		boolean mi6 = false;
		boolean nfeletronica = false;
		boolean etransparencia = false;
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VINHEDOISS_PROD)){
			nota_por_lote = Integer.MAX_VALUE;
			prescon = true;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BRUMADINHOISS_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.IGARAPEISS_PROD)){
			nota_por_lote = Integer.MAX_VALUE;
			brumadinho = true;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CABEDELOISS_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CABEDELOISS_PROD)){
			tinus = true;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SETELAGOAS_PROD)){
			mi6 = true;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTANADEPARNAIBA_PROD)){
			nfeletronica = true;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_PROD)){
			etransparencia = true;
		}
		
		// Prefeitura de Paracatu, Serra e Mogi das Cruzes tem que ter somente uma nota por XML.
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRAISS_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRAISS_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JANDIRA_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JANDIRA_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARACAJUISS_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARACAJUISS_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_PROD)){
			nota_por_lote = 1;
		}
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.PARACATUISS_PROD) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_HOM) ||
				configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_PROD)) {
			nota_por_lote = 1;
		}
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ids.length; i++) {
			if(i != 0 && i % nota_por_lote == 0){
				listaWhereInNotas.add(sb.toString());
				sb = new StringBuilder();
			}
			if(sb.length() > 0) sb.append(",");
			sb.append(ids[i]);
		}
		listaWhereInNotas.add(sb.toString());
		
		
		String string;
		for (String in : listaWhereInNotas) {
			if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRAISS_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRAISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JANDIRA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JANDIRA_PROD)){
				Tbnfd tbnfd = this.createXmlEnviarLoteNfseSmarapd(in, configuracaonfe);
				string = new String(Util.strings.tiraAcento(tbnfd.toString()).getBytes(), "ISO-8859-1");
				string = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + string;
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.PARACATUISS_PROD)){
				Nfse nfse = this.createXmlEnviarLoteNfseParacatu(in, configuracaonfe);
				string = new String(Util.strings.tiraAcento(nfse.toString()).getBytes(), "ISO-8859-1");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VINHEDOISS_PROD)){
				br.com.linkcom.lknfe.xml.nfse.prescon.Nfe nfe = this.createXmlEnviarLoteNfseVinhedo(in, configuracaonfe);
				string = new String(Util.strings.tiraAcento(nfe.toString()).getBytes(), "UTF-8");
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + string;
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BRUMADINHOISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.IGARAPEISS_PROD)){
				LoteBrumadinho loteBrumadinho = this.createXmlEnviarLoteNfseBrumadinho(in, configuracaonfe);				
				string = new String(Util.strings.tiraAcento(loteBrumadinho.toString()).getBytes(), "ISO-8859-1");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTANADEPARNAIBA_PROD)){
				LoteNfeletronica loteNfeletronica = this.createXmlEnviarLoteNfseNfeletronica(in, configuracaonfe);
				string = new String(Util.strings.tiraAcento(loteNfeletronica.toString()).getBytes(), "ISO-8859-1");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LAVRASISS_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LAVRASISS_PROD)){
				GovDigital govDigital = this.createXmlEnviarLoteNfseLavras(in, configuracaonfe);
				string = new String(Util.strings.tiraAcento(govDigital.toString()).getBytes(), "ISO-8859-1");
				string = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + string;
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_HOM)){
				br.com.linkcom.lknfe.xml.nfse.datapublic.RPS rps = this.createXmlEnviarLoteNfseDataPublic(in, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(rps.toString()));
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_PROD)){
				LoteRPS loteRPS = this.createXmlEnviarLoteEtransparencia(in, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(loteRPS.toString()).getBytes(), "UTF-8");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VOTORANTIMISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MOGIMIRIMISS_PROD)){
				throw new SinedException("Não é possível gerar NFS-e para o fornecedor de NFS-e GeisWeb.");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CAMPINASISS_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CAMPINASISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SOROCABAISS_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SOROCABAISS_PROD)){
				ReqEnvioLoteRPS reqEnvioLoteRPS = this.createXmlEnviarLoteNfseCampinas(in, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(reqEnvioLoteRPS.toString()).getBytes(), "UTF-8");
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + string;
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SPISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SPISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BLUMENAUISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BLUMENAUISS_PROD)){
				PedidoEnvioLoteRPS pedidoEnvioLoteRPS = this.createXmlEnviarLoteNfseSp(in, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(pedidoEnvioLoteRPS.toString()).getBytes(), "UTF-8");
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + string;
			} else if (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SALTO_HOM)) {
				request.addError("Para a prefeitura de Salto não existe webservice disponível para emissão no ambiente de homologação.");
				throw new SinedException("Não é possivel emitir a nota " + in);
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SALTO_PROD)){
				NfseObaratec nfseObaratec = this.createXmlEnviarLoteNfseObaratec(in, empresa);
				string = new String(Util.strings.tiraAcento(nfseObaratec.toString()).getBytes(), "ISO-8859-1");
				string = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + string;
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SIMOESFILHOISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SIMOESFILHOISS_PROD)){
				List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
				
				br.com.linkcom.lknfe.xml.nfse.eliss.LoteRps loteRps = this.createXmlEnviarLoteNfseElIss(listaNota, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(loteRps.toString()).getBytes(), "UTF-8");
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + string;
			} else if (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_PROD)) {
				NfseIssMapRps nfseIssMapRps = this.createXmlEnviarLoteNfseIssMap(in, empresa, configuracaonfe);
				string = new String(Util.strings.tiraAcento(nfseIssMapRps.toString()).getBytes(), "UTF-8");
			} else if (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD)) {
				br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nfse nfse = this.createXmlEnviarNfseAssessorPublico(in, empresa, configuracaonfe);
				numeroLote = nfse != null && nfse.getNotas() != null && nfse.getNotas().getListaNota() != null && nfse.getNotas().getListaNota().get(0) != null && 
						nfse.getNotas().getListaNota().get(0).getLote() != null ? nfse.getNotas().getListaNota().get(0).getLote() : null;
				string = new String(Util.strings.tiraAcento(nfse.toString()).getBytes(), "ISO-8859-1");
				string = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + string;
            } else if (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_HOM) || configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_PROD)) {
                List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
                EnviarLoteRpsEnvio enviarLoteRpsEnvio = this.createXmlEnviarLoteNfseEel2(listaNota, empresa, request, configuracaonfe);
                
                string = new String(Util.strings.tiraAcento(enviarLoteRpsEnvio.toString()).getBytes(), "UTF-8");
            } else if (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARABA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARABA_PROD)){
            	List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
				string = this.createXmlEnviarNfseGoverna(listaNota, empresa, configuracaonfe, null);
			} else {
				List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
				if(request != null) this.validaCriacaoNfse(request, listaNota, configuracaonfe);
				
				EnviarLoteRpsEnvio enviarLoteRpsEnvio = null;
				if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTALUZIAISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTALUZIAISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITABIRITOISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITABIRITOISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JUIZDEFORAISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JUIZDEFORAISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARACAJUISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARACAJUISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JOAO_PESSOA_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.JOAO_PESSOA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_PROD) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SIMOESFILHOISS_NOVO_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SIMOESFILHOISS_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.NOVALIMAISS_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.NOVALIMAISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SETELAGOAS_PROD) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_PROD) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CAMACARI_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CAMACARI_PROD) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_NOVO_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BOA_VISTA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BOA_VISTA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BOA_VISTA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.GOIANIA_HOM) || 
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.GOIANIA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.PETROLINA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.PETROLINA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_NOVO_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_NOVO_PROD)) {
					enviarLoteRpsEnvio = this.createXmlEnviarLoteNfseIssonline(listaNota, empresa, request, configuracaonfe);
				} else {
					enviarLoteRpsEnvio = this.createXmlEnviarLoteNfse(listaNota, empresa, request, configuracaonfe);
				}
				
				if (!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_PROD)) {
					string = new String(Util.strings.tiraAcento(enviarLoteRpsEnvio.toString()).getBytes(), "UTF-8");
				} else {
					string = new String(enviarLoteRpsEnvio.toString().getBytes());
				}
				
				string = new String(Util.strings.tiraAcento(enviarLoteRpsEnvio.toString()).getBytes(), "UTF-8");
				
				if(!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARIANAISS_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARIANAISS_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BARBACENA_NOVO_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BARBACENA_NOVO_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.AVARE_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.IVOTI_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.IVOTI_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MANAUS_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MANAUS_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CHAPECO_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CHAPECO_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.GOIANIA_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.GOIANIA_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_HOM) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_NOVO_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_PROD) &&
						!configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ITUMBIARA_HOM)) {
					string = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + string;
				}
				
				if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) ||
						configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD)){
					try{
						SAXBuilder sab = new SAXBuilder();  
						Document d = sab.build(new ByteArrayInputStream(string.getBytes()));
						
						Element root = d.getRootElement();
						Namespace namespace = root.getNamespace("tc");
						
						
						Element loteRps = SinedUtil.getChildElement("LoteRps", root.getContent());
						
						this.setaNamespace(loteRps.getContent(), namespace);
						
						XMLOutputter xout = new XMLOutputter();  
						ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
						
						Format newFormat = Format.getCompactFormat();
						newFormat.setEncoding("LATIN1");
						xout.setFormat(newFormat);
						
						xout.output(d, byteArrayOutputStream);
						
						string = new String(byteArrayOutputStream.toByteArray());
						
//						System.out.println(string);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			mapLote.put(in, numeroLote);
			mapArquivos.put(in, string);
		}
		
		Nota nota;
		Arquivonf arquivonf;
		Arquivonfnota arquivonfnota;
		Arquivo arquivo;
		Set<Entry<String, String>> entrySet = mapArquivos.entrySet();
		
		List<Entry<String, String>> entryList = new ArrayList<Map.Entry<String,String>>(entrySet);
		Collections.sort(entryList, new Comparator<Entry<String, String>>(){
			public int compare(Entry<String, String> o1, Entry<String, String> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		
		for (Entry<String, String> entry : entryList) {
			if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.BRUMADINHOISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.IGARAPEISS_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SANTANADEPARNAIBA_PROD)){
				arquivo = new Arquivo(entry.getValue().getBytes(), "NFE_LOTE-" + SinedUtil.datePatternForReport() + ".txt", "text/plain");
			} else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_PROD) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARABA_HOM) ||
					configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.MARABA_PROD)){
				arquivo = new Arquivo(entry.getValue().getBytes(), "LOTE-" + SinedUtil.datePatternForReport() + ".txt", "text/plain");
			} else {
				arquivo = new Arquivo(entry.getValue().getBytes(), "nf_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			}
			
			arquivonf = new Arquivonf();
			arquivonf.setEmpresa(empresa);
			arquivonf.setNumeroLote(mapLote.get(entry.getKey()));
			arquivonf.setArquivoxml(arquivo);
			arquivonf.setConfiguracaonfe(configuracaonfe);
			arquivonf.setArquivonfsituacao(Arquivonfsituacao.GERADO);
			arquivonf.setEmitindo(Boolean.FALSE);
			
			arquivoDAO.saveFile(arquivonf, "arquivoxml");
			arquivoService.saveOrUpdate(arquivo);
			this.saveOrUpdate(arquivonf);
			
			if(brumadinho || tinus || prescon || mi6 || nfeletronica || etransparencia){
				DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
				request.addMessage("Para fazer o download do arquivo para ser importado no site da prefeitura <a href='" + SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + arquivo.getCdarquivo() + "'>clique aqui</a>.");
			}
			
			String[] notas = entry.getKey().split(",");
			for (int i = 0; i < notas.length; i++) {
				nota = new Nota(Integer.parseInt(notas[i]));
				
				arquivonfnota = new Arquivonfnota();
				arquivonfnota.setArquivonf(arquivonf);
				arquivonfnota.setNota(nota);
				
				nota.setNotaStatus(NotaStatus.NFSE_SOLICITADA);
				notaService.alterarStatusAcao(nota, "Arquivo de NFS-e criado. <a href=\"javascript:visualizarArquivonf(" + arquivonf.getCdarquivonf() + ")\">" + arquivonf.getCdarquivonf() + "</a>", null);
				arquivonfnotaService.saveOrUpdate(arquivonfnota);
			}
		}
	}
	
	private br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nfse createXmlEnviarNfseAssessorPublico(String in, Empresa empresa, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		NotaFiscalServico notaFiscalServico = listaNota.get(0);
		
		Identificacao identificacao = new Identificacao();
		identificacao.setMesComp(StringUtils.stringCheia(Integer.toString(SinedDateUtils.getMes(notaFiscalServico.getDtEmissao()) + 1), "0", 2, false));
		identificacao.setAnoComp(Integer.toString(SinedDateUtils.getAno(notaFiscalServico.getDtEmissao())));
		identificacao.setInscricao(configuracaonfe.getInscricaomunicipal());
		identificacao.setVersao("1.00");
		
		List<br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nota> notaList = new ArrayList<br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nota>();
		Notas notas = new Notas();
		br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nota nota = new br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nota();
		nota.setRps(formataRPS(notaFiscalServico.getNumero()));
		
		Integer proxNum = configuracaonfeService.getNextNumLoteNfse(configuracaonfe);
		nota.setLote(proxNum.toString());
		
		nota.setSequencia("1");
		nota.setDataEmissao(SinedDateUtils.toString(notaFiscalServico.getDtEmissao()));
		nota.setHoraEmissao(SinedDateUtils.toString(notaFiscalServico.getDtEmissao(), "hh:mm:ss"));
		nota.setLocal("D");
		
		nota.setSituacao("1");
		nota.setRetido(configuracaonfe.getIssretidopelotomador() != null && configuracaonfe.getIssretidopelotomador() ? "S" : "N");
		nota.setAtividade(notaFiscalServico.getItemlistaservicoBean() != null && notaFiscalServico.getItemlistaservicoBean().getCodigo() != null ? notaFiscalServico.getItemlistaservicoBean().getCodigo() : "");
		nota.setAliquotaAplicada(notaFiscalServico.getIss());
		nota.setDeducao(notaFiscalServico.getDeducao() != null ? notaFiscalServico.getDeducao().getValue().doubleValue() : 0d);
		nota.setImposto(notaFiscalServico.getValorIss() != null ? notaFiscalServico.getValorIss().getValue().doubleValue() : 0d);
		nota.setRetencao(0d);
		nota.setObservacao(null);
		nota.setCpfCnpj(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getCpfOrCnpjByTipopessoa() != null ? notaFiscalServico.getCliente().getCpfOrCnpjByTipopessoa() : "");
		nota.setRgIe(notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getRgOrIeByTipopessoa() : "");
		nota.setNomeRazao(notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getRazaoOrNomeByTipopessoa() : "");
		nota.setNomeFantasia(notaFiscalServico.getNomefantasiaCliente() != null ? notaFiscalServico.getNomefantasiaCliente() : (notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getRazaoOrNomeByTipopessoa() : ""));
		nota.setMunicipio(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getMunicipio() != null && 
				notaFiscalServico.getEnderecoCliente().getMunicipio().getCdibge() != null ? notaFiscalServico.getEnderecoCliente().getMunicipio().getCdibge() : "");
		nota.setPrefixo(notaFiscalServico.getEnderecoCliente() != null ? notaFiscalServico.getEnderecoCliente().getLogradouro().split(" ")[0] : "");
		
		if(notaFiscalServico.getMunicipioissqn() != null && notaFiscalServico.getMunicipioissqn().getUf() != null && notaFiscalServico.getMunicipioissqn().getUf().getSigla() != null &&
				notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getMunicipio() != null && 
				notaFiscalServico.getEnderecoCliente().getMunicipio().getUf() != null &&
				!notaFiscalServico.getMunicipioissqn().getUf().getSigla().equals(notaFiscalServico.getEnderecoCliente().getMunicipio().getUf().getSigla())){
			nota.setUfFora(notaFiscalServico.getMunicipioissqn() != null && notaFiscalServico.getMunicipioissqn().getUf() != null ? notaFiscalServico.getMunicipioissqn().getUf().getSigla() : "");
			nota.setMunicipioFora(notaFiscalServico.getMunicipioissqn() != null ? notaFiscalServico.getMunicipioissqn().getCdibge() : "");
			nota.setLocal("F");
//			nota.setPaisFora("BRASIL");
		}
		
		nota.setBairro(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getBairro() != null ? notaFiscalServico.getEnderecoCliente().getBairro() : "");
		nota.setCep(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getCep() != null ? 
				notaFiscalServico.getEnderecoCliente().getCep().getValue() : "");
		nota.setLogradouro(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getLogradouro() != null ? notaFiscalServico.getEnderecoCliente().getLogradouro() : "");
		nota.setComplemento(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getComplemento() != null ? notaFiscalServico.getEnderecoCliente().getComplemento() : "");
		nota.setNumero(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getNumero() != null ? notaFiscalServico.getEnderecoCliente().getNumero() : "");
		nota.setEmail(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getEmail() != null ? notaFiscalServico.getCliente().getEmail() : "");
		nota.setDentroPais("S");
		nota.setDedMateriais(notaFiscalServico.getDeducao() != null && notaFiscalServico.getDeducao().getValue().doubleValue() > 0d ? "S" : "N");
		nota.setDataVencimento(notaFiscalServico.getDtVencimento() != null ? SinedDateUtils.toString(notaFiscalServico.getDtVencimento()) : "");
		nota.setContraApresentacao(null);
		nota.setPis(notaFiscalServico.getValorPis() != null ? notaFiscalServico.getValorPis().getValue().doubleValue() : 0d);
		nota.setRetPis(notaFiscalServico.getIncidepis() != null && notaFiscalServico.getIncidepis() ? "S" : "N");
		nota.setCofins(notaFiscalServico.getValorCofins() != null ? notaFiscalServico.getValorCofins().getValue().doubleValue() : 0d);
		nota.setRetCofins(notaFiscalServico.getIncidecofins() != null && notaFiscalServico.getIncidecofins() ? "S" : "N");
		nota.setInss(notaFiscalServico.getValorInss() != null ? notaFiscalServico.getValorInss().getValue().doubleValue() : 0d);
		nota.setRetInss(notaFiscalServico.getIncideinss() != null && notaFiscalServico.getIncideinss() ? "S" : "N");
		nota.setIr(notaFiscalServico.getValorIr() != null ? notaFiscalServico.getValorIr().getValue().doubleValue() : 0d);
		nota.setRetIr(notaFiscalServico.getIncideir() != null && notaFiscalServico.getIncideir() ? "S" : "N");
		nota.setCsll(notaFiscalServico.getValorCsll() != null ? notaFiscalServico.getValorCsll().getValue().doubleValue() : 0d);
		nota.setRetCsll(notaFiscalServico.getIncidecsll() != null && notaFiscalServico.getIncidecsll() ? "S" : "N");
		nota.setIcms(notaFiscalServico.getValorIcms() != null ? notaFiscalServico.getValorIcms().getValue().doubleValue() : 0d);
		nota.setRetIcms(notaFiscalServico.getIncideicms() != null && notaFiscalServico.getIncideicms() ? "S" : "N");
		nota.setIpi(0d);
		nota.setRetIpi("N");
		nota.setIof(0d);
		nota.setRetIof("N");
		nota.setCide(0d);
		nota.setRetCide("N");
		nota.setOutrosTributos(0d);
		nota.setRetOutrosTributos("N");
		nota.setOutrasRetencoes(notaFiscalServico.getOutrasretencoes() != null ? notaFiscalServico.getOutrasretencoes().getValue().doubleValue() : 0d);
		nota.setRetOutrasRetencoes(notaFiscalServico.getOutrasretencoes() != null && notaFiscalServico.getOutrasretencoes().getValue().doubleValue() > 0d ? "S" : "N");
		nota.setObra(null);
		
		br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servicos servicos = new br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servicos();
		List<br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servico> listaServico = new ArrayList<br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servico>();
		br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servico servico;
		
		if (notaFiscalServico.getListaItens() != null && notaFiscalServico.getListaItens().size() > 0) {
			for (NotaFiscalServicoItem nfsi : notaFiscalServico.getListaItens()) {
				servico = new br.com.linkcom.lknfe.xml.nfse.assessorpublico.Servico();
				String descricao = this.preencheDescricaoItemNfs(nfsi, configuracaonfe);
				servico.setDescricao(descricao.substring(0, descricao.length()-1));
				servico.setQuantidade(nfsi.getQtde());
				servico.setValorUnit(nfsi.getPrecoUnitario());
				servico.setDesconto(nfsi.getDesconto());
				
				listaServico.add(servico);
			}
		}
		
		servicos.setListaServico(listaServico);
		nota.setServicos(servicos);
		
		notaList.add(nota);
		notas.setListaNota(notaList);
		
		br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nfse nfse = new br.com.linkcom.lknfe.xml.nfse.assessorpublico.Nfse();
		nfse.setIdentificacao(identificacao);
		nfse.setNotas(notas);
		
		return nfse;
	}
	
	private String formataRPS(String numero) {
		String rps = null;
		if(org.apache.commons.lang.StringUtils.isNotBlank(numero)){
			rps = StringUtils.stringCheia(numero, "0", 12, false);
			if(rps.length() == 12){
				rps = rps.substring(0, 4) + "-" + rps.substring(4, 8) + "-" + rps.substring(8, 12); 
			}
		}
			
		return rps;
	}
	
	public NfseCancelamento createXmlCancelamentoNfseAssessorPublico(Arquivonfnota arquivonfnota, String motivo) {		
		NfseCancelamento nfseCancelamento = new NfseCancelamento();
		nfseCancelamento.setInscricao(arquivonfnota.getArquivonf().getConfiguracaonfe().getInscricaomunicipal());
		nfseCancelamento.setLote(arquivonfnota.getArquivonf().getNumeroLote());
		nfseCancelamento.setSequencia(arquivonfnota.getArquivonf().getNumerorecibo());
		nfseCancelamento.setObservacao(org.apache.commons.lang.StringUtils.isNotEmpty(motivo) ? motivo : "");
		
		return nfseCancelamento;
	}
	
	private NfseConsulta createXmlConsultaNfseAssessorPublico(String codigo, Configuracaonfe configuracaonfe, Arquivonf arquivonf) {
		NfseConsulta nfseConsulta = new NfseConsulta();
		nfseConsulta.setInscricao(configuracaonfe.getInscricaomunicipal());
		nfseConsulta.setLote(arquivonf.getNumeroLote());
		nfseConsulta.setSequencia(codigo);
		
		return nfseConsulta;
	}
	
	private NfseObaratec createXmlEnviarLoteNfseObaratec(String in, Empresa empresa) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		NotaFiscalServico notaFiscalServico = listaNota.get(0);
		
		NfseObaratec nfseObaratec = new NfseObaratec();
		NfseObaratecNota nfseObaratecNota = new NfseObaratecNota();
		NfseObaratecNotaTomador nfseObaratecNotaTomador = new NfseObaratecNotaTomador();
		List<NfseObaratecNota> listaNfseObaratecNota = new ArrayList<NfseObaratecNota>();
		
		nfseObaratecNota.setNumeroRps(notaFiscalServico.getNumero());
		nfseObaratecNota.setInscricaoMunicipal(empresa.getInscricaomunicipal());
		
		nfseObaratecNota.setDescricaoObservacao(notaFiscalServico.getDescricaoServico().length() > 1000 ? notaFiscalServico.getDescricaoServico().substring(0, 1000) : notaFiscalServico.getDescricaoServico());
		nfseObaratecNota.setDataCompetencia(SinedDateUtils.toString(notaFiscalServico.getDtEmissao()));
		nfseObaratecNota.setValorNf(notaFiscalServico.getValorNota() != null ? notaFiscalServico.getValorNota().toString() : "0,00");
		nfseObaratecNota.setDescontosLegais(notaFiscalServico.getDeducao() != null ? notaFiscalServico.getDeducao().toString() : "0,00");
		nfseObaratecNota.setValorInss(notaFiscalServico.getValorInss() != null ? notaFiscalServico.getValorInss().toString() : "0,00");
		nfseObaratecNota.setValorIrrf(notaFiscalServico.getValorIr() != null ? notaFiscalServico.getValorIr().toString() : "0,00");
		nfseObaratecNota.setValorCsll(notaFiscalServico.getValorCsll() != null ? notaFiscalServico.getValorCsll().toString() : "0,00");
		nfseObaratecNota.setValorPis(notaFiscalServico.getValorPis() != null ? notaFiscalServico.getValorPis().toString() : "0,00");
		nfseObaratecNota.setValorCofins(notaFiscalServico.getValorCofins() != null ? notaFiscalServico.getValorCofins().toString() : "0,00");
		
		if (notaFiscalServico.getCliente() != null) {
			Cliente cliente = notaFiscalServico.getCliente();
			
			nfseObaratecNotaTomador.setNumCpf(cliente.getTipopessoa() != null && cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA) && cliente.getCpf() != null ? cliente.getCpf().getValue() : "");
			nfseObaratecNotaTomador.setNumCnpj(cliente.getTipopessoa() != null && cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) && cliente.getCnpj() != null ? cliente.getCnpj().getValue() : "");
			nfseObaratecNotaTomador.setInscMunicipalTomador(cliente.getInscricaomunicipal() != null ? cliente.getInscricaomunicipal() : "");
			nfseObaratecNotaTomador.setRazaoSocial(cliente.getRazaoOrNomeByTipopessoa() != null ? cliente.getRazaoOrNomeByTipopessoa() : "");
			nfseObaratecNotaTomador.setEndereco(cliente.getEnderecoLogradouro() != null ? cliente.getEnderecoLogradouro() : "");
			nfseObaratecNotaTomador.setNumero(cliente.getEnderecoNumero() != null ? cliente.getEnderecoNumero() : "");
			nfseObaratecNotaTomador.setComplemento(cliente.getEnderecoComplemento() != null ? cliente.getEnderecoComplemento() : "");
			nfseObaratecNotaTomador.setBairro(cliente.getEnderecoBairro() != null ? cliente.getEnderecoBairro() : "");
			nfseObaratecNotaTomador.setCidade(cliente.getEnderecoCidade() != null ? cliente.getEnderecoCidade() : "");
			nfseObaratecNotaTomador.setUf(cliente.getEnderecoSiglaEstado() != null ? cliente.getEnderecoSiglaEstado() : "");
			nfseObaratecNotaTomador.setCep(cliente.getEnderecoCep() != null ? cliente.getEnderecoCep() : "");
			nfseObaratecNotaTomador.setTelefone(cliente.getTelefoneprincipal() != null ? cliente.getTelefoneprincipal() : "");
			nfseObaratecNotaTomador.setFax(cliente.getTelefoneFax() != null ? cliente.getTelefoneFax().getTelefone() : "");
			nfseObaratecNotaTomador.setEmail(cliente.getEmail() != null ? cliente.getEmail() : "");
			nfseObaratecNotaTomador.setInscricaoEstadual(cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "");
			nfseObaratecNotaTomador.setStaEstrangeiro(cliente.getEnderecoCdPais() != null && cliente.getEnderecoCdPais() != 36 ? "S" : "N");
			nfseObaratecNotaTomador.setSiaf("N");
			
			nfseObaratecNota.setNfseObaratecNotaTomador(nfseObaratecNotaTomador);
		}
		
		nfseObaratecNota.setNumeroCnaeAtividade(notaFiscalServico.getCodigocnaeBean() != null && notaFiscalServico.getCodigocnaeBean().getCnae() != null ? 
				notaFiscalServico.getCodigocnaeBean().getCnae().replace("-", "").replace("/", "") : "");
		nfseObaratecNota.setCnaeItemLista(notaFiscalServico.getItemlistaservicoBean() != null && notaFiscalServico.getItemlistaservicoBean().getCodigo() != null ? 
				notaFiscalServico.getItemlistaservicoBean().getCodigo().replace(".", "") : "");
		nfseObaratecNota.setAliquotaSimplesNascional(notaFiscalServico.getIss() != null ? new Money(notaFiscalServico.getIss() / 100d).toString() : "0,00");
		nfseObaratecNota.setNomCidadePrestacao(notaFiscalServico.getMunicipioissqn() != null ? notaFiscalServico.getMunicipioissqn().getNome() : "");
		nfseObaratecNota.setNomUfPrestacao(notaFiscalServico.getMunicipioissqn() != null && notaFiscalServico.getMunicipioissqn().getUf() != null ? notaFiscalServico.getMunicipioissqn().getUf().getSigla() : "");
		nfseObaratecNota.setIssPagoTomadorServico(empresa.getEnderecoCidade() != null && notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getEnderecoCidade() != null &&
				empresa.getEnderecoCidade().equals(notaFiscalServico.getCliente().getEnderecoCidade()) ? "N" : "S");
		nfseObaratecNota.setAbatimentoPadrao("N");
		nfseObaratecNota.setObraIsenta("N");
		
		listaNfseObaratecNota.add(nfseObaratecNota);
		nfseObaratec.setListaNfseObaratecNota(listaNfseObaratecNota);
		
		return nfseObaratec;
	}
	
	private void setaNamespace(List<?> content, Namespace namespace) {
		for (Object object : content) {
			if(object instanceof Element){
				Element elt = (Element)object;
				elt.setNamespace(namespace);
				
				this.setaNamespace(elt.getContent(), namespace);
			}
		}
	}
	
	/**
	 * Valida a criação das NFS-e.
	 *
	 * @param request
	 * @param listaNota
	 * @since 28/03/2012
	 * @author Rodrigo Freitas
	 * @param webservice 
	 */
		// VERIFICAÇÃO
	public void validaCriacaoNfse(WebRequestContext request, List<NotaFiscalServico> listaNota, Configuracaonfe configuracaonfe) {
		String webservice = configuracaonfe.getPrefixowebservice().getPrefixo();
		boolean erro = false;
		
		for (NotaFiscalServico nf : listaNota) {
			if(nf.getNumero() == null || nf.getNumero().equals("")){
				erro = true;
				request.addError("Favor cadastrar o número das notas.");
			} else {
				if(webservice.equals("memory")){
					if(nf.getItemlistaservicoBean() == null){
						erro = true;
						request.addError("Favor cadastrar o item da lista de serviço para a nota " + nf.getNumero());
					}
				}
				
				if(!webservice.equals("issonline") && !webservice.equals("webiss") && !webservice.equals("supernova") && !webservice.equals("iibrasil")){
					if(!webservice.equals("salvadoriss") && !webservice.equals("fiorilli") && nf.getCodigotributacao() == null){
						erro = true;
						request.addError("Favor cadastrar o código de tributação para a nota " + nf.getNumero());
					}
					
					if(nf.getNaturezaoperacao() == null){
						erro = true;
						request.addError("Favor cadastrar a natureza de operação para a nota " + nf.getNumero());
					}
				}
				
				if(nf.getMunicipioissqn() == null && !webservice.equals("fiorilli")) {
					erro = true;
					request.addError("Favor cadastrar o município da incidência do ISSQN para a nota " + nf.getNumero());
				}
				
				if(nf.getEnderecoCliente() == null){
					erro = true;
					request.addError("Endereço do cliente não cadastrado na nota. Número da nota:  " + nf.getNumero());
				} else {
					br.com.linkcom.sined.geral.bean.Endereco enderecoCliente = enderecoService.loadEndereco(nf.getEnderecoCliente());
					
					if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) {
						erro = true;
						request.addError("Logradouro do cliente " + nf.getCliente().getNome() + " não informado.<BR>");
					}
					if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")){
						erro = true;
						request.addError("Bairro do cliente " + nf.getCliente().getNome() + " não informado.<BR>");
					}
					if(enderecoCliente.getCep() == null || StringUtils.soNumero(enderecoCliente.getCep().getValue()).equals("")){
						erro = true;
						request.addError("CEP do cliente " + nf.getCliente().getNome() + " não informado.<BR>");
					}
					if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")){
						erro = true;
						request.addError("Número do endereço do cliente " + nf.getCliente().getNome() + " não informado.<BR>");
					}
					if(enderecoCliente.getMunicipio() == null){
						erro = true;
						request.addError("Município do cliente " + nf.getCliente().getNome() + " não informado.<BR>");
					}
				}
				
				if(!webservice.equals("simpliss") && !webservice.equals("fisslex")){
					String telefonecliente = null;
					if (nf.getTelefoneCliente() != null && !"".equals(nf.getTelefoneCliente())) {
						telefonecliente = StringUtils.soNumero(nf.getTelefoneCliente());
					} else if (nf.getCliente().getListaTelefone() != null && nf.getCliente().getListaTelefone().size() > 0) {
						telefonecliente = StringUtils.soNumero(nf.getCliente().getListaTelefone().iterator().next().getTelefone());
					}
					
					if (telefonecliente != null && !telefonecliente.equals("")) {
						if(webservice.equals("ginfesiss") || webservice.equals("bhiss") || webservice.equals("memory") || webservice.equals("salvadoriss")){
							if (telefonecliente.length() > 11){
								erro = true;
								request.addError("O campo telefone do cliente " + nf.getCliente().getNome() +  " não pode ter mais que 11 dígitos.");
							}
						} else {
							if (!(telefonecliente.length() == 10 || telefonecliente.length() == 11 || 
									(telefonecliente.length() == 12 && telefonecliente.startsWith("0")))) {
								erro = true;
								request.addError("O campo telefone do cliente " + nf.getCliente().getNome() +  " deve conter 10 dígitos numéricos sem espacos ou 11 ou 12 dígitos numéricos com o primeiro dígito igual a zero.");
							}
							if (telefonecliente.length() == 10 && telefonecliente.startsWith("0")){
								erro = true;
								request.addError("O campo telefone do cliente " + nf.getCliente().getNome() +  " caso tenha 10 dígitos numéricos não pode ter o primeiro dígito igual a zero.");
							}
						}
					}
				}
				
				if (webservice.equals("iibrasil")) {
					if (configuracaonfe.getToken() == null || org.apache.commons.lang.StringUtils.isEmpty(configuracaonfe.getToken())) {
						erro = true;
						request.addError("O campo token da configuração de NFS-e precisa estar preenchido.");
					}
				}
			}
		}
		
		if(erro) throw new SinedException("Não foi possível criar o arquivo NFS-e.");
	}
	
	/**
	 * Estorna notas do mesmo lote setando a situação do arquivo para 'NÃO ENVIADO';
	 *
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#findByArquivonf(Arquivonf arquivonf)
	 * @see br.com.linkcom.sined.geral.service.NotaService#alterarStatusAcao(Nota nota, String observacao, boolean cancelarDocumentos)
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao)
	 *
	 * @param nota
	 * @param obs
	 * @since 04/04/2012
	 * @author Rodrigo Freitas
	 */
	public void naoEnviarArquivo_NotaEstornada(Nota nota, String obs) {
		Arquivonf arquivonf = this.findGeradosByNota(nota);
		if(arquivonf != null){
			List<Arquivonfnota> lista = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : lista) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), obs, null);
			}
			
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
		}
	}
	
	/**
	 * Faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#findGeradosByNota(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @since 04/04/2012
	 * @author Rodrigo Freitas
	 */
	private Arquivonf findGeradosByNota(Nota nota) {
		return arquivonfDAO.findGeradosByNota(nota);
	}
	
	public void consultarArquivoETransparencia(WebRequestContext request, Arquivonf arquivonf, boolean fromEnvio) throws ServiceException, SOAPException, UnsupportedEncodingException, JDOMException, IOException {
		
	}
	
	@SuppressWarnings("unchecked")
	public void consultarArquivoEliss(WebRequestContext request, Arquivonf arquivonf, boolean fromEnvio) throws ServiceException, SOAPException, UnsupportedEncodingException, JDOMException, IOException {
		if(!fromEnvio){
			arquivonf = this.loadForEntrada(arquivonf);
		}
		
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		Empresa empresa = configuracaonfe.getEmpresa();
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		String login = "23596650000147";
		if(empresa != null && ambienteProducao){
			empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.cnpj, empresa.nome, empresa.razaosocial");
			login = empresa.getCnpj().getValue();
		}
		
		String xml = "<el:ConsultarLoteRpsEnvio xmlns:el=\"http://des36.el.com.br:8080/el-issonline/\">" +
				        "<identificacaoPrestador>" + login + "</identificacaoPrestador>" +
				        "<numeroProtocolo>" + arquivonf.getNumeroprotocolo() + "</numeroProtocolo>" +
				    "</el:ConsultarLoteRpsEnvio>"; 
		
		String retorno = this.executeEliss(xml, "ConsultarLoteRpsEnvio");
		
		Arquivo arquivo = new Arquivo(retorno.getBytes(), "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivonf.setArquivoretornoconsulta(arquivo);
		
		arquivoDAO.saveFile(arquivonf, "arquivoretornoconsulta");
		arquivoService.saveOrUpdate(arquivo);
		this.updateArquivoretornoconsulta(arquivonf);
		
		Element rootElement = SinedUtil.getRootElementXML(retorno.getBytes());
		
		List<Element> notasFiscais = SinedUtil.getListChildElement("notasFiscais", rootElement.getContent());
		List<Element> mensagens = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
		
		Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
		if(notasFiscais != null && notasFiscais.size() > 0){
			Map<String, String> mapaNumNfse = new HashMap<String, String>();
			Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
			
			for (Element elt : notasFiscais) {
				Element numeroRpsElt = SinedUtil.getChildElement("rpsNumero", elt.getContent());
				Element numeroNfseElt = SinedUtil.getChildElement("numero", elt.getContent());
				Element codigoVerificacaoElt = SinedUtil.getChildElement("idNota", elt.getContent());
				
				mapaNumNfse.put(numeroRpsElt.getText(), numeroNfseElt.getText());
				mapaCodVerificacao.put(numeroRpsElt.getText(), codigoVerificacaoElt.getText());
			}
			
			String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				String numeroNfse = mapaNumNfse.get(arquivonfnota.getNota().getNumero());
				String codigoVerificacao = mapaCodVerificacao.get(arquivonfnota.getNota().getNumero());
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
					try {
						vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao, dataAtual, dataAtual);
				if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
				} else {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
				}
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
			}
			
			this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
			
			try{
				if(paramEnvioNota != null && paramEnvioNota.trim().toUpperCase().equals("TRUE")){
					this.enviarNFCliente(CollectionsUtil.listAndConcatenate(listan, "nota.cdNota", ","), TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if(mensagens != null && mensagens.size() > 0){
			for (Element elt : mensagens) {
				if(elt.getText().contains("EL68")){
					Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
					
					arquivonfnotaerro.setArquivonf(arquivonf);
					arquivonfnotaerro.setMensagem("Lote em processamento");
					arquivonfnotaerro.setData(dataAtual);
					
					arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					
					throw new SinedException("Lote em processamento");
				}
				
				Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
				
				arquivonfnotaerro.setArquivonf(arquivonf);
				arquivonfnotaerro.setMensagem(elt.getText());
				arquivonfnotaerro.setData(dataAtual);
				
				arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			}
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com erro.", null);
			}
			
			this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
		}
		
	}
	
	public void cancelarArquivoETransparencia(Arquivonfnota arquivonfnota) throws ServiceException, SOAPException, UnsupportedEncodingException, JDOMException, IOException {
		
	}
	
	@SuppressWarnings("unchecked")
	public void cancelarArquivoEliss(Arquivonfnota arquivonfnota) throws ServiceException, SOAPException, UnsupportedEncodingException, JDOMException, IOException {
		Arquivonf arquivonf = this.loadForEntrada(arquivonfnota.getArquivonf());
		
		Nota nota = arquivonfnota.getNota();
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		Empresa empresa = configuracaonfe.getEmpresa();
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		String login = "23596650000147";
		if(empresa != null && ambienteProducao){
			empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.cnpj, empresa.nome, empresa.razaosocial");
			login = empresa.getCnpj().getValue();
		}
		
		String xml = "<el:CancelarNfseEnvio xmlns:el=\"http://des36.el.com.br:8080/el-issonline/\">" +
				        "<identificacaoPrestador>" + login + "</identificacaoPrestador>" +
				        "<numeroNfse>" + arquivonfnota.getNumeronfse() + "</numeroNfse>" +
				    "</el:CancelarNfseEnvio>"; 
		
		String retorno = this.executeEliss(xml, "CancelarNfseEnvio");
		
		Element rootElement = SinedUtil.getRootElementXML(retorno.getBytes());
		
		List<Element> mensagens = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
		if(mensagens != null && mensagens.size() > 0){
			for (Element element : mensagens) {
				Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
				arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
				arquivonfnotaerrocancelamento.setMensagem(element.getText());
				NeoWeb.getRequestContext().addError(element.getText());
				
				arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
			}
		} else {
			String obs = "Cancelamento na Receita";
			nota.setNotaStatus(NotaStatus.CANCELADA);
			notaService.alterarStatusAcao(nota, obs, null);
			
			arquivonfnotaService.updateCancelado(arquivonfnota);
		}
	}
	
	public void enviarArquivoETransparencia(WebRequestContext request, Arquivonf arquivonf) throws Exception {
		arquivonf = this.loadForEntrada(arquivonf);
		
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		Arquivo arquivoxml = arquivoDAO.loadWithContents(arquivonf.getArquivoxml());
		String xmlDentro = new String(arquivoxml.getContent());
		
		String retorno = this.executeETransparencia(xmlDentro, "NFeaction/AWS_NFE.PROCESSARPS", prefixowebservice);
		
		Arquivo arquivoretorno = new Arquivo(retorno.getBytes(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		arquivonf.setArquivoretornoenvio(arquivoretorno);
		
		arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
		arquivoService.saveOrUpdate(arquivoretorno);
		this.updateArquivoretornoenvio(arquivonf);
	}
	
	@SuppressWarnings("unchecked")
	public void enviarArquivoEliss(WebRequestContext request, Arquivonf arquivonf) throws Exception {
		arquivonf = this.loadForEntrada(arquivonf);
		
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		Empresa empresa = configuracaonfe.getEmpresa();
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		boolean ambienteProducao = !prefixowebservice.name().endsWith("_HOM");
		
		Arquivo arquivoxml = arquivoDAO.loadWithContents(arquivonf.getArquivoxml());
		
		String xmlDentro = new String(arquivoxml.getContent());
		
		boolean erro = false;
		
		String login = "23596650000147";
		if(empresa != null && ambienteProducao){
			empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.cnpj, empresa.nome, empresa.razaosocial");
			login = empresa.getCnpj().getValue();
		}
		String senha = configuracaonfe.getSenha();
		
		// VALIDAÇÃO DOS DADOS NECESSÁRIOS
		
		if(login == null || login.trim().equals("")){
			request.addError("Favor preencher o login na configuração de NFS-e.");
			erro = true;
		}
		
		if(senha == null || senha.trim().equals("")){
			request.addError("Favor preencher o senha na configuração de NFS-e.");
			erro = true;
		}
		
		if(erro) return;
		
		String hashSessao = this.iniciarSessaoEliss(login, senha);
		
		String xml = "<el:EnviarLoteRpsEnvio xmlns:el=\"http://des36.el.com.br:8080/el-issonline/\">" +
				        "<identificacaoPrestador>" + login + "</identificacaoPrestador>" +
				        "<hashIdentificador>" + hashSessao + "</hashIdentificador>" +
				        "<arquivo><![CDATA[" + xmlDentro + "]]></arquivo>" +
				    "</el:EnviarLoteRpsEnvio>"; 
		
		String retorno = this.executeEliss(xml, "EnviarLoteRpsEnvio");
		
		this.finalizarSessaoEliss(hashSessao);
		
		Arquivo arquivoretorno = new Arquivo(retorno.getBytes(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		arquivonf.setArquivoretornoenvio(arquivoretorno);
		
		arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
		arquivoService.saveOrUpdate(arquivoretorno);
		this.updateArquivoretornoenvio(arquivonf);
		
		Element rootElement = SinedUtil.getRootElementXML(retorno.getBytes());
		
		Element numeroProtocolo = SinedUtil.getChildElement("numeroProtocolo", rootElement.getContent());
		
		if(numeroProtocolo != null){
			String protocoloStr = numeroProtocolo.getValue();
			arquivonf.setNumeroprotocolo(protocoloStr);
			
			this.updateProtocolo(arquivonf, protocoloStr);
			this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
			}
			
			request.addMessage("Arquivo de NFS-e enviado com sucesso.");
			
			try{
				this.consultarArquivoEliss(request, arquivonf, true);
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Problema na consulta do lote, favor tentar realizar a consulta novamente.");
				request.addError(e.getMessage());
				throw new SinedException();
			}
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
			}
			
			List<Element> lista = SinedUtil.getListChildElement("mensagens", rootElement.getContent());
			if(lista != null && lista.size() > 0){
				for (int i = 0; i < lista.size(); i++) {
					Object object = lista.get(i);
					if(object instanceof Element){
						Element msg = (Element)object;
						if(msg.getName().equals("mensagens")){
							Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
							
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setMensagem(msg.getValue());
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
						}
					}
				}
			}
			
			request.addError("Arquivo de NFS-e enviado com erros favor consultar o lote para mais informações.");
		}
	}
	
	private String iniciarSessaoEliss(String identificacaoPrestador, String senhaPrestador) throws AxisFault, ServiceException, SOAPException{
		String xml = "<el:autenticarContribuinte xmlns:el=\"http://des36.el.com.br:8080/el-issonline/\">" +
				        "<identificacaoPrestador>" + identificacaoPrestador + "</identificacaoPrestador>" +
				        "<senha>" + senhaPrestador + "</senha>" +
				    "</el:autenticarContribuinte>";
		
		return this.executeEliss(xml, "autenticarContribuinte");
	}
	
	private void finalizarSessaoEliss(String hashSessao) throws AxisFault, ServiceException, SOAPException {
        String xml = "<el:finalizarSessao xmlns:el=\"http://des36.el.com.br:8080/el-issonline/\">" +
                        "<hashIdentificador>" + hashSessao + "</hashIdentificador>" +
                    "</el:finalizarSessao>";
        
        this.executeEliss(xml, "finalizarSessao");
    }
	
	@SuppressWarnings("deprecation")
	private String executeETransparencia(String xmlDentro, String operation, Prefixowebservice prefixowebservice) throws ClientProtocolException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, CertificateException {
		String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:nfe=\"NFe\">"
				+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
						+ xmlDentro
					+ "</soapenv:Body>" 
				+ "</soapenv:Envelope>";
		
		String url = null;
        if(prefixowebservice.equals(Prefixowebservice.LIMEIRA_HOM)){
        	url = "https://nfehomologacao.etransparencia.com.br/sp.limeira/webservice/aws_nfe.aspx";
        } else if(prefixowebservice.equals(Prefixowebservice.LIMEIRA_HOM)){
        	url = "https://nfe.etransparencia.com.br/sp.limeira/webservice/aws_nfe.aspx";
        }
        
        try{
            URL urlCadeiaCertificados = new URL("http", "utilpub.linkcom.com.br", "/w3erp/nfe.keystore");
            InputStream is = urlCadeiaCertificados.openConnection().getInputStream();

            File file = new File("nfe.keystore");
            file.createNewFile();

            OutputStream out = new FileOutputStream(file);

            byte buf[] = new byte[1024];
            int len;
            while((len = is.read(buf)) > 0)
                    out.write(buf,0,len);

            out.close();
            is.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        
        Security.insertProviderAt(new BouncyCastleProvider(), 1);
		
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("SOAPAction", operation);
		httpPost.setEntity(new StringEntity(xml));
		
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
		}};
		
		SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());

        SSLSocketFactory socketFactory = new SSLSocketFactory(sc);
        socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        
        System.setProperty("javax.net.ssl.trustStoreType", "JKS");
        System.setProperty("javax.net.ssl.trustStore", "nfe.keystore");
        
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", 443, socketFactory));
		
        HttpResponse response = httpclient.execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        
        String xmlRetorno = EntityUtils.toString(entityResponse, "UTF-8");
        xmlRetorno = Util.strings.tiraAcento(xmlRetorno.toString());
		return xmlRetorno;
	}
	
	
	private String executeEliss(String xmlDentro, String operation) throws ServiceException, AxisFault, SOAPException{
		String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" >"
					+ "<soapenv:Header/>"
						+ "<soapenv:Body>"
							+ xmlDentro
						+ "</soapenv:Body>" 
					+ "</soapenv:Envelope>";

		SimpleProvider config = new SimpleProvider(new BasicClientConfig());
		SimpleTargetedChain c = new SimpleTargetedChain(new CommonsHTTPSender());
		config.deployTransport("http", c);

		Service service = new Service(config);
		Call call = (Call) service.createCall();
		call.setOperationName(operation);
		call.setTargetEndpointAddress("http://201.49.29.18:8081/el-nfse/RpsServiceService");

		RPCElement next = (RPCElement) call.invoke(new Message(xml)).getBody().getChildElements().next();
		Iterator<?> childElements = next.getChildElements();

		if (childElements.hasNext()) {
			MessageElement outputXML = (MessageElement) childElements.next();

			if (operation.equals("autenticarContribuinte")) {
				return outputXML.getChildElements().next().toString();
			} else
				return outputXML.toString();
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void enviarArquivoCancelamentoIssMap(WebRequestContext request, Arquivonf arquivonf) throws Exception {
		arquivonf = this.loadForParacatu(arquivonf);
		
		//NfseIssMapCartaCancelamento nfseIssMapCartaCancelamento = this.createXmlCancelamentoNfseIssMap(arquivonf.getNumero(), arquivonf.getEmpresa(), arquivonf.getNota(), "", arquivonf.getConfiguracaonfe());
	}
	
	@SuppressWarnings("unchecked")
	public void enviarArquivoIssMap(WebRequestContext request, Arquivonf arquivonf) throws Exception {
		arquivonf = this.loadForParacatu(arquivonf);
		
		Arquivo arquivo = arquivoService.loadWithContents(arquivonf.getArquivoxml());
		
		Element rootElementXml = SinedUtil.getRootElementXML(arquivo.getContent());
		
		if (rootElementXml != null && rootElementXml.getChildren() != null && rootElementXml.getChildren().size() > 0) {
			for (int i = 0 ; i < rootElementXml.getChildren().size() ; i++) {
				Element elementFound = (Element) rootElementXml.getChildren().get(i);
				
				if (!elementFound.getName().equals("key") && !org.apache.commons.lang.StringUtils.isEmpty(elementFound.getText())) {
					elementFound.setText(NfseIssMapCriptografia.cifrar(elementFound.getText(), arquivonf.getConfiguracaonfe().getToken()));
				}
			}
		} else {
			throw new SinedException("Arquivo XML vazio.");
		}
		
		String a = new XMLOutputter().outputString(rootElementXml); //XML Criptografado
		StringEntity stringEntity = new StringEntity(new XMLOutputter().outputString(rootElementXml));
		
		HttpPost httpPost = null;
		if (Prefixowebservice.ARANDU_PROD.equals(arquivonf.getConfiguracaonfe().getPrefixowebservice())) {
			httpPost = new HttpPost("https://www.issmap.com.br/ws/rps/novo/enviar/17");
		} else {
			httpPost = new HttpPost("https://www.issmap.com.br/ws/rps/teste/enviar/17");
		}
		
		httpPost.setEntity(stringEntity);
		httpPost.setHeader(HTTP.CONTENT_TYPE, "application/xml; charset=UTF-8");
		
		PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
        cm.setMaxTotal(100);

        HttpClient httpclient = new DefaultHttpClient(cm);
		
        HttpResponse response = httpclient.execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        
        // PROCESSAMENTO DO RETORNO

        String xmlRetorno = EntityUtils.toString(entityResponse, "UTF-8");
        xmlRetorno = Util.strings.tiraAcento(xmlRetorno.toString());
		Arquivo arquivoXmlRetornoEnvio = new Arquivo(xmlRetorno.getBytes(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivonf.setArquivoretornoenvio(arquivoXmlRetornoEnvio);
		arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
		
		arquivoService.saveOrUpdate(arquivoXmlRetornoEnvio);
		
		this.updateArquivoretornoenvio(arquivonf);
        
		Element rootElement = SinedUtil.getRootElementXML(xmlRetorno.getBytes());
		
		if(rootElement != null){
			List<Element> listaCodigoElement = SinedUtil.getListChildElement("codigo", rootElement.getContent());
			
			List<String> listaErro = new ArrayList<String>();
			boolean sucesso = false;
			for (Element codigoElement : listaCodigoElement) {
				String descricaoCodigo = codigoElement.getText();
				if(descricaoCodigo != null && !descricaoCodigo.equals("")){
					if(descricaoCodigo.equals("100")){
						sucesso = true;
					} else {
						listaErro.add(descricaoCodigo);
					}
				}
			}
			
			if(sucesso){
				/*this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				
				Element numero_nfse = SinedUtil.getChildElement("numero_nfse", rootElement.getContent());
				Element data_nfse = SinedUtil.getChildElement("data_nfse", rootElement.getContent());
				Element hora_nfse = SinedUtil.getChildElement("hora_nfse", rootElement.getContent());
				Element link_nfse = SinedUtil.getChildElement("link_nfse", rootElement.getContent());
				
				List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByArquivonf(arquivonf);
				Arquivonfnota arquivonfnota = listaArquivonfnota.get(0);
				
				String numeroNfse = numero_nfse.getText();
				Date dataEmissao = FORMAT_DATETIME_PARACATU.parse(data_nfse.getText() + " " + hora_nfse.getText());
				String urlimpressaoparacatu = link_nfse.getText();
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
					try {
						vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				arquivonfnotaService.updateNumeroParacatu(arquivonfnota, numeroNfse, new Timestamp(dataEmissao.getTime()), urlimpressaoparacatu);
				
				if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
				} else {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
				}
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);*/
				
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			for (String erroStr : listaErro) {
				Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
				
				arquivonfnotaerro.setArquivonf(arquivonf);
				arquivonfnotaerro.setMensagem(erroStr);
				arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
				
				arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			}
			
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void enviarArquivoParacatu(WebRequestContext request, Arquivonf arquivonf) throws ClientProtocolException, IOException, JDOMException, ParseException {
		arquivonf = this.loadForParacatu(arquivonf);
		
		boolean erro = false;
		
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		
		String login = configuracaonfe.getLoginparacatu();
		String senha = configuracaonfe.getSenhaparacatu();
		
		// VALIDAÇÃO DOS DADOS NECESSÁRIOS
		
		if(login == null || login.trim().equals("")){
			request.addError("Favor preencher o login na configuração de NFS-e.");
			erro = true;
		}
		
		if(senha == null || senha.trim().equals("")){
			request.addError("Favor preencher o senha na configuração de NFS-e.");
			erro = true;
		}
		
		Municipio municipio = configuracaonfe.getEndereco().getMunicipio();
		if(municipio == null){
			request.addError("Favor preencher o município na configuração de NFS-e.");
			erro = true;
		}
		String cdsiafi = municipio.getCdsiafi();
		
		if(erro) return;
		
		// MONTAGEM DA REQUISIÇÃO HTTP
		
		MultipartEntity entity = new MultipartEntity();
		entity.addPart("login", new StringBody(login));
		entity.addPart("senha", new StringBody(senha));
		entity.addPart("cidade", new StringBody(cdsiafi));
		entity.addPart("f1", new FileBody(new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(arquivonf.getArquivoxml()))));
		
		HttpPost httpPost = new HttpPost("http://www.nfs-e.net/datacenter/include/nfw/importa_nfw/nfw_import_upload.php?eletron=1");
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept-Charset", "UTF-8");
		
		PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
        cm.setMaxTotal(100);

        HttpClient httpclient = new DefaultHttpClient(cm);
		
        HttpResponse response = httpclient.execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        
        // PROCESSAMENTO DO RETORNO

        String xmlRetorno = EntityUtils.toString(entityResponse, "UTF-8");
        xmlRetorno = Util.strings.tiraAcento(xmlRetorno.toString());
		Arquivo arquivoXmlRetornoEnvio = new Arquivo(xmlRetorno.getBytes(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivonf.setArquivoretornoenvio(arquivoXmlRetornoEnvio);
		arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
		
		arquivoService.saveOrUpdate(arquivoXmlRetornoEnvio);
		
		this.updateArquivoretornoenvio(arquivonf);
        
		Element rootElement = SinedUtil.getRootElementXML(xmlRetorno.getBytes());
		
		Element mensagemElement = SinedUtil.getChildElement("mensagem", rootElement.getContent());
		if(mensagemElement != null){
			List<Element> listaCodigoElement = SinedUtil.getListChildElement("codigo", mensagemElement.getContent());
			
			List<String> listaErro = new ArrayList<String>();
			boolean sucesso = false;
			for (Element codigoElement : listaCodigoElement) {
				String descricaoCodigo = codigoElement.getText();
				if(descricaoCodigo != null && !descricaoCodigo.equals("")){
					String[] desc = descricaoCodigo.split(" - ");
					if(desc.length != 2) continue;
					
					if(desc[0].equals("00001")){
						sucesso = true;
					} else {
						listaErro.add(descricaoCodigo);
					}
				}
			}
			
			if(sucesso){
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
				
				Element numero_nfse = SinedUtil.getChildElement("numero_nfse", rootElement.getContent());
				Element data_nfse = SinedUtil.getChildElement("data_nfse", rootElement.getContent());
				Element hora_nfse = SinedUtil.getChildElement("hora_nfse", rootElement.getContent());
				Element link_nfse = SinedUtil.getChildElement("link_nfse", rootElement.getContent());
				
				List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByArquivonf(arquivonf);
				Arquivonfnota arquivonfnota = listaArquivonfnota.get(0);
				
				String numeroNfse = numero_nfse.getText();
				Date dataEmissao = FORMAT_DATETIME_PARACATU.parse(data_nfse.getText() + " " + hora_nfse.getText());
				String urlimpressaoparacatu = link_nfse.getText();
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
					try {
						vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				arquivonfnotaService.updateNumeroParacatu(arquivonfnota, numeroNfse, new Timestamp(dataEmissao.getTime()), urlimpressaoparacatu);
				
				if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
				} else {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
				}
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
				
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			for (String erroStr : listaErro) {
				Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
				
				arquivonfnotaerro.setArquivonf(arquivonf);
				arquivonfnotaerro.setMensagem(erroStr);
				arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
				
				arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			}
			
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
			}
		}
	}
	
	/**
	 * Método que faz referência ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfDAO#loadForParacatu(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/12/2012
	 */
	private Arquivonf loadForParacatu(Arquivonf arquivonf) {
		return arquivonfDAO.loadForParacatu(arquivonf);
	}
	
	/**
	 * Gera o Arquivo XML da NFS-e para download a partir do Arquivonfnota.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#updateArquivoxml(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 26/07/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Arquivo getArquivoXmlNfse(Arquivonfnota arquivonfnota) {
		if(arquivonfnota.getArquivoxml() != null && arquivonfnota.getArquivoxml().getCdarquivo() != null){
			return arquivoDAO.loadWithContents(arquivonfnota.getArquivoxml());
		} 

		String numeroNFse = arquivonfnota.getNumeronfse();
		String numeroNota = arquivonfnota.getNota().getNumero();
		if(numeroNota == null || numeroNota.equals("")) 
			numeroNota = "<sem número> (ID: " + arquivonfnota.getNota().getCdNota() + ")";
		
		try {
			Arquivo arquivo = null;
			Element compNfseRetornado = null;
			String erro = null;
			
			if(arquivonfnota.getArquivonf() != null){
				Arquivonf arquivonf = arquivonfnota.getArquivonf();
				if((arquivonf.getArquivoretornoconsulta() != null && arquivonf.getArquivoretornoconsulta().getCdarquivo() != null) ||
						arquivonf.getArquivoretornoenvio() != null && arquivonf.getArquivoretornoenvio().getCdarquivo() != null) {
					Arquivo arquivoretornoconsulta = null;
					
					if(arquivonfnota.getArquivoxmlretornoconsulta() != null){
						arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonfnota.getArquivoxmlretornoconsulta());
					} else if (arquivonf.getArquivoretornoconsulta() != null) {
						arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonf.getArquivoretornoconsulta());
					} else if (arquivonf.getArquivoretornoenvio() != null) {
						arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonf.getArquivoretornoenvio());
					}
					
					SAXBuilder sb = new SAXBuilder();  
					Document d = sb.build(new ByteArrayInputStream(arquivoretornoconsulta.getContent()));
					Element rootElement = d.getRootElement();
					
					if("fisslex".equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().getPrefixo())){
						rootElement = SinedUtil.getChildElement("Consultarloterpsresposta", d.getRootElement().getContent());
						rootElement = SinedUtil.getRootElementXML(rootElement.getValue().getBytes());
					}
					
					if(rootElement.getName().equals("CompNfse") || rootElement.getName().equals("NFSE") || rootElement.getName().equals("ComplNfse")){
						rootElement.detach();
						compNfseRetornado = rootElement;
					} else {
						Element listaNfse = SinedUtil.getChildElement("ListaNfse", rootElement.getContent());
						if(listaNfse == null){
							if(rootElement.getName().equals("ConsultarLoteRpsResponse")){
								Element consultarLoteRpsResultElt = SinedUtil.getChildElement("ConsultarLoteRpsResult", rootElement.getContent());
								if(consultarLoteRpsResultElt != null){
									listaNfse = SinedUtil.getChildElement("ListaNfse", consultarLoteRpsResultElt.getContent());
								}
							}
							if(listaNfse == null) listaNfse = rootElement;
						}
						
						List<Element> listaCompNfse = SinedUtil.getListChildElement("CompNfse", listaNfse.cloneContent());
						if(listaCompNfse != null && listaCompNfse.size() > 0){
							for (Element compNfse : listaCompNfse) {
								Element nfse = SinedUtil.getChildElement("Nfse", compNfse.getContent());
								if(nfse == null) {
									Element tcCompNfse = SinedUtil.getChildElement("tcCompNfse", compNfse.getContent());
									if(tcCompNfse != null){
										nfse = SinedUtil.getChildElement("Nfse", tcCompNfse.getContent());
									}
								}
								if(nfse != null){
									Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
									if(infNfse != null){
										Element numero = SinedUtil.getChildElement("Numero", infNfse.getContent());
										if(numero != null){
											String numeroStr = numero.getText();
											if(numeroNFse.equals(numeroStr)){
												compNfseRetornado = compNfse;
												break;
											}
										} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Numero'.";
									} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'InfNfse'.";
								} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Nfse'.";
							}
						} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'CompNfse'.";
						
						if (compNfseRetornado == null) {
							List<Element> listaComplNfse = SinedUtil.getListChildElement("ComplNfse", listaNfse.cloneContent());
							if(listaComplNfse != null && listaComplNfse.size() > 0){
								for (Element complNfse : listaComplNfse) {
									Element nfse = SinedUtil.getChildElement("Nfse", complNfse.getContent());
									if(nfse == null) {
										Element tcComplNfse = SinedUtil.getChildElement("tcComplNfse", complNfse.getContent());
										if(tcComplNfse != null){
											nfse = SinedUtil.getChildElement("Nfse", tcComplNfse.getContent());
										}
									}
									if(nfse != null){
										Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
										if(infNfse != null){
											Element numero = SinedUtil.getChildElement("Numero", infNfse.getContent());
											if(numero != null){
												String numeroStr = numero.getText();
												if(numeroNFse.equals(numeroStr)){
													compNfseRetornado = complNfse;
													break;
												}
											} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Numero'.";
										} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'InfNfse'.";
									} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Nfse'.";
								}
							} else erro = "Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'ComplNfse'.";
						}
					}
				} else erro = "Não foi possível carregar o arquivo XML de retorno da consulta do lote.";
			} else erro = "Não foi possível carregar o 'Arquivonf'.";
			
			if (org.apache.commons.lang.StringUtils.isNotEmpty(erro) && compNfseRetornado == null) {
				throw new SinedException(erro);
			}
			
			if(compNfseRetornado != null){
				Document doc = new Document(compNfseRetornado);
				String xml = new XMLOutputter().outputString(doc);
				
				arquivo = new Arquivo(xml.getBytes(), "nfse_" + numeroNFse + ".xml", "text/xml");
				
				arquivonfnota.setArquivoxml(arquivo);
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
			}
			
			return arquivo;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na geração do XML da nota " + numeroNota + ": " + e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public Arquivo getArquivoXmlNfseSmarapd(Arquivonfnota arquivonfnota) {
		if(arquivonfnota.getArquivoxml() != null && arquivonfnota.getArquivoxml().getCdarquivo() != null){
			return arquivoDAO.loadWithContents(arquivonfnota.getArquivoxml());
		} 

		String numeroNFse = arquivonfnota.getNumeronfse();
		String numeroNota = arquivonfnota.getNota().getNumero();
		if(numeroNota == null || numeroNota.equals("")) 
			numeroNota = "<sem número> (ID: " + arquivonfnota.getNota().getCdNota() + ")";
		
		try {
			Arquivo arquivo = null;
			Element nfdokRetornado = null;
			
			if(arquivonfnota.getArquivonf() != null){
				Arquivonf arquivonf = arquivonfnota.getArquivonf();
				if(arquivonf.getArquivoretornoconsulta() != null && arquivonf.getArquivoretornoconsulta().getCdarquivo() != null){
					Arquivo arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonf.getArquivoretornoconsulta());
					
					SAXBuilder sb = new SAXBuilder();  
					Document d = sb.build(new ByteArrayInputStream(arquivoretornoconsulta.getContent()));
					Element rootElement = d.getRootElement();
					
					List<Element> listaNfdok = SinedUtil.getListChildElement("nfdok", rootElement.cloneContent());
					if(listaNfdok != null && listaNfdok.size() > 0){
						for (Element nfdok : listaNfdok) {
							Element newDataSet = SinedUtil.getChildElement("NewDataSet", nfdok.getContent());
							if(newDataSet != null){
								Element nota_fiscal = SinedUtil.getChildElement("NOTA_FISCAL", newDataSet.getContent());
								if(nota_fiscal != null){
									Element numero = SinedUtil.getChildElement("NumeroNota", nota_fiscal.getContent());
									if(numero != null){
										String numeroStr = numero.getText();
										if(numeroNFse.equals(numeroStr)){
											nfdokRetornado = nfdok;
											break;
										}
									} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Numero'.");
								} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'InfNfse'.");
							} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'Nfse'.");
						}
					} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'CompNfse'.");
				} else throw new SinedException("Não foi possível carregar o arquivo XML de retorno da consulta do lote.");
			} else throw new SinedException("Não foi possível carregar o 'Arquivonf'.");
			
			if(nfdokRetornado != null){
				Document doc = new Document(nfdokRetornado);
				String xml = new XMLOutputter().outputString(doc);
				
				arquivo = new Arquivo(xml.getBytes(), "nfse_" + numeroNFse + ".xml", "text/xml");
				
				arquivonfnota.setArquivoxml(arquivo);
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
			}
			
			return arquivo;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na geração do XML da nota " + numeroNota + ": " + e.getMessage());
		}
	}
	
	public Arquivo getArquivoxmlEstadoByArquivonfnota(Arquivonfnota arquivonfnota) throws JDOMException, IOException{
		Arquivo arquivoxml;
		if(arquivonfnota.getArquivonf() == null || 
				arquivonfnota.getArquivonf().getArquivoxmlassinado() == null || 
				arquivonfnota.getArquivonf().getArquivoxmlassinado().getCdarquivo() == null || 
				(arquivonfnota.getArquivonf().getArquivoretornoconsulta() == null && arquivonfnota.getArquivoxmlcancelamento() == null) ||
				(arquivonfnota.getArquivonf().getArquivoretornoconsulta().getCdarquivo() == null && arquivonfnota.getArquivoxmlcancelamento().getCdarquivo() ==null)){
			throw new SinedException("Não foi possível obter o arquivo XML na NF-e.");
		}
		
		boolean considerarXmlCancelamento = arquivonfnota.getNota() != null && arquivonfnota.getNota().getNotaStatus() != null && NotaStatus.CANCELADA.equals(arquivonfnota.getNota().getNotaStatus()) && arquivonfnota.getDtcancelamento() != null;
		if(considerarXmlCancelamento || (arquivonfnota.getArquivoxml() == null || (arquivonfnota.getArquivoxml() != null && arquivonfnota.getArquivoxml().getCdarquivo() == null))){
			arquivoxml = this.getArquivoXmlNotaEstado(arquivonfnota.getArquivonf().getArquivoxmlassinado(), 
													arquivonfnota.getArquivonf().getArquivoretornoconsulta(), 
													arquivonfnota.getArquivoxmlretornoconsulta(),
													arquivonfnota.getChaveacesso(),
													arquivonfnota.getProtocolonfe(),
													considerarXmlCancelamento ? arquivonfnota.getArquivoretornocancelamento() : null);
			
			arquivonfnota.setArquivoxml(arquivoxml);
			arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
			arquivonfnotaService.updateArquivoxml(arquivonfnota);
		} else {
			arquivoxml = arquivoService.loadWithContents(arquivonfnota.getArquivoxml());
		}
			
		return arquivoxml;
	}
	
	@SuppressWarnings("unchecked")
	private Arquivo getArquivoXmlNotaEstado(Arquivo arquivoenvio, Arquivo arquivoretorno, Arquivo arquivoxmlretornoconsulta, String chaveacesso, String protocolonfe, Arquivo arquivoCancelamento) throws JDOMException, IOException {
		arquivoenvio = arquivoService.loadWithContents(arquivoenvio);
		arquivoretorno = arquivoService.loadWithContents(arquivoretorno);
		if(arquivoxmlretornoconsulta != null){
			arquivoxmlretornoconsulta = arquivoService.loadWithContents(arquivoxmlretornoconsulta);
		}
		
		SAXBuilder sb = new SAXBuilder();
		Document d_retorno = sb.build(new ByteArrayInputStream(arquivoretorno.getContent()));
		Document d_envio = sb.build(new ByteArrayInputStream(arquivoenvio.getContent()));
		
		Document d_retornoindividual = null;
		if(arquivoxmlretornoconsulta != null){
			d_retornoindividual = sb.build(new ByteArrayInputStream(arquivoxmlretornoconsulta.getContent()));
		}
		
		String chacessoId = "NFe" + chaveacesso;
		String nfe = "", protnfe = "";
		boolean achounfe = false, achouprotnfe = false;
		
		Element rootElement_envio = d_envio.getRootElement();
		List<Element> listaNFe = SinedUtil.getListChildElement("NFe", rootElement_envio.cloneContent());
		for (Element nfeEle : listaNFe) {
			Element infNFe = SinedUtil.getChildElement("infNFe", nfeEle.getContent());
			Attribute attribute = infNFe.getAttribute("Id");
			String value = attribute.getValue();
			if(chacessoId.equals(value)){
				nfeEle.setNamespace(Namespace.getNamespace("http://www.portalfiscal.inf.br/nfe"));
				Document doc = new Document(nfeEle);
				nfe = new XMLOutputter().outputString(doc);
				achounfe = true;
				break;
			}
		}
		
		boolean v3 = false;
		boolean v4 = false;
		
		Element rootElement_retorno = d_retorno.getRootElement();
		List<Element> listaProtNFe = SinedUtil.getListChildElement("protNFe", rootElement_retorno.cloneContent());
		
		if(d_retornoindividual != null){
			Element rootElement_retornoindividual = d_retornoindividual.getRootElement();
			listaProtNFe = SinedUtil.getListChildElement("protNFe", rootElement_retornoindividual.cloneContent());
		}
		
		for (Element protnfeEle : listaProtNFe) {
			Attribute attrVersao = protnfeEle.getAttribute("versao");
			if(attrVersao != null && attrVersao.getValue() != null && attrVersao.getValue().trim().equals("3.10")){
				v3 = true;
			}
			if(attrVersao != null && attrVersao.getValue() != null && attrVersao.getValue().trim().equals("4.00")){
				v4 = true;
			}
			Element infProt = SinedUtil.getChildElement("infProt", protnfeEle.getContent());
			Element chNFe = SinedUtil.getChildElement("chNFe", infProt.getContent());
			String value = chNFe.getText();
			
			if(chaveacesso.equals(value)){
				try {
					if(arquivoCancelamento != null){
						arquivoCancelamento = arquivoService.loadWithContents(arquivoCancelamento);
						Document d_cancelamento = sb.build(new ByteArrayInputStream(arquivoCancelamento.getContent()));
						Element rootElement_cancelamento = d_cancelamento.getRootElement();
						Element retEvento = SinedUtil.getChildElement("retEvento", rootElement_cancelamento.cloneContent());
						Element infEvento = SinedUtil.getChildElement("infEvento", retEvento.getContent());
						
						Element dhRegEvento = SinedUtil.getChildElement("dhRegEvento", infEvento.getContent());
						Element nProt = SinedUtil.getChildElement("nProt", infEvento.getContent());
						for(Object obj : protnfeEle.getContent()){
							if(obj instanceof Element && ((Element)obj).getName().equalsIgnoreCase("infProt")){
								for(Object obj2 : ((Element)obj).getContent()){
									if(obj2 instanceof Element){
										Element elt = ((Element)obj2);
										if(elt.getName().equalsIgnoreCase("dhRecbto") && dhRegEvento != null){
											elt.setText(dhRegEvento.getText());
										}
										if(elt.getName().equalsIgnoreCase("nProt") && nProt != null){
											elt.setText(nProt.getText());
										}
										if(elt.getName().equalsIgnoreCase("cStat")){
											elt.setText("101");
										}
										if(elt.getName().equalsIgnoreCase("xMotivo")){
											elt.setText("Cancelamento de NF-e homologado");
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Document doc = new Document(protnfeEle);
				protnfe = new XMLOutputter().outputString(doc);
				achouprotnfe = true;
				break;
			}
		}
		
		if(!achounfe || !achouprotnfe){
			throw new SinedException("Não foi possível obter o arquivo XML na NF-e.");
		}
		
		nfe = nfe.replaceAll("<\\?.+\\?>", "");
		protnfe = protnfe.replaceAll("<\\?.+\\?>", "");
		String versao = "2.00";
		if(v3) versao = "3.10";
		if(v4) versao = "4.00";
		String s = 
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
			"<nfeProc versao=\"" + versao + "\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">" +
				nfe + 
				protnfe +
			"</nfeProc>";
			
		return new Arquivo(s.getBytes(), protocolonfe + "-procNFe.xml", "text/xml");
	}
	
	/**
	 * Método que faz referência ao método de envio de e-mail de NF passando a lista de e-mail nula.
	 *
	 * @param whereIn
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2014
	 */
	public EnvioEmailResultadoBean enviarNFCliente(String whereIn, TipoEnvioNFCliente... tipo) {
		return this.enviarNFCliente(whereIn, null, false, true, tipo);
	}
	
	public EnvioEmailResultadoBean enviarNFCliente(String whereIn, Boolean isNfe, TipoEnvioNFCliente... tipo) {
		return this.enviarNFCliente(whereIn, null, false, isNfe, tipo);
	}
	
	/**
	 * Método que envia por e-mail o PDF e/ou o XML de NF de serviço ou produto
	 *
	 * @param whereIn
	 * @param listaEmailParaEnviar
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2014
	 */
	public EnvioEmailResultadoBean enviarNFCliente(String whereIn, List<EnvioEmailPessoaBean> listaEmailParaEnviar, Boolean enviarTransportador, Boolean isNfe, TipoEnvioNFCliente... tipo) {
		EnvioEmailResultadoBean resultado = new EnvioEmailResultadoBean();
		
		Boolean boletoAnexado = false;
		
		// CARREGA AS NOTAS GENÉRICAS PARA MONTAR A LISTA DE E-MAILS
		List<Nota> listaNota = notaService.findForEnvioNFCliente(whereIn);
		List<TipoEnvioNFCliente> listaTipo = Arrays.asList(tipo);
		
		// CARREGA A NOTAS DE SERVIÇO CASO VENHA O TIPO DE SERVIÇO
		List<NotaFiscalServico> listaNotaFiscalServico = null;
		if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF) || listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML)){
			listaNotaFiscalServico = notaFiscalServicoService.findForNFe(whereIn);
		}
		
		// CARREGA A NOTAS DE PRODUTO CASO VENHA O TIPO DE PRODUTO
		List<Notafiscalproduto> listaNotafiscalproduto = null;
		if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF) || listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML)){
			listaNotafiscalproduto = notafiscalprodutoService.findForDanfe(whereIn);
		}
		
		// PARÂMETRO PARA SABER SE VAI SER ENVIADO PARA O E-MAIL PRINCIPAL DO CLIENTE
		// CASO VIER DA TELA DE ESCOLHA DO E-MAIL NãO CONSIDERA ESTE PARÂMETRO
		String paramStr = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.ENVIOFINANCEIROMAILPRINCIPALCLIENTE);
		boolean paramEmailPrincipal = paramStr != null && paramStr.trim().toUpperCase().equals("TRUE");
		
		for (Nota nota : listaNota) {
			String boletosStr = "";
			Cliente cliente = nota.getCliente();
			List<PessoaContato> listaPessoaContato = cliente.getListaContato();
			MergeReport mergeReportBoleto = null;
			
			String numero = nota.getNumero();
			if(numero == null || numero.trim().equals("")){
				numero = "'sem número'";
			}
			
			List<EnvioEmailPessoaBean> listaPessoaEnvio = new ArrayList<EnvioEmailPessoaBean>();
			
			if(listaEmailParaEnviar != null && listaEmailParaEnviar.size() > 0){
				// VINDO DA TELA DE ESCOLHA DE E-MAIL
				listaPessoaEnvio.addAll(listaEmailParaEnviar);
			} else {
				// SEM A ESCOLHA DE E-MAIL
				if(paramEmailPrincipal){
					// INCLUIR E-MAIL PRINCIPAL DO CLIENTE
					if(cliente != null && 
							cliente.getEmail() != null && 
							!cliente.getEmail().trim().equals("")){
						listaPessoaEnvio.add(new EnvioEmailPessoaBean(true, cliente.getCdpessoa(), cliente.getNome(), cliente.getEmail(), true));
					}
				}
				
				// INCLUIR OS CONTATOS COM A MARCAÇÃO DE RECEBER NF
				if(listaPessoaContato != null && listaPessoaContato.size() > 0){
					for (PessoaContato pessoaContato : listaPessoaContato) {
						br.com.linkcom.sined.geral.bean.Contato contato = contatoService.getContatoByCdPessoaLigacao(pessoaContato.getContato().getCdpessoa());
						if(contato.getEmailcontato() != null && 
								!contato.getEmailcontato().trim().equals("")){
							
							if(Boolean.TRUE.equals(contato.getRecebernf())){
								listaPessoaEnvio.add(new EnvioEmailPessoaBean(true, contato.getCdpessoa(), contato.getNome(), contato.getEmailcontato(), Boolean.TRUE.equals(contato.getReceberboleto()))); 
							}
						}
					}
				}
			}
			
			List<Documento> listaDocumentoEnviadosDownloadCaptcha = new ArrayList<Documento>();
			List<Documento> listaDocumento = contareceberService.findByNota(nota.getCdNota().toString());
			List<Documento> docsGerarBoleto = new ArrayList<Documento>();
						
			if(listaDocumento!=null && !listaDocumento.isEmpty()){
				String whereInDocumentos = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
				List<Documento> docs = documentoService.findForBoleto(whereInDocumentos);
				String nomeArquivo = "boleto_" + numero + ".pdf";
				
				for(Documento doc: docs){
					Boolean enviarBoleto = doc.getContacarteira() != null && doc.getContacarteira().getAprovadoproducao() != null 
							&& doc.getContacarteira().getAprovadoproducao()
							&& (doc.getContacarteira().getNaogerarboleto() == null || !doc.getContacarteira().getNaogerarboleto());
					
					if(doc.getConta() != null && doc.getDocumentotipo() != null && Boolean.TRUE.equals(doc.getDocumentotipo().getBoleto()) && doc.getContacarteira() != null 
							&& ((doc.getContacarteira().getBancogeranossonumero() == null || !doc.getContacarteira().getBancogeranossonumero()) 
							|| (doc.getContacarteira().getBancogeranossonumero() != null && doc.getContacarteira().getBancogeranossonumero() && !org.apache.commons.lang.StringUtils.isEmpty(doc.getNossonumero())))
							&& enviarBoleto) {
						try {
							docsGerarBoleto.add(doc);
							
							if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
								StringBuilder url = new StringBuilder();
								url.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(doc.getCddocumento())
									.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(doc.getCddocumento().toString()))));
								
								listaDocumentoEnviadosDownloadCaptcha.add(doc);
								
								if (boletosStr.length() == 0) {
									boletosStr = "E o(s) link(s) para impressão do(s) boleto(s) são: <br>";
								}
								boletosStr += "<a href=\"" + url + "\">Clique aqui para imprimir o boleto " + doc.getNumeroCddocumento() + "</a><br>";
							} else if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
								Report report = new Report("/financeiro/relBoleto");
								report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(doc, false)});
								if(mergeReportBoleto == null){
									mergeReportBoleto = new MergeReport(nomeArquivo);
								}
								mergeReportBoleto.addReport(report);	
							} else if (doc.getEmpresa() != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(doc.getEmpresa().getFormaEnvioBoleto())) {
								StringBuilder url = new StringBuilder();
								url.append(SinedUtil.getUrlWithContext()).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(doc.getCddocumento())
									.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(doc.getCddocumento().toString()))));
								
								if (boletosStr.length() == 0) {
									boletosStr = "E o(s) link(s) para impressão do(s) boleto(s) são: <br>";
								}
								boletosStr += "<a href=\"" + url + "\">Clique aqui para imprimir o boleto " + doc.getNumeroCddocumento() + "</a><br>";
							}
						} catch (Exception e) {
							resultado.addMsgErro("Não foi possível gerar boleto do documento " + doc.getNumeroCddocumento());
						}
					}
				}				
			}
			
			// INCLUIR E-MAIL DO TRANSPORTADOR
			Notafiscalproduto notafiscalproduto = null;
			if(enviarTransportador != null && enviarTransportador &&
				(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF) || listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML))){
				notafiscalproduto = this.getNotaFiscalProdutoInLista(nota, listaNotafiscalproduto);
				if(notafiscalproduto == null){
					resultado.addErroComMsg("Não foi possível carregar a nota " + numero + " para o envio de NF-e.");
					continue;
				}
				Fornecedor transportador = notafiscalproduto.getTransportador();
				if(transportador != null && transportador.getEmail() != null && !transportador.getEmail().trim().equals("")){
					listaPessoaEnvio.add(new EnvioEmailPessoaBean(Boolean.TRUE, transportador.getCdpessoa(), transportador.getNome(), transportador.getEmail(), false));
				}
			}
			
			// VALIDAÇÃO SE FOI ESCOLHIDO ALGUM E-MAIL A SER ENVIADO
			if(listaPessoaEnvio == null || listaPessoaEnvio.size() == 0){
				resultado.addErroComMsg("E-mail não encontrado para a nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : ""));
				continue;
			}
			
			Envioemailtipo envioemailtipo = null;
			Arquivonfnota arquivonfnota = null;
			
			// BUSCA INFORMAÇÕES COMPARTILHADAS PARA NOTA FISCAL DE SERVIÇO
			NotaFiscalServico notaFiscalServico = null;
			String numeroNfse = null;
			Configuracaonfe configuracaonfe = null;
			Prefixowebservice prefixowebservice = null;
			ReportTemplateBean templateNfse = null;
			if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) || listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
				notaFiscalServico = this.getNotaFiscalServicoInLista(nota, listaNotaFiscalServico);
				if(notaFiscalServico == null){
					resultado.addErroComMsg("Não foi possível carregar a nota " + numero + " para o envio de NFS-e.");
					continue;
				}
				arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(notaFiscalServico);
				
				if(arquivonfnota == null){
					resultado.addErroComMsg("Não foi possível carregar a informação do arquivo da nota " + numero + " para o envio de NFS-e.");
					continue;
				}
				numeroNfse = arquivonfnota.getNumeronfse();
				configuracaonfe = configuracaonfeService.loadForEntrada(arquivonfnota.getArquivonf().getConfiguracaonfe());
				prefixowebservice = configuracaonfe.getPrefixowebservice();
				
				if(configuracaonfe.getTemplateemissaonfse() != null){
					templateNfse = reporttemplateService.load(configuracaonfe.getTemplateemissaonfse());
				}
				
				if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF) && !listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML)){
					envioemailtipo = Envioemailtipo.ENVIO_NFSE_CLIENTE;
				}
				if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) && !listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
					envioemailtipo = Envioemailtipo.ENVIO_XML_CLIENTE;
				}
				if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) && listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
					envioemailtipo = Envioemailtipo.ENVIO_NFSE_XML_CLIENTE;
				}
			}
			
			// BUSCA INFORMAÇÕES COMPARTILHADAS PARA NOTA FISCAL DE PRODUTO
			String chaveacesso = null;
			String protocolonfe = null;
			String uf = null;
			boolean producao = true;
			if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF) || listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML)){
				if(notafiscalproduto == null){
					notafiscalproduto = this.getNotaFiscalProdutoInLista(nota, listaNotafiscalproduto);
				}
				if(notafiscalproduto == null){
					resultado.addErroComMsg("Não foi possível carregar a nota " + numero + " para o envio de NF-e.");
					continue;
				}
				arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
				if(arquivonfnota == null){
					resultado.addErroComMsg("Não foi possível carregar a chave de acesso da nota " + numero + " para o envio de NF-e.");
					continue;
				}
				configuracaonfe = configuracaonfeService.loadForEntrada(arquivonfnota.getArquivonf().getConfiguracaonfe());
				prefixowebservice = configuracaonfe.getPrefixowebservice();
				chaveacesso = arquivonfnota.getChaveacesso();
				protocolonfe = arquivonfnota.getProtocolonfe();
				uf = configuracaonfe.getUf().getSigla();
				producao = prefixowebservice.name().endsWith("PROD");
				envioemailtipo = Envioemailtipo.ENVIO_DANFE_XML_CLIENTE;
			}
			
			// DEFINE O ASSUNTO DO E-MAIL
			String assunto = null;
			if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) || listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
				assunto = "NFS-e número: " + numeroNfse;
			}
			if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) || listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
				if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF) && !listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML)){
					if(isNfe){
						assunto = "DANFE da NF-e protocolo: " + protocolonfe;
					} else {
						assunto = "DANFCE da NFC-e protocolo: " + protocolonfe;
					}
				}
				if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) && !listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
					if(isNfe){
						assunto = "Arquivo XML da NF-e protocolo: " + protocolonfe;
					} else {
						assunto = "Arquivo XML da NFC-e protocolo: " + protocolonfe;
					}
				}
				if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) && listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
					if(isNfe){
						assunto = "Arquivo XML e DANFE da NF-e protocolo: " + protocolonfe;
					} else {
						assunto = "Arquivo XML e DANFCE da NFC-e protocolo: " + protocolonfe;
					}
				}
			}
			
//			INCLUSÃO DOS ANEXOS QUE IRÃO NO E-MAIL
			List<Resource> anexos = new ArrayList<Resource>();
			if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
				try{
					String nomeArquivo = "nfse_" + numeroNfse + ".pdf";
					
					boolean salvador = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD));
					String modeloNFSE = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.LAYOUT_EMISSAO_NFSE);
					
					Report report;
					if(modeloNFSE != null && !modeloNFSE.isEmpty()){
						report = new Report("faturamento/" + modeloNFSE);
					} else {
						if(salvador){
							report = new Report("faturamento/notaFiscalEletronica_salvador"); 
						} else {
							report = new Report("faturamento/notaFiscalEletronica"); 
						}
					}
					
					boolean possuiTemplate = templateNfse != null;
					NotaFiscalEletronicaReportBean notafiscaleletronicaReportBean = notaFiscalServicoService.makePdfNfse(notaFiscalServico, arquivonfnota, configuracaonfe, null, possuiTemplate, false, null, modeloNFSE);
					if(possuiTemplate){
						Neo.getRequestContext().setAttribute("bean", notafiscaleletronicaReportBean);
						ReportTemplateFiltro filtro = new ReportTemplateFiltro();
						filtro.setTemplate(templateNfse);
						Resource notaTemplate = notaFiscalServicoTemplateReport.getPdfResource((WebRequestContext)Neo.getRequestContext(), filtro);
						anexos.add(notaTemplate);
					}else{
						List<NotaFiscalEletronicaReportBean> lista = new ArrayList<NotaFiscalEletronicaReportBean>();
						lista.add(notafiscaleletronicaReportBean);
						report.setDataSource(lista);
						report.addParameter("TITULO", "NFS-e - NOTA FISCAL DE SERVIÇO ELETRÔNICA");
						
						MergeReport mergeReport = new MergeReport(nomeArquivo);
						mergeReport.addReport(report);
						
						anexos.add(mergeReport.generateResource());
					
					}
				} catch (Exception e) {
					e.printStackTrace();
					resultado.addErroComMsg("Erro na impressão do PDF da nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : "") + ": " + e.getMessage());
					continue;
				}
			}
			
			if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML)){
				try {
					if(prefixowebservice.equals(Prefixowebservice.SPISS_HOM) || prefixowebservice.equals(Prefixowebservice.SPISS_PROD)){
						throw new SinedException("Não é possível gerar o XML da Prefeitura de São Paulo.");
					}
					if(prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_PROD)){
						throw new SinedException("Não é possível gerar o XML da Prefeitura de Blumenau.");
					}
					if(prefixowebservice.equals(Prefixowebservice.LIMEIRA_HOM) || prefixowebservice.equals(Prefixowebservice.LIMEIRA_PROD)){
						throw new SinedException("Não é possível gerar o XML da Prefeitura de Limeira.");
					}
					
					
					Arquivo arquivo;
					if("smarapd".equals(prefixowebservice.getPrefixo())){
						arquivo = this.getArquivoXmlNfseSmarapd(arquivonfnota);
					}else {
						arquivo = this.getArquivoXmlNfse(arquivonfnota);
					}
					
					Resource resourceXml = new Resource();
					resourceXml = new Resource();
					resourceXml.setContents(arquivo.getContent());
					resourceXml.setContentType(arquivo.getContenttype());
					resourceXml.setFileName(arquivo.getName());
					resourceXml.setSize(arquivo.getSize().intValue());
					
					anexos.add(resourceXml);
				} catch (Exception e) {
					e.printStackTrace();
					resultado.addErroComMsg("Erro na obtenção do XML da nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : "") + ": " + e.getMessage());
					continue;					
				}
			}
			
			if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
				try {
					String nomeArquivo = "danfe_" + protocolonfe + ".pdf";
					
					Report report = notafiscalprodutoService.danfe(notafiscalproduto);
					MergeReport mergeReport = new MergeReport(nomeArquivo);
					mergeReport.addReport(report);
					
					anexos.add(mergeReport.generateResource());
				} catch (Exception e) {
					e.printStackTrace();
					resultado.addErroComMsg("Erro na impressão da DANFE da nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : "") + ": " + e.getMessage());
					continue;
				}
			}
			
			if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML)){
				try {
					Arquivo arquivo = this.getArquivoxmlEstadoByArquivonfnota(arquivonfnota);
					
					Resource resourceXml = new Resource();
					resourceXml = new Resource();
					resourceXml.setContents(arquivo.getContent());
					resourceXml.setContentType(arquivo.getContenttype());
					resourceXml.setFileName(arquivo.getName());
					resourceXml.setSize(arquivo.getSize().intValue());
					
					anexos.add(resourceXml);
				} catch (Exception e) {
					e.printStackTrace();
					resultado.addErroComMsg("Erro na obtenção do XML da nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : "") + ": " + e.getMessage());
					continue;
				}
			}
			
			String emailRemetente = null;
			if(SinedUtil.getUsuarioLogado() != null){
				emailRemetente = SinedUtil.getUsuarioLogado().getEmail();
			} else if(nota.getEmpresa() != null){
				emailRemetente = nota.getEmpresa().getEmail();
			}
			if(emailRemetente == null || emailRemetente.equals("")){
				resultado.addErroComMsg("E-mail do remetente  não encontrado para envio da nota " + numero + (cliente != null ? (" do cliente " + cliente.getNome()) : ""));
				continue;
			}
			Map<Documento, Documentohistorico> listaDocumentohistorico = new HashMap<Documento, Documentohistorico>();;
			List<Resource> anexosAux = new ArrayList<Resource>();
			boolean sucesso = false;
			List<String> listaMensagemErro = new ArrayList<String>();
			EmailCobranca emailCobranca = null;
			for (EnvioEmailPessoaBean pEnvio : listaPessoaEnvio) {
				anexosAux.addAll(anexos);
				emailCobranca = null;
				if(Boolean.TRUE.equals(pEnvio.getReceber_boleto())){
					if (mergeReportBoleto != null) {
						try {
							anexosAux.add(mergeReportBoleto.generateResource());
							boletoAnexado = true;
						} catch (Exception e) {
							e.printStackTrace();
							listaMensagemErro.add("Erro ao anexar boleto no e-mail da nota " + numero + ": " + e.getMessage());
							continue;
						}
					}
					
//					REGISTRO DE E-MAIL DE COBRANÇA
					try {
						emailCobranca = new EmailCobranca(pEnvio.getEmail(), pEnvio.getCdpessoa());
						
						List<EmailCobrancaDocumento> listaEmailCobrancaDocumento = new ArrayList<EmailCobrancaDocumento>();
						for (Documento doc : docsGerarBoleto) {
							listaEmailCobrancaDocumento.add(new EmailCobrancaDocumento(doc));
						}
						emailCobranca.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
						
						EmailCobrancaService.getInstance().saveOrUpdate(emailCobranca);
					}catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro ao registrar e-mail de cobrança da nota " + numero + ": " + e.getMessage());
						continue;
					}
				}
				// BUSCA DO TEMPLATE DE ACORDO COM O TIPO
				String template = null;
				EmailManager emailmanager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				try{
					emailmanager
							.setFrom(emailRemetente)
							.setSubject(assunto)
							.setTo(pEnvio.getEmail());
				} catch (Exception e) {
					e.printStackTrace();
					listaMensagemErro.add("Erro ao criar e-mail da nota " + numero + ": " + e.getMessage());
					continue;
				}
				if(emailCobranca != null){
					emailmanager.setEmailId(emailCobranca.getCdemailcobranca());
				}
				
				try {
					String pdfXml = null;
					if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF) && !listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML)){
						pdfXml = "PDF";
					}
					if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) && !listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
						pdfXml = "XML";
					}
					if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) && listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
						pdfXml = "XML/PDF";
					}
					
					String assinatura = usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), emailmanager);
					
					if(listaTipo.contains(TipoEnvioNFCliente.SERVICO_XML) || listaTipo.contains(TipoEnvioNFCliente.SERVICO_PDF)){
						template = new TemplateManager("/WEB-INF/template/templateEnvioPdfXmlNfse.tpl")
										.assign("numero", numeroNfse)
										.assign("pdfXml", pdfXml)
										.assign("nomecliente", pEnvio.getNome())
										.assign("assinatura", assinatura)
										.assign("boletos", Boolean.TRUE.equals(pEnvio.getReceber_boleto()) ? boletosStr : "")
										.getTemplate();
					}
					
					if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF) && !listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML)){
						if(isNfe){
							pdfXml = "PDF com a DANFE";
						} else {
							pdfXml = "PDF com a DANFCE";
						}
					}
					if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) && !listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
						pdfXml = "XML";
					}
					if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) && listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
						if(isNfe){
							pdfXml = "XML e o PDF com a DANFE";
						} else {
							pdfXml = "XML e o PDF com a DANFCE";
						}
					}
					
					if(listaTipo.contains(TipoEnvioNFCliente.PRODUTO_XML) || listaTipo.contains(TipoEnvioNFCliente.PRODUTO_PDF)){
						if(isNfe){
							template = new TemplateManager("/WEB-INF/template/templateEnvioPdfXmlNfe.tpl")
										.assign("chaveacesso", chaveacesso)
										.assign("pdfXml", pdfXml)
										.assign("nomecliente", pEnvio.getNome())
										.assign("assinatura", assinatura)
										.assign("boletos", Boolean.TRUE.equals(pEnvio.getReceber_boleto()) ? boletosStr : "")
										.getTemplate();
						} else {
							template = new TemplateManager("/WEB-INF/template/templateEnvioPdfXmlNfce.tpl")
								.assign("chaveacesso", chaveacesso)
								.assign("pdfXml", pdfXml)
								.assign("nomecliente", pEnvio.getNome())
								.assign("assinatura", assinatura)
								.assign("url", NFCeUtil.getUrlConsulta(uf, producao))
								.assign("boletos", Boolean.TRUE.equals(pEnvio.getReceber_boleto()) ? boletosStr : "")
								.getTemplate();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					listaMensagemErro.add("Erro na busca do template para envio de e-mail da nota " + numero + ": " + e.getMessage());
					continue;
				}
				
//				REGISTRO DO ENVIO DE E-MAIL
				Envioemail envioemail = null;
				try{
					envioemail = envioemailService.registrarEnvio(
							emailRemetente, 
							assunto, 
							template, 
							envioemailtipo, 
							new Pessoa(pEnvio.getCdpessoa()), 
							pEnvio.getEmail(), 
							pEnvio.getNome(),
							emailmanager);
				} catch (Exception e) {
					e.printStackTrace();
					listaMensagemErro.add("Erro no registro de envio de e-mail da nota " + numero + ": " + e.getMessage());
					continue;
				}
				
				if(boletoAnexado){
					Envioemailtipo envioemailtipo2 = Envioemailtipo.BOLETO;
					try{
						envioemailService.registrarEnvio(
								emailRemetente, 
								assunto, 
								template, 
								envioemailtipo2, 
								new Pessoa(pEnvio.getCdpessoa()), 
								pEnvio.getEmail(), 
								pEnvio.getNome(),
								emailmanager);
					} catch (Exception e) {
						e.printStackTrace();
						listaMensagemErro.add("Erro no registro de envio de e-mail da nota " + numero + ": " + e.getMessage());
						continue;
					}
				}
				
//				ENVIO DE E-MAIL
				try{
					emailmanager.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, pEnvio.getEmail()));
					if(anexosAux != null && anexosAux.size() > 0){
						for (Resource anexo : anexosAux) {
							emailmanager.attachFileUsingByteArray(anexo.getContents(), anexo.getFileName(), anexo.getContentType(), anexo.getFileName());
						}
					}
					
					emailmanager.sendMessage();
				} catch (Exception e) {
					e.printStackTrace();
					listaMensagemErro.add("Erro no envio de e-mail da nota " + numero + ": " + e.getMessage());
					continue;
				}
				
				if(emailCobranca != null){
					EmailCobrancaService.getInstance().updateEmailCobranca(emailCobranca);
				}
				
//				REGISTRO DE HISTÓRICO DA NOTA DO ENVIO
				try {
					this.registroHistoricoEnvioEmailNF(nota, assunto, anexosAux, pEnvio.getEmail(), listaDocumentoEnviadosDownloadCaptcha);
				} catch (Exception e) {
					e.printStackTrace();
					listaMensagemErro.add("Erro no registro do histórico do envio de e-mail da nota " + numero + ": " + e.getMessage());
				}
				
				if(Boolean.TRUE.equals(pEnvio.getReceber_boleto()) && !docsGerarBoleto.isEmpty()){
					for(Documento doc: docsGerarBoleto){
						//Mapa usado para não duplicar o histórico, considerando que podemos estar gravando o envio de e-mail para mais de um contato
						if(!listaDocumentohistorico.containsKey(doc)){
//							REGISTRO DE HISTÓRICO DE ENVIO DE E-MAIL NA CONTA A RECEBER
							try {
								Documentohistorico historico = new Documentohistorico(doc);
								historico.setDocumentoacao(Documentoacao.ENVIO_BOLETO);
								historico.setObservacao("Boleto enviado juntamente a Nota Fiscal <a href='javascript:visualizarNota("+nota.getCdNota().toString()+");'>"+numero+"</a>" + ".");
							
								documentohistoricoService.saveOrUpdate(historico);
								
								listaDocumentohistorico.put(doc, historico);
							} catch (Exception e) {
								e.printStackTrace();
								listaMensagemErro.add("Erro ao registrar histórico de envio de e-mail na conta a receber referente à nota " + numero + ": " + e.getMessage());
								continue;								
							}
							
						}
					}
				}
				
				sucesso = true;
				anexosAux.removeAll(anexosAux);
			}
			
			if(listaMensagemErro != null && listaMensagemErro.size() > 0){
				for (String erro : listaMensagemErro) {
					resultado.addMsgErro(erro);
				}
			}
			
			if(sucesso){
				resultado.addSucesso();
			} else {
				resultado.addErro();
			}
		}
		
		return resultado;
	}
	
	/**
	 * Registra no histórico da nota o envio de e-mail com o e-mail, assunto e nome do arquivo.
	 *
	 * @param notaFiscalServico
	 * @param assunto
	 * @param anexos
	 * @param email
	 * @author Rodrigo Freitas
	 * @since 11/02/2014
	 */
	private void registroHistoricoEnvioEmailNF(Nota nota, String assunto, List<Resource> anexos, String email, List<Documento> listaDocumentoEnviadosDownloadCaptcha) {
		StringBuilder obs = new StringBuilder();
		obs.append("Email de envio: ").append(email).append(" ");
		obs.append("Assunto: ").append(assunto).append(" ");
		obs.append("Nome do(s) arquivo(s): ");
		for (Resource anexo : anexos) {
			obs.append(anexo.getFileName()).append("; ");
		}
		
		if(SinedUtil.isListNotEmpty(listaDocumentoEnviadosDownloadCaptcha)) {
			obs.append("Boleto(s) disponibilizado(s) através de link para download com Captcha: ");
			for(Documento doc : listaDocumentoEnviadosDownloadCaptcha) {
				obs.append("<a href=\"")
					.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(doc.getCddocumento())
					.append("&hash=")
					.append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(doc.getCddocumento().toString()))))
					.append("\">")
					.append(doc.getCddocumento().toString())
					.append("</a> ");
			}
		}
		
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setNota(nota);
		notaHistorico.setNotaStatus(NotaStatus.ENVIO_EMAIL);
		notaHistorico.setObservacao(obs.toString());
		
		notaHistoricoService.saveOrUpdate(notaHistorico);
	}
	
	/**
	 * Pega a nota fiscal de serviço a partir da nota genérica para o envio de e-mail
	 *
	 * @param nota
	 * @param listaNotaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2014
	 */
	private NotaFiscalServico getNotaFiscalServicoInLista(Nota nota, List<NotaFiscalServico> listaNotaFiscalServico) {
		if(listaNotaFiscalServico != null && listaNotaFiscalServico.size() > 0){
			for (NotaFiscalServico notaFiscalServico : listaNotaFiscalServico) {
				if(nota.getCdNota() != null && 
						notaFiscalServico.getCdNota() != null && 
						nota.getCdNota().equals(notaFiscalServico.getCdNota())){
					return notaFiscalServico;
				}
			}
		}
		return null;
	}
	
	/**
	 * Pega a nota fiscal de produto a partir da nota genérica para o envio de e-mail
	 *
	 * @param nota
	 * @param listaNotaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2014
	 */
	private Notafiscalproduto getNotaFiscalProdutoInLista(Nota nota, List<Notafiscalproduto> listaNotafiscalproduto) {
		if(listaNotafiscalproduto != null && listaNotafiscalproduto.size() > 0){
			for (Notafiscalproduto notafiscalproduto : listaNotafiscalproduto) {
				if(nota.getCdNota() != null && 
						notafiscalproduto.getCdNota() != null && 
						nota.getCdNota().equals(notafiscalproduto.getCdNota())){
					return notafiscalproduto;
				}
			}
		}
		return null;
	}
	
	/**
	 * Gera o Arquivo XML da NFS-e para download a partir do Arquivonfnota.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfnotaService#getArquivoXmlNfseSorocaba(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 15/05/2018
	 * @author Mairon Cezar
	 */
	@SuppressWarnings("unchecked")
	public Arquivo getArquivoXmlNfseSorocaba(Arquivonfnota arquivonfnota) {
		if(arquivonfnota.getArquivoxml() != null && arquivonfnota.getArquivoxml().getCdarquivo() != null){
			return arquivoDAO.loadWithContents(arquivonfnota.getArquivoxml());
		} 

		String numeroNFse = arquivonfnota.getNumeronfse();
		String numeroNota = arquivonfnota.getNota().getNumero();
		if(numeroNota == null || numeroNota.equals("")) 
			numeroNota = "<sem número> (ID: " + arquivonfnota.getNota().getCdNota() + ")";
		
		try {
			Arquivo arquivo = null;
			Element nfseRetornado = null;
			Element nfse = null;
			Element nfseFinal = new Element("nfse");
			
			if(arquivonfnota.getArquivonf() != null){
				Arquivonf arquivonf = arquivonfnota.getArquivonf();
				if(arquivonf.getArquivoretornoconsulta() == null || arquivonf.getArquivoretornoconsulta().getCdarquivo() == null){
					throw new SinedException("Não foi possível carregar o arquivo XML de retorno da consulta do lote.");
				}
				if(arquivonf.getArquivoxml() == null || arquivonf.getArquivoxml().getCdarquivo() == null){
					throw new SinedException("Não foi possível carregar o arquivo XML de retorno da consulta do lote.");
				}

				Arquivo arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonf.getArquivoretornoconsulta());
				Arquivo arquivoxml = arquivoDAO.loadWithContents(arquivonf.getArquivoxml());
				
				SAXBuilder sb = new SAXBuilder();  
				Document d = sb.build(new ByteArrayInputStream(arquivoretornoconsulta.getContent()));
				Element rootElement = d.getRootElement();
				
				Element listaNfse = SinedUtil.getChildElement("ListaNFSe", rootElement.getContent());
				if(listaNfse != null){
					List<Element> listaCompNfse = SinedUtil.getListChildElement("ConsultaNFSe", listaNfse.cloneContent());
					if(listaCompNfse != null && listaCompNfse.size() > 0){
						for (Element compNfse : listaCompNfse) {
							Element numero = SinedUtil.getChildElement("NumeroRPS", compNfse.getContent());
							if(numero != null){
								String numeroStr = numero.getText();
								if(numeroNota.equals(numeroStr)){
									nfseRetornado = compNfse;
									break;
								}
							} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'NumeroRPS'.");
						}
					} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'ConsultaNFSe'.");
				}else  throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'ListaNFSe'.");
				
				Document d2 = sb.build(new ByteArrayInputStream(arquivoxml.getContent()));
				Element rootElement2 = d2.getRootElement();
				
				List<Element> listaNfseDados = SinedUtil.getListChildElement("Lote", rootElement2.getContent());
				if(listaNfseDados != null && listaNfseDados.size() > 0){
					for (Element lote : listaNfseDados) {
						List<Element> listaRps = SinedUtil.getListChildElement("RPS", lote.getContent());
						if(SinedUtil.isListNotEmpty(listaRps)){
							for(Element rps: listaRps){
								if(rps.getAttribute("Id") == null){
									throw new SinedException("Estrutura do arquivo XML de envio inválida. Não foi possível obter o atributo Id da tag 'RPS'.");
								}
								if(("rps:"+numeroNota.toString()).equals(rps.getAttribute("Id").getValue())){
									nfse = rps;
									break;
								}							
							}
						}else throw new SinedException("Estrutura do arquivo XML de envio inválida. Não foi possível obter a tag 'RPS'.");
					}
				}else  throw new SinedException("Estrutura do arquivo XML de envio inválida. Não foi possível obter a tag 'ListaNFSe'.");
			} else throw new SinedException("Não foi possível carregar o 'Arquivonf'.");
			if(nfseRetornado != null && nfse != null){
				nfseFinal.getContent().addAll(nfseRetornado.cloneContent());
				nfseFinal.getContent().addAll(nfse.cloneContent());
				
				Document doc = new Document(nfseFinal);
				String xml = new XMLOutputter().outputString(doc);
				
				arquivo = new Arquivo(xml.getBytes(), "nfse_" + numeroNFse + ".xml", "text/xml");
				
				arquivonfnota.setArquivoxml(arquivo);
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
			}
			
			return arquivo;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na geração do XML da nota " + numeroNota + ": " + e.getMessage());
		}
	}
	
	public Arquivo getArquivoXmlNfseCastelo(Arquivonfnota arquivonfnota) {
		if(arquivonfnota.getArquivoxml() != null && arquivonfnota.getArquivoxml().getCdarquivo() != null){
			return arquivoDAO.loadWithContents(arquivonfnota.getArquivoxml());
		} 

		String numeroNFse = arquivonfnota.getNumeronfse();
		String numeroNota = arquivonfnota.getNota().getNumero();
		if(numeroNota == null || numeroNota.equals("")) 
			numeroNota = "<sem número> (ID: " + arquivonfnota.getNota().getCdNota() + ")";
		
		try {
			Arquivo arquivo = null;
			Element nfseRetornado = null;
			Element rpsXml = null;
			Element nfseFinal = new Element("tcListaNFse");
			Element nfse = new Element("nfse");
			String rpsNumeroXml = null;
			String numeroXml = null;
			
			if(arquivonfnota.getArquivonf() != null){
				Arquivonf arquivonf = arquivonfnota.getArquivonf();
				if(arquivonf.getArquivoretornoconsulta() == null || arquivonf.getArquivoretornoconsulta().getCdarquivo() == null){
					throw new SinedException("Não foi possível carregar o arquivo XML de retorno da consulta do lote.");
				}
				if(arquivonf.getArquivoxml() == null || arquivonf.getArquivoxml().getCdarquivo() == null){
					throw new SinedException("Não foi possível carregar o arquivo XML de retorno da consulta do lote.");
				}

				Arquivo arquivoretornoconsulta = arquivoDAO.loadWithContents(arquivonf.getArquivoretornoconsulta());
				Arquivo arquivoxml = arquivoDAO.loadWithContents(arquivonf.getArquivoxml());
				
				SAXBuilder sb = new SAXBuilder();  
				Document d = sb.build(new ByteArrayInputStream(arquivoretornoconsulta.getContent()));
				Element rootElement = d.getRootElement();
				
				List<Element> notasFiscais = SinedUtil.getListChildElement("notasFiscais", rootElement.getContent());
				if(notasFiscais != null){
					for(Element notaFiscas : notasFiscais){
						Element rpsNumero = SinedUtil.getChildElement("rpsNumero", notaFiscas.getContent());
						Element numero = SinedUtil.getChildElement("numero", notaFiscas.getContent());
						if(rpsNumero != null){
							String numeroStr = rpsNumero.getText();
							if(numeroNota.equals(numeroStr)){
								rpsNumeroXml = numeroStr;
								numeroXml = numero.getText();
								nfseRetornado = notaFiscas;
							} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'rpsNumero'.");
						} else throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'rpsNumero'.");
					}
				}else  throw new SinedException("Estrutura do arquivo XML de retorno da consulta do lote inválida. Não foi possível obter a tag 'notasFiscais'.");
				
				Document d2 = sb.build(new ByteArrayInputStream(arquivoxml.getContent()));
				Element rootElement2 = d2.getRootElement();
				
				List<Element> listaNfseDados = SinedUtil.getListChildElement("ListaRps", rootElement2.getContent());
				if(rpsNumeroXml != null && numeroXml != null && listaNfseDados != null && listaNfseDados.size() > 0){
					for (Element lote : listaNfseDados) {
						List<Element> listaRps = SinedUtil.getListChildElement("Rps", lote.getContent());
						if(SinedUtil.isListNotEmpty(listaRps)){
							for(Element rps: listaRps){
								Element identificacaoRps = SinedUtil.getChildElement("IdentificacaoRps", rps.getContent());
								if(identificacaoRps != null){
									Element numero = SinedUtil.getChildElement("Numero", identificacaoRps.getContent());
									if(numero != null && rpsNumeroXml.equals(numero.getText())){
										identificacaoRps.getContent().add(new Element("NumeroRps").setText(rpsNumeroXml));
										numero.setText(numeroXml);
										rpsXml = rps;
										break;
									}
								}else throw new SinedException("");							
							}
						}else throw new SinedException("Estrutura do arquivo XML de envio inválida. Não foi possível obter a tag 'RPS'.");
					}
				}else  throw new SinedException("Estrutura do arquivo XML de envio inválida. Não foi possível obter a tag 'ListaNFSe'.");
			} else throw new SinedException("Não foi possível carregar o 'Arquivonf'.");
			
			if(nfseRetornado != null && rpsXml != null){
				nfse.getContent().addAll(rpsXml.cloneContent());
				nfseFinal.getContent().add(nfse);
				
				Document doc = new Document(nfseFinal);
				String xmlFinal = new XMLOutputter().outputString(doc);
				
				arquivo = new Arquivo(xmlFinal.getBytes(), "nfse_" + numeroNFse + ".xml", "text/xml");
				
				arquivonfnota.setArquivoxml(arquivo);
				arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota);
			}
			
			return arquivo;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na geração do XML da nota " + numeroNota + ": " + e.getMessage());
		}
	}
	
	public CancelamentoNfseObaratec createXmlCancelamentoNfseObaratec(String numeronfse, Empresa empresa, String numeroRps, String motivo) {
		CancelamentoNfseObaratec cancelamentoNfeObaratec = new CancelamentoNfseObaratec();
		
		List<CancelamentoNfseObaratecNota> listaCancelamentoNfseObaratecNota = new ArrayList<CancelamentoNfseObaratecNota>();
		CancelamentoNfseObaratecNota cancelamentoNfseObaratecNota = new CancelamentoNfseObaratecNota();
		
		cancelamentoNfseObaratecNota.setNumeroNota(numeronfse);
		cancelamentoNfseObaratecNota.setNumeroRps(numeroRps);
		cancelamentoNfseObaratecNota.setCodigoMotivo(motivo != null ? motivo.substring(0, 3) : "");
		cancelamentoNfseObaratecNota.setInscricaoMunicipal(empresa.getInscricaomunicipal());
		
		listaCancelamentoNfseObaratecNota.add(cancelamentoNfseObaratecNota);
		
		cancelamentoNfeObaratec.setListaCancelamentoNfseObaratecNota(listaCancelamentoNfseObaratecNota);
		
		return cancelamentoNfeObaratec;
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioObaratec(Arquivonf arquivonf, byte[] content) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		Element nfeResposta = SinedUtil.getChildElement("return", rootElement.getContent());
		
		if (nfeResposta != null) {
			List<Element> listaNotaFiscal = SinedUtil.getListChildElement("nfse", nfeResposta.getContent());
			
			if (listaNotaFiscal != null && listaNotaFiscal.size() > 0) {
				for (Element notaFiscalElement : listaNotaFiscal) {
					Map<String, String> mapaNumNfse = new HashMap<String, String>();
					Map<String, String> mapaCodVerificacao = new HashMap<String, String>();
					
					Element statusEmissaoElement = SinedUtil.getChildElement("statusEmissao", notaFiscalElement.getContent());
					Element numeroNotaElement = SinedUtil.getChildElement("numeroNota", notaFiscalElement.getContent());
					Element numeroRpsElement = SinedUtil.getChildElement("numeroRps", notaFiscalElement.getContent());
					Element codigoVerificacaoElement = SinedUtil.getChildElement("codigoVerificacao", notaFiscalElement.getContent());
					
					String numNota = numeroRpsElement != null ? numeroRpsElement.getText() : "";
					String numNfse = numeroNotaElement != null ? numeroNotaElement.getText() : "";
					String codVerificacao = codigoVerificacaoElement != null ? codigoVerificacaoElement.getText() : "";
					
					mapaNumNfse.put(numNota, numNfse);
					mapaCodVerificacao.put(numNota, codVerificacao);
					
					if (statusEmissaoElement != null && statusEmissaoElement.getText().equals("200")) {
						String numeroNfse, codigoVerificacao;
						List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
						for (Arquivonfnota arquivonfnota : listan) {
							String numeroNota = SinedUtil.retiraZeroEsquerda(arquivonfnota.getNota().getNumero());
							numeroNfse = mapaNumNfse.get(numeroNota);
							codigoVerificacao = mapaCodVerificacao.get(numeroNota);
							
							if(numeroNfse != null && codigoVerificacao != null){
								if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
									documentoService.updateDocumentoNumeroByNota(numeroNfse, arquivonfnota.getNota());
									try {
										vendaService.adicionarParcelaNumeroDocumento(arquivonfnota.getNota(), numeroNfse, true);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								
								arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao);
								
								if(notaDocumentoService.isNotaWebservice(arquivonfnota.getNota()) || notaService.isNotaPossuiReceita(arquivonfnota.getNota())){
									arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
								} else {
									arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
								}
								notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
							}
						}
						
						this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
					} else {
						List<Element> listaAlerta = SinedUtil.getListChildElement("Header", notaFiscalElement.getContent());
						if(listaAlerta != null && listaAlerta.size() > 0){
							for (Element alerta : listaAlerta) {
								Attribute descricao = alerta.getAttribute("Answer");
								if(descricao != null){
									Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
									
									arquivonfnotaerro.setArquivonf(arquivonf);
									arquivonfnotaerro.setMensagem(descricao.getValue());
									arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
									
									arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
								}
							}
							
							List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
							for (Arquivonfnota arquivonfnota : listan) {
								if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
									arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
									notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
								}
							}
							
							this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
						}
					}
				}
			} else {
				List<Element> listaAlerta = SinedUtil.getListChildElement("Header", nfeResposta.getContent());
				
				if(listaAlerta != null && listaAlerta.size() > 0){
					for (Element alerta : listaAlerta) {
						Attribute descricao = alerta.getAttribute("Answer");
						if(descricao != null){
							Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
							
							arquivonfnotaerro.setArquivonf(arquivonf);
							arquivonfnotaerro.setMensagem(descricao.getValue());
							arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
							
							arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
						}
					}
					
					List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
					for (Arquivonfnota arquivonfnota : listan) {
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
					
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamentoObaratec(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element notaFiscalElement = SinedUtil.getChildElement("notaFiscal", rootElement.getContent());
			if(notaFiscalElement != null){
				Element statusEmissaoElt = SinedUtil.getChildElement("statusEmissao", notaFiscalElement.getContent());
				if(statusEmissaoElt != null) {
					arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
					Nota nota = arquivonfnota.getNota();
					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
					
					String statusEmissao = statusEmissaoElt.getTextTrim();
					if(statusEmissao != null && statusEmissao.equals("200")){
						String obs = "Cancelamento na Receita";
						nota.setNotaStatus(NotaStatus.CANCELADA);
						notaService.alterarStatusAcao(nota, obs, null);
						
						arquivonfnotaService.updateCancelado(arquivonfnota);
						
						if(nota.getCdNota() != null){
							requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
							
							List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
							List<Venda> listaVenda = new ArrayList<Venda>();
							for (NotaVenda nv : listaNotaVenda) {
								if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
									listaVenda.add(nv.getVenda());
								}
							}
							
							String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
							if (!whereIn.equals("")){
								vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
							}
							
							notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
						}
					} else {
						Element messages = SinedUtil.getChildElement("messages", notaFiscalElement.getContent());
						
						Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
						arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
						arquivonfnotaerrocancelamento.setMensagem(messages != null ? messages.getAttributeValue("message") : "Motivo não identificado.");
						
						arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado.";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
					}
					
				} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'statusEmissao'.");
			} else throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'notaFiscal'.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private NfseIssMapRps createXmlEnviarLoteNfseIssMap(String in, Empresa empresa, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(in);
		NotaFiscalServico notaFiscalServico = listaNota.get(0);
		
		NfseIssMapRps nfseIssMapRps = new NfseIssMapRps();
		nfseIssMapRps.setAliquota(notaFiscalServico.getIss() != null ? notaFiscalServico.getIss().toString() : "");
		nfseIssMapRps.setCancelada("Normal");
		nfseIssMapRps.setCepPrestador(StringUtils.soNumero(empresa.getEnderecoCep()));
		nfseIssMapRps.setCepTomador(notaFiscalServico.getCliente() != null ? StringUtils.soNumero(notaFiscalServico.getCliente().getEnderecoCep()) : "");
		nfseIssMapRps.setCidadePrestador(empresa.getEnderecoCidade());
		nfseIssMapRps.setCidadeTomador(notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getEnderecoCidade() : "");
		nfseIssMapRps.setCodigoServicoPrestado(notaFiscalServico.getItemlistaservicoBean() != null ? StringUtils.soNumero(notaFiscalServico.getItemlistaservicoBean().getCodigo()) : "");
		nfseIssMapRps.setCpfCnpjPrestador(StringUtils.soNumero(empresa.getCpfCnpj()));
		nfseIssMapRps.setCpfCnpjTomador(notaFiscalServico.getCliente() != null ? StringUtils.soNumero(notaFiscalServico.getCliente().getCpfCnpj()) : "");
		nfseIssMapRps.setDataHoraEmissao(notaFiscalServico.getDtEmissao() != null ? LkNfeUtil.FORMATADOR_DATA.format(notaFiscalServico.getDtEmissao()) : "");
		nfseIssMapRps.setDescServico(notaFiscalServico.getDescricaoServico() != null ? notaFiscalServico.getDescricaoServico() : "");
		nfseIssMapRps.setEmailPrestador(empresa.getEmail() != null ? empresa.getEmail() : "");
		nfseIssMapRps.setEmailTomador(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getEmail() != null ? notaFiscalServico.getCliente().getEmail() : "");
		nfseIssMapRps.setEnderecoPrestador(empresa.getEnderecoLogradouro());
		nfseIssMapRps.setEnderecoTomador(notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getEnderecoLogradouro() : "");
		nfseIssMapRps.setEstadoPrestador(empresa.getEnderecoSiglaEstado());
		nfseIssMapRps.setEstadoTomador(notaFiscalServico.getCliente() != null ? notaFiscalServico.getCliente().getEnderecoSiglaEstado() : "");
		nfseIssMapRps.setIeRgPrestador(empresa.getInscricaoestadual() != null ? StringUtils.soNumero(empresa.getInscricaoestadual()) : "");
		nfseIssMapRps.setIeRgTomador(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getInscricaoestadual() != null ? StringUtils.soNumero(notaFiscalServico.getCliente().getInscricaoestadual()) : "");
		nfseIssMapRps.setImPrestador(empresa.getInscricaomunicipal() != null ? StringUtils.soNumero(empresa.getInscricaomunicipal()) : "");
		nfseIssMapRps.setImTomador(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getInscricaomunicipal() != null ? StringUtils.soNumero(notaFiscalServico.getCliente().getInscricaomunicipal()) : "");
		nfseIssMapRps.setKey(configuracaonfe != null && configuracaonfe.getCodigochaveacesso() != null ? configuracaonfe.getCodigochaveacesso() : "");
		nfseIssMapRps.setPass(configuracaonfe != null && configuracaonfe.getSenha() != null ? configuracaonfe.getSenha() : "");
		nfseIssMapRps.setLocalExecucao(notaFiscalServico.getMunicipioissqn() != null ? notaFiscalServico.getMunicipioissqn().getCdibge() : "");
		nfseIssMapRps.setMotivoCancelamento("");
		nfseIssMapRps.setNomeRazaoPrestador(empresa.getRazaosocialOuNome() != null ? empresa.getRazaosocialOuNome() : "");
		nfseIssMapRps.setNomeRazaoTomador(notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getRazaoOrNomeByTipopessoa() != null ? notaFiscalServico.getCliente().getRazaoOrNomeByTipopessoa() : "");
		nfseIssMapRps.setNumero(notaFiscalServico.getNumero() != null ? StringUtils.soNumero(notaFiscalServico.getNumero()) : notaFiscalServico.getNumero());
		nfseIssMapRps.setPorcentagemCOFINS(notaFiscalServico.getCofins() != null ? notaFiscalServico.getCofins().toString() : "");
		nfseIssMapRps.setPorcentagemCSLL(notaFiscalServico.getCsll() != null ? notaFiscalServico.getCsll().toString() : "");
		nfseIssMapRps.setPorcentagemINSS(notaFiscalServico.getInss() != null ? notaFiscalServico.getInss().toString() : "");
		nfseIssMapRps.setPorcentagemIRRF(notaFiscalServico.getIr() != null ? notaFiscalServico.getIr().toString() : "");
		nfseIssMapRps.setPorcentagemOutros("");
		nfseIssMapRps.setPorcentagemPIS(notaFiscalServico.getPis() != null ? notaFiscalServico.getPis().toString() : "");
		nfseIssMapRps.setRetidoNaFonte(configuracaonfe != null && configuracaonfe.getIssretidopelotomador() != null && configuracaonfe.getIssretidopelotomador() ? "S" : "N");
		
		String materiais = "";
		for (NotaFiscalServicoItem nfseI : notaFiscalServico.getListaItens()) {
			materiais = this.preencheDescricaoItemNfs(nfseI, configuracaonfe);
		}
		nfseIssMapRps.setServicoPrestado(!org.apache.commons.lang.StringUtils.isEmpty(materiais) ? materiais.substring(0, materiais.length() - 2) : "");
		
		nfseIssMapRps.setValorBaseCalculo(notaFiscalServico.getBasecalculo() != null ? new Double(notaFiscalServico.getBasecalculo().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorCOFINS(notaFiscalServico.getValorCofins() != null ? new Double(notaFiscalServico.getValorCofins().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorCSLL(notaFiscalServico.getValorCsll() != null ? new Double(notaFiscalServico.getValorCsll().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorDeducoes(notaFiscalServico.getDeducao() != null ? new Double(notaFiscalServico.getDeducao().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorINSS(notaFiscalServico.getValorInss() != null ? new Double(notaFiscalServico.getValorInss().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorIRRF(notaFiscalServico.getValorIr() != null ? new Double(notaFiscalServico.getValorIr().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorIss(notaFiscalServico.getValorIss() != null ? new Double(notaFiscalServico.getValorIss().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorNota(notaFiscalServico.getValorNota() != null ? new Double(notaFiscalServico.getValorNota().getValue().doubleValue()).toString() : "");
		nfseIssMapRps.setValorOutros("");
		nfseIssMapRps.setValorPIS(notaFiscalServico.getValorPis() != null ? new Double(notaFiscalServico.getValorPis().getValue().doubleValue()).toString() : "");
		
		return nfseIssMapRps;
	}
	
	public NfseIssMapCartaCancelamento createXmlCancelamentoNfseIssMap(String numeronfse, Empresa empresa, Nota nfse, String motivo, Configuracaonfe configuracaoNfe) {
		nfse = (NotaFiscalServico) nfse;
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		NfseIssMapCartaCancelamento nfseIssMapCartaCancelamento = new NfseIssMapCartaCancelamento();
		
		nfseIssMapCartaCancelamento.setCpfCnpjPre(empresa.getCpfCnpjProprietarioPrincipalOuEmpresa() != null ? empresa.getCpfCnpjProprietarioPrincipalOuEmpresa() : null);
		nfseIssMapCartaCancelamento.setCpfCnpjTom(nfse.getCliente() != null ? nfse.getCliente().getCpfcnpj() : null);
		nfseIssMapCartaCancelamento.setKey(configuracaoNfe.getToken() != null ? configuracaoNfe.getToken() : null);
		nfseIssMapCartaCancelamento.setPass(configuracaoNfe.getSenha() != null ? configuracaoNfe.getSenha() : null);
		nfseIssMapCartaCancelamento.setMotivoCancelamento(motivo != null ? motivo : null);
		nfseIssMapCartaCancelamento.setNumRPS(nfse.getNumero() != null ? nfse.getNumero() : null);
		
		return nfseIssMapCartaCancelamento;
	}
	
	public void processaRetornoCancelamentoIssMap(Arquivonfnota arquivonfnota, byte[] content) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element respostaElement = SinedUtil.getChildElement("resposta", rootElement.getContent());
			if(respostaElement != null){
				Element codigoElement = SinedUtil.getChildElement("codigo", respostaElement.getContent());
				if(codigoElement != null) {
					arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
					Nota nota = arquivonfnota.getNota();
					NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
					
					String codigo = codigoElement.getTextTrim();
					if(codigo != null && codigo.equals("100")){
						String obs = "Cancelamento na Receita";
						nota.setNotaStatus(NotaStatus.CANCELADA);
						notaService.alterarStatusAcao(nota, obs, null);
						
						arquivonfnotaService.updateCancelado(arquivonfnota);
						
						if(nota.getCdNota() != null){
							requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(nota.getCdNota().toString());
							
							List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
							List<Venda> listaVenda = new ArrayList<Venda>();
							for (NotaVenda nv : listaNotaVenda) {
								if (nv.getVenda() != null && !notaVendaService.existNotaEmitida(nv.getVenda(), null) && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
									listaVenda.add(nv.getVenda());
								}
							}
							
							String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
							if (!whereIn.equals("")){
								vendaService.updateSituacaoVenda(whereIn, Vendasituacao.REALIZADA);
							}
							
							notaDocumentoService.verificarVendaAposCancelamentoOrigemNotaDocumento(nota);
						}
					} else {
						Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
						arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
						arquivonfnotaerrocancelamento.setMensagem("Código: " + codigo);
						
						arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado.";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
					}
				} else {
					throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'codigo'.");
				}
			} else { 
				throw new SinedException("Erro no processamento do retorno do cancelamento, não encontrado a tag 'resposta'.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoEnvioIssMap(Arquivonf arquivonf, byte[] content,	Configuracaonfe configuracaonfe) throws UnsupportedEncodingException, JDOMException, IOException {
		String xml = new String(content);
		Element rootElement = SinedUtil.getRootElementXML(content);
		
		if(rootElement != null){
			Element codigoElement = SinedUtil.getChildElement("codigo", rootElement.getContent());
			if(codigoElement != null && codigoElement.getTextTrim().equals("100")){
				String codigo = codigoElement.getTextTrim();
				
				this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
				//this.updateProtocolo(arquivonf, numeroLoteStr);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
				}
				
				xml = xml.replaceAll("<\\?.+\\?>", "");
				String stringXmlConsultaLote = Util.strings.tiraAcento("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xml);
				Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
				arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
				arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
				this.updateArquivoxmlconsultalote(arquivonf);
			}
		} else {
			this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
				}
			}
			
			Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
			
			arquivonfnotaerro.setArquivonf(arquivonf);
			arquivonfnotaerro.setMensagem(xml);
			arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
		}
	}
	
	public void cancelarNfseIssMap(Arquivonfnota arquivonfnota, Empresa empresa, CancelamentoNfseBean cancelamentoNfseBean) throws Exception {
		NfseIssMapCartaCancelamento nfseIssMapCartaCancelamento = this.createXmlCancelamentoNfseIssMap(arquivonfnota.getNumeronfse(), empresa, arquivonfnota.getNota(), cancelamentoNfseBean.getMotivo(), arquivonfnota.getArquivonf().getConfiguracaonfe());
		String xml = nfseIssMapCartaCancelamento.toString();
		
		xml = new String(Util.strings.tiraAcento(xml).getBytes(), "UTF-8");
		Arquivo arquivo = new Arquivo(xml.getBytes(), "nfcancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		Arquivonf arquivonf = arquivonfnota.getArquivonf();
		
		arquivonfnota.setArquivoxmlcancelamento(arquivo);
		
		arquivoDAO.saveFile(arquivonfnota, "arquivoxmlcancelamento");
		arquivoService.saveOrUpdate(arquivo);
		arquivonfnotaService.updateArquivoxmlcancelamento(arquivonfnota);
		
		arquivo = arquivoService.loadWithContents(arquivonfnota.getArquivoxmlcancelamento());
		
		Element rootElementXml = SinedUtil.getRootElementXML(arquivo.getContent());
		
		if (rootElementXml != null && rootElementXml.getChildren() != null && rootElementXml.getChildren().size() > 0) {
			for (int i = 0 ; i < rootElementXml.getChildren().size() ; i++) {
				Element elementFound = (Element) rootElementXml.getChildren().get(i);
				
				if (!elementFound.getName().equals("key") && !org.apache.commons.lang.StringUtils.isEmpty(elementFound.getText())) {
					elementFound.setText(NfseIssMapCriptografia.cifrar(elementFound.getText(), arquivonf.getConfiguracaonfe().getToken()));
				}
			}
		} else {
			throw new SinedException("Arquivo XML vazio.");
		}

		StringEntity stringEntity = new StringEntity(new XMLOutputter().outputString(rootElementXml));
		
		HttpPost httpPost = new HttpPost("https://www.issmap.com.br/ws/rps/cancela/17");
		
		httpPost.setEntity(stringEntity);
		httpPost.setHeader(HTTP.CONTENT_TYPE, "application/xml; charset=UTF-8");
		
		PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
        cm.setMaxTotal(100);

        HttpClient httpclient = new DefaultHttpClient(cm);
		
        HttpResponse response = httpclient.execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        
        // PROCESSAMENTO DO RETORNO

        String xmlRetorno = EntityUtils.toString(entityResponse, "UTF-8");
        xmlRetorno = Util.strings.tiraAcento(xmlRetorno.toString());
        
        this.processaRetornoCancelamentoIssMap(arquivonfnota, xmlRetorno.getBytes());
	}
	
	public br.com.linkcom.lknfe.xml.nfse.datapublic.RPS createXmlEnviarLoteNfseDataPublic(String whereIn, Empresa empresa, Configuracaonfe configuracaonfe) {
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForNFe(whereIn);
		
		if (listaNota != null) {
			NotaFiscalServico nfs = listaNota.get(0);
			empresaService.setInformacoesPropriedadeRural(nfs.getEmpresa());
			br.com.linkcom.lknfe.xml.nfse.datapublic.RPS rps = new br.com.linkcom.lknfe.xml.nfse.datapublic.RPS();
			
			br.com.linkcom.lknfe.xml.nfse.datapublic.Registro1 registro1 = new br.com.linkcom.lknfe.xml.nfse.datapublic.Registro1();
			registro1.setInscricaomunicipal(configuracaonfe.getInscricaomunicipal() != null ? Integer.parseInt(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal())) : null);
			registro1.setLotenumero(configuracaonfe.getContadorlotenfse());
			registro1.setDatainicio(SinedDateUtils.currentDate());
			registro1.setDatafim(SinedDateUtils.currentDate());
			rps.setRegistro1(registro1);
			
			br.com.linkcom.lknfe.xml.nfse.datapublic.Registro2 registro2 = new br.com.linkcom.lknfe.xml.nfse.datapublic.Registro2();
			registro2.setDataemissao(nfs.getDtEmissao());
			registro2.setNumerorps(nfs.getNumero());
			rps.setRegistro2(registro2);
			
			br.com.linkcom.lknfe.xml.nfse.datapublic.Registro3 registro3 = new br.com.linkcom.lknfe.xml.nfse.datapublic.Registro3();
			nfs.setEmpresa(empresa);
			if (nfs.getEmpresa() != null) {
				registro3.setBairro(nfs.getEmpresa().getEnderecoBairro() != null ? (nfs.getEmpresa().getEnderecoBairro().length() > 30 ? nfs.getEmpresa().getEnderecoBairro().substring(0, 29) : nfs.getEmpresa().getEnderecoBairro()) : "");
				registro3.setCep(nfs.getEmpresa().getEnderecoCep() != null ? StringUtils.soNumero(nfs.getEmpresa().getEnderecoCep()) : "");
				registro3.setCidade(nfs.getEmpresa().getEnderecoCidade() != null ? (nfs.getEmpresa().getEnderecoCidade().length() > 40 ? nfs.getEmpresa().getEnderecoCidade().substring(0, 39) : nfs.getEmpresa().getEnderecoCidade()) : "");
				registro3.setComplemento(nfs.getEmpresa().getEnderecoComplemento() != null ? (nfs.getEmpresa().getEnderecoComplemento().length() > 40 ? nfs.getEmpresa().getEnderecoComplemento().substring(0, 39) : nfs.getEmpresa().getEnderecoComplemento()) : "");
				registro3.setCpfcnpj(nfs.getEmpresa().getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() != null ? nfs.getEmpresa().getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() : "");
				registro3.setEmail(nfs.getEmpresa().getEmail() != null ? (nfs.getEmpresa().getEmail().length() > 60 ? nfs.getEmpresa().getEmail().substring(0, 59) : nfs.getEmpresa().getEmail()) : "");
				String inscricaoEstadual = nfs.getEmpresa().getInscricaoestadual() != null ? StringUtils.soNumero(nfs.getEmpresa().getInscricaoestadual()) : null;
				registro3.setInscricaoestadual(nfs.getEmpresa().getInscricaoestadual() != null ? (inscricaoEstadual.length() > 14 ? inscricaoEstadual.substring(0, 13) : inscricaoEstadual) : "");
				String inscricaoMunicipal = nfs.getEmpresa().getInscricaomunicipal() != null ? StringUtils.soNumero(nfs.getEmpresa().getInscricaomunicipal()) : null;
				registro3.setInscricaomunicipal(nfs.getEmpresa().getInscricaomunicipal() != null ? (inscricaoMunicipal.length() > 9 ? inscricaoMunicipal.substring(0, 8) : inscricaoMunicipal) : "");
				registro3.setLogradouro(nfs.getEmpresa().getEnderecoLogradouro() != null ? (nfs.getEmpresa().getEnderecoLogradouro().length() > 60 ? nfs.getEmpresa().getEnderecoLogradouro().substring(0, 59) : nfs.getEmpresa().getEnderecoLogradouro()) : "");
				registro3.setNumero(nfs.getEmpresa().getEnderecoNumero() != null ? (nfs.getEmpresa().getEnderecoNumero().length() > 10 ? nfs.getEmpresa().getEnderecoNumero().substring(0, 9) : nfs.getEmpresa().getEnderecoNumero()) : "");
				registro3.setRazaosocial(nfs.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() != null ? (nfs.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa().length() > 50 ? nfs.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa().substring(0, 49) : nfs.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa()) : "");
				registro3.setTelefone(nfs.getEmpresa().getTelefoneprincipal() != null ? nfs.getEmpresa().getTelefoneprincipal().trim() : "");
				registro3.setUf(nfs.getEmpresa().getEnderecoSiglaEstado() != null ? nfs.getEmpresa().getEnderecoSiglaEstado() : "");
			}
			rps.setRegistro3(registro3);
			
			br.com.linkcom.lknfe.xml.nfse.datapublic.Registro4 registro4 = new br.com.linkcom.lknfe.xml.nfse.datapublic.Registro4();
			registro4.setAliquota(nfs.getIss() != null ? nfs.getIss() : 0d);
			registro4.setCidadeprestacao(nfs.getCliente().getEnderecoCidadeCodIbge() != null ? nfs.getCliente().getEnderecoCidadeCodIbge() : "");
			registro4.setCodigoatividade(nfs.getItemlistaservicoBean() != null ? StringUtils.soNumero(nfs.getItemlistaservicoBean().getCodigo()) : "");
			registro4.setTiporecolhimento(configuracaonfe.getIssretidopelotomador() != null && configuracaonfe.getIssretidopelotomador() ? "R" : "A");
			registro4.setTributacao(configuracaonfe.getTributacaodatapublic() != null ? configuracaonfe.getTributacaodatapublic().getValue().toString() : "");
			registro4.setUfprestacao(nfs.getCliente().getEnderecoSiglaEstado() != null ? nfs.getCliente().getEnderecoSiglaEstado() : "");
			registro4.setValorbasecalculo(nfs.getBasecalculo() != null ? new Double(nfs.getBasecalculo().getValue().doubleValue()) : 0d);
			registro4.setValorcofins(nfs.getValorCofins() != null ? new Double(nfs.getValorCofins().getValue().doubleValue()) : 0d);
			registro4.setValorcsll(nfs.getValorCsll() != null ? new Double(nfs.getValorCsll().getValue().doubleValue()) : 0d);
			registro4.setValordeducao(nfs.getDeducao() != null ? new Double(nfs.getDeducao().getValue().doubleValue()) : 0d);
			registro4.setValorinss(nfs.getValorInss() != null ? new Double(nfs.getValorInss().getValue().doubleValue()) : 0d);
			registro4.setValorir(nfs.getValorIr() != null ? new Double(nfs.getValorIr().getValue().doubleValue()) : 0d);
			registro4.setValoriss(nfs.getValorIss() != null ? new Double(nfs.getValorIss().getValue().doubleValue()) : 0d);
			registro4.setValoroutrasretencoes(0d);
			registro4.setValorpis(nfs.getValorPis() != null ? new Double(nfs.getValorPis().getValue().doubleValue()) : 0d);
			registro4.setValorrps(nfs.getValorNota() != null ? new Double(nfs.getValorNota().getValue().doubleValue()) : 0d);
			rps.setRegistro4(registro4);
			
			List<Registro5> registro5List = new ArrayList<Registro5>();
			for (NotaFiscalServicoItem nfsi : nfs.getListaItens()) {
				Registro5 registro5 = new Registro5();
				
				String descricao = this.preencheDescricaoItemNfs(nfsi, configuracaonfe);
				
				registro5.setDiscriminacao(descricao.length() > 80 ? descricao.substring(0, 79) : descricao);
				
				registro5List.add(registro5);
			}
			rps.setListaRegistro5(registro5List);
			
			return rps;
		} else {
			return null;
		}
	}
	
	public void createArquivoXmlConsultaLote(Arquivonf arquivonfFound) throws UnsupportedEncodingException, JDOMException, IOException {
		Arquivo arquivo = arquivoService.loadWithContents(arquivonfFound.getArquivoretornoenvio());
		Element retEnviNfe = SinedUtil.getRootElementXML(arquivo.getContent());
		Element infRec = SinedUtil.getChildElement("infRec", retEnviNfe.getContent());
		Element nRec = SinedUtil.getChildElement("nRec", infRec.getContent());
		
		String nRecStr = nRec != null ? nRec.getValue() : "";
		
		ConsReciNFe consReciNFe = this.createXmlConsultaLoteNfe(nRecStr, arquivonfFound.getConfiguracaonfe());
		String stringXmlConsultaLote = Util.strings.tiraAcento(consReciNFe.toString());
		Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivonfFound.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
		
		arquivoDAO.saveFile(arquivonfFound, "arquivoxmlconsulta");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		arquivonfDAO.updateConsultando(arquivonfFound, Boolean.TRUE);
		this.updateArquivoxmlconsultalote(arquivonfFound);
	}
	
	
	
	public void processaRetornoEnvioAssessorPublico(Arquivonf arquivonf, byte[] content, Configuracaonfe configuracaonfe) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			if (rootElement != null) {
				Element inconsistencia = SinedUtil.getChildElement("INCONSISTENCIA", rootElement.getContent());
				List<Element> listaErro = SinedUtil.getListChildElement("ERRO", inconsistencia.getContent());
							
				if (listaErro == null || listaErro.size() == 0) {
					//??
				} else {
					this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
					
					List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
					for (Arquivonfnota arquivonfnota : listan) {
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
					
					for (Element erro : listaErro) {
						Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
						arquivonfnotaerro.setArquivonf(arquivonf);
						arquivonfnotaerro.setMensagem(erro.getValue());
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
				}
			} else {
				this.updateSituacao(arquivonf, Arquivonfsituacao.NAO_ENVIADO);
				
				List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
				for (Arquivonfnota arquivonfnota : listan) {
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					}
				}
				
				Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
				arquivonfnotaerro.setArquivonf(arquivonf);
				arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
				
				arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
			}
		} catch (Exception e) {
			String codigo = new String(content);
			
			this.updateReciboDatarecebimento(arquivonf, codigo, SinedDateUtils.currentTimestamp());
			this.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
			
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			for (Arquivonfnota arquivonfnota : listan) {
				arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_ENVIADA);
				notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo de NFS-e enviado.", null);
			}
			
			arquivonf = arquivonfDAO.loadForEntrada(arquivonf);
			NfseConsulta nfseConsulta = this.createXmlConsultaNfseAssessorPublico(codigo, configuracaonfe, arquivonf);
			String stringXmlConsultaLote = new String(Util.strings.tiraAcento(nfseConsulta.toString()).getBytes(), "ISO-8859-1");
			stringXmlConsultaLote = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + stringXmlConsultaLote;
			Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
			
			arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
			arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
			
			this.updateArquivoxmlconsultalote(arquivonf);
		}
	}
	
	public Boolean processaRetornoConsultaAssessorPublico(Arquivonf arquivonf, String xml, Prefixowebservice prefixowebservice) {
		try {
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			Arquivonfnota arquivonfnota = listan.get(0);
			
			Element rootElement = SinedUtil.getRootElementXML(xml.getBytes());
			
			Element inconsistencia = SinedUtil.getChildElement("INCONSISTENCIA", rootElement.getContent());
			if (inconsistencia != null) {
				Boolean loteProcessado = Boolean.FALSE;
				List<Element> listaErro = SinedUtil.getListChildElement("ERRO", inconsistencia.getContent());
				
				for (Element erro : listaErro) {
					if ("O Codigo do RPS informado ja foi importado anteriormente.".equals(erro.getTextTrim())) {
						loteProcessado = Boolean.TRUE;
					}
				}
				
				if (loteProcessado) {
					arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
			
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
					
					arquivonfnotaerroService.deleteByArquivonf(arquivonf);
				} else {
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
					
					arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
					
					for (Element erro : listaErro) {
						String erroStr = erro.getTextTrim();
						
						Arquivonfnotaerro arquivonfnotaerro = new Arquivonfnotaerro();
						
						arquivonfnotaerro.setArquivonf(arquivonf);
						arquivonfnotaerro.setMensagem(erroStr);
						arquivonfnotaerro.setData(new Timestamp(System.currentTimeMillis()));
						
						arquivonfnotaerroService.saveOrUpdate(arquivonfnotaerro);
					}
				}
			} else {
				Element nota = SinedUtil.getChildElement("NOTA", rootElement.getContent());
				
				if(nota != null) {
					Element sitCod = SinedUtil.getChildElement("SITCOD", nota.getContent());
					if (sitCod != null && org.apache.commons.lang.StringUtils.isNotEmpty(sitCod.getTextTrim()) && sitCod.getTextTrim().equals("1")) {
						
						arquivonfnota.getNota().setNotaStatus(NotaStatus.NFSE_EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e processado com sucesso.", null);
				
						this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
						
						try {
							String numeroNfse;
							
							numeroNfse = SinedUtil.getChildElement("COD", nota.getContent()).getTextTrim();
							
							//TODO remover println após os testes
							System.out.println("Número NFS-e Votorantim: " + numeroNfse);
							
							arquivonfnotaService.updateNumeroCodigo(arquivonfnota, StringUtils.soNumero(numeroNfse), null, null, null);
						} catch (Exception e) {
							e.printStackTrace();
							System.out.println("Erro Número NFS-e Votorantim: " + xml);
							return false;
						}
					} else {
						this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
						
						if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
							arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
							notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
						}
					}
				} else {
					this.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_ERRO);
					
					if(!NotaStatus.NFSE_EMITIDA.equals(arquivonfnota.getNota().getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(arquivonfnota.getNota().getNotaStatus())){
						arquivonfnota.getNota().setNotaStatus(NotaStatus.EMITIDA);
						notaService.alterarStatusAcao(arquivonfnota.getNota(), "Arquivo NFS-e rejeitado.", null);
						
					}
				}
			}
			
			return true;
		} catch (SinedException e) {
			return false;		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void processaRetornoCancelamentoAssessorPublico(Arquivonfnota arquivonfnota, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			if (rootElement != null) {
				Element inconsistencia = SinedUtil.getChildElement("INCONSISTENCIA", rootElement.getContent());
				
				if (inconsistencia != null) {
					List<Element> listaErro = SinedUtil.getListChildElement("ERRO", inconsistencia.getContent());
								
					if (listaErro == null || listaErro.size() == 0) {
						//??
					} else {
						arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
						Nota nota = arquivonfnota.getNota();
						NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado. ";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
						
						for (Element erro : listaErro) {
							Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
							arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
							arquivonfnotaerrocancelamento.setMensagem(erro.getValue());
							
							arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
						}
					}
				} else {
					Element notaElement = SinedUtil.getChildElement("NOTA", rootElement.getContent());
					
					if (org.apache.commons.lang.StringUtils.isNotEmpty(notaElement.getTextTrim()) && notaElement.getTextTrim().equals("Nota Nao Encontrada")) {
						arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
						Nota nota = arquivonfnota.getNota();
						NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado. ";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
						
						Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
						arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
						arquivonfnotaerrocancelamento.setMensagem(notaElement.getTextTrim());
						
						arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
					} else {
						arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
						Nota nota = arquivonfnota.getNota();
						NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
						
						if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
							String obs = "Cancelamento na Receita não efetuado. ";
							nota.setNotaStatus(lastSituacao);
							notaService.alterarStatusAcao(nota, obs, null);
						}
						
						Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
						arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
						arquivonfnotaerrocancelamento.setMensagem("Erro não identificado.");
						
						arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
					}
				}
			} else {
				arquivonfnota = arquivonfnotaService.loadForCancelamento(arquivonfnota);
				Nota nota = arquivonfnota.getNota();
				NotaStatus lastSituacao = notaHistoricoService.getNotaStatusLast(nota, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA);
				
				if(nota.getNotaStatus() == null || !nota.getNotaStatus().equals(NotaStatus.CANCELADA)){
					String obs = "Cancelamento na Receita não efetuado. ";
					nota.setNotaStatus(lastSituacao);
					notaService.alterarStatusAcao(nota, obs, null);
				}
				
				Arquivonfnotaerrocancelamento arquivonfnotaerrocancelamento = new Arquivonfnotaerrocancelamento();
				arquivonfnotaerrocancelamento.setArquivonfnota(arquivonfnota);
				arquivonfnotaerrocancelamento.setMensagem("Erro ao tentar cancelar NFS-e.");
				
				arquivonfnotaerrocancelamentoService.saveOrUpdate(arquivonfnotaerrocancelamento);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public Element adicionarQrCode(Arquivonf arquivoNf, byte[] xmlAssinado) throws Exception {
		Element rootElement = SinedUtil.getRootElementXML(xmlAssinado);
		
		if (rootElement != null && arquivoNf != null & arquivoNf.getConfiguracaonfe() != null) {
			Element nfeElement = SinedUtil.getChildElement("NFe", rootElement.getContent());
			
			if (nfeElement != null) {
				Element infNfeElement = SinedUtil.getChildElement("infNFe", nfeElement.getContent());
				
				if (infNfeElement != null) {
					Element ideElement = SinedUtil.getChildElement("ide", infNfeElement.getContent());
					
					if (ideElement != null) {
						Element tpAmbElement = SinedUtil.getChildElement("tpAmb", ideElement.getContent());
						Element dhEmiElement = SinedUtil.getChildElement("dhEmi", ideElement.getContent());
						
						Element totalElement = SinedUtil.getChildElement("total", infNfeElement.getContent());
						Element icmsTot = null;
						if (totalElement != null) {
							icmsTot = SinedUtil.getChildElement("ICMSTot", totalElement.getContent());
						}
						Element vNfElement = null;
						if (icmsTot != null) {
							vNfElement = SinedUtil.getChildElement("vNF", icmsTot.getContent());
						}
						
						Element modElement = SinedUtil.getChildElement("mod", ideElement.getContent());
						Element tpEmisElement = SinedUtil.getChildElement("tpEmis", ideElement.getContent());
						
						if (modElement != null && tpEmisElement != null && tpAmbElement != null && dhEmiElement != null) {
							if ("65".equals(modElement.getValue()) && ("6".equals(tpEmisElement.getValue()) || "7".equals(tpEmisElement.getValue()))) {
								Element infNfeSupl = new Element("infNFeSupl");
								
								Element qrCodeElement = new Element("qrCode");
								qrCodeElement.setText(NFCeUtil.getCodeQRCodeContingencia("", tpAmbElement.getValue(), 
										dhEmiElement.getValue(), vNfElement.getValue(), new XMLOutputter().outputString(nfeElement), 
										arquivoNf.getConfiguracaonfe().getIdToken(), arquivoNf.getConfiguracaonfe().getCsc(), 
										NFCeUtil.getUrlQRCode(arquivoNf.getConfiguracaonfe().getUf().getSigla(), "1".equals(tpAmbElement.getValue()))));
								infNfeSupl.addContent(qrCodeElement);
								
								Element urlChaveElement = new Element("urlChave");
								urlChaveElement.setText(NFCeUtil.getUrlConsulta(arquivoNf.getConfiguracaonfe().getUf().getSigla(), "1".equals(tpAmbElement.getValue())));
								infNfeSupl.addContent(urlChaveElement);
								
								nfeElement.addContent(infNfeSupl);
							}
						}
					}
				}
			}
		}
		
		return rootElement;
	}
	
	public void createArquivoXmlConsultaLoteNfse(Arquivonf arquivonf) {
		ConsultarLoteRpsEnvio consultarLoteRpsEnvio = this.createXmlConsultaLoteNfse(arquivonf.getEmpresa(), arquivonf.getNumeroprotocolo(), arquivonf.getConfiguracaonfe());
		String stringXmlConsultaLote = Util.strings.tiraAcento(consultarLoteRpsEnvio.toString());
		
		Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivonf.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
		arquivoDAO.saveFile(arquivonf, "arquivoxmlconsultalote");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		this.updateArquivoxmlconsultalote(arquivonf);	
	}
	
	public Arquivonf findPrefixoWebService(Integer cdarquivonf) {
		return arquivonfDAO.findPrefixoWebService(cdarquivonf);
	}

	public Arquivonf loadForEntradaWriter(Arquivonf arquivonf){
		return arquivonfDAO.loadForEntradaWriter(arquivonf);
	}
	
	private String preencheDescricaoItemNfs(NotaFiscalServicoItem notaFiscalServicoItem, Configuracaonfe configuracaonfe) {
		String descricao = null;
	
		if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_PROD) || configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SERRATALHADA_HOM)){	
			if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
				descricao = "||" + (notaFiscalServicoItem.getNomematerial() != null ? notaFiscalServicoItem.getNomematerial() + " - " : "") + (notaFiscalServicoItem.getDescricao() != null ? notaFiscalServicoItem.getDescricao() : "") + (notaFiscalServicoItem.getQtde() != null ?  " | Quantidade: " + notaFiscalServicoItem.getQtde() : "0") + (notaFiscalServicoItem.getPrecoUnitario() != null ? " | Valor Unitário: R$ " + notaFiscalServicoItem.getPrecoUnitario() : "0,00") + (notaFiscalServicoItem.getTotalParcial() != null ? " | Valor Total: R$ " + notaFiscalServicoItem.getTotalParcial() : "0,00") + "||";
			}else {
				descricao = (notaFiscalServicoItem.getNomematerial() != null ? notaFiscalServicoItem.getNomematerial() + " - " : "") + notaFiscalServicoItem.getDescricao() != null ? notaFiscalServicoItem.getDescricao() : "";
			}	
		}else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_HOM) || configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.ARANDU_PROD)){
			if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
				descricao = "||" + (notaFiscalServicoItem.getMaterial() != null && notaFiscalServicoItem.getMaterial().getNome() != null ? notaFiscalServicoItem.getMaterial().getNome() : " -") + " | Quantidade: " + (notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : "0") + " | Valor Unitário: R$ " + (notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : "0,00") + " | Valor Total: R$ " + (notaFiscalServicoItem.getTotalParcial() != null ? notaFiscalServicoItem.getTotalParcial() : "0,00") + "|| ";
			}else{
				descricao = notaFiscalServicoItem.getMaterial() != null && notaFiscalServicoItem.getMaterial().getNome() != null ? notaFiscalServicoItem.getMaterial().getNome() : " -";				
			}
		}else if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_HOM) || configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.CASTELO_PROD)){
			if (notaFiscalServicoItem.getMaterial() != null && notaFiscalServicoItem.getMaterial().getNome() != null && !notaFiscalServicoItem.getMaterial().getNome().equals("")) {
    			if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
    				descricao = "||" + notaFiscalServicoItem.getMaterial().getNome() + " | Quantidade: " + (notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : "0") + " | Valor Unitário: R$ " + (notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : "0,00") + " | Valor Total: R$ " + (notaFiscalServicoItem.getTotalParcial() != null ? notaFiscalServicoItem.getTotalParcial() : "0,00") + "||";
    			}else {
    				descricao = notaFiscalServicoItem.getMaterial().getNome();					
    			}
    		}else if (notaFiscalServicoItem.getDescricao() != null && !notaFiscalServicoItem.getDescricao().equals("")) {
    			if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
    				descricao = "||" + notaFiscalServicoItem.getDescricao() + " | Quantidade: " + (notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : "0") + " | Valor Unitário: R$ " + (notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : "0,00") + " | Valor Total: R$ " + (notaFiscalServicoItem.getTotalParcial() != null ? notaFiscalServicoItem.getTotalParcial() : "0,00") + "||";
    			}else {
    				descricao = notaFiscalServicoItem.getDescricao();						
    			}
    		}
		}else {
			if (notaFiscalServicoItem.getDescricao() != null && !notaFiscalServicoItem.getDescricao().equals("")) {
				if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
					descricao = "||" + notaFiscalServicoItem.getDescricao() + " | Quantidade: " + (notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : "0") + " | Valor Unitário: R$ " + (notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : "0,00") + " | Valor Total: R$ " + (notaFiscalServicoItem.getTotalParcial() != null ? notaFiscalServicoItem.getTotalParcial() : "0,00") + "|| ";
				}else {
					descricao = notaFiscalServicoItem.getDescricao() + " ";						
				}
			} else if (notaFiscalServicoItem.getMaterial() != null && notaFiscalServicoItem.getMaterial().getNome() != null && !notaFiscalServicoItem.getMaterial().getNome().equals("")) {
				if(parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)){
					descricao = "||" + notaFiscalServicoItem.getMaterial().getNome() + " | Quantidade: " + (notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : "0") + " | Valor Unitário: R$ " + (notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : "0,00") + " | Valor Total: R$ " + (notaFiscalServicoItem.getTotalParcial() != null ? notaFiscalServicoItem.getTotalParcial() : "0,00") + "|| ";
				}else {
					descricao = notaFiscalServicoItem.getMaterial().getNome() + " ";					
				}
			}
		}
		
		return descricao;
	}
}