package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetiporesponsavel;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.dao.AtividadetiporesponsavelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AtividadetiporesponsavelService extends GenericService<Atividadetiporesponsavel> {
	
	protected AtividadetiporesponsavelDAO atividadetiporesponsavelDAO;

	public void setAtividadetiporesponsavelDAO(
			AtividadetiporesponsavelDAO atividadetiporesponsavelDAO) {
		this.atividadetiporesponsavelDAO = atividadetiporesponsavelDAO;
	}
	
	public List<Atividadetiporesponsavel> findByAtividadetipo(Atividadetipo atividadetipo){
		return atividadetiporesponsavelDAO.findByAtividadetipo(atividadetipo);
	}
	
	public Atividadetiporesponsavel findByAtividadetipoAndProjeto(Atividadetipo atividadetipo, Projeto projeto){
		return atividadetiporesponsavelDAO.findByAtividadetipoAndProjeto(atividadetipo, projeto);
	}
	
	/* singleton */
	private static AtividadetiporesponsavelService instance;
	public static AtividadetiporesponsavelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AtividadetiporesponsavelService.class);
		}
		return instance;
	}
}
