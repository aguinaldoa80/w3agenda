package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAgrupamento;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRemessaAgrupamentoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoRemessaAgrupamentoService extends GenericService<BancoConfiguracaoRemessaAgrupamento> {

	private BancoConfiguracaoRemessaAgrupamentoDAO bancoConfiguracaoRemessaAgrupamentoDAO;
	
	public void setBancoConfiguracaoRemessaAgrupamentoDAO(
			BancoConfiguracaoRemessaAgrupamentoDAO bancoConfiguracaoRemessaAgrupamentoDAO) {
		this.bancoConfiguracaoRemessaAgrupamentoDAO = bancoConfiguracaoRemessaAgrupamentoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param listaDocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2014
	 */
	public List<BancoConfiguracaoRemessaAgrupamento> findForRetornoConfiguravel(List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento) {
		return bancoConfiguracaoRemessaAgrupamentoDAO.findForRetornoConfiguravel(listaDocumento);
	}

}