package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrauinstrucaoService extends GenericService<Grauinstrucao> {	
	
	/* singleton */
	private static GrauinstrucaoService instance;
	public static GrauinstrucaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(GrauinstrucaoService.class);
		}
		return instance;
	}
	
}
