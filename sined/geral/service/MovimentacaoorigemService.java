	package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.dao.MovimentacaoorigemDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoorigemService extends GenericService<Movimentacaoorigem>{
	
	private MovimentacaoorigemDAO movimentacaoorigemDAO;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private HistoricooperacaoService historicooperacaoService;
	
	public void setMovimentacaoorigemDAO(MovimentacaoorigemDAO movimentacaoorigemDAO) {
		this.movimentacaoorigemDAO = movimentacaoorigemDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setMovimentacaohistoricoService(MovimentacaohistoricoService movimentacaohistoricoService) {
		this.movimentacaohistoricoService = movimentacaohistoricoService;
	}
	public void setHistoricooperacaoService(
			HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	
	/**
	 * Prepara e salva a movimenta��o origem de todos os documentos.
	 * - Usado na tela de baixar conta a pagar. 
	 * 
	 * @param listaDocumento
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public void salvarMovimentacaoOrigem(List<Documento> listaDocumento, Movimentacao movimentacao, List<Movimentacao> listaMovimentacao, BaixarContaBean baixarContaBean) {
		
		Movimentacaoorigem movimentacaoOrigem = null;
		
		for (Documento bean : listaDocumento) {
			movimentacaoOrigem = new Movimentacaoorigem();
			
			movimentacaoOrigem.setDocumento(bean);
			movimentacaoOrigem.setMovimentacao(movimentacao);
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			if(usuarioLogado != null){
				movimentacaoOrigem.setCdusuarioaltera(usuarioLogado.getCdpessoa());
			}
			movimentacaoOrigem.setDtaltera(new Timestamp(System.currentTimeMillis()));
			
			Documento documentoTaxa = documentoService.loadForMovimentacaoTaxa(bean);
			Taxaitem taxaitem = documentoService.getTaxaitemByTipo(documentoTaxa, Tipotaxa.TAXAMOVIMENTO);
			
			
			if (documentoTaxa!=null && taxaitem != null && taxaitem.getValor() != null){
				Documentotipo documentotipo = documentoTaxa.getDocumentotipo();
				Money valor;
				
				if (taxaitem.isPercentual()){
					valor = documentoTaxa.getValor().multiply(taxaitem.getValor()).divide(new Money(100D));
				}else {
					valor = taxaitem.getValor();
				}
				
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				Rateioitem rateioitem = new Rateioitem();
				rateioitem.setContagerencial(documentotipo.getContagerencialtaxa());
				rateioitem.setCentrocusto(documentotipo.getCentrocusto());
				rateioitem.setPercentual(100D);
				rateioitem.setValor(valor);
				listaRateioitem.add(rateioitem);
				
				Rateio rateio = new Rateio();
				rateio.setListaRateioitem(listaRateioitem);
				
				Movimentacao movimentacaoTaxa = new Movimentacao();
				movimentacaoTaxa.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				
				Formapagamento formapagamento = movimentacao.getFormapagamento();
				if(formapagamento != null && Formapagamento.CREDITOCONTACORRENTE.equals(formapagamento)){
					formapagamento = Formapagamento.DEBITOCONTACORRENTE;
				}
				
				movimentacaoTaxa.setFormapagamento(formapagamento);
				movimentacaoTaxa.setConta(movimentacao.getConta());
				movimentacaoTaxa.setEmpresa(movimentacao.getEmpresa());
				movimentacaoTaxa.setValor(valor);
				movimentacaoTaxa.setDtmovimentacao(movimentacao.getDtmovimentacao());
				movimentacaoTaxa.setDtbanco(movimentacao.getDtbanco());
				
				movimentacaoTaxa.setRateio(rateio);
				movimentacaoTaxa.setMovimentacaoacao(Movimentacaoacao.CRIADA);
				movimentacaoTaxa.setTaxa(Boolean.TRUE);
				String historicoAux = "Taxas" + (StringUtils.isNotBlank(movimentacao.getHistorico()) ? " " + movimentacao.getHistorico() : "");
				
				documentoService.ajustaNumeroParcelaDocumento(documentoTaxa);
				if(Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse())){
					Historicooperacao historicooperacao = historicooperacaoService.findByTipo(Tipohistoricooperacao.TAXA_RECEBIMENTO);
					if(historicooperacao != null){
						List<Documento> listaDocTaxa = new ArrayList<Documento>();
						listaDocTaxa.add(documentoTaxa);
						historicoAux = historicooperacaoService.findHistoricoByMovimentofinanceiro(movimentacaoTaxa, listaDocTaxa,
																								Tipohistoricooperacao.TAXA_RECEBIMENTO, baixarContaBean.getDocumentoclasse());
						if(historicoAux.contains(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO) && documentoTaxa.getValor() != null){
							historicoAux = historicoAux.replace(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO, documentoTaxa.getValor().toString());
						}
					}					
				}
				historicoAux = historicoAux.length() > 150? historicoAux.substring(0, 150): historicoAux;
				movimentacaoTaxa.setHistorico(historicoAux);
				
				Movimentacaohistorico movimentacaohistorico = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacaoTaxa);
				String historico = "Movimenta��o relacionada: " + SinedUtil.makeLinkHistorico(movimentacao.getCdmovimentacao().toString(), "visualizaMovimentacao");
				if(StringUtils.isNotBlank(movimentacaohistorico.getObservacao())){
					movimentacaohistorico.setObservacao(movimentacaohistorico.getObservacao() + ". " + historico);
				}else {
					movimentacaohistorico.setObservacao(historico);
				}
				
				movimentacaoTaxa.setMovimentacaoacao(movimentacao.getMovimentacaoacao());
				
				movimentacaoService.saveOrUpdateNoUseTransaction(movimentacaoTaxa);
				movimentacaohistoricoService.saveOrUpdateNoUseTransaction(movimentacaohistorico);
				
				if(movimentacao.getMovimentacaohistoricoTransient() != null){
					historico = "Taxa: " + SinedUtil.makeLinkHistorico(movimentacaoTaxa.getCdmovimentacao().toString(), "visualizaMovimentacao");
					if(StringUtils.isNotBlank(movimentacao.getMovimentacaohistoricoTransient().getObservacao())){
						movimentacao.getMovimentacaohistoricoTransient().setObservacao(movimentacao.getMovimentacaohistoricoTransient().getObservacao() + ". " + historico);
					}else {
						movimentacao.getMovimentacaohistoricoTransient().setObservacao(historico);
					}
					movimentacaohistoricoService.saveOrUpdateNoUseTransaction(movimentacao.getMovimentacaohistoricoTransient());
				}
				
				movimentacaoOrigem.setMovimentacaorelacionada(movimentacaoTaxa);
				
				if (listaMovimentacao!=null){
					listaMovimentacao.add(movimentacaoTaxa);
				}
			}
			
			this.saveOrUpdateNoUseTransaction(movimentacaoOrigem);
		}
	}
	
	/**
	 * Cria a lista de <code>Movimentacaoorigem</code> em uma <code>Movimentacao</code>.
	 * <p>Se a movimenta��o j� existir, carrega toda a origem da <code>Movimentacao</code> antes de criar a origem.</p>
	 * 
	 * @see #findByMovimentacao(Movimentacao)
	 * @see #createMovimentacaoorigem(Movimentacao)
	 * @param movimentacao
	 * @author Fl�vio Tavares
	 */
	public void insereBeanMovimentacaoorigem(Movimentacao movimentacao){
		List<Movimentacaoorigem> listaOrigem = null;
		if(movimentacao.getCdmovimentacao() != null){
			listaOrigem = this.findByMovimentacao(movimentacao);
		}
		if(listaOrigem == null){
			listaOrigem = new ArrayList<Movimentacaoorigem>();
		}
		Movimentacaoorigem origem = this.createMovimentacaoorigem(movimentacao);
		listaOrigem.add(origem);
		movimentacao.setListaMovimentacaoorigem(listaOrigem);
	}
	
	/**
	 * M�todo para criar um bean de Movimentacaoorigem com os atributos de uma Movimentacao.
	 * 
	 * @param movimentacao
	 * @return Movimentacaoorigem
	 * @author Fl�vio Tavares
	 */
	public Movimentacaoorigem createMovimentacaoorigem(Movimentacao movimentacao){
		Movimentacaoorigem origem = new Movimentacaoorigem();
		origem.setMovimentacao(movimentacao);
		origem.setConta(movimentacao.getConta());
		origem.setDtaltera(movimentacao.getDtaltera());
		origem.setCdusuarioaltera(movimentacao.getCdusuarioaltera());
		return origem;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de <code>Movimentacaoorigem</code> pela <code>Movimentacao</code>.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoorigemDAO#findByMovimentacao(Movimentacao)
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacaoorigem> findByMovimentacao(Movimentacao movimentacao){
		return movimentacaoorigemDAO.findByMovimentacao(movimentacao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoorigemDAO#deleteFromDocumento
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void deleteFromDocumento(Documento documento) {
		movimentacaoorigemDAO.deleteFromDocumento(documento);
	}

	/**
	 * 
	 * 
	 * @param movimentacao
	 * @return
	 */
	public Money somaDosDocumentos(Movimentacao movimentacao) {		
		List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacaoorigemDAO.somaDosDocumentos(movimentacao);
		Money total = new Money();
		for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {			
			total = total.add(movimentacaoorigem.getDocumento().getValor());
		}
		return total;
	}

	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Movimentacaoorigem> findByAgendamento(List<Agendamento> listaAgendamento) {
		return movimentacaoorigemDAO.findByAgendamento(listaAgendamento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoorigemDAO#findForSubstituicaoByMovimentacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoorigem> findForSubstituicaoByMovimentacao(String whereIn){
		return movimentacaoorigemDAO.findForSubstituicaoByMovimentacao(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoorigemDAO#existeOrigemVariosDocumentos(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 15/06/2016
	* @author Luiz Fernando
	*/
	public boolean existeOrigemVariosDocumentos(String whereIn){
		return this.existeOrigemVariosDocumentos(whereIn, false);
	}
	
	/**
	*  M�todo com refer�ncia no DAO
	* 
	 * @param whereIn
	 * @param incluiFiltroBaixadaParcial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/07/2016
	 */
	public boolean existeOrigemVariosDocumentos(String whereIn, boolean incluiFiltroBaixadaParcial){
		return movimentacaoorigemDAO.existeOrigemVariosDocumentos(whereIn, incluiFiltroBaixadaParcial);
	}
	
	public Movimentacaoorigem createMovimentacaoorigem(Movimentacao movimentacao, Movimentacao movimentacaorelacionada){
		Movimentacaoorigem origem = new Movimentacaoorigem();
		origem.setMovimentacao(movimentacao);
		origem.setConta(movimentacao.getConta());
		origem.setMovimentacaorelacionada(movimentacaorelacionada);
		origem.setDtaltera(movimentacao.getDtaltera());
		origem.setCdusuarioaltera(movimentacao.getCdusuarioaltera());
		return origem;
	}
	
	public void updateMovimentacaorelacionada(Movimentacao movimentacao, Documento documento, Movimentacao movimentacaorelacionada){
		movimentacaoorigemDAO.updateMovimentacaorelacionada(movimentacao, documento, movimentacaorelacionada);
	}
	public List<Movimentacaoorigem> buscarMovimentacaoDocumento(Documento doc) {
		return movimentacaoorigemDAO.buscarMovimentacaoDocumento(doc);
	}
}