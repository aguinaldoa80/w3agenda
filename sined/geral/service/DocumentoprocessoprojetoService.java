package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessoprojeto;
import br.com.linkcom.sined.geral.dao.DocumentoprocessoprojetoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoprocessoprojetoService extends GenericService<Documentoprocessoprojeto>{

	private DocumentoprocessoprojetoDAO documentoprocessoprojetoDAO;
		
	public void setClientevendedorDAO(DocumentoprocessoprojetoDAO documentoprocessoprojetoDAO) {
		this.documentoprocessoprojetoDAO = documentoprocessoprojetoDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO.
	 * @param documentoprocesso
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documentoprocessoprojeto> findByDocumentoprocesso(Documentoprocesso documentoprocesso){
		return documentoprocessoprojetoDAO.findByDocumentoprocesso(documentoprocesso);
	}
	
	public void saveOrUpdateListaDocumentoprocessoprojeto(Documentoprocesso documentoprocesso){
		if(documentoprocesso == null || documentoprocesso.getCddocumentoprocesso() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser null.");
		}
		if(documentoprocesso.getListadocumentoprocessoprojeto() == null){
			return;
		}
		
		List<Documentoprocessoprojeto> listaAtual = this.findByDocumentoprocesso(documentoprocesso);
		List<Documentoprocessoprojeto> listaDelete = new ArrayList<Documentoprocessoprojeto>();
		List<Documentoprocessoprojeto> lista = documentoprocesso.getListadocumentoprocessoprojeto();
		
		if(listaAtual != null && !listaAtual.isEmpty()){
			for (Documentoprocessoprojeto documentoprocessoprojeto : listaAtual) {
				if(documentoprocessoprojeto.getCddocumentoprocessoprojeto() != null && !lista.contains(documentoprocessoprojeto)){
					listaDelete.add(documentoprocessoprojeto);
				}
			}
			
			for (Documentoprocessoprojeto documentoprocessoprojeto : listaDelete) {
				this.delete(documentoprocessoprojeto);
			}
		}
		
		for(Documentoprocessoprojeto doc : lista){
			doc.setDocumentoprocesso(new Documentoprocesso(documentoprocesso.getCddocumentoprocesso()));
			this.saveOrUpdateNoUseTransaction(doc);
		}
	}
}
