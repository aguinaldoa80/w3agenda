package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Processojuridicoandamento;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.dao.ProcessojuridicoandamentoDAO;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoandamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessojuridicoandamentoService extends GenericService<Processojuridicoandamento>{

	private ProcessojuridicoandamentoDAO processojuridicoandamentoDAO;
	
	public void setProcessojuridicoandamentoDAO(ProcessojuridicoandamentoDAO processojuridicoandamentoDAO) {
		this.processojuridicoandamentoDAO = processojuridicoandamentoDAO;
	}
	
	/**
	 * M�todo que carrega o andamento do processo
	 * 
	 * @param processojuridicoinstancia
	 * @auhtor Marden Silva
	 * 
	 */
	public List<Processojuridicoandamento> loadAndamento(Processojuridicoinstancia processojuridicoinstancia) {
		return processojuridicoandamentoDAO.loadAndamento(processojuridicoinstancia); 
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem de acordo com o filtro
	 * 
	 * @param filtro
	 * @auhtor Thiago Clemente
	 * 
	 */
	public IReport gerarRelatorioListagem(ProcessojuridicoandamentoFiltro filtro) {
		
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Processojuridicoandamento> listaListagemResult = processojuridicoandamentoDAO.findForListagem(filtro);
		List<Processojuridicoandamento> listaProcessojuridicoandamento = listaListagemResult.list();
		
		Report report = new Report("/juridico/processojuridicoandamento");
		report.setDataSource(listaProcessojuridicoandamento);
		
		return report;
	}
}
