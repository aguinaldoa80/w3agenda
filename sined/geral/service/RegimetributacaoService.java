package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.geral.dao.RegimetributacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RegimetributacaoService extends GenericService<Regimetributacao> {

	private RegimetributacaoDAO regimetributacaoDAO;
	
	public void setRegimetributacaoDAO(RegimetributacaoDAO regimetributacaoDAO) {
		this.regimetributacaoDAO = regimetributacaoDAO;
	}
	
	/**
	 * M�todo que retorna o regime de tributa��o pelo codigonfse
	 *
	 * @param regimeEspecialTributacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Regimetributacao getRegimetributacaoForImportacaoxml(String regimeEspecialTributacao) {
		Regimetributacao regimetributacao = null;
		List<Regimetributacao> listaRegimetributacao = this.findByCodigocnaexml(regimeEspecialTributacao);
		if(listaRegimetributacao != null && listaRegimetributacao.size() > 0)
			regimetributacao = listaRegimetributacao.get(0);
		return regimetributacao;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param regimeEspecialTributacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Regimetributacao> findByCodigocnaexml(String regimeEspecialTributacao) {
		return regimetributacaoDAO.findByCodigocnaexml(regimeEspecialTributacao);
	}


}
