package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaDocumentoacao;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoRemessaDocumentoacaoService extends GenericService<BancoConfiguracaoRemessaDocumentoacao> {

	/**
	* M�todo que cria a lista de situa��o referente a configura��o da remessa
	*
	* @param listaDocumentoacao
	* @param bean
	* @return
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	public List<BancoConfiguracaoRemessaDocumentoacao> createListaBancoConfiguracaoRemessaDocumentoacao(List<Documentoacao> listaDocumentoacao,	BancoConfiguracaoRemessa bean) {
		List<BancoConfiguracaoRemessaDocumentoacao> lista = new ListSet<BancoConfiguracaoRemessaDocumentoacao>(BancoConfiguracaoRemessaDocumentoacao.class);
		if (SinedUtil.isListNotEmpty(listaDocumentoacao)) {
			for (Documentoacao documentoacao : listaDocumentoacao) {
				BancoConfiguracaoRemessaDocumentoacao bancoConfiguracaoRemessaDocumentoacao = new BancoConfiguracaoRemessaDocumentoacao();
				bancoConfiguracaoRemessaDocumentoacao.setDocumentoacao(documentoacao);
				bancoConfiguracaoRemessaDocumentoacao.setBancoConfiguracaoRemessa(bean);
				lista.add(bancoConfiguracaoRemessaDocumentoacao);
			}
		}
		return lista;
	}
	
}
