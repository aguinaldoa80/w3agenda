package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresacodigocnae;
import br.com.linkcom.sined.geral.dao.EmpresacodigocnaeDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresacodigocnaeService extends GenericService<Empresacodigocnae> {

	private EmpresacodigocnaeDAO empresacodigocnaeDAO;
	private CodigocnaeService codigocnaeService;
	
	public void setEmpresacodigocnaeDAO(EmpresacodigocnaeDAO empresacodigocnaeDAO) {
		this.empresacodigocnaeDAO = empresacodigocnaeDAO;
	}
	
	public void setCodigocnaeService(CodigocnaeService codigocnaeService) {
		this.codigocnaeService = codigocnaeService;
	}
	
	public List<Codigocnae> findByEmpresa(String cnae) {
		Empresa empresa = new Empresa(Integer.parseInt(SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete()));
		
		if (empresa != null && empresacodigocnaeDAO.existsCnaeByEmpresa(empresa)) {
			List<Empresacodigocnae> empresaCnaeList = empresacodigocnaeDAO.findByEmpresa(empresa, cnae);
			List<Codigocnae> cnaeList = new ArrayList<Codigocnae>();
			
			for (Empresacodigocnae ec : empresaCnaeList) {
				cnaeList.add(ec.getCodigocnae());
			}
				
			return cnaeList;
		} else {
			return codigocnaeService.findByCnae(cnae);
		}
	}
}
