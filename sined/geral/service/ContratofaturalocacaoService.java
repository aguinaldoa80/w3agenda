package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaohistorico;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaomaterial;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaoorigem;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratomateriallocacaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.CentrocustoValorVO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnviarFaturaLocacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnviarFaturaLocacaoEmailBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnviarFaturaLocacaoResultadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratofaturalocacaoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.EmitirFaturaLocacaoContratoReport;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirFaturaLocacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirFaturaLocacaoMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirFaturaLocacaoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EvolucaoFaturamentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.ArquivoW3erpUtil;
import br.com.linkcom.sined.util.report.MergeReport;

public class ContratofaturalocacaoService extends GenericService<Contratofaturalocacao> {
	
	private ContratofaturalocacaoDAO contratofaturalocacaoDAO;
	private ParametrogeralService parametrogeralService;
	private ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService;
	private EmpresaService empresaService;
	private ContratohistoricoService contratohistoricoService;
	private ContratofaturalocacaomaterialService contratofaturalocacaomaterialService;
	private EnderecoService enderecoService;
	private ContratoService contratoService;
	private DocumentotipoService documentotipoService;
	private RateioService rateioService;
	private DocumentoService documentoService;
	private ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService;
	private DocumentohistoricoService documentohistoricoService;
	private TaxaService taxaService;
	private TelefoneService telefoneService;
	private ContratofaturalocacaoorigemService contratofaturalocacaoorigemService;
	private VcontratoqtdelocacaoService vcontratoqtdelocacaoService;
	private ContagerencialService contagerencialService;
	private CentrocustoService centrocustoService;
	private ClienteService clienteService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	private ContareceberService contareceberService;
	private EnvioemailService envioemailService;
	private RateioitemService rateioitemService;
	private ContratomateriallocacaoService contratomateriallocacaoService;
	
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setVcontratoqtdelocacaoService(
			VcontratoqtdelocacaoService vcontratoqtdelocacaoService) {
		this.vcontratoqtdelocacaoService = vcontratoqtdelocacaoService;
	}
	public void setContratofaturalocacaoorigemService(
			ContratofaturalocacaoorigemService contratofaturalocacaoorigemService) {
		this.contratofaturalocacaoorigemService = contratofaturalocacaoorigemService;
	}
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setContratofaturalocacaodocumentoService(
			ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService) {
		this.contratofaturalocacaodocumentoService = contratofaturalocacaodocumentoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setContratofaturalocacaomaterialService(
			ContratofaturalocacaomaterialService contratofaturalocacaomaterialService) {
		this.contratofaturalocacaomaterialService = contratofaturalocacaomaterialService;
	}
	public void setContratohistoricoService(
			ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContratofaturalocacaohistoricoService(
			ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService) {
		this.contratofaturalocacaohistoricoService = contratofaturalocacaohistoricoService;
	}
	public void setContratofaturalocacaoDAO(
			ContratofaturalocacaoDAO contratofaturalocacaoDAO) {
		this.contratofaturalocacaoDAO = contratofaturalocacaoDAO;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setTelefoneService(TelefoneService telefoneService) {
		this.telefoneService = telefoneService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {
		this.vdocumentonegociadoService = vdocumentonegociadoService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setContratomateriallocacaoService(ContratomateriallocacaoService contratomateriallocacaoService) {
		this.contratomateriallocacaoService = contratomateriallocacaoService;
	}
	
	public Contratofaturalocacao criaContratofaturalocacaoByContrato(Contrato contrato, Money valor, boolean existeContratoIgnorarValorMaterial){
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		listaContrato.add(contrato);
		
		Contratofaturalocacao contratofaturalocacao = new Contratofaturalocacao();
		contratofaturalocacao.setEmpresa(contrato.getEmpresa());
		contratofaturalocacao.setCliente(contrato.getCliente());
		contratofaturalocacao.setContrato(contrato);
		contratofaturalocacao.setDescricao(this.getDescricaoForFaturarLocacao(listaContrato));
		contratofaturalocacao.setDadosadicionais(contrato.getAnotacao());
		contratofaturalocacao.setSituacao(Contratofaturalocacaosituacao.FATURADA);
		contratofaturalocacao.setDtvencimento(contrato.getDtproximovencimento());
		contratofaturalocacao.setDtemissao(SinedDateUtils.currentDate());
		contratofaturalocacao.setDtcontrato(contrato.getDtinicio());
		
		Endereco enderecoentrega = null;
		Endereco enderecofatura = null;
		
		if (contrato.getEnderecoentrega() != null){
			enderecoentrega = contrato.getEnderecoentrega();
		}else if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
			enderecoentrega = contrato.getCliente().getEnderecoWithPrioridade(Enderecotipo.ENTREGA);
		}
		
		if (contrato.getEndereco()!=null){
			enderecofatura = contrato.getEndereco();
		}else if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
			enderecofatura = contrato.getCliente().getEnderecoWithPrioridade(Enderecotipo.FATURAMENTO);
		}
		
		contratofaturalocacao.setEndereco(enderecoentrega);
		contratofaturalocacao.setEnderecofatura(enderecofatura);
		
		if(!existeContratoIgnorarValorMaterial && (contrato.getIgnoravalormaterial() == null || !contrato.getIgnoravalormaterial())){
			List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial = new ArrayList<Contratofaturalocacaomaterial>();
			List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
			boolean existParcela = contrato.getListaContratoparcela() != null && !contrato.getListaContratoparcela().isEmpty();
			
			Money valorTotalItens = new Money();
			Double qtdeTottal = 0d;
			if(!existParcela){
				valor = new Money();
			}else {
				for (Contratomaterial contratomaterial : listaContratomaterial){
					valorTotalItens = valorTotalItens.add(contratomaterial.getValortotal());
					if(contratomaterial.getQtde() != null){
						qtdeTottal += contratomaterial.getQtde();
					}
				}
			}
			boolean faturamentoporitemdevolvido = contrato.getContratotipo() != null && Boolean.TRUE.equals(contrato.getContratotipo().getFaturamentoporitensdevolvidos());
			Date dataatual = new Date(System.currentTimeMillis());
			Integer diferencadias = 0;
			
			for (Contratomaterial contratomaterial : listaContratomaterial) {
				boolean addItem = true;
				Contratofaturalocacaomaterial contratofaturalocacaomaterial = new Contratofaturalocacaomaterial();
				contratofaturalocacaomaterial.setMaterial(contratomaterial.getServico());
				contratofaturalocacaomaterial.setPatrimonioitem(contratomaterial.getPatrimonioitem());
				if(existParcela){
					contratofaturalocacaomaterial.setValor(getValorUnitarioProporcional(contratomaterial.getValorunitario(), 
							contratomaterial.getQtde(), contratomaterial.getValordesconto(), valorTotalItens, valor));
				}else {
					if(faturamentoporitemdevolvido){
						List<Contratomateriallocacao> listaContratomateriallocacao = contratomateriallocacaoService.findByContratomaterial(contratomaterial);
						Double totallocado = 0.0;
						Double totaldevolvido = 0.0;
						if(SinedUtil.isListNotEmpty(listaContratomateriallocacao)){
							for(Contratomateriallocacao contratomateriallocacao : listaContratomateriallocacao){
								if(contratomaterial.getValorfechado() != null && !Boolean.TRUE.equals(contratomateriallocacao.getSubstituicao())){
									diferencadias = SinedDateUtils.diferencaDias(dataatual,contratomateriallocacao.getDtmovimentacao());
									if(Contratomateriallocacaotipo.ENTRADA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
										totaldevolvido += diferencadias * ((contratomateriallocacao.getQtde() * contratomaterial.getValorfechado()) / 30);
									}else if(Contratomateriallocacaotipo.SAIDA.equals(contratomateriallocacao.getContratomateriallocacaotipo())){
										totallocado += diferencadias * ((contratomateriallocacao.getQtde() * contratomaterial.getValorfechado()) / 30);
									}
								}
							}
							contratofaturalocacaomaterial.setValor(new Money(totallocado-totaldevolvido));
							if(contratofaturalocacaomaterial.getValor().doubleValue() > 0){
								valor = valor.add(contratofaturalocacaomaterial.getValor());
							}else {
								addItem = false;
							}
						}else {
							addItem = false;
						}
					}else {
						contratofaturalocacaomaterial.setValor(contratomaterial.getValortotal());
						valor = valor.add(contratomaterial.getValortotal());
					}
				}
				if(addItem){
					listaContratofaturalocacaomaterial.add(contratofaturalocacaomaterial);
				}
				
			}
			contratofaturalocacao.setListaContratofaturalocacaomaterial(listaContratofaturalocacaomaterial);
		}
		contratofaturalocacao.setValortotal(valor);
		
		return contratofaturalocacao;
	}
	
	public Money getValorUnitarioProporcional(Double valorUnitario, Double qtde, Money valorDesconto, Money totalProdutos, Money valorFaturar){
		if(valorUnitario == null || qtde == null || qtde == 0 || totalProdutos == null || totalProdutos.getValue().doubleValue() == 0) 
			return new Money();
		
		if(valorDesconto == null) valorDesconto = new Money();
		
		Double valorTotalComDesconto = valorUnitario * qtde - valorDesconto.getValue().doubleValue();
		Double valor = valorTotalComDesconto / qtde;
		
		Double valorProporcional = valor / totalProdutos.getValue().doubleValue() * valorFaturar.getValue().doubleValue() * qtde;
		
		return	new Money(valorProporcional);	
	}
	
	public void criaDocumentoFaturaContratoLocacao(Documento documento, Contratofaturalocacao contratofaturalocacao, List<Contrato> contratos, boolean salvarDocumento, boolean salvarDocumentohistorico) {
		documento.setNumero(contratofaturalocacao.getNumero().toString());
		documento.setMensagem1("Referente a fatura: " + contratofaturalocacao.getNumero().toString());
		
		if (salvarDocumento){
			documentoService.saveOrUpdate(documento);
		}
		
		Contratofaturalocacaodocumento contratofaturalocacaodocumento = new Contratofaturalocacaodocumento();
		contratofaturalocacaodocumento.setDocumento(documento);
		contratofaturalocacaodocumento.setContratofaturalocacao(contratofaturalocacao);
		contratofaturalocacaodocumentoService.saveOrUpdate(contratofaturalocacaodocumento);
		
		if (salvarDocumentohistorico){
			Documentohistorico documentohistorico = contratoService.criaDocumentoHistoricoFaturamentoListaContrato(documento, contratos);
			String linkFatura = "Referente � fatura: " + createLink(contratofaturalocacao);
			if(documentohistorico.getObservacao() != null){
				documentohistorico.setObservacao(documentohistorico.getObservacao() + " " + linkFatura);
			}else {
				documentohistorico.setObservacao(linkFatura);
			}
			documentohistoricoService.saveOrUpdate(documentohistorico);
		}
		
		if (contratos!=null){
			for (Contrato contrato : contratos) {
				contratoService.salvaDocumentoorigemContrato(documento);
				
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setAcao(Contratoacao.COSOLIDADO);
				contratohistorico.setContrato(contrato);
				contratohistorico.setUsuario((Usuario)NeoWeb.getUser());
				contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
				contratohistorico.setObservacao("Gerada a conta <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>.");
				contratohistoricoService.saveOrUpdate(contratohistorico);
				
				if(contrato.getAcrescimoproximofaturamento() != null && contrato.getAcrescimoproximofaturamento()){
					contratoService.updateZeraAcrescimo(contrato);
				}
				contratoService.atualizaParcelaCobradaFaturamento(contrato);
				contratoService.atualizaDtProximoVencimentoFaturamento(contrato);
				contratoService.atualizaValorRateioFaturamento(contrato);
				contratoService.updateAux(contrato.getCdcontrato());
			}
		}
	}
	
	public void saveFromFaturamento(Contratofaturalocacao contratofaturalocacao, Documento documento) {
		this.saveOrUpdate(contratofaturalocacao);
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		if(contratofaturalocacao.getContrato() != null){
			listaContrato.add(contratofaturalocacao.getContrato());
			
			createHistoricoOrigemContrato(contratofaturalocacao, listaContrato);
		} else if(contratofaturalocacao.getListaContrato() != null && contratofaturalocacao.getListaContrato().size() > 0){
			listaContrato.addAll(contratofaturalocacao.getListaContrato());
			createHistoricoOrigemContrato(contratofaturalocacao, contratofaturalocacao.getListaContrato());
		} else {
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = criaFaturaHistoricoFaturamento(contratofaturalocacao, documento);
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		}
		
		
		for (Contrato contrato : listaContrato) {
			if(vcontratoqtdelocacaoService.isHabilitadoParaRenovacao(contrato)){
				contratoService.atualizaPeriodoLocacaoByContrato(contrato.getCdcontrato().toString());
			}
		}
	}
	
	/**
	* M�todo que cria o hist�rico da fatura no faturamento/gerar receita
	*
	* @param contratofaturalocacao
	* @param documento
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	private Contratofaturalocacaohistorico criaFaturaHistoricoFaturamento(Contratofaturalocacao contratofaturalocacao, Documento documento) {
		Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
		contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.FATURADA);
		contratofaturalocacaohistorico.setContratofaturalocacao(contratofaturalocacao); 
		if(documento != null && documento.getCddocumento() != null){
			String linkConta = "<a href=\"javascript:visualizarContareceber(" +
					documento.getCddocumento() +
					")\">" +
					documento.getCddocumento() +
					"</a>";
			contratofaturalocacaohistorico.setObservacao("Receita gerada " + linkConta);
		}
		return contratofaturalocacaohistorico;
	}
	
	public void saveFromFechamento(Contratofaturalocacao contratofaturalocacao) {
		this.saveOrUpdate(contratofaturalocacao);
		
		if(contratofaturalocacao.getContrato() != null){
			List<Contrato> listaContrato = new ArrayList<Contrato>();
			listaContrato.add(contratofaturalocacao.getContrato());
			
			createHistoricoOrigemContrato(contratofaturalocacao, listaContrato);
		} else if(contratofaturalocacao.getListaContrato() != null && contratofaturalocacao.getListaContrato().size() > 0){
			createHistoricoOrigemContrato(contratofaturalocacao, contratofaturalocacao.getListaContrato());
		} else {
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.FATURADA);
			contratofaturalocacaohistorico.setContratofaturalocacao(contratofaturalocacao);
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		}
	}
	
	public void createHistoricoOrigemContrato(Contratofaturalocacao contratofaturalocacao, List<Contrato> listaContrato) {
		Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
		if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.EMITIDA);
		}else {
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.FATURADA);
		}
		contratofaturalocacaohistorico.setContratofaturalocacao(contratofaturalocacao);
		
		StringBuilder sbHistorico = new StringBuilder();
		if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
			sbHistorico.append("Fechamento do(s) contrato(s) ");
		}else {
			sbHistorico.append("Faturamento do(s) contrato(s) ");
		}
		
		for (Contrato contrato : listaContrato) {
			String identificador = contrato.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				identificador = contrato.getCdcontrato().toString();
			}
			
			Integer numeroFatura = contratofaturalocacao.getNumero();
			if(numeroFatura == null){
				numeroFatura = contratofaturalocacao.getCdcontratofaturalocacao();
			}
			
			sbHistorico
				.append("<a href=\"javascript:visualizarContrato(")
				.append(contrato.getCdcontrato())
				.append(")\">")
				.append(identificador)
				.append("</a>; ");
			
			Contratofaturalocacaoorigem contratofaturalocacaoorigem = new Contratofaturalocacaoorigem();
			contratofaturalocacaoorigem.setContrato(contrato);
			contratofaturalocacaoorigem.setContratofaturalocacao(contratofaturalocacao);
			
			contratofaturalocacaoorigemService.saveOrUpdate(contratofaturalocacaoorigem);
			
			Contratohistorico historico = new Contratohistorico();
			if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
				historico.setAcao(Contratoacao.FECHAMENTO_ROMANEIO);
			}else {
				historico.setAcao(Contratoacao.COSOLIDADO);
			}
			historico.setContrato(contrato);
			historico.setUsuario((Usuario)NeoWeb.getUser());
			historico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			
			StringBuilder sb = new StringBuilder();
			if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
				sb.append("Fatura de loca��o ")
				.append("<a href=\"javascript:visualizarFaturalocacao(")
				.append(contratofaturalocacao.getCdcontratofaturalocacao())
				.append(")\">")
				.append(numeroFatura)
				.append("</a> gerada.");
			}else {
				sb.append("Gerada a fatura de loca��o ")
					.append("<a href=\"javascript:visualizarFaturalocacao(")
					.append(contratofaturalocacao.getCdcontratofaturalocacao())
					.append(")\">")
					.append(numeroFatura)
					.append("</a>.");
			}
			historico.setObservacao(sb.toString());
			
			
			contratohistoricoService.saveOrUpdate(historico);
		}
		contratofaturalocacaohistorico.setObservacao(sbHistorico.toString());
		
		contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
	}
	
	/**
	 * M�todo que faz a gera��o do n�mero sequencial da fatura de loca��o
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 05/08/2014
	 */
	public void gerarNumeroSequencial(Contratofaturalocacao bean) {
		Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.proximonumfaturalocacao");
		
		Integer proxNum = 1;
		if(empresa.getProximonumfaturalocacao() != null){ 
			proxNum = empresa.getProximonumfaturalocacao();
		}
		
		while(true){
			if(this.existNumeroEmpresa(bean.getEmpresa(), proxNum)){
				proxNum++;
			} else break;
		}
		bean.setNumero(proxNum);
		
		proxNum++;
		empresaService.updateProximoNumFaturalocacao(empresa, proxNum);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2014
	 */
	public boolean existNumeroEmpresa(Empresa empresa, Integer numero) {
		return contratofaturalocacaoDAO.existNumeroEmpresa(empresa, numero);
	}
	
	public LinkedList<EmitirFaturaLocacaoBean> emitirFaturaLocacao(String whereIn) throws Exception {
		LinkedList<EmitirFaturaLocacaoBean> lista = new LinkedList<EmitirFaturaLocacaoBean>();		
		
		List<Contratofaturalocacao> listaContratofaturalocacao = this.findForFaturalocacao(whereIn);
		if(listaContratofaturalocacao != null && !listaContratofaturalocacao.isEmpty()){
			for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
				contratofaturalocacao.setListaContratofaturalocacaomaterial(contratofaturalocacaomaterialService.findByContratofaturalocacao(contratofaturalocacao));
				lista.add(this.criaFaturalocacaoReport(contratofaturalocacao));
			}
		}
		
		return lista;
	}
	
	private List<Contratofaturalocacao> findForFaturalocacao(String whereIn) {
		List<Contratofaturalocacao> lista = contratofaturalocacaoDAO.findForFaturalocacao(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			String whereInCliente = SinedUtil.listAndConcatenate(lista, "cliente.cdpessoa", ",");
			if(whereInCliente != null && !whereInCliente.trim().equals("")){
				List<Cliente> listaCliente = clienteService.findForFaturalocacao(whereInCliente.toString());
				if(SinedUtil.isListNotEmpty(listaCliente)){
					for(Contratofaturalocacao cf : lista){
						for(Cliente cliente : listaCliente){
							if(cliente != null &&
									cliente.getCdpessoa() != null &&
									cf.getCliente() != null && 
									cf.getCliente().getCdpessoa() != null &&
									cf.getCliente().getCdpessoa().equals(cliente.getCdpessoa())){
								cf.setCliente(cliente);
							}
						}
					}
				}
			}
		}
		return lista;
	}

	@SuppressWarnings("unchecked")
	public EmitirFaturaLocacaoBean criaFaturalocacaoReport(Contratofaturalocacao contratofaturalocacao){
		EmitirFaturaLocacaoBean bean = new EmitirFaturaLocacaoBean();
		
		Empresa empresa = contratofaturalocacao.getEmpresa();
		
		if(empresa != null){
			bean.setEmpresa_url_logomarca(SinedUtil.getLogoURLForReport(empresa));
			bean.setEmpresa_nome(StringUtils.isNotEmpty(empresa.getRazaosocial()) ?  empresa.getRazaosocial() : (empresa.getNome() != null ? empresa.getNome() : ""));
			bean.setEmpresa_email(empresa.getEmail() != null ? empresa.getEmail() : "");
			
			Endereco endereco = empresa.getEndereco();
			StringBuilder empresaendereco = new StringBuilder();
			if(endereco != null){
				if(endereco.getLogradouro() != null){ 
					empresaendereco.append(endereco.getLogradouro());
					bean.setEmpresa_logradouro(endereco.getLogradouro());
				}
				if(endereco.getNumero() != null){ 
					 empresaendereco.append(", ").append(endereco.getNumero());
					bean.setEmpresa_numero(endereco.getNumero());
				}
				if(endereco.getComplemento() != null) { 
					empresaendereco.append(" - ").append(endereco.getComplemento());
				}
				if(endereco.getBairro() != null) { 
					empresaendereco.append(", ").append(endereco.getBairro());
					bean.setEmpresa_bairro(endereco.getBairro());
				}
				if(endereco.getCep() != null) { 
					empresaendereco.append("<BR/>CEP:").append(endereco.getCep().toString());
					bean.setEmpresa_cep(endereco.getCep().toString());
				}
				if(endereco.getMunicipio() != null){ 
					 empresaendereco.append(endereco.getMunicipio().getNomecompleto());	
					bean.setEmpresa_municipio(endereco.getMunicipio().getNomecompleto());
				}			
			}
			bean.setEmpresa_endereco(empresaendereco.toString());
			
			String telefone = empresa.getTelefonesTipo();
			bean.setEmpresa_telefone(telefone);
			
			List<Telefone> listaTelefoneEmpresa = telefoneService.findByPessoa(empresa);
			String telefoneprincipal = "";
			if(listaTelefoneEmpresa != null && !listaTelefoneEmpresa.isEmpty()){
				for(Telefone tel : listaTelefoneEmpresa){
					if(tel.getTelefonetipo() != null && Telefonetipo.PRINCIPAL.equals(tel.getTelefonetipo().getCdtelefonetipo())){
						telefoneprincipal = tel.getTelefone();
						break;
					}
				}
			}
			bean.setEmpresa_telefone_principal(telefoneprincipal);
			bean.setEmpresa_cnpj(empresa.getCnpj() != null ? empresa.getCnpj().toString() : "");
			bean.setEmpresa_inscricaomunicipal(empresa.getInscricaomunicipal() != null ? empresa.getInscricaomunicipal() : "");
			bean.setEmpresa_inscricaoestadual(empresa.getInscricaoestadual() != null ? empresa.getInscricaoestadual() : "");
		}
		
		Date dataemissao = contratofaturalocacao.getDtemissao();
		StringBuilder mesreferencia = new StringBuilder()
			.append(SinedDateUtils.getDescricaoMes(dataemissao))
			.append(" ")
			.append(new SimpleDateFormat("yyyy").format(dataemissao));
		
		Date datacontrato = contratofaturalocacao.getDtcontrato(); 
		Contrato contrato = contratofaturalocacao.getContrato();
		if(datacontrato == null && contrato != null && contrato.getDtinicio() != null){
			datacontrato = contrato.getDtinicio();
		}
		
		bean.setNumero(contratofaturalocacao.getNumeroFormatado() != null ? contratofaturalocacao.getNumeroFormatado().toString() : "");
		bean.setCfop(contratofaturalocacao.getCfop() != null ? contratofaturalocacao.getCfop().getDescricaoCodigo() : null);
		bean.setMes_referencia(mesreferencia.toString());
		bean.setData_emissao(dataemissao);
		bean.setData_vencimento(contratofaturalocacao.getDtvencimento());
		bean.setData_contrato(datacontrato);
		bean.setValor_total(contratofaturalocacao.getValortotal());
		if(contratofaturalocacao.getDtvencimento() != null){
			bean.setPeriodo_servico(SinedUtil.getDescricaoPeriodo(SinedDateUtils.addMesData(contratofaturalocacao.getDtvencimento(), -1), contratofaturalocacao.getDtvencimento()));
		}
		
		Extenso extenso = new Extenso(contratofaturalocacao.getValortotal());
		bean.setValor_total_extenso(extenso.toString().toUpperCase());
		
		if(contrato != null){
			if(contrato.getIdentificador() != null){
				bean.setContrato_numero(contrato.getIdentificador());
			} else {
				bean.setContrato_numero(contrato.getCdcontrato().toString());
			}
			bean.setContrato_data_inicio(contrato.getDtinicio());
			bean.setContrato_data_renovacao(contrato.getDtrenovacao());
			bean.setContrato_quantidade(contrato.getQtde());
			bean.setContrato_valorunitario(contrato.getValor());
			
			Contrato contrato_aux = contratoService.loadForEntrada(contrato);
			bean.setContrato_valortotal(contrato_aux.getValorContrato());
		}
		
		String contratoDataDia = datacontrato != null ? SinedDateUtils.getDateProperty(datacontrato, Calendar.DAY_OF_MONTH) + "" : null;
		String contratoDataMes = datacontrato != null ? SinedDateUtils.getDescricaoMes(datacontrato).toUpperCase() : null;
		String contratoDataAno = datacontrato != null ? SinedDateUtils.getDateProperty(datacontrato, Calendar.YEAR) + "" : null;
		
		if(contratoDataDia != null && contratoDataMes != null && contratoDataAno != null){
			bean.setData_contrato_extenso(contratoDataDia + " DE " + contratoDataMes + " DE " + contratoDataAno);
		}
		
		Cliente cliente = contratofaturalocacao.getCliente();
		if(cliente != null){
			bean.setCliente_nome(cliente.getNome());
			bean.setCliente_razaosocial(cliente.getRazaosocial());
			
			String cliente_telefone = cliente.getTelefonesSemQuebraLinha();
			bean.setCliente_telefone(cliente_telefone);
			
			List<Telefone> listaTelefoneCliente = telefoneService.findByPessoa(cliente);
			String telefoneprincipal = "";
			if(listaTelefoneCliente != null && !listaTelefoneCliente.isEmpty()){
				for(Telefone tel : listaTelefoneCliente){
					if(tel.getTelefonetipo() != null && Telefonetipo.PRINCIPAL.equals(tel.getTelefonetipo().getCdtelefonetipo())){
						telefoneprincipal = tel.getTelefone();
						break;
					}
				}
			}
			bean.setCliente_telefone_principal(telefoneprincipal);
			
			String cpfOuCnpj = "";
			if(contratofaturalocacao.getCliente().getTipopessoa() != null){
				if(contratofaturalocacao.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) && contratofaturalocacao.getCliente().getCnpj() != null){
					cpfOuCnpj = contratofaturalocacao.getCliente().getCnpj().toString();
				} else if(contratofaturalocacao.getCliente().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA) && contratofaturalocacao.getCliente().getCpf() != null){
					cpfOuCnpj = contratofaturalocacao.getCliente().getCpf().toString();
				}
			} 
			bean.setCliente_cpf_cnpj(cpfOuCnpj);
			bean.setCliente_inscricaoestadual(cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "");
			
			Endereco endereco = null;
			/*if(contratofaturalocacao.getEndereco() != null){
				endereco = contratofaturalocacao.getEndereco();
			} else {*/
				if(cliente != null){
					List<Endereco> listaEnderecoCliente = enderecoService.findByCliente(cliente);
					if(listaEnderecoCliente != null && !listaEnderecoCliente.isEmpty()){
						cliente.setListaEndereco(SinedUtil.listToSet(listaEnderecoCliente, Endereco.class));
						endereco = cliente.getEnderecoWithPrioridade(Enderecotipo.ENTREGA);
					}
				}
			//}
			
			if(endereco != null){
				StringBuilder clienteendereco = new StringBuilder();
				if(endereco.getLogradouro() != null) {
					clienteendereco.append(endereco.getLogradouro()).append(" ");
					bean.setCliente_logradouro(endereco.getLogradouro());
				}
				if(endereco.getNumero() != null) {
					clienteendereco.append(endereco.getNumero()).append(" ");
					bean.setCliente_numero(endereco.getNumero());
				}
				if(endereco.getComplemento() != null) {
					clienteendereco.append(endereco.getComplemento());
				}
				bean.setCliente_endereco(clienteendereco.toString());
				bean.setCliente_bairro(endereco.getBairro() != null ? endereco.getBairro() : "");
				bean.setCliente_cep(endereco.getCep() != null ? endereco.getCep().toString() : "");
				bean.setCliente_municipio(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "");
				bean.setCliente_uf(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : "");
			}
			
			//AT 49858 - Tag de endere�os
			if (contratofaturalocacao.getEndereco()!=null){
				bean.setEndereco_entrega_logradouro(contratofaturalocacao.getEndereco().getLogradouro());
				bean.setEndereco_entrega_numero(contratofaturalocacao.getEndereco().getNumero());
				bean.setEndereco_entrega_bairro(contratofaturalocacao.getEndereco().getBairro());
				bean.setEndereco_entrega_cep(contratofaturalocacao.getEndereco().getCep() != null ? contratofaturalocacao.getEndereco().getCep().toString() : "");
				bean.setEndereco_entrega_municipio(contratofaturalocacao.getEndereco().getMunicipio()!=null ? contratofaturalocacao.getEndereco().getMunicipio().getNome() : "");
				bean.setEndereco_entrega_complemento(contratofaturalocacao.getEndereco().getComplemento()!=null ? contratofaturalocacao.getEndereco().getComplemento() : "");
				bean.setEndereco_entrega_uf(contratofaturalocacao.getEndereco().getMunicipio()!=null && contratofaturalocacao.getEndereco().getMunicipio().getUf() != null? contratofaturalocacao.getEndereco().getMunicipio().getUf().getSigla() : "");
			}
			
			if (contratofaturalocacao.getEnderecofatura()!=null){
				bean.setEndereco_fatura_logradouro(contratofaturalocacao.getEnderecofatura().getLogradouro());
				bean.setEndereco_fatura_numero(contratofaturalocacao.getEnderecofatura().getNumero());
				bean.setEndereco_fatura_bairro(contratofaturalocacao.getEnderecofatura().getBairro());
				bean.setEndereco_fatura_cep(contratofaturalocacao.getEnderecofatura().getCep() != null ? contratofaturalocacao.getEnderecofatura().getCep().toString() : "");
				bean.setEndereco_fatura_municipio(contratofaturalocacao.getEnderecofatura().getMunicipio()!=null ? contratofaturalocacao.getEnderecofatura().getMunicipio().getNome() : "");
				bean.setEndereco_fatura_complemento(contratofaturalocacao.getEnderecofatura().getComplemento()!=null ? contratofaturalocacao.getEnderecofatura().getComplemento() : "");				
				bean.setEndereco_fatura_uf(contratofaturalocacao.getEnderecofatura().getMunicipio()!=null && contratofaturalocacao.getEnderecofatura().getMunicipio().getUf() != null? contratofaturalocacao.getEnderecofatura().getMunicipio().getUf().getSigla() : "");
			}			
		}		
		
		if(contratofaturalocacao.getDadosadicionais() != null && !contratofaturalocacao.getDadosadicionais().trim().equals("")) 
			bean.setDados_adicionais_fatura(contratofaturalocacao.getDadosadicionais().replaceAll("\n", "<BR/>"));
		
		if(empresa.getObservacaoreciboreceber() != null && !empresa.getObservacaoreciboreceber().trim().equals("")) 
			bean.setDados_adicionais_empresa(empresa.getObservacaoreciboreceber().replaceAll("\n", "<BR/>"));
		
		Money valor_liquido = contratofaturalocacao.getValortotal();
		if(contrato != null){
			if(contrato.getValorIr() != null && contrato.getValorIr().getValue().doubleValue() > 0){
				bean.setValor_ir(contrato.getValorIr());
				if(contrato.getIncideir() != null && contrato.getIncideir()){
					valor_liquido = valor_liquido.subtract(contrato.getValorIr());
				}
			}
			if(contrato.getValorCsll() != null && contrato.getValorCsll().getValue().doubleValue() > 0){
				bean.setValor_csll(contrato.getValorCsll());
				if(contrato.getIncidecsll() != null && contrato.getIncidecsll()){
					valor_liquido = valor_liquido.subtract(contrato.getValorCsll());
				}
			}
			if(contrato.getValorPis() != null && contrato.getValorPis().getValue().doubleValue() > 0){
				bean.setValor_pis(contrato.getValorPis());
				if(contrato.getIncidepis() != null && contrato.getIncidepis()){
					valor_liquido = valor_liquido.subtract(contrato.getValorPis());
				}
			}
			if(contrato.getValorCofins() != null && contrato.getValorCofins().getValue().doubleValue() > 0){
				bean.setValor_cofins(contrato.getValorCofins());
				if(contrato.getIncidecofins() != null && contrato.getIncidecofins()){
					valor_liquido = valor_liquido.subtract(contrato.getValorCofins());
				}
			}
			if(contrato.getValorIss() != null && contrato.getValorIss().getValue().doubleValue() > 0){
				bean.setValor_iss(contrato.getValorIss());
				if(contrato.getIncideiss() != null && contrato.getIncideiss()){
					valor_liquido = valor_liquido.subtract(contrato.getValorIss());
				}
			}
		}
		bean.setValor_liquido(valor_liquido);
		
		LinkedList<EmitirFaturaLocacaoMaterialBean> lista = new LinkedList<EmitirFaturaLocacaoMaterialBean>();
		
		String faturasomentedescricao = parametrogeralService.getValorPorNome(Parametrogeral.FATURA_SOMENTE_DESCRICAO); 
		List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial = contratofaturalocacao.getListaContratofaturalocacaomaterial();
		if(listaContratofaturalocacaomaterial != null && !listaContratofaturalocacaomaterial.isEmpty() && !"TRUE".equalsIgnoreCase(faturasomentedescricao)){
			for (Contratofaturalocacaomaterial contratofaturalocacaomaterial : listaContratofaturalocacaomaterial) {
				EmitirFaturaLocacaoMaterialBean it = new EmitirFaturaLocacaoMaterialBean();
				it.setMaterial_nome(contratofaturalocacaomaterial.getMaterial().getNome());
				it.setPatrimonio_plaqueta(contratofaturalocacaomaterial.getPatrimonioitem() != null ? contratofaturalocacaomaterial.getPatrimonioitem().getPlaqueta() : null);
				it.setValor(contratofaturalocacaomaterial.getValor());
				
				lista.add(it);
			}
		} else {
			EmitirFaturaLocacaoMaterialBean it = new EmitirFaturaLocacaoMaterialBean();
			it.setMaterial_nome(contratofaturalocacao.getDescricao() != null ? contratofaturalocacao.getDescricao().replaceAll("\n", "<BR>") : "");
			it.setValor(contratofaturalocacao.getValortotal());
			
			lista.add(it);
		}
		bean.setListaMateriais(lista);
		bean.setSituacao_nome(contratofaturalocacao.getSituacao()!=null ? contratofaturalocacao.getSituacao().name() : "");
		bean.setUrl_marca_dagua_cancelada(SinedUtil.getUrlWithContext() + "/imagens/sistema/marca_dagua_cancelada.png");
		
		return bean;
	}
	
	public String createReportFaturaLocacaoMatricial(Contratofaturalocacao contratofaturalocacao) {
		contratofaturalocacao = this.findForFaturalocacao(contratofaturalocacao.getCdcontratofaturalocacao().toString()).get(0);
		contratofaturalocacao.setListaContratofaturalocacaomaterial(contratofaturalocacaomaterialService.findByContratofaturalocacao(contratofaturalocacao));
		
		StringBuilder relatorioMatricial = new StringBuilder();
		
		Cliente cliente = contratofaturalocacao.getCliente();
		
		Endereco endereco = null;
		if(contratofaturalocacao.getEndereco() != null){
			endereco = contratofaturalocacao.getEndereco();
		} else {
			endereco = contratofaturalocacao.getCliente().getEnderecoWithPrioridade(Enderecotipo.ENTREGA);
		}
		
		Money valorServicos = contratofaturalocacao.getValortotal();
		
//		Money valortotalservicocusto = new Money();
//		List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial = contratofaturalocacao.getListaContratofaturalocacaomaterial();
//		for (Contratofaturalocacaomaterial mat : listaContratofaturalocacaomaterial) {
//			if(mat.getValor() != null){
//				valortotalservicocusto = valortotalservicocusto.add(mat.getValor());
//			}
//		}

		Date dataemissao = contratofaturalocacao.getDtemissao();
		
		String contratoDataEmissao = SinedDateUtils.toString(SinedDateUtils.currentDate());
		String contratoDataVencimento = contratofaturalocacao.getDtvencimento() != null ? SinedDateUtils.toString(contratofaturalocacao.getDtvencimento()) : "";
		String contratoValorTotalServicos = valorServicos != null ? valorServicos.toString() : "";
		
		String contratoDataDia = dataemissao != null ? SinedDateUtils.getDateProperty(dataemissao, Calendar.DAY_OF_MONTH) + "" : "";
		String contratoDataMes = dataemissao != null ? SinedDateUtils.getDescricaoMes(dataemissao).toUpperCase() : "";
		String contratoDataAno = dataemissao != null ? SinedDateUtils.getDateProperty(dataemissao, Calendar.YEAR) + "" : "";
		
		String contratoIdentificador = contratofaturalocacao.getContrato() != null && contratofaturalocacao.getContrato().getIdentificador() != null ? contratofaturalocacao.getContrato().getIdentificador() : "";
		
		String clienteNome = cliente != null && cliente.getNome() != null ? cliente.getNome() : "";
		String clienteEnderecoLogradouro = endereco != null && endereco.getLogradouroCompleto() != null ? endereco.getLogradouroCompleto() : "";
		String clienteEnderecoBairro = endereco != null && endereco.getBairro() != null ? endereco.getBairro() : "";
		String clienteEnderecoCidade = endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null ? endereco.getMunicipio().getNome() : "";
		String clienteEnderecoUf = endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null ? endereco.getMunicipio().getUf().getSigla() : "";
		String clienteEnderecoCep = endereco != null && endereco.getCep() != null ? endereco.getCep().toString() : "";
		String clienteIe = cliente != null && cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "";
		
		String clienteCnpjCpf = "";
		if(cliente.getTipopessoa() != null){
			if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) && cliente.getCnpj() != null){
				clienteCnpjCpf = cliente.getCnpj().toString();
			} else if(cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA) && cliente.getCpf() != null){
				clienteCnpjCpf = cliente.getCpf().toString();
			}
		} 
		
		
//		LINHA 1 - 7
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 7);
		
//		LINHA 8 - 10
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 61);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataEmissao, 19, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 3);
		
//		LINHA 11 - 12
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 8);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteNome, 72, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 13 - 14
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 8);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoLogradouro, 41, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoBairro, 26, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 15 - 16
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 8);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoCidade, 37, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoCep, 13, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteEnderecoUf, 7, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 17 - 18
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 8);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteCnpjCpf, 32, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, clienteIe, 35, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		
//		LINHA 19 - 21
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataDia, 2, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 4);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataMes, 10, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 1);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataAno, 4, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 5);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoIdentificador, 20, ' ', true);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 21);
//		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 68);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoDataVencimento, 12, ' ', true);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 4);
		
//		LINHA 22 - 36 (CORPO)
		
		String descricao = contratofaturalocacao.getDescricao().replaceAll("\r\n", " ").replaceAll("\n", " ");
		
		int contInfo = 0;
		String infoadicional = (contratofaturalocacao.getDadosadicionais() != null ? contratofaturalocacao.getDadosadicionais().replaceAll("\r\n", " ").replaceAll("\n", " ") : "");
		String contratoIdentificadorCopia = new String(contratoIdentificador);
		
		for (int i = 0; i < 14; i++) {
			int posicaoInicial = (52 * i);
			int posicaoFinal = (52 * i) + 52;
			if(posicaoFinal > descricao.length()){
				posicaoFinal = descricao.length();
			}
			
			if(posicaoInicial > descricao.length()){
				int posicaoInicialInfo = (52 * contInfo);
				int posicaoFinalInfo = (52 * contInfo) + 52;
				if(posicaoFinalInfo > infoadicional.length()){
					posicaoFinalInfo = infoadicional.length();
				}
				
				if(posicaoInicialInfo > infoadicional.length()){
					
					if(contratoIdentificadorCopia != null){
						ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 10);
						ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, "CONTRATO: " + contratoIdentificadorCopia, 52, ' ', true);
						contratoIdentificadorCopia = null;
					}
					
					ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
					continue;
				}
				
				ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 10);
				ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, infoadicional.substring(posicaoInicialInfo, posicaoFinalInfo), 52, ' ', true);
				contInfo++;
				
				ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
				continue;
			}
			
			ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 10);
			ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, descricao.substring(posicaoInicial, posicaoFinal), 52, ' ', true);
			if(i == 0){
				ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 7);
				ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorTotalServicos, 10, ' ', false);
			}
			ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
		}
		
//		LINHA 37 - 41
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 1);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 68);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorTotalServicos, 12, ' ', false);
		ArquivoW3erpUtil.addQuebraLinha(relatorioMatricial, 2);
		ArquivoW3erpUtil.addColunaVazia(relatorioMatricial, 68);
		ArquivoW3erpUtil.formataTamanhoSemCaracteresEspeciais(relatorioMatricial, contratoValorTotalServicos, 12, ' ', false);
		
		return relatorioMatricial.toString();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#updateSituacao(String whereIn, Contratofaturalocacaosituacao situacao)
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 * @since 05/08/2013
	 */
	public void updateSituacao(String whereIn, Contratofaturalocacaosituacao situacao) {
		contratofaturalocacaoDAO.updateSituacao(whereIn, situacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#findForCalculoTotalGeral(ContratofaturalocacaoFiltro filtro)
	 *
     * @param filtro
	 * @author Luiz Fernando
	 */
	public Money findForCalculoTotalGeral(ContratofaturalocacaoFiltro filtro) {
		return contratofaturalocacaoDAO.findForCalculoTotalGeral(filtro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#findForListagemCsvPdf(ContratofaturalocacaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacao> findForListagemCsvPdf(ContratofaturalocacaoFiltro filtro) {
		return contratofaturalocacaoDAO.findForListagemCsvPdf(filtro);
	}
	
	public void gerarReceita(String whereIn) {
		List<Contratofaturalocacao> listaContratofaturalocacao = this.findForGerarReceita(whereIn);
		
		boolean erro = false;
		for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
			boolean erroItem = false;
			if(contratofaturalocacao.getSituacao() != null && 
				!contratofaturalocacao.getSituacao().equals(Contratofaturalocacaosituacao.EMITIDA)){
				NeoWeb.getRequestContext().addError("Para gerar receita a fatura de loca��o tem que estar na situa��o 'EMITIDA'. Fatura: " + contratofaturalocacao.getNumero());
				erroItem = true;
			}
			
			if(!erroItem){
				if(contratofaturalocacao.getContrato() == null){
				NeoWeb.getRequestContext().addError("Para gerar receita a fatura de loca��o tem que ter o contrato selecionado. Fatura: " + contratofaturalocacao.getNumero());
				erroItem = true;
				}
			}
			
			if(erroItem){
				erro = erroItem;
			}
		}
		
		if(erro) throw new SinedException("N�o foi poss�vel gerar receita.");
		
		Documentotipo documentotipo = documentotipoService.findBoleto();
		
		for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
			Contrato contrato = contratoService.findForCobranca(contratofaturalocacao.getContrato().getCdcontrato().toString()).get(0);
			Rateio rateioIndenizacao = null;
			if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
				if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao()){
					Contagerencial contagerencial = contagerencialService.getContagerencialIndenizacaoByParametro();
					Centrocusto centrocusto = centrocustoService.getCentrocustoIndenizacaoByParametro();
					if(contagerencial != null || centrocusto != null){
						rateioIndenizacao = new Rateio();
						Rateioitem rateioitem = new Rateioitem(contagerencial, centrocusto, null, contratofaturalocacao.getValortotal(), 100d);
						List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
						listaRateioitem.add(rateioitem);
						rateioIndenizacao.setListaRateioitem(listaRateioitem);
					}
				}
			}
			if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
				contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			}
			if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
				contrato.setTaxa(taxaService.findTaxa(contrato.getTaxa()));
			}
			
			contratoService.calculaValorFaturaLocacao(contrato);
			
			List<Contrato> contratos = new ArrayList<Contrato>();
			contratos.add(contrato);
//			contrato.selecionarEnderecoPrioritario();
			contrato = contratoService.selecionarEnderecoAlternativoDeCobranca(contrato);
			Documento documento = contratoService.criaContaReceberContrato(contrato, documentotipo, false);
			documento.setValor(contratofaturalocacao.getValortotal());
			if(contratofaturalocacao.getIndenizacao() != null && contratofaturalocacao.getIndenizacao() && 
					rateioIndenizacao != null){
				documento.setRateio(rateioIndenizacao);
			}
			rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
			
			if(contratofaturalocacao.getDtvencimento() != null && SinedDateUtils.afterOrEqualsIgnoreHour(contratofaturalocacao.getDtvencimento(), new Date(System.currentTimeMillis()))){
				documento.setDtvencimento(contratofaturalocacao.getDtvencimento());
			}
			this.criaDocumentoFaturaContratoLocacao(documento, contratofaturalocacao, contratos, true, true);
			
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = criaFaturaHistoricoFaturamento(contratofaturalocacao, documento); 
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		}
		
		this.updateSituacao(whereIn, Contratofaturalocacaosituacao.FATURADA);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#findForGerarReceita(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacao> findForGerarReceita(String whereIn) {
		return contratofaturalocacaoDAO.findForGerarReceita(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#findForValidacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/10/2013
	 */
	public List<Contratofaturalocacao> findForValidacao(String whereIn) {
		return contratofaturalocacaoDAO.findForValidacao(whereIn);
	}
	
	/**
	 * Cria e salva a fatura de loca��o referente a substitui��o das faturas passadas por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/10/2013
	 */
	public Contratofaturalocacao createSubstituicaoFatura(String whereIn) {
		
		Contratofaturalocacao contratofaturalocacao = new Contratofaturalocacao();
		
		contratofaturalocacao.setDtemissao(SinedDateUtils.currentDate());
		
		StringBuilder descricao = new StringBuilder();
		StringBuilder dadosadicionais = new StringBuilder();
		StringBuilder link = new StringBuilder();
		
		List<Contratofaturalocacao> lista = this.findForGerarReceita(whereIn);
		List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial = new ArrayList<Contratofaturalocacaomaterial>();
		Money valortotal = new Money();
		Cfop cfop = null;
		Contrato contrato = null;
		Endereco endereco = null;
		Endereco enderecoFatura = null;
		boolean primeiraIteracao = true;
		Date dtvencimentoMaior = null;
		
		HashMap<Contrato, String> mapContratoLinkFatura = new HashMap<Contrato, String>();
		for (Contratofaturalocacao aux: lista) {
			contratofaturalocacao.setCliente(aux.getCliente());
			contratofaturalocacao.setEmpresa(aux.getEmpresa());
			
			String linkFatura = this.createLink(aux);
			link.append(linkFatura).append(", ");
			
			if(aux.getContrato() != null){
				if(mapContratoLinkFatura.get(aux.getContrato()) == null){
					mapContratoLinkFatura.put(aux.getContrato(), linkFatura);
				}else {
					mapContratoLinkFatura.put(aux.getContrato(), mapContratoLinkFatura.get(aux.getContrato()) + ", " + linkFatura);
				}
			}
			
			if(primeiraIteracao){
				contrato = aux.getContrato();
				cfop = aux.getCfop();
				endereco = aux.getEndereco();
				enderecoFatura = aux.getEnderecofatura();
				dtvencimentoMaior = aux.getDtvencimento();
				
				primeiraIteracao = false;
			} else {
				if(contrato != null && !contrato.equals(aux.getContrato())) contrato = null;
				if(endereco != null && !endereco.equals(aux.getEndereco())) endereco = null;
				if(enderecoFatura != null && !enderecoFatura.equals(aux.getEnderecofatura())) enderecoFatura = null;
				if(cfop != null && !cfop.equals(aux.getCfop())) cfop = null;
				
				if(aux.getDtvencimento() != null){
					if(dtvencimentoMaior == null){
						dtvencimentoMaior = aux.getDtvencimento();
					}else if(SinedDateUtils.afterIgnoreHour(aux.getDtvencimento(), dtvencimentoMaior)){
						dtvencimentoMaior = aux.getDtvencimento();
					}
				}
					
			}
			
			valortotal = valortotal.add(aux.getValortotal());
			if(StringUtils.isNotBlank(aux.getDescricao())){
				descricao.append(aux.getDescricao()).append("\n");
			}
			if(StringUtils.isNotBlank(aux.getDadosadicionais())){
				dadosadicionais.append(aux.getDadosadicionais()).append("\n");
			}
			
			if(aux.getListaContratofaturalocacaomaterial() != null)
				listaContratofaturalocacaomaterial.addAll(aux.getListaContratofaturalocacaomaterial());
		}
		
		if(descricao.length() > 0){
			descricao.delete(descricao.length() - 1, descricao.length());
		}
		if(dadosadicionais.length() > 0){
			dadosadicionais.delete(dadosadicionais.length() - 1, dadosadicionais.length());
		}
		link.delete(link.length() - 2, link.length());
		
		contratofaturalocacao.setDescricao(descricao.toString());
		contratofaturalocacao.setDadosadicionais(dadosadicionais.toString());
		contratofaturalocacao.setListaContratofaturalocacaomaterial(listaContratofaturalocacaomaterial);
		contratofaturalocacao.setSituacao(Contratofaturalocacaosituacao.EMITIDA);
		contratofaturalocacao.setValortotal(valortotal);
		contratofaturalocacao.setCfop(cfop);
		contratofaturalocacao.setContrato(contrato);
		contratofaturalocacao.setEndereco(endereco);
		contratofaturalocacao.setEnderecofatura(enderecoFatura);
		contratofaturalocacao.setDtvencimento(dtvencimentoMaior);
		
		this.gerarNumeroSequencial(contratofaturalocacao);
		this.saveOrUpdate(contratofaturalocacao);
		
		Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
		contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.EMITIDA);
		contratofaturalocacaohistorico.setContratofaturalocacao(contratofaturalocacao);
		contratofaturalocacaohistorico.setObservacao("Fatura criada pela substitui��o da(s) fatura(s) " + link);
		contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		
		if(mapContratoLinkFatura != null && mapContratoLinkFatura.size() > 0){
			Timestamp dthistorico = new Timestamp(System.currentTimeMillis());
			Usuario usuarioAltera = SinedUtil.getUsuarioLogado();
			for(Contrato contratoBean : mapContratoLinkFatura.keySet()){
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setContrato(contratoBean);
				contratohistorico.setAcao(Contratoacao.SUBSTITUICAO_FATURA);
				contratohistorico.setDthistorico(dthistorico);
				contratohistorico.setUsuario(usuarioAltera);
				contratohistorico.setObservacao("Fatura(s) " + mapContratoLinkFatura.get(contratoBean) + " substitu�da pela fatura " + createLink(contratofaturalocacao));
				contratohistoricoService.saveOrUpdate(contratohistorico);
			}
		}
		
		return contratofaturalocacao;
	}
	
	public String createLink(Contratofaturalocacao contratofaturalocacao) {
		StringBuilder link = new StringBuilder()
								.append("<a href=\"javascript:visualizarFaturalocacao(")
								.append(contratofaturalocacao.getCdcontratofaturalocacao())
								.append(")\">")
								.append(contratofaturalocacao.getNumero())
								.append("</a>");
		
		return link.toString();
		
	}
	
	public void cancelamentoDocumentoByContratofaturalocacao(WebRequestContext request, String observacao, String whereIn) {
		List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = contratofaturalocacaodocumentoService.findForValidacao(whereIn);
		for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaContratofaturalocacaodocumento) {
			Documento documento = contratofaturalocacaodocumento.getDocumento();
			boolean erro = false;
			if(documento != null){
				Documentoacao documentoacao = documento.getDocumentoacao();
				
				if(documentoacao != null && (documentoacao.equals(Documentoacao.BAIXADA) || documentoacao.equals(Documentoacao.BAIXADA_PARCIAL))){
					request.addError("A conta a receber vinculada � fatura " + contratofaturalocacaodocumento.getContratofaturalocacao().getNumero() + " est� baixada. N�o � poss�vel cancelar.");
					erro = true;
				} else if(documentoacao != null && documentoacao.equals(Documentoacao.NEGOCIADA)){
					if(vdocumentonegociadoService.existeParcelasBaixadas(documento.getCddocumento().toString())){
						request.addError("A conta a receber vinculada � fatura " + contratofaturalocacaodocumento.getContratofaturalocacao().getNumero() + " est� baixada. N�o � poss�vel cancelar.");
						erro = true;
					}
				} else{
					try{
						documentoService.validacaoCancelamentoDocumento(request, documento.getCddocumento().toString(), true);
					} catch (Exception e) {
						erro = true;
						request.addError("Erro no cancelamento da conta a receber da fatura " + contratofaturalocacaodocumento.getContratofaturalocacao().getNumero() + ": " + e.getMessage());
					}
				}
				
				if(!erro){
					Documentohistorico documentohistorico = new Documentohistorico();
					documentohistorico.setObservacao("Fatura de loca��o cancelada: " + (observacao == null ? "" : observacao));
					documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
					documentohistorico.setFromcontareceber(Boolean.TRUE);
					if(Documentoacao.NEGOCIADA.equals(documentoacao)){
						documentohistorico.setIds(vdocumentonegociadoService.montaWhereInParcelasNegociadas(documento.getCddocumento().toString()));
					} else{
						documentohistorico.setIds(contratofaturalocacaodocumento.getDocumento().getCddocumento().toString());
					}
					
					documentoService.doCancelar(documentohistorico, Boolean.TRUE);
				}
			}
		}
	}
	
	/**
	 * 
	 * @param listaContrato
	 * @author Thiago Clemente
	 * 
	 */
	public String getDescricaoForFaturarLocacao(List<Contrato> listaContrato){
		List<String> descricoes = new ArrayList<String>();
		
		for (Contrato contrato: listaContrato){
			String descricao = contrato.getDescricao() + " - CONTRATO " + ((contrato.getIdentificador()!=null && !contrato.getIdentificador().equals("")) ? contrato.getIdentificador() : contrato.getCdcontrato().toString());
			if (contrato.getIgnoravalormaterial() != null && contrato.getIgnoravalormaterial() && contrato.getValor()!=null){
				descricao += " (R$ " + contrato.getValor().toString() + ")";
			}
			
			if(contrato.getFrequencia() != null && 
					contrato.getFrequencia().getCdfrequencia() != null  /*&& 
					contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL)*/){
				if(contrato.getDtiniciolocacao() != null && contrato.getDtfimlocacao() != null){
					descricao += " - Per�odo: " + SinedDateUtils.toString(contrato.getDtiniciolocacao()) + " a " + SinedDateUtils.toString(contrato.getDtfimlocacao());
				}
			}
			descricoes.add(descricao);
		}
		
		return CollectionsUtil.concatenate(descricoes, "\n");		
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isFaturaAvulsaSemContrato(String whereIn){
		return contratofaturalocacaoDAO.isFaturaAvulsaSemContrato(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see linkcom.sined.geral.dao.ContratofaturalocacaoDAO#findForEvolucao(EvolucaoFaturamentoFiltro filtro)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 17/05/2014
	 */
	public List<Contratofaturalocacao> findForEvolucao(EvolucaoFaturamentoFiltro filtro) {
		return contratofaturalocacaoDAO.findForEvolucao(filtro);
	}
	
	public boolean isFaturaIndenizacaoWithContrato(String whereIn){
		return contratofaturalocacaoDAO.isFaturaIndenizacaoWithContrato(whereIn);
	}
	
	public boolean existFaturaDiferenteEmitida(String whereIn){
		return contratofaturalocacaoDAO.existFaturaDiferenteEmitida(whereIn);
	}
	
	/**
	* M�todo que retorna um arquivo txt com os inserts de origem da fatura de loca��o de acordo com o hist�rico
	*
	* @param request
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public ModelAndView verificarContratoFaturaSemOrigem(WebRequestContext request) {
		StringBuilder scriptInsert = new StringBuilder();
		Integer cdcontratofaturalocacao = null;
		try {cdcontratofaturalocacao = Integer.parseInt(request.getParameter("cdcontratofaturalocacao"));} catch (Exception e) {}
		List<Contratofaturalocacao> listaContratofaturalocacao = contratofaturalocacaoDAO.findForSemOrigem(cdcontratofaturalocacao);
		if(SinedUtil.isListNotEmpty(listaContratofaturalocacao)){
			for(Contratofaturalocacao bean : listaContratofaturalocacao){
				if(SinedUtil.isListNotEmpty(bean.getListaContratofaturalocacaohistorico())){
					for(Contratofaturalocacaohistorico cfl : bean.getListaContratofaturalocacaohistorico()){
						if(cfl.getObservacao() != null && cfl.getObservacao().contains("visualizarContrato")){
							String observacao = cfl.getObservacao();
							List<String> listaId = new ArrayList<String>();
							while (observacao.contains("visualizarContrato(")) {
								observacao = observacao.substring(observacao.indexOf("visualizarContrato(")+19);
								if(observacao.indexOf(")") == -1) break;
								String idContrato = observacao.substring(0, observacao.indexOf(")"));
								try {Integer.parseInt(idContrato);} catch (Exception e) {break;}
								if(observacao.indexOf(")") == -1) break;
								if(!listaId.contains(idContrato)){
									scriptInsert.append("insert into contratofaturalocacaoorigem " +
											"(cdcontratofaturalocacaoorigem, cdcontratofaturalocacao, cdcontrato) " +
											" values (nextval('sq_contratofaturalocacaoorigem'), " + 
											bean.getCdcontratofaturalocacao() + ", " + idContrato + ");\n");
									listaId.add(idContrato);
								}								
							}
						}
					}
				}
			}
		}
		
		Resource resource = new Resource("text/txt","script_insert_contratofaturalocacaoorigem_" + SinedUtil.datePatternForReport() + ".txt", scriptInsert.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaoDAO#ignorarNumeracaoByEmpresa(Empresa empresa, Integer numfaturalocacaoReiniciar)
	*
	* @param empresa
	* @param numfaturalocacaoReiniciar
	* @since 10/09/2014
	* @author Luiz Fernando
	*/
	public void ignorarNumeracaoByEmpresa(Empresa empresa, Integer numfaturalocacaoReiniciar) {
		contratofaturalocacaoDAO.ignorarNumeracaoByEmpresa(empresa, numfaturalocacaoReiniciar);
	}
	
	public List<Contratofaturalocacao> findForEnviarFaturaLocacao(String whereIn) {
		return contratofaturalocacaoDAO.findForEnviarFaturaLocacao(whereIn);
	}
	
	public void savePopupEnviarFaturaLocacao(WebRequestContext request, EnviarFaturaLocacaoBean bean) throws Exception {
		EnviarFaturaLocacaoResultadoBean resultadoBean = enviarFaturaLocacao(request, bean); 
		
		if (resultadoBean.getSucesso()>0){
			request.addMessage(resultadoBean.getSucesso() + " e-mail(s) enviado(s) para o(s) cliente(s) com sucesso.");
		}
		
		if (resultadoBean.getErro()>0){
			request.addError(resultadoBean.getErro() + " e-mail(s) n�o foi(ram) enviado(s) para o(s) cliente(s).");
		}
		
		if (resultadoBean.getListaErro()!=null){
			for (String erro: resultadoBean.getListaErro()){
				request.addError(erro);
			}
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	private EnviarFaturaLocacaoResultadoBean enviarFaturaLocacao(WebRequestContext request, EnviarFaturaLocacaoBean bean) throws Exception {
		EnviarFaturaLocacaoResultadoBean resultadoBean = new EnviarFaturaLocacaoResultadoBean();
		List<String> listaMensagemErro = new ArrayList<String>();
		
		Map<Integer, List<EnviarFaturaLocacaoEmailBean>> mapaFatura = new HashMap<Integer, List<EnviarFaturaLocacaoEmailBean>>();
		List<Integer> listaCddocumento = new ArrayList<Integer>();
		
		for (EnviarFaturaLocacaoEmailBean emailBean: bean.getListaDestinatario()){
			if (Boolean.TRUE.equals(emailBean.getSelecao())){
				//Podem ter e-mails avulsos com mais de uma fatura selecionada (AT 172966)
				List<Contratofaturalocacao> listaContratofaturalocacao = new ArrayList<Contratofaturalocacao>();
				if (emailBean.isAvulso()){
					listaContratofaturalocacao = findForEnviarFaturaLocacao(bean.getSelectedItens());
				}else {
					Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoDAO.loadForEntrada(emailBean.getContratofaturalocacao());
					contratofaturalocacao.setListaContratofaturalocacaodocumento(contratofaturalocacaodocumentoService.findForValidacao(emailBean.getContratofaturalocacao().getCdcontratofaturalocacao().toString()));
					listaContratofaturalocacao.add(contratofaturalocacao);
				}
				
				for (Contratofaturalocacao contratofaturalocacao: listaContratofaturalocacao){
					final StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append("<CLIENTE_NOME>,<br/><br/>");
					stringBuilder.append("Segue em anexo � fatura de loca��o<CONTRATOFATURALOCACAO_DESCRICAO>.<br/><br/>");
					
					if (contratofaturalocacao.getEmpresa() != null && !FormaEnvioBoletoEnum.POR_ANEXO.equals(contratofaturalocacao.getEmpresa().getFormaEnvioBoleto())) {
						stringBuilder.append("<CONTRATOFATURALOCACAO_BOLETO>");
					}
					
					stringBuilder.append("Atenciosamente,<br/><CONTRATOFATURALOCACAO_EMPRESA_NOME>.");
					
					boolean sucesso = false;
					String descricao = contratofaturalocacao.getDescricao();
					String boletosStr = "";
					
					if (descricao!=null && !descricao.trim().equals("")){
						descricao = " referente a " + descricao;
					}
					
					EmailManager emailManager = new EmailManager(parametrogeralService.getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
					
					if (Boolean.TRUE.equals(bean.getEnviarBoleto())){
						try {
							String whereInCddocumentoBoleto;
							if (emailBean.isAvulso()){
								whereInCddocumentoBoleto = contratofaturalocacao.getWhereInCddocumentoBoleto();
							}else {
								whereInCddocumentoBoleto = emailBean.getWhereInCddocumento();
							}
							
							if (emailBean.isEnviarBoleto() && whereInCddocumentoBoleto!=null && !whereInCddocumentoBoleto.equals("")){
								List<Documento> listaDocumento = documentoService.findForBoleto(whereInCddocumentoBoleto);
								
								for (Documento documento: listaDocumento){
									if(Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) ||
										Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao())){
										try {
											if(documento.getContacarteira() != null) {
												Boolean contaHomologada = documento.getContacarteira().getAprovadoproducao();
												Boolean flagNaoGerarBoleto = documento.getContacarteira().getNaogerarboleto();
												
												if(contaHomologada != null && contaHomologada && (flagNaoGerarBoleto == null || !flagNaoGerarBoleto)) {
													
													if (contratofaturalocacao.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(contratofaturalocacao.getEmpresa().getFormaEnvioBoleto())) {
														Report report = new Report("/financeiro/relBoleto");
														report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documento, false)});
														
														MergeReport mergeReport = new MergeReport("boleto_" + System.currentTimeMillis() + ".pdf");
														mergeReport.addReport(report);
														
														Resource resource = mergeReport.generateResource();
														emailManager.attachFileUsingByteArray(resource.getContents(), resource.getFileName(), "application/pdf", resource.getFileName());
													} else if (contratofaturalocacao.getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(contratofaturalocacao.getEmpresa().getFormaEnvioBoleto())) {
														StringBuilder url = new StringBuilder();
														url.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(documento.getCddocumento())
															.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
														
														if (boletosStr.length() == 0) {
															boletosStr = "E o(s) link(s) para impress�o do(s) boleto(s) s�o: <br>";
														}
														boletosStr += "<a href=\"" + url + "\">Clique aqui para imprimir o boleto " + documento.getNumeroCddocumento() + "</a><br>";
														
														Report report = new Report("/financeiro/relBoleto");
														report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documento, false)});
													} else if (contratofaturalocacao.getEmpresa() != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(contratofaturalocacao.getEmpresa().getFormaEnvioBoleto())) {
														StringBuilder url = new StringBuilder();
														url.append(SinedUtil.getUrlWithContext()).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(documento.getCddocumento())
															.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
														
														if (boletosStr.length() == 0) {
															boletosStr = "E o(s) link(s) para impress�o do(s) boleto(s) s�o: <br>";
														}
														boletosStr += "<a href=\"" + url + "\">Clique aqui para imprimir o boleto " + documento.getNumeroCddocumento() + "</a><br>";
														
														Report report = new Report("/financeiro/relBoleto");
														report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documento, false)});
													}
													
													if (!listaCddocumento.contains(documento.getCddocumento())){
														documento.setAcaohistorico(Documentoacao.ENVIO_BOLETO);
														documento.setObservacaoHistorico("Boleto enviado pela primeira vez");
														documentohistoricoService.saveOrUpdate(new Documentohistorico(documento));
														listaCddocumento.add(documento.getCddocumento());
													}
													
													sucesso = true;
												}
											}
										} catch (Exception e) { }										
									}
								}
							}
						} catch (Exception e) {
						}
					}
					
					if (StringUtils.isNotEmpty(boletosStr)) {
						boletosStr += "<br>";
					}
					
					String remetente = SinedUtil.getUsuarioLogado().getEmail();
					String assunto = "Fatura de loca��o - " + contratofaturalocacao.getCliente().getNome();
					String destinatario = emailBean.getEmail();
					String mensagem = stringBuilder.toString()
							.replace("<CLIENTE_NOME>", contratofaturalocacao.getCliente().getNome())
							.replace("<CONTRATOFATURALOCACAO_DESCRICAO>", Util.strings.emptyIfNull(descricao))
							.replace("<CONTRATOFATURALOCACAO_EMPRESA_NOME>", contratofaturalocacao.getEmpresa().getNome())
							.replace("<CONTRATOFATURALOCACAO_BOLETO>", boletosStr);

					Envioemail envioemail = envioemailService.registrarEnvio(remetente, assunto, mensagem, Envioemailtipo.ENVIO_FATURA_LOCACAO, new Pessoa(contratofaturalocacao.getCliente().getCdpessoa()), destinatario, contratofaturalocacao.getCliente().getNome(), emailManager);
					
					emailManager.setFrom(remetente);
					emailManager.setSubject(assunto);
					emailManager.setTo(destinatario);
					emailManager.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, emailBean.getEmail()));
					
					if (Boolean.TRUE.equals(bean.getEnviarFatura())){
						try {
							Resource resource = Neo.getObject(EmitirFaturaLocacaoContratoReport.class).getPdfResource(request, new EmitirFaturaLocacaoContratoFiltro(contratofaturalocacao.getCdcontratofaturalocacao().toString()));
							emailManager.attachFileUsingByteArray(resource.getContents(), "fatura.pdf", "application/pdf", "fatura.pdf");
							putMapaEnviarFaturaLocacao(contratofaturalocacao, emailBean, mapaFatura);
							sucesso = true;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					if (sucesso){
						try {
							emailManager.sendMessage();
							resultadoBean.addSucesso();
						} catch (Exception e) {
							resultadoBean.addErro();
							listaMensagemErro.add("Erro no envio de e-mail da fatura de loca��o " + contratofaturalocacao.getNumero() + ".");
						}
					}else {
						resultadoBean.addErro();
					}
					
					if (listaMensagemErro!=null && listaMensagemErro.size()>0){
						for (String erro: listaMensagemErro) {
							resultadoBean.addMsgErro(erro);
						}
					}
				}
				
			}
		}
		
		for (Integer cdcontratofaturalocacao: mapaFatura.keySet()){
			String emails = CollectionsUtil.listAndConcatenate(mapaFatura.get(cdcontratofaturalocacao), "email", ", ");
			String observacao = "Envio da fatura " + (Boolean.TRUE.equals(bean.getEnviarBoleto()) ? "e boleto " : "") + "para o(s) e-mail(s) " + emails + ".";
			contratofaturalocacaohistoricoService.createAndSaveHitorico(new Contratofaturalocacao(cdcontratofaturalocacao), observacao, Contratofaturalocacaoacao.ENVIO_FATURA);
		}
		
		return resultadoBean;
	}
	
	private void putMapaEnviarFaturaLocacao(Contratofaturalocacao contratofaturalocacao, EnviarFaturaLocacaoEmailBean emailBean, Map<Integer, List<EnviarFaturaLocacaoEmailBean>> mapa) throws Exception {
		List<EnviarFaturaLocacaoEmailBean> lista = mapa.get(contratofaturalocacao.getCdcontratofaturalocacao());
		if (lista==null){
			lista = new ArrayList<EnviarFaturaLocacaoEmailBean>();
		}
		lista.add(emailBean);
		mapa.put(contratofaturalocacao.getCdcontratofaturalocacao(), lista);
	}
	
	/**
	 * Preenche o bean de CentrocustoValorVO a partir dos dados da fatura.
	 * 	 
	 * @param lista
	 * @param listaRateioitem
	 * @author Rodrigo Freitas
	 * @since 09/11/2017
	 */
	public void makeCentrocustoValorVO(List<Contratofaturalocacao> lista, List<Rateioitem> listaRateioitem) {
		for (Contratofaturalocacao c : lista) {
			List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = c.getListaContratofaturalocacaodocumento();
			for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaContratofaturalocacaodocumento) {
				if(contratofaturalocacaodocumento != null && 
						contratofaturalocacaodocumento.getDocumento() != null &&
						contratofaturalocacaodocumento.getDocumento().getRateio() != null &&
						contratofaturalocacaodocumento.getDocumento().getRateio().getCdrateio() != null){
					Integer cdrateioDocumento = contratofaturalocacaodocumento.getDocumento().getRateio().getCdrateio();
					
					if(listaRateioitem != null){
						for (Rateioitem rateioitem : listaRateioitem) {
							if(rateioitem != null && rateioitem.getRateio() != null && rateioitem.getRateio().getCdrateio() != null){
								Integer cdrateioIt = rateioitem.getRateio().getCdrateio();
								if(cdrateioIt.equals(cdrateioDocumento)){
									CentrocustoValorVO centrocustoValorVO = new CentrocustoValorVO();
									centrocustoValorVO.setCentrocusto(rateioitem.getCentrocusto());
									centrocustoValorVO.setValor(rateioitem.getValor());
									c.addCentrocustoValorVO(centrocustoValorVO);
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Retorna a lista de rateioitem das faturas de loca��o
	 * 	 
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/11/2017
	 */
	public List<Rateioitem> getListaRateioitemByContratofaturalocacao(List<Contratofaturalocacao> lista) {
		List<Integer> listIdsRateio = new ArrayList<Integer>();
		for (Contratofaturalocacao contratofaturalocacao : lista) {
			List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = contratofaturalocacao.getListaContratofaturalocacaodocumento();
			if(listaContratofaturalocacaodocumento != null && listaContratofaturalocacaodocumento.size() > 0){
				for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaContratofaturalocacaodocumento) {
					if(contratofaturalocacaodocumento != null && 
							contratofaturalocacaodocumento.getDocumento() != null && 
							contratofaturalocacaodocumento.getDocumento().getRateio() != null &&
							contratofaturalocacaodocumento.getDocumento().getRateio().getCdrateio() != null){
						listIdsRateio.add(contratofaturalocacaodocumento.getDocumento().getRateio().getCdrateio());
					}
				}
			}
		}
		
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		if(listIdsRateio != null && listIdsRateio.size() > 0){
			listaRateioitem = rateioitemService.findForFaturaLocacao(CollectionsUtil.concatenate(listIdsRateio, ","));
		}
		
		return listaRateioitem;
	}
}