package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhhistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DespesaavulsarhhistoricoService  extends GenericService<Despesaavulsarhhistorico>{

	private static DespesaavulsarhhistoricoService instance;

	public static DespesaavulsarhhistoricoService getInstance() {
		if (instance == null)
			instance = Neo.getObject(DespesaavulsarhhistoricoService.class);
		
		return instance;
	}

}
