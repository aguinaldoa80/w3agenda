package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.dao.OportunidadematerialDAO;

public class OportunidadematerialService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Oportunidadematerial> {

	private OportunidadematerialDAO oportunidadematerialDAO;

	public void setOportunidadematerialDAO(OportunidadematerialDAO oportunidadematerialDAO) {
		this.oportunidadematerialDAO = oportunidadematerialDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OportunidadematerialDAO#findByOportunidade(Oportunidade oportunidade)
	 *
	 * @param oportunidade
	 * @return
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public List<Oportunidadematerial> findByOportunidade(Oportunidade oportunidade){
		return oportunidadematerialDAO.findByOportunidade(oportunidade);
	}

	public List<Oportunidadematerial> findByOportunidade(String whereIn) {
		return oportunidadematerialDAO.findByOportunidade(whereIn);
	}

	public List<Oportunidadematerial> getListaOportunidadematerialByOportunidade(List<Oportunidadematerial> list, Oportunidade oportunidade) {
		List<Oportunidadematerial> listaNova = new ArrayList<Oportunidadematerial>();
		for (Oportunidadematerial op : list) {
			Boolean test = op.getOportunidade() != null && op.getOportunidade().getCdoportunidade() != null 
							&& oportunidade != null && oportunidade.getCdoportunidade() != null
							&& oportunidade.getCdoportunidade().equals(op.getOportunidade().getCdoportunidade());
			if(test) {
				listaNova.add(op);
			}
		}
		return listaNova;
	}
}
