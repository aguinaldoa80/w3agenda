package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materiallocalarmazenagem;
import br.com.linkcom.sined.geral.dao.MateriallocalarmazenagemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MateriallocalarmazenagemService extends GenericService<Materiallocalarmazenagem> {
	
	private MateriallocalarmazenagemDAO materiallocalarmazenagemDAO;
	
	public void setMateriallocalarmazenagemDAO(MateriallocalarmazenagemDAO materiallocalarmazenagemDAO) {
		this.materiallocalarmazenagemDAO = materiallocalarmazenagemDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MateriallocalarmazenagemDAO#findByMaterial(Material material)
	*
	* @param material
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public List<Materiallocalarmazenagem> findByMaterial(Material material){
		return materiallocalarmazenagemDAO.findByMaterial(material);
	}

}
