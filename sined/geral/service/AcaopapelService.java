package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Acaopapel;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.dao.AcaopapelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AcaopapelService extends GenericService<Acaopapel> {
	
	private AcaopapelDAO acaopapelDAO;
	
	public void setAcaopapelDAO(AcaopapelDAO acaopapelDAO) {
		this.acaopapelDAO = acaopapelDAO;
	}
	
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Pega todos os a��o papel a partir de um papel especificado.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AcaopapelDAO#findAllBy(Papel papel)
	 * @param papel
	 * @return
	 * @author Pedro Gon�alves
	 */
	public List<Acaopapel> findAllBy(Papel papel){
		return acaopapelDAO.findAllBy(papel);
	}
	
	/**
	 * Pega todos os a��o papel a partir de um papel especificado.
	 * @param papel
	 * @return
	 * @author Pedro Gon�alves
	 */
	public List<Acaopapel> findAllPermissionsByPapel(List<Usuariopapel> lista){
		return acaopapelDAO.findAllPermissionsByPapel(lista);
	}
	
	/* singleton */
	private static AcaopapelService instance;
	
	public static AcaopapelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AcaopapelService.class);
		}
		return instance;
	}
}
