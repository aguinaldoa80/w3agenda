package br.com.linkcom.sined.geral.service;


import java.util.List;

import br.com.linkcom.sined.geral.bean.ProcessoReferenciado;
import br.com.linkcom.sined.geral.dao.ProcessoReferenciadoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessoReferenciadoService extends GenericService<ProcessoReferenciado>{
	private ProcessoReferenciadoDAO processoReferenciadoDao;
	
	
	public void setProcessoReferenciadoDao(ProcessoReferenciadoDAO processoReferenciadoDao) {this.processoReferenciadoDao = processoReferenciadoDao;}







	public List<ProcessoReferenciado>findForSpedPisconfinsReg1010 (SpedpiscofinsFiltro filtro){
		
		return processoReferenciadoDao.findForSpedPisconfinsReg1010(filtro);
		
	}

}
