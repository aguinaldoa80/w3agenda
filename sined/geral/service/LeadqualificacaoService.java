package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.dao.LeadqualificacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadqualificacaoService extends GenericService<Leadqualificacao>{
	
	private LeadqualificacaoDAO leadqualificacaoDAO;

	public void setLeadqualificacaoDAO(LeadqualificacaoDAO leadqualificacaoDAO) {
		this.leadqualificacaoDAO = leadqualificacaoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2016
	 */
	public Leadqualificacao findByNome(String nome) {
		return leadqualificacaoDAO.findByNome(nome);
	}

}
