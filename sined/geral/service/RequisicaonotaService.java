package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaonota;
import br.com.linkcom.sined.geral.dao.RequisicaonotaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RequisicaonotaService extends GenericService<Requisicaonota>{
	
	private RequisicaonotaDAO requisicaonotaDAO;
	
	public void setRequisicaonotaDAO(RequisicaonotaDAO requisicaonotaDAO) {
		this.requisicaonotaDAO = requisicaonotaDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/06/2015
	 */
	public List<Requisicaonota> findByNota(String whereInNota) {
		return requisicaonotaDAO.findByNota(whereInNota);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaonotaDAO#findForPercentualfaturado(Requisicao requisicao, String whereInRequisicaomaterial)
	*
	* @param requisicao
	* @param whereInRequisicaomaterial
	* @return
	* @since 15/10/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaonota> findForPercentualfaturado(Requisicao requisicao, String whereInRequisicaomaterial) {
		return requisicaonotaDAO.findForPercentualfaturado(requisicao, whereInRequisicaomaterial);
	}
}
