package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresahistorico;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.dao.EmpresaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.EmpresaOfflineJSON;
import br.com.linkcom.sined.util.rest.android.empresa.EmpresaRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.empresa.EmpresaW3producaoRESTModel;

public class EmpresaService extends GenericService<Empresa> {

	private final static String  CRIADO_PELO_CADASTRO_CLIENTE= "Criado atrav�s do cadastro de cliente";
	
	private EmpresaService empresaService;
	private EmpresaDAO empresaDAO;
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private ContaService contaService;
	private EmpresahistoricoService empresahistoricoService;
	
	public void setEmpresahistoricoService(
			EmpresahistoricoService empresahistoricoService) {
		this.empresahistoricoService = empresahistoricoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	
	public void setLocalarmazenagemempresaService(LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}

	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}

	/**
	 * Desmarca o atributo "principal" de uma empresa, se estiver salvando uma
	 * outra empresa com este atributo marcado.
	 * 
	 * @see #updatePrincipal(Integer, Boolean)
	 * @see #saveOrUpdateNoUseTransaction(Empresa)
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdatePrincipal(final Empresa bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				empresaService.updatePrincipal(null, false);
				empresaService.saveOrUpdateNoUseTransaction(bean);

				return null;
			}
		});
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findAtivosSemFiltro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findAtivosSemFiltro() {
		return empresaDAO.findAtivosSemFiltro();
	}
	
	public List<Empresa> findAtivosSemFiltroProprietarioRural() {
		return empresaDAO.findAtivosSemFiltroProprietarioRural();
	}
	
	public List<Empresa> findAtivosSemFiltro(Empresa empresa) {
		return empresaDAO.findAtivosSemFiltro(empresa);
	}

	/**
	 * Cria uma lista de usuarioempresa a partir das empresas e o usu�rio
	 * passado por par�metro.
	 * 
	 * @param listaEmpresa
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Usuarioempresa> createListaUsuarioempresa(List<Empresa> listaEmpresa, Usuario bean) {
		List<Usuarioempresa> listaUsuarioempresa = new ListSet<Usuarioempresa>(Usuarioempresa.class);
		if (listaEmpresa != null) {
			for (Empresa empresa : listaEmpresa) {
				Usuarioempresa usuarioempresa = new Usuarioempresa();
				usuarioempresa.setEmpresa(empresa);
				usuarioempresa.setUsuario(bean);
				listaUsuarioempresa.add(usuarioempresa);
			}
		}
		return listaUsuarioempresa;
	}

	/**
	 * M�todo de refer�ncia ao DAO. Carrega a empresa marcada como principal.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadPrincipal()
	 * @return
	 * @author Hugo Ferreira
	 */
	public Empresa loadPrincipal() {
		return empresaDAO.loadPrincipal(false);
	}
	
	public Empresa loadPrincipalEmitirValeCompra() {
		return empresaDAO.loadPrincipalEmitirValeCompra(false);
	}
	
	public Empresa loadPrincipalOrEmpresaUsuario() {
		Empresa empresa = empresaDAO.loadPrincipal(true);
		if(empresa == null){
			String whereInEmpresa = new SinedUtil().getListaEmpresa();
			if(StringUtils.isNotBlank(whereInEmpresa)){
				try {
					empresa = empresaDAO.loadEmpresa(new Empresa(Integer.parseInt(whereInEmpresa.split(",")[0])));
				} catch (Exception e) {}
			}
		}
		return empresa;
	}
	
	public Empresa loadEmpresa(Empresa empresa) {
		return empresaDAO.loadEmpresa(empresa);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @return
	 * @author Thiago Gon�alves
	 */
	public String razaoEmpresaPrincipal() {
		return empresaDAO.razaoEmpresaPrincipal();
	}

	public String cnpjEmpresaPrincipal() {
		return empresaDAO.cnpjEmpresaPrincipal();
	}

	public String cpfEmpresaPrincipal() {
		return empresaDAO.cpfEmpresaPrincipal();
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadArquivoPrincipal()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Empresa loadArquivoPrincipal() {
		return empresaDAO.loadArquivoPrincipal();
	}

	/**
	 * M�todo de refer�ncia ao DAO. Faz um update no campo "principal".
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updatePrincipal(Integer,
	 *      Boolean)
	 * @param cdEmpresa
	 * @param principal
	 * @param connection
	 * @author Hugo Ferreira
	 */
	public void updatePrincipal(Integer cdEmpresa, Boolean principal) {
		empresaDAO.updatePrincipal(cdEmpresa, principal);
	}

	/**
	 * Carrega uma empresa com o arquivo de logomarca
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadComArquivo(Empresa)
	 * @param bean
	 * @return
	 * @author Hugo Ferreira
	 */
	public Empresa loadComArquivo(Empresa bean) {
		return empresaDAO.loadComArquivo(bean);
	}

	/**
	 * Renorna uma lista de empresa contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Empresa> findDescricao(String whereIn) {
		return empresaDAO.findDescricao(whereIn);
	}

	/**
	 * Retorna os nomes das Empresas, separados por v�rgula, presentes na lista
	 * do par�metro. Usado para apresentar os dados nos cabe�alhos dos
	 * relat�rios
	 * 
	 * @see #findDescricao(String)
	 * @param listaCentrocusto
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeEmpresas(List<ItemDetalhe> listaEmpresa) {
		if (listaEmpresa == null || listaEmpresa.isEmpty())
			return null;

		String stCds = CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);

		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "razaosocialOuNome", ", ");
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findAtivosForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findAtivosForFlex() {
		return empresaDAO.findAtivosForFlex();
	}

	/* singleton */
	private static EmpresaService instance;

	public static EmpresaService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(EmpresaService.class);
		}
		return instance;
	}

	public Integer carregaProxIdentificadorContrato(Empresa empresa) {
		return empresaDAO.carregaProxIdentificadorContrato(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaProxNumNF
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumNF(Empresa empresa) {
		return empresaDAO.carregaProxNumNF(empresa);
	}

//	/**
//	 * Faz refer�ncia ao DAO.
//	 * 
//	 * @param empresa
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	public Integer carregaProxNumContaReceber(Empresa empresa) {
//		return empresaDAO.carregaProxNumContaReceber(empresa);
//	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Romario Filho
	 */
	public Integer carregaProxNumAnimal(Empresa empresa) {
		return empresaDAO.carregaProxNumAnimal(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumNF
	 * 
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumNF(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumNF(empresa, proximoNumero);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumAnimal
	 * 
	 * @param empresa
	 * @param proximoNumero
	 * @author Luiz RomarioFilho
	 * @since 12/08/2013
	 */
	public void updateProximoNumAnimal(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumAnimal(empresa, proximoNumero);
	}

	public void updateProximoIdentificadorContrato(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoIdentificadorContrato(empresa, proximoNumero);
	}

//	/**
//	 * Faz refer�ncia ao DAO.
//	 * 
//	 * @param empresa
//	 * @param proximoNumero
//	 * @author Tom�s Rabelo
//	 */
//	public void updateProximoNumContaReceber(Empresa empresa, Integer proximoNumero) {
//		empresaDAO.updateProximoNumContaReceber(empresa, proximoNumero);
//	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaProxNumNF
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumNFProduto(Empresa empresa) {
		return empresaDAO.carregaProxNumNFProduto(empresa);
	}
	
	public Integer carregaProxNumNFConsumidor(Empresa empresa) {
		return empresaDAO.carregaProxNumNFConsumidor(empresa);
	}
	
	public Integer carregaProxNumMdfe(Empresa empresa) {
		return empresaDAO.carregaProxNumMdfe(empresa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumNF
	 * 
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumNFProduto(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumNFProduto(empresa, proximoNumero);
	}
	
	public void updateProximoNumNFConsumidor(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumNFConsumidor(empresa, proximoNumero);
	}
	
	public void updateProximoNumMdfe(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumMdfe(empresa, proximoNumero);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorVendaorcamento(Empresa empresa, Integer identificador) {
		empresaDAO.updateProximoIdentificadorVendaorcamento(empresa, identificador);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorVenda(Empresa empresa, Integer identificador) {
		empresaDAO.updateProximoIdentificadorVenda(empresa, identificador);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorPedidovenda(Empresa empresa, Integer identificador) {
		empresaDAO.updateProximoIdentificadorPedidovenda(empresa, identificador);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param identificador
	 * @author Jo�o Vitor
	 * @since 04/12/2014
	 */
	public void updateProximoNumeroMatricula(Empresa empresa, Integer matricula) {
		empresaDAO.updateProximoNumeroMatricula(empresa, matricula);
	}
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorVenda(Empresa empresa){
		return empresaDAO.carregaProximoIdentificadorVenda(empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorVendaorcamento(Empresa empresa){
		return empresaDAO.carregaProximoIdentificadorVendaorcamento(empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorPedidovenda(Empresa empresa){
		return empresaDAO.carregaProximoIdentificadorPedidovenda(empresa);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Jo�o Vitor
	 * @since 04/12/2014
	 */
	public Integer carregaProximoNumeroMatricula(Empresa empresa){
		return empresaDAO.carregaProximoNumeroMatricula(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param empresa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findCentroCustoVeiculoUso(Empresa empresa) {
		return empresaDAO.findCentroCustoVeiculoUso(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findByNotas
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findByNotas(String whereIn) {
		return empresaDAO.findByNotas(whereIn);
	}
	
	public List<Empresa> findByVendas(String whereIn) {
		return empresaDAO.findByVendas(whereIn);
	}
	
	public List<Empresa> findByMdfe(String whereIn) {
		return empresaDAO.findByMdfe(whereIn);
	}

	public Empresa loadForRecibo(Empresa empresa) {
		return empresaDAO.loadForRecibo(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findEmpresaOrdemCompra(Ordemcompra ordemcompra) {
		return empresaDAO.findEmpresaOrdemCompra(ordemcompra);
	}

	/**
	 * Busca a empresa que � das notas passadas por par�metro. Se tiver mais de
	 * uma empresa vai ocasionar um erro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#findByNotas
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa getEmpresaNFeNotas(String whereIn) {
		List<Empresa> listaEmpresa = empresaService.findByNotas(whereIn);
		Empresa empresa;
		if (listaEmpresa == null || listaEmpresa.size() > 1) {
			throw new SinedException("S� se pode gerar nota fiscal eletr�nica de uma empresa.");
		} else
			if (listaEmpresa.size() == 0) {
				empresa = empresaService.loadPrincipal();
			} else {
				empresa = listaEmpresa.get(0);
			}
		return empresa;
	}
	
	public Empresa getEmpresaForTEFNotas(String whereIn) {
		List<Empresa> listaEmpresa = empresaService.findByNotas(whereIn);
		Empresa empresa;
		if (listaEmpresa == null || listaEmpresa.size() > 1) {
			throw new SinedException("S� se pode realizar pagamento com TEF de uma empresa.");
		} else
			if (listaEmpresa.size() == 0) {
				empresa = empresaService.loadPrincipal();
			} else {
				empresa = listaEmpresa.get(0);
			}
		return empresa;
	}
	
	public Empresa getEmpresaMDFeNotas(String whereIn) {
		List<Empresa> listaEmpresa = empresaService.findByMdfe(whereIn);
		Empresa empresa;
		if (listaEmpresa == null || listaEmpresa.size() > 1) {
			throw new SinedException("S� se pode gerar nota fiscal eletr�nica de uma empresa.");
		} else
			if (listaEmpresa.size() == 0) {
				empresa = empresaService.loadPrincipal();
			} else {
				empresa = listaEmpresa.get(0);
			}
		return empresa;
	}
	
	public Empresa getEmpresaByVendas(String whereIn) {
		List<Empresa> listaEmpresa = empresaService.findByVendas(whereIn);
		Empresa empresa;
		if (listaEmpresa == null || listaEmpresa.size() > 1) {
			throw new SinedException("S� se pode realizar pagamento com TEF de uma empresa.");
		} else
			if (listaEmpresa.size() == 0) {
				empresa = empresaService.loadPrincipal();
			} else {
				empresa = listaEmpresa.get(0);
			}
		return empresa;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param empresa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa carregaEmpresa(Empresa empresa) {
		return empresaDAO.carregaEmpresa(empresa);
	}
	
	public Empresa carregaEmpresaComProprietarioRural(Empresa empresa) {
		return empresaDAO.carregaEmpresaComProprietarioRural(empresa);
	}

	public Empresa loadPrincipalWithEndereco() {
		return empresaDAO.loadPrincipalWithEndereco();
	}

	public Empresa loadWithEndereco(Empresa empresa) {
		return empresaDAO.loadWithEndereco(empresa);
	}
	
	public Empresa loadWithEnderecoTelefone(Empresa empresa) {
		return empresaDAO.loadWithEnderecoTelefone(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSpedReg0000
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0000(Empresa empresa) {
		return empresaDAO.loadForSpedReg0000(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSpedReg0005
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0005(Empresa empresa) {
		return empresaDAO.loadForSpedReg0005(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSpedReg0100
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0100(Empresa empresa) {
		return empresaDAO.loadForSpedReg0100(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findByDocumento(Documento documento) {
		return empresaDAO.findByDocumento(documento);
	}

	/**
	 * M�todo com refer�ncia no DAO busca o munic�pio da empresa principal
	 * 
	 * @see
	 * 
	 * @return
	 * @since Aug 30, 2011
	 * @author Luiz Fernando F Silva
	 */
	public Empresa buscaMunicipioEmpresaPrincipal() {
		return empresaDAO.buscaMunicipioEmpresaPrincipal();
	}

	/**
	 * M�todo para buscar site e e-mail da empresa para a gera��o do relat�rio
	 * de venda.
	 * 
	 * @author Thiago Augusto
	 * @param empresa
	 * @return
	 */
	public Empresa buscarEmalSiteEmpresa(Empresa empresa) {
		return empresaDAO.buscarEmalSiteEmpresa(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSpedPiscofinsReg0100(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForSpedPiscofinsReg0100(Empresa empresa) {
		return empresaDAO.loadForSpedPiscofinsReg0100(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSpedPiscofinsReg0140(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForSpedPiscofinsReg0140(Empresa empresa) {
		return empresaDAO.loadForSpedPiscofinsReg0140(empresa);
	}

	public List<Empresa> findForSpedPiscofinsReg0140(SpedpiscofinsFiltro filtro) {
		return empresaDAO.findForSpedPiscofinsReg0140(filtro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#cnpjEmpresa(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String cnpjEmpresa(Empresa empresa) {
		return empresaDAO.cnpjEmpresa(empresa);
	}

	/***
	 * M�todo com refer�ncia ao DAO
	 * 
	 * @param cnpj
	 * @return
	 * @author Thiers Euller
	 */
	public Empresa findByCnpj(Cnpj cnpj) {
		return empresaDAO.findByCnpj(cnpj);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadByArquivonf
	 * 
	 * @param arquivonf
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public Empresa loadByArquivonf(Arquivonf arquivonf) {
		return empresaDAO.loadByArquivonf(arquivonf);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findForSintegra(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa findForSintegra(Empresa empresa) {
		return empresaDAO.findForSintegra(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#getEmpresaPrincipalForPendenciafinanceira()
	 * 
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa getEmpresaPrincipalForPendenciafinanceira(Cnpj cnpjlinkcom) {
		return empresaDAO.getEmpresaPrincipalForPendenciafinanceira(cnpjlinkcom);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#isIntegracaoWms(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isIntegracaoWms(Empresa empresa) {
		if (empresa == null)
			empresa = this.loadPrincipal();
		return empresaDAO.isIntegracaoWms(empresa);
	}

	/**
	 * Busca os dados para o cache da tela de pedido de venda offline
	 * 
	 * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<EmpresaOfflineJSON> findForPVOffline() {
		List<EmpresaOfflineJSON> lista = new ArrayList<EmpresaOfflineJSON>();
		for (Empresa e : empresaDAO.findForPVOffline())
			lista.add(new EmpresaOfflineJSON(e));

		return lista;
	}
	
	public List<EmpresaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	public List<EmpresaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<EmpresaRESTModel> lista = new ArrayList<EmpresaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Empresa e : empresaDAO.findForAndroid(whereIn))
				lista.add(new EmpresaRESTModel(e));
		}
		
		return lista;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#getUrlIntegracaoWms(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String getUrlIntegracaoWms(Empresa empresa) {
		if (empresa == null)
			empresa = this.loadPrincipal();
		return empresaDAO.getUrlIntegracaoWms(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadComContagerencialCentrocusto(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComContagerencialCentrocusto(Empresa empresa) {
		return empresaDAO.loadComContagerencialCentrocusto(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Empresa> findAtivosByUsuario() {
		return empresaDAO.findAtivosByUsuario();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findByUsuario()
	 * 
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Empresa> findByUsuario() {
		return empresaDAO.findByUsuario();
	}

	/**
	 * Retorna o comprovante configuravel da empresa, caso ela o tenha definido
	 * 
	 * @param tipoComprovante
	 *            Tipo do comprovante desejado
	 * @param empresa
	 * @return
	 */
	public ComprovanteConfiguravel findComprovaConfiguravelByTipoAndEmpresa(TipoComprovante tipoComprovante, Empresa empresa) {
		return empresaDAO.findComprovaConfiguravelByTipoAndEmpresa(tipoComprovante, empresa);
	}

	/**
	 * Verifica se empresa possui um comprovante configuravel daquele tipo
	 * configurado
	 * 
	 * @param tipoComprovante
	 * @param empresa
	 * @return
	 */
	public boolean possuiComprovanteConfiguravel(TipoComprovante tipoComprovante, Empresa empresa) {
		return empresaDAO.possuiComprovanteConfiguravel(tipoComprovante, empresa, false);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#possuiComprovanteConfiguravel(TipoComprovante tipoComprovante, Empresa empresa, boolean alternativo)
	*
	* @param tipoComprovante
	* @param empresa
	* @param alternativo
	* @return
	* @since 31/01/2017
	* @author Luiz Fernando
	*/
	public boolean possuiComprovanteConfiguravel(TipoComprovante tipoComprovante, Empresa empresa, boolean alternativo) {
		return empresaDAO.possuiComprovanteConfiguravel(tipoComprovante, empresa, alternativo);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @name buscarProximoIdentificadorVenda
	 * @param bean
	 * @return
	 * @return Integer
	 * @author Thiago Augusto
	 * @date 12/07/2012
	 * 
	 */
	public Empresa buscarProximoIdentificadorVenda(Empresa bean) {
		return empresaDAO.buscarProximoIdentificadorVenda(bean);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#buscarProximoIdentificadorPedidovenda(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa buscarProximoIdentificadorPedidovenda(Empresa empresa) {
		return empresaDAO.buscarProximoIdentificadorPedidovenda(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadComRepresentantes(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComRepresentantes(Empresa empresa) {
		return empresaDAO.loadComRepresentantes(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadComContagerenciaiscompra(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComContagerenciaiscompra(Empresa empresa) {
		return empresaDAO.loadComContagerenciaiscompra(empresa);
	}

	public Empresa loadEmpresaForRemessaConfiguravel(Empresa empresa) {
		return empresaDAO.loadEmpresaForRemessaConfiguravel(empresa);
	}

	public EmpresaVO getEmpresaForRemessaConfiguravel(Empresa empresa) {
		EmpresaVO empresaVO = new EmpresaVO();
		empresa = this.loadEmpresaForRemessaConfiguravel(empresa);
		setInformacoesPropriedadeRural(empresa);
		empresaVO.setNome(empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
		if(Boolean.TRUE.equals(empresa.getPropriedadeRural())){
			empresaVO.setCnpj(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() != null ? empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() : null);
		}else {
			empresaVO.setCnpj(empresa.getCnpj() != null ? empresa.getCnpj().getValue() : null);
		}
		if (empresa.getEndereco() != null) {
			empresaVO.setLogradouro(empresa.getEndereco().getLogradouro());
			empresaVO.setNumero(empresa.getEndereco().getNumero());
			empresaVO.setComplemento(empresa.getEndereco().getComplemento());
			empresaVO.setBairro(empresa.getEndereco().getBairro());
			if (empresa.getEndereco().getCep() != null)
				empresaVO.setCep(empresa.getEndereco().getCep().getValue());
			if (empresa.getEndereco().getMunicipio() != null) {
				empresaVO.setMunicipio(empresa.getEndereco().getMunicipio().getNome());
				if (empresa.getEndereco().getMunicipio().getUf() != null)
					empresaVO.setUf(empresa.getEndereco().getMunicipio().getUf().getSigla());
			}
		}
		return empresaVO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#buscarInfContribuinte(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa buscarInfContribuinte(Empresa empresa) {
		return empresaDAO.buscarInfContribuinte(empresa);
	}

	public Empresa loadForArquivoSEFIP(Empresa empresa) {
		return empresaDAO.loadForArquivoSEFIP(empresa);
	}

	public Empresa loadForArquivoDIRF(Empresa empresa) {
		return empresaDAO.loadForArquivoDIRF(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumFaturalocacao(Empresa
	 *      empresa, Integer proximoNumero)
	 * 
	 * @param empresa
	 * @param proximoNumero
	 * @author Luiz Fernando
	 */
	public void updateProximoNumFaturalocacao(Empresa empresa, Integer proximoNumero) {
		empresaDAO.updateProximoNumFaturalocacao(empresa, proximoNumero);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumOportunidade(Empresa empresa, Integer proxNum)
	 *
	 * @param empresa
	 * @param proxNum
	 * @author Rodrigo Freitas
	 * @since 06/09/2013
	 */
	public void updateProximoNumOportunidade(Empresa empresa, Integer proxNum) {
		empresaDAO.updateProximoNumOportunidade(empresa, proxNum);
	}

	/**
	 * M�todo que adiciona a empresa na sess�o
	 * 
	 * @param request
	 * @param empresa
	 * @param id
	 * @author Luiz Fernando
	 */
	public void adicionarEmpresaSessaoForFluxocompra(WebRequestContext request, Empresa empresa, String id) {
		String emp = request.getParameter("empresa");
		try {
			if (empresa != null && empresa.getCdpessoa() != null) {
				emp = empresa.getCdpessoa().toString();
			} else
				if (empresa == null && emp != null && !emp.equals("")) {
					String[] aux = emp.split("=");
					emp = aux[1].replace("]", "");
				}

			if ("ENTREGA".equals(id) && (emp == null || emp.equals(""))) {
				emp = request.getParameter("empresaentrega");
				if (emp != null && !emp.equals("")) {
					String[] aux = emp.split("=");
					emp = aux[1].replace("]", "");
				}
			}
			request.getSession().setAttribute("EMPRESA_FILTRO_MATERIAIS_FLUXOCOMPRA_" + id, emp);
		} catch (Exception e) {
			request.getSession().removeAttribute("EMPRESA_FILTRO_MATERIAIS_FLUXOCOMPRA_" + id);
		}
	}

	/**
	 * M�todo que retorna a empresa que est� na sess�o de acordo com a tela do
	 * fluxo de compra
	 * 
	 * @param id
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa getEmpresaSessaoFluxocompraForBuscamaterial(String id) {
		String cdempresa = "";
		Empresa empresa = null;
		try {
			if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS_FLUXOCOMPRA_" + id) != null) {
				cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS_FLUXOCOMPRA_" + id);
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
		} catch (Exception e) {
			empresa = null;
		}
		return empresa;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForDocumento(Empresa
	 *      empresa)
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForDocumento(Empresa empresa) {
		return empresaDAO.loadForDocumento(empresa);
	}

	public List<Empresa> findFiliais(Empresa matriz) {
		return empresaDAO.findFiliais(matriz);
	}

	public List<Empresa> findForIntegracaoEmporium() {
		return empresaDAO.findForIntegracaoEmporium();
	}

	public Empresa loadForVenda(Empresa empresa) {
		return empresaDAO.loadForVenda(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaEmpresaByCnpj(Cnpj
	 *      cnpj)
	 * 
	 * @param cnpj
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa carregaEmpresaByCnpj(Cnpj cnpj) {
		return empresaDAO.carregaEmpresaByCnpj(cnpj);
	}

	/**
	 * M�todo com refer�ncia no DAO
     *
     * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#getNaopreencherdtsaidaNota(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean getNaopreencherdtsaidaNota(Empresa empresa) {
		return empresaDAO.getNaopreencherdtsaidaNota(empresa);
	}
	
	/**
	 * 
	 * @param empresa
	 * @param proximonumeroordemservico
	 * @author Thiago Clemente
	 * 
	 */
	public void updateProximonumeroordemservico(Empresa empresa, Integer proximonumeroordemservico){
		empresaDAO.updateProximonumeroordemservico(empresa, proximonumeroordemservico);
	}
	
	/**
	 * 
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isRepresentacao(Empresa empresa){
		return empresaDAO.isRepresentacao(empresa);
	}

	/**
	 * 
	 * @param empresa
	 * @return
	 */
	public Empresa loadWithTransportador(Empresa empresa) {
		return empresaDAO.getTransportador(empresa);
	}

	public Empresa getEmpresaByDocumentos(String whereIn) {
		return empresaDAO.getEmpresaByDocumentos(whereIn);
	}

	/**
	 * M�todo que retorna true se o municipio fiscal da empresa for brasilia
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 12/11/2013
	 */
	public Boolean isMunicipioBrasilia(Empresa empresa) {
		Boolean municipioBrasilia = false;
		if(empresa != null && empresa.getCdpessoa() != null){
			Empresa emp = this.loadWithMunicipioFiscal(empresa);
			if(emp != null && emp.getMunicipiosped() != null && emp.getMunicipiosped().getNome() != null && 
					("Brasilia".equalsIgnoreCase(emp.getMunicipiosped().getNome()) || 
					 "Bras�lia".equalsIgnoreCase(emp.getMunicipiosped().getNome()))){
				municipioBrasilia = true;
			}
		}
		return municipioBrasilia;
	}
	
	/**
	 * 
	 * @param cdpessoa
	 */
	public boolean isEmpresa(Integer cdpessoa){
		return empresaDAO.isEmpresa(cdpessoa);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadWithMunicipioFiscal(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 12/11/2013
	 */
	public Empresa loadWithMunicipioFiscal(Empresa empresa){
		return empresaDAO.loadWithMunicipioFiscal(empresa);
	}
	
	/**
	 * M�todo para incluir o cdpessoa na tabela empresa
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public ModelAndView incluirPessoaComoEmpresa(Pessoa pessoa){
		String mensagemErro = null;
		boolean sucesso = false;
		
		if(pessoa == null || pessoa.getCdpessoa() == null){
			mensagemErro = "Erro: Id n�o pode ser nulo.";
		}
		
		if(existCdpessoaTabelaEmpresa(pessoa.getCdpessoa())){
			mensagemErro = "Existe empresa cadastrada com o mesmo cdpessoa (" + pessoa.getCdpessoa() + ").";
		}else {
			try {
				insertEmpresaByCdpessoa(pessoa.getCdpessoa());
				sucesso = true;
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}
		}
		
		return new JsonModelAndView().addObject("sucesso", sucesso)
									 .addObject("mensagemErro", mensagemErro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#existCdpessoaTabelaEmpresa(Integer cdpessoa)
	 *
	 * @param cdpessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public Boolean existCdpessoaTabelaEmpresa(Integer cdpessoa) {
		return empresaDAO.existCdpessoaTabelaEmpresa(cdpessoa);
	}
	
	public Boolean existCdpessoaTabelaFornecedorCliente(String cdpessoa) {
		return empresaDAO.existCdpessoaTabelaFornecedorCliente(cdpessoa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#insertEmpresaByCdpessoa(Integer cdpessoa)
	 *
	 * @param cdpessoa
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	private void insertEmpresaByCdpessoa(Integer cdpessoa) {
		if(cdpessoa !=null){
			empresaDAO.insertEmpresaByCdpessoa(cdpessoa);
			Empresahistorico historico = new Empresahistorico();
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao(CRIADO_PELO_CADASTRO_CLIENTE);
			historico.setEmpresa(new Empresa(cdpessoa));
			historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			empresahistoricoService.saveOrUpdate(historico);
		}
	}
	
	/**
	 * M�todo que carrega a conta e carteira padr�es vinculados � uma empresa.
	 * 
	 * @param empresa
	 * @return
	 * @throws Exception 
	 * @author Rafael Salvio
	 */
	public Empresa loadWithContaECarteira(Empresa empresa) throws Exception{
		return empresaDAO.loadWithContaECarteira(empresa);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public Empresa loadWithDadosTransferencia(Empresa empresa) {
		return empresaDAO.loadWithDadosTransferencia(empresa);
	}
	
	public void saveOrUpdateNumeroEstabelecimento(Empresa empresa){
		empresaDAO.saveOrUpdateNumeroEstabelecimento(empresa);
	}
	
	/**
	* M�todo que retorna a raz�o social ou nome da empresa
	*
	* @param empresa
	* @return
	* @since 12/12/2014
	* @author Luiz Fernando
	*/
	public String getEmpresaRazaosocialOuNome(Empresa empresa){
		if(empresa == null) 
			return "";
		if(StringUtils.isNotEmpty(empresa.getRazaosocial())) 
			return empresa.getRazaosocial();
		
		Empresa bean = load(empresa, "empresa.nome, empresa.razaosocial");
		if(bean == null) return null;
		return StringUtils.isNotEmpty(bean.getRazaosocial()) ? bean.getRazaosocial() : bean.getNome(); 
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findEmpresaIntegracaoWMS()
	*
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public List<Empresa> findEmpresaIntegracaoWMS() {
		return empresaDAO.findEmpresaIntegracaoWMS();
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForImportacaoFeedApplication(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public Empresa loadForImportacaoFeedApplication(Empresa empresa) {
		return empresaDAO.loadForImportacaoFeedApplication(empresa);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateConfiguracaoImportacao(Empresa empresa)
	*
	* @param empresa
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public void updateConfiguracaoImportacao(Empresa empresa) {
		empresaDAO.updateConfiguracaoImportacao(empresa);
	}
	
	public List<EmpresaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<EmpresaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<EmpresaW3producaoRESTModel> lista = new ArrayList<EmpresaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Empresa e : empresaDAO.findForW3Producao(whereIn))
				lista.add(new EmpresaW3producaoRESTModel(e));
		}
		
		return lista;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#findEmpresaWithContagerecialCentrocustoTransferencia()
	*
	* @return
	* @since 28/03/2016
	* @author Luiz Fernando
	*/
	public List<Empresa> findEmpresaWithContagerecialCentrocustoTransferencia() {
		return empresaDAO.findEmpresaWithContagerecialCentrocustoTransferencia();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadPrincipalWithContagerecialCentrocustoTransferencia()
	*
	* @return
	* @since 28/03/2016
	* @author Luiz Fernando
	*/
	public Empresa loadPrincipalWithContagerecialCentrocustoTransferencia() {
		return empresaDAO.loadPrincipalWithContagerecialCentrocustoTransferencia();
	}
	
	/**
	* M�todo retorna a empresa que tenha informa��es de transfer�ncia ou a empresa principal
	*
	* @see br.com.linkcom.sined.geral.service.EmpresaService#findEmpresaWithContagerecialCentrocustoTransferencia()
	* @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipalWithContagerecialCentrocustoTransferencia()
	*
	* @return
	* @since 28/03/2016
	* @author Luiz Fernando
	*/
	public Empresa getEmpresaForTransferencia(){
		List<Empresa> lista = findEmpresaWithContagerecialCentrocustoTransferencia();
		if(lista != null && lista.size() == 1){
			return lista.get(0);
		}
		return loadPrincipalWithContagerecialCentrocustoTransferencia();
	}
	
	public Empresa loadPrincipalWithContagerecialDevolucao() {
		return empresaDAO.loadPrincipalWithContagerecialDevolucao();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/04/2016
	 */
	public Empresa findByCnpjWSSOAP(Cnpj cnpj) {
		return empresaDAO.findByCnpjWSSOAP(cnpj);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome {@link Empresa}.getNome()
	 * @return {@link Empresa}
	 * @author Diogo souza
	 * @since 18/05/2020
	 */
	public Empresa findByEmpresaNome(String nome) {
		return empresaDAO.findByEmpresaNome(nome);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.EmpresaService#exibirIpiVenda(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since 18/06/2016
	* @author Luiz Fernando
	*/
	public boolean exibirIpiVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null) return false;
		return empresaDAO.exibirIpiVenda(empresa);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#existeEmpresaExibirIpi()
	*
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public boolean existeEmpresaExibirIpi() {
		return empresaDAO.existeEmpresaExibirIpi();
	}
	
	public boolean existeEmpresa(Empresa empresa) {
		return empresaDAO.existeEmpresa(empresa);
	}
		
	public void insertEmpresaCliente(Empresa empresa) {
		empresaDAO.insertEmpresaCliente(empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForInfNota(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since 06/10/2016
	* @author Luiz Fernando
	*/
	public Empresa loadForInfNota(Empresa empresa) {
		return empresaDAO.loadForInfNota(empresa);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForTemplate(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since 03/10/2016
	* @author Luiz Fernando
	*/
	public Empresa loadForTemplate(Empresa empresa) {
		return empresaDAO.loadForTemplate(empresa);
	}
	
	public Empresa loadForInfoContribuinte(Empresa empresa) {
		Empresa bean = empresaDAO.loadForInfoContribuinte(empresa);
		setInformacoesPropriedadeRural(bean);
		return bean;
	}
	
	public List<Empresa> findByListaCnpj(List<Cnpj> listaCnpj){
		if (listaCnpj==null || listaCnpj.isEmpty()){
			return new ArrayList<Empresa>();
		}
		return empresaDAO.findByListaCnpj(listaCnpj);
	}
	
	public List<Empresa> findAllContagerencialVenda() {
		return empresaDAO.findAllContagerencialVenda();
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.EmpresaDAO#loadForSageFiscal(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public Empresa loadForSageFiscal(Empresa empresa) {
		return empresaDAO.loadForSageFiscal(empresa);
	}

	/**
	* M�todo que busca as empresas para o combo
	*
	* @param listaEmpresa
	* @param ativo
	* @return
	* @since 24/01/2017
	* @author Luiz Fernando
	*/
	public List<Empresa> findForCombo(List<Empresa> listaEmpresa, boolean ativo) {
		return ativo ? findAtivos(listaEmpresa) : findAll();
	}
	
	/**
	* M�todo que retornas as empresa da conta ou empresas que o usu�rio tenha permiss�o
	*
	* @param bean
	* @return
	* @since 02/05/2017
	* @author Luiz Fernando
	*/
	public List<Empresa> getListaEmpresaByConta(Conta bean){
		List<Empresa> lista = new ArrayList<Empresa>();
		if(bean != null && bean.getCdconta() != null){
			Conta conta = contaService.loadConta(bean); 
			if(conta != null && SinedUtil.isListNotEmpty(conta.getListaContaempresa())){
				Iterator<Contaempresa> iterator = conta.getListaContaempresa().iterator();
				Contaempresa contaempresa;
				while(iterator.hasNext()){
					contaempresa = iterator.next();
					if(contaempresa.getEmpresa() != null && !lista.contains(contaempresa)){
						lista.add(contaempresa.getEmpresa());
					}
				}
			}
		}
		
		if(SinedUtil.isListEmpty(lista)){
			lista = findByUsuario();
		}
		
		return lista;
	}
	
	public List<Empresa> getListaEmpresaByConta(WebRequestContext request, Conta bean){
		List<Empresa> lista = getListaEmpresaByConta(bean);
		List<Empresa> listaAjustada = new ArrayList<Empresa>();
		if(SinedUtil.isListNotEmpty(lista)){
			String whereInEmpresa = request.getParameter("empresaWhereIn");
			if(StringUtils.isNotBlank(whereInEmpresa)){
				String[] ids = whereInEmpresa.split(",");
				int index;
				for(String id : ids){
					index = lista.indexOf(new Empresa(Integer.parseInt(id)));
					if(index != -1 && !listaAjustada.contains(lista.get(index))){
						listaAjustada.add(lista.get(index));
					}
				}
			}else {
				listaAjustada = lista;
			}
		}
		
		return listaAjustada;
	}
	
	/**
	* M�todo que retorna as empresas do local de armazenagem ou empresas ativas
	*
	* @param localarmazenagem
	* @param empresa
	* @return
	* @since 10/04/2017
	* @author Luiz Fernando
	*/
	public List<Empresa> findAtivoByLocalarmazenagem(Localarmazenagem localarmazenagem, Empresa empresa) {
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if(localarmazenagem != null){
			List<Localarmazenagemempresa> listaLocalarmazenagemempresa = localarmazenagemempresaService.findByLocalarmazenagem(localarmazenagem);
			if(SinedUtil.isListNotEmpty(listaLocalarmazenagemempresa)){
				for(Localarmazenagemempresa localarmazenagemempresa : listaLocalarmazenagemempresa){
					if(localarmazenagemempresa.getEmpresa() != null && Boolean.TRUE.equals(localarmazenagemempresa.getEmpresa().getAtivo())){
						listaEmpresa.add(localarmazenagemempresa.getEmpresa());
					}
				}
				if(empresa != null && !listaEmpresa.contains(empresa)){
					if(StringUtils.isBlank(empresa.getNomefantasia())){
						try {
							empresa.setNomefantasia(empresaService.load(empresa, "empresa.cdpessoa, empresa.nomefantasia").getNomefantasia());
						} catch (Exception e) {}
					}
					listaEmpresa.add(empresa);
					Collections.sort(listaEmpresa, new Comparator<Empresa>(){
						public int compare(Empresa e1, Empresa e2) {
							try {
								return e1.getNomefantasia().compareTo(e2.getNomefantasia());
							} catch (Exception e) {
								return -1;
							}
						}
					});
					
				}
			}
		}
		if(SinedUtil.isListEmpty(listaEmpresa)){
			listaEmpresa = empresaService.findAtivos(empresa);
		}
		
		return listaEmpresa;
	}
	
	public List<Empresa> findByCampo(String campo, Object valorCampo){
		return empresaDAO.findByCampo(campo, valorCampo);
	}

	public Empresa loadForBaixa(Empresa empresa) {
		return empresaDAO.loadForBaixa(empresa);
	}

	public void setInfoPessoaconfiguracaoEntrada(Empresa empresa) {
		if(empresa.getPessoaconfiguracao() != null){
			empresa.setPessoaconfiguracao_transportador(empresa.getPessoaconfiguracao().getTransportador());
		}
	}
	public Empresa loadWithModeloorcamentoRTF(Empresa empresa){
		return empresaDAO.loadWithModeloorcamentoRTF(empresa);
	}

	public Empresa loadWithLocalproducao(Empresa empresa) {
		return empresaDAO.loadWithLocalproducao(empresa);
	}
	public List<Empresa> findEmpresasConfiguradasNfe(Empresa empresa) {
		return empresaDAO.findEmpresasConfiguradasNfe(empresa);
	}
	
	public Empresa loadWithProprietario(Empresa empresa) {
		return empresaDAO.loadWithProprietario(empresa);
	}
	
	public void setInformacoesPropriedadeRural(Empresa bean) {
		setInformacoesPropriedadeRural(bean, false);
	}
	
	public void setInformacoesPropriedadeRural(Empresa bean, boolean verificarListaPreenchida) {
		if(bean != null && bean.getCdpessoa() != null){
			if(!verificarListaPreenchida || bean.getListaEmpresaproprietario() == null){
				Empresa empresa = loadWithProprietario(bean);
				if(empresa != null){
					bean.setPropriedadeRural(empresa.getPropriedadeRural());
					bean.setListaEmpresaproprietario(empresa.getListaEmpresaproprietario());
					if(bean.getListaEmpresaproprietario() == null){
						bean.setListaEmpresaproprietario(new ArrayList<Empresaproprietario>());
					}
				}
			}
		}
	}
	
	public List<Empresa> findForCriarContabilEmpresa(Material material) {
		return empresaDAO.findForCriarContabilEmpresa(material);
	}
	public List<Empresa> findFazendas() {
		return empresaDAO.findFazendas();
	}
	
	public List<GenericBean> findForWSVenda(){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findAtivos());
	}
	
	public boolean exibirIcmsVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return empresaDAO.exibirIcmsVenda(empresa); 
	}
	
	public boolean exibirDifalVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return empresaDAO.exibirDifalVenda(empresa);
	}
	
	public boolean exibirCalculoImpostosVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return empresaDAO.exibirIpiVenda(empresa) || empresaDAO.exibirIcmsVenda(empresa) || empresaDAO.exibirDifalVenda(empresa);
	}

	public boolean existeEmpresaExibirIcms() {
		return empresaDAO.existeEmpresaExibirIcms(); 
	}
	public boolean existeEmpresaExibirDifal() {
		return empresaDAO.existeEmpresaExibirDifal(); 
	}
	
	public Integer carregaProximoIdentificadorCarregamento(Empresa empresa){
		return empresaDAO.carregaProximoIdentificadorCarregamento(empresa);
	}
	
	public void updateProximoIdentificadorCarregamento(Empresa empresa, Integer identificador) {
		empresaDAO.updateProximoIdentificadorCarregamento(empresa, identificador);
	}

	public Empresa loadEmpresaForEntradaeSaida(Integer cdEmpresa){
		return empresaDAO.loadEmpresaForEntradaeSaida(cdEmpresa);
	}

	public List<Empresa> findWithConfiguracaoCorreios(){
		return empresaDAO.findWithConfiguracaoCorreios();
	}
}