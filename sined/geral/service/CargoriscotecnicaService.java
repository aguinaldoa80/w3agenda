package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargoriscotecnica;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargoriscotecnicaService extends GenericService<Cargoriscotecnica> {	
	
	/* singleton */
	private static CargoriscotecnicaService instance;
	public static CargoriscotecnicaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoriscotecnicaService.class);
		}
		return instance;
	}
	
}
