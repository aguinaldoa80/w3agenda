package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Emporiumfiscaisgerais;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumfiscaisgeraisService extends GenericService<Emporiumfiscaisgerais> {
	
	/* singleton */
	private static EmporiumfiscaisgeraisService instance;
	public static EmporiumfiscaisgeraisService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EmporiumfiscaisgeraisService.class);
		}
		return instance;
	}
	
}