package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialrede;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominioemailusuario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Servicobanco;
import br.com.linkcom.sined.geral.bean.Servicoftp;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servicoweb;
import br.com.linkcom.sined.geral.dao.ContratomaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratomaterialService extends GenericService<Contratomaterial> {

	private ContratomaterialDAO contratomaterialDAO;
	private MaterialtipoService materialtipoService;
	private ServicoftpService servicoftpService;
	private ServicowebService servicowebService;
	private DominioemailusuarioService dominioemailusuarioService;
	private DominioService dominioService;
	private ContratomaterialredeService contratomaterialredeService;
	private ServicobancoService servicobancoService;
	
	public void setServicobancoService(ServicobancoService servicobancoService) {
		this.servicobancoService = servicobancoService;
	}
	public void setContratomaterialredeService(
			ContratomaterialredeService contratomaterialredeService) {
		this.contratomaterialredeService = contratomaterialredeService;
	}
	public void setDominioService(DominioService dominioService) {
		this.dominioService = dominioService;
	}
	public void setDominioemailusuarioService(
			DominioemailusuarioService dominioemailusuarioService) {
		this.dominioemailusuarioService = dominioemailusuarioService;
	}
	public void setServicowebService(ServicowebService servicowebService) {
		this.servicowebService = servicowebService;
	}
	public void setServicoftpService(ServicoftpService servicoftpService) {
		this.servicoftpService = servicoftpService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setContratomaterialDAO(ContratomaterialDAO contratomaterialDAO) {
		this.contratomaterialDAO = contratomaterialDAO;
	}
	
	/**
	 * Retorna uma lista de contratos de material de acordo com o cliente
	 * @see br.com.linkcom.sined.geral.service.ContratomaterialService#findByCliente
	 * @param cliente
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Contratomaterial> findByCliente(Cliente cliente){
		return contratomaterialDAO.findByCliente(cliente);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#findByContrato
	 * 
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratomaterial> findByContrato(Contrato contrato){
		return contratomaterialDAO.findByContrato(contrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#findByContrato(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2012
	 */
	public List<Contratomaterial> findByContrato(String whereIn){
		return contratomaterialDAO.findByContrato(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#findByContratoProducao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/02/2013
	 */
	public List<Contratomaterial> findByContratoProducao(String whereIn){
		return contratomaterialDAO.findByContratoProducao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#findForRTFByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contratomaterial> findForRTFByContrato(Contrato contrato){
		return contratomaterialDAO.findForRTFByContrato(contrato);
	}
	
	/**
	 * Retorna o cliente de determinado contrato de material
	 * @param contratomaterial
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Cliente findCliente(Contratomaterial contratomaterial){
		return contratomaterialDAO.findCliente(contratomaterial);
	}
	
	public List<Contratomaterial> findByClienteTipoServico(Cliente cliente, Servicoservidortipo... servicoservidortipo){
		
		List<Materialtipo> listaTipoMaterial = materialtipoService.findByServicoservidortipo(servicoservidortipo);
		
		return contratomaterialDAO.findByClienteTipoServico(cliente, listaTipoMaterial);
	}
	
	public List<Contratomaterial> findForServicoftp(Cliente cliente){
		return this.findByClienteTipoServico(cliente, Servicoservidortipo.FTP_OBJ);
	}
	
	public List<Contratomaterial> findForServicoftpAberto(Cliente cliente, Integer cdservicoftp){
		List<Contratomaterial> lista = this.findForServicoftp(cliente);
		
		Contratomaterial cm = null;
		Servicoftp ld;
		if(cdservicoftp != null){
			ld = servicoftpService.load(new Servicoftp(cdservicoftp), "servicoftp.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = servicoftpService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForServicoftpAberto(Cliente cliente){
		return this.findForServicoftpAberto(cliente, null);
	}
	
	public List<Contratomaterial> findForServicoweb(Cliente cliente){
		return this.findByClienteTipoServico(cliente, Servicoservidortipo.WEB_OBJ);
	}
	
	public List<Contratomaterial> findForServicowebAberto(Cliente cliente, Integer cdservicoweb){
		List<Contratomaterial> lista = this.findForServicoweb(cliente);

		Contratomaterial cm = null;
		Servicoweb ld;
		if(cdservicoweb != null){
			ld = servicowebService.load(new Servicoweb(cdservicoweb), "servicoweb.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = servicowebService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForServicowebAberto(Cliente cliente){
		return this.findForServicowebAberto(cliente, null);
	}
	
	public List<Contratomaterial> findForDominioemailusuario(Cliente cliente){
		return this.findByClienteTipoServico(cliente, 	Servicoservidortipo.CAIXAPOSTAL_OBJ, 
														Servicoservidortipo.SMTP_OBJ, 
														Servicoservidortipo.EMAIL_OBJ, 
														Servicoservidortipo.SMTPAV_OBJ);
	}
	
	public List<Contratomaterial> findForDominioemailusuarioAberto(Cliente cliente, Integer cddominioemailusuario){
		List<Contratomaterial> lista = this.findForDominioemailusuario(cliente);
		
		Contratomaterial cm = null;
		Dominioemailusuario ld;
		if(cddominioemailusuario != null){
			ld = dominioemailusuarioService.load(new Dominioemailusuario(cddominioemailusuario), "dominioemailusuario.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = dominioemailusuarioService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForDominioemailusuarioAberto(Cliente cliente){
		return this.findForDominioemailusuarioAberto(cliente,null);
	}
	
	public List<Contratomaterial> findForDominio(Cliente cliente){
		return this.findByClienteTipoServico(cliente, Servicoservidortipo.DOMINIO_OBJ);
	}
	
	public List<Contratomaterial> findForDominioAberto(Cliente cliente, Integer cddominio){
		List<Contratomaterial> lista = this.findForDominio(cliente);

		Contratomaterial cm = null;
		Dominio ld;
		if(cddominio != null){
			ld = dominioService.load(new Dominio(cddominio), "dominio.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = dominioService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForDominioAberto(Cliente cliente){
		return this.findForDominioAberto(cliente, null);
	}
	
	
	public List<Contratomaterial> findForContratomaterialrede(Cliente cliente){
		return this.findByClienteTipoServico(cliente, 	Servicoservidortipo.DHCP_OBJ, 
														Servicoservidortipo.ARP_OBJ, 
														Servicoservidortipo.DHCP_ARP_OBJ, 
														Servicoservidortipo.AUTENTICACAO_RADIUS_OBJ, 
														Servicoservidortipo.DHCP_RADIUS_OBJ, 
														Servicoservidortipo.FIREWALL_OBJ, 
														Servicoservidortipo.NAT_OBJ, 
														Servicoservidortipo.BANDA_OBJ);
		
	}
	
	public List<Contratomaterial> findForContratomaterialredeAberto(Cliente cliente, Integer cdcontratomaterialrede){
		List<Contratomaterial> lista = this.findForContratomaterialrede(cliente);
		
		Contratomaterial cm = null;
		Contratomaterialrede ld;
		if(cdcontratomaterialrede != null){
			ld = contratomaterialredeService.load(new Contratomaterialrede(cdcontratomaterialrede), "contratomaterialrede.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = contratomaterialredeService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForContratomaterialredeAberto(Cliente cliente){
		return this.findForContratomaterialredeAberto(cliente, null);
	}
	
	public List<Contratomaterial> findForServicobanco(Cliente cliente){
		return this.findByClienteTipoServico(cliente, Servicoservidortipo.BANCO_DADOS_OBJ);
	}
	
	public List<Contratomaterial> findForServicobancoAberto(Cliente cliente, Integer cdservicobanco){
		List<Contratomaterial> lista = this.findForServicobanco(cliente);
		
		Contratomaterial cm = null;
		Servicobanco ld;
		if(cdservicobanco != null){
			ld = servicobancoService.load(new Servicobanco(cdservicobanco), "servicobanco.contratomaterial");
			cm = ld != null ? ld.getContratomaterial() : null;
		}
		
		Contratomaterial contratomaterial;
		int qtdeReg;
		for (int i = 0; i < lista.size(); i++) {
			contratomaterial = lista.get(i);
			if(cm != null && cm.getCdcontratomaterial().equals(contratomaterial.getCdcontratomaterial())){
				continue;
			}
			
			qtdeReg = servicobancoService.countContratomaterial(null, contratomaterial);
			
			if(qtdeReg >= contratomaterial.getQtde()){
				lista.remove(i);
				i--;
			}
		}
		return lista;
	}
	
	public List<Contratomaterial> findForServicobancoAberto(Cliente cliente){
		return this.findForServicobancoAberto(cliente, null);
	}
	
	/**
	 * M�todo que retorna a lista de contratos materiais que ser� usada para a montagem do combo, 
	 * na tela de entrada de contrato materiais rede.
	 * 
	 * Al�m do filtro dos tipos de materiais, somente ser�o exibidos os contratos materiais que n�o est�o vinculados. 
	 * @param cliente
	 * @param materialtipos
	 * @param cdContratoMaterialRedeCod
	 * @return
	**/
	public List<Contratomaterial> findForEntrada(Cliente cliente){
		return findForEntrada(cliente,null);
	}
	
	/**
	 * M�todo que retorna a lista de contratos materiais que ser� usada para a montagem do combo, 
	 * na tela de entrada de contrato materiais rede.
	 * 
	 * Al�m do filtro dos tipos de materiais, somente ser�o exibidos os contratos materiais que n�o est�o vinculados. 
	 * @param cliente
	 * @param materialtipos
	 * @param cdContratoMaterialRedeCod
	 * @return
	**/
	public List<Contratomaterial> findForEntrada(Cliente cliente, Integer cdContratoMaterialRedeCod){
		
		List<Materialtipo> listaTipoMaterial = materialtipoService.findByServicoservidortipo(Servicoservidortipo.DHCP_OBJ, 
				Servicoservidortipo.ARP_OBJ, 
				Servicoservidortipo.DHCP_ARP_OBJ, 
				Servicoservidortipo.AUTENTICACAO_RADIUS_OBJ, 
				Servicoservidortipo.DHCP_RADIUS_OBJ, 
				Servicoservidortipo.FIREWALL_OBJ, 
				Servicoservidortipo.NAT_OBJ, 
				Servicoservidortipo.BANDA_OBJ);
		
		return contratomaterialDAO.findForCombo(cliente, listaTipoMaterial, cdContratoMaterialRedeCod);
		
	}
	
	public Contratomaterial loadContratomaterial(Contratomaterial contratomaterial){
		return contratomaterialDAO.loadContratomaterial(contratomaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param contratomaterial
	 * @return
	 * @author Marden Silva
	 */	
	public Contratomaterial loadForEmail(Contratomaterial contratomaterial){
		return contratomaterialDAO.loadForEmail(contratomaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#updatePatrimonioitem(Contratomaterial contratomaterial, Patrimonioitem patrimonioitem)
	 *
	 * @param contratomaterial
	 * @param patrimonioitem
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public void updatePatrimonioitem(Contratomaterial contratomaterial, Patrimonioitem patrimonioitem) {
		contratomaterialDAO.updatePatrimonioitem(contratomaterial, patrimonioitem);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialDAO#updateMaterial(Contratomaterial contratomaterial, Material material)
	 *
	 * @param contratomaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public void updateMaterial(Contratomaterial contratomaterial, Material material) {
		contratomaterialDAO.updateMaterial(contratomaterial, material);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contratomaterial
	 * @param valorunitario
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public void updateValorunitario(Contratomaterial contratomaterial, Double valorunitario) {
		contratomaterialDAO.updateValorunitario(contratomaterial, valorunitario);
	}

	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contratomaterial
	 * @param valorfechado
	 * @author Rodrigo Freitas
	 * @since 17/01/2017
	 */
	public void updateValorfechado(Contratomaterial contratomaterial, Double valorfechado) {
		contratomaterialDAO.updateValorfechado(contratomaterial, valorfechado);
	}
	
	public Contratomaterial loadByRomaneioitem(Romaneioitem romaneioitem, boolean byContratofechamento){
		if(romaneioitem == null || romaneioitem.getCdromaneioitem() == null){
			return null;
		}
		return contratomaterialDAO.loadByRomaneioitem(romaneioitem, byContratofechamento);
	}
	
	public void updateCamposRetornoContrato(Contratomaterial contratomaterial) {
		contratomaterialDAO.updateCamposRetornoContrato(contratomaterial);
	}
}
