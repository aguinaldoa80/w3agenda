package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicoacao;
import br.com.linkcom.sined.geral.bean.Agendamentoservicohistorico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicohistoricopapel;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.AgendamentoservicohistoricoDAO;

public class AgendamentoservicohistoricoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Agendamentoservicohistorico> {

	private UsuariopapelService usuariopapelService;
	private AgendamentoservicohistoricoDAO agendamentoservicohistoricoDAO;
	private AgendamentoservicoService agendamentoservicoService;
	
	public void setUsuariopapelService(UsuariopapelService usuariopapelService) {
		this.usuariopapelService = usuariopapelService;
	}
	public void setAgendamentoservicohistoricoDAO(
			AgendamentoservicohistoricoDAO agendamentoservicohistoricoDAO) {
		this.agendamentoservicohistoricoDAO = agendamentoservicohistoricoDAO;
	}
	public void setAgendamentoservicoService(
			AgendamentoservicoService agendamentoservicoService) {
		this.agendamentoservicoService = agendamentoservicoService;
	}
	
	/**
	 *  M�todo que retorna os hist�ricos do agendamento servi�o que o usu�rio logado tem permiss�o de ver
	 * 
	 * @param agendamentoservico
	 * @param usuarioLogado
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservicohistorico> findHistoricosDoAgendamentoPermissaoUsuario(Agendamentoservico agendamentoservico, Usuario usuarioLogado) {
		List<Papel> listaPapeis = new ArrayList<Papel>();
		if(usuarioLogado.getListaPapel() == null || usuarioLogado.getListaPapel().isEmpty())
			listaPapeis = usuariopapelService.carregaPapel(usuarioLogado);
		else
			listaPapeis = usuarioLogado.getListaPapel();
			
		return agendamentoservicohistoricoDAO.findHistoricosDoAgendamentoPermissaoUsuario(agendamentoservico, listaPapeis);
	}
	
	/**
	 * M�todo que salva os hist�ricos para os agendamentos servi�o hist�ricos
	 * 
	 * @param agendamentoservicohistorico
	 * @author Tom�s Rabelo
	 * @param listaAgendamentos 
	 */
	public void saveHistoricos(final Agendamentoservicohistorico agendamentoservicohistorico, List<Agendamentoservico> listaAgendamentos) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String[] where = agendamentoservicohistorico.getWhereIn().split(",");
				
				if(agendamentoservicohistorico.getAgendamentoservicocancela() != null &&
						agendamentoservicohistorico.getAgendamentoservicocancela().getNome() != null){
					agendamentoservicohistorico.setObservacao(agendamentoservicohistorico.getAgendamentoservicocancela().getNome() + 
							(agendamentoservicohistorico.getObservacao() != null && !agendamentoservicohistorico.getObservacao().equals("") ? " - " + agendamentoservicohistorico.getObservacao() : ""));
				}
				
				//Cria um hist�rico para cara agendamento servi�o selecionado
				for (String cds : where){
					Agendamentoservico agendamentoservico = new Agendamentoservico(Integer.valueOf(cds));
					List<Agendamentoservicohistoricopapel> lista = new ArrayList<Agendamentoservicohistoricopapel>();
					if(agendamentoservicohistorico.getListaagendamentohistoricopapel() != null && !agendamentoservicohistorico.getListaagendamentohistoricopapel().isEmpty()){
						for (Agendamentoservicohistoricopapel agendamentoservicohistoricopapel : agendamentoservicohistorico.getListaagendamentohistoricopapel()) {
							lista.add(new Agendamentoservicohistoricopapel(agendamentoservicohistoricopapel.getPapel()));
						}
					}
					saveOrUpdateNoUseTransaction(new Agendamentoservicohistorico(agendamentoservicohistorico.getObservacao(), agendamentoservico, agendamentoservicohistorico.getAgendamentoservicoacao(), lista));
				}
				
				if(agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.CANCELADO))
					agendamentoservicoService.cancelar(agendamentoservicohistorico.getWhereIn(), agendamentoservicohistorico.getAgendamentoservicocancela());
				else if(agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.REALIZADO))
					agendamentoservicoService.realizado(agendamentoservicohistorico.getWhereIn());
				
				return status;
			}
		});
		
		agendamentoservicoService.callProcedureAtualizaSessaoAgendamentoServico(listaAgendamentos);
	}

	
}
