package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.dao.OrdemcomprahistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcomprahistoricoService extends GenericService<Ordemcomprahistorico>{

	private OrdemcomprahistoricoDAO ordemcomprahistoricoDAO;
	
	public void setOrdemcomprahistoricoDAO(
			OrdemcomprahistoricoDAO ordemcomprahistoricoDAO) {
		this.ordemcomprahistoricoDAO = ordemcomprahistoricoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcomprahistoricoDAO#findForVisualizacao
	 * @param ordemcomprahistorico
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcomprahistorico findForVisualizacao(Ordemcomprahistorico ordemcomprahistorico) {
		return ordemcomprahistoricoDAO.findForVisualizacao(ordemcomprahistorico);
	}

	public Ordemcomprahistorico getUsuarioLastAction(Ordemcompra ordemcompra, Ordemcompraacao ordemcompraacao) {
		return ordemcomprahistoricoDAO.getUsuarioLastAction(ordemcompra, ordemcompraacao);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 19/03/2012
	 * @param ordemcompra
	 * @return
	 */
	public List<Ordemcomprahistorico> getListaOrdemCompraHistoricoForReport(Ordemcompra ordemcompra){
		return ordemcomprahistoricoDAO.getListaOrdemCompraHistoricoForReport(ordemcompra);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcomprahistoricoDAO#getPrimeiroHistorico(Ordemcompra ordemcompra)
	 *
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/02/2013
	 */
	public Ordemcomprahistorico getPrimeiroHistorico(Ordemcompra ordemcompra) {
		return ordemcomprahistoricoDAO.getPrimeiroHistorico(ordemcompra);
	}

	/**
	 * M�todo que cria o hist�rico na ordem de compra ap�s criar antecipa��o
	 *
	 * @param documento
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public void criaHistoricoAntecipacao(Documento documento) {
		if(documento != null && documento.getCddocumento() != null && documento.getWhereInOrdemcompra() != null &&
				!"".equals(documento.getWhereInOrdemcompra())){
			
			try {
				StringBuilder obs = new StringBuilder();
				obs.append("<a href=\"javascript:visualizarContapagar("+ documento.getCddocumento() +");\">"+ documento.getCddocumento() +"</a>");
				
				if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
					String obsDoc;
					for(Documento doc : documento.getListaDocumento()){
						obsDoc = "<a href=\"javascript:visualizarContapagar("+ doc.getCddocumento() +");\">"+ doc.getCddocumento() +"</a>";
						if(!obs.toString().contains(obsDoc)){
							if(!obs.toString().equals("")) obs.append(",");
							obs.append(obsDoc);
						}
					}
				}
				
				if(!obs.toString().equals("")){
					Ordemcomprahistorico ordemcomprahistorico = new Ordemcomprahistorico();
					ordemcomprahistorico.setOrdemcompra(new Ordemcompra(Integer.parseInt(documento.getWhereInOrdemcompra())));
					ordemcomprahistorico.setObservacao("Adiantamento: " + obs.toString());
					ordemcomprahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					ordemcomprahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					this.saveOrUpdate(ordemcomprahistorico);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
