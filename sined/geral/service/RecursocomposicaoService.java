package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.collections.keyvalue.MultiKey;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Dependenciacargocomposicao;
import br.com.linkcom.sined.geral.bean.Dependenciafaixacomposicao;
import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Formulacomposicao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Periodoorcamento;
import br.com.linkcom.sined.geral.bean.Periodoorcamentocargo;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.geral.bean.Referenciacalculo;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorg;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.bean.Tipoitemcomposicao;
import br.com.linkcom.sined.geral.bean.Tipoocorrencia;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoGeralFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RecursocomposicaoService extends GenericService<Recursocomposicao>{
	
	public final static String SESSION_FILTRO_RELATORIO_RECGERAL = "session_filtro_relatorio_recgeral";
	public final static String SESSION_FILTRO_RELATORIO_RECGERAL_SINTETICO = "session_filtro_relatorio_recgeralsintetico";
	
	private RecursocomposicaoDAO recursocomposicaoDAO;
	private TarefaorcamentorgService tarefaorcamentorgService;
	private ComposicaoorcamentoService composicaoorcamentoService;
	private CargomaterialsegurancaService cargomaterialsegurancaService;
	private PlanejamentoService planejamentoService;
	private ContagerencialService contagerencialService;
	private MaterialService materialService;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private ParametrogeralService parametrogeralService;

	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPeriodoorcamentocargoService(
			PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setRecursocomposicaoDAO(RecursocomposicaoDAO recursocomposicaoDAO) {
		this.recursocomposicaoDAO = recursocomposicaoDAO;
	}
	
	public void setTarefaorcamentorgService(TarefaorcamentorgService tarefaorcamentorgService) {
		this.tarefaorcamentorgService = tarefaorcamentorgService;
	}
	
	public void setComposicaoorcamentoService(ComposicaoorcamentoService composicaoorcamentoService) {
		this.composicaoorcamentoService = composicaoorcamentoService;
	}
	
	public void setCargomaterialsegurancaService(CargomaterialsegurancaService cargomaterialsegurancaService) {
		this.cargomaterialsegurancaService = cargomaterialsegurancaService;
	}

	/**
	 * Retorna uma lista de recursos de uma determinada composi��o de or�amento
	 *
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findByComposicaoOrcamentoForFlex
	 * @param composicaoorcamento
	 * @return lista de Recursocomposicao
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Recursocomposicao> findByComposicaoOrcamentoForFlex(Composicaoorcamento composicaoorcamento) {
		return recursocomposicaoDAO.findByComposicaoOrcamentoForFlex(composicaoorcamento);
	}
	
	/**
	 * Busca os recursos gerais diretos de um or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findRecursoDiretoByOrcamento(Orcamento)
	 *
	 * @param orcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoDiretoByOrcamento(Orcamento orcamento) {
		return recursocomposicaoDAO.findRecursoDiretoByOrcamento(orcamento);
	}
	
	/**
	 * Busca os recursos gerais indiretos de um or�amento
	 *
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findRecursoIndiretoByOrcamento(Orcamento)
	 *
	 * @param orcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoIndiretoByOrcamento(Orcamento orcamento) {
		return recursocomposicaoDAO.findRecursoIndiretoByOrcamento(orcamento);
	}	
	
	/**
	 * Busca os recursos gerais do or�amento a partir de um filtro
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findForListagemFlex(OrcamentoRecursoGeralFiltro)
	 * 
	 * @param filtro
	 * @return List<Recursocomposicao>
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findForListagemFlex(OrcamentoRecursoGeralFiltro filtro) {	
		List<Recursocomposicao> listaRecursoComposicaoFiltrada = new ArrayList<Recursocomposicao>();
		Map<MultiKey, Recursocomposicao> mapaRecursoMaterial = new HashMap<MultiKey, Recursocomposicao>();
		Map<MultiKey, Recursocomposicao> mapaRecursoOutro = new HashMap<MultiKey, Recursocomposicao>();
		Recursocomposicao recursoComposicaoMapa;
		MultiKey keyMapaMaterial;
		MultiKey keyMapaOutro;
		Set<MultiKey> keySetMapaMaterial;
		Set<MultiKey> keySetMapaOutro;
		
		List<Recursocomposicao> listaRecursoComposicao = recursocomposicaoDAO.findForListagemFlex(filtro);
		
		for (Recursocomposicao recursocomposicao : listaRecursoComposicao) {
			if (recursocomposicao.getMaterial() != null) {
				keyMapaMaterial = new MultiKey(new Object[]{recursocomposicao.getMaterial(), recursocomposicao.getNumocorrencia(), recursocomposicao.getReferenciacalculo()});
				recursoComposicaoMapa = mapaRecursoMaterial.get(keyMapaMaterial);
				
				//Verifica se j� existe o material no Mapa					
				if (recursoComposicaoMapa == null) {
					recursoComposicaoMapa = new Recursocomposicao();
					recursoComposicaoMapa.setNome(recursocomposicao.getNome());
					recursoComposicaoMapa.setMaterial(recursocomposicao.getMaterial());
					recursoComposicaoMapa.setUnidademedida(recursocomposicao.getUnidademedida());
					recursoComposicaoMapa.setQuantidadecalculada(recursocomposicao.getQuantidadecalculada());
					recursoComposicaoMapa.setNumocorrencia(recursocomposicao.getNumocorrencia());
					recursoComposicaoMapa.setCustounitario(recursocomposicao.getCustounitario());
					recursoComposicaoMapa.setReferenciacalculo(recursocomposicao.getReferenciacalculo());
				}
				else {
					recursoComposicaoMapa.setQuantidadecalculada(recursoComposicaoMapa.getQuantidadecalculada() + recursocomposicao.getQuantidadecalculada());
				}
				mapaRecursoMaterial.put(keyMapaMaterial,recursoComposicaoMapa);
			}
			else {
				keyMapaOutro = new MultiKey(new Object[]{recursocomposicao.getNome(), recursocomposicao.getUnidademedida(), recursocomposicao.getNumocorrencia(), recursocomposicao.getReferenciacalculo()});
				recursoComposicaoMapa = mapaRecursoOutro.get(keyMapaOutro);
				
				//Verifica se j� existe o item (nome/unidade medida) no Mapa					
				if (recursoComposicaoMapa == null) {
					recursoComposicaoMapa = new Recursocomposicao();
					recursoComposicaoMapa.setNome(recursocomposicao.getNome());
					recursoComposicaoMapa.setUnidademedida(recursocomposicao.getUnidademedida());
					recursoComposicaoMapa.setQuantidadecalculada(recursocomposicao.getQuantidadecalculada());
					recursoComposicaoMapa.setNumocorrencia(recursocomposicao.getNumocorrencia());
					recursoComposicaoMapa.setCustounitario(recursocomposicao.getCustounitario());
					recursoComposicaoMapa.setReferenciacalculo(recursocomposicao.getReferenciacalculo());
				}
				else {
					recursoComposicaoMapa.setQuantidadecalculada(recursoComposicaoMapa.getQuantidadecalculada() + recursocomposicao.getQuantidadecalculada());
				}
				mapaRecursoOutro.put(keyMapaOutro,recursoComposicaoMapa);					
			}
		}
		
		keySetMapaMaterial = mapaRecursoMaterial.keySet();
		for (MultiKey keyMapaMaterial2 : keySetMapaMaterial) {
			recursoComposicaoMapa = mapaRecursoMaterial.get(keyMapaMaterial2);
			listaRecursoComposicaoFiltrada.add(recursoComposicaoMapa);
		}
		
		keySetMapaOutro = mapaRecursoOutro.keySet();
		for (MultiKey keyMapaOutro2 : keySetMapaOutro) {
			recursoComposicaoMapa = mapaRecursoOutro.get(keyMapaOutro2);
			listaRecursoComposicaoFiltrada.add(recursoComposicaoMapa);
		}
		
		//Ordena os elementos da lista pelo nome
		Collections.sort(listaRecursoComposicaoFiltrada,new Comparator<Recursocomposicao>() {		
			public int compare(Recursocomposicao o1, Recursocomposicao o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});		
		
		return listaRecursoComposicaoFiltrada;
	}
	
	/**
	 * Atualiza o custo unit�rio do material ou item presente no recursocomposicao passado como par�metro
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#atualizaCustoUnitarioForFlex(Recursocomposicao)
	 *
	 * @param recursocomposicao
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaCustoUnitarioForFlex(Recursocomposicao recursocomposicao) {
		recursocomposicaoDAO.atualizaCustoUnitarioForFlex(recursocomposicao);
	}
	
	/**
	 * Atualiza os recursos gerais diretos, indiretos e os materiais de seguran�a de um determinado or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#atualizaOrcamentoRecursoGeralDiretoFlex(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#atualizaOrcamentoRecursoGeralIndiretoFlex(Orcamento)
	 *
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargo
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoRecursoGeralFlex(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		//Atualiza a lista dos recursos gerais relacionados �s tarefas
		atualizaOrcamentoRecursoGeralDiretoFlex(orcamento);

		//Atualiza a lista dos recursos gerais n�o relacionados �s tarefas		
		atualizaOrcamentoRecursoGeralIndiretoFlex(orcamento, null, listaPeriodoOrcamentoCargo);
		
		if (Boolean.TRUE.equals(orcamento.getCalcularmaterialseguranca())) {
			//Atualiza a lista dos materiais de seguran�a do or�amento		
			atualizaOrcamentoMaterialSegurancaFlex(orcamento, listaPeriodoOrcamentoCargo);		
		}
		else {
			//Apaga a composi��o de material de seguran�a, caso exista
			List<Composicaoorcamento> listaComposicaoOrcamentoEPI = composicaoorcamentoService.findByOrcamentoForFlex(orcamento, true);
			if (listaComposicaoOrcamentoEPI != null) {
				for (Composicaoorcamento composicaoOrcamento : listaComposicaoOrcamentoEPI) {
					composicaoorcamentoService.delete(composicaoOrcamento);
				}
			}
		}
	}
	
	/**
	 * Atualiza os recursos gerais diretos de um determinado or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#findForListagemFlex(OrcamentoRecursoGeralFiltro)
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentorgService#findByOrcamentoFlex(Orcamento)
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#saveListaRecursoComposicaoInTransaction(List, List)
	 *
	 * @param orcamento
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoRecursoGeralDiretoFlex(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		List<Recursocomposicao> listaRecursoComposicaoForUpdate = new ArrayList<Recursocomposicao>();
		List<Recursocomposicao> listaRecursoComposicaoForDelete = new ArrayList<Recursocomposicao>();
		
		MultiKey keyMapaMaterial;
		Map<MultiKey, Recursocomposicao> mapaRecursoGeral = new HashMap<MultiKey, Recursocomposicao>();
		Recursocomposicao recursoComposicaoMapa;
		Recursocomposicao recursoGeral;
		
		boolean encontrouRecurso;
		int i;
		int indexRecurso;
		
		//Busca os recursos gerais diretos do or�amento
		List<Recursocomposicao> listaRecursoGeralOrcamento = recursocomposicaoDAO.findRecursoDiretoByOrcamento(orcamento);
		
		//Busca os recursos gerais vinculados �s tarefas
		List<Tarefaorcamentorg> listaRecursoGeralTarefa = tarefaorcamentorgService.findByOrcamentoFlex(orcamento);
		
		
		if (listaRecursoGeralTarefa != null) {
			//Para cada recurso geral da tarefa
			for (Tarefaorcamentorg recursoGeralTarefa : listaRecursoGeralTarefa) {
				
				recursoGeral = new Recursocomposicao();
				recursoGeral.setOrcamento(orcamento);
				recursoGeral.setTipoitemcomposicao(Tipoitemcomposicao.MATERIAL);
				recursoGeral.setTipodependenciarecurso(Tipodependenciarecurso.SEM_DEPENDENCIA);
				recursoGeral.setTiporelacaorecurso(Tiporelacaorecurso.VALOR_UNICO);
				recursoGeral.setTipoocorrencia(Tipoocorrencia.INFORMAR);
				recursoGeral.setNumocorrencia(1d);
				recursoGeral.setQuantidadecalculada(0d);
				
				if(recursoGeralTarefa.getMaterial() != null){
					recursoGeral.setMaterial(recursoGeralTarefa.getMaterial());
					recursoGeral.setNome(recursoGeralTarefa.getMaterial().getNome());
					recursoGeral.setUnidademedida(recursoGeralTarefa.getMaterial().getUnidademedida());
					
					Double valorMaterial = recursoGeral.getMaterial().getValorcusto();
					
					String paramValorProjeto = parametrogeralService.getValorPorNome(Parametrogeral.PRECOCUSTO_MATERIAL_PROJETO);
					if(paramValorProjeto != null && paramValorProjeto.trim().toUpperCase().equals("FALSE")){
						valorMaterial = recursoGeral.getMaterial().getValorvenda();
					}

					if(valorMaterial != null) 
						recursoGeral.setCustounitario(new Money(valorMaterial));
					
				} else {
					recursoGeral.setMaterial(null);
					recursoGeral.setNome(recursoGeralTarefa.getOutro());
					recursoGeral.setUnidademedida(recursoGeralTarefa.getUnidademedida());
					recursoGeral.setCustounitario(new Money());
				}
				
				if (listaRecursoGeralOrcamento != null) {
					encontrouRecurso = false;
					indexRecurso = -1;
					i = 0;
					
					//Para cada recurso geral do or�amento
					for (Recursocomposicao recursoGeralOrcamento : listaRecursoGeralOrcamento) {
						if(
							(recursoGeralOrcamento.getMaterial() != null && recursoGeralOrcamento.getMaterial().equals(recursoGeralTarefa.getMaterial())) ||
							(recursoGeralOrcamento.getMaterial() == null && recursoGeralOrcamento.getNome() != null && recursoGeralTarefa.getOutro() != null && recursoGeralOrcamento.getUnidademedida() != null && recursoGeralTarefa.getUnidademedida() != null 
							&& recursoGeralOrcamento.getNome().trim().toUpperCase().equals(recursoGeralTarefa.getOutro().trim().toUpperCase()) && recursoGeralOrcamento.getUnidademedida().equals(recursoGeralTarefa.getUnidademedida()))
						) {
							recursoGeral.setCdrecursocomposicao(recursoGeralOrcamento.getCdrecursocomposicao());
							recursoGeral.setCustounitario(recursoGeralOrcamento.getCustounitario());
							
							indexRecurso = i; 
							encontrouRecurso = true;
							break;
						} 
						i++;
					}
					
					if (encontrouRecurso) {
						//Se existe o recurso geral na tarefa, deve-se retir�-lo da lista.
						//No fim, o que sobrar nessa lista ser�o os itens para exclus�o.
						listaRecursoGeralOrcamento.remove(indexRecurso);
					}
				
					//Adiciona o recurso geral no mapa
					keyMapaMaterial = new MultiKey(new Object[]{recursoGeralTarefa.getMaterial(),  recursoGeralTarefa.getUnidademedida(), recursoGeralTarefa.getOutro()});
					recursoComposicaoMapa = mapaRecursoGeral.get(keyMapaMaterial);
					if (recursoComposicaoMapa == null) {
						recursoComposicaoMapa = recursoGeral;
					}
					recursoComposicaoMapa.setQuantidadecalculada(recursoComposicaoMapa.getQuantidadecalculada() + recursoGeralTarefa.getQuantidade());
					mapaRecursoGeral.put(keyMapaMaterial, recursoComposicaoMapa);					
				}				
			}
		}
		
		//Adiciona os itens para inser��o/atualiza��o
		Set<MultiKey> keySet = mapaRecursoGeral.keySet();
		for (MultiKey key : keySet) {
			recursoComposicaoMapa = mapaRecursoGeral.get(key);
			listaRecursoComposicaoForUpdate.add(recursoComposicaoMapa);
		}
		
		//Adiciona os itens para exclus�o
		if (listaRecursoGeralOrcamento != null) {
			for (Recursocomposicao recursoComposicaoForDelete : listaRecursoGeralOrcamento) {
				listaRecursoComposicaoForDelete.add(recursoComposicaoForDelete);
			}
		}
		recursocomposicaoDAO.saveListaRecursoComposicao(listaRecursoComposicaoForUpdate, listaRecursoComposicaoForDelete);
	}
	
	/***
	 * Calcula valores referentes � quantidade e n�mero de ocorr�ncias para os recursos gerais de um determinado or�amento 
	 * e salva o resultado no banco de dados.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findRecursoIndiretoByOrcamento(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaValoresRecursoGeral(Orcamento, List, List)
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#atualizaQuantidadeCalculadaRecursoForFlex(List)
	 * 
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargo
	 * @return 
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoRecursoGeralIndiretoFlex(Orcamento orcamento, List<Recursocomposicao> listaRecursoGeralOrcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		if (listaRecursoGeralOrcamento == null) {
			//Busca os recursos gerais indiretos do or�amento (n�o vinculados �s tarefas)
			listaRecursoGeralOrcamento = recursocomposicaoDAO.findRecursoIndiretoByOrcamento(orcamento);
		}
		
		//Calcula os valores referentes � quantidade e n�mero de ocorr�ncias do recurso, baseado no histograma
		calculaValoresRecursoGeral(orcamento, listaRecursoGeralOrcamento, listaPeriodoOrcamentoCargo);
		
		this.atualizaMateriais(listaRecursoGeralOrcamento);
	}
	
	public void atualizaMateriais(List<Recursocomposicao> listaRecursoGeralOrcamento) {
		for (Recursocomposicao recursocomposicao : listaRecursoGeralOrcamento) {
			if (recursocomposicao.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
				this.calculaQuantidadeRecursoSemDependencia(recursocomposicao);
			} else {
				if(recursocomposicao.getQuantidadecalculada() == null){
					recursocomposicao.setQuantidadecalculada(0d);
				}
			}
		}
		
		this.atualizaQuantidadeCalculadaRecursoForFlex(listaRecursoGeralOrcamento);
		
		String paramPrecoCusto = parametrogeralService.getValorPorNome(Parametrogeral.PRECOCUSTO_MATERIAL_PROJETO);
		
		Material material;
		for (Recursocomposicao recursocomposicao : listaRecursoGeralOrcamento) {
			if(recursocomposicao.getMaterial() != null){
				material = materialService.load(recursocomposicao.getMaterial(), "material.cdmaterial, material.valorcusto, material.valorvenda");
				
				Double precoMaterial = material.getValorcusto();
				if(paramPrecoCusto != null && paramPrecoCusto.trim().toUpperCase().equals("FALSE")){
					precoMaterial = material.getValorvenda();
				}
				
				if(precoMaterial != null){
					recursocomposicao.setCustounitario(new Money(precoMaterial));
					recursocomposicaoDAO.updateCustoUnitario(recursocomposicao);
				}
			}
		}
	}	
	
	public void atualizaQuantidadeCalculadaRecursoForFlex(List<Recursocomposicao> listaRecursoGeral){
		recursocomposicaoDAO.atualizaQuantidadeCalculadaRecursoForFlex(listaRecursoGeral);
	}
	
	/***
	 * Calcula valores referentes � quantidade e n�mero de ocorr�ncias para os recursos gerais de um determinado or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeRecursoSemDependencia(Recursocomposicao)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeRecursoDependenciaMDO(Recursocomposicao, List, List, Boolean)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeRecursoDependenciaSelecaoCargo(Recursocomposicao, List)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaCustoUnitarioRecurso(Recursocomposicao)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaNumeroOcorrenciaRecurso(Recursocomposicao, Integer)
	 * 
	 * @param orcamento
	 * @param listaRecursoGeralOrcamento
	 * @param listaPeriodoOrcamentoCargo
	 * @return List<Recursocomposicao>
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Recursocomposicao> calculaValoresRecursoGeral(Orcamento orcamento, List<Recursocomposicao> listaRecursoGeralOrcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		if (orcamento == null || orcamento.getCdorcamento() == null || listaRecursoGeralOrcamento == null || listaPeriodoOrcamentoCargo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		for (Recursocomposicao recursoGeral : listaRecursoGeralOrcamento) {
			if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
				calculaQuantidadeRecursoSemDependencia(recursoGeral);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.MOD)) {
				calculaQuantidadeRecursoDependenciaMDO(recursoGeral, null, listaPeriodoOrcamentoCargo, true);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.TODOS_CARGOS)) {
				calculaQuantidadeRecursoDependenciaMDO(recursoGeral, null, listaPeriodoOrcamentoCargo, false);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
				calculaQuantidadeRecursoDependenciaSelecaoCargo(recursoGeral, listaPeriodoOrcamentoCargo);
			}			
				
			//Preenche o valor do custo unit�rio do recurso
			calculaCustoUnitarioRecurso(recursoGeral);
			
			//Preenche o valor do n�mero de ocorr�ncias do recurso
			calculaNumeroOcorrenciaRecurso(recursoGeral, orcamento.getPrazosemana(), recursoGeral.getNumocorrencia());
		}
		
		return listaRecursoGeralOrcamento;
	}
	
	/***
	 * Calcula quantidade de um recurso geral que n�o possua depend�ncia com nenhum cargo.
	 * O resultado obtido � atribu�do ao campo quantidadecalculada do par�metro recursoGeral
	 * 
	 * @param recursoGeral
	 * @return
	 * 
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeRecursoSemDependencia(Recursocomposicao recursoGeral) {
		if (recursoGeral == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		recursoGeral.setQuantidadecalculada(recursoGeral.getQuantidade());		
	}
	
	/***
	 * Calcula a quantidade de um recurso geral baseada na quantidade de alguns cargos espec�ficos.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeRecursoDependenciaMDO(Recursocomposicao, List, List, Boolean)
	 * 
	 * @param recursoGeral
	 * @param listaPeriodoOrcamentoCargo
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeRecursoDependenciaSelecaoCargo(Recursocomposicao recursoGeral, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		if (recursoGeral == null || listaPeriodoOrcamentoCargo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		
		for (Dependenciacargocomposicao dependenciaCargoComposicao : recursoGeral.getListaDependenciacargocomposicao()) {
			listaCargo.add(dependenciaCargoComposicao.getCargo());
		}
		calculaQuantidadeRecursoDependenciaMDO(recursoGeral, listaCargo, listaPeriodoOrcamentoCargo, false);
	}	
	
	/***
	 * Calcula a quantidade de um recurso geral baseada na quantidade de m�o-de-obra.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeValor(Recursocomposicao, Map)
	 * 
	 * @param recursoGeral
	 * @param listaCargo
	 * @param listaPeriodoOrcamentoCargo
	 * @param somenteMOD
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeRecursoDependenciaMDO(Recursocomposicao recursoGeral, List<Cargo> listaCargo, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo, Boolean somenteMOD) {
		if (recursoGeral == null || listaPeriodoOrcamentoCargo == null || somenteMOD == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		Map<Integer, Double> mapaQuantidadePeriodo = new HashMap<Integer, Double>();
		Double quantidade;		
		
		for (Periodoorcamentocargo periodoOrcamentoCargo : listaPeriodoOrcamentoCargo) {
			
			if ((listaCargo == null || listaCargo.contains(periodoOrcamentoCargo.getCargo())) && 
				(!somenteMOD || periodoOrcamentoCargo.getCargo().getTipocargo().equals(Tipocargo.MOD))) {
				
				if (periodoOrcamentoCargo.getListaPeriodoorcamento() != null) {
					for (Periodoorcamento periodoOrcamento : periodoOrcamentoCargo.getListaPeriodoorcamento()) {
						
						quantidade = mapaQuantidadePeriodo.get(periodoOrcamento.getNumero());
						if (quantidade == null) {
							quantidade = 0.0;
						}
						
						if(recursoGeral.getReferenciacalculo() != null && recursoGeral.getReferenciacalculo().equals(Referenciacalculo.HORAS)){
							quantidade += periodoOrcamento.getQtde() * periodoOrcamentoCargo.getCargohorasemanal();
						} else {
							quantidade += periodoOrcamento.getQtde();
						}
						mapaQuantidadePeriodo.put(periodoOrcamento.getNumero(), quantidade);
					}
				}
			}
		}
		calculaQuantidadeValor(recursoGeral, mapaQuantidadePeriodo);
	}
	
	/***
	 * Calcula a quantidade de um recurso geral.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeValorUnico(Recursocomposicao, Double, Integer, Integer)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaQuantidadeValorPorFaixa(Recursocomposicao, Double, Integer, Integer)
	 * 
	 * @param recursoGeral
	 * @param mapaQuantidadePeriodo
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeValor(Recursocomposicao recursoGeral, Map<Integer, Double> mapaQuantidadePeriodo) {
		if (recursoGeral == null || mapaQuantidadePeriodo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		Double valorMedio    = 0d;
		Double valorTotal   = 0d;
		Double valorMaximo  = 0d;
		Double quantidade   = 0d;
		Integer numRegistros = 0;		
		
		//Calcula os valores m�dio, total e m�ximo
		Set<Integer> keySet = mapaQuantidadePeriodo.keySet();
		for (Integer periodo : keySet) {
			quantidade = mapaQuantidadePeriodo.get(periodo);
			if (quantidade > valorMaximo) {
				valorMaximo = quantidade;
			}
			valorTotal += quantidade;
			numRegistros++;
		}
		valorMedio = valorTotal.doubleValue() / numRegistros;
			
		//Verifica qual o tipo de valor
		if (recursoGeral.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
			calculaQuantidadeValorUnico(recursoGeral, valorMedio, valorTotal, valorMaximo);						
		}
		else {
			calculaQuantidadeValorPorFaixa(recursoGeral, valorMedio, valorTotal, valorMaximo);
		}		
	}
		
	/***
	 * Calcula a quantidade de um recurso geral baseado na rela��o 1 p/ cada.
	 * O resultado obtido � atribu�do ao campo quantidadecalculada do par�metro recursoGeral
	 * 
	 * @param recursoGeral
	 * @param valorMedio
	 * @param valorTotal
	 * @param valorMaximo
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeValorUnico(Recursocomposicao recursoGeral, Double valorMedio, Double valorTotal, Double valorMaximo) {
		if (recursoGeral == null || valorMedio == null || valorTotal == null || valorMaximo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		
		if (recursoGeral.getFormulacomposicao() != null) {
			Double quantidade = 0d;
			if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.MEDIA)) {
				quantidade = valorMedio.doubleValue();
			}
			else if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.TOTAL)) {
				quantidade = valorTotal.doubleValue();
			}
			else if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.VALOR_MAXIMO)) {
				quantidade = valorMaximo.doubleValue();
			}			
			recursoGeral.setQuantidadecalculada(Long.valueOf(Math.round(quantidade / recursoGeral.getQuantidade())).doubleValue());
		}
	}
	
	/***
	 * Calcula a quantidade de um recurso geral baseado na depend�ncia por faixa de valores.
	 * O resultado obtido � atribu�do ao campo quantidadecalculada do par�metro recursoGeral
	 * 
	 * @param recursoGeral
	 * @param valorMedio
	 * @param valorTotal
	 * @param valorMaximo
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaQuantidadeValorPorFaixa(Recursocomposicao recursoGeral, Double valorMedio, Double valorTotal, Double valorMaximo) {
		if (recursoGeral == null || valorMedio == null || valorTotal == null || valorMaximo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		Double quantidade = 0d;
		Double faixaDe;
		Double faixaAte;
		
		if (recursoGeral.getFormulacomposicao() != null) {
			if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.MEDIA)) {
				quantidade = valorMedio.doubleValue();	
			}
			else if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.TOTAL)) {
				quantidade = valorTotal.doubleValue();
			}
			else if (recursoGeral.getFormulacomposicao().equals(Formulacomposicao.VALOR_MAXIMO)) {
				quantidade = valorMaximo.doubleValue();
			}
			
			if (recursoGeral.getListaDependenciafaixacomposicao() != null) {
				for (Dependenciafaixacomposicao dependenciaFaixaComposicao : recursoGeral.getListaDependenciafaixacomposicao()) {
					faixaDe  = dependenciaFaixaComposicao.getFaixade();
					faixaAte = dependenciaFaixaComposicao.getFaixaate() != null ? dependenciaFaixaComposicao.getFaixaate() : Double.MAX_VALUE;

					//O primeiro valor que satisfizer a rela��o t� dentro.
					if (quantidade >= faixaDe && quantidade <= faixaAte) {
						quantidade = dependenciaFaixaComposicao.getQuantidade();
						break;
					}
				}
			}			
			recursoGeral.setQuantidadecalculada(quantidade);
		}
	}	
	
	/***
	 * Calcula o custo unit�rio do recurso geral caso ele seja um material e seu custo ainda n�o esteja preenchido.
	 * O resultado obtido � atribu�do ao campo custounitario do par�metro recursoGeral
	 * 
	 * @param recursoGeral
	 * @return
	 * 
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaCustoUnitarioRecurso(Recursocomposicao recursoGeral) {
		if (recursoGeral == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		if (recursoGeral.getCustounitario() == null && recursoGeral.getMaterial() != null) {
			Double valorMaterial = recursoGeral.getMaterial().getValorcusto();
			
			String paramValorProjeto = parametrogeralService.getValorPorNome(Parametrogeral.PRECOCUSTO_MATERIAL_PROJETO);
			if(paramValorProjeto != null && paramValorProjeto.trim().toUpperCase().equals("FALSE")){
				valorMaterial = recursoGeral.getMaterial().getValorvenda();
			}
			recursoGeral.setCustounitario(new Money(valorMaterial));
		}
	}
	
	/***
	 * Calcula o n�mero de ocorr�ncias de um recurso geral baseado no seu tipo de ocorr�ncia (MENSAL,SEMANAL,INFORMAR).
	 * O resultado obtido � atribu�do ao campo numocorrencia do par�metro recursoGeral
	 * 
	 * @param recursoGeral
	 * @param prazoSemana
	 * @param numOcorrencia
	 * @return
	 * 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private void calculaNumeroOcorrenciaRecurso(Recursocomposicao recursoGeral, Integer prazoSemana, Double numOcorrencia) {
		if (recursoGeral == null || prazoSemana == null || numOcorrencia == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		
		if (recursoGeral.getTipoocorrencia().equals(Tipoocorrencia.MENSAL)) {
			numOcorrencia = prazoSemana.doubleValue() / 4.28;
			
		}
		else if (recursoGeral.getTipoocorrencia().equals(Tipoocorrencia.SEMANAL)) {
			numOcorrencia = prazoSemana.doubleValue();
		}
		recursoGeral.setNumocorrencia(new BigDecimal(numOcorrencia).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());		
	}	
	
	/***
	 * Calcula valores referentes � quantidade de material de seguran�a necess�rio para um determinado or�amento 
	 * e salva o resultado no banco de dados.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#findByOrcamentoForFlex(Orcamento, Boolean)
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaMaterialSeguranca(List, List)
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#saveOrUpdate(Composicaoorcamento)
	 * 
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargo
	 * @return 
	 * 
	 * @throws SinedException - caso haja mais de uma composi��o de or�amento do tipo EPI
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoMaterialSegurancaFlex(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo) {
		
		Composicaoorcamento composicaoOrcamentoEPI;
		
		//Busca a composicao de material de seguran�a do banco, caso exista.
		List<Composicaoorcamento> listaComposicaoOrcamentoEPI = composicaoorcamentoService.findByOrcamentoForFlex(orcamento, true);
		
		//N�o existe a composi��o. Cria uma, ent�o.
		if (listaComposicaoOrcamentoEPI == null || listaComposicaoOrcamentoEPI.isEmpty()) {
			composicaoOrcamentoEPI = new Composicaoorcamento();
			composicaoOrcamentoEPI.setNome("Material de Seguran�a");
			composicaoOrcamentoEPI.setMaterialseguranca(true);
			composicaoOrcamentoEPI.setOrcamento(orcamento);
		}
		else {
			if (listaComposicaoOrcamentoEPI.size() > 1) {
				throw new SinedException("Existe mais de uma composi��o de material de seguran�a.");
			}
			composicaoOrcamentoEPI = listaComposicaoOrcamentoEPI.get(0);
		}
		
		//Calcula as quantidades dos materiais de seguran�a
		List<Recursocomposicao> listaRecursoGeralEPI = calculaMaterialSeguranca(orcamento, listaPeriodoOrcamentoCargo, composicaoOrcamentoEPI.getListaRecursocomposicao());
		
		//Adiciona os recursos calculados na composi��o		
		composicaoOrcamentoEPI.setListaRecursocomposicao(listaRecursoGeralEPI);
		
		//Salva a composi��o de material de seguran�a no banco de dados
		composicaoorcamentoService.saveOrUpdate(composicaoOrcamentoEPI);
	}
	
	/***
	 * Calcula valores referentes � quantidade de material de seguran�a necess�rio para um determinado or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CargomaterialsegurancaService#findByCargo(Cargo)
	 * 
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargo
	 * @param listaRecursoComposicaoBanco
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private List<Recursocomposicao> calculaMaterialSeguranca(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo, List<Recursocomposicao> listaRecursoComposicaoBanco) {
		
		//Listas
		List<Recursocomposicao> listaRecursoComposicaoCalculado = new ArrayList<Recursocomposicao>();
		List<Cargomaterialseguranca> listaCargoMaterialSeguranca;
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		
		//Mapas
		Map<MultiKey, Double> mapaEstoqueEPIPeriodo = new HashMap<MultiKey, Double>();
		Map<Epi,Double> mapaCompraEPI = new HashMap<Epi, Double>();
		
		//Beans
		MultiKey key, key2;
		Double quantidadeEPINecessaria;
		Double quantidadeEPIEstoque;
		Double quantidadeEPIAComprar;
		Double quantidadeEPIComprada;
		Integer duracaoEPI;
		Recursocomposicao recursoComposicaoNovo;
		
		for (Periodoorcamentocargo periodoOrcamentoCargo : listaPeriodoOrcamentoCargo) {
			listaCargo.add(periodoOrcamentoCargo.getCargo());
		}
		//Busca os materiais de seguran�a relacionados aos cargos do histograma
		listaCargoMaterialSeguranca = cargomaterialsegurancaService.findByCargo(listaCargo);
		
		for (int i = 1; i <= orcamento.getPrazosemana(); i++) {
			
			for (Periodoorcamentocargo periodoOrcamentoCargo : listaPeriodoOrcamentoCargo) {
				
				if (periodoOrcamentoCargo.getListaPeriodoorcamento() != null) {
					
					for (Periodoorcamento periodoOrcamento : periodoOrcamentoCargo.getListaPeriodoorcamento()) {
						
						if (periodoOrcamento.getNumero().intValue() == i) {						
							
							for (Cargomaterialseguranca cargoMaterialSeguranca : listaCargoMaterialSeguranca) {
								
								if (cargoMaterialSeguranca.getCargo().equals(periodoOrcamentoCargo.getCargo())) {
									
									//Calcula a quantidade necess�ria do material para o cargo no per�odo
									quantidadeEPINecessaria = cargoMaterialSeguranca.getQuantidade() * periodoOrcamento.getQtde();
									
									//Verifica se j� existe a quantidade necess�ria no estoque
									key = new MultiKey(new Object[]{cargoMaterialSeguranca.getEpi(), periodoOrcamento.getNumero()});
									quantidadeEPIEstoque = mapaEstoqueEPIPeriodo.get(key);
									
									if (quantidadeEPIEstoque == null) {
										quantidadeEPIEstoque = 0d;
									}
									
									//Se a quantidade em estoque atender � necessidade, n�o precisa comprar. 
									//Simplesmente, diminui a quantidade necess�ria do estoque.
									if (quantidadeEPIEstoque >= quantidadeEPINecessaria) {
										quantidadeEPIEstoque -= quantidadeEPINecessaria;
									}
									//Sen�o, tem que comprar a diferen�a (necessidade - estoque).
									//E zerar o estoque do material.
									else {
										quantidadeEPIAComprar = quantidadeEPINecessaria - quantidadeEPIEstoque;
										
										quantidadeEPIComprada = mapaCompraEPI.get(cargoMaterialSeguranca.getEpi());
										if (quantidadeEPIComprada == null) {
											quantidadeEPIComprada = 0d;
										}
										mapaCompraEPI.put(cargoMaterialSeguranca.getEpi(), quantidadeEPIComprada + quantidadeEPIAComprar);
//										System.out.println("Semana : " + i + " Compra Produto: " + cargoMaterialSeguranca.getEpi().getNome() + " Quantidade: " + quantidadeEPIAComprar);
										
										//Deve-se tamb�m acrescentar a quantidade do material comprado no estoque dos per�odos seguintes,
										//de acordo com sua dura��o.
										duracaoEPI = cargoMaterialSeguranca.getEpi().getDuracao();
										
										//Do caso de uso: Os materiais sem dura��o dever�o ser considerados com dura��o infinita
										//N�o h� necessidade da dura��o do material ser maior que o prazo do or�amento.
										//Portanto, sua dura��o assume o valor do total de per�odo do mesmo.
										if (duracaoEPI == null) {
											duracaoEPI = orcamento.getPrazosemana();
										}								
										
										for (int j = 1; j < duracaoEPI; j++) {
											if (j + periodoOrcamento.getNumero() > orcamento.getPrazosemana()) {
												break;
											}
											key2 = new MultiKey(new Object[]{cargoMaterialSeguranca.getEpi(), j + periodoOrcamento.getNumero()});
											quantidadeEPIComprada = mapaEstoqueEPIPeriodo.get(key2);
											if (quantidadeEPIComprada == null) {
												quantidadeEPIComprada = 0d;
											}
											mapaEstoqueEPIPeriodo.put(key2, quantidadeEPIComprada + quantidadeEPIAComprar);
										}								
										
										//Zera o estoque do material
										quantidadeEPIEstoque = 0d;
									}
									mapaEstoqueEPIPeriodo.put(key, quantidadeEPIEstoque);
								}
							}
							break;
						}
					}
				}
			}			
		}
		
		//Verifica os recursos que j� existiam no banco. Nesse caso, basta atualizar as quantidades.
		if (listaRecursoComposicaoBanco != null) {
			for (Recursocomposicao recursoComposicaoBanco : listaRecursoComposicaoBanco) {
				quantidadeEPIAComprar = mapaCompraEPI.get(recursoComposicaoBanco.getMaterial());				
				if (quantidadeEPIAComprar != null) {
					recursoComposicaoBanco.setQuantidadecalculada(quantidadeEPIAComprar);
					listaRecursoComposicaoCalculado.add(recursoComposicaoBanco);
					mapaCompraEPI.remove(recursoComposicaoBanco.getMaterial());
				}
			}
		}
		
		//O que sobrou no mapa � recurso novo, que n�o existia no banco
		Set<Epi> keySet = mapaCompraEPI.keySet();
		for (Epi epi : keySet) {
			recursoComposicaoNovo = new Recursocomposicao();
			recursoComposicaoNovo.setFormulacomposicao(null);
			recursoComposicaoNovo.setCustounitario(epi.getValorvenda() != null ? new Money(epi.getValorvenda()) : null);
			recursoComposicaoNovo.setMaterial(epi);
			recursoComposicaoNovo.setNome(epi.getNome());
			recursoComposicaoNovo.setNumocorrencia(1d);
			recursoComposicaoNovo.setOrcamento(orcamento);			
			recursoComposicaoNovo.setQuantidadecalculada(mapaCompraEPI.get(epi));
			recursoComposicaoNovo.setTipodependenciarecurso(Tipodependenciarecurso.SEM_DEPENDENCIA);
			recursoComposicaoNovo.setTipoitemcomposicao(Tipoitemcomposicao.MATERIAL);
			recursoComposicaoNovo.setTipoocorrencia(Tipoocorrencia.INFORMAR);
			recursoComposicaoNovo.setTiporelacaorecurso(Tiporelacaorecurso.VALOR_UNICO);
			recursoComposicaoNovo.setUnidademedida(epi.getUnidademedida());
			
			listaRecursoComposicaoCalculado.add(recursoComposicaoNovo);
		}
		
		return listaRecursoComposicaoCalculado;
	}
	
	/**
	 * Busca os recursos gerais de uma determinada composi��o do or�amento
	 *
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#findRecursoByComposicao(Composicaoorcamento)
	 *
	 * @param composicaoorcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoByComposicao(Composicaoorcamento composicaoorcamento) {
		return recursocomposicaoDAO.findRecursoByComposicao(composicaoorcamento);
	}

	/**
	 * Gera o relat�rio de recurso geral do or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#findForListagemFlex
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#calculaValorTotal
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#agrupaListagem
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelarorioRecursoGeralOrcamento(OrcamentoRecursoGeralFiltro filtro) {
		Report report;
		
		if(filtro.getIncluirValores() != null && filtro.getIncluirValores()){
			report = new Report("/projeto/recursoGeral_comValores");
		} else {
			report = new Report("/projeto/recursoGeral_semValores");
		}
		
		List<Recursocomposicao> lista =  this.findForListagemFlex(filtro);
		
		Double total = this.calculaValorTotal(lista);
		report.addParameter("TOTAL", "R$ " + new DecimalFormat("#,##0.00").format(total));
		
		lista = this.agrupaListagem(lista);
		
		report.addParameter("ORCAMENTO", filtro.getOrcamento() != null ? filtro.getOrcamento().getNome() : null);
		report.addParameter("COMPOSICAO",  filtro.getComposicaoOrcamento() != null ? filtro.getComposicaoOrcamento().getNome() : null);
		report.addParameter("RECURSO", filtro.getNomeRecurso().equals("") ? null : filtro.getNomeRecurso());
		
		String origem = null;
		if(filtro.getRecursoDireto() && filtro.getRecursoComposicao()){
			origem = "Recurso direto e Composi��o";
		} else if(filtro.getRecursoDireto()){
			origem = "Recurso direto";
		} else if(filtro.getRecursoComposicao()){
			origem = "Composi��o";
		}
		report.addParameter("ORIGEM", origem);
		
        report.setDataSource(lista);
        
		return report;
	}
	
	/**
	 * Agrupa os materiais com mesmo nome somando a quantidade total e valor total.
	 *
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Recursocomposicao> agrupaListagem(List<Recursocomposicao> lista) {
		List<Recursocomposicao> listaNova = new ArrayList<Recursocomposicao>();
		Map<String, Recursocomposicao> mapaRecurso = new HashMap<String, Recursocomposicao>();		
		Recursocomposicao recursocomposicao;
		
		
		for (Recursocomposicao rc : lista) {
			if(mapaRecurso.containsKey(rc.getNome())){
				recursocomposicao = mapaRecurso.get(rc.getNome());
				recursocomposicao.setQuantidadeTotal(recursocomposicao.getQuantidadeTotal() + rc.getQuantidadeTotal());
				recursocomposicao.setValortotal(recursocomposicao.getValortotal().add(rc.getValortotal()));
				
				mapaRecurso.put(rc.getNome(), recursocomposicao);
			} else {
				mapaRecurso.put(rc.getNome(), rc);
				listaNova.add(rc);
			}
		}
		
		for (Recursocomposicao rc2 : listaNova) {
			rc2 = mapaRecurso.get(rc2.getNome());
		}
		
		return listaNova;
	}

	/**
	 * Calcula o valor total de cada recurso e o valor total de todos os recursos.
	 *
	 * @param listaRecComp
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Double calculaValorTotal(List<Recursocomposicao> listaRecComp) {
		Double valorTotal = 0.0;
		
		for (Recursocomposicao recursocomposicao : listaRecComp) {
			if(recursocomposicao.getCustounitario() != null &&
					recursocomposicao.getQuantidadecalculada() != null &&
					recursocomposicao.getNumocorrencia() != null) {
				
				recursocomposicao.setValortotal(new Money((recursocomposicao.getCustounitario().getValue().doubleValue()*
															recursocomposicao.getQuantidadecalculada()*
															recursocomposicao.getNumocorrencia()), false));
				recursocomposicao.setQuantidadeTotal(recursocomposicao.getQuantidadecalculada()*
															recursocomposicao.getNumocorrencia());
				
				valorTotal += recursocomposicao.getValortotal().getValue().doubleValue();				
			}
		}
		
		return valorTotal;
	}
	
	/**
	 * Coloca o filtro no escopo de sess�o, para a gera��o do relat�rio.
	 *
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void putSessionFiltroRelatorio(OrcamentoRecursoGeralFiltro filtro){
		NeoWeb.getRequestContext().getSession().setAttribute(SESSION_FILTRO_RELATORIO_RECGERAL, filtro);
	}
	
	/**
	 * Coloca o filtro no escopo de sess�o, para a gera��o do relat�rio.
	 *
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void putSessionFiltroRelatorioSintetico(OrcamentoRecursoGeralFiltro filtro){
		NeoWeb.getRequestContext().getSession().setAttribute(SESSION_FILTRO_RELATORIO_RECGERAL_SINTETICO, filtro);
	}

	public IReport gerarRelatorioRecursoGeralOrcamentoSintetico(OrcamentoRecursoGeralFiltro filtro) {
		Report report = new Report("/projeto/recursoGeral_sintetico");
		
		List<AnaliseRecDespBean> listaFolhas =  new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaCompleta =  new ArrayList<AnaliseRecDespBean>();
		List<AnaliseRecDespBean> listaOutrosMaterial = new ArrayList<AnaliseRecDespBean>();
		AnaliseRecDespBean ard;
		
		List<Recursocomposicao> lista =  this.findForListagemFlex(filtro);
		this.calculaValorTotal(lista);
		Money somamaterial = new Money();;
		
		for (Recursocomposicao rc : lista) {
			ard = new AnaliseRecDespBean();
			if(rc.getMaterial() != null && rc.getMaterial().getContagerencial() != null){
				ard.setIdentificador(rc.getMaterial().getContagerencial().getVcontagerencial().getIdentificador());
				ard.setNome(rc.getMaterial().getContagerencial().getDescricao());
				ard.setOperacao(rc.getMaterial().getContagerencial().getTipooperacao().getSigla());
				ard.setValor(rc.getValortotal());
				
				listaFolhas.add(ard);
			} else {
				ard.setIdentificador("XX.XX");
				ard.setNome(rc.getNome());
				ard.setOperacao("D");
				ard.setValor(rc.getValortotal());
				
				somamaterial = somamaterial.add(rc.getValortotal());
				
				listaOutrosMaterial.add(ard);
			}
		}
		
		planejamentoService.agruparContasGerenciais(listaFolhas);
		
		listaCompleta = contagerencialService.buscarArvoreContaGerencial(null);
		
		contagerencialService.inserirFolhasNaListaCompleta(listaCompleta, listaFolhas);
		contagerencialService.calcularValores(listaCompleta, false);
		
		if(listaOutrosMaterial != null && listaOutrosMaterial.size() > 0){
			AnaliseRecDespBean bean = new AnaliseRecDespBean();
			bean.setIdentificador("XX");
			bean.setNome("Outros Materiais");
			bean.setOperacao("D");
			bean.setValor(somamaterial);
			listaCompleta.add(bean);
			
			planejamentoService.agruparContasGerenciais(listaOutrosMaterial);
			listaCompleta.addAll(listaOutrosMaterial);
		}
		
		Map<String, Money> mpTotais = contagerencialService.totalizar(listaCompleta, false);
		Money totalCredito = mpTotais.get("totalCredito");
		Money totalDebito = mpTotais.get("totalDebito");
		
		report.setDataSource(listaCompleta);
		
		report.addParameter("totalCredito", totalCredito);
		report.addParameter("totalDebito", totalDebito);
		report.addParameter("saldo", totalCredito.subtract(totalDebito));
		
		report.addParameter("ORCAMENTO", filtro.getOrcamento() != null ? filtro.getOrcamento().getNome() : null);
		report.addParameter("COMPOSICAO",  filtro.getComposicaoOrcamento() != null ? filtro.getComposicaoOrcamento().getNome() : null);
		report.addParameter("RECURSO", filtro.getNomeRecurso().equals("") ? null : filtro.getNomeRecurso());
		
		String origem = null;
		if(filtro.getRecursoDireto() && filtro.getRecursoComposicao()){
			origem = "Recurso direto e Composi��o";
		} else if(filtro.getRecursoDireto()){
			origem = "Recurso direto";
		} else if(filtro.getRecursoComposicao()){
			origem = "Composi��o";
		}
		report.addParameter("ORIGEM", origem);
		
		return report;
	}
	
	public void atualizaOrcamentoRecursoGeralSemHistogramaFlex(Orcamento orcamento) {
		Map<Cargo, Double> mapa = periodoorcamentocargoService.carregaListaCargoQuantidadeSemHistograma(orcamento);
		
		//Atualiza a lista dos recursos gerais relacionados �s tarefas
		atualizaOrcamentoRecursoGeralDiretoFlex(orcamento);

		//Atualiza a lista dos recursos gerais n�o relacionados �s tarefas		
		atualizaOrcamentoRecursoGeralIndiretoFlex(orcamento, null, mapa);
		
		if (Boolean.TRUE.equals(orcamento.getCalcularmaterialseguranca())) {
			//Atualiza a lista dos materiais de seguran�a do or�amento		
			atualizaOrcamentoMaterialSegurancaFlex(orcamento, mapa);
		}
		else {
			//Apaga a composi��o de material de seguran�a, caso exista
			List<Composicaoorcamento> listaComposicaoOrcamentoEPI = composicaoorcamentoService.findByOrcamentoForFlex(orcamento, true);
			if (listaComposicaoOrcamentoEPI != null) {
				for (Composicaoorcamento composicaoOrcamento : listaComposicaoOrcamentoEPI) {
					composicaoorcamentoService.delete(composicaoOrcamento);
				}
			}
		}
	}
	
	public void atualizaOrcamentoMaterialSegurancaFlex(Orcamento orcamento, Map<Cargo, Double> mapa) {
		Composicaoorcamento composicaoOrcamentoEPI;
		
		//Busca a composicao de material de seguran�a do banco, caso exista.
		List<Composicaoorcamento> listaComposicaoOrcamentoEPI = composicaoorcamentoService.findByOrcamentoForFlex(orcamento, true);
		
		//N�o existe a composi��o. Cria uma, ent�o.
		if (listaComposicaoOrcamentoEPI == null || listaComposicaoOrcamentoEPI.isEmpty()) {
			composicaoOrcamentoEPI = new Composicaoorcamento();
			composicaoOrcamentoEPI.setNome("Material de Seguran�a");
			composicaoOrcamentoEPI.setMaterialseguranca(true);
			composicaoOrcamentoEPI.setOrcamento(orcamento);
		}
		else {
			if (listaComposicaoOrcamentoEPI.size() > 1) {
				throw new SinedException("Existe mais de uma composi��o de material de seguran�a.");
			}
			composicaoOrcamentoEPI = listaComposicaoOrcamentoEPI.get(0);
		}
		
		//Calcula as quantidades dos materiais de seguran�a
		List<Recursocomposicao> listaRecursoGeralEPI = calculaMaterialSeguranca(orcamento, mapa, composicaoOrcamentoEPI.getListaRecursocomposicao());
		
		//Adiciona os recursos calculados na composi��o		
		composicaoOrcamentoEPI.setListaRecursocomposicao(listaRecursoGeralEPI);
		
		//Salva a composi��o de material de seguran�a no banco de dados
		composicaoorcamentoService.saveOrUpdate(composicaoOrcamentoEPI);
	}
	
	private List<Recursocomposicao> calculaMaterialSeguranca(Orcamento orcamento, Map<Cargo, Double> mapa, List<Recursocomposicao> listaRecursoComposicaoBanco) {
		//Listas
		List<Recursocomposicao> listaRecursoComposicaoCalculado = new ArrayList<Recursocomposicao>();
		List<Cargomaterialseguranca> listaCargoMaterialSeguranca;
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		
		//Mapas
		Map<Epi,Double> mapaCompraEPI = new HashMap<Epi, Double>();
		
		//Beans
		Double quantidadeEPINecessaria;
		Double quantidadeEPIAComprar;
		Double quantidadeEPIComprada;
		Recursocomposicao recursoComposicaoNovo;
		Cargo cargo;
		Double quantidade;
		
		Set<Entry<Cargo, Double>> entrySet = mapa.entrySet();
		
		for (Entry<Cargo, Double> entry : entrySet) {
			listaCargo.add(entry.getKey());
		}
		//Busca os materiais de seguran�a relacionados aos cargos do histograma
		listaCargoMaterialSeguranca = cargomaterialsegurancaService.findByCargo(listaCargo);
		
		
		for (Entry<Cargo, Double> entry : entrySet) {			
			cargo = entry.getKey();
			quantidade = entry.getValue() / cargo.getTotalhorasemana();
			
			for (Cargomaterialseguranca cargoMaterialSeguranca : listaCargoMaterialSeguranca) {
				if (cargoMaterialSeguranca.getCargo().equals(cargo)) {
					//Calcula a quantidade necess�ria do material para o cargo no per�odo
					quantidadeEPINecessaria = cargoMaterialSeguranca.getQuantidade() * quantidade;
					
					quantidadeEPIComprada = mapaCompraEPI.get(cargoMaterialSeguranca.getEpi());
					if (quantidadeEPIComprada == null) {
						quantidadeEPIComprada = 0d;
					}
					mapaCompraEPI.put(cargoMaterialSeguranca.getEpi(), quantidadeEPIComprada + quantidadeEPINecessaria);
				}
			}
		}
		
		//Verifica os recursos que j� existiam no banco. Nesse caso, basta atualizar as quantidades.
		if (listaRecursoComposicaoBanco != null) {
			for (Recursocomposicao recursoComposicaoBanco : listaRecursoComposicaoBanco) {
				quantidadeEPIAComprar = mapaCompraEPI.get(recursoComposicaoBanco.getMaterial());				
				if (quantidadeEPIAComprar != null) {
					recursoComposicaoBanco.setQuantidadecalculada(quantidadeEPIAComprar);
					listaRecursoComposicaoCalculado.add(recursoComposicaoBanco);
					mapaCompraEPI.remove(recursoComposicaoBanco.getMaterial());
				}
			}
		}
		
		//O que sobrou no mapa � recurso novo, que n�o existia no banco
		Set<Epi> keySet = mapaCompraEPI.keySet();
		for (Epi epi : keySet) {
			recursoComposicaoNovo = new Recursocomposicao();
			recursoComposicaoNovo.setFormulacomposicao(null);
			if(epi.getValorvenda() != null){
				recursoComposicaoNovo.setCustounitario(new Money(epi.getValorvenda()));
			}
			recursoComposicaoNovo.setMaterial(epi);
			recursoComposicaoNovo.setNome(epi.getNome());
			recursoComposicaoNovo.setNumocorrencia(1d);
			recursoComposicaoNovo.setOrcamento(orcamento);			
			recursoComposicaoNovo.setQuantidadecalculada(mapaCompraEPI.get(epi));
			recursoComposicaoNovo.setTipodependenciarecurso(Tipodependenciarecurso.SEM_DEPENDENCIA);
			recursoComposicaoNovo.setTipoitemcomposicao(Tipoitemcomposicao.MATERIAL);
			recursoComposicaoNovo.setTipoocorrencia(Tipoocorrencia.INFORMAR);
			recursoComposicaoNovo.setTiporelacaorecurso(Tiporelacaorecurso.VALOR_UNICO);
			recursoComposicaoNovo.setUnidademedida(epi.getUnidademedida());
			
			listaRecursoComposicaoCalculado.add(recursoComposicaoNovo);
		}
		
		return listaRecursoComposicaoCalculado;
	}
	
	public void atualizaOrcamentoRecursoGeralIndiretoFlex(Orcamento orcamento, List<Recursocomposicao> listaRecursoGeralOrcamento, Map<Cargo, Double> mapa) {
		if (listaRecursoGeralOrcamento == null) {
			//Busca os recursos gerais indiretos do or�amento (n�o vinculados �s tarefas)
			listaRecursoGeralOrcamento = recursocomposicaoDAO.findRecursoIndiretoByOrcamento(orcamento);
		}
		
		//Calcula os valores referentes � quantidade e n�mero de ocorr�ncias do recurso, baseado no histograma
		calculaValoresRecursoGeral(orcamento, listaRecursoGeralOrcamento, mapa);
		
		this.atualizaMateriais(listaRecursoGeralOrcamento);
	}
	
	public List<Recursocomposicao> calculaValoresRecursoGeral(Orcamento orcamento, List<Recursocomposicao> listaRecursoGeralOrcamento, Map<Cargo, Double> mapa) {
		for (Recursocomposicao recursoGeral : listaRecursoGeralOrcamento) {
			if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
				calculaQuantidadeRecursoSemDependencia(recursoGeral);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.MOD)) {
				calculaQuantidadeRecursoDependenciaMDO(recursoGeral, null, mapa, true);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.TODOS_CARGOS)) {
				calculaQuantidadeRecursoDependenciaMDO(recursoGeral, null, mapa, false);
			}
			else if (recursoGeral.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
				calculaQuantidadeRecursoDependenciaSelecaoCargo(recursoGeral, mapa);
			}			
				
			//Preenche o valor do custo unit�rio do recurso
			calculaCustoUnitarioRecurso(recursoGeral);
			
			//Preenche o valor do n�mero de ocorr�ncias do recurso
			calculaNumeroOcorrenciaRecurso(recursoGeral, orcamento.getPrazosemana(), recursoGeral.getNumocorrencia());
		}
		
		return listaRecursoGeralOrcamento;
	}
	
	private void calculaQuantidadeRecursoDependenciaSelecaoCargo(Recursocomposicao recursoGeral, Map<Cargo, Double> mapa) {
		if (recursoGeral == null || mapa == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		
		for (Dependenciacargocomposicao dependenciaCargoComposicao : recursoGeral.getListaDependenciacargocomposicao()) {
			listaCargo.add(dependenciaCargoComposicao.getCargo());
		}
		calculaQuantidadeRecursoDependenciaMDO(recursoGeral, listaCargo, mapa, false);
	}
	
	private void calculaQuantidadeRecursoDependenciaMDO(Recursocomposicao recursoGeral, List<Cargo> listaCargo, Map<Cargo, Double> mapa, Boolean somenteMOD) {
		if (recursoGeral == null || mapa == null || somenteMOD == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		Double quantidade = 0d;		
		Set<Entry<Cargo, Double>> entrySet = mapa.entrySet();
		
		for (Entry<Cargo, Double> entry : entrySet) {
			if ((listaCargo == null || listaCargo.contains(entry.getKey())) && 
				(!somenteMOD || entry.getKey().getTipocargo().equals(Tipocargo.MOD))) {
				quantidade += entry.getValue();
			}
		}
		calculaQuantidadeValor(recursoGeral, quantidade);
	}
	
	private void calculaQuantidadeValor(Recursocomposicao recursoGeral, Double valorTotal) {
		if (recursoGeral == null || valorTotal == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		//Verifica qual o tipo de valor
		if (recursoGeral.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
			calculaQuantidadeValorUnico(recursoGeral, valorTotal);						
		}
		else {
			calculaQuantidadeValorPorFaixa(recursoGeral, valorTotal);
		}		
	}
	
	private void calculaQuantidadeValorUnico(Recursocomposicao recursoGeral, Double valorTotal) {
		if (recursoGeral == null || valorTotal == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		
		if (recursoGeral.getFormulacomposicao() != null) {
			recursoGeral.setQuantidadecalculada(Long.valueOf(Math.round(valorTotal.doubleValue() / recursoGeral.getQuantidade())).doubleValue());
		}
	}
	
	private void calculaQuantidadeValorPorFaixa(Recursocomposicao recursoGeral, Double valorTotal) {
		if (recursoGeral == null || valorTotal == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		Double faixaDe;
		Double faixaAte;
		
		if (recursoGeral.getFormulacomposicao() != null) {
			Double quantidade = valorTotal.doubleValue();
			
			if (recursoGeral.getListaDependenciafaixacomposicao() != null) {
				for (Dependenciafaixacomposicao dependenciaFaixaComposicao : recursoGeral.getListaDependenciafaixacomposicao()) {
					
					faixaDe  = dependenciaFaixaComposicao.getFaixade();
					faixaAte = dependenciaFaixaComposicao.getFaixaate() != null ? dependenciaFaixaComposicao.getFaixaate() : Double.MAX_VALUE;

					//O primeiro valor que satisfizer a rela��o t� dentro.
					if (quantidade >= faixaDe && quantidade <= faixaAte) {
						quantidade = dependenciaFaixaComposicao.getQuantidade();
						break;
					}
				}
			}	
			recursoGeral.setQuantidadecalculada(quantidade);
		}
	}	
	
}

