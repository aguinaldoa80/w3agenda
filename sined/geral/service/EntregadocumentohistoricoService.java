package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.dao.EntregadocumentohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregadocumentohistoricoService extends GenericService<Entregadocumentohistorico> {
	
	private EntregadocumentohistoricoDAO entregadocumentohistoricoDAO;
	
	public void setEntregadocumentohistoricoDAO(EntregadocumentohistoricoDAO entregadocumentohistoricoDAO) {
		this.entregadocumentohistoricoDAO = entregadocumentohistoricoDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregadocumentohistoricoDAO#findByEntregadocumento(Entregadocumento entregadocumento)
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentohistorico> findByEntregadocumento(Entregadocumento entregadocumento){
		return entregadocumentohistoricoDAO.findByEntregadocumento(entregadocumento);
	}

	/**
	 * M�todo que cria a lista de hist�rico da entradafiscal
	 *
	 * @param listaEdh
	 * @return
	 * @author Luiz Fernando
	 */
	public Set<Entregadocumentohistorico> getLista(List<Entregadocumentohistorico> listaEdh) {
		Set<Entregadocumentohistorico> lista = new ListSet<Entregadocumentohistorico>(Entregadocumentohistorico.class);
		if(listaEdh != null && !listaEdh.isEmpty()){
			for(Entregadocumentohistorico entregadocumentohistorico : listaEdh){
				lista.add(entregadocumentohistorico);
			}
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregadocumentohistoricoDAO#findForDevolucaoEntregadocumento(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentohistorico> findForDevolucaoEntregadocumento(String whereIn){
		return entregadocumentohistoricoDAO.findForDevolucaoEntregadocumento(whereIn);
	}
	
	/**
	 * M�todo cpm refer�ncia no DAO.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		entregadocumentohistoricoDAO.deleteAllFromEntregaNotInWhereIn(cdentrega, whereNotIn);
	}
}
