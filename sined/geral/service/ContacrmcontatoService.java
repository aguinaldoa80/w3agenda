package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.dao.ContacrmcontatoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmcontatoService extends GenericService<Contacrmcontato>{
	
	protected ContacrmcontatoDAO contacrmcontatoDAO;
	
	public void setContacrmcontatoDAO(ContacrmcontatoDAO contacrmcontatoDAO) {
		this.contacrmcontatoDAO = contacrmcontatoDAO;
	}
	
	public List<Contacrmcontato> findByContacrm(Contacrm contacrm){
		return contacrmcontatoDAO.findByContacrm(contacrm);
	}
	
	public List<Contacrmcontato> findByContacrmForCombo(Contacrm contacrm){
		return contacrmcontatoDAO.findByContacrmForCombo(contacrm);
	}
	
	
	public void saveOrUpdateListContacrmcontato(Contacrm contacrm, List<Contacrmcontato> listaContato){
		if(listaContato == null){
			return;
		}
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser null.");
		}
		
		List<Contacrmcontato> listaAtual = this.findByContacrm(contacrm);
		List<Contacrmcontato> listaDelete = new ArrayList<Contacrmcontato>();
			
		for (Contacrmcontato contato : listaAtual) {
			if(contato.getCdcontacrmcontato() != null && !listaContato.contains(contato)){
				listaDelete.add(contato);
			}
		}
		
		for (Contacrmcontato contato : listaDelete) {
			this.delete(contato);
		}
		for (Contacrmcontato contato : listaContato) {
			contato.setContacrm(contacrm);
			this.saveOrUpdateNoUseTransaction(contato);
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ContacrmcontatoDAO#buscaEmailsTelefones(Contacrmcontato contacrmcontato)
	 *
	 * @param contacrmcontato
	 * @return
	 * @author Luiz Fernando
	 */
	public Contacrmcontato buscaEmailsTelefones(Contacrmcontato contacrmcontato) {
		return contacrmcontatoDAO.buscaEmailsTelefones(contacrmcontato);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @See br.com.linkcom.sined.geral.dao.ContacrmcontatoDAO#findByContacrm(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 05/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contacrmcontato> findByContacrm(String whereIn) {
		return contacrmcontatoDAO.findByContacrm(whereIn);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContacrmcontatoDAO#findContatoEnderecoByContacrm(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public List<Contacrmcontato> findContatoEnderecoByContacrm (String whereIn) {
		return contacrmcontatoDAO.findContatoEnderecoByContacrm(whereIn);
	}
}
