package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.dao.ProducaoetapaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapa.ProducaoEtapaW3producaoRESTModel;

public class ProducaoetapaService extends GenericService<Producaoetapa> {

	private ProducaoetapaDAO producaoetapaDAO;

	public void setProducaoetapaDAO(ProducaoetapaDAO producaoetapaDAO) {
		this.producaoetapaDAO = producaoetapaDAO;
	}
	
	/* singleton */
	private static ProducaoetapaService instance;
	public static ProducaoetapaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProducaoetapaService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoetapaDAO#loadWithList(Producaoetapa producaoetapa)
	 *
	 * @param producaoetapa
	 * @return
	 * @author Luiz Fernando
	 */
	public Producaoetapa loadWithList(Producaoetapa producaoetapa){
		return producaoetapaDAO.loadWithList(producaoetapa);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoetapaitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2014
	 */
	public boolean isUltimaEtapa(Producaoetapaitem producaoetapaitem) {
		return producaoetapaDAO.isUltimaEtapa(producaoetapaitem);
	}
	
	/**
	* M�todo que retorna a �ltima etapaitem
	*
	* @param producaoetapa
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public Producaoetapaitem getUltimaEtapa(Producaoetapa producaoetapa){
		Producaoetapaitem ultimaEtapa = null;
		if(producaoetapa != null && SinedUtil.isListNotEmpty(producaoetapa.getListaProducaoetapaitem())){
			for(Producaoetapaitem pei : producaoetapa.getListaProducaoetapaitem()){
				if(pei.getOrdem() != null){
					if(ultimaEtapa == null){
						ultimaEtapa = pei;
					}else if(pei.getOrdem() > ultimaEtapa.getOrdem()){
						ultimaEtapa = pei;
					}
				}
			}
		}
		return ultimaEtapa;
	}
	
	public Producaoetapa loadBaixarEstoqueMateriaprima(Producaoetapa producaoetapa){
		return producaoetapaDAO.loadBaixarEstoqueMateriaprima(producaoetapa);
	}
	
	public List<ProducaoEtapaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo que monta a lista de producaoetapa para sincronizar com o W3Producao.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<ProducaoEtapaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ProducaoEtapaW3producaoRESTModel> lista = new ArrayList<ProducaoEtapaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Producaoetapa pe : producaoetapaDAO.findForW3Producao(whereIn))
				lista.add(new ProducaoEtapaW3producaoRESTModel(pe));
		}
		
		return lista;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdproducaoetapa
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdproducaoetapa) {
		producaoetapaDAO.atualizaW3producao(cdproducaoetapa);
	}
	
	public Boolean validaOrdemPrioridade(Producaoetapa bean){
		return producaoetapaDAO.validaOrdemPrioridade(bean);
	}
}
