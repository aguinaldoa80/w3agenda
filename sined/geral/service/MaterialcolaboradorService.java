package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Materialcolaborador;
import br.com.linkcom.sined.geral.dao.MaterialcolaboradorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialcolaboradorService extends GenericService<Materialcolaborador> {

	private MaterialcolaboradorDAO materialcolaboradorDAO;
		
	public void setMaterialcolaboradorDAO(MaterialcolaboradorDAO materialcolaboradorDAO) {this.materialcolaboradorDAO = materialcolaboradorDAO;}

	public List<Materialcolaborador> findMaterialcolaboradorByVenda(String whereIn, Colaborador colaborador) {
		return materialcolaboradorDAO.findMaterialcolaboradorByVenda(whereIn, colaborador);
	}
}
