package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.dao.ContagerencialDAO;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespAnaliticoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBeanFlex;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoListagemBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.ApuracaoResultadoProjetoFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContagerencialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author Desktop
 *
 */
public class ContagerencialService extends GenericService<Contagerencial>{

	public static final String FILTRO_SESSION = "FILTRO_ANALISE_RECEITAS_DESPESAS";
	private ContagerencialDAO contagerencialDAO;
	private ContaService contaService;
	private ControleorcamentoService controleorcamentoService;
	private ProjetoService projetoService;
	private ParametrogeralService parametrogeralService;
	
	public void setControleorcamentoService(
			ControleorcamentoService controleorcamentoService) {
		this.controleorcamentoService = controleorcamentoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setContagerencialDAO(ContagerencialDAO contagerencialDAO) {
		this.contagerencialDAO = contagerencialDAO;
	}
	public void setProjetoService (ProjetoService projetoService){
		this.projetoService = projetoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}


	/* singleton */
	private static ContagerencialService instance;
	public static ContagerencialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContagerencialService.class);
		}
		return instance;
	}
	
	@Override
	public void saveOrUpdate(Contagerencial bean) {
		super.saveOrUpdate(bean);
		// fazer update se a conta gerencial for anal�tca e se tiver registros.
		if (BooleanUtils.isTrue(bean.getAnaliticacomregistros())) {
//			this.updateRateioItens(bean);
//			this.updateMaterial(bean);
			contagerencialDAO.updateRelacionamento(bean);
		}
	}

	/**
	 * Faz referencia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#carregaConta
	 * @param contagerencial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Contagerencial carregaConta(Contagerencial bean) {
		return contagerencialDAO.carregaConta(bean);
	}

	/**
	 * Faz referencia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findFilhos
	 * @param contagerencial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contagerencial> findFilhos(Contagerencial pai){
		return contagerencialDAO.findFilhos(pai);
	}

	/**
	 * Faz referencia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findRaiz
	 * @param contagerencial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contagerencial> findRaiz(){
		return contagerencialDAO.findRaiz();
	}

	/**
	 * Faz referencia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#isAnalitica
	 * @param contagerencial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isAnalitica(Contagerencial contagerencial){
		return contagerencialDAO.haveRegister(contagerencial);
	}
	

	/**
	 * Faz referencia ao DAO.
	 * Carrega a lista de contas gerenciais analiticas.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findAnaliticas
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contagerencial> findAnaliticas(Tipooperacao tipooperacao){
		return contagerencialDAO.findAnaliticas(tipooperacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findAnaliticasByNatureza(Tipooperacao tipooperacao, String whereInNatureza)
	 *
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findAnaliticasByNatureza(Tipooperacao tipooperacao, String whereInNatureza){
		return contagerencialDAO.findAnaliticasByNatureza(tipooperacao, whereInNatureza);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findByNatureza(String whereInNatureza)
	 *
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findByNatureza(String whereInNatureza){
		return contagerencialDAO.findByNatureza(whereInNatureza);
	}

	/**
	 * Faz referencia ao DAO.
	 * Carrega a lista de contas gerenciais analiticas.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findAnaliticas
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findAnaliticasDebito(String whereIn){
		return findAnaliticas(whereIn, Tipooperacao.TIPO_DEBITO);
	}

	/**
	 * Faz referencia ao DAO.
	 * Carrega a lista de contas gerenciais analiticas.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findAnaliticas
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findAnaliticasCredito(String whereIn){
		return findAnaliticas(whereIn, Tipooperacao.TIPO_CREDITO);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param tipooperacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findAnaliticas(String whereIn, Tipooperacao tipooperacao) {
		return contagerencialDAO.findAnaliticas(whereIn, tipooperacao);
	}
	
	/**
	 * Prepara o bean para ser salvo no CRUD.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findFilhos
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#carregaConta
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void preparaBeanParaSalvar(Contagerencial bean) {
		//setar o contagerencialpai e o tipooperacao do pai.
		if (bean.getPai() != null) {
			bean.setContagerencialpai(bean.getPai());
			bean.setTipooperacao(this.carregaConta(bean.getContagerencialpai()).getTipooperacao());
		}

		if (bean.getItem() == null) {
			//setar o campo item para gera��o do identificador
			if (bean.getContagerencialpai() != null && bean.getContagerencialpai().getCdcontagerencial() != null) {
				List<Contagerencial> listaFilhos = findFilhos(bean.getContagerencialpai());
				if (listaFilhos.size() > 0) {
					bean.setItem(listaFilhos.get(0).getItem() + 1);
					bean.setFormato(listaFilhos.get(0).getFormato());
				} else {
					bean.setItem(1);
				}
			} else {
				List<Contagerencial> listaPais = findRaiz();
				if (listaPais.size() > 0) {
					bean.setItem(listaPais.get(0).getItem() + 1);
					bean.setFormato(listaPais.get(0).getFormato());
				} else {
					bean.setItem(1);
				}
			}
		}


		if (bean.getTipooperacao() == null) {
			bean.setTipooperacao(bean.getTipooperacao2());
		}
	}


	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de Contagerencial por tipooperacao.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findByTipooperacao(Tipooperacao)
	 * @param tipooperacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Contagerencial> findByTipooperacao(Tipooperacao tipooperacao){
		return contagerencialDAO.findByTipooperacao(tipooperacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#findByTipooperacaoNatureza(Tipooperacao tipooperacao, String whereInNatureza)
	 *
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findByTipooperacaoNatureza(Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza){
		return contagerencialDAO.findByTipooperacaoNatureza(tipooperacao, whereInCdcontagerencial, whereInNatureza, whereNotInNatureza);
	}

	public List<Contagerencial> findForError(String listaContaGerencial) {
		return contagerencialDAO.findForError(listaContaGerencial);
	}
	
	
	public AnaliseRecDespBeanFlex gerarAnaliseRecDespesaFlex() {
		AnaliseReceitaDespesaReportFiltro filtro = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(FILTRO_SESSION);
		return gerarAnaliseRecDespesaFlex(filtro);
	}
	
	public AnaliseRecDespBeanFlex gerarAnaliseRecDespesaFlex(AnaliseReceitaDespesaReportFiltro filtro){
		List<AnaliseRecDespBean> listaFolhas = null;
		List<AnaliseRecDespBean> listaCompleta = null;
		
		listaFolhas = this.buscarParaRelatorioAnaliseReceitaDespesa(filtro);
		listaCompleta = this.buscarArvoreContaGerencial(filtro);
		
		this.inserirFolhasNaListaCompleta(listaCompleta, listaFolhas);
		this.calcularValores(listaCompleta, !filtro.getContassemlancamentos());
		Map<String, Money> mpTotais = this.totalizar(listaCompleta, true);
		Money totalCredito = mpTotais.get("totalCredito");
		Money totalDebito = mpTotais.get("totalDebito");
		
		for (AnaliseRecDespBean a : listaCompleta) {
			a.setIdentificador(a.getIdentificador().trim());
			if(a.getPercentual()!=null){
				a.setPercentual(a.getPercentual()*100);				
			}
		}
		
		listaCompleta = this.algoritmoListaAnaliseRecDespesaFlex(listaCompleta);
		
		AnaliseRecDespBeanFlex flex = new AnaliseRecDespBeanFlex();
		flex.setListaDP(listaCompleta);
		flex.setTotalcredito(totalCredito.getValue().doubleValue());
		flex.setTotaldebito(totalDebito.getValue().doubleValue());
		flex.setSaldo(totalCredito.subtract(totalDebito).getValue().doubleValue());
		
		Money saldoinicial = contaService.calculaSaldoAtual(SinedDateUtils.addDiasData(filtro.getDtInicio(), -1), true, NaturezaContagerencial.CONTA_GERENCIAL.getValue().toString(), filtro.getEmpresas());
//		Money saldofinal = null;
//		if(filtro.getDtFim() != null)
//			saldofinal = contaService.calculaSaldoAtual(filtro.getDtFim(), true, filtro.getEmpresas());
//		else
//			saldofinal = contaService.calculaSaldoAtual(new Date(System.currentTimeMillis()), true, filtro.getEmpresas());
		
		Money saldofinal = saldoinicial != null ? saldoinicial : new Money();
		saldofinal = saldofinal.add(totalCredito.subtract(totalDebito));
		
		flex.setSaldoinicial(saldoinicial != null ? saldoinicial.getValue().doubleValue() : 0.0);
		flex.setSaldofinal(saldofinal != null ? saldofinal.getValue().doubleValue() : 0.0);
		
		flex.setDtinicio(filtro.getDtInicio());
		flex.setDtfim(filtro.getDtFim() != null ? filtro.getDtFim() : new Date(System.currentTimeMillis()));
		flex.setMovimentacoes(AnaliseReceitaDespesaOrigem.MOVIMENTACOES.equals(filtro.getOrigem()));
		
		return flex;
	}
	
	

	private List<AnaliseRecDespBean> algoritmoListaAnaliseRecDespesaFlex(List<AnaliseRecDespBean> listaCompleta) {
		List<AnaliseRecDespBean> listaRaiz = null;
		if(listaCompleta != null && listaCompleta.size() > 0){
			listaRaiz = this.getListaRaiz(listaCompleta);			
			listaRaiz = this.getListaWithFilha(listaRaiz, listaCompleta);
		}
		
		return listaRaiz;
	}

	private List<AnaliseRecDespBean> getListaWithFilha(List<AnaliseRecDespBean> listaRaiz, List<AnaliseRecDespBean> listaCompleta) {
		List<AnaliseRecDespBean> listaFilha = null;
		for (int i = 0; i < listaRaiz.size(); i++) {
			listaFilha = new ArrayList<AnaliseRecDespBean>();
			for (AnaliseRecDespBean a : listaCompleta) {
				if (a.getIdentificador().trim().startsWith(listaRaiz.get(i).getIdentificador().trim()) && 
						a.getIdentificador().trim().split("\\.").length == (listaRaiz.get(i).getIdentificador().trim().split("\\.").length + 1)) {
					listaFilha.add(a);
				}
			}
			listaRaiz.get(i).setListaFilha(getListaWithFilha(listaFilha, listaCompleta));
		}
		
		return listaRaiz;
	}

	private List<AnaliseRecDespBean> getListaRaiz(List<AnaliseRecDespBean> listaCompleta) {
		List<AnaliseRecDespBean> listaRaiz = new ArrayList<AnaliseRecDespBean>();
		
		for (AnaliseRecDespBean a : listaCompleta) {
			if(a.getIdentificador().split("\\.").length == 1)
				listaRaiz.add(a);
		}
		
		return listaRaiz;
	}

	/*###################################################################*/
	/*########## RELAT�RIO DE AN�LISE DE RECEITAS E DESPESAS ############*/
	/*###################################################################*/
	/**
	 * <p>Gera um relat�rio de an�lise de receitas e despesas.</p>
	 * 
	 * @see #buscarParaRelatorioAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro)
	 * @see #buscarArvoreContaGerencial(List)
	 * @see #inserirFolhasNaListaCompleta(List, List)
	 * @see #calcularValores(List)
	 * @see #totalizar(List)
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public IReport gerarRelatorioAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) {
		Report report = new Report("/financeiro/analiseReceitaDespesa");
		
		
		List<AnaliseRecDespBean> listaFolhas = null;
		List<AnaliseRecDespBean> listaCompleta = null;
		
		listaFolhas = this.buscarParaRelatorioAnaliseReceitaDespesa(filtro);
		listaCompleta = this.buscarArvoreContaGerencial(filtro);
		
		this.inserirFolhasNaListaCompleta(listaCompleta, listaFolhas);
		this.calcularValores(listaCompleta, !filtro.getContassemlancamentos());
		Map<String, Money> mpTotais = this.totalizar(listaCompleta, false);
		Money totalCredito = mpTotais.get("totalCredito");
		Money totalDebito = mpTotais.get("totalDebito");
		
		report.setDataSource(listaCompleta);

		/*Filtro*/
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		/*Filtro*/

		/*totais*/
		report.addParameter("totalCredito", totalCredito);
		report.addParameter("totalDebito", totalDebito);
		report.addParameter("saldo", totalCredito.subtract(totalDebito));
		/*totais*/

		return report;
	}

	/**
	 * Calcula e seta, em cada conta gerencial da listaCompleta, o valor total de
	 * movimenta��es.
	 * <p>
	 * O algoritmo funciona da seguinte maneira:
	 * <UL>
	 * 	<li> � feito um "caminhamento em n�vel" na "�rvore" de contas gerenciais
	 * 	<li> encontra-se a conta gerencial de maior n�vel (maior identificador.length)
	 * 	<li> busca todas as contas gerenciais deste n�vel na lista completa e coloca na lista de trabalho
	 * 	<li> pega o primeiro item da lista de trabalho
	 * 	<li> encontra suas irm�s (que tem o mesmo pai) e contra a CG pai
	 * 	<li> calcula o valor da CG pai
	 * 	<li> remove as CGs processadas da lista de trabalho
	 * </UL>
	 * </p>
	 * <p> PS.: CG == Conta gerencial </p>
	 * 
	 * @see #encontrarMaiorNivel(List)
	 * @see #buscarTodasNivel(List, int)
	 * @see #buscarIrmas(List, AnaliseRecDespBean)
	 * @see #buscarPai(List, AnaliseRecDespBean)
	 * @see #somarValores(List)
	 * 
	 * @param listaCompleta
	 * 
	 * @author Hugo Ferreira
	 * @param contascomlancamentos 
	 */
	public void calcularValores(List<AnaliseRecDespBean> listaCompleta, Boolean contascomlancamentos) {
		List<AnaliseRecDespBean> listaTrabalho = null;
		List<AnaliseRecDespBean> listaIrmas = null;
		List<AnaliseRecDespBean> listaZerados = new ArrayList<AnaliseRecDespBean>();
		AnaliseRecDespBean filho = null;
		AnaliseRecDespBean pai = null;

		int nivel = this.encontrarMaiorNivel(listaCompleta);

		while (nivel > 1) {
			listaTrabalho = this.buscarTodasNivel(listaCompleta, nivel);
			while (!listaTrabalho.isEmpty()) { 
				filho = listaTrabalho.get(0);
				listaIrmas = this.buscarIrmas(listaTrabalho, filho);
				pai = this.buscarPai(listaCompleta, filho);
				if(pai != null){ 
					pai.setValor(this.somarValores(listaIrmas));
					
					for(AnaliseRecDespBean irmas : listaIrmas){
						if(irmas.getItemIncluidoControleOrcamento() != null && irmas.getItemIncluidoControleOrcamento()){
							pai.setItemIncluidoControleOrcamento(irmas.getItemIncluidoControleOrcamento());
						}
					}
				}
				listaTrabalho.removeAll(listaIrmas);
			}
			nivel-=1;
		}
		
		if(contascomlancamentos)
			for (AnaliseRecDespBean analiseRecDespBean : listaCompleta) 
				if(analiseRecDespBean.getValor().getValue().doubleValue() == 0.0 && (analiseRecDespBean.getItemIncluidoControleOrcamento() == null || !analiseRecDespBean.getItemIncluidoControleOrcamento()))
					listaZerados.add(analiseRecDespBean);
				
		if(listaZerados != null && !listaZerados.isEmpty())
			listaCompleta.removeAll(listaZerados);
		
	}

	/**
	 * Retorna uma lista de contas gerenciais que sejam do n�vel passado no par�metro.
	 * 
	 * @param listaCompleta
	 * @param nivel
	 * @return
	 * @author Hugo Ferreira
	 */
	private List<AnaliseRecDespBean> buscarTodasNivel(List<AnaliseRecDespBean> listaCompleta, int nivel) {
		List<AnaliseRecDespBean> listaMesmoNivel = new ArrayList<AnaliseRecDespBean>();
		
		for (AnaliseRecDespBean bean : listaCompleta) {
			if (bean.getIdentificador().split("\\.").length == nivel && ((bean.getAtivo() != null && bean.getAtivo()) || bean.getAtivo() == null))
				listaMesmoNivel.add(bean);
		}
		
		return listaMesmoNivel;
	}

	/**
	 * Faz uma pesquisa seq�encial na listaCompleta para encontrar a conta
	 * gerencial de maior n�vel.
	 * <p> A conta gerencial de maior n�vel � aquela que possui o maior identificador
	 * (identificador.length). </p>
	 * 
	 * 
	 * @param listaCompleta
	 * @return
	 * @author Hugo Ferreira
	 */
	private int encontrarMaiorNivel(List<AnaliseRecDespBean> listaCompleta) {
		if (listaCompleta == null || listaCompleta.isEmpty()) return -1;
		
		int tamMax = -1;
		for (AnaliseRecDespBean bean : listaCompleta) {
			if (bean.getIdentificador().split("\\.").length > tamMax) tamMax = bean.getIdentificador().split("\\.").length;
		}
		return tamMax;
	}

	/**
	 * Pesquisa e encontra na lista todas as contas gerenciais irm�s da contaGerencial (par�metro).
	 * 
	 * <p>
	 * A condi��o para descobrir se as contas gerenciais s�o irm�s � se seus identificadores
	 * possuem o mesmo prefixo. P.e.: 01.01.01, 01.01.02, 01.01.03 s�o irm�s, pois todas come�am
	 * com 01.01; 01.02.01 n�o � irm� dessas 3 �ltimas.
	 * </p>
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean#IDENTIFICADOR_COMPARATOR
	 * @see java.util.Collections#binarySearch(List, Object, Comparator)
	 * @param lista
	 * @param contaGerencial
	 * @return
	 * @author Hugo Ferreira
	 */
	private List<AnaliseRecDespBean> buscarIrmas(List<AnaliseRecDespBean> lista, AnaliseRecDespBean contaGerencial) {
		String id = contaGerencial.getIdentificador();
		String prefixo = id.split("\\.").length > 1 ? StringUtils.substring(id, 0, (id.length()-(id.split("\\.")[id.split("\\.").length-1].length() + 1))) : "";
		if (StringUtils.isEmpty(prefixo)) return null;
		
		AnaliseRecDespBean irma = null;
		List<AnaliseRecDespBean> listaIrmas = new ArrayList<AnaliseRecDespBean>();
		
		int index = lista.indexOf(contaGerencial);
		listaIrmas.add(lista.get(index));
		index++;
		
		while (lista.size() > index) {
			irma = lista.get(index);
			if (irma.getIdentificador().startsWith(prefixo)) {
				listaIrmas.add(irma);
			} else {
				break;
				//como a lista est� ordenada, se a CG atual n�o � irm� da CG do par�metro da fun��o,
				//as seguintes tamb�m n�o ser�o.
			}
			
			index++;
		}
		
		return listaIrmas;
	}

	/**
	 * Retorna o somat�rio dos valores de cada conta gerencial da lista
	 * 
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	private Money somarValores(List<AnaliseRecDespBean> lista) {
		Money somatorio = new Money();
		for (AnaliseRecDespBean recDespBean : lista) {
			somatorio = somatorio.add(recDespBean.getValor());
		}
		return somatorio;
	}

	/**
	 * Coloca cada conta gerencial da listaFolhas (que possuem valor) em seu respectivo
	 * lugar na listaCompleta.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean#IDENTIFICADOR_COMPARATOR
	 * @see java.util.Collections#binarySearch(List, Object, Comparator)
	 * @param listaCompleta
	 * @param listaFolhas
	 * @author Hugo Ferreira
	 */
	public void inserirFolhasNaListaCompleta(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaFolhas) {
		for (AnaliseRecDespBean folha : listaFolhas) {
			if(listaCompleta.contains(folha)){
				int index = listaCompleta.indexOf(folha);
				listaCompleta.set(index, folha);
			}
		}
	}
	
	public void inserirFolhasNaListaCompletaPorCentroCusto(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaFolhas,List<AnaliseRecDespBean> listaCompletaFinal) {
		List<Centrocusto> listaCentroCusto = new ArrayList<Centrocusto>();
		
		for (AnaliseRecDespBean folha : listaFolhas) {
			Centrocusto centrocusto = new Centrocusto(folha.getCdcentrocusto(),folha.getNomecentrocusto());
			if(!listaCentroCusto.contains(centrocusto)){
				listaCentroCusto.add(centrocusto);
			}
		}
		
		for(AnaliseRecDespBean completa : listaCompleta){
			for(Centrocusto cc : listaCentroCusto){
				AnaliseRecDespBean completaFinal = new AnaliseRecDespBean(cc.getCdcentrocusto(), cc.getNome(), completa.getCdcontagerencial(), completa.getIdentificador(), completa.getNome(), completa.getOperacao(), completa.getValor().toLong(), completa.getMes(), completa.getAno());
				listaCompletaFinal.add(completaFinal);
			}
		}
		
		
		for(AnaliseRecDespBean folha : listaFolhas){
			int i = 0;
			for(AnaliseRecDespBean completaFinal : listaCompletaFinal){
				if((folha.getCdcentrocusto() != null && completaFinal.getCdcentrocusto() != null) &&  (folha.getIdentificador() != null && completaFinal.getIdentificador() != null)){
					if(folha.getCdcentrocusto().equals(completaFinal.getCdcentrocusto()) && folha.getIdentificador().equals(completaFinal.getIdentificador())){
						listaCompletaFinal.set(i, folha);
						break;
					}	
				}
				i++;
			}		
		}
	}
	
	

	/**
	 * Recupera toda a �rvore de Contas Gerenciais a partir de uma
	 * lista de Contas Gerenciais folha.
	 * 
	 * <p>
	 * Para descobrir qual � a �rvore, descobre-se quais s�o os identificadores de todas as 
	 * Contas Gerenciais da �rvore a partir dos identificadores das Contas Gerenciais da 
	 * listaFolhas, montando uma string com estes identificadores, separados por v�rgula. Essa
	 * string � usada numa query no par�metro WHERE IN.
	 * </p>
	 * 
	 * @see br.com.linkcom.sined.util.SinedUtil#montaWhereInString(String)
	 * @see #buscarTudoEm(String)
	 * @param listaFolhas
	 * @return
	 * @author Hugo Ferreira
	 * @param contassemlancamentos 
	 */
	public List<AnaliseRecDespBean> buscarArvoreContaGerencial(AnaliseReceitaDespesaReportFiltro filtro) {
		return this.buscarTudoEm(filtro);
	}

//	/**
//	 * Descobre todos os identificadores de toda a �rvore de Contas Gerenciais correspondente
//	 * �s Contas Gerenciais da listaFolhas.
//	 * 
//	 * @see br.com.linkcom.neo.util.CollectionsUtil#listAndConcatenate(java.util.Collection, String, String)
//	 * @see br.com.linkcom.sined.util.SinedUtil#removeValoresDuplicados(String)
//	 * @param listaFolhas
//	 * @return
//	 * @author Hugo Ferreira
//	 */
//	private String descobreIdentificadoresDaArvore(List<AnaliseRecDespBean> listaFolhas) {
//		StringBuffer buffer = new StringBuffer(); //Uso StringBuffer para concatenar strings dentro de uma repeti��o
//		
//		String identificador = CollectionsUtil.listAndConcatenate(listaFolhas, "identificador", ",");
//
//		//j� coloca de cara os identificadores das Contas Gerenciais da listaFolhas
//		buffer.append(identificador);
//		
//		String[] listIdentificadores = identificador.split(",");
//		for (String ident : listIdentificadores) {
//			do {
//				/*
//				 * Este la�o, feito em cada identificador da listaFolha, vai removendo os 3
//				 * �ltimos caracteres do identificador. Desta maneira ele encontra as Contas
//				 * Gerenciais de n�vel mais alto, at� a raiz.
//				 * 
//				 * P.e.: Identificador: 01.01.01.02:
//				 * 		 encontra ---->	01.01.01
//				 * 						01.01
//				 * 						01
//				 */
//				ident = ident.split("\\.").length > 0 ? StringUtils.substring(ident, 0, (ident.length()-(ident.split("\\.")[ident.split("\\.").length-1].length() + 1))) : "";
//				
//				if (StringUtils.isNotEmpty(ident)) { 
//					buffer.append(",");
//					buffer.append(ident);
//				}
//			} while (StringUtils.isNotEmpty(ident));
//		}
//		
//		//Para reduzir o n�mero de par�metros na query, remove-se os identificadores repetidos
//		return SinedUtil.removeValoresDuplicados(buffer.toString());
//	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Recupera uma lista de Contas Gerenciais cujo identificador esteja relacionado
	 * na string whereIn.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#buscarTudoEm(String)
	 * @return
	 * @author Hugo Ferreira
	 */

	private List<AnaliseRecDespBean> buscarTudoEm(AnaliseReceitaDespesaReportFiltro filtro) {
		return contagerencialDAO.buscarTudoEm(filtro);
	}

	/**
	 * <p>Fornece os dados de origem para relat�rio de An�lise de Receitas e Despesas.
	 * Pega uma lista de movimenta��es que corresponda aos crit�rios do par�metro filtro.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#buscarParaRelatorioAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro)
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<AnaliseRecDespBean> buscarParaRelatorioAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) {
		return contagerencialDAO.buscarParaRelatorioAnaliseReceitaDespesa(filtro);
	}
	
	/**
	 * Faz uma pesquisa bin�ria na lista de AnaliseRecDespBean para encontrar
	 * o pai de uma dada Conta Gerencial.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean#PAI_COMPARATOR
	 * @see java.util.Collections#binarySearch(List, Object, Comparator)
	 * @param lista
	 * @param filho
	 * @return Uma refer�ncia ao objeto pai, ou null, caso o pai n�o seja encontrado.
	 * @author Hugo Ferreira
	 */
	private AnaliseRecDespBean buscarPai(List<AnaliseRecDespBean> lista, AnaliseRecDespBean filho) {
		int indexPai = Collections.binarySearch(lista, filho, AnaliseRecDespBean.PAI_COMPARATOR);

		if (indexPai < 0) {
			return null;
		}
		return lista.get(indexPai);
	}

	/**
	 * <p>Calcula o percentual do atributo 'valor' do filho em rela��o ao pai.</p>
	 * 
	 * @param filho
	 * @param pai
	 * @return 1, se o nodo filho n�o possuir pai (� raiz da �rvore), ou o valor
	 * 		do percentual calculado, dividindo-se o valor do filho pelo valor
	 * 		do pai.
	 * @author Hugo Ferreira
	 */
	private double calcularPercentual(AnaliseRecDespBean filho, AnaliseRecDespBean pai) {
		if (pai == null) { // N�vel 0
			return 1D;
		}

		double valorPai = pai.getValor().getValue().doubleValue();
		double valorFilho = filho.getValor().getValue().doubleValue();
		
		return valorFilho / valorPai;
	}

	/**
	 * <p>Edenta o nodo de acordo com seu n�vel hier�rquico, para exibi��o no relat�rio.</p>
	 * 
	 * @see br.com.linkcom.neo.util.StringUtils#stringCheia(String, String, int, boolean)
	 * @param nodo
	 * @param numEspacos n�mero de espa�os para multiplicar pelo n�vel.
	 * 		Por exemplo, se o n�mero de espa�os for 2, no n�vel 0 ter� 0 espa�os,
	 * 		no n�vel 1 ter� 2, no n�vel 2 ter� 4, etc.
	 * @author Hugo Ferreira
	 */
	private void edentar(AnaliseRecDespBean nodo, int numEspacos) {
		String id = nodo.getIdentificador();
		
		int tamId = id.length();
		int nivel = id.split("\\.").length * 2;
		int tamTotal = tamId + nivel * numEspacos;
		
		id = br.com.linkcom.neo.util.StringUtils.stringCheia(id, " ", tamTotal, false);
		nodo.setIdentificador(id);
	}
	
	/**
	 * <p>Edenta o nodo de acordo com seu n�vel hier�rquico, para exibi��o no relat�rio.</p>
	 * 
	 * @see br.com.linkcom.neo.util.StringUtils#stringCheia(String, String, int, boolean)
	 * @param nodo
	 * @param numEspacos n�mero de espa�os para multiplicar pelo n�vel.
	 * 		Por exemplo, se o n�mero de espa�os for 2, no n�vel 0 ter� 0 espa�os,
	 * 		no n�vel 1 ter� 2, no n�vel 2 ter� 4, etc.
	 * @author Hugo Ferreira
	 */
	private void edentarNome(AnaliseRecDespBean nodo, int numEspacos) {
		String nome = nodo.getNome();
		String id = nodo.getIdentificador();
		
		int nivel = id.split("\\.").length * 2;
		int tamTotal = nodo.getNome().length() + nivel * numEspacos;
		
		nome = br.com.linkcom.neo.util.StringUtils.stringCheia(nome, " ", tamTotal, false);
		nodo.setNome(nome);
	}

	/**
	 * Calcula os totais de cr�ditos e d�bitos, o percentual de movimenta��es
	 * e edenta a lista.
	 * 
	 * @see #buscarPai(List, AnaliseRecDespBean)
	 * @see #calcularPercentual(AnaliseRecDespBean, AnaliseRecDespBean)
	 * @see #edentar(AnaliseRecDespBean, int)
	 * @param listaCompleta
	 * @return
	 * @author Hugo Ferreira
	 */
	public Map<String, Money> totalizar(List<AnaliseRecDespBean> listaCompleta, Boolean flex) {
		AnaliseRecDespBean pai = null;
		Money totalCredito = new Money();
		Money totalDebito = new Money();

		for (AnaliseRecDespBean nodoAtual : listaCompleta) {
			//C�lculo do percentual
			pai = this.buscarPai(listaCompleta, nodoAtual);
			nodoAtual.setPercentual(this.calcularPercentual(nodoAtual, pai));
			
			if(!flex){
				this.edentar(nodoAtual, 2);
			} else {
				this.edentarNome(nodoAtual, 2);
			}
			/*Faz a totaliza��o. Calcula o somat�rio de cr�ditos e d�bitos (somente com os
			valores das contas gerenciais de n�vel 0)*/
			if (pai == null) {
				if (nodoAtual.getOperacao().equals("C")) {
					totalCredito = totalCredito.add(nodoAtual.getValor());
				} else {
					totalDebito = totalDebito.add(nodoAtual.getValor());
				}
			}
		}
		
		Map<String, Money> map = new HashMap<String, Money>();
		map.put("totalCredito", totalCredito);
		map.put("totalDebito", totalDebito);
		return map;
	}
	/*###################################################################*/
	/*######## FIM RELAT�RIO DE AN�LISE DE RECEITAS E DESPESAS ##########*/
	/*###################################################################*/

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findContasGerenciais(String whereIn) {
		return contagerencialDAO.findContasGerenciais(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contagerencial findContaGerencialVeiculo(Veiculo veiculo) {
		return contagerencialDAO.findContaGerencialVeiculo(veiculo);
	}
	
	public Contagerencial loadWithIdentificador(Contagerencial contagerencial) {
		return contagerencialDAO.loadWithIdentificador(contagerencial);
	}
	
	public List<Contagerencial> findAllFilhos(Contagerencial cg) {
		return contagerencialDAO.findAllFilhos(cg);
	}
	
	public Contagerencial findPaiNivel1(Contagerencial contagerencial) {
		return contagerencialDAO.findPaiNivel1(contagerencial);
	}
	
	public Contagerencial findByIdentificador(String identificador) {
		return contagerencialDAO.findByIdentificador(identificador);
	}
	
	public List<Contagerencial> findAnaliticasDebitoForFlex(){
		return this.findAnaliticas(Tipooperacao.TIPO_DEBITO);		
	}
	
	public List<Contagerencial> findDebitoForFlex(){
		return this.findByTipooperacao(Tipooperacao.TIPO_DEBITO);
	}
	
	public List<Contagerencial> findForReport(ContagerencialFiltro filtro){
		return contagerencialDAO.findForReport(filtro);
	}
	
	public IReport createRelatorioContagerencial(ContagerencialFiltro contagerencialFiltro){
		
		Report report = new Report("/financeiro/contagerencial");
		List<Contagerencial> lista = this.findForReport(contagerencialFiltro);
		
		report.setDataSource(lista);
		return report;
	}

	/**
	 * M�todo para ajustar as listas de Pagamento e Recebimento do filtro.]
	 * 
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	public void ajustaListasAnaliseReceitaDespesaFiltro(AnaliseReceitaDespesaReportFiltro filtro) {
		List<ItemDetalhe> listaFornecedor = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaCliente = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaColaborador = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaOutrospagamento = new ArrayList<ItemDetalhe>();
		
		List<ItemDetalhe> listaPagamentoa = filtro.getListaPagamentoa();
		List<ItemDetalhe> listaRecebimentode = filtro.getListaRecebimentode();
		
		if(SinedUtil.isListNotEmpty(listaPagamentoa)){
			for (ItemDetalhe id : listaPagamentoa) {
				id.setRecebidode(false);
				switch(id.getTipopagamento()){
					case CLIENTE:
						listaCliente.add(id);
						break;
					case COLABORADOR:
						listaColaborador.add(id);
						break;
					case FORNECEDOR:
						listaFornecedor.add(id);
						break;
					case OUTROS:
						listaOutrospagamento.add(id);
						break;
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaRecebimentode)){
			for (ItemDetalhe id : listaRecebimentode) {
				id.setRecebidode(true);
				switch(id.getTipopagamento()){
				case CLIENTE:
					listaCliente.add(id);
					break;
				case COLABORADOR:
					listaColaborador.add(id);
					break;
				case FORNECEDOR:
					listaFornecedor.add(id);
					break;
				case OUTROS:
					listaOutrospagamento.add(id);
					break;
				}
			}
		}
		
		filtro.setListaFornecedor(listaFornecedor);
		filtro.setListaCliente(listaCliente);
		filtro.setListaColaborador(listaColaborador);
		filtro.setListaOutrospagamento(listaOutrospagamento);
	}
	
	/**
	 * M�todo que gera relat�rio de analise de despesas em Excel
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public Resource gerarExcelAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) throws IOException{
		List<AnaliseRecDespBean> listaFolhas = null;
		List<AnaliseRecDespBean> listaCompleta = null;
		
		String whereInEmpresa = ""; 
		if(filtro.getListaEmpresa() != null){
			whereInEmpresa = CollectionsUtil.listAndConcatenate(filtro.getListaEmpresa(), "empresa.cdpessoa", ",");
		}
		
		String whereInCentrocusto = "";
		if(filtro.getListaCentrocusto() != null) {
			whereInCentrocusto = CollectionsUtil.listAndConcatenate(filtro.getListaCentrocusto(),"centrocusto.cdcentrocusto",",");
		}

		String whereInProjeto = "";
		if(filtro.getListaProjeto() != null){
			whereInProjeto = CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(),"projeto.cdprojeto",",");
		}
		
		String whereInProjetotipo = "";
		if(filtro.getListaProjetotipo() != null){
			whereInProjetotipo = CollectionsUtil.listAndConcatenate(filtro.getListaProjetotipo(),"projetotipo.cdprojetotipo",",");
		}
		
		listaFolhas = this.buscarParaRelatorioAnaliseReceitaDespesaExcel(filtro);
		
		Date dataMaior = SinedDateUtils.currentDate();
		for (AnaliseRecDespBean analiseRecDespBean : listaFolhas) {
			if(analiseRecDespBean.getMes() != null && analiseRecDespBean.getAno() != null){
				try {
					Date aux_date = SinedDateUtils.stringToDate("01/" + analiseRecDespBean.getMes() + "/" + analiseRecDespBean.getAno());
					if(SinedDateUtils.afterIgnoreHour(aux_date, dataMaior)){
						dataMaior = aux_date;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		
//		int anoDe = SinedDateUtils.getDateProperty(filtro.getDtInicio(), Calendar.YEAR);
//		int anoAte = SinedDateUtils.getDateProperty((filtro.getDtFim() != null ? filtro.getDtFim() : dataMaior), Calendar.YEAR);
		
		List<Controleorcamento> listaOrcamentos = controleorcamentoService.findForAnaliseRecDesp(filtro.getDtInicio(), filtro.getDtFim(), whereInEmpresa, whereInCentrocusto, whereInProjeto, whereInProjetotipo);
		
		if(StringUtils.isNotBlank(whereInProjeto)){
			AnaliseRecDespBean analiseRecDespBean = null;
			for(Controleorcamento controleorcamento : listaOrcamentos){
				if(SinedUtil.isListNotEmpty(controleorcamento.getListaControleorcamentoitem())){
					for(Controleorcamentoitem item : controleorcamento.getListaControleorcamentoitem()){
						if(item.getContagerencial() != null && item.getContagerencial().getVcontagerencial() != null && 
								item.getContagerencial().getVcontagerencial().getIdentificador() != null && 
								item.getContagerencial().getTipooperacao() != null &&
								!listaFolhas.contains(item)){
							analiseRecDespBean = new AnaliseRecDespBean(item.getContagerencial().getVcontagerencial().getIdentificador(), item.getContagerencial().getNome(), item.getContagerencial().getTipooperacao().getNome());
							analiseRecDespBean.setItemIncluidoControleOrcamento(Boolean.TRUE);
							listaFolhas.add(analiseRecDespBean);
						}
					}
				}
			}
		}
		
		listaCompleta = this.buscarArvoreContaGerencial(filtro);
		
		this.inserirFolhasNaListaCompleta(listaCompleta, listaFolhas);
		this.calcularValores(listaCompleta, !filtro.getContassemlancamentos());
		
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));

        HSSFRow headerRow = planilha.createRow((short) 0);
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Relat�rio an�lise de receitas e despesas");


		
		if (filtro.getRadProjeto().getId().equals("comProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cellSubHeader.setCellValue("PROJETO: Apenas com projeto");
		} 
		else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);			
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cellSubHeader.setCellValue("PROJETO: Apenas sem projeto");
		} 
		else if (filtro.getRadProjeto().getId().equals("escolherProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			try {
				cellSubHeader.setCellValue("PROJETO: "+projetoService.getNomeProjetos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaProjeto").invoke(filtro)));
			} catch (Exception e) {}	
		} 
		
		
		HSSFRow row = null;
		HSSFCell cell = null;
		StringBuilder totalizadorCreditosDebitos = new StringBuilder("SUM(");
		
		Calendar dtDe = Calendar.getInstance();
		Calendar dtAte = Calendar.getInstance();
		dtDe.setTimeInMillis(filtro.getDtInicio().getTime());
		dtAte.setTimeInMillis(filtro.getDtFim() != null ? filtro.getDtFim().getTime() : dataMaior.getTime());

		Calendar dtMenorCalAux1 = Calendar.getInstance();
		dtMenorCalAux1.setTimeInMillis(dtDe.getTimeInMillis());
		
		Calendar dtMenorCalAux2 = Calendar.getInstance();
		dtMenorCalAux2.setTimeInMillis(dtDe.getTimeInMillis());
		
		int meses = adicionaCabecalhosRelatorioExcel(planilha, row, cell, filtro, dtMenorCalAux1, listaOrcamentos, dataMaior);
		List<Integer> linhasTotaisOrcados = new ArrayList<Integer>();
		criaTabelaComDadosRelatorioExcel(planilha, row, cell, dtMenorCalAux2, dtDe, meses, totalizadorCreditosDebitos, listaCompleta, listaFolhas, listaOrcamentos, linhasTotaisOrcados);
		
		if (listaOrcamentos != null && listaOrcamentos.size() > 0)
			planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) (5 + (meses*2))));
		else planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) (2 + (meses*2))));
		
		boolean haveOrcamento = listaOrcamentos != null && listaOrcamentos.size() > 0;
		adicionaTotaisRelatorioExcel(planilha, row, cell, meses, totalizadorCreditosDebitos, haveOrcamento);
		

        HSSFRow rowTotalOrcado = planilha.createRow((short) planilha.getLastRowNum()-3);
		
		HSSFCell cellTotal = rowTotalOrcado.createCell((short) 0);
		cellTotal.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY); 
		cellTotal.setCellValue("Total");
		
		cellTotal = rowTotalOrcado.createCell((short) 1);
		cellTotal.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY); 

		for(int i=0; i<meses;i++){
			int colunaAtual = 2+(i*2)+1;
			if(!haveOrcamento){
				colunaAtual = 2+i;
			} else {
				//somat�rio coluna or�amento
				int colunaOrcamento = (2+(i*2));
				cellTotal = rowTotalOrcado.createCell((short) (colunaOrcamento));
				cellTotal.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY); 
				
				if(linhasTotaisOrcados != null && linhasTotaisOrcados.size() > 0){
					StringBuilder sum = new StringBuilder("SUM(");
					String colunaNoExcel = SinedExcel.getAlgarismoColuna(colunaOrcamento);
					for(Integer linha : linhasTotaisOrcados){
						sum.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(linha)).append(" = \"C\";0;(").append(colunaNoExcel+(linha)).append("*-1));");
						sum.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(linha)).append(" = \"D\";0;").append(colunaNoExcel+(linha)).append(");");
					}
					String sumStr = sum.substring(0, sum.length()-1) + ")";
					cellTotal.setCellFormula(sumStr);
				} else {
					cellTotal.setCellValue("0,00");
				}	
			}
			
			HSSFCell cellTotalOrcado = rowTotalOrcado.createCell((short) colunaAtual);
			cellTotalOrcado.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY); 
			
			if(linhasTotaisOrcados != null && linhasTotaisOrcados.size() > 0){
				StringBuilder sum = new StringBuilder("SUM(");
				String colunaNoExcel = SinedExcel.getAlgarismoColuna(colunaAtual);
				for(Integer linha : linhasTotaisOrcados){
					sum.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(linha)).append(" = \"C\";0;(").append(colunaNoExcel+(linha)).append("*-1));");
					sum.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(linha)).append(" = \"D\";0;").append(colunaNoExcel+(linha)).append(");");
				}
				String sumStr = sum.substring(0, sum.length()-1) + ")";
				cellTotalOrcado.setCellFormula(sumStr);
			} else {
				cellTotalOrcado.setCellValue("0,00");
			}
		}
		
		
		return wb.getWorkBookResource("analisereceitasdespesas_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * M�todo que gera relat�rio de analise de despesas em Excel por Centro de custo
	 *
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Resource gerarExcelAnaliseReceitaDespesaPorCentrocusto(AnaliseReceitaDespesaReportFiltro filtro) throws IOException{
		List<AnaliseRecDespBean> listaFolhas = null;
		List<AnaliseRecDespBean> listaCompleta = null;
		List<AnaliseRecDespBean> listaCompletaFinal = new ArrayList<AnaliseRecDespBean>();
		
		listaFolhas = this.buscarParaRelatorioAnaliseReceitaDespesaExcel(filtro);
		
		listaCompleta = this.buscarArvoreContaGerencial(filtro);
		
		if(filtro.getContassemlancamentos()){
			this.inserirFolhasNaListaCompletaPorCentroCusto(listaCompleta, listaFolhas, listaCompletaFinal);
		}else {
			listaCompletaFinal = listaFolhas;
		}
//		this.calcularValores(listaCompletaFinal, !filtro.getContassemlancamentos());

		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));
        
		HSSFRow headerRow = planilha.createRow((short) 0);
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Relat�rio an�lise de receitas e despesas");
		
		if (filtro.getRadProjeto().getId().equals("comProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cellSubHeader.setCellValue("PROJETO: Apenas com projeto");
		} 
		else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);			
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cellSubHeader.setCellValue("PROJETO: Apenas sem projeto");
		} 
		else if (filtro.getRadProjeto().getId().equals("escolherProjeto")) {
			HSSFRow subHeaderRow = planilha.createRow((short) 3);	
			HSSFCell cellSubHeader = subHeaderRow.createCell((short) 0);
			cellSubHeader.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			try {
				cellSubHeader.setCellValue("PROJETO: "+projetoService.getNomeProjetos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaProjeto").invoke(filtro)));
			} catch (Exception e) {}	
		}		
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		Calendar dtDe = Calendar.getInstance();
		Calendar dtAte = Calendar.getInstance();
		dtDe.setTimeInMillis(filtro.getDtInicio().getTime());
		dtAte.setTimeInMillis(filtro.getDtFim() != null ? filtro.getDtFim().getTime() : new Date(System.currentTimeMillis()).getTime());

		Calendar dtMenorCalAux = Calendar.getInstance();
		dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
		
		//cria cabe�alho e adiciona os dados ao relat�rio
		this.adicionaCabecalhosEDadosRelatorioExcelPorCentrocusto(planilha, row, cell, filtro, dtMenorCalAux, listaCompletaFinal);

		return wb.getWorkBookResource("analisereceitasdespesas_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * M�todo que adiciona as fileiras pais do relat�rio de analise e despesas excel
	 * 
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param dtMenorCalAux
	 * @param dtDe
	 * @param meses
	 * @param totalizadorCreditosDebitos
	 * @param listaCompleta
	 * @param listaFolhas
	 * @author Tom�s Rabelo
	 * @param listaOrcamentos 
	 */
	private void criaTabelaComDadosRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, Calendar dtMenorCalAux, Calendar dtDe, int meses, StringBuilder totalizadorCreditosDebitos, List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaFolhas, List<Controleorcamento> listaOrcamentos, List<Integer> linhasTotaisOrcados) {
		AnaliseRecDespBean aux;
		Integer rowPai = 0;
		
		boolean haveOrcamento = listaOrcamentos != null && listaOrcamentos.size() > 0;
		
		for (AnaliseRecDespBean analiseRecDespBean : listaCompleta) {
			if(analiseRecDespBean.getIdentificador().split("\\.").length == 1){
				rowPai = planilha.getLastRowNum()+1;
				linhasTotaisOrcados.add(rowPai+1);
				row = planilha.createRow(rowPai);
				analiseRecDespBean.setRowPai(rowPai);
			
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY);
				cell.setCellValue(analiseRecDespBean.getIdentificador()+" - "+analiseRecDespBean.getNome());

				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue(analiseRecDespBean.getOperacao());
				
				adicionaSubReceitas(listaCompleta, listaFolhas, analiseRecDespBean, planilha, row, cell, meses, dtMenorCalAux, dtDe, listaOrcamentos);
				
				//Verifica se h� algum valor para a conta gerencial
				int index = -1;
				for (int i = 0; i < listaFolhas.size(); i++) {
					if(listaFolhas.get(i).getIdentificador().equals(analiseRecDespBean.getIdentificador())){
						index = i;
						break;
					}
				}
				
				if(rowPai.equals(planilha.getLastRowNum())){
					aux = null;
					if(index >= 0)
						aux = listaFolhas.get(index);
					dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
					
					row = planilha.getRow(rowPai);
					if(haveOrcamento){
						for (int i = 0; i < (meses*2); i++) {
							Double valorOrcamento = controleorcamentoService.getValorOrcamento(analiseRecDespBean.getIdentificador(), dtMenorCalAux.get(Calendar.YEAR), dtMenorCalAux.get(Calendar.MONTH), listaOrcamentos);
							
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellValue(valorOrcamento);
							
							i++;
							
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							
							if(aux != null && aux.getMes() != null && aux.getAno() != null && dtMenorCalAux.get(Calendar.MONTH)+1 == aux.getMes() && dtMenorCalAux.get(Calendar.YEAR) == aux.getAno()){
								cell.setCellValue(aux.getValorDouble());
								index += index+1 >= listaFolhas.size() ? 0 : 1; 
								aux = listaFolhas.get(index);
								if(!aux.getIdentificador().equals(analiseRecDespBean.getIdentificador()))
									aux = null;
							}else{
								cell.setCellValue(0.0);
							}
							dtMenorCalAux.add(Calendar.MONTH, 1);
						}
					} else {
						for (int i = 0; i < meses; i++) {
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							
							if(aux != null && aux.getMes() != null && dtMenorCalAux.get(Calendar.MONTH)+1 == aux.getMes() && dtMenorCalAux.get(Calendar.YEAR) == aux.getAno()){
								cell.setCellValue(aux.getValorDouble());
								index += index+1 >= listaFolhas.size() ? 0 : 1; 
								aux = listaFolhas.get(index);
								if(!aux.getIdentificador().equals(analiseRecDespBean.getIdentificador()))
									aux = null;
							}else{
								cell.setCellValue(0.0);
							}
							dtMenorCalAux.add(Calendar.MONTH, 1);
						}
					}
				} else {
					row = planilha.getRow(rowPai);
					if(haveOrcamento){
						for (int i = 0; i < (meses*2); i++) {
							Double valorOrcamento = controleorcamentoService.getValorOrcamento(analiseRecDespBean.getIdentificador(), dtMenorCalAux.get(Calendar.YEAR), dtMenorCalAux.get(Calendar.MONTH), listaOrcamentos);
							
							if(valorOrcamento != null && valorOrcamento > 0d){
								cell = row.createCell((short) (2+i));
								cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
								cell.setCellValue(valorOrcamento);
							}
							
							i++;
							
							dtMenorCalAux.add(Calendar.MONTH, 1);
						}
					}
				}
				
				row = planilha.getRow(rowPai);
				if(haveOrcamento){
					int indexTotalOrcado = (meses*2)+3;
					int indexPercentualOrcado = (meses*2)+2;
					int indexTotal = (meses*2)+5;
					int indexPercentual = (meses*2)+4;
					for (int i = 0; i < (meses*2); i++) {
						i++;
						cell = row.getCell((short) (indexTotalOrcado));
						if(cell == null){
							cell = row.createCell((short) (indexTotalOrcado));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1+i)+(rowPai+1)+")");
						}else{
							String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(1+i)+(rowPai+1)+")";
							cell = row.createCell((short) (indexTotalOrcado));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula(formula);
						}
						
						cell = row.getCell((short) (indexTotal));
						if(cell == null){
							cell = row.createCell((short) (indexTotal));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(2+i)+(rowPai+1)+")");
						}else{
							String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(2+i)+(rowPai+1)+")";
							cell = row.createCell((short) (indexTotal));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula(formula);
						}
					}
					
					//C�lula da raiz � sempre 100%
					cell = row.createCell((short) (indexPercentualOrcado));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
					cell.setCellValue(1);
					
					//C�lula da raiz � sempre 100%
					cell = row.createCell((short) (indexPercentual));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
					cell.setCellValue(1);
				} else {
					for (int i = 0; i < meses; i++) {
						cell = row.getCell((short) (meses+3));
						if(cell == null){
							cell = row.createCell((short) (meses+3));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(2+i)+(rowPai+1)+")");
						}else{
							String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(2+i)+(rowPai+1)+")";
							cell = row.createCell((short) (meses+3));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
							cell.setCellFormula(formula);
						}
					}
					
					//C�lula da raiz � sempre 100%
					cell = row.createCell((short) (meses+2));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
					cell.setCellValue(1);
				}
				
				if(haveOrcamento){
					totalizadorCreditosDebitos.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(rowPai+1)).append(" = \"D\";0;").append(SinedExcel.getAlgarismoColuna((meses*2)+5)+(rowPai+1)).append("); ");
				} else {
					totalizadorCreditosDebitos.append("if(").append(SinedExcel.getAlgarismoColuna(1)+(rowPai+1)).append(" = \"D\";0;").append(SinedExcel.getAlgarismoColuna(meses+3)+(rowPai+1)).append("); ");
				}
			}
		}
	}
	
	/**
	 * M�todo que adiciona cabe�alhos ao relat�rio de analise de despesas excel
	 * 
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param filtro
	 * @param dtMenorCalAux
	 * @return
	 * @author Tom�s Rabelo
	 * @param listaOrcamentos 
	 * @param dataMaior 
	 */
	private int adicionaCabecalhosRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, AnaliseReceitaDespesaReportFiltro filtro, Calendar dtMenorCalAux, List<Controleorcamento> listaOrcamentos, Date dataMaior) {
		boolean haveOrcamento = listaOrcamentos != null && listaOrcamentos.size() > 0;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
		row = planilha.createRow(4);
		
		SinedExcel.createHeaderCell(row, 0, "Conta Gerencial");
		SinedExcel.createHeaderCell(row, 1, "OP");
		
		Date dtInicioAux = SinedDateUtils.firstDateOfMonth(filtro.getDtInicio());
		Date dtFimAux = filtro.getDtFim() != null ? SinedDateUtils.lastDateOfMonth(filtro.getDtFim()) : SinedDateUtils.lastDateOfMonth(dataMaior);
		
		int meses = SinedDateUtils.mesesEntre(dtInicioAux, dtFimAux) + 1;
		
		if(haveOrcamento){
			for (int i = 0; i < (meses*2); i++) {
				cell = row.createCell((short) (i+2));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(dateFormat.format(dtMenorCalAux.getTime()) + " (O)");
				
				i++;
				
				cell = row.createCell((short) (i+2));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(dateFormat.format(dtMenorCalAux.getTime()) + " (R)");
				
				dtMenorCalAux.add(Calendar.MONTH, 1);
			}
			//Cabe�alho totalizador Total
			row = planilha.getRow(4);
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "% (O)");
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "Total (O)");
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "% (R)");
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "Total (R)");
		} else {
			for (int i = 0; i < meses; i++) {
				cell = row.createCell((short) (i+2));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(dateFormat.format(dtMenorCalAux.getTime()));
				dtMenorCalAux.add(Calendar.MONTH, 1);
			}
			//Cabe�alho totalizador Total
			row = planilha.getRow(4);
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "%");
			SinedExcel.createHeaderCell(row, (row.getLastCellNum()+1), "Total");
		}
		
		return meses;
	}
	
	/**
	 * M�todo que adiciona o cabe�alho e os dados do relat�rio por centro de custo
	 *
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param filtro
	 * @param dtMenorCalAux
	 * @param listaCompleta
	 * @author Luiz Fernando
	 * @param listaCompleta2 
	 */
	private void adicionaCabecalhosEDadosRelatorioExcelPorCentrocusto(HSSFSheet planilha, HSSFRow row, HSSFCell cell, AnaliseReceitaDespesaReportFiltro filtro, Calendar dtMenorCalAux, List<AnaliseRecDespBean> listaCompletaFinal) {
		Map<Integer, Integer> mapCentrocustoColuna = new HashMap<Integer, Integer>();
		Map<String, Integer> mapContagerencialColuna = new HashMap<String, Integer>();
		
		Map<String, Integer> mapContagerencialPaiTotal = new HashMap<String, Integer>();
		Map<String, Integer> mapContagerencialPai = new HashMap<String, Integer>();
		Map<String, List<Integer>> mapContagerencialPaiLinhasFilhas = new HashMap<String, List<Integer>>();
		
		Map<MultiKey, String> mapFormulaContagerencialPai = new HashMap<MultiKey, String>();
		
		row = planilha.createRow(4);
		
		HSSFRow rowdados;
		SinedExcel.createHeaderCell(row, 0, "Conta Gerencial");
		
		Integer col = 1;
		Integer lin = 5;
		for(AnaliseRecDespBean bean : listaCompletaFinal){
			if(mapCentrocustoColuna.get(bean.getCdcentrocusto()) == null){
				mapCentrocustoColuna.put(bean.getCdcentrocusto(), col);
				SinedExcel.createHeaderCell(row, col, bean.getNomecentrocusto());
				SinedExcel.createHeaderCell(row, col+1, "%");
				col = col + 2;
				if(mapContagerencialColuna.get(bean.getIdentificador()) == null){
					String identificadorContaPaiSuperior = null;
					if(bean.getIdentificador() != null && bean.getCdcontagerencial() != null && bean.getIdentificador().indexOf(".") != -1){
						String[] identificadorBean = bean.getIdentificador().split("\\.");
						for(int i = 0; i < identificadorBean.length-1; i++){
							//feito isto para adicionar todas as contas gerenciais pais
							StringBuilder identificadorContaPai = new StringBuilder();
							identificadorContaPaiSuperior = null;
							for(int j = 0; j <= i ; j++){
								if(j == i && !"".equals(identificadorContaPai.toString())){
									identificadorContaPaiSuperior = identificadorContaPai.substring(0, identificadorContaPai.length()-1);
								}
								identificadorContaPai.append(identificadorBean[j]).append(".");
							}
							
							String identificadorPai = identificadorContaPai.substring(0, identificadorContaPai.length()-1);
							if(mapContagerencialColuna.get(identificadorPai) == null){
								Contagerencial contagerencialpai = this.findByIdentificador(identificadorPai);
								if(contagerencialpai != null){
									rowdados = planilha.createRow(lin);					
									cell = rowdados.createCell((short) 0);
									cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
									cell.setCellValue(identificadorPai+" - "+contagerencialpai.getNome());					
									mapContagerencialColuna.put(identificadorPai, lin);
									mapContagerencialPai.put(identificadorPai, lin);
									if(i == 0){
										mapContagerencialPaiTotal.put(identificadorPai, lin);
									}
									lin++;
									if(i == identificadorBean.length-2){
										mapContagerencialPaiLinhasFilhas.put(identificadorPai, new ArrayList<Integer>());
										mapContagerencialPaiLinhasFilhas.get(identificadorPai).add(lin);
									}
								}
							}
							if(identificadorContaPaiSuperior != null){
								if(mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior) == null){
									mapContagerencialPaiLinhasFilhas.put(identificadorContaPaiSuperior, new ArrayList<Integer>());
								}
								if(mapContagerencialColuna.get(identificadorPai) != null && !mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).contains(mapContagerencialColuna.get(identificadorPai))){
									mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).add(mapContagerencialColuna.get(identificadorPai));
								}
							}
							if(i+1 == identificadorBean.length-1 && identificadorContaPaiSuperior != null){
								identificadorContaPaiSuperior += "."+identificadorBean[i];
							}else if(identificadorBean.length == 2){
								identificadorContaPaiSuperior = identificadorPai;
							}
						}
					}else if(bean.getIdentificador() != null && bean.getCdcontagerencial() != null){
						mapContagerencialPaiTotal.put(bean.getIdentificador(), lin);
					}
					rowdados = planilha.createRow(lin);					
					cell = rowdados.createCell((short) 0);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					cell.setCellValue(bean.getIdentificador()+" - "+bean.getNome());					
					mapContagerencialColuna.put(bean.getIdentificador(), lin);
					if(identificadorContaPaiSuperior != null){
						if(mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior) == null){
							mapContagerencialPaiLinhasFilhas.put(identificadorContaPaiSuperior, new ArrayList<Integer>());
						}
						if(mapContagerencialColuna.get(bean.getIdentificador()) != null && !mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).contains(mapContagerencialColuna.get(bean.getIdentificador()))){
							mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).add(lin);
						}
					}
					lin++;
				}
			}else {
				if(mapContagerencialColuna.get(bean.getIdentificador()) == null){
					String identificadorContaPaiSuperior = null;
					if(bean.getIdentificador() != null && bean.getCdcontagerencial() != null && bean.getIdentificador().indexOf(".") != -1){
						String[] identificadorBean = bean.getIdentificador().split("\\.");
						for(int i = 0; i < identificadorBean.length-1; i++){
							StringBuilder identificadorContaPai = new StringBuilder();
							identificadorContaPaiSuperior = null;
							for(int j = 0; j <= i ; j++){
								if(j == i && !"".equals(identificadorContaPai.toString())){
									identificadorContaPaiSuperior = identificadorContaPai.substring(0, identificadorContaPai.length()-1);
								}
								identificadorContaPai.append(identificadorBean[j]).append(".");
							}
							
							String identificadorPai = identificadorContaPai.substring(0, identificadorContaPai.length()-1);
							if(mapContagerencialColuna.get(identificadorPai) == null){
								Contagerencial contagerencialpai = this.findByIdentificador(identificadorPai);
								if(contagerencialpai != null){
									rowdados = planilha.createRow(lin);					
									cell = rowdados.createCell((short) 0);
									cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
									cell.setCellValue(identificadorPai+" - "+contagerencialpai.getNome());					
									mapContagerencialColuna.put(identificadorPai, lin);
									mapContagerencialPai.put(identificadorPai, lin);
									if(i == 0){
										mapContagerencialPaiTotal.put(identificadorPai, lin);
									}
									lin++;
									if(i == identificadorBean.length-2){
										mapContagerencialPaiLinhasFilhas.put(identificadorPai, new ArrayList<Integer>());
										mapContagerencialPaiLinhasFilhas.get(identificadorPai).add(lin);
									}
								}
							}
							if(identificadorContaPaiSuperior != null){
								if(mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior) == null){
									mapContagerencialPaiLinhasFilhas.put(identificadorContaPaiSuperior, new ArrayList<Integer>());
								}
								if(mapContagerencialColuna.get(identificadorPai) != null && !mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).contains(mapContagerencialColuna.get(identificadorPai))){
									mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).add(mapContagerencialColuna.get(identificadorPai));
								}
							}
							if(i+1 == identificadorBean.length-1 && identificadorContaPaiSuperior != null){
								identificadorContaPaiSuperior += "."+identificadorBean[i];
							}else if(identificadorBean.length == 2){
								identificadorContaPaiSuperior = identificadorPai;
							}
						}
					}else if(bean.getIdentificador() != null && bean.getCdcontagerencial() != null){
						mapContagerencialPaiTotal.put(bean.getIdentificador(), lin);
					}
					rowdados = planilha.createRow(lin);					
					cell = rowdados.createCell((short) 0);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					cell.setCellValue(bean.getIdentificador()+" - "+bean.getNome());					
					mapContagerencialColuna.put(bean.getIdentificador(), lin);
					if(identificadorContaPaiSuperior != null){
						if(mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior) == null){
							mapContagerencialPaiLinhasFilhas.put(identificadorContaPaiSuperior, new ArrayList<Integer>());
						}
						if(mapContagerencialColuna.get(bean.getIdentificador()) != null && !mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).contains(mapContagerencialColuna.get(bean.getIdentificador()))){
							mapContagerencialPaiLinhasFilhas.get(identificadorContaPaiSuperior).add(lin);
						}
					}
					lin++;
				}
			}
		}
		
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) col.intValue()));
		
		SinedExcel.createHeaderCell(row, col, "TOTAL");
		Integer ultimaColuna = col;
		
		HSSFRow rowcriacao;
		HSSFRow rowtotal = planilha.createRow(lin);
		for(int i = 5; i < lin; i++){
			for(int j = 1; j < col; j++){
				if(planilha.getRow(i) == null)
					rowcriacao = planilha.createRow(i);
				else {
					rowcriacao = planilha.getRow(i);
				}
				
				cell = rowcriacao.createCell(((short)j));
				if(j % 2 == 0){
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
				}else {
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				}
				cell.setCellValue(0);
				
				
			}
		}
		
		HSSFRow rowtotalcg;
		StringBuilder soma;
		//TOTAIS POR CONTA GERENCIAL
		for(int i = 5; i < lin; i++){
			rowtotalcg = planilha.getRow(i);
			cell = rowtotalcg.createCell(((short) col.intValue()));		
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			soma = new StringBuilder();
			for(int j = 1; j < col; j++){
				if(j % 2 != 0){
					if(!"".equals(soma.toString())) soma.append(";");
					soma.append(SinedExcel.getAlgarismoColuna(j) + (i+1));
				}
				
			}
			if(!"".equals(soma.toString())){
				cell.setCellFormula("SUM(" + soma.toString() + ")");
			}else {
				cell.setCellValue(0);
			}
		}
		
		cell = rowtotal.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue("TOTAL");
		
		//TOTAIS POR CENTRO DE CUSTO
		StringBuilder somaPai;
		for(int j = 1; j <= col; j++){
			somaPai = new StringBuilder();
			for(Integer linContagerencialPai : mapContagerencialPaiTotal.values()){
				if(!"".equals(somaPai.toString())) somaPai.append(";");
				somaPai.append(SinedExcel.getAlgarismoColuna(j) + (linContagerencialPai+1));
			}
			cell = rowtotal.createCell(((short) j));
			if(j % 2 == 0){
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			}else {
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellFormula("SUM(" + (StringUtils.isNotBlank(somaPai.toString()) ? somaPai.toString() : "0") + ")");
			}
		}
		
		
		
		for(AnaliseRecDespBean bean : listaCompletaFinal){
			if(bean != null && bean.getIdentificador() != null && mapContagerencialColuna.get(bean.getIdentificador()) != null){
				row = planilha.getRow(mapContagerencialColuna.get(bean.getIdentificador()));
				col = mapCentrocustoColuna.get(bean.getCdcentrocusto());
				
				String identificadorPai = StringUtils.deleteWhitespace(bean.getIdentificador());

				if (identificadorPai.split("\\.").length > 1) {
					identificadorPai = identificadorPai.substring( 0, (identificadorPai.length() - (identificadorPai.split("\\.")[identificadorPai.split("\\.").length-1].length() + 1)) );
				}
				Integer linhaPai = mapContagerencialColuna.containsKey(identificadorPai) ? mapContagerencialColuna.get(identificadorPai) : 0;
				
				// CRIA A COLUNA DO VALOR
	        	cell = row.getCell((short) (col.intValue()));
	            cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
	            if("D".equals(bean.getOperacao())){
	            	cell.setCellValue(-bean.getValorDouble());
	            }else {
	            	cell.setCellValue(bean.getValorDouble());
	            }
	            
	            // CRIA A COLUNA DO PERCENTUAL
	            cell = row.getCell((short) (col+1));
	            cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		        cell.setCellFormula("IF(IF(AND(" + SinedExcel.getAlgarismoColuna(col) + (lin+1) + "<>0,"+ SinedExcel.getAlgarismoColuna(col) + (linhaPai+1) + "<>0);" + SinedExcel.getAlgarismoColuna(col) + ((mapContagerencialColuna.get(bean.getIdentificador()))+1) + "/" + SinedExcel.getAlgarismoColuna(col) + (linhaPai+1) + ";0) > 0;IF(" + SinedExcel.getAlgarismoColuna(col) + (lin+1) + "<>0;" + SinedExcel.getAlgarismoColuna(col) + ((mapContagerencialColuna.get(bean.getIdentificador()))+1) + "/" + SinedExcel.getAlgarismoColuna(col) + (linhaPai+1) + ";0);0)");
		        
		        while(identificadorPai.split("\\.").length > 0){
		        	String identificadorFilho = identificadorPai;
		        	if (identificadorPai.split("\\.").length > 1) {
						identificadorPai = identificadorPai.substring( 0, (identificadorPai.length() - (identificadorPai.split("\\.")[identificadorPai.split("\\.").length-1].length() + 1)) );
					} else {
						identificadorPai = identificadorFilho;
					}
		        	
		        	Integer linhaPai2 = mapContagerencialColuna.containsKey(identificadorPai) ? mapContagerencialColuna.get(identificadorPai) : 0;
		        	Integer linhaFilho2 = mapContagerencialColuna.containsKey(identificadorFilho) ? mapContagerencialColuna.get(identificadorFilho) : 0; 
		        	
		        	String formulaPai = "IF(IF(" + SinedExcel.getAlgarismoColuna(col) + (lin+1) + "<>0;" + SinedExcel.getAlgarismoColuna(col) + ((linhaFilho2)+1) + "/" + SinedExcel.getAlgarismoColuna(col) + (linhaPai2+1) + ";0) > 0;IF(" + SinedExcel.getAlgarismoColuna(col) + (lin+1) + "<>0;" + SinedExcel.getAlgarismoColuna(col) + ((linhaFilho2)+1) + "/" + SinedExcel.getAlgarismoColuna(col) + (linhaPai2+1) + ";0);0)";
		        	
		        	MultiKey key = new MultiKey(identificadorFilho, SinedExcel.getAlgarismoColuna(col+1) + mapContagerencialColuna.get(identificadorFilho));
		        	mapFormulaContagerencialPai.put(key, formulaPai);
		        	
		        	if(identificadorPai.equals(identificadorFilho)) break;
		        }
			}
		}
		
		String formula = null;
		for(int j = 1; j < ultimaColuna; j++){
			somaPai = new StringBuilder();
			for(String identificadorPai : mapContagerencialPai.keySet()){
				List<Integer> listaLinhaFilhas = mapContagerencialPaiLinhasFilhas.get(identificadorPai);
				if(listaLinhaFilhas != null && !listaLinhaFilhas.isEmpty()){
//					Integer inicio = listaLinhaFilhas.get(0);
//					Integer fim = listaLinhaFilhas.get(listaLinhaFilhas.size()-1);
					
					row = planilha.getRow(mapContagerencialColuna.get(identificadorPai));
					
					// CRIA A COLUNA DO VALOR
		        	cell = row.getCell((short) (j));
					if(j % 2 == 0){
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
					}else {
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
					}
//					cell.setCellFormula("SUM(" + SinedExcel.getAlgarismoColuna(j) + (inicio+1) + ":" + SinedExcel.getAlgarismoColuna(j) + (fim+1) + ")");
					
					StringBuilder somaLinhaPai = new StringBuilder();
					for(Integer linhaFilha : listaLinhaFilhas){
						if(!"".equals(somaLinhaPai.toString())) somaLinhaPai.append(";");
						somaLinhaPai.append(SinedExcel.getAlgarismoColuna(j) + (linhaFilha+1));
					}
					
					if(j % 2 == 0){
						formula = mapFormulaContagerencialPai.get(new MultiKey(identificadorPai, SinedExcel.getAlgarismoColuna(j) + mapContagerencialColuna.get(identificadorPai)));
						cell.setCellFormula(formula != null ? formula : "0");
					}else {
						cell.setCellFormula("SUM(" + somaLinhaPai + ")");
					}
				}
				
			}
		}
	}
	
	/**
	 * M�todo que adiciona totais (cr�dito, d�bito e geral) no relat�rio de analise de receitas e despesas excel
	 * 
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param meses
	 * @param totalizadorCreditos
	 * @author Tom�s Rabelo
	 * @param haveOrcamento 
	 */
	private void adicionaTotaisRelatorioExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, int meses, StringBuilder totalizadorCreditos, boolean haveOrcamento) {
		if(haveOrcamento) meses = meses*2;

		if(!totalizadorCreditos.toString().equals("SUM(")){
			totalizadorCreditos.replace(totalizadorCreditos.length()-2, totalizadorCreditos.length()-1, ")");
			row = planilha.createRow(planilha.getLastRowNum()+2);
			planilha.addMergedRegion(new Region(planilha.getLastRowNum(), (short) (meses+3), planilha.getLastRowNum(), (short) (meses+4)));
			cell = row.createCell((short) (meses+4));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
			
			cell = row.createCell((short) (meses+3));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
			cell.setCellValue("TOTAL DE CR�DITOS");
	
			cell = row.createCell((short) (meses+5));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
			cell.setCellFormula(totalizadorCreditos.toString());
	
			row = planilha.createRow(planilha.getLastRowNum()+1);
			planilha.addMergedRegion(new Region(planilha.getLastRowNum(), (short) (meses+3), planilha.getLastRowNum(), (short) (meses+4)));
			cell = row.createCell((short) (meses+4));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
	
			cell = row.createCell((short) (meses+3));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
			cell.setCellValue("TOTAL DE D�BITOS");
			
			cell = row.createCell((short) (meses+5));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
			cell.setCellFormula(totalizadorCreditos.toString().replaceAll("\"D\"", "\"C\""));
	
			row = planilha.createRow(planilha.getLastRowNum()+1);
			planilha.addMergedRegion(new Region(planilha.getLastRowNum(), (short) (meses+3), planilha.getLastRowNum(), (short) (meses+4)));
			cell = row.createCell((short) (meses+4));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
	
			cell = row.createCell((short) (meses+3));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT);
			cell.setCellValue("TOTAL");
			
			
			cell = row.createCell((short) (meses+5));
			cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
			cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(meses+5)+(planilha.getLastRowNum()-1)+";("+SinedExcel.getAlgarismoColuna(meses+5)+(planilha.getLastRowNum())+"*-1))");
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<AnaliseRecDespBean> buscarParaRelatorioAnaliseReceitaDespesaExcel(AnaliseReceitaDespesaReportFiltro filtro) {
		return contagerencialDAO.buscarParaRelatorioAnaliseReceitaDespesaExcel(filtro);
	}
	
	/**
	 * M�todo recursivo
	 * 
	 * @param listaCompleta
	 * @param listaFolhas
	 * @param analiseRecDespBeanPai
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param meses
	 * @param dtMenorCalAux
	 * @param dtDe
	 * @author Tom�s Rabelo
	 * @param listaOrcamentos 
	 */
	private void adicionaSubReceitas(List<AnaliseRecDespBean> listaCompleta, List<AnaliseRecDespBean> listaFolhas, AnaliseRecDespBean analiseRecDespBeanPai, HSSFSheet planilha, HSSFRow row, HSSFCell cell, int meses, Calendar dtMenorCalAux, Calendar dtDe, List<Controleorcamento> listaOrcamentos) {
		Integer rowPai = 0;
		boolean haveOrcamento = listaOrcamentos != null && listaOrcamentos.size() > 0;
		
		for (AnaliseRecDespBean analiseAux : listaCompleta) {
			
			//Para entrar na condi��o, n�o pode ser a analise em quest�o, deve ter o come�o do identificador igual ao do pai e ser o pr�ximo n�vel
			if(!analiseAux.getIdentificador().equals(analiseRecDespBeanPai.getIdentificador()) && analiseAux.getIdentificador().indexOf(analiseRecDespBeanPai.getIdentificador()) == 0 && analiseRecDespBeanPai.getIdentificador().split("\\.").length == analiseAux.getIdentificador().split("\\.").length-1){
				dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());				
				
				rowPai = planilha.getLastRowNum()+1;
				row = planilha.createRow(rowPai);
				analiseAux.setRowPai(rowPai);
				
				cell = row.createCell((short) (0));
				cell.setCellValue(analiseAux.getIdentificador()+" - "+analiseAux.getNome());

				cell = row.createCell((short) (1));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue(analiseAux.getOperacao());
				
				//Colocar chamada recusriva
				adicionaSubReceitas(listaCompleta, listaFolhas, analiseAux, planilha, row, cell, meses, dtMenorCalAux, dtDe, listaOrcamentos);
				
				//Verifica se h� algum valor para a conta gerencial
				int index = -1;
				for (int i = 0; i < listaFolhas.size(); i++) {
					if(listaFolhas.get(i).getIdentificador().equals(analiseAux.getIdentificador())){
						index = i;
						break;
					}
				}
				AnaliseRecDespBean aux = null;

				//Se for �ltima coluna � filho, se n�o � pai
				if(rowPai.equals(planilha.getLastRowNum())){
					if(index >= 0)
						aux = listaFolhas.get(index);
					dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
					
					row = planilha.getRow(rowPai);
					if(haveOrcamento){
						for (int i = 0; i < (meses*2); i++) {
							Double valorOrcamento = controleorcamentoService.getValorOrcamento(analiseAux.getIdentificador(), dtMenorCalAux.get(Calendar.YEAR), dtMenorCalAux.get(Calendar.MONTH), listaOrcamentos);
							
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
							cell.setCellValue(valorOrcamento);
							
//							adicionaSomatorioPai(planilha, row, cell, analiseRecDespBeanPai, i, rowPai, meses*2);
							
							i++;
							 
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
							
							if(aux != null && aux.getMes() != null && aux.getAno() != null && dtMenorCalAux.get(Calendar.MONTH)+1 == aux.getMes() && dtMenorCalAux.get(Calendar.YEAR) == aux.getAno()){
								cell.setCellValue(aux.getValorDouble());
								index += index+1 >= listaFolhas.size() ? 0 : 1; 
								aux = listaFolhas.get(index);
								if(!aux.getIdentificador().equals(analiseAux.getIdentificador()))
									aux = null;
							}else{
								cell.setCellValue(0.0);
							}
							dtMenorCalAux.add(Calendar.MONTH, 1);
							adicionaSomatorioPai(planilha, row, cell, analiseRecDespBeanPai, i, rowPai, meses*2, haveOrcamento);
						}
						
						adicionaPercentual(cell, row, meses*2, rowPai, analiseRecDespBeanPai.getRowPai(), haveOrcamento);
					} else {
						for (int i = 0; i < meses; i++) {
							cell = row.createCell((short) (2+i));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
							
							if(aux != null && aux.getMes() != null && dtMenorCalAux.get(Calendar.MONTH)+1 == aux.getMes() && dtMenorCalAux.get(Calendar.YEAR) == aux.getAno()){
								cell.setCellValue(aux.getValorDouble());
								index += index+1 >= listaFolhas.size() ? 0 : 1; 
								aux = listaFolhas.get(index);
								if(!aux.getIdentificador().equals(analiseAux.getIdentificador()))
									aux = null;
							}else{
								cell.setCellValue(0.0);
							}
							dtMenorCalAux.add(Calendar.MONTH, 1);
							adicionaSomatorioPai(planilha, row, cell, analiseRecDespBeanPai, i, rowPai, meses, haveOrcamento);
						}
						
						adicionaPercentual(cell, row, meses, rowPai, analiseRecDespBeanPai.getRowPai(), haveOrcamento);
					}
					
					
				}else{
					dtMenorCalAux.setTimeInMillis(dtDe.getTimeInMillis());
					
					row = planilha.getRow(rowPai);
					if(haveOrcamento){
						for (int i = 0; i < (meses*2); i++) {
							Double valorOrcamento = controleorcamentoService.getValorOrcamento(analiseAux.getIdentificador(), dtMenorCalAux.get(Calendar.YEAR), dtMenorCalAux.get(Calendar.MONTH), listaOrcamentos);
							
							if(valorOrcamento != null && valorOrcamento > 0d){
								cell = row.createCell((short) (2+i));
								cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
								cell.setCellValue(valorOrcamento);
							}
							
							i++;
							dtMenorCalAux.add(Calendar.MONTH, 1);
							adicionaSomatorioPai(planilha, row, cell, analiseRecDespBeanPai, i, rowPai, (meses*2), haveOrcamento);
						}
						
						adicionaPercentual(cell, row, (meses*2), rowPai, analiseRecDespBeanPai.getRowPai(), haveOrcamento);
					} else {
						for (int i = 0; i < meses; i++) {
							adicionaSomatorioPai(planilha, row, cell, analiseRecDespBeanPai, i, rowPai, meses, haveOrcamento);
							dtMenorCalAux.add(Calendar.MONTH, 1);
						}
						
						adicionaPercentual(cell, row, meses, rowPai, analiseRecDespBeanPai.getRowPai(), haveOrcamento);
					}
				}
				
			}
		}
	}
	
	/**
	 * M�todo que preenche a coluna % de todos os registros (folhas e subpais), menos da raiz
	 * 
	 * @param cell
	 * @param row
	 * @param meses
	 * @param rowFilho
	 * @param rowPai
	 * @author Tom�s Rabelo
	 */
	private void adicionaPercentual(HSSFCell cell, HSSFRow row, int meses, Integer rowFilho, Integer rowPai, boolean haveOrcamento) {
		int indexPercentual = meses+2;
		int indexValor = meses+3;
		if(haveOrcamento){
			cell = row.getCell((short) (indexPercentual));
			if(cell == null){
				cell = row.createCell((short) (indexPercentual));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
				cell.setCellFormula("IF(" + SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1) + ">0;" + SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1)+"/"+SinedExcel.getAlgarismoColuna(indexValor)+(rowPai+1) + ";0)");
			}else{
				String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+"/"+SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1);
				cell = row.createCell((short) (indexValor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
				cell.setCellFormula(formula);
			}
			indexPercentual += 2;
			indexValor += 2;
		}
		cell = row.getCell((short) (indexPercentual));
		if(cell == null){
			cell = row.createCell((short) (indexPercentual));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
			cell.setCellFormula("IF(" + SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1) + ">0;" + SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1)+"/"+SinedExcel.getAlgarismoColuna(indexValor)+(rowPai+1) + ";0)");
		}else{
			String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+"/"+SinedExcel.getAlgarismoColuna(indexValor)+(rowFilho+1);
			cell = row.createCell((short) (indexValor));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_PERCENT);
			cell.setCellFormula(formula);
		}
	}
	
	/**
	 * M�todo que coloca somat�rio nas colunas pais do relat�rio de analise de despesas
	 * 
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param analiseRecDespBeanPai
	 * @param i
	 * @param rowFilho
	 * @author Tom�s Rabelo
	 * @param meses 
	 */
	private void adicionaSomatorioPai(HSSFSheet planilha, HSSFRow row, HSSFCell cell, AnaliseRecDespBean analiseRecDespBeanPai, int i, Integer rowFilho, int meses, boolean haveOrcamento) {
		//Totalizador do final
		row = planilha.getRow(rowFilho);
		int indexTotal = meses+3;
		if(haveOrcamento){
			cell = row.getCell((short) (indexTotal));
			if(cell == null){
				cell = row.createCell((short) (indexTotal));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
				cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1+i)+(rowFilho+1)+")");
			}else{
				String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(1+i)+(rowFilho+1)+")";
				cell = row.createCell((short) (indexTotal));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
				cell.setCellFormula(formula);
			}
			indexTotal += 2;
		}
		cell = row.getCell((short) (indexTotal));
		if(cell == null){
			cell = row.createCell((short) (indexTotal));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
			cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(2+i)+(rowFilho+1)+")");
		}else{
			String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(2+i)+(rowFilho+1)+")";
			cell = row.createCell((short) (indexTotal));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
			cell.setCellFormula(formula);
		}

		if(haveOrcamento){
			row = planilha.getRow(analiseRecDespBeanPai.getRowPai());
			cell = row.getCell((short) (1+i));
			if(cell == null){
				cell = row.createCell((short) (1+i));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
				cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1+i)+(rowFilho+1)+")");
			}else{
				String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(1+i)+(rowFilho+1)+")";
				cell = row.createCell((short) (1+i));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
				cell.setCellFormula(formula);
			}
		}
		//Recupera fileira do paiz�o e adiciona soma	
		row = planilha.getRow(analiseRecDespBeanPai.getRowPai());
		cell = row.getCell((short) (2+i));
		if(cell == null){
			cell = row.createCell((short) (2+i));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
			cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(2+i)+(rowFilho+1)+")");
		}else{
			String formula = cell.getCellFormula().substring(0, cell.getCellFormula().length()-1)+";"+SinedExcel.getAlgarismoColuna(2+i)+(rowFilho+1)+")";
			cell = row.createCell((short) (2+i));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_GREY_MONEY);
			cell.setCellFormula(formula);
		}
	}
	
	public List<Contagerencial> findAutoCompleteCfop(String q){
  		String cfoptipo = null;
		try {
			cfoptipo = NeoWeb.getRequestContext().getParameter("cfoptipo");
			if(Cfoptipo.ENTRADA.equals(Integer.parseInt(cfoptipo))){
				return findAutocomplete(q,Tipooperacao.TIPO_CREDITO);
			}else {
				return findAutocomplete(q,Tipooperacao.TIPO_DEBITO);
			}
		} catch (Exception e) {
			return findAutocomplete(q);
		} 
	}
	
	public List<Contagerencial> findAutocomplete(String q, Tipooperacao tipooperacao) {
		return contagerencialDAO.findAutocomplete(q, tipooperacao, null, null);
	}
	
	public List<Contagerencial> findAutocomplete(String q, Tipooperacao tipooperacao, String whereInNatureza, String whereInCdcontagerencial) {
		return contagerencialDAO.findAutocomplete(q, tipooperacao, whereInNatureza, whereInCdcontagerencial);
	}
	
	public List<Contagerencial> findAutocompleteDebitoTodos(String q) {
		return contagerencialDAO.findAutocompleteTodos(q, Tipooperacao.TIPO_DEBITO);
	}
	
//	public List<Contagerencial> findTreeView(Tipooperacao tipooperacao) {
//		return findTreeView(tipooperacao, null, null);
//	}
	
	/**
	 * M�todo que carrega a lista de conta gerencial para a popup
	 *
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
//	public List<Contagerencial> findTreeView(Tipooperacao tipooperacao, String whereInNatureza) {
//		return findTreeView(tipooperacao, null, whereInNatureza);
//	}
	
	/**
	 * M�todo que carrega a lista de conta gerencial para a popup
	 *
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findTreeView(Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza) {
		List<Contagerencial> lista = this.findByTipooperacaoNatureza(tipooperacao, whereInCdcontagerencial, whereInNatureza, whereNotInNatureza);
		
		List<Contagerencial> raiz = new ArrayList<Contagerencial>();
		for (Contagerencial cg : lista) {
			if(cg.getContagerencialpai() == null || cg.getContagerencialpai().getCdcontagerencial() == null){
				raiz.add(cg);
			}
		}
		for (Contagerencial pai : raiz) {
			pai.setFilhos(this.somenteFilhos(pai, lista));
		}
		
		return raiz;
	}
	
	private List<Contagerencial> somenteFilhos(Contagerencial pai, List<Contagerencial> lista) {
		List<Contagerencial> filhos = new ArrayList<Contagerencial>();
		for (Contagerencial cg : lista) {
			if(cg.getContagerencialpai() != null && 
					cg.getContagerencialpai().getCdcontagerencial() != null &&
					cg.getContagerencialpai().getCdcontagerencial().equals(pai.getCdcontagerencial())){
				cg.setFilhos(somenteFilhos(cg, lista));
				filhos.add(cg);
			}
		}
		return filhos;
	}
	
	public List<Contagerencial> findInTarefaOrcamento(Orcamento orcamento) {
		return this.findInTarefaOrcamento(orcamento, null);
	}
	
	public List<Contagerencial> findInTarefaOrcamento(Orcamento orcamento, Tarefaorcamento tarefaorcamento) {
		return contagerencialDAO.findInTarefaOrcamento(orcamento, tarefaorcamento);
	}
	
	public List<Contagerencial> findInComposicao(Orcamento orcamento) {
		return this.findInComposicao(orcamento, null);
	}
	
	public List<Contagerencial> findInComposicao(Orcamento orcamento, Composicaoorcamento composicaoorcamento) {
		return contagerencialDAO.findInComposicao(orcamento, composicaoorcamento);
	}
	
	public Contagerencial findByProjetoOrcamentoMOD(Projeto projeto) {
		return contagerencialDAO.findByProjetoOrcamentoMOD(projeto);
	}

	public AnaliseRecDespBean findForGraficoDistribuicaoReceitaDespesaFlex(Date dtInicio, Date dtFim) {
		
		AnaliseRecDespBean analiseRecDespBean = new AnaliseRecDespBean();
		AnaliseRecDespBeanFlex beanAnaliseRecDespesaFlex = this.gerarAnaliseRecDespesaFlex();
		if (beanAnaliseRecDespesaFlex != null && beanAnaliseRecDespesaFlex.getListaDP() != null && !beanAnaliseRecDespesaFlex.getListaDP().isEmpty()) {
			List<AnaliseRecDespBean> listaReceita = new ArrayList<AnaliseRecDespBean>();
			for (AnaliseRecDespBean analiseRecDespBeanNivel1 : beanAnaliseRecDespesaFlex.getListaDP()) {				
				adicionaLista(analiseRecDespBeanNivel1,listaReceita);
			}
			analiseRecDespBean.setListaFilha(listaReceita);
		}
		
		return analiseRecDespBean;
	
	}
	 
	public void adicionaLista(AnaliseRecDespBean beanAnaliseRecDespesaFlex, List<AnaliseRecDespBean> listaReceita){
		if(!listaReceita.contains(beanAnaliseRecDespesaFlex)){
			listaReceita.add(beanAnaliseRecDespesaFlex);
		}
		if(beanAnaliseRecDespesaFlex.getHaveFilhos()){
			for (AnaliseRecDespBean analiseRecDespBeanNivel2 : beanAnaliseRecDespesaFlex.getListaFilha()) {
				if(analiseRecDespBeanNivel2.getNome()!=null){
					analiseRecDespBeanNivel2.setNome(analiseRecDespBeanNivel2.getNome().trim());
				}
				if(analiseRecDespBeanNivel2.getPercentual()!=null && !analiseRecDespBeanNivel2.getPercentual().isNaN()){
					analiseRecDespBeanNivel2.setPercentual(SinedUtil.round(analiseRecDespBeanNivel2.getPercentual(), 0));
				}
				AnaliseRecDespBean novoBean =
						new AnaliseRecDespBean(analiseRecDespBeanNivel2.getCodigo(),
						analiseRecDespBeanNivel2.getIdentificador(),
						analiseRecDespBeanNivel2.getNome(),
						analiseRecDespBeanNivel2.getValorDouble(),
						analiseRecDespBeanNivel2.getCodigoPai(),
						analiseRecDespBeanNivel2.getAberto(),
						analiseRecDespBeanNivel2.getGrafico(),
						analiseRecDespBeanNivel2.getHaveFilhos(),
						analiseRecDespBeanNivel2.getOperacao(),
						analiseRecDespBeanNivel2.getPercentual());
						novoBean.setListaFilha(analiseRecDespBeanNivel2.getListaFilha());							
				if(!listaReceita.contains(novoBean)){
					listaReceita.add(novoBean);					
				}
				if(analiseRecDespBeanNivel2.getHaveFilhos()){		
					adicionaLista(analiseRecDespBeanNivel2, listaReceita);	
				}
			}
		}
	} 
	
	/**
	* M�todo que gera relat�rio anal�tico de an�lise Receita/Despesas
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContagerencialDAO#findForRelatorioAnaliticoAnaliseRecDesp(AnaliseReceitaDespesaReportFiltro filtro)
	*
	* @param filtro
	* @return
	* @since Oct 4, 2011
	* @author Luiz Fernando F Silva
	*/
	public IReport gerarRelatorioAnaliticoAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) {
		
		Report report = new Report("/financeiro/analiseReceitaDespesaAnalitico");
				
		List<AnaliseRecDespAnaliticoBean> lista = contagerencialDAO.findForRelatorioAnaliticoAnaliseRecDesp(filtro);
		
		if(lista != null && !lista.isEmpty()){	
			int i = 0;
			for(AnaliseRecDespAnaliticoBean aRecDespSintetico : lista){
				if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
					if(aRecDespSintetico.getDtbanco() != null)
						aRecDespSintetico.setVencimento(aRecDespSintetico.getDtbanco());
					else if(aRecDespSintetico.getDtmovimentacao() != null)
						aRecDespSintetico.setVencimento(aRecDespSintetico.getDtmovimentacao());
				}
				
				if(aRecDespSintetico.getValorrateado() != null){
					aRecDespSintetico.setValorpago(aRecDespSintetico.getValorrateado());
					
					if(aRecDespSintetico.getNaoconsiderartotalrateado() == null || !aRecDespSintetico.getNaoconsiderartotalrateado()){
						setNaoconsiderartotalrateadoDocumentoIgual(aRecDespSintetico, lista, i);
					}
				}
				i++;
			}					
		}
		
		report.setDataSource(lista);
		
		if(filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0){
			String whereInProjeto = CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "projeto.cdprojeto", ",");
			List<Projeto> listaProjeto = projetoService.findDescricao(whereInProjeto);
			if(listaProjeto != null && listaProjeto.size() > 0){
				report.addParameter("projetos", "PROJETO(S): " + CollectionsUtil.listAndConcatenate(listaProjeto, "nome", "; "));
			}
		}
		report.addParameter("periodo", "PER�ODO: " + SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		return report;
	}
	
	private void setNaoconsiderartotalrateadoDocumentoIgual(AnaliseRecDespAnaliticoBean bean, List<AnaliseRecDespAnaliticoBean> lista, int i) {
		if(SinedUtil.isListNotEmpty(lista) && bean != null){	
			int j = 0;
			for(AnaliseRecDespAnaliticoBean aRecDespSintetico : lista){
				if(i != j){
					if(aRecDespSintetico.getValorrateado() != null && (aRecDespSintetico.getNaoconsiderartotalrateado() == null || !aRecDespSintetico.getNaoconsiderartotalrateado())){
						if(		
								((aRecDespSintetico.getNumeroduplicata() == null && bean.getNumeroduplicata() == null) ||
								(aRecDespSintetico.getNumeroduplicata() != null && bean.getNumeroduplicata() != null &&
								 aRecDespSintetico.getNumeroduplicata().equals(bean.getNumeroduplicata()))) &&
								
								((aRecDespSintetico.getNomepessoa() == null && bean.getNomepessoa() == null) ||
								(aRecDespSintetico.getNomepessoa() != null && bean.getNomepessoa() != null &&
								aRecDespSintetico.getNomepessoa().equals(bean.getNomepessoa()))) &&
								
								aRecDespSintetico.getValorrateado() != null && bean.getValorrateado() != null &&
								aRecDespSintetico.getValorrateado().compareTo(bean.getValorrateado()) == 0 &&
								
								aRecDespSintetico.getOperacao() != null && bean.getOperacao() != null && 
								aRecDespSintetico.getOperacao().equals(bean.getOperacao()) &&
								
								((aRecDespSintetico.getNumerodocumento() == null && bean.getNumerodocumento() == null) ||
								(aRecDespSintetico.getNumerodocumento() != null && bean.getNumerodocumento() != null &&
								aRecDespSintetico.getNumerodocumento().equals(bean.getNumerodocumento())))
							){
							aRecDespSintetico.setNaoconsiderartotalrateado(true);
						}
					}
				}
				j++;
			}					
		}
		
	}
	
	/**
	 * M�todo que gera o relat�rio CSV an�litico de receitas e despesas
	 *
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Luiz Fernando
	 * @since 16/12/2013
	 */
	public Resource gerarRelatorioAnaliticoCSVAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) throws IOException {
		List<AnaliseRecDespAnaliticoBean> lista = contagerencialDAO.findForRelatorioAnaliticoAnaliseRecDesp(filtro);
		
		if(lista != null && !lista.isEmpty()){			
			for(AnaliseRecDespAnaliticoBean aRecDespAnalitico : lista){
				if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
					if(aRecDespAnalitico.getDtbanco() != null)
						aRecDespAnalitico.setVencimento(aRecDespAnalitico.getDtbanco());
					else if(aRecDespAnalitico.getDtmovimentacao() != null)
						aRecDespAnalitico.setVencimento(aRecDespAnalitico.getDtmovimentacao());
				}
				if(aRecDespAnalitico.getValorrateado() != null)
					aRecDespAnalitico.setValorpago(aRecDespAnalitico.getValorrateado());
			}
		}
		
		SinedExcel wb = new SinedExcel();
		
		short numLinha = 0;
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));

        HSSFRow headerRow = planilha.createRow(numLinha);
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("RAZ�O DE CONTA GERENCIAL");
		
		planilha.addMergedRegion(new Region(numLinha, (short) 0, numLinha + 1, (short) 8));
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		numLinha += 2;
		
		row = planilha.createRow(numLinha);
		cell = row.createCell((short) (0));
		cell.setCellStyle(SinedExcel.STYLE_FILTRO);
		cell.setCellValue("Per�odo: " + SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		
		planilha.addMergedRegion(new Region(numLinha, (short) 0, numLinha, (short) 8));
		
		if(filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0){
			String whereInProjeto = CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "projeto.cdprojeto", ",");
			List<Projeto> listaProjeto = projetoService.findDescricao(whereInProjeto);
			if(listaProjeto != null && listaProjeto.size() > 0){
				numLinha += 1;
				
				row = planilha.createRow(numLinha);
				cell = row.createCell((short) (0));
				cell.setCellStyle(SinedExcel.STYLE_FILTRO);
				cell.setCellValue("Projeto(s): " + CollectionsUtil.listAndConcatenate(listaProjeto, "nome", "; "));
				
				planilha.addMergedRegion(new Region(numLinha, (short) 0, numLinha, (short) 7));
			}
		}
		
		numLinha += 2;
		
		row = planilha.createRow(numLinha);	
		
		int numColuna = 9;
		for (int i = 0; i < numColuna; i++) {
			cell = row.createCell((short) (i));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			switch (i) {
				case 0:
					cell.setCellValue("Conta Gerencial");
					break;
				case 1:
					cell.setCellValue("C�digo");
					break;
				case 2:
					cell.setCellValue("Num. Documento");
					break;
				case 3:
					cell.setCellValue("Vencto.");
					break;
				case 4:
					cell.setCellValue("Cliente/Fornecedor");
					break;
				case 5:
					cell.setCellValue("Hist�rico");
					break;
				case 6:
					cell.setCellValue("Valor Item");
					break;
				case 7:
					cell.setCellValue("Valor");
					break;
				case 8:
					cell.setCellValue("V�nculo");
					break;
				default:
					break;
			}
			
		}
		
		numLinha += 1;
		criaTabelaComDadosRelatorioAnaliticoExcel(planilha, row, cell, lista, numColuna, numLinha, Boolean.TRUE.equals(filtro.getExibirTotalizadorAnaliticoCSV()));
		
		return wb.getWorkBookResource("analisereceitasdespesas_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * M�todo que cria a tabela com os dados do relat�rio
	 *
	 * @param planilha
	 * @param row
	 * @param cell
	 * @param lista
	 * @param numColuna
	 * @param numLinha
	 * @author Luiz Fernando
	 * @since 16/12/2013
	 */
	private void criaTabelaComDadosRelatorioAnaliticoExcel(HSSFSheet planilha, HSSFRow row, HSSFCell cell, List<AnaliseRecDespAnaliticoBean> lista, int numColuna, short numLinha, boolean exibirTotalizadorAnaliticoCSV) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(lista != null && !lista.isEmpty()){		
			String identificador = null;
			Money totalCG = new Money();
			Money totalIt = new Money();
			for(AnaliseRecDespAnaliticoBean aRecDespAnalitico : lista){
				row = planilha.createRow(numLinha);
				if(aRecDespAnalitico.getIdentificador() != null){
					if(identificador == null){
						totalCG = new Money();
						totalIt = new Money();
						identificador = aRecDespAnalitico.getIdentificador();
					}else { 
						if(!identificador.equals(aRecDespAnalitico.getIdentificador()) && exibirTotalizadorAnaliticoCSV){
							cell = row.createCell((short) (0));
							cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
							cell.setCellValue("Totais");
									
							cell = row.createCell((short) (numColuna-3));
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
							cell.setCellValue(totalIt != null ? totalIt.getValue().doubleValue() : 0.0);
							
							cell = row.createCell((short) (numColuna-2));
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
							cell.setCellValue(totalCG != null ? totalCG.getValue().doubleValue() : 0.0);
							
							planilha.addMergedRegion(new Region(numLinha, (short) 0, numLinha, (short) 5));
							
							cell = row.createCell((short) 8);
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
							
							numLinha++;
							row = planilha.createRow(numLinha);
							totalCG = new Money();
							totalIt = new Money();
							identificador = aRecDespAnalitico.getIdentificador();
						}						
					}
				}
				if(totalCG != null && aRecDespAnalitico.getValorrateado() != null){
					totalCG = totalCG.add(aRecDespAnalitico.getValorrateado());
				}
				if(totalIt != null && aRecDespAnalitico.getValorrateadoCG() != null){
					totalIt = totalIt.add(aRecDespAnalitico.getValorrateadoCG());
				}
				for(int i = 0; i < numColuna; i++){
					cell = row.createCell((short) (i));
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
					switch (i) {
						case 0:
							cell.setCellValue(aRecDespAnalitico.getIdentificador() + " - " + aRecDespAnalitico.getCgnome());
							break;
						case 1:
							cell.setCellValue(aRecDespAnalitico.getNumeroduplicata());
							break;
						case 2:
							cell.setCellValue(aRecDespAnalitico.getNumerodocumento());
							break;
						case 3:
							cell.setCellValue(aRecDespAnalitico.getVencimento() != null ? format.format(aRecDespAnalitico.getVencimento()) : "");
							break;
						case 4:
							cell.setCellValue(aRecDespAnalitico.getNomepessoa());
							break;
						case 5:
							cell.setCellValue(aRecDespAnalitico.getHistorico());
							break;
						case 6:
							cell.setCellValue(aRecDespAnalitico.getValorrateadoCG() != null ? aRecDespAnalitico.getValorrateadoCG().getValue().doubleValue() : 0.0);
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
							break;
						case 7:
							cell.setCellValue(aRecDespAnalitico.getValorrateado() != null ? aRecDespAnalitico.getValorrateado().getValue().doubleValue() : 0.0);
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
							break;
						case 8:
							cell.setCellValue(aRecDespAnalitico.getVinculo());
							break;
						default:
							break;
					}
				}
				numLinha++;
			}
			if(identificador != null && exibirTotalizadorAnaliticoCSV){
				row = planilha.createRow(numLinha);
				
				for(int i = 0; i < numColuna; i++){
					cell = row.createCell((short) (i));
					cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				}
				
				cell = row.getCell((short) 0);
				cell.setCellValue("Totais");
				
				cell = row.getCell((short) (numColuna-3));
				cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
				cell.setCellValue(totalIt != null ? totalIt.getValue().doubleValue() : 0.0);
				
						
				cell = row.getCell((short) (numColuna-2));
				cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
				cell.setCellValue(totalCG != null ? totalCG.getValue().doubleValue() : 0.0);
				
				planilha.addMergedRegion(new Region(numLinha, (short) 0, numLinha, (short) 5));
				cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT12);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.findForFluxoCaixaMensal
	 *
	 * @return
	 * @author Marden Silva
	 * @param listaNaturez 
	 */
	public List<Contagerencial> findForFluxoCaixaMensal(List<NaturezaContagerencial> listaNaturezaContagerencial){
		return contagerencialDAO.findForFluxoCaixaMensal(listaNaturezaContagerencial);
	}
	
	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return contagerencialDAO.findForArquivoGerencial();
	}
	public List<Contagerencial> findAutocomplete(String q) {
		return contagerencialDAO.findAutocomplete(q);
	}
	
	public List<Contagerencial> loadForSpedPiscofinsReg0500(Set<String> listaIdentificador) {
		return contagerencialDAO.loadForSpedPiscofinsReg0500(listaIdentificador);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#isPai(Contagerencial pai)
	 *
	 * @param pai
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isPai(Contagerencial pai){
		return contagerencialDAO.isPai(pai);
	}
	
	/**
	 * M�todo que retorno um whereIn da natureza da contagerencial. (Resultado, Compensa��o e Outras)
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public String getWhereInNaturezaResultadoCompensacaoOutras(){
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTAS_RESULTADO);
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContagerencial.add(NaturezaContagerencial.OUTRAS);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial, "value", ",");
	}
	
	public List<Contagerencial> findForGerarMovimentacao(Tipooperacao tipooperacao){
		return contagerencialDAO.findForGerarMovimentacao(tipooperacao);
	}
	
	/**
	 * 
	 * @param contagerencial
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarItem(Contagerencial contagerencial){
		return contagerencialDAO.verificarItem(contagerencial);
	}
	
	/**
	 *
	 * @return
	 * @author Thiago Clemente
	 */
	public String getWhereInNaturezaContaGerencial(){
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial, "value", ",");
	}
	
	/**
	 *
	 * @return
	 * @author Thiago Clemente
	 */
	public String getWhereInNaturezaCompensacaoContaGerencialOutras(){
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
		listaNaturezaContagerencial.add(NaturezaContagerencial.OUTRAS);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial, "value", ",");
	}
	
	public Contagerencial getContagerencialIndenizacaoByParametro(){
		Contagerencial contagerencial = null;
		try {
			String idContagerencial = parametrogeralService.getValorPorNome(Parametrogeral.INDENIZACAO_CONTAGERENCIAL);
			contagerencial = load(new Contagerencial(Integer.parseInt(idContagerencial)), "contagerencial.cdcontagerencial, contagerencial.nome");
		} catch (Exception e) {}
		return contagerencial;
	}
	
	/**
	 * M�todo que retorna a listagem completa de contas gerenciais com seus respectivos identificadores.
	 * 
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Contagerencial> findListaCompleta(){
		return contagerencialDAO.findListaCompleta();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#updateUsadocalculocustooperacional(String whereInContagerencial)
	*
	* @param whereInContagerencial
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public void updateUsadocalculocustooperacional(String whereInContagerencial) {
		contagerencialDAO.updateUsadocalculocustooperacional(whereInContagerencial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#updateUsadocalculocustocomercial(String whereInContagerencial)
	*
	* @param whereInContagerencial
	* @since 16/09/2015
	* @author Luiz Fernando
	*/
	public void updateUsadocalculocustocomercial(String whereInContagerencial) {
		contagerencialDAO.updateUsadocalculocustocomercial(whereInContagerencial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContagerencialDAO#getContagerencialUsadaCustoOperacional()
	*
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public List<Contagerencial> getContagerencialUsadaCustoOperacional() {
		return contagerencialDAO.getContagerencialUsadaCustoOperacional();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/08/2015
	 */
	public List<Contagerencial> getContagerencialUsadaCustoComercial(){
		return contagerencialDAO.getContagerencialUsadaCustoComercial();
	}
	/**
	 * M�todo faz refer�ncia ao DAO
	 * @param id
	 * @return
	 * @since 03/02/2016
	 * @author C�sar
	 */
	public List<GenericBean> ocorrenciaContaGerencial(Integer id){
		return contagerencialDAO.ocorrenciaContaGerencial(id);
	}
		
	/**
	 * Carregamento do n�vel via ajax para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findPais
	 * @see br.com.linkcom.sined.geral.bean.Contagerencial#getNivel
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxNivel(WebRequestContext request, Contagerencial bean) {
		request.getServletResponse().setContentType("text/html");
		if (bean == null || bean.getContagerencialpai() == null) {
			View.getCurrent().eval("");
		} else {
			bean = carregaConta(bean.getContagerencialpai());
			int proximoNivel = (bean.getVcontagerencial().getNivel()+1);
			View.getCurrent().eval(String.valueOf(proximoNivel));
		}
	}

	/**
	 * Carregamento do tipo de opera��o via ajax  para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#carregaConta
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxOperacao(WebRequestContext request, Contagerencial bean) {
		if (bean == null || bean.getContagerencialpai() == null) {
			throw new SinedException("Contagerencialpai nao pode ser nula.");
		} else {
			request.getServletResponse().setContentType("text/html");
			bean = carregaConta(bean.getContagerencialpai());
			View.getCurrent().println("var tipoOperacao = '" + bean.getTipooperacao().getCdtipooperacao().toString() + "';");
			View.getCurrent().println("var natureza = '" + (bean.getNatureza() != null ? bean.getNatureza().name() : "<null>") + "';");
		}
	}
	
	/**
	 * Carrega o campo transient boolean para fazer a confirma��o de update no banco.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#isAnalitica
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxAnalitica(WebRequestContext request, Contagerencial bean) {
		if (bean == null || bean.getContagerencialpai() == null) {
			throw new SinedException("Contagerencialpai nao pode ser nula.");
		} else {
			// vereifica se a conta gerencial pai � anal�tica e tem registros.
			if (isAnalitica(bean.getContagerencialpai())) {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().eval("true");
			} else View.getCurrent().eval("false");			
		}
	}
	
	public void ajaxItem(WebRequestContext request, Contagerencial bean) {
		
		Integer item = null;
		
		if (bean.getContagerencialpai() != null && bean.getContagerencialpai().getCdcontagerencial() != null) {
			List<Contagerencial> listaFilhos = findFilhos(bean.getContagerencialpai());
			if (listaFilhos.size() > 0) {
				item = listaFilhos.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		} else {
			List<Contagerencial> listaPais = findRaiz();
			if (listaPais.size() > 0) {
				item = listaPais.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		}
		
		View.getCurrent().println("var item = " + item + ";");
	}
	
	public void ajaxBuscaNaturezaContagerencial(WebRequestContext request, Contagerencial contagerencial){
		List<NaturezaContagerencial> listaNaturezacontagerencial = new ArrayList<NaturezaContagerencial>();
		if(Tipooperacao.TIPO_CREDITO.equals(contagerencial.getTipooperacao()) ||
			Tipooperacao.TIPO_DEBITO.equals(contagerencial.getTipooperacao())){
			listaNaturezacontagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
			listaNaturezacontagerencial.add(NaturezaContagerencial.OUTRAS);
		}else if(Tipooperacao.TIPO_CONTABIL.equals(contagerencial.getTipooperacao())){
			listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_ATIVO);
			listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_PASSIVO);
			listaNaturezacontagerencial.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
			listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_RESULTADO);
			listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		}
		View.getCurrent().println(getOptionComboNaturezaContagerencial(listaNaturezacontagerencial,
				contagerencial.getNatureza() != null? contagerencial.getNatureza().getValue(): null));
	}
	
	public String getOptionComboNaturezaContagerencial(List<NaturezaContagerencial> lista, Integer cdnaturezaselecionada) {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value=\"<null>\"></option>");
		
		for (NaturezaContagerencial natureza : lista) {
			sb.append("<option ");
			
			if(cdnaturezaselecionada != null && cdnaturezaselecionada.equals(natureza.getValue())){
				sb.append("selected");
			}
			
			sb.append(" value=\""+natureza.name())
				.append("\">")
				.append(natureza.getNome())
				.append("</option>");
		}
		
		return sb.toString();
	}
	
	public List<Contagerencial> buscarUltimoNivelAtivos(Tipooperacao tipoOperacao, String strWhereIn) {
		return contagerencialDAO.buscarUltimoNivelAtivos(tipoOperacao,strWhereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @since 10/09/2019
	 * @author Rodrigo Freitas
	 */
	public List<ApuracaoResultadoProjetoBean> findByApuracaoResultadoProjeto(ApuracaoResultadoProjetoFiltro filtro) {
		return contagerencialDAO.findByApuracaoResultadoProjeto(filtro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @since 10/09/2019
	 * @author Rodrigo Freitas
	 */
	public List<ApuracaoResultadoProjetoListagemBean> findByApuracaoResultadoProjetoListagem(ApuracaoResultadoProjetoFiltro filtro) {
		return contagerencialDAO.findByApuracaoResultadoProjetoListagem(filtro);
	}
	public List<EmitirBalancoPatrimonialBean> buscarArvoreContaGerencialBalancoPatrimonial() {
		return contagerencialDAO.buscarTudoBalancoPatrimonial();
	}
	
}