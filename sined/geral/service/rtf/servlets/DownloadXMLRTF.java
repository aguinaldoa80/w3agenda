package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class DownloadXMLRTF extends HttpServlet {

	private static final long serialVersionUID = -688481001373652300L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fileName = getNomeArquivo(request) + ".fields.xml";
		InputStream resourceAsStream = AbstractW3erpRTFServlet.class.getResourceAsStream(AbstractW3erpRTFServlet.CAMINHO_ARQUIVO_XML + fileName);
		byte[] bytes = IOUtils.toByteArray(resourceAsStream);
		
		response.setContentType("text/xml");
		response.setHeader("Content-Disposition","attachment; filename=\""+ fileName + "\";");
		
		response.getOutputStream().write(bytes);
		response.flushBuffer();
	}

	private String getNomeArquivo(HttpServletRequest request) {
		if(request.getParameter("file") != null){
			return request.getParameter("file");
		}
		return null;
	}

}
