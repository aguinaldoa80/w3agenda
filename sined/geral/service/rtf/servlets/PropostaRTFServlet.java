package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class PropostaRTFServlet extends AbstractW3erpRTFServlet{

	private static final long serialVersionUID = -7773951047737647688L;

	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Proposta proposta = new Proposta();
		proposta.setCdproposta(Integer.parseInt(request.getParameter("cdproposta")));
		
		
		proposta = PropostaService.getInstance().load(proposta);
		
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(proposta.getPropostacaixa().getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected String getNomeArquivo() {
		return "proposta";
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx) throws Exception {
		Proposta proposta = new Proposta();
		proposta.setCdproposta(Integer.parseInt(request.getParameter("cdproposta")));
		ctx.put("proposta", PropostaService.getInstance().makePropostaRTF(proposta));
	}

	@Override
	protected void error(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
		e.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF da proposta. Favor verificar se a Caixa de Proposta cont�m um arquivo RTF.");
		response.sendRedirect(request.getContextPath() + "/crm/crud/Proposta");
	}
}
