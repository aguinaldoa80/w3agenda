package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.EmpresamodelocontratoService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class ContratoRTFServlet extends AbstractW3erpRTFServlet {

	private static final long serialVersionUID = -7773951047737647688L;

	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Empresamodelocontrato modelocontrato = new Empresamodelocontrato(Integer.parseInt(request.getParameter("cdempresamodelocontrato")));
		
		modelocontrato = EmpresamodelocontratoService.getInstance().load(modelocontrato, "empresamodelocontrato.cdempresamodelocontrato, empresamodelocontrato.arquivo");
		
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(modelocontrato.getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected String getNomeArquivo() {
		return "contrato";
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx) throws Exception {
		Contrato contrato = new Contrato(Integer.parseInt(request.getParameter("cdcontrato")));
		ctx.put("contrato", ContratoService.getInstance().makeContratoRTF(contrato));
	}

	@Override
	protected void error(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
		e.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF. Favor conferir o modelo no cadastro de empresa.");
		response.sendRedirect(request.getContextPath() + "/faturamento/crud/Contrato");
	}
}
