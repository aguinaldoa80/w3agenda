package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ContratojuridicoService;
import br.com.linkcom.sined.geral.service.EmpresamodelocontratoService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class ContratojuridicoRTFServlet extends AbstractW3erpRTFServlet {

	private static final long serialVersionUID = -7773951047737647688L;

	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Empresamodelocontrato modelocontrato = new Empresamodelocontrato(Integer.parseInt(request.getParameter("cdempresamodelocontrato")));
		
		modelocontrato = EmpresamodelocontratoService.getInstance().load(modelocontrato, "empresamodelocontrato.cdempresamodelocontrato, empresamodelocontrato.arquivo");
		
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(modelocontrato.getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected String getNomeArquivo() {
		return "contratojuridico";
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx) throws Exception {
		Contratojuridico contratojuridico = new Contratojuridico(Integer.parseInt(request.getParameter("cdcontratojuridico")));
		
		ctx.put("contratojuridico", ContratojuridicoService.getInstance().makeContratojuridicoRTF(contratojuridico));
		
	}

	@Override
	protected void error(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
		e.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF. Favor conferir o modelo no cadastro de empresa.");
		response.sendRedirect(request.getContextPath() + "/juridico/crud/Contratojuridico");
	}
	
}
