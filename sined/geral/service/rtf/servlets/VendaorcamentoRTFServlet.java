package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresamodelocomprovanteorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.EmpresamodelocomprovanteorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentohistoricoService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class VendaorcamentoRTFServlet extends AbstractW3erpRTFServlet{
	
	private static final long serialVersionUID = -7773951047737647688L;
	
	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Integer cdempresamodelocomprovanteorcamento = Integer.parseInt(request.getParameter("cdempresamodelocomprovanteorcamento"));
		Empresamodelocomprovanteorcamento modelo = new Empresamodelocomprovanteorcamento();
		modelo.setCdempresamodelocomprovanteorcamento(cdempresamodelocomprovanteorcamento);
		modelo = EmpresamodelocomprovanteorcamentoService.getInstance().loadWithArquivo(modelo);
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(modelo.getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected String getNomeArquivo() {
		return "vendaorcamento";
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx)
			throws Exception {
		Vendaorcamento vendaorcamento = new Vendaorcamento(Integer.parseInt(request.getParameter("cdvendaorcamento")));
		ctx.put("vendaorcamento", VendaorcamentoService.getInstance().makeComprovanteOrcamento(vendaorcamento));
	}

	@Override
	protected void error(HttpServletRequest request,
			HttpServletResponse response, Exception cause)
			throws ServletException, IOException {
		cause.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF. Favor conferir o modelo de comprovante de or�amento.");
		response.sendRedirect(request.getContextPath() + "/faturamento/crud/VendaOrcamento");
	}
	
	@Override
	protected void sucess(HttpServletRequest request) throws ServletException,
			IOException {
		Vendaorcamento vendaorcamento = new Vendaorcamento(Integer.parseInt(request.getParameter("cdvendaorcamento")));
		VendaorcamentohistoricoService.getInstance().makeHistoricoComprovanteEmitido(vendaorcamento);
		super.sucess(request);
	}
}
