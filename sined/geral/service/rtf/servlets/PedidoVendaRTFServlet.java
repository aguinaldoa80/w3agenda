package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresamodelocomprovanteorcamento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.EmpresamodelocomprovanteorcamentoService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;

public class PedidoVendaRTFServlet extends AbstractW3erpRTFServlet{

	private static final long serialVersionUID = 4055883584447257066L;

	@Override
	protected String getNomeArquivo() {
		return "pedidovenda";
	}

	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Integer cdempresamodelocomprovanteorcamento = Integer.parseInt(request.getParameter("cdempresamodelocomprovanteorcamento"));
		Empresamodelocomprovanteorcamento modelo = new Empresamodelocomprovanteorcamento();
		modelo.setCdempresamodelocomprovanteorcamento(cdempresamodelocomprovanteorcamento);
		modelo = EmpresamodelocomprovanteorcamentoService.getInstance().loadWithArquivo(modelo);
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(modelo.getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx)
			throws Exception {
		Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(request.getParameter("cdpedidovenda")));
		ctx.put("pedidovenda", PedidovendaService.getInstance().makeComprovanteOrcamento(pedidovenda));
		
	}
	
	@Override
	protected void error(HttpServletRequest request,
			HttpServletResponse response, Exception cause)
			throws ServletException, IOException {
		cause.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF. Favor conferir o modelo de comprovante RTF.");
		response.sendRedirect(request.getContextPath() + "/faturamento/crud/Pedidovenda");
	}
	
	
	@Override
	protected void sucess(HttpServletRequest request) throws ServletException,
			IOException {
		Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(request.getParameter("cdpedidovenda")));
		PedidovendahistoricoService.getInstance().makeHistoricoComprovanteEmitido(pedidovenda);
		super.sucess(request);
	}

}
