package br.com.linkcom.sined.geral.service.rtf.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.template.IContext;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.rtf.AbstractW3erpRTFServlet;


public class OportunidadeRTFServlet extends AbstractW3erpRTFServlet {

	private static final long serialVersionUID = -7773951047737647688L;

	@Override
	protected byte[] getBytesRTF(HttpServletRequest request) {
		Oportunidade oportunidade = OportunidadeService.getInstance().loadWithPropostacaixa(new Oportunidade(Integer.parseInt(request.getParameter("cdoportunidade"))));
		Arquivo arquivo = ArquivoService.getInstance().loadWithContents(oportunidade.getPropostacaixa().getArquivo());
		return arquivo.getContent();
	}

	@Override
	protected String getNomeArquivo() {
		return "oportunidade";
	}

	@Override
	protected void putContext(HttpServletRequest request, IContext ctx) throws Exception {
		Oportunidade oportunidade = new Oportunidade(Integer.parseInt(request.getParameter("cdoportunidade")));
		ctx.put("oportunidade", OportunidadeService.getInstance().makeOportunidadeRTF(oportunidade));
	}

	@Override
	protected void error(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException, IOException {
		e.printStackTrace();
		
		Neo.getRequestContext().addError("N�o foi poss�vel gerar o RTF. Favor conferir o modelo no cadastro da proposta da oportunidade.");
		response.sendRedirect(request.getContextPath() + "/crm/crud/Oportunidade");
	}
	
	@Override
	protected void sucess(HttpServletRequest request) throws ServletException, IOException {
		try {
			OportunidadeService.getInstance().createHistoricoEmissaoPropostaContrato( new Oportunidade(Integer.parseInt(request.getParameter("cdoportunidade"))));
		} catch (Exception e) {}
	}
	
	@Override
	protected void doRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String cdoportunidade = request.getParameter("cdoportunidade");
		if(!OportunidadeService.getInstance().isUsuarioPodeEditarOportunidades(cdoportunidade)){
			String msgErro = "A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.";
			boolean erroJaAdd = false;
			for(Message msg: Neo.getRequestContext().getMessages()){
				if(msg.getType().equals(MessageType.ERROR) && msg.toString().equals(msgErro)){
					erroJaAdd = true;
				}
			}
			if(!erroJaAdd)
				Neo.getRequestContext().addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			String urlRedirecionamento = request.getContextPath() + "/crm/crud/Oportunidade";
			if("entrada".equals(request.getParameter("controller"))){
				urlRedirecionamento +=  "?ACAO=consultar&cdoportunidade="+cdoportunidade;
			}
			response.sendRedirect(urlRedirecionamento);
		}else{
			super.doRequest(request, response);
		}
	}
}
