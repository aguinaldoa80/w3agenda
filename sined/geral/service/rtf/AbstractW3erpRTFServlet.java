package br.com.linkcom.sined.geral.service.rtf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.rtf.web.servlet.AbstractRTFTemplateServlet;
import br.com.linkcom.sined.util.SinedUtil;

public abstract class AbstractW3erpRTFServlet extends AbstractRTFTemplateServlet {

	private static final long serialVersionUID = 2019977638646445371L;
	
	public static final String CAMINHO_ARQUIVO_XML = "xml/";

	protected abstract String getNomeArquivo();
	protected abstract byte[] getBytesRTF(HttpServletRequest request);
	
	@Override
	protected InputStream getXMLFieldsAvailable(HttpServletRequest request) throws Exception {
		return AbstractW3erpRTFServlet.class.getResourceAsStream(CAMINHO_ARQUIVO_XML + getNomeArquivo() + ".fields.xml");
	}
	
	@Override
	protected InputStream getRTFInputStream(HttpServletRequest request) throws Exception {
		return new ByteArrayInputStream(getBytesRTF(request));
	}
	
	protected String getFileNameOfContentDisposition(HttpServletRequest request) {
		return getNomeArquivo() + "_" + SinedUtil.datePatternForReport() + ".rtf";
	}
	
	@Override
	protected void error(HttpServletRequest request, HttpServletResponse response, Exception cause) throws ServletException, IOException {
		cause.printStackTrace();
	}

}
