package br.com.linkcom.sined.geral.service.rtf.bean;

public class RomaneioMaterialNumeroSerieBean {

	private String numeroSerie;
	
	public String getNumeroSerie() {
		return numeroSerie;
	}
	
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
}
