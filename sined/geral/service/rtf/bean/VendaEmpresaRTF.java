package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Empresa;

public class VendaEmpresaRTF {

	private String nomefantasia;
	private String razaosocial;
	private String cnpj;
	private String email;
	private String responsavelcontato;
	private String endereco;
	private String inscricaoestadual;
	private String telefones;
	private String site;

	
	public VendaEmpresaRTF(Empresa empresa){
		if(empresa != null && empresa.getCdpessoa() != null){
			setNomefantasia(empresa.getNomefantasia());
			setRazaosocial(empresa.getRazaosocialOuNome());
			setCnpj(empresa.getCnpj() != null? empresa.getCnpj().toString(): "");
			setEndereco(empresa.getEndereco() != null? empresa.getEndereco().getLogradouroCompletoComBairro(): "");
			setInscricaoestadual(empresa.getInscricaoestadual());
			setTelefones(empresa.getTelefonesSemQuebraLinha());
			setEmail(empresa.getEmail());
			setSite(empresa.getSite());
			setResponsavelcontato(empresa.getResponsavel() != null? empresa.getResponsavel().getNome(): "");
		}
	}
	
	public String getNomefantasia() {
		return nomefantasia;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getResponsavelcontato() {
		return responsavelcontato;
	}
	public void setResponsavelcontato(String responsavelcontato) {
		this.responsavelcontato = responsavelcontato;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public String getTelefones() {
		return telefones;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
}
