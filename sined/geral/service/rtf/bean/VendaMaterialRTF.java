package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;

public class VendaMaterialRTF{

	private String cdmaterial;
	private String codigo;
	private String identificador;
	private String identificadorespecifico;
	private String descricao;
	private String unidademedida;
	private String unidademedidasimbolo;
	private Double quantidadeunidadeprincipal;
	private Double quantidade;
	private String preco;
	private String dtentrega;
	private String total;
	private String observacao;
	private String loteestoque;
	private String desconto;
	private String comprimento;
	private String pesoliquido;
	private String pesobruto;
	private String altura;
	private String largura;
	private String ncm;
	private String ncmcompleto;
	private String ncmdescricao;
	private InputStream imagem;
	private String cdmaterialmestre;
	private String localizacaoestoque;
	private String cdvendamaterial;
	private String marca;
	private String serie;
	private String dot;
	private String modelo;
	private String medida;
	private String identificadormaterialmestre;
	private String caracteristica;
	
	
	public String getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterial(String cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public String getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
	public String getUnidademedidasimbolo() {
		return unidademedidasimbolo;
	}
	public void setUnidademedidasimbolo(String unidademedidasimbolo) {
		this.unidademedidasimbolo = unidademedidasimbolo;
	}
	public Double getQuantidadeunidadeprincipal() {
		return quantidadeunidadeprincipal;
	}
	public void setQuantidadeunidadeprincipal(Double quantidadeunidadeprincipal) {
		this.quantidadeunidadeprincipal = quantidadeunidadeprincipal;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(String loteestoque) {
		this.loteestoque = loteestoque;
	}
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public String getComprimento() {
		return comprimento;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public String getPesoliquido() {
		return pesoliquido;
	}
	public void setPesoliquido(String pesoliquido) {
		this.pesoliquido = pesoliquido;
	}
	public String getPesobruto() {
		return pesobruto;
	}
	public void setPesobruto(String pesobruto) {
		this.pesobruto = pesobruto;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public String getLargura() {
		return largura;
	}
	public void setLargura(String largura) {
		this.largura = largura;
	}
	public String getNcm() {
		return ncm;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	public String getNcmdescricao() {
		return ncmdescricao;
	}
	public void setNcmdescricao(String ncmdescricao) {
		this.ncmdescricao = ncmdescricao;
	}
	public String getCdmaterialmestre() {
		return cdmaterialmestre;
	}
	public void setCdmaterialmestre(String cdmaterialmestre) {
		this.cdmaterialmestre = cdmaterialmestre;
	}
	public String getLocalizacaoestoque() {
		return localizacaoestoque;
	}
	public void setLocalizacaoestoque(String localizacaoestoque) {
		this.localizacaoestoque = localizacaoestoque;
	}
	public String getCdvendamaterial() {
		return cdvendamaterial;
	}
	public void setCdvendamaterial(String cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getDot() {
		return dot;
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMedida() {
		return medida;
	}
	public void setMedida(String medida) {
		this.medida = medida;
	}
	public InputStream getImagem() {
		return imagem;
	}
	public void setImagem(InputStream imagem) {
		this.imagem = imagem;
	}
	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	public String getDtentrega() {
		return dtentrega;
	}
	public void setDtentrega(String dtentrega) {
		this.dtentrega = dtentrega;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getIdentificadormaterialmestre() {
		return identificadormaterialmestre;
	}
	public void setIdentificadormaterialmestre(
			String identificadormaterialmestre) {
		this.identificadormaterialmestre = identificadormaterialmestre;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
}