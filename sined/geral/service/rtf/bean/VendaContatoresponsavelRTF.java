package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Contato;

public class VendaContatoresponsavelRTF {

	private String nome;
	private String email;
	private String telefones;
	
	public VendaContatoresponsavelRTF(Contato contato){
		if(contato != null){
			this.setNome(contato.getNome());
			this.setEmail(contato.getEmailcontato());
			this.setTelefones(contato.getTelefonesSemQuebraLinha());
		}
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefones() {
		return telefones;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
}
