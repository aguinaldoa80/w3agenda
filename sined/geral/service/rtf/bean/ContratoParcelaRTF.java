package br.com.linkcom.sined.geral.service.rtf.bean;

public class ContratoParcelaRTF {
	
	private String valor_parcela;
	private String data_vencimento;

	public String getValor_parcela() {
		return valor_parcela;
	}
	public String getData_vencimento() {
		return data_vencimento;
	}
	public void setValor_parcela(String valorParcela) {
		valor_parcela = valorParcela;
	}
	public void setData_vencimento(String dataVencimento) {
		data_vencimento = dataVencimento;
	}
}
