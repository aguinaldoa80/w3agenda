package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;
import java.util.List;

public class ContratoRTF {
	
	private String cdcontrato;
	private String identificador;
	private String nome_cliente;
	private String email_cliente;                             
	private String representante;
	private String enderecocompleto;
	private String endereco;
	private String complemento;
	private String numero;
	private String bairro;
	private String municipio;
	private String uf;
	private String enderecocompletoentrega;
	private String enderecoentrega;
	private String complementoentrega;
	private String numeroentrega;
	private String bairroentrega;
	private String municipioentrega;
	private String ufentrega;
	private String cpf;
	private String cnpj;
	private String celular;
	private String telefone;
	private String descricao_contrato;
	private String data_inicio_contrato;
	private String data_fim_contrato;
	private String data_inicio_locacao;
	private String data_fim_locacao;
	private String data_vencimento_contrato;
	private String valor_contrato;
	private String data_extenso;
	private String profissao;
	private String estado_civil;
	private String identidade;
	private String nacionalidade;
	private String anotacoes;
	private String historico;
	private String indicecorrecao;
	private InputStream logo;
	private List<ContratoMaterialRTF> servicos;
	private List<ContratoParcelaRTF> parcelas;
	private String enderecocompleto_cobranca_cliente;
	private String enderecocompleto_entrega_cliente;
	private String enderecocompleto_faturamento_cliente;
	private String enderecocompleto_entrega_contrato;
	private String enderecocompleto_faturamento_contrato;
	private String cep_cliente;
	private String cep_enderecoentrega;
	private String cnae_contrato;
	private String inscricaomunicipal;
	private String contato_cliente;
	private String valorporextenso_contrato;
	private String valortotalservicosporextenso_contrato;
	private String valortotalservicos_contrato;
	private String valortotalservicosporextensocusto_contrato;
	private String valortotalservicoscusto_contrato;
	private String contato_contrato;
	private String vendedornome;
	private String vendedoridentidade;
	private String data_renovacao;
	private String dia_vencimento;
	private String nome_contato_socio;
	private String cpf_contato_socio;
	private String horainicio;
	private String periodicidade;
	private String valor_adicional;
	private String inscricaoestadual;
	private String telefoneprincipal;
	private String razaosocial;
	private String ir;
	private String iss;
	private String pis;
	private String cofins;
	private String csll;
	private String inss;
	private String icms;
	private String valor_ir;
	private String valor_iss;
	private String valor_pis;
	private String valor_cofins;
	private String valor_csll;
	private String valor_inss;
	private String valor_icms;
	private String valor_acrescimo;
	private String email_contato;
	private String quantidadeparcelas;
	private String cpf_representante;
	private String rg_representante;
	private String data_proxima_parcela;
	private String valor_proxima_parcela;
	private String notaremessalocacao;
	private String periodicidadeRenovacao;
	private String dtemissaonotaremessalocacao;
	private String saldoromaneio;
	
	
	
	public String getNome_cliente() {
		return nome_cliente;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getNumero() {
		return numero;
	}
	public String getBairro() {
		return bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getUf() {
		return uf;
	}
	public String getEnderecocompletoentrega() {
		return enderecocompletoentrega;
	}
	public String getEnderecoentrega() {
		return enderecoentrega;
	}
	public String getComplementoentrega() {
		return complementoentrega;
	}
	public String getNumeroentrega() {
		return numeroentrega;
	}
	public String getBairroentrega() {
		return bairroentrega;
	}
	public String getMunicipioentrega() {
		return municipioentrega;
	}
	public String getUfentrega() {
		return ufentrega;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCelular() {
		return celular;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getDescricao_contrato() {
		return descricao_contrato;
	}
	public String getData_inicio_contrato() {
		return data_inicio_contrato;
	}
	public String getData_fim_contrato() {
		return data_fim_contrato;
	}
	public String getValor_contrato() {
		return valor_contrato;
	}
	public String getData_extenso() {
		return data_extenso;
	}
	public String getProfissao() {
		return profissao;
	}
	public String getEstado_civil() {
		return estado_civil;
	}
	public String getIdentidade() {
		return identidade;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public String getAnotacoes() {
		return anotacoes;
	}
	public String getHistorico() {
		return historico;
	}
	public List<ContratoMaterialRTF> getServicos() {
		return servicos;
	}
	public List<ContratoParcelaRTF> getParcelas() {
		return parcelas;
	}
	public InputStream getLogo() {
		return logo;
	}
	public String getEmail_cliente() {
		return email_cliente;
	}
	public String getIr() {
		return ir;
	}
	public String getIss() {
		return iss;
	}
	public String getPis() {
		return pis;
	}
	public String getCofins() {
		return cofins;
	}
	public String getCsll() {
		return csll;
	}
	public String getInss() {
		return inss;
	}
	public String getIcms() {
		return icms;
	}
	public String getValor_ir() {
		return valor_ir;
	}
	public String getValor_iss() {
		return valor_iss;
	}
	public String getValor_pis() {
		return valor_pis;
	}
	public String getValor_cofins() {
		return valor_cofins;
	}
	public String getValor_csll() {
		return valor_csll;
	}
	public String getValor_inss() {
		return valor_inss;
	}
	public String getValor_icms() {
		return valor_icms;
	}
	public String getValor_acrescimo() {
		return valor_acrescimo;
	}
	public String getQuantidadeparcelas() {
		return quantidadeparcelas;
	}
	public String getNotaremessalocacao() {
		return notaremessalocacao;
	}
	public void setNotaremessalocacao(String notaremessalocacao) {
		this.notaremessalocacao = notaremessalocacao;
	}
	public void setIr(String ir) {
		this.ir = ir;
	}
	public void setIss(String iss) {
		this.iss = iss;
	}
	public void setPis(String pis) {
		this.pis = pis;
	}
	public void setCofins(String cofins) {
		this.cofins = cofins;
	}
	public void setCsll(String csll) {
		this.csll = csll;
	}
	public void setInss(String inss) {
		this.inss = inss;
	}
	public void setIcms(String icms) {
		this.icms = icms;
	}
	public void setValor_ir(String valorIr) {
		valor_ir = valorIr;
	}
	public void setValor_iss(String valorIss) {
		valor_iss = valorIss;
	}
	public void setValor_pis(String valorPis) {
		valor_pis = valorPis;
	}
	public void setValor_cofins(String valorCofins) {
		valor_cofins = valorCofins;
	}
	public void setValor_csll(String valorCsll) {
		valor_csll = valorCsll;
	}
	public void setValor_inss(String valorInss) {
		valor_inss = valorInss;
	}
	public void setValor_icms(String valorIcms) {
		valor_icms = valorIcms;
	}
	public void setEmail_cliente(String emailCliente) {
		email_cliente = emailCliente;
	}
	public void setLogo(InputStream logo) {
		this.logo = logo;
	}
	public String getCep_cliente() {
		return cep_cliente;
	}
	public String getCep_enderecoentrega() {
		return cep_enderecoentrega;
	}
	public String getCnae_contrato() {
		return cnae_contrato;
	}
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}
	public String getContato_cliente() {
		return contato_cliente;
	}
	public String getValorporextenso_contrato() {
		return valorporextenso_contrato;
	}
	public String getData_vencimento_contrato() {
		return data_vencimento_contrato;
	}
	public String getEnderecocompleto_cobranca_cliente() {
		return enderecocompleto_cobranca_cliente;
	}
	public String getEnderecocompleto_entrega_cliente() {
		return enderecocompleto_entrega_cliente;
	}
	public String getEnderecocompleto_faturamento_cliente() {
		return enderecocompleto_faturamento_cliente;
	}
	public String getEnderecocompleto_entrega_contrato() {
		return enderecocompleto_entrega_contrato;
	}
	public String getEnderecocompleto_faturamento_contrato() {
		return enderecocompleto_faturamento_contrato;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getTelefoneprincipal() {
		return telefoneprincipal;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public String getEmail_contato() {
		return email_contato;
	}
	public String getCpf_representante() {
		return cpf_representante;
	}
	public String getRg_representante() {
		return rg_representante;
	}
	public String getData_inicio_locacao() {
		return data_inicio_locacao;
	}
	public String getData_fim_locacao() {
		return data_fim_locacao;
	}
	public void setData_inicio_locacao(String dataInicioLocacao) {
		data_inicio_locacao = dataInicioLocacao;
	}
	public void setData_fim_locacao(String dataFimLocacao) {
		data_fim_locacao = dataFimLocacao;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setTelefoneprincipal(String telefoneprincipal) {
		this.telefoneprincipal = telefoneprincipal;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setData_vencimento_contrato(String dataVencimentoContrato) {
		data_vencimento_contrato = dataVencimentoContrato;
	}
	public void setEnderecocompleto_cobranca_cliente(
			String enderecocompletoCobrancaCliente) {
		enderecocompleto_cobranca_cliente = enderecocompletoCobrancaCliente;
	}
	public void setEnderecocompleto_entrega_cliente(
			String enderecocompletoEntregaCliente) {
		enderecocompleto_entrega_cliente = enderecocompletoEntregaCliente;
	}
	public void setEnderecocompleto_faturamento_cliente(
			String enderecocompletoFaturamentoCliente) {
		enderecocompleto_faturamento_cliente = enderecocompletoFaturamentoCliente;
	}
	public void setEnderecocompleto_entrega_contrato(
			String enderecocompletoEntregaContrato) {
		enderecocompleto_entrega_contrato = enderecocompletoEntregaContrato;
	}
	public void setEnderecocompleto_faturamento_contrato(
			String enderecocompletoFaturamentoContrato) {
		enderecocompleto_faturamento_contrato = enderecocompletoFaturamentoContrato;
	}
	public void setCep_cliente(String cepCliente) {
		cep_cliente = cepCliente;
	}
	public void setCep_enderecoentrega(String cepEnderecoentrega) {
		cep_enderecoentrega = cepEnderecoentrega;
	}
	public void setCnae_contrato(String cnaeContrato) {
		cnae_contrato = cnaeContrato;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public void setContato_cliente(String contatoCliente) {
		contato_cliente = contatoCliente;
	}
	public void setValorporextenso_contrato(String valorporextensoContrato) {
		valorporextenso_contrato = valorporextensoContrato;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setEnderecocompletoentrega(String enderecocompletoentrega) {
		this.enderecocompletoentrega = enderecocompletoentrega;
	}
	public void setEnderecoentrega(String enderecoentrega) {
		this.enderecoentrega = enderecoentrega;
	}
	public void setComplementoentrega(String complementoentrega) {
		this.complementoentrega = complementoentrega;
	}
	public void setNumeroentrega(String numeroentrega) {
		this.numeroentrega = numeroentrega;
	}
	public void setBairroentrega(String bairroentrega) {
		this.bairroentrega = bairroentrega;
	}
	public void setMunicipioentrega(String municipioentrega) {
		this.municipioentrega = municipioentrega;
	}
	public void setUfentrega(String ufentrega) {
		this.ufentrega = ufentrega;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setDescricao_contrato(String descricaoContrato) {
		descricao_contrato = descricaoContrato;
	}
	public void setData_inicio_contrato(String dataInicioContrato) {
		data_inicio_contrato = dataInicioContrato;
	}
	public void setData_fim_contrato(String dataFimContrato) {
		data_fim_contrato = dataFimContrato;
	}
	public void setValor_contrato(String valorContrato) {
		valor_contrato = valorContrato;
	}
	public void setData_extenso(String dataExtenso) {
		data_extenso = dataExtenso;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public void setEstado_civil(String estadoCivil) {
		estado_civil = estadoCivil;
	}
	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}
	public void setHistorico(String observacao) {
		this.historico = observacao;
	}
	public void setServicos(List<ContratoMaterialRTF> servicos) {
		this.servicos = servicos;
	}
	public void setParcelas(List<ContratoParcelaRTF> parcelas) {
		this.parcelas = parcelas;
	}
	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getContato_contrato() {
		return contato_contrato;
	}
	public void setContato_contrato(String contatoContrato) {
		contato_contrato = contatoContrato;
	}
	public String getCdcontrato() {
		return cdcontrato;
	}
	public String getRepresentante() {
		return representante;
	}
	public String getEnderecocompleto() {
		return enderecocompleto;
	}
	public String getIndicecorrecao() {
		return indicecorrecao;
	}
	public String getValortotalservicosporextenso_contrato() {
		return valortotalservicosporextenso_contrato;
	}
	public String getValortotalservicos_contrato() {
		return valortotalservicos_contrato;
	}
	public String getValortotalservicosporextensocusto_contrato() {
		return valortotalservicosporextensocusto_contrato;
	}
	public String getValortotalservicoscusto_contrato() {
		return valortotalservicoscusto_contrato;
	}
	public void setCdcontrato(String cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setRepresentante(String representante) {
		this.representante = representante;
	}
	public void setEnderecocompleto(String enderecocompleto) {
		this.enderecocompleto = enderecocompleto;
	}
	public void setIndicecorrecao(String indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public void setValortotalservicosporextenso_contrato(String valortotalservicosporextensoContrato) {
		valortotalservicosporextenso_contrato = valortotalservicosporextensoContrato;
	}
	public void setValortotalservicos_contrato(String valortotalservicosContrato) {
		valortotalservicos_contrato = valortotalservicosContrato;
	}
	public void setValortotalservicosporextensocusto_contrato(String valortotalservicosporextensocustoContrato) {
		valortotalservicosporextensocusto_contrato = valortotalservicosporextensocustoContrato;
	}
	public void setValortotalservicoscusto_contrato(String valortotalservicoscustoContrato) {
		valortotalservicoscusto_contrato = valortotalservicoscustoContrato;
	}
	public String getVendedornome() {
		return vendedornome;
	}
	public String getVendedoridentidade() {
		return vendedoridentidade;
	}
	public void setVendedornome(String vendedornome) {
		this.vendedornome = vendedornome;
	}
	public void setVendedoridentidade(String vendedoridentidade) {
		this.vendedoridentidade = vendedoridentidade;
	}
	public String getData_renovacao() {
		return data_renovacao;
	}
	public String getDia_vencimento() {
		return dia_vencimento;
	}
	public String getNome_contato_socio() {
		return nome_contato_socio;
	}
	public String getCpf_contato_socio() {
		return cpf_contato_socio;
	}
	public void setData_renovacao(String dataRenovacao) {
		data_renovacao = dataRenovacao;
	}
	public void setDia_vencimento(String diaVencimento) {
		dia_vencimento = diaVencimento;
	}
	public void setNome_contato_socio(String nomeContatoSocio) {
		nome_contato_socio = nomeContatoSocio;
	}
	public void setCpf_contato_socio(String cpfContatoSocio) {
		cpf_contato_socio = cpfContatoSocio;
	}
	public String getHorainicio() {
		return horainicio;
	}
	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}
	public String getPeriodicidade() {
		return periodicidade;
	}
	public String getValor_adicional() {
		return valor_adicional;
	}
	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	public void setValor_adicional(String valorAdicional) {
		valor_adicional = valorAdicional;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setValor_acrescimo(String valorAcrescimo) {
		valor_acrescimo = valorAcrescimo;
	}
	public void setEmail_contato(String emailContato) {
		this.email_contato = emailContato;
	}
	public void setQuantidadeparcelas(String quantidadeparcelas) {
		this.quantidadeparcelas = quantidadeparcelas;
	}
	public void setCpf_representante(String cpfRepresentante) {
		this.cpf_representante = cpfRepresentante;
	}
	public void setRg_representante(String rgRepresentante) {
		this.rg_representante = rgRepresentante;
	}
	public String getData_proxima_parcela() {
		return data_proxima_parcela;
	}
	public String getValor_proxima_parcela() {
		return valor_proxima_parcela;
	}
	public void setData_proxima_parcela(String dataProximaParcela) {
		data_proxima_parcela = dataProximaParcela;
	}
	public void setValor_proxima_parcela(String valorProximaParcela) {
		valor_proxima_parcela = valorProximaParcela;
	}
	public String getPeriodicidadeRenovacao() {
		return periodicidadeRenovacao;
	}
	public void setPeriodicidadeRenovacao(String periodicidadeRenovacao) {
		this.periodicidadeRenovacao = periodicidadeRenovacao;
	}
	public String getDtemissaonotaremessalocacao() {
		return dtemissaonotaremessalocacao;
	}
	public void setDtemissaonotaremessalocacao(String dtemissaonotaremessalocacao) {
		this.dtemissaonotaremessalocacao = dtemissaonotaremessalocacao;
	}
	public String getSaldoromaneio() {
		return saldoromaneio;
	}
	public void setSaldoromaneio(String saldoromaneio) {
		this.saldoromaneio = saldoromaneio;
	}
	
}
