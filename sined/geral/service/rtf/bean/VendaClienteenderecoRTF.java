package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Endereco;


public class VendaClienteenderecoRTF {

	private String tipoendereco;
	private String cep;
	private String logradouro;
	private String complemento;
	private String numero;
	private String bairro;
	private String municipio;
	private String uf;
	private String pais;
	
	public VendaClienteenderecoRTF(Endereco endereco){
		if(endereco != null){
			this.setTipoendereco(endereco.getEnderecotipo() != null? endereco.getEnderecotipo().getNome(): "");
			this.setCep(endereco.getCep() != null? endereco.getCep().toString(): "");
			this.setLogradouro(endereco.getLogradouro());
			this.setComplemento(endereco.getComplemento());
			this.setNumero(endereco.getNumero());
			this.setBairro(endereco.getBairro());
			this.setMunicipio(endereco.getMunicipio() != null? endereco.getMunicipio().getNome(): "");
			this.setUf(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null? endereco.getMunicipio().getUf().getSigla(): "");
			this.setPais(endereco.getPais() != null? endereco.getPais().getNome(): "");
			
		}
	}
	
	public String getTipoendereco() {
		return tipoendereco;
	}
	public void setTipoendereco(String tipoendereco) {
		this.tipoendereco = tipoendereco;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
	
}
