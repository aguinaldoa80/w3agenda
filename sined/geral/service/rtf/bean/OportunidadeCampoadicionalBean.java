package br.com.linkcom.sined.geral.service.rtf.bean;

public class OportunidadeCampoadicionalBean {
	
	private String campo;
	private String valor;
	
	public OportunidadeCampoadicionalBean(){}
	
	public OportunidadeCampoadicionalBean(String campo, String valor){
		this.campo = campo;
		this.valor = valor;
	}
	
	public String getCampo() {
		return campo;
	}
	public String getValor() {
		return valor;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
