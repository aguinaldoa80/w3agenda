package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;
import java.util.List;

public class ContratojuridicoRTF {
	
	private String id_contrato;
	private String tipo_contrato;
	private String nome_cliente;
	private String endereco;
	private String complemento;
	private String numero;
	private String bairro;
	private String municipio;
	private String uf;
	private String cpf;
	private String cnpj;
	private String celular;
	private String telefone;
	private String descricao_contrato;
	private String data_inicio_contrato;
	private String data_fim_contrato;
	private String valor_contrato;
	private String valor_contrato_extenso;
	private String data_extenso;
	private String profissao;
	private String estado_civil;
	private String identidade;
	private String nacionalidade;
	private String anotacoes;
	private String tipo_honorario;
	private String honorario;
	private InputStream logo;
	
	private List<ContratojuridicoParcelaRTF> parcelas;

	public String getId_contrato() {
		return id_contrato;
	}

	public String getTipo_contrato() {
		return tipo_contrato;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getNumero() {
		return numero;
	}

	public String getBairro() {
		return bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public String getUf() {
		return uf;
	}

	public String getCpf() {
		return cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public String getCelular() {
		return celular;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getDescricao_contrato() {
		return descricao_contrato;
	}

	public String getData_inicio_contrato() {
		return data_inicio_contrato;
	}

	public String getData_fim_contrato() {
		return data_fim_contrato;
	}

	public String getValor_contrato() {
		return valor_contrato;
	}

	public String getValor_contrato_extenso() {
		return valor_contrato_extenso;
	}

	public String getData_extenso() {
		return data_extenso;
	}

	public String getProfissao() {
		return profissao;
	}

	public String getEstado_civil() {
		return estado_civil;
	}

	public String getIdentidade() {
		return identidade;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public String getAnotacoes() {
		return anotacoes;
	}

	public String getTipo_honorario() {
		return tipo_honorario;
	}

	public String getHonorario() {
		return honorario;
	}

	public List<ContratojuridicoParcelaRTF> getParcelas() {
		return parcelas;
	}
	
	public InputStream getLogo() {
		return logo;
	}
	
	public void setLogo(InputStream logo) {
		this.logo = logo;
	}

	public void setId_contrato(String idContrato) {
		id_contrato = idContrato;
	}

	public void setTipo_contrato(String tipoContrato) {
		tipo_contrato = tipoContrato;
	}

	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setDescricao_contrato(String descricaoContrato) {
		descricao_contrato = descricaoContrato;
	}

	public void setData_inicio_contrato(String dataInicioContrato) {
		data_inicio_contrato = dataInicioContrato;
	}

	public void setData_fim_contrato(String dataFimContrato) {
		data_fim_contrato = dataFimContrato;
	}

	public void setValor_contrato(String valorContrato) {
		valor_contrato = valorContrato;
	}

	public void setValor_contrato_extenso(String valorContratoExtenso) {
		valor_contrato_extenso = valorContratoExtenso;
	}

	public void setData_extenso(String dataExtenso) {
		data_extenso = dataExtenso;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public void setEstado_civil(String estadoCivil) {
		estado_civil = estadoCivil;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}

	public void setTipo_honorario(String tipoHonorario) {
		tipo_honorario = tipoHonorario;
	}

	public void setHonorario(String honorario) {
		this.honorario = honorario;
	}

	public void setParcelas(List<ContratojuridicoParcelaRTF> parcelas) {
		this.parcelas = parcelas;
	}

}
