package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Contacrm;

public class VendaContaRTF {

	private String nome;
	private String cpf;
	private String cnpj;
	
	public VendaContaRTF(Contacrm contacrm){
		if(contacrm != null){
			this.setNome(contacrm.getNome());
			this.setCnpj(contacrm.getCnpj() != null? contacrm.getCnpj().toString(): "");
			this.setCpf(contacrm.getCpf() != null? contacrm.getCpf().toString(): "");
		}
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
