package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;

public class PropostaRTF {

	private String nomeCliente;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;
	private String uf;
	private String cpf;
	private String cnpj;
	private String descricaoProposta;
	private String prazoPagamento;
	private String valorTotal;
	private String numeroProposta;
	private String contatoCliente;
	private String observacao;
	private String dataAtualExtenso;
	private InputStream logo;
	private String email_contato;
	private String telefone_contato;
	private String razaosocial_empresa;
	private String revisao_validade;
	private String revisao_prazoexecucao;
	private String nome_projeto;
	private String nome_responsavelproposta;
	private String prazoexecucao;
	private String valormateriais;
	private String valormaodeobra;

	
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getContatoCliente() {
		return contatoCliente;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getDataAtualExtenso() {
		return dataAtualExtenso;
	}
	public InputStream getLogo() {
		return logo;
	}
	public void setLogo(InputStream logo) {
		this.logo = logo;
	}
	public void setContatoCliente(String contatoCliente) {
		this.contatoCliente = contatoCliente;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDataAtualExtenso(String dataAtualExtenso) {
		this.dataAtualExtenso = dataAtualExtenso;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getDescricaoProposta() {
		return descricaoProposta;
	}
	public void setDescricaoProposta(String descricaoProposta) {
		this.descricaoProposta = descricaoProposta;
	}
	public String getPrazoPagamento() {
		return prazoPagamento;
	}
	public void setPrazoPagamento(String prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	public String getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getNumeroProposta() {
		return numeroProposta;
	}
	public void setNumeroProposta(String numeroProposta) {
		this.numeroProposta = numeroProposta;
	}
	public String getEmail_contato() {
		return email_contato;
	}
	public String getTelefone_contato() {
		return telefone_contato;
	}
	public String getNome_projeto() {
		return nome_projeto;
	}
	public String getNome_responsavelproposta() {
		return nome_responsavelproposta;
	}
	public String getPrazoexecucao() {
		return prazoexecucao;
	}
	public void setEmail_contato(String emailContato) {
		email_contato = emailContato;
	}
	public void setTelefone_contato(String telefoneContato) {
		telefone_contato = telefoneContato;
	}
	public String getRazaosocial_empresa() {
		return razaosocial_empresa;
	}
	public String getRevisao_validade() {
		return revisao_validade;
	}
	public String getRevisao_prazoexecucao() {
		return revisao_prazoexecucao;
	}
	public String getValormateriais() {
		return valormateriais;
	}
	public String getValormaodeobra() {
		return valormaodeobra;
	}
	
	public void setRazaosocial_empresa(String razaosocialEmpresa) {
		razaosocial_empresa = razaosocialEmpresa;
	}
	public void setRevisao_validade(String revisaoValidade) {
		revisao_validade = revisaoValidade;
	}
	public void setRevisao_prazoexecucao(String revisaoPrazoexecucao) {
		revisao_prazoexecucao = revisaoPrazoexecucao;
	}
	public void setNome_projeto(String nomeProjeto) {
		nome_projeto = nomeProjeto;
	}
	public void setNome_responsavelproposta(String nomeResponsavelproposta) {
		nome_responsavelproposta = nomeResponsavelproposta;
	}
	public void setPrazoexecucao(String prazoexecucao) {
		this.prazoexecucao = prazoexecucao;
	}
	public void setValormateriais(String valormateriais) {
		this.valormateriais = valormateriais;
	}
	public void setValormaodeobra(String valormaodeobra) {
		this.valormaodeobra = valormaodeobra;
	}
}
