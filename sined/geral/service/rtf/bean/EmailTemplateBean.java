package br.com.linkcom.sined.geral.service.rtf.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class EmailTemplateBean {

	private String nomeCliente;
	private String emailCliente;
	private String nomeContatoCliente;
	private String emailContatoCliente;
	private String descricaoContaReceber;
	private Date vencimentoContaReceber;
	private String textoautenticidadeboleto;
	private String numeroDocumento;
	private Money valorOriginal;
	private Money valorAtual;
	private String nomeEmpresa;
	private String emailEmpresa;
	private String telefoneEmpresa;
	private String complementoTextoBoleto;
	private String linkComCaptcha;
	private String linkDireto;
	
	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getNomeContatoCliente() {
		return nomeContatoCliente;
	}

	public void setNomeContatoCliente(String nomeContatoCliente) {
		this.nomeContatoCliente = nomeContatoCliente;
	}

	public String getEmailContatoCliente() {
		return emailContatoCliente;
	}
	
	public void setEmailContatoCliente(String emailContatoCliente) {
		this.emailContatoCliente = emailContatoCliente;
	}

	public String getDescricaoContaReceber() {
		return descricaoContaReceber;
	}

	public void setDescricaoContaReceber(String descricaoContaReceber) {
		this.descricaoContaReceber = descricaoContaReceber;
	}

	public Date getVencimentoContaReceber() {
		return vencimentoContaReceber;
	}

	public void setVencimentoContaReceber(Date vencimentoContaReceber) {
		this.vencimentoContaReceber = vencimentoContaReceber;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getLinkComCaptcha() {
		return linkComCaptcha;
	}
	
	public void setLinkComCaptcha(String linkComCaptcha) {
		this.linkComCaptcha = linkComCaptcha;
	}
	
	public String getLinkDireto() {
		return linkDireto;
	}
	
	public void setLinkDireto(String linkDireto) {
		this.linkDireto = linkDireto;
	}

	public Money getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Money valorOriginal) {
		this.valorOriginal = valorOriginal;
	}

	public Money getValorAtual() {
		return valorAtual;
	}

	public void setValorAtual(Money valorAtual) {
		this.valorAtual = valorAtual;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getEmailEmpresa() {
		return emailEmpresa;
	}

	public void setEmailEmpresa(String emailEmpresa) {
		this.emailEmpresa = emailEmpresa;
	}

	public String getTelefoneEmpresa() {
		return telefoneEmpresa;
	}

	public void setTelefoneEmpresa(String telefoneEmpresa) {
		this.telefoneEmpresa = telefoneEmpresa;
	}
	
	public String getComplementoTextoBoleto() {
		return complementoTextoBoleto;
	}
	
	public void setComplementoTextoBoleto(String complementoTextoBoleto) {
		this.complementoTextoBoleto = complementoTextoBoleto;
	}
	
	public String getTextoautenticidadeboleto() {
		return textoautenticidadeboleto;
	}
	
	public void setTextoautenticidadeboleto(String textoautenticidadeboleto) {
		this.textoautenticidadeboleto = textoautenticidadeboleto;
	}
}
