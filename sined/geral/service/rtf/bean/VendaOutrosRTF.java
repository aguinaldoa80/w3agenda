package br.com.linkcom.sined.geral.service.rtf.bean;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;

public class VendaOutrosRTF {

	private String proposta;
	
	public VendaOutrosRTF(Vendaorcamento vendaorcamento){
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null && vendaorcamento.getDtorcamento() != null &&
			vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getRazaosocialOuNome() != null){
			this.setProposta(new SimpleDateFormat("yyyy").format(vendaorcamento.getDtorcamento())+
																vendaorcamento.getEmpresa().getRazaosocialOuNome().substring(0, 1)+
																vendaorcamento.getCdvendaorcamento().toString());
		}
	}
	
	public VendaOutrosRTF(Pedidovenda pedidovenda){
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null && pedidovenda.getDtpedidovenda() != null &&
				pedidovenda.getEmpresa() != null && pedidovenda.getEmpresa().getRazaosocialOuNome() != null){
			this.setProposta(new SimpleDateFormat("yyyy").format(pedidovenda.getDtpedidovenda())+
					pedidovenda.getEmpresa().getRazaosocialOuNome().substring(0, 1)+
					pedidovenda.getCdpedidovenda().toString());
		}
	}
	
	public String getProposta() {
		return proposta;
	}
	public void setProposta(String proposta) {
		this.proposta = proposta;
	}
}
