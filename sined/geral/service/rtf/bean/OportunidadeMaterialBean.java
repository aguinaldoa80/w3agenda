package br.com.linkcom.sined.geral.service.rtf.bean;

public class OportunidadeMaterialBean {
	
	private String nome_material;
	private String qtd_material;
	private String valor_unitario_material;
	private String valor_total_material;
	private String prazo_entrega_material;
	private String caracteristica_material;
	private String observacao_material;
	private String ncm_material;
	private String ncmcompleto_material;
	private String garantia_material;
	
	public String getNome_material() {
		return nome_material;
	}
	public String getQtd_material() {
		return qtd_material;
	}
	public String getValor_unitario_material() {
		return valor_unitario_material;
	}
	public String getValor_total_material() {
		return valor_total_material;
	}
	public String getPrazo_entrega_material() {
		return prazo_entrega_material;
	}
	public String getCaracteristica_material() {
		return caracteristica_material;
	}
	public String getObservacao_material() {
		return observacao_material;
	}
	
	public void setNome_material(String nomeMaterial) {
		nome_material = nomeMaterial;
	}
	public void setQtd_material(String qtdMaterial) {
		qtd_material = qtdMaterial;
	}
	public void setValor_unitario_material(String valorUnitarioMaterial) {
		valor_unitario_material = valorUnitarioMaterial;
	}
	public void setValor_total_material(String valorTotalMaterial) {
		valor_total_material = valorTotalMaterial;
	}
	public void setPrazo_entrega_material(String prazoEntregaMaterial) {
		prazo_entrega_material = prazoEntregaMaterial;
	}
	public void setCaracteristica_material(String caracteristicaMaterial) {
		caracteristica_material = caracteristicaMaterial;
	}
	public void setObservacao_material(String observacaoMaterial) {
		observacao_material = observacaoMaterial;
	}
	public String getNcm_material() {
		return ncm_material;
	}
	public String getNcmcompleto_material() {
		return ncmcompleto_material;
	}
	public String getGarantia_material() {
		return garantia_material;
	}
	public void setNcm_material(String ncmMaterial) {
		ncm_material = ncmMaterial;
	}
	public void setNcmcompleto_material(String ncmcompletoMaterial) {
		ncmcompleto_material = ncmcompletoMaterial;
	}
	public void setGarantia_material(String garantiaMaterial) {
		garantia_material = garantiaMaterial;
	}
}
