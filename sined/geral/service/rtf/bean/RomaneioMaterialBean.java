package br.com.linkcom.sined.geral.service.rtf.bean;

import java.util.LinkedList;

public class RomaneioMaterialBean {

	private String nome;
	private String codigoBarras;
	private String identificador;
	private Integer cdMaterial;
	private Double quantidade;
	private LinkedList<RomaneioMaterialNumeroSerieBean> materialNumeroSerieList;

	public RomaneioMaterialBean() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Integer getCdMaterial() {
		return cdMaterial;
	}

	public void setCdMaterial(Integer cdMaterial) {
		this.cdMaterial = cdMaterial;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public LinkedList<RomaneioMaterialNumeroSerieBean> getMaterialNumeroSerieList() {
		if (materialNumeroSerieList == null) {
			materialNumeroSerieList = new LinkedList<RomaneioMaterialNumeroSerieBean>();
		}
		
		return materialNumeroSerieList;
	}
	
	public void setMaterialNumeroSerieList(
			LinkedList<RomaneioMaterialNumeroSerieBean> materialNumeroSerieList) {
		this.materialNumeroSerieList = materialNumeroSerieList;
	}
}
