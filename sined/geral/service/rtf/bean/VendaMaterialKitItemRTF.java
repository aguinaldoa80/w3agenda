package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class VendaMaterialKitItemRTF {

	private String descricao;
	private String unidademedida;
	private String unidademedidasimbolo;
	private Double quantidade;
	private Double preco;
	private String dtentrega;
	private Double total;
	private String observacao;
	private String loteestoque;
	private String desconto;
	private String comprimento;
	private String altura;
	private String largura;
	private Material material;
	private String codigo;
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
	public String getUnidademedidasimbolo() {
		return unidademedidasimbolo;
	}
	public void setUnidademedidasimbolo(String unidademedidasimbolo) {
		this.unidademedidasimbolo = unidademedidasimbolo;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public String getDtentrega() {
		return dtentrega;
	}
	public void setDtentrega(String dtentrega) {
		this.dtentrega = dtentrega;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(String loteestoque) {
		this.loteestoque = loteestoque;
	}
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public String getComprimento() {
		return comprimento;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public String getLargura() {
		return largura;
	}
	public void setLargura(String largura) {
		this.largura = largura;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}