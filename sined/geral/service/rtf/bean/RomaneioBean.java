package br.com.linkcom.sined.geral.service.rtf.bean;

import java.sql.Date;
import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.enumeration.Romaneiotipo;

public class RomaneioBean {

	private Integer cdRomaneio;
	private Romaneiotipo romaneioTipo;
	private String localOrigem;
	private String localDestino;
	private String descricao;
	private String placa;
	private String motorista;
	private String solicitante;
	private String conferente;
	private Double qtdeAjudantes;
	private Date dtRomaneio;

	private RomaneioEmpresaBean empresaBean;
	private RomaneioClienteBean clienteBean;
	private LinkedList<RomaneioItemBean> romaneioItemBeanList;

	public RomaneioBean() {}

	public Integer getCdRomaneio() {
		return cdRomaneio;
	}

	public void setCdRomaneio(Integer cdRomaneio) {
		this.cdRomaneio = cdRomaneio;
	}

	public Romaneiotipo getRomaneioTipo() {
		return romaneioTipo;
	}
	
	public void setRomaneioTipo(Romaneiotipo romaneioTipo) {
		this.romaneioTipo = romaneioTipo;
	}

	public String getLocalDestino() {
		return localDestino;
	}
	
	public void setLocalDestino(String localDestino) {
		this.localDestino = localDestino;
	}
	
	public String getLocalOrigem() {
		return localOrigem;
	}
	
	public void setLocalOrigem(String localOrigem) {
		this.localOrigem = localOrigem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getMotorista() {
		return motorista;
	}

	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public String getConferente() {
		return conferente;
	}

	public void setConferente(String conferente) {
		this.conferente = conferente;
	}

	public Double getQtdeAjudantes() {
		return qtdeAjudantes;
	}

	public void setQtdeAjudantes(Double qtdeAjudantes) {
		this.qtdeAjudantes = qtdeAjudantes;
	}

	public Date getDtRomaneio() {
		return dtRomaneio;
	}

	public void setDtRomaneio(Date dtRomaneio) {
		this.dtRomaneio = dtRomaneio;
	}

	public RomaneioEmpresaBean getEmpresaBean() {
		return empresaBean;
	}

	public void setEmpresaBean(RomaneioEmpresaBean empresaBean) {
		this.empresaBean = empresaBean;
	}

	public RomaneioClienteBean getClienteBean() {
		return clienteBean;
	}

	public void setClienteBean(RomaneioClienteBean clienteBean) {
		this.clienteBean = clienteBean;
	}
	
	public LinkedList<RomaneioItemBean> getRomaneioItemBeanList() {
		if (romaneioItemBeanList == null) {
			romaneioItemBeanList = new LinkedList<RomaneioItemBean>();
		}
		
		return romaneioItemBeanList;
	}
	
	public void setRomaneioItemBeanList(LinkedList<RomaneioItemBean> romaneioItemBeanList) {
		this.romaneioItemBeanList = romaneioItemBeanList;
	}
}
