package br.com.linkcom.sined.geral.service.rtf.bean;

public class RomaneioItemBean {

	private RomaneioMaterialBean romaneioMaterialBean;
	private RomaneioPatrimonioBean romaneioPatrimonioBean;
	private String numeroLote;

	public RomaneioItemBean(){}
	
	public RomaneioMaterialBean getRomaneioMaterialBean() {
		return romaneioMaterialBean;
	}

	public void setRomaneioMaterialBean(RomaneioMaterialBean romaneioMaterialBean) {
		this.romaneioMaterialBean = romaneioMaterialBean;
	}

	public RomaneioPatrimonioBean getRomaneioPatrimonioBean() {
		return romaneioPatrimonioBean;
	}

	public void setRomaneioPatrimonioBean(RomaneioPatrimonioBean romaneioPatrimonioBean) {
		this.romaneioPatrimonioBean = romaneioPatrimonioBean;
	}

	public String getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
}
