package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;

public class VendaMaterialKitflexivelRTF {

	private String codigo;
	private String cdmaterial;
	private String descricao;
	private Double pesoliquido;
	private Double pesobruto;
	private String ncmcompleto;
	private String observacao;
	private String caracteristica;
	private String identificadormaterialmestre;
	private Double desconto;
	private String dtentrega;
	private String unidademedida;
	private String unidademedidasimbolo;
	private List<VendaMaterialKitflexivelItemRTF> itens;
	private Material material;
	private Double quantidade;
	private Double preco;
	private Double total;
	private InputStream imagem;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCdmaterial() {
		cdmaterial = "123456";
		return cdmaterial;
	}
	public void setCdmaterial(String cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getPesoliquido() {
		return pesoliquido;
	}
	public void setPesoliquido(Double pesoliquido) {
		this.pesoliquido = pesoliquido;
	}
	public Double getPesobruto() {
		return pesobruto;
	}
	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public String getIdentificadormaterialmestre() {
		return identificadormaterialmestre;
	}
	public void setIdentificadormaterialmestre(String identificadormaterialmestre) {
		this.identificadormaterialmestre = identificadormaterialmestre;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public String getDtentrega() {
		return dtentrega;
	}
	public void setDtentrega(String dtentrega) {
		this.dtentrega = dtentrega;
	}
	public String getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
	public String getUnidademedidasimbolo() {
		return unidademedidasimbolo;
	}
	public void setUnidademedidasimbolo(String unidademedidasimbolo) {
		this.unidademedidasimbolo = unidademedidasimbolo;
	}
	public List<VendaMaterialKitflexivelItemRTF> getItens() {
		if(itens == null){
			itens = new ArrayList<VendaMaterialKitflexivelItemRTF>();
		}
		return itens;
	}
	public void setItens(List<VendaMaterialKitflexivelItemRTF> itens) {
		this.itens = itens;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public InputStream getImagem() {
		return imagem;
	}
	public void setImagem(InputStream imagem) {
		this.imagem = imagem;
	}
}