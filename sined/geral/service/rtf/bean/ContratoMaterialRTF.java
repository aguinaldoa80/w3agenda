package br.com.linkcom.sined.geral.service.rtf.bean;

public class ContratoMaterialRTF {
	
	private String nome_material;
	private String qtd_material;
	private String valor_unitario_material;
	private String valor_custo_material;
	private String valor_total_material;
	private String valor_total_custo_material;
	private String valor_indenizacao_material;
	private String horario_inicio;
	private String caracteristica_material;
	private String observacao;
	
	public String getNome_material() {
		return nome_material;
	}
	public String getQtd_material() {
		return qtd_material;
	}
	public String getValor_unitario_material() {
		return valor_unitario_material;
	}
	public String getValor_total_material() {
		return valor_total_material;
	}
	public String getValor_indenizacao_material() {
		return valor_indenizacao_material;
	}
	public String getCaracteristica_material() {
		return caracteristica_material;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setValor_indenizacao_material(String valorIndenizacaoMaterial) {
		valor_indenizacao_material = valorIndenizacaoMaterial;
	}
	public void setNome_material(String nomeMaterial) {
		nome_material = nomeMaterial;
	}
	public void setQtd_material(String qtdMaterial) {
		qtd_material = qtdMaterial;
	}
	public void setValor_unitario_material(String valorUnitarioMaterial) {
		valor_unitario_material = valorUnitarioMaterial;
	}
	public void setValor_total_material(String valorTotalMaterial) {
		valor_total_material = valorTotalMaterial;
	}
	public String getValor_custo_material() {
		return valor_custo_material;
	}
	public String getValor_total_custo_material() {
		return valor_total_custo_material;
	}
	public void setValor_custo_material(String valorCustoMaterial) {
		valor_custo_material = valorCustoMaterial;
	}
	public void setValor_total_custo_material(String valorTotalCustoMaterial) {
		valor_total_custo_material = valorTotalCustoMaterial;
	}
	public String getHorario_inicio() {
		return horario_inicio;
	}
	public void setHorario_inicio(String horarioInicio) {
		horario_inicio = horarioInicio;
	}
	public void setCaracteristica_material(String caracteristicaMaterial) {
		caracteristica_material = caracteristicaMaterial;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
