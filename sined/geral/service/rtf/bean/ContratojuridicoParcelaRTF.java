package br.com.linkcom.sined.geral.service.rtf.bean;

public class ContratojuridicoParcelaRTF {
	
	private String data_vencimento;
	private String valor;
	
	public String getData_vencimento() {
		return data_vencimento;
	}
	public String getValor() {
		return valor;
	}
	public void setData_vencimento(String dataVencimento) {
		data_vencimento = dataVencimento;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

}
