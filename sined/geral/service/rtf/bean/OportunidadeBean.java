package br.com.linkcom.sined.geral.service.rtf.bean;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.Map;

import br.com.linkcom.sined.modulo.crm.controller.report.bean.TelefoneBean;

public class OportunidadeBean {

	private String cdoportunidade;
	private String identificador;
	private String tipopessoacrm;
	private String nome_cliente;
	private String razaosocial_cliente;
	private String cpf_cnpj_cliente;
	private String cdcliente_cliente;
	private String identificador_cliente;
	private String contato_cliente;
	private String email_contato_cliente;
	private String municipio_cliente;
	private String nome_contacrm;
	private String telefone;
	private LinkedList<TelefoneBean> telefone_responsavel;
	private String nome_contato;
	private String email_contato;
	private String nome;
	private String tipo_responsavel;
	private String responsavel;
	private String data_inicio;
	private String valortotal;
	private String probabilidade;
	private String periodicidade;
	private String observacao;
	private String dataporextenso;
	private String valorporextenso;
	private Map<String, String> mapCamposAdicionais;
	private String empresa_nome;
	private String empresa_razaosocial;
	private String empresa_cnpj;
	private String empresa_ie;
	private String empresa_im;
	private String empresa_logradouro;
	private String empresa_bairro;
	private String empresa_cidade;
	private String empresa_uf;
	private String empresa_cep;
	private String empresa_telefones;
	private String empresa_email;
	private String empresa_site;
	private InputStream logo;
	private String anoAtual;
	private LinkedList<OportunidadeMaterialBean> produtos;
	private LinkedList<OportunidadeCampoadicionalBean> camposAdicionais;
	private String email_representante;
	private String telefone_representante;
	private String telefone_cliente;
	private String cpf_cnpj;
	private String cpf;
	private String cnpj;
	private String bairro;
	private String cidade;
	private String logradouro;
	private String numero;
	private String cep;
	private String uf;
	private String complemento_endereco;
	
	public String getNome_contacrm() {
		return nome_contacrm;
	}
	
	public String getTelefone() {
		return telefone;
	}

	public String getNome_contato() {
		return nome_contato;
	}

	public String getNome() {
		return nome;
	}
	
	public String getTipo_responsavel() {
		return tipo_responsavel;
	}
	
	public String getResponsavel() {
		return responsavel;
	}
	
	public String getData_inicio() {
		return data_inicio;
	}
	
	public String getValortotal() {
		return valortotal;
	}
	
	public String getProbabilidade() {
		return probabilidade;
	}

	public String getObservacao() {
		return observacao;
	}
	
	public InputStream getLogo() {
		return logo;
	}
	
	public String getCdoportunidade() {
		return cdoportunidade;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getTipopessoacrm() {
		return tipopessoacrm;
	}

	public String getNome_cliente() {
		return nome_cliente;
	}

	public String getContato_cliente() {
		return contato_cliente;
	}

	public String getEmail_contato_cliente() {
		return email_contato_cliente;
	}
	
	public String getMunicipio_cliente() {
		return municipio_cliente;
	}
	
	public String getEmail_contato() {
		return email_contato;
	}

	public String getDataporextenso() {
		return dataporextenso;
	}
	public String getValorporextenso() {
		return valorporextenso;
	}
	
	public String getPeriodicidade() {
		return periodicidade;
	}
	
	public LinkedList<OportunidadeMaterialBean> getProdutos() {
		return produtos;
	}
	
	public String campoAdicional(String nome){
		if(mapCamposAdicionais != null && mapCamposAdicionais.containsKey(nome)){
			return mapCamposAdicionais.get(nome);
		}
		return "";
	}
	
	public String getAnoAtual() {
		return anoAtual;
	}

	public String getCpf_cnpj() {
		return cpf_cnpj;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getCep() {
		return cep;
	}
	public String getUf() {
		return uf;
	}
	public String getComplemento_endereco() {
		return complemento_endereco;
	}
	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	
	public void setMapCamposAdicionais(Map<String, String> mapCamposAdicionais) {
		this.mapCamposAdicionais = mapCamposAdicionais;
	}
	
	public void setEmail_contato(String emailContato) {
		email_contato = emailContato;
	}
	
	public void setMunicipio_cliente(String municipioCliente) {
		municipio_cliente = municipioCliente;
	}

	public void setCdoportunidade(String cdoportunidade) {
		this.cdoportunidade = cdoportunidade;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setTipopessoacrm(String tipopessoacrm) {
		this.tipopessoacrm = tipopessoacrm;
	}

	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}

	public void setContato_cliente(String contatoCliente) {
		contato_cliente = contatoCliente;
	}

	public void setEmail_contato_cliente(String emailContatoCliente) {
		email_contato_cliente = emailContatoCliente;
	}

	public void setLogo(InputStream logo) {
		this.logo = logo;
	}

	public void setNome_contacrm(String nome_contacrm) {
		this.nome_contacrm = nome_contacrm;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setNome_contato(String nome_contato) {
		this.nome_contato = nome_contato;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipo_responsavel(String tipo_responsavel) {
		this.tipo_responsavel = tipo_responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public void setData_inicio(String data_inicio) {
		this.data_inicio = data_inicio;
	}

	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public void setProbabilidade(String probabilidade) {
		this.probabilidade = probabilidade;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDataporextenso(String dataporextenso) {
		this.dataporextenso = dataporextenso;
	}
	public void setValorporextenso(String valorporextenso) {
		this.valorporextenso = valorporextenso;
	}

	public void setProdutos(LinkedList<OportunidadeMaterialBean> produtos) {
		this.produtos = produtos;
	}
	
	public void setAnoAtual(String anoAtual) {
		this.anoAtual = anoAtual;
	}

	public String getEmail_representante() {
		return email_representante;
	}

	public String getTelefone_representante() {
		return telefone_representante;
	}

	public void setEmail_representante(String emailRepresentante) {
		email_representante = emailRepresentante;
	}

	public void setTelefone_representante(String telefoneRepresentante) {
		telefone_representante = telefoneRepresentante;
	}
	
	public String getMapCamposAdicionais() {
		return mapCamposAdicionais.toString().replace("{", "").replace("=", " - ").replace("}", "");
	}

	public String getTelefone_cliente() {
		return telefone_cliente;
	}

	public void setTelefone_cliente(String telefoneCliente) {
		telefone_cliente = telefoneCliente;
	}

	public LinkedList<OportunidadeCampoadicionalBean> getCamposAdicionais() {
		return camposAdicionais;
	}

	public void setCamposAdicionais(LinkedList<OportunidadeCampoadicionalBean> camposAdicionais) {
		this.camposAdicionais = camposAdicionais;
	}
	public void setCpf_cnpj(String cpfCnpj) {
		cpf_cnpj = cpfCnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setComplemento_endereco(String complementoEndereco) {
		complemento_endereco = complementoEndereco;
	}

	public String getRazaosocial_cliente() {
		return razaosocial_cliente;
	}

	public String getCpf_cnpj_cliente() {
		return cpf_cnpj_cliente;
	}

	public String getCdcliente_cliente() {
		return cdcliente_cliente;
	}

	public String getIdentificador_cliente() {
		return identificador_cliente;
	}

	public LinkedList<TelefoneBean> getTelefone_responsavel() {
		return telefone_responsavel;
	}

	public String getEmpresa_nome() {
		return empresa_nome;
	}

	public String getEmpresa_razaosocial() {
		return empresa_razaosocial;
	}

	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}

	public String getEmpresa_ie() {
		return empresa_ie;
	}

	public String getEmpresa_im() {
		return empresa_im;
	}

	public String getEmpresa_logradouro() {
		return empresa_logradouro;
	}

	public String getEmpresa_bairro() {
		return empresa_bairro;
	}

	public String getEmpresa_cidade() {
		return empresa_cidade;
	}

	public String getEmpresa_uf() {
		return empresa_uf;
	}

	public String getEmpresa_cep() {
		return empresa_cep;
	}

	public String getEmpresa_telefones() {
		return empresa_telefones;
	}

	public String getEmpresa_email() {
		return empresa_email;
	}

	public String getEmpresa_site() {
		return empresa_site;
	}

	public void setRazaosocial_cliente(String razaosocialCliente) {
		razaosocial_cliente = razaosocialCliente;
	}

	public void setCpf_cnpj_cliente(String cpfCnpjCliente) {
		cpf_cnpj_cliente = cpfCnpjCliente;
	}

	public void setCdcliente_cliente(String cdclienteCliente) {
		cdcliente_cliente = cdclienteCliente;
	}

	public void setIdentificador_cliente(String identificadorCliente) {
		identificador_cliente = identificadorCliente;
	}

	public void setTelefone_responsavel(
			LinkedList<TelefoneBean> telefoneResponsavel) {
		telefone_responsavel = telefoneResponsavel;
	}

	public void setEmpresa_nome(String empresaNome) {
		empresa_nome = empresaNome;
	}

	public void setEmpresa_razaosocial(String empresaRazaosocial) {
		empresa_razaosocial = empresaRazaosocial;
	}

	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}

	public void setEmpresa_ie(String empresaIe) {
		empresa_ie = empresaIe;
	}

	public void setEmpresa_im(String empresaIm) {
		empresa_im = empresaIm;
	}

	public void setEmpresa_logradouro(String empresaLogradouro) {
		empresa_logradouro = empresaLogradouro;
	}

	public void setEmpresa_bairro(String empresaBairro) {
		empresa_bairro = empresaBairro;
	}

	public void setEmpresa_cidade(String empresaCidade) {
		empresa_cidade = empresaCidade;
	}

	public void setEmpresa_uf(String empresaUf) {
		empresa_uf = empresaUf;
	}

	public void setEmpresa_cep(String empresaCep) {
		empresa_cep = empresaCep;
	}

	public void setEmpresa_telefones(String empresaTelefones) {
		empresa_telefones = empresaTelefones;
	}

	public void setEmpresa_email(String empresaEmail) {
		empresa_email = empresaEmail;
	}

	public void setEmpresa_site(String empresaSite) {
		empresa_site = empresaSite;
	}
}
