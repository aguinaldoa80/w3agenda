package br.com.linkcom.sined.geral.service.rtf.bean;

public class RomaneioPatrimonioBean {

	private Integer cdPatrimonioItem;
	private String plaqueta;
	private String numeroSerie;
	private Double quantidade;

	public RomaneioPatrimonioBean(){}
	
	public Integer getCdPatrimonioItem() {
		return cdPatrimonioItem;
	}

	public void setCdPatrimonioItem(Integer cdPatrimonioItem) {
		this.cdPatrimonioItem = cdPatrimonioItem;
	}

	public String getPlaqueta() {
		return plaqueta;
	}

	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
}
