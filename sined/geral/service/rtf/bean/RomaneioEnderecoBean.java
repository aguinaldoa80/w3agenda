package br.com.linkcom.sined.geral.service.rtf.bean;

public class RomaneioEnderecoBean {

	private String endereco;
	private String cep;
	private String cidade;
	private String estado;

	public RomaneioEnderecoBean(){}
	
	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}
