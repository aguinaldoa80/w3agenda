package br.com.linkcom.sined.geral.service.rtf.bean;

import java.util.LinkedList;

public class RomaneioClienteBean {

	private String nome;
	private String razaoSocial;
	private String cpfCnpj;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private LinkedList<RomaneioEnderecoBean> romaneioEnderecoBeanList;
	private LinkedList<String> telefoneList;

	public RomaneioClienteBean() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public LinkedList<RomaneioEnderecoBean> getRomaneioEnderecoBeanList() {
		if (romaneioEnderecoBeanList == null) {
			romaneioEnderecoBeanList = new LinkedList<RomaneioEnderecoBean>();
		}
		
		return romaneioEnderecoBeanList;
	}

	public void setRomaneioEnderecoBeanList(LinkedList<RomaneioEnderecoBean> romaneioEnderecoBeanList) {
		this.romaneioEnderecoBeanList = romaneioEnderecoBeanList;
	}

	public LinkedList<String> getTelefoneList() {
		if (telefoneList == null) {
			telefoneList = new LinkedList<String>();
		}
		
		return telefoneList;
	}

	public void setTelefoneList(LinkedList<String> telefoneList) {
		this.telefoneList = telefoneList;
	}
}
