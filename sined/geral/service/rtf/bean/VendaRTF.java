package br.com.linkcom.sined.geral.service.rtf.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.types.Money;

public class VendaRTF {

	private String codigo;
	private String tipopedidovenda;
	private String responsavel;
	private String vendedorprincipal;
	private VendaClienteRTF cliente;
	private VendaContaRTF conta;
	private VendaEmpresaRTF empresa;
	private List<VendaPagamentoRTF> listaPagamento;
	private List<VendaMaterialRTF> listaVendamaterialavulso;
	private List<VendaMaterialRTF> listaVendamaterial;
	private List<VendaMaterialKitRTF> listaVendamaterialkit;
	private List<VendaMaterialKitflexivelRTF> listaVendamaterialkitflexivel;
	private VendaOutrosRTF outros;
	private Double qtdetotalproduto;
	private String valorfinal;
	private Double qtdetotalservico;
	private String desconto;
	private String valortotalprodutos;
	private String valortotal;
	private String dtvenda;
	private String observacoes;
	private String observacoesinternas;
	private String identificadorexterno;
	private String observacaoPedidoVendaTipo;
	private String pedidovendaSituacao;
	private String projetoNome;
	private String codigoOrcamento;
	private String prazoPagamento;
	private String frete;
	private Integer qtdeParcelas;
	private Money valorFrete;
	private Money taxaPedidoVenda;
	private Money valorValeCompra;
	private Money valorFinalipi;
	private String terceiro;
	private String localArmazenagem;
	private String valorporextenso;
	private HashMap<String, String> camposadicionais;
	private Integer qtdeParcelasPagamento;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getTipopedidovenda() {
		return tipopedidovenda;
	}
	public void setTipopedidovenda(String tipopedidovenda) {
		this.tipopedidovenda = tipopedidovenda;
	}
	
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	public String getVendedorprincipal() {
		return vendedorprincipal;
	}
	public void setVendedorprincipal(String vendedorprincipal) {
		this.vendedorprincipal = vendedorprincipal;
	}
	
	public VendaClienteRTF getCliente() {
		return cliente;
	}
	public void setCliente(VendaClienteRTF cliente) {
		this.cliente = cliente;
	}
	
	public VendaContaRTF getConta() {
		return conta;
	}
	public void setConta(VendaContaRTF conta) {
		this.conta = conta;
	}
	
	public VendaEmpresaRTF getEmpresa() {
		return empresa;
	}
	public void setEmpresa(VendaEmpresaRTF empresa) {
		this.empresa = empresa;
	}
	
	public List<VendaPagamentoRTF> getListaPagamento() {
		if(listaPagamento == null){
			listaPagamento = new ArrayList<VendaPagamentoRTF>();
		}
		return listaPagamento;
	}
	public void setListaPagamento(List<VendaPagamentoRTF> listaPagamento) {
		this.listaPagamento = listaPagamento;
	}
	
	public List<VendaMaterialRTF> getListaVendamaterial() {
		if(listaVendamaterial == null){
			listaVendamaterial = new ArrayList<VendaMaterialRTF>();
		}
		return listaVendamaterial;
	}
	public void setListaVendamaterial(
			List<VendaMaterialRTF> listaVendamaterial) {
		this.listaVendamaterial = listaVendamaterial;
	}
	
	public List<VendaMaterialRTF> getListaVendamaterialavulso() {
		if(listaVendamaterialavulso == null){
			listaVendamaterialavulso = new ArrayList<VendaMaterialRTF>();
		}
		return listaVendamaterialavulso;
	}
	public void setListaVendamaterialavulso(
			List<VendaMaterialRTF> listaVendamaterialavulso) {
		this.listaVendamaterialavulso = listaVendamaterialavulso;
	}
	
	public List<VendaMaterialKitRTF> getListaVendamaterialkit() {
		if(listaVendamaterialkit == null){
			listaVendamaterialkit = new ArrayList<VendaMaterialKitRTF>();
		}
		return listaVendamaterialkit;
	}
	public void setListaVendamaterialkit(
			List<VendaMaterialKitRTF> listaVendamaterialkit) {
		this.listaVendamaterialkit = listaVendamaterialkit;
	}
	
	public List<VendaMaterialKitflexivelRTF> getListaVendamaterialkitflexivel() {
		if(listaVendamaterialkitflexivel == null){
			listaVendamaterialkitflexivel = new ArrayList<VendaMaterialKitflexivelRTF>();
		}
		return listaVendamaterialkitflexivel;
	}
	public void setListaVendamaterialkitflexivel(
			List<VendaMaterialKitflexivelRTF> listaVendamaterialkitflexivel) {
		this.listaVendamaterialkitflexivel = listaVendamaterialkitflexivel;
	}
	
	public VendaOutrosRTF getOutros() {
		return outros;
	}
	public void setOutros(VendaOutrosRTF outros) {
		this.outros = outros;
	}
	
	public Double getQtdetotalproduto() {
		return qtdetotalproduto;
	}
	public void setQtdetotalproduto(Double qtdetotalproduto) {
		this.qtdetotalproduto = qtdetotalproduto;
	}
	
	public Double getQtdetotalservico() {
		return qtdetotalservico;
	}
	public void setQtdetotalservico(Double qtdetotalservico) {
		this.qtdetotalservico = qtdetotalservico;
	}
	
	public String getDesconto() {
		return desconto;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	
	public String getValortotalprodutos() {
		return valortotalprodutos;
	}
	public void setValortotalprodutos(String valortotalprodutos) {
		this.valortotalprodutos = valortotalprodutos;
	}
	
	public String getValortotal() {
		return valortotal;
	}
	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}
	
	public String getDtvenda() {
		return dtvenda;
	}
	public void setDtvenda(String dtvenda) {
		this.dtvenda = dtvenda;
	}
	
	public String getObservacoes() {
		return observacoes;
	}
	public String getObservacoesinternas() {
		return observacoesinternas;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setObservacoesinternas(String observacoesinternas) {
		this.observacoesinternas = observacoesinternas;
	}
	public HashMap<String, String> getCamposadicionais() {
		return camposadicionais;
	}
	public void setCamposadicionais(HashMap<String, String> camposadicionais) {
		this.camposadicionais = camposadicionais;
	}
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}
	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}
	public String getObservacaoPedidoVendaTipo() {
		return observacaoPedidoVendaTipo;
	}
	public void setObservacaoPedidoVendaTipo(String observacaoPedidoVendaTipo) {
		this.observacaoPedidoVendaTipo = observacaoPedidoVendaTipo;
	}
	
	public String getPedidovendaSituacao() {
		return pedidovendaSituacao;
	}
	public void setPedidovendaSituacao(String pedidovendaSituacao) {
		this.pedidovendaSituacao = pedidovendaSituacao;
	}
	
	public String getFrete() {
		return frete;
	}
	public void setFrete(String frete) {
		this.frete = frete;
	}
	public Money getValorFrete() {
		return valorFrete;
	}
	public void setValorFrete(Money valorFrete) {
		this.valorFrete = valorFrete;
	}
	public Money getTaxaPedidoVenda() {
		return taxaPedidoVenda;
	}
	public void setTaxaPedidoVenda(Money taxaPedidoVenda) {
		this.taxaPedidoVenda = taxaPedidoVenda;
	}
	public Money getValorValeCompra() {
		return valorValeCompra;
	}
	public void setValorValeCompra(Money valorValeCompra) {
		this.valorValeCompra = valorValeCompra;
	}
	public Money getValorFinalipi() {
		return valorFinalipi;
	}
	public void setValorFinalipi(Money valorFinalipi) {
		this.valorFinalipi = valorFinalipi;
	}
	
	public String getTerceiro() {
		return terceiro;
	}
	public void setTerceiro(String terceiro) {
		this.terceiro = terceiro;
	}
	
	public String getLocalArmazenagem() {
		return localArmazenagem;
	}
	public void setLocalArmazenagem(String localArmazenagem) {
		this.localArmazenagem = localArmazenagem;
	}
	public String getProjetoNome() {
		return projetoNome;
	}
	public void setProjetoNome(String projetoNome) {
		this.projetoNome = projetoNome;
	}
	public String getCodigoOrcamento() {
		return codigoOrcamento;
	}
	public void setCodigoOrcamento(String codigoOrcamento) {
		this.codigoOrcamento = codigoOrcamento;
	}
	
	public String getPrazoPagamento() {
		return prazoPagamento;
	}
	public void setPrazoPagamento(String prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}
	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}
	public String getValorfinal() {
		return valorfinal;
	}
	public void setValorfinal(String valorfinal) {
		this.valorfinal = valorfinal;
	}
	public String getValorporextenso() {
		return valorporextenso;
	}
	public void setValorporextenso(String valorporextenso) {
		this.valorporextenso = valorporextenso;
	}
	public Integer getQtdeParcelasPagamento() {
		return qtdeParcelasPagamento;
	}
	public void setQtdeParcelasPagamento(Integer qtdeParcelasPagamento) {
		this.qtdeParcelasPagamento = qtdeParcelasPagamento;
	}
}