package br.com.linkcom.sined.geral.service.rtf.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Endereco;

public class VendaClienteRTF {

	private String nome;
	private String razaosocial;
	private String cpf;
	private String cnpj;
	private String email;
	private VendaClienteenderecoRTF endereco;
	private String telefones;
	private String inscricaoestadual;
	private VendaContatoresponsavelRTF contatoresponsavel;
	private String observacao;
	private String rg;
	private String enderecotipo;
	private String identificador;
	private List<VendaClienteenderecoRTF> listaEndereco;
	
	public VendaClienteRTF(Cliente cliente){
		if(cliente != null){
			this.setNome(cliente.getNome());
			this.setRazaosocial(cliente.getRazaosocial());
			this.setCpf(cliente.getCpf() != null? cliente.getCpf().toString(): "");
			this.setCnpj(cliente.getCnpj() != null? cliente.getCnpj().toString(): "");
			this.setEmail(cliente.getEmail());
			for(Endereco endereco: cliente.getListaEndereco()){
				this.getListaEndereco().add(new VendaClienteenderecoRTF(endereco));
			}
			this.setTelefones(cliente.getTelefonesSemQuebraLinha());
			this.setInscricaoestadual(cliente.getInscricaoestadual());
			this.setContatoresponsavel(new VendaContatoresponsavelRTF(cliente.getContatoResponsavel()));
			this.setObservacao(cliente.getObservacao());
			this.setRg(cliente.getRg());
			this.setEnderecotipo(cliente.getEndereco() != null && cliente.getEndereco().getEnderecotipo() != null? cliente.getEndereco().getEnderecotipo().getNome(): "");
			this.setIdentificador(cliente.getIdentificador());
			this.setEndereco(new VendaClienteenderecoRTF(cliente.getEndereco()));
		}
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getEmail() {
		return email;
	}
	public VendaClienteenderecoRTF getEndereco() {
		return endereco;
	}
	public void setEndereco(VendaClienteenderecoRTF endereco) {
		this.endereco = endereco;
	}
	public String getTelefones() {
		return telefones;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public VendaContatoresponsavelRTF getContatoresponsavel() {
		return contatoresponsavel;
	}
	public void setContatoresponsavel(
			VendaContatoresponsavelRTF contatoresponsavel) {
		this.contatoresponsavel = contatoresponsavel;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getEnderecotipo() {
		return enderecotipo;
	}
	public void setEnderecotipo(String enderecotipo) {
		this.enderecotipo = enderecotipo;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public List<VendaClienteenderecoRTF> getListaEndereco() {
		if(listaEndereco == null){
			listaEndereco = new ArrayList<VendaClienteenderecoRTF>();
		}
		return listaEndereco;
	}
	public void setListaEndereco(List<VendaClienteenderecoRTF> listaEndereco) {
		this.listaEndereco = listaEndereco;
	}

}
