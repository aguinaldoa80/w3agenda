package br.com.linkcom.sined.geral.service.rtf.bean;

import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

public class VendaPagamentoRTF {
	
	private String descricao;
	private String qtdeparcela;
	private String data1parcela;
	private String valorparcela;
	private String valortotal;
	private String formaPagamento;
	
	public VendaPagamentoRTF(Vendaorcamentoformapagamento formapagamento){
		setDescricao(formapagamento.getPrazopagamento().getNome());
		setData1parcela(formapagamento.getDtparcela() != null ? SinedDateUtils.toString(formapagamento.getDtparcela()) : "");
		setValorparcela(formapagamento.getValorparcela().toString());
		setValortotal(formapagamento.getValortotal().toString());
		if(formapagamento.getQtdeparcelas() != null){
			setQtdeparcela(formapagamento.getQtdeparcelas().toString());
		}else{
			if(formapagamento.getPrazopagamento() != null && SinedUtil.isListNotEmpty(formapagamento.getPrazopagamento().getListaPagamentoItem())){
				setQtdeparcela(String.valueOf(formapagamento.getPrazopagamento().getListaPagamentoItem().size()));
			}
		}
		
	}
	
	public VendaPagamentoRTF (Pedidovendapagamento pedidovendapagamento){
		setDescricao(pedidovendapagamento.getDocumentotipo().getNome());
		setFormaPagamento(pedidovendapagamento.getDocumentotipo().getNome());
		setData1parcela(pedidovendapagamento.getDataparcela() != null ? SinedDateUtils.toString(pedidovendapagamento.getDataparcela()) : "");
		setValorparcela(pedidovendapagamento.getValororiginal() != null ? pedidovendapagamento.getValororiginal().toString() : "");
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getQtdeparcela() {
		return qtdeparcela;
	}
	public void setQtdeparcela(String qtdeparcela) {
		this.qtdeparcela = qtdeparcela;
	}
	public String getData1parcela() {
		return data1parcela;
	}
	public void setData1parcela(String data1parcela) {
		this.data1parcela = data1parcela;
	}
	public String getValorparcela() {
		return valorparcela;
	}
	public void setValorparcela(String valorparcela) {
		this.valorparcela = valorparcela;
	}
	public String getValortotal() {
		return valortotal;
	}
	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
}
