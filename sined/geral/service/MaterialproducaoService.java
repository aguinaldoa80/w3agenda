package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialproducaoformula;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.MaterialVO;
import br.com.linkcom.sined.geral.dao.MaterialproducaoDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.Materialajustarpreco;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.rest.android.materialproducao.MaterialproducaoRESTModel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.materialproducao.MaterialProducaoW3producaoRESTModel;

public class MaterialproducaoService extends GenericService<Materialproducao> {

	private MaterialproducaoDAO materialproducaoDAO;
	private ProdutoService produtoService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendatipoService producaoagendatipoService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setMaterialproducaoDAO(MaterialproducaoDAO materialproducaoDAO){this.materialproducaoDAO = materialproducaoDAO;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setProducaoagendatipoService(ProducaoagendatipoService producaoagendatipoService) {this.producaoagendatipoService = producaoagendatipoService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {
		this.materialformulavalorvendaService = materialformulavalorvendaService;
	}
	
	/* singleton */
	private static MaterialproducaoService instance;
	public static MaterialproducaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialproducaoService.class);
		}
		return instance;
	}
	
//	public Double calculaConsumo(Materialproducao materialproducao, Double qtdevendida){
//		Double quantidadeprodutovenda = qtdevendida;
//		Double largura = materialproducao.getLargura();
//		Double altura = materialproducao.getAltura();
//		Double partespeca = materialproducao.getPartespeca();		
//		Double multiplo = materialproducao.getMultiplo();
//		Double larguraparte = materialproducao.getLarguraparte();
//		Double alturaparte = materialproducao.getAlturaparte();		
//		
//		Double consumo = 0D;
//		if (!(multiplo >= 1))
//			return consumo;		
//		if (!(partespeca > 0 && larguraparte > 0 && alturaparte > 0 && largura >= 0 && altura >= 0))
//			return consumo;
//		if (largura == 0 && altura == 0){
//			consumo = partespeca * quantidadeprodutovenda;
//			return consumo;
//		}		
//		
//		if (altura == 0){ //bobina
//			if (larguraparte > largura && alturaparte > largura)
//				return consumo;
//			
//			Double qtpartes = partespeca / multiplo * quantidadeprodutovenda;			
//			
//	        Boolean SegAlt = false;
//	        Double Aproveita_V_1 = 0D;
//	        Double Aproveita_V_2 = 0D;
//	        Double Metrag_V_1 = 0D;
//	        Double Metrag_V_2 = 0D;
//	        
//			if(larguraparte > largura){ // Largura Parte > Largura Bobina
//		        // invers�o de Largura x Altura - �nica alternativa
//		        Double LargParte_V_1 = alturaparte;
//		        Double AltParte_V_1 = larguraparte;
//		        Double PartePTira_V_1 = Math.floor(largura / LargParte_V_1);
//		        Double Tiras_V_1 = calcularplacas(qtpartes, (PartePTira_V_1));
//		        Metrag_V_1 = Tiras_V_1 * AltParte_V_1 / 1000;
//		        Double Area_V_1 = Metrag_V_1 * largura / 1000;
//		        Aproveita_V_1 = ((LargParte_V_1 / 1000) * (AltParte_V_1 / 1000) * qtpartes * 100) / Area_V_1;
//        	} else {
//        		Double LargParte_V_1 = larguraparte;
//        		Double AltParte_V_1 = alturaparte;
//        		Double PartePTira_V_1 = Math.floor(largura / LargParte_V_1);
//        		Double Tiras_V_1 = calcularplacas(qtpartes, (PartePTira_V_1));
//        		Metrag_V_1 = Tiras_V_1 * AltParte_V_1 / 1000;
//        		Double Area_V_1 = Metrag_V_1 * largura / 1000;
//        		Aproveita_V_1 = ((LargParte_V_1 / 1000) * (AltParte_V_1 / 1000) * qtpartes * 100) / Area_V_1;
//		        if (larguraparte != alturaparte && alturaparte <= largura){        
//	                SegAlt = true;
//	                Double LargParte_V_2 = alturaparte;
//	                Double AltParte_V_2 = larguraparte;
//	                Double PartePTira_V_2 = Math.floor(largura / LargParte_V_2);
//	                Double Tiras_V_2 = calcularplacas(qtpartes, (PartePTira_V_2));
//	                Metrag_V_2 = Tiras_V_2 * AltParte_V_2 / 1000;
//	                Double Area_V_2 = Metrag_V_2 * largura / 1000;
//	                Aproveita_V_2 = ((LargParte_V_2 / 1000) * (AltParte_V_2 / 1000) * qtpartes * 100) / Area_V_2;
//		        }
//			}
//			
//        	if (SegAlt && Aproveita_V_2 > Aproveita_V_1){
//        		consumo = Metrag_V_2;
//        	} else {
//        		consumo = Metrag_V_1;
//        	}
//        	
//		} else { // Placa
//			if ((larguraparte > largura && (larguraparte > altura || alturaparte > largura)) ||
//			    (alturaparte > altura && (alturaparte > largura || larguraparte > altura))){
//				return consumo;
//			}
//			
//			Double qtpartes = partespeca / multiplo * quantidadeprodutovenda;
//			Boolean SegAlt = false;
//	        Double Aproveita_F_1 = 0D;
//	        Double Aproveita_F_2 = 0D;
//	        Double Placas_F_1 = 0D;
//	        Double Placas_F_2 = 0D;
//	        
//   			if (larguraparte > largura || alturaparte > altura){			
//				// Calcula itens da 1� alternativa com invers�o
//				Double LargParte_F_1 = alturaparte;
//				Double AltParte_F_1 = larguraparte;
//				Double PartePTira_F_1 = Math.floor(largura / LargParte_F_1);
//				Double TirasPPlaca_F_1 = Math.floor(altura / AltParte_F_1);
//				Double PartesPPlaca_F_1 = PartePTira_F_1 * TirasPPlaca_F_1;
//				Placas_F_1 = calcularplacas(qtpartes, PartesPPlaca_F_1);
//				Double Area_F_1 = Placas_F_1 * largura * altura / 1000000;
//				Aproveita_F_1 = ((LargParte_F_1 / 1000) * (AltParte_F_1 / 1000) * qtpartes * 100) / Area_F_1;
//			} else {
//				//   Calcula itens da 1� alternativa
//				Double LargParte_F_1 = larguraparte;
//				Double AltParte_F_1 = alturaparte;
//				Double PartePTira_F_1 = Math.floor(largura / LargParte_F_1);
//				Double TirasPPlaca_F_1 = Math.floor(altura / AltParte_F_1);
//				Double PartesPPlaca_F_1 = PartePTira_F_1 * TirasPPlaca_F_1;
//				Placas_F_1 = calcularplacas(qtpartes, PartesPPlaca_F_1);
//				Double Area_F_1 = Placas_F_1 * largura * altura / 1000000;
//				Aproveita_F_1 = ((LargParte_F_1 / 1000) * (AltParte_F_1 / 1000) * qtpartes * 100) / Area_F_1;
//			    if (larguraparte != alturaparte && larguraparte < altura && alturaparte < largura) { // n�o � quadrado
//					//  Calcula itens da 2� alternativa
//					SegAlt = true;
//					Double LargParte_F_2 = alturaparte;
//					Double AltParte_F_2 = larguraparte;
//					Double PartePTira_F_2 = Math.floor(largura / LargParte_F_2);
//					Double TirasPPlaca_F_2 = Math.floor(altura / AltParte_F_2);
//					Double PartesPPlaca_F_2 = PartePTira_F_2 * TirasPPlaca_F_2;
//					Placas_F_2 = calcularplacas(qtpartes, PartesPPlaca_F_2);
//					Double Area_F_2 = Placas_F_2 * largura * altura / 1000000;
//					Aproveita_F_2 = ((LargParte_F_2 / 1000) * (AltParte_F_2 / 1000) * qtpartes * 100) / Area_F_2;
//				}
//			}
//			if (SegAlt && Aproveita_F_2 > Aproveita_F_1){
//        		consumo = Placas_F_2;
//        	} else {
//        		consumo = Placas_F_1;
//        	}	        	
//		}	
//		return consumo;
//	}
//	
//	public Double calcularplacas(Double total, Double partes){
//		if (partes == 0)
//			partes = 1D;
//		
//		if (total <= partes)
//		    return 1D;
//		else {
//			Double calcplacas = Math.floor(total / partes);
//			if (calcplacas * partes != total)
//		    	calcplacas = calcplacas + 1;		
//		    return calcplacas;
//		}
//	}
//
//	public Double calculaConsumoServico(Materialproducao materialproducao, Double qtdevendida, Integer qtdeReferencia) {
//		if(qtdevendida.doubleValue() == qtdeReferencia.doubleValue())
//			return materialproducao.getConsumo();
//		else
//			return SinedUtil.round(qtdevendida / qtdeReferencia * materialproducao.getConsumo(), 2);
//	}

	/**
	* Faz refer�ncia ao DAO
	* 
	* Busca materialproducao para atualiza��o
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialproducaoDAO#findForAtualizar(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Materialproducao> findForAtualizar(String whereIn){
		return materialproducaoDAO.findForAtualizar(whereIn);
	}
	
	/**
	* Faz refer�ncia ao DAO
	*
	*@see	br.com.linkcom.sined.geral.dao.MaterialproducaoDAO#upadateValor(Materialproducao materialproducao)
	*
	* @param materialproducao
	* @param valorpercentual
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updatePreco(Materialproducao materialproducao, Materialajustarpreco valorpercentual){
		if(materialproducao.getPreco() != null && materialproducao.getConsumo() != null){
			Double valorPreco, valorConsumo;
			
			valorPreco = materialproducao.getPreco();				
			valorConsumo = valorPreco * materialproducao.getConsumo();
			
			materialproducao.setPreco(valorPreco);
			materialproducao.setValorconsumo(valorConsumo);
			
			materialproducaoDAO.upadateValor(materialproducao);
		}
	}

	public List<Materialproducao> findListaProducao(Integer cdmaterial) {
		return materialproducaoDAO.findListaProducao(cdmaterial, Boolean.FALSE);
	}
	
	public List<Materialproducao> findListaProducao(Integer cdmaterial, Boolean somenteCarcaca) {
		return materialproducaoDAO.findListaProducao(cdmaterial, somenteCarcaca);
	}

	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		materialproducaoDAO.deleteWhereNotInMaterial(material, whereIn);
	}
	
	/**
	* M�todo que retorna o total do consumo do material de produ��o
	*
	* @param material
	* @return
	* @throws ScriptException
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public Double getValorConsumoTotal(Material material) throws ScriptException{
		if(material.getListaProducao() != null){
			this.calculaConsumoProducao(material);
			
			Double valorconsumoTotal = 0.0;
			
			for (Materialproducao materialproducao : material.getListaProducao()) {
				if(materialproducao.getConsumo() == null) materialproducao.setConsumo(0d);
				
				Double valorconsumo = (materialproducao.getPreco() != null ? materialproducao.getPreco() : 0.0) * (materialproducao.getConsumo() != null ? materialproducao.getConsumo() : 0.0);
				
				if(material.getQuantidade() != null && material.getQuantidade() > 0 && 
						(materialproducao.getListaMaterialproducaoformula() == null || 
						materialproducao.getListaMaterialproducaoformula().isEmpty())){
					valorconsumo = valorconsumo * material.getQuantidade();
				}
				materialproducao.setValorconsumo(valorconsumo);
				valorconsumoTotal += valorconsumo;
			}
			
			return valorconsumoTotal;
		}
		return null;
	}

	public Double getValorvendaproducao(Material material) throws ScriptException {
		if(material.getListaProducao() != null){
			this.calculaConsumoProducao(material);
			
			Double valorconsumoTotal = 0.0;
			
			for (Materialproducao materialproducao : material.getListaProducao()) {
				if(materialproducao.getConsumo() == null) materialproducao.setConsumo(0d);
				
				Double valorconsumo = (materialproducao.getPreco() != null ? materialproducao.getPreco() : 0.0) * (materialproducao.getConsumo() != null ? materialproducao.getConsumo() : 0.0);
				
				if(material.getQuantidade() != null && material.getQuantidade() > 0 && 
						(materialproducao.getListaMaterialproducaoformula() == null || 
						materialproducao.getListaMaterialproducaoformula().isEmpty())){
					valorconsumo = valorconsumo * material.getQuantidade();
				}
				materialproducao.setValorconsumo(valorconsumo);
				valorconsumoTotal += valorconsumo;
			}
			
//			if(material.getLucro() != null){
//				Double lucroPercentual = material.getLucro().getValue().doubleValue();
//				if(lucroPercentual > 0){
//					Double lucroValor = (lucroPercentual * valorconsumoTotal) / 100d;
//					valorconsumoTotal += lucroValor;
//				}
//			}
			
			boolean isValorformula = materialformulavalorvendaService.setValorvendaByFormula(material, material.getProduto_altura(), material.getProduto_largura(), material.getProduto_comprimento(), null, valorconsumoTotal, true);
			valorconsumoTotal = isValorformula ? material.getValorvenda() : valorconsumoTotal;
			
			if(material.getFrete() != null){
				Double fretePercentual = material.getFrete().getValue().doubleValue();
				if(fretePercentual > 0){
					Double freteValor = (fretePercentual * valorconsumoTotal) / 100d;
					valorconsumoTotal += freteValor;
				}
			}
			
			return SinedUtil.roundByParametro(valorconsumoTotal);
		}
		
		return null;
	}
	
	/**
	 * M�todo que cria um item de producao com a soma dos produtos que n�o est�o marcados para exibir na venda
	 *
	 * @param listaProducaoOld
	 * @param materialmestre
	 * @return
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public Materialproducao createItemProducaoByMaterialmestre(Set<Materialproducao> listaProducaoOld, Material materialmestre) {
		Materialproducao materialproducaoMestre = null;
		if(materialmestre != null && listaProducaoOld != null && listaProducaoOld.size() > 0){
			Double valortotal = 0d;
			boolean exibirMestre = false;
			
			for (Materialproducao materialproducao : listaProducaoOld) {
				if(materialproducao != null && (materialproducao.getExibirvenda() == null || !materialproducao.getExibirvenda())){
					if(materialproducao.getPreco() != null && materialproducao.getConsumo() != null){
						valortotal += materialproducao.getPreco() * materialproducao.getConsumo();
					}
					exibirMestre = true;
				}
			}
			if(exibirMestre){
				materialproducaoMestre = new Materialproducao();
				materialproducaoMestre.setMaterial(materialmestre.copiaMaterial());
				materialproducaoMestre.getMaterial().setValorvenda(valortotal);
				materialproducaoMestre.getMaterial().setValorvendasemdesconto(materialmestre.getValorvenda());
				materialproducaoMestre.setExibirvenda(true);
			}
		}
		
		if(materialproducaoMestre == null && SinedUtil.isRecapagem()){
			materialproducaoMestre = new Materialproducao();
			materialproducaoMestre.setMaterial(materialmestre.copiaMaterial());
		}
		
		return materialproducaoMestre;
	}
	
	/**
	 * M�todo que ordena a lista de produ��o do material de acordo com o campo - ordemexibicao
	 *
	 * @param listaProducao
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Set<Materialproducao> ordernarListaProducao(Set<Materialproducao> listaProducaoOld, boolean retirarItensNaoExibirVenda) {
		List<Materialproducao> listaProducao = new ArrayList<Materialproducao>();
		if(retirarItensNaoExibirVenda){
			if(listaProducaoOld != null && listaProducaoOld.size() > 0){
				for (Materialproducao materialproducao : listaProducaoOld) {
					if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
						listaProducao.add(materialproducao);
					}
				}
			}
		} else {
			listaProducao.addAll(listaProducaoOld);
		}
		
		if(listaProducao == null || listaProducao.size() < 2)
			return SinedUtil.listToSet(listaProducao, Materialproducao.class);
		
		List<Materialproducao> listaOrdenada = new ArrayList<Materialproducao>();
		listaOrdenada.addAll(listaProducao);
		
		Collections.sort(listaProducao, new Comparator<Materialproducao>(){
			public int compare(Materialproducao mp1, Materialproducao mp2) {
				if(mp1.getOrdemexibicao() == null){
					return 1;
				}else if(mp2.getOrdemexibicao() == null){
					return -1;
				}
				return mp1.getOrdemexibicao().compareTo(mp2.getOrdemexibicao());
			}
		});
		
		return SinedUtil.listToSet(listaProducao, Materialproducao.class);
	}
	
	public void calculaConsumoProducaoForNota(Material material){
		try {
			this.calculaConsumoProducao(material);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void calculaConsumoProducao(Material material) throws ScriptException{
		if(material.getListaProducao() != null){
			Producaoagenda producaoagenda = material.getProducaoagendatrans();
			if(material.getProducaoagendatrans() != null){ 
				if( material.getProducaoagendatrans().getCdproducaoagenda() != null && 
					(material.getProducaoagendatrans().getProducaoagendatipo() == null || 
					material.getProducaoagendatrans().getProducaoagendatipo().getCdproducaoagendatipo() == null)){
					if(material.getProducaoagendatrans().getCdproducaoagenda() != null){
							producaoagenda = producaoagendaService.loadWithoutProducaoagendatipo(material.getProducaoagendatrans());
					}
				}else {
					material.getProducaoagendatrans().setProducaoagendatipo(
							producaoagendatipoService.load(
									material.getProducaoagendatrans().getProducaoagendatipo(), 
									"producaoagendatipo.cdproducaoagendatipo, producaoagendatipo.nome")
						);
				}
			}
			List<String> idsMateriais = new ArrayList<String>();
			Pattern pattern_material = Pattern.compile("formula\\.[a-zA-Z]+Material\\(([0-9]+)\\)");
			
			for (Materialproducao materialproducao : material.getListaProducao()) {
				if(materialproducao.getListaMaterialproducaoformula() != null){
					for (Materialproducaoformula materialproducaoformula : materialproducao.getListaMaterialproducaoformula()) {
						Matcher m_material = pattern_material.matcher(materialproducaoformula.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
					}
				}
			}
			
			FormulaVO formulaVO = new FormulaVO();
			formulaVO.setProducaoagenda(producaoagenda);
			
			if(idsMateriais.size() > 0){
				List<Produto> listaMaterial = produtoService.findForCalculoFormula(CollectionsUtil.concatenate(idsMateriais, ","));
				for (Produto p : listaMaterial) {
					MaterialVO materialVO = new MaterialVO();
					materialVO.setCdmaterial(p.getCdmaterial());
					materialVO.setAltura(p.getAltura() != null ? p.getAltura() : 0.0);
					materialVO.setLargura(p.getLargura() != null ? p.getLargura() : 0.0);
					materialVO.setPeso(p.getPeso() != null ? p.getPeso() : 0.0);
					formulaVO.addMaterial(materialVO);
				}
			}
			
			for (Materialproducao mp : material.getListaProducao()) {
				MaterialVO materialVO = new MaterialVO();
				if(mp.getMaterial() != null){
					materialVO.setCdmaterial(mp.getMaterial().getCdmaterial());
				}
				materialVO.setAltura(mp.getAltura() != null ? mp.getAltura() : 0.0);
				materialVO.setLargura(mp.getLargura() != null ? mp.getLargura() : 0.0);
				formulaVO.addMaterialProducao(materialVO);
			}
			
			if(material.getQuantidade() != null)
				formulaVO.setQuantidadeMaterialVenda(material.getQuantidade());
			if(material.getProduto_altura() != null)
				formulaVO.setAlturaMaterialVenda(material.getProduto_altura());
			if(material.getProduto_largura() != null)
				formulaVO.setLarguraMaterialVenda(material.getProduto_largura());
			
			for (Materialproducao materialproducao : material.getListaProducao()) {
				List<Materialproducaoformula> listaMaterialproducaoformula = materialproducao.getListaMaterialproducaoformula();
				if(listaMaterialproducaoformula != null && listaMaterialproducaoformula.size() > 0){
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine engine = manager.getEngineByName("JavaScript");
					
					engine.put("formula", formulaVO);
					
					Collections.sort(listaMaterialproducaoformula, new Comparator<Materialproducaoformula>(){
						public int compare(Materialproducaoformula o1, Materialproducaoformula o2) {
							return o1.getOrdem().compareTo(o2.getOrdem());
						}
					});
					
					Double consumo = 0.0;
					
					for (Materialproducaoformula materialproducaoformula : listaMaterialproducaoformula) {
						Object obj = engine.eval(materialproducaoformula.getFormula());

						Double resultado = 0d;
						if(obj != null){
							String resultadoStr = obj.toString();
							resultado = new Double(resultadoStr);
						}
						
						engine.put(materialproducaoformula.getIdentificador(), resultado);
						consumo = resultado;
					}
					
					materialproducao.setConsumo(consumo);
				}
			}
			
		}
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialproducaoDAO#atualizaPrecoEValorConsumo(Materialproducao materialproducao, Double preco, Double valorconsumo)
	*
	* @param materialproducao
	* @param preco
	* @param valorconsumo
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public void atualizaPrecoEValorConsumo(Materialproducao materialproducao, Double preco, Double valorconsumo){
		materialproducaoDAO.atualizaPrecoEValorConsumo(materialproducao, preco, valorconsumo);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.MaterialproducaoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<MaterialproducaoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.MaterialproducaoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<MaterialproducaoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialproducaoRESTModel> lista = new ArrayList<MaterialproducaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialproducao mp : materialproducaoDAO.findForAndroid(whereIn))
				lista.add(new MaterialproducaoRESTModel(mp));
		}
		
		return lista;
	}
	
	/**
	 * M�todo que retorna a lista de materiais de produ��o a partir de um material, fazendo busca "em casacata" <br>
	 * para baixa das mat�rias primas na agenda de produ��o ou ordem de produ��o 
	 * @param material
	 * @param consumo
	 * @author Danilo Guimar�es
	 * @since 09/06/2015
	 */
	public List<Materialproducao> getMateriaisProducao(Material material, Double consumo, Boolean baixarmateriaprimacascata){
		List<Materialproducao> listaProducaoAux = materialproducaoDAO.findListaProducao(material.getCdmaterial());
		List<Materialproducao> listaProducao = new ArrayList<Materialproducao>();
		
		for(Materialproducao item : listaProducaoAux){
			item.setConsumo(item.getConsumo() * (consumo != null ? consumo : 1d));
			if(item.getMaterial() != null && item.getMaterial().getProducao() != null && item.getMaterial().getProducao() && 
					baixarmateriaprimacascata != null && baixarmateriaprimacascata){
				listaProducao.addAll(getMateriaisProducao(item.getMaterial(), item.getConsumo(), baixarmateriaprimacascata));
			} else {
				listaProducao.add(item);
			}
		}
		
		return listaProducao;
	}
	
	public List<MaterialProducaoW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de Materialproducao para a sincroniza��o com o W3Producao.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<MaterialProducaoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialProducaoW3producaoRESTModel> lista = new ArrayList<MaterialProducaoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialproducao mp : materialproducaoDAO.findForW3Producao(whereIn))
				lista.add(new MaterialProducaoW3producaoRESTModel(mp));
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialproducaoDAO#findForSped(String whereIn)
	*
	* @param material
	* @return
	* @since 09/12/2016
	* @author Luiz Fernando
	*/
	public List<Materialproducao> findForSped(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return findForSped(material.getCdmaterial().toString());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialproducaoDAO#findForSped(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 09/12/2016
	* @author Luiz Fernando
	*/
	public List<Materialproducao> findForSped(String whereIn){
		return materialproducaoDAO.findForSped(whereIn);
	}
	
	public Materialproducao findAcompanhabanda(Material material) {
		return materialproducaoDAO.findAcompanhabanda(material);
	}
}
