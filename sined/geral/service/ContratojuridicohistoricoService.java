package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicohistorico;
import br.com.linkcom.sined.geral.dao.ContratojuridicohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratojuridicohistoricoService extends GenericService<Contratojuridicohistorico> {

	private ContratojuridicohistoricoDAO contratojuridicohistoricoDAO;
	
	public void setContratojuridicohistoricoDAO(
			ContratojuridicohistoricoDAO contratojuridicohistoricoDAO) {
		this.contratojuridicohistoricoDAO = contratojuridicohistoricoDAO;
	}
	
	public List<Contratojuridicohistorico> findByContratojuridico(Contratojuridico contratojuridico) {
		return contratojuridicohistoricoDAO.findByContratojuridico(contratojuridico);
	}

}
