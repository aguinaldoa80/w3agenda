package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Rateiomodelo;
import br.com.linkcom.sined.geral.bean.Rateiomodeloitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialRateioEstoqueTipoContaGerencial;
import br.com.linkcom.sined.geral.dao.RateioDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RateioService extends GenericService<Rateio>{
	
	private RateioDAO rateioDAO;
	private TipotaxaService tipotaxaService;	
	private static RateioService instance;
	private RateiomodeloService rateiomodeloService;
	private MaterialService materialService;
	
	public static RateioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RateioService.class);
		}
		return instance;
	}
	
	public void setRateioDAO(RateioDAO rateioDAO) {
		this.rateioDAO = rateioDAO;
	}
	
	public void setTipotaxaService(TipotaxaService tipotaxaService) {
		this.tipotaxaService = tipotaxaService;
	}
	public void setRateiomodeloService(RateiomodeloService rateiomodeloService) {
		this.rateiomodeloService = rateiomodeloService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	/**
	 * M�todo utilizado para calcular os valores do detalhe do rateio.
	 * 
	 * @param rateio
	 * @param valor
	 * @author Fl�vio Tavares
	 */
	public void calculaValoresRateio(Rateio rateio, Money valor){
		if(rateio != null && rateio.getListaRateioitem() != null){
			Money soma = new Money();
			for (Rateioitem rateioitem : rateio.getListaRateioitem()) {
				soma = soma.add(rateioitem.getValor());
			}
			soma = soma.getValorArredondadoDuasCasasDecimais(soma);
			valor = valor.getValorArredondadoDuasCasasDecimais(valor);
			rateio.setValoraux(new String(valor.subtract(soma).toString()).replaceAll("\\.", ""));
			rateio.setValortotaux(new String(valor.toString()).replaceAll("\\.", ""));
		}
	}
	
	/**
	 * <p>Faz a valda��o de Rateio.</p>
	 * 
	 * @param rateio
	 * @param valorTotal
	 * @throws SinedException
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public void validateRateio(Rateio rateio, Money valorTotal)throws SinedException {
		if(rateio == null){
			throw new SinedException("O rateio n�o pode vazio.");
		}
		if (valorTotal == null) {
			throw new SinedException("O valor total n�o pode ser vazio.");
		}
		
		Money valorRateado = new Money();
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
		
		for (Rateioitem rateioitem : listaRateioitem) {
			if (rateioitem != null && rateioitem.getValor() != null)
				valorRateado = valorRateado.add(new Money(rateioitem.getValor().getValue(), false));
		}
		
		if(SinedUtil.round(valorRateado.getValue(), 2).compareTo(SinedUtil.round(valorTotal.getValue(), 2)) != 0 && 
				SinedUtil.round(valorRateado.getValue(), 2).compareTo(SinedUtil.round(new BigDecimal(valorTotal.toString().replaceAll("\\.", "").replace(",", ".")), 2)) != 0){
			throw new SinedException("O valor total rateado � diferente do valor total.");
		}
	}
	
	/**
	 * M�todo para tirar as refer�ncias da lista de Rateioitem.
	 * 
	 * @param rateio
	 * @author Fl�vio Tavares
	 */
	public void limpaReferenciaRateio(Rateio rateio){
		rateio.setCdrateio(null);
		rateio.setCdusuarioaltera(null);
		rateio.setDtaltera(null);
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();

		for (Rateioitem rateioitem : listaRateioitem) {
			rateioitem.setCdrateioitem(null);
		}
	}
	
	public void limpaReferenciaApenasRateioitem(Rateio rateio){
		rateio.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		rateio.setDtaltera(new Timestamp(System.currentTimeMillis()));
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();

		for (Rateioitem rateioitem : listaRateioitem) {
			rateioitem.setCdrateioitem(null);
		}
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o Rateio e os itens do rateio.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RateioDAO#findByDocumento(Documento)
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Rateio findByDocumento(Documento documento){
		return rateioDAO.findByDocumento(documento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param rateio
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Rateio findRateio(Rateio rateio) {
		return rateioDAO.findRateio(rateio);
	}
	
	/**
	 * M�todo para recalcular os valores dos itens do rateio com base
	 * no percentual de cada item.
	 * 
	 * @see #calculaValoresRateio(Rateio, Money)
	 * @param rateio
	 * @param novoValor
	 * @author Fl�vio Tavares
	 */
	public void atualizaValorRateio(Rateio rateio, Money novoValor){
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
		if(rateio == null || listaRateioitem == null || listaRateioitem.size() == 0){
			return;
		}
		
		this.calculaValorRateioitem(novoValor, listaRateioitem);
		this.calculaValoresRateio(rateio, novoValor);
	}

	public void calculaValorRateioitem(Money novoValor,
			List<Rateioitem> listaRateioitem) {
		double valor = novoValor.getValue().doubleValue();
		if(SinedUtil.isListNotEmpty(listaRateioitem)){
			for (Rateioitem ri : listaRateioitem) {
				double x = valor * (ri.getPercentual() != null ? ri.getPercentual() : (listaRateioitem.size() == 1 ? 100d : 0d)) / 100;
				ri.setValor(new Money(SinedUtil.round(new BigDecimal(x), 2)));
			}
		}
	}

	
	/**
	 * Calcula a propor��o dos percentuais
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Documento rateioItensPercentual(Documento form, Money valorTotal){
		double valorAux;
		
		for (Rateioitem ri : form.getRateio().getListaRateioitem()) {	
			valorAux = ri.getValor().getValue().doubleValue()/valorTotal.getValue().doubleValue() * 100;
			Rateioitem rateioItem = new Rateioitem();

			rateioItem.setPercentual(valorAux);
			form.getRateio().getListaRateioitem().add(rateioItem);
	
		}
		this.calculaValoresRateio(form.getRateio(), valorTotal);
		return(form);
	}
	
	public void rateioItensPercentual(Rateio rateio, Money valorTotal){
		this.ajustaPercentualRateioitem(valorTotal, rateio.getListaRateioitem());
		this.calculaValoresRateio(rateio, valorTotal);
	}

	public void ajustaPercentualRateioitem(Money valorTotal, List<Rateioitem> listaRateioitem) {
		if(valorTotal != null && valorTotal.getValue().doubleValue() > 0){
			for (Rateioitem ri : listaRateioitem) {	
				double valorAux = ri.getValor().getValue().doubleValue()/valorTotal.getValue().doubleValue() * 100d;
				valorAux = SinedUtil.round(valorAux, 10);
				ri.setPercentual(valorAux);
			}
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RateioDAO#findForAtualizadadosContrato(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 05/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Rateio> findForAtualizadadosContrato(String whereIn) {
		return rateioDAO.findForAtualizadadosContrato(whereIn);
	}
	
	public Boolean existAlteracaorateio(Rateio rateioOriginal, Rateio rateioModificado){
		boolean existalteracao = false;
		boolean verificado = false;
		
		boolean contagerencial = false;
		boolean centrocusto = false;
		boolean projeto = false;
		boolean valor = false;	
		
		if(rateioOriginal != null && rateioModificado != null && 
				rateioOriginal.getListaRateioitem() != null && !rateioOriginal.getListaRateioitem().isEmpty() && 
				rateioModificado.getListaRateioitem() != null && !rateioModificado.getListaRateioitem().isEmpty()){
			String cdrateioitem[] = new String[rateioModificado.getListaRateioitem().size()];
			if(rateioModificado.getListaRateioitem().size() != rateioOriginal.getListaRateioitem().size()){
				existalteracao = true;
			}else {
				for(Rateioitem ri : rateioOriginal.getListaRateioitem()){
					for(Rateioitem ri2 : rateioModificado.getListaRateioitem()){
						for(String cd : cdrateioitem){
							if(ri2.getCdrateioitem().toString().equals(cd)){
								verificado = true; 
								break;
							}
						}
						if(!verificado){
							if(ri.getContagerencial() != null){
								if(ri.getContagerencial().equals(ri2.getContagerencial())){
									contagerencial = true;
								}
							}else {
								if(ri2.getContagerencial() == null){
									contagerencial = true;
								}
							}
							if(ri.getCentrocusto() != null){
								if(ri.getCentrocusto().equals(ri2.getCentrocusto())){
									centrocusto = true;
								}
							}else {
								if(ri2.getCentrocusto() == null){
									centrocusto = true;
								}
							}
							if(ri.getProjeto() != null){
								if(ri.getProjeto().equals(ri2.getProjeto())){
									projeto = true;
								}
							}else {
								if(ri2.getProjeto() == null){
									projeto = true;
								}
							}
							if(ri.getValor() != null){
								if(ri.getValor().compareTo(ri2.getValor()) == 0){
									valor = true;
								}
							}else {
								if(ri2.getValor() == null){
									valor = true;
								}
							}					
							if(contagerencial && centrocusto && projeto && valor){
								for(int i = 0; i < cdrateioitem.length; i++){
									if(cdrateioitem[i] == null || "".equals(cdrateioitem[i])){
										cdrateioitem[i] = ri2.getCdrateioitem().toString();
										break;
									}
								}								
								break;
							}
						}
						verificado = false;
						contagerencial = false;
						centrocusto = false;
						projeto = false;
						valor = false;	
					}
				}
				for(String cd : cdrateioitem){
					if(cd == null || "".equals(cd)){
						existalteracao = true;
						break;
					}
				}
			}
		}
		
		return existalteracao;
	}

	public void validaRateioitem(Rateio rateio) {
		if(rateio == null){
			throw new SinedException("O rateio n�o pode ser null.");
		}
		if (rateio.getListaRateioitem() == null || rateio.getListaRateioitem().isEmpty()) {
			throw new SinedException("O rateio deve possuir pelo menos 1 item.");
		}
		
		for(Rateioitem ri : rateio.getListaRateioitem()){
			if(ri.getContagerencial() == null){
				throw new SinedException("Conta gerencial do rateio � obrigat�rio.");
			}
			if(ri.getCentrocusto() == null){
				throw new SinedException("Centro de custo do rateio � obrigat�rio.");
			}
			if(ri.getValor() == null){
				throw new SinedException("O campo valor da lista do rateio � obrigat�rio.");
			}
		}		
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RateioDAO#atualizaRateioMovimentacao(Movimentacao movimentacao)
	 *
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public void atualizaRateioMovimentacao(Movimentacao movimentacao) {
		rateioDAO.atualizaRateioMovimentacao(movimentacao);
	}

	public List<Rateio> findByOrdemcompra(String whereIn) {
		return rateioDAO.findByOrdemcompra(whereIn);
	}
	
	/**
	 * M�todo que ajusta o percentual do rateio do Documento
	 *
	 * @param documento
	 * @author Luiz Fernando
	 */
	public void ajustePercentualRateioByDocumento(Documento documento){
		if(documento.getValor() != null && documento.getValor().getValue().doubleValue() > 0 && 
				documento.getRateio() != null && documento.getRateio().getListaRateioitem() != null && !documento.getRateio().getListaRateioitem().isEmpty()){
			this.rateioItensPercentual(documento.getRateio(), documento.getValor());
		}
	}
	
	/**
	 * Atualiza os valores do rateio referente as taxas de desconto e multa 
	 * no processamento do arquivo de retorno
	 *
	 * @param rateio
	 * @param valorDesconto
	 * @param valorMulta
	 * @param valorTotalDocumento
	 * @param valorTotalPago
	 * @param tipooperacao
	 * @author Marden Silva
	 */
	public void atualizaRateioWithDescontoAndMulta(Rateio rateio, Money valorDesconto, Money valorOutroDesconto, Money valorMultaJuros, Money valorTotalDocumento, Money valorTotalPago, Tipooperacao tipooperacao){
		
		Tipotaxa tipotaxa1;
		Tipotaxa tipotaxa2;
		Rateioitem rateioitem;
		
		if (valorDesconto != null && valorDesconto.toLong() > 0 || valorOutroDesconto != null && valorOutroDesconto.toLong() > 0){
			
			tipotaxa1 = tipotaxaService.loadForEntrada(Tipotaxa.DESCONTO);
			if (tipotaxa1.getContadebito() != null && tipotaxa1.getCentrocusto() != null){
				rateioitem = new Rateioitem();
				rateioitem.setCentrocusto(tipotaxa1.getCentrocusto());
				rateioitem.setContagerencial(tipotaxa1.getContadebito());
				rateioitem.setValor(SinedUtil.zeroIfNull(valorDesconto).add(SinedUtil.zeroIfNull(valorOutroDesconto)));
				
				if (rateio != null && rateio.getListaRateioitem() != null){
					for (Rateioitem ri : rateio.getListaRateioitem()){
						if (ri.getContagerencial().equals(rateioitem.getContagerencial()) &&
								ri.getCentrocusto().equals(rateioitem.getCentrocusto())){
							ri.setValor(rateioitem.getValor());							
						}
					}
				} else {
					rateio = new Rateio();
					rateio.setListaRateioitem(new ArrayList<Rateioitem>());
					rateio.getListaRateioitem().add(rateioitem);
				}
				
				this.rateioItensPercentual(rateio, valorTotalDocumento);
			}			
				
		}
		
		if (valorMultaJuros != null && valorMultaJuros.toLong() > 0){
			
			tipotaxa1 = tipotaxaService.loadForEntrada(Tipotaxa.JUROS);
			tipotaxa2 = tipotaxaService.loadForEntrada(Tipotaxa.MULTA);
			
			Contagerencial contagerencial = null;
			Centrocusto centrocusto = null;
			if(Tipooperacao.TIPO_CREDITO.equals(tipooperacao)){
				if (tipotaxa1.getContacredito() != null && tipotaxa1.getCentrocusto() != null &&
						tipotaxa1.getContacredito().equals(tipotaxa2.getContacredito()) && 
							tipotaxa1.getCentrocusto().equals(tipotaxa2.getCentrocusto())){
					contagerencial = tipotaxa1.getContacredito();
					centrocusto = tipotaxa1.getCentrocusto();
				}
			}else if(Tipooperacao.TIPO_DEBITO.equals(tipooperacao)){
				if (tipotaxa1.getContadebito() != null && tipotaxa1.getCentrocusto() != null &&
						tipotaxa1.getContadebito().equals(tipotaxa2.getContadebito()) && 
							tipotaxa1.getCentrocusto().equals(tipotaxa2.getCentrocusto())){
					contagerencial = tipotaxa1.getContadebito();
					centrocusto = tipotaxa1.getCentrocusto();
				}
			}
				
			if (contagerencial != null && centrocusto != null){
				rateioitem = new Rateioitem();
				rateioitem.setCentrocusto(centrocusto);
				rateioitem.setContagerencial(contagerencial);
				rateioitem.setValor(valorMultaJuros);
				
				if (rateio != null && rateio.getListaRateioitem() != null){
					boolean existeRateioJuros = false;
					for (Rateioitem ri : rateio.getListaRateioitem()){
						if (ri.getContagerencial().equals(rateioitem.getContagerencial()) &&
								ri.getCentrocusto().equals(rateioitem.getCentrocusto())){
							ri.setValor(rateioitem.getValor());
							existeRateioJuros = true;
							break;
						}
					}
					if(!existeRateioJuros){
						rateio.getListaRateioitem().add(rateioitem);
					}
				} else {
					rateio = new Rateio();
					rateio.setListaRateioitem(new ArrayList<Rateioitem>());
					rateio.getListaRateioitem().add(rateioitem);
				}
				
				this.rateioItensPercentual(rateio, valorTotalPago);
			}
		}		
	}
	
	/**
	* M�todo que busca os rateios dos documentos
	*
	* @param listaDocumento
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public void setRateioDocumento(List<Documento> listaDocumento) {
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			List<Rateio> listaRateio = findByRateio(CollectionsUtil.listAndConcatenate(listaDocumento, "rateio.cdrateio", ","));
			if(SinedUtil.isListNotEmpty(listaRateio)){
				for(Documento documento : listaDocumento){
					if(documento.getRateio() != null && documento.getRateio().getCdrateio() != null){
						for (Iterator<Rateio> iterator = listaRateio.iterator(); iterator.hasNext();) {
							Rateio it = iterator.next();
							if(documento.getRateio().getCdrateio().equals(it.getCdrateio())){
								documento.setRateio(it);
								iterator.remove();
								break;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RateioDAO#findByRateio(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public List<Rateio> findByRateio(String whereIn){
		return rateioDAO.findByRateio(whereIn);
	}

	/**
	* M�todo que cria uma nova instancia do rateio
	*
	* @param rateio
	* @return
	* @since 20/09/2016
	* @author Luiz Fernando
	*/
	public Rateio getNovoRateio(Rateio rateio) {
		Rateio rateioNovo = new Rateio();
		if(rateio == null){
			rateio = new Rateio();
		}
		rateioNovo.setCdrateio(rateio.getCdrateio());
		if(rateio.getListaRateioitem() == null){
			rateio.setListaRateioitem(new ArrayList<Rateioitem>());
		}
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		for(Rateioitem rateioitem : rateio.getListaRateioitem()){
			listaRateioitem.add(new Rateioitem(rateioitem));
		}
		rateioNovo.setListaRateioitem(listaRateioitem);
		return rateioNovo;
	}
	
	public Rateio createRateioByModelo(Rateiomodelo rateiomodelo){
		Rateiomodelo modelo = rateiomodeloService.loadForRateio(rateiomodelo);
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(new ArrayList<Rateioitem>());
		for(Rateiomodeloitem item: modelo.getListaRateiomodeloitem()){
			Rateioitem rateioitem = new Rateioitem(item.getContagerencial(), item.getCentrocusto(), item.getProjeto(),
												new Money(), item.getPercentual());
			rateio.getListaRateioitem().add(rateioitem);
		}
		return rateio;
	}

	public void ajustaDiferencaRateio(Money valor, List<Rateioitem> listaRateioitem) {
		if(valor != null && valor.getValue().doubleValue() > 0 && SinedUtil.isListNotEmpty(listaRateioitem)){
			Money total = new Money();
			Rateioitem rateioitemUltimo = null;
			for(Rateioitem it : listaRateioitem){
				if(it.getValor() != null){
					total = total.add(it.getValor());
					rateioitemUltimo = it;
				}
			}
			Double diferenca = SinedUtil.round(total.subtract(valor).getValue().doubleValue(), 2); 
			if(diferenca != 0 && diferenca >= -0.01 && diferenca <= 0.01 && rateioitemUltimo != null){
				rateioitemUltimo.setValor(rateioitemUltimo.getValor().subtract(new Money(diferenca)));
				rateioitemUltimo.setPercentual(rateioitemUltimo.getValor().divide(valor).multiply(new Money(100d)).getValue().doubleValue());
			}
		}
	}

	public Integer calculaPorcentagemRateada(Double valorReferencia,List<Rateioitem> listaItemRateio) {
		Double porcentagem =0.0;
		for (Rateioitem item : listaItemRateio) {
			porcentagem = porcentagem + item.getPercentual();
		}	
		return porcentagem.intValue();
	}
	
	public Rateio criarRateio(Movimentacaoestoque movimentacaoestoque, Material materialOrigem, MaterialRateioEstoqueTipoContaGerencial materialRateioEstoqueTipoContaGerencial, Centrocusto centrocusto, Projeto projeto){
		return criarRateio(movimentacaoestoque, materialOrigem, materialRateioEstoqueTipoContaGerencial, centrocusto, projeto, null, null);
	}
	

	public Rateio criarRateio(Movimentacaoestoque movimentacaoestoque, Material materialOrigem, MaterialRateioEstoqueTipoContaGerencial materialRateioEstoqueTipoContaGerencial, Centrocusto centrocusto, Projeto projeto, Money valorItemRateio){
		return criarRateio(movimentacaoestoque, materialOrigem, materialRateioEstoqueTipoContaGerencial, centrocusto, projeto, valorItemRateio, null);
	}
	
	public Rateio criarRateio(Movimentacaoestoque movimentacaoestoque, Material materialOrigem, MaterialRateioEstoqueTipoContaGerencial materialRateioEstoqueTipoContaGerencial, Centrocusto centrocusto, Projeto projeto, Cfop cfop){
		return criarRateio(movimentacaoestoque, materialOrigem, materialRateioEstoqueTipoContaGerencial, centrocusto, projeto, null, cfop);
	}

	public Rateio criarRateio(Movimentacaoestoque movimentacaoestoque, Material materialOrigem, MaterialRateioEstoqueTipoContaGerencial materialRateioEstoqueTipoContaGerencial, Centrocusto centrocusto, Projeto projeto, Money valorItemRateio, Cfop cfop){

		if(!SinedUtil.isRateioMovimentacaoEstoque()) return null;
		
		Material material = null;
		if(materialOrigem != null){
			material = materialOrigem.getMaterialRateioEstoque() != null ? materialOrigem : materialService.loadForMaterialRateioEstoque(materialOrigem);
		}
		
		if(material != null && material.getMaterialRateioEstoque() != null){
			if(movimentacaoestoque.getContaGerencial() == null){
				if(cfop != null && cfop.getContaGerencial() != null){
					movimentacaoestoque.setContaGerencial(cfop.getContaGerencial());
				}else if(MaterialRateioEstoqueTipoContaGerencial.CONSUMO.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialConsumo());
				}else if(MaterialRateioEstoqueTipoContaGerencial.REQUISICAO_MATERIAL.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialRequisicao());
				}else if(MaterialRateioEstoqueTipoContaGerencial.ENTRADA_MERCADORIA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialEntrada());
				}else if(MaterialRateioEstoqueTipoContaGerencial.VENDA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialVenda());
				}else if(MaterialRateioEstoqueTipoContaGerencial.AJUSTE_ENTRADA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialAjusteEntrada());
				}else if(MaterialRateioEstoqueTipoContaGerencial.AJUSTE_SAIDA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialAjusteSaida());
				}else if(MaterialRateioEstoqueTipoContaGerencial.ROMANEIO_ENTRADA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialRomaneioEntrada());
				}else if(MaterialRateioEstoqueTipoContaGerencial.ROMANEIO_SAIDA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialRomaneioSaida());
				}else if(MaterialRateioEstoqueTipoContaGerencial.DEVOLUCAO_ENTRADA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialDevolucaoEntrada());
				}else if(MaterialRateioEstoqueTipoContaGerencial.DEVOLUCAO_SAIDA.equals(materialRateioEstoqueTipoContaGerencial)){
					movimentacaoestoque.setContaGerencial(material.getMaterialRateioEstoque().getContaGerencialDevolucaoSaida());
				}
			}
			if(movimentacaoestoque.getProjetoRateio() == null){
				movimentacaoestoque.setProjetoRateio(projeto != null ? projeto : (movimentacaoestoque.getProjeto() != null ? movimentacaoestoque.getProjeto() : material.getMaterialRateioEstoque().getProjeto()));
			}
			if(movimentacaoestoque.getCentrocusto() == null){
				movimentacaoestoque.setCentrocusto(centrocusto);
			}
		}
		
		Money valorTotal = movimentacaoestoque.getValorTotal();
		if(valorItemRateio != null){
			valorTotal = valorItemRateio;
		}
		
		return criarRateio(movimentacaoestoque.getContaGerencial(), 
				movimentacaoestoque.getCentrocusto(), 
				movimentacaoestoque.getProjetoRateio(), 
				valorTotal != null ? valorTotal : new Money(), 
				100d);
	}
	
	public Rateio criarRateio(Movimentacaoestoque movimentacaoestoque){
		if(!SinedUtil.isRateioMovimentacaoEstoque()) return null;
		
		return criarRateio(movimentacaoestoque.getContaGerencial(), 
				movimentacaoestoque.getCentrocusto(), 
				movimentacaoestoque.getProjetoRateio(), 
				movimentacaoestoque.getValorTotal(), 
				100d);
	}
	
	public Rateio criarRateio(Contagerencial contagerencial, Centrocusto centroCusto, Projeto projeto, Money valor, Double percentual){
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(new ArrayList<Rateioitem>());
		rateio.getListaRateioitem().add(new Rateioitem(contagerencial, centroCusto, projeto, valor, percentual));
		return rateio;
	}

	public ModelAndView abrirRateio(WebRequestContext request) {
		String cdrateio = request.getParameter("cdrateio");
		CusteioProcessado custeioProcessado = new CusteioProcessado();
		custeioProcessado.setRateio(cdrateio != null ? findRateio(new Rateio(Integer.parseInt(cdrateio))) : new Rateio());
		if(SinedUtil.isListNotEmpty(custeioProcessado.getRateio().getListaRateioitem())){
			Money valorTotal = new Money();
			for(Rateioitem rateioitem : custeioProcessado.getRateio().getListaRateioitem()){
				if(rateioitem.getValor() != null){
					valorTotal = valorTotal.add(rateioitem.getValor());
				}
			}
			custeioProcessado.setValor(valorTotal);
		}
		request.setAttribute("consultar", true);
		return new ModelAndView("direct:/process/popup/popUpRateio" ,"bean", custeioProcessado);
	}
}
