package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.dao.RateioitemDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RateioitemService extends GenericService<Rateioitem>{
	
	private RateioitemDAO rateioitemDAO;
	
	public void setRateioitemDAO(RateioitemDAO rateioitemDAO) {
		this.rateioitemDAO = rateioitemDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RateioitemDAO#findByRateio
	 * @param rateio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Rateioitem> findByRateio(Rateio rateio){
		return rateioitemDAO.findByRateio(rateio);
	}
	
	/**
	 * Faz o agrupamento dos itens de rateio se o trio, conta gerencial, projeto e centrocusto s�o iguais;
	 * 
	 * @param listaTodosItens
	 * @author Rodrigo Freitas
	 */
	public void agruparItensRateio(List<Rateioitem> listaTodosItens) {
		Rateioitem ibean;
		Rateioitem jbean;
		for (int i = 0; i < listaTodosItens.size(); i++) {
			ibean = listaTodosItens.get(i);
			for (int j = 0; j < listaTodosItens.size(); j++) {
				jbean = listaTodosItens.get(j);
				if (j != i && 
						ibean.getCentrocusto() != null && jbean.getCentrocusto() != null &&
						ibean.getContagerencial() != null && jbean.getContagerencial() != null &&
						ibean.getCentrocusto().getCdcentrocusto().equals(jbean.getCentrocusto().getCdcentrocusto()) &&
						ibean.getContagerencial().getCdcontagerencial().equals(jbean.getContagerencial().getCdcontagerencial())) {
					if (ibean.getProjeto() == null && jbean.getProjeto() == null) {
						//jun��o dos dois itens de rateio.
						ibean.setValor(ibean.getValor().add(jbean.getValor()));
						if(jbean.getPercentual() != null){
							if(ibean.getPercentual() != null){
								ibean.setPercentual(ibean.getPercentual() + jbean.getPercentual());
							}else {
								ibean.setPercentual(jbean.getPercentual());
							}
						}
						listaTodosItens.remove(j);
						j--;
					} else {
						if (ibean.getProjeto() != null && jbean.getProjeto() != null 
								&& ibean.getProjeto().getCdprojeto().equals(jbean.getProjeto().getCdprojeto())) {
							//jun��o dos dois itens de rateio.
							ibean.setValor(ibean.getValor().add(jbean.getValor()));
							if(jbean.getPercentual() != null){
								if(ibean.getPercentual() != null){
									ibean.setPercentual(ibean.getPercentual() + jbean.getPercentual());
								}else {
									ibean.setPercentual(jbean.getPercentual());
								}
							}
							listaTodosItens.remove(j);
							j--;
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que verifica se o trio (centro custo, projeto e conta ger�ncial) do rateio s�o iguais 
	 * podendo os campos obrigat�rios serem null.
	 * 
	 * @param rateioitem
	 * @param rateioitemLista
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean verificaItemRateioIgual(Rateioitem rateioitem, Rateioitem rateioitemLista) {
		if(rateioitemLista.getContagerencial().equals(rateioitem.getContagerencial())){
			if((rateioitemLista.getCentrocusto() == null && rateioitem.getCentrocusto() == null) || 
			   (rateioitemLista.getCentrocusto() != null && rateioitem.getCentrocusto() != null)){
				if(rateioitemLista.getCentrocusto() != null && rateioitem.getCentrocusto() != null)
					if(!rateioitemLista.getCentrocusto().equals(rateioitem.getCentrocusto()))
						return false;
			
				if((rateioitemLista.getProjeto() == null && rateioitem.getProjeto() == null) || 
				   (rateioitemLista.getProjeto() != null && rateioitem.getProjeto() != null)){
					if(rateioitemLista.getProjeto() != null && rateioitem.getProjeto() != null)
						if(!rateioitemLista.getProjeto().equals(rateioitem.getProjeto()))
							return false;
				} else{
					return false;
				}
			} else{
				return false;
			}
			
			return true;
		}
		return false;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.RateioitemDAO#findByDocumentos
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Rateioitem> findByDocumentos(String whereIn) {
		return rateioitemDAO.findByDocumentos(whereIn);
	}

	/**
	 * M�todo que retorna lista dos projetos utilizados em um determinado rateio
	 * 
	 * @param listaRateioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> getProjetos(List<Rateioitem> listaRateioitem) {
		List<Projeto> projetos = new ListSet<Projeto>(Projeto.class);
		for (Rateioitem rateioitem : listaRateioitem) {
			if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto()!= null)
				projetos.add(rateioitem.getProjeto());
		}
		
		return projetos;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param rateioitem
	 * @Author Tom�s Rabelo
	 */
	public void updateValorRateioItem(Rateioitem rateioitem) {
		rateioitemDAO.updateValorRateioItem(rateioitem);
	}
	
	/**
	 * 
	 * M�todo que calcula o percentual de um rateioItems.
	 *
	 * @name calculaPercentualRateioItem
	 * @param rateioitem
	 * @param valor
	 * @return
	 * @return Double
	 * @author Thiago Augusto
	 * @date 21/06/2012
	 *
	 */
	public Rateioitem calculaPercentualRateioItem(Rateioitem rateioitem, Money valor){
		Rateioitem ri = new Rateioitem();
		ri.setContagerencial(rateioitem.getContagerencial());
		ri.setProjeto(rateioitem.getProjeto());
		ri.setCentrocusto(rateioitem.getCentrocusto());
		ri.setValor(valor.multiply(new Money(rateioitem.getPercentual()).divide(new Money(100))));
		ri.setPercentual(rateioitem.getPercentual());
		return ri;
//		return Double.parseDouble(rateioitem.getValor().multiply(new Money(100)).divide(valor).toString());
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RateioitemDAO#findByRateioWithProjetoCliente(Rateio rateio)
	 *
	 * @param rateio
	 * @return
	 * @author Luiz Fernando
	 * @since 02/12/2013
	 */
	public List<Rateioitem> findByRateioWithProjetoCliente(Rateio rateio) {
		return rateioitemDAO.findByRateioWithProjetoCliente(rateio);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param rateioitem
	 * @Author Lucas Costa
	 */
	public void updateProcentagemRateioItem(Rateioitem rateioitem) {
		rateioitemDAO.updateProcentagemRateioItem(rateioitem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param concatenate
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/11/2017
	 */
	public List<Rateioitem> findForFaturaLocacao(String whereInRateio) {
		return rateioitemDAO.findForFaturaLocacao(whereInRateio);
	}

	/**
	* M�todo que atualiza e agrupa o item de rateio de acordo com a nova conta gerencial
	*
	* @param rateio
	* @param contagerencial
	* @since 10/01/2018
	* @author Luiz Fernando
	*/
	public void atualizaContagerencial(Rateio rateio, Contagerencial contagerencial) {
		if(rateio != null && SinedUtil.isListNotEmpty(rateio.getListaRateioitem()) && contagerencial != null){
			for(Rateioitem rateioitem : rateio.getListaRateioitem()){
				rateioitem.setContagerencial(contagerencial);
			}
			agruparItensRateio(rateio.getListaRateioitem());
		}
	}
}
