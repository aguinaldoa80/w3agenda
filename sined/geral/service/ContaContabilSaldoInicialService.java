package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.dao.ContaContabilSaldoInicialDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContaContabilSaldoInicialService extends GenericService<ContaContabilSaldoInicial>{

	private ContaContabilSaldoInicialDAO contaContabilSaldoInicialDAO;
	private ContacontabilService contacontabilService;
	private ArquivoService arquivoService;
	
	public void setContaContabilSaldoInicialDAO(
			ContaContabilSaldoInicialDAO contaContabilSaldoInicialDAO) {
		this.contaContabilSaldoInicialDAO = contaContabilSaldoInicialDAO;
	}
	public void setContacontabilService(
			ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void saveOrUpdateList(List<ContaContabilSaldoInicial> lista){
		Timestamp dataAtual = SinedDateUtils.currentTimestamp();
		arquivoService.saveFile(lista.get(0), "arquivo");
		for(ContaContabilSaldoInicial bean: lista){
			bean.setDtaltera(dataAtual);
			bean.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			this.saveOrUpdateNoUseTransaction(bean);
			contacontabilService.saveSaldoInicial(bean);
		}
	}
}
