package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Customaoobraitem;
import br.com.linkcom.sined.geral.bean.Customaoobraitemcargo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.dao.CustomaoobraitemDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CustomaoobraitemService extends GenericService<Customaoobraitem> {
	
	private CustomaoobraitemDAO customaoobraitemDAO;

	public void setCustomaoobraitemDAO(CustomaoobraitemDAO customaoobraitemDAO) {
		this.customaoobraitemDAO = customaoobraitemDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public List<Customaoobraitem> findForAlteracaoPercentual(String whereIn) {
		return customaoobraitemDAO.findForAlteracaoPercentual(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param customaoobraitem
	 * @param percentualvenda
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public void updatePercentualVenda(Customaoobraitem customaoobraitem, Double percentualvenda) {
		customaoobraitemDAO.updatePercentualVenda(customaoobraitem, percentualvenda);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param customaoobraitem
	 * @param custovenda
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public void updateCustoVenda(Customaoobraitem customaoobraitem, Money custovenda) {
		customaoobraitemDAO.updateCustoVenda(customaoobraitem, custovenda);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cargo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Customaoobraitem> findByCargo(Cargo cargo) {
		return customaoobraitemDAO.findByCargo(cargo);
	}
	
	public Customaoobraitem criaCopia(Customaoobraitem origem) {
		Customaoobraitem copia = new Customaoobraitem();
		
		origem = this.loadForEntrada(origem);
		
		copia.setIdentificador(origem.getIdentificador());
		copia.setDescricao(origem.getDescricao());
		copia.setCustomaoobraitemgrupo(origem.getCustomaoobraitemgrupo());
		copia.setUnidademedida(origem.getUnidademedida());
		copia.setCustounitario(origem.getCustounitario());
		copia.setQuantidade(origem.getQuantidade());
		copia.setPercentualvenda(origem.getPercentualvenda());
		copia.setCustovenda(origem.getCustovenda());
		
		if(SinedUtil.isListNotEmpty(origem.getListaCustomaoobraitemcargo())){
			for(Customaoobraitemcargo customaoobraitemcargo : origem.getListaCustomaoobraitemcargo()){
				customaoobraitemcargo.setCdcustomaoobraitemcargo(null);
				customaoobraitemcargo.setCustomaoobraitem(null);
			}
			copia.setListaCustomaoobraitemcargo(origem.getListaCustomaoobraitemcargo());
		}
		return copia;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CustomaoobraitemDAO#findForImportarOrcamento(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 05/03/2015
	* @author Luiz Fernando
	*/
	public List<Customaoobraitem> findForImportarOrcamento(Orcamento orcamento) {
		return customaoobraitemDAO.findForImportarOrcamento(orcamento);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CustomaoobraitemDAO#buscarCustomaoobraitemParaCopia(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	public List<Customaoobraitem> buscarCustomaoobraitemParaCopia(Orcamento orcamento) {
		return customaoobraitemDAO.buscarCustomaoobraitemParaCopia(orcamento);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CustomaoobraitemDAO#getTotalcustovenda(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 06/07/2015
	* @author Luiz Fernando
	*/
	public Double getTotalcustovenda(Orcamento orcamento) {
		return customaoobraitemDAO.getTotalcustovenda(orcamento);
	} 
}
