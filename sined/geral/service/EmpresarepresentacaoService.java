package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresarepresentacao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.dao.EmpresarepresentacaoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresarepresentacaoService extends GenericService<Empresarepresentacao>{
	
	private EmpresarepresentacaoDAO empresarepresentacaoDAO;
	
	public void setEmpresarepresentacaoDAO(EmpresarepresentacaoDAO empresarepresentacaoDAO) {
		this.empresarepresentacaoDAO = empresarepresentacaoDAO;
	}
	
	/**
	 * 
	 * @param empresa
	 * @param fornecedor
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isEmpresaAndFornecedor(Empresa empresa, Fornecedor fornecedor){
		return empresarepresentacaoDAO.isEmpresaAndFornecedor(empresa, fornecedor);
	}	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param empresa
	 * @return
	 */
	public boolean isEmpresaRepresentacao(Empresa empresa){
		return empresarepresentacaoDAO.isEmpresaRepresentacao(empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param empresa
	 * @return
	 */
	public List<Empresarepresentacao> findByEmpresa(Empresa empresa) {
		return empresarepresentacaoDAO.findByEmpresa(empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmpresarepresentacaoDAO#findByEmpresaFornecedor(Empresa empresa, Fornecedor fornecedor)
	*
	* @param empresa
	* @param fornecedor
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Empresarepresentacao> findByEmpresaFornecedor(Empresa empresa, Fornecedor fornecedor) {
		return empresarepresentacaoDAO.findByEmpresaFornecedor(empresa, fornecedor);
	}
	
	/**
	* M�todo que retorna empresarepresentacao de acordo com a empresa e fornecedor
	*
	* @param empresa
	* @param fornecedor
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public Empresarepresentacao getEmpresarepresentacao(Empresa empresa, Fornecedor fornecedor) {
		Empresarepresentacao empresarepresentacao = null;
		List<Empresarepresentacao> lista = findByEmpresaFornecedor(empresa, fornecedor);
		if(SinedUtil.isListNotEmpty(lista) && lista.size() >= 1){
			empresarepresentacao = lista.get(0);
		}
		return empresarepresentacao;
	}

	/**
	* M�todo que verifica se o repasse ao representante � �nico
	*
	* @param empresa
	* @param fornecedor
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public boolean isRepasseUnico(Empresa empresa, Fornecedor fornecedor) {
		boolean repasseunico = false;
		if(empresa != null && fornecedor != null){
			List<Empresarepresentacao> listaRepresentacao = findByEmpresaFornecedor(empresa, fornecedor);
			if(listaRepresentacao != null && listaRepresentacao.size() == 1){
				repasseunico = listaRepresentacao.get(0).getRepasseunico() != null ? listaRepresentacao.get(0).getRepasseunico() : false ;
			}
		}
		return repasseunico;
	}
	
	/**
	* M�todo que retorna o percentual de comiss�o do fornecedor
	*
	* @param empresa
	* @param fornecedor
	* @return
	* @since 23/02/2015
	* @author Luiz Fernando
	*/
	public Money getPercentualComissaoFornecedor(Empresa empresa, Fornecedor fornecedor){
		Money percentual = new Money();
		if(empresa != null && fornecedor != null){
			List<Empresarepresentacao> listaRepresentacao = findByEmpresaFornecedor(empresa, fornecedor);
			if(listaRepresentacao != null && listaRepresentacao.size() == 1){
				percentual = listaRepresentacao.get(0).getComissaovenda();
			}
		}
		return percentual;
	}
}
