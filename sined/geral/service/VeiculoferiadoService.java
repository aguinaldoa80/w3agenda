package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Veiculoferiado;
import br.com.linkcom.sined.geral.dao.VeiculoferiadoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;

public class VeiculoferiadoService extends GenericService<Veiculoferiado> {

	VeiculoferiadoDAO feriadoDAO;
	
	public void setFeriadoDAO(VeiculoferiadoDAO feriadoDAO) {
		this.feriadoDAO = feriadoDAO;
	}
	
	/**
	 * <b>M�todo respons�vel em retornar uma lista contendo os feriados dentro do intervalo passado</b>
	 * @param firtDate data inicial
	 * @param secondDate data final
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoferiadoDAO#listaIntervaloFeriado(Date firtDate, Date secondDate)
	 * @return uma lista contendo os feriados dentro do intervalo passado
	 */
	public List<Veiculoferiado> listaIntervaloFeriado(Date firtDate, Date secondDate){
		return feriadoDAO.listaIntervaloFeriado(firtDate, secondDate);
	}
	
	/**
	 * <b>M�todo respons�vel em retorna um Boolean se o data enviada � feriado ou n�o</b> 
	 * @param date data enviada 
	 * @return {@link Boolean}
	 */
	public Boolean isFeriado(Date date){
		return feriadoDAO.isFeriado(date);
	}

	/**
	 * M�todo que verifica se a data � um feriado
	 * 
	 * @param data
	 * @param listFeriados
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isFeriado(Date data, List<Veiculoferiado> listFeriados) {
		data = SinedDateUtils.dateToBeginOfDay(data);
		if(listFeriados != null && !listFeriados.isEmpty())
			for (Veiculoferiado veiculoferiado : listFeriados) {
				Date dateAux = SinedDateUtils.dateToBeginOfDay(veiculoferiado.getData());
				if(data.getTime() == dateAux.getTime())
					return true;
			}
		return false;
	}
}
