package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.geral.dao.SolicitacaocomprahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaocomprahistoricoService extends GenericService<Solicitacaocomprahistorico>{

	private SolicitacaocomprahistoricoDAO solicitacaocomprahistoricoDAO;
	
	public void setSolicitacaocomprahistoricoDAO(SolicitacaocomprahistoricoDAO solicitacaocomprahistoricoDAO) {
		this.solicitacaocomprahistoricoDAO = solicitacaocomprahistoricoDAO;
	}
	
	public Solicitacaocomprahistorico getUsuarioLastAction(Solicitacaocompra solicitacaocompra, Solicitacaocompraacao acao) {
		return solicitacaocomprahistoricoDAO.getUsuarioLastAction(solicitacaocompra, acao);
	}
	
}
