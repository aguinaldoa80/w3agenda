package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.dao.PrazopagamentoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.PrazopagamentoWSBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoitemapoio;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.PrazopagamentoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.prazopagamento.PrazopagamentoRESTModel;

public class PrazopagamentoService extends GenericService<Prazopagamento> {
	
	private PrazopagamentoDAO prazopagamentoDAO;
	private ParametrogeralService parametrogeralService;
	private DocumentotipoService documentotipoService;
	
	public void setPrazopagamentoDAO(PrazopagamentoDAO prazopagamentoDAO) {
		this.prazopagamentoDAO = prazopagamentoDAO;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	
	/* singleton */
	private static PrazopagamentoService instance;
	public static PrazopagamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PrazopagamentoService.class);
		}
		return instance;
	}
	
	/**
	 * Retorna o n�mero de itens de um prazo de pagamento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PrazopagamentoDAO#countItens(Prazopagamento)
	 * @param prazo
	 * @return
	 * @author Hugo Ferreira
	 */
	public Long countItens(Prazopagamento prazo) {
		return prazopagamentoDAO.countItens(prazo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Prazopagamento getPrazoPagamentoUnico() {
		return prazopagamentoDAO.getPrazoPagamentoUnico();
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @param prazopagamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Prazopagamento loadAll(Prazopagamento prazopagamento){
		return prazopagamentoDAO.loadAll(prazopagamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Prazopagamento> findAtivosForFlex(){
		return prazopagamentoDAO.findAtivosForFlex();
	}
	
	/**
	 * M�todo para verificar se o prazo de pagamento � considerado �nico, ou seja,
	 * se suas parcelas possuem dias ou meses diferentes de zero.
	 * 
	 * @param prazopagamento
	 * @return true - se o prazo for �nico
	 * 			false- caso contr�rio
	 * @author Fl�vio Tavares
	 */
	public boolean verificaPrazoUnico(Prazopagamento prazopagamento){
		List<Prazopagamentoitem> listaPagamentoItem = prazopagamento.getListaPagamentoItem();
		boolean existeDiaOuMes = false;
		
		if(SinedUtil.isListNotEmpty(listaPagamentoItem)){
			for (Prazopagamentoitem item : listaPagamentoItem) {
				existeDiaOuMes = item.getDias() != null && item.getDias() != 0
							|| item.getMeses() != null && item.getMeses() != 0;
				
				if(existeDiaOuMes) break;
			}
		}
		
		return !existeDiaOuMes;
	}
	
	/**
	 * Monta as parcelas a partir do prazo de pagamento
	 * 
	 * @param prazopagamentoAux
	 * @param valorTotalAux
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservicoprazopagamentoitemapoio> montaPrazoPagamentoFlex(Prazopagamento prazopagamentoAux, Double valorTotalAux){
		List<Agendamentoservicoprazopagamentoitemapoio> listaItens = new ArrayList<Agendamentoservicoprazopagamentoitemapoio>();
		Prazopagamento prazopagamento = prazopagamentoDAO.loadForEntrada(prazopagamentoAux);
		Agendamentoservicoprazopagamentoitemapoio agendamentoservicoprazopagamentoitemapoio;
		java.sql.Date dtparcela = new java.sql.Date(System.currentTimeMillis());
		Money valorTotal = new Money(valorTotalAux);
		
		String codigoDocumentoTipo = parametrogeralService.getValorPorNome(Parametrogeral.DOCUMENTOTIPOPADRAO_AGENDAMENTOSERVICO);
		Documentotipo documentotipo = null;
		if(codigoDocumentoTipo != null && !codigoDocumentoTipo.equals(""))
			documentotipo = documentotipoService.loadForEntrada(new Documentotipo(Integer.valueOf(codigoDocumentoTipo)));
		
		for (Prazopagamentoitem prazopagamentoitem : prazopagamento.getListaPagamentoItem()) {
			if(prazopagamentoitem.getDias() != null)
				dtparcela = SinedDateUtils.incrementDate(dtparcela, prazopagamentoitem.getDias().intValue(), Calendar.DAY_OF_MONTH);
			if(prazopagamentoitem.getMeses() != null)
				dtparcela = SinedDateUtils.incrementDate(dtparcela, prazopagamentoitem.getMeses().intValue(), Calendar.MONTH);
			
			agendamentoservicoprazopagamentoitemapoio = new Agendamentoservicoprazopagamentoitemapoio(valorTotal.divide(new Money(prazopagamento.getListaPagamentoItem().size())).getValue().doubleValue(), dtparcela, documentotipo);
			listaItens.add(agendamentoservicoprazopagamentoitemapoio);
		}
		
		return listaItens;
	}
	
	/**
	 * M�todo que retorna todos para combo
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Prazopagamento> findAllForFlex(){
		return prazopagamentoDAO.findAllForFlex();
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* M�todo que retorna o juros do prazo de pagamento
	* 
	* @see	br.com.linkcom.sined.geral.dao.PrazopagamentoDAO.findForPrazopagamentoJuros(Prazopagamento prazopagamento)
	*
	* @param prazopagamento
	* @return
	* @since Jul 21, 2011
	* @author Luiz Fernando F Silva
	*/
	public Prazopagamento findForPrazopagamentoJuros(Prazopagamento prazopagamento){
		return prazopagamentoDAO.findForPrazopagamentoJuros(prazopagamento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PrazopagamentoDAO#findByPrazomedio(Boolean prazomedio)
	 *
	 * @param prazomedio
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Prazopagamento> findByPrazomedio(Boolean prazomedio){
		return prazopagamentoDAO.findByPrazomedio(prazomedio);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PrazopagamentoDAO#findByPrazomedioOrcamento()
	*
	* @return
	* @since 19/03/2015
	* @author Luiz Fernando
	*/
	public List<Prazopagamento> findByPrazomedioOrcamento(){
		return prazopagamentoDAO.findByPrazomedioOrcamento();
	}
	
		
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 02/04/2012
	 * @param cdPrazoPagamento
	 * @return
	 */
	public Prazopagamento findForCdPrazoPagamento(Integer cdPrazoPagamento){
		return prazopagamentoDAO.findForCdPrazoPagamento(cdPrazoPagamento);
	}
	
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<PrazopagamentoOfflineJSON> findForPVOffline() {
		List<PrazopagamentoOfflineJSON> lista = new ArrayList<PrazopagamentoOfflineJSON>();
		for(Prazopagamento pp : prazopagamentoDAO.findForPVOffline())
			lista.add(new PrazopagamentoOfflineJSON(pp));
		
		return lista;
	
	}
	
	/**
	 * s
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name buscaValormaximoDesconto
	 * @param bean
	 * @return
	 * @return Prazopagamento
	 * @author Thiago Augusto
	 * @date 12/06/2012
	 *
	 */
	public Prazopagamento buscaValormaximoDesconto(Prazopagamento bean){
		return prazopagamentoDAO.buscaValormaximoDesconto(bean);
	}
	
	public List<Prazopagamento> findForProcessoCompra(){
		return prazopagamentoDAO.findForProcessoCompra();
	}
	
	public List<PrazopagamentoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<PrazopagamentoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PrazopagamentoRESTModel> lista = new ArrayList<PrazopagamentoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Prazopagamento bean : prazopagamentoDAO.findForAndroid(whereIn))
				lista.add(new PrazopagamentoRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PrazopagamentoDAO#findByQtdeParcelas(Integer qtdeParcelas)
	*
	* @param qtdeParcelas
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	public List<Prazopagamento> findByQtdeParcelas(Integer qtdeParcelas) {
		return prazopagamentoDAO.findByQtdeParcelas(qtdeParcelas);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/04/2016
	 */
	public List<PrazopagamentoWSBean> findForWSSOAP() {
		List<PrazopagamentoWSBean> lista = new ArrayList<PrazopagamentoWSBean>();
		for(Prazopagamento p: prazopagamentoDAO.findForWSSOAP())
			lista.add(new PrazopagamentoWSBean(p));
		return lista;
	}
	
	/**
	 * Busca prazos de pagamento do cliente
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 27/05/2016
	 * 
	 */
	public List<Prazopagamento> findByCliente(Cliente cliente) {
		if (cliente==null || cliente.getCdpessoa()==null){
			return new ArrayList<Prazopagamento>();
		}
		return prazopagamentoDAO.findByCliente(cliente);	
	}
	
	public ModelAndView ajaxPrazopagamentoParcelasiguaisjuros(Prazopagamento prazopagamento){
		ModelAndView retorno = new JsonModelAndView();
		boolean parcelasiguaisjuros = false;
		
		if (prazopagamento!=null){
			parcelasiguaisjuros = Boolean.TRUE.equals(load(prazopagamento).getParcelasiguaisjuros());
		}
		
		retorno.addObject("parcelasiguaisjuros", parcelasiguaisjuros);
		
		return retorno;
	}
	
	public List<Prazopagamento> findForProcessoVenda(){
		return prazopagamentoDAO.findForProcessoVenda();
	}
	
	public List<Prazopagamento> findAtivosForCombo(){
		return prazopagamentoDAO.findAtivosForCombo(null);
	}
	
	public List<Prazopagamento> findAtivosForCombo(Prazopagamento prazopagamento){
		return prazopagamentoDAO.findAtivosForCombo(prazopagamento);
	}
	
	public Prazopagamento findByPrazoPagamento(Prazopagamento prazopagamento) {
		return prazopagamentoDAO.findByPrazoPagamento(prazopagamento);
	}
	public List<GenericBean> findByPrazomedioWSVenda(Boolean prazomedio){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findByPrazomedio(prazomedio));
	}
}