package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargoexameobrigatorio;
import br.com.linkcom.sined.geral.dao.CargoexameobrigatorioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargoexameobrigatorioService extends GenericService<Cargoexameobrigatorio> {
	
	private CargoexameobrigatorioDAO cargoexameobrigatorioDAO;
	
	public void setCargoexameobrigatorioDAO(
			CargoexameobrigatorioDAO cargoexameobrigatorioDAO) {
		this.cargoexameobrigatorioDAO = cargoexameobrigatorioDAO;
	}

	/* singleton */
	private static CargoexameobrigatorioService instance;
	public static CargoexameobrigatorioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoexameobrigatorioService.class);
		}
		return instance;
	}
	
	public List<Cargoexameobrigatorio> findExameobrigatorioByCargo(Cargo cargo){
		return cargoexameobrigatorioDAO.findExameobrigatorioByCargo(cargo);
	}

}
