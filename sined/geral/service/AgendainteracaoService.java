package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaocontratomodelo;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AgendainteracaocontratomodeloPessoatipo;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaorelacionado;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDataReferenciaContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.dao.AgendainteracaoDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.AgendainteracaoFiltro;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.AgendaInteracaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AgendainteracaoService extends GenericService<Agendainteracao>{
	private AgendainteracaoDAO agendainteracaoDAO;
	private ClientehistoricoService clientehistoricoService;
	private ContacrmhistoricoService contacrmhistoricoService;
	private AgendainteracaocontratomodeloService agendainteracaocontratomodeloService;
	private AgendainteracaohistoricoService agendainteracaohistoricoService;
	private ContratohistoricoService contratohistoricoService;
	private EmpresaService empresaService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private ColaboradorService colaboradorService;
	private DepartamentoService departamentoService;
	private UsuarioService usuarioService;
	
	public void setAgendainteracaoDAO(AgendainteracaoDAO agendainteracaoDAO) {
		this.agendainteracaoDAO = agendainteracaoDAO;
	}
	public void setClientehistoricoService(ClientehistoricoService clientehistoricoService) {
		this.clientehistoricoService = clientehistoricoService;
	}
	public void setContacrmhistoricoService(ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setAgendainteracaocontratomodeloService(
			AgendainteracaocontratomodeloService agendainteracaocontratomodeloService) {
		this.agendainteracaocontratomodeloService = agendainteracaocontratomodeloService;
	}
	public void setAgendainteracaohistoricoService(
			AgendainteracaohistoricoService agendainteracaohistoricoService) {
		this.agendainteracaohistoricoService = agendainteracaohistoricoService;
	}
	public void setContratohistoricoService(
			ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public List<Agendainteracao> findListagem(String whereIn, String orderBy, boolean asc) {
		return agendainteracaoDAO.findListagem(whereIn, orderBy, asc);
	}
	
	public List<Agendainteracao> findForAndroid(String whereIn, String orderBy, boolean asc) {
		return agendainteracaoDAO.findForAndroid(whereIn, orderBy, asc);
	}
	
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService=colaboradorService;
	}
	public void setDepartamentoService(DepartamentoService departamentoService) {
		this.departamentoService=departamentoService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService= usuarioService ;
	}
	

	/**
	 * Faz refer�ncia ao DAO.
	 * @param itensSelecionados
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void updateDataCancelamento(String itensSelecionados) {
		agendainteracaoDAO.updateDataCancelamento(itensSelecionados);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public void updateDataConclusao(String itensSelecionados) {
		agendainteracaoDAO.updateDataConclusao(itensSelecionados);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param itensSelecionados
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void updateDataProxInteracao(Integer cdagendainteracao, Date dtproximainteracao) {
		agendainteracaoDAO.updateDataProxInteracao(cdagendainteracao, dtproximainteracao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param agendainteracao
	 * @return
	 */
	public Agendainteracao loadForInteracao(Agendainteracao agendainteracao) {
		return agendainteracaoDAO.loadForInteracao(agendainteracao);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param agendainteracao
	 * @return
	 */
	public Agendainteracao loadForGerarOS(Agendainteracao agendainteracao){
		return agendainteracaoDAO.loadForGerarOS(agendainteracao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendainteracaoDAO.findForAgendaPortlet(Date data)
	 *
	 * @param data
	 * @return
	 * @since 09/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Agendainteracao> findForAgendaPortlet(Date data) {
		return agendainteracaoDAO.findForAgendaPortlet(data);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Agendainteracao> findForRelatorio(String whereIn){
		return agendainteracaoDAO.findForRelatorio(whereIn);
	}
	
	public String getEscala(Agendainteracao agendainteracao, AgendaInteracaoFiltro filtro){
		
		StringBuilder escala = new StringBuilder();
		
		Frequencia frequencia = agendainteracao.getPeriodicidade();
		
		if (frequencia!=null
				&& frequencia.getCdfrequencia()!=null
				&& filtro.getApuracaoPeriodoInicio()!=null 
				&& filtro.getApuracaoPeriodoFim()!=null
				&& SinedDateUtils.diferencaDias(filtro.getApuracaoPeriodoInicio(), filtro.getApuracaoPeriodoFim())<=0){
			
			Calendar c1 = Calendar.getInstance();
			c1.setTimeInMillis(filtro.getApuracaoPeriodoInicio().getTime());
			c1.set(Calendar.HOUR, 0);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);
			
			Calendar c2 = Calendar.getInstance();
			c2.setTimeInMillis(filtro.getApuracaoPeriodoFim().getTime());
			c2.set(Calendar.HOUR, 0);
			c2.set(Calendar.MINUTE, 0);
			c2.set(Calendar.SECOND, 0);
			c2.set(Calendar.MILLISECOND, 0);
			
			SimpleDateFormat formatador = new SimpleDateFormat("dd - EEEE");
			
			int multiplicador = agendainteracao.getQtdefrequencia()==null ? 1 : agendainteracao.getQtdefrequencia(); 
			
			escala.append(formatador.format(c1.getTime()));
			
			while (SinedDateUtils.diferencaDias(c1.getTime(), c2.getTime())<=0){
				if (Frequencia.DIARIA.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.DAY_OF_MONTH, multiplicador);
				}else if (Frequencia.SEMANAL.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.DAY_OF_MONTH, multiplicador * 7);
				}else if (Frequencia.QUINZENAL.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.DAY_OF_MONTH, multiplicador * 15);
				}else if (Frequencia.MENSAL.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.MONTH, multiplicador);
				}else if (Frequencia.TRIMESTRAL.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.MONTH, multiplicador * 3);
				}else if (Frequencia.SEMESTRE.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.MONTH, multiplicador * 6);
				}else if (Frequencia.ANUAL.equals(frequencia.getCdfrequencia())){
					c1.add(Calendar.YEAR, multiplicador);
				}else {
					break;
				}
				escala.append("<br>");
				escala.append(formatador.format(c1.getTime()));
			}
		}
		
		return escala.toString();
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.AgendainteracaoDAO#findProximaCobranca(String whereInCliente)
	*
	* @param whereInCliente
	* @return
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	public List<Agendainteracao> findProximaCobranca(Empresa empresa, String whereInCliente) {
		return agendainteracaoDAO.findProximaCobranca(empresa, whereInCliente);
	}

	public void registrarHistoricoClienteConta(Agendainteracao agendainteracao, String observacao) {
		if(agendainteracao.getTipoagendainteracao().equals(Tipoagendainteracao.CLIENTE)){
			Clientehistorico clienteHistorico = new Clientehistorico();
			clienteHistorico.setCliente(agendainteracao.getCliente());
			clienteHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			clienteHistorico.setObservacao(observacao);
			clienteHistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
			clienteHistorico.setAtividadetipo(agendainteracao.getAtividadetipo());
			clienteHistorico.setEmpresahistorico(agendainteracao.getEmpresahistorico());
			
			clientehistoricoService.saveOrUpdate(clienteHistorico);
		} else if(agendainteracao.getTipoagendainteracao().equals(Tipoagendainteracao.CONTA)){
			Contacrmhistorico contaHistorico = new Contacrmhistorico();
			contaHistorico.setContacrm(agendainteracao.getContacrm());
			contaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			contaHistorico.setObservacao(observacao);
			contaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
			contaHistorico.setAtividadetipo(agendainteracao.getAtividadetipo());
			contaHistorico.setEmpresahistorico(agendainteracao.getEmpresahistorico());
			
			contacrmhistoricoService.saveOrUpdate(contaHistorico);
		}
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param empresahistorico
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/05/2017
	 */
	public List<Agendainteracao> findByEmpresaCliente(Empresa empresa, Cliente cliente) {
		return agendainteracaoDAO.findByEmpresaCliente(empresa, cliente);
	}
	
	public void createAgendainteracaoByContratotipo(Contrato contrato){
		
		List<Agendainteracaocontratomodelo> listaModelosAgendainteracao = agendainteracaocontratomodeloService.findForCreationAgendainteracao(contrato.getContratotipo());
		List<Agendainteracao> listaAgendasForHistorico = new ArrayList<Agendainteracao>();
		for(Agendainteracaocontratomodelo modelo: listaModelosAgendainteracao){
			if(AgendainteracaocontratomodeloPessoatipo.VENDEDOR.equals(modelo.getPessoatipo()) &&
				!Vendedortipo.COLABORADOR.equals(contrato.getVendedortipo())){
				continue;
			}
			Colaborador responsavel = AgendainteracaocontratomodeloPessoatipo.VENDEDOR.equals(modelo.getPessoatipo())?
																		contrato.getColaborador(): contrato.getResponsavel();
			if(responsavel == null){
				continue;
			}
			Date data = null;
			if(TipoDataReferenciaContrato.DATAASSINATURA.equals(modelo.getReferencia())){
				data = contrato.getDtassinatura();
			}else if(TipoDataReferenciaContrato.DATAFINALIZACAO.equals(modelo.getReferencia())){
				data = contrato.getDtconclusao();
			}else if(TipoDataReferenciaContrato.DATAINICIO.equals(modelo.getReferencia())){
				data = contrato.getDtinicio();
			}else if(TipoDataReferenciaContrato.DATARENOVACAO.equals(modelo.getReferencia())){
				data = contrato.getDtrenovacao();
			}else if(TipoDataReferenciaContrato.PROXIMOVENCIMENTO.equals(modelo.getReferencia())){
				data = contrato.getDtproximovencimento();
			}
			if(data == null){
				continue;
			}
			data = SinedDateUtils.addDiasData(data, modelo.getDiasparacomecar());
			Agendainteracao bean = new Agendainteracao();
			bean.setTipoagendainteracao(Tipoagendainteracao.CLIENTE);
			bean.setEmpresahistorico(contrato.getEmpresa());
			bean.setCliente(contrato.getCliente());
			bean.setAgendainteracaorelacionado(Agendainteracaorelacionado.CONTRATO);
			bean.setContrato(contrato);
			bean.setQtdefrequencia(modelo.getFrequencia());
			bean.setResponsavel(responsavel);
			bean.setPeriodicidade(modelo.getPeriodicidade());
			bean.setAtividadetipo(modelo.getAtividadetipo());
			bean.setDescricao(modelo.getDescricao());
			bean.setAgendainteracaosituacao(Agendainteracaosituacao.NORMAL);
			bean.setDtproximainteracao(data);
			
			
			saveOrUpdate(bean);
			
			Agendainteracaohistorico historico = new Agendainteracaohistorico();
			historico.setAgendainteracao(bean);
			historico.setObservacao("Agenda de intera��o autom�tica <a href=\"javascript:visualizarContrato("+contrato.getCdcontrato()+")\">"+contrato.getCdcontrato().toString() +"</a>.");
			agendainteracaohistoricoService.saveOrUpdate(historico);
			
			listaAgendasForHistorico.add(bean);
			
		}
		contratohistoricoService.criaHistoricoForAgendainteracao(contrato, listaAgendasForHistorico);
	}
	
	public void criarAvisoAgendaInteracaoAtrasada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Agendainteracao> agendainteracaoList = agendainteracaoDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
//		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Agendainteracao a : agendainteracaoList) {
			Aviso aviso = new Aviso("Atraso na Agenda de Intera��o", "C�digo da agenda de intera��o: " + a.getCdagendainteracao(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), a.getCdagendainteracao(), empresa, SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, a.getCdagendainteracao());
			Usuario supervisor =null;
			Usuario responsavel = null;
			if(a.getResponsavel() !=null && a.getResponsavel().getCdpessoa() !=null){
				String whereIn = a.getResponsavel().getCdpessoa().toString();
				responsavel = usuarioService.findByCdpessoa(a.getResponsavel().getCdpessoa());
				Colaborador colaborador = colaboradorService.carregaColaborador(whereIn);
				
				colaborador = colaboradorService.carregaColaboradorCargoAtual(colaborador);
				Departamento dep = colaborador.getDepartamentotransient();
				if(dep.getResponsavel() ==null){
					dep = departamentoService.load(dep);
				}
				if(dep.getResponsavel() !=null){
					dep = departamentoService.retornarResponsavel(dep.getCddepartamento());
					supervisor = usuarioService.findByCdpessoa(dep.getResponsavel().getCdpessoa());
					
				}		
			}
			
			List<Usuario> listaUsuario = new ArrayList<Usuario>();
			if (responsavel !=null){
				listaUsuario.add(responsavel);
			}
			if(supervisor !=null){
				listaUsuario.add(supervisor);
			}

			if (lastAvisoDate != null) {
//				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, a.getCdagendainteracao(), lastAvisoDate);
//				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					aviso.setListaEnvolvidos(listaUsuario);
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				aviso.setListaEnvolvidos(listaUsuario);
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

	public void processarListagem(ListagemResult<Agendainteracao> listagemResult, AgendainteracaoFiltro filtro) {
		List<Agendainteracao> listaAgendainteracao = listagemResult.list();
		
		if(SinedUtil.isListNotEmpty(listaAgendainteracao)){
			String whereIn = CollectionsUtil.listAndConcatenate(listaAgendainteracao, "cdagendainteracao", ",");
			listaAgendainteracao.removeAll(listaAgendainteracao);
			listaAgendainteracao.addAll(findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}
	}
	
	public Resource gerarListagemCSV(AgendainteracaoFiltro filtro) {
		ListagemResult<Agendainteracao> listagemResult = findForExportacao(filtro);
		processarListagem(listagemResult, filtro);
		List<Agendainteracao> lista = listagemResult.list();
		
		SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
		String cabecalho = "\"Tipo\";\"Cliente/Conta\";\"Motivo\";\"Contato\";\"Respons�vel\";\"Pr�xima intera��o\";\"Situa��o\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Agendainteracao bean : lista) {
			
			csv.append("\"" + bean.getTipoagendainteracao().getNome() + "\";");
			
			if(bean.getTipoagendainteracao() != null && bean.getTipoagendainteracao().getValue() == 0) {
				Boolean ativo = bean.getCliente() != null && bean.getCliente().getAtivo();
				
				csv.append("\"" + (bean.getCliente() != null ? bean.getCliente().getNome() : "") + 
						(ativo != null && ativo.equals(Boolean.TRUE) ? " - Ativo" : " - Inativo") + "\";");
			} else {
				csv.append("\"" + (bean.getContacrm() != null ? bean.getContacrm().getNome() : "") + "\";");
			}
			
			if(bean.getAgendainteracaorelacionado() != null && bean.getAgendainteracaorelacionado().getValue() == 0) {
				csv.append("\"" + (bean.getMaterial() != null && bean.getMaterial().getAutocompleteDescription() != null ? bean.getMaterial().getAutocompleteDescription() : "") + "\";");		
			} else if (bean.getAgendainteracaorelacionado() != null && bean.getAgendainteracaorelacionado().getValue() == 1) {
				csv.append("\"" + (bean.getContrato() != null && bean.getContrato().getDescricao() != null ? bean.getContrato().getDescricao() : "") + "\";");				
			} else {
				csv.append("\"\";");
			}				
			
			csv.append("\"" + (bean.getContato() != null ? bean.getContato() : "") + "\";");
			csv.append("\"" + (bean.getResponsavel() != null ? bean.getResponsavel().getNome() : "") + "\";");
			csv.append("\"" + (bean.getDtproximainteracao() != null ? 
					pattern.format(bean.getDtproximainteracao()) : "") + "\";");
			csv.append("\"" + (bean.getAgendainteracaosituacao() != null && bean.getAgendainteracaosituacao().getNome() != null ? bean.getAgendainteracaosituacao().getNome() : "") + "\";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "agenda_de_interacao.csv", csv.toString().getBytes());
		return resource;
	}
}