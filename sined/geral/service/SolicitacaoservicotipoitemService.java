package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipoitem;
import br.com.linkcom.sined.geral.dao.SolicitacaoservicotipoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaoservicotipoitemService extends GenericService<Solicitacaoservicotipoitem>{

	private SolicitacaoservicotipoitemDAO solicitacaoservicotipoitemDAO;
	
	public void setSolicitacaoservicotipoitemDAO(
			SolicitacaoservicotipoitemDAO solicitacaoservicotipoitemDAO) {
		this.solicitacaoservicotipoitemDAO = solicitacaoservicotipoitemDAO;
	}

	/**
	 * Retorna a lista de tipoitem da solicitacao tipo
	 * @param tipo
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicotipoitemDAO#findByTipo(Solicitacaoservicotipo)
	 * @author C�ntia Nogueira
	 */
	public List<Solicitacaoservicotipoitem> findByTipo(Solicitacaoservicotipo tipo){
		return solicitacaoservicotipoitemDAO.findByTipo(tipo);
	}

}
