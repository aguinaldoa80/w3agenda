package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Expedicaoproducao;
import br.com.linkcom.sined.geral.bean.Expedicaoproducaoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.dao.ExpedicaoproducaoDAO;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ExpedicaoproducaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExpedicaoproducaoService extends GenericService<Expedicaoproducao> {

	private ExpedicaoproducaoDAO expedicaoproducaoDAO;
	private RegiaoService regiaoService;

	public void setRegiaoService(RegiaoService regiaoService) {
		this.regiaoService = regiaoService;
	}
	public void setExpedicaoproducaoDAO(ExpedicaoproducaoDAO expedicaoproducaoDAO) {
		this.expedicaoproducaoDAO = expedicaoproducaoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rafael Salvio
	 */
	public void updateSituacao(String whereIn, Expedicaosituacao situacao) {
		expedicaoproducaoDAO.updateSituacao(whereIn, situacao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 *
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Expedicaoproducao> findForConfirmacao(String whereIn) {
		return expedicaoproducaoDAO.findForConfirmacao(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @author Rafael Salvio
	 */
	public boolean haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao... situacoes) {
		return expedicaoproducaoDAO.haveExpedicaoNotInSituacao(whereIn, situacoes);
	}
	
	/**
	 * Cria o relat�rio da listagem de Expedi��oproducao
	 * 
	 *
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public IReport gerarRelatorioListagem(ExpedicaoproducaoFiltro filtro) {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		
		List<Expedicaoproducao> lista = this.findForGerarPDF(filtro);
		
		Report report = new Report("/producao/expedicaoproducao");
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 *
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	private List<Expedicaoproducao> findForGerarPDF(ExpedicaoproducaoFiltro filtro) {
		return expedicaoproducaoDAO.findForGerarPDF(filtro);
	}
	
	/**
	 * Cria o resgitro de Movimentacaoestoque na a��o de confirma��o da expedi��o.
	 *
	 * @param listaMovimentacaoestoque
	 * @param expedicaoproducao
	 * @param expedicaoproducaoitem
	 * @param material
	 * @param qtde
	 * @param movimentacaoestoquetipo
	 * @author Rafael Salvio
	 */
	public void makeMovimentacaoestoqueByExpedicao(List<Movimentacaoestoque> listaMovimentacaoestoque, Expedicaoproducao expedicaoproducao, 
			Expedicaoproducaoitem expedicaoproducaoitem, Material material, Double qtde, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setDtmovimentacao(expedicaoproducaoitem.getDtexpedicaoproducaoitem());
		movimentacaoestoque.setMaterial(material);
		movimentacaoestoque.setMovimentacaoestoquetipo(movimentacaoestoquetipo);
		movimentacaoestoque.setQtde(qtde);
		movimentacaoestoque.setLocalarmazenagem(expedicaoproducaoitem.getLocalarmazenagem());
		
		
		
		if(material.getProduto() != null && material.getProduto()){
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		} else if(material.getEpi() != null && material.getEpi()){
			movimentacaoestoque.setMaterialclasse(Materialclasse.EPI);
		} else {
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		}
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setExpedicaoproducao(expedicaoproducao);
		
		
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		listaMovimentacaoestoque.add(movimentacaoestoque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Expedicaoproducao> findForGerarNotaFiscalProduto(String whereIn){
		return expedicaoproducaoDAO.findForGerarNotaFiscalProduto(whereIn);
	}
	
	/**
	 * M�todo que verifica e retorna um cliente, caso exista somente um �nico na lista de expedi��es fornecida.
	 * Caso existam clientes diferentes, uma exception � lan�ada. 
	 * 
	 * @param lista
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public Cliente getClienteUnico(List<Expedicaoproducao> lista) throws Exception{
		Cliente cliente = null;
		Boolean achouPrimeiroCliente = Boolean.FALSE;
		for(Expedicaoproducao ep : lista){
			if(ep.getListaExpedicaoproducaoitem() == null || ep.getListaExpedicaoproducaoitem().isEmpty()){
				continue;	
			}else if(!achouPrimeiroCliente){
				cliente = ep.getListaExpedicaoproducaoitem().get(0).getCliente();
				achouPrimeiroCliente = Boolean.TRUE;
			}
			
			for(Expedicaoproducaoitem epi : ep.getListaExpedicaoproducaoitem()){
				if((cliente == null && epi.getCliente() == null)  || (cliente != null && cliente.equals(epi.getCliente())))
					continue;
				else
					throw new Exception("Somente � poss�vel gerar nota para expedi��es de um mesmo cliente.");
			}
		}
		
		return cliente;
	}

	public void createByProducaoagenda(Expedicaoproducao bean, Producaoagenda producaoagenda) {
		bean.setDtexpedicao(SinedDateUtils.timestampToDate(producaoagenda.getDtentrega()));
		bean.setProducaoagenda(producaoagenda);
		
		List<Expedicaoproducaoitem> listaExpedicaoproducaoitem = new ArrayList<Expedicaoproducaoitem>();
		Set<Producaoagendamaterial> listaProducaoagendamaterial = producaoagenda.getListaProducaoagendamaterial();
		
		for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
			Expedicaoproducaoitem expedicaoproducaoitem = new Expedicaoproducaoitem();
			expedicaoproducaoitem.setMaterial(producaoagendamaterial.getMaterial());
			expedicaoproducaoitem.setQtdeexpedicaoproducao(new Double(producaoagendamaterial.getQtde()));
			expedicaoproducaoitem.setDtexpedicaoproducaoitem(SinedDateUtils.timestampToDate(producaoagenda.getDtentrega()));
			expedicaoproducaoitem.setCliente(producaoagenda.getCliente());
			expedicaoproducaoitem.setContrato(producaoagenda.getContrato());
			
			listaExpedicaoproducaoitem.add(expedicaoproducaoitem);
		}
		
		bean.setListaExpedicaoproducaoitem(listaExpedicaoproducaoitem);
	}
}
