package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Versao;
import br.com.linkcom.sined.geral.bean.Versaonovidade;
import br.com.linkcom.sined.geral.dao.VersaonovidadeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VersaonovidadeService extends GenericService<Versaonovidade> {

	private VersaonovidadeDAO versaonovidadeDAO;
	
	public void setVersaonovidadeDAO(VersaonovidadeDAO versaonovidadeDAO) {
		this.versaonovidadeDAO = versaonovidadeDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param versao
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public List<Versaonovidade> getNovidadesVersao(Versao versao){
		return versaonovidadeDAO.getNovidadesVersao(versao);
	}
	
	/* singleton */
	private static VersaonovidadeService instance;
	public static VersaonovidadeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VersaonovidadeService.class);
		}
		return instance;
	}
}
