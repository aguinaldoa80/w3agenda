package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessohistorico;
import br.com.linkcom.sined.geral.bean.Documentoprocessosituacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.DocumentoprocessoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoprocessoService extends GenericService<Documentoprocesso>{
	
	private DocumentoprocessohistoricoService historicoService;
	private DocumentoprocessoDAO documentoprocessoDAO;
	private DocumentoprocessoprojetoService documentoprocessoprojetoService;
	private DocumentoprocessomaterialService documentoprocessomaterialService;
	
	public void setHistoricoService(DocumentoprocessohistoricoService historicoService) {
		this.historicoService = historicoService;
	}
	public void setDocumentoprocessoDAO(DocumentoprocessoDAO documentoprocessoDAO) {
		this.documentoprocessoDAO = documentoprocessoDAO;
	}	
	public void setDocumentoprocessoprojetoService(	DocumentoprocessoprojetoService documentoprocessoprojetoService) {
		this.documentoprocessoprojetoService = documentoprocessoprojetoService;
	}
	public void setDocumentoprocessomaterialService(DocumentoprocessomaterialService documentoprocessomaterialService) {
		this.documentoprocessomaterialService = documentoprocessomaterialService;
	}
	
	@Override
	public void saveOrUpdate(Documentoprocesso bean) {
		if(bean.getDtdocumento()==null && (bean.getDocumentoprocessosituacao().getCddocumentoprocessosituacao().
				equals(Documentoprocessosituacao.DISPONIVEL.getCddocumentoprocessosituacao()))){
			bean.setDtdocumento(new Timestamp(System.currentTimeMillis()));
		}
		
		Boolean revisao = bean.getRevisao();
		Documentoprocesso documento=null;
		if(bean.getCddocumentoprocesso()!=null){
			documento =loadSituacao(bean);		
		}
		super.saveOrUpdate(bean);
		documentoprocessoprojetoService.saveOrUpdateListaDocumentoprocessoprojeto(bean);
		documentoprocessomaterialService.saveOrUpdateListaDocumentoprocessomaterial(bean);

		List<Documentoprocessohistorico> listahistorico = bean.getListahistorico();
		if (listahistorico == null) {
			listahistorico = new ArrayList<Documentoprocessohistorico>();
		}
		
		
		if ((revisao == null || !revisao) && 
			(documento== null  || 
					!documento.getDocumentoprocessosituacao().getCddocumentoprocessosituacao().
					equals(bean.getDocumentoprocessosituacao().getCddocumentoprocessosituacao()))) {
			Documentoprocessohistorico dh = new Documentoprocessohistorico();
			dh.setDocumentoprocessosituacao(bean.getDocumentoprocessosituacao());			
			listahistorico.add(dh);
		}
		
		//Salvando hist�rico (necess�rio para salvar o arquivo corretamente)

		for(Documentoprocessohistorico historico : listahistorico){			
			if(historico.getCddocumentoprocessohistorico()==null){
				historico.setDocumentoprocesso(bean);				
				historicoService.saveOrUpdate(historico);
			}	
		}
		
	}
	
	/**
	 * Retorna o documento com os dados do arquivo e da lista de
	 * hist�rico carregados
	 * @param documento
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO#loadArquivo(Documentoprocesso)
	 * @author C�ntia Nogueira
	 */
	public Documentoprocesso loadArquivo(Documentoprocesso documento){
		return documentoprocessoDAO.loadArquivo(documento);
	}

	/**
	 * Retorna todos os documentos do usu�rio logado com arquivos
	 * carregados
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO#findAllwithArquivos()
	 */
	public List<Documentoprocesso> findAllwithArquivos(){
		return documentoprocessoDAO.findAllwithArquivos();
		 				
	}	

	/**
	 * Gera o relat�rio de documento/processo
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO#findForRelatorio(DocumentoprocessoFiltro)
	 */
	public IReport gerarRelatorio(DocumentoprocessoFiltro filtro) {
		Report report = new Report("/servicointerno/documentoprocesso");
		
		List<Documentoprocesso> documentos = documentoprocessoDAO.findForRelatorio(filtro);
	    
		if(documentos != null && documentos.size() > 0)
			report.setDataSource(documentos);
		
		
		return report;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO#findByResponsavel
	 * @param usuario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documentoprocesso> findByResponsavel(Usuario usuario) {
		return documentoprocessoDAO.findByResponsavel(usuario);
	}
	
	/**
	 * Retorna com a situacao do documento
	 * @param bean
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessoDAO#loadSituacao(Documentoprocesso)
	 */
	public Documentoprocesso loadSituacao(Documentoprocesso bean) {
		return documentoprocessoDAO.loadSituacao(bean);
	
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param bean
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean isDtVencimentoAtrasada(String whereIn){
		return documentoprocessoDAO.isDtVencimentoAtrasada(whereIn);
	}
	
	public List<Documentoprocesso> loadForRenovar(String whereIn){
		return documentoprocessoDAO.loadForRenovar(whereIn);
	}
	
	
	/**
	 * M�todo que atualiza o vencimento de uma lista de documentoprocessos.
	 * @param listaDocumentoprocesso
	 * @author Rafael Salvio
	 */
	public void saveDocumentoprocessoRenovado(List<Documentoprocesso> listaDocumentoprocesso){
		if(listaDocumentoprocesso == null || listaDocumentoprocesso.isEmpty()){
			throw new SinedException("Par�metro inv�lido!");
		}
		
		for(Documentoprocesso documentoprocesso : listaDocumentoprocesso){
			documentoprocessoDAO.saveDocumentoprocessoRenovado(documentoprocesso);
		}
	}
}
