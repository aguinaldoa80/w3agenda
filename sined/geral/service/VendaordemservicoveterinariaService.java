package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendaordemservicoveterinaria;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaordemservicoveterinariaService extends GenericService<Vendaordemservicoveterinaria> {

	/**
	 * M�todo que vincula uma venda com as respectivas ordens de servi�o veterin�ria
	 * @param whereInOSV
	 * @param venda
	 * @author Danilo Guimar�es
	 */
	public void saveVendaordemservicoveterinaria(String whereInOSV, Venda venda){
		for(String id : whereInOSV.split(",")){
			Ordemservicoveterinaria ordemservicoveterinaria = new Ordemservicoveterinaria(Integer.parseInt(id));
			Vendaordemservicoveterinaria vendaordemservicoveterinaria = new Vendaordemservicoveterinaria();
			vendaordemservicoveterinaria.setOrdemservicoveterinaria(ordemservicoveterinaria);
			vendaordemservicoveterinaria.setVenda(venda);
			saveOrUpdate(vendaordemservicoveterinaria);
		}
	}
	
}
