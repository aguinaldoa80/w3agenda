package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.dao.EmpresamodelocontratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class EmpresamodelocontratoService extends GenericService<Empresamodelocontrato> {

	private EmpresamodelocontratoDAO empresamodelocontratoDAO;
	private ContratoService contratoService;
	private ContratojuridicoService contratojuridicoService;
	private EmpresaService empresaService;
	
	private static EmpresamodelocontratoService instance;
	
	public void setEmpresamodelocontratoDAO(EmpresamodelocontratoDAO empresamodelocontratoDAO) {
		this.empresamodelocontratoDAO = empresamodelocontratoDAO;
	}
	
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	
	public void setContratojuridicoService(ContratojuridicoService contratojuridicoService) {
		this.contratojuridicoService = contratojuridicoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public static EmpresamodelocontratoService getInstance() {
		
		if (instance == null){
			instance = Neo.getObject(EmpresamodelocontratoService.class);
		}
		
		return instance;
	}
	
	/**
	 * Busca uma lista de modelos de contratos associados a um contrato.
	 * Primeiro ir� buscar os contratos associados aos Materiais/Servidos, caso
	 * n�o encontre nenhum ir� buscar os modelos associados � empresa.
	 * 
	 * @author Giovane Freitas
	 * @param contrato
	 * @return
	 */
	public List<Empresamodelocontrato> findByContrato(Contrato contrato) {
		
		List<Empresamodelocontrato> listaModelo = empresamodelocontratoDAO.findByContrato(contrato);
		
		if (listaModelo != null && !listaModelo.isEmpty())
			return listaModelo;
		
		Contrato contratoEmpresa = contratoService.load(contrato, "contrato.cdcontrato, contrato.empresa");
		
		Empresa empresa;
		if(contratoEmpresa != null && contratoEmpresa.getEmpresa() != null){
			empresa = contratoEmpresa.getEmpresa();
		}else{
			empresa = empresaService.loadPrincipal();
		}
		
		return empresamodelocontratoDAO.findByEmpresa(empresa);
	}

	/**
	 * Busca uma lista de modelos de contratos associados a um contrato.
	 * Primeiro ir� buscar os contratos associados aos Materiais/Servidos, caso
	 * n�o encontre nenhum ir� buscar os modelos associados � empresa.
	 * 
	 * @author Giovane Freitas
	 * @param contrato
	 * @return
	 */
	public List<Empresamodelocontrato> findByContratojuridico(Contratojuridico contratojuridico) {

		Contratojuridico contratoEmpresa = contratojuridicoService.load(contratojuridico, "contratojuridico.cdcontratojuridico, contratojuridico.empresa");
		
		Empresa empresa;
		if(contratoEmpresa != null && contratoEmpresa.getEmpresa() != null){
			empresa = contratoEmpresa.getEmpresa();
		}else{
			empresa = empresaService.loadPrincipal();
		}
		
		return empresamodelocontratoDAO.findByEmpresa(empresa);
	}


	/**
	 * Busca uma lista de modelos de contratos associados a uma oportunidade.
	 * Primeiro ir� buscar os contratos associados aos Materiais/Servidos, caso
	 * n�o encontre nenhum ir� buscar os modelos associados � empresa.
	 * 
	 * @author Giovane Freitas
	 * @param contrato
	 * @return
	 */
	public  List<Empresamodelocontrato> findByProposta(Proposta proposta) {
		return empresamodelocontratoDAO.findByProposta(proposta);
	}
}
