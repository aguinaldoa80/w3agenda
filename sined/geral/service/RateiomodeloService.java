package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Rateiomodelo;
import br.com.linkcom.sined.geral.dao.RateiomodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RateiomodeloService extends GenericService<Rateiomodelo>{

	private RateiomodeloDAO rateiomodeloDAO;
	
	public void setRateiomodeloDAO(RateiomodeloDAO rateiomodeloDAO) {
		this.rateiomodeloDAO = rateiomodeloDAO;
	}
	
	public Rateiomodelo loadForRateio(Rateiomodelo rateiomodelo){
		return rateiomodeloDAO.loadForRateio(rateiomodelo);
	}
	
	public Rateiomodelo findByNome(String nome){
		return rateiomodeloDAO.findByNome(nome);
	}
}
