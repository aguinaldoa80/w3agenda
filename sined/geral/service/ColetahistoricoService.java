package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.dao.ColetahistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColetahistoricoService extends GenericService<Coletahistorico> {

	private ColetahistoricoDAO coletahistoricoDAO;
	
	public void setColetahistoricoDAO(ColetahistoricoDAO coletahistoricoDAO) {
		this.coletahistoricoDAO = coletahistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Coletahistorico> findByColeta(Coleta coleta) {
		return coletahistoricoDAO.findByColeta(coleta);
	}
	
	public void gerarHistoricoNotaRetorno(Notafiscalproduto nota, Coleta coleta){
		Coletahistorico ch = new Coletahistorico();
		ch.setColeta(coleta);
		ch.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		ch.setDtaltera(new Timestamp(System.currentTimeMillis()));
		ch.setObservacao("Nota Fiscal de retorno gerada: " + SinedUtil.makeLinkNota(nota));
		ch.setAcao(Coletaacao.NOTA_EMITIDA);
		saveOrUpdate(ch);
	}
}
