package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.geral.dao.PneuqualificacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.pneuqualificacao.PneuQualificacaoW3producaoRESTModel;

public class PneuqualificacaoService extends GenericService<Pneuqualificacao>{
	
	private PneuqualificacaoDAO pneuqualificacaoDAO;
	private static PneuqualificacaoService instance;
	
	public static PneuqualificacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneuqualificacaoService.class);
		}
		return instance;
	}
	
	public void setPneuqualificacaoDAO(PneuqualificacaoDAO pneuqualificacaoDAO) {
		this.pneuqualificacaoDAO = pneuqualificacaoDAO;
	}
	
	public List<PneuQualificacaoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PneuQualificacaoW3producaoRESTModel> lista = new ArrayList<PneuQualificacaoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneuqualificacao pneuqualificacao : pneuqualificacaoDAO.findForW3Producao(whereIn))
				lista.add(new PneuQualificacaoW3producaoRESTModel(pneuqualificacao));
		}
		
		return lista;
	}
	
	public List<Pneuqualificacao> findAutocomplete(String q) {
		return pneuqualificacaoDAO.findAutocomplete(q);
	}
}
