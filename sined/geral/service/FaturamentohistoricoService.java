package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Faturamentohistorico;
import br.com.linkcom.sined.geral.bean.Faturamentohistoricomaterial;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NotaFiscalTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipodesconto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.FaturamentohistoricoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FaturamentohistoricoService extends GenericService<Faturamentohistorico> {

	private FaturamentohistoricoDAO faturamentohistoricoDAO;
	private ContratoparcelaService contratoparcelaService;
	private ContratodescontoService contratodescontoService;
	private ContratoService contratoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ParametrogeralService parametrogeralService;
	private EmpresaService empresaService;
	private NotaDocumentoService notaDocumentoService;
	private DocumentohistoricoService documentohistoricoService;
	private DocumentoService documentoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private VendaService vendaService;
	private ExpedicaoitemService expedicaoitemService;
	private ConfiguracaonfeService configuracaonfeService;
	
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setExpedicaoitemService(
			ExpedicaoitemService expedicaoitemService) {
		this.expedicaoitemService = expedicaoitemService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setFaturamentohistoricoDAO(
			FaturamentohistoricoDAO faturamentohistoricoDAO) {
		this.faturamentohistoricoDAO = faturamentohistoricoDAO;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setContratodescontoService(
			ContratodescontoService contratodescontoService) {
		this.contratodescontoService = contratodescontoService;
	}
	public void setContratoparcelaService(
			ContratoparcelaService contratoparcelaService) {
		this.contratoparcelaService = contratoparcelaService;
	}
	
	/**
	 * M�todo que realiza a cria��o do registro de hist�rico do faturamento do contrato.
	 *
	 * @param contrato
	 * @param documento
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public void saveHistoricoFaturamento(Contrato contrato, Documento documento) {
		Faturamentohistorico faturamentohistorico = new Faturamentohistorico();
		faturamentohistorico.setContrato(contrato);
		faturamentohistorico.setDocumento(documento);
		faturamentohistorico.setMes(contrato.getMes());
		faturamentohistorico.setAno(contrato.getAno());
		
		// DESCONTO NF DE PRODUTO
		Money totalDescontoProduto = contratoService.valorDescontoAba(contrato, Tipodesconto.NF_PRODUTO);
		faturamentohistorico.setDescontoproduto(totalDescontoProduto);
		
		// DESCONTO NF DE SERVI�O
		Map<NotaFiscalTipoEnum, Money> mapDescontos = contratodescontoService.getDescontosContrato(contrato.getDtproximovencimento(), contrato.getListaContratodesconto());
		Money abatimentoValorBruto = new Money();
		
		Set<Entry<NotaFiscalTipoEnum, Money>> entrySet = mapDescontos.entrySet();
		for (Entry<NotaFiscalTipoEnum, Money> entry : entrySet) {
			NotaFiscalTipoEnum tiponota = entry.getKey();
			Money valor = entry.getValue();
			
			if (tiponota.equals(NotaFiscalTipoEnum.ABATIMENTO)){
				abatimentoValorBruto = abatimentoValorBruto.add(valor);
			} else if (tiponota.equals(NotaFiscalTipoEnum.DEDUCAO)){
				faturamentohistorico.setDeducao(valor);
			} else if (tiponota.equals(NotaFiscalTipoEnum.DESCONTO_CONDICIONADO)){
				faturamentohistorico.setDescontocondicionado(valor);
			} else if (tiponota.equals(NotaFiscalTipoEnum.DESCONTO_INCONDICIONADO)){
				faturamentohistorico.setDescontoincondicionado(valor);
			} else if(tiponota.equals(NotaFiscalTipoEnum.OUTRAS_RETENCOES)){
				faturamentohistorico.setOutrasretencoes(valor);
			}
		}
		
		contrato.setListaContratoparcela(contratoparcelaService.findByContrato(contrato));
		List<Contratoparcela> listaContratoparcela = contrato.getListaContratoparcela();
		
		Integer numeroParcela = null;
		Integer tamParcela = null;
		if(listaContratoparcela != null && listaContratoparcela.size() > 0){
			tamParcela = listaContratoparcela.size();
			
			Contratoparcela contratoparcela = contratoService.getParcelaAtual(listaContratoparcela);
			numeroParcela = listaContratoparcela.indexOf(contratoparcela);
			numeroParcela++;
		}
		
		faturamentohistorico.setNumeroparcela(numeroParcela);
		faturamentohistorico.setTotalparcelas(tamParcela);
		
		List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
		List<Contratomaterial> listaProdutoServico = new ArrayList<Contratomaterial>();
		
		if(listaContratomaterial != null && listaContratomaterial.size() > 0 && (contrato.getIgnoravalormaterial() == null || !contrato.getIgnoravalormaterial())){
			for (Contratomaterial item : listaContratomaterial) {
				if(SinedDateUtils.afterOrEqualsIgnoreHour(contrato.getDtproximovencimento(), item.getDtinicio()) && 
						(item.getDtfim() == null || SinedDateUtils.beforeOrEqualIgnoreHour(contrato.getDtproximovencimento(), item.getDtfim()))){
					listaProdutoServico.add(item);
				}
			}
		}
		
		if(listaProdutoServico != null && listaProdutoServico.size() > 0){
			Set<Faturamentohistoricomaterial> listaFaturamentohistoricomaterial = new ListSet<Faturamentohistoricomaterial>(Faturamentohistoricomaterial.class);
			Faturamentohistoricomaterial faturamentohistoricomaterial;
			
			for (Contratomaterial cm : listaProdutoServico) {
				// CASO O VALOR UNIT�RIO DO SERVI�O/PRODUTO SEJA ZERO ELE N�O INCLUI O ITEM NA NOTA
				if(cm.getValorfechadoOrValorunitario() == null || cm.getValorfechadoOrValorunitario().equals(0d)){
					continue;
				}
				
				faturamentohistoricomaterial = new Faturamentohistoricomaterial();
				faturamentohistoricomaterial.setMaterial(cm.getServico());
				
				Double qtde = cm.getQtde();
				if(cm.getPeriodocobranca() != null){
					qtde = qtde * cm.getPeriodocobranca();
				}
				
				if(tamParcela != null && tamParcela > 0){
					double tamParcelaDbl = new Double(tamParcela).doubleValue();
					
					faturamentohistoricomaterial.setQtde(qtde / tamParcelaDbl);
					faturamentohistoricomaterial.setValorunitario(new Money(cm.getValorfechadoOrValorunitario()));
					faturamentohistoricomaterial.setValordesconto(new Money(cm.getValordesconto().getValue().doubleValue() / tamParcelaDbl));
				} else {
					faturamentohistoricomaterial.setQtde(qtde);
					faturamentohistoricomaterial.setValorunitario(new Money(cm.getValorfechadoOrValorunitario()));
					faturamentohistoricomaterial.setValordesconto(cm.getValordesconto());
				}
				
				listaFaturamentohistoricomaterial.add(faturamentohistoricomaterial);
			}
			
			if(abatimentoValorBruto != null && abatimentoValorBruto.getValue().doubleValue() > 0){
				int numItens = listaFaturamentohistoricomaterial.size();
				double descontoItem = abatimentoValorBruto.getValue().doubleValue() / new Double(numItens);
				
				for (Faturamentohistoricomaterial it : listaFaturamentohistoricomaterial) {
					it.setValorunitario(it.getValorunitario().subtract(new Money(descontoItem)));
				}
			}
			
			faturamentohistorico.setListaFaturamentohistoricomaterial(listaFaturamentohistoricomaterial);			
		} else {
			if(tamParcela != null && tamParcela > 0) {
				Contratoparcela parcela = contratoService.getParcelaAtual(contrato.getListaContratoparcela());
				faturamentohistorico.setValor(parcela.getValor());
				if(parcela.getDesconto() != null && parcela.getDesconto().getValue().doubleValue() > 0){
					faturamentohistorico.setValor(faturamentohistorico.getValor().subtract(parcela.getDesconto()));
				}
			} else {
				faturamentohistorico.setValor(contrato.getValorQtde().subtract(abatimentoValorBruto));
			}
		}
		
		this.saveOrUpdate(faturamentohistorico);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveHistoricoDocumento(Documento documento) {
		return faturamentohistoricoDAO.haveHistoricoDocumento(documento);
	}
	
	/**
	 * Cria as NF de Servi�o a partir da conta a receber.
	 *
	 * @param request
	 * @param whereIn
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public void realizarFaturamentoServicoDocumento(WebRequestContext request, String whereIn, boolean notaEmEspera, boolean considerarValorDocumento) {
		List<Faturamentohistorico> listaFaturamentohistorico = this.findForFaturamentoByDocumento(whereIn);
		
		List<NotaFiscalServico> listaNotaServico = new ArrayList<NotaFiscalServico>();
		
		boolean forcarDescricaoContrato = "TRUE".equals(parametrogeralService.getValorPorNome("FaturamentoContratoForcarDescricao"));
		
		for (Faturamentohistorico faturamentohistorico : listaFaturamentohistorico) {
			try{
				Contrato contrato = faturamentohistorico.getContrato();
				Documento documento = faturamentohistorico.getDocumento();
				Set<Faturamentohistoricomaterial> listaFaturamentohistoricomaterial = faturamentohistorico.getListaFaturamentohistoricomaterial();
				
				NotaFiscalServico nota = new NotaFiscalServico();
				nota.setCliente(contrato.getCliente());
				nota.setDocumento(documento);
				
				if(StringUtils.isNotBlank(contrato.getAnotacaocorpo())){
					nota.setInfocomplementar(contrato.getAnotacaocorpo());
				}
				
				if(contrato.getGrupotributacao()!=null && StringUtils.isNotBlank(contrato.getGrupotributacao().getInfoadicionalfisco())){
					String infoComplementar = nota.getInfocomplementar()!=null?nota.getInfocomplementar():"";
					nota.setInfocomplementar(infoComplementar+" "+contrato.getGrupotributacao().getInfoadicionalfisco());
				}
				
				if(contrato.getGrupotributacao()!=null && StringUtils.isNotBlank(contrato.getGrupotributacao().getInfoadicionalcontrib())){
					nota.setDadosAdicionais(contrato.getGrupotributacao().getInfoadicionalcontrib());
				}
				
				if(contrato != null && contrato.getCdcontrato() != null){
					Contrato contratoRateio = contratoService.loadContratoWithRateioProjeto(contrato);
					if(contratoRateio != null && contratoRateio.getRateio() != null && contratoRateio.getRateio().getListaRateioitem() != null){
						for (Rateioitem ri : contratoRateio.getRateio().getListaRateioitem()) {
							if(ri.getProjeto() != null){
								nota.setProjeto(ri.getProjeto());
							}
						}
					}
				}
				
				Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(contrato.getEmpresa());
				notaFiscalServicoService.preencheValoresPadroesEmpresa(contrato.getEmpresa(), nota);
				
				if(contrato.getEndereco() != null){
					nota.setEnderecoCliente(contrato.getEndereco());
				} else {
					if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
						nota.setEnderecoCliente(contrato.getCliente().getEndereco());
					}
				}
				
				if(nota.getEnderecoCliente() == null){
					throw new SinedException("Nenhum endere�o cadastrado para o cliente " + contrato.getCliente().getNome() + ".");
				}
				
				Integer numeroParcela = faturamentohistorico.getNumeroparcela() != null ? faturamentohistorico.getNumeroparcela() : 0;
				Integer tamParcela = faturamentohistorico.getTotalparcelas() != null ? faturamentohistorico.getTotalparcelas() : 1;
				
				nota.setDeducao(faturamentohistorico.getDeducao());
				nota.setDescontocondicionado(faturamentohistorico.getDescontocondicionado());
				nota.setDescontoincondicionado(faturamentohistorico.getDescontoincondicionado());
				nota.setOutrasretencoes(faturamentohistorico.getOutrasretencoes());
				
				NotaFiscalServicoItem item;
				List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
				
				
				if(listaFaturamentohistoricomaterial != null && listaFaturamentohistoricomaterial.size() > 0 && !considerarValorDocumento){
					boolean haveServico = false;
					
					for (Faturamentohistoricomaterial faturamentohistoricomaterial : listaFaturamentohistoricomaterial) {
						if(faturamentohistoricomaterial.getMaterial() != null && 
								faturamentohistoricomaterial.getMaterial().getServico() != null &&
								faturamentohistoricomaterial.getMaterial().getServico() && 
								((faturamentohistoricomaterial.getMaterial().getTributacaoestadual() == null && !isMunicipioBrasilia) ||
								 (!faturamentohistoricomaterial.getMaterial().getTributacaoestadual() && !isMunicipioBrasilia))){
							haveServico = true;
						} else {
							continue;
						}
						
						item = new NotaFiscalServicoItem();
						
						if(contrato.getAnotacaocorpo() != null && !contrato.getAnotacaocorpo().trim().equals("")){
							item.setDescricao(contrato.getAnotacaocorpo());
							if (numeroParcela > 0)
								item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + tamParcela);
						}else if(forcarDescricaoContrato){
							item.setDescricao(contrato.getDescricao());
							if (numeroParcela > 0)
								item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + tamParcela);
						} else {
							item.setDescricao(faturamentohistoricomaterial.getMaterial().getNome());
							if (numeroParcela > 0)
								item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + tamParcela);
						}
						
						if (faturamentohistoricomaterial.getMaterial() != null) {
							item.setMaterial(faturamentohistoricomaterial.getMaterial());
						}
						
						if (faturamentohistorico.getMes() != null && faturamentohistorico.getAno() != null){
							item.setDescricao(item.getDescricao() + " " + faturamentohistorico.getMes().getNome() + "/" + faturamentohistorico.getAno());
						}
						
						item.setQtde(faturamentohistoricomaterial.getQtde());
						item.setPrecoUnitario(faturamentohistoricomaterial.getValorunitario().getValue().doubleValue());
						item.setDesconto(faturamentohistoricomaterial.getValordesconto());
						
						listaItens.add(item);
					}
					
					if(!haveServico){
						throw new SinedException("N�o existe nenhum servi�o para este contrato.");
					}
				} else {
					item = new NotaFiscalServicoItem();
					item.setQtde(1.0);
					
					if(considerarValorDocumento){
						item.setPrecoUnitario(documento.getValorDouble());
					} else {
						item.setPrecoUnitario(faturamentohistorico.getValor().getValue().doubleValue());
					}
					
					if(contrato.getAnotacaocorpo() != null && !contrato.getAnotacaocorpo().trim().equals("")){
						item.setDescricao(contrato.getAnotacaocorpo());
						if (numeroParcela > 0)
							item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + tamParcela);
					} else {
						item.setDescricao(contrato.getDescricao());
						if (numeroParcela > 0)
							item.setDescricao(item.getDescricao() + " - Parcela " + numeroParcela + "/" + tamParcela);
					}
					
					if(contrato.getFrequencia() != null){
						StringBuilder descricao = new StringBuilder();
						if(contrato.getFrequencia().getCdfrequencia().equals(Frequencia.MENSAL)){
							
							if(faturamentohistorico.getMes() != null && faturamentohistorico.getAno() != null){
								descricao.append(" Referente a: ").append(faturamentohistorico.getMes().getNome() + " / " + faturamentohistorico.getAno());
							} else if(faturamentohistorico.getMes() != null){
								descricao.append(" Referente a: ").append(faturamentohistorico.getMes().getNome());
							} else if (faturamentohistorico.getAno() != null){
								descricao.append(" Referente a: ").append(faturamentohistorico.getAno());
							}
						}
						
						item.setDescricao(item.getDescricao() + descricao.toString());
					}
					
					listaItens.add(item);
				}
				
				nota.setListaItens(listaItens);
				
				Money baseCalculo = nota.getValorTotalServicos();
				if(nota.getDeducao() != null) baseCalculo = baseCalculo.subtract(nota.getDeducao());
				if(nota.getDescontoincondicionado() != null) baseCalculo = baseCalculo.subtract(nota.getDescontoincondicionado());
				
				nota.setBasecalculo(baseCalculo);
				nota.setBasecalculoir(baseCalculo);
				nota.setBasecalculoiss(baseCalculo);
				nota.setBasecalculopis(baseCalculo);
				nota.setBasecalculocofins(baseCalculo);
				nota.setBasecalculocsll(baseCalculo);
				nota.setBasecalculoinss(baseCalculo);
				nota.setBasecalculoicms(baseCalculo);
				
				nota.setDtVencimento(documento.getDtvencimento());
				if(notaEmEspera){
					nota.setNotaStatus(NotaStatus.EM_ESPERA);
				} else {
					nota.setNotaStatus(NotaStatus.EMITIDA);
				}
				nota.setDtEmissao(SinedDateUtils.currentDate());
				nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
				nota.setFaturamentolote(contrato.getFaturamentolote());
				nota.setConta(contrato.getConta());
				nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
				nota.setFrequencia(contrato.getFrequencia());
				nota.setGrupotributacao(contrato.getGrupotributacao());
				
				if(contrato.getItemlistaservicoBean() != null) nota.setItemlistaservicoBean(contrato.getItemlistaservicoBean());
				if(contrato.getCodigocnaeBean() != null) nota.setCodigocnaeBean(contrato.getCodigocnaeBean());
				if(contrato.getCodigotributacao() != null) nota.setCodigotributacao(contrato.getCodigotributacao());
				
				listaNotaServico.add(nota);
			} catch (Exception e) {
				e.printStackTrace();
				
				if(request != null)
					request.addError("N�o foi poss�vel gerar a NF de servi�o da conta a receber " + faturamentohistorico.getDocumento().getCddocumento() + ": " + e.getMessage());
			}
		}
		
		
		
		for (NotaFiscalServico nfs : listaNotaServico) {
			Documento documento = nfs.getDocumento();
			
			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
			if(!haveVotorantim){
				Integer proximoNumero = empresaService.carregaProxNumNF(nfs.getEmpresa());
				if(proximoNumero == null){
					proximoNumero = 1;
				}
				nfs.setNumero(proximoNumero.toString());
				proximoNumero++;
	
				empresaService.updateProximoNumNF(nfs.getEmpresa(), proximoNumero);
			}
			
			List<NotaHistorico> listaHistorico = new ArrayList<NotaHistorico>();
			NotaHistorico historico = new NotaHistorico();
			
			historico.setNotaStatus(NotaStatus.EMITIDA);
			if(NeoWeb.getUser() != null){
				historico.setCdusuarioaltera(((Usuario)NeoWeb.getUser()).getCdpessoa());
			}
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao("Vinculado a conta receber <a href=\"javascript:visualizarContaReceber(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a>");
			
			listaHistorico.add(historico);
			nfs.setListaNotaHistorico(listaHistorico);
			
			//Calcula a tributa��o com base no grupo de tributa��o do contrato, busca do primeiro contrato
			//pois � feita a valida��o para que todos os contratos possuem o mesmo grupo
			if (nfs.getListaContrato() != null && nfs.getListaContrato().size() > 0) {
				Contrato contrato = nfs.getListaContrato().get(0);
				contratoService.calculaTributacaoNotaFiscalServico(contrato, contrato.getGrupotributacao(), nfs);
			}
			
			notaFiscalServicoService.saveOrUpdate(nfs);
			
			documentoService.updateNumerosNFDocumentoFaturamento(documento, nfs.getNumero(), NotaTipo.NOTA_FISCAL_SERVICO);
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setDocumento(documento);
			documentohistorico.setDocumentoacao(Documentoacao.ASSOCIADA);
			documentohistorico.setObservacao("Vinculado a nota fiscal de servi�o <a href=\"javascript:visualizarNotaFiscalServico(" + nfs.getCdNota() + ");\">" + nfs.getCdNota() + "</a>");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setDocumento(documento);
			notaDocumento.setNota(nfs);
			if(notaEmEspera){
				notaDocumento.setFromwebservice(true);
			}
			notaDocumentoService.saveOrUpdate(notaDocumento);
			
			if(request != null)
				request.addMessage("Criada a NF de servi�o da conta " + documento.getCddocumento());
		}
		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	private List<Faturamentohistorico> findForFaturamentoByDocumento(String whereIn) {
		return faturamentohistoricoDAO.findForFaturamentoByDocumento(whereIn);
	}
	
	/**
	 * Realiza o faturamento para cria��o de NF de Produto.
	 *
	 * @param request
	 * @param listaDocumentoContrato
	 * @param listaDocumentoVenda
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public void realizarFaturamentoProdutoDocumento(WebRequestContext request, List<Documento> listaDocumentoContrato, List<Documento> listaDocumentoVenda) {
		if(listaDocumentoVenda != null && listaDocumentoVenda.size() > 0)
			this.realizarFaturamentoProdutoVenda(request, listaDocumentoVenda);
		
		if(listaDocumentoContrato != null && listaDocumentoContrato.size() > 0)
			this.realizarFaturamentoProdutoContrato(request, listaDocumentoContrato);
	}
	
	private void realizarFaturamentoProdutoContrato(WebRequestContext request, List<Documento> listaDocumentoContrato) {
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumentoContrato, "cddocumento", ",");
		List<Faturamentohistorico> listaFaturamentohistorico = this.findForFaturamentoByDocumento(whereIn);
		
		List<Notafiscalproduto> listaNotaProduto = new ArrayList<Notafiscalproduto>();
		
		for (Faturamentohistorico faturamentohistorico : listaFaturamentohistorico) {
			try {
				Contrato contrato = faturamentohistorico.getContrato();			
				Set<Faturamentohistoricomaterial> listaFaturamentohistoricomaterial = faturamentohistorico.getListaFaturamentohistoricomaterial();
				List<Faturamentohistoricomaterial> listaProduto = new ArrayList<Faturamentohistoricomaterial>();
				boolean haveProduto = false;
				Boolean isMunicipioBrasilia = empresaService.isMunicipioBrasilia(contrato.getEmpresa());
				
				if(listaFaturamentohistoricomaterial != null && listaFaturamentohistoricomaterial.size() > 0){
					for (Faturamentohistoricomaterial faturamentohistoricomaterial : listaFaturamentohistoricomaterial) {
						if(faturamentohistoricomaterial.getMaterial() != null && 
								faturamentohistoricomaterial.getMaterial().getServico() != null &&
								faturamentohistoricomaterial.getMaterial().getServico() && 
								((faturamentohistoricomaterial.getMaterial().getTributacaoestadual() == null && !isMunicipioBrasilia) ||
								 (!faturamentohistoricomaterial.getMaterial().getTributacaoestadual() && !isMunicipioBrasilia))){
							continue;
						} else {
							listaProduto.add(faturamentohistoricomaterial);
							haveProduto = true;
						}
					}
				}
				
				if(!haveProduto){
					throw new SinedException("N�o existe nenhum produto para este contrato.");
				}
				
				
				
				Notafiscalproduto nota = new Notafiscalproduto();
				nota.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
				nota.setCliente(contrato.getCliente());
				nota.setEmpresa(contrato.getEmpresa());
				nota.setDocumento(faturamentohistorico.getDocumento());
				
				if(contrato.getEndereco() != null){
					nota.setEnderecoCliente(contrato.getEndereco());
				} else {
					if(contrato.getCliente() != null && contrato.getCliente().getListaEndereco() != null){
						nota.setEnderecoCliente(contrato.getCliente().getEndereco());
					}
				}
				
				if(nota.getEnderecoCliente() == null){
					throw new SinedException("Nenhum endere�o cadastrado para o cliente " + contrato.getCliente().getNome() + ".");
				}
				
				Money totalDesconto = faturamentohistorico.getDescontoproduto();
				Money itemDesconto = totalDesconto.getValue().doubleValue() > 0 ? totalDesconto.divide(new Money(listaProduto.size())) : new Money();
				Money valorTotalProdutos = new Money(0.0);
				Money valorTotalDesconto = new Money(0.0);
				
				List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
				for (Faturamentohistoricomaterial faturamentohistoricomaterial : listaProduto) {
					Notafiscalprodutoitem item = new Notafiscalprodutoitem();
					
					item.setMaterial(faturamentohistoricomaterial.getMaterial());
					item.setUnidademedida(faturamentohistoricomaterial.getMaterial().getUnidademedida());
					item.setNcmcompleto(faturamentohistoricomaterial.getMaterial().getNcmcompleto());
					item.setNve(faturamentohistoricomaterial.getMaterial().getNve());
					item.setExtipi(faturamentohistoricomaterial.getMaterial().getExtipi());
					item.setNcmcapitulo(faturamentohistoricomaterial.getMaterial().getNcmcapitulo());
					item.setIncluirvalorprodutos(true);
					
					if(faturamentohistoricomaterial.getMaterial().getCodigobarras() != null && !"".equals(faturamentohistoricomaterial.getMaterial().getCodigobarras()) && SinedUtil.validaCEAN(faturamentohistoricomaterial.getMaterial().getCodigobarras()))
						item.setGtin(faturamentohistoricomaterial.getMaterial().getCodigobarras());
					
					item.setQtde(faturamentohistoricomaterial.getQtde());
					item.setValorunitario(faturamentohistoricomaterial.getValorunitario() != null ? faturamentohistoricomaterial.getValorunitario().getValue().doubleValue() : null);
					item.setValorbruto(faturamentohistoricomaterial.getValorunitario().multiply(new Money(item.getQtde())));
					
					if(faturamentohistoricomaterial.getValordesconto() != null && 
							faturamentohistoricomaterial.getValordesconto().getValue().doubleValue() > 0){
						item.setValorbruto(item.getValorbruto().subtract(faturamentohistoricomaterial.getValordesconto()));
					}
					
					item.setValordesconto(itemDesconto);
					
					valorTotalDesconto = valorTotalDesconto.add(itemDesconto);
					valorTotalProdutos = valorTotalProdutos.add(item.getValorbruto());
					
					listaItens.add(item);
				}
				
				nota.setListaItens(listaItens);
				
				nota.setValordesconto(valorTotalDesconto);
				nota.setValorprodutos(valorTotalProdutos);
				nota.setValor(valorTotalProdutos.subtract(valorTotalDesconto));
				nota.setDadosAdicionais(contrato.getAnotacaocorpo());
				if(org.apache.commons.lang.StringUtils.isNotEmpty(contrato.getAnotacaocorpo())){
					if(org.apache.commons.lang.StringUtils.isNotEmpty(nota.getInfoadicionalcontrib()) && 
							!nota.getInfoadicionalcontrib().contains(contrato.getAnotacaocorpo())){
						nota.setInfoadicionalcontrib(nota.getInfoadicionalcontrib()+ ". " + contrato.getAnotacaocorpo());
					}else if(org.apache.commons.lang.StringUtils.isEmpty(nota.getInfoadicionalcontrib())){
						nota.setInfoadicionalcontrib(contrato.getAnotacaocorpo());
					}
				}
				nota.setDtEmissao(SinedDateUtils.currentDate());
				nota.setFinalidadenfe(Finalidadenfe.NORMAL);
				nota.setTipooperacaonota(Tipooperacaonota.SAIDA);
				nota.setNotaStatus(NotaStatus.EMITIDA);
				nota.setNotaTipo(NotaTipo.NOTA_FISCAL_PRODUTO);
				nota.setGrupotributacao(contrato.getGrupotributacao());
				if(contrato.getEmpresa() != null && contrato.getEmpresa().getSerienfe() != null){
					nota.setSerienfe(contrato.getEmpresa().getSerienfe());
				}
				
				listaNotaProduto.add(nota);
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel gerar a NF de produto da conta a receber " + faturamentohistorico.getDocumento().getCddocumento() + ": " + e.getMessage());
			}
		}
		
		for (Notafiscalproduto nfp : listaNotaProduto) {
			Documento documento = nfp.getDocumento();
			
//			Integer proximoNumero = empresaService.carregaProxNumNFProduto(nfp.getEmpresa());
//			if(proximoNumero == null){
//				proximoNumero = 1;
//			}
//			nfp.setNumero(proximoNumero.toString());
//			proximoNumero++;
//
//			empresaService.updateProximoNumNFProduto(nfp.getEmpresa(), proximoNumero);
			
			List<NotaHistorico> listaHistorico = new ArrayList<NotaHistorico>();
			NotaHistorico historico = new NotaHistorico();
			
			historico.setNotaStatus(NotaStatus.EMITIDA);
			historico.setCdusuarioaltera(((Usuario)NeoWeb.getUser()).getCdpessoa());
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			historico.setObservacao("Vinculado a conta receber <a href=\"javascript:visualizarContaReceber(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a>");
			
			listaHistorico.add(historico);
			
			nfp.setListaNotaHistorico(listaHistorico);
			
			notafiscalprodutoService.saveOrUpdate(nfp);
			notafiscalprodutoService.updateNumeroNFProduto(nfp);
			
			documentoService.updateNumerosNFDocumentoFaturamento(documento, nfp.getNumero(), NotaTipo.NOTA_FISCAL_PRODUTO);
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setDocumento(documento);
			documentohistorico.setDocumentoacao(Documentoacao.ASSOCIADA);
			documentohistorico.setObservacao("Vinculado a nota fiscal de produto <a href=\"javascript:visualizarNotaFiscalProduto(" + nfp.getCdNota() + ");\">" + nfp.getCdNota() + "</a>");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setDocumento(documento);
			notaDocumento.setNota(nfp);
			notaDocumentoService.saveOrUpdate(notaDocumento);
			
			request.addMessage("Criada a NF de produto da conta " + documento.getCddocumento() + ".");
		}
	}
	
	/**
	 * Cria as NF de produtos das Contas a receber vinculadas a uma venda.
	 *
	 * @param request
	 * @param listaDocumento
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	private void realizarFaturamentoProdutoVenda(WebRequestContext request, List<Documento> listaDocumento) {
		String whereInDocumento = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
		List<Venda> listaVendasDocumento = vendaService.findByDocumento(whereInDocumento);
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaVendasDocumento, "cdvenda", ",");
		if(expedicaoitemService.haveFaturada(whereIn)){
			throw new SinedException("Quando � gerado o faturamento de expedi��o n�o � poss�vel gerar o faturamento normal.");
		}

		List<Venda> listaVendas = vendaService.findForCobranca(whereIn);
		notafiscalprodutoService.gerarFaturamentoVenda(request, listaVendas, false, false, false, true);
		vendaService.updateSituacaoVenda(whereIn, Vendasituacao.FATURADA);
		
		request.addMessage("Criada a NF de produto a partir da venda da(s) conta(s) " + whereInDocumento + ".");
	}

}
