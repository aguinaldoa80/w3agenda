package br.com.linkcom.sined.geral.service;

import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Sincronizacaotabelaw3producao;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaw3producao;
import br.com.linkcom.sined.geral.dao.Sincronizacaotabelaw3producaoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean.DownloadBeanW3producaoRESTWSBean;

public class Sincronizacaotabelaw3producaoService extends GenericService<Sincronizacaotabelaw3producao>{

	protected Sincronizacaotabelaw3producaoDAO sincronizacaotabelaw3producaoDAO;
	public void setSincronizacaotabelaw3producaoDAO(Sincronizacaotabelaw3producaoDAO sincronizacaotabelaw3producaoDAO) {
		this.sincronizacaotabelaw3producaoDAO = sincronizacaotabelaw3producaoDAO;
	}
	
	public HashMap<Tabelaw3producao, String> getMaptabela(DownloadBeanW3producaoRESTWSBean command, Boolean excluido) {
		HashMap<Tabelaw3producao, StringBuilder> mapTabelaAux = new HashMap<Tabelaw3producao, StringBuilder>();
		List<Sincronizacaotabelaw3producao> lista = this.findForW3producao(command, excluido);
		
		if(SinedUtil.isListNotEmpty(lista)){
			for(Sincronizacaotabelaw3producao bean : lista){
				if(bean.getTabelaw3producao() != null && bean.getTabelaid() != null){
					if(mapTabelaAux.get(bean.getTabelaw3producao()) == null){
						mapTabelaAux.put(bean.getTabelaw3producao(), new StringBuilder(bean.getTabelaid().toString()));
					} else {
						mapTabelaAux.put(bean.getTabelaw3producao(), mapTabelaAux.get(bean.getTabelaw3producao()).append(",").append(bean.getTabelaid().toString()));
					}
				}
			}
		}
		
		HashMap<Tabelaw3producao, String> mapTabela = new HashMap<Tabelaw3producao, String>();
		if(mapTabelaAux.size() > 0){
			for(Tabelaw3producao value : mapTabelaAux.keySet()){
				mapTabela.put(value, mapTabelaAux.get(value).toString());
			}
		}
		return mapTabela;
	}

	private List<Sincronizacaotabelaw3producao> findForW3producao(DownloadBeanW3producaoRESTWSBean command, Boolean excluido) {
		return sincronizacaotabelaw3producaoDAO.findForW3producao(command, excluido);
	}
}
