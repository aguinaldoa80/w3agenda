package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.FechamentofinanceiroDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FechamentofinanceiroService extends GenericService<Fechamentofinanceiro> {
	
	private FechamentofinanceiroDAO fechamentofinanceiroDAO;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private AgendamentoService agendamentoService;
	private ContratoService contratoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ContaService contaService;
	private EmpresaService empresaService;
	private ContatipoService contatipoService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setFechamentofinanceiroDAO(	FechamentofinanceiroDAO fechamentofinanceiroDAO) {
		this.fechamentofinanceiroDAO = fechamentofinanceiroDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContatipoService(ContatipoService contatipoService) {
		this.contatipoService = contatipoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}

	/* singleton */
	private static FechamentofinanceiroService instance;
	public static FechamentofinanceiroService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FechamentofinanceiroService.class);
		}
		return instance;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @return
	 * @param empresa 
	 * @param conta 
	 * @author Rafael Salvio
	 */
	public Date loadUltimaDatalimite(Empresa empresa, Conta conta){
		return fechamentofinanceiroDAO.loadUltimaDatalimiteByEmpresa(empresa, conta);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/07/2014
	 */
	public List<Fechamentofinanceiro> findByEmpresaConta(Empresa empresa, Conta conta){
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if(empresa != null) listaEmpresa.add(empresa);
		return this.findByEmpresaConta(listaEmpresa, conta);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param listaEmpresa
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/05/2017
	 */
	public List<Fechamentofinanceiro> findByEmpresaConta(List<Empresa> listaEmpresa, Conta conta){
		return fechamentofinanceiroDAO.findByEmpresaConta(listaEmpresa, conta);
	}
	
	/**
	 * Verifica se a data � v�lida
	 *
	 * @param listaFechamentofinanceiro
	 * @param data
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/07/2014
	 */
	public boolean isDataValida(List<Fechamentofinanceiro> listaFechamentofinanceiro, Date data) {
		boolean datavalida = true;
		if(listaFechamentofinanceiro != null && listaFechamentofinanceiro.size() > 0){
			for (Fechamentofinanceiro fechamentofinanceiro : listaFechamentofinanceiro) {
				if(SinedDateUtils.afterOrEqualsIgnoreHour(data, fechamentofinanceiro.getDatainicio()) && 
						SinedDateUtils.beforeOrEqualIgnoreHour(data, fechamentofinanceiro.getDatalimite())){
					datavalida = false;
					break;
				}
			}
		}
		return datavalida;
	}
	
	
	/**
	 * M�todo que verifica se o per�odo da data de vencimento da conta encontra-se fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaFechamento(Documento bean){
		Boolean retorno = Boolean.FALSE; 
		if(bean.getEmpresa() != null){
			if(!this.isDataValida(this.findByEmpresaConta(bean.getEmpresa(), null), bean.getDtvencimento())){
				retorno = Boolean.TRUE;
			}else if(bean.getCddocumento() == null && Boolean.TRUE.equals(bean.getFinanciamento()) && SinedUtil.isListNotEmpty(bean.getListaDocumento())){
				for(Documento beanFinanciamento : bean.getListaDocumento()){
					if(beanFinanciamento.getDtvencimento() != null && !this.isDataValida(this.findByEmpresaConta(bean.getEmpresa(), null), beanFinanciamento.getDtvencimento())){
						retorno = Boolean.TRUE;
						break;
					}
				}
			}
		}
		return retorno;
	}
	
	/**
	 * M�todo que verifica se o per�odo da data de sa�da da despesa de viagem encontra-se fechado. 
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public Boolean verificaFechamento(Despesaviagem bean){
		if(bean.getEmpresa() != null){
			return !this.isDataValida(this.findByEmpresaConta(bean.getEmpresa(), null), bean.getDtsaida());
		}
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que verifica se o per�odo da data da movimenta��o encontra-se fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaFechamento(Movimentacao bean){
		Conta conta = bean.getConta();
		if(conta != null){
			conta = contaService.loadContaWithEmpresa(conta);
			
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			if(bean.getEmpresa() != null){
				listaEmpresa.add(bean.getEmpresa());
			}else {
				Set<Contaempresa> listaContaempresa = conta.getListaContaempresa();
				for (Contaempresa contaempresa : listaContaempresa) {
					if(contaempresa.getEmpresa() != null) listaEmpresa.add(contaempresa.getEmpresa());
				}
			}
			
			return !this.isDataValida(this.findByEmpresaConta(listaEmpresa, conta), bean.getDtmovimentacao());
		}
		return Boolean.FALSE;
	}
	
	public Boolean verificaFechamentoDatabanco(Movimentacao bean){
		Conta conta = bean.getConta();
		if(conta != null && bean.getDtbanco() != null){
			conta = contaService.loadContaWithEmpresa(conta);
			
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			Set<Contaempresa> listaContaempresa = conta.getListaContaempresa();
			for (Contaempresa contaempresa : listaContaempresa) {
				if(contaempresa.getEmpresa() != null) listaEmpresa.add(contaempresa.getEmpresa());
			}
			
			return !this.isDataValida(this.findByEmpresaConta(listaEmpresa, conta), bean.getDtbanco());
		}
		return Boolean.FALSE;
	}
	
	
	/**
	 * M�todo que verifica se existe alguma movimenta��o cuja data refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(Movimentacao bean){
		if(bean.getWhereIn() != null){
			return movimentacaoService.verificaListaFechamento(bean.getWhereIn());
		}
		

		return Boolean.FALSE;
	}
	
	
	/**
	 * M�todo que verifica se existe alguma conta cuja de vencimento data refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(Documento bean){
		if(bean.getWhereIn() != null){
			return documentoService.verificaListaFechamento(bean.getWhereIn());
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que verifica se existe algum agendamento cuja data de vencimento refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(Agendamento bean){
		if(bean.getWhereIn() != null){
			return agendamentoService.verificaListaFechamento(bean.getWhereIn());
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * Verifi o fechamento financeiro a partir de um whereIn de contrato.
	 *
	 * @param whereIn
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	public Boolean verificaListaFechamentoContrato(String whereIn){
		Contrato aux = new Contrato();
		aux.setWhereIn(whereIn);
		return this.verificaListaFechamento(aux);
	}
	
	/**
	 * M�todo que verifica se existe algum contrato cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(Contrato bean){
		if(bean.getWhereIn() != null){
			return contratoService.verificaListaFechamento(bean.getWhereIn());
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que verifica se existe alguma nota fiscal de servico cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(NotaFiscalServico bean){
		if(bean.getWhereIn() != null){
			return notaFiscalServicoService.verificaListaFechamento(bean.getWhereIn());
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que verifica se existe alguma conta a ser baixada cuja data de vencimento refere-se a um per�odo j� fechado. 
	 * @param bean
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(BaixarContaBean bean){
		String whereInDocumento = CollectionsUtil.listAndConcatenate(bean.getListaDocumento(), "cddocumento", ",");
		String whereInConta = CollectionsUtil.listAndConcatenate(bean.getListaBaixarContaBeanMovimentacao(), "vinculo.cdconta", ",");
		Date dtpagamento = bean.getDtpagamento();
		if(whereInDocumento != null){
			return documentoService.verificaListaFechamento(whereInDocumento, dtpagamento, whereInConta);
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo que busca o fechamento financeiro anterior ao que vai ser salvo
	 * @param cdfechamentofinanceiro
	 * @return
	 */
	public Fechamentofinanceiro findFechamentoFinanceiroAntigoByCdFechamento(Integer cdfechamentofinanceiro) {
		return fechamentofinanceiroDAO.findFechamentoFinanceiroAntigoByCdFechamento(cdfechamentofinanceiro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param fechamentofinanceiro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/03/2015
	 */
	public boolean haveFechamento(Fechamentofinanceiro fechamentofinanceiro) {
		return fechamentofinanceiroDAO.haveFechamento(fechamentofinanceiro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.FechamentofinanceiroDAO#haveFechamentoPeriodo(Fechamentofinanceiro fechamentofinanceiro)
	*
	* @param fechamentofinanceiro
	* @return
	* @since 12/05/2015
	* @author Luiz Fernando
	*/
	public boolean haveFechamentoPeriodo(Fechamentofinanceiro fechamentofinanceiro) {
		return fechamentofinanceiroDAO.haveFechamentoPeriodo(fechamentofinanceiro);
	}
	
	public boolean haveFechamentoPeriodo(Empresa empresa, Date datainicio, Date datalimite, Conta conta) {
		return fechamentofinanceiroDAO.haveFechamentoPeriodo(empresa, datainicio, datalimite, conta);
	}
	
	public void criarAvisoFechamentoFinanceiro(Motivoaviso m, Date data) {
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Empresa> empresaList = empresaService.findAtivos();
		Boolean fechamentoGeral = Boolean.FALSE;
		Boolean verificarContaSemEmpresa = Boolean.FALSE;
		List<Avisousuario> avisoUsuarioList = null;
		
		for (Empresa e : empresaList) {
			fechamentoGeral = fechamentofinanceiroDAO.haveFechamentoGeralMesAnterior(data, e);
			
			if (!fechamentoGeral) {
				List<Contatipo> contaTipoList = contatipoService.findAll();
				
				for (Contatipo c : contaTipoList) {
					List<Conta> contaList = contaService.findByContatipo(c, e);
					
					for (Conta o : contaList) {
						if ((o.getDtsaldo() == null || SinedDateUtils.calculaDiferencaDias(o.getDtsaldo(), data) > 30) && !fechamentofinanceiroDAO.haveFechamentoContaMesAnterior(data, o)) {
							Aviso aviso = new Aviso("Fechamento financeiro n�o realizado", "Nome da conta: " + o.getNome(), 
									m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), o.getCdconta(), e, SinedDateUtils.currentDate(), m, Boolean.FALSE);
							
							Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, o.getCdconta());
							
							if (lastAvisoDate != null) {
								avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, o.getCdconta(), lastAvisoDate);
								
								List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
								
								if (SinedUtil.isListNotEmpty(listaUsuario)) {
									avisoService.salvarAvisos(aviso, listaUsuario, false);
								}
							} else {
								avisoList.add(aviso);
							}
						}
					}
				}
			} else {
				verificarContaSemEmpresa = fechamentoGeral;
			}
		}
		
		if (verificarContaSemEmpresa) {
			List<Contatipo> contaTipoList = contatipoService.findAll();
			
			for (Contatipo c : contaTipoList) {
				List<Conta> contaList = contaService.findByContatipo(c, null);
				
				for (Conta o : contaList) {
					if ((o.getDtsaldo() == null || SinedDateUtils.calculaDiferencaDias(o.getDtsaldo(), data) > 30) && !fechamentofinanceiroDAO.haveFechamentoContaMesAnterior(data, o)) {
						Aviso aviso = new Aviso("Fechamento financeiro n�o realizado", "C�digo da conta: " + o.getCdconta(), 
								m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), o.getCdconta(), null, SinedDateUtils.currentDate(), m, Boolean.FALSE);
						
						Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, o.getCdconta());
						
						if (lastAvisoDate != null) {
							avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, o.getCdconta(), lastAvisoDate);
							
							List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
							
							if (SinedUtil.isListNotEmpty(listaUsuario)) {
								avisoService.salvarAvisos(aviso, listaUsuario, false);
							}
						} else {
							avisoList.add(aviso);
						}
					}
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
}