package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Faturamentocontrato;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.dao.FaturamentocontratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FaturamentocontratoService extends GenericService<Faturamentocontrato> {

	private FaturamentocontratoDAO faturamentocontratoDAO;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ContratoService contratoService;
	private DespesaviagemService despesaviagemService;
	
	public void setFaturamentocontratoDAO(
			FaturamentocontratoDAO faturamentocontratoDAO) {
		this.faturamentocontratoDAO = faturamentocontratoDAO;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Faturamentocontrato> findFaturamentosForListagem(String whereIn) {
		return faturamentocontratoDAO.findFaturamentosForListagem(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Faturamentocontrato> findFaturamentos(String whereIn) {
		return faturamentocontratoDAO.findFaturamentos(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Faturamentocontrato findByContrato(Contrato contrato) {
		return faturamentocontratoDAO.findByContrato(contrato);
	}
	
	/**
	 * M�todo que preenche a lista de faturamento com as notas de servi�o e produto
	 * 
	 * @param listaFaturamentos
	 * @author Tom�s Rabelo
	 */
	public void preencheListaFaturamentos(List<Faturamentocontrato> listaFaturamentos) {
		if(listaFaturamentos != null && !listaFaturamentos.isEmpty()){
			StringBuilder whereIn = new StringBuilder("");
			for (Faturamentocontrato faturamentocontrato : listaFaturamentos) 
				if(faturamentocontrato.getListaNotascontrato() != null && !faturamentocontrato.getListaNotascontrato().isEmpty())
					whereIn.append(CollectionsUtil.listAndConcatenate(faturamentocontrato.getListaNotascontrato(), "nota.cdNota", ",")).append(",");
			
			if(whereIn.toString() != null && !whereIn.toString().equals("")){
				whereIn.delete(whereIn.length()-1, whereIn.length());
				List<Notafiscalproduto> listaNotasFiscaisProdutos = notafiscalprodutoService.findByWhereIn(whereIn.toString());
				List<NotaFiscalServico> listaNotasFiscaisServicos = notaFiscalServicoService.findByWhereIn(whereIn.toString());
				
				if(listaNotasFiscaisServicos != null && !listaNotasFiscaisServicos.isEmpty()){
					for (NotaFiscalServico notaFiscalServico : listaNotasFiscaisServicos) {
						boolean achou = false;
						for (Faturamentocontrato faturamentocontrato : listaFaturamentos){ 
							if(faturamentocontrato.getListaNotascontrato() != null && !faturamentocontrato.getListaNotascontrato().isEmpty()){
								for (NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()){ 
									if(notaFiscalServico.equals(notaContrato.getNota())){
										notaContrato.setNota(notaFiscalServico);
										achou = true;
										break;
									}
								}
								if(achou)
									break;
							}
						}
					}
				}
				
				if(listaNotasFiscaisProdutos != null && !listaNotasFiscaisProdutos.isEmpty()){
					for (Notafiscalproduto notafiscalproduto : listaNotasFiscaisProdutos) {
						boolean achou = false;
						for (Faturamentocontrato faturamentocontrato : listaFaturamentos){ 
							if(faturamentocontrato.getListaNotascontrato() != null && !faturamentocontrato.getListaNotascontrato().isEmpty()){
								for (NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()){ 
									if(notafiscalproduto.equals(notaContrato.getNota())){
										notaContrato.setNota(notafiscalproduto);
										achou = true;
										break;
									}
								}
								if(achou)
									break;
							}
						}
					}
				}
			}
			
			StringBuilder whereInContrato = new StringBuilder();
			for(Faturamentocontrato faturamentocontrato : listaFaturamentos){
				if(faturamentocontrato.getListaNotascontrato() != null && !faturamentocontrato.getListaNotascontrato().isEmpty()){
					for(NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()){
						if(notaContrato.getContrato() != null){
							if(!"".equals(whereInContrato.toString())) whereInContrato.append(",");
							whereInContrato.append(notaContrato.getContrato().getCdcontrato());
						}
					}
				}
			}
			
			if(!"".equals(whereInContrato.toString())){
				StringBuilder whereInProjeto;
				List<Contrato> listaContrato = contratoService.findContratoWithRateioProjeto(whereInContrato.toString()); 
				
				if(listaContrato != null && !listaContrato.isEmpty()){
					for(Contrato contrato : listaContrato){
						if(contrato.getRateio() != null && contrato.getRateio().getListaRateioitem() != null && 
								!contrato.getRateio().getListaRateioitem().isEmpty()){
							for(Faturamentocontrato faturamentocontrato : listaFaturamentos){
								if(faturamentocontrato.getListaNotascontrato() != null && !faturamentocontrato.getListaNotascontrato().isEmpty()){
									for(NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()){
										if(notaContrato.getContrato() != null && contrato.equals(notaContrato.getContrato())){
											whereInProjeto = new StringBuilder();
											for(Rateioitem rateioitem : contrato.getRateio().getListaRateioitem()){
												if(rateioitem.getProjeto() != null){
													if(!"".equals(whereInProjeto.toString())) whereInProjeto.append(",");
													whereInProjeto.append(rateioitem.getProjeto().getCdprojeto());
												}
											}
											if(!"".equals(whereInContrato.toString())){
												faturamentocontrato.setListaDespesaviagem(despesaviagemService.findForGerarreceitalote(whereInProjeto.toString()));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

}
