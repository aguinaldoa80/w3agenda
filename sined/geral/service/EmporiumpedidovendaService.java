package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovenda;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.EmporiumpedidovendaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioECFBean;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumImportacaoClientesUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumpedidovendaService extends GenericService<Emporiumpedidovenda>{

	private EmporiumpedidovendaDAO emporiumpedidovendaDAO;
	private PedidovendaService pedidovendaService;
	private ArquivoDAO arquivoDAO;
	private EmporiumpdvService emporiumpdvService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private EnderecoService enderecoService;
	private ParametrogeralService parametrogeralService;
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setPedidovendahistoricoService(
			PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setEmporiumpedidovendaDAO(
			EmporiumpedidovendaDAO emporiumpedidovendaDAO) {
		this.emporiumpedidovendaDAO = emporiumpedidovendaDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	
	/**
	 * Envia o(s) pedido(s) de venda para o ECF
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	public void enviarPedidovendaECF(EnvioECFBean bean) {
		WebRequestContext request = NeoWeb.getRequestContext();
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForEnvioECF(bean.getWhereIn());
		List<Emporiumpedidovenda> listaEmporiumpedidovenda = new ArrayList<Emporiumpedidovenda>();
		Emporiumpdv emporiumpdv = emporiumpdvService.load(bean.getEmporiumpdv());
		
		if(listaPedidovenda != null && listaPedidovenda.size() > 0){
			for (Pedidovenda pedidovenda : listaPedidovenda) {
				try {
					if(!this.havePedidovendaEnviado(pedidovenda)){
						listaEmporiumpedidovenda.add(this.montaPedidovenda(pedidovenda, emporiumpdv));
						
						Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
						pedidovendahistorico.setAcao("Enviado ao ECF");
						pedidovendahistorico.setObservacao("Pedido enviado ao PDV: " + emporiumpdv.getCodigoemporium());
						pedidovendahistorico.setPedidovenda(pedidovenda);
						
						pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
						
						if(IntegracaoEmporiumUtil.util.haveIpEmporium()){
							IntegracaoEmporiumImportacaoClientesUtil.util.criarArquivoImportacao(SinedUtil.getStringConexaoBanco(), pedidovenda.getCliente() != null ? pedidovenda.getCliente().getCdpessoa() : null);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					if(e.getMessage().contains("/")) {
						for(String erro : e.getMessage().split("/")) {
							if(!erro.equals("")) {
								request.addError("Erro ao montar o envio do pedido de venda " + pedidovenda.getCdpedidovenda() + ": " + erro);
							}
						}
					} else {
						request.addError("Erro ao montar o envio do pedido de venda " + pedidovenda.getCdpedidovenda() + ": " + e.getMessage());
					}
				}
			}
		}
		
		List<Emporiumpedidovenda> listaEmporiumpedidovendaSalva = new ArrayList<Emporiumpedidovenda>();
		for (Emporiumpedidovenda emporiumpedidovenda : listaEmporiumpedidovenda) {
			try {
				this.saveOrUpdate(emporiumpedidovenda);
				listaEmporiumpedidovendaSalva.add(emporiumpedidovenda);
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Erro ao salvar o bean auxiliar do pedido de venda " + emporiumpedidovenda.getPedidovenda().getCdpedidovenda() + ".");
			}
		}
		
		int sucesso = 0;
		if(IntegracaoEmporiumUtil.util.haveIpEmporium()){
			String pathArquivo = IntegracaoEmporiumUtil.util.saveDir(SinedUtil.getStringConexaoBanco(), false);
			for (Emporiumpedidovenda emporiumpedidovenda : listaEmporiumpedidovendaSalva) {
				try {
					String arquivoStr = this.montaStringArquivo(emporiumpedidovenda);
					String nomeArquivo = "arquivo_importacaopedidovenda_" + SinedUtil.datePatternForEmporium();
					
					Resource resource = new Resource("application/xml", nomeArquivo+ ".xml", arquivoStr.getBytes());
					
					Arquivo arquivo = new Arquivo();
					arquivo.setNome(resource.getFileName());
					arquivo.setContent(resource.getContents());
					arquivo.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
					arquivo.setTamanho(Long.parseLong("" + resource.getContents().length));
					arquivo.setTipoconteudo(resource.getContentType());
					
					emporiumpedidovenda.setArquivo(arquivo);
					arquivoDAO.saveFile(emporiumpedidovenda, "arquivo");
					
					this.updateArquivo(emporiumpedidovenda);
					IntegracaoEmporiumUtil.util.stringToFile(pathArquivo, nomeArquivo + ".xml", arquivoStr);
					
					this.updateProcessado(emporiumpedidovenda);
					
					sucesso++;
				} catch (Exception e) {
					e.printStackTrace();
					request.addError("Erro ao montar o arquivo de envio do pedido de venda " + emporiumpedidovenda.getPedidovenda().getCdpedidovenda() + ".");
				}
			}
		} else {
			sucesso = listaEmporiumpedidovendaSalva.size();
		}
		
		CacheWebserviceUtil.removeEmporiumpedidovenda();
		if(sucesso > 0){
			request.addMessage("Foram enviado(s) " + sucesso + " pedido(s) de venda com sucesso.");
		} else {
			request.addError("N�o foi enviado nenhum pedido de venda.");
		}
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	public boolean havePedidovendaEnviado(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			return false;
		return havePedidovendaEnviado(pedidovenda.getCdpedidovenda().toString());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EmporiumpedidovendaDAO#havePedidovendaEnviado(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 30/07/2014
	* @author Luiz Fernando
	*/
	public boolean havePedidovendaEnviado(String whereIn) {
		return emporiumpedidovendaDAO.havePedidovendaEnviado(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param emporiumpedidovenda
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	private void updateProcessado(Emporiumpedidovenda emporiumpedidovenda) {
		emporiumpedidovendaDAO.updateProcessado(emporiumpedidovenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param emporiumpedidovenda
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	private void updateArquivo(Emporiumpedidovenda emporiumpedidovenda) {
		emporiumpedidovendaDAO.updateArquivo(emporiumpedidovenda);
	}
	
	/**
	 * Monta a string do XML do envio do pedido de venda para o emporium.
	 *
	 * @param emporiumpedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	private String montaStringArquivo(Emporiumpedidovenda emporiumpedidovenda) {
		StringBuilder xml = new StringBuilder();
		
		xml.append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
		xml.append("<REQUEST>");
			xml.append("<STORE>").append(emporiumpedidovenda.getLoja()).append("</STORE>");
			xml.append("<POS>").append(emporiumpedidovenda.getPdv()).append("</POS>");
			xml.append("<TICKET>").append(emporiumpedidovenda.getPedidovenda().getCdpedidovenda()).append("</TICKET>");
			xml.append("<TRN>").append(emporiumpedidovenda.getPedidovenda().getCdpedidovenda()).append("</TRN>");
			xml.append("<FISCAL_STORE>").append("</FISCAL_STORE>");
			xml.append("<FISCAL_POS>").append("</FISCAL_POS>");
			xml.append("<CASHIER_ID>").append("</CASHIER_ID>");
			xml.append("<CASHIER_NAME>").append("</CASHIER_NAME>");
			xml.append("<FISCAL_DAY>").append(SinedDateUtils.toString(emporiumpedidovenda.getDatahorafiscal(), "yyyyMMdd")).append("</FISCAL_DAY>");
			xml.append("<FISCAL_TIME>").append(SinedDateUtils.toString(emporiumpedidovenda.getDatahorafiscal(), "yyyyMMddHHmmss")).append("</FISCAL_TIME>");
			
			if(emporiumpedidovenda.getCodigo_cliente() != null){
				String customerId = emporiumpedidovenda.getCodigo_cliente() + "";
				String customerName = emporiumpedidovenda.getCliente_nome();
				
				xml.append("<ANSWER><ID>1</ID><LABEL>Cliente</LABEL><DATA>").append(customerId).append("</DATA></ANSWER>");
				xml.append("<ANSWER><ID>81</ID><LABEL>Nome Cliente</LABEL><DATA>").append(customerName).append("</DATA></ANSWER>");
				
				String customerAdress = emporiumpedidovenda.getCliente_logradouro();
				String customerCity = emporiumpedidovenda.getCliente_cidade();
				
				if(emporiumpedidovenda.getCliente_bairro() != null){
					customerAdress += ", " + emporiumpedidovenda.getCliente_bairro();
				}
				if(emporiumpedidovenda.getCliente_logradouro() != null){
					xml.append("<ANSWER><ID>83</ID><LABEL>Endere�o</LABEL><DATA>").append(customerAdress).append("</DATA></ANSWER>");
				}
				if(emporiumpedidovenda.getCliente_cidade() != null){
					xml.append("<ANSWER><ID>85</ID><LABEL>Cidade</LABEL><DATA>").append(customerCity).append("</DATA></ANSWER>");
				}
			}
			
			if(emporiumpedidovenda.getCodigo_vendedor() != null &&
					emporiumpedidovenda.getPedidovenda() != null && 
					emporiumpedidovenda.getPedidovenda().getColaborador() != null){
				String vendedorId = emporiumpedidovenda.getCodigo_vendedor() + "";
				String vendedorNome = emporiumpedidovenda.getPedidovenda().getColaborador().getNome();
				
				xml.append("<ANSWER><ID>20</ID><LABEL>Cod Vendedor</LABEL><DATA>").append(vendedorId).append("</DATA></ANSWER>");
				xml.append("<ANSWER><ID>77</ID><LABEL>Nome Vendedor</LABEL><DATA>").append(vendedorNome).append("</DATA></ANSWER>");
			}
			
			List<Emporiumpedidovendaitem> listaEmporiumpedidovendaitem = emporiumpedidovenda.getListaEmporiumpedidovendaitem();
			for (Emporiumpedidovendaitem emporiumpedidovendaitem : listaEmporiumpedidovendaitem) {
				xml.append("<ITEM>");
					xml.append("<SEQ>").append(emporiumpedidovendaitem.getSequencial()).append("</SEQ>");
					xml.append("<SKU>").append(emporiumpedidovendaitem.getProduto_id()).append("</SKU>");
					xml.append("<ID>").append(emporiumpedidovendaitem.getProduto_id()).append("</ID>");
					xml.append("<DESCRIPTION>").append(IntegracaoEmporiumUtil.util.maxPrintString(emporiumpedidovendaitem.getProduto_descricao(), 22)).append("</DESCRIPTION>");
					xml.append("<UNIT_PRICE>").append(this.formataDouble(emporiumpedidovendaitem.getPreco_unitario())).append("</UNIT_PRICE>");
					xml.append("<AMOUNT>").append(this.formataDouble(emporiumpedidovendaitem.getTotal())).append("</AMOUNT>");
					xml.append("<DECS_PRICE>").append(3).append("</DECS_PRICE>");
					xml.append("<QTY>").append(this.formataDouble(emporiumpedidovendaitem.getQuantidade())).append("</QTY>");
					xml.append("<DECS_QTY>").append(3).append("</DECS_QTY>");
					xml.append("<TAX_PERC>").append("</TAX_PERC>");
					xml.append("<TAX_ID>").append(emporiumpedidovendaitem.getTaxa_id()).append("</TAX_ID>");
					xml.append("<DISCOUNT>");
					if(emporiumpedidovendaitem.getDesconto() != null){
						xml.append(this.formataDouble(emporiumpedidovendaitem.getDesconto()));
					}
					xml.append("</DISCOUNT>");
					xml.append("<SUBTOTAL>").append(this.formataDouble(emporiumpedidovendaitem.getSubtotal())).append("</SUBTOTAL>");
					xml.append("<UNIT>").append(emporiumpedidovendaitem.getUnidademedida()).append("</UNIT>");
				xml.append("</ITEM>"); 
			}
		
			xml.append("<SUBTOTAL>").append("</SUBTOTAL>");
			xml.append("<DISCOUNT>").append("</DISCOUNT>");
			xml.append("<INCREASE>").append("</INCREASE>");
			xml.append("<INTEREST>").append("</INTEREST>");
			xml.append("<DUE>").append("</DUE>");
		
		xml.append("</REQUEST>"); 
		
		return xml.toString();
	}
	
	/**
	 * Formata o double para o XML do pedido de venda
	 *
	 * @param dbl
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	private String formataDouble(Double dbl){
		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
		decimalFormatSymbols.setDecimalSeparator('.');
		
		DecimalFormat format = new DecimalFormat("##0.000", decimalFormatSymbols);
		return format.format(dbl);
	}
	
	/**
	 * Monta o pedido de venda do ECF a partir do pedido de venda do W3.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @param emporiumpdv 
	 * @since 25/04/2013
	 */
	private Emporiumpedidovenda montaPedidovenda(Pedidovenda pedidovenda, Emporiumpdv emporiumpdv) {
		List<Pedidovendamaterial> listaPedidovendamaterialAntigo = pedidovenda.getListaPedidovendamaterial();
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		
		Emporiumpedidovenda emporiumpedidovenda = new Emporiumpedidovenda();
		
		if((pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0) ||
				(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0)){
			Double valorvalecompra = pedidovenda.getValorusadovalecompra() != null ? pedidovenda.getValorusadovalecompra().getValue().doubleValue() : 0d;
			Double desconto = pedidovenda.getDesconto() != null ? pedidovenda.getDesconto().getValue().doubleValue() : 0d;

			//Double desconto = valordesconto + valorvalecompra;
			Double valortotal = pedidovenda.getTotalvenda().getValue().doubleValue();
			Double descontoRestante = desconto;
			Double valorvalecompraRestante = valorvalecompra;
			
			// FEITO ISSO POIS O VALOR VEM COM O DESCONTO APLICADO
			valortotal += desconto + valorvalecompra;
			
			for (int i = 0; i < listaPedidovendamaterialAntigo.size(); i++) {
				Pedidovendamaterial it = listaPedidovendamaterialAntigo.get(i);
				
				Double valor_it = it.getTotalproduto().getValue().doubleValue();
				if(it.getDesconto() != null){
					valor_it -= it.getDesconto().getValue().doubleValue();
				}
				
				Double percent = (valor_it * 100d)/valortotal;
				Double desconto_it = SinedUtil.round((percent * desconto)/100d, 2);
				Double valorvalecompra_it = SinedUtil.round((percent * valorvalecompra)/100d, 2);
				
				descontoRestante -= desconto_it;
				if((i+1) == listaPedidovendamaterialAntigo.size()){
					desconto_it += descontoRestante;
				}
				valorvalecompraRestante -= valorvalecompra_it;
				if((i+1) == listaPedidovendamaterialAntigo.size()){
					//desconto negativo - deve ser removido do vale compra o desconto negativo do �ltimo item 
					if(descontoRestante < 0 && valorvalecompraRestante >= descontoRestante){
						valorvalecompra_it += valorvalecompraRestante + descontoRestante;
					}else {
						valorvalecompra_it += valorvalecompraRestante;
					}
				}
				
				it.setDesconto_it(desconto_it);
				it.setValorvalecomprapropocional(valorvalecompra_it);
			}
		}
		
		for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterialAntigo) {
			Material material = pedidovendamaterial.getMaterial();
			if(material != null && material.getVendaecf() != null && material.getVendaecf()){
				listaPedidovendamaterial.add(pedidovendamaterial);
			}
		}
		
		int sequencial = 0;
		Double subtotal = 0d;
		
		StringBuilder mensagemItensComErro = new StringBuilder();
		List<Emporiumpedidovendaitem> listaEmporiumpedidovendaitem = new ArrayList<Emporiumpedidovendaitem>();
		for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
			Emporiumpedidovendaitem emporiumpedidovendaitem = new Emporiumpedidovendaitem();
			
			Material material = pedidovendamaterial.getMaterial();
			Unidademedida unidademedida = pedidovendamaterial.getUnidademedida();
			Tributacaoecf tributacaoecf = material.getTributacaoecf();
			
			emporiumpedidovendaitem.setProduto_id(material.getCdmaterial());
			emporiumpedidovendaitem.setProduto_descricao(material.getNome());
			
			if(unidademedida != null){
				emporiumpedidovendaitem.setUnidademedida(unidademedida.getSimbolo());
			}
			if(tributacaoecf != null){
				emporiumpedidovendaitem.setTaxa_id(tributacaoecf.getCodigoecf());
			}
			
			Double desconto = 0d;
			Double total = pedidovendamaterial.getTotalproduto().getValue().doubleValue();
			if(pedidovendamaterial.getDesconto() != null){
				desconto += pedidovendamaterial.getDesconto().getValue().doubleValue();
			}
			if(pedidovendamaterial.getDesconto_it() != null){
				desconto += pedidovendamaterial.getDesconto_it();
			}
			subtotal += total;
			
			emporiumpedidovendaitem.setDesconto(desconto > 0 ? desconto : null);
			emporiumpedidovendaitem.setValorvalecompra(pedidovendamaterial.getValorvalecomprapropocional());
//			emporiumpedidovendaitem.setQuantidade(pedidovendamaterial.getQuantidade());
			emporiumpedidovendaitem.setPreco_unitario(pedidovendamaterial.getPreco());
			emporiumpedidovendaitem.setTotal(total);
			if(pedidovendamaterial.getPreco() != null && pedidovendamaterial.getPreco().equals(0d)){
				mensagemItensComErro.append("N�o � poss�vel enviar o pedido ao ECF, devido o valor item " +  
						emporiumpedidovendaitem.getProduto_descricao() + " ser igual a zero. /");
				continue;
			}
			
			Double vQtd = SinedUtil.roundByParametro(total/pedidovendamaterial.getPreco(), 10);
			
			DecimalFormat formatoQuantidade = new DecimalFormat("#.###"); 
			String qtm = formatoQuantidade.format(vQtd);
			Double quantidade =  Double.parseDouble(qtm.replace(",", "."));
			
			Double totalEPV = SinedUtil.round(quantidade * pedidovendamaterial.getPreco(), 3, false);
			
			String parteDecimalTotalEPV = totalEPV.toString().substring(totalEPV.toString().indexOf(".") + 1);
			
			if(!pedidovendamaterial.getQuantidade().equals(1D) 
					&& (vQtd.compareTo(pedidovendamaterial.getQuantidade()) != 0 
							|| (parteDecimalTotalEPV.length() == 3 && parteDecimalTotalEPV.endsWith("5")))){
                if(total.compareTo(totalEPV) != 0){
                    emporiumpedidovendaitem.setQuantidade(quantidade + 0.001);
                }else {
                    emporiumpedidovendaitem.setQuantidade(pedidovendamaterial.getQuantidade());
                }
			}else{
				emporiumpedidovendaitem.setQuantidade(pedidovendamaterial.getQuantidade());
			}
			Double totalItem = emporiumpedidovendaitem.getTotal() != null ? emporiumpedidovendaitem.getTotal() : 0D;
			Double descontoItem = emporiumpedidovendaitem.getDesconto() != null ? emporiumpedidovendaitem.getDesconto() : 0D;
			Double valeCompraItem = emporiumpedidovendaitem.getValorvalecompra() != null ? emporiumpedidovendaitem.getValorvalecompra() : 0D;
			
			if(totalItem - descontoItem - valeCompraItem <= 0) {
				if(MoneyUtils.isMaiorQueZero(pedidovenda.getDesconto()) || 
						MoneyUtils.isMaiorQueZero(pedidovenda.getValorusadovalecompra())){
					emporiumpedidovendaitem.setTotal(totalItem + 0.01);
				}
//				mensagemItensComErro.append("N�o � poss�vel enviar o pedido ao ECF, devido o valor total " + 
//					"geral de desconto (desconto do item + desconto rateado + vale-compra rateado)  do " +  
//					emporiumpedidovendaitem.getProduto_descricao() + " ser igual ou maior que o valor total do item. /");
			}
			
			emporiumpedidovendaitem.setSequencial(sequencial);
			emporiumpedidovendaitem.setSubtotal(subtotal);
			
			listaEmporiumpedidovendaitem.add(emporiumpedidovendaitem);
			
			sequencial++;
		}
		
		if(!mensagemItensComErro.toString().equals("")) {
			throw new SinedException(mensagemItensComErro.toString());
		}
		
		if(listaEmporiumpedidovendaitem.size() == 0){
			throw new SinedException("Nenhum item a ser enviado ao Emporium.");
		}
		
		if(pedidovenda.getValorfrete() != null && pedidovenda.getValorfrete().getValue().doubleValue() > 0){
			String idMaterialFrete = parametrogeralService.getValorPorNome(Parametrogeral.EMPORIUM_VENDA_FRETEMATERIAL);
			if(idMaterialFrete != null && !idMaterialFrete.trim().equals("")){
				try{
					Integer idMaterialFreteInt = Integer.parseInt(idMaterialFrete);
					Material materialFrete = materialService.loadForIntegracaoECF(new Material(idMaterialFreteInt));
					if(materialFrete != null){
						Emporiumpedidovendaitem emporiumpedidovendaitem = new Emporiumpedidovendaitem();
						
						Double valorFreteDbl = pedidovenda.getValorfrete().getValue().doubleValue();
						Unidademedida unidademedida = materialFrete.getUnidademedida();
						Tributacaoecf tributacaoecf = materialFrete.getTributacaoecf();
						
						emporiumpedidovendaitem.setProduto_id(materialFrete.getCdmaterial());
						emporiumpedidovendaitem.setProduto_descricao(materialFrete.getNome());
						
						if(unidademedida != null){
							emporiumpedidovendaitem.setUnidademedida(unidademedida.getSimbolo());
						}
						if(tributacaoecf != null){
							emporiumpedidovendaitem.setTaxa_id(tributacaoecf.getCodigoecf());
						}
						
						emporiumpedidovendaitem.setDesconto(null);
						emporiumpedidovendaitem.setQuantidade(1d);
						emporiumpedidovendaitem.setPreco_unitario(valorFreteDbl);
						emporiumpedidovendaitem.setTotal(valorFreteDbl);
						subtotal += valorFreteDbl;
						
						emporiumpedidovendaitem.setSequencial(sequencial);
						emporiumpedidovendaitem.setSubtotal(subtotal);
						
						listaEmporiumpedidovendaitem.add(emporiumpedidovendaitem);
						
						sequencial++;
					}
				} catch (Exception e) {}
			}
		}
		
		Empresa empresa = pedidovenda.getEmpresa();
		
		emporiumpedidovenda.setListaEmporiumpedidovendaitem(listaEmporiumpedidovendaitem);
		
		if(empresa != null && 
				empresa.getIntegracaopdv() != null && 
				!empresa.getIntegracaopdv().equals("")) {
			emporiumpedidovenda.setLoja(StringUtils.stringCheia(empresa.getIntegracaopdv(), "0", 5, false));
		} else {
			emporiumpedidovenda.setLoja("00001");
		}
		
		if(emporiumpdv != null && 
				emporiumpdv.getCodigoemporium() != null &&
				!emporiumpdv.getCodigoemporium().equals("")){
			emporiumpedidovenda.setPdv(StringUtils.stringCheia(emporiumpdv.getCodigoemporium(), "0", 3, false));
		} else {
			emporiumpedidovenda.setPdv("001");
		}
		
		emporiumpedidovenda.setDatahorafiscal(new Timestamp(System.currentTimeMillis()));
		emporiumpedidovenda.setPedidovenda(pedidovenda);
		emporiumpedidovenda.setTotal(subtotal);
		
		emporiumpedidovenda.setCodigo(pedidovenda.getCdpedidovenda());
		try {
			Colaborador colaborador = pedidovenda.getColaborador();
			emporiumpedidovenda.setCodigo_vendedor(colaborador != null && colaborador.getCodigoemporium() != null &&
													!StringUtils.soNumero(colaborador.getCodigoemporium()).equals("") ? 
													Integer.parseInt(StringUtils.soNumero(colaborador.getCodigoemporium())) : 
													null);
		} catch (Exception e) {}
		
		Cliente cliente = pedidovenda.getCliente();
		if(cliente != null){
			emporiumpedidovenda.setCodigo_cliente(cliente.getCdpessoa());
			String cpfcnpj = cliente.getCpfcnpj();
			if(cpfcnpj != null){
				emporiumpedidovenda.setCliente_cpf_cnpj(StringUtils.soNumero(cpfcnpj));
			}
			emporiumpedidovenda.setCliente_nome(cliente.getNome());
			
			List<Endereco> listaEndereco = enderecoService.findByCliente(cliente);
			if(listaEndereco != null && listaEndereco.size() > 0){
				Collections.sort(listaEndereco, new Comparator<Endereco>(){
					public int compare(Endereco o1, Endereco o2) {
						if(o1.getEnderecotipo() == null
								|| o1.getEnderecotipo().getCdenderecotipo() == null
								|| o2.getEnderecotipo() == null
								|| o2.getEnderecotipo().getCdenderecotipo() == null){
							return 0;
						} else {
							Integer cdenderecotipo = o1.getEnderecotipo().getCdenderecotipo();
							Integer cdenderecotipo2 = o2.getEnderecotipo().getCdenderecotipo();
							
							return cdenderecotipo.compareTo(cdenderecotipo2);
						}
					}
				});
				
				Endereco endereco = listaEndereco != null && listaEndereco.size() > 0 ? listaEndereco.get(0) : null;
				if(endereco != null){
					emporiumpedidovenda.setCliente_logradouro(endereco.getLogradouroCompleto() != null ? endereco.getLogradouroCompleto() : "");
					emporiumpedidovenda.setCliente_bairro(endereco.getBairro() != null ? endereco.getBairro() : "");
					emporiumpedidovenda.setCliente_cep(endereco.getCep() != null ? Integer.parseInt(endereco.getCep().getValue()) : null);
					
					Municipio municipio = endereco.getMunicipio();
					if(municipio != null){
						emporiumpedidovenda.setCliente_cidade(municipio.getNome() != null ? municipio.getNome() : "");
						emporiumpedidovenda.setCliente_uf(municipio.getUf() != null ? municipio.getUf().getSigla() : "");
					}
				} else {
					emporiumpedidovenda.setCliente_logradouro("");
					emporiumpedidovenda.setCliente_bairro("");
					emporiumpedidovenda.setCliente_cep(null);
					emporiumpedidovenda.setCliente_cidade("");
					emporiumpedidovenda.setCliente_uf("");
				}
			} else {
				emporiumpedidovenda.setCliente_logradouro("");
				emporiumpedidovenda.setCliente_bairro("");
				emporiumpedidovenda.setCliente_cep(null);
				emporiumpedidovenda.setCliente_cidade("");
				emporiumpedidovenda.setCliente_uf("");
			}
		}
		
		return emporiumpedidovenda;
	}
	
	public List<Emporiumpedidovenda> findForWebserviceGET() {
		return emporiumpedidovendaDAO.findForWebserviceGET();
	}
	
	public void updateEnviadoemporium(Integer id) {
		emporiumpedidovendaDAO.updateEnviadoemporium(id);
		CacheWebserviceUtil.removeEmporiumpedidovenda();
	}

	public Emporiumpedidovenda findByPedidovenda(Pedidovenda pedidovenda){
		return emporiumpedidovendaDAO.findByPedidovenda(pedidovenda);
	}
	
	public Money loadValorTotalValeCompraEmporiumByPedidovenda(Pedidovenda pedidovenda){
		return emporiumpedidovendaDAO.loadValorTotalValeCompraEmporiumByPedidovenda(pedidovenda);
	}
}
