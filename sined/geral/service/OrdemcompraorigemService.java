package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraorigem;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcompraorigemService extends GenericService<Ordemcompraorigem> {

	private SolicitacaocomprahistoricoService solicitacaocomprahistoricoService;
	private SolicitacaocompraService solicitacaocompraService;
	
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setSolicitacaocomprahistoricoService(SolicitacaocomprahistoricoService solicitacaocomprahistoricoService) {
		this.solicitacaocomprahistoricoService = solicitacaocomprahistoricoService;
	}
	
	/**
	 * Cria e salva a ordemcompraorigem de solicita��es
	 *
	 * @param ordemcompra
	 * @param whereInSolicitacaocompra
	 * @author Luiz Fernando
	 */
	public void criaSalvaOrdemcompraorigemBySolicitacoes(Ordemcompra ordemcompra, String whereInSolicitacaocompra){
		if(ordemcompra != null && ordemcompra.getCdordemcompra() != null && whereInSolicitacaocompra != null && !"".equals(whereInSolicitacaocompra)){
			String[] solicitacoes = whereInSolicitacaocompra.split(",");
			if(solicitacoes != null && solicitacoes.length > 0){
				Ordemcompraorigem ordemcompraorigem;
				Solicitacaocomprahistorico solicitacaocomprahistorico;
				for(String idsolicitacao : solicitacoes){
					Solicitacaocompra solicitacaocompra = new Solicitacaocompra(Integer.parseInt(idsolicitacao));
					
					ordemcompraorigem = new Ordemcompraorigem();
					ordemcompraorigem.setOrdemcompra(ordemcompra);
					ordemcompraorigem.setSolicitacaocompra(solicitacaocompra);
					
					this.saveOrUpdate(ordemcompraorigem);
					
					solicitacaocomprahistorico = new Solicitacaocomprahistorico();
					solicitacaocomprahistorico.setSolicitacaocompra(solicitacaocompra);
					solicitacaocomprahistorico.setSolicitacaocompraacao(Solicitacaocompraacao.EM_PROCESSO_COMPRA);
					solicitacaocomprahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					solicitacaocomprahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					
					solicitacaocomprahistoricoService.saveOrUpdate(solicitacaocomprahistorico);
					
					solicitacaocompraService.callProcedureAtualizaSolicitacaocompra(solicitacaocompra);
				}
			}
		}
	}
	
	/**
	* M�todo que salva a origem da ordem de compra de acordo com o recurso geral do planejamento
	*
	* @param ordemcompra
	* @param whereInPlanejamentorecursogeral
	* @since 15/03/2016
	* @author Luiz Fernando
	*/
	public void saveOrdemcompraorigemByPlanejamentorecursogeral(Ordemcompra ordemcompra, String whereInPlanejamento) {
		if(ordemcompra != null && ordemcompra.getCdordemcompra() != null && StringUtils.isNotBlank(whereInPlanejamento)){
			String[] whereIn = whereInPlanejamento.split(",");
			if(whereIn != null && whereIn.length > 0){
				Ordemcompraorigem ordemcompraorigem;
				for(String id : whereIn){
					Planejamento planejamento = new Planejamento(Integer.parseInt(id));
					
					ordemcompraorigem = new Ordemcompraorigem();
					ordemcompraorigem.setOrdemcompra(ordemcompra);
					ordemcompraorigem.setPlanejamento(planejamento);
					this.saveOrUpdate(ordemcompraorigem);
				}
			}
		}
	}
}
