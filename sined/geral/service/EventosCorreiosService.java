package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
import br.com.linkcom.sined.geral.dao.EventosCorreiosDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EventosCorreiosService extends GenericService<EventosCorreios>{

	private EventosCorreiosDAO eventosCorreiosDAO;
	private static EventosCorreiosService instance;
	
	public static EventosCorreiosService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EventosCorreiosService.class);
		}
		return instance;
	}
	
	public void setEventosCorreiosDAO(EventosCorreiosDAO eventosCorreiosDAO) {this.eventosCorreiosDAO = eventosCorreiosDAO;}


	public EventosCorreios loadByCodigoAndTipoEvento(TipoEventoCorreios tipoEventoCorreios, Integer codigo){
		return eventosCorreiosDAO.loadByCodigoAndTipoEvento(tipoEventoCorreios, codigo);
	}

	public List<EventosCorreios> findAllWithTipoEvento(){
		return eventosCorreiosDAO.findAllWithTipoEvento();
	}
	public List<EventosCorreios>findDescricaoByTipo(TipoEventoCorreios tipoEvento){
		return eventosCorreiosDAO.findDescricaoByTipo(tipoEvento);
	}
	
	public EventosCorreios findCodigoByEvento (EventosCorreios evento){
		return eventosCorreiosDAO.findCodigoByEvento(evento);
	}
}
