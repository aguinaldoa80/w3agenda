package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.dao.ExpedicaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExpedicaohistoricoService extends GenericService<Expedicaohistorico> {

	private ExpedicaohistoricoDAO expedicaohistoricoDAO;
	private static ExpedicaohistoricoService instance;
	
	public static ExpedicaohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExpedicaohistoricoService.class);
		}
		return instance;
	}
	
	public void setExpedicaohistoricoDAO(
			ExpedicaohistoricoDAO expedicaohistoricoDAO) {
		this.expedicaohistoricoDAO = expedicaohistoricoDAO;
	}
	
	public void saveHistorico(String whereIn, String obs, Expedicaoacao expedicaoacao) {
		String[] ids = whereIn.split(",");
		Expedicaohistorico expedicaohistorico;
		for (int i = 0; i < ids.length; i++) {
			expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(new Expedicao(Integer.parseInt(ids[i])));
			expedicaohistorico.setExpedicaoacao(expedicaoacao);
			expedicaohistorico.setObservacao(obs);			
			this.saveOrUpdate(expedicaohistorico);
		}
	}
	
	public Expedicaoacao ultimaAcao(Expedicao expedicao, Expedicaoacao... acoes){
		return expedicaohistoricoDAO.ultimaAcao(expedicao, acoes);
	}

	public Expedicaohistorico getLastExpedicaohistoricoEmconferencia(Expedicaoitem expedicaoitem) {
		return expedicaohistoricoDAO.getLastExpedicaohistoricoEmconferencia(expedicaoitem);
	}
}
