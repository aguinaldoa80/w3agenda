package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SexoService extends GenericService<Sexo> {	
	
	/* singleton */
	private static SexoService instance;
	public static SexoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(SexoService.class);
		}
		return instance;
	}
	
}
