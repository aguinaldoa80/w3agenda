package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.dao.ProducaoetapaitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapaitem.ProducaoEtapaItemW3producaoRESTModel;

public class ProducaoetapaitemService extends GenericService<Producaoetapaitem> {

	private ProducaoetapaitemDAO producaoetapaitemDAO;
	
	public void setProducaoetapaitemDAO(
			ProducaoetapaitemDAO producaoetapaitemDAO) {
		this.producaoetapaitemDAO = producaoetapaitemDAO;
	}
	
	/**
	 * Método que faz referência ao DAO.
	 *
	 * @param producaoetapa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2014
	 */
	public List<Producaoetapaitem> findByProducaoetapa(Producaoetapa producaoetapa) {
		return producaoetapaitemDAO.findByProducaoetapa(producaoetapa);
	}
	
	public List<ProducaoEtapaItemW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de producaoetapaitem para a sincronização com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<ProducaoEtapaItemW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ProducaoEtapaItemW3producaoRESTModel> lista = new ArrayList<ProducaoEtapaItemW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Producaoetapaitem pei : producaoetapaitemDAO.findForW3Producao(whereIn))
				lista.add(new ProducaoEtapaItemW3producaoRESTModel(pei));
		}
		
		return lista;
	}
	
	public Producaoetapaitem loadForProducao(Producaoetapaitem producaoetapaitem){
		return producaoetapaitemDAO.loadForProducao(producaoetapaitem);
	}

}
