package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.dao.PedidovendahistoricoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaHistoricoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.pedidovendahistorico.PedidovendahistoricoRESTModel;

public class PedidovendahistoricoService extends GenericService<Pedidovendahistorico> {

	private PedidovendahistoricoDAO pedidovendahistoricoDAO;
	private MaterialService materialService;
	private PedidovendamaterialService pedidovendamaterialService;
	
	public void setPedidovendahistoricoDAO(
			PedidovendahistoricoDAO pedidovendahistoricoDAO) {
		this.pedidovendahistoricoDAO = pedidovendahistoricoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendahistoricoDAO#findByPedidovenda(Pedidovenda pedidovenda)
	 * 
	 * @param pedidovenda
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendahistorico> findByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendahistoricoDAO.findByPedidovenda(pedidovenda);
	}
	
	/**
	 * Cria um hist�rico a partir de uma ordem de compra.
	 *
	 * @param pedidovenda
	 * @param ordemcompra
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public void createByOrdemcompra(Pedidovenda pedidovenda, Ordemcompra ordemcompra){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Ordem de compra gerada");
		pedidovendahistorico.setObservacao("Ordem de compra gerada <a href=\"javascript:visualizarOrdemcompra(" + ordemcompra.getCdordemcompra() + ")\">" + ordemcompra.getCdordemcompra() + "</a>.");
		
		this.saveOrUpdate(pedidovendahistorico);
	}

	public static PedidovendahistoricoService instance;
	
	public static PedidovendahistoricoService getInstance(){
		if (instance==null){
			return Neo.getObject(PedidovendahistoricoService.class);
		}else {
			return instance;
		}
	}

	public void salvaPedidoHistoricoAndroid(Pedidovenda pedidovenda){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setObservacao(pedidovenda.getObservacao());
		pedidovendahistorico.setCdusuarioaltera(pedidovenda.getColaborador() != null ? pedidovenda.getColaborador().getCdpessoa() : null);
		pedidovendahistorico.setAcao("Criado (app w3erp)");
		saveOrUpdate(pedidovendahistorico);
	}

	
	public List<PedidovendahistoricoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial, String whereInPedidovenda) {
		List<PedidovendahistoricoRESTModel> lista = new ArrayList<PedidovendahistoricoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pedidovendahistorico bean : pedidovendahistoricoDAO.findForAndroid(whereIn, whereInPedidovenda)){
				PedidovendahistoricoRESTModel m = new PedidovendahistoricoRESTModel(bean);
				m.setNomeUsuario( PessoaService.getInstance().findNomeByCdpessoa(bean.getCdusuarioaltera()) );
				lista.add(m);
			}
		}
		return lista;
	}
	
	public void salvaPedidoHistoricoAtualizacaoWS(Pedidovenda pedidovenda){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setObservacao("Quantidades atualizadas pelo WMS. Qte de itens separados: " + pedidovenda.getQtdeTotalVolumesWms() + " itens.");
		pedidovendahistorico.setAcao("Alterado");
		saveOrUpdate(pedidovendahistorico);
	}
	
	/**
	 * Cria hist�rico de emiss�o de ficha de coleta, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param whereIn
	 * @since 16/07/2019
	 * @author Rodrigo Freitas
	 */
	public void makeHistoricoFichaColetaEmitido(String whereIn){
		if(StringUtils.isNotBlank(whereIn)){
			String[] ids = whereIn.split(",");
			for (String id : ids) {
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Emiss�o de Ficha/Etiqueta de Pneu");
				pedidovendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				pedidovendahistorico.setDtaltera(SinedDateUtils.currentTimestamp());
				pedidovendahistorico.setObservacao("Ficha/Etiqueta de Pneu.");
				pedidovendahistorico.setPedidovenda(new Pedidovenda(Integer.parseInt(id)));
				this.saveOrUpdate(pedidovendahistorico);
			}
		}
	}
	
	/**
	 * Cria hist�rico de envio do comprovante do pedido de venda via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Pedidovenda
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(Pedidovenda pedidovenda){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setAcao(Comprovantestatus.ENVIADO.toString());
		pedidovendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		pedidovendahistorico.setObservacao("Comprovante enviado");
		pedidovendahistorico.setPedidovenda(pedidovenda);
		
		pedidovendahistoricoDAO.saveOrUpdate(pedidovendahistorico);
	}
	
	/**
	 * Cria hist�rico de envio do comprovante de uma lista de pedidos de venda via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param List<Pedidovenda>
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(List<Pedidovenda> listaPedidovenda){
		for(Pedidovenda pedidovenda: listaPedidovenda){
			makeHistoricoComprovanteEnviado(pedidovenda);
		}
	}
	
	/**
	 * Cria hist�rico de emiss�o do comprovante do pedido de venda, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Venda
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(Pedidovenda pedidovenda){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setAcao(Comprovantestatus.EMITIDO.toString());
		pedidovendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		pedidovendahistorico.setObservacao("Comprovante emitido");
		pedidovendahistorico.setPedidovenda(pedidovenda);
		
		pedidovendahistoricoDAO.saveOrUpdate(pedidovendahistorico);
	}
	
	/**
	 * Cria hist�rico de emiss�o do comprovante de uma lista de pedidos de venda, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Venda
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(List<Pedidovenda> listaPedidovenda){
		for(Pedidovenda pedidovenda: listaPedidovenda){
			makeHistoricoComprovanteEmitido(pedidovenda);
		}
	}
	
	public void gerarHistoricoNotaRetorno(Notafiscalproduto nota, Pedidovenda pedidovenda){
		Pedidovendahistorico pvh = new Pedidovendahistorico();
		pvh.setPedidovenda(pedidovenda);
		pvh.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		pvh.setDtaltera(new Timestamp(System.currentTimeMillis()));
		pvh.setObservacao("Nota Fiscal de retorno gerada: " + SinedUtil.makeLinkNota(nota));
		pvh.setAcao("Gera��o da nota");
		saveOrUpdate(pvh);
	}
	
	public void makeHistoricoRecalculoFaixaMarkup(String vendas) {
		String[] ids = vendas.split(",");
		
		Pedidovendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Pedidovendahistorico();
			vendahistorico.setAcao("Rec�lculo de markup");
			vendahistorico.setPedidovenda(new Pedidovenda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Faixas de markup recalculadas.");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	public List<PedidoVendaHistoricoWSBean> toWSList(List<Pedidovendahistorico> listaPedidoVendaHistorico){
		List<PedidoVendaHistoricoWSBean> lista = new ArrayList<PedidoVendaHistoricoWSBean>();
		if(Hibernate.isInitialized(listaPedidoVendaHistorico) && SinedUtil.isListNotEmpty(listaPedidoVendaHistorico)){
			for(Pedidovendahistorico pvh: listaPedidoVendaHistorico){
				PedidoVendaHistoricoWSBean bean = new PedidoVendaHistoricoWSBean();
				
				bean.setAcao(pvh.getAcao());
				bean.setCdpedidovendahistorico(pvh.getCdpedidovendahistorico());
				bean.setCdusuarioaltera(pvh.getCdusuarioaltera());
				bean.setDtaltera(pvh.getDtaltera());
				bean.setObservacao(pvh.getObservacao());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvh.getPedidovenda()));
				
				lista.add(bean);
			}
		}
		return lista;
	}
	
	public boolean isExistsHistoricoAprovacao(Pedidovenda bean){
		return pedidovendahistoricoDAO.isExistsHistoricoAprovacao(bean);
	}

	public void registrarAlteracaoServico(Pedidovendamaterial pedidovendamaterial, Material material, Material servicoNovo) {
		try {
			if(Util.objects.isPersistent(pedidovendamaterial) && Util.objects.isPersistent(material) && Util.objects.isPersistent(servicoNovo)){
				String obs = "Altera��o na produ��o: " 
							+ materialService.loadSined(material, "material.cdmaterial, material.nome").getNome()
							+ " por "
							+ materialService.loadSined(servicoNovo, "material.cdmaterial, material.nome").getNome()
							+ ".";
				
				Pedidovendamaterial pvm = pedidovendamaterialService.loadWithPedidovenda(pedidovendamaterial);
				Usuario usuario = SinedUtil.getUsuarioLogado();
				
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Altera��o na produ��o");
				pedidovendahistorico.setPedidovenda(pvm.getPedidovenda());
				pedidovendahistorico.setObservacao(obs);
				pedidovendahistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				pedidovendahistoricoDAO.saveOrUpdate(pedidovendahistorico);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
