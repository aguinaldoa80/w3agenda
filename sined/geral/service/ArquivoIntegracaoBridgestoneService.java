package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoIntegracaoBridgestone;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivoIntegracaoBridgestoneDAO;

public class ArquivoIntegracaoBridgestoneService extends GenericService<ArquivoIntegracaoBridgestone> {

	protected ArquivoIntegracaoBridgestoneDAO arquivoIntegracaoBridgestoneDAO;
	protected ArquivoDAO arquivoDAO;
	public void setArquivoIntegracaoBridgestoneDAO(ArquivoIntegracaoBridgestoneDAO arquivoIntegracaoBridgestoneDAO) {this.arquivoIntegracaoBridgestoneDAO = arquivoIntegracaoBridgestoneDAO;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	
	/* singleton */
	private static ArquivoIntegracaoBridgestoneService instance;
	public static ArquivoIntegracaoBridgestoneService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivoIntegracaoBridgestoneService.class);
		}
		return instance;
	}
	
	public List<ArquivoIntegracaoBridgestone> findForArquivoIntegracaoBridgestoneJob(){
		return arquivoIntegracaoBridgestoneDAO.findForArquivoIntegracaoBridgestoneJob();
	}
	
	public List<Arquivo> findForDownloadZip(String whereIn){
		List<Arquivo> listaArquivos = new ArrayList<Arquivo>();
		for(ArquivoIntegracaoBridgestone aib : arquivoIntegracaoBridgestoneDAO.findForDownloadZip(whereIn)){
			listaArquivos.add(arquivoDAO.loadWithContents(aib.getArquivo()));
		}
		return listaArquivos;
	}
	
}
