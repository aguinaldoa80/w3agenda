package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.dao.EscalacolaboradorDAO;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EscalacolaboradorService extends GenericService<Escalacolaborador> {

	private EscalacolaboradorDAO escalacolaboradorDAO;
	private EscalaService escalaService;
	
	public void setEscalacolaboradorDAO(
			EscalacolaboradorDAO escalacolaboradorDAO) {
		this.escalacolaboradorDAO = escalacolaboradorDAO;
	}
	
	public void setEscalaService(EscalaService escalaService) {
		this.escalaService = escalaService;
	}


	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalacolaborador> findByColaborador(Colaborador colaborador) {
		return escalacolaboradorDAO.findByColaborador(colaborador);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @return
	 * 
	 * @author Tom�s Rabelo
	 */
	public Escalacolaborador findEscalaHorarioDoColaboradorDoPeriodo(Colaborador colaborador, Date dtReferencia, Date dtReferenciaAte) {
		return escalacolaboradorDAO.findEscalaHorarioDoColaboradorDoPeriodo(colaborador, dtReferencia, dtReferenciaAte);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalacolaborador> findListaEscalaHorarioDoColaboradorDoPeriodo(Colaborador colaborador, Date dtReferencia, Date dtReferenciaAte) {
		return escalacolaboradorDAO.findListaEscalaHorarioDoColaboradorDoPeriodo(colaborador, dtReferencia, dtReferenciaAte);
	}

	/**
	 * M�todo para salvar a nova escala do colaborador
	 * 
	 * @see br.com.linkcom.sined.geral.service.EscalacolaboradorService#atualizaDataFimUltimaEscalacolaborador(Escalacolaborador escalacolaborador)
	 * @see br.com.linkcom.sined.geral.service.EscalacolaboradorService#limpaReferencia(Escalacolaborador escalacolaborador)
	 *
	 * @param escalacolaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public String salvaNovaEscalahorarioColaboradorFlex(Escalacolaborador escalacolaborador){
		String erro = "";
		if(escalacolaborador != null && escalacolaborador.getColaborador() != null &&
				escalacolaborador.getEscala() != null && 
				escalacolaborador.getEscala().getListaescalahorario() != null && 
				!escalacolaborador.getEscala().getListaescalahorario().isEmpty()){
			
				boolean achouIntercalado = false;
				for (Escalahorario escalahorario : escalacolaborador.getEscala().getListaescalahorario()) {
					escalahorario.setHorainicio(new Hora(escalahorario.getHorainiciostring()));
					escalahorario.setHorafim(new Hora(escalahorario.getHorafimstring()));
					if(escalahorario.getHorafim().getTime() <= escalahorario.getHorainicio().getTime()){
						erro = "Hora final deve ser maior que hora inicial.";
						break;
					}
					
					for (Escalahorario escalahorarioAux : escalacolaborador.getEscala().getListaescalahorario()) {
						if(escalahorario.getDiasemana() == null || (escalahorario.getDiasemana() != null && escalahorario.getDiasemana().equals(escalahorarioAux.getDiasemana()))){
							escalahorarioAux.setHorainicio(new Hora(escalahorarioAux.getHorainiciostring()));
							escalahorarioAux.setHorafim(new Hora(escalahorarioAux.getHorafimstring()));
							if((escalahorario.getHorainicio().before(escalahorarioAux.getHorafim()) && escalahorario.getHorainicio().after(escalahorarioAux.getHorainicio())) || 
							   (escalahorario.getHorafim().before(escalahorarioAux.getHorafim()) && escalahorario.getHorafim().after(escalahorarioAux.getHorainicio()))){
								erro = "Existem hor�rios intercalados.";
								achouIntercalado = true;
								break;
							}
						}
					}
					if(achouIntercalado)
						break;
				}
				if(!achouIntercalado){		
					Escalacolaborador ultimaEc = this.findUltimaEscalaByColaborador(escalacolaborador.getColaborador());
					if(ultimaEc != null && ultimaEc.getDtfim() == null){
						ultimaEc.setDtfim(SinedDateUtils.addDiasData(escalacolaborador.getDtinicio(), -1));
						this.atualizaDataFimUltimaEscalacolaborador(ultimaEc);
					}
					this.limpaReferencia(escalacolaborador);
					
					escalacolaborador.getEscala().setDiastrabalho(1);
					escalacolaborador.getEscala().setDiasfolga(1);
					escalacolaborador.getEscala().setNaotrabalhadomingo(Boolean.FALSE);
					escalacolaborador.getEscala().setNaotrabalhaferiado(Boolean.FALSE);
					
					try {
						escalaService.saveOrUpdate(escalacolaborador.getEscala());
						escalacolaboradorDAO.saveOrUpdate(escalacolaborador);
					} catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "IDX_ESCALA_NOME"))
							erro += "Escala j� cadastrada no sistema.\n";
					}
				}
		}
		
		return erro;
	}

	/**
	 * M�todo que limpa as refer�ncias da escalacolaborador
	 *
	 * @param escalacolaborador
	 * @author Luiz Fernando
	 */
	private void limpaReferencia(Escalacolaborador escalacolaborador){
		escalacolaborador.setCdescalacolaborador(null);
		escalacolaborador.getEscala().setCdescala(null);
		for(Escalahorario eh : escalacolaborador.getEscala().getListaescalahorario())
			eh.setCdescalahorario(null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EscalacolaboradorDAO#findUltimaEscalaByColaborador(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	private Escalacolaborador findUltimaEscalaByColaborador(Colaborador colaborador){
		return escalacolaboradorDAO.findUltimaEscalaByColaborador(colaborador);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EscalacolaboradorDAO#atualizaDataFimUltimaEscalacolaborador(Escalacolaborador escalacolaborador)
	 *
	 * @param escalacolaborador
	 * @author Luiz Fernando
	 */
	private void atualizaDataFimUltimaEscalacolaborador(Escalacolaborador escalacolaborador){
		escalacolaboradorDAO.atualizaDataFimUltimaEscalacolaborador(escalacolaborador);
	}
}
