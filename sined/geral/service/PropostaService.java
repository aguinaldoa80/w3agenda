package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.BdiForm;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Interacaotipo;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_proposta;
import br.com.linkcom.sined.geral.dao.PropostaDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.PropostaRTF;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PropostaFiltro;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.PropostaReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.text.SimpleDateFormat;

public class PropostaService extends GenericService<Proposta>{
	
	private InteracaoService interacaoService;
	private PropostaDAO propostaDAO;
	private ClienteService clienteService;
	private ContatoService contatoService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private TarefaorcamentoService tarefaorcamentoService;
	private ComposicaoorcamentoService composicaoorcamentoService;
	private BdiService bdiService;
	private InteracaoitemService interacaoitemService;
	
	public void setComposicaoorcamentoService(
			ComposicaoorcamentoService composicaoorcamentoService) {
		this.composicaoorcamentoService = composicaoorcamentoService;
	}
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}
	public void setOrcamentorecursohumanoService(
			OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}
	public void setPropostaDAO(PropostaDAO propostaDAO) {
		this.propostaDAO = propostaDAO;
	}
	public void setBdiService(BdiService bdiService) {
		this.bdiService = bdiService;
	}
	public void setInteracaoitemService(InteracaoitemService interacaoitemService) {
		this.interacaoitemService = interacaoitemService;
	}
	
	/**
	 * Busca a lista de proposta para o combo em flex.
	 * Refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findForComboFlex
	 * @return
	 * @since 05/10/2011
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findForComboFlex(){
		return propostaDAO.findForComboFlex();
	}
	
	@Override
	public void saveOrUpdate(Proposta bean) {
		if (bean.getInteracao() != null) {
			interacaoService.saveOrUpdate(bean.getInteracao());
		}
		super.saveOrUpdate(bean);
		
		callProcedureAtualizaProposta(bean);
	}
	
	/**
	 * M�todo que cria os par�metros e os manipula os bean para cria��o do relat�rio.
	 * 
	 * OBS: M�todo muito grande por causa da impossibilidade de quebra.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PropostaService#findForReport
	 * @see br.com.linkcom.sined.geral.service.PropostaService#setarValoresProposta
	 * @see br.com.linkcom.sined.geral.service.PropostaService#criaListaSub
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @throws Exception 
	 */
	public Resource createRelatorioProposta(PropostaFiltro filtro) throws Exception {
		
		String caminho = null;
		if (filtro.getSimples()) {
			caminho = "/crm/propostasimples";
		} else {
			caminho = "/crm/proposta";
		}
		
		Report report = new Report(caminho);
		Report subreport = new Report("/crm/sub_proposta");
		
		List<Proposta> lista = this.findForReport(filtro);
		
		Integer ganha = 0;
		Integer perdida = 0;
		Integer ee = 0;
		Integer aguardando = 0;
		Integer susp = 0;
		Integer orcadas = 0;
		
		Money zero = new Money();
		
//		DECLARA��O DAS VARAI�VEIS QUE SER�O USADAS NO RELAT�RIO
		Money mobilizacaoganhas = new Money();
		Money mdoganhas = new Money();
		Money materiaisganhas = new Money();
		Money mobilizacaoelaboracao = new Money();
		Money mdoelaboracao = new Money();
		Money materiaiselaboracao = new Money();
		Money mobilizacaoperdidas = new Money();
		Money mdoperdidas = new Money();
		Money materiaisperdidas = new Money();
		Money mobilizacaoaguardando = new Money();
		Money mdoaguardando = new Money();
		Money materiaisaguardando = new Money();
		Money mobilizacaosuspensa = new Money();
		Money mdosuspensa = new Money();
		Money materiaissuspensa = new Money();
		Money totalganhas = new Money();
		Money totalelaboracao = new Money();
		Money totalperdidas = new Money();
		Money totalaguardando = new Money();
		Money totalsuspensa = new Money();
		
		// OR�ADAS S�O TODAS AS PROPOSTAS LISTADAS
		Money mobilizacaoorcadas = new Money();
		Money mdoorcadas = new Money();
		Money materiaisorcadas = new Money();
		Money totalorcadas = new Money();
		
		//Percorre a lista de proposta setando email, telefone e contato (caso nulos)
		//e setando o Status da proposta para cada tipo.
		for (Proposta proposta : lista) {
			
			//Foi feito a query dentro do loop pois quando � carregado junto com todos os dados s� vem o primeiro telefone da lista.
			if (proposta.getContato() != null && proposta.getContato().getCdpessoa() != null) {
				proposta.setContato(contatoService.carregaContato(proposta.getContato()));
			}
			
			if (proposta.getPropostasituacao().equals(Propostasituacao.GANHOU)){
				
//				SETA A ABREVIA��O
				proposta.setStatus("G");
				
//				SOMA N�MERO DE PROPOSTAS
				ganha++;
				
//				SOMAT�RIO DE VALORES E TOTALIZA��O
				mobilizacaoganhas = mobilizacaoganhas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() :zero);
				mdoganhas = mdoganhas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				materiaisganhas = materiaisganhas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
				totalganhas = totalganhas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				totalganhas = totalganhas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				totalganhas = totalganhas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			} else if (proposta.getPropostasituacao().equals(Propostasituacao.PERDIDA)){
				
//				SETA A ABREVIA��O
				proposta.setStatus("P");
				
//				SOMA N�MERO DE PROPOSTAS
				perdida++;
				
//				SOMAT�RIO DE VALORES E TOTALIZA��O
				mobilizacaoperdidas = mobilizacaoperdidas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				mdoperdidas = mdoperdidas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				materiaisperdidas = materiaisperdidas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
				totalperdidas = totalperdidas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				totalperdidas = totalperdidas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				totalperdidas = totalperdidas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			} else if (proposta.getPropostasituacao().equals(Propostasituacao.EM_ELABORACAO)){
				
//				SETA A ABREVIA��O
				proposta.setStatus("EE");
				
//				SOMA N�MERO DE PROPOSTAS
				ee++;
				
//				SOMAT�RIO DE VALORES E TOTALIZA��O
				mobilizacaoelaboracao = mobilizacaoelaboracao.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				mdoelaboracao = mdoelaboracao.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				materiaiselaboracao = materiaiselaboracao.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
				totalelaboracao = totalelaboracao.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				totalelaboracao = totalelaboracao.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				totalelaboracao = totalelaboracao.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			} else if (proposta.getPropostasituacao().equals(Propostasituacao.AGUARDANDO_POSICAO_CLIENTE)){
				
//				SETA A ABREVIA��O
				proposta.setStatus("A");
				
//				SOMA N�MERO DE PROPOSTAS
				aguardando++;
				
//				SOMAT�RIO DE VALORES E TOTALIZA��O
				mobilizacaoaguardando = mobilizacaoaguardando.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				mdoaguardando = mdoaguardando.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				materiaisaguardando = materiaisaguardando.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
				totalaguardando = totalaguardando.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				totalaguardando = totalaguardando.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				totalaguardando = totalaguardando.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			} else if (proposta.getPropostasituacao().equals(Propostasituacao.SUSPENSA) || proposta.getPropostasituacao().equals(Propostasituacao.DECLINADA)){
				
//				SETA A ABREVIA��O
				if(proposta.getPropostasituacao().equals(Propostasituacao.SUSPENSA)){
					proposta.setStatus("S");
				} else if(proposta.getPropostasituacao().equals(Propostasituacao.DECLINADA)){
					proposta.setStatus("D");
				}
				
//				SOMA N�MERO DE PROPOSTAS
				susp++;
				
//				SOMAT�RIO DE VALORES E TOTALIZA��O
				mobilizacaosuspensa = mobilizacaosuspensa.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				mdosuspensa = mdosuspensa.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() :zero);
				materiaissuspensa = materiaissuspensa.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
				totalsuspensa = totalsuspensa.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
				totalsuspensa = totalsuspensa.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
				totalsuspensa = totalsuspensa.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			}
			
			orcadas++;
			mobilizacaoorcadas = mobilizacaoorcadas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
			mdoorcadas = mdoorcadas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
			materiaisorcadas = materiaisorcadas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			totalorcadas = totalorcadas.add(proposta.getAux_proposta().getMobilizacao() != null ? proposta.getAux_proposta().getMobilizacao() : zero);
			totalorcadas = totalorcadas.add(proposta.getAux_proposta().getMdo() != null ? proposta.getAux_proposta().getMdo() : zero);
			totalorcadas = totalorcadas.add(proposta.getAux_proposta().getMateriais() != null ? proposta.getAux_proposta().getMateriais() : zero);
			
			this.setarValoresProposta(proposta);
		}
		
		Long somaValorTotal = this.somaValorTotal(lista);
		
		List<PropostaReportBean> listaBean = this.criaListaSub(lista);
		
		report.addParameter("GANHA", ganha);
		report.addParameter("PERDIDA", perdida);
		report.addParameter("EE", ee);
		report.addParameter("AGUARDANDO", aguardando);
		report.addParameter("SUSPENSA", susp);
		report.addParameter("ORCADAS", orcadas);

		report.addParameter("MOBILIZACAOGANHAS", mobilizacaoganhas);
		report.addParameter("MDOGANHAS", mdoganhas);
		report.addParameter("MATERIAISGANHAS", materiaisganhas);
		
		report.addParameter("MOBILIZACAOPERDIDAS", mobilizacaoperdidas);
		report.addParameter("MDOPERDIDAS", mdoperdidas);
		report.addParameter("MATERIAISPERDIDAS", materiaisperdidas);
		
		report.addParameter("MOBILIZACAOELABORACAO", mobilizacaoelaboracao);
		report.addParameter("MDOELABORACAO", mdoelaboracao);
		report.addParameter("MATERIAISELABORACAO", materiaiselaboracao);
		
		report.addParameter("MOBILIZACAOAGUARDANDO", mobilizacaoaguardando);
		report.addParameter("MDOAGUARDANDO", mdoaguardando);
		report.addParameter("MATERIAISAGUARDANDO", materiaisaguardando);
		
		report.addParameter("MOBILIZACAOSUSPENSA", mobilizacaosuspensa);
		report.addParameter("MDOSUSPENSA", mdosuspensa);
		report.addParameter("MATERIAISSUSPENSA", materiaissuspensa);
		
		report.addParameter("MOBILIZACAOORCADAS", mobilizacaoorcadas);
		report.addParameter("MDOORCADAS", mdoorcadas);
		report.addParameter("MATERIAISORCADAS", materiaisorcadas);
		
		report.addParameter("TOTALGANHAS", totalganhas);
		report.addParameter("TOTALELABORACAO", totalelaboracao);
		report.addParameter("TOTALPERDIDAS", totalperdidas);
		report.addParameter("TOTALAGUARDANDO", totalaguardando);
		report.addParameter("TOTALSUSPENSA", totalsuspensa);
		report.addParameter("TOTALORCADAS", totalorcadas);
		report.addParameter("SOMAVALORTOTAL", somaValorTotal.doubleValue() / 100);
		
		report.setDataSource(lista);
		subreport.setDataSource(listaBean);
		
		report.addParameter("TITULO", "CONTROLE DE PROPOSTAS");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		Empresa empresa = EmpresaService.getInstance().loadPrincipal();
		Image image = SinedUtil.getLogo(empresa);
		report.addParameter("EMPRESA", empresa.getRazaosocialOuNome());
		report.addParameter("LOGO", image);
		subreport.addParameter("LOGO", image);
		subreport.addParameter("EMPRESA", empresa.getRazaosocialOuNome());

		subreport.addParameter("TITULO", "CONTROLE DE PROPOSTAS");
		subreport.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		subreport.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		MergeReport mergeReport = new MergeReport("proposta_"+SinedUtil.datePatternForReport()+".pdf");
		mergeReport.addReport(report);
		mergeReport.addReport(subreport);
		
		return mergeReport.generateResource();
	}
	
	
	/**
	 * Seta os valores do bean proposta para que seja criado o relatorio.
	 * 
	 * @param proposta
	 * @author Rodrigo Freitas
	 */
	private void setarValoresProposta(Proposta proposta) {
		proposta.setValor(proposta.getAux_proposta().getValor());
		proposta.setEntrega(proposta.getEntrega() != null ? proposta.getEntrega() : null);
		proposta.setMateriais(proposta.getAux_proposta().getMateriais());
		proposta.setMdo(proposta.getAux_proposta().getMdo());
		proposta.setMobilizacao(proposta.getAux_proposta().getMobilizacao());
		
		StringBuilder sb = new StringBuilder();
		try{sb.append("PTCM ").append(proposta.getNumero().toString()).append("/").append(proposta.getSufixo().toString());}catch (Exception e) {}
		try{sb.append("\n").append(proposta.getAux_proposta().getRevisaoptm().toString());}catch (Exception e) {}
		try{sb.append("\n").append(proposta.getAux_proposta().getRevisaopcm().toString());}catch (Exception e) {}
		proposta.setNumerorevisao(sb.toString());
	}
	
	
	/**
	 * Cria a lista para o sub relat�rio de proposta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ClienteService#findForPropostaReport
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<PropostaReportBean> criaListaSub(List<Proposta> lista) {
		List<PropostaReportBean> listaBean = new ArrayList<PropostaReportBean>();
		PropostaReportBean bean = null;
		if (lista != null && lista.size() > 0) {
			List<Cliente> listaCliente = clienteService.findForPropostaReport(CollectionsUtil.listAndConcatenate(lista, "cdproposta", ","));
	
			for (Cliente cliente : listaCliente) {
				bean = new PropostaReportBean();
				bean.setCliente(cliente.getNome());
				for (Proposta proposta : cliente.getListaProposta()) {
					if (proposta.getPropostasituacao().equals(Propostasituacao.GANHOU)) {
						bean.adicionaGanhas();
					}
					if (proposta.getPropostasituacao().equals(Propostasituacao.EM_ELABORACAO)) {
						bean.adicionaElaboracao();
					}
					if (proposta.getPropostasituacao().equals(Propostasituacao.AGUARDANDO_POSICAO_CLIENTE)) {
						bean.adicionaAguardando();
					}
					if (proposta.getPropostasituacao().equals(Propostasituacao.PERDIDA)) {
						bean.adicionaPerdidas();
					}
					if (proposta.getPropostasituacao().equals(Propostasituacao.SUSPENSA) || proposta.getPropostasituacao().equals(Propostasituacao.DECLINADA)) {
						bean.adicionaSuspensa();
					}
					bean.adicionaTotal();
				}
				listaBean.add(bean);
			}
		}
		return listaBean;
	}
	
	/**
	 * Faz refr�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findForReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Proposta> findForReport(PropostaFiltro filtro) {
		return propostaDAO.findForReport(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findByProspeccao
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findByProspeccao(Prospeccao prospeccao){
		return propostaDAO.findByProspeccao(prospeccao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findForVerificacao
	 * @param propostacaixa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findForVerificacao(Proposta proposta)  {
		return propostaDAO.findForVerificacao(proposta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#updateInteracao
	 * @param proposta
	 * @author Rodrigo Freitas
	 */
	public void updateInteracao(Proposta proposta) {
		propostaDAO.updateInteracao(proposta);		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#loadWithInteracao
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadWithInteracao(Proposta proposta){
		return propostaDAO.loadWithInteracao(proposta);
	}
	
	/**
	 * Cria relat�rio de Conte�do Caixa.
	 * @see #findListaProposta(String)
	 * @param whereIn
	 * @return
	 * @author Simon Gontijo
	 */
	public IReport createConteudoCaixaReport(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Report report = new Report("/crm/conteudocaixa");
		
		List<Proposta> listaProposta = this.findListaProposta(whereIn);
		if (listaProposta == null || listaProposta.size() == 0) throw new SinedException("Nenhuma caixa encontrada.");
		
		report.setDataSource(listaProposta);
		return report;
	}
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findListaProposta(String)
	 * @param whereIn
	 * @return
	 * @author Simon Gontijo
	 */
	private List<Proposta> findListaProposta(String whereIn) {
		return propostaDAO.findListaProposta(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findUltimoNumero
	 * @return
	 * @author Rodrigo Freitas
	 * @param sufixo 
	 */
	public Integer findUltimoNumero(String sufixo){
		return propostaDAO.findUltimoNumero(sufixo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#loadWithView
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadWithView(Proposta proposta) {
		return propostaDAO.loadWithView(proposta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#findByPropostacaixa
	 * @param propostacaixa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findByPropostacaixa(Propostacaixa propostacaixa) {
		return propostaDAO.findByPropostacaixa(propostacaixa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#callProcedureAtualizaProposta
	 * @param proposta
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaProposta(Proposta proposta){
		propostaDAO.callProcedureAtualizaProposta(proposta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#loadForContrato
	 * 
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadForContrato(Proposta proposta) {
		return propostaDAO.loadForContrato(proposta);
	}
	
	
	/**
	 * Somat�rio do valor total de propostas
	 * @param proposta
	 * @return
	 * @author Taidson
	 * @since 10/06/2010
	 */
	public Long somaValorTotal (List<Proposta> listaPropostas){
		Long somaValorTotal = 0L;
		for (Proposta item : listaPropostas) {
			somaValorTotal += propostaDAO.somaValorTotal(item);			
		}
		return somaValorTotal;
	}
	
	public boolean havePropostaCliente(Cliente cliente) {
		return propostaDAO.havePropostaCliente(cliente);
	}
	
	/**
	 * Faz refer�ncia DAO.
	 * @param cliente
	 * @param empresa
	 * @return List<Proposta>
	 * @author Taidson
	 * @since 16/09/2010
	 */
	public List<Proposta> findByCliente(Cliente cliente, Empresa empresa) {
		return propostaDAO.findByCliente(cliente, empresa);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* utilizado para o combo proposta, exibe o n�mero e o ano da proposta
	*
	* @see	br.com.linkcom.sined.geral.dao.PropostaDAO#findNumeroAnoProposta()
	*
	* @return
	* @since Aug 25, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Proposta> findNumeroAnoProposta() {
		return propostaDAO.findNumeroAnoProposta();
	}
	
	/**
	 * Busca o valor de MDO da revis�o de uma proposta a partir de um or�amento.
	 * � a soma de M�o-de-obra direta e indireta.
	 *
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMOD
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMOI
	 * 
	 * @param orcamento
	 * @return
	 * @since 06/10/2011
	 * @author Rodrigo Freitas
	 */
	public Money getValorMDO(Orcamento orcamento) {
		Double mod = orcamentorecursohumanoService.calculaCustoTotalMOD(orcamento);
		Double moi = orcamentorecursohumanoService.calculaCustoTotalMOI(orcamento);
		
		return new Money(mod + moi);
	}
	
	/**
	 * Busca o valor de Materiais da revis�o de uma proposta a partir de um or�amento.
	 * � a soma de recursos gerais e composi��es indiretas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#getTotalRecursosGeraisDireto
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#findByOrcamento 
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#calculaCustoTotalComposicao
	 *
	 * @param orcamento
	 * @return
	 * @since 06/10/2011
	 * @author Rodrigo Freitas
	 */
	public Money getValorMateriais(Orcamento orcamento) {
		Double recursoGeral = tarefaorcamentoService.getTotalRecursosGeraisDireto(orcamento);
		
		List<Composicaoorcamento> listaComposicaoOrcamento = composicaoorcamentoService.findByOrcamento(orcamento);
		Double composicaoIndireta = 0.0;
		for (Composicaoorcamento composicaoorcamento : listaComposicaoOrcamento) {
			composicaoIndireta += composicaoorcamentoService.calculaCustoTotalComposicao(composicaoorcamento);
		}
		return new Money(recursoGeral + composicaoIndireta);
	}
	
	/**
	 * Busca o valor de Materiais da revis�o de uma proposta a partir de um or�amento.
	 * � a soma de outras tarefas e acrescimos das tarefas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#getTotalOutrasTarefas
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#getTotalAcrescimo
	 *
	 * @param orcamento
	 * @return
	 * @since 06/10/2011
	 * @author Rodrigo Freitas
	 */
	public Money getValorMobilizacao(Orcamento orcamento) {
//		Double outrasTarefas = tarefaorcamentoService.getTotalOutrasTarefas(orcamento);
//		Double acresimoTarefas = tarefaorcamentoService.getTotalAcrescimo(orcamento);

		Money valoresMateriais = this.getValorMateriais(orcamento);
		Money valoresMDO = this.getValorMDO(orcamento);
		BdiForm bdiForm = bdiService.getByOrcamentoFlex(orcamento);		
		
		Double total = bdiForm.getPrecoComFolga().getValue().doubleValue() - valoresMateriais.getValue().doubleValue() - valoresMDO.getValue().doubleValue();
		
		return new Money(total);
	}
	
	/* singleton */
	private static PropostaService instance;
	public static PropostaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PropostaService.class);
		}
		return instance;
	}
	
	/**
	 * 
	 * M�todo que busca o contrato do banco e logo depois transforma a Proposta em um ContratoRTF.
	 *
	 *@author Thiago Augusto
	 *@date 06/02/2012
	 * @param contrato
	 * @return
	 */
	public PropostaRTF makePropostaRTF(Proposta proposta) {
		proposta = this.carregarProposta(proposta);
		return proposta.getPropostaRTF();
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 06/02/2012
	 * @param proposta
	 * @return
	 */
	public Proposta carregarProposta(Proposta proposta){
		return propostaDAO.carregarProposta(proposta);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#atualizaSituacao(Proposta proposta, Propostasituacao propostasituacao)
	 *
	 * @param proposta
	 * @param propostasituacao
	 * @author Luiz Fernando
	 */
	public void atualizaSituacao(Proposta proposta, Propostasituacao propostasituacao){
		propostaDAO.atualizaSituacao(proposta, propostasituacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PropostaDAO#loadForNotafiscalservico(Proposta proposta)
	 *
	 * @param proposta
	 * @return
	 * @author Luiz Fernando
	 */
	public Proposta loadForNotafiscalservico(Proposta proposta) {
		return propostaDAO.loadForNotafiscalservico(proposta);
	}
	
	/**
	 * M�todo que registar intera��o do cria��o do contrato
	 *
	 * @param proposta
	 * @param contrato
	 * @author Luiz Fernando
	 */
	public void criaInteracaoByContrato(Proposta proposta, Contrato contrato) {
		try {
			if(proposta != null && proposta.getCdproposta() != null && contrato != null && contrato.getCdcontrato() != null){
				Interacaoitem interacaoitem = new Interacaoitem();
				interacaoitem.setDe(SinedUtil.getUsuarioLogado().getNome());
				interacaoitem.setData(new java.sql.Date(System.currentTimeMillis()));
				Interacaotipo interacaotipo = new Interacaotipo();
				interacaotipo.setCdinteracaotipo(6);
				interacaoitem.setInteracaoTipo(interacaotipo);
				interacaoitem.setAssunto("Cria��o do contrato " + contrato.getCdcontrato() +".");
				
				Proposta p = this.loadWithInteracao(proposta);
				Interacao interacao = null;
				if(p.getInteracao() != null){
					interacao = p.getInteracao();	
					interacaoitem.setInteracao(interacao);
					interacaoitemService.saveOrUpdate(interacaoitem);
				}else {
					interacao = new Interacao();
					interacao.setListaInteracaoitem(new ArrayList<Interacaoitem>());
					interacao.getListaInteracaoitem().add(interacaoitem);
					interacaoService.saveOrUpdate(interacao);
					
					proposta.setInteracao(interacao);
					this.updateInteracao(proposta);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * M�todo que registar intera��o do cria��o da nota
	 *
	 * @param whereInProposta
	 * @param notaFiscalServico
	 * @author Luiz Fernando
	 */
	public void criaInteracaoByNota(String whereInProposta, NotaFiscalServico notaFiscalServico) {
		try {
			if(whereInProposta != null && !"".equals(whereInProposta) && notaFiscalServico != null && notaFiscalServico.getCdNota() != null){
				String[] ids = whereInProposta.split(",");
				
				if(ids.length > 0){
					Proposta proposta;
					for(String cdpropostastr : ids){
						Interacaoitem interacaoitem = new Interacaoitem();
						interacaoitem.setDe(SinedUtil.getUsuarioLogado().getNome());
						interacaoitem.setData(new java.sql.Date(System.currentTimeMillis()));
						Interacaotipo interacaotipo = new Interacaotipo();
						interacaotipo.setCdinteracaotipo(6);
						interacaoitem.setInteracaoTipo(interacaotipo);
						interacaoitem.setAssunto("Cria��o da nota " + notaFiscalServico.getCdNota() + ".");
						
						proposta = new Proposta();
						proposta.setCdproposta(Integer.parseInt(cdpropostastr));
						proposta = this.loadWithInteracao(proposta);
						Interacao interacao = null;
						if(proposta.getInteracao() != null){
							interacao = proposta.getInteracao();	
							interacaoitem.setInteracao(interacao);
							interacaoitemService.saveOrUpdate(interacaoitem);
						}else {
							interacao = new Interacao();
							interacao.setListaInteracaoitem(new ArrayList<Interacaoitem>());
							interacao.getListaInteracaoitem().add(interacaoitem);
							interacaoService.saveOrUpdate(interacao);
							
							proposta.setInteracao(interacao);
							this.updateInteracao(proposta);
						}
					}
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Resource gerarListagemCSV(FiltroListagem filtro) {
		ListagemResult<Proposta> listagemResult = findForExportacao(filtro);
		List<Proposta> lista = listagemResult.list();
		
		SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
		String cabecalho = "\"N�mero/ano\nRevis�o\";\"Validade\";\"Data de entrega\";\"Convite\";\"Cliente\";\"Descri��o\";\"Localidade\";\"Valor total\";\"Situa��o\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Proposta bean : lista) {
			if (bean.getAux_proposta() != null) {
				Aux_proposta aux = bean.getAux_proposta();
				
				csv.append("\"" + (bean.getNumeroano() != null ? bean.getNumeroano() : "") + "\n" + 
						(aux.getRevisaoptm() != null ? aux.getRevisaoptm() : "") + "\n" +
						(aux.getRevisaopcm() != null ? aux.getRevisaopcm() : "") + "\";");
				csv.append("\"" + (aux.getDtvalidade() != null ? pattern.format(aux.getDtvalidade()) : "") + "\";");
			} else {
				csv.append("\"" + bean.getNumeroano() +"\";\"\";");
			}
			
			csv.append("\"" + (bean.getEntrega() != null ? bean.getEntrega() : "") + "\";");
			csv.append("\"" + (bean.getNumeroconvite() != null ? bean.getNumeroconvite() : "") + "\";");
			csv.append("\"" + (bean.getCliente() != null ? bean.getCliente().getNome() : "") + "\";");
			csv.append("\"" + (bean.getDescricao() != null ? bean.getDescricao() : "") + "\";");
			csv.append("\"" + (bean.getLocalidade() != null ? bean.getLocalidade() : "") + "\";");
			csv.append("\"" + (bean.getAux_proposta() != null ? 
					bean.getAux_proposta().getValor() != null ? bean.getAux_proposta().getValor() : "" : "") + "\";");
			csv.append("\"" + (bean.getPropostasituacao().getNome() != null ? bean.getPropostasituacao().getNome() : "") + "\";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "proposta.csv", csv.toString().getBytes());
		return resource;
	}
	
	
	public Boolean verificaPropostaPorContato(Contato contato){
		return propostaDAO.verificaPropostaPorContato(contato);
	}
}
