package br.com.linkcom.sined.geral.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.view.Vwmovimentacaomensal;
import br.com.linkcom.sined.geral.dao.VwmovimentacaomensalDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwmovimentacaomensalService extends GenericService<Vwmovimentacaomensal> {

	private VwmovimentacaomensalDAO vwmovimentacaomensalDAO;
	
	public void setVwmovimentacaomensalDAO(VwmovimentacaomensalDAO vwmovimentacaomensalDAO) {
		this.vwmovimentacaomensalDAO = vwmovimentacaomensalDAO;
	}
	
	/**
	 * Refer�ncia ao DAO.
	 * 
	 * @param listaMesAno
	 * @return
	 */
	public List<Vwmovimentacaomensal> findByMesAno(List<String> listaMesAno) {
		return vwmovimentacaomensalDAO.findByMesAno(listaMesAno);
	}
	
	/**
	 * Retorna as movimenta��es conciliadas de receita e despesa 
	 * dos 12 �ltimos meses at� o m�s atual. 
	 * 
	 * @return
	 */
	public List<Vwmovimentacaomensal> findForGraficoBalancoMovimentacaoFlex() {
		List<String> listaMesAno = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM/yy");
		
		for (int i = 0; i < 12; i++) {
			listaMesAno.add(StringUtils.stringCheia(String.valueOf(cal.get(Calendar.MONTH)+1), "0", 2, false) + "/" + String.valueOf(cal.get(Calendar.YEAR)));
			cal.add(Calendar.MONTH,-1);
		}
		List<Vwmovimentacaomensal> listaVwmovimentacaomensal = this.findByMesAno(listaMesAno);
		if (listaVwmovimentacaomensal != null) {
			completaMesVazio(listaVwmovimentacaomensal);
			createMenorByTipooperacao(listaVwmovimentacaomensal, Tipooperacao.TIPO_CREDITO);
			createMenorByTipooperacao(listaVwmovimentacaomensal, Tipooperacao.TIPO_DEBITO);
			
			// Converte os valores do n�mero do m�s em descri��o
			for (Vwmovimentacaomensal vwmovimentacaomensal : listaVwmovimentacaomensal) {
				if (vwmovimentacaomensal.getMesano().length() == 7) {
					Integer mes = Integer.parseInt(vwmovimentacaomensal.getMesano().substring(0,2));
					Integer ano = Integer.parseInt(vwmovimentacaomensal.getMesano().substring(3,7));
					cal.set(Calendar.MONTH, mes-1);
					cal.set(Calendar.YEAR, ano);
					vwmovimentacaomensal.setDescMesAno(simpleDateFormat.format(cal.getTime()));
					vwmovimentacaomensal.setDataMov(new Date(cal.getTimeInMillis()));
				}
			}
			
			Collections.sort(listaVwmovimentacaomensal,new Comparator<Vwmovimentacaomensal>(){
				public int compare(Vwmovimentacaomensal o1, Vwmovimentacaomensal o2) {
					try {
						return o1.getDataMov().compareTo(o2.getDataMov());
					} catch (Exception e) {
						return -1;
					} 
				}
			});
		}
		return listaVwmovimentacaomensal;
	}

	private void completaMesVazio(List<Vwmovimentacaomensal> listaVwmovimentacaomensal) {
		Map<String, Vwmovimentacaomensal> mapCredito = new HashMap<String, Vwmovimentacaomensal>();
		Map<String, Vwmovimentacaomensal> mapDebito = new HashMap<String, Vwmovimentacaomensal>();
		
		for (Vwmovimentacaomensal vwmovimentacaomensal : listaVwmovimentacaomensal) {
			if(vwmovimentacaomensal.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				mapCredito.put(vwmovimentacaomensal.getMesano(), vwmovimentacaomensal);
			} else {
				mapDebito.put(vwmovimentacaomensal.getMesano(), vwmovimentacaomensal);
			}
		}
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM/yy");
		
		Set<Entry<String, Vwmovimentacaomensal>> entrySetCredito = mapCredito.entrySet();
		for (Entry<String, Vwmovimentacaomensal> entry : entrySetCredito) {
			String mesAno = entry.getKey();
			if(!mapDebito.containsKey(mesAno)){
				Integer mes = Integer.parseInt(mesAno.substring(0,2));
				Integer ano = Integer.parseInt(mesAno.substring(3,7));
				
				cal.set(Calendar.MONTH, mes-1);
				cal.set(Calendar.YEAR, ano);
				
				Vwmovimentacaomensal vwmovimentacaomensal = new Vwmovimentacaomensal();
				vwmovimentacaomensal.setId(ano + "" + mes + "" + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() + "");
				vwmovimentacaomensal.setDescMesAno(simpleDateFormat.format(cal.getTime()));
				vwmovimentacaomensal.setMesano(mesAno);
				vwmovimentacaomensal.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				vwmovimentacaomensal.setValor(new Money());
				
				listaVwmovimentacaomensal.add(vwmovimentacaomensal);
			}
		}
		
		Set<Entry<String, Vwmovimentacaomensal>> entrySetDebito = mapDebito.entrySet();
		for (Entry<String, Vwmovimentacaomensal> entry : entrySetDebito) {
			String mesAno = entry.getKey();
			if(!mapCredito.containsKey(mesAno)){
				Integer mes = Integer.parseInt(mesAno.substring(0,2));
				Integer ano = Integer.parseInt(mesAno.substring(3,7));
				
				cal.set(Calendar.MONTH, mes-1);
				cal.set(Calendar.YEAR, ano);
				
				Vwmovimentacaomensal vwmovimentacaomensal = new Vwmovimentacaomensal();
				vwmovimentacaomensal.setId(ano + "" + mes + "" + Tipooperacao.TIPO_CREDITO.getCdtipooperacao() + "");
				vwmovimentacaomensal.setDescMesAno(simpleDateFormat.format(cal.getTime()));
				vwmovimentacaomensal.setMesano(mesAno);
				vwmovimentacaomensal.setTipooperacao(Tipooperacao.TIPO_CREDITO);
				vwmovimentacaomensal.setValor(new Money());
				
				listaVwmovimentacaomensal.add(vwmovimentacaomensal);
			}
		}
		
	}

	private void createMenorByTipooperacao(List<Vwmovimentacaomensal> listaVwmovimentacaomensal, Tipooperacao tipooperacao) {
		if(!haveOnlyOneMesAnoByTipooperacao(listaVwmovimentacaomensal, tipooperacao)){
			return;
		}
		
		Integer mesMenor = null;
		Integer anoMenor = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM/yy");
		
		for (Vwmovimentacaomensal vwmovimentacaomensal : listaVwmovimentacaomensal) {
			if(vwmovimentacaomensal.getTipooperacao().equals(tipooperacao)){
				Integer mes = Integer.parseInt(vwmovimentacaomensal.getMesano().substring(0,2));
				Integer ano = Integer.parseInt(vwmovimentacaomensal.getMesano().substring(3,7));
				if(mesMenor == null && anoMenor == null){
					mesMenor = mes;
					anoMenor = ano;
				} else if(ano < anoMenor){
					mesMenor = mes;
					anoMenor = ano;
				} else if(ano.equals(anoMenor) && mes < mesMenor){
					mesMenor = mes;
					anoMenor = ano;
				}
			}
		}
		
		if(mesMenor != null && anoMenor != null){
			if(mesMenor.equals(1)){
				mesMenor = 12;
				anoMenor = anoMenor - 1;
			} else {
				mesMenor = mesMenor - 1;
			}
			
			cal.set(Calendar.MONTH, mesMenor-1);
			cal.set(Calendar.YEAR, anoMenor);
			
			String mesMenorStr = StringUtils.stringCheia(mesMenor + "", "0", 2, false);
			
			Vwmovimentacaomensal vwmovimentacaomensal = new Vwmovimentacaomensal();
			vwmovimentacaomensal.setId(anoMenor + "" + mesMenorStr + "" + tipooperacao.getCdtipooperacao() + "");
			vwmovimentacaomensal.setDescMesAno(simpleDateFormat.format(cal.getTime()));
			vwmovimentacaomensal.setMesano(mesMenorStr + "/" + anoMenor);
			vwmovimentacaomensal.setTipooperacao(tipooperacao);
			vwmovimentacaomensal.setValor(new Money());
			
			listaVwmovimentacaomensal.add(vwmovimentacaomensal);
		}
	}

	private boolean haveOnlyOneMesAnoByTipooperacao(List<Vwmovimentacaomensal> listaVwmovimentacaomensal, Tipooperacao tipooperacao) {
		String mesAno = null;
		for (Vwmovimentacaomensal vwmovimentacaomensal : listaVwmovimentacaomensal) {
			if(vwmovimentacaomensal.getTipooperacao().equals(tipooperacao)){
				if(mesAno == null){
					mesAno = vwmovimentacaomensal.getMesano();
				} else if(!mesAno.equals(vwmovimentacaomensal.getMesano())){
					return false;
				}
			}
		}
		return true;
	}	

}
