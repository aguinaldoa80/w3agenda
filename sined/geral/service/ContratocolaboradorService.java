package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.dao.ContratocolaboradorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratocolaboradorService extends GenericService<Contratocolaborador> {

	private ContratocolaboradorDAO contratocolaboradorDAO;
	
	public void setContratocolaboradorDAO(ContratocolaboradorDAO contratocolaboradorDAO) {this.contratocolaboradorDAO = contratocolaboradorDAO;}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratocolaboradorDAO#findForEntradaByContrato(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<Contratocolaborador> findForEntradaByContrato(String whereIn) {
		return contratocolaboradorDAO.findForEntradaByContrato(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratocolaboradorDAO#findByContrato
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratocolaborador> findByContrato(Contrato contrato) {
		return contratocolaboradorDAO.findByContrato(contrato);
	}

	/**
	 * Salva a lista de <code>Contratocolaborador</code>.
	 *
	 * @param listaContrato
	 * @author Rodrigo Freitas
	 */
	public void saveLista(final List<Contratocolaborador> listaContrato) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Contratocolaborador cc : listaContrato) {
					contratocolaboradorDAO.saveOrUpdateNoUseTransaction(cc);
				}
				return null;
			}
		});
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratocolaboradorDAO#findByColaboradorContrato
	 * 
	 * @param colaborador
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratocolaborador> findByColaboradorContrato(Colaborador colaborador, Contrato contrato) {
		return contratocolaboradorDAO.findByColaboradorContrato(colaborador, contrato);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param whereInColaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contratocolaborador> findColaboradorNoContrato(String whereIn, String whereInColaborador) {
		return contratocolaboradorDAO.findColaboradorNoContrato(whereIn, whereInColaborador);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ContratocolaboradorDAO#findContratocolaboradorByContrato(Contrato contrato)
	 * 	 
	 * @param contrato
	 * @return
	 */
	public List<Contratocolaborador> findContratocolaboradorByContrato(Contrato contrato) {
		return contratocolaboradorDAO.findContratocolaboradorByContrato(contrato);
	}
}
