package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.FornecedorDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.DominiointegracaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.sined.util.rest.android.fornecedor.FornecedorRESTModel;
import br.com.linkcom.sined.util.tag.TagFunctions;

public class FornecedorService extends GenericService<Fornecedor>{

	private FornecedorDAO fornecedorDAO;
	private ParametrogeralService parametrogeralService;
	private EmpresaService empresaService;
	private ArquivoService arquivoService;
	private PessoaquestionarioService pessoaquestionarioService;
	
	public void setFornecedorDAO(FornecedorDAO fornecedorDAO) {
		this.fornecedorDAO = fornecedorDAO;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {
		this.pessoaquestionarioService = pessoaquestionarioService;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#carregaFornecedor(Fornecedor)
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Fornecedor carregaFornecedor(Fornecedor bean){
		return fornecedorDAO.carregaFornecedor(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#findListagem
	 * 
	 * @param whereIn
	 * @param orderby
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecedor> findListagem(String whereIn, String orderby, boolean asc){
		return fornecedorDAO.findListagem(whereIn, orderby, asc);
	}
	
	/**
	 * M�todo para carregar os dados de um Fornecedor pelo c�digo.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#load(Fornecedor)
	 * @param cdpessoa
	 * @return
	 * @author Flavio Tavares
	 */
	public Fornecedor load(Integer cdpessoa){
		if(cdpessoa==null){
			throw new SinedException("O par�metro cdpessoa n�o pode ser null.");
		}
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setCdpessoa(cdpessoa);
		return fornecedorDAO.load(fornecedor);
	}
	
		
	/**
	 * M�todo para verificar se existe fornecedor cadastrado com o cnpj.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#countFornecedorCnpj(Fornecedor)
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean existeCnpjFornecedor(Fornecedor bean){
		Integer count=fornecedorDAO.countFornecedorCnpj(bean);
		return count!=0;
	}
	
	/**
	 * Renorna uma lista de fornecedor contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Fornecedor> findDescricao(String whereIn) {
		return fornecedorDAO.findDescricao(whereIn);
	}

	/**
	 * Retorna os nomes dos Fornecedores, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 *  
	 * @see #findDescricao(String)
	 * @param listaCentrocusto
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeFornecedores(List<ItemDetalhe> listaFornecedor) {
		if (listaFornecedor == null || listaFornecedor.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);
		
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/* singleton */
	private static FornecedorService instance;
	public static FornecedorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FornecedorService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#exiteFornecedorLogin
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean exiteFornecedorLogin(Usuario bean) {
		return fornecedorDAO.exiteFornecedorLogin(bean);
	}
	
	/**
	 * M�todo que carrega os fornecedores de uma categoria espec�fica.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#procurarFornecedoresPorCategoria(Categoria)
	 * @param categoria
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Fornecedor> procurarFornecedoresPorCategoria(Categoria categoria) {
		return fornecedorDAO.procurarFornecedoresPorCategoria(categoria);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForBoleto
	 * 
	 * @param cdpessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForBoleto(Integer cdpessoa) {
		return fornecedorDAO.loadForBoleto(cdpessoa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresForBuscaGeral(String busca) {
		return fornecedorDAO.findFornecedoresForBuscaGeral(busca);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereLike
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresAutoComplete(String whereLike){
		return fornecedorDAO.findFornecedoresAutoComplete(whereLike);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereLike
	 * @return
	 * @author Filipe Santos
	 */
	public List<Fornecedor> findAllFornecedoresAutoComplete(String whereLike){
		return fornecedorDAO.findAllFornecedoresAutoComplete(whereLike);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#findFornecedoresNomeCnpj(String whereLike)
	 *
	 * @param whereLike
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresNomeCnpj(String whereLike) {
		return fornecedorDAO.findFornecedoresNomeCnpj(whereLike);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param mes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresAniversariantesDoMesFlex(Integer mes){
		return fornecedorDAO.findFornecedoresAniversariantesDoMesFlex(mes);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#insertSomenteFornecedor
	 *
	 * @param fornecedor
	 * @author Rodrigo Freitas
	 */
	public void insertSomenteFornecedor(Fornecedor fornecedor) {
		fornecedorDAO.insertSomenteFornecedor(fornecedor);
	}

	public boolean verificaOutrosRegistros(String itens) {
		return fornecedorDAO.verificaOutrosRegistros(itens);
	}
	
	/**
	 * M�todo respons�vel por manipular dodos da listagem e montar o arquivo CSV
	 * da listagem de Fornecedor
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public Resource gerarRelarorioCSVListagem(FiltroListagem filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Fornecedor> listagemResult = this.listagemFornecedorCSV(filtro);
		List<Fornecedor> listaFornecedor = listagemResult.list();
		
		List<Pessoaquestionario> listaPessoaquestionario;
		Integer quantidade, pontos, quantidadeNotasMedia;
		
		StringBuilder csv = new StringBuilder();
		csv.append("Nome fantasia;Tipo de pessoa;CNPJ/CPF;Telefones;Categorias;Ativo;Nota;\n");
		FornecedorFiltro filtroFornecedor = (FornecedorFiltro) filtro;
		
		for (Fornecedor fornecedor : listaFornecedor) {
			
			if(filtroFornecedor.getExibirNotaMediaCsv()){
				quantidade = 0;
				pontos= 0;
				listaPessoaquestionario = pessoaquestionarioService.findForFornecedorCSVListagem(fornecedor.getCdpessoa().toString(), (FornecedorFiltro) filtro);
				if (listaPessoaquestionario != null && listaPessoaquestionario.size() > 0){
					quantidadeNotasMedia = listaPessoaquestionario.get(0).getQuestionario().getQuantidadeNotasMedia();
					for (Pessoaquestionario pessoaquestionario: listaPessoaquestionario){
						if (quantidadeNotasMedia != null && quantidadeNotasMedia.equals(quantidade)){
							break;
						}
						pontos += pessoaquestionario.getPontuacao();
						quantidade ++;
					}
				}
				  
				Double pontuacao = quantidade.equals(0) ? 0.0 : (((double) pontos)/ quantidade);

				csv
				.append(fornecedor.getNome()).append(";")
				.append(fornecedor.getTipopessoa()).append(";")
				.append(fornecedor.getCpfOuCnpj() != null ? fornecedor.getCpfOuCnpj() : "").append(";")
				.append(fornecedor.getTelefones().replace("\n", " ")).append(";")
				.append(fornecedor.getCategorias()).append(";")
				.append(fornecedor.getAtivo() == true ? "Sim" : "N�o").append(";")
				.append(pontuacao!=null ? new DecimalFormat("0.00").format(pontuacao) : 0).append(";")
				.append("\n");
			}else{
				listaPessoaquestionario = pessoaquestionarioService.findForFornecedorCSVListagem(fornecedor.getCdpessoa().toString(), (FornecedorFiltro) filtro);
				if (listaPessoaquestionario != null && listaPessoaquestionario.size() > 0){
					for (Pessoaquestionario pessoaquestionario: listaPessoaquestionario){
						csv
						.append(fornecedor.getNome()).append(";")
						.append(fornecedor.getTipopessoa()).append(";")
						.append(fornecedor.getCpfOuCnpj() != null ? fornecedor.getCpfOuCnpj() : "").append(";")
						.append(fornecedor.getTelefones().replace("\n", " ")).append(";")
						.append(fornecedor.getCategorias()).append(";")
						.append(fornecedor.getAtivo() == true ? "Sim" : "N�o").append(";")
						.append(pessoaquestionario!=null ? new DecimalFormat("0.00").format(pessoaquestionario.getPontuacao()) : 0).append(";")
						.append("\n");
					}
				}else{
					Double pontuacao = 0.0;
					csv
					.append(fornecedor.getNome()).append(";")
					.append(fornecedor.getTipopessoa()).append(";")
					.append(fornecedor.getCpfOuCnpj() != null ? fornecedor.getCpfOuCnpj() : "").append(";")
					.append(fornecedor.getTelefones().replace("\n", " ")).append(";")
					.append(fornecedor.getCategorias()).append(";")
					.append(fornecedor.getAtivo() == true ? "Sim" : "N�o").append(";")
					.append(pontuacao!=null ? new DecimalFormat("0.00").format(pontuacao) : 0).append(";")
					.append("\n");
				}
			}			
		}
		
		Resource resource = new Resource("text/csv", "fornecedor_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * Faz refer�ncia a DAO.
	 * @param _filtro
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public ListagemResult<Fornecedor> listagemFornecedorCSV(FiltroListagem _filtro){
		return fornecedorDAO.listagemFornecedorCSV(_filtro);
	}

	public void atualizaTipopessoa(Tipopessoa tipopessoa, Integer cdpessoa) {
		fornecedorDAO.atualizaTipopessoa(tipopessoa, cdpessoa);
	}

	public void atualizaCnpj(Cnpj cnpj, Integer cdpessoa) {
		fornecedorDAO.atualizaCnpj(cnpj, cdpessoa);
	}

	public void atualizaCpf(Cpf cpf, Integer cdpessoa) {
		fornecedorDAO.atualizaCpf(cpf, cdpessoa);
	}

	public void atualizaRazaosocial(String razaosocial, Integer cdpessoa) {
		fornecedorDAO.atualizaRazaosocial(razaosocial, cdpessoa);
	}

	public void atualizaNomefantasia(String nomefantasia, Integer cdpessoa) {
		fornecedorDAO.atualizaNomefantasia(nomefantasia, cdpessoa);
	}

	public void atualizaInscricaoestadual(String ie, Integer cdpessoa) {
		fornecedorDAO.atualizaInscricaoestadual(ie, cdpessoa);
	}

	public void atualizaInscricaomunicipal(String im, Integer cdpessoa) {
		fornecedorDAO.atualizaInscricaomunicipal(im, cdpessoa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForSpedReg0100Contabilista
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForSpedReg0100Contabilista(Fornecedor fornecedor) {
		return fornecedorDAO.loadForSpedReg0100Contabilista(fornecedor);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForSpedReg0100Escritorio
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForSpedReg0100Escritorio(Fornecedor fornecedor) {
		return fornecedorDAO.loadForSpedReg0100Escritorio(fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param string
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresWhereNotIn(String whereNotin) {
		return fornecedorDAO.findFornecedoresWhereNotIn(whereNotin);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* busca os fornecedores marcados como agenciavenda para o autocomplete 
	* 
	* @see	br.com.linkcom.sined.geral.dao.FornecedorDAO#findFornecedoresAutoCompleteAgenciavenda(String whereLike) 
	*
	* @param whereLike
	* @return
	* @since Sep 26, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Fornecedor> findFornecedoresAutoCompleteAgenciavenda(String whereLike){
		return fornecedorDAO.findFornecedoresAutoCompleteAgenciavenda(whereLike);
	}
	
	/**
	 * Busca a {@link Contagerencial} que est� associada a um dado {@link Fornecedor}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 25, 2011
	 * @param fornecedor
	 * @return
	 */
	public Contagerencial findContaGerencial(Fornecedor fornecedor) {
		return fornecedorDAO.findContaGerencial(fornecedor);
	}
	
	public ContaContabil findContaContabil(Fornecedor fornecedor) {
		return fornecedorDAO.findContaContabil(fornecedor);
	}
	
	public Contagerencial findContaGerencialCredito(Fornecedor fornecedor) {
		return fornecedorDAO.findContaGerencialCredito(fornecedor);
	}

	/**
	 * M�todo que cria identificador para usu�rio sem o digito verificador
	 * 
	 * @author Giovane Freitas
	 */
	public String geraIdentificadorSemDV() {
		String ultimo = parametrogeralService.getValorPorNome("ProximoIdentificadorFornecedor");
		String newNum = "";
		if(ultimo == null || ultimo.equals("")){
			newNum = "1";
		}else {
			String ultimoNum = ultimo;
			Long nextNum = Long.valueOf(ultimoNum);
			newNum = nextNum+"";
		}
		return newNum;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param identificador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeIdentificadorFornecedor(String identificador, Integer cdpessoa) {
		return fornecedorDAO.existeIdentificadorFornecedor(identificador, cdpessoa);
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return fornecedorDAO.findForArquivoGerencial();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForSpedPiscofinsReg0100Contabilista(Fornecedor fornecedor)
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForSpedPiscofinsReg0100Contabilista(Fornecedor fornecedor) {
		return fornecedorDAO.loadForSpedPiscofinsReg0100Contabilista(fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForSpedPiscofinsReg0100Escritorio(Fornecedor fornecedor)
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForSpedPiscofinsReg0100Escritorio(Fornecedor fornecedor) {
		return fornecedorDAO.loadForSpedPiscofinsReg0100Escritorio(fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.FornecedorDAO#findFornecedoresAtivosAutoComplete(String whereLike)
	 *
	 * @param whereLike
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresAtivosAutoComplete(String whereLike) {
		return fornecedorDAO.findFornecedoresAtivosAutoComplete(whereLike, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#findFornecedoresTransportadora()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresTransportadora(String whereInEmpresa){
		return fornecedorDAO.findFornecedoresTransportadora(whereInEmpresa);
	}
	/**
	 * M�todo com referencia no DAO
	 * @param whereLike
	 * @return
	 * @author Thiers Euller
	 */
	public List<Fornecedor> findFornecedoresTransportadoraForAutocomplete(String whereLike){
		return fornecedorDAO.findFornecedoresTransportadoraForAutocomplete(whereLike, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Fornecedor> findFornecedoresParceiros(String whereLike){
		return fornecedorDAO.findFornecedoresParceiros(whereLike, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
	 * 
	 * M�todo que cria o relat�rio com as avalia��es dos fornecedores selecionados
	 *
	 *@author Thiago Augusto
	 *@date 07/03/2012
	 * @param whereIn
	 * @return
	 * @throws Exception 
	 */
	public Resource createAvaliarFornecedor(String whereIn) throws Exception{
		
		MergeReport mergeReport = new MergeReport("avaliarfornecedor"+SinedUtil.datePatternForReport()+".pdf");
		
		List<Fornecedor> listaFornecedor = new ArrayList<Fornecedor>();
		listaFornecedor = this.carregarDadosRelatorioAvaliativo(whereIn);

		Empresa empresa = empresaService.loadPrincipal();
		
		for (Fornecedor fornecedor : listaFornecedor) {
			Report report = new Report("/suprimento/avaliacaofornecedor");
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			report.addParameter("TITULO", "AVALIA��ES DE FORNECEDORES");
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			report.addParameter("FORNECEDOR", fornecedor.getNome());
			report.addParameter("ENDERECO", fornecedor.getEndereco() != null ? fornecedor.getEndereco().getLogradouroCompletoComCep() : "");
			report.addParameter("TELEFONES", fornecedor.getTelefone() != null ? fornecedor.getTelefone().getTelefone() : "");
			report.addParameter("CNPJCPF", fornecedor.getCpf() != null ? fornecedor.getCpf().getValue() : fornecedor.getCnpj() != null ? fornecedor.getCnpj().getValue() : "");
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			for (Pessoaquestionario pq : fornecedor.getListaQuestionario()) {
				pq.setAvaliadorString(TagFunctions.findUserByCd(pq.getCdusuarioaltera()));
			}
			if (fornecedor.getListaQuestionario().size() > 0 && !fornecedor.getListaQuestionario().isEmpty()){
				for (Pessoaquestionario pq : fornecedor.getListaQuestionario()) {
					if (pq.getQuestionario() != null && pq.getQuestionario().getArquivo() != null){
						Arquivo arquivo = null;
						try{
							arquivo = arquivoService.loadWithContents(pq.getQuestionario().getArquivo());
						} catch (Exception e) {
							e.printStackTrace();
						}
						pq.setArquivoString(new String(arquivo.getContent()));
					} else {
						pq.setArquivoString("");
					}
				}
				report.setDataSource(fornecedor.getListaQuestionario());
				Report subreport = new Report("/suprimento/avaliacaofornecedor_subreport");
				report.addSubReport("AVALIACAOFORNECEDOR_SUBREPORT", subreport);
			}
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 07/03/2012
	 * @param whereIn
	 * @return
	 */
	public List<Fornecedor> carregarDadosRelatorioAvaliativo(String whereIn){
		return fornecedorDAO.carregarDadosRelatorioAvaliativo(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro) {
		return fornecedorDAO.findForDominiointegracaoCadastro(filtro);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name getFornecedorByCpfCnpj
	 * @param numero
	 * @param pessoaJuridica
	 * @return
	 * @return Fornecedor
	 * @author Thiago Augusto
	 * @date 03/04/2012
	 *
	 */
	public Fornecedor getFornecedorByCpfCnpj(Cnpj cnpj, Cpf cpf){
		return fornecedorDAO.getFornecedorByCpfCnpj(cnpj, cpf);
	}
	
	public Fornecedor loadFornecedorForImportacaoXml(Fornecedor fornecedor){
		return fornecedorDAO.loadFornecedorForImportacaoXml(fornecedor);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param cpf
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByCpf(Cpf cpf){
		return fornecedorDAO.findByCpf(cpf);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param cnpj
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByCnpj(Cnpj cnpj){
		return fornecedorDAO.findByCnpj(cnpj);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByNome(String nome){
		return fornecedorDAO.findByNome(nome);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForMovimentacaocontabil(Pessoa pessoa)
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForMovimentacaocontabil(Pessoa pessoa) {
		return fornecedorDAO.loadForMovimentacaocontabil(pessoa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForOrdemcompraInfo(Fornecedor fornecedor)
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForOrdemcompraInfo(Fornecedor fornecedor) {
		return fornecedorDAO.loadForOrdemcompraInfo(fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#loadForEntradaFiscalInfo(Fornecedor fornecedor)
	 * @param fornecedor
	 * @author Murilo
	 */
	public Fornecedor loadForEntradaFiscalInfo(Fornecedor fornecedor) {
		return fornecedorDAO.loadForEntradaFiscalInfo(fornecedor);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FornecedorDAO#haveFornecedorIdentificadorNome(String identificador, String nome)
	 *
	 * @param identificador
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public boolean haveFornecedorIdentificadorNome(String identificador,String nome) {
		return fornecedorDAO.haveFornecedorIdentificadorNome(identificador, nome);
	}
	
	/**
	 * 
	 * @param empresa
	 * @param whereInMaterial
	 * @author Thiago Clemente
	 * 
	 */
	public List<Fornecedor> findForFaturamento(Empresa empresa, String whereInMaterial){
		return fornecedorDAO.findForFaturamento(empresa, whereInMaterial);
	}
	
	public Fornecedor loadForTributacao(Fornecedor fornecedor) {
		return fornecedorDAO.loadForTributacao(fornecedor);
	}
	
	/**
	 * 
	 * @param cdpessoa
	 */
	public boolean isFornecedor(Integer cdpessoa){
		return fornecedorDAO.isFornecedor(cdpessoa);
	}
	
	public boolean isPermissaoFornecedor(Fornecedor fornecedor){
		return fornecedorDAO.isPermissaoFornecedor(fornecedor);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereLike
	 * @return
	 * @author Lucas Costa
	 */
	public List<Fornecedor> loadAllVinculadosUsuarioLogado (String whereLike){
		return fornecedorDAO.loadAllVinculadosUsuarioLogado(whereLike, false, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Fornecedor> loadAllVinculadosUsuarioLogadoForListagem (String whereLike){
		return fornecedorDAO.loadAllVinculadosUsuarioLogado(whereLike, true, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<FornecedorRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<FornecedorRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<FornecedorRESTModel> lista = new ArrayList<FornecedorRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Fornecedor bean : fornecedorDAO.findForAndroid(whereIn))
				lista.add(new FornecedorRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * M�todo com ref�ncia no DAO
	 * @param whereLike
	 * @return
	 */
	public List<Fornecedor> findFornecedoresAgenciaVendaAutoComplete(String whereLike){
		return fornecedorDAO.findFornecedoresAgenciaVenda(whereLike);
	}
	
	public Fornecedor loadWithoutPermissao(Fornecedor fornecedor, String campos){
		return fornecedorDAO.loadWithoutPermissao(fornecedor, campos);
	}

	public Fornecedor loadRepresentanteForWSArvoreAcesso(Pessoa vendedor) {
		return fornecedorDAO.loadRepresentanteForWSArvoreAcesso(vendedor);
	}
	
	public void updateContaContabil(Fornecedor fornecedor, ContaContabil contacontabil){
		fornecedorDAO.updateContaContabil(fornecedor, contacontabil);
	}
	
	public List<Fornecedor> findForSAGE(Date dataBase){
		return fornecedorDAO.findForSAGE(dataBase);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.FornecedorDAO#findForSAGE(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Fornecedor> findForSAGE(String whereIn){
		return fornecedorDAO.findForSAGE(whereIn);
	}
	
	public List<Fornecedor> findForValidacaoSAGE(String whereIn){
		return fornecedorDAO.findForValidacaoSAGE(whereIn);
	}
	
	public List<Fornecedor> findForInventario(String whereIn) {
		return fornecedorDAO.findForInventario(whereIn);
	}
	
	public List<Fornecedor> findAssociacaoAtivoAutocomplete(String q) {
		return fornecedorDAO.findAssociacaoAtivoAutocomplete(q);
	}
	
	public List<Fornecedor> findCaptadorAtivoAutocomplete(String q) {
		return fornecedorDAO.findCaptadorAtivoAutocomplete(q);
	}

	public Fornecedor loadFornecedorAssociacao(Fornecedor fornecedor) {
		return fornecedorDAO.loadFornecedorAssociacao(fornecedor);
	}

	public Fornecedor loadForAtualizacaoCampos(Fornecedor fornecedor) {
		return fornecedorDAO.loadForAtualizacaoCampos(fornecedor);
	}
	public List<GenericBean> findForWSVenda(Empresa empresa) {
		return ObjectUtils.translateEntityListToGenericBeanList(this.findFornecedoresTransportadora(empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null));
	}
	
	public List<Fornecedor> findCredenciadoras(String nome) {
		return fornecedorDAO.findCredenciadoras(nome);
	}

	public Boolean isFabricante(Integer cdpessoa) {
		return fornecedorDAO.isFabricante(cdpessoa);
	}
	
	public Fornecedor carregaTiketMedio(Fornecedor bean){
		return fornecedorDAO.carregaTiketMedio(bean);
	}
	
	public List<Fornecedor> findFornecedoresFabricantesAtivosAutoComplete(String whereLike) {
		boolean isFabricante = false;
		boolean isFornecedor = false;
		try {
			isFabricante = Boolean.parseBoolean(NeoWeb.getRequestContext().getParameter("isFabricante"));
			isFornecedor = Boolean.parseBoolean(NeoWeb.getRequestContext().getParameter("isFornecedor"));
			
		} catch (Exception e) {}		
		return fornecedorDAO.findFornecedoresFabricantesAtivosAutoComplete(whereLike, isFabricante, isFornecedor);
	}
	
	public Boolean existeCodigoParceiro (Integer codigoParceiro){
		return fornecedorDAO.existeCodigoParceiro(codigoParceiro);
	}
	public Fornecedor findByCodigoParceiro (Integer codigoParceiro){
		return fornecedorDAO.findByCodigoParceiro(codigoParceiro);
	}
}