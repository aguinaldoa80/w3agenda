package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.enumeration.CAGED;
import br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarcargocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarsituacaocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.GerarArquivoSEFIPFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EmitirautorizacaoReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EntregaEPIReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorcargoService extends GenericService<Colaboradorcargo> {

	private ColaboradorcargoDAO colaboradorcargoDAO;
	private ColaboradorService colaboradorService;

	public void setColaboradorcargoDAO(ColaboradorcargoDAO colaboradorcargoDAO) {
		this.colaboradorcargoDAO = colaboradorcargoDAO;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}


	public List<Colaboradorcargo> findForAutocompleteColaborador(String s){
		return colaboradorcargoDAO.findForAutocompleteColaborador(s);
	}

	/**
	 * M�todo para passar os dados do colaborador para colaboradorcargo
	 *
	 * @param bean
	 * @return listaColaboradorcargo
	 * @author Jo�o Paulo Zica
	 * @author Flavio
	 */
	public Colaboradorcargo makeColaboradorcargo(Colaborador bean){
		if(bean ==  null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		Colaboradorcargo colaboradorcargo = new Colaboradorcargo();
		colaboradorcargo.setColaborador(bean);
		colaboradorcargo.setCargo(bean.getCargotransient());
		colaboradorcargo.setDepartamento(bean.getDepartamentotransient());
		colaboradorcargo.setRegimecontratacao(bean.getRegimecontratacao());
		colaboradorcargo.setSalario(bean.getSalario());
		colaboradorcargo.setEmpresa(bean.getEmpresa());
		colaboradorcargo.setColaboradorsituacao(bean.getColaboradorsituacao());
		colaboradorcargo.setMatricula(bean.getMatricula());
		if(SinedUtil.isSined()){
			colaboradorcargo.getEmpresa().setNome("DALTEC");
		}
		colaboradorcargo.setSindicato(bean.getSindicato());
		colaboradorcargo.setDtinicio(bean.getDtinicio() != null ? bean.getDtinicio() : bean.getDtadmissao());
		colaboradorcargo.setFornecedor(bean.getFornecedor());
		
		return colaboradorcargo;
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findForRelatorioColaboradores
	 * @param filtro
	 * @return Lista de Colaboradorcargo
	 * @author Flavio Tavares
	 */
	public List<Colaboradorcargo> findForRelatorioColaboradores(ColaboradorReportFiltro filtro){
		return colaboradorcargoDAO.findForRelatorioColaboradores(filtro);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findColaboradorcargoAltera
	 * @param filtro
	 * @return ListagemResult<Colaboradorcargo>
	 * @author Flavio Tavares
	 */
	public List<Colaboradorcargo> findColaboradorcargoAltera(AlterarcargocolaboradorFilter filtro){
		return colaboradorcargoDAO.findColaboradorcargoAltera(filtro);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Altera o cargo/departamento de um colaborador
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#alterarCargoColaborador(Colaborador)
	 * @see #findSindicatoCargoAtual(Colaboradorcargo)
	 * @param colaborador
	 * @author Flavio
	 */
	public void alterarCargoColaborador(Colaborador colaborador){
		Colaboradorcargo load = new Colaboradorcargo();
		load.setColaborador(colaborador);
		Colaboradorcargo colaboradorcargo = this.findSindicatoCargoAtual(load);
		colaborador.setSindicato(colaboradorcargo.getSindicato());
		colaboradorcargoDAO.alterarCargoColaborador(colaborador);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findForAutorizacaoExame(EmitirautorizacaoFilter)
	 * @param filtro
	 * @return Lista de Colaboradorcargo
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findForAutorizacaoExame(EmitirautorizacaoReportFiltro filtro){
		return colaboradorcargoDAO.findForAutorizacaoExame(filtro);
	}


	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findColaboradorAltera(AlterarsituacaocolaboradorFilter)
	 * @param filtro
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findColaboradorAltera(AlterarsituacaocolaboradorFilter filtro){
		return colaboradorcargoDAO.findColaboradorAltera(filtro);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findHistoricoProfissional(Colaborador)
	 * @param colaborador
	 * @return
	 * @author Flavio
	 */
	public List<Colaboradorcargo> findHistoricoProfissional(Colaborador colaborador){
		return colaboradorcargoDAO.findHistoricoProfissional(colaborador);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findCargoAtual(Colaborador)
	 * @param colaborador
	 * @return
	 * @author Flavio
	 */
	public Colaboradorcargo findCargoAtual(Colaborador colaborador){
		return colaboradorcargoDAO.findCargoAtual(colaborador);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o sindicato de um colaboradorcargo.
	 * 
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaboradorcargo findSindicatoCargoAtual(Colaboradorcargo bean){
		return colaboradorcargoDAO.findSindicatoCargoAtual(bean);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega os dados do cargo atual do colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Flavio
	 */
	public Colaboradorcargo carregaDadosCargo(Colaborador colaborador){
		return colaboradorcargoDAO.loadDadosCargo(colaborador);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m determinados campos de um colaboradorcargo.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaboradorcargo load(Colaboradorcargo bean, String campos){
		return colaboradorcargoDAO.load(bean, campos);
	}
	
	/* singleton */
	private static ColaboradorcargoService instance;
	public static ColaboradorcargoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoService.class);
		}
		return instance;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeColaboradorCargoData(Colaboradorcargo bean) {
		return colaboradorcargoDAO.existeColaboradorCargoData(bean);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaboradorcargo> carregaColaboradoresCargos(String whereIn) {
		return colaboradorcargoDAO.carregaColaboradoresCargos(whereIn);
	}

	@Override
	public void saveOrUpdate(final Colaboradorcargo bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			
			public Object doInTransaction(TransactionStatus status) {
			if(bean.getIdColaboradores() != null && !bean.getIdColaboradores().equals("")){
			
				List<Colaboradorcargo> listaCargos = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
				listaCargos = cargosAnteriores(bean);
				
				String itens[] = bean.getIdColaboradores().split(",");
				for (String item : itens) {
					Integer cdcolaborador = Integer.valueOf(item);
					Colaborador colaborador = new Colaborador();
					colaborador.setCdpessoa(cdcolaborador);
				
					Colaboradorcargo colaboradorcargo = new Colaboradorcargo(bean, colaborador);
				
					saveOrUpdateNoUseTransaction(colaboradorcargo);
					if(bean.getDtfim() == null && bean.getColaboradorsituacao() != null){
						colaboradorService.updateColaboradorsituacaoByColaborador(colaborador, bean.getColaboradorsituacao());
					}
					setDtfimCargoAnterior(listaCargos);
				}
			}else{
				ColaboradorcargoService.super.saveOrUpdate(bean);
			}
			return status;
		}
	});
	}
	
	/**
	 * Busca busca o �ltimo cargo de cada colaborador que ter� o cargo alterado.
	 * @param colaboradorcargo
	 * @return
	 * @author Taidson
	 * @since 01/06/2010
	 */
	public List<Colaboradorcargo> cargosAnteriores(Colaboradorcargo colaboradorcargo){
		List<Colaboradorcargo> listaCargos = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
		
		String itens[] = colaboradorcargo.getIdColaboradores().split(",");
		for (String item : itens) {
			Integer cdcolaborador = Integer.valueOf(item);
			Colaborador colaborador = new Colaborador();
			colaborador.setCdpessoa(cdcolaborador);
			List<Colaboradorcargo> listaTemp = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
			listaTemp = colaboradorcargoDAO.loadCargosAnteriores(colaborador);
			if(listaTemp != null && listaTemp.size() > 0){
				for (Colaboradorcargo ca : listaTemp) {
					listaCargos.add(ca);				
				}
			}
		}
		return listaCargos;
	}
	
	/**
	 * Chama m�todo no DAO para seta a data atual 
	 * como data fim do cargo anterior de cada colaborador.
	 * @param colaboradorcargo
	 * @return
	 * @author Taidson
	 * @since 27/05/2010
	 */
	public void setDtfimCargoAnterior(List<Colaboradorcargo> listaCargos){
		for (Colaboradorcargo item : listaCargos) {
			colaboradorcargoDAO.setDtfimCargoAnterior(item);			
			
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param projeto
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> cargosProjeto(Projeto projeto){
		return colaboradorcargoDAO.cargosProjeto(projeto);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param cargo
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> colaboradoresCargo(Cargo cargo){
		return colaboradorcargoDAO.colaboradoresCargo(cargo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#colaboradoresCargo(Cargo cargo, Empresa empresa)
	 *
	 * @param cargo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaboradorcargo> colaboradoresCargo(Cargo cargo, Empresa empresa) {
		return colaboradorcargoDAO.colaboradoresCargo(cargo, empresa);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> findForRelatorioEntregaEPI(EntregaEPIReportFiltro filtro, String whereIn){
		return colaboradorcargoDAO.findForRelatorioEntregaEPI(filtro, whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param colaborador
	 * @param dtdemissao
	 * @author Taidson
	 * @since 20/08/2010
	 */
	public void demissaoColaborador(Colaborador colaborador) {
		colaboradorcargoDAO.demissaoColaborador(colaborador);
	}

	public void updateDtfim(Colaboradorcargo colaboradorcargo, Date dtfim) {
		colaboradorcargoDAO.updateDtfim(colaboradorcargo, dtfim);
	}

	public void updateCaged(Colaboradorcargo colaboradorcargo, CAGED caged) {
		colaboradorcargoDAO.updateCaged(colaboradorcargo, caged);		
	}

	public void updateSindicato(Colaboradorcargo colaboradorcargo, Sindicato sindicato) {
		colaboradorcargoDAO.updateSindicato(colaboradorcargo, sindicato);		
	}

	public void updateDtinicio(Colaboradorcargo colaboradorcargo, Date dtinicio) {
		colaboradorcargoDAO.updateDtinicio(colaboradorcargo, dtinicio);
	}

	public boolean haveColaboradorcargo(Cargo cargo, Date dtinicio, Colaborador colaborador) {
		return colaboradorcargoDAO.haveColaboradorcargo(cargo, dtinicio, colaborador);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 12/01/2012
	 * @param colaborador
	 * @return
	 */
	public List<Colaboradorcargo> findListColaboradorCargoForColaborador(Colaborador colaborador){
		return colaboradorcargoDAO.findListColaboradorCargoForColaborador(colaborador);
	}
	
	/**
	 * 
	 * M�todo para buscar uma lista de cargos de colaborador a partir de uma Lista de cdcolaborador.
	 *
	 * @author Filipe Santos
	 * @date 24/01/2012
	 * @param List<colaborador>
	 * @return
	 */
	public List<Colaboradorcargo> findByCdpessoa(String codigo){
		return colaboradorcargoDAO.findByCdpessoa(codigo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param colaboradorcargo
	 * @param salario
	 * @param sindicato
	 * @param empresa
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSindicatoEmpresaSalario(Colaboradorcargo colaboradorcargo, Money salario, Sindicato sindicato, Empresa empresa) {
		colaboradorcargoDAO.updateSindicatoEmpresaSalario(colaboradorcargo, salario, sindicato, empresa);
	}
	
	public List<Colaboradorcargo> findForGerarArquivoSEFIP(GerarArquivoSEFIPFiltro filtro){
		return colaboradorcargoDAO.findForGerarArquivoSEFIP(filtro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#loadCargoatualWithResponsavelDepartamento(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaboradorcargo loadCargoatualWithResponsavelDepartamento(Colaborador colaborador){
		return colaboradorcargoDAO.loadCargoatualWithResponsavelDepartamento(colaborador);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcargoDAO#findCargoAtualForTotalatividade(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaboradorcargo findCargoAtualForTotalatividade(Colaborador colaborador){
		return colaboradorcargoDAO.findCargoAtualForTotalatividade(colaborador);
	}
}
