package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.dao.VendahistoricoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaHistoricoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendahistoricoService extends GenericService<Vendahistorico>{

	private VendahistoricoDAO vendahistoricoDAO;
	
	public void setVendahistoricoDAO(VendahistoricoDAO vendahistoricoDAO) {
		this.vendahistoricoDAO = vendahistoricoDAO;
	}
	
	/* singleton */
	private static VendahistoricoService instance;
	public static VendahistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendahistoricoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VendahistoricoService#findByVenda(Venda venda)
	 * 
	 * @param venda
	 * @return
	 * @since 16/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendahistorico> findByVenda(Venda venda) {
		return vendahistoricoDAO.findByVenda(venda);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * 
	 * @param cpfcnpj, tipopessoa
	 * @since 21/05/2012
	 * @author Rafael Salvio
	 */
	public List<Vendahistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa){
		return vendahistoricoDAO.findByCpfCnpjCliente(cpfcnpj, tipopessoa);
	}
	
	/**
	 * Cria hist�rico de emiss�o de ficha de coleta, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param whereIn
	 * @since 16/07/2019
	 * @author Rodrigo Freitas
	 */
	public void makeHistoricoFichaColetaEmitido(String whereIn){
		if(StringUtils.isNotBlank(whereIn)){
			String[] ids = whereIn.split(",");
			for (String id : ids) {
				Vendahistorico vendahistorico = new Vendahistorico();
				vendahistorico.setAcao("Emiss�o de Ficha/Etiqueta de Pneu");
				vendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				vendahistorico.setDtaltera(SinedDateUtils.currentTimestamp());
				vendahistorico.setObservacao("Ficha/Etiqueta de Pneu.");
				vendahistorico.setVenda(new Venda(Integer.parseInt(id)));
				this.saveOrUpdate(vendahistorico);
			}
		}
	}

	/**
	 * Cria hist�ricos das vendas de uma expedi��o.
	 *
	 * @param expedicao
	 * @param vendas
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public void makeHistoricoExpedicao(Expedicao expedicao, String vendas) {
		String[] ids = vendas.split(",");
		
		Vendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Vendahistorico();
			vendahistorico.setAcao("Expedi��o criada");
			vendahistorico.setVenda(new Venda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Expedi��o <a href='javascript:visualizarExpedicao(" + expedicao.getCdexpedicao() + ")'>" + expedicao.getCdexpedicao() + "</a>");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	/**
	 * Cria hist�rico de emiss�o do comprovante da venda com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Venda
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(Venda venda) {
		Vendahistorico vendahistorico = new Vendahistorico();
		vendahistorico.setAcao(Comprovantestatus.EMITIDO.toString());
		vendahistorico.setVenda(venda);
		vendahistorico.setObservacao("Comprovante emitido");
		vendahistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
		this.saveOrUpdate(vendahistorico);
	}
	
	/**
	 * Cria hist�ricos de emiss�o dos comprovantes das vendas com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param List<Venda>
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEmitido(List<Venda> vendas) {
		for (Venda venda: vendas) {
			makeHistoricoComprovanteEmitido(venda);
		}
	}
	
	/**
	 * Cria hist�ricos de envio dos comprovantes das vendas via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param List<Venda>
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(List<Venda> vendas) {
		for (Venda venda: vendas) {
			makeHistoricoComprovanteEnviado(venda);
		}
	}
	
	/**
	 * Cria hist�ricos de envio do comprovante da venda via e-mail, com o usu�rio logado como respons�vel pela a��o.
	 *
	 * @param Venda
	 * @since 17/10/2016
	 * @author Mairon Cezar
	 */
	public void makeHistoricoComprovanteEnviado(Venda venda) {
		Vendahistorico vendahistorico = new Vendahistorico();
		vendahistorico.setAcao(Comprovantestatus.ENVIADO.toString());
		vendahistorico.setVenda(venda);
		vendahistorico.setObservacao("Comprovante enviado");
		vendahistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
		this.saveOrUpdate(vendahistorico);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendahistoricoDAO#findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa, Date dtinicio, Date dtfim)
	 *
	 * @param cpfcnpj
	 * @param tipopessoa
	 * @param dtinicio
	 * @param dtfim
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendahistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa, Date dtinicio, Date dtfim, Empresa empresa){
		return vendahistoricoDAO.findByCpfCnpjCliente(cpfcnpj, tipopessoa, dtinicio, dtfim, empresa);
	}
	
	/**
	 * Cria hist�ricos de unifica��o de vendas na expedi��o.
	 *
	 * @param expedicao
	 * @param vendas
	 * @since 15/05/2017
	 * @author Mairon Cezar
	 */
	public void makeHistoricoExpedicaoUnificacao(Expedicao expedicao, String vendas) {
		String[] ids = vendas.split(",");
		
		Vendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Vendahistorico();
			vendahistorico.setAcao("Unifica��o na expedi��o");
			vendahistorico.setVenda(new Venda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Unifica��o na expedi��o <a href='javascript:visualizarExpedicao(" + expedicao.getCdexpedicao() + ")'>" + expedicao.getCdexpedicao() + "</a>");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	/**
	 * Cria hist�ricos das vendas de uma expedi��o.
	 *
	 * @param expedicao
	 * @param vendas
	 * @since 15/05/2017
	 * @author Mairon Cezar
	 */
	public void makeHistoricoExpedicaoByVendas(Expedicao expedicao, String vendas) {
		String[] ids = vendas.split(",");
		
		Vendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Vendahistorico();
			vendahistorico.setAcao("Adicionada a expedi��o");
			vendahistorico.setVenda(new Venda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Adicionada a expedi��o <a href='javascript:visualizarExpedicao(" + expedicao.getCdexpedicao() + ")'>" + expedicao.getCdexpedicao() + "</a>");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	/**
	 * Cria hist�ricos de remo��o de itens de venda.
	 *
	 * @param expedicao
	 * @param vendas
	 * @since 15/05/2017
	 * @author Mairon Cezar
	 */
	public void makeHistoricoRemocaoItensExpedicao(Expedicao expedicao, String vendas) {
		String[] ids = vendas.split(",");
		
		Vendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Vendahistorico();
			vendahistorico.setAcao("Venda removida da expedi��o");
			vendahistorico.setVenda(new Venda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Removida da expedi��o <a href='javascript:visualizarExpedicao(" + expedicao.getCdexpedicao() + ")'>" + expedicao.getCdexpedicao() + "</a>");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	public void gerarHistoricoNotaRetorno(Notafiscalproduto nota, Venda venda){
		Vendahistorico pvh = new Vendahistorico();
		pvh.setVenda(venda);
		pvh.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		pvh.setDtaltera(new Timestamp(System.currentTimeMillis()));
		pvh.setObservacao("Nota Fiscal de retorno gerada: " + SinedUtil.makeLinkNota(nota));
		pvh.setAcao("Gera��o da nota");
		saveOrUpdate(pvh);
	}
	
	public void makeHistoricoRecalculoFaixaMarkup(String vendas) {
		String[] ids = vendas.split(",");
		
		Vendahistorico vendahistorico;
		for (int i = 0; i < ids.length; i++) {
			vendahistorico = new Vendahistorico();
			vendahistorico.setAcao("Rec�lculo de markup");
			vendahistorico.setVenda(new Venda(Integer.parseInt(ids[i])));
			vendahistorico.setObservacao("Faixas de markup recalculadas.");
			
			this.saveOrUpdate(vendahistorico);
		}
	}
	
	public List<VendaHistoricoWSBean> toWSList(List<Vendahistorico> listaVendaHistorico){
		List<VendaHistoricoWSBean> lista = new ArrayList<VendaHistoricoWSBean>();
		if(Hibernate.isInitialized(listaVendaHistorico) && SinedUtil.isListNotEmpty(listaVendaHistorico)){
			for(Vendahistorico vh: listaVendaHistorico){
				VendaHistoricoWSBean bean = new VendaHistoricoWSBean();
				bean.setAcao(vh.getAcao());
				bean.setCdusuarioaltera(vh.getCdusuarioaltera());
				bean.setCdvendahistorico(vh.getCdvendahistorico());
				//bean.setDtaltera(vh.getDtaltera());
				bean.setEmpresa(ObjectUtils.translateEntityToGenericBean(vh.getEmpresa()));
				bean.setObservacao(vh.getObservacao());
				bean.setUsuarioaltera(ObjectUtils.translateEntityToGenericBean(vh.getUsuarioaltera()));
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vh.getVenda()));
				
				lista.add(bean);
			}
		}
		return lista;
	}

	public void salvarHistoricoPneuJaAssociado(NotaFiscalServico notaFiscalServico, Venda venda, String pneusJaAssociado) {
		StringBuilder observacao = new StringBuilder();
		String observacaoString = "";
		
		observacao.append("Nota Fiscal de retorno gerada ");
		observacao.append("<a href=\"/w3erp/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=");
		observacao.append(notaFiscalServico.getCdNota() + "\">" + notaFiscalServico.getCdNota());
		observacao.append("</a>. ");
		
		observacao.append("Visualizar Pneu faturado/recusado: ");
		
		for (String s : pneusJaAssociado.split(",")) {
			observacao.append("<a href=\"/w3erp/sistema/crud/Pneu?ACAO=consultar&cdpneu=");
			observacao.append(s + "\">" + s);
			observacao.append("</a>,");
		}
		
		observacaoString = observacao.toString();
		observacaoString = observacaoString.substring(0, observacao.length() - 1);
		
		Vendahistorico vendaHistorico = new Vendahistorico();
		
		vendaHistorico.setVenda(venda);
		vendaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
		vendaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		vendaHistorico.setObservacao(observacaoString);
		
		vendahistoricoDAO.saveOrUpdate(vendaHistorico);
	}
}
