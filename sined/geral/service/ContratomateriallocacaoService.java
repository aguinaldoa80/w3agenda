package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.geral.dao.ContratomateriallocacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratomateriallocacaoService extends GenericService<Contratomateriallocacao> {

	private ContratomateriallocacaoDAO contratomateriallocacaoDAO;
	
	public void setContratomateriallocacaoDAO(ContratomateriallocacaoDAO contratomateriallocacaoDAO) {
		this.contratomateriallocacaoDAO = contratomateriallocacaoDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratomateriallocacaoDAO#updateQtdeLocacao(Contratomateriallocacao contratomateriallocacao, Double qtde)
	 *
	 * @param contratomateriallocacao
	 * @param qtde
	 * @author Luiz Fernando
	 */
	public void updateQtdeLocacao(Contratomateriallocacao contratomateriallocacao,	Double qtde) {
		contratomateriallocacaoDAO.updateQtdeLocacao(contratomateriallocacao, qtde);
	}
	
	public List<Contratomateriallocacao> findByContratomaterial(Contratomaterial contratomaterial){
		return contratomateriallocacaoDAO.findByContratomaterial(contratomaterial);
	}
}
