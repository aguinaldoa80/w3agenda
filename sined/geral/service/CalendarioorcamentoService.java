package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.dao.CalendarioorcamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CalendarioorcamentoService extends GenericService<Calendarioorcamento> {

	private CalendarioorcamentoDAO calendarioorcamentoDAO;
	
	public void setCalendarioorcamentoDAO(
			CalendarioorcamentoDAO calendarioorcamentoDAO) {
		this.calendarioorcamentoDAO = calendarioorcamentoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CalendarioorcamentoDAO#verificaOutroCalendario
	 * 
	 * @param orcamento
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean verificaOutroCalendario(Orcamento orcamento, Calendario bean) {
		return calendarioorcamentoDAO.verificaOutroCalendario(orcamento, bean);
	}

}
