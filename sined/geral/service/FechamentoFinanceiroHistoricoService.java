package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.FechamentoFinanceiroHistorico;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.geral.dao.FechamentoFinanceiroHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FechamentoFinanceiroHistoricoService extends GenericService<FechamentoFinanceiroHistorico>{

	private FechamentoFinanceiroHistoricoDAO fechamentoFinanceiroHistoricoDAO;
	
	public void setFechamentoFinanceiroHistoricoDAO(FechamentoFinanceiroHistoricoDAO fechamentoFinanceiroHistoricoDAO) {
		this.fechamentoFinanceiroHistoricoDAO = fechamentoFinanceiroHistoricoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param form
	 * @return
	 */
	public List<FechamentoFinanceiroHistorico> findHistoricoByFechamentoFinanceiro(Fechamentofinanceiro fechamentofinanceiro) {
		return fechamentoFinanceiroHistoricoDAO.findHistoricoByFechamentoFinanceiro(fechamentofinanceiro);
	}

}
