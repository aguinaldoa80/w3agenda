package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoaatalho;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.dao.PessoaatalhoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoaatalhoService extends GenericService<Pessoaatalho> {
	
	private  PessoaatalhoDAO pessoaatalhoDAO;
	
	public void setPessoaatalhoDAO(PessoaatalhoDAO pessoaatalhoDAO) {
		this.pessoaatalhoDAO = pessoaatalhoDAO;
	}
	

	/**
	 * Faz referÍncia ao DAO.
	 * @param pessoa
	 * @return
	 * @since 30/03/2011
	 */
	public List<Pessoaatalho> findByPessoa(Pessoa pessoa){
		return pessoaatalhoDAO.findByPessoa(pessoa);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * @param pessoa
	 * @param tela
	 * @return
	 */
	public Pessoaatalho loadByPessoaTela(Pessoa pessoa, Tela tela) {
		return pessoaatalhoDAO.loadByPessoaTela(pessoa, tela);
	}
}
