package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.dao.PneumarcaDAO;
import br.com.linkcom.sined.util.rest.android.pneumarca.PneumarcaRESTModel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.pneumarca.PneuMarcaW3producaoRESTModel;

public class PneumarcaService extends GenericService<Pneumarca> {

	private PneumarcaDAO pneumarcaDAO;

	public void setPneumarcaDAO(PneumarcaDAO pneumarcaDAO) {
		this.pneumarcaDAO = pneumarcaDAO;
	}

	/* singleton */
	private static PneumarcaService instance;
	public static PneumarcaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneumarcaService.class);
		}
		return instance;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumarcaDAO#findAutocomplete(String q)
	*
	* @param q
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumarca> findAutocomplete(String q){
		return pneumarcaDAO.findAutocomplete(q);
	}
	
	public List<PneuMarcaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de pneumarca para sincronizar com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PneuMarcaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PneuMarcaW3producaoRESTModel> lista = new ArrayList<PneuMarcaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumarca pm : pneumarcaDAO.findForW3Producao(whereIn))
				lista.add(new PneuMarcaW3producaoRESTModel(pm));
		}
		
		return lista;
	}
	
	/**
	 * Carrega pelo nome, se n�o existir insere o registro.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumarca loadOrInsertByNome(String nome){
		Pneumarca pneumarca = this.loadByNome(nome);
		if(pneumarca == null){
			pneumarca = new Pneumarca();
			pneumarca.setNome(nome);
			this.saveOrUpdate(pneumarca);
		}
		return pneumarca;
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.PneumarcaService.findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumarcaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumarcaDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumarcaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PneumarcaRESTModel> lista = new ArrayList<PneumarcaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumarca bean : pneumarcaDAO.findForAndroid(whereIn))
				lista.add(new PneumarcaRESTModel(bean));
		}
		
		return lista;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumarca loadByNome(String nome) {
		return pneumarcaDAO.loadByNome(nome);
	}
	
	public Pneumarca findForCd(String whereIn) {
		return pneumarcaDAO.findForCd(whereIn);
	}
}