package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.ObjectUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.AgendamentoHistorico;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.AgendamentoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.AgendamentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AgendamentoService extends GenericService<Agendamento> {

	private AgendamentoDAO agendamentoDAO;
	private AgendamentoHistoricoService agendamentoHistoricoService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private DocumentoService documentoService;
	private RateioitemService rateioitemService;
	private EmpresaService empresaService;
	private AvisousuarioService avisoUsuarioService;
	private AvisoService avisoService;
	private PessoaService pessoaService;
	private ColaboradorService colaboradorService;

	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setAgendamentoDAO(AgendamentoDAO agendamentoDAO) {this.agendamentoDAO = agendamentoDAO;}
	public void setAgendamentoHistoricoService(AgendamentoHistoricoService agendamentoHistoricoService) {this.agendamentoHistoricoService = agendamentoHistoricoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	/**
	 * <p>Faz ajustes na lista da listagem.</p>
	 * 
	 * @see #processaListagem(List, AgendamentoFiltro)
	 * @author Hugo Ferreira
	 */
	@Override
	public ListagemResult<Agendamento> findForListagem(FiltroListagem filtro) {
		ListagemResult<Agendamento> listagemResult = super.findForListagem(filtro);
		List<Agendamento> listaAgendamento = listagemResult.list();
		AgendamentoFiltro _filtro = (AgendamentoFiltro) filtro;
		this.processaListagem(listaAgendamento, _filtro);
		return listagemResult;
	}

	/**
	 * <p>Ajusta o valor do campo 'tipoOperacao' e calcula o somat�rio de cr�ditos e d�bitos antes de listar.</p>
	 * 
	 * @see #findForSomaListagem(AgendamentoFiltro)
	 * @author Hugo Ferreira
	 */
	private void processaListagem(List<Agendamento> listaAgendamento, AgendamentoFiltro filtro) {

		for (Agendamento agendamento : listaAgendamento) {

			// ajusta o campo nome do atributo 'tipooperacao' de acordo com o pedido no caso de uso
			if (agendamento.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
				agendamento.getTipooperacao().setNome("C");
			} else {
				agendamento.getTipooperacao().setNome("D");
			}
		}
		
		Money totalCredito = new Money();
		Money totalDebito = new Money();
		
		List<Agendamento> list = this.findForSomaListagem(filtro);
		if(SinedUtil.isListNotEmpty(list)){
			for (Agendamento a : list) {
				if (a.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {
					totalCredito = totalCredito.add(a.getValor());
				} else {
					totalDebito = totalDebito.add(a.getValor());
				}
			}
		}		
		
		filtro.setTotalCredito(totalCredito);
		filtro.setTotalDebito(totalDebito);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#findForSomaListagem(AgendamentoFiltro)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Agendamento> findForSomaListagem(AgendamentoFiltro filtro){
		return agendamentoDAO.findForSomaListagem(filtro);
	}
	
	/**
	 * <p>Executa os procedimentos antes de salvar:</p>
	 * 
	 * <ul>
	 * 	<li> Adiciona um item no hist�rico do agendamento corrente
	 * 	<li> Se a frequ�ncia for �nica, seta a dtFim igual a dtProximo
	 * </ul>
	 * 
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#gerenciaHistorico(Agendamento, Documento)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#calcularDataUltimaParcela(Frequencia, Integer, Date)
	 * @param request
	 * @param bean
	 * @author Hugo Ferreira
	 */
	@Override
	public void saveOrUpdate(Agendamento bean) {
		agendamentoHistoricoService.gerenciaHistorico(bean, null);
		if (bean.getFrequencia().getCdfrequencia().equals(Frequencia.UNICA)) {
			bean.setDtfim(bean.getDtproximo());
		}
		super.saveOrUpdate(bean);
	}
	
	/**
	 * <p>Carrega um bean de Agendamento com todas as propriedades necess�rias
	 * para uma consolida��o.</p>
	 * <p>A lista de item de rateio � carregada a parte por causa do bug gigante
	 * que tem no translator do Neo.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#carregarParaConsolidar(Integer)
	 * @param cdAgendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Agendamento carregarParaConsolidar(Integer cdAgendamento) {
		Agendamento agendamento = agendamentoDAO.carregarParaConsolidar(cdAgendamento);
		agendamento.getRateio().setListaRateioitem(rateioitemService.findByRateio(agendamento.getRateio()));
		return agendamento;
	}
	
	/**
	 * <p>Carrega um agendamento para salvar ap�s uma consolida��o.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#carregarParaSalvarConsolidar(Integer)
	 * @param cdAgendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Agendamento carregarParaSalvarConsolidar(Integer cdAgendamento) {
		Agendamento agendamento = agendamentoDAO.carregarParaSalvarConsolidar(cdAgendamento);
		agendamento.getRateio().setListaRateioitem(rateioitemService.findByRateio(agendamento.getRateio()));
		return agendamento;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Agendamento> findForReportFluxocaixa(FluxocaixaFiltroReport filtro){
		return agendamentoDAO.findForReportFluxocaixa(filtro);
	}
	
	/**
	 * M�todo para adicionar a lista de Agendamentos � lista de Fluxo de caixa para gera��o do relat�rio.
	 * As consolida��es do agendamento s�o simuladas de acordo com sua frequ�ncia at� a data fim do relat�rio.
	 * 
	 * @see #gerarIteracoesAgendamento(Agendamento, Date, Date)
	 * @see #criaListaFluxocaixa(List)
	 * 
	 * @param filtro
	 * @param listaFluxo
	 * @param listaMovimentacao
	 * @param periodoDe
	 * @param periodoAte
	 * @author Fl�vio Tavares
	 * @param valoresAnteriores 
	 * @param radEvento 
	 */
	public void adicionaAgendamentoFluxocaixa(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo, List<Agendamento> listaAgendamento, Date periodoDe, Date periodoAte, Boolean valoresAnteriores){
		List<Agendamento> listaIteracoesAgendamento = new ArrayList<Agendamento>();
		for (Agendamento ag : listaAgendamento) {
			listaIteracoesAgendamento.addAll(this.gerarIteracoesAgendamento(ag, periodoDe, periodoAte, valoresAnteriores));
		}
		listaFluxo.addAll(this.criaListaFluxocaixa(listaIteracoesAgendamento, periodoDe, valoresAnteriores));
	}
	
	/**
	 * M�todo para criar a lista do bean <tt>Fluxocaixa</tt> a partir de uma lista de <tt>Agendamento</tt>.
	 * 
	 * @param listaAgendamento
	 * @return List<Fluxocaixa>
	 * @author Fl�vio Tavares
	 * @param filtro 
	 * @param radEvento 
	 */
	private List<FluxocaixaBeanReport> criaListaFluxocaixa(List<Agendamento> listaAgendamento, Date periodoDe, Boolean valoresAnteriores){
		List<FluxocaixaBeanReport> listaFluxo = new ArrayList<FluxocaixaBeanReport>();
		
		for (Agendamento ag : listaAgendamento) {
			FluxocaixaBeanReport fluxocaixaBeanReport = new FluxocaixaBeanReport(ag);
			if(valoresAnteriores != null && valoresAnteriores && ag.getDtproximo().before(periodoDe)){
				fluxocaixaBeanReport.setDataAtrasada(ag.getDtproximo());
				fluxocaixaBeanReport.setData(SinedDateUtils.remoteDate());
			}
			listaFluxo.add(fluxocaixaBeanReport);
		}
		return listaFluxo;
	}
	
	/**
	 * M�todo para simular as consolida��es do agendamento at� uma determinada data.
	 * <p>
	 * Este m�todo � respons�vel por gerar uma lista de agendamentos que seriam consolidados, ao passar do tempo, em 
	 * uma determidada data (<tt>dtproximo</tt>). Os agendamentos gerados s�o id�nticos aos informados no par�metro <tt>agendamento</tt> exceto
	 * seu atributo <tt>dtproximo</tt>, este ter� intervalos, ao longo da lista, de acordo com a frequ�ncia do <tt>agendamento</tt>.
	 * <b>Para cada agendamento ser� gerada as itera��es, se existirem, com o pr�prio agendamento inclu�do na lista retornada.</b> 
	 * </p>
	 * 
	 * @see Constantes de br.com.linkcom.sined.geral.bean.Frequencia
	 * @see SinedDateUtils#calculaDiferencaDias(Date, Date)
	 * @see SinedDateUtils#incrementDateFrequencia(Date, Frequencia)
	 * 
	 * @param agendamento
	 * @param dataInicio
	 * @param dataFim
	 * @return Lista de agendamento com as simula��es.
	 * @author Fl�vio Tavares
	 * @param filtro 
	 */
	private List<Agendamento> gerarIteracoesAgendamento(Agendamento agendamento, Date dataInicio, Date dataFim, Boolean valoresAnteriores){
		List<Agendamento> listaAgrupada = new ArrayList<Agendamento>();
		
		Frequencia frequencia = agendamento.getFrequencia();
		Date dtproximo = new Date(agendamento.getDtproximo().getTime());
		
		int count = agendamento.getIteracoes() != null ? agendamento.getIteracoes().intValue()+1 : -1;
		int i = 0;
		
		if ((valoresAnteriores != null && valoresAnteriores) && agendamento.getDtfim() != null && agendamento.getDtfim().before(dataInicio)){
			i = 0;
			while(SinedDateUtils.calculaDiferencaDias(dtproximo, agendamento.getDtfim()) >= 0){	

				if(count != -1 && i == count){
					break;
				}
				i++;
				
				Agendamento ag = new Agendamento(agendamento);
				if(!SinedDateUtils.afterIgnoreHour(dtproximo, agendamento.getDtfim())){
					listaAgrupada.add(ag);
					ag.setDtproximo(dtproximo);
				}

				dtproximo = SinedDateUtils.incrementDateFrequencia(dtproximo, frequencia, 1);
		
				if(frequencia.getCdfrequencia().equals(Frequencia.UNICA)){
					break;
				}
			}
		}
		else{
			
			/*
			 * Enquanto houver uma diferen�a de dias entre a data pr�ximo e a data fim do filtro, s�o geradas
			 * itera��es do agendamento em quest�o.
			 */
			i = 0;
			while(SinedDateUtils.calculaDiferencaDias(dtproximo, dataFim) >= 0){
				
				/*
				 *	Se o agendamento tiver prazo para t�rmino e o contador atingir o n�mero m�ximo de itera��es. 
				 */
				if(count != -1 && i == count){
					break;
				}
				i++;
				
				/*
				 *	A lista de itera��es do agendamento s� poder� ser composta por agendamentos cuja data de consolida��o(dtproximo)
				 *	esteja entre a data in�cio e a data fim do filtro, as quais s�o informadas por par�metro. 
				 */
				if((valoresAnteriores != null && valoresAnteriores) || !dtproximo.before(dataInicio)){
					Agendamento ag = new Agendamento(agendamento);
					if(agendamento.getDtfim() != null){
	//					if(!dtproximo.after(agendamento.getDtfim())){
						if(!SinedDateUtils.afterIgnoreHour(dtproximo, agendamento.getDtfim())){
							listaAgrupada.add(ag);
							ag.setDtproximo(dtproximo);
						}
					}else{
						listaAgrupada.add(ag);
						ag.setDtproximo(dtproximo);
					}
					
				}
				/*
				 * A data da pr�xima consolida��o do agendamento(dtproximo) � incrementada de acordo com a frequ�ncia do mesmo.
				 * (Vide Documenta��o do m�todo)
				 */
				dtproximo = SinedDateUtils.incrementDateFrequencia(dtproximo, frequencia, 1);
				
				/*
				 *  Se o agendamento for de frequ�ncia �nica, s� possui uma itera��o, que � ele mesmo.
				 *  O loop deve ser encerrado pois, com frequ�ncia �NICA, a data n�o � incrementada. Portanto este seria eterno.
				 */
				if(frequencia.getCdfrequencia().equals(Frequencia.UNICA)){
					break;
				}
			}
		}
		return listaAgrupada;
	}
	
	/**
	 * Atualiza um agendamento ap�s ser consolildado.
	 *
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#saveAgendamentoConsolidacao(Agendamento, Object)
	 * @param agendamento
	 * @param vinculo
	 * @author Hugo Ferreira
	 */
	public void saveAgendamentoConsolidacao(Agendamento agendamento, Object vinculo) {
		agendamentoDAO.saveAgendamentoConsolidacao(agendamento, vinculo);
	}
	
	/**
	 * <p>Atualiza os campos de um agendamento quando ele � consolidado e gera um documento.
	 * Os campos a atualizar est�o definidos no caso de uso UC0131_ManterAgendamento.</p>
	 * 
	 * @see #carregarParaConsolidar(Integer)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#atualizaHistoricoDocumento(Documento)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#gerenciaHistorico(Agendamento, Documento)
	 * @see #atualizaDtProximo(Agendamento)
	 * @see #saveAgendamentoConsolidacao(Agendamento, Object)
	 * @param nota
	 * @param vinculo
	 * @author Hugo Ferreira
	 */
	public void doConsolidar(Object vinculo) {
		Agendamento agendamento = null;
		Boolean finalizarAgendamento = Boolean.FALSE;
		if (vinculo instanceof Movimentacao) {
			Movimentacao mv = (Movimentacao) vinculo;
			agendamento = this.carregarParaSalvarConsolidar(mv.getCdAgendamento());
			finalizarAgendamento = this.verificaQtdeIteracoes(agendamento, mv, null);
		} else if (vinculo instanceof Documento) {
			Documento doc = (Documento) vinculo;
			documentoService.atualizaHistoricoDocumento(doc, Boolean.FALSE);
			agendamento = this.carregarParaSalvarConsolidar(doc.getCdAgendamento());
			finalizarAgendamento = this.verificaQtdeIteracoes(agendamento, null, doc);
		} else {
			throw new SinedException("Agendamento n�o gera registros de " + vinculo.getClass().getName());
		}
		
		if(finalizarAgendamento)
			agendamento.setSituacao(SituacaoAgendamento.FINALIZADO);
		
		this.atualizaDtProximo(agendamento);
		this.saveAgendamentoConsolidacao(agendamento, vinculo);
	}

	/**
	 * M�todo que verifica se � a �ltima itera��o do agendamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#numeroIteracoesByAgendamento(Agendamento agendamento)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#buscaQtdeIteracoes(Agendamento agendamento, Movimentacao movimentacao, Documento documento)
	 *
	 * @param agendamento
	 * @param mv
	 * @param doc
	 * @return
	 * @author Luiz Fernando
	 */
	private Boolean verificaQtdeIteracoes(Agendamento agendamento, Movimentacao mv, Documento doc) {		
		Integer numeroIteracoes = this.numeroIteracoesByAgendamento(agendamento);
		if(numeroIteracoes != null){
			Integer qtdeIteracoes = this.buscaQtdeIteracoes(agendamento, mv, doc);
			if(qtdeIteracoes != null){
				return (qtdeIteracoes+1) == numeroIteracoes;
			}
		}		
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.AgendamentoDAO#buscaQtdeIteracoes(Agendamento agendamento, Movimentacao movimentacao, Documento documento)
	 *
	 * @param agendamento
	 * @param movimentacao
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	private Integer buscaQtdeIteracoes(Agendamento agendamento, Movimentacao movimentacao, Documento documento) {
		return agendamentoDAO.buscaQtdeIteracoes(agendamento, movimentacao, documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#numeroIteracoesByAgendamento(Agendamento agendamento)
	 *
	 * @param agendamento
	 * @return
	 * @author Luiz Fernando
	 */
	private Integer numeroIteracoesByAgendamento(Agendamento agendamento) {
		return agendamentoDAO.numeroIteracoesByAgendamento(agendamento);
	}
	
	/**
	 * <p>Procedimento de atualiza��o do campo dtProximo. A atualiza��o � feita com base na 
	 * freq�ncia do agendamento.</p>
	 * 
	 * @param agendamento
	 * @author Hugo Ferreira
	 */
	private void atualizaDtProximo(Agendamento agendamento) {
		Calendar novaDtProximo = SinedDateUtils.javaSqlDateToCalendar(agendamento.getDtproximo());
		Integer frequencia = agendamento.getFrequencia().getCdfrequencia();

		switch (frequencia) {
		case 1: //UNICA
			//Decrementa 1 dia no campo dtfim
			if(agendamento.getDtproximo()!=null && agendamento.getDtfim()==null){
				agendamento.setDtfim(agendamento.getDtproximo());
			}else if(agendamento.getDtfim()==null && agendamento.getDtproximo()==null){
				throw new SinedException("Para consolidar o(s) agendamento(s) � necess�rio denifir a 'data final' do(s) agendamento(s).");
			}
			Date dtfim = agendamento.getDtfim();
			dtfim = SinedDateUtils.incrementDate(dtfim, -1, Calendar.DAY_OF_MONTH);
			agendamento.setDtfim(dtfim);
			break;
		case 2: //DIARIA
			novaDtProximo.add(Calendar.DAY_OF_MONTH, 1);
			break;
		case 3: //SEMANAL
			novaDtProximo.add(Calendar.DAY_OF_MONTH, 7);
			break;
		case 4: //QUINZENAL
			novaDtProximo.add(Calendar.DAY_OF_MONTH, 15);
			break;
		case 5: //MENSAL
			Calendar dtAux = (Calendar)novaDtProximo.clone();
			dtAux.set(Calendar.MONTH, dtAux.get(Calendar.MONTH) + 1);

			if (dtAux.get(Calendar.MONTH) > (novaDtProximo.get(Calendar.MONTH) + 1)) {
				dtAux.set(Calendar.DAY_OF_MONTH, 1);
				dtAux.add(Calendar.DAY_OF_MONTH, -1);
			}

			novaDtProximo = dtAux;
			break;
		case 6:
			novaDtProximo.add(Calendar.YEAR, 1);
			break;
		case 7: //SEMESTRAL
			novaDtProximo.add(Calendar.MONTH, 6);
			break;
		case 9: //TRIMESTRAL
			novaDtProximo.add(Calendar.MONTH, 3);
			break;
		}

		agendamento.setDtproximo(new Date(novaDtProximo.getTimeInMillis()));
	}

	/**
	 * <p>Calcula a data da �ltima itera��o de um agendamento.</p>
	 * <p>O c�lculo � feito com base na primeira data, somando-se a frequ�ncia
	 * 'iteracoes' vezes. Por exemplo: se a primeira data for 01/01/2000, a frequencia for
	 * 5 (Mensal - ver classe {@link br.com.linkcom.sined.geral.bean.Frequencia}) e o n�mero de
	 * itera��es for 12, a �ltima data ser� dtFim.add(Calendar.MONTH, (iteracoes - 1)) = 01/12/2000.</p>
	 * 
	 * @see br.com.linkcom.sined.util.SinedUtil#javaSqlDateToCalendar(Date date)
	 * @param request
	 * @param frequencia
	 * @param iteracoes
	 * @param primeiraData
	 * @author Hugo Ferreira
	 */
	public Date calcularDataUltimaParcela(Frequencia frequencia, Integer iteracoes, Date primeiraData) {
		if (frequencia == null || frequencia.getCdfrequencia() == null) {
			throw new SinedException("O campo 'Frequ�ncia' n�o pode ser nulo. Informe uma frequ�ncia.");
		}
		if (iteracoes == null || iteracoes < 1) {
			throw new SinedException("O campo 'N�mero de itera��es' est� vazio ou possui um valor inv�lido. Entre com um n�mero maior ou igual a 1.");
		}
		if (primeiraData == null) {
			throw new SinedException("O campo 'Pr�ximo vencimento' est� vazio. Entre com uma data neste campo.");
		}

		Integer freq = frequencia.getCdfrequencia();
		Calendar ultimaData = SinedDateUtils.javaSqlDateToCalendar(primeiraData);

		switch (freq) {
		case 2: //DIARIA
			ultimaData.add(Calendar.DAY_OF_MONTH, iteracoes);
			break;
		case 3: //SEMANAL
			ultimaData.add(Calendar.DAY_OF_MONTH, 7*iteracoes);
			break;
		case 4: //QUINZENAL
			ultimaData.add(Calendar.DAY_OF_MONTH, 15*iteracoes);
			break;
		case 5: //MENSAL
			ultimaData.add(Calendar.MONTH, iteracoes);
			break;
		case 6: // ANUAL
			ultimaData.add(Calendar.YEAR, iteracoes);
			break;
		case 7: // SEMESTRAL
			ultimaData.add(Calendar.MONTH, iteracoes*6);
			break;
		case 9: //TRIMESTRAL	
			ultimaData.add(Calendar.MONTH, iteracoes*3);
			break;
		default:
			break;
		}
		
		Date dtfim = new Date(ultimaData.getTimeInMillis());
		dtfim = SinedDateUtils.incrementDate(dtfim, -1, Calendar.DAY_OF_MONTH);
		return dtfim;
	}
	
	/**
	 * <p>Limpa os Id's dos rateios e dos juros, para copiar o agendamento
	 * ao consolidar.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioService#limpaReferenciaRateio(br.com.linkcom.sined.geral.bean.Rateio)
	 * @see br.com.linkcom.sined.geral.service.TaxaService#limpaReferenciasTaxa(br.com.linkcom.sined.geral.bean.Taxa)
	 * @param agendamento
	 * @author Hugo Ferreira
	 */
	public void limparDadosAgendamento(Agendamento agendamento) {
		if (agendamento.getRateio() != null) {
			rateioService.limpaReferenciaRateio(agendamento.getRateio());
		}

		if (agendamento.getTaxa() != null) {
			taxaService.limpaReferenciasTaxa(agendamento.getTaxa());
		}
	}

	public boolean alterouDtProximo(Agendamento agendamento) {
		Agendamento agendamento2 = this.load(agendamento);
		if (agendamento2 == null) {
			return false;
		}
		if (SinedDateUtils.equalsIgnoreHour(agendamento.getDtproximo(), agendamento2.getDtproximo())) {
			return false;
		}
		return true;
	}
	
	/* singleton */
	private static AgendamentoService instance;
	public static AgendamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AgendamentoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo para atualizar todos os registros da tabela AUX_AGENDAMENTO
	 *
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		agendamentoDAO.updateAux();
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem de acordo com o filtro
	 * 
	 * @param filtro
	 * @return
	 * @auhtor Tom�s Rabelo
	 */
	public IReport gerarRelatorio(AgendamentoFiltro filtro) {
		Report report = new Report("/financeiro/agendamento");
		
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Agendamento> agendamentos = agendamentoDAO.findForListagem(filtro);
		if(agendamentos != null && agendamentos.list().size() > 0)
			report.setDataSource(agendamentos.list());
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamento> findAgendamentosForBuscaGeral(String busca) {
		return agendamentoDAO.findAgendamentosForBuscaGeral(busca);
	}
	
	/**
	 * M�todo que cria relat�rio CSV, com os mesmo dados do relat�rio PDF
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource gerarRelatorioCSV(AgendamentoFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Agendamento> agendamentos = agendamentoDAO.findForListagem(filtro);
		StringBuilder csv = new StringBuilder();
		csv.append("Agend.;Descri��o;Tipo de pagamento;Pessoa;V�nculo;O;Valor;�lt. consol.;Pr�x. venc.;Situa��o;Ativo\n");
		
		if(agendamentos != null && agendamentos.list().size() > 0){
			for (Agendamento agendamento : agendamentos.list()) {
				csv
				.append(agendamento.getCdagendamento()).append(";")
				.append(agendamento.getDescricao()).append(";")
				.append(agendamento.getTipopagamento().getNome()).append(";")
				.append(agendamento.getPessoaOutros() == null ? "" : agendamento.getPessoaOutros()).append(";")
				.append(agendamento.getVinculo()).append(";")
				.append(agendamento.getTipooperacao().getNome()).append(";")
				.append(agendamento.getValor()).append(";")
				.append(agendamento.getAux_agendamento().getDtUltimaConsolidacao() == null ? "" : agendamento.getAux_agendamento().getDtUltimaConsolidacao()).append(";")
				.append(agendamento.getDtproximo()).append(";")
				.append(agendamento.getSituacao().getNome()).append(";")
				.append(agendamento.getAtivo() != null && agendamento.getAtivo() ? "Sim" : "N�o").append(";\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "agendamento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public Agendamento loadTipooperacao(Agendamento agendamento){
		return agendamentoDAO.loadTipooperacao(agendamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAConsolidar() {
		return agendamentoDAO.getQtdeAConsolidar();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAtrasados() {
		return agendamentoDAO.getQtdeAtrasados();
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		return agendamentoDAO.verificaListaFechamento(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoDAO#loadWithPessoa(Agendamento agendamento)
	 *
	 * @param agendamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Agendamento loadWithPessoa(Agendamento agendamento){
		return agendamentoDAO.loadWithPessoa(agendamento);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Agendamento> loadListAgendamentoEstorno(String whereIn) {
		return agendamentoDAO.loadListAgendamentoEstorno(whereIn);
	}
	
	/**
	 * 
	 * @param listaAgendamento
	 */
	public void estornarAgendamento(List<Agendamento> listaAgendamento) {
		try {
			agendamentoDAO.estornarAgendamento(listaAgendamento);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("N�o foi poss�vel completar o estorno do agendamento.");
		}
		
	}
	
	/**
	 * M�todo com refer�ncias no DAO
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Agendamento> findByCentroCustoValidacao(Centrocusto centrocusto){
		return agendamentoDAO.findByCentroCustoValidacao(centrocusto);
	}
	
	/**
	 * M�todo com refer�ncias no DAO
	 * @param contagerencial
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Agendamento> findByContaGerencialValidacao(Contagerencial contagerencial){
		return agendamentoDAO.findByContaGerencialValidacao(contagerencial);
	}
	
	/**
	 * M�todo com refer�ncias no DAO
	 * @param contacontabil
	 * @return
	 * @since 29/10/2019	
	 * @author Arthur Gomes
	 */
	public List<Agendamento> findByContaGerencialValidacao(ContaContabil contacontabil){
		return agendamentoDAO.findByContaGerencialValidacao(contacontabil);
	}
	
	public void criarAvisoAgendamentoProximo(Motivoaviso m, Date data, Date dateToSearch) {
		List<Agendamento> agendamentoList = agendamentoDAO.findByDiasFaltantes(2, data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		for (Agendamento a : agendamentoList) {
			Aviso aviso = new Aviso("Agendamento pr�ximo do vencimento", "C�digo do agendamento: " + a.getCdagendamento(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), a.getCdagendamento(), a.getEmpresa() != null ? a.getEmpresa() : empresaService.loadPrincipal(), 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, a.getCdagendamento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, a.getCdagendamento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public void criarAvisoAgendamentoAtrasado(Motivoaviso m, Date data, Date dateToSearch) {
		List<Agendamento> agendamentoList = agendamentoDAO.findByAtraso(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		for (Agendamento a : agendamentoList) {
			Aviso aviso = new Aviso("Agendamento atrasado", "C�digo do agendamento: " + a.getCdagendamento(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), a.getCdagendamento(), a.getEmpresa() != null ? a.getEmpresa() : empresaService.loadPrincipal(), 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, a.getCdagendamento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, a.getCdagendamento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	public Agendamento findByCdAgendamento(Integer cdAgendamento) {
		return agendamentoDAO.findByCdAgendamento(cdAgendamento);		
	}
	
	public void updateValor(Agendamento agendamento, Money valor){
		agendamentoDAO.updateValor(agendamento, valor);
	}
	
	public List<Agendamento> findAutoCompleteWithEmpresa(String nome){
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));;
		} catch (Exception e) {} 
		return agendamentoDAO.findAutoCompleteWithEmpresa(nome, empresa);
	}
	
	public void updateDtproximo(Agendamento agendamento, Date dtProximo) {
		agendamentoDAO.updateDtproximo(agendamento, dtProximo);
	}
	public Agendamento findValorAgendamento(Agendamento agendamento) {
		return agendamentoDAO.findValorAgendamento(agendamento);
	}
	
	public Agendamento criarCopia(Agendamento origem) {
		Agendamento a = agendamentoDAO.findAgendamento(origem);
		origem = loadForEntrada(origem);
		
		Agendamento copia = new Agendamento();
		copia = origem;
		copia.setRateio(a.getRateio());
		copia.setCdagendamento(null);
		copia.setListaAgendamentoHistorico(null);
		
		Pessoa pessoa = pessoaService.load(a.getPessoa());
		copia.setPessoa(pessoa);
		
		if (Tipopagamento.COLABORADOR.equals(origem.getTipopagamento())){
			copia.setFornecedor(null);
			copia.setCliente(null);
			copia.setOutrospagamento(null);
			if(pessoa != null && pessoa.getCdpessoa()!=null){
				copia.setColaborador(colaboradorService.findColaboradorByPk(pessoa.getCdpessoa()));
			}
		}else if (Tipopagamento.FORNECEDOR.equals(origem.getTipopagamento())){
			copia.setColaborador(null);
			copia.setCliente(null);
			copia.setOutrospagamento(null);
			if(Util.objects.isPersistent(pessoa)){
				copia.setFornecedor(new Fornecedor(pessoa.getCdpessoa(), pessoa.getNome()));
			}
		}else if (Tipopagamento.CLIENTE.equals(origem.getTipopagamento())){
			copia.setColaborador(null);
			copia.setFornecedor(null);
			copia.setOutrospagamento(null);
			if(Util.objects.isPersistent(pessoa)){
				copia.setCliente((Cliente) new Cliente(pessoa.getCdpessoa(), pessoa.getNome()));
			}
		}else if (Tipopagamento.OUTROS.equals(origem.getTipopagamento())){
			copia.setColaborador(null);
			copia.setFornecedor(null);
			copia.setCliente(null);
		}

		if(origem.getRateio() != null && origem.getRateio().getCdrateio() != null){
			Rateio rateio = rateioService.findRateio(origem.getRateio());
			if(rateio != null){
				copia.setRateio(rateio);
				rateioService.limpaReferenciaRateio(copia.getRateio());
			}
		}
		
		return copia;
		
	}
}