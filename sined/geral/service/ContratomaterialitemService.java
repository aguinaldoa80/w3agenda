package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialitem;
import br.com.linkcom.sined.geral.dao.ContratomaterialitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratomaterialitemService extends GenericService<Contratomaterialitem> {

	private ContratomaterialitemDAO contratomaterialitemDAO;
	
	public void setContratomaterialitemDAO(
			ContratomaterialitemDAO contratomaterialitemDAO) {
		this.contratomaterialitemDAO = contratomaterialitemDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialitemDAO#findByContratomaterial(Contratomaterial contratomaterial)
	 *
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public List<Contratomaterialitem> findByContratomaterial(Contratomaterial contratomaterial) {
		return contratomaterialitemDAO.findByContratomaterial(contratomaterial);
	}

}
