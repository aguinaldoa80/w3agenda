package br.com.linkcom.sined.geral.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.dao.MovpatrimonioDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovpatrimonioFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovpatrimonioService extends GenericService<Movpatrimonio> {

	private MovpatrimonioDAO movpatrimonioDAO;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private LocalarmazenagemService localarmazenagemService;
	private PatrimonioitemService patrimonioitemService;
	
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMovpatrimonioorigemService(
			MovpatrimonioorigemService movpatrimonioorigemService) {
		this.movpatrimonioorigemService = movpatrimonioorigemService;
	}
	public void setMovpatrimonioDAO(MovpatrimonioDAO movpatrimonioDAO) {
		this.movpatrimonioDAO = movpatrimonioDAO;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	
	@Override
	public void saveOrUpdate(final Movpatrimonio bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				if (bean.getMovpatrimonioorigem() == null) {
					Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
					movpatrimonioorigem.setUsuario(SinedUtil.getUsuarioLogado());
					movpatrimonioorigemService.saveOrUpdateNoUseTransaction(movpatrimonioorigem);
					
					bean.setMovpatrimonioorigem(movpatrimonioorigem);
				}
				
				saveOrUpdateNoUseTransaction(bean);
				
				return null;
			}
		});
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovpatrimonioDAO#findLastMov
	 * @param patrimonioitem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movpatrimonio findLastMov(Movpatrimonio movpatrimonio) {
		return movpatrimonioDAO.findLastMov(movpatrimonio);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovpatrimonioDAO#isLastMov
	 * @param movpatrimonio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isLastMov(Movpatrimonio movpatrimonio){
		return movpatrimonioDAO.isLastMov(movpatrimonio);
	}
	public Boolean existMovPatrimonioRomaneio(Movpatrimonio movpatrimonio){
		return movpatrimonioDAO.isMovPatrimonioRomaneio(movpatrimonio);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovpatrimonioDAO#updateCancelada
	 * @param movpatrimonio
	 * @author Rodrigo Freitas
	 */
	public void updateCancelada(Movpatrimonio movpatrimonio) {
		movpatrimonioDAO.updateCancelada(movpatrimonio);		
	}
	
	public void updateCancelada(String whereIn) {
		movpatrimonioDAO.updateCancelada(whereIn);		
	}
	
	
	/**
	 * M�todo monta a origem dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Movpatrimonio form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getMovpatrimonioorigem().getUsuario() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.CADASTRO, "CADASTRO", form.getMovpatrimonioorigem().getUsuario().getNome(), 
															  form.getMovpatrimonioorigem().getUsuario().getCdpessoa() != null ? form.getMovpatrimonioorigem().getUsuario().getCdpessoa().toString() : "", 
																	  null);
		
			listaBean.add(origemsuprimentosBean);
		} else if(form.getMovpatrimonioorigem().getRequisicaomaterial() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.REQUISICAO, "REQUISI��O DE MATERIAL", 
					  form.getMovpatrimonioorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null ?  form.getMovpatrimonioorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString() : "", 
					  form.getMovpatrimonioorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null ? form.getMovpatrimonioorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		} else if(form.getMovpatrimonioorigem().getEntrega() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTREGA, "ENTREGA", 
					  form.getMovpatrimonioorigem().getEntrega().getCdentrega() != null ? form.getMovpatrimonioorigem().getEntrega().getCdentrega().toString() : "", 
					  form.getMovpatrimonioorigem().getEntrega().getCdentrega() != null ? form.getMovpatrimonioorigem().getEntrega().getCdentrega().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		} else if(form.getMovpatrimonioorigem().getRomaneio() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ROMANEIO, "ROMANEIO", 
					  form.getMovpatrimonioorigem().getRomaneio().getCdromaneio() != null ? form.getMovpatrimonioorigem().getRomaneio().getCdromaneio().toString() : "", 
					  form.getMovpatrimonioorigem().getRomaneio().getCdromaneio() != null ? form.getMovpatrimonioorigem().getRomaneio().getCdromaneio().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * Gera o relat�rio de Movimenta��o de patrim�nio
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovpatrimonioDAO#findForListagem(br.com.linkcom.neo.controller.crud.FiltroListagem)
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorio(MovpatrimonioFiltro filtro) {
		Report report = new Report("/suprimento/movpatrimonio");
		
		List<Movpatrimonio> lista = movpatrimonioDAO.findForReport(filtro);
		
//		if ("TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
//			Map<Integer, List<Materialnumeroserie>> mapa = new HashMap<Integer, List<Materialnumeroserie>>();
//			String whereIn = "-1";
//			
//			for (Movpatrimonio movpatrimonio: lista){
//				if (movpatrimonio.getPatrimonioitem()!=null &&  movpatrimonio.getPatrimonioitem().getBempatrimonio()!=null){
//					whereIn += ", " +movpatrimonio.getPatrimonioitem().getBempatrimonio().getCdmaterial();
//				}
//			}
//			
//			for (Materialnumeroserie materialnumeroserie: materialnumeroserieService.findByCdsmaterial(whereIn)){
//				List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(materialnumeroserie.getMaterial().getCdmaterial());
//				if (listaMaterialnumeroserie==null){
//					listaMaterialnumeroserie = new ArrayList<Materialnumeroserie>();
//				}
//				listaMaterialnumeroserie.add(materialnumeroserie);
//				mapa.put(materialnumeroserie.getMaterial().getCdmaterial(), listaMaterialnumeroserie);
//			}			
//			
//			for (Movpatrimonio movpatrimonio: lista){
//				if (movpatrimonio.getPatrimonioitem()!=null &&  movpatrimonio.getPatrimonioitem().getBempatrimonio()!=null){
//					List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(movpatrimonio.getPatrimonioitem().getBempatrimonio().getCdmaterial());
//					if (listaMaterialnumeroserie!=null){
//						movpatrimonio.getPatrimonioitem().getBempatrimonio().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, listaMaterialnumeroserie));
//					}
//				}
//			}
//		}
		
		
		report.setDataSource(lista);
		report.addParameter("TEXTO_PLAQUETA_SERIE", patrimonioitemService.getTextoPlaquetaSerie());
		
		return report;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param requisicaomaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQuantidadeMovimentadaDaRequisicaoMaterial(Requisicaomaterial requisicaomaterial) {
		return movpatrimonioDAO.getQuantidadeMovimentadaDaRequisicaoMaterial(requisicaomaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movpatrimonio> getListaMovPatrimonioDaEntrega(Entrega entrega) {
		return movpatrimonioDAO.getListaMovPatrimonioDaEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#existMovPatrimonioDaEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMovPatrimonioDaEntrega(Entrega entrega) {
		return movpatrimonioDAO.existMovPatrimonioDaEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param movpatrimonio
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isPlaquetaValida(Movpatrimonio movpatrimonio) {
		return movpatrimonioDAO.isPlaquetaValida(movpatrimonio);
	}
	
	public Double qtdeDisponivelPatrimonio(Material material, Patrimonioitem patrimonioitem, Localarmazenagem localarmazenagemorigem) {
		return movpatrimonioDAO.qtdeDisponivelPatrimonio(material, patrimonioitem, localarmazenagemorigem, null, null);
	}
	
	public Double qtdeDisponivelPatrimonio(Material material, Patrimonioitem patrimonioitem, Localarmazenagem localarmazenagemorigem, Empresa empresa, Projeto projeto) {
		return movpatrimonioDAO.qtdeDisponivelPatrimonio(material, patrimonioitem, localarmazenagemorigem, empresa, projeto);
	}
	
	public List<Movpatrimonio> gerarMovimentacaoNota(Notafiscalproduto notafiscalproduto) {
		List<Movpatrimonio> listaMovpatrimonio = new ArrayList<Movpatrimonio>();
		
		if(notafiscalproduto != null && 
				notafiscalproduto.getCdNota() != null && 
				notafiscalproduto.getListaItens() != null && 
				!notafiscalproduto.getListaItens().isEmpty()){
			Movpatrimonio movpatrimonio;
			
			for(Notafiscalprodutoitem item : notafiscalproduto.getListaItens()){
				if(item.getMaterialclasse() != null && item.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
					if(item.getQtde() != null){
						for(int i = 0; i < item.getQtde(); i++){
							movpatrimonio = new Movpatrimonio();
							
							movpatrimonio.setMaterial(item.getMaterial());
							if(i == 0){
								movpatrimonio.setPatrimonioitem(item.getPatrimonioitem());
							}
							movpatrimonio.setNotafiscalproduto(notafiscalproduto);
							if(item.getLocalarmazenagem() != null){
								movpatrimonio.setLocalarmazenagem(item.getLocalarmazenagem());
							}else if (notafiscalproduto.getEmpresa() != null) {
								List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(notafiscalproduto.getEmpresa());
								if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
									movpatrimonio.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
							}
							movpatrimonio.setNotafiscalproduto(notafiscalproduto);
							
							listaMovpatrimonio.add(movpatrimonio);
						}
					}
				}
			}
		}
		
		return listaMovpatrimonio;
	}
	
	public boolean existMovpatrimonioByNotas(String whereIn) {
		return movpatrimonioDAO.existMovpatrimonioByNotas(whereIn);
	}
	
	public List<Movpatrimonio> findByRomaneio(Romaneio romaneio) {
		return movpatrimonioDAO.findByRomaneio(romaneio);
	}
	
	public String makeLinkHistoricoMovpatrimonio(Movpatrimonio movpatrimonio) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<a href=\"javascript:visualizarMovpatrimonio(")
			.append(movpatrimonio.getCdmovpatrimonio())
			.append(");\">")
			.append(movpatrimonio.getCdmovpatrimonio())
			.append("</a>");
		
		return sb.toString();
	}

	public List<Movpatrimonio> findForIndenizacaoContrato(String whereIn){
		return movpatrimonioDAO.findForIndenizacaoContrato(whereIn);
	}

	public Resource generateCsvReport(MovpatrimonioFiltro filtro, String nomeArquivo) {
		List<Movpatrimonio> lista = movpatrimonioDAO.findForReport(filtro);
		
		String cabecalho = "\"Mov.\";\"Data\";\"Item de patrim�nio\";\"Grupo do material\";\"Tipo do material\";\"Plaqueta/S�rie\";\"Origem\";\"Destino\";\"Cancel\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
		
		for(Movpatrimonio bean : lista) {
			csv.append("\"" + bean.getCdmovpatrimonio() + "\";");
			csv.append("\"" + pattern.format(bean.getDtmovimentacao()) + "\";");
			csv.append("\"" + bean.getPatrimonioitem().getDescricao() + "\";");
			csv.append("\"" + bean.getPatrimonioitem().getBempatrimonio().getMaterialgrupo().getNome() + "\";");
			
			if(bean.getPatrimonioitem().getBempatrimonio().getMaterialtipo() != null) {
				csv.append("\"" + bean.getPatrimonioitem().getBempatrimonio()
						.getMaterialtipo().getNome() + "\";");
			} else {
				csv.append("\" \";");
			}
			
			csv.append("\"" + bean.getPatrimonioitem().getPlaquetaSerie() + "\";");
			
			if(bean.getFornecedororigem() != null) {
				csv.append("\"" + bean.getFornecedororigem().getNome() + "\";");
			} else if(bean.getVmovpatrimonio() != null) {
				if(bean.getVmovpatrimonio().getFornecedor() != null) {
					csv.append("\"" + bean.getVmovpatrimonio().getFornecedor().getNome() + "\";");
				} else if(bean.getVmovpatrimonio().getLocalarmazenagem() != null) {
					csv.append("\"" + bean.getVmovpatrimonio().getLocalarmazenagem().getNome() + "\";");
				} else {
					csv.append("\"Desconhecida\";");
				}
			} else {
				csv.append("\"Desconhecida\";");
			}
			
			if(bean.getFornecedor() != null) {
				csv.append("\"" + bean.getFornecedor().getNome() + "\";");
			} else if (bean.getLocalarmazenagem() != null) {
				csv.append("\"" + bean.getLocalarmazenagem().getNome() + "\";");
			} else {
				csv.append("\" \";");
			}
			
			csv.append("\"" + (bean.getDtcancelamento() != null ? "Sim" : "N�o") + "\";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
	
	public List<Movpatrimonio> findBySerie(Materialnumeroserie materialNumeroSerie) {
		return movpatrimonioDAO.findBySerie(materialNumeroSerie);
	}
}
