package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ChequeMovimentacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeChequeDoc;


public class ChequemovimentacaoService {

	/**
	 * Crio uma lista que representa o relat�rio
	 *
	 * @param listaMovimentacao
	 * @author Thiago Clemente
	 */
	public List<ChequeMovimentacaoBean> getListaBean(List<Movimentacao> listaMovimentacao){
		
		List<ChequeMovimentacaoBean> listaBean = new ArrayList<ChequeMovimentacaoBean>();
		
		String cidade = "";
		Empresa empresa = EmpresaService.getInstance().loadPrincipal();
		
		if (empresa!=null && empresa.getCdpessoa()!=null){
			List<Endereco> listaEndereco = EnderecoService.getInstance().carregarListaEndereco(empresa);
			try {
				cidade = listaEndereco.get(0).getMunicipio().getNome();
			}catch(Exception e){				
			}
		}
		
		for (Movimentacao movimentacao: listaMovimentacao){
			
			String nominal = "";
			try {
				nominal = getNominalCheque(movimentacao.getListaMovimentacaoorigem().get(0).getDocumento());
			}catch(Exception e){				
			}			
			
			ChequeMovimentacaoBean bean;
			if(movimentacao.getCheque() != null && movimentacao.getCheque().getNumero() != null)
				bean = new ChequeMovimentacaoBean(movimentacao.getCheque().getNumero().toString(), movimentacao.getValor(), StringUtils.isBlank(movimentacao.getHistorico()) ?  "" : movimentacao.getHistorico(), movimentacao.getDtmovimentacao(), StringUtils.isBlank(cidade) ? "" : cidade, StringUtils.isBlank(nominal) ? "" : nominal);
			else
				bean = new ChequeMovimentacaoBean("", movimentacao.getValor(), StringUtils.isBlank(movimentacao.getHistorico()) ? "" : movimentacao.getHistorico(), movimentacao.getDtmovimentacao(), StringUtils.isBlank(cidade) ? "" : cidade, StringUtils.isBlank(nominal) ? "" : nominal);
			listaBean.add(bean);			
		}
		
		return listaBean;
	}
	
	public List<ChequeMovimentacaoBean> getListaBeanDocumento(List<Documento> listaContapagar){
		List<ChequeMovimentacaoBean> listaBean = new ArrayList<ChequeMovimentacaoBean>();
		
		String cidade = "";
		Empresa empresa = EmpresaService.getInstance().loadPrincipal();
		
		if (empresa!=null && empresa.getCdpessoa()!=null){
			List<Endereco> listaEndereco = EnderecoService.getInstance().carregarListaEndereco(empresa);
			try {
				cidade = listaEndereco.get(0).getMunicipio().getNome();
			}catch(Exception e){				
			}
		}
		
		for (Documento documento: listaContapagar){
			
			String nominal = "";
			if(isPreencherNominal(documento, listaContapagar)){
				try {
					nominal = getNominalCheque(documento);
				}catch(Exception e){				
				}	
			}
			
			ChequeMovimentacaoBean bean;
			if(documento.getCheque() != null){
				bean = new ChequeMovimentacaoBean(documento.getCheque(), documento.getAux_documento().getValoratual(), documento.getDescricao(), documento.getDtvencimento(), cidade, nominal);
			}else if(documento.getNumero() != null)
				bean = new ChequeMovimentacaoBean(documento.getNumero(), documento.getAux_documento().getValoratual(), documento.getDescricao(), documento.getDtvencimento(), cidade, nominal);
			else
				bean = new ChequeMovimentacaoBean("", documento.getAux_documento().getValoratual(), documento.getDescricao(), documento.getDtvencimento(), cidade, nominal);
			listaBean.add(bean);			
		}
		
		
		List<ChequeMovimentacaoBean> listaAgrupada = new ArrayList<ChequeMovimentacaoBean>();
		boolean achou = false;
		for (ChequeMovimentacaoBean bean : listaBean) {
			achou = false;
			for (ChequeMovimentacaoBean bean2 : listaAgrupada) {
				if(bean.getCheque() != null && bean.getCheque().getNumero() != null && bean2.getCheque() != null && bean2.getCheque().getNumero() != null){
					if(bean.getCheque().getNumero().equals(bean2.getCheque().getNumero())){
						bean2.setValor(bean2.getValor().add(bean.getValor()));
						bean2.ajustaValorExtensoInterno();
						achou = true;
						break;
					}
				}else {
					if(bean.getChequenumero() != null && !bean.getChequenumero().equals("") && bean2.getChequenumero() != null && bean.getChequenumero().equals(bean2.getChequenumero())){
						bean2.setValor(bean2.getValor().add(bean.getValor()));
						bean2.ajustaValorExtensoInterno();
						achou = true;
						break;
					}
				}
			}
			
			if(!achou){
				listaAgrupada.add(bean);
			}
		}
		
		return listaAgrupada;
	}
	
	private String getNominalCheque(Documento documento) {
		if(documento != null && documento.getPessoa() != null){
			if(Tipopagamento.CLIENTE.equals(documento.getTipopagamento())){
				String razaosocial = ClienteService.getInstance().load(new Cliente(documento.getPessoa().getCdpessoa()), "cliente.cdpessoa, cliente.razaosocial").getRazaosocial();
				return StringUtils.isNotBlank(razaosocial) ? razaosocial : documento.getPessoa().getNome();
			}else if(Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())){
				String razaosocial = FornecedorService.getInstance().load(new Fornecedor(documento.getPessoa().getCdpessoa()), "fornecedor.cdpessoa, fornecedor.razaosocial").getRazaosocial();
				return StringUtils.isNotBlank(razaosocial) ? razaosocial : documento.getPessoa().getNome();
			}else {
				return documento.getPessoa().getNome();
			}
		}
		return null;
	}

	/**
	 * M�todo que verifica se existe documento com o mesmo cheque e pessoa diferente
	 *
	 * @param documento
	 * @param listaContapagar
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean isPreencherNominal(Documento documento, List<Documento> listaContapagar) {
		boolean isPreencherNominal = true;
		if(documento.getCheque() != null && documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
			for(Documento d : listaContapagar){
				if(d.getCheque() != null && d.getCheque().getCdcheque() != null && d.getPessoa() != null && d.getPessoa().getCdpessoa() != null){
//					SE EXISTIR DOCUMENTO COM O MESMO CHEQUE E PESSOA DIFERENTE N�O PREENCHER CAMPO NOMINAL
					if(!d.getCddocumento().equals(documento.getCddocumento()) && 
							d.getCheque().getCdcheque().equals(documento.getCheque().getCdcheque()) &&
							!d.getPessoa().getCdpessoa().equals(documento.getPessoa().getCdpessoa())){
						isPreencherNominal = false;
					}
				}
			}
		}
		
		return isPreencherNominal;
	}

	/**
	 * Clonagem da lista que representa o relat�rio
	 *
	 * @param listaBean
	 * @author Thiago Clemente
	 */
	public List<ChequeMovimentacaoBean> getListaClonada(List<ChequeMovimentacaoBean> listaBean) throws CloneNotSupportedException {
		
		List<ChequeMovimentacaoBean> listaClone = new ArrayList<ChequeMovimentacaoBean>();
		
		for (ChequeMovimentacaoBean bean: listaBean){
			listaClone.add((ChequeMovimentacaoBean) bean.clone());
		}
		
		return listaClone;
	}
	
	/**
	 * Crio uma lista com os cheques apenas num�ricos
	 *
	 * @param listaBean
	 * @author Thiago Clemente
	 */
	public List<ChequeMovimentacaoBean> getListaNumericos(List<ChequeMovimentacaoBean> listaBean) {
		
		List<ChequeMovimentacaoBean> listaNumerico = new ArrayList<ChequeMovimentacaoBean>();
		
		for (ChequeMovimentacaoBean bean: listaBean){
			try {
				new Long(bean.getChequenumero());
				listaNumerico.add(bean);
			}catch(NumberFormatException e){				
			}
		}
		
		return listaNumerico;
	}
	
	/**
	 * Crio uma lista com os cheques apenas n�o num�ricos
	 *
	 * @param listaBean
	 * @author Thiago Clemente
	 */
	public List<ChequeMovimentacaoBean> getListaNaoNumericos(List<ChequeMovimentacaoBean> listaBean) {
		
		List<ChequeMovimentacaoBean> listaNaoNumerico = new ArrayList<ChequeMovimentacaoBean>();
		
		for (ChequeMovimentacaoBean bean: listaBean){
			try {
				new Long(bean.getChequenumero());				
			}catch(NumberFormatException e){
				listaNaoNumerico.add(bean);
			}
		}
		
		return listaNaoNumerico;
	}
	
	/**
	 * Ordeno a lista que representa o relat�rio, de acordo com o tipo de ordena��o.<br>
	 * Converto o campo "n�mero do cheque" de String para "Long" e ordeno pelo par�metro passado (asc) no metodo.<br>
	 * Aos registros que n�o for poss�vel converter, aparecer� no final da lista, ordenados pelo formato String mesmo. 
	 *
	 * @param listaBean
	 * @param asc
	 * @author Thiago Clemente
	 */
	public List<ChequeMovimentacaoBean> getListaOrdenada(List<ChequeMovimentacaoBean> listaBean, boolean asc) throws CloneNotSupportedException {
		
		List<ChequeMovimentacaoBean> listaOrdenada = new ArrayList<ChequeMovimentacaoBean>();
		List<ChequeMovimentacaoBean> listaClone = getListaClonada(listaBean);
		List<ChequeMovimentacaoBean> listaNumericos = getListaNumericos(listaClone);
		List<ChequeMovimentacaoBean> listaNaoNumericos = getListaNaoNumericos(listaClone);
				
		/***************************************************************************************
		Primeiro eu ordeno apenas a lista com os cheques que podem ser convertidos em n�meros.		
		Depois, eu ordeno a lista apenas com os cheques que n�o podem ser convertidos em n�meros, 
		deixando nas �ltimas posi��es.
		****************************************************************************************/
		
		Collections.sort(listaNumericos);
		Collections.sort(listaNaoNumericos);
		
		listaOrdenada.addAll(listaNumericos);
		listaOrdenada.addAll(listaNaoNumericos);
		
		if (!asc){
			Collections.reverse(listaOrdenada);
		}
				
		addOrdem(listaOrdenada);
		
		return listaOrdenada;		
	}
	
	/**
	 * Adiciono a ordem crescente na lista
	 *
	 * @param listaBean
	 * @author Thiago Clemente
	 */
	public void addOrdem(List<ChequeMovimentacaoBean> listaBean){
		
		int i = 1;
		for (ChequeMovimentacaoBean bean: listaBean){
			bean.setOrdem(i++);
		}
	}
	
	/**
	 * M�todo que preenche uma lista para gerar os cheques com template configuravel.
	 * 
	 * @param whereIn
	 * @param order
	 * @return LinkedList<ChequeMovimentacaoBean>
	 * @author Jo�o Vitor
	 * @since 03/12/2014
	 */
	public LinkedList<ChequeMovimentacaoBean> geraListaChequeMovimentacaoBean(String whereIn, String whereInCheque, String order) throws Exception {
		boolean asc = order==null || order.equals("asc");
		MovimentacaoService movimentacaoService = Neo.getObject(MovimentacaoService.class);
		List<Movimentacao> listaMovimentacao = movimentacaoService.findForChequeMovimentacao(whereIn, whereInCheque);
		List<ChequeMovimentacaoBean> listaBean = this.getListaBean(listaMovimentacao);
		List<ChequeMovimentacaoBean> listaOrdenada = this.getListaOrdenada(listaBean, asc);
		
		LinkedList<ChequeMovimentacaoBean> listaChequeMovimentacao = new LinkedList<ChequeMovimentacaoBean>();
		listaChequeMovimentacao.addAll(listaOrdenada);
		
		return listaChequeMovimentacao; 
	}
	
	public LinkedList<EmitirCopiaChequeBean> geraListaCopiaChequeBean(List<Cheque> cheques){
		LinkedList<EmitirCopiaChequeBean> retorno = new LinkedList<EmitirCopiaChequeBean>();
		for(Cheque cheque: cheques){
			retorno.add(new EmitirCopiaChequeBean(cheque));
		}
		
		return retorno; 
	}
	
	public LinkedList<EmitirCopiaChequeChequeDoc> geraListaCopiaChequeDocumentoBean(List<Cheque> cheques){
		LinkedList<EmitirCopiaChequeChequeDoc> retorno = new LinkedList<EmitirCopiaChequeChequeDoc>();
		for(Cheque cheque: cheques){
			retorno.add(new EmitirCopiaChequeChequeDoc(cheque));
		}
		
		return retorno; 
	}
}