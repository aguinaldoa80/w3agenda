package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialgrupousuario;
import br.com.linkcom.sined.geral.dao.MaterialgrupousuarioDAO;

public class MaterialgrupousuarioService extends GenericService<Materialgrupousuario> {
	
	private MaterialgrupousuarioDAO materialgrupousuarioDAO;
	
	public void setMaterialgrupousuarioDAO(MaterialgrupousuarioDAO materialgrupousuarioDAO) {
		this.materialgrupousuarioDAO = materialgrupousuarioDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param materialgrupo
	 * @return
	 * @author Marden Silva
	 */
	public List<Materialgrupousuario> findByMaterialGrupo(Materialgrupo materialgrupo) {
		return materialgrupousuarioDAO.findByMaterialGrupo(materialgrupo);
	}	
}
