package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Bdicalculotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BdicalculotipoService extends GenericService<Bdicalculotipo> {

	/**
	 * M�todo criado para carregar as op��es no flex.
	 *
	 * @return
	 * @since 31/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Bdicalculotipo> findForComboFlex(){
		return this.findAll();
	}
	
}
