package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Emporiumfiscaistributacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumfiscaistributacaoService extends GenericService<Emporiumfiscaistributacao> {
	
	/* singleton */
	private static EmporiumfiscaistributacaoService instance;
	public static EmporiumfiscaistributacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EmporiumfiscaistributacaoService.class);
		}
		return instance;
	}
	
}
