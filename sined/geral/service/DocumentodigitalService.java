package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.Documentodigitaldestinatario;
import br.com.linkcom.sined.geral.bean.Documentodigitalobservador;
import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitalsituacao;
import br.com.linkcom.sined.geral.dao.DocumentodigitalDAO;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.EmailAceiteDigital;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.AceiteFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentodigitalService extends GenericService<Documentodigital> {
	
	private DocumentodigitalDAO documentodigitalDAO;
	private EnvioemailService envioemailService;
	private ArquivoService arquivoService;
	private DocumentodigitaldestinatarioService documentodigitaldestinatarioService;
	private ReportTemplateService reportTemplateService;
	
	public void setReportTemplateService(
			ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setDocumentodigitaldestinatarioService(
			DocumentodigitaldestinatarioService documentodigitaldestinatarioService) {
		this.documentodigitaldestinatarioService = documentodigitaldestinatarioService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setDocumentodigitalDAO(DocumentodigitalDAO documentodigitalDAO) {
		this.documentodigitalDAO = documentodigitalDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @param cddocumentodigitaldestinatario
	 * @author Rodrigo Freitas
	 * @since 18/01/2019
	 */
	public List<Documentodigital> findForEnvioAceiteDigital(String whereIn, Integer cddocumentodigitaldestinatario) {
		return documentodigitalDAO.findForEnvioAceiteDigital(whereIn, cddocumentodigitaldestinatario);
	}
	
	public void reenviarAceiteDigital(Documentodigitaldestinatario documentodigitaldestinatario) {
		this.enviarAceiteDigital(this.findForEnvioAceiteDigital(documentodigitaldestinatario.getDocumentodigital().getCddocumentodigital() + "", documentodigitaldestinatario.getCddocumentodigitaldestinatario()), true);
	}

	public void enviarAceiteDigital(List<Documentodigital> listaDocumentodigital, boolean reenvio) {
		for (Documentodigital documentodigital : listaDocumentodigital) {
			Empresa empresa = documentodigital.getEmpresa();
			Cliente cliente = documentodigital.getCliente();
			Oportunidade oportunidade = documentodigital.getOportunidade();
			String nomeDestinatario = cliente != null ? cliente.getNome() : oportunidade.getNome();
			
			String remetenteEmail = "";
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			if(org.apache.commons.lang.StringUtils.isNotBlank(usuarioLogado.getEmail())){
				remetenteEmail = usuarioLogado.getEmail();
			} else if(org.apache.commons.lang.StringUtils.isNotBlank(empresa.getEmailfinanceiro())){
				remetenteEmail = empresa.getEmailfinanceiro();
			} else {
				remetenteEmail = empresa.getEmail();
			}
			
			ITemplateEngine engine = null;
			ReportTemplateBean reportTemplate = documentodigital.getDocumentodigitalmodelo().getReporttemplate();
			if(reportTemplate != null){
				reportTemplate = reportTemplateService.load(reportTemplate);
	    		engine = new GroovyTemplateEngine().build(reportTemplate.getLeiaute());
			}
    		
			Arquivo arquivo = documentodigital.getArquivo();
			arquivo = arquivoService.loadWithContents(arquivo);
			byte[] content = arquivo.getContent();
			String contentType =  arquivo.getContenttype().split("/")[1];
			
			int sucesso = 0;
			int erro = 0;
			List<Documentodigitaldestinatario> listaDocumentodigitaldestinatario = documentodigital.getListaDocumentodigitaldestinatario();
			for (Documentodigitaldestinatario documentodigitaldestinatario : listaDocumentodigitaldestinatario) {
				if(documentodigitaldestinatario.getDtenvio() != null && !reenvio) continue;
				
				Documentodigitalusuario documentodigitalusuario = documentodigitaldestinatario.getDocumentodigitalusuario();
				if(documentodigitalusuario == null) continue;
				String enderecoEmail = documentodigitalusuario.getEmail();
				
				try {
					String token = null;
					if(reenvio && StringUtils.isNotBlank(documentodigitaldestinatario.getToken())){
						token = documentodigitaldestinatario.getToken();
					} else {
						token = documentodigitaldestinatarioService.generateAndSaveToken(documentodigitaldestinatario);
					}
					AceiteFileServlet.add(token, content, contentType);
					
					EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
					String assunto = "Documento para aceite digital";
					
					StringBuilder url = new StringBuilder()
							.append(SinedUtil.getUrlWithContext())
							.append("/pub/process/Aceite?token=")
							.append(token);
					
					String html = "";
					if(reportTemplate != null){
						EmailAceiteDigital bean = new EmailAceiteDigital();
						bean.setCliente_nome(nomeDestinatario);
						bean.setEmpresa_nome(empresa.getNome());
						bean.setLink(url.toString());
					
			    		Map<String,Object> datasource = new HashMap<String, Object>();
			    		datasource.put("bean", bean);
			    		html = engine.make(datasource);
					} else {
						TemplateManager template = new TemplateManager("/WEB-INF/template/templateEnvioAceiteDigital.tpl");
						html = template
											.assign("descricaotipo", nomeDestinatario)
											.assign("link", url.toString())
											.assign("empresa", empresa.getNome())
											.getTemplate();
					}
					
					Envioemail envioemail = envioemailService.registrarEnvio(remetenteEmail, assunto, html, Envioemailtipo.ENVIO_ACEITE_DIGITAL, null, enderecoEmail, nomeDestinatario, email);	
					
					email
						.setFrom(remetenteEmail)
						.setSubject(assunto)
						.setTo(enderecoEmail)
						.addHtmlText(html + EmailUtil.getHtmlConfirmacaoEmail(envioemail, enderecoEmail))
						.sendMessage();
					
					documentodigitaldestinatarioService.updateDtenvio(documentodigitaldestinatario, SinedDateUtils.currentTimestamp());
					sucesso++;
				} catch (Exception e) {
					erro++;
					e.printStackTrace();
					NeoWeb.getRequestContext().addError("Erro ao enviar e-mail para " + enderecoEmail + ": " + e.getMessage());
				}
			}
			
			if(!reenvio && erro == 0 && sucesso > 0){
				this.updateSituacao(documentodigital, Documentodigitalsituacao.ENVIADO);
			}
			
			if(sucesso > 0){
				NeoWeb.getRequestContext().addMessage("Foi(ram) enviado(s) " + sucesso + " e-mail(s) com sucesso.");
			} else {
				NeoWeb.getRequestContext().addError("Nenhum e-mail enviado.");
			}
		}
		
	}
	
	public void updateSituacao(Documentodigital documentodigital, Documentodigitalsituacao documentodigitalsituacao) {
		documentodigitalDAO.updateSituacao(documentodigital, documentodigitalsituacao);
	}
	
	public void updateSituacaoAceiteByDestinatarios(Documentodigital documentodigital) {
		List<Documentodigitaldestinatario> listaDocumentodigitaldestinatario = documentodigitaldestinatarioService.findByDocumentodigital(documentodigital);
		boolean haveAceite = false;
		boolean haveNotAceite = false;
		if(listaDocumentodigitaldestinatario != null && listaDocumentodigitaldestinatario.size() > 0){
			for (Documentodigitaldestinatario documentodigitaldestinatario : listaDocumentodigitaldestinatario) {
				haveAceite = haveAceite || (documentodigitaldestinatario.getDtaceite() != null);
				haveNotAceite = haveNotAceite || (documentodigitaldestinatario.getDtaceite() == null);
			}
		}
		
		if(haveAceite && !haveNotAceite){
			this.updateSituacao(documentodigital, Documentodigitalsituacao.ACEITE);
		} else if(haveAceite && haveNotAceite){
			this.updateSituacao(documentodigital, Documentodigitalsituacao.ACEITE_PARCIALMENTE);
		}
	}
	
	public void enviarEmailObservadores(Documentodigital documentodigital, Documentodigitaldestinatario documentodigitaldestinatario) throws IOException {
		documentodigital = this.loadForEnvioEmailObservadores(documentodigital);
		documentodigitaldestinatario = documentodigitaldestinatarioService.loadForEnvioEmailObservadores(documentodigitaldestinatario);
		
		Set<Documentodigitalobservador> listaDocumentodigitalobservador = documentodigital.getListaDocumentodigitalobservador();
		if(listaDocumentodigitalobservador == null || listaDocumentodigitalobservador.size() == 0) return;
		
		Empresa empresa = documentodigital.getEmpresa();
		Documentodigitalusuario documentodigitalusuario = documentodigitaldestinatario.getDocumentodigitalusuario();
		
		String html = new TemplateManager("/WEB-INF/template/templateEnvioAceiteDigitalObservador.tpl")
							.assign("codigo", documentodigital.getCddocumentodigital() + "")
							.assign("empresa", empresa.getNome())
							.assign("modelo", documentodigital.getDocumentodigitalmodelo().getNome())
							.assign("arquivo", documentodigital.getArquivo().getNome())
							.assign("hash", documentodigital.getHash())
							.assign("tipo", documentodigital.getTipo().getNome())
							.assign("clienteoportunidade", documentodigital.getDescricaotipo())
							.assign("situacao", documentodigital.getSituacao().getNome())
							.assign("email", documentodigitalusuario.getEmail())
							.assign("nome", documentodigitalusuario.getNome())
							.assign("cpf", documentodigitalusuario.getCpfFormatado())
							.assign("dtnascimento", SinedDateUtils.toString(documentodigitalusuario.getDtnascimento()))
							.assign("dtenvio", SinedDateUtils.toString(documentodigitaldestinatario.getDtenvio(), "dd/MM/yyyy HH:mm"))
							.assign("token", documentodigitaldestinatario.getToken())
							.assign("dtaceite", SinedDateUtils.toString(documentodigitaldestinatario.getDtaceite(), "dd/MM/yyyy HH:mm"))
							.assign("ip", documentodigitaldestinatario.getIpaceite())
							.assign("useragent", documentodigitaldestinatario.getUseragentaceite())
							.getTemplate();
		
		
		String remetenteEmail = "";
		if(org.apache.commons.lang.StringUtils.isNotBlank(empresa.getEmailfinanceiro())){
			remetenteEmail = empresa.getEmailfinanceiro();
		} else {
			remetenteEmail = empresa.getEmail();
		}
		
		String assunto = "Evento no documento digital #" + documentodigital.getCddocumentodigital();
		for (Documentodigitalobservador documentodigitalobservador : listaDocumentodigitalobservador) {
			try {
				String enderecoEmail = documentodigitalobservador.getEmail();
				
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				Envioemail envioemail = envioemailService.registrarEnvio(remetenteEmail, assunto, html, Envioemailtipo.ENVIO_ACEITE_DIGITAL, null, enderecoEmail, enderecoEmail, email);	
				
				email
					.setFrom(remetenteEmail)
					.setSubject(assunto)
					.setTo(enderecoEmail)
					.addHtmlText(html + EmailUtil.getHtmlConfirmacaoEmail(envioemail, enderecoEmail))
					.sendMessage();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private Documentodigital loadForEnvioEmailObservadores(Documentodigital documentodigital) {
		return documentodigitalDAO.loadForEnvioEmailObservadores(documentodigital);
	}
	
}
