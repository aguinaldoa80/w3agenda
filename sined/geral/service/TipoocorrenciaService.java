package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipoocorrencia;
import br.com.linkcom.sined.geral.dao.TipoocorrenciaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoocorrenciaService extends GenericService<Tipoocorrencia>{

	private TipoocorrenciaDAO tipoocorrenciaDAO;
	
	public void setTipoocorrenciaDAO(TipoocorrenciaDAO tipoocorrenciaDAO) {
		this.tipoocorrenciaDAO = tipoocorrenciaDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TipoocorrenciaDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	public List<Tipoocorrencia> findAllForFlex(){
		return tipoocorrenciaDAO.findAllForFlex();
	}	
	
}
