package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Locacaomaterial;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.LocacaomaterialDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LocacaomaterialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LocacaomaterialService extends GenericService<Locacaomaterial> {

	private LocacaomaterialDAO locacaomaterialDAO;
	private EmpresaService empresaService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setLocacaomaterialDAO(LocacaomaterialDAO locacaomaterialDAO) {
		this.locacaomaterialDAO = locacaomaterialDAO;
	}
	
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.LocacaomaterialDAO#findForReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Locacaomaterial> findForReport(LocacaomaterialFiltro filtro){
		return locacaomaterialDAO.findForReport(filtro);
	}
	
	/**
	 * Gera o relat�rio de controle de loca��o de material
	 *
	 * @see br.com.linkcom.sined.geral.service.LocacaomaterialService#findForReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorio(LocacaomaterialFiltro filtro) {
		Report report = new Report("/suprimento/locacaomaterial");
		
		List<Locacaomaterial> lista = this.findForReport(filtro);
		report.setDataSource(lista);
		
		return report;
	}

	public void criarAvisoDataLocacaoMaterialVencida(Motivoaviso m, Date data, Date dateToSearch) {
		List<Locacaomaterial> locacaoMaterialList = locacaomaterialDAO.findByVencida(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Locacaomaterial l : locacaoMaterialList) {
			Aviso aviso = new Aviso("Data da loca��o do material vencida", "C�digo da loca��o de material: " + l.getCdlocacaomaterial(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), l.getCdlocacaomaterial(), empresa, SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, l.getCdlocacaomaterial());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, l.getCdlocacaomaterial(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

	public Resource generateCsvReport(LocacaomaterialFiltro filtro, String nomeArquivo) {
		List<Locacaomaterial> lista = locacaomaterialDAO.findForCsvReport(filtro);
		
		String [] fields = {"fornecedor", "bempatrimonio", "valorlocacao", "locacaomaterialtipo", "dtinicio", "dtfim"};
		String cabecalho = "\"Fornecedor\";\"Material (Patrim�nio)\";\"Valor de loca��o\";\"Tipo de loca��o\";\"Data de in�cio\";\"Data de t�rmino\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
			
		for(Locacaomaterial bean : lista) {
			beanToCSV(fields, csv, bean);
		}
			
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
}
