package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.TarefarecursohumanoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefarecursohumanoService extends GenericService<Tarefarecursohumano>{

	private TarefarecursohumanoDAO tarefarecursohumanoDAO;
	
	public void setTarefarecursohumanoDAO(
			TarefarecursohumanoDAO tarefarecursohumanoDAO) {
		this.tarefarecursohumanoDAO = tarefarecursohumanoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefarecursohumanoDAO#findByPlanejamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursohumano> findByPlanejamento(Planejamento planejamento) {
		return tarefarecursohumanoDAO.findByPlanejamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de refer�ncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#findByPlanejamento
	 * @param planejamento
	 * @param listaReferencia
	 * @param listaTodas
	 * @author Rodrigo Freitas
	 */
	public void valoresTarefaHumano(Planejamento planejamento, List<Valorreferencia> listaReferencia, List<Valorreferencia> listaTodas) {
		Valorreferencia valorreferencia;
		List<Tarefarecursohumano> listaTarefaHumano = this.findByPlanejamento(planejamento);
		for (Tarefarecursohumano tarefarecursohumano : listaTarefaHumano) {
			valorreferencia = new Valorreferencia();
			valorreferencia.setCargo(tarefarecursohumano.getCargo());
			valorreferencia.setPlanejamento(planejamento);
			valorreferencia.setValor(tarefarecursohumano.getCargo().getCustohora());
			
			if (!listaTodas.contains(valorreferencia) && !listaReferencia.contains(valorreferencia)) {
				listaReferencia.add(valorreferencia);
			}
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefarecursohumanoDAO#findForOrcamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursohumano> findForOrcamento(Planejamento planejamento) {
		return tarefarecursohumanoDAO.findForOrcamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de refer�ncia a partir de uma lista de tarefaRecursoHumano.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#findForOrcamento
	 * @see br.com.linkcom.sined.geral.bean.Valorreferencia#getValor
	 * @param filtro
	 * @param listaFolhas
	 * @param listaOutrosCargo
	 * @param somacargo
	 * @param listaValorReferencia
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money preencheListaVRTarefaCar(OrcamentoAnaliticoReportFiltro filtro, List<AnaliseRecDespBean> listaFolhas,
			List<AnaliseRecDespBean> listaOutrosCargo, Money somacargo, List<Valorreferencia> listaValorReferencia) {
		AnaliseRecDespBean bean;
		Money valor;
		List<Tarefarecursohumano> listaTarefaHumano = this.findForOrcamento(filtro.getPlanejamento());
		for (Tarefarecursohumano tarefarecursohumano : listaTarefaHumano) {
			bean = new AnaliseRecDespBean();
			if (tarefarecursohumano.getCargo().getContagerencial() != null) {
				bean.setIdentificador(tarefarecursohumano.getCargo().getContagerencial().getVcontagerencial().getIdentificador());
				bean.setNome(tarefarecursohumano.getCargo().getContagerencial().getNome());
				bean.setOperacao(tarefarecursohumano.getCargo().getContagerencial().getTipooperacao().getSigla());
				valor = Valorreferencia.getValor(tarefarecursohumano.getCargo(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(tarefarecursohumano.getQtde())) : new Money());
				
				listaFolhas.add(bean);
			} else {
				bean.setIdentificador("XX.XX.XX");
				bean.setNome(tarefarecursohumano.getCargo().getNome());
				bean.setOperacao("D");
				valor = Valorreferencia.getValor(tarefarecursohumano.getCargo(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(tarefarecursohumano.getQtde())) :  new Money());
				somacargo = somacargo.add(valor != null ? valor.multiply(new Money(tarefarecursohumano.getQtde())) :  new Money()); 
				
				listaOutrosCargo.add(bean);
			}
		}
		return somacargo;
	}
	
	public List<Tarefarecursohumano> preencheListaPlanejamentoRecHumano(Collection<Planejamentorecursohumano> lista){
		List<Tarefarecursohumano> listaRecHumano = new ArrayList<Tarefarecursohumano>();
		Tarefarecursohumano pg = null;
		for (Planejamentorecursohumano t : lista) {
			pg = new Tarefarecursohumano();
			pg.setCdtarefarecursohumano(t.getCdplanejamentorecursohumano());
			pg.setCargo(t.getCargo());
			pg.setQtde(t.getQtde());
			
			listaRecHumano.add(pg);
		}
		
		return listaRecHumano;
	}
	
	/**
	* M�todo com refer�ncia ao DAO 
	* Carrega lista de tarefarecursohumano com tarefa e cargo
	*
	* @see	br.com.linkcom.sined.geral.dao.TarefarecursohumanoDAO#carregaCargoTarefa(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Aug 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefarecursohumano> carregaCargoTarefa(String whereIn){
		return tarefarecursohumanoDAO.carregaCargoTarefa(whereIn);
	}

}
