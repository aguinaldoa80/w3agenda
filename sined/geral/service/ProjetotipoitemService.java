package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Projetotipoitem;
import br.com.linkcom.sined.geral.dao.ProjetotipoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProjetotipoitemService extends GenericService<Projetotipoitem>{
	
	private ProjetotipoitemDAO projetotipoitemDAO;
	
	public void setProjetotipoitemDAO(ProjetotipoitemDAO projetotipoitemDAO) {
		this.projetotipoitemDAO = projetotipoitemDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetotipoitemDAO#findByProjetotipo(Projetotipo projetotipo)
	 * 
	 * @param projetotipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/06/2013
	 */
	public List<Projetotipoitem> findByProjetotipo(Projetotipo projetotipo) {
		return projetotipoitemDAO.findByProjetotipo(projetotipo);
	}
	

}
