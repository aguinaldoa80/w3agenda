package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemitemadicional;
import br.com.linkcom.sined.geral.dao.ProducaoordemitemadicionalDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ProducaoOrdemItemAdicionalWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemitemadicionalService extends GenericService<Producaoordemitemadicional> {

	private ProducaoordemitemadicionalDAO producaoordemitemadicionalDAO;
	
	public void setProducaoordemitemadicionalDAO(ProducaoordemitemadicionalDAO producaoordemitemadicionalDAO) {
		this.producaoordemitemadicionalDAO = producaoordemitemadicionalDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemitemadicionalDAO#findForDelete(Producaoordem producaoordem, String whereIn)
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemitemadicional> findForDelete(Producaoordem producaoordem, String whereIn) {
		return producaoordemitemadicionalDAO.findForDelete(producaoordem, whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2017
	 */
	public List<Producaoordemitemadicional> findByProducaoordem(Producaoordem producaoordem) {
		return producaoordemitemadicionalDAO.findByProducaoordem(producaoordem);
	}
	
	public List<ProducaoOrdemItemAdicionalWSBean> toWSList(List<Producaoordemitemadicional> lista){
		List<ProducaoOrdemItemAdicionalWSBean> retorno = new ArrayList<ProducaoOrdemItemAdicionalWSBean>();
		if(Hibernate.isInitialized(lista) && SinedUtil.isListNotEmpty(lista)){
			for(Producaoordemitemadicional item: lista){
				ProducaoOrdemItemAdicionalWSBean bean = new ProducaoOrdemItemAdicionalWSBean();
				bean.setCdproducaoordemitemadicional(item.getCdproducaoordemitemadicional());
				bean.setQuantidade(item.getQuantidade());
				bean.setQuantidadeDisponivel(item.getQuantidadeDisponivel());
				bean.setValortotal(item.getValortotal());
				bean.setValorvenda(item.getValorvenda());
				bean.setLocalarmazenagem(ObjectUtils.translateEntityToGenericBean(item.getLocalarmazenagem()));
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(item.getMaterial()));
				bean.setPneu(ObjectUtils.translateEntityToGenericBean(item.getPneu()));
				bean.setProducaoordem(ObjectUtils.translateEntityToGenericBean(item.getProducaoordem()));

				retorno.add(bean);
			}
		}
		return retorno;
	}
	
	public List<Producaoordemitemadicional> findForProducaoordemEntrada(Producaoordem producaoordem) {
		return producaoordemitemadicionalDAO.findForProducaoordemEntrada(producaoordem);
	}
}
