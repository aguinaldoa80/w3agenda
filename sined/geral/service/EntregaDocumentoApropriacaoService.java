package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.EntregaDocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.dao.EntregaDocumentoApropriacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregaDocumentoApropriacaoService extends GenericService<EntregaDocumentoApropriacao>{

	private EntregaDocumentoApropriacaoDAO entregaDocumentoApropriacaoDAO;
	
	public void setEntregaDocumentoApropriacaoDAO(
			EntregaDocumentoApropriacaoDAO entregaDocumentoApropriacaoDAO) {
		this.entregaDocumentoApropriacaoDAO = entregaDocumentoApropriacaoDAO;
	}
	
	public List<EntregaDocumentoApropriacao> findByEntregaDocumento(Entregadocumento entregaDocumento){
		return entregaDocumentoApropriacaoDAO.findByEntregaDocumento(entregaDocumento);
	}
}
