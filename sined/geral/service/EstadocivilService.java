package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EstadocivilService extends GenericService<Estadocivil> {	
	
	/* singleton */
	private static EstadocivilService instance;
	public static EstadocivilService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EstadocivilService.class);
		}
		return instance;
	}
	
}
