package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefacolaborador;
import br.com.linkcom.sined.geral.dao.TarefacolaboradorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefacolaboradorService extends GenericService<Tarefacolaborador>{

	private TarefacolaboradorDAO tarefacolaboradorDAO;
	
	
	public void setTarefacolaboradorDAO(TarefacolaboradorDAO tarefacolaboradorDAO) {
		this.tarefacolaboradorDAO = tarefacolaboradorDAO;
	}


	/**
	* M�todo com refer�ncia ao DAO
	* retorna os colaboradores cadastrados na tarefa
	*
	* @see	br.com.linkcom.sined.geral.dao.TarefacolaboradorDAO#findColaboradorNaTarefa(String whereIn, String whereInColaborador)
	*
	* @param whereIn
	* @param whereInColaborador
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefacolaborador> findColaboradorNaTarefa(String whereIn, String whereInColaborador){
		return tarefacolaboradorDAO.findColaboradorNaTarefa(whereIn, whereInColaborador);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TarefacolaboradorDAO#existeColaboradorExclusivoAlocadoPeriodo(Tarefa tarefa, Date dtinicio, Colaborador colaborador, boolean exclusivo)
	*
	* @param tarefa
	* @param dtinicio
	* @param colaborador
	* @param exclusivo
	* @return
	* @since 12/03/2015
	* @author Luiz Fernando
	*/
	public boolean existeColaboradorExclusivoAlocadoPeriodo(Tarefa tarefa, Date dtinicio, Colaborador colaborador, boolean exclusivo) {
		return tarefacolaboradorDAO.existeColaboradorExclusivoAlocadoPeriodo(tarefa, dtinicio, colaborador, exclusivo);
	}
}
