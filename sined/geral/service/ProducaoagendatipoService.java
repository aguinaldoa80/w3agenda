package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.geral.dao.ProducaoagendatipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendatipoService extends GenericService<Producaoagendatipo>{

	private ProducaoagendatipoDAO producaoagendatipoDAO;
	
	public void setProducaoagendatipoDAO(ProducaoagendatipoDAO producaoagendatipoDAO) {
		this.producaoagendatipoDAO = producaoagendatipoDAO;
	}

	public Producaoagendatipo loadWithProducaoetapa(Producaoagendatipo producaoagendatipo){
		return producaoagendatipoDAO.loadWithProducaoetapa(producaoagendatipo);
	}
}
