package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.dao.ProducaoagendamaterialmateriaprimaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendamaterialmateriaprimaService extends GenericService<Producaoagendamaterialmateriaprima> {
	
	private ProducaoagendamaterialmateriaprimaDAO producaoagendamaterialmateriaprimaDAO;

	public void setProducaoagendamaterialmateriaprimaDAO(ProducaoagendamaterialmateriaprimaDAO producaoagendamaterialmateriaprimaDAO) {
		this.producaoagendamaterialmateriaprimaDAO = producaoagendamaterialmateriaprimaDAO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendamaterialmateriaprimaDAO#findByProducaoagendamaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterialmateriaprima> findByProducaoagendamaterial(String whereIn){
		return producaoagendamaterialmateriaprimaDAO.findByProducaoagendamaterial(whereIn);
	}
	
}
