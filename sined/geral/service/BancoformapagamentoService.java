package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.dao.BancoformapagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoformapagamentoService extends GenericService<BancoFormapagamento> {
	
	protected BancoformapagamentoDAO bancoformapagamentoDAO;
	
	public void setBancoformapagamentoDAO(BancoformapagamentoDAO bancoformapagamentoDAO) {this.bancoformapagamentoDAO = bancoformapagamentoDAO;}
	
	public List<BancoFormapagamento> FindByBanco(Banco banco){
		return bancoformapagamentoDAO.findByBanco(banco);
	}
	
	public BancoFormapagamento findByFormaPagamento(BancoFormapagamento bean){
		return bancoformapagamentoDAO.findByFormaPagamento(bean);
	}
	
}
