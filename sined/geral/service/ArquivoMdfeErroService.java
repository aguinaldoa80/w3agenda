package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.ArquivoMdfeErro;
import br.com.linkcom.sined.geral.dao.ArquivoMdfeErroDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivoMdfeErroService extends GenericService<ArquivoMdfeErro> {
	
	private ArquivoMdfeErroDAO arquivoMdfeErroDAO;

	public void setArquivoMdfeErroDAO(ArquivoMdfeErroDAO arquivoMdfeErroDAO) {
		this.arquivoMdfeErroDAO = arquivoMdfeErroDAO;
	}

	public List<ArquivoMdfeErro> findByArquivoMdfe(ArquivoMdfe arquivoMdfe ) {
		return arquivoMdfeErroDAO.findByArquivoMdfe(arquivoMdfe);
	}

}
