package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Fornecimentocontratoitem;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcomprafornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Fornecimentocontratoitemtipo;
import br.com.linkcom.sined.geral.dao.FornecimentocontratoDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FornecimentocontratoService extends GenericService<Fornecimentocontrato> {

	private FornecimentocontratoDAO fornecimentocontratoDAO;
	private FluxocaixaTipoService fluxocaixaTipoService;
	private FluxocaixaService fluxocaixaService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private OrdemcompraService ordemcompraService;
	private OrdemcomprafornecimentocontratoService ordemcomprafornecimentocontratoService;
		
	public void setFluxocaixaService(FluxocaixaService fluxocaixaService) {
		this.fluxocaixaService = fluxocaixaService;
	}
	public void setFluxocaixaTipoService(
			FluxocaixaTipoService fluxocaixaTipoService) {
		this.fluxocaixaTipoService = fluxocaixaTipoService;
	}
	public void setFornecimentocontratoDAO(FornecimentocontratoDAO fornecimentocontratoDAO) {
		this.fornecimentocontratoDAO = fornecimentocontratoDAO;
	}
	public void setPrazopagamentoitemService(
			PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setOrdemcomprafornecimentocontratoService(OrdemcomprafornecimentocontratoService ordemcomprafornecimentocontratoService) {
		this.ordemcomprafornecimentocontratoService = ordemcomprafornecimentocontratoService;
	}
	
	@Override
	public void saveOrUpdate(Fornecimentocontrato f) {
		Fluxocaixa fluxocaixa = new Fluxocaixa();
		if(f.getCdfornecimentocontrato() != null && f.getFluxocaixa() != null && f.getFluxocaixa().getCdfluxocaixa() != null){
			fluxocaixa.setCdfluxocaixa(f.getFluxocaixa().getCdfluxocaixa());
		}
		fluxocaixa.setDescricao(f.getDescricao());
		fluxocaixa.setValor(f.getValor());
		
		FluxocaixaTipo fluxocaixaTipo = fluxocaixaTipoService.getFluxoCaixaTipoFornecimentoContrato();
		if(fluxocaixaTipo == null){
			fluxocaixaTipo = new FluxocaixaTipo("Contrato de fornecimento", "CF");
			fluxocaixaTipoService.saveOrUpdateNoUseTransaction(fluxocaixaTipo);
		}
		fluxocaixa.setFluxocaixaTipo(fluxocaixaTipo);
		
		fluxocaixa.setPrazopagamento(f.getPrazopagamento());
		fluxocaixa.setDtinicio(f.getDtinicio());
		fluxocaixa.setDtfim(f.getDtfim());
		fluxocaixa.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		
		Rateio rateio = new Rateio();
		if(f.getFluxocaixa() != null && f.getFluxocaixa().getRateio() != null && f.getFluxocaixa().getRateio().getCdrateio() != null){
			rateio.setCdrateio(f.getFluxocaixa().getRateio().getCdrateio());
		}
		Rateioitem rateioitem = new Rateioitem();
		rateioitem.setCentrocusto(f.getCentrocusto());
		rateioitem.setProjeto(f.getProjeto());
		rateioitem.setContagerencial(f.getContagerencial());
		rateioitem.setPercentual(100.00);
		rateioitem.setValor(f.getValor());
		
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		listaRateioitem.add(rateioitem);
		
		rateio.setListaRateioitem(listaRateioitem);
		
		fluxocaixa.setRateio(rateio);
		
		fluxocaixa.setAtivo(f.getAtivo());
		
		fluxocaixaService.saveOrUpdateNoUseTransaction(fluxocaixa);
		f.setFluxocaixa(fluxocaixa);
		
		super.saveOrUpdate(f);
	}
	
	/**
	 * 
	 * 
	 * @param filtro
	 * @return
	 */
	public boolean exibirMsgContratoDuplicado(Fornecimentocontrato fornecimentocontrato) {
		return fornecimentocontratoDAO.exibirMsgContratoDuplicado(fornecimentocontrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param fornecimentocontrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Fornecimentocontrato carregaFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		return fornecimentocontratoDAO.carregaFornecimentocontrato(fornecimentocontrato);
	}
	

	/**
	 * M�todo respons�vel por calcular a data do pr�ximo vencimento.
	 * @param bean
	 * @param numParcela
	 * @author Taidson
	 * @since 28/06/2010
	 */
	public void atualizaProximoVencimento(Fornecimentocontrato bean, Integer numParcela){
	
		Integer diasProximoVencimento = null;
		
		for (Prazopagamentoitem item : bean.getPrazopagamento().getListaPagamentoItem()) {
			Integer dias;
			Integer meses;
			if(item.getDias() != null){
				dias = item.getDias();
			}else{
				dias = 0;
			}
			if(item.getMeses() != null && item.getMeses() > 0){
				meses = item.getMeses()*30;
			}else{
				meses = 0;
			}
			diasProximoVencimento = dias + meses;
		}
		bean = loadForEntrada(bean);
		
		Integer qtdeParcelasPrazoPagamento = prazopagamentoitemService.qtdeParcelasPrazoPagamento(bean.getPrazopagamento());
		
		if(bean.getPrazopagamento() != null && bean.getPrazopagamento().getCdprazopagamento() != null){
			Date proximoVencimento = SinedDateUtils.addDiasData(bean.getDtinicio(), diasProximoVencimento);
			bean.setDtproximovencimento(proximoVencimento);
			if(numParcela >= qtdeParcelasPrazoPagamento){
				bean.setDtfim(proximoVencimento);
			}
			fornecimentocontratoDAO.updateDtproximovencimento(bean);
		}
	}
	public List<Fornecimentocontrato> findForCSV() {
		return fornecimentocontratoDAO.findForCSV();
	}
	
	/**
	 * M�todo que gera o fornecimento a partir das ordens de compra
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecimentocontrato gerarFornecimento(String whereIn) {
		Fornecimentocontrato fornecimentocontrato = new Fornecimentocontrato();
		Set<Fornecimentocontratoitem> listaFornecimentocontratoitem = new ListSet<Fornecimentocontratoitem>(Fornecimentocontratoitem.class);
		Fornecimentocontratoitem fornecimentocontratoitem;
		
		List<Ordemcompra> listaOrdemcompra = ordemcompraService.findForGerarfornecimento(whereIn);
		
		fornecimentocontrato.setColaborador(SinedUtil.getUsuarioComoColaborador());
		fornecimentocontrato.setWhereInOrdemcompra(whereIn);
		fornecimentocontrato.setDescricao("Referente a Ordem de Compra (" + whereIn + ")");
		fornecimentocontrato.setDtinicio(new Date(System.currentTimeMillis()));
		
		for(Ordemcompra ordemcompra : listaOrdemcompra){
			if(ordemcompra.getFornecedor() != null && fornecimentocontrato.getFornecedor() == null){
				fornecimentocontrato.setFornecedor(ordemcompra.getFornecedor());
			}
			if(ordemcompra.getPrazopagamento() != null && fornecimentocontrato.getPrazopagamento() == null){
				fornecimentocontrato.setPrazopagamento(ordemcompra.getPrazopagamento());
			}
			
			if(fornecimentocontrato.getContagerencial() == null || fornecimentocontrato.getCentrocusto() == null ||
				fornecimentocontrato.getProjeto() == null){
				if(ordemcompra.getRateio() != null && ordemcompra.getRateio().getListaRateioitem() != null && 
						!ordemcompra.getRateio().getListaRateioitem().isEmpty()){
					for(Rateioitem rateioitem : ordemcompra.getRateio().getListaRateioitem()){
						if(rateioitem.getContagerencial() != null && fornecimentocontrato.getContagerencial() == null){
							fornecimentocontrato.setContagerencial(rateioitem.getContagerencial());
						}
						if(rateioitem.getCentrocusto() != null && fornecimentocontrato.getCentrocusto() == null){
							fornecimentocontrato.setCentrocusto(rateioitem.getCentrocusto());
						}
						if(rateioitem.getProjeto() != null && fornecimentocontrato.getProjeto() == null){
							fornecimentocontrato.setProjeto(rateioitem.getProjeto());
						}
					}
				}
			}
				
			if(ordemcompra.getListaMaterial() != null && !ordemcompra.getListaMaterial().isEmpty()){
				for(Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()){
					fornecimentocontratoitem = new Fornecimentocontratoitem();
					fornecimentocontratoitem.setFornecimentocontratoitemtipo(Fornecimentocontratoitemtipo.MATERIAL);
					fornecimentocontratoitem.setMaterial(ordemcompramaterial.getMaterial());
					fornecimentocontratoitem.setValor(ordemcompramaterial.getValor());
					fornecimentocontratoitem.setQtde(ordemcompramaterial.getQtdpedida());
					if(fornecimentocontratoitem.getValor() != null && fornecimentocontratoitem.getQtde() != null && 
							fornecimentocontratoitem.getQtde() > 0){
						fornecimentocontratoitem.setValorequipamento(fornecimentocontratoitem.getValor()/fornecimentocontratoitem.getQtde());
					}
					listaFornecimentocontratoitem.add(fornecimentocontratoitem);
				}
			}
		}
		
		fornecimentocontrato.setListaFornecimentocontratoitem(listaFornecimentocontratoitem);
		
		return fornecimentocontrato;
	}
	
	public List<OrigemsuprimentosBean> montaOrigem(Fornecimentocontrato form) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		OrigemsuprimentosBean origemsuprimentosBean = null;
		
		List<Ordemcomprafornecimentocontrato> listaOrdemcopmra = ordemcomprafornecimentocontratoService.findByFornecimentocontrato(form);
		
		if(listaOrdemcopmra != null && !listaOrdemcopmra.isEmpty())
			for (Ordemcomprafornecimentocontrato ordemcomprafornecimentocontrato : listaOrdemcopmra){
				if(ordemcomprafornecimentocontrato.getOrdemcompra() != null){
					origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ORDEMCOMPRA, "ORDEM DE COMPRA", ordemcomprafornecimentocontrato.getOrdemcompra().getCdordemcompra().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
			}
		
		return listaBean;
	}
	public List<Fornecimentocontrato> findForCombo(List<Fornecimentocontrato> listaFornecimentocontrato, Boolean ativo) {
		return fornecimentocontratoDAO.findForCombo(listaFornecimentocontrato, ativo);
	}
	
	public List<Fornecimentocontrato> findByEmpresa (Empresa empresa){
		return fornecimentocontratoDAO.findByEmpresa(empresa);
		
	}
}
