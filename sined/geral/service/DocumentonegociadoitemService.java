package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentonegociadoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentonegociadoitemService extends GenericService<Documentonegociadoitem> {	
	
	/* singleton */
	private static DocumentonegociadoitemService instance;
	public static DocumentonegociadoitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentonegociadoitemService.class);
		}
		return instance;
	}
	
}
