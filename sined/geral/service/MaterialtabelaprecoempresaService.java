package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.dao.MaterialtabelaprecoempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoempresa.MaterialtabelaprecoempresaRESTModel;

public class MaterialtabelaprecoempresaService extends GenericService<Materialtabelaprecoempresa> {

	private MaterialtabelaprecoempresaDAO materialtabelaprecoempresaDAO;
	
	public void setMaterialtabelaprecoempresaDAO(MaterialtabelaprecoempresaDAO materialtabelaprecoempresaDAO) {
		this.materialtabelaprecoempresaDAO = materialtabelaprecoempresaDAO;
	}

	/* singleton */
	private static MaterialtabelaprecoempresaService instance;
	public static MaterialtabelaprecoempresaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialtabelaprecoempresaService.class);
		}
		return instance;
	}
	
	public List<MaterialtabelaprecoempresaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialtabelaprecoempresaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialtabelaprecoempresaRESTModel> lista = new ArrayList<MaterialtabelaprecoempresaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialtabelaprecoempresa bean : materialtabelaprecoempresaDAO.findForAndroid(whereIn))
				lista.add(new MaterialtabelaprecoempresaRESTModel(bean));
		}
		
		return lista;
	}
}
