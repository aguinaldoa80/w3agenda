package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicoparcela;
import br.com.linkcom.sined.geral.dao.ContratojuridicoparcelaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratojuridicoparcelaService extends GenericService<Contratojuridicoparcela> {

	private ContratojuridicoparcelaDAO contratojuridicoparcelaDAO;
	
	public void setContratojuridicoparcelaDAO(
			ContratojuridicoparcelaDAO contratojuridicoparcelaDAO) {
		this.contratojuridicoparcelaDAO = contratojuridicoparcelaDAO;
	}
	
	public void updateParcelaCobrada(Contratojuridico contratojuridico) {
		contratojuridicoparcelaDAO.updateParcelaCobrada(contratojuridico);
	}
	
	/**
	 * 
	 * @param contratojuridico
	 * @author Thiago Clemente
	 * 
	 */
	public List<Contratojuridicoparcela> findByContratojuridico(Contratojuridico contratojuridico){
		return contratojuridicoparcelaDAO.findByContratojuridico(contratojuridico);
	}	
}