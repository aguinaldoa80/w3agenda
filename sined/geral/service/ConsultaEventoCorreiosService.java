package br.com.linkcom.sined.geral.service;

import java.util.Date;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.ConsultaEventoCorreios;
import br.com.linkcom.sined.geral.dao.ConsultaEventoCorreiosDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConsultaEventoCorreiosService extends GenericService<ConsultaEventoCorreios>{

	private ConsultaEventoCorreiosDAO consultaEventoCorreiosDAO;
	
	public static ConsultaEventoCorreiosService instance;
	
	public static ConsultaEventoCorreiosService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ConsultaEventoCorreiosService.class);
		}
		return instance;
	}
	
	public void setConsultaEventoCorreiosDAO(ConsultaEventoCorreiosDAO consultaEventoCorreiosDAO) {
		this.consultaEventoCorreiosDAO = consultaEventoCorreiosDAO;
	}
	
	public ConsultaEventoCorreios loadByData(String objeto, Date data){
		return consultaEventoCorreiosDAO.loadByData(objeto, data);
	}
	
	public void saveConsulta(String objeto){
		ConsultaEventoCorreios consulta = this.loadByData(objeto, SinedDateUtils.currentDate());
		if(consulta == null){
			consulta = new ConsultaEventoCorreios();
			consulta.setDtConsulta(SinedDateUtils.currentDate());
			consulta.setObjeto(objeto);
			consulta.setQuantidadeConsultas(1);
		}else{
			if(consulta.getQuantidadeConsultas() == null){
				consulta.setQuantidadeConsultas(0);
			}
			consulta.setQuantidadeConsultas(consulta.getQuantidadeConsultas() + 1);
		}
		this.saveOrUpdate(consulta);
	}
	
	public void updateConsultaManual(String objeto, java.sql.Date data){
		consultaEventoCorreiosDAO.updateConsultaManual(objeto, data);
	}
}