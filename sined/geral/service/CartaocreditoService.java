package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.dao.CartaocreditoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CartaocreditoService extends GenericService<Conta> {

	private CartaocreditoDAO cartaocreditoDAO;

	public void setCartaocreditoDAO(CartaocreditoDAO cartaocreditoDAO) {
		this.cartaocreditoDAO = cartaocreditoDAO;
	}
		
	/**
	 * M�todo para verificar se a propriedade n�mero do cart�o de cr�dito
	 * j� est� cadastrada no banco.
	 * 
	 * @see #countCartaocreditoByNumero(Conta)
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean existeNumeroCartaocredito(Conta bean){
		Long count = countCartaocreditoByNumero(bean);
		if(count != 0)
			return true;
		return false;
	}
	
	
	/**
	 * M�todo para verificar se a propriedade nome do cart�o de cr�dito j� est�
	 * cadastrada no banco.
	 * 
	 * @see #countCartaocreditoByNome(Conta)
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean existeNomeCartaocredito(Conta bean) {
		Long count = countCartaocreditoByNome(bean);
		if(count != 0)
			return true;
		return false;
	}
	
	/**
	 * M�todo para obter o n�mero de cart�es de cr�dito com determinado n�mero.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CartaocreditoDAO#findCartaocreditoByNumero(Conta)
	 * @param bean
	 * @return
	 */
	public Long countCartaocreditoByNumero(Conta bean){
		return cartaocreditoDAO.findCartaocreditoByNumero(bean);
	}
	
	/**
	 * M�todo para obter o n�mero de cart�es de cr�dito com determinado nome.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CartaocreditoDAO#findCartaocreditoByNome(Conta)
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Long countCartaocreditoByNome(Conta bean){
		return cartaocreditoDAO.findCartaocreditoByNome(bean);
	}
}
