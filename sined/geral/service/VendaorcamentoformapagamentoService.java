package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.geral.dao.VendaorcamentoformapagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaorcamentoformapagamentoService extends GenericService<Vendaorcamentoformapagamento> {

	private VendaorcamentoformapagamentoDAO vendaorcamentoformapagamentoDAO;
	
	public void setVendaorcamentoformapagamentoDAO(
			VendaorcamentoformapagamentoDAO vendaorcamentoformapagamentoDAO) {
		this.vendaorcamentoformapagamentoDAO = vendaorcamentoformapagamentoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentoformapagamentoDAO#findByVendaorcamento(Vendaorcamento vendaorcamento)
	 *
	 * @param vendaorcamento
	 * @return
	 * @since 27/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentoformapagamento> findByVendaorcamento(Vendaorcamento vendaorcamento) {
		return vendaorcamentoformapagamentoDAO.findByVendaorcamento(vendaorcamento);
	}

}
