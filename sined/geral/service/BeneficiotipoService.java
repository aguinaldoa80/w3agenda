package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Beneficiotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BeneficiotipoService extends GenericService<Beneficiotipo>{

	private static BeneficiotipoService instance;
	
	public static BeneficiotipoService getInstance() {
		if (instance==null){
			instance = Neo.getObject(BeneficiotipoService.class);
		}
		return instance;
	}
}