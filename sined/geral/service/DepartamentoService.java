package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.dao.DepartamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DepartamentoService extends GenericService<Departamento> {
	
	private DepartamentoDAO departamentoDAO;
	
	public void setDepartamentoDAO(DepartamentoDAO departamentoDAO) {
		this.departamentoDAO = departamentoDAO;
	}
	
	/* singleton */
	private static DepartamentoService instance;
		public static DepartamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DepartamentoService.class);
		}
		return instance;
	}	
	
	public List<Departamento> findAllDepartamentos(Cargo cargo){
		return departamentoDAO.findAllDepartamentos(cargo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DepartamentoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Departamento> findAllForFlex(){
		return departamentoDAO.findAllForFlex();
	}

	/**
	 * Busca o departamento pelo seu c�digo folha. 
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DepartamentoDAO#findByCodigofolha(String codigofolha)
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Departamento findByCodigofolha(String codigofolha) {
		return departamentoDAO.findByCodigofolha(codigofolha);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DepartamentoDAO#findDepartamentoAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Departamento> findDepartamentoAutocomplete(String q) {
		return departamentoDAO.findDepartamentoAutocomplete(q);
	}

	public Departamento retornarResponsavel(Integer dep) {
		return departamentoDAO.retornarResponsavel(dep);
		
	}

	public Departamento findByNome(String nome_departamento) {
		return departamentoDAO.findByNome(nome_departamento);
	}
	
}
