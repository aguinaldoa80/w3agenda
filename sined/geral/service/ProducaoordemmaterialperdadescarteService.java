package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialperdadescarte;
import br.com.linkcom.sined.geral.dao.ProducaoordemmaterialperdadescarteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemmaterialperdadescarteService extends GenericService<Producaoordemmaterialperdadescarte>{

	protected ProducaoordemmaterialperdadescarteDAO producaoordemmaterialperdadescarteDAO;
	
	public void setProducaoordemmaterialperdadescarteDAO(
			ProducaoordemmaterialperdadescarteDAO producaoordemmaterialperdadescarteDAO) {
		this.producaoordemmaterialperdadescarteDAO = producaoordemmaterialperdadescarteDAO;
	}
	
	public List<Producaoordemmaterialperdadescarte> findByProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial){
		return producaoordemmaterialperdadescarteDAO.findByProducaoordemmaterial(producaoordemmaterial);
	}
	
	public void delete(Producaoordemmaterial producaoordemmaterial) {
		producaoordemmaterialperdadescarteDAO.delete(producaoordemmaterial);
	}
	
	public void delete(Producaoordem producaoordem) {
		producaoordemmaterialperdadescarteDAO.delete(producaoordem);
	}
}
