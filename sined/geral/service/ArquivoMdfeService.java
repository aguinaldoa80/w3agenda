package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.mdfe.Aereo;
import br.com.linkcom.mdfe.Aquav;
import br.com.linkcom.mdfe.AutXml;
import br.com.linkcom.mdfe.Condutor;
import br.com.linkcom.mdfe.ConsReciMdfe;
import br.com.linkcom.mdfe.DetEvento;
import br.com.linkcom.mdfe.Disp;
import br.com.linkcom.mdfe.EnviMdfe;
import br.com.linkcom.mdfe.EventoMdfe;
import br.com.linkcom.mdfe.Ferrov;
import br.com.linkcom.mdfe.InfAntt;
import br.com.linkcom.mdfe.InfCiot;
import br.com.linkcom.mdfe.InfContratante;
import br.com.linkcom.mdfe.InfCte;
import br.com.linkcom.mdfe.InfDoc;
import br.com.linkcom.mdfe.InfEmbComb;
import br.com.linkcom.mdfe.InfEvento;
import br.com.linkcom.mdfe.InfMdfe;
import br.com.linkcom.mdfe.InfMdfeSupl;
import br.com.linkcom.mdfe.InfMdfeTransp;
import br.com.linkcom.mdfe.InfModal;
import br.com.linkcom.mdfe.InfMunCarrega;
import br.com.linkcom.mdfe.InfMunDescarga;
import br.com.linkcom.mdfe.InfNfe;
import br.com.linkcom.mdfe.InfPercurso;
import br.com.linkcom.mdfe.InfResp;
import br.com.linkcom.mdfe.InfSeg;
import br.com.linkcom.mdfe.InfTermCarreg;
import br.com.linkcom.mdfe.InfTermDescarreg;
import br.com.linkcom.mdfe.InfUnidCarga;
import br.com.linkcom.mdfe.InfUnidCargaVazia;
import br.com.linkcom.mdfe.InfUnidTransp;
import br.com.linkcom.mdfe.InfUnidTranspVazia;
import br.com.linkcom.mdfe.LacRodo;
import br.com.linkcom.mdfe.LacUnidCarga;
import br.com.linkcom.mdfe.LacUnidTransp;
import br.com.linkcom.mdfe.Lacres;
import br.com.linkcom.mdfe.ListNAver;
import br.com.linkcom.mdfe.ListaAutXml;
import br.com.linkcom.mdfe.ListaCondutor;
import br.com.linkcom.mdfe.ListaDisp;
import br.com.linkcom.mdfe.ListaInfCiot;
import br.com.linkcom.mdfe.ListaInfContratante;
import br.com.linkcom.mdfe.ListaInfCte;
import br.com.linkcom.mdfe.ListaInfEmbComb;
import br.com.linkcom.mdfe.ListaInfMdfeTransp;
import br.com.linkcom.mdfe.ListaInfMunCarrega;
import br.com.linkcom.mdfe.ListaInfMunDescarga;
import br.com.linkcom.mdfe.ListaInfNfe;
import br.com.linkcom.mdfe.ListaInfPercurso;
import br.com.linkcom.mdfe.ListaInfTermCarreg;
import br.com.linkcom.mdfe.ListaInfTermDescarreg;
import br.com.linkcom.mdfe.ListaInfUnidCarga;
import br.com.linkcom.mdfe.ListaInfUnidCargaVazia;
import br.com.linkcom.mdfe.ListaInfUnidTransp;
import br.com.linkcom.mdfe.ListaInfUnidTranspVazia;
import br.com.linkcom.mdfe.ListaLacRodo;
import br.com.linkcom.mdfe.ListaLacUnidCarga;
import br.com.linkcom.mdfe.ListaLacUnidTransp;
import br.com.linkcom.mdfe.ListaLacres;
import br.com.linkcom.mdfe.ListaPeri;
import br.com.linkcom.mdfe.ListaSeg;
import br.com.linkcom.mdfe.ListaVag;
import br.com.linkcom.mdfe.ListaVeicReboque;
import br.com.linkcom.mdfe.NAver;
import br.com.linkcom.mdfe.Peri;
import br.com.linkcom.mdfe.Prop;
import br.com.linkcom.mdfe.Rodo;
import br.com.linkcom.mdfe.Seg;
import br.com.linkcom.mdfe.Tot;
import br.com.linkcom.mdfe.Trem;
import br.com.linkcom.mdfe.Vag;
import br.com.linkcom.mdfe.ValePed;
import br.com.linkcom.mdfe.VeicReboque;
import br.com.linkcom.mdfe.VeicTracao;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.ArquivoMdfeErro;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioEmbarcacaoComboio;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioTerminalCarregamento;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioTerminalDescarregamento;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioUnidadeCargaVazia;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioUnidadeTransporteVazia;
import br.com.linkcom.sined.geral.bean.MdfeAutorizacaoDownload;
import br.com.linkcom.sined.geral.bean.MdfeCte;
import br.com.linkcom.sined.geral.bean.MdfeFerroviarioVagao;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaCte;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaNfe;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteCte;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteNfe;
import br.com.linkcom.sined.geral.bean.MdfeLacre;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeCargaCte;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeCargaMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeCargaNfe;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeTransporteCte;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeTransporteMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeLacreUnidadeTransporteNfe;
import br.com.linkcom.sined.geral.bean.MdfeLocalCarregamento;
import br.com.linkcom.sined.geral.bean.MdfeNfe;
import br.com.linkcom.sined.geral.bean.MdfeProdutoPerigosoCte;
import br.com.linkcom.sined.geral.bean.MdfeProdutoPerigosoMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeProdutoPerigosoNfe;
import br.com.linkcom.sined.geral.bean.MdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioCiot;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioCondutor;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioContratante;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioReboque;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioValePedagio;
import br.com.linkcom.sined.geral.bean.MdfeSeguroCarga;
import br.com.linkcom.sined.geral.bean.MdfeSeguroCargaNumeroAverbacao;
import br.com.linkcom.sined.geral.bean.MdfeUfPercurso;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModalidadefreteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivoMdfeDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.EventoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.LkMdfeUtil;
import br.com.linkcom.utils.LkNfeUtil;
import br.com.linkcom.utils.StringUtils;

public class ArquivoMdfeService extends GenericService<ArquivoMdfe> {

	private ArquivoMdfeDAO arquivoMdfeDAO;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	private MdfeService mdfeService;
	private ArquivoMdfeErroService arquivoMdfeErroService;
	private ParametrogeralService parametrogeralService;
	private ConfiguracaonfeService configuracaonfeService;
	
	public void setArquivoMdfeDAO(ArquivoMdfeDAO arquivoMdfeDAO) {
		this.arquivoMdfeDAO = arquivoMdfeDAO;
	}
	
	public int marcarArquivoMdfe(String whereInMdfe, String flag) {
		return arquivoMdfeDAO.marcarArquivoMdfe(whereInMdfe, flag);
	}
	
	public List<ArquivoMdfe> findArquivoMdfeNovosForEmissao(Empresa empresa){
		return arquivoMdfeDAO.findArquivoMdfeNovosForEmissao(empresa);
	}
	
	public List<ArquivoMdfe> findArquivoMdfeNovosForConsulta(Empresa empresa){
		return arquivoMdfeDAO.findArquivoMdfeNovosForConsulta(empresa);
	}
	
	public List<ArquivoMdfe> findNovosEncerramentos(Empresa empresa) {
		return arquivoMdfeDAO.findNovosEncerramentos(empresa);
	}
	
	public List<ArquivoMdfe> findNovosCancelamentos(Empresa empresa) {
		return arquivoMdfeDAO.findNovosCancelamentos(empresa);
	}
	
	public List<ArquivoMdfe> findNovosInclusaoCondutor(Empresa empresa) {
		return arquivoMdfeDAO.findNovosInclusaoCondutor(empresa);
	}
	
	public void updateEncerramentoPorEvento(ArquivoMdfe arquivoMdfe, Boolean encerramentoPorEvento) {
		arquivoMdfeDAO.updateEncerramentoPorEvento(arquivoMdfe, encerramentoPorEvento);		
	}
	
	public void updateCancelamentoPorEvento(ArquivoMdfe arquivoMdfe, Boolean cancelamentoPorEvento) {
		arquivoMdfeDAO.updateCancelamentoPorEvento(arquivoMdfe, cancelamentoPorEvento);		
	}
	
	public void updateInclusaoCondutorPorEvento(ArquivoMdfe arquivoMdfe, Boolean inclusaoCondutorPorEvento) {
		arquivoMdfeDAO.updateInclusaoCondutorPorEvento(arquivoMdfe, inclusaoCondutorPorEvento);		
	}
	
	public void updateArquivoxmlEncerrando(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlEncerrando(arquivoMdfe);
	}
	
	public void updateArquivoXmlCancelando(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoXmlCancelando(arquivoMdfe);
	}
	
	public void updateArquivoXmlInclusaoCondutor(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoXmlInclusaoCondutor(arquivoMdfe);
	}
	
	public void updateArquivoxmlRetornoEncerramento(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlRetornoEncerramento(arquivoMdfe);
	}
	
	public void updateArquivoxmlRetornoCancelamento(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlRetornoCancelamento(arquivoMdfe);
	}
	
	public void updateArquivoxmlRetornoInclusaoCondutor(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlRetornoInclusaoCondutor(arquivoMdfe);
	}

	public void updateArquivoxmlassinado(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlassinado(arquivoMdfe);
	}

	public void updateArquivoretornoenvio(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoretornoenvio(arquivoMdfe);
	}

	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setMdfeService(MdfeService mdfeService) {
		this.mdfeService = mdfeService;
	}
	
	public void setArquivoMdfeErroService(ArquivoMdfeErroService arquivoMdfeErroService) {
		this.arquivoMdfeErroService = arquivoMdfeErroService;
	}
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	public static ArquivoMdfeService instance;
	public static ArquivoMdfeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivoMdfeService.class);
		}
		return instance;
	}
	
	public void naoEnviarArquivo_MdfeEstornada(Mdfe mdfe) {
		List<ArquivoMdfe> listaArquivoMdfe = findGeradosByMdfe(mdfe);
		if(SinedUtil.isListNotEmpty(listaArquivoMdfe)){
			for (ArquivoMdfe arquivoMdfe : listaArquivoMdfe) {
				updateSituacao(arquivoMdfe, ArquivoMdfeSituacao.NAO_ENVIADO);
			}
		}
	}
	
	public List<ArquivoMdfe> findGeradosByMdfe(Mdfe mdfe) {
		return arquivoMdfeDAO.findGeradosByMdfe(mdfe);
	}
	
	public ArquivoMdfe loadForEncerramento(Mdfe mdfe) {
		return arquivoMdfeDAO.loadForEncerramento(mdfe);
	}
	
	public List<ArquivoMdfe> findByMdfe(String whereIn) {
		return arquivoMdfeDAO.findByMdfe(whereIn);
	}
	
	public List<ArquivoMdfe> findByMdfeAutorizado(String whereIn) {
		return arquivoMdfeDAO.findByMdfeAutorizado(whereIn);
	}
	
	public ArquivoMdfe loadByMdfe(Mdfe mdfe, ArquivoMdfeSituacao situacao) {
		return arquivoMdfeDAO.loadByMdfe(mdfe, situacao);
	}

	@SuppressWarnings("unchecked")
	public void processaRetorno(ArquivoMdfe arquivoMdfe, byte[] content, Configuracaonfe configuracaonfe) throws UnsupportedEncodingException, JDOMException, IOException, ParseException {
		Element retEnviMdfe = SinedUtil.getRootElementXML(content);
		
		Element cStat = SinedUtil.getChildElement("cStat", retEnviMdfe.getContent());
		String cStatStr = cStat != null ? cStat.getValue() : null;
		
		if(cStatStr != null && cStatStr.equals("103")){
			Element infRec = SinedUtil.getChildElement("infRec", retEnviMdfe.getContent());
			
			Element dhRecbto = SinedUtil.getChildElement("dhRecbto", infRec.getContent());
			Element nRec = SinedUtil.getChildElement("nRec", infRec.getContent());
			
			String nRecStr = nRec.getValue();
			String dhRecbtoStr = dhRecbto.getValue();
			
			java.util.Date data = LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr);
			Timestamp dtrecebimento = new Timestamp(data.getTime());
			
			this.updateReciboDatarecebimento(arquivoMdfe, nRecStr, dtrecebimento);
			this.updateSituacao(arquivoMdfe, ArquivoMdfeSituacao.ENVIADO);
			
			mdfeService.atualizaMdfeSituacao(arquivoMdfe.getMdfe(), MdfeSituacao.ENVIADO);
			
			ConsReciMdfe consReciMdfe = this.createXmlConsultaLoteMdfe(nRecStr, configuracaonfe);
			String stringXmlConsultaLote = Util.strings.tiraAcento(consReciMdfe.toString());
			Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			arquivoMdfe.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoxmlconsultalote");
			arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
			
			arquivoMdfeDAO.updateConsultando(arquivoMdfe, Boolean.TRUE);
			this.updateArquivoxmlconsultalote(arquivoMdfe);
		} else {
			this.updateSituacao(arquivoMdfe, ArquivoMdfeSituacao.NAO_ENVIADO);
			
			if(arquivoMdfe.getMdfe() != null && arquivoMdfe.getMdfe().getCdmdfe() != null){
				mdfeService.atualizaMdfeSituacao(arquivoMdfe.getMdfe(), MdfeSituacao.EMITIDO);
			}
			
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retEnviMdfe.getContent());
			String xMotivoStr = xMotivo != null ? xMotivo.getValue() : "";
			
			ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
			arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
			arquivoMdfeErro.setMensagem(xMotivoStr);
			arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
		}
	}
	
	private ConsReciMdfe createXmlConsultaLoteMdfe(String nRecStr, Configuracaonfe configuracaonfe) {
		ConsReciMdfe consReciMdfe = new ConsReciMdfe();
		
		consReciMdfe.setnRec(nRecStr);
		
		if (configuracaonfe != null && configuracaonfe.getPrefixowebservice() != null && configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SEFAZMDFE_HOM)) {
			consReciMdfe.setTpAmb(2);
		} else if (configuracaonfe != null && configuracaonfe.getPrefixowebservice() != null && configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SEFAZMDFE_PROD)) {
			consReciMdfe.setTpAmb(1);
		}
		
		consReciMdfe.setVersao("3.00");
		
		return consReciMdfe;
	}

	private void updateArquivoxmlconsultalote(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoxmlconsultalote(arquivoMdfe);
	}

	public void updateSituacao(ArquivoMdfe arquivoMdfe, ArquivoMdfeSituacao situacao) {
		arquivoMdfeDAO.updateSituacao(arquivoMdfe, situacao);
	}

	private void updateReciboDatarecebimento(ArquivoMdfe arquivoMdfe, String numerorecibo, Timestamp dtrecebimento) {
		arquivoMdfeDAO.updateReciboDatarecebimento(arquivoMdfe, numerorecibo, dtrecebimento);
	}

	public void updateArquivoretornoconsulta(ArquivoMdfe arquivoMdfe) {
		arquivoMdfeDAO.updateArquivoretornoconsulta(arquivoMdfe);
	}

	@SuppressWarnings("unchecked")
	public boolean processaRetornoConsulta(ArquivoMdfe arquivoMdfe, String xml, Prefixowebservice prefixowebservice) throws ParseException, JDOMException, IOException {
		String strNovo = new String(Util.strings.tiraAcento(xml).getBytes(),"UTF-8");
		Element retConsReciMdfeElement = SinedUtil.getRootElementXML(strNovo.getBytes());
		
		arquivoMdfe = this.load(arquivoMdfe, "arquivoMdfe.cdArquivoMdfe, arquivoMdfe.mdfe");
		
		Element cStat = SinedUtil.getChildElement("cStat", retConsReciMdfeElement.getContent());
		String cStatStr = cStat.getValue();
		
		if(cStatStr.equals("104")){
			List<Element> listaProtMdfe = SinedUtil.getListChildElement("protMDFe", retConsReciMdfeElement.getContent());
			
			boolean sucesso = true;
			
			String paramEnvioNota = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIOAUTOMATICODENF);
			List<String> ids = new ArrayList<String>();
			for (Element protMDFe : listaProtMdfe) {
				Element infProt = SinedUtil.getChildElement("infProt", protMDFe.getContent());
				
				Element cStat_MDFe = SinedUtil.getChildElement("cStat", infProt.getContent());
				String cStatStr_MDFe = cStat_MDFe.getValue();
				
				Element xMotivo_MDFe = SinedUtil.getChildElement("xMotivo", infProt.getContent());
				String xMotivoStrMDFe = xMotivo_MDFe.getValue();
				
				Element chMDFe = SinedUtil.getChildElement("chMDFe", infProt.getContent());
				String chMDFeStr = chMDFe.getValue();
				
				if(cStatStr_MDFe.equals("100") || cStatStr_MDFe.equals("110") || cStatStr_MDFe.equals("301") || cStatStr_MDFe.equals("302")){
					Element dhRecbto = SinedUtil.getChildElement("dhRecbto", infProt.getContent());
					String dhRecbtoStr = dhRecbto != null ? dhRecbto.getValue() : null;
					Timestamp dataRec = null;
					if(dhRecbtoStr != null){
						dataRec = new Timestamp(LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr).getTime());
					} else {
						dataRec = new Timestamp(System.currentTimeMillis());
					}
					
					Element nProt = SinedUtil.getChildElement("nProt", infProt.getContent());
					String nProtStr = nProt != null ? nProt.getValue() : "";
					
					this.updateArquivoMdfenota(arquivoMdfe, dataRec, nProtStr, chMDFeStr);
					
					if(arquivoMdfe.getMdfe() != null){
						arquivoMdfe.getMdfe().setMdfeSituacao(MdfeSituacao.AUTORIZADO);
						mdfeService.alterarStatusAcao(arquivoMdfe.getMdfe(), xMotivoStrMDFe);
					}
				} else if (cStatStr_MDFe.equals("204")) {
					Element dhRecbto = SinedUtil.getChildElement("dhRecbto", infProt.getContent());
					String dhRecbtoStr = dhRecbto != null ? dhRecbto.getValue() : null;
					Timestamp dataRec = null;
					if(dhRecbtoStr != null){
						dataRec = new Timestamp(LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoStr).getTime());
					} else {
						dataRec = new Timestamp(System.currentTimeMillis());
					}

					String nProtStr = xMotivoStrMDFe.substring(xMotivoStrMDFe.indexOf("nProt") + 6, xMotivoStrMDFe.indexOf("nProt") + 21);
					
					this.updateArquivoMdfenota(arquivoMdfe, dataRec, nProtStr, chMDFeStr);
					
					if(arquivoMdfe.getMdfe() != null){
						arquivoMdfe.getMdfe().setMdfeSituacao(MdfeSituacao.AUTORIZADO);
						mdfeService.alterarStatusAcao(arquivoMdfe.getMdfe(), xMotivoStrMDFe);
					}
				} else {
					ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
					arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
					
					if(arquivoMdfe.getMdfe() != null){
						arquivoMdfe.getMdfe().setMdfeSituacao(MdfeSituacao.EMITIDO);
						mdfeService.alterarStatusAcao(arquivoMdfe.getMdfe(), xMotivoStrMDFe);
					}
					
					arquivoMdfeErro.setCodigo(cStatStr_MDFe);
					arquivoMdfeErro.setMensagem(xMotivoStrMDFe);
					arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
					arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
					
					sucesso = false;
				}
			}
			
			ArquivoMdfeSituacao situacao = ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO;
			if(!sucesso){
				situacao = ArquivoMdfeSituacao.PROCESSADO_COM_ERRO;
			} 
			
			this.updateSituacao(arquivoMdfe, situacao);
			
			return true;
		} else if(cStatStr.equals("215") ||
				cStatStr.equals("223") ||
				cStatStr.equals("225") ||
				cStatStr.equals("114") ||
				cStatStr.equals("248") ||
				cStatStr.equals("243") ||
				cStatStr.equals("252") ||
				cStatStr.equals("516") ||
				cStatStr.equals("517") ||
				cStatStr.equals("545") ||
				cStatStr.equals("565") ||
				cStatStr.equals("567") ||
				cStatStr.equals("656") ||
				cStatStr.equals("568")){
			this.updateSituacao(arquivoMdfe, ArquivoMdfeSituacao.NAO_ENVIADO);
			
			Mdfe mdfe = arquivoMdfe.getMdfe();
			mdfe.setMdfeSituacao(MdfeSituacao.EMITIDO);
			mdfeService.alterarStatusAcao(mdfe, "Arquivo MDF-e rejeitado.");
			
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retConsReciMdfeElement.getContent());
			String xMotivoStr = xMotivo.getValue();
			
			ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
			arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
			arquivoMdfeErro.setMensagem(xMotivoStr);
			arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
			
			return true;
		} else {
			Element xMotivo = SinedUtil.getChildElement("xMotivo", retConsReciMdfeElement.getContent());
			String xMotivoStr = xMotivo.getValue();
			
			ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
			arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
			arquivoMdfeErro.setMensagem(xMotivoStr);
			arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
			
			arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
			
			return false;
		}
	}
	
	private void updateArquivoMdfenota(ArquivoMdfe arquivoMdfe, Timestamp dataRec, String nProtStr, String chMDFeStr) {
		arquivoMdfeDAO.updateArquivoMdfenota(arquivoMdfe, dataRec, nProtStr, chMDFeStr);
	}
	
	public EnviMdfe createXmlMdfeEnvio(Mdfe bean, Configuracaonfe configuracaonfe) {
		EnviMdfe enviMdfe = new EnviMdfe();
		enviMdfe.setVersao("3.00");
		enviMdfe.setIdLote(bean.getCdmdfe());
		
		Integer tpEmis = (bean.getFormaEmissao() != null ? bean.getFormaEmissao().getValue() : 1); // EMISS�O NORMAL
		Integer nextNumMdfe = configuracaonfeService.getNextNumMdfe(configuracaonfe);
		
		String chaveAcesso = 
			StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getUf().getCdibge().toString()), "0", 2, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(new SimpleDateFormat("yyMM").format(bean.getDataHoraEmissao())), "0", 4, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(bean.getEmpresa().getCnpj().getValue()), "0", 14, false) + 
			StringUtils.stringCheia(StringUtils.soNumero("58"), "0", 2, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(bean.getSerie().toString()), "0", 3, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(bean.getNumero().toString()), "0", 9, false) + 
			tpEmis +
			StringUtils.stringCheia(StringUtils.soNumero(nextNumMdfe.toString()), "0", 8, false); 
		
		int dvChaveAcesso = modulo11(chaveAcesso);
		chaveAcesso = chaveAcesso + dvChaveAcesso;
		
		br.com.linkcom.mdfe.Mdfe mdfe = new br.com.linkcom.mdfe.Mdfe();
		InfMdfe infMdfe = new InfMdfe();
		infMdfe.setVersao("3.00");
		infMdfe.setId("MDFe" + (org.apache.commons.lang.StringUtils.isNotBlank(chaveAcesso) ? chaveAcesso : ""));
		infMdfe.setIde(createBeanIde(bean, configuracaonfe, nextNumMdfe, dvChaveAcesso));
		infMdfe.setEmit(createBeanEmit(bean, configuracaonfe));
		infMdfe.setInfModal(createBeanInfModal(bean));
		infMdfe.setInfDoc(createInfDoc(bean));
		infMdfe.setListaSeg(createBeanSeg(bean));
		infMdfe.setListaLacres(createBeanLacres(bean.getListaLacre()));
		infMdfe.setListaAutXml(createBeanAutXml(bean.getListaAutorizacaoDownload()));
		infMdfe.setTot(createBeanTot(bean));
		infMdfe.setInfAdic(createBeanInfAdic(bean));
		
		mdfe.setInfMdfe(infMdfe);
		
		if (configuracaonfe != null && configuracaonfe.getPrefixowebservice() != null && org.apache.commons.lang.StringUtils.isNotEmpty(chaveAcesso)) {
			InfMdfeSupl infMdfeSupl = new InfMdfeSupl();
			
			infMdfeSupl.setQrCodMdfe("https://dfe-portal.svrs.rs.gov.br/mdfe/qrCode?chMDFe=" + chaveAcesso + "&tpAmb=" + (configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SEFAZMDFE_PROD) ? 1 : 2));
			
			mdfe.setInfMdfeSupl(infMdfeSupl);
		}
		
		enviMdfe.setMdfe(mdfe);
		
		return enviMdfe;
	}
	
	private int modulo11(String chave){
		int[] pesos={4,3,2,9,8,7,6,5};  
		int somaPonderada = 0;  
		for (int i = 0; i < chave.length(); i++){
			somaPonderada += pesos[i%8]*(Integer.parseInt(chave.substring(i, i+1)));
		}  
		
		int resto = somaPonderada%11;
		
		if(resto == 0 || resto == 1) return 0;
		else return 11-resto;
	}
	
	private InfModal createBeanInfModal(Mdfe bean) {
		InfModal infModal = new InfModal();
		infModal.setVersaoModal("3.00");
		ModalidadefreteMdfe modalidadeFrete = bean.getModalidadeFrete();
		if(modalidadeFrete.equals(ModalidadefreteMdfe.RODOVIARIO)) infModal.setRodo(createBeanRodo(bean));
		if(modalidadeFrete.equals(ModalidadefreteMdfe.AEREO)) infModal.setAereo(createBeanAereo(bean));
		if(modalidadeFrete.equals(ModalidadefreteMdfe.AQUAVIARIO)) infModal.setAquav(createBeanAquav(bean));
		if(modalidadeFrete.equals(ModalidadefreteMdfe.FERROVIARIO)) infModal.setFerrov(createBeanFerrov(bean));
		return infModal;
	}
	
	private Rodo createBeanRodo(Mdfe bean) {
		Rodo rodo = new Rodo();
		rodo.setInfAntt(createBeanInfAntt(bean));
		rodo.setVeicTracao(createBeanVeicTracao(bean));
		rodo.setListaVeicReboque(createBeanListaVeicReboque(bean));
		rodo.setCodAgPorto(bean.getCodigoAgendamentoPorto());
		//rodo.setListaLacRodo(createBeanListaLacRodo(bean));
		
		return rodo;
	}
	
	private ListaLacRodo createBeanListaLacRodo(Mdfe bean) {
		ListaLacRodo listaLacRodo = new ListaLacRodo();
		if(SinedUtil.isListNotEmpty(bean.getListaLacre())){
			List<LacRodo> list = new ArrayList<LacRodo>();
			LacRodo lacRodo;
			for(MdfeLacre item : bean.getListaLacre()){
				lacRodo = new LacRodo();
				lacRodo.setnLacre(item.getNumeroLacre());
				list.add(lacRodo);
			}
			listaLacRodo.setLacRodoList(list);
		}
		return listaLacRodo;
	}

	private ListaVeicReboque createBeanListaVeicReboque(Mdfe bean) {
		ListaVeicReboque listaVeicReboque = new ListaVeicReboque();
		if(SinedUtil.isListNotEmpty(bean.getListaReboque())){
			List<VeicReboque> list = new ArrayList<VeicReboque>();
			VeicReboque veicReboque;
			for(MdfeRodoviarioReboque item : bean.getListaReboque()){
				veicReboque = new VeicReboque();
				veicReboque.setcInt(item.getCodigoInternoVeiculo());
				veicReboque.setPlaca(item.getPlaca());
				veicReboque.setRenavam(item.getRenavam());
				veicReboque.setTara(item.getTara());
				veicReboque.setCapKg(item.getCapacidadeKg());
				veicReboque.setCapM3(item.getCapacidadeM3());
				
				if (bean.getPertenceEmpresaEmitente() == null || !bean.getPertenceEmpresaEmitente()) {
					veicReboque.setProp(createBeanPropReboque(item));
				}
				
				veicReboque.setTpCar(item.getTipoCarroceria() != null ? item.getTipoCarroceria().getCdnfe() : null);
				Uf ufLicenciamento = item.getUfLicenciamento();
				if(ufLicenciamento != null){
					ufLicenciamento = UfService.getInstance().load(ufLicenciamento);
					veicReboque.setUf(ufLicenciamento != null ? ufLicenciamento.getSigla() : null);
				}
				list.add(veicReboque);
			}
			listaVeicReboque.setVeicReboqueList(list);
		}
		
		return listaVeicReboque;
	}

	private Prop createBeanPropReboque(MdfeRodoviarioReboque item) {
		Prop prop = new Prop();
		prop.setCpf(item.getCpf() != null ? item.getCpf().getValue() : null);
		prop.setCnpj(item.getCnpj() != null ? item.getCnpj().getValue() : null);
		prop.setRntrc(item.getRntrcProprietario());
		prop.setxNome(item.getNomeProprietario());
		prop.setIe(item.getIsento() != null && item.getIsento() ? "" : item.getInscEstProprietario());
		Uf ufProprietario = item.getUfProprietario();
		if(ufProprietario != null){
			ufProprietario = UfService.getInstance().load(ufProprietario);
			prop.setUf(ufProprietario != null ? ufProprietario.getSigla() : null);
		}
		prop.setTpProp(item.getTipoProprietario() != null ? item.getTipoProprietario().getValue() : null);
		return prop;
	}

	private VeicTracao createBeanVeicTracao(Mdfe bean) {
		VeicTracao veicTracao = new VeicTracao();
		veicTracao.setcInt(bean.getCodigoInternoVeiculo());
		if(bean.getPlaca()!=null){
			veicTracao.setPlaca(bean.getPlaca().toUpperCase().replace("-",""));						
		}
		veicTracao.setRenavam(bean.getRenavam());
		veicTracao.setTara(bean.getTara());
		veicTracao.setCapKg(bean.getCapacidadeKgRodoviario());
		veicTracao.setCapM3(bean.getCapacidadeM3Rodoviario());
		
		if (bean.getPertenceEmpresaEmitente() == null || !bean.getPertenceEmpresaEmitente()) {
			veicTracao.setProp(createBeanPropTracao(bean));
		}
		
		veicTracao.setListaCondutor(createBeanListaCondutor(bean));
		veicTracao.setTpRod(bean.getTipoRodado().getCdnfe());
		veicTracao.setTpCar(bean.getTipoCarroceria().getCdnfe());
		veicTracao.setUf(bean.getUfFreteRodoviario() != null ? bean.getUfFreteRodoviario().getSigla() : null);
		
		return veicTracao;
	}

	private ListaCondutor createBeanListaCondutor(Mdfe bean) {
		ListaCondutor listaCondutor = new ListaCondutor();
		if(SinedUtil.isListNotEmpty(bean.getListaCondutor())){
			List<Condutor> list = new ArrayList<Condutor>();
			Condutor condutor;
			for (MdfeRodoviarioCondutor item : bean.getListaCondutor()) {
				condutor = new Condutor();
				condutor.setCpf(item.getCpf() != null ? item.getCpf().getValue() : null);
				condutor.setxNome(item.getNome());
				list.add(condutor);
			}
			listaCondutor.setCondutorList(list);
		}
		return listaCondutor;
	}

	private Prop createBeanPropTracao(Mdfe bean) {
		Prop prop = new Prop();
		prop.setCpf(bean.getCpfProprietarioRodoviario() != null ? bean.getCpfProprietarioRodoviario().getValue() : null);
		prop.setCnpj(bean.getCnpjProprietarioRodoviario() != null ? bean.getCnpjProprietarioRodoviario().getValue() : null);
		prop.setRntrc(bean.getRntrcProprietario());
		prop.setxNome(bean.getNomeProprietarioRodoviario());
		prop.setIe(bean.getIsentoRodoviario() != null && bean.getIsentoRodoviario() ? "" : bean.getInscEstProprietarioRodoviario());
		prop.setUf(bean.getUfProprietarioRodoviario() != null && bean.getUfProprietarioRodoviario().getCdibge() != null ? bean.getUfProprietarioRodoviario().getSigla() : null);
		prop.setTpProp(bean.getTipoProprietarioVeiculoRodoviario() != null ? bean.getTipoProprietarioVeiculoRodoviario().getValue() : null);
		return prop;
	}

	private ListaInfContratante createBeanListaInfContratante(Mdfe bean) {
		ListaInfContratante listaInfContratante = new ListaInfContratante();
		if(SinedUtil.isListNotEmpty(bean.getListaContratante())){
			List<InfContratante> list = new ArrayList<InfContratante>();
			InfContratante infContratante;
			for(MdfeRodoviarioContratante item : bean.getListaContratante()){
				infContratante = new InfContratante();
				infContratante.setCnpj(item.getCnpj() != null ? item.getCnpj().getValue() : null);
				infContratante.setCpf(item.getCpf() != null ? item.getCpf().getValue() : null);
				list.add(infContratante);
			}
			listaInfContratante.setInfContratanteList(list);
		}
		return listaInfContratante;
	}

	private ValePed createBeanValePed(Mdfe bean) {
		ValePed valePed = new ValePed();
		ListaDisp listaDisp = new ListaDisp();
		if(SinedUtil.isListNotEmpty(bean.getListaValePedagio())){
			List<Disp> list = new ArrayList<Disp>();
			Disp disp;
			for(MdfeRodoviarioValePedagio item : bean.getListaValePedagio()){
				disp = new Disp();
				disp.setCnpjForn(item.getCnpjEmpresaFornecedora() != null ? item.getCnpjEmpresaFornecedora().getValue() : null);
				disp.setCnpjPg(item.getCnpjResponsavelPagamento() != null ? item.getCnpjResponsavelPagamento().getValue() : null);
				disp.setCpfPg(item.getCpfResponsavelPagamento() != null ? item.getCpfResponsavelPagamento().getValue() : null);
				disp.setnCompra(item.getNumeroComprovante());
				disp.setvValePed(item.getValor() != null ? item.getValor().getValue().doubleValue() : null);
				list.add(disp);
			}
			listaDisp.setDispList(list);
		}
		valePed.setListaDisp(listaDisp);
		return valePed;
	}

	private ListaInfCiot createBeanInfCiot(Mdfe bean) {
		ListaInfCiot listaInfCiot = new ListaInfCiot();
		if(SinedUtil.isListNotEmpty(bean.getListaCiot())){
			List<InfCiot> lista = new ArrayList<InfCiot>();
			InfCiot infCiot;
			for(MdfeRodoviarioCiot item : bean.getListaCiot()){
				infCiot = new InfCiot();
				infCiot.setCiot(item.getCiot());
				infCiot.setCnpj(item.getCnpj() != null ? item.getCnpj().getValue() : null);
				infCiot.setCpf(item.getCpf() != null ? item.getCpf().getValue() : null);
				lista.add(infCiot);
			}
			listaInfCiot.setInfCiotList(lista);
		}
		
		return listaInfCiot;
	}
	
	private InfAntt createBeanInfAntt(Mdfe bean) {
		InfAntt infAntt = new InfAntt();
		infAntt.setRntrc(bean.getRntrc());
		infAntt.setListaInfCiot(createBeanInfCiot(bean));
		infAntt.setValePed(createBeanValePed(bean));
		infAntt.setListaInfContratante(createBeanListaInfContratante(bean));
		return infAntt;
	}
	private br.com.linkcom.mdfe.Emit createBeanEmit(Mdfe mdfe, Configuracaonfe configuracaonfe) {
		br.com.linkcom.mdfe.Emit emit = new br.com.linkcom.mdfe.Emit();
		
		if(mdfe.getEmpresa() != null){
			emit.setCnpj(mdfe.getEmpresa().getCnpj() != null ? mdfe.getEmpresa().getCnpj().getValue() : null);
			emit.setIe(mdfe.getEmpresa().getInscricaoestadual());
			emit.setxNome(mdfe.getEmpresa().getRazaosocialOuNome());
			emit.setxFant(mdfe.getEmpresa().getNomefantasia());
			
			br.com.linkcom.mdfe.EnderEmit enderEmit = new br.com.linkcom.mdfe.EnderEmit();
			if(configuracaonfe != null){
				if(configuracaonfe.getEndereco() != null){
					Endereco endereco = configuracaonfe.getEndereco();
					enderEmit.setxLgr(endereco.getLogradouro());
					enderEmit.setNro(endereco.getNumero());
					enderEmit.setxCpl(endereco.getComplemento());
					enderEmit.setxBairro(endereco.getBairro());
					
					if(endereco.getPais() == null || Pais.BRASIL.equals(endereco.getPais())){
						enderEmit.setcMun(endereco.getMunicipio() != null ? endereco.getMunicipio().getCdibge() : null);
						enderEmit.setxMun(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : null);
						enderEmit.setUf(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : null);
					}else {
						enderEmit.setcMun(Municipio.EXTERIOR_MDFE.getCdibge());
						enderEmit.setxMun(Municipio.EXTERIOR_MDFE.getNome());
						enderEmit.setUf(Municipio.EXTERIOR_MDFE.getUf().getSigla());
					}
					
					enderEmit.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
				}
				enderEmit.setFone(StringUtils.soNumero(configuracaonfe.getTelefone()));
			}
			enderEmit.setEmail(mdfe.getEmpresa().getEmail());
			emit.setEnderEmit(enderEmit);
		}
		
		return emit;
	}
	
	private Aereo createBeanAereo(Mdfe bean){
		Aereo aereo = new Aereo();
		
		aereo.setNac(bean.getMarcaNacionalidadeAeronave());
		aereo.setMatr(bean.getMarcaMatriculaAeronave());
		aereo.setnVoo(bean.getNumeroVoo());
		aereo.setcAerEmb(bean.getAerodromoEmbarque());
		aereo.setcAerDes(bean.getAerodromoDestino());
		aereo.setdVoo(bean.getDataVoo() != null ? SinedUtil.FORMATADOR_DATA.format(bean.getDataVoo()) : null);
		
		return aereo;
	}
	
	private Aquav createBeanAquav(Mdfe mdfe){
		Aquav aquav = new Aquav();
		
		aquav.setIrin(mdfe.getIrinNavio());
		aquav.setTpEmb(mdfe.getCodigoTipoEmbarcacao());
		aquav.setcEmbar(mdfe.getCodigoEmbarcacao());
		aquav.setxEmbar(mdfe.getNomeEmbarcacao());
		aquav.setnViag(mdfe.getNumeroViagemEmbarcacao());
		aquav.setcPrtEmb(mdfe.getCodigoPortoEmbarque());
		aquav.setcPrtDest(mdfe.getCodigoPortoDestino());
		aquav.setPrtTrans(mdfe.getPortoTransbordo());
		aquav.setTpNav(mdfe.getTipoNavegacao() != null ? mdfe.getTipoNavegacao().getValue() : null);
		
		aquav.setListaInfTermCarreg(createBeanInfTermCarreg(mdfe.getListaAquaviarioTerminalCarregamento()));
		aquav.setListaInfTermDescarreg(createBeanInfTermDescarreg(mdfe.getListaAquaviarioTerminalDescarregamento()));
		aquav.setListaInfEmbComb(createBeanInfEmbComb(mdfe.getListaAquaviarioEmbarcacaoComboio()));
		aquav.setListaInfUnidCargaVazia(createBeanInfUnidCargaVazia(mdfe.getListaAquaviarioUnidadeCargaVazia()));
		aquav.setListaInfUnidTranspVazia(createBeanInfTermCarreg(mdfe.getListaAquaviarioUnidadeTransporteVazia()));
		
		return aquav;
	}
	
	private ListaInfUnidTranspVazia createBeanInfTermCarreg(List<MdfeAquaviarioUnidadeTransporteVazia> lista) {
		ListaInfUnidTranspVazia listaInfUnidTranspVazia = new ListaInfUnidTranspVazia();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfUnidTranspVazia> list = new ArrayList<InfUnidTranspVazia>();
			InfUnidTranspVazia infUnidTranspVazia;
			for(MdfeAquaviarioUnidadeTransporteVazia bean : lista){
				infUnidTranspVazia = new InfUnidTranspVazia();
				infUnidTranspVazia.setIdUnidTranspVazia(bean.getIdentificacaoUnidadeTransporte());
				infUnidTranspVazia.setTpUnidTranspVazia(bean.getTipoUnidadeTransporte() != null ? bean.getTipoUnidadeTransporte().getCdnfe() : null);
				list.add(infUnidTranspVazia);
			}
			listaInfUnidTranspVazia.setInfUnidTranspVaziaList(list);
		}
		return listaInfUnidTranspVazia;
	}
	
	private ListaInfUnidCargaVazia createBeanInfUnidCargaVazia(List<MdfeAquaviarioUnidadeCargaVazia> lista) {
		ListaInfUnidCargaVazia listaInfUnidCargaVazia = new ListaInfUnidCargaVazia();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfUnidCargaVazia> list = new ArrayList<InfUnidCargaVazia>();
			InfUnidCargaVazia InfUnidCargaVazia;
			for(MdfeAquaviarioUnidadeCargaVazia bean : lista){
				InfUnidCargaVazia = new InfUnidCargaVazia();
				InfUnidCargaVazia.setIdUnidCargaVazia(bean.getIdentificacaoUnidadeCarga());
				InfUnidCargaVazia.setTpUnidCargaVazia(bean.getTipoUnidadeCarga() != null ? bean.getTipoUnidadeCarga().getCdnfe() : null);
				list.add(InfUnidCargaVazia);
			}
			listaInfUnidCargaVazia.setInfUnidCargaVaziaList(list);
		}
		return listaInfUnidCargaVazia;
	}
	
	private ListaInfEmbComb createBeanInfEmbComb(List<MdfeAquaviarioEmbarcacaoComboio> lista) {
		ListaInfEmbComb listaInfEmbComb = new ListaInfEmbComb();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfEmbComb> list = new ArrayList<InfEmbComb>();
			InfEmbComb infEmbComb;
			for(MdfeAquaviarioEmbarcacaoComboio bean : lista){
				infEmbComb = new InfEmbComb();
				infEmbComb.setcEmbComb(bean.getCodigoEmbarcacao());
				infEmbComb.setxBalsa(bean.getIdentificadorBalsa());
				list.add(infEmbComb);
			}
			listaInfEmbComb.setInfEmbCombList(list);
		}
		return listaInfEmbComb;
	}
	
	private ListaInfTermDescarreg createBeanInfTermDescarreg(List<MdfeAquaviarioTerminalDescarregamento> lista) {
		ListaInfTermDescarreg listaInfTermDescarreg = new ListaInfTermDescarreg();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfTermDescarreg> list = new ArrayList<InfTermDescarreg>();
			InfTermDescarreg infTermDescarreg;
			for(MdfeAquaviarioTerminalDescarregamento bean : lista){
				infTermDescarreg = new InfTermDescarreg();
				infTermDescarreg.setcTermDescarreg(bean.getCodigoTerminal());
				infTermDescarreg.setxTermDescarreg(bean.getNomeTerminal());
				list.add(infTermDescarreg);
			}
			listaInfTermDescarreg.setInfTermDescarregList(list);
		}
		return listaInfTermDescarreg;
	}
	
	private ListaInfTermCarreg createBeanInfTermCarreg(List<MdfeAquaviarioTerminalCarregamento> lista){
		ListaInfTermCarreg listaInfTermCarreg = new ListaInfTermCarreg();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfTermCarreg> list = new ArrayList<InfTermCarreg>();
			InfTermCarreg infTermCarreg;
			for(MdfeAquaviarioTerminalCarregamento bean : lista){
				infTermCarreg = new InfTermCarreg();
				infTermCarreg.setcTermCarreg(bean.getCodigoTerminal());
				infTermCarreg.setxTermCarreg(bean.getNomeTerminal());
				list.add(infTermCarreg);
			}
			listaInfTermCarreg.setInfTermCarregList(list);
		}
		return listaInfTermCarreg;
	}
	
	private Ferrov createBeanFerrov(Mdfe mdfe){
		Ferrov ferrov = new Ferrov();
		
		ferrov.setTrem(createBeanTrem(mdfe));
		ferrov.setListaVag(createBeanListaVag(mdfe.getListaVagao()));
		
		return ferrov;
	}
	
	private ListaVag createBeanListaVag(List<MdfeFerroviarioVagao> lista) {
		ListaVag listaVag = new ListaVag();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<Vag> list = new ArrayList<Vag>();
			Vag vag;
			for(MdfeFerroviarioVagao bean : lista){
				vag = new Vag();
				vag.setPesoBC(bean.getPesoBaseCalculoFreteToneladas() != null ? LkMdfeUtil.impressaoValorTresCasasPonto(bean.getPesoBaseCalculoFreteToneladas()) : "");
				vag.setPesoR(bean.getPesoRealToneladas() != null ? LkMdfeUtil.impressaoValorTresCasasPonto(bean.getPesoRealToneladas()) : "");
				vag.setTpVag(bean.getTipoVagao());
				vag.setSerie(bean.getSerieIdentificacao());
				vag.setnVag(bean.getNumeroIdentificacao());
				vag.setnSeq(bean.getSequenciaNaComposicao());
				vag.setTu(bean.getToneladaUtil() != null ? LkMdfeUtil.impressaoValorTresCasasPonto(bean.getToneladaUtil()) : "");
				list.add(vag);
			}
			listaVag.setVagList(list);
		}
		return listaVag;
	}

	private Trem createBeanTrem(Mdfe mdfe) {
		Trem trem = new Trem();
		trem.setxPref(mdfe.getPrefixoTrem());
		trem.setDhTrem(SinedUtil.impressaoDataMDFe(mdfe.getDataHoraLiberacaoTremOrigem()));
		trem.setxOri(mdfe.getOrigemTrem());
		trem.setxDest(mdfe.getDestinoTrem());
		trem.setqVag(mdfe.getQtdeVagoesCarregados());
		
		return trem;
	}
	
	public br.com.linkcom.mdfe.Ide createBeanIde(Mdfe mdfe, Configuracaonfe configuracaonfe, Integer cMdf, Integer dvChaveAcesso){
		br.com.linkcom.mdfe.Ide ide = new br.com.linkcom.mdfe.Ide();
		if(mdfe.getUf()!=null){
			ide.setcUf(mdfe.getUf().getCdibge());
		}
		
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		ide.setTpAmb(prefixowebservice.equals(Prefixowebservice.SEFAZMDFE_PROD) ? 1 : 2);
		ide.setTpEmit(mdfe.getTipoEmitenteMdfe() != null ? mdfe.getTipoEmitenteMdfe().getValue() : null);
		ide.setTpTransp(mdfe.getTipoTransportador() != null ? mdfe.getTipoTransportador().getValue() : null);
		ide.setMod("58");
		ide.setSerie(mdfe.getSerie()!=null? mdfe.getSerie(): "0");
		ide.setnMdf(mdfe.getNumero());
		ide.setcMdf(cMdf);
		ide.setcDv(dvChaveAcesso);
		ide.setModal(mdfe.getModalidadeFrete() != null ? mdfe.getModalidadeFrete().getValue() : null);
		ide.setDhEmi(mdfe.getDataHoraEmissao());
		ide.setTpEmis(mdfe.getFormaEmissao().getValue());
		ide.setProcEmi(0);
		ide.setVerProc("W3erp 2.0");
		ide.setIndCanalVerde(null);
		ide.setUfIni(mdfe.getUfLocalCarregamento() != null ? mdfe.getUfLocalCarregamento().getSigla() : null);
		ide.setUfFim(mdfe.getUfLocalDescarregamento() != null ? mdfe.getUfLocalDescarregamento().getSigla() : null);
		ide.setListaInfMunCarrega(createBeanListMunCarrega(mdfe));
		
		if (mdfe.getCarregamentoNoExterior() != null && mdfe.getCarregamentoNoExterior()) {
			ListaInfMunCarrega listaInfMunCarrega = new ListaInfMunCarrega();
			List<InfMunCarrega> lista = new ArrayList<InfMunCarrega>();
			
			InfMunCarrega infMunCarrega = new InfMunCarrega();
			infMunCarrega.setcMunCarrega(Municipio.EXTERIOR_MDFE.getCdibge());
			infMunCarrega.setxMunCarrega(Municipio.EXTERIOR_MDFE.getNome());
			
			lista.add(infMunCarrega);
			listaInfMunCarrega.setInfMunCarregaList(lista);
			
			ide.setUfIni(Municipio.EXTERIOR_MDFE.getUf().getSigla());
			ide.setListaInfMunCarrega(listaInfMunCarrega);
		}
		
		if (mdfe.getDescarregamentoNoExterior() != null && mdfe.getDescarregamentoNoExterior()) {
			ide.setUfFim(Municipio.EXTERIOR_MDFE.getUf().getSigla());
		}
		
		ide.setListaInfPercurso(createBeanInfPercurso(mdfe));
		ide.setDhIniViagem(mdfe.getDataHoraInicioViagem());
		
		return ide;
	}
	
	private ListaInfPercurso createBeanInfPercurso(Mdfe mdfe) {
		ListaInfPercurso listaInfPercurso = new ListaInfPercurso();
		if(SinedUtil.isListNotEmpty(mdfe.getListaUfPercurso())){
			List<InfPercurso> lista = new ArrayList<InfPercurso>();
			InfPercurso infPercurso;
			for(MdfeUfPercurso bean : mdfe.getListaUfPercurso()){
				infPercurso = new InfPercurso();
				infPercurso.setUfPer(bean.getUf() != null ? bean.getUf().getSigla() : null);
				lista.add(infPercurso);
			}
			listaInfPercurso.setInfPercursoList(lista);
		}
		return listaInfPercurso;
	}
	
	public ListaInfMunCarrega createBeanListMunCarrega(Mdfe mdfe){
		ListaInfMunCarrega listaInfMunCarrega = new ListaInfMunCarrega();
		if(SinedUtil.isListNotEmpty(mdfe.getListaLocalCarregamento())){
			List<InfMunCarrega> lista = new ArrayList<InfMunCarrega>();
			InfMunCarrega infMunCarrega;
			for(MdfeLocalCarregamento bean : mdfe.getListaLocalCarregamento()){
				infMunCarrega = new InfMunCarrega();
				infMunCarrega.setcMunCarrega(bean.getMunicipio() != null ? bean.getMunicipio().getCdibge() : null);
				infMunCarrega.setxMunCarrega(bean.getMunicipio() != null ? bean.getMunicipio().getNome() : null);
				lista.add(infMunCarrega);
			}
			listaInfMunCarrega.setInfMunCarregaList(lista);
		}
		return listaInfMunCarrega;
	}
	
	private Tot createBeanTot(Mdfe mdfe) {
		Tot tot = new Tot();
		
		tot.setcUnid(mdfe.getUnidadeMedidaPesoBrutoCarga() != null ? mdfe.getUnidadeMedidaPesoBrutoCarga().getCdnfe() : null);
		tot.setqCarga(mdfe.getPesoBrutoTotalCarga());
		tot.setvCarga(mdfe.getValorTotalCargaTransportada() != null ? mdfe.getValorTotalCargaTransportada().getValue().doubleValue() : null);
		
		if (mdfe.getListaMdfeCte() != null && mdfe.getListaMdfeCte().size() > 0) {
			tot.setqCte(mdfe.getListaMdfeCte().size());
		}
		
		if (mdfe.getListaNfe() != null && mdfe.getListaNfe().size() > 0) {
			tot.setqNfe(mdfe.getListaNfe().size());
		}
		
		if (mdfe.getListaMdfeReferenciado() != null && mdfe.getListaMdfeReferenciado().size() > 0) {
			tot.setqMdfe(mdfe.getListaMdfeReferenciado().size());
		}
		
		return tot;
	}
	
	private ListaLacres createBeanLacres(List<MdfeLacre> lista) {
		ListaLacres listaLacres = new ListaLacres();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<Lacres> list = new ArrayList<Lacres>();
			Lacres lacres;
			
			for(MdfeLacre bean : lista){
				lacres = new Lacres();
				
				lacres.setnLacre(bean.getNumeroLacre().toString());
				
				list.add(lacres);
			}
			
			listaLacres.setLacresList(list);
		}
		
		return listaLacres;
	}
	
	private ListaAutXml createBeanAutXml(List<MdfeAutorizacaoDownload> lista) {
		ListaAutXml listaAutXml = new ListaAutXml();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<AutXml> list = new ArrayList<AutXml>();
			AutXml autXml;
			
			for(MdfeAutorizacaoDownload mdfeAutorizacaoDownload : lista){
				autXml = new AutXml();
				
				if (mdfeAutorizacaoDownload.getCnpj() != null) autXml.setCnpj(mdfeAutorizacaoDownload.getCnpj().getValue());
				if (mdfeAutorizacaoDownload.getCpf() != null) autXml.setCpf(mdfeAutorizacaoDownload.getCpf().getValue());
				
				list.add(autXml);
			}
			
			listaAutXml.setAutXmlList(list);
		}
		
		return listaAutXml;
	}
	
	private br.com.linkcom.mdfe.InfAdic createBeanInfAdic(Mdfe mdfe) {
		br.com.linkcom.mdfe.InfAdic infAdic = new br.com.linkcom.mdfe.InfAdic();
		
		infAdic.setInfAdFisco(mdfe.getInfoAdicionaisFisco());
		infAdic.setInfCpl(mdfe.getInfoAdicionaisContribuinte());
		
		return infAdic;
	}

	private InfDoc createInfDoc(Mdfe mdfe){
		InfDoc infDoc = new InfDoc();
		HashMap<Municipio, List<MdfeCte>> mapMunicipioMdfeCte = new HashMap<Municipio, List<MdfeCte>>();
		HashMap<Municipio, List<MdfeNfe>> mapMunicipioMdfeNfe = new HashMap<Municipio, List<MdfeNfe>>();
		HashMap<Municipio, List<MdfeReferenciado>> mapMunicipioMdfeReferenciado = new HashMap<Municipio, List<MdfeReferenciado>>();
		
		List<Municipio> listaMunicipios = preencherMapMunicipio(mdfe, mapMunicipioMdfeCte, mapMunicipioMdfeNfe, mapMunicipioMdfeReferenciado);
		if(SinedUtil.isListNotEmpty(listaMunicipios)){
			infDoc.setListaInfMunDescarga(createBeanInfMunDescarga(listaMunicipios, mdfe, mapMunicipioMdfeCte, mapMunicipioMdfeNfe, mapMunicipioMdfeReferenciado));
		}
		return infDoc;
	}
	
	private List<Municipio> preencherMapMunicipio(Mdfe mdfe, HashMap<Municipio, List<MdfeCte>> mapMunicipioMdfeCte, HashMap<Municipio, List<MdfeNfe>> mapMunicipioMdfeNfe,	HashMap<Municipio, List<MdfeReferenciado>> mapMunicipioMdfeReferenciado) {
		List<Municipio> listaMunicipios = new ArrayList<Municipio>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaMdfeCte())){
			List<MdfeCte> listaMdfecte;
			for(MdfeCte bean : mdfe.getListaMdfeCte()){
				listaMdfecte = mapMunicipioMdfeCte.get(bean.getMunicipio());
				
				if(listaMdfecte == null) listaMdfecte = new ArrayList<MdfeCte>();
				listaMdfecte.add(bean);
				
				if (bean.getMunicipio() != null) {
					mapMunicipioMdfeCte.put(bean.getMunicipio(), listaMdfecte);
				} else {
					mapMunicipioMdfeCte.put(Municipio.EXTERIOR_MDFE, listaMdfecte);
				}
				
				if(!listaMunicipios.contains(bean.getMunicipio())) listaMunicipios.add(bean.getMunicipio());
			}
		}
		if(SinedUtil.isListNotEmpty(mdfe.getListaNfe())){
			List<MdfeNfe> listaMdfeNfe;
			for(MdfeNfe bean : mdfe.getListaNfe()){
				listaMdfeNfe = mapMunicipioMdfeNfe.get(bean.getMunicipio());
				
				if(listaMdfeNfe == null) listaMdfeNfe = new ArrayList<MdfeNfe>();
				listaMdfeNfe.add(bean);
				
				if (bean.getMunicipio() != null) {
					mapMunicipioMdfeNfe.put(bean.getMunicipio(), listaMdfeNfe);
					
					if(!listaMunicipios.contains(bean.getMunicipio())) {
						listaMunicipios.add(bean.getMunicipio());
					}
				} else {
					mapMunicipioMdfeNfe.put(Municipio.EXTERIOR_MDFE, listaMdfeNfe);
					
					if(!listaMunicipios.contains(Municipio.EXTERIOR_MDFE)) {
						listaMunicipios.add(Municipio.EXTERIOR_MDFE);
					}
				}
			}
		}
		if(SinedUtil.isListNotEmpty(mdfe.getListaMdfeReferenciado())){
			List<MdfeReferenciado> listaMdfeReferenciado;
			for(MdfeReferenciado bean : mdfe.getListaMdfeReferenciado()){
				listaMdfeReferenciado = mapMunicipioMdfeReferenciado.get(bean.getMunicipio());
				
				if(listaMdfeReferenciado == null) listaMdfeReferenciado = new ArrayList<MdfeReferenciado>();
				listaMdfeReferenciado.add(bean);
				
				if (bean.getMunicipio() != null) {
					mapMunicipioMdfeReferenciado.put(bean.getMunicipio(), listaMdfeReferenciado);
				} else {
					mapMunicipioMdfeReferenciado.put(Municipio.EXTERIOR_MDFE, listaMdfeReferenciado);
				}
				
				if(!listaMunicipios.contains(bean.getMunicipio())) listaMunicipios.add(bean.getMunicipio());
			}
		}
		
		
		return listaMunicipios;
	}

	private ListaInfCte createBeanInfCte(Mdfe mdfe, List<MdfeCte> lista) {
		ListaInfCte listaInfCte = new ListaInfCte();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfCte> list = new ArrayList<InfCte>(); 
			InfCte infCte;
			for(MdfeCte bean : lista){
				infCte = new InfCte();
				infCte.setChCte(bean.getChaveAcesso());
				infCte.setSegCodBarra(bean.getCodigoBarra());
				infCte.setIndReentrega(Boolean.TRUE.equals(bean.getIndicadorReentrega()) ? "1" : null);
				infCte.setListaInfUnidTransp(createBeanInfUnidTranspCte(mdfe, bean));
				infCte.setListaPeri(createBeanPeriCte(mdfe, bean));
				
				list.add(infCte);
			}
			listaInfCte.setInfCteList(list);
		}
		return listaInfCte;
	}
	
	private ListaInfNfe createBeanInfNfe(Mdfe mdfe, List<MdfeNfe> lista) {
		ListaInfNfe listaInfNfe = new ListaInfNfe();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfNfe> list = new ArrayList<InfNfe>(); 
			InfNfe infNfe;
			
			for(MdfeNfe mdfeNfe : lista) {
				infNfe = new InfNfe();
				infNfe.setChNfe(mdfeNfe.getChaveAcesso());
				infNfe.setSegCodBarra(null);
				infNfe.setIndReentrega(mdfeNfe.getIndicadorReentrega() != null && mdfeNfe.getIndicadorReentrega() ? 1 : null);
				infNfe.setListaInfUnidTransp(createBeanInfUnidTranspNfe(mdfe, mdfeNfe));
				infNfe.setListaPeri(createBeanPeriNfe(mdfe, mdfeNfe));
				
				list.add(infNfe);
			}
			
			listaInfNfe.setInfNfeList(list);
		}
		
		return listaInfNfe;
	}
	
	private ListaInfMdfeTransp createBeanInfMdfeReferenciado(Mdfe mdfe, List<MdfeReferenciado> lista) {
		ListaInfMdfeTransp listaInfMdfeTransp = new ListaInfMdfeTransp();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfMdfeTransp> list = new ArrayList<InfMdfeTransp>(); 
			InfMdfeTransp infMdfeTransp;
			
			for(MdfeReferenciado mdfeReferenciado : lista) {
				infMdfeTransp = new InfMdfeTransp();
				infMdfeTransp.setChMdfeTransp(mdfeReferenciado.getChaveAcesso());
				infMdfeTransp.setIndReentrega(mdfeReferenciado.getIndicadorReentrega() != null && mdfeReferenciado.getIndicadorReentrega() ? 1 : null);
				infMdfeTransp.setListaInfUnidTransp(createBeanInfUnidTranspMdfeReferenciado(mdfe, mdfeReferenciado));
				infMdfeTransp.setListaPeri(createBeanPeriMdfeReferenciado(mdfe, mdfeReferenciado));
				
				list.add(infMdfeTransp);
			}
			
			listaInfMdfeTransp.setInfMdfeTranspList(list);
		}
		
		return listaInfMdfeTransp;
	}
	
	private ListaInfUnidTransp createBeanInfUnidTranspMdfeReferenciado(Mdfe mdfe, MdfeReferenciado bean) {
		ListaInfUnidTransp listaInfUnidTransp = new ListaInfUnidTransp();
		List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> lista = bean.getListaInformacaoUnidadeTransporteMdfeReferenciado(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidTransp> list = new ArrayList<InfUnidTransp>();
			InfUnidTransp infUnidTransp;
			
			for (MdfeInformacaoUnidadeTransporteMdfeReferenciado item : lista) {
				infUnidTransp = new InfUnidTransp();
				
				infUnidTransp.setIdUnidTransp(item.getIdUnidadeTransporte());
				infUnidTransp.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidTransp.setTpUnidTransp(item.getTipoTransporte() != null ? item.getTipoTransporte().getCdnfe() : null);
				
				infUnidTransp.setListaInfUnidCarga(createBeanInfUnidCargaMdfe(mdfe, item));
				infUnidTransp.setListaLacUnidTransp(createBeanLacUnidTranspMdfeReferenciado(mdfe, item));
				
				list.add(infUnidTransp);
			}
			
			listaInfUnidTransp.setInfUnidTranspList(list);
		}
		
		return listaInfUnidTransp;
	}
	
	private ListaInfUnidTransp createBeanInfUnidTranspNfe(Mdfe mdfe, MdfeNfe bean) {
		ListaInfUnidTransp listaInfUnidTransp = new ListaInfUnidTransp();
		List<MdfeInformacaoUnidadeTransporteNfe> lista = bean.getListaInformacaoUnidadeTransporteNfe(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidTransp> list = new ArrayList<InfUnidTransp>();
			InfUnidTransp infUnidTransp;
			
			for (MdfeInformacaoUnidadeTransporteNfe item : lista) {
				infUnidTransp = new InfUnidTransp();
				
				infUnidTransp.setIdUnidTransp(item.getIdUnidadeTransporte());
				infUnidTransp.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidTransp.setTpUnidTransp(item.getTipoTransporte() != null ? item.getTipoTransporte().getCdnfe() : null);
				
				infUnidTransp.setListaInfUnidCarga(createBeanInfUnidCargaNfe(mdfe, item));
				infUnidTransp.setListaLacUnidTransp(createBeanLacUnidTranspNfe(mdfe, item));
				
				list.add(infUnidTransp);
			}
			
			listaInfUnidTransp.setInfUnidTranspList(list);
		}
		
		return listaInfUnidTransp;
	}
	
	private ListaInfUnidTransp createBeanInfUnidTranspCte(Mdfe mdfe, MdfeCte mdfeCte) {
		List<MdfeInformacaoUnidadeTransporteCte> lista = mdfeCte.getListaInformacaoUnidadeTransporteCte(mdfeCte, mdfe);
		ListaInfUnidTransp listaInfUnidTransp = new ListaInfUnidTransp();
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidTransp> list = new ArrayList<InfUnidTransp>();
			InfUnidTransp infUnidTransp;
			
			for (MdfeInformacaoUnidadeTransporteCte item : lista) {
				infUnidTransp = new InfUnidTransp();
				
				infUnidTransp.setIdUnidTransp(item.getIdUnidadeTransporte());
				infUnidTransp.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidTransp.setTpUnidTransp(item.getTipoTransporte() != null ? item.getTipoTransporte().getCdnfe() : null);
				
				infUnidTransp.setListaInfUnidCarga(createBeanInfUnidCargaCte(mdfe, item));
				infUnidTransp.setListaLacUnidTransp(createBeanLacUnidTranspCte(mdfe, item));
				
				list.add(infUnidTransp);
			}
			
			listaInfUnidTransp.setInfUnidTranspList(list);
		}
		
		return listaInfUnidTransp;
	}
	
	private ListaInfUnidCarga createBeanInfUnidCargaMdfe(Mdfe mdfe, MdfeInformacaoUnidadeTransporteMdfeReferenciado bean) {
		ListaInfUnidCarga listaInfUnidCarga = new ListaInfUnidCarga();
		List<MdfeInformacaoUnidadeCargaMdfeReferenciado> lista = bean.getListaInformacaoUnidadeCargaMdfeReferenciado(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidCarga> list = new ArrayList<InfUnidCarga>();
			InfUnidCarga infUnidCarga;
			
			for (MdfeInformacaoUnidadeCargaMdfeReferenciado item : lista) {
				infUnidCarga = new InfUnidCarga();
				
				infUnidCarga.setIdUnidCarga(item.getIdUnidadeTransporte());
				infUnidCarga.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidCarga.setTpUnidCarga(item.getTipoCarga() != null ? item.getTipoCarga().getCdnfe() : null);
				infUnidCarga.setListaLacUnidCarga(createBeanLacUnidCarga(mdfe, item));
				
				list.add(infUnidCarga);
			}
			
			listaInfUnidCarga.setInfUnidCargaList(list);
		}
		
		return listaInfUnidCarga;
	}
	
	private ListaInfUnidCarga createBeanInfUnidCargaNfe(Mdfe mdfe, MdfeInformacaoUnidadeTransporteNfe bean) {
		ListaInfUnidCarga listaInfUnidCarga = new ListaInfUnidCarga();
		List<MdfeInformacaoUnidadeCargaNfe> lista = bean.getListaInformacaoUnidadeCargaNfe(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidCarga> list = new ArrayList<InfUnidCarga>();
			InfUnidCarga infUnidCarga;
			
			for (MdfeInformacaoUnidadeCargaNfe item : lista) {
				infUnidCarga = new InfUnidCarga();
				
				infUnidCarga.setIdUnidCarga(item.getIdUnidadeTransporte());
				infUnidCarga.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidCarga.setTpUnidCarga(item.getTipoCarga() != null ? item.getTipoCarga().getCdnfe() : null);
				infUnidCarga.setListaLacUnidCarga(createBeanLacUnidCarga(mdfe, item));
				
				list.add(infUnidCarga);
			}
			
			listaInfUnidCarga.setInfUnidCargaList(list);
		}
		
		return listaInfUnidCarga;
	}
	
	private ListaInfUnidCarga createBeanInfUnidCargaCte(Mdfe mdfe, MdfeInformacaoUnidadeTransporteCte infUniTransCte) {
		List<MdfeInformacaoUnidadeCargaCte> lista = infUniTransCte.getListaInformacaoUnidadeCargaCte(infUniTransCte, mdfe);
		ListaInfUnidCarga listaInfUnidCarga = new ListaInfUnidCarga();
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<InfUnidCarga> list = new ArrayList<InfUnidCarga>();
			InfUnidCarga infUnidCarga;
			
			for (MdfeInformacaoUnidadeCargaCte item : lista) {
				infUnidCarga = new InfUnidCarga();
				
				infUnidCarga.setIdUnidCarga(item.getIdUnidadeTransporte());
				infUnidCarga.setQtdRat(item.getQuantidadeRateada() != null ? item.getQuantidadeRateada().getValue().doubleValue() : null);
				infUnidCarga.setTpUnidCarga(item.getTipoCarga() != null ? item.getTipoCarga().getCdnfe() : null);
				infUnidCarga.setListaLacUnidCarga(createBeanLacUnidCarga(mdfe, item));
				
				list.add(infUnidCarga);
			}
			
			listaInfUnidCarga.setInfUnidCargaList(list);
		}
		
		return listaInfUnidCarga;
	}
	
	private ListaLacUnidCarga createBeanLacUnidCarga(Mdfe mdfe, MdfeInformacaoUnidadeCargaCte bean) {
		ListaLacUnidCarga listaLacUnidCarga = new ListaLacUnidCarga();
		List<MdfeLacreUnidadeCargaCte> lista = bean.getListaInformacaoUnidadeCargaCte(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<LacUnidCarga> list = new ArrayList<LacUnidCarga>();
			LacUnidCarga lacUnidCarga;
			
			for (MdfeLacreUnidadeCargaCte mdfeLacreUnidadeCargaCte : lista) {
				lacUnidCarga = new LacUnidCarga();
				lacUnidCarga.setnLacre(mdfeLacreUnidadeCargaCte.getNumeroLacre());
				list.add(lacUnidCarga);
			}
			
			listaLacUnidCarga.setLacUnidCargaList(list);
		}
		return listaLacUnidCarga;
	}
	
	private ListaLacUnidCarga createBeanLacUnidCarga(Mdfe mdfe, MdfeInformacaoUnidadeCargaNfe bean) {
		ListaLacUnidCarga listaLacUnidCarga = new ListaLacUnidCarga();
		List<MdfeLacreUnidadeCargaNfe> lista = bean.getListaLacreUnidadeCargaNfe(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<LacUnidCarga> list = new ArrayList<LacUnidCarga>();
			LacUnidCarga lacUnidCarga;
			
			for (MdfeLacreUnidadeCargaNfe item : lista) {
				lacUnidCarga = new LacUnidCarga();
				lacUnidCarga.setnLacre(item.getNumeroLacre());
				list.add(lacUnidCarga);
			}
			
			listaLacUnidCarga.setLacUnidCargaList(list);
		}
		return listaLacUnidCarga;
	}
	
	private ListaLacUnidCarga createBeanLacUnidCarga(Mdfe mdfe, MdfeInformacaoUnidadeCargaMdfeReferenciado bean) {
		ListaLacUnidCarga listaLacUnidCarga = new ListaLacUnidCarga();
		List<MdfeLacreUnidadeCargaMdfeReferenciado> lista = bean.getListaLacreUnidadeCargaMdfeReferenciado(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<LacUnidCarga> list = new ArrayList<LacUnidCarga>();
			LacUnidCarga lacUnidCarga;
			
			for (MdfeLacreUnidadeCargaMdfeReferenciado item : lista) {
				lacUnidCarga = new LacUnidCarga();
				lacUnidCarga.setnLacre(item.getNumeroLacre());
				list.add(lacUnidCarga);
			}
			
			listaLacUnidCarga.setLacUnidCargaList(list);
		}
		return listaLacUnidCarga;
	}
	
	private ListaLacUnidTransp createBeanLacUnidTranspMdfe(List<MdfeLacreUnidadeTransporteMdfeReferenciado> lista) {
		ListaLacUnidTransp listaLacUnidTransp = new ListaLacUnidTransp();
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<LacUnidTransp> list = new ArrayList<LacUnidTransp>();
			LacUnidTransp lacUnidTransp;
			
			for (MdfeLacreUnidadeTransporteMdfeReferenciado mdfeLacreUnidadeTransporteMdfeReferenciado : lista) {
				lacUnidTransp = new LacUnidTransp();
				
				lacUnidTransp.setnLacre(mdfeLacreUnidadeTransporteMdfeReferenciado.getNumeroLacre());
				
				list.add(lacUnidTransp);
			}
			
			listaLacUnidTransp.setLacUnidTranspList(list);
		}
		
		return listaLacUnidTransp;
	}
	
	private ListaLacUnidTransp createBeanLacUnidTranspNfe(Mdfe mdfe, MdfeInformacaoUnidadeCargaNfe bean) {
		ListaLacUnidTransp listaLacUnidTransp = new ListaLacUnidTransp();
		List<MdfeLacreUnidadeCargaNfe> lista = bean.getListaLacreUnidadeCargaNfe(bean, mdfe);
		if(SinedUtil.isListNotEmpty(lista)){
			List<LacUnidTransp> list = new ArrayList<LacUnidTransp>();
			LacUnidTransp lacUnidTransp;
			for (MdfeLacreUnidadeCargaNfe mdfeLacreUnidadeTransporteNfe : lista) {
				lacUnidTransp = new LacUnidTransp();
				lacUnidTransp.setnLacre(mdfeLacreUnidadeTransporteNfe.getNumeroLacre());
				list.add(lacUnidTransp);
			}
			
			listaLacUnidTransp.setLacUnidTranspList(list);
		}
		
		return listaLacUnidTransp;
	}
	
	private ListaLacUnidTransp createBeanLacUnidTranspNfe(Mdfe mdfe, MdfeInformacaoUnidadeTransporteNfe bean) {
		ListaLacUnidTransp listaLacUnidTransp = new ListaLacUnidTransp();
		List<MdfeLacreUnidadeTransporteNfe> lista = bean.getListaLacreUnidadeTransporteNfe(bean, mdfe);
		if(SinedUtil.isListNotEmpty(lista)){
			List<LacUnidTransp> list = new ArrayList<LacUnidTransp>();
			LacUnidTransp lacUnidTransp;
			for (MdfeLacreUnidadeTransporteNfe item : lista) {
				lacUnidTransp = new LacUnidTransp();
				lacUnidTransp.setnLacre(item.getNumeroLacre());
				list.add(lacUnidTransp);
			}
			
			listaLacUnidTransp.setLacUnidTranspList(list);
		}
		
		return listaLacUnidTransp;
	}
	
	private ListaLacUnidTransp createBeanLacUnidTranspMdfeReferenciado(Mdfe mdfe, MdfeInformacaoUnidadeTransporteMdfeReferenciado bean) {
		ListaLacUnidTransp listaLacUnidTransp = new ListaLacUnidTransp();
		List<MdfeLacreUnidadeTransporteMdfeReferenciado> lista = bean.getListaLacreUnidadeTransporteMdfeReferenciado(bean, mdfe);
		if(SinedUtil.isListNotEmpty(lista)){
			List<LacUnidTransp> list = new ArrayList<LacUnidTransp>();
			LacUnidTransp lacUnidTransp;
			for (MdfeLacreUnidadeTransporteMdfeReferenciado item : lista) {
				lacUnidTransp = new LacUnidTransp();
				lacUnidTransp.setnLacre(item.getNumeroLacre());
				list.add(lacUnidTransp);
			}
			
			listaLacUnidTransp.setLacUnidTranspList(list);
		}
		
		return listaLacUnidTransp;
	}
	
	private ListaLacUnidTransp createBeanLacUnidTranspCte(Mdfe mdfe, MdfeInformacaoUnidadeTransporteCte bean) {
		ListaLacUnidTransp listaLacUnidTransp = new ListaLacUnidTransp();
		List<MdfeLacreUnidadeTransporteCte> lista = bean.getListaLacreUnidadeTransporteCte(bean, mdfe);
		if(SinedUtil.isListNotEmpty(lista)) {
			List<LacUnidTransp> list = new ArrayList<LacUnidTransp>();
			LacUnidTransp lacUnidTransp;
			
			for (MdfeLacreUnidadeTransporteCte item : lista) {
				lacUnidTransp = new LacUnidTransp();
				lacUnidTransp.setnLacre(item.getNumeroLacre());
				list.add(lacUnidTransp);
			}
			
			listaLacUnidTransp.setLacUnidTranspList(list);
		}
		return listaLacUnidTransp;
	}

	private ListaPeri createBeanPeriMdfeReferenciado(Mdfe mdfe, MdfeReferenciado bean) {
		ListaPeri listaPeri = new ListaPeri();
		List<MdfeProdutoPerigosoMdfeReferenciado> lista = bean.getListaProdutoPerigosoMdfeReferenciado(bean, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<Peri> list = new ArrayList<Peri>();
			Peri peri;
			
			for (MdfeProdutoPerigosoMdfeReferenciado mdfeProdutoPerigosoMdfeReferenciado : lista) {
				peri = new Peri();
				
				peri.setGrEmb(mdfeProdutoPerigosoMdfeReferenciado.getGrupoEmbalagem());
				peri.setnOnu(mdfeProdutoPerigosoMdfeReferenciado.getNumeroOnuUn());
				peri.setqTotProd(mdfeProdutoPerigosoMdfeReferenciado.getQuantidadeTotalPorProdutos());
				peri.setqVolTipo(mdfeProdutoPerigosoMdfeReferenciado.getQuantidadeAndTipoPorVolume());
				peri.setxClaRisco(mdfeProdutoPerigosoMdfeReferenciado.getClasse());
				peri.setxNomeAe(mdfeProdutoPerigosoMdfeReferenciado.getNomeApropriadoEmbarque());
				
				list.add(peri);
			}
			
			listaPeri.setPeriList(list);
		}
		
		return listaPeri;
	}
	
	private ListaPeri createBeanPeriNfe(Mdfe mdfe, MdfeNfe mdfeNfe) {
		ListaPeri listaPeri = new ListaPeri();
		List<MdfeProdutoPerigosoNfe> lista = mdfeNfe.getListaProdutoPerigosoNfe(mdfeNfe, mdfe);
		
		if(SinedUtil.isListNotEmpty(lista)) {
			List<Peri> list = new ArrayList<Peri>();
			Peri peri;
			
			for (MdfeProdutoPerigosoNfe mdfeProdutoPerigosoNfe : lista) {
				peri = new Peri();
				
				peri.setGrEmb(mdfeProdutoPerigosoNfe.getGrupoEmbalagem());
				peri.setnOnu(mdfeProdutoPerigosoNfe.getNumeroOnuUn());
				peri.setqTotProd(mdfeProdutoPerigosoNfe.getQuantidadeTotalPorProdutos());
				peri.setqVolTipo(mdfeProdutoPerigosoNfe.getQuantidadeAndTipoPorVolume());
				peri.setxClaRisco(mdfeProdutoPerigosoNfe.getClasse());
				peri.setxNomeAe(mdfeProdutoPerigosoNfe.getNomeApropriadoEmbarque());
				
				list.add(peri);
			}
			
			listaPeri.setPeriList(list);
		}
		
		return listaPeri;
	}
	
	private ListaPeri createBeanPeriCte(Mdfe mdfe, MdfeCte mdfeCte) {
		ListaPeri listaPeri = new ListaPeri();
		List<MdfeProdutoPerigosoCte> lista = mdfeCte.getListaProdutoPerigosoCte(mdfeCte, mdfe);
		if(SinedUtil.isListNotEmpty(lista)) {
			List<Peri> list = new ArrayList<Peri>();
			Peri peri;
			
			for (MdfeProdutoPerigosoCte mdfeProdutoPerigosoCte : lista) {
				peri = new Peri();
				
				peri.setGrEmb(mdfeProdutoPerigosoCte.getGrupoEmbalagem());
				peri.setnOnu(mdfeProdutoPerigosoCte.getNumeroOnuUn());
				peri.setqTotProd(mdfeProdutoPerigosoCte.getQuantidadeTotalPorProdutos());
				peri.setqVolTipo(mdfeProdutoPerigosoCte.getQuantidadeAndTipoPorVolume());
				peri.setxClaRisco(mdfeProdutoPerigosoCte.getClasse());
				peri.setxNomeAe(mdfeProdutoPerigosoCte.getNomeApropriadoEmbarque());
				
				list.add(peri);
			}
			
			listaPeri.setPeriList(list);
		}
		
		return listaPeri;
	}
	
	private ListaInfMunDescarga createBeanInfMunDescarga(List<Municipio> lista, Mdfe mdfe, HashMap<Municipio, List<MdfeCte>> mapMunicipioMdfeCte, HashMap<Municipio, List<MdfeNfe>> mapMunicipioMdfeNfe,	HashMap<Municipio, List<MdfeReferenciado>> mapMunicipioMdfeReferenciado) {
		ListaInfMunDescarga listaInfMunDescarga = new ListaInfMunDescarga();
		if(SinedUtil.isListNotEmpty(lista)){
			List<InfMunDescarga> list = new ArrayList<InfMunDescarga>();
			InfMunDescarga infMunDescarga;
			for(Municipio bean : lista){
				if (bean != null) {
					infMunDescarga = new InfMunDescarga();
					infMunDescarga.setcMunDescarga(bean.getCdibge());
					infMunDescarga.setxMunDescarga(bean.getNome());
					infMunDescarga.setListaInfCte(createBeanInfCte(mdfe, mapMunicipioMdfeCte.get(bean)));
					infMunDescarga.setListaInfNfe(createBeanInfNfe(mdfe, mapMunicipioMdfeNfe.get(bean)));
					infMunDescarga.setListaInfMdfeTransp(createBeanInfMdfeReferenciado(mdfe, mapMunicipioMdfeReferenciado.get(bean)));
					list.add(infMunDescarga);
				}
			}
			listaInfMunDescarga.setInfMunDescargaList(list);
		}
		return listaInfMunDescarga;
	}
	
	private ListaSeg createBeanSeg(Mdfe mdfe) {
		List<MdfeSeguroCarga> mdfeSeguroCargaList = mdfe.getListaSeguroCarga();
		List<MdfeSeguroCargaNumeroAverbacao> mdfeSeguroCargaNumeroAverbacaoList = mdfe.getListaSeguroCargaNumeroAverbacao();
		ListaSeg listaSeg = new ListaSeg();
		
		if(SinedUtil.isListNotEmpty(mdfeSeguroCargaList)) {
			List<Seg> list = new ArrayList<Seg>();
			Seg seg = null;
			InfResp infResp = null;
			InfSeg infSeg = null;
			
			for (MdfeSeguroCarga mdfeSeguroCarga : mdfeSeguroCargaList) {
				seg = new Seg();
				if (mdfeSeguroCarga.getResponsavelSeguro() != null) {
					infResp = new InfResp();
					
					infResp.setRespSeg(mdfeSeguroCarga.getResponsavelSeguro().getCdnfe());
					infResp.setCnpj(mdfeSeguroCarga.getCnpj() != null ? mdfeSeguroCarga.getCnpj().getValue() : null);
					infResp.setCpf(mdfeSeguroCarga.getCpf() != null ? mdfeSeguroCarga.getCpf().getValue() : null);
					
					seg.setInfResp(infResp);
				}

				infSeg = new InfSeg();
				infSeg.setCnpj(mdfeSeguroCarga.getCnpjSeguradora() != null ? mdfeSeguroCarga.getCnpjSeguradora().getValue() : null);
				infSeg.setxSeg(mdfeSeguroCarga.getNomeSeguradora());
				
				seg.setInfSeg(infSeg);
				
				seg.setnApol(mdfeSeguroCarga.getNumeroApolice());
				
				seg.setListNAver(createBeanNAver(mdfeSeguroCargaNumeroAverbacaoList));
				list.add(seg);
			}
			
			listaSeg.setSegList(list);
		}
		
		return listaSeg;
	}

	private ListNAver createBeanNAver(List<MdfeSeguroCargaNumeroAverbacao> lista) {
		ListNAver listNAver = new ListNAver();
		
		if(SinedUtil.isListNotEmpty(lista)){
			List<NAver> list = new ArrayList<NAver>();
			NAver nAver;
			
			for(MdfeSeguroCargaNumeroAverbacao bean : lista){
				nAver = new NAver();
				
				nAver.setnAver(bean.getNumeroAverbacao());
				
				list.add(nAver);
			}
			
			listNAver.setNAverList(list);
		}
		
		return listNAver;
	}

	@SuppressWarnings("unchecked")
	public Arquivo getArquivoxmlDistribuicao(Mdfe mdfe) throws JDOMException, IOException {
		ArquivoMdfe arquivomdfe = this.loadByMdfe(mdfe, ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO);
		
		Arquivo arquivoenvio = arquivoService.loadWithContents(arquivomdfe.getArquivoxmlassinado());
		Arquivo arquivoretorno = arquivoService.loadWithContents(arquivomdfe.getArquivoretornoconsulta());
		
		SAXBuilder sb = new SAXBuilder();  
		Document d_envio = sb.build(new ByteArrayInputStream(arquivoenvio.getContent()));
		Document d_retorno = sb.build(new ByteArrayInputStream(arquivoretorno.getContent()));
		
		Element rootElement_envio = d_envio.getRootElement();
		Element mdfeElement = SinedUtil.getChildElement("MDFe", rootElement_envio.cloneContent());
		Document docMDFe = new Document(mdfeElement);
		String mdfeString = new XMLOutputter().outputString(docMDFe);
		
		Element rootElement_retorno = d_retorno.getRootElement();
		Element protMDFeElement = SinedUtil.getChildElement("protMDFe", rootElement_retorno.cloneContent());
		Document docProtMDFe = new Document(protMDFeElement);
		String protMDFeString = new XMLOutputter().outputString(docProtMDFe);
		
		mdfeString = mdfeString.replaceAll("<\\?.+\\?>", "");
		protMDFeString = protMDFeString.replaceAll("<\\?.+\\?>", "");
		
		String s = 
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<mdfeProc versao=\"3.00\" xmlns=\"http://www.portalfiscal.inf.br/mdfe\">" +
					mdfeString + 
					protMDFeString +
				"</mdfeProc>";
		
		return new Arquivo(s.getBytes(), arquivomdfe.getProtocolomdfe() + "-procMDFe.xml", "text/xml");
	}

	public EventoMdfe createBeanEventoMdfe(EventoBean eventoBean, Integer contadorevento, String id, Configuracaonfe configuracaonfe,
			boolean cancelamento, boolean encerramento, boolean inclusaoCondutor, String motivo, String tpEvento) {
		boolean ambienteProducao = !configuracaonfe.getPrefixowebservice().name().endsWith("_HOM");
		Timestamp atual = new Timestamp(System.currentTimeMillis());
		
		EventoMdfe eventoMdfe =  new EventoMdfe();
		eventoMdfe.setVersao(LkMdfeUtil.versao);
		
		InfEvento infEvento = new InfEvento();
		eventoMdfe.setInfEvento(infEvento);
		
		DetEvento detEvento = new DetEvento();
		
		String descEvento = "";
		
		if(cancelamento) {
			descEvento = "Cancelamento";
		}else if(encerramento){
			descEvento = "Encerramento";
			detEvento.setDtEnc(LkMdfeUtil.FORMATADOR_DATA.format(new Date(atual.getTime())));
		}else if(inclusaoCondutor){
			descEvento = "Inclusao Condutor";
		}
		
		String cOrgao = configuracaonfe.getUf().getCdibge() + "";
		
		Integer nSeqEvento = 1;
		
		infEvento.setId(id);
		infEvento.setcOrgao(cOrgao);
		infEvento.setTpAmb(ambienteProducao ? 1 : 2);
		infEvento.setCnpj(eventoBean.getEmpresa() != null ? eventoBean.getEmpresa().getCnpj().getValue() : "");
//		infEvento.setcPF(cPF);
		infEvento.setChMdfe(eventoBean.getArquivoMdfe().getChaveacesso());
		infEvento.setDhEvento(atual);
		infEvento.setTpEvento(tpEvento);
		infEvento.setnSeqEvento(nSeqEvento);
		
		infEvento.setDetEvento(detEvento);
		
		detEvento.setCancelamento(cancelamento);
		detEvento.setEncerramento(encerramento);
		detEvento.setInclusaoCondutor(inclusaoCondutor);
		detEvento.setVersaoEvento(LkMdfeUtil.versao);
		detEvento.setcUf(eventoBean.getUf() != null ? eventoBean.getUf().getCdibge() : null);
		detEvento.setcMun(eventoBean.getMunicipio() != null ? Integer.parseInt(eventoBean.getMunicipio().getCdibge()) : null);
		
		String justificativa = motivo;
		if(justificativa != null){
			justificativa = justificativa.replaceAll("\\r?\\n", " ");
			justificativa = addScape(justificativa);
			justificativa = justificativa.trim();
		}
		
		detEvento.setDescEvento(descEvento);
		detEvento.setnProt(eventoBean.getArquivoMdfe().getProtocolomdfe());
		detEvento.setxJust(justificativa);
		
		Condutor condutor = new Condutor();
		condutor.setxNome(eventoBean.getNome());
		condutor.setCpf(eventoBean.getCpf() != null ? eventoBean.getCpf().getValue() : null);
		
		detEvento.setCondutor(condutor);
		
		return eventoMdfe;
	}
	
	private String addScape(String string){
		if(string == null) return null;
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			switch (string.charAt(i)) {
			case '"' :
				stringBuilder.append("&quot;");
				break;
			case '\'' :
				stringBuilder.append("&#39;");
				break;
			case '<' :
				stringBuilder.append("&lt;");
				break;
			case '>' :
				stringBuilder.append("&gt;");
				break;
			case '&' :
				stringBuilder.append("&amp;");
				break;
			default :
				stringBuilder.append(string.charAt(i));
			}
		}
		
		return Util.strings.tiraAcento(stringBuilder.toString().replace("\n", " ").replace("\r", " ")).trim();
	}

	@SuppressWarnings("unchecked")
	public void processaRetornoEncerramento(ArquivoMdfe arquivoMdfe, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Mdfe mdfe = arquivoMdfe.getMdfe();
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element infEvento = SinedUtil.getChildElement("infEvento", rootElement.getContent());
			Element xMotivo = SinedUtil.getChildElement("xMotivo", infEvento.getContent());
			Element cStat = SinedUtil.getChildElement("cStat", infEvento.getContent());;
			
			if(xMotivo != null && !cStat.getValue().equals("135")){
				String strMsg = xMotivo.getValue();
				if(strMsg != null && !strMsg.equals("")){
					ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
					arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
					arquivoMdfeErro.setMensagem(strMsg);
					arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
				}
				
				if(mdfe.getMdfeSituacao() == null || !mdfe.getMdfeSituacao().equals(MdfeSituacao.ENCERRADO)){
					mdfe.setMdfeSituacao(MdfeSituacao.AUTORIZADO);
					String obs = "Encerramento na Receita n�o efetuado.";
					mdfeService.alterarStatusAcao(mdfe, obs);
				}
				
			} else if(cStat.getValue().equals("135")){
				String obs = "Encerramento registrado na Receita";
				mdfe.setMdfeSituacao(MdfeSituacao.ENCERRADO);
				mdfeService.alterarStatusAcao(mdfe, obs);
				
			} else throw new SinedException("Erro no processamento do retorno do encerramento.");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoCancelamento(ArquivoMdfe arquivoMdfe, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Mdfe mdfe = arquivoMdfe.getMdfe();
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element infEvento = SinedUtil.getChildElement("infEvento", rootElement.getContent());
			Element xMotivo = SinedUtil.getChildElement("xMotivo", infEvento.getContent());
			Element cStat = SinedUtil.getChildElement("cStat", infEvento.getContent());;
			
			if(xMotivo != null && !cStat.getValue().equals("135")){
				String strMsg = xMotivo.getValue();
				if(strMsg != null && !strMsg.equals("")){
					ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
					arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
					arquivoMdfeErro.setMensagem(strMsg);
					arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
				}
				
				if(mdfe.getMdfeSituacao() == null || !mdfe.getMdfeSituacao().equals(MdfeSituacao.CANCELADO)){
					mdfe.setMdfeSituacao(MdfeSituacao.AUTORIZADO);
					String obs = "Cancelamento na Receita n�o efetuado.";
					mdfeService.alterarStatusAcao(mdfe, obs);
				}
				
			} else if(cStat.getValue().equals("135")){
				String obs = "Cancelamento registrado na Receita";
				mdfe.setMdfeSituacao(MdfeSituacao.CANCELADO);
				mdfeService.alterarStatusAcao(mdfe, obs);
				
			} else throw new SinedException("Erro no processamento do retorno do cancelamento.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoInclusaoCondutor(ArquivoMdfe arquivoMdfe, byte[] content, Prefixowebservice prefixowebservice) {
		try {
			Mdfe mdfe = arquivoMdfe.getMdfe();
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element infEvento = SinedUtil.getChildElement("infEvento", rootElement.getContent());
			Element xMotivo = SinedUtil.getChildElement("xMotivo", infEvento.getContent());
			Element cStat = SinedUtil.getChildElement("cStat", infEvento.getContent());;
			
			if(xMotivo != null && !cStat.getValue().equals("135")){
				String strMsg = xMotivo.getValue();
				if(strMsg != null && !strMsg.equals("")){
					ArquivoMdfeErro arquivoMdfeErro = new ArquivoMdfeErro();
					arquivoMdfeErro.setArquivoMdfe(arquivoMdfe);
					arquivoMdfeErro.setMensagem(strMsg);
					arquivoMdfeErro.setData(new Timestamp(System.currentTimeMillis()));
					
					arquivoMdfeErroService.saveOrUpdate(arquivoMdfeErro);
				}
				
				if(mdfe.getMdfeSituacao() == null || !mdfe.getMdfeSituacao().equals(MdfeSituacao.AUTORIZADO)){
					mdfe.setMdfeSituacao(MdfeSituacao.AUTORIZADO);
					String obs = "Inclus�o do Condutor na Receita n�o efetuado.";
					mdfeService.alterarStatusAcao(mdfe, obs);
				}
				
			} else if(cStat.getValue().equals("135")){
				String obs = "Inclus�o do Condutor registrado na Receita";
				mdfe.setMdfeSituacao(MdfeSituacao.AUTORIZADO);
				mdfeService.alterarStatusAcao(mdfe, obs);
				
			} else throw new SinedException("Erro no processamento do retorno do inclus�o do condutor.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void desmarcarArquivoMdfe(Integer cdArquivoMdfe, String flag) {
		arquivoMdfeDAO.desmarcarArquivoMdfe(cdArquivoMdfe, flag);
	}

	public void createArquivoXmlConsultaLote(ArquivoMdfe arquivoMdfe) throws Exception {
		Arquivo arquivo = arquivoService.loadWithContents(arquivoMdfe.getArquivoretornoenvio());
		Element retEnviMdfe = SinedUtil.getRootElementXML(arquivo.getContent());
		Element infRec = SinedUtil.getChildElement("infRec", retEnviMdfe.getContent());
		Element nRec = SinedUtil.getChildElement("nRec", infRec.getContent());
		
		String nRecStr = nRec != null ? nRec.getValue() : "";
		
		ConsReciMdfe consReciMdfe = this.createXmlConsultaLoteMdfe(nRecStr, arquivoMdfe.getConfiguracaonfe());
		String stringXmlConsultaLote = Util.strings.tiraAcento(consReciMdfe.toString());
		Arquivo arquivoXmlConsultaLote = new Arquivo(stringXmlConsultaLote.getBytes(), "nfconsultalote_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		
		arquivoMdfe.setArquivoxmlconsultalote(arquivoXmlConsultaLote);
		
		arquivoDAO.saveFile(arquivoMdfe, "arquivoxmlconsultalote");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		arquivoMdfeDAO.updateConsultando(arquivoMdfe, Boolean.TRUE);
		this.updateArquivoxmlconsultalote(arquivoMdfe);
	}
}