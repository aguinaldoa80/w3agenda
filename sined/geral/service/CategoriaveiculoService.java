package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.dao.CategoriaveiculoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CategoriaveiculoService extends GenericService<Categoriaveiculo> {
	
	private CategoriaveiculoDAO categoriaveiculoDAO;
	
	public void setCategoriaveiculoDAO(CategoriaveiculoDAO categoriaveiculoDAO) {
		this.categoriaveiculoDAO = categoriaveiculoDAO;
	}

	public List<Categoriaveiculo> loadCategoriaveiculoWithListaItemInspecao(String whereIn, String orderBy, Boolean asc){
		return categoriaveiculoDAO.loadCategoriaveiculoWithListaItemInspecao(whereIn, orderBy, asc);
	}
	
}
