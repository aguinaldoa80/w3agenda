package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipoaviso;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoavisoService extends GenericService<Tipoaviso> {

	public List<Tipoaviso> getTipoAvisoByMotivoaviso(){
		List<Tipoaviso> lista = new ArrayList<Tipoaviso>();
		lista.add(Tipoaviso.NIVEL);
		lista.add(Tipoaviso.USUARIO);
		return lista;
	}
}