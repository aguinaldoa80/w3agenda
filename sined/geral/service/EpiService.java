package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.dao.EpiDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EpiService extends GenericService<Epi> {

	private EpiDAO epiDAO;
	
	public void setEpiDAO(EpiDAO epiDAO) {
		this.epiDAO = epiDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#deleteEpi(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteEpi(Material material) {
		epiDAO.deleteEpi(material);
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#insertEpi(Epi)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void insertEpi(Epi epi) {
		epiDAO.insertEpi(epi);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#updateEpi(Epi)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateEpi(Epi epi) {
		epiDAO.updateEpi(epi);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#carregaEpi(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Epi carregaEpi(Material material){
		return epiDAO.carregaEpi(material);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#countEpi(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Long countEpi(Material material){
		return epiDAO.countEpi(material);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#verificaEpi
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaEpi(Material bean){
		return epiDAO.verificaEpi(bean);
	}

	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#findAllEpi
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Epi> findAllEpi(){
		return epiDAO.findAllEpi();		
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EpiDAO#findAllEpisAtivos
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Epi> findAllEpisAtivos() {
		return epiDAO.findAllEpisAtivos();
	}

}
