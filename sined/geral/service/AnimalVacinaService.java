package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.AnimalVacina;
import br.com.linkcom.sined.geral.dao.AnimalVacinaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AnimalVacinaService extends GenericService<AnimalVacina> {
	
	private AnimalVacinaDAO animalVacinaDAO;
	
	public void setAnimalVacinaDAO(AnimalVacinaDAO animalVacinaDAO) { this.animalVacinaDAO = animalVacinaDAO; }
	
	/* singleton */
	private static AnimalVacinaService instance;
	public static AnimalVacinaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AnimalVacinaService.class);
		}
		return instance;
	}
	
	/**
	 * Metodo com refer�ncia no DAO
	 * @param proximaVacina
	 * @return
	 * @since 28/04/2016
	 * @author C�sar
	 */
	public List<AnimalVacina> findForDataVacina(Date proximaVacina){
		return animalVacinaDAO.findForDataVacina(proximaVacina);
	}
	
}
