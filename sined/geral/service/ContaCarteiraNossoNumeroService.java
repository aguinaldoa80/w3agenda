package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.ContaCarteiraNossoNumero;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.dao.ContaCarteiraNossoNumeroDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContaCarteiraNossoNumeroService extends GenericService<ContaCarteiraNossoNumero>{
	private ContaCarteiraNossoNumeroDAO contaCarteiraNossoNumeroDAO;
	
	public void setContaCarteiraNossoNumeroDAO(ContaCarteiraNossoNumeroDAO contaCarteiraNossoNumeroDAO) {this.contaCarteiraNossoNumeroDAO = contaCarteiraNossoNumeroDAO;}
	
	public ContaCarteiraNossoNumero findByContacarteira(Contacarteira contacarteira){
		return contaCarteiraNossoNumeroDAO.findByContacarteira(contacarteira);
	}

	public void updateContaCarteiraNossoNumero(ContaCarteiraNossoNumero contaCarteiraNossoNumero, Integer nossoNumeroUltimo) {
		contaCarteiraNossoNumeroDAO.updateContaCarteiraNossoNumero(contaCarteiraNossoNumero, nossoNumeroUltimo);	
	}

}
