package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.geral.dao.CotacaofornecedoritemsolicitacaocompraDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CotacaofornecedoritemsolicitacaocompraService extends GenericService<Cotacaofornecedoritemsolicitacaocompra> {
	
	private CotacaofornecedoritemsolicitacaocompraDAO cotacaofornecedoritemsolicitacaocompraDAO;

	public void setCotacaofornecedoritemsolicitacaocompraDAO(
			CotacaofornecedoritemsolicitacaocompraDAO cotacaofornecedoritemsolicitacaocompraDAO) {
		this.cotacaofornecedoritemsolicitacaocompraDAO = cotacaofornecedoritemsolicitacaocompraDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cotacaofornecedoritem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/05/2015
	 */
	public List<Cotacaofornecedoritemsolicitacaocompra> findByCotacaofornecedoritem(Cotacaofornecedoritem cotacaofornecedoritem) {
		return cotacaofornecedoritemsolicitacaocompraDAO.findByCotacaofornecedoritem(cotacaofornecedoritem);
	}

}
