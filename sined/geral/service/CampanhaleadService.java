package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhalead;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.dao.CampanhaleadDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CampanhaleadService extends GenericService<Campanhalead>{
	
	protected CampanhaleadDAO campanhaleadDAO;
	
	public void setCampanhaleadDAO(CampanhaleadDAO campanhaleadDAO) {
		this.campanhaleadDAO = campanhaleadDAO;
	}
	
	
	
	public List<Campanhalead> findByCampanha(Campanha campanha){
		return campanhaleadDAO.findByCampanha(campanha);
	}
	
	public List<Campanhalead> loadByLead(Lead lead){
		return campanhaleadDAO.loadByLead(lead);
	}

}
