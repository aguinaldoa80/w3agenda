package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Empresamodelocomprovanteorcamento;
import br.com.linkcom.sined.geral.dao.EmpresamodelocomprovanteorcamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresamodelocomprovanteorcamentoService extends GenericService<Empresamodelocomprovanteorcamento>{

	private EmpresamodelocomprovanteorcamentoDAO empresamodelocomprovanteorcamentoDAO;
	
	private static EmpresamodelocomprovanteorcamentoService instance;
	public static EmpresamodelocomprovanteorcamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EmpresamodelocomprovanteorcamentoService.class);
		}
		return instance;
	}
	
	public void setEmpresamodelocomprovanteorcamentoDAO(
			EmpresamodelocomprovanteorcamentoDAO empresamodelocomprovanteorcamentoDAO) {
		this.empresamodelocomprovanteorcamentoDAO = empresamodelocomprovanteorcamentoDAO;
	}
	
	public Empresamodelocomprovanteorcamento loadWithArquivo(Empresamodelocomprovanteorcamento bean){
		return empresamodelocomprovanteorcamentoDAO.loadWithArquivo(bean);
	}
}
