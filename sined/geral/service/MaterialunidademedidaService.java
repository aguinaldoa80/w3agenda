package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialunidademedidaVO;
import br.com.linkcom.sined.geral.dao.MaterialunidademedidaDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.UnidadeMedidaWSBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialunidademedida.MaterialunidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialunidademedida.MaterialUnidademedidaW3producaoRESTModel;

public class MaterialunidademedidaService extends GenericService<Materialunidademedida> {

	private MaterialunidademedidaDAO materialunidademedidaDAO;
	private MaterialService materialService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	
	public void setMaterialunidademedidaDAO(MaterialunidademedidaDAO materialunidademedidaDAO) {
		this.materialunidademedidaDAO = materialunidademedidaDAO;
	}
		
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	
	/* singleton */
	private static MaterialunidademedidaService instance;
	public static MaterialunidademedidaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialunidademedidaService.class);
		}
		return instance;
	}

	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Materialunidademedida> findForAtualizarmaterial(String whereIn){
		return materialunidademedidaDAO.findForAtualizarmaterial(whereIn);
	}
	
	/**
	 * 
	 * @param materialunidademedida
	 * @author Thiago Clemente
	 * 
	 */
	public void updateValorunitario(Materialunidademedida materialunidademedida){
		materialunidademedidaDAO.updateValorunitario(materialunidademedida);
	}	
	
	/**
	 * M�todo que retorna uma lista com os totais das unidades de medidas dos produtos buscados.
	 * 
	 * @param whereIn
	 * @param mapaMateriais
	 * @return
	 */
	public List<MaterialunidademedidaVO> getTotaisUnidadeMedida (String whereIn, Map<Integer, Double> mapaMateriais){		
		List<MaterialunidademedidaVO> listaMaterialunidademedidaVO = new ArrayList<MaterialunidademedidaVO>();
		if(whereIn!=null && !whereIn.isEmpty()){
			List<Material> listaMateriaisComUnidademedidaconversao = materialService.findForComUnidademedidaconversao(whereIn, true);
			Map<Integer, MaterialunidademedidaVO> mapaTotais = new HashMap<Integer, MaterialunidademedidaVO>();
			MaterialunidademedidaVO materialunidademedidaVO = new MaterialunidademedidaVO();
			Integer cdunidademedida = null;
			String descricao = null;
			if(listaMateriaisComUnidademedidaconversao != null && !listaMateriaisComUnidademedidaconversao.isEmpty()){
				for (Material material : listaMateriaisComUnidademedidaconversao) {
					Double qtdeMaterial = mapaMateriais.get(material.getCdmaterial());
					if(qtdeMaterial == null) continue;
					
					cdunidademedida = material.getUnidademedida().getCdunidademedida();
					descricao = material.getUnidademedida().getNome();
					if(cdunidademedida!=null && mapaTotais.containsKey(cdunidademedida)){
						materialunidademedidaVO = mapaTotais.get(cdunidademedida);
						materialunidademedidaVO.setDescricao(descricao);
						materialunidademedidaVO.setQtde(materialunidademedidaVO.getQtde()+qtdeMaterial);
						materialunidademedidaVO.setTotal(materialunidademedidaVO.getTotal()+qtdeMaterial);							
					}else if(cdunidademedida!=null){
						materialunidademedidaVO = new MaterialunidademedidaVO();
						materialunidademedidaVO.setDescricao(descricao);
						materialunidademedidaVO.setQtde(qtdeMaterial);
						materialunidademedidaVO.setTotal(materialunidademedidaVO.getQtde());
						mapaTotais.put(cdunidademedida, materialunidademedidaVO);
					}
					
					if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
						for (Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()) {						
							if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao()){
								cdunidademedida = materialunidademedida.getUnidademedida().getCdunidademedida();
								descricao = materialunidademedida.getUnidademedida().getNome();
								if(cdunidademedida!=null && mapaTotais.containsKey(cdunidademedida)){
									materialunidademedidaVO = mapaTotais.get(cdunidademedida);
									materialunidademedidaVO.setDescricao(descricao);
//									materialunidademedidaVO.setQtde(materialunidademedidaVO.getQtde()+qtdeMaterial);
									materialunidademedidaVO.setTotal(materialunidademedidaVO.getTotal()+(materialunidademedida.getFracaoQtdereferencia() * qtdeMaterial));							
								}else if(cdunidademedida!=null){
									materialunidademedidaVO = new MaterialunidademedidaVO();
									materialunidademedidaVO.setDescricao(descricao);
									materialunidademedidaVO.setQtde(qtdeMaterial);
									materialunidademedidaVO.setTotal(materialunidademedida.getFracaoQtdereferencia() * materialunidademedidaVO.getQtde());
									mapaTotais.put(cdunidademedida, materialunidademedidaVO);
								}
							}
						}
					}
				}
			}	
			for (MaterialunidademedidaVO mum : mapaTotais.values()) {
				listaMaterialunidademedidaVO.add(mum);
			}
		}
		return listaMaterialunidademedidaVO;
	}
	
	/**
	 * M�todo que retorna o item correspondente a unidade de convers�o
	 *
	 * @param material
	 * @param unidademedida
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialunidademedida getMaterialunidademedida(Material material, Unidademedida unidademedida){
		Materialunidademedida materialunidademedida = null;
		
		if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()&& 
				unidademedida != null){
			for(Materialunidademedida bean : material.getListaMaterialunidademedida()){
				if(bean.getUnidademedida() != null && bean.getUnidademedida().equals(unidademedida)){
					materialunidademedida = bean;
					break;
				}
			}
		}
		
		return materialunidademedida;
	}

	/**
	 * Busca todas as unidades de medida de convers�o do material.
	 * 
	 * @param produto
	 * @return
	 * 
	 * @author Giovane Freitas 
	 */
	public List<Materialunidademedida> findByMaterial(Material material) {
		return materialunidademedidaDAO.findByMaterial(material);
	}

	public List<MaterialunidademedidaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialunidademedidaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialunidademedidaRESTModel> lista = new ArrayList<MaterialunidademedidaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialunidademedida bean : materialunidademedidaDAO.findForAndroid(whereIn))
				lista.add(new MaterialunidademedidaRESTModel(bean));
		}
		
		return lista;
	}
	
	public List<MaterialUnidademedidaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	public List<MaterialUnidademedidaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialUnidademedidaW3producaoRESTModel> lista = new ArrayList<MaterialUnidademedidaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialunidademedida ms : materialunidademedidaDAO.findForW3Producao(whereIn))
				lista.add(new MaterialUnidademedidaW3producaoRESTModel(ms));
		}
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Materialunidademedida> findByMaterialForSincronizacao(String whereIn){
		return materialunidademedidaDAO.findByMaterialForSincronizacao(whereIn);
	}

	/**
	* M�todo que valida se as convers�es est�o iguais entre as unidades de medida
	*
	* @param bean
	* @param errors
	* @return
	* @since 12/04/2016
	* @author Luiz Fernando
	*/
	public boolean validaConversoesUnidade(Material bean, BindException errors) {
		if(bean.getUnidademedida() != null && SinedUtil.isListNotEmpty(bean.getListaMaterialunidademedida())){
			List<Unidademedidaconversao> list;
			for(Materialunidademedida materialunidademedida : bean.getListaMaterialunidademedida()){
				
				list = unidademedidaconversaoService.findForValidacaoConversao(bean.getUnidademedida(), materialunidademedida.getUnidademedida());
				if(SinedUtil.isListNotEmpty(list)){
					for(Unidademedidaconversao item : list){
						Double fracao = item.getFracao();
						Double qtdereferencia = item.getQtdereferencia();
						if(materialunidademedida.getFracaoQtdereferencia().compareTo(qtdereferencia / fracao) != 0){
							errors.reject("001", "Existe convers�o divergente relacionado a unidade de medida " + item.getUnidademedidarelacionada().getNome()+ ". Unidade de medida com valor divergente: " + item.getUnidademedida().getNome());
							return true;
						}
					}
				}
				
				Double fracao = materialunidademedida.getFracao();
				Double qtdereferencia = materialunidademedida.getQtdereferencia();
				
				for(Materialunidademedida materialunidademedida2 : bean.getListaMaterialunidademedida()){
					if(!materialunidademedida2.getUnidademedida().equals(materialunidademedida.getUnidademedida())){
						list = unidademedidaconversaoService.findForValidacaoConversao(materialunidademedida2.getUnidademedida(), materialunidademedida.getUnidademedida());
						if(SinedUtil.isListNotEmpty(list)){
							for(Unidademedidaconversao item : list){
								Double fracao2 = materialunidademedida2.getFracao();
								Double qtdereferencia2 = materialunidademedida2.getQtdereferencia();
								
								Double fracaoQtdereferencia = (qtdereferencia * fracao2) / (fracao * qtdereferencia2); 
								if(item.getFracaoQtdereferencia().compareTo(fracaoQtdereferencia) != 0){
									errors.reject("001", "Existe convers�o divergente relacionado a unidade de medida " + item.getUnidademedidarelacionada().getNome() + ". Unidade de medida com valor divergente: " + item.getUnidademedida().getNome());
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public Materialunidademedida getMaterialunidademedidaPrioritariaProducao(Material material){
		Materialunidademedida materialunidademedida = null;
		
		if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialunidademedida())){
			for(Materialunidademedida bean : material.getListaMaterialunidademedida()){
				if(bean.getUnidademedida() != null && bean.getPrioridadeproducao() != null && bean.getPrioridadeproducao()){
					materialunidademedida = bean;
					break;
				}
			}
		}
		
		return materialunidademedida;
	}
	
	public List<UnidadeMedidaWSBean> findByMaterialForWSVenda(Material material){
		List<UnidadeMedidaWSBean> lista = new ArrayList<UnidadeMedidaWSBean>();
		for(Materialunidademedida mum: this.findByMaterial(material)){
			UnidadeMedidaWSBean bean = new UnidadeMedidaWSBean();
			bean.setId(mum.getUnidademedida().getCdunidademedida());
			bean.setSimbolo(mum.getUnidademedida().getSimbolo());
			bean.setValue(mum.getUnidademedida().getNome());
			
			lista.add(bean);
		}
		
		return lista;
	}
}
