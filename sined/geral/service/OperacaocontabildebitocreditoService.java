package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.dao.OperacaocontabildebitocreditoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OperacaocontabildebitocreditoService extends GenericService<Operacaocontabildebitocredito>{
	
	private OperacaocontabildebitocreditoDAO operacaocontabildebitocreditoDAO;
	
	public void setOperacaocontabildebitocreditoDAO(OperacaocontabildebitocreditoDAO operacaocontabildebitocreditoDAO) {
		this.operacaocontabildebitocreditoDAO = operacaocontabildebitocreditoDAO;
	}
	
	/**
	 * 
	 * @param operacaocontabil
	 * @param propertyOperacaocontabil
	 * @author Thiago Clemente
	 * 
	 */
	public List<Operacaocontabildebitocredito> findByOperacaocontabil(Operacaocontabil operacaocontabil, String propertyOperacaocontabil){
		return operacaocontabildebitocreditoDAO.findByOperacaocontabil(operacaocontabil, propertyOperacaocontabil);
	}	
}
	


