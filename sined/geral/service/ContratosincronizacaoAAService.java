package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.ContratosincronizacaoAA;
import br.com.linkcom.sined.geral.dao.ContratosincronizacaoAADAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratosincronizacaoAAService extends GenericService<ContratosincronizacaoAA> {
	
	private static ContratosincronizacaoAAService instance;
	private ContratosincronizacaoAADAO contratosincronizacaoAADAO;
	public void setContratosincronizacaoAADAO(ContratosincronizacaoAADAO contratosincronizacaoAADAO) {this.contratosincronizacaoAADAO = contratosincronizacaoAADAO;}
	
	public List<ContratosincronizacaoAA> findNaoSincronizados() {
		return contratosincronizacaoAADAO.findNaoSincronizados();
	}

	public static ContratosincronizacaoAAService getInstance() {
		if(instance==null)
			instance = Neo.getObject(ContratosincronizacaoAAService.class);
		return instance;
	}
	
	public void updateSincronizacao(Integer cdempresa, Integer id){
		contratosincronizacaoAADAO.updateSincronizacao(cdempresa, id);
	}
	
	public void updateError(Integer cdempresa, String mensagem){
		contratosincronizacaoAADAO.updateError(cdempresa, mensagem);
	}
	
	public ContratosincronizacaoAA findByCdCliente(Integer cdcliente) {
		return contratosincronizacaoAADAO.findByCdCliente(cdcliente);
	}

}