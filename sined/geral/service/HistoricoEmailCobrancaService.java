package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.HistoricoEmailCobranca;
import br.com.linkcom.sined.geral.bean.enumeration.BounceTypeEnum;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SendgridEvent;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EmailWebHook;

public class HistoricoEmailCobrancaService extends GenericService<HistoricoEmailCobranca>{
	
	private static HistoricoEmailCobrancaService instance;
	public static HistoricoEmailCobrancaService getInstance() {
		if(instance==null)
			instance = Neo.getObject(HistoricoEmailCobrancaService.class);
		return instance;
	}
	
	public void atualizarStatus(EmailWebHook emailWebHook){
		HistoricoEmailCobranca historico = new HistoricoEmailCobranca();
		EmailCobranca emailCobranca = new EmailCobranca();
		
		emailCobranca.setCdemailcobranca(emailWebHook.getCdemail());
		emailCobranca = EmailCobrancaService.getInstance().loadEmailCobranca(emailCobranca);
		
		if(emailCobranca != null){
			historico.setEmailcobranca(emailCobranca);
			historico.setEvent(SendgridEvent.valueOf(emailWebHook.getEvent().toUpperCase()));
			if(emailWebHook.getType() != null && !emailWebHook.getType().isEmpty()){
				historico.setType(BounceTypeEnum.valueOf(emailWebHook.getType().toUpperCase()));
			}
			historico.setAttempt(emailWebHook.getAttempt());
			historico.setResponse(emailWebHook.getResponse());
			historico.setUrlclicked(emailWebHook.getUrlclicked());
			historico.setStatus(emailWebHook.getStatus());
			historico.setReason(emailWebHook.getReason());
			historico.setIp(emailWebHook.getIp());
			historico.setUseragent(emailWebHook.getUseragent());
			historico.setUltimaverificacao(new Timestamp(emailWebHook.getUltimaverificacao()));
			
			saveOrUpdate(historico);
			
			EmailStatusEnum statusEmail = EmailStatusEnum.fromSendgridEvent(historico.getEvent());
			if(statusEmail != null && emailCobranca.getSituacaoemail() != null && statusEmail.getValor() > emailCobranca.getSituacaoemail().getValor()){
				emailCobranca.setSituacaoemail(statusEmail);
			} else if(emailCobranca.getSituacaoemail() == null){
				emailCobranca.setSituacaoemail(statusEmail);
			}
			
			emailCobranca.setUltimaverificacao(historico.getUltimaverificacao());
			EmailCobrancaService.getInstance().saveOrUpdate(emailCobranca);
		}
	}
}
