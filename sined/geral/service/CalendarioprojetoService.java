package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioprojeto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.dao.CalendarioprojetoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CalendarioprojetoService extends GenericService<Calendarioprojeto> {

	private CalendarioprojetoDAO calendarioprojetoDAO;
	
	public void setCalendarioprojetoDAO(
			CalendarioprojetoDAO calendarioprojetoDAO) {
		this.calendarioprojetoDAO = calendarioprojetoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CalendarioprojetoDAO#verificaOutroCalendario
	 *
	 * @param projeto
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean verificaOutroCalendario(Projeto projeto, Calendario bean) {
		return calendarioprojetoDAO.verificaOutroCalendario(projeto, bean);
	}

	/**
	 * Retorna a lista de projetos de uma lista de Calendarioprojeto.
	 *
	 * @param listaCalendarioprojeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> getProjetos(List<Calendarioprojeto> listaCalendarioprojeto) {
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		for (Calendarioprojeto calendarioprojeto : listaCalendarioprojeto) {
			if(calendarioprojeto.getProjeto() != null && calendarioprojeto.getProjeto().getCdprojeto() != null){
				listaProjeto.add(calendarioprojeto.getProjeto());
			}
		}
		return listaProjeto;
	}

}
