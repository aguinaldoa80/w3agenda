package br.com.linkcom.sined.geral.service;
 
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaBean;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauCT;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauDOC;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauHeader;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauHeaderLote;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauLT;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauNF;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauTrailer;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauTrailerLote;
import br.com.linkcom.lkbanco.filesend.util.GerarArquivoRemessa;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAgrupamento;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.geral.bean.Contratotipodesconto;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoautori;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Motivocancelamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.DocumentoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.PessoaVO;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NotaFiscalTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoContapagarReport;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoContareceberReport;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoMailingDevedor;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCriacaoPedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipodesconto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.DocumentoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FaturamentoConjuntoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivobancarioProcessFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaPagamentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBeanMovimentacao;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.DevedoresBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.EstornaContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.GerarArquivoRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.SelecionarDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ContaReceberPagarBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.BaixarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarFaturaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarFaturaClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.RateioitemBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoapoioFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoitemapoio;
import br.com.linkcom.sined.util.AlterarEstadoException;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.sined.util.rest.android.documento.DocumentoRESTModel;

public class DocumentoService extends GenericService<Documento>{
	
	private static final String PARAMETRO_CANCELAMENTO_CONTA_RECEBER = "CANCELAMENTO_CONTA_RECEBER";
	private static final String PARAMETRO_DIAS_CANCELAMENTO_CONTA_RECEBER = "DIAS_CANCELAMENTO_CONTA_RECEBER";

	private DocumentoDAO documentoDAO;
	private DocumentoService documentoService;
	private ParcelamentoService parcelamentoService;
	private RateioService rateioService;
	private AgendamentoService agendamentoService;
	private ContapagarService contapagarService;
	private ContareceberService contareceberService;	 
	private DocumentohistoricoService documentohistoricoService;
	private DocumentoautoriService documentoautorizacaoService;
	private MovimentacaoService movimentacaoService;
	private ContagerencialService contagerencialService;
	private FormapagamentoService formapagamentoService;
	private CentrocustoService centrocustoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ContaService contaService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private NotaService notaService;
	private AvisoService avisoService;
	private DocumentotipoService documentotipoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private RateioitemService rateioitemService;
	private TaxaService taxaService;
	private ArquivobancarioService arquivobancarioService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private PessoaService pessoaService;
	private EmpresaService empresaService;
	private ColaboradorService colaboradorService;
	private IndicecorrecaoService indicecorrecaoService;
	private EnderecoService enderecoService;
	private MaterialService materialService;
	private ParametrogeralService parametrogeralService;
	private NotaDocumentoService notaDocumentoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ClienteService clienteService;
	private EntregaService entregaService;
	private ControleorcamentoitemService controleorcamentoitemService;
	private FornecedorService fornecedorService;
	private BancoformapagamentoService bancoformapagamentoService;
	private BancotipopagamentoService bancotipopagamentoService; 
	private TaxaitemService taxaitemService;
	private ArquivonfnotaService arquivonfnotaService;
	private ChequehistoricoService chequehistoricoService;
	private ContratoparcelaService contratoparcelaService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private DocumentoorigemService documentoorigemService;
	private MaterialdevolucaoService materialdevolucaoService;
	private BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService;
	private NotaVendaService notaVendaService;
	private ContratoService contratoService;	
	private ContratodescontoService contratodescontoService;
	private OrdemcompraService ordemcompraService;
	private VendaService vendaService;
	private DocumentoconfirmacaoService documentoconfirmacaoService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	private GrupotributacaoService grupotributacaoService;
	private ChequeService chequeService;
	private ValecompraService valecompraService;
	private ValecompraorigemService valecompraorigemService;
	private BancoConfiguracaoRemessaAgrupamentoService bancoConfiguracaoRemessaAgrupamentoService;
	private ContacorrenteService contacorrenteService;
	private PedidovendaService pedidovendaService;
	private TipotaxaService tipotaxaService;
	private PrazopagamentoService prazopagamentoService;
	private ContacarteiraService contacarteiraService;
	private CategoriaService categoriaService;
	private ContatoService contatoService;
	private AgendainteracaoService agendainteracaoService;
	private MotivoavisoService motivoavisoService;
	private HistoricooperacaoService historicooperacaoService;
	private NotaContratoService notaContratoService;
	private EntradafiscalService entradafiscalService;
	private CalendarioService calendarioService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {this.agendainteracaoService = agendainteracaoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setBancoConfiguracaoRemessaAgrupamentoService(BancoConfiguracaoRemessaAgrupamentoService bancoConfiguracaoRemessaAgrupamentoService) {this.bancoConfiguracaoRemessaAgrupamentoService = bancoConfiguracaoRemessaAgrupamentoService;}
	public void setValecompraorigemService(ValecompraorigemService valecompraorigemService) {this.valecompraorigemService = valecompraorigemService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {this.vdocumentonegociadoService = vdocumentonegociadoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setContratodescontoService(ContratodescontoService contratodescontoService) {this.contratodescontoService = contratodescontoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {this.materialdevolucaoService = materialdevolucaoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {this.formapagamentoService = formapagamentoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setDocumentoDAO(DocumentoDAO documentoDAO) {this.documentoDAO = documentoDAO;}
	public void setParcelamentoService(ParcelamentoService parcelamentoService) {this.parcelamentoService = parcelamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setDocumentoautorizacaoService(DocumentoautoriService documentoautorizacaoService) {this.documentoautorizacaoService = documentoautorizacaoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {this.movimentacaoorigemService = movimentacaoorigemService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {this.indicecorrecaoService = indicecorrecaoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}	
	public void setControleorcamentoitemService(ControleorcamentoitemService controleorcamentoitemService) {this.controleorcamentoitemService = controleorcamentoitemService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setBancoformapagamentoService(BancoformapagamentoService bancoformapagamentoService) {this.bancoformapagamentoService = bancoformapagamentoService;}
	public void setTaxaitemService(TaxaitemService taxaitemService) {this.taxaitemService = taxaitemService;}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setBancotipopagamentoService(BancotipopagamentoService bancotipopagamentoService) {this.bancotipopagamentoService = bancotipopagamentoService;}
	public void setContratoparcelaService(ContratoparcelaService contratoparcelaService) {this.contratoparcelaService = contratoparcelaService;}
	public void setBancoConfiguracaoRemessaService(BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService) {this.bancoConfiguracaoRemessaService = bancoConfiguracaoRemessaService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	public void setDocumentoconfirmacaoService(DocumentoconfirmacaoService documentoconfirmacaoService) {this.documentoconfirmacaoService = documentoconfirmacaoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {this.motivoavisoService = motivoavisoService;}
	public void setHistoricooperacaoService(HistoricooperacaoService historicooperacaoService) {this.historicooperacaoService = historicooperacaoService;}
	public void setNotaContratoService(NotaContratoService notaContratoService) {this.notaContratoService = notaContratoService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {this.historicoAntecipacaoService = historicoAntecipacaoService;}
	
	/**
	 * M�todo para estornar contas a pagar/receber.
	 * 
	 * @see #ajustaBeanParaEstorno(EstornaContaBean)
	 * @see #retornaDocumentosAcaoAnt(Documento, Documentoacao, String)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#delete(Movimentacao)
	 * @see #doBaixarConta(WebRequestContext, BaixarContaBean)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#saveOrUpdateNoUseTransaction(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#callProcedureAtualizaMovimentacao(Movimentacao)
	 * 
	 * @param bean
	 * @throws Exception
	 * @author Fl�vio Tavares
	 */
	@SuppressWarnings("unchecked")
	public void doEstornarContasBaixadas(final EstornaContaBean bean) throws Exception{
		/*
		 * Prepara��o do bean EstornaContaBean para o processo de estornar
		 */
		this.ajustaBeanParaEstorno(bean);
		
		List<Movimentacao> movimentacoes = (List<Movimentacao>) getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String whereInMovReferente = null;
				Money valorTotalMovReferente = null;
				List<Movimentacao> lista = new ArrayList<Movimentacao>();
				/*
				 *	A Movimenta��o anterior � exclu�da e � gerada outra com a baixa. 
				 */				
				if(bean.getWhereInMovimentacaoReferente() != null && !"".equals(bean.getWhereInMovimentacaoReferente())){
					whereInMovReferente = bean.getWhereInMovimentacaoReferente();
					if(bean.getBaixarContaBean() != null && bean.getBaixarContaBean().getVinculo() != null){
						valorTotalMovReferente = movimentacaoService.getValorTotalMovimentacoes(whereInMovReferente, bean.getBaixarContaBean().getVinculo());
					}
					movimentacaoService.cancelar(whereInMovReferente, "Conta estornada.");
				} else if(bean.getMovimentacaoReferente() != null && bean.getMovimentacaoReferente().getCdmovimentacao() != null){
					whereInMovReferente = bean.getMovimentacaoReferente().getCdmovimentacao().toString();
					movimentacaoService.cancelar(whereInMovReferente, "Conta estornada.");
				}
				
				if(whereInMovReferente != null && !whereInMovReferente.equals("")){
					chequeService.estornaChequeByMovimentacao(bean.getDocumentoclasse(), bean.getListaDocumentoAEstornar(), whereInMovReferente);
				}
				
				if(bean.isExisteBaixa()){
					
					/*
					 * Retorno das contas a n�o serem estornadas para a situa��o anterior para serem baixadas novamente.
					 */
					BaixarContaBean baixarContaBean = bean.getBaixarContaBean();
					for (Documento d : baixarContaBean.getListaDocumento()) {
						if(d.getDocumentoclasse() == null && baixarContaBean.getDocumentoclasse() != null){
							d.setDocumentoclasse(baixarContaBean.getDocumentoclasse());
						}
						documentoService.retornaDocumentosAcaoAnt(d, Documentoacao.ESTORNADA, "Estornada para baixar novamente.", false);
					}
				
					baixarContaBean.setWhereInMovReferente(whereInMovReferente);
					baixarContaBean.setValorTotalMovReferente(valorTotalMovReferente);
					/*
					 * Realiza��o da baixa das contas n�o estornadas e verifica��o de ocorr�ncia de erros.
					 */
					Movimentacao movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
					if(Util.booleans.isTrue(baixarContaBean.getErro())){
						throw new SinedException("N�o foi poss�vel baixar as contas. Poss�velmente elas j� est�o baixadas e a " +
								"movimenta��o est� cancelada.");
					}
					if(movimentacao != null) lista.add(movimentacao);
				}
				
				/*
				 * Volta as contas estornadas para a situa��o PREVISTA
				 */
				if(SinedUtil.isListNotEmpty(bean.getListaDocumentoAEstornar())){
					for (Documento d : bean.getListaDocumentoAEstornar()) {
//						if(notaDocumentoService.isDocumentoWebservice(d)){
//							notaDocumentoService.estornaNotaDocumentoWebservice(d);
//						}
						
						List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByDocumento(d);
						if(SinedUtil.isListNotEmpty(listaValecompraorigem)){
							for (Valecompraorigem valecompraorigem : listaValecompraorigem) {
								Valecompra valecompra = valecompraorigem.getValecompra();
								valecompraorigemService.delete(valecompraorigem);
								if(valecompra != null){
									valecompraService.delete(valecompra);
								}
							}
						}
						
//						List<HistoricoAntecipacao> listaHistoricoanAntecipacao = historicoAntecipacaoService.findByDocumentoReferencia(d);
//						if(SinedUtil.isListNotEmpty(listaHistoricoanAntecipacao)){
//							for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoanAntecipacao) {
//								historicoAntecipacaoService.delete(historicoAntecipacao);
//								Documentohistorico dh = new Documentohistorico();
//								documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.ESTORNADA, d);
//								dh.setObservacao("Registro de utiliza��o da antecipa��o desfeito.");
//								documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
//							}
//						}
						
						excluirHistoricoAntecipacaoDocumento(d, false);
						
						if(d.getDocumentoclasse() == null && bean.getDocumentoclasse() != null){
							d.setDocumentoclasse(bean.getDocumentoclasse());
						}
						documentoService.retornaDocumentosAcaoAnt(d, Documentoacao.ESTORNADA, null, true);
						
						// COLOCA MENSAGENS PARA CANCELAR AS NOTAS
						notaDocumentoService.validaEstornoContaWebserviceTrue(NeoWeb.getRequestContext(), d);
					}
				}
				
				/*
				 * Se a diferen�a n�o for desconsiderada, a nova movimenta��o com este valor � criada.
				 */
				Movimentacao mov = bean.getMovimentacao();
				if(Util.booleans.isFalse(bean.getDesconsiderarDiferenca())){
					movimentacaoService.saveOrUpdateNoUseTransaction(mov);
				}else{
					mov = null;
				}
				
				if(mov != null) lista.add(mov);
				return lista;
			}
		});
		
		if(movimentacoes != null && movimentacoes.size() > 0){
			for (Movimentacao movimentacao : movimentacoes) {
				movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
			}
		}
	}
	
	public void excluirHistoricoAntecipacaoDocumento(Documento documento, Boolean isCompensacao) {
		List<HistoricoAntecipacao> listaHistoricoanAntecipacao = historicoAntecipacaoService.findByDocumentoReferencia(documento, isCompensacao);
		
		if(SinedUtil.isListNotEmpty(listaHistoricoanAntecipacao)){
			for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoanAntecipacao) {
				historicoAntecipacaoService.delete(historicoAntecipacao);
				
				Documentohistorico dh = new Documentohistorico();
				documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.ESTORNADA, documento);
				
				dh.setObservacao("Registro de utiliza��o do adiantamento desfeito.");
				
				documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
			}
		}
	}
	
	/**
	 * M�todo para ajustar o bean para o processo de estornar contas a pagar/receber.
	 * 
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	private void ajustaBeanParaEstorno(EstornaContaBean bean){
		if(bean.isExisteBaixa()){
			BaixarContaBean baixarContaBean = bean.getBaixarContaBean();
			baixarContaBean.setRateio(bean.getRateio());
			List<Documento> listaDocumentoBaixar = bean.getListaDocumentoNaoEstornar();
			for (Documento d : listaDocumentoBaixar) {
				d.setValoratual(d.getAux_documento().getValoratual());
			}
			baixarContaBean.setListaDocumento(listaDocumentoBaixar);
		}
		
		Movimentacao movimentacao = bean.getMovimentacao();
		if(movimentacao != null){
			movimentacao.setRateio(bean.getRateioDiferenca());
			movimentacao.getRateio().setCdrateio(null);
			movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		}
	}
	
	public void criaDescontoAutomaticoContratoPagamento(BaixarContaBean baixarContaBean) {
//		METODO QUE CRIA UM DESCONTO AUTOM�TICO NO CONTRATO
		Contrato contrato;
		for (Documento doc : baixarContaBean.getListaDocumento()) {
			contrato = new Contrato();
			contrato = contratoService.carregarContratoComDocumentoOrigem(doc);
			if (contrato == null){
				contrato = contratoService.carregarContratoComNotaFiscal(doc);
			}
			if (contrato != null && 
					contrato.getContratotipo() != null && 
					contrato.getContratotipo().getListacontratotipodesconto() != null && 
					contrato.getContratotipo().getListacontratotipodesconto().size() > 0 &&
					contrato.getPrazopagamento() == null /* FEITO ISSO POIS CONTRATOS COM PARCELA N�O PODE TER DESCONTO */){
				doc = documentoService.carregaDocumento(doc);
				
				Date dtvencimento = doc.getDtvencimentooriginal();
				if(dtvencimento == null){
					dtvencimento = doc.getDtvencimento();
				}
				
				Contrato contrato_aux = null;
				for (Contratotipodesconto ctd : contrato.getContratotipo().getListacontratotipodesconto()) {
					boolean dentroLimite = ctd.getLimite() == null || ctd.getLimite().after(baixarContaBean.getDtpagamento());
					boolean pagamentoAntecipado = SinedDateUtils.diferencaDias(dtvencimento, baixarContaBean.getDtpagamento()) >= ctd.getDiasantesvencimento();
					
					if (dentroLimite && pagamentoAntecipado){
						if(!contratodescontoService.haveDescontoContratoData(contrato, contrato.getDtproximovencimento())){
						Contratodesconto contratodesconto = new Contratodesconto();
						contratodesconto.setContrato(contrato);
						
						Formafaturamento formafaturamento = contrato.getFormafaturamento();
						if(formafaturamento == null || !formafaturamento.equals(Formafaturamento.SOMENTE_BOLETO)){
							if(contratoService.haveContratoProduto(contrato.getCdcontrato().toString())){
								contratodesconto.setTipodesconto(Tipodesconto.NF_PRODUTO);
							}else {
								contratodesconto.setTipodesconto(Tipodesconto.NF_SERVICO);
							}
						} else {
							contratodesconto.setTipodesconto(Tipodesconto.CONTA_RECEBER);
						}
						contratodesconto.setTiponota(NotaFiscalTipoEnum.DESCONTO_CONDICIONADO);
						
						if(ctd.isForma()){
							if(contrato_aux == null){
								contrato_aux = contratoService.loadForEntrada(contrato);
							}
							Money valorContrato = contrato_aux.getValorContrato();
							contratodesconto.setValor(valorContrato.multiply(ctd.getValor()).divide(new Money(100d)));
						} else {
							contratodesconto.setValor(ctd.getValor());
						}
						contratodesconto.setMotivo("Desconto Autom�tico");
						contratodesconto.setDtinicio(contrato.getDtproximovencimento());
						contratodesconto.setDtfim(contrato.getDtproximovencimento());
						contratodescontoService.saveOrUpdate(contratodesconto);
					}
				}
			}
		}
	}
	}
	
	/**
	 * M�todo criado para que seja aberto uma transaction para salvar tudo s� se n�o der erro.
	 * <p>Por realizar v�rias transa��es, <b>deve ser usado dentro de uma transaction</b> para garantir a integridade
	 * dos dados em caso de erro.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#listaMovimentcaoNaoCancelada(String)
	 * @see #findListaDocumentoBaixarConta(String)
	 * @see br.com.linkcom.sined.geral.service.ContaService#carregaConta(Conta)
	 * @see #gerarArquivoBancario(List, Conta)
	 * @see #updateStatusConta(String, Documentoacao)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @see br.com.linkcom.sined.geral.service.RateioService#saveOrUpdateNoUseTransaction(Rateio)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#salvarMovimentacao(BaixarContaBean, Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#callProcedureAtualizaMovimentacao(Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoorigemService#salvarMovimentacaoOrigem(List, Movimentacao)
	 * 
	 * @param request 
	 * @param baixarContaBean
	 * 
	 * @return A Movimenta��o gerada na baixa.
	 * 
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 * @author Fl�vio Tavares - Desmembramento do m�todo
	 */
	public Movimentacao doBaixarConta(WebRequestContext request, BaixarContaBean baixarContaBean){
		List<Documento> listaDocumento = baixarContaBean.getListaDocumento();
		
		if(baixarContaBean.getValorTotal() == null){
			Double total = 0.0;
			for (Documento documento : listaDocumento) {
				if(listaDocumento.size() == 1){
					Documento documentoTaxa = documentoService.loadForMovimentacaoTaxa(documento);
					Taxaitem taxaitem = documentoService.getTaxaitemByTipo(documentoTaxa, Tipotaxa.TAXAMOVIMENTO);
					
					if (documentoTaxa!=null && taxaitem != null && taxaitem.getValor() != null){
						documento.setValoratualbaixaSemTaxa(this.getValorAtual(documento.getCddocumento(), baixarContaBean.getDtpagamento(), true));
					}
					total += documento.getValoratualbaixaSemTaxa() != null ? documento.getValoratualbaixaSemTaxa().getValue().doubleValue() : documento.getValoratual().getValue().doubleValue();
				}else {
					total += documento.getValoratual().getValue().doubleValue();
				}
			}
			baixarContaBean.setValorTotal(total);
		}
		
		for (Documento documento : listaDocumento) {
			if(movimentacaoService.listaMovimentcaoNaoCancelada(documento.getCddocumento() + "", baixarContaBean.getWhereInMovReferente()) > 0){
				if(request != null) request.addError("Conta " + documento.getCddocumento() + " j� baixada.");
				baixarContaBean.setErro(Boolean.TRUE);
				baixarContaBean.setIsContaBaixada(Boolean.TRUE);
			}
		}
		if (baixarContaBean.getErro() != null && baixarContaBean.getErro()) {
			return null;
		}
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
		
		Documentoacao[] condicoes = null;
		if(Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())){
			// CONTA A PAGAR
			condicoes = new Documentoacao[]{Documentoacao.AUTORIZADA, Documentoacao.AUTORIZADA_PARCIAL};
		}else{
			// CONTA A RECEBER
			if(baixarContaBean.getFromRetorno() || baixarContaBean.getFromWebservice() || baixarContaBean.getFromBaixaautomatica()){
				condicoes = new Documentoacao[]{Documentoacao.PREVISTA, Documentoacao.DEFINITIVA};
			} else {
				
				condicoes = new Documentoacao[]{Documentoacao.DEFINITIVA};
			}
		}
		
		if (listaDocumento != null && listaDocumento.size() > 0) {
			for (Documento documento : listaDocumento) {
				if (Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse()) && 
						!documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA) && 
						!documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA_PARCIAL) &&
						!documento.getDocumentoacao().equals(Documentoacao.ESTORNADA)) {
					String message = "Registros com situa��o diferente de Autorizada, Autorizada Parcial.";
					throw new SinedException(message);
				} else if (Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse())) {
					if (baixarContaBean.getFromRetorno() || baixarContaBean.getFromWebservice() || baixarContaBean.getFromBaixaautomatica()){
						if (!documento.getDocumentoacao().equals(Documentoacao.PREVISTA) &&  !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)) {
							String message = "Registros com situa��o diferente de Prevista, Definitiva.";
							throw new SinedException(message);
						}
					} else {
						if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && !documento.getDocumentoacao().equals(Documentoacao.ESTORNADA)) {
							String message = "Registros com situa��o diferente de Definitiva.";
							throw new SinedException(message);
						}
					}
				}
			}
		}
		
		if(baixarContaBean.getFormapagamento() != null && baixarContaBean.getFormapagamento().equals(Formapagamento.VALECOMPRA)){
			boolean mesmoCliente = true;
			Integer cdpessoa = null;
			for (Documento documento : listaDocumento) {
				if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
					if(cdpessoa == null) {
						cdpessoa = documento.getPessoa().getCdpessoa();
					} else if(!documento.getPessoa().getCdpessoa().equals(cdpessoa)){
						mesmoCliente = false;
					}
				} else mesmoCliente = false;
			}
			
			if(cdpessoa != null && mesmoCliente){
				Cliente cliente = clienteService.load(new Cliente(cdpessoa), "cliente.cdpessoa, cliente.nome");
				if(cliente != null){
					Valecompra valecompra = new Valecompra();
					valecompra.setCliente(cliente);
					valecompra.setData(baixarContaBean.getDtpagamento());
					valecompra.setIdentificacao("Uso na baixa de documento");
					valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
					valecompra.setValor(new Money(baixarContaBean.getValorTotal()));
					
					valecompraService.saveOrUpdateNoUseTransaction(valecompra);
					
					for (Documento documento : listaDocumento) {
						Valecompraorigem valecompraorigem = new Valecompraorigem();
						valecompraorigem.setDocumento(documento);
						valecompraorigem.setValecompra(valecompra);
						
						valecompraorigemService.saveOrUpdateNoUseTransaction(valecompraorigem);
					}
				}
			}
			
			return null;
		} else {
			Conta conta = contaService.carregaConta(baixarContaBean.getVinculo());
			baixarContaBean.setVinculo(conta);
			
			//Gera o arquivo de remessa na baixa do documento
			Arquivo arquivo = null;
			try {
				if (conta.getBanco() != null && conta.getBanco().getGerarsispag() != null && conta.getBanco().getNumero() != null &&
						baixarContaBean.getGerararquivoremessa() != null && baixarContaBean.getGerararquivoremessa() != null && conta.getBanco().getGerarsispag() &&  
						Formapagamento.DEBITOCONTACORRENTE.equals(baixarContaBean.getFormapagamento())){			
					try {
						arquivo = bancoConfiguracaoRemessaService.gerarArquivoRemessaSispag(baixarContaBean);			
					} catch (SinedException e) {
						request.addError(e.getMessage());
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("Erro desconhecido: " + e.getMessage());
					}
				} else if (conta.getBanco() != null && conta.getBanco().getGerarsispag() != null && conta.getBanco().getNumero() != null &&
						conta.getBanco().getNumero().equals(341) && baixarContaBean.getGerararquivoremessa() != null && baixarContaBean.getGerararquivoremessa() && conta.getBanco().getGerarsispag() &&  
						Formapagamento.DEBITOCONTACORRENTE.equals(baixarContaBean.getFormapagamento())){
					arquivo = gerarArquivoSispagItau(baixarContaBean);
				} else if (baixarContaBean.getGerararquivoregistrocobranca() != null && baixarContaBean.getGerararquivoregistrocobranca()){
					arquivo = gerarArquivoBradesco(baixarContaBean); 
				}		
				baixarContaBean.setArquivo(arquivo);
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Erro ao gerar arquivo de remessa: " + e.getMessage());
			}
			
			Movimentacao movimentacao = new Movimentacao();
			
			baixarContaBean.getRateio().setCdrateio(null);
			for (Rateioitem rateioitem : baixarContaBean.getRateio().getListaRateioitem()) {
				rateioitem.setCdrateioitem(null);
				if (rateioitem.getProjeto() == null || rateioitem.getProjeto().getCdprojeto() == null) {
					rateioitem.setProjeto(null);
				}
			}
			rateioService.atualizaValorRateio(baixarContaBean.getRateio(), new Money(baixarContaBean.getValorTotal()));
			rateioService.saveOrUpdateNoUseTransaction(baixarContaBean.getRateio());
			
			if(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_EMPRESA)){
				if(listaDocumento != null && listaDocumento.size() == 1 && baixarContaBean.getVinculo() != null){
					Empresa empresa = listaDocumento.get(0).getEmpresa();
					if(empresa != null && contaService.isPermissaoConta(baixarContaBean.getVinculo(), empresa)){
						baixarContaBean.setEmpresa(empresa);
					}
				}else if(listaDocumento != null && listaDocumento.size() > 1 && baixarContaBean.getVinculo() != null){
					List<Empresa> listaEmpresa = new ArrayList<Empresa>();
					boolean empresaNula = false;
					for(Documento documento : listaDocumento){
						if(documento.getEmpresa() == null){
							empresaNula = true;
						}else if(documento.getEmpresa() != null && !listaEmpresa.contains(documento.getEmpresa())){
							listaEmpresa.add(documento.getEmpresa());
						}
					}
					if(!empresaNula && listaEmpresa.size() == 1){
						Empresa empresa = listaDocumento.get(0).getEmpresa();
						if(empresa != null && contaService.isPermissaoConta(baixarContaBean.getVinculo(), empresa)){
							baixarContaBean.setEmpresa(empresa);
						}
					}
				}
			}
			
			movimentacaoService.salvarMovimentacao(baixarContaBean, movimentacao, listaDocumento);
			movimentacaoorigemService.salvarMovimentacaoOrigem(listaDocumento, movimentacao, null, baixarContaBean);
			//movimentacaocontabilService.insertMovimentacaocontabilNoUseTransaction(movimentacao,listaDocumento);
			
			contapagarService.updateStatusConta(Documentoacao.BAIXADA, whereIn, condicoes);
			
			Documentohistorico documentoHistorico = null;
			for (Documento documento : listaDocumento) {
				documento.setDocumentoacao(Documentoacao.BAIXADA);
				documento.setFromRetorno(baixarContaBean.getFromRetorno());
				documento.setFromWebservice(baixarContaBean.getFromWebservice());
				documento.setFromBaixaautomatica(baixarContaBean.getFromBaixaautomatica());
				documentoHistorico = documentohistoricoService.gerarHistoricoDocumento(null, Documentoacao.BAIXADA, documento);
				
				if(StringUtils.isNotBlank(baixarContaBean.getMensagem())){
					String motivoBaixa = "Motivos de baixa: " + baixarContaBean.getMensagem();
					if(StringUtils.isNotBlank(documentoHistorico.getObservacao())){
						documentoHistorico.setObservacao(documentoHistorico.getObservacao() + ". " + motivoBaixa);
					}else {
						documentoHistorico.setObservacao(motivoBaixa);
					}
				}
				documentohistoricoService.saveOrUpdateNoUseTransaction(documentoHistorico);
			}
			
			movimentacao.setConta(conta);
			return movimentacao;
		}
	}
	
	private Arquivo gerarArquivoSispagItau(BaixarContaBean baixarContaBean) {
		if (baixarContaBean.getBancoformapagamento() == null || baixarContaBean.getBancotipopagamento() == null)
			throw new SinedException("� obrigat�rio informar a forma e tipo de pagamento para gera��o do arquivo de remessa.");
		
		baixarContaBean.setBancoformapagamento(bancoformapagamentoService.load(baixarContaBean.getBancoformapagamento()));
		baixarContaBean.setBancotipopagamento(bancotipopagamentoService.load(baixarContaBean.getBancotipopagamento()));
		
		this.validaPagamentosSispag(baixarContaBean.getBancoformapagamento(), baixarContaBean.getBancotipopagamento());
		
		Arquivo arquivo = null;
		Conta conta = baixarContaBean.getVinculo();

		if (conta.getEmpresa() == null){
			Set<Contaempresa> listaContaempresa = new ListSet<Contaempresa>(Contaempresa.class);
			listaContaempresa.add(new Contaempresa(empresaService.loadPrincipal()));
			conta.setListaContaempresa(listaContaempresa);
		} 
		
		conta.getEmpresa().setEnderecoAux(enderecoService.carregaEnderecoEmpresa(conta.getEmpresa()));
		baixarContaBean.setVinculo(conta);
		
		String whereIn = CollectionsUtil.listAndConcatenate(baixarContaBean.getListaDocumento(), "cddocumento", ",");
		GerarArquivoRemessaBean filtro = new GerarArquivoRemessaBean();
		filtro.setContas(whereIn);
		List<Documento> listaDocumentoRemessa = documentoService.findDocumentosRemessa(filtro);		
				
		for (Documento documento : listaDocumentoRemessa) {
			documento.setFornecedor(fornecedorService.load(documento.getPessoa().getCdpessoa()));
		}
				
		try {
			String strArquivo = "";
			
			// Preenche o Header e Trailer de Arquivos e Lotes 
			ArquivoRemessaItauHeader itauHeader = arquivobancarioService.setHeaderSispagItau(baixarContaBean);
			ArquivoRemessaItauHeaderLote itauHeaderLote = arquivobancarioService.setHeaderLoteSispagItau(listaDocumentoRemessa, baixarContaBean);
			ArquivoRemessaItauTrailerLote itauTrailerLote = arquivobancarioService.setTrailerLoteSispagItau(listaDocumentoRemessa, baixarContaBean);
			ArquivoRemessaItauTrailer itauTrailer = arquivobancarioService.setTrailerSispagItau(listaDocumentoRemessa, baixarContaBean);
			
			GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();	
			
			Integer identificador = baixarContaBean.getBancoformapagamento().getIdentificador();
			
			//Pagamentos atrav�s de cheque, OP, DOC, TED e Cr�dito em Conta Corrente (Segmento A)
			if (identificador == 2 || identificador == 3 || identificador == 5 || identificador == 6 
					|| identificador == 7 || identificador == 10 || identificador == 41 || identificador == 43){
				List<ArquivoRemessaItauDOC> listaItauDOC = arquivobancarioService.setDetalheDOCSispagItau(listaDocumentoRemessa, baixarContaBean, identificador);
				strArquivo = arquivoRemessa.gerarArquivoRemessaBancoItauDOC(itauHeader, itauHeaderLote, listaItauDOC, itauTrailerLote, itauTrailer);
			} 
			
			//Pagamentos atrav�s de Nota Fiscal � Liquida��o Eletr�nica
			else if (identificador == 32){
				List<ArquivoRemessaItauNF> listaItauNF = arquivobancarioService.setDetalheNFSispagItau(listaDocumentoRemessa, baixarContaBean);
				strArquivo = arquivoRemessa.gerarArquivoRemessaBancoItauNF(itauHeader, itauHeaderLote, listaItauNF, itauTrailerLote, itauTrailer);
			} 
			
			//Liquida��o de t�tulos (bloquetos) em cobran�a no Ita� e em outros Bancos
			else if (identificador == 30 || identificador == 31){
				List<ArquivoRemessaItauLT> listaItauLT = arquivobancarioService.setDetalheLTSispagItau(listaDocumentoRemessa, baixarContaBean);
				strArquivo = arquivoRemessa.gerarArquivoRemessaBancoItauLT(itauHeader, itauHeaderLote, listaItauLT, itauTrailerLote, itauTrailer);
			}
			
			//Pagamento de Contas de Concession�rias e Tributos com c�digo de barras
			else if (identificador == 13){
				List<ArquivoRemessaItauCT> listaItauCT = arquivobancarioService.setDetalheCTSispagItau(listaDocumentoRemessa, baixarContaBean);
				strArquivo = arquivoRemessa.gerarArquivoRemessaBancoItauCT(itauHeader, itauHeaderLote, listaItauCT, itauTrailerLote, itauTrailer);
			}
			
			//Pagamento de Tributos sem c�digo de barras e FGTS-GRF/GRRF/GRDE com c�digo de barras (N�O SER� FEITO AGORA)
			else if (identificador == 35 || identificador == 91){
				throw new SinedException("A forma e tipo de pagamento selecionado para gera��o do arquivo SISPAG n�o � suportada pelo sistema.");
			}
			
			//Caso n�o seja de nenhum segmento
			else {
				throw new SinedException("A forma e tipo de pagamento selecionado para gera��o do arquivo SISPAG n�o � suportada pelo sistema.");
			}
			
			SimpleDateFormat format = new SimpleDateFormat("ddMM");
			Date date = new Date(System.currentTimeMillis());
			String data = format.format(date);
			
			Integer sequencial = conta.getSequencial() != null ? conta.getSequencial() : 1;
			
			String nomeArq = "CB" + data + br.com.linkcom.neo.util.StringUtils.stringCheia(sequencial.toString(), "0", 2, false)+ ".REM";
			
			arquivo = new Arquivo(strArquivo.getBytes(), nomeArq , "text/plain");
			Arquivobancario arquivobancario = new Arquivobancario(nomeArq, Arquivobancariotipo.REMESSA, arquivo, new Date(System.currentTimeMillis()),Arquivobancariosituacao.GERADO);
			
			arquivoDAO.saveFile(arquivobancario, "arquivo");
			arquivoService.saveOrUpdate(arquivo);
			arquivobancarioService.saveOrUpdate(arquivobancario);
			
			contaService.setProximoSequencial(conta);
		
		} catch (SinedException e) {
			throw new SinedException(e.getMessage());
		}		
		catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na gera��o do arquivo de remessa: " + e.getMessage());
		}

		return arquivo;
	}
	
	private Arquivo gerarArquivoBradesco(BaixarContaBean baixarContaBean) {
		
		Arquivo arquivo = null;
		Conta conta = baixarContaBean.getVinculo();
		
		//Carrega todas as carteiras, desta forma utiliza-se a carteira padr�o para gera��o do arquivo remessa
		conta = contaService.carregaConta(conta);
		
		if (conta.getEmpresa() == null){
			Set<Contaempresa> listaContaempresa = new ListSet<Contaempresa>(Contaempresa.class);
			listaContaempresa.add(new Contaempresa(empresaService.loadPrincipal()));
			conta.setListaContaempresa(listaContaempresa);
		} 
		
		String whereIn = CollectionsUtil.listAndConcatenate(baixarContaBean.getListaDocumento(), "cddocumento", ",");
		GerarArquivoRemessaBean filtro = new GerarArquivoRemessaBean();
		filtro.setContas(whereIn);		
		List<Documento> listaDocumentoRemessa = documentoService.findDadosArquivoRemessa(filtro);
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();					

		try {
			ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400(conta, listaDocumentoRemessa.size());				
			List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyRegistroBaixa400(conta, listaDocumentoRemessa);
			
			String strArquivo = arquivoRemessa.gerarArquivoRemessaBradesco(ht, listaRegistros);
			
			SimpleDateFormat format = new SimpleDateFormat("ddMM");
			Date date = new Date(System.currentTimeMillis());
			String data = format.format(date);
			
			Integer sequencial = conta.getSequencial() != null ? conta.getSequencial() : 1;
			
			String nomeArq = "CB" + data + br.com.linkcom.neo.util.StringUtils.stringCheia(sequencial.toString(), "0", 2, false)+ ".REM";
			
			arquivo = new Arquivo(strArquivo.getBytes(), nomeArq , "text/plain");
			Arquivobancario arquivobancario = new Arquivobancario(nomeArq, Arquivobancariotipo.REMESSA, arquivo, new Date(System.currentTimeMillis()),Arquivobancariosituacao.GERADO);
			
			arquivoDAO.saveFile(arquivobancario, "arquivo");
			arquivoService.saveOrUpdate(arquivo);
			arquivobancarioService.saveOrUpdate(arquivobancario);
			
			contaService.setProximoSequencial(conta);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro na gera��o de arquivo de remessa.");
		}
		
		return arquivo;
	}
	
	/**
	 * M�todo criado para que seja aberto uma transaction para salvar tudo s� se n�o der erro.
	 * <p>Por realizar v�rias transa��es, <b>deve ser usado dentro de uma transaction</b> para garantir a integridade
	 * dos dados em caso de erro.</p> 
	 * 
	 * Isto � para v�rias movimenta��es
	 *
	 * @see
	 *
	 * @param request
	 * @param baixarContaBean
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> doBaixarContaVariasMovimentacoes(WebRequestContext request, BaixarContaBean baixarContaBean, boolean fromBaixaParcial){
		List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();
		List<Documento> listaDocumento = baixarContaBean.getListaDocumento();
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
		Conta conta;
		
		Rateio rateioOriginal = new Rateio();
		
		if (baixarContaBean.getRateio()!=null && baixarContaBean.getRateio().getListaRateioitem()!=null){
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			for (Rateioitem rateioitem1: baixarContaBean.getRateio().getListaRateioitem()){
				Rateioitem rateioitem2 = new Rateioitem(); 
				rateioitem2.setContagerencial(rateioitem1.getContagerencial());
				rateioitem2.setCentrocusto(rateioitem1.getCentrocusto());
				rateioitem2.setProjeto(rateioitem1.getProjeto());
				rateioitem2.setValor(rateioitem1.getValor());
				rateioitem2.setPercentual(rateioitem1.getPercentual());
				listaRateioitem.add(rateioitem2);
			}
			rateioOriginal.setListaRateioitem(listaRateioitem);
		}
		
		Double total = 0.0;
		for (Documento documento : listaDocumento) {
			if(documento.getValoratual()!=null)
				total += documento.getValoratual().getValue().doubleValue();
			else
				total += documento.getValor().getValue().doubleValue();
		}
	
		Documentoacao[] condicoes = null;
		if(Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())){
			// CONTA A PAGAR
			condicoes = new Documentoacao[]{Documentoacao.AUTORIZADA, Documentoacao.AUTORIZADA_PARCIAL};
		}else{
			// CONTA A RECEBER
			if(baixarContaBean.getFromRetorno() || baixarContaBean.getFromWebservice() || baixarContaBean.getFromBaixaautomatica()){
				condicoes = new Documentoacao[]{Documentoacao.PREVISTA, Documentoacao.DEFINITIVA};
			} else {				
				condicoes = new Documentoacao[]{Documentoacao.DEFINITIVA, Documentoacao.BAIXADA_PARCIAL};
			}
		}
		
		if(Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())){
			contapagarService.updateStatusConta(Documentoacao.BAIXADA, whereIn, condicoes);		
		} else {
			if(fromBaixaParcial){
				if(baixarContaBean.getDocumento() != null
						&& (baixarContaBean.getDocumento().getValorRestante() == null || 
								baixarContaBean.getDocumento().getValorRestante().getValue().equals(baixarContaBean.getValorRecebido().getValue())))
					contapagarService.updateStatusConta(Documentoacao.BAIXADA, whereIn, condicoes);			
				else
					contapagarService.updateStatusConta(Documentoacao.BAIXADA_PARCIAL, whereIn, condicoes);
			} else {
				contapagarService.updateStatusConta(Documentoacao.BAIXADA, whereIn, condicoes);
			}
		}
		
		Documentohistorico documentoHistorico = null;
		
		for (Documento documento : listaDocumento) {
			documento.setFromRetorno(baixarContaBean.getFromRetorno());
			if(Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())){
				documentoHistorico = documentohistoricoService.gerarHistoricoDocumento(null, Documentoacao.BAIXADA, documento);
			}else {
				if(documento.getValorRestante() == null || baixarContaBean.getValorRecebido().compareTo(documento.getValorRestante()) == 0)
					documentoHistorico = documentohistoricoService.gerarHistoricoDocumento(null, Documentoacao.BAIXADA, documento);
				else
					documentoHistorico = documentohistoricoService.gerarHistoricoDocumento(null, Documentoacao.BAIXADA_PARCIAL, documento);
			}
			documentohistoricoService.saveOrUpdateNoUseTransaction(documentoHistorico);
		}
		
		Movimentacao movimentacao;
		List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao = getListaAjustadaBaixarContaBeanMovimentacao(baixarContaBean, rateioOriginal);
		
		for(BaixarContaBeanMovimentacao baixarContaBeanMovimentacao : listaBaixarContaBeanMovimentacao){		
			if(!baixarContaBeanMovimentacao.getFormapagamento().equals(Formapagamento.ANTECIPACAO)){
				if(baixarContaBeanMovimentacao.getFormapagamento().equals(Formapagamento.VALECOMPRA)){
					boolean mesmoCliente = true;
					Integer cdpessoa = null;
					if(fromBaixaParcial && baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
						cdpessoa = baixarContaBeanMovimentacao.getPessoa() != null ? baixarContaBeanMovimentacao.getPessoa().getCdpessoa() : null;
					}else{
						for (Documento documento : listaDocumento) {
							if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
								if(cdpessoa == null) {
									cdpessoa = documento.getPessoa().getCdpessoa();
								} else if(!documento.getPessoa().getCdpessoa().equals(cdpessoa)){
									mesmoCliente = false;
								}
							} else mesmoCliente = false;
						}
					}
					if(cdpessoa != null && mesmoCliente){
						Cliente cliente = clienteService.load(new Cliente(cdpessoa), "cliente.cdpessoa, cliente.nome");
						if(cliente != null){
							Valecompra valecompra = new Valecompra();
							valecompra.setCliente(cliente);
							valecompra.setIdentificacao("Uso na baixa de documento");
							valecompra.setData(baixarContaBean.getDtpagamento());
							valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
							if (baixarContaBean.getListaBaixarContaBeanMovimentacao()!=null &&
									baixarContaBean.getListaBaixarContaBeanMovimentacao().size()==1 &&
									baixarContaBean.getListaDocumento()!=null &&
									baixarContaBean.getListaDocumento().size()>1 && 
									Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar()) && 
									baixarContaBeanMovimentacao.getValorTotalVinculado() != null &&
									baixarContaBeanMovimentacao.getDocumentovinculado() != null){
								valecompra.setValor(new Money(baixarContaBeanMovimentacao.getValorTotalVinculado()));
								valecompraService.saveOrUpdateNoUseTransaction(valecompra);
								
								Valecompraorigem valecompraorigem = new Valecompraorigem();
								valecompraorigem.setDocumento(baixarContaBeanMovimentacao.getDocumentovinculado());
								valecompraorigem.setValecompra(valecompra);
								
								valecompraorigemService.saveOrUpdateNoUseTransaction(valecompraorigem);
							}else {
								valecompra.setValor(baixarContaBeanMovimentacao.getValor());
								valecompraService.saveOrUpdateNoUseTransaction(valecompra);
								
								for (Documento documento : listaDocumento) {
									Valecompraorigem valecompraorigem = new Valecompraorigem();
									valecompraorigem.setDocumento(documento);
									valecompraorigem.setValecompra(valecompra);
									
									valecompraorigemService.saveOrUpdateNoUseTransaction(valecompraorigem);
								}
							}
							
						}
					}
				} else {
					baixarContaBean = criarBaixarContaBeanApartirBaixarContaBeanMovimentacao(baixarContaBean, baixarContaBeanMovimentacao);
					
					conta = contaService.carregaConta(baixarContaBean.getVinculo());
					baixarContaBean.setVinculo(conta);
					
					//Gera o arquivo de remessa na baixa do documento
					Arquivo arquivo = null;
					try {
						if (conta.getBanco() != null && conta.getBanco().getGerarsispag() != null && conta.getBanco().getNumero() != null &&
							baixarContaBean.getGerararquivoremessa() != null && baixarContaBean.getGerararquivoremessa() && conta.getBanco().getGerarsispag() &&  
							Formapagamento.DEBITOCONTACORRENTE.equals(baixarContaBean.getFormapagamento())){			
							try {
								arquivo = bancoConfiguracaoRemessaService.gerarArquivoRemessaSispag(baixarContaBean);			
							} catch (SinedException e) {
								request.addError(e.getMessage());
							} catch (Exception e) {
								e.printStackTrace();
								request.addError("Erro desconhecido: " + e.getMessage());
							}
						} else if (conta.getBanco() != null && conta.getBanco().getGerarsispag() != null && conta.getBanco().getNumero() != null &&
								conta.getBanco().getNumero().equals(341) && baixarContaBean.getGerararquivoremessa() != null && baixarContaBean.getGerararquivoremessa() && conta.getBanco().getGerarsispag() &&  
								Formapagamento.DEBITOCONTACORRENTE.equals(baixarContaBean.getFormapagamento())){
							arquivo = gerarArquivoSispagItau(baixarContaBean);
						} else if (baixarContaBean.getGerararquivoregistrocobranca() != null && baixarContaBean.getGerararquivoregistrocobranca()){
							arquivo = gerarArquivoBradesco(baixarContaBean); 
						}		
						baixarContaBean.setArquivo(arquivo);
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("Erro ao gerar arquivo de remessa: " + e.getMessage());
					}
					
					movimentacao = new Movimentacao();
					
					baixarContaBean.getRateio().setCdrateio(null);
					for (Rateioitem rateioitem : baixarContaBean.getRateio().getListaRateioitem()) {
						rateioitem.setCdrateioitem(null);
						if (rateioitem.getProjeto() == null || rateioitem.getProjeto().getCdprojeto() == null) {
							rateioitem.setProjeto(null);
						}
					}
					rateioService.atualizaValorRateio(baixarContaBean.getRateio(), new Money(baixarContaBean.getValorTotal()));
					rateioService.saveOrUpdateNoUseTransaction(baixarContaBean.getRateio());
					
					if(baixarContaBeanMovimentacao.getDocumentovinculado() != null){
						baixarContaBean.setDocumentoVinculado(baixarContaBeanMovimentacao.getDocumentovinculado());
					}else if(listaDocumento != null && listaDocumento.size() == 1 && baixarContaBean.getVinculo() != null){
						Empresa empresa = listaDocumento.get(0).getEmpresa();
						if(empresa != null && contaService.isPermissaoConta(baixarContaBean.getVinculo(), empresa)){
							baixarContaBean.setEmpresa(empresa);
						}
					}else if(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_EMPRESA) && listaDocumento != null && listaDocumento.size() > 1 && baixarContaBean.getVinculo() != null){
						List<Empresa> listaEmpresa = new ArrayList<Empresa>();
						boolean empresaNula = false;
						for(Documento documento : listaDocumento){
							if(documento.getEmpresa() == null){
								empresaNula = true;
							}else if(documento.getEmpresa() != null && !listaEmpresa.contains(documento.getEmpresa())){
								listaEmpresa.add(documento.getEmpresa());
							}
						}
						if(!empresaNula && listaEmpresa.size() == 1){
							Empresa empresa = listaDocumento.get(0).getEmpresa();
							if(empresa != null && contaService.isPermissaoConta(baixarContaBean.getVinculo(), empresa)){
								baixarContaBean.setEmpresa(empresa);
							}
						}
					}

					List<Documento> listaDocumentoMovimentacao = new ArrayList<Documento>();
					if(baixarContaBeanMovimentacao.getDocumentovinculado() != null){
						listaDocumentoMovimentacao.add(baixarContaBeanMovimentacao.getDocumentovinculado());
					}else {
						listaDocumentoMovimentacao = listaDocumento;
					}
					movimentacaoService.salvarMovimentacao(baixarContaBean, movimentacao, listaDocumentoMovimentacao);
					movimentacaoorigemService.salvarMovimentacaoOrigem(listaDocumentoMovimentacao, movimentacao, listaMovimentacao, baixarContaBean);
					
					if(movimentacao != null && movimentacao.getCheque() != null && movimentacao.getCheque().getCdcheque() != null){
						movimentacao.getCheque().setChequesituacao(Movimentacaoacao.CONCILIADA.equals(movimentacao.getMovimentacaoacao()) ? Chequesituacao.COMPENSADO : Chequesituacao.BAIXADO);
						chequeService.updateSituacao(movimentacao.getCheque().getCdcheque().toString(), movimentacao.getCheque().getChequesituacao());
						Chequehistorico chequehistorico = null;
						if(Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse())){
							chequehistorico = chequehistoricoService.criaHistorico(movimentacao.getCheque(), listaDocumento, movimentacao,Documentoclasse.OBJ_RECEBER);
							if(chequehistorico != null){
								chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
							}
						}else if(Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())){
							for(Documento documentoByCheque : listaDocumento){
								if(documentoByCheque.getCheque() != null && documentoByCheque.getCheque().getCdcheque() != null){
									chequehistorico = chequehistoricoService.criaHistorico(movimentacao.getCheque(), documentoByCheque, movimentacao, Documentoclasse.OBJ_PAGAR);
									if(chequehistorico != null){
										chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
									}
								}
							}
							if(movimentacao.getCheque() != null && movimentacao.getCheque().getCdcheque() != null){
								chequehistorico = chequehistoricoService.criaHistorico(movimentacao.getCheque(), listaDocumento, movimentacao, Documentoclasse.OBJ_PAGAR);
								if(chequehistorico != null){
									chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
								}
							}
						}
					}
					
					movimentacao.setConta(conta);
					listaMovimentacao.add(movimentacao);
				}
			}
		}
		
		if (Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())
				&& Boolean.TRUE.equals(baixarContaBean.getGerarValeCompra())
				&& baixarContaBean.getValorvalecompra()!=null
				&& baixarContaBean.getValorvalecompra().toLong()>0L
				&& isMesmaPessoa(listaDocumento)){
			Cliente cliente = clienteService.load(new Cliente(listaDocumento.get(0).getPessoa().getCdpessoa()), "cliente.cdpessoa, cliente.nome");
			
			if (cliente!=null){
				Valecompra valecompra = new Valecompra();
				valecompra.setCliente(cliente);
				valecompra.setIdentificacao("Referente ao valor restante da baixa da(s) conta(s) " + CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ", ") + ".");
				valecompra.setData(baixarContaBean.getDtpagamento());
				valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
				valecompra.setValor(baixarContaBean.getValorvalecompra());
				valecompraService.saveOrUpdateNoUseTransaction(valecompra);
				
				for (Documento documento: listaDocumento) {
					Valecompraorigem valecompraorigem = new Valecompraorigem();
					valecompraorigem.setDocumento(documento);
					valecompraorigem.setValecompra(valecompra);
					valecompraorigemService.saveOrUpdateNoUseTransaction(valecompraorigem);
				}
			}
		}
		
		return listaMovimentacao;
	}
		
	private List<BaixarContaBeanMovimentacao> getListaAjustadaBaixarContaBeanMovimentacao(BaixarContaBean baixarContaBean, Rateio rateioOriginal) {
		List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao = new ArrayList<BaixarContaBeanMovimentacao>();
		
		if (baixarContaBean.getListaBaixarContaBeanMovimentacao()!=null &&
				baixarContaBean.getListaBaixarContaBeanMovimentacao().size()==1 &&
				baixarContaBean.getListaDocumento()!=null &&
				baixarContaBean.getListaDocumento().size()>1 && 
				Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
			for (Documento documento: baixarContaBean.getListaDocumento()){
				BaixarContaBeanMovimentacao bean = baixarContaBean.getListaBaixarContaBeanMovimentacao().get(0).getClone();
				bean.setDocumentovinculado(documento);
				bean.setValorTotalVinculado(documento.getValoratual().getValue().doubleValue());
				/*Caso a op��o "GERAR UMA MOVIMENTA��O PARA CADA CONTA A PAGAR(OU RECEBER)" seja marcada, o campo documento � ocultado
				 * e na movimenta��o financeira o N� de documento ser� o mesmo N� de documento da conta*/
				if((Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse())||Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse()))&&
						Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar()) &&
						parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
					Documento docAux = documentoService.load(documento);
					bean.setChecknum(docAux.getNumero());
				}
				
//				Rateio rateio = new Rateio();
//				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
//				
//				for (Rateioitem rateioitemOriginal: rateioOriginal.getListaRateioitem()){
//					Rateioitem rateioitem = new Rateioitem(); 
//					rateioitem.setContagerencial(rateioitemOriginal.getContagerencial());
//					rateioitem.setCentrocusto(rateioitemOriginal.getCentrocusto());
//					rateioitem.setProjeto(rateioitemOriginal.getProjeto());
//					rateioitem.setValor(documento.getValoratual().multiply(new Money(rateioitemOriginal.getPercentual())).divide(new Money(100)));
//					rateioitem.setPercentual(rateioitemOriginal.getPercentual());
//					listaRateioitem.add(rateioitem);
//				}
//				rateio.setListaRateioitem(listaRateioitem);
				
				Rateio rateio = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				
				for (Rateioitem rateioitemOriginal: rateioService.findByDocumento(documento).getListaRateioitem()){
					Rateioitem rateioitem = new Rateioitem(); 
					rateioitem.setContagerencial(rateioitemOriginal.getContagerencial());
					rateioitem.setCentrocusto(rateioitemOriginal.getCentrocusto());
					rateioitem.setProjeto(rateioitemOriginal.getProjeto());
					rateioitem.setValor(documento.getValoratual().multiply(new Money(rateioitemOriginal.getPercentual())).divide(new Money(100)));
					rateioitem.setPercentual(rateioitemOriginal.getPercentual());
					rateioitem.setObservacao(rateioitemOriginal.getObservacao());
					listaRateioitem.add(rateioitem);
				}
				rateio.setListaRateioitem(listaRateioitem);
				
				bean.setRateiovinculado(rateio);
				
				if (bean.getHistorico()==null || bean.getHistorico().trim().equals("")){
					bean.setHistorico(documento.getDescricao());
				}

				listaBaixarContaBeanMovimentacao.add(bean);
			}
		}else {
			if(baixarContaBean.getListaDocumento() != null 
					&& baixarContaBean.getListaDocumento().size() == 1 
					&& (Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse()) || Documentoclasse.OBJ_PAGAR.equals(baixarContaBean.getDocumentoclasse())) 
					&& Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar()) 
					&& parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
				Documento docAux = documentoService.load(baixarContaBean.getListaDocumento().get(0), "documento.cddocumento, documento.numero");
				
				for(BaixarContaBeanMovimentacao baixarContaBeanMov : baixarContaBean.getListaBaixarContaBeanMovimentacao()){
					baixarContaBeanMov.setChecknum(docAux.getNumero());
				}
				
			}
			listaBaixarContaBeanMovimentacao = baixarContaBean.getListaBaixarContaBeanMovimentacao();
		}
		
		return listaBaixarContaBeanMovimentacao;
	}
	
	public BaixarContaBeanMovimentacao criarCopiaBaixarContaBeanMovimentacao(BaixarContaBeanMovimentacao baixarContaBeanMovimentacao){
		BaixarContaBeanMovimentacao bean = new BaixarContaBeanMovimentacao();
		bean.setDtpagamento(baixarContaBeanMovimentacao.getDtpagamento());
		bean.setFormapagamento(baixarContaBeanMovimentacao.getFormapagamento());
		bean.setVinculo(baixarContaBeanMovimentacao.getVinculo());
		bean.setContatipo(baixarContaBeanMovimentacao.getContatipo());
		bean.setFinalidadedoc(baixarContaBeanMovimentacao.getFinalidadedoc());
		bean.setFinalidadeted(baixarContaBeanMovimentacao.getFinalidadeted());
		bean.setBancoformapagamento(baixarContaBeanMovimentacao.getBancoformapagamento());
		bean.setBancotipopagamento(baixarContaBeanMovimentacao.getBancotipopagamento());
		bean.setChecknum(baixarContaBeanMovimentacao.getChecknum());
		bean.setCheque(baixarContaBeanMovimentacao.getCheque());
		bean.setArquivo(baixarContaBeanMovimentacao.getCdarquivo());
		bean.setGerararquivoregistrocobranca(baixarContaBeanMovimentacao.getGerararquivoregistrocobranca());
		bean.setGerararquivoremessa(baixarContaBeanMovimentacao.getGerararquivoremessa());
		bean.setHistorico(baixarContaBeanMovimentacao.getHistorico());
		bean.setComprovante(baixarContaBeanMovimentacao.getComprovante());
		
		return bean;
	}
	
	/**
	 * M�todo que cria uma BaixarContaBean a partir de uma BaixarContaBeanMovimentacao
	 *
	 * @param baixarContaBean
	 * @param baixarContaBeanMovimentacao
	 * @return
	 * @author Luiz Fernando
	 */
	public BaixarContaBean criarBaixarContaBeanApartirBaixarContaBeanMovimentacao(BaixarContaBean baixarContaBean, BaixarContaBeanMovimentacao baixarContaBeanMovimentacao){
		baixarContaBean.setDtpagamento(baixarContaBeanMovimentacao.getDtpagamento());
		baixarContaBean.setFormapagamento(baixarContaBeanMovimentacao.getFormapagamento());
		baixarContaBean.setVinculo(baixarContaBeanMovimentacao.getVinculo());
		baixarContaBean.setContatipo(baixarContaBeanMovimentacao.getContatipo());
		baixarContaBean.setFinalidadedoc(baixarContaBeanMovimentacao.getFinalidadedoc());
		baixarContaBean.setFinalidadeted(baixarContaBeanMovimentacao.getFinalidadeted());
		baixarContaBean.setBancoformapagamento(baixarContaBeanMovimentacao.getBancoformapagamento());
		baixarContaBean.setBancotipopagamento(baixarContaBeanMovimentacao.getBancotipopagamento());
		baixarContaBean.setChecknum(baixarContaBeanMovimentacao.getChecknum());
		baixarContaBean.setCheque(baixarContaBeanMovimentacao.getCheque());
		baixarContaBean.setArquivo(baixarContaBeanMovimentacao.getCdarquivo());
		baixarContaBean.setGerararquivoregistrocobranca(baixarContaBeanMovimentacao.getGerararquivoregistrocobranca());
		baixarContaBean.setGerararquivoremessa(baixarContaBeanMovimentacao.getGerararquivoremessa());
		baixarContaBean.setHistorico(baixarContaBeanMovimentacao.getHistorico());
		baixarContaBean.setComprovante(baixarContaBeanMovimentacao.getComprovante());
		
		if(baixarContaBeanMovimentacao.getValor() != null)
			baixarContaBean.setValorTotal(baixarContaBeanMovimentacao.getValor().getValue().doubleValue());
		if(baixarContaBeanMovimentacao.getValortaxa() != null)
			baixarContaBean.setValortaxaTotal(baixarContaBeanMovimentacao.getValortaxa().getValue().doubleValue());
		
		if (Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
			if(baixarContaBeanMovimentacao.getDocumentovinculado() != null && baixarContaBeanMovimentacao.getDocumentovinculado().getValoratualbaixaSemTaxa() != null && 
					baixarContaBeanMovimentacao.getDocumentovinculado().getValoratualbaixaSemTaxa().getValue().doubleValue() > 0){
				baixarContaBean.setValorTotal(baixarContaBeanMovimentacao.getDocumentovinculado().getValoratualbaixaSemTaxa().getValue().doubleValue());
			}else {
				baixarContaBean.setValorTotal(baixarContaBeanMovimentacao.getValorTotalVinculado());
			}
			baixarContaBean.setRateio(baixarContaBeanMovimentacao.getRateiovinculado());			
		}		
		
		return baixarContaBean;
	}
	
	public void updateNossoNumero(Documento documento) {
		documentoDAO.updateNossoNumero(documento);
	}
	
	public void updateAssociadomanualmente(Documento documento) {
		documentoDAO.updateAssociadomanualmente(documento);
	}
	
	/**
	 * M�todo para obter o valor total da lista de documentos. Soma e retorna o valor de todas os documentos.
	 * Pode usar para somar o valor atual, definido pela tabela Aux_documento ou o pr�prio valor do documento.
	 * 
	 * @param listaDocumento
	 * @param useValorAtual - true se for considerar o valor atual do documento, valor obtido pela fun��o ValorAtual do banco.
	 * 						false se for para considerar o valor cadastrado do documento.
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money valorTotalLista(List<Documento> listaDocumento, boolean useValorAtual){
		Money total = new Money();
		for (Documento d : listaDocumento) {
			if(useValorAtual) total = total.add(d.getAux_documento().getValoratual());
			else total = total.add(d.getValor());
		}
		return total;
	}
	
	/**
	 * Refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findByNota(Nota)
	 * @param nota
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findByNota(Nota nota){
		return documentoDAO.findByNota(nota);
	}
	
	public List<Documento> findNotaDiferenteCanceladaNegociada(Nota nota){
		return documentoDAO.findNotaDiferenteCanceladaNegociada(nota);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.DocumentoService#findSaldoAntecipacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<GenericBean> findSaldoAntecipacao(String whereIn, Documentoclasse documentoClasse) {
		return documentoDAO.findSaldoAntecipacao(whereIn, documentoClasse);
	}
	
	/**
	 * Refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAntecipacaoConsideraPedidoVenda
	 * @param cliente
	 * @param documentotipo
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForAntecipacaoConsideraVenda(Cliente cliente, Documentotipo documentotipo){
		return documentoDAO.findForAntecipacao(cliente, documentotipo, false, null, Documentoclasse.OBJ_RECEBER);
	}
	
	/**
	 * Refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAntecipacaoConsideraPedidoVenda
	 * @param cliente
	 * @param documentotipo
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForAntecipacaoConsideraPedidoVenda(Pessoa pessoa, Documentotipo documentotipo, Pedidovenda pedidovenda, Documentoclasse documentoClasse){
		return documentoDAO.findForAntecipacao(pessoa, documentotipo, true, pedidovenda, documentoClasse);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAntecipacao(Pessoa pessoa, Documentotipo documentotipo, boolean consideraPedido, Pedidovenda pedidovenda)
	*
	* @param pessoa
	* @param documentotipo
	* @param pedidovenda
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public List<Documento> findForAntecipacaoConsideraPedidoVenda(Pessoa pessoa, Documentotipo documentotipo, Pedidovenda pedidovenda){
		return documentoDAO.findForAntecipacao(pessoa, documentotipo, true, pedidovenda, Documentoclasse.OBJ_RECEBER);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findForAntecipacaoConsideraVendaUtilizados(Cliente cliente, Documentotipo documentotipo)
	 *
	 * @param cliente
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForAntecipacaoConsideraVendaUtilizados(Cliente cliente, Documentotipo documentotipo) {
		return documentoDAO.findForAntecipacaoConsideraVendaUtilizados(cliente, documentotipo, false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAntecipacaoEntrega(Fornecedor fornecedor, Documentotipo documentotipo)
	 *
	 * @param fornecedor
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForAntecipacaoEntrega(Pessoa pessoa, Documentotipo documentotipo) {
		return documentoDAO.findForAntecipacaoEntrega(pessoa, documentotipo);
	}
	
	public List<Documento> findForAntecipacaoEntregaContaReceber(Pessoa pessoa, Documentotipo documentotipo) {
		return documentoDAO.findForAntecipacaoEntregaContaReceber(pessoa, documentotipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAntecipacaoConsideraEntregaUtilizados(Fornecedor fornecedor, Documentotipo documentotipo)
	 *
	 * @param fornecedor
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForAntecipacaoConsideraEntregaUtilizados(Fornecedor fornecedor, Documentotipo documentotipo) {
		return documentoDAO.findForAntecipacaoConsideraEntregaUtilizados(fornecedor, documentotipo);
	}
	
	/**
	 * Monta uma lista de Tipo de pagamento com base na classe do documento
	 * para as telas de conta a pagar/receber.
	 * 
	 * @param tipooperacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tipopagamento> findTipopagamentoForDocumento(Tipooperacao tipooperacao){
		List<Tipopagamento> lista = new ArrayList<Tipopagamento>();
		for(Tipopagamento tipo : Tipopagamento.values()) {
			lista.add(tipo);
		}
		return lista;
	}
	
	public List<Tipopagamento> findTipopagamentoForDocumento(Tipooperacao tipooperacao, boolean considerarTipoOperacao){
		if(considerarTipoOperacao) {
			List<Tipopagamento> list = new ArrayList<Tipopagamento>();
			list.add(Tipopagamento.CLIENTE);
			list.add(Tipopagamento.COLABORADOR);
			if(tipooperacao == null || tipooperacao.equals(Tipooperacao.TIPO_DEBITO)){
				list.add(Tipopagamento.FORNECEDOR);	
			}
			list.add(Tipopagamento.OUTROS);
			return list;
		}
		
		return findTipopagamentoForDocumento(tipooperacao);
	}
	
	public boolean isAlowToSaveBean(WebRequestContext request, Documento bean) {
		Documento save = (Documento) request.getSession().getAttribute(bean.getClass().getName());
		if(save != null && save.equalsToSave(bean)){
			return false;
		}
		request.getSession().setAttribute(bean.getClass().getName(), bean);
		return true;
	}
	
	/**
	 * M�todo para buscar o valor atual de uma lista de documentos set�-los nos beans.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#obterListaValorAtual(String)
	 * @param listaDocumento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> obterListaValorAtual(List<Documento> listaDocumento){
		List<Documento> listaValorAtual = null;
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
			listaValorAtual = documentoDAO.obterListaValorAtual(whereIn);
			for (Documento doc : listaValorAtual) {
				doc.setValoratual(doc.getAux_documento().getValoratual());
			}
		}
		return listaValorAtual;
	}
	
	/**
	 * <p>M�todo para gerar o hist�rico quando o documento � alterado.</p>
	 * 
	 * <pre>Para preencher a a��o (<b>Documentoacao</b>) do hist�rico, o m�todo insere a propriedade 
	 * <b>acaohistorico</b> se ela n�o for nula. Caso contr�rio ele insere a propriedade  <b>documentoacao</b> padr�o
	 * que est� preenchida no bean</pre>
	 * 
	 * @see #carregaDocumento(Documento)
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	public void atualizaHistoricoDocumento(Documento bean, Boolean alteradoPeloUsuario){
		Documentohistorico documentohistorico = null;
		Set<Documentohistorico> listaDocumentohistorico = null;
		String textoAlterouVencimento = bean.getTextoAlterouVencimento(); 
		
		if (bean.getCddocumento() != null) {
			Documento documento = this.carregaDocumento(bean);
			listaDocumentohistorico = documento.getListaDocumentohistorico();
			
			if (alteradoPeloUsuario) {
				documento.setAcaohistorico(Documentoacao.ALTERADA_PELO_USUARIO);
			} else {
				documento.setAcaohistorico(Documentoacao.ALTERADA);
			}

			
			if (textoAlterouVencimento!=null){
				documento.setObservacaoHistorico(textoAlterouVencimento + ". " + Util.strings.emptyIfNull(documento.getObservacaoHistorico()));
			}
			documentohistorico = new Documentohistorico(documento);
		} else if (bean.getListaDocumentohistorico() != null && bean.getListaDocumentohistorico().size() > 0) {
			return; // O hist�rico inicial j� � preenchido em rotina pr�pria na cria��o de conta via negocia��o
		} else {
			if (textoAlterouVencimento!=null){
				bean.setObservacaoHistorico(textoAlterouVencimento + ". " + Util.strings.emptyIfNull(bean.getObservacaoHistorico()));
			}
			documentohistorico = new Documentohistorico(bean);
		}
		if(listaDocumentohistorico == null){
			listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		}
		listaDocumentohistorico.add(documentohistorico);
		bean.setListaDocumentohistorico(listaDocumentohistorico);
	}

	/**
	 * M�todo respons�vel por gerar a lista de financiamento do documento.
	 * 
	 * @param documento
	 * @return 
	 * @author Fl�vio Tavares
	 * @author Hugo Ferreira
	 */
	public List<Documento> geraFinanciamento(Documento documento){
		if(documento.getDtvencimento() == null) throw new SinedException("O par�metro dtvencimento n�o pode ser null");

		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(documento.getPrazo());
		List<Documento> listaDoc = new ArrayList<Documento>();
		Calendar ultimaData = Calendar.getInstance();
		Documento docTmp = null;

		int parcela = 1;
		for (int i = 0; i < documento.getRepeticoes(); i++) {
			/*
			 * Se a listaDoc for vazia, pega a dtVencimento de documento, sen�o,
			 * pega a dtVencimento do �ltimo item da lista.
			 */
			long millis = listaDoc.isEmpty() ? documento.getDtvencimento().getTime() : listaDoc.get(listaDoc.size()-1).getDtvencimento().getTime();
			Prazopagamentoitem prazoItem = null;
			int begin = 0;
			if (listaPrazoItem.size() > 1)
				begin = 1;
			
			for (int j = begin; j < listaPrazoItem.size(); j++) {
				prazoItem = listaPrazoItem.get(j);
				docTmp = new Documento();
				ultimaData.setTimeInMillis(millis);
				Boolean considerarDiaUtilDentroDoMes = (prazoItem.getPrazopagamento() != null && prazoItem.getPrazopagamento().getDataparcelaultimodiautilmes() != null && 
						prazoItem.getPrazopagamento().getDataparcelaultimodiautilmes()) ? Boolean.TRUE : Boolean.FALSE;
				
				ultimaData.add(Calendar.DAY_OF_MONTH, prazoItem.getDias() != null ? prazoItem.getDias() : 0);
				ultimaData.add(Calendar.MONTH, prazoItem.getMeses() != null ? prazoItem.getMeses() : 0);
				
				docTmp.setParcela(++parcela);
				
				Date dtVenc = SinedDateUtils.getProximaDataUtil(new Date(ultimaData.getTimeInMillis()), documento.getEmpresa(), considerarDiaUtilDentroDoMes);
				docTmp.setDtvencimento(dtVenc);
				
				docTmp.setNumero(documento.getNumero());
				docTmp.setDocumentoacao(documento.getDocumentoacao());
				docTmp.setValor(documento.getValor());
				
				listaDoc.add(docTmp);
			}
		}
		return listaDoc;
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForSomaListagem(DocumentoFiltro)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findForSomaListagem(DocumentoFiltro filtro){
		return documentoDAO.findForSomaListagem(filtro);
	}
	
	/**
	 * M�todo para corrigir os valores do rateio para documentos com financiamento.
	 * 
	 * @see #criarEstruturaParcelamento(Documento)
	 * @see #corrigeValoresRateio(Documento)
	 * @see br.com.linkcom.sined.geral.service.ParcelamentoService#saveOrUpdate(Parcelamento)
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public Parcelamento salvaDocumentoFinanciado(Documento documento){
		Parcelamento parcelamento = this.criarEstruturaParcelamento(documento);
		this.corrigeValoresRateio(parcelamento);
		this.verificaTipoDocumento(parcelamento);
		parcelamentoService.saveOrUpdate(parcelamento);
		return parcelamento;
	}
	
	/**
	 * M�todo para corrigir os valores do rateio para documentos com financiamento.
	 * 
	 * @see #criarEstruturaParcelamento(Documento)
	 * @see #corrigeValoresRateio(Documento)
	 * @see br.com.linkcom.sined.geral.service.ParcelamentoService#saveOrUpdate(Parcelamento)
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void salvaDocumentoFinanciadoSemTransacao(Documento documento){
		Parcelamento parcelamento = this.criarEstruturaParcelamento(documento);
		this.corrigeValoresRateio(parcelamento);
		parcelamentoService.saveOrUpdateNoUseTransaction(parcelamento);
	}
	
	/**
	 * M�todo para corrigir os valores dos rateios com base nos percentuais. Em caso de o valor do documento financiado
	 * for diferente do valor do documento inicial.
	 * 
	 * @param parcelamento
	 * @author Fl�vio Tavares
	 */
	private void corrigeValoresRateio(Parcelamento parcelamento){
		for (Parcela parcela : parcelamento.getListaParcela()) {
			Rateio rateio = parcela.getDocumento().getRateio();
			Money valor = parcela.getDocumento().getValor();
			for (Rateioitem item : rateio.getListaRateioitem()) {
				Money valorItem = valor.multiply(new Money(item.getPercentual())).divide(new Money(100));
				item.setValor(valorItem);
			}
			verificaDiferenca(rateio, valor);
		}
	}
	
	private void verificaTipoDocumento(Parcelamento parcelamento){
		for (Parcela parcela : parcelamento.getListaParcela()) {
			Documentotipo documentotipo = parcela.getDocumento().getDocumentotipo();
			if(documentotipo != null){
				Documentotipo docTipo = documentotipoService.load(documentotipo, "documentotipo.cddocumentotipo, documentotipo.boleto");
				if(docTipo != null && (docTipo.getBoleto() == null || !docTipo.getBoleto())){
					parcela.getDocumento().setConta(null);
					parcela.getDocumento().setContacarteira(null);
					removerTaxaJuros(parcela.getDocumento());
				}
			}
		}
	}
	
	private void removerTaxaJuros(Documento documento) {
		if(documento != null && documento.getConta() == null && documento.getTaxa() != null && SinedUtil.isListNotEmpty(documento.getTaxa().getListaTaxaitem())){
			Iterator<Taxaitem> iterator = documento.getTaxa().getListaTaxaitem().iterator();
			while (iterator.hasNext()) {
				Taxaitem taxaitem = (Taxaitem) iterator.next();
				if(Tipotaxa.JUROS.equals(taxaitem.getTipotaxa())){
					iterator.remove();
				}
			}
		}
		
	}
	/**
	 * M�todo que verifica diferenca no total do rateio
	 *
	 * @param rateio
	 * @param valor
	 * @author Luiz Fernando
	 */
	private void verificaDiferenca(Rateio rateio, Money valor) {
		if(rateio != null && rateio.getListaRateioitem() != null && !rateio.getListaRateioitem().isEmpty() && valor != null){
			Double valortotal = 0.0;
			Double diferenca = 0.0;
			Integer i = 0;
			for (Rateioitem item : rateio.getListaRateioitem()) {
				valortotal += SinedUtil.round(item.getValor().getValue(),2).doubleValue();
				if((i+1)  == rateio.getListaRateioitem().size()){
					if(SinedUtil.round(valor.getValue(),2).doubleValue() > valortotal){
						diferenca = SinedUtil.round(valor.getValue(),2).doubleValue() - valortotal;
						if(diferenca != null && diferenca > 0){
							item.setValor(item.getValor().add(new Money(diferenca)));
						}
					}else {
						diferenca = valortotal - SinedUtil.round(valor.getValue(),2).doubleValue();
						if(diferenca != null && diferenca > 0){
							item.setValor(item.getValor().subtract(new Money(diferenca)));
						}
					}
					
				}else {
					i++;
				}
				
			}						
		}		
	}
	
	/**
	 * M�todo para montar e salvar a lista de documento caso a conta seja financiada.
	 * 
	 * @see #atualizaHistoricoDocumento(Documento)
	 * @param docPrincipal
	 * @author Fl�vio Tavares
	 */
	public Parcelamento criarEstruturaParcelamento(Documento docPrincipal){
		List<Documento> listaDocs = docPrincipal.getListaDocumento();
		listaDocs.add(0, docPrincipal);
		
		Conta conta = docPrincipal.getConta();
		Contacarteira contacarteira = docPrincipal.getContacarteira();
		
		Parcelamento parcelamento = new Parcelamento();
		parcelamento.setPrazopagamento(docPrincipal.getPrazo());
		parcelamento.setIteracoes(listaDocs.size());
		parcelamento.setListaParcela(new ListSet<Parcela>(Parcela.class));
		
		int ordem = 1;
		Parcela parcela = null;
		Integer totalParcela = listaDocs.size();
		for (Documento doc : listaDocs) {
			doc.setDocumentoclasse(docPrincipal.getDocumentoclasse());
			doc.setEmpresa(docPrincipal.getEmpresa());
			doc.setFornecedor(docPrincipal.getFornecedor());
			doc.setCliente(docPrincipal.getCliente());
			doc.setDescricao(doc.getDescricao() == null || doc.getDescricao().equals("") ? docPrincipal.getDescricao() : doc.getDescricao());
			if(doc.getDocumentotipoTransient() != null){
				doc.setDocumentotipo(doc.getDocumentotipoTransient());
			}
			doc.setDocumentotipo(doc.getDocumentotipo() == null || doc.getDocumentotipo().getCddocumentotipo() == null ? docPrincipal.getDocumentotipo() : doc.getDocumentotipo());
			doc.setObservacaoHistorico(docPrincipal.getObservacaoHistorico());
			doc.setDtemissao(docPrincipal.getDtemissao());
			doc.setDtcompetencia(docPrincipal.getDtcompetencia());
			doc.setTipopagamento(docPrincipal.getTipopagamento());
			doc.setOutrospagamento(docPrincipal.getOutrospagamento());
			doc.setPessoa(docPrincipal.getPessoa());
			doc.setIndicecorrecao(docPrincipal.getIndicecorrecao());
			doc.setAgendamentoservicodocumento(docPrincipal.getAgendamentoservicodocumento());
			doc.setVinculoProvisionado(docPrincipal.getVinculoProvisionado());
			
			doc.setConta(conta);
			doc.setContacarteira(contacarteira);
			doc.setEndereco(docPrincipal.getEndereco());
			doc.setMensagem1(docPrincipal.getMensagem1());
			doc.setMensagem2(docPrincipal.getMensagem2());
			doc.setMensagem3(docPrincipal.getMensagem3());
			doc.setMensagem4(docPrincipal.getMensagem4());
			doc.setMensagem5(docPrincipal.getMensagem5());
			doc.setMensagem6(docPrincipal.getMensagem6());
			
			doc.setIdsFornecimento(docPrincipal.getIdsFornecimento());
			doc.setFromFornecimento(docPrincipal.getFromFornecimento());
			
			doc.setCdNota(docPrincipal.getCdNota());
			if(docPrincipal.getCdNota() != null){
				doc.setObservacaoHistorico("Receita da nota <a href=\"javascript:visualizarNota(" + docPrincipal.getCdNota() + ");\">" +  (docPrincipal.getNumeroNota() != null ? docPrincipal.getNumeroNota() : docPrincipal.getCdNota().toString()) + "</a>.");
			}
			
			doc.setEntrega(docPrincipal.getEntrega());
			if(docPrincipal.getEntrega() != null && docPrincipal.getEntrega().getCdentrega() != null){
				doc.setObservacaoHistorico("Origem entrega: <a href=\"javascript:visualizaEntrega("+ docPrincipal.getEntrega().getCdentrega() +");\">"+ docPrincipal.getEntrega().getCdentrega() +"</a>.");
			}
			if(SinedUtil.isListNotEmpty(docPrincipal.getListaApropriacao())){
				List<DocumentoApropriacao> listaApropriacao = new ArrayList<DocumentoApropriacao>();
				for(DocumentoApropriacao apropriacao : docPrincipal.getListaApropriacao()){
					DocumentoApropriacao docApropriacao = new DocumentoApropriacao();
					docApropriacao.setMesAno(apropriacao.getMesAno());
					docApropriacao.setValor(apropriacao.getValor());
					listaApropriacao.add(docApropriacao);
				}
				doc.setListaApropriacao(listaApropriacao);
				ajustaValoresApropriacao(doc, docPrincipal.getValor());
			}
			Entrega entrega = null;
			String whereInEntregas = docPrincipal.getWhereInEntrega();
			List<Entrega> listaEntregas = new ArrayList<Entrega>();
			if (docPrincipal.getEntrega() != null){
				entrega = docPrincipal.getEntrega();
				doc.setObservacaoHistorico("Origem entrega: <a href=\"javascript:visualizaEntrega("+ entrega.getCdentrega() +");\">"+ entrega.getCdentrega() +"</a>.");
			}
			if(whereInEntregas != null){
				listaEntregas = entregaService.listEntregaForFaturar(whereInEntregas);
				String obs = "Origem entrega: ";
				int tam = 0, i = 1;
				if(listaEntregas != null && !listaEntregas.isEmpty()){
					StringBuilder whereInEntregadocumento = new StringBuilder();
					tam = listaEntregas.size();					
					for(Entrega e : listaEntregas){
						if(e.getListaEntregadocumento() != null && !e.getListaEntregadocumento().isEmpty()){
							for(Entregadocumento ed : e.getListaEntregadocumento()){
								if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
									if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
										if(!whereInEntregadocumento.toString().equals("")) whereInEntregadocumento.append(",");
										whereInEntregadocumento.append(ed.getCdentregadocumento());
									}
								}
							}
						}
						if(i == tam){
							obs += "<a href=\"javascript:visualizaEntrega("+ e.getCdentrega() +");\">"+ e.getCdentrega() +"</a> ";
						}else{
							obs += "<a href=\"javascript:visualizaEntrega("+ e.getCdentrega() +");\">"+ e.getCdentrega() +"</a>, ";
						}
						i++;
					}
					doc.setObservacaoHistorico(obs);
					if(whereInEntregadocumento != null && !"".equals(whereInEntregadocumento.toString())){
						entradafiscalService.updateSituacaoEntradafiscal(whereInEntregadocumento.toString(), Entregadocumentosituacao.FATURADA);
					}
				}
			}
			
			String whereInEntregadocumento = docPrincipal.getWhereInEntregadocumento();
			if(whereInEntregadocumento != null && !"".equals(whereInEntregadocumento)){
				String obs = "Origem entrada fiscal: ";
				int tam = 0, i = 1;
				if(whereInEntregadocumento.split(",").length > 0){
					for(String idEntradafiscal : whereInEntregadocumento.split(",")){
						if(i == tam){
							obs += "<a href=\"javascript:visualizaEntradafiscal("+ idEntradafiscal +");\">"+ idEntradafiscal +"</a> ";
						}else{
							obs += "<a href=\"javascript:visualizaEntradafiscal("+ idEntradafiscal +");\">"+ idEntradafiscal +"</a>, ";
						}
						i++;
					}
					
					obs = obs.trim();
					if (obs.endsWith(",")){
						obs = obs.substring(0, obs.length()-1);
					}
					
					docPrincipal.setObservacaoHistorico(obs);
				}
			}
			
			doc.setWhereInOrdemcompra(docPrincipal.getWhereInOrdemcompra());
			if(docPrincipal.getWhereInOrdemcompra() != null && !"".equals(docPrincipal.getWhereInOrdemcompra())){
				doc.setObservacaoHistorico("Origem de ordem de compra (Adiantamento) : <a href=\"javascript:visualizarOrdemcompra("+ docPrincipal.getWhereInOrdemcompra() +");\">"+ docPrincipal.getWhereInOrdemcompra() +"</a>.");
			}
			
			doc.setContrato(docPrincipal.getContrato());
			if(docPrincipal.getContrato() != null && docPrincipal.getContrato().getCdcontrato() != null){
				doc.setObservacaoHistorico("Faturamento do contrato <a href=\"javascript:visualizarContrato("+docPrincipal.getContrato().getCdcontrato()+")\">"+docPrincipal.getContrato().getCdcontrato()+"</a>");
			}
			
			doc.setContratojuridico(docPrincipal.getContratojuridico());
			if(docPrincipal.getContratojuridico() != null && docPrincipal.getContratojuridico().getCdcontratojuridico() != null){
				doc.setObservacaoHistorico("Faturamento do contrato <a href=\"javascript:visualizarContratojuridico("+docPrincipal.getContratojuridico().getCdcontratojuridico()+")\">"+docPrincipal.getContratojuridico().getCdcontratojuridico()+"</a>");
			}
			
			doc.setColeta(docPrincipal.getColeta());
			if(doc.getColeta() != null){
				StringBuilder hist = new StringBuilder();
				hist.append("Pagamento da coleta: <a href=\"javascript:visualizarColeta(")
					.append(docPrincipal.getColeta().getCdcoleta())
					.append(");\">")
					.append(docPrincipal.getColeta().getCdcoleta())
					.append("</a>");
				doc.setObservacaoHistorico(hist.toString());
			}
			
			doc.setFromDespesaVeiculo(docPrincipal.getFromDespesaVeiculo());
			doc.setIdsVeiculosDespesa(docPrincipal.getIdsVeiculosDespesa());
			doc.setWhereInDespesaviagem(docPrincipal.getWhereInDespesaviagem());
			doc.setWhereInEntregadocumento(docPrincipal.getWhereInEntregadocumento());
			doc.setAdiatamentodespesa(docPrincipal.getAdiatamentodespesa());
			
			doc.setRateio(new Rateio(docPrincipal.getRateio()));
			doc.setReferencia(docPrincipal.getReferencia());
			doc.setFinanciamento(false);
			this.atualizaHistoricoDocumento(doc, Boolean.FALSE);
			doc.setListaParcela(new ListSet<Parcela>(Parcela.class));

			parcela = new Parcela();
			parcela.setDocumento(doc);
			parcela.setOrdem(ordem);
			parcela.setParcelamento(parcelamento);
			doc.getListaParcela().add(parcela);
			parcelamento.getListaParcela().add(parcela);
			
			doc.setTaxa(arrumaTaxasDocumento(docPrincipal.getTaxa(), SinedDateUtils.diferencaDias(doc.getDtvencimento(), docPrincipal.getDtvencimento()), doc.getDtemissao(), doc, doc.getDocumentotipoTransient() == null));
			
			if(doc.getDocumentotipoTransient() != null){
				addTaxaMovimentoTipoDocumento(doc);
				removerContaTipoDocumentoNaoBoleto(doc);
			}
			
			if(doc.getTaxa() != null && doc.getTaxa().getListaTaxaitem() != null && doc.getTaxa().getListaTaxaitem().size() > 0){
				List<StringBuilder> listaSb = new ArrayList<StringBuilder>();
				StringBuilder sb;
				for (Taxaitem ti : doc.getTaxa().getListaTaxaitem()) {
					if(ti.getGerarmensagem() != null && ti.getGerarmensagem()){
						sb = new StringBuilder();
						sb
							.append(Tipotaxa.valueOf(ti.getTipotaxa()).getNome())
							.append(" de ")
							.append(ti.isPercentual() ? "" : "R$")
							.append(ti.getValor().toString())
							.append(ti.isPercentual() ? "%" : "")
							.append(ateApos(ti.getTipotaxa().getCdtipotaxa()))
							.append(SinedDateUtils.toString(ti.getDtlimite()))
							.append(". ");
						listaSb.add(sb);
					}
				}
				
				if(listaSb.size() > 0){
					doc.setMensagem2("");
					doc.setMensagem3("");
					
					for (StringBuilder sb2 : listaSb) {
						if(doc.getMensagem2().length() + sb2.length() <= 80){
							doc.setMensagem2(doc.getMensagem2() + sb2.toString());
						} else if(doc.getMensagem3().length() + sb2.length() <= 80){
							doc.setMensagem3(doc.getMensagem3() + sb2.toString());
						}
					}
				}
			}
			
			if(SinedUtil.isListNotEmpty(doc.getListaNotaTransient()) || SinedUtil.isListNotEmpty(docPrincipal.getListaNotaTransient()) || StringUtils.isNotBlank(docPrincipal.getWhereInNota())){
				List<Nota> listaNotaTransient = null;
				if(doc.getListaNotaTransient() != null) listaNotaTransient = doc.getListaNotaTransient();
				if(docPrincipal.getListaNotaTransient() != null) listaNotaTransient = docPrincipal.getListaNotaTransient();
				if(StringUtils.isNotBlank(docPrincipal.getWhereInNota())){
					String[] ids = docPrincipal.getWhereInNota().split(",");
					try {
						Nota nota;
						for(String idNota : ids){
							nota = new Nota(Integer.parseInt(idNota));
							boolean existeNota = false;
							if(listaNotaTransient != null){
								for(Nota n : listaNotaTransient){
									if(nota.getCdNota().equals(n.getCdNota())){
										existeNota = true;
										break;
									}
								}
							}
							if(!existeNota){
								if(listaNotaTransient == null){
									listaNotaTransient = new ArrayList<Nota>();
								}
								listaNotaTransient.add(nota);
							}
						}
					} catch (Exception e) {} 
				}
				historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, listaNotaTransient, ordem + "/" + totalParcela);
			}
			
			ordem ++;
		}

		return parcelamento;
	}
	
	public void removerContaTipoDocumentoNaoBoleto(Documento documento) {
		if(documento.getDocumentotipo() != null && documento.getConta() != null){
			Documentotipo documentotipo = documentotipoService.load(documento.getDocumentotipo());
			if(documentotipo != null && (documentotipo.getBoleto() == null  || !documentotipo.getBoleto())){
				documento.setConta(null);
				documento.setContacarteira(null);
			}
		}
	}
	
	private String ateApos(Integer cdtipotaxa){
		if(cdtipotaxa == 3 || cdtipotaxa == 4){
			return " at� ";
		}
		return " ap�s ";
	}
	
	public Taxa arrumaTaxasDocumento(Taxa taxa, Integer numDias, Date dtEmissao, Documento documento) {
		return arrumaTaxasDocumento(taxa, numDias, dtEmissao, documento, true);
	}
	
	/**
	 * M�todo que cria uma lista nova de taxas com as datas limites reorganizadas
	 * 
	 * @param taxa
	 * @param prazopagamentoitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Taxa arrumaTaxasDocumento(Taxa taxa, Integer numDias, Date dtEmissao, Documento documento, Boolean adicionaTaxaMovimento) {
		Taxa taxaNova = null;
		if(taxa != null && taxa.getListaTaxaitem() != null && !taxa.getListaTaxaitem().isEmpty()){
			taxaNova = new Taxa();
			Set<Taxaitem> taxas = new ListSet<Taxaitem>(Taxaitem.class);
			Taxaitem taxaitem = null;
			for (Taxaitem taxaitemAux : taxa.getListaTaxaitem()) {
				if(!adicionaTaxaMovimento && tipotaxaService.findTipotaxaContareceberForDocumentotipo().contains(taxaitemAux.getTipotaxa())){
					continue;
				}
				taxaitem = new Taxaitem(taxaitemAux.getTipotaxa(), taxaitemAux.isPercentual(), taxaitemAux.getValor(), taxaitemAux.getGerarmensagem());
				Calendar dataNova = Calendar.getInstance();
				dataNova.setTimeInMillis(taxaitemAux.getDtlimite().getTime());
				
				dataNova.add(Calendar.DAY_OF_MONTH, numDias);
				if(Tipotaxa.TAXAMOVIMENTO.equals(taxaitem.getTipotaxa()) && dtEmissao != null){
					taxaitem.setDtlimite(dtEmissao);
				}else{
					taxaitem.setDtlimite(new Date(dataNova.getTimeInMillis()));
				}
				taxaitem.setDias(taxaitemAux.getDias());
				taxas.add(taxaitem);
			}
			taxaNova.setListaTaxaitem(taxas);
		}
		
		return taxaNova;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de contas a pagar.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForGerarArquivobancario
	 * @param filtro
	 * @return
	 * @author Flavio
	 */
	public List<Documento> findForGerarArquivobancario(ArquivobancarioProcessFiltro filtro){
		return documentoDAO.findForGerarArquivobancario(filtro);
	}

	/**
	 * Obt�m uma lista de documentos associados a uma conta atrav�s do parcelamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ParcelamentoService#findByDocumento(Documento)
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findDocumentosAssociados(Documento, Parcelamento)
	 * @param documento
	 * @return <code>null</code> - Caso o documento n�o fa�a parte de um financiamento, ou seja n�o tem nenhum parcelamento.
	 * 			<code>List</code> - Caso o documento seja financiado, retorna uma lista de documentos associados ao parcelamento.
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findDocumentosAssociados(Documento documento){
		Parcelamento parcelamento = parcelamentoService.findByDocumento(documento);
		if(parcelamento == null){
			return null;
		}else return documentoDAO.findDocumentosAssociados(documento,parcelamento);
	}

	/**
	 * M�todo para calcular valores dos campos nos detalhes da conta.
	 * 
	 * @see #calculaValoresJuros(Documento)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Documento)
	 * @see #montaListaDocumentos(Documento)
	 * 
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void calculaValores(Documento documento){
		this.montaListaDocumentos(documento);
		rateioService.calculaValoresRateio(documento.getRateio(), documento.getValor());
	}

	/**
	 * M�todo para montar a lista de documentos associados a uma conta.
	 * 
	 * @see #findDocumentosAssociados(Documento)
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	private void montaListaDocumentos(Documento documento){
		List<Documento> listDocumento = this.findDocumentosAssociados(documento);

		//CASO O DOCUMENTO FA�A PARTE DE UM FINANCIAMENTO
		if(listDocumento != null){
			
			for (Documento doc : listDocumento) {
				if(SinedUtil.isListNotEmpty(doc.getListaParcela())){
					Parcela parcela = doc.getListaParcela().iterator().next();
					doc.setParcela(parcela.getOrdem());
				}
			}
			documento.setListaDocumento(listDocumento);
			documento.setFinanciamento(documento.getListaDocumento() != null && documento.getListaDocumento().size() != 0);
			documento.setRepeticoes(documento.getListaDocumento().size());
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#carregaDocumento
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento carregaDocumento(Documento documento){
		return documentoDAO.carregaDocumento(documento);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateValorDocumento(Documento)
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void updateValorDocumento(Documento documento){
		documentoDAO.updateValorDocumento(documento);
	}

	/**
	 * M�todo para montar um whereIn com os ids dos documentos informados na lista e 
	 * carregar os documentos.
	 * 
	 * @see #carregaDocumentos(String)
	 * @param listaDocumentos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> carregaDocumentos(Collection<Documento> listaDocumentos){
		return carregaDocumentos(CollectionsUtil.listAndConcatenate(listaDocumentos,"cddocumento", ","));
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#carregaDocumentos
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> carregaDocumentos(String whereIn){
		return documentoDAO.carregaDocumentos(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findForReportFluxocaixa(FluxocaixaFiltroReport filtro){
		return documentoDAO.findForReportFluxocaixa(filtro);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findAnterioresForFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findAnterioresForReportFluxocaixa(FluxocaixaFiltroReport filtro){
		List<Documento> listaDoc = documentoDAO.findAnterioresForFluxocaixa(filtro);
		for (Documento d : listaDoc) {
			d.setDataAtrasada(d.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()));
			d.setDtvencimento(SinedDateUtils.remoteDate());
		}
		Collections.sort(listaDoc,new Comparator<Documento>(){
			public int compare(Documento o1, Documento o2) {
				return o1.getDataAtrasada().compareTo(o2.getDataAtrasada());
			}
		});
		return listaDoc;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findDocsCopiaCheque
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findDocsCopiaCheque(String whereIn){
		return documentoDAO.findDocsCopiaCheque(whereIn);
	}

	/**
	 * <p>Gera uma conta a pagar ou uma conta a receber para o processo de
	 * consolidar agendamento.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#limparDadosAgendamento(Agendamento)
	 * @see br.com.linkcom.sined.geral.bean.Documento#ajustaBeanToLoad()
	 * @param agendamento
	 * @author Hugo Ferreira
	 */
	public Documento gerarDocumento(Agendamento agendamento, Integer documentoClasse) {
		if(agendamento == null){
			throw new SinedException("O par�metro agendamento n�o pode ser null.");
		}
		Documento documento = new Documento();

		agendamentoService.limparDadosAgendamento(agendamento);

		documento.setEmpresa(agendamento.getEmpresa());
		documento.setDocumentotipo(agendamento.getDocumentotipo());
		documento.setNumero(agendamento.getDocumento());
		documento.setDocumentoacao(agendamento.getDocumentoacao() != null ? agendamento.getDocumentoacao() : Documentoacao.DEFINITIVA);
		documento.setDescricao(agendamento.getDescricao());
		documento.setFornecedor(agendamento.getFornecedor());
		documento.setCliente(agendamento.getCliente());
		documento.setPessoa(agendamento.getPessoa());
		documento.setTipopagamento(agendamento.getTipopagamento());
		documento.setOutrospagamento(agendamento.getOutrospagamento());
		documento.setDtemissao(new Date(System.currentTimeMillis()));
		documento.setDtvencimento(SinedDateUtils.getDataVencimentoValida(agendamento.getDtproximo(), documento.getDtemissao()));
		documento.setValor(agendamento.getValor());
		documento.setRateio(agendamento.getRateio());
		documento.setTaxa(agendamento.getTaxa());
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setObservacaoHistorico(agendamento.getObservacao());
		documento.setOperacaocontabil(agendamento.getOperacaocontabil());
		
		if(documentoClasse!=null && documentoClasse.equals(Documentoclasse.CD_PAGAR)){
			documento.setVinculoProvisionado(agendamento.getVinculoProvisionado());
		}else if(documentoClasse!=null && documentoClasse.equals(Documentoclasse.CD_RECEBER)){
			documento.setConta(agendamento.getVinculoProvisionado());
			if(agendamento.getPessoa() != null){
				documento.setEndereco(agendamento.getPessoa().getEnderecoWithPrioridade(Enderecotipo.COBRANCA, Enderecotipo.FATURAMENTO, Enderecotipo.UNICO));
			}
		}
		
		return documento.ajustaBeanToLoad();
	}
	
	public Documento gerarDocumento(GeraReceita geraReceita) {
		return gerarDocumento(geraReceita, false);
	}

	/**
	 * Cria um documento baseado nos dados do par�metro geraReceita.
	 * Fun��o utilizada no caso de uso Gerar Receita do m�dulo Faturamento.
	 *
	 * @param geraReceita
	 * @return um documento com os seguintes dados oriundos de geraReceita:
	 * 		- Cliente
	 * 		- Valor
	 * 		- Prazo de pagamento
	 * 		- N�mero de parcelas extras
	 *
	 * @author Hugo Ferreira
	 */
	public Documento gerarDocumento(GeraReceita geraReceita, boolean receitaEmLote) {
		Documento doc = new Documento();
		
		if(geraReceita.getPrazoPagamento() != null && geraReceita.getPrazoPagamento().getCdprazopagamento() != null){
			Prazopagamento prazopagamento = prazopagamentoService.load(geraReceita.getPrazoPagamento(), "prazopagamento.cdprazopagamento, prazopagamento.dataparceladiautil");
			if(prazopagamento != null){
				geraReceita.getPrazoPagamento().setDataparceladiautil(prazopagamento.getDataparceladiautil());
			}
		}
		
		Nota nota = geraReceita.getNota();
		
		doc.setCdNota(nota.getCdNota());
		doc.setValor(geraReceita.getValorUmaParcela());
		
		doc.setEndereco(geraReceita.getEndereco());
		if(doc.getEndereco() != null)
			doc.setEndereco(enderecoService.loadEndereco(doc.getEndereco()));
		
		nota = notaService.findForReceitaIndividual(nota);
		doc.setNumeroNota(nota.getNumero());
		
		Notafiscalproduto notafiscalproduto = null;
		NotaFiscalServico notaFiscalServico = null;
		if(nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
			notaFiscalServico = notaFiscalServicoService.loadForReceita(nota.getCdNota());
			doc.setDocumentoacao(Documentoacao.DEFINITIVA);
			this.makeDocumentoNfServico(doc, notaFiscalServico, geraReceita.getDtemissao());
			historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, notaFiscalServico, "1/1");
		} else {
			doc.setDocumentoacao(Documentoacao.DEFINITIVA);
			notafiscalproduto = new Notafiscalproduto(nota.getCdNota());
			notafiscalproduto = notafiscalprodutoService.loadForReceita(notafiscalproduto);
			
			if(notafiscalproduto != null){
				notafiscalprodutoService.setPrazogapamentoUnico(notafiscalproduto);
				
				Empresa empresa = notafiscalproduto.getEmpresa();
				Endereco enderecoPessoa = null;
				if(empresa != null && empresa.getCdpessoa() != null){
					empresa = empresaService.loadForEntrada(empresa);
					enderecoPessoa = enderecoService.getEnderecoPrincipalPessoa(empresa);
				}
				
//				if(receitaEmLote){
//					List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
//					listaRateioitem.addAll(notafiscalprodutoService.criaRateioByNota(notafiscalproduto, empresa));
//					Rateio rateio = new Rateio();
//					rateio.setListaRateioitem(listaRateioitem);
//					rateioitemService.agruparItensRateio(listaRateioitem);
//					rateioService.rateioItensPercentual(rateio, notafiscalproduto.getValor());
//					doc.setRateio(rateio);
//				}else {
					List<Rateioitem> listaRateioitem = notafiscalprodutoService.getListaRateioitemFaturamento(notafiscalproduto);
					String msg6 = null;
					
					if(SinedUtil.isListNotEmpty(listaRateioitem)){
						rateioitemService.agruparItensRateio(listaRateioitem);
						rateioService.calculaValorRateioitem(notafiscalproduto.getValor(), listaRateioitem);
						rateioService.ajustaDiferencaRateio(notafiscalproduto.getValor(), listaRateioitem);
					}else {
						List<NotaContrato> listaNotaContrato = notaContratoService.findForRateioCobranca(notafiscalproduto.getCdNota().toString());
						if(SinedUtil.isListNotEmpty(listaNotaContrato)){
							List<Contrato> listaContrato = new ArrayList<Contrato>();
							for (NotaContrato c : listaNotaContrato) {
								if(c.getContrato().getRateio() != null && c.getContrato().getRateio().getCdrateio() != null){
									listaContrato.add(c.getContrato());
									listaRateioitem.addAll(rateioitemService.findByRateio(c.getContrato().getRateio()));
								}
								if (Boolean.TRUE.equals(c.getContrato().getReajuste())){
									msg6 = c.getContrato().getMensagemBoletoReajuste();
								}
							}
							if(SinedUtil.isListNotEmpty(listaContrato) && SinedUtil.isListNotEmpty(listaRateioitem)){
								List<Contrato> lista = contratoService.findForCobranca(CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ","));
								if(SinedUtil.isListNotEmpty(lista)){
									Money valorTotalContrato = new Money();
									for(Contrato contrato : lista){
										valorTotalContrato = valorTotalContrato.add(contrato.getValorContrato(contrato.getDtproximovencimento())); 
									}
									rateioitemService.agruparItensRateio(listaRateioitem);
									rateioService.ajustaPercentualRateioitem(valorTotalContrato, listaRateioitem);
									rateioService.calculaValorRateioitem(notafiscalproduto.getValor(), listaRateioitem);
									rateioService.ajustaDiferencaRateio(notafiscalproduto.getValor(), listaRateioitem);
								}
							}
						}else {
							listaRateioitem.addAll(notafiscalprodutoService.criaRateioByNota(notafiscalproduto, empresa));
						}
					}
					Rateio rateio = new Rateio();
					rateio.setListaRateioitem(listaRateioitem);
					rateioitemService.agruparItensRateio(listaRateioitem);
					rateioService.rateioItensPercentual(rateio, notafiscalproduto.getValor());
					doc.setRateio(rateio);
//				}
				
				if(geraReceita.getDtemissao() != null){
					doc.setDtemissao(geraReceita.getDtemissao());
				}else {
					doc.setDtemissao(notafiscalproduto.getDtEmissao());
				}
				
				if(geraReceita.getDtsaida() != null) {
					doc.setDtcompetencia(geraReceita.getDtsaida());
				} else {
					doc.setDtcompetencia(geraReceita.getDtemissao());
				}
				
				doc.setDtvencimento(notafiscalprodutoService.getDtvencimentoGerarReceita(geraReceita, notafiscalproduto));
				doc.setCliente(notafiscalproduto.getCliente());	
				doc.setEmpresa(empresa);
				doc.setNumero(notafiscalproduto.getNumero());
				doc.setTipopagamento(Tipopagamento.CLIENTE);
				
				if(notafiscalproduto.getNumero() != null){
					doc.setDescricao("Referente a NF: " + (notafiscalproduto.getNumero() != null ? notafiscalproduto.getNumero() : "<sem n�mero>"));
				}
				
				Documentotipo nf = documentotipoService.findNotafiscal();
				if(nf != null){
					doc.setDocumentotipo(nf);
					doc.setVinculoProvisionado(nf.getContadestino());
				}
				
				Conta contaboleto = null;
				if(notafiscalproduto.getContaboleto() != null){
					contaboleto = contaService.loadWithTaxa(notafiscalproduto.getContaboleto());
				}else if(receitaEmLote && Documentotipo.BOLETO.equals(notafiscalproduto.getDocumentotipo()) &&
						empresa != null && empresa.getContabancariacontareceber() != null){
					contaboleto = contaService.loadWithTaxa(empresa.getContabancariacontareceber());
					Contacarteira contacarteira = empresa.getContacarteiracontareceber();
					if(contacarteira != null){
						contacarteira = contacarteiraService.load(contacarteira);
					}
					notafiscalproduto.setContacarteira(contacarteira);
				}
				if(receitaEmLote && contaboleto != null && notafiscalproduto.getContacarteira() == null && Documentotipo.BOLETO.equals(notafiscalproduto.getDocumentotipo())){
					Contacarteira contacarteira = null;
					if(empresa != null && empresa.getContacarteiracontareceber() != null){
						contacarteira = contacarteiraService.load(empresa.getContacarteiracontareceber());
					}else{
						for(Contacarteira cc: contacarteiraService.findByConta(contaboleto)){
							if(cc.isPadrao() && !Boolean.TRUE.equals(cc.getNaoassociarcontareceber())){
								contacarteira = cc;
								break;
							}
						}
					}

					notafiscalproduto.setContacarteira(contacarteira);					
				}
				if(contaboleto != null){
					doc.setVinculoProvisionado(contaboleto);
					doc.setConta(contaboleto);
					
					if(notafiscalproduto.getContacarteira() != null){
						doc.setContacarteira(notafiscalproduto.getContacarteira());
						if(msg6==null && StringUtils.isNotEmpty(notafiscalproduto.getContacarteira().getMsgboleto2())){
							msg6 = notafiscalproduto.getContacarteira().getMsgboleto2();
						}
					}
				}
				
				String msg1 = null;
				String numero = null;
				String chave = null;
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
					if(nota.getNumero() != null && !"".equals(nota.getNumero())){
						numero = "NF: " + nota.getNumero();
					}
					
					Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nota);
					if(arquivonfnota != null && arquivonfnota.getChaveacesso() != null && !"".equals(arquivonfnota.getChaveacesso())){
						chave = "Chave: " + arquivonfnota.getChaveacesso();
					}
					
					if(numero != null && chave != null){
						msg1 = numero + " - " + chave;
					} else if(numero != null){
						msg1 = numero;
					} else if(chave != null){
						msg1 = chave;
					}
				}
				
				doc.setMensagem1(msg1);
				doc.setMensagem6(msg6);
				
				if(contaboleto != null){
					
					Set<Taxaitem> listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
					if(contaboleto.getTaxa() != null && 
						contaboleto.getTaxa().getListaTaxaitem() != null &&
						contaboleto.getTaxa().getListaTaxaitem().size() > 0){
						
						listaTaxaitem = contacorrenteService.getTaxas(contaboleto, doc.getDtvencimento(), enderecoPessoa);
						if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
							StringBuilder msg2 = new StringBuilder();
							StringBuilder msg3 = new StringBuilder();
							String preposicao = "";
							String msg = "";
							
							if(doc.getMensagem2() != null)
								msg2.append(doc.getMensagem2());
							if(doc.getMensagem3() != null)
								msg3.append(doc.getMensagem3());
							
							for(Taxaitem txitem : listaTaxaitem){
								if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
									if(tipotaxaService.isApos(txitem.getTipotaxa())) {
										preposicao = " ap�s ";
									}else {
										preposicao = " at� ";
									}
									msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + SinedDateUtils.toString(txitem.getDtlimite()) : "") + ". ";
									if((msg2.length() + msg.length()) <= 80){
										msg2.append(msg);
									}else if((msg3.length() + msg.length()) <= 80){
										msg3.append(msg);
									}
								}
							}
							if(!msg2.toString().equals("")){
								doc.setMensagem2(msg2.toString());
							}
							if(!msg3.toString().equals("")){
								doc.setMensagem3(msg3.toString());
							}
						}
					}
					
					if(doc.getCliente() != null && (doc.getCliente().getNaoconsiderartaxaboleto() == null || !doc.getCliente().getNaoconsiderartaxaboleto())){
						Money taxaBoleto = contacorrenteService.getValorTaxaBoleto(doc.getConta()); 
						if (taxaBoleto != null && taxaBoleto.compareTo(new Money()) != 0){
							Tipotaxa tipotaxa = tipotaxaService.getTipoTaxa(Tipotaxa.TAXABOLETO);
							Taxaitem taxaitem = new Taxaitem();
							taxaitem.setTipotaxa(tipotaxa);
							taxaitem.setValor(taxaBoleto);
							taxaitem.setMotivo("");
							taxaitem.setDtlimite(contareceberService.getAdicionarAnoData(doc.getDtvencimento()));
							if(listaTaxaitem == null)
								listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
							listaTaxaitem.add(taxaitem);
						}
					}
					
					if(SinedUtil.isListNotEmpty(listaTaxaitem)){
						Taxa taxa = new Taxa();
						taxa.setListaTaxaitem(listaTaxaitem);
						doc.setTaxa(taxa);
					}
				}
				
				historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, notafiscalproduto, "1/1");
			}
		}
		
		boolean considerarDiaUtilDentroDoMes = notafiscalproduto != null && notafiscalproduto.getListaNotaContrato() != null && 
				notafiscalproduto.getListaNotaContrato().size() > 0 && notafiscalproduto.getListaNotaContrato().get(0).getContrato() != null &&
				notafiscalproduto.getListaNotaContrato().get(0).getContrato().getDataparcelaultimodiautilmes() != null && 
				notafiscalproduto.getListaNotaContrato().get(0).getContrato().getDataparcelaultimodiautilmes();
		
		boolean considerarDiaUtilContrato = notafiscalproduto != null && notafiscalproduto.getListaNotaContrato() != null && 
				notafiscalproduto.getListaNotaContrato().size() > 0 && notafiscalproduto.getListaNotaContrato().get(0).getContrato() != null &&
				notafiscalproduto.getListaNotaContrato().get(0).getContrato().getDataparceladiautil() != null && 
				notafiscalproduto.getListaNotaContrato().get(0).getContrato().getDataparceladiautil();
				
		boolean considerarDiaUtil = (considerarDiaUtilContrato)	|| (geraReceita.getPrazoPagamento() != null && 
				geraReceita.getPrazoPagamento().getDataparceladiautil() != null &&
				geraReceita.getPrazoPagamento().getDataparceladiautil());
		
		boolean verificarDtvencimento = true;
		
		boolean cobrancaNota = false;
		if(notaFiscalServico != null && 
				notaFiscalServico.getListaDuplicata() != null && 
				notaFiscalServico.getListaDuplicata().size() > 0 &&
				notaFiscalServico.getPrazopagamentofatura() != null){
			cobrancaNota = true;
			geraReceita.setQtdeParcelas(notaFiscalServico.getListaDuplicata().size()-1);
		}
		if(notafiscalproduto != null && 
				notafiscalproduto.getListaDuplicata() != null && 
				notafiscalproduto.getListaDuplicata().size() > 0){
			cobrancaNota = true;
			geraReceita.setQtdeParcelas(notafiscalproduto.getListaDuplicata().size()-1);
		}
		
		boolean recalcularRateio = false;
		if(!geraReceita.getPrazoPagamento().equals(Prazopagamento.UNICA) || cobrancaNota) {
			if(geraReceita.getQtdeParcelas() >= 1){
				doc.setFinanciamento(Boolean.TRUE);
				doc.setPrazo(geraReceita.getPrazoPagamento());
				doc.setRepeticoes(geraReceita.getQtdeParcelas());
				
				try {
					if(notafiscalproduto != null && 
							notafiscalproduto.getCadastrarcobranca() != null && 
							notafiscalproduto.getCadastrarcobranca() && 
							notafiscalproduto.getListaDuplicata() != null && 
							notafiscalproduto.getListaDuplicata().size() > 0){
						recalcularRateio = true;
						verificarDtvencimento = false;
						List<Notafiscalprodutoduplicata> listaDuplicata = notafiscalproduto.getListaDuplicata();
						Integer totalParcela  = listaDuplicata.size();
						
						Notafiscalprodutoduplicata primeira = listaDuplicata.remove(0);
						if(considerarDiaUtil){
							doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(primeira.getDtvencimento(), notafiscalproduto.getEmpresa(), considerarDiaUtilDentroDoMes));
						}else {
							doc.setDtvencimento(primeira.getDtvencimento());
						}
						String numeroDoc = StringUtils.isNotEmpty(primeira.getNumero()) ? primeira.getNumero() :  notafiscalproduto.getNumero();
						if(StringUtils.isNotEmpty(numeroDoc)){
							doc.setNumero(numeroDoc.contains("/1") ? numeroDoc : (numeroDoc + "/1"));
						}
						doc.setDocumentoacao(Documentoacao.DEFINITIVA);
						doc.setValor(primeira.getValor());
						doc.setDocumentotipo(primeira.getDocumentotipo());
						doc.setDocumentotipoTransient(primeira.getDocumentotipo());
						historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, notafiscalproduto, "1/"+totalParcela);
						
						List<Documento> listaDoc = new ArrayList<Documento>();
						Documento docTmp;
						int parcela = 1;
						for (Notafiscalprodutoduplicata dup : listaDuplicata) {
							docTmp = new Documento();
							
							docTmp.setParcela(++parcela);
							if(considerarDiaUtil){
								docTmp.setDtvencimento(SinedDateUtils.getProximaDataUtil(dup.getDtvencimento(), notafiscalproduto.getEmpresa(), considerarDiaUtilDentroDoMes));
							}else {
								docTmp.setDtvencimento(dup.getDtvencimento());
							}
							numeroDoc = StringUtils.isNotEmpty(dup.getNumero()) ? dup.getNumero() :  notafiscalproduto.getNumero();
							if(StringUtils.isNotEmpty(numeroDoc)){
								docTmp.setNumero(numeroDoc.contains("/" + parcela) ? numeroDoc : (numeroDoc + "/" + parcela));
							}
							docTmp.setDocumentoacao(Documentoacao.DEFINITIVA);
							docTmp.setValor(dup.getValor());
							docTmp.setDocumentotipo(dup.getDocumentotipo());
							docTmp.setDocumentotipoTransient(dup.getDocumentotipo());
							historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, docTmp, notafiscalproduto, parcela+"/"+totalParcela);
							
							listaDoc.add(docTmp);
						}
						
						doc.setListaDocumento(listaDoc);
						
					}else if(notaFiscalServico != null && 
							notaFiscalServico.getListaDuplicata() != null && 
							notaFiscalServico.getListaDuplicata().size() > 0){
						verificarDtvencimento = false;
						recalcularRateio = true;
						
						setDuplicatasDocumento(notaFiscalServico, notaFiscalServico.getListaDuplicata(), doc, considerarDiaUtil, true, null);
						
					} else {
						Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nota);
						if(arquivonfnota != null){
							if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
								String numeroDoc = notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse());
								if(StringUtils.isNotBlank(numeroDoc)){
									doc.setNumero(numeroDoc);
								}
							}
						}
						
						doc.setListaDocumento(this.geraFinanciamento(doc));
						if(SinedUtil.isListNotEmpty(doc.getListaDocumento())){
							recalcularRateio = true;
							Integer parcela = 1;
							if(StringUtils.isNotBlank(doc.getNumero()) && parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_NOTA) &&
									!doc.getNumero().contains("/")){
								doc.setNumero(doc.getNumero() + "/" + parcela);
								parcela++;
							}
							for (Documento docTmp : doc.getListaDocumento()) {
								if(considerarDiaUtil){
									docTmp.setDtvencimento(SinedDateUtils.getProximaDataUtil(docTmp.getDtvencimento(), notafiscalproduto.getEmpresa(), considerarDiaUtilDentroDoMes));
								}
								if(StringUtils.isNotBlank(docTmp.getNumero()) && parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_NOTA) &&
										!docTmp.getNumero().contains("/")){
									docTmp.setNumero(docTmp.getNumero() + "/" + parcela);
								}
								parcela++;
							}
						}
					}
				} catch (Exception e) {
					doc.setListaDocumento(null);
				}
			}else if(notaFiscalServico != null && 
					notaFiscalServico.getListaDuplicata() != null && 
					notaFiscalServico.getListaDuplicata().size() == 1){
				verificarDtvencimento = false;
				recalcularRateio = true;
				Notafiscalservicoduplicata primeira = notaFiscalServico.getListaDuplicata().get(0);
				
				if(considerarDiaUtil){
					doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(primeira.getDtvencimento(), notaFiscalServico.getEmpresa(), considerarDiaUtilDentroDoMes));
				}else {
					doc.setDtvencimento(primeira.getDtvencimento());
				}
				doc.setDocumentoacao(Documentoacao.DEFINITIVA);
				doc.setValor(primeira.getValor());
				if(primeira.getDocumentotipo() != null){
					doc.setDocumentotipo(primeira.getDocumentotipo());
				}
			}else if(notafiscalproduto != null && 
					notafiscalproduto.getListaDuplicata() != null && 
					notafiscalproduto.getListaDuplicata().size() == 1){
				verificarDtvencimento = false;
				recalcularRateio = true;
				Notafiscalprodutoduplicata primeira = notafiscalproduto.getListaDuplicata().get(0);
				
				if(considerarDiaUtil){
					doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(primeira.getDtvencimento(), notafiscalproduto.getEmpresa(), considerarDiaUtilDentroDoMes));
				}else {
					doc.setDtvencimento(primeira.getDtvencimento());
				}
				doc.setDocumentoacao(Documentoacao.DEFINITIVA);
				doc.setValor(primeira.getValor());
				if(primeira.getDocumentotipo() != null){
					doc.setDocumentotipo(primeira.getDocumentotipo());
				}
			}
			
			List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(geraReceita.getPrazoPagamento());
			if(listaPrazoItem != null && listaPrazoItem.size() > 0 && !cobrancaNota){
				verificarDtvencimento = false;
				Prazopagamentoitem prazopagamentoitem = listaPrazoItem.get(0);
				
				Calendar primeiraData = Calendar.getInstance();
				primeiraData.setTimeInMillis(doc.getDtemissao().getTime());
				
				primeiraData.add(Calendar.DAY_OF_MONTH, prazopagamentoitem.getDias() != null ? prazopagamentoitem.getDias() : 0);
				primeiraData.add(Calendar.MONTH, prazopagamentoitem.getMeses() != null ? prazopagamentoitem.getMeses() : 0);
				
				Empresa empresa = notafiscalproduto != null ? notafiscalproduto.getEmpresa() : (notaFiscalServico != null ? notaFiscalServico.getEmpresa() : null);
				if(considerarDiaUtil){
					doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(new Date(primeiraData.getTimeInMillis()), empresa, considerarDiaUtilDentroDoMes));
				}else {
					doc.setDtvencimento(new Date(primeiraData.getTimeInMillis()));
				}
			}
		}
		
		if(recalcularRateio && doc.getRateio() != null && SinedUtil.isListNotEmpty(doc.getRateio().getListaRateioitem())){
			rateioService.calculaValorRateioitem(doc.getValor(), doc.getRateio().getListaRateioitem());
			rateioService.ajustaDiferencaRateio(doc.getValor(), doc.getRateio().getListaRateioitem());
		}
		
		if(verificarDtvencimento && doc.getDtvencimento() != null && considerarDiaUtil && notafiscalproduto != null){
			doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(doc.getDtvencimento(), notafiscalproduto.getEmpresa(), considerarDiaUtilDentroDoMes));
		}
		
		if(doc.getConta() != null && doc.getContacarteira() != null){
			Conta conta = contaService.carregaContaComMensagem(doc.getConta(), doc.getContacarteira());
			if (conta!=null){
				doc.setMensagem4(conta.getContacarteira().getMsgboleto1() != null ? conta.getContacarteira().getMsgboleto1() : "");
				doc.setMensagem5(conta.getContacarteira().getMsgboleto2() != null ? conta.getContacarteira().getMsgboleto2() : "");
				doc.setVinculoProvisionado(conta);
			}
		}
		
		addTaxaMovimentoTipoDocumento(doc);
		
		if(doc.getRateio() != null && SinedUtil.isListNotEmpty(doc.getRateio().getListaRateioitem())){
			rateioService.limpaReferenciaRateio(doc.getRateio());
		}
		
		return doc;
	}
	
	/**
	* M�todo que adiciona uma taxa de movimento na conta caso exista taxa no tipo de documento
	*
	* @param documento
	* @since 28/03/2017
	* @author Luiz Fernando
	*/
	public void addTaxaMovimentoTipoDocumento(Documento documento){
		if(documento.getDocumentotipo() != null){
			Documentotipo documentotipo = documentotipoService.load(documento.getDocumentotipo());
			if(Boolean.TRUE.equals(documentotipo.getTaxanoprocessamentoextratocartaocredito())){
				return;
			}
			Tipotaxa tipotaxa = documentotipo.getTipotaxacontareceber() != null && documentotipo.getTipotaxacontareceber().getCdtipotaxa() != null?
									documentotipo.getTipotaxacontareceber(): Tipotaxa.TAXAMOVIMENTO;
			tipotaxa = tipotaxaService.getTipoTaxa(tipotaxa);
			if(taxaitemService.existeTaxaitem(documento.getTaxa(), tipotaxa)){
				return;
			}
			if(documentotipo != null && documentotipo.getTaxavenda() != null && documentotipo.getTaxavenda() > 0){
				Double taxaVenda = documentotipo.getTaxavenda();
				if(taxaVenda != null && taxaVenda > 0.0){
					Taxaitem taxaitem = new Taxaitem();
					taxaitem.setTipotaxa(tipotaxa);
					taxaitem.setPercentual(documentotipo.getPercentual() != null ? documentotipo.getPercentual() : true);
					taxaitem.setValor(new Money(documentotipo.getTaxavenda()));
					taxaitem.setDtlimite(documento.getDtemissao());
					
					if(documento.getTaxa() == null) documento.setTaxa(new Taxa());
					if(SinedUtil.isListEmpty(documento.getTaxa().getListaTaxaitem())) documento.getTaxa().setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class));
					documento.getTaxa().getListaTaxaitem().add(taxaitem);
				}
			}
		}
	}
	
	public void setDuplicatasDocumento(NotaFiscalServico notaFiscalServico, List<Notafiscalservicoduplicata> listaDuplicata, Documento doc, boolean considerarDiaUtil, boolean considerarNumeroNota, List<NotaFiscalServico> listaNota) {
		if(notaFiscalServico != null && doc != null && SinedUtil.isListNotEmpty(listaDuplicata)){
			Integer totalParcela = listaDuplicata.size();
			Notafiscalservicoduplicata primeira = listaDuplicata.remove(0);
			if(considerarDiaUtil){
				doc.setDtvencimento(SinedDateUtils.getProximaDataUtil(primeira.getDtvencimento(), notaFiscalServico.getEmpresa()));
			}else {
				doc.setDtvencimento(primeira.getDtvencimento());
			}
			String numeroDoc = listaNota == null || listaNota.size() == 1 ? (StringUtils.isNotEmpty(primeira.getNumero()) ? primeira.getNumero() :  (considerarNumeroNota ? notaFiscalServico.getNumero() : "")) : "";
			boolean considerarNumeroNFSe = false;
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(notaFiscalServico);
			if(arquivonfnota != null && considerarNumeroNota){
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					numeroDoc = notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse());
					considerarNumeroNFSe = true;
				}
			}
			if(StringUtils.isNotEmpty(numeroDoc)){
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_NOTA)){
					doc.setNumero(numeroDoc.contains("/1") ? numeroDoc : (numeroDoc + "/1"));
				}else {
					doc.setNumero(numeroDoc);
				}
			}
			
			doc.setDocumentoacao(Documentoacao.DEFINITIVA);
			doc.setValor(primeira.getValor());
			doc.setDocumentotipo(primeira.getDocumentotipo());
			doc.setDocumentotipoTransient(primeira.getDocumentotipo());
			if(SinedUtil.isListNotEmpty(listaNota)){
				historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, listaNota, "1/"+totalParcela);
			}else {
				historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, (considerarNumeroNota ? notaFiscalServico : null), "1/"+totalParcela);
			}
			
			List<Documento> listaDoc = new ArrayList<Documento>();
			Documento docTmp;
			int parcela = 1;
			for (Notafiscalservicoduplicata dup : listaDuplicata) {
				docTmp = new Documento();
				
				docTmp.setParcela(++parcela);
				if(considerarDiaUtil){
					docTmp.setDtvencimento(SinedDateUtils.getProximaDataUtil(dup.getDtvencimento(), notaFiscalServico.getEmpresa()));
				}else {
					docTmp.setDtvencimento(dup.getDtvencimento());
				}
				if(!considerarNumeroNFSe && (listaNota == null || listaNota.size() == 1)){
					numeroDoc = StringUtils.isNotEmpty(dup.getNumero()) ? dup.getNumero() : (considerarNumeroNota ? notaFiscalServico.getNumero() : "");
				}
				
				if(StringUtils.isNotEmpty(numeroDoc)){
					if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_NOTA)){
						docTmp.setNumero(numeroDoc.contains("/" + parcela) ? numeroDoc : (numeroDoc + "/" + parcela));
					}else {
						docTmp.setNumero(numeroDoc);
					}
				}
				docTmp.setDocumentoacao(Documentoacao.DEFINITIVA);
				docTmp.setValor(dup.getValor());
				docTmp.setDocumentotipo(dup.getDocumentotipo());
				docTmp.setDocumentotipoTransient(dup.getDocumentotipo());
				if(SinedUtil.isListNotEmpty(listaNota)){
					historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, docTmp, listaNota, parcela+"/"+totalParcela);
				}else {
					historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, docTmp, (considerarNumeroNota ? notaFiscalServico : null), parcela+"/"+totalParcela);
				}
				
				listaDoc.add(docTmp);
			}
			
			doc.setListaDocumento(listaDoc);
		}
	}
	
	private void makeDocumentoNfServico(Documento documento, NotaFiscalServico nfs, Date dtEmissaoGerarReceita){
		notaFiscalServicoService.setPrazogapamentoUnico(nfs);
		
		// Setando o tipo de documento do tipo nota fiscal.
		Documentotipo nf = documentotipoService.findNotafiscal();
		documento.setDocumentotipo(nf);
		if(nf != null){
			documento.setVinculoProvisionado(nf.getContadestino());
		}
		
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		
		documento.setPessoa(nfs.getCliente());
		documento.setCliente(nfs.getCliente());
		if(dtEmissaoGerarReceita != null){
			documento.setDtemissao(dtEmissaoGerarReceita);
			documento.setDtcompetencia(dtEmissaoGerarReceita);
		}else {
			documento.setDtemissao(nfs.getDtEmissao());
			documento.setDtcompetencia(nfs.getDtEmissao());
			
		}
		documento.setDtvencimento(nfs.getDtVencimento() != null ? nfs.getDtVencimento() : nfs.getDtEmissao());
		documento.setNumero(nfs.getNumero());
		documento.setEmpresa(nfs.getEmpresa());
		documento.setConta(nfs.getContaboleto());
		documento.setContacarteira(nfs.getContacarteira());
		
		Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
		if(arquivonfnota != null){
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
				documento.setNumero(notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()));
			}
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
				documento.setMensagem1("Referente a NF: " + notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()));
			
				if(arquivonfnota.getChaveacesso() != null && !"".equals(arquivonfnota.getChaveacesso())){
					documento.setMensagem1(documento.getMensagem1() + " - Chave: " + arquivonfnota.getChaveacesso());
				}
			}
			
			if(StringUtils.isNotEmpty(arquivonfnota.getCodigoverificacao())){
				documento.setMensagem2("C�digo de Verifica��o da NFS-e: "+arquivonfnota.getCodigoverificacao());
			}
		} else if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
			documento.setMensagem1("Referente a NF: " + (nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));
		}
		
		if(nfs.getListaItens().size() > 1){
			//documento.setDescricao("Documento gerado a partir da NF " + (nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));	
			notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, "Presta��o de Servi�os");
		} else {
			notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, null);
		}
		
		if(nfs.getListaNotaContrato() != null && nfs.getListaNotaContrato().size() > 0){
		
			Contrato contrato = nfs.getListaNotaContrato().get(0).getContrato();
			List<NotaContrato> listNotaContrato = new ArrayList<NotaContrato>();
			listNotaContrato.add(new NotaContrato(nfs, contrato));
			documento.setDtvencimento(contratoService.getDataVencimento(contrato, contratoService.getDtVencimento(listNotaContrato)));
			if(nfs.getContaboleto() != null){
				Conta conta = contaService.loadConta(nfs.getContaboleto());
				documento.setConta(conta);
				if(nfs.getContacarteira() != null){
					documento.setContacarteira(nfs.getContacarteira());
				}else {
					//Preenche a carteira do documento com a carteira padr�o
					documento.setContacarteira(conta.getContacarteira());
					if(documento.getContacarteira() != null && documento.getContacarteira().getCdcontacarteira() == null){
						documento.setContacarteira(null);
					}
				}
			}else if (contrato.getConta() != null){
				Conta conta = contaService.loadConta(contrato.getConta());
				documento.setConta(conta);
				//Preenche a carteira do documento com a carteira padr�o
				documento.setContacarteira(conta.getContacarteira());
				if(documento.getContacarteira() != null && documento.getContacarteira().getCdcontacarteira() == null){
					documento.setContacarteira(null);
				}
			}
		
			Rateio rateio = new Rateio();
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			
			Taxa taxa = new Taxa();
			List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
			
			List<Tipotaxa> listaTipotaxaGerarmensagem = new ArrayList<Tipotaxa>();
			if(documento.getConta() != null){
				Conta conta = contaService.loadWithTaxa(documento.getConta());
				if(conta != null && conta.getTaxa() != null && SinedUtil.isListNotEmpty(conta.getTaxa().getListaTaxaitem())){
					for(Taxaitem taxaitem : conta.getTaxa().getListaTaxaitem()){
						if(taxaitem.getTipotaxa() != null && taxaitem.getGerarmensagem() != null && taxaitem.getGerarmensagem()){
							listaTipotaxaGerarmensagem.add(taxaitem.getTipotaxa());
						}
					}
				}
			}
			String msg_aux;
			String msg2 = "";
			String msg3 = "";
			
			List<Contrato> listaContrato = new ArrayList<Contrato>();
			for (NotaContrato c : nfs.getListaNotaContrato()) {
				if(c.getContrato().getRateio() != null && c.getContrato().getRateio().getCdrateio() != null){
					listaRateioitem.addAll(rateioitemService.findByRateio(c.getContrato().getRateio()));
					listaContrato.add(c.getContrato());
				}
				
				contratoService.preencherTaxasContrato(listaTaxaitem, c.getContrato());
				
				if(SinedUtil.isListNotEmpty(listaTaxaitem)){
					if(SinedUtil.isListNotEmpty(listaTipotaxaGerarmensagem) && documento.getDtvencimento() != null){
						for(Taxaitem taxaitem : listaTaxaitem){
							if(taxaitem.getTipotaxa() != null && listaTipotaxaGerarmensagem.contains(taxaitem.getTipotaxa())){
								taxaitem.setDtlimite(documento.getDtvencimento());
								
								if(taxaitem.getDias() != null && documento.getDtvencimento() != null){
									if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
										taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), -taxaitem.getDias()));
									}else {
										taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), taxaitem.getDias()));
									}
								}
								
								msg_aux = 	taxaitem.getTipotaxa().getNome() + 
										" de " + 
										(taxaitem.isPercentual() ? "" : "R$") + 
										taxaitem.getValor().toString() + 
										(taxaitem.isPercentual() ? "%" : "") + 
										ateApos(taxaitem.getTipotaxa().getCdtipotaxa())+
										SinedDateUtils.toString(documento.getDtvencimento()) +
										". ";
								
								if((msg2+msg_aux).length() <= 80){
									msg2 += msg_aux;
								} else if((msg3+msg_aux).length() <= 80){
									msg3 += msg_aux;
								}
								
								
							}
						}
					}
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaContrato) && SinedUtil.isListNotEmpty(listaRateioitem)){
				List<Contrato> lista = contratoService.findForCobranca(CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ","));
				if(SinedUtil.isListNotEmpty(lista)){
					Money valorTotalContrato = new Money();
					for(Contrato it : lista){
						valorTotalContrato = valorTotalContrato.add(it.getValorContrato(it.getDtproximovencimento())); 
					}
					rateioitemService.agruparItensRateio(listaRateioitem);
					rateioService.ajustaPercentualRateioitem(valorTotalContrato, listaRateioitem);
					rateioService.calculaValorRateioitem(documento.getValor(), listaRateioitem);
					rateioService.ajustaDiferencaRateio(documento.getValor(), listaRateioitem);
				}
			}
			
			if(StringUtils.isNotEmpty(msg2)){
				documento.setMensagem2((StringUtils.isNotEmpty(documento.getMensagem2()) ? documento.getMensagem2() + ". " : "") + msg2);
			}
			if(StringUtils.isNotEmpty(msg3)){
				documento.setMensagem3((StringUtils.isNotEmpty(documento.getMensagem3()) ? documento.getMensagem3() + ". " : "") + msg3);
			}
			
			rateioitemService.agruparItensRateio(listaRateioitem);			
			rateio.setListaRateioitem(listaRateioitem);
			rateioService.limpaReferenciaRateio(rateio);
			rateioService.atualizaValorRateio(rateio, documento.getValor());
			
			for (Taxaitem taxaitem : listaTaxaitem) {
				taxaitem.setDtlimite(documento.getDtvencimento());
			}
			taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
			
			documento.setRateio(rateio);
			documento.setTaxa(taxa);
		}else {
			Rateio rateio = new Rateio();
			List<Rateioitem> listaRateioitem = notaFiscalServicoService.getListaRateioitemFaturamento(nfs);
				
			rateioitemService.agruparItensRateio(listaRateioitem);			
			rateio.setListaRateioitem(listaRateioitem);
			rateioService.limpaReferenciaRateio(rateio);
			rateioService.atualizaValorRateio(rateio, documento.getValor());
			rateioService.ajustaDiferencaRateio(documento.getValor(), listaRateioitem);
			documento.setRateio(rateio);
		}
		
		Conta contaboleto = null;
		if(nfs.getContaboleto() != null){
			contaboleto = contaService.loadWithTaxa(nfs.getContaboleto());
		}
		documento.setConta(contaboleto);
		
		Documentotipo documentotipo = null;
		if (nfs.getDocumentotipo()!=null){
			documentotipo = documentotipoService.load(nfs.getDocumentotipo()); 
		}
		
		documento.setDocumentotipo(documentotipo);
	}
	
	/**
	 * <p>M�todo para calcular os valores em atraso, em aberto e total do <code>documento</code> na listagem.</p>
	 * <p>Tamb�m verifica se a conta est� protestada e vencida e preenche as propriedades <code>protestada e vencida</code></p>
	 * 
	 * @see #findForSomaListagem(DocumentoFiltro)
	 * @param filtro
	 * @param lista
	 * @author Fl�vio Tavares
	 */
	public void ajustaValoresListagem(DocumentoFiltro filtro, List<Documento> lista){
		Date hoje = SinedDateUtils.currentDate();
		
		if(lista != null && !lista.isEmpty()){
			List<Documentoconfirmacao> listaConfirmacao = documentoconfirmacaoService.findByDocumentos(lista);
			
			for (Documento d: lista) {
				//Marca os documentos que foram confirmados por arquivo de retorno
				if (listaConfirmacao != null && !listaConfirmacao.isEmpty()){
					for (Documentoconfirmacao confirmacao : listaConfirmacao){
						if (confirmacao != null){
							if (d.equals(confirmacao.getDocumento())){
								d.setConfirmadobanco(Boolean.TRUE);
								d.setDtagendamentobanco(confirmacao.getData());
							}									
						}
					}
				}
				
				boolean vencida = SinedDateUtils.beforeIgnoreHour(d.getDtvencimento(), hoje);
				d.setProtestada(d.getDtcartorio() != null);
	
				/* Condi��o para mostrar o �cone de vencida na listagem
				 * O Documento deve estar vencido e seu Documentoacao deve ser diferente de CANCELADA e BAIXADA
				 */
				vencida = vencida && !Documentoacao.CANCELADA.equals(d.getDocumentoacao()) && !Documentoacao.BAIXADA.equals(d.getDocumentoacao()) && !Documentoacao.NEGOCIADA.equals(d.getDocumentoacao());
				d.setVencida(vencida);
			}
		}
		
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			Money emAberto = new Money();
			Money emAtraso = new Money();
			Money total = new Money();
			Money totalOriginal = new Money();
			
			Money totalValorContaGerencial = new Money();
			Money totalValorCentroCusto = new Money();
			Money totalValorProjeto = new Money();
					
			List<Documento> list = this.findForSomaListagem(filtro);
			
			if(SinedUtil.isListNotEmpty(list)){
				List<Integer> listaIdsBaixadaParcial = new ArrayList<Integer>();
				List<Integer> listaIdsBaixadaParcialAtraso = new ArrayList<Integer>();
				
				for (Documento d : list) {
					boolean vencida = SinedDateUtils.beforeIgnoreHour(d.getDtvencimento(), hoje);
					Money valoratual = d.getAux_documento().getValoratual();
					
					if(!Documentoacao.CANCELADA.equals(d.getDocumentoacao()) && 
							!Documentoacao.NAO_AUTORIZADA.equals(d.getDocumentoacao()) && 
							!Documentoacao.NEGOCIADA.equals(d.getDocumentoacao()) && 
							!Documentoacao.BAIXADA.equals(d.getDocumentoacao()) 
							/*&& !Documentoacao.BAIXADA_PARCIAL.equals(d.getDocumentoacao())*/ // COMENTADO ESSA LINHA POIS ELE SOMA O VALOR DA BAIXADA AQUI DEPOIS SUBSTRAI EM BAIXO PELO VALOR QUE J� FOI BAIXADO
					){
						emAberto = emAberto.add(valoratual);
						
						if(vencida){
							emAtraso = emAtraso.add(valoratual);
						}
					}
					if(Documentoacao.BAIXADA_PARCIAL.equals(d.getDocumentoacao())){
						listaIdsBaixadaParcial.add(d.getCddocumento());
						if(vencida){
							listaIdsBaixadaParcialAtraso.add(d.getCddocumento());
						}
					}
					total = total.add(valoratual);
					totalOriginal = totalOriginal.add(d.getValor());				
				}
				if(listaIdsBaixadaParcial.size() > 0){
					Money valorBaixadoParcial = movimentacaoService.calcularMovimentacoesByWhereInDocumento(CollectionsUtil.concatenate(listaIdsBaixadaParcial, ",")); 
					if(valorBaixadoParcial != null){
						emAberto = emAberto.subtract(valorBaixadoParcial);
						total = total.subtract(valorBaixadoParcial);
					}
					Money valorBaixadoParcialValecompra = valecompraorigemService.calcularValecompraByWhereInDocumento(CollectionsUtil.concatenate(listaIdsBaixadaParcial, ",")); 
					if(valorBaixadoParcialValecompra != null){
						emAberto = emAberto.subtract(valorBaixadoParcialValecompra);
						total = total.subtract(valorBaixadoParcialValecompra);
					}
					
					if(listaIdsBaixadaParcialAtraso.size() > 0){
						Money valorBaixadoParcialAtraso = movimentacaoService.calcularMovimentacoesByWhereInDocumento(CollectionsUtil.concatenate(listaIdsBaixadaParcialAtraso, ",")); 
						if(valorBaixadoParcial != null){
							emAtraso = emAtraso.subtract(valorBaixadoParcialAtraso);
						}
						Money valorBaixadoParcialValecompraAtraso = valecompraorigemService.calcularValecompraByWhereInDocumento(CollectionsUtil.concatenate(listaIdsBaixadaParcialAtraso, ",")); 
						if(valorBaixadoParcialValecompraAtraso != null){
							emAtraso = emAtraso.subtract(valorBaixadoParcialValecompraAtraso);
						}
					}
				}
				
				if(filtro.getContagerencial()!=null){
					
					for(Documento d: list){
						if(d.getRateio().getListaRateioitem()!=null && !d.getRateio().getListaRateioitem().isEmpty()){
							for(Rateioitem item: d.getRateio().getListaRateioitem()){
								if(item.getContagerencial().equals(filtro.getContagerencial()))
									totalValorContaGerencial = totalValorContaGerencial.add(item.getValor());
							}
						}
					}
					filtro.setTotalValorContaGerencial(totalValorContaGerencial);
				}
				if(filtro.getCentrocusto()!=null){
					
					for(Documento d: list){
						if(d.getRateio().getListaRateioitem()!=null && !d.getRateio().getListaRateioitem().isEmpty()){
							for(Rateioitem item: d.getRateio().getListaRateioitem()){
								if(item.getCentrocusto().equals(filtro.getCentrocusto()))
									totalValorCentroCusto = totalValorCentroCusto.add(item.getValor());
							}
						}
					}
					filtro.setTotalValorCentroCusto(totalValorCentroCusto);
				}
				if(filtro.getProjeto()!=null){
					
					for(Documento d: list){
						if(d.getRateio().getListaRateioitem()!=null && !d.getRateio().getListaRateioitem().isEmpty()){
							for(Rateioitem item: d.getRateio().getListaRateioitem()){
								if(item.getProjeto().equals(filtro.getProjeto()))
									totalValorProjeto = totalValorProjeto.add(item.getValor());
							}
						}
					}
					filtro.setTotalValorProjeto(totalValorProjeto);
				}
			}
			
			filtro.setTotal(total);
			filtro.setTotalOriginal(totalOriginal);
			filtro.setEmAberto(emAberto);
			filtro.setEmAtraso(emAtraso);
		}
	}

	/**
	 * M�todo respons�vel por inserir no bean de documento o n�mero da parcela correspondente
	 * e o total de parcelas do parcelamento.
	 * 
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void ajustaNumeroParcelaDocumento(Documento documento){
		if(SinedUtil.isListNotEmpty(documento.getListaParcela())){
			Parcela parcela = documento.getListaParcela().iterator().next();
			long parc = parcela.getOrdem() != null ? parcela.getOrdem() : 1;
			long total = parcela.getParcelamento().getIteracoes();
			documento.setNumParcela(parc + "/" + total);
		}
		
//		String outros = documento.getOutrospagamento() != null && !documento.getOutrospagamento().equals("") ? documento.getOutrospagamento() : documento.getPessoa() != null ? documento.getPessoa().getNome() : ""; 
//		
//		documento.setOutrospagamento(outros);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Atualiza o Documentoacao de um Documento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateDocumentoacao(Documento)
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void updateDocumentoacao(Documento documento){
		documentoDAO.updateDocumentoacao(documento);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega a lista de Documentohistorico em documento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#carregaDocumentohistorico(Documento)
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documento carregaDocumentohistorico(Documento documento){
		return documentoDAO.carregaDocumentohistorico(documento);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#carregaJurosDocumento
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento carregaJurosDocumento(Documento documento){
		return documentoDAO.carregaJurosDocumento(documento);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO.updateTaxa
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void updateTaxa(Documento documento) {
		documentoDAO.updateTaxa(documento);		
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 16/08/2017
	 */
	public void updateRateio(Documento documento) {
		documentoDAO.updateRateio(documento);	
	}

	/**
	 * Retorna a lista de documentos para a �ltima a��o antes da baixada. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#findPenultimaAcao
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateStatusConta
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#geraHistoricoDocumento
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdateNoUseTransaction
	 * @param documento
	 * @param documentoacao - Acao a ser inserida no documentohistorico
	 * @param observacao
	 * @author Rodrigo Freitas
	 */
	public void retornaDocumentosAcaoAnt(Documento documento, Documentoacao documentoacao, String observacao, boolean forcarPrevista) {
		boolean existevalecompra = valecompraorigemService.existeValecompra(documento);
		boolean existeAntecipacao = historicoAntecipacaoService.existeAntecipacao(documento);
		
		Documentoacao acao = Documentoacao.PREVISTA;
		if(!Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse()) && (existevalecompra || existeAntecipacao)){
			acao = Documentoacao.BAIXADA_PARCIAL;
		}else {
			if(!forcarPrevista){
				acao = documentohistoricoService.findPenultimaAcao(documento);
				if(acao == null) {
					acao = Documentoacao.DEFINITIVA;
					documentoautorizacaoService.deleteAutorizacao(documento);
				}
			} else {
				Boolean hasAutorizacao = documentoautorizacaoService.hasAutorizacao(documento);
				if(hasAutorizacao)
					documentoautorizacaoService.deleteAutorizacao(documento);
			}
		}
		
		contapagarService.updateStatusConta(acao, documento.getCddocumento()+"");
		documento.setDocumentoacao(documentoacao);

		Documentohistorico dh = documentohistoricoService.geraHistoricoDocumento(documento);
		dh.setObservacao(observacao);

		documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
	}

	/**
	 * M�todo para adicionar a lista de Documentos � lista de Fluxo de caixa para gera��o do relat�rio.
	 * 
	 * @param filtro
	 * @param listaFluxo
	 * @param listaMovimentacao
	 * @author Fl�vio Tavares
	 * @param radEvento 
	 */
	public void adicionaDocumentoFluxocaixa(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo, List<Documento> listaDocumento){
		boolean useValoratual = filtro.getValoresDocAtualizados();
		for (Documento doc : listaDocumento) {
			listaFluxo.add(new FluxocaixaBeanReport(doc, useValoratual, filtro.isFiltroOrigem()));
		}
	}

	/**
	 * Faz o processamento de autoriza��o dos documentos selecionados.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateStatusConta(String, Documentoacao)
	 * @see #carregaDocumentos(String)
	 * @see br.com.linkcom.sined.geral.service.DocumentoautoriService#gerarDocumentoAutorizacao(Money, String, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentoautoriService#saveOrUpdateNoUseTransaction(Documentoautori)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @param acao
	 * @param whereIn
	 * @author Hugo Ferreira - revis�o das transactions
	 * @author Tom�s Rabelo - Gera��o de avisos gerais
	 */
	public void doAutorizar(final Documentoacao acao, final String whereIn) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				// muda o status do documento.
				Documentohistorico documentoHistorico = null;
				Documentoautori documentoAutori = null;

				Documentoacao[] condicoes = null;
				if(acao.equals(Documentoacao.AUTORIZADA)){
					condicoes = new Documentoacao[]{Documentoacao.DEFINITIVA, Documentoacao.NAO_AUTORIZADA};
				}
				if(acao.equals(Documentoacao.PREVISTA)){
					condicoes = new Documentoacao[]{Documentoacao.PREVISTA};
				}

				contapagarService.updateStatusConta(acao, whereIn, condicoes);

				List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);

				// gera os historicos das contas selecionadas.
				for (Documento documento : listaDocumento) {
					documentoHistorico = new Documentohistorico();

					if (Documentoacao.AUTORIZADA.equals(acao)) {
						documentoAutori = documentoautorizacaoService.gerarDocumentoAutorizacao(documento.getValor(), "", documento);
						documentoautorizacaoService.saveOrUpdateNoUseTransaction(documentoAutori);
					}

					documentoHistorico = documentohistoricoService.gerarHistoricoDocumento(documentoHistorico, acao, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(documentoHistorico);
					
					atualizaValoreDocumento(documento, documentoHistorico);
				}
				
				//Se a a��o for definita � pq o usu�rio esta confirmando a conta
				if(acao.equals(Documentoacao.DEFINITIVA) && !SinedUtil.isUserHasAction("AUTORIZARPARCIAL_CONTAPAGAR")) {
					avisoService.salvarAvisos(montaAvisoDocumento(MotivoavisoEnum.AUTORIZAR_CONTAPAGAR, listaDocumento, "Conta a pagar autorizada"), true);
				}
				
				return null;
			}

		});

		callProcedureAtualizaDocumento(whereIn);
	}
	
	/**
	 * M�todo que faz as atualiza��es necess�rias no documento. Quando o �ndice de corre��o � aplicado
	 * 
	 * @param documento
	 * @auhtor  Tom�s Rabelo
	 */
	public void atualizaValoreDocumento(Documento documento, Documentohistorico documentoHistorico) {
		StringBuilder observacao = new StringBuilder();
		Money valorAntigo = new Money(documento.getValor());
		Money novoValor = indicecorrecaoService.calculaIndiceCorrecao(documento.getIndicecorrecao(), documento.getValor(), documento.getDtvencimento(), documento.getDtemissao());
		
		if(novoValor.getValue().doubleValue() != valorAntigo.getValue().doubleValue()){
			observacao.append(documento.getDescricao());
			
			documento.setDescricao(documento.getDescricao()+" - R$"+valorAntigo.toString()+" + Corre��o R$"+(novoValor.subtract(valorAntigo).toString()));
			documento.setValorPrevista(documento.getValor());
			documento.setValor(novoValor);
			documento.setDescriacaoCorrecao(" - R$"+valorAntigo.toString()+" + Corre��o R$"+(novoValor.subtract(valorAntigo).toString()));
			updateValorDocumento(documento);
			contareceberService.updateDadosAntesIndeceCorrecao(documento);
			
			if(documento.getRateio() != null && documento.getRateio().getCdrateio() != null && 
			   documento.getRateio().getListaRateioitem() != null && !documento.getRateio().getListaRateioitem().isEmpty()){
				rateioService.atualizaValorRateio(documento.getRateio(), novoValor);
				for (Rateioitem rateioitem : documento.getRateio().getListaRateioitem()) 
					rateioitemService.updateValorRateioItem(rateioitem);
			}
			
			observacao.append(" - R$ " + valorAntigo);
			documentohistoricoService.updateObservacaoDocumento(documentoHistorico, observacao.toString());
		}
	}
	
	/**
	 * M�todo que monta avisos gerais para as contas a pagar e receber
	 * 
	 * @param listaPapeis
	 * @param listaDocumento
	 * @param assunto
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> montaAvisoDocumento(MotivoavisoEnum motivo, List<Documento> listaDocumento, String assunto) {
		List<Aviso> avisos = new ListSet<Aviso>(Aviso.class);
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		if(motivoAviso != null){
			for (Documento documento : listaDocumento){
				if(Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse())){
					avisos.add(new Aviso(assunto, "Conta pagar: "+documento.getCddocumento(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
							motivoAviso.getUsuario(), AvisoOrigem.CONTA_A_PAGAR,  documento.getCddocumento(), documento.getEmpresa(), 
							getWhereInProjetoRateioDocumento(documento), motivoAviso));
				}
			}
		}
		
		return avisos;
	}
	
	private String getWhereInProjetoRateioDocumento(Documento documento) {
		StringBuilder whereInProjeto = new StringBuilder();
		if(documento != null && documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
			for(Rateioitem ri : documento.getRateio().getListaRateioitem()){
				if(ri.getProjeto() != null && ri.getProjeto().getCdprojeto() != null){
					whereInProjeto.append(ri.getProjeto().getCdprojeto().toString()).append(",");
				}
			}
		}
		return whereInProjeto.length() > 0 ? whereInProjeto.substring(0, whereInProjeto.length()-1) : null;
	}
	/**
	 * Cancela um ou mais documentos a partir de seus IDs, setando uma observa��o.
	 * 
	 * @see #cancelarSemTransacao(Documentohistorico)
	 * 
	 * @param itensSelecionados: IDs de documentos a serem cancelados
	 * @param observacao: um coment�rio para ser adicionado no hist�rico (normalmente
	 * 				uma justificativa para o cancelamento).
	 * 
	 * @throws AlterarEstadoException: caso n�o seja poss�vel cancelar algum documento
	 * 
	 * @author Hugo Ferreira
	 */
	public void cancelarSemTransacao(String itensSelecionados, String observacao) {
		Documentohistorico documentoHistorico = new Documentohistorico();
		documentoHistorico.setIds(itensSelecionados);
		documentoHistorico.setObservacao(observacao);
		documentoHistorico.setDocumentoacao(Documentoacao.CANCELADA);
		
		this.cancelarSemTransacao(documentoHistorico);
	}
	
	/**
	 * Executa a a��o "Cancelar documento", sem garantir a transa��o.
	 * 
	 * @see #carregaDocumentos(String)
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateStatusConta(String, Documentoacao)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documento)
	 * @param documentoHistorico
	 * @return
	 * @author Hugo Ferreira
	 */
	public Documentoacao cancelarSemTransacao(Documentohistorico documentoHistorico) {
		return cancelarSemTransacao(documentoHistorico, Boolean.FALSE);
	}
	
	public Documentoacao cancelarSemTransacao(Documentohistorico documentoHistorico, Boolean cancelarNegociada) {
		String whereIn = documentoHistorico.getIds();
		String obs = documentoHistorico.getObservacao();
		Documentoacao acao = documentoHistorico.getDocumentoacao();
		Motivocancelamento motivocancelamento = documentoHistorico.getMotivocancelamento();
		Documentohistorico dh = null;
		
		List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);
		
		Documentoacao condicao = null;
		Documentoacao condicao2 = null;
		Documentoacao condicao3 = null;
		for (Documento documento : listaDocumento) {
			if (acao.equals(Documentoacao.NAO_AUTORIZADA)) {
				condicao = Documentoacao.DEFINITIVA; 
				if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)) {
					throw new AlterarEstadoException("Documento(s) (Conta a pagar ou receber) com situa��o diferente de 'definitiva'.");
				}
			} else if (acao.equals(Documentoacao.CANCELADA)) {
				if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA) || 
					((documentoHistorico.getFromcontareceber() == null || !documentoHistorico.getFromcontareceber()) && notaDocumentoService.isDocumentoWebservice(documento))) 
				{} else {
					condicao = Documentoacao.PREVISTA;
					condicao2 = Documentoacao.DEFINITIVA;
					if(cancelarNegociada != null && cancelarNegociada){
						condicao3 = Documentoacao.NEGOCIADA;
					}
					if(!Boolean.TRUE.equals(cancelarNegociada) && !documento.getDocumentoacao().equals(Documentoacao.PREVISTA) 
							&& !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
						throw new AlterarEstadoException("Documento(s) (Conta a pagar ou receber) com situa��o diferente de 'prevista'  'definitiva'.");
					} else if(Boolean.TRUE.equals(cancelarNegociada) && !documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)
							&& !documento.getDocumentoacao().equals(Documentoacao.PREVISTA) && !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
						throw new AlterarEstadoException("Documento(s) (Conta a pagar ou receber) com situa��o diferente de 'prevista', 'definitiva' e 'negociada'.");
					}
				}
			} else if (acao.equals(Documentoacao.NEGOCIADA)){
				condicao = Documentoacao.PREVISTA;
				condicao2 = Documentoacao.DEFINITIVA;
				condicao3 = Documentoacao.BAIXADA_PARCIAL;
				updateDocumentoacaoanteriornegociacao(documento, documento.getDocumentoacao());
			}
			
			contapagarService.updateStatusConta(documentoHistorico.getDocumentoacao(), documento.getCddocumento().toString(), condicao, condicao2, condicao3);
			
			documentoDAO.cancelaContaDevolvida(documento);
		}
		
		listaDocumento = documentoService.carregaDocumentos(whereIn);
		
		for (Documento documento : listaDocumento) {
			dh = new Documentohistorico();
			dh.setObservacao(obs);
			
			dh = documentohistoricoService.gerarHistoricoDocumento(dh, acao, documento);
			
			dh.setMotivocancelamento(motivocancelamento);
			documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
		}
		return acao;
	}

	/**
	 * Executa a a��o "Cancelar documento" em uma transa��o.
	 * 
	 * 
	 * @see #cancelarSemTransacao(Documentohistorico)
	 * @param documentoHistorico
	 * @return
	 * @throws AlterarEstadoException: caso n�o seja poss�vel cancelar algum documento
	 * 
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public Documentoacao doCancelar(final Documentohistorico documentoHistorico) {
		return doCancelar(documentoHistorico, Boolean.FALSE);
	}
	
	public Documentoacao doCancelar(final Documentohistorico documentoHistorico, final Boolean cancelarNegociada) {
		Object acao = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				return cancelarSemTransacao(documentoHistorico, cancelarNegociada);
			}
		});
		callProcedureAtualizaDocumento(documentoHistorico.getIds());
		return (Documentoacao) acao;
	}

	/**
	 * Executa a a��o "Reagendar documento" em uma transaction, quando � solicitada uma n�o-autoriza��o de documento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateDataVencimento(String, Date)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#carregaDocumentos(String)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @param documentoHistorico
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public void doReagendar(final Documentohistorico documentoHistorico) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				contapagarService.updateDataVencimento(documentoHistorico.getIds(), documentoHistorico.getDtcartorio());

				Documentohistorico dh = null;
				String whereIn = documentoHistorico.getIds();
				String obs = documentoHistorico.getObservacao();
				List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);

				for (Documento documento : listaDocumento) {
					dh = new Documentohistorico();
					dh.setObservacao(obs);

					dh = documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.REAGENDAMENTO, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
				}

				return null;
			}
		});
		callProcedureAtualizaDocumento(documentoHistorico.getIds());
	}

	/**
	 * Executa a a��o "Autorizar parcial" de Contas a pagar em uma transaction.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateStatusConta(String, Documentoacao)
	 * @see #carregaDocumento(Documento)
	 * @see #updateValorDocumento(Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentoautoriService#gerarDocumentoAutorizacao(Money, String, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentoautoriService#saveOrUpdateNoUseTransaction(Documentoautori)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @see br.com.linkcom.sined.geral.service.RateioService#atualizaValorRateio(Rateio, Money)
	 * @see br.com.linkcom.sined.geral.service.RateioService#saveOrUpdateNoUseTransaction(Rateio)
	 * @param listaDocumentohistorico
	 * @author Hugo Ferreira - revis�o das transactions
	 * @param proporcaoPercentual 
	 */
	public void doAutorizarParcial(final List<Documentohistorico> listaDocumentohistorico, final double proporcaoPercentual) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {

				for (Documentohistorico documentohistorico : listaDocumentohistorico){
					String obs = documentohistorico.getObservacao();
					Documentoacao acao = documentohistorico.getDocumentoacao();
					Documento documento = documentohistorico.getDocumento();
					documento = documentoService.loadForEntrada(documento);
									
					if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && !documento.getDocumentoacao().equals(Documentoacao.NAO_AUTORIZADA)) {
						throw new SinedException("Registro(s) com situa��o diferente de 'definitiva' e 'n�o autorizada'.");
					}
					
					contapagarService.updateStatusConta(documentohistorico.getDocumentoacao(), String.valueOf(documentohistorico.getDocumento().getCddocumento()), 
							/*Condi��es*/Documentoacao.DEFINITIVA,Documentoacao.NAO_AUTORIZADA);
					
					documento.setValor(documento.getValor().multiply(new Money(proporcaoPercentual)));
					documentoService.updateValorDocumento(documento);
					
					// Atualiza��o dos valores do rateio.
					Rateio rateio = documento.getRateio();
					rateioService.atualizaValorRateio(rateio, documento.getValor());
					rateioService.saveOrUpdateNoUseTransaction(rateio);
					
					documentohistorico.setObservacao(obs);
	
					// n�o pode colocar o prepara hist�rico primeiro pois ele seta outro valor.
					Documentoautori autorizacao = documentoautorizacaoService.gerarDocumentoAutorizacao(documento.getValor(), obs, documento);
					documentoautorizacaoService.saveOrUpdateNoUseTransaction(autorizacao);
	
					Documentohistorico dh = documentohistoricoService.gerarHistoricoDocumento(documentohistorico, acao, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
				}
				
				return null;
			}
		});
		
		for (Documentohistorico documentohistorico : listaDocumentohistorico){
			
			callProcedureAtualizaDocumento(documentohistorico.getDocumento());
		}
	}

	/**
	 * Executa a a��o "Estornar" em uma transaction.
	 * 
	 * @see #carregaDocumentos(String)
	 * @see br.com.linkcom.sined.geral.service.DocumentoautoriService#deleteAutorizacao(Documento)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#isMovimentacaoCancelada(Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateStatusConta(String, Documentoacao)
	 * @param documentohistorico
	 * @author Hugo Ferreira - revis�o das transactions
	 * @param request 
	 */
	@SuppressWarnings("unchecked")
	public List<String> doEstornar(final Documentohistorico documentoHistorico) {
		Object listaErros = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				List<String> listaErros = new ArrayList<String>();
				String idsstring = documentoHistorico.getIds();
				String obs = documentoHistorico.getObservacao();
				Documentoacao acao = documentoHistorico.getDocumentoacao();
				List<Documento> listaDocumento = documentoService.carregaDocumentos(idsstring);
				Documentohistorico dh = null;

				List<Documento> listaReceber = new ArrayList<Documento>();
				List<Documento> listaPagar = new ArrayList<Documento>();

				for (Documento documento : listaDocumento) {
					if (documento.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)) {
						// CONTA A PAGAR
						listaPagar.add(documento);
						if (documento.getDocumentoacao().equals(Documentoacao.PREVISTA)) {
							throw new SinedException("Registro(s) com situa��o 'prevista'.");
						}
					} else {
						// CONTA A RECEBER
						listaReceber.add(documento);
						if (documento.getDocumentoacao().equals(Documentoacao.PREVISTA)) {
							throw new SinedException("Registro(s) com situa��o 'prevista'.");
						} else if (documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)) {
							List<Documento> parcelasNegociacao = contareceberService.findParcelasNegociacao(documento);
							if (parcelasNegociacao != null){
								for (Documento parcela : parcelasNegociacao) {
									if (parcela.getDocumentoacao().equals(Documentoacao.BAIXADA)){
										throw new SinedException("N�o � poss�vel estornar a conta "+documento.getCddocumento()+". J� existe(m) parcela(s) baixada(s).");
									}
									if (parcela.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL)){
										throw new SinedException("N�o � poss�vel estornar a conta "+documento.getCddocumento()+". J� existe(m) parcela(s) baixada(s) parcialmente.");
									}
									if (parcela.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
										throw new SinedException("N�o � poss�vel estornar a conta "+documento.getCddocumento()+". Existe(m) parcela(s) que foi(ram) negociada(s). Para estornar essa conta a(s) parcela(s) negociada(s) deve(m) ser estornada(s).");
									}
								}
							}							
						}
					}

					dh = new Documentohistorico();
					dh.setObservacao(obs);

					if (Documentoacao.AUTORIZADA.equals(documento.getDocumentoacao()) || Documentoacao.AUTORIZADA_PARCIAL.equals(documento.getDocumentoacao())) {
						documentoautorizacaoService.deleteAutorizacao(documento);
					}

					if (Documentoacao.BAIXADA.equals(documento.getDocumentoacao())) {
						if (movimentacaoService.isMovimentacaoCancelada(documento)) {
							listaErros.add("N�o foi poss�vel estornar a situa��o da conta a pagar "+documento.getCddocumento()+", pois h� uma movimenta��o financeira(n�o cancelada) vinculada.");
							listaPagar.remove(documento);
							listaReceber.remove(documento);
							continue;
						} else {
							documentoautorizacaoService.deleteAutorizacao(documento);
						}
					}

					List<Documento> parcelasNegociacao = contareceberService.findParcelasNegociacao(documento);
					if (parcelasNegociacao != null){
						for (Documento parcela : parcelasNegociacao) {
							if (parcela.getDocumentoacao().equals(Documentoacao.DEFINITIVA) ||
								parcela.getDocumentoacao().equals(Documentoacao.PREVISTA)){
								parcela.setDocumentoacao(Documentoacao.CANCELADA);
								updateDocumentoacao(parcela);
							}									
						}
					}
					
					dh = documentohistoricoService.gerarHistoricoDocumento(dh, acao, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
					
//					if(notaDocumentoService.isDocumentoWebservice(documento)){
//						notaDocumentoService.estornaNotaDocumentoWebservice(documento);
//					}
				}

				// ATUALIZA��O DAS CONTAS A PAGAR
				Documentoacao[] condicoes = new Documentoacao[]{Documentoacao.BAIXADA,Documentoacao.BAIXADA_PARCIAL,Documentoacao.AUTORIZADA,
						Documentoacao.AUTORIZADA_PARCIAL,Documentoacao.NAO_AUTORIZADA,Documentoacao.CANCELADA, Documentoacao.DEFINITIVA};
				if (listaPagar != null && listaPagar.size() > 0) {
					contapagarService.updateStatusConta(Documentoacao.PREVISTA, CollectionsUtil.listAndConcatenate(listaPagar,"cddocumento", ","), 
							/* Condi��es -> */ condicoes);
				}

				// ATUALIZA��O DAS CONTAS A RECEBER
				condicoes = new Documentoacao[]{Documentoacao.BAIXADA,Documentoacao.BAIXADA_PARCIAL,Documentoacao.AUTORIZADA,
						Documentoacao.AUTORIZADA_PARCIAL,Documentoacao.NAO_AUTORIZADA,Documentoacao.CANCELADA,Documentoacao.DEFINITIVA, Documentoacao.NEGOCIADA};
				if (listaReceber != null && listaReceber.size() > 0) {
					contapagarService.updateStatusConta(Documentoacao.PREVISTA, CollectionsUtil.listAndConcatenate(listaReceber,"cddocumento", ","), 
							/* Condi��es -> */ condicoes);
					for (Documento documento : listaReceber){
						if (documento.getIndicecorrecao() != null){
							Documento aux = documentoService.load(documento, "documento.valorPrevista, documento.descriacaoCorrecao");
							String descricao;
							if (aux.getDescriacaoCorrecao() == null || aux.getDescriacaoCorrecao().isEmpty()){
								descricao = documento.getDescricao();
								try {
									NeoWeb.getRequestContext().addMessage(
									 "A conta " + documento.getCddocumento() + " estornada possui �ndice de corre��o e n�o foi poss�vel recuperar o valor original. � necess�rio altera-la manualmente.",
									 MessageType.WARN);
								} catch (Exception e) {}
								contareceberService.voltaValorIndiceCorrecao(documento, null, descricao);
							}
							else {
								if (documento.getDescricao().contains(aux.getDescriacaoCorrecao())){
									descricao = documento.getDescricao().replace(aux.getDescriacaoCorrecao(), "");
								}
								else{
									descricao = documento.getDescricao();
								}
								contareceberService.voltaValorIndiceCorrecao(documento, aux.getValorPrevista(), descricao);
							}
						}
					}
				}
				
				if (!listaReceber.isEmpty()){
					valecompraService.estornarContaReceber(false, CollectionsUtil.listAndConcatenate(listaReceber, "cddocumento", ","));
				}
				
				return listaErros;
			}
		});
		
		callProcedureAtualizaDocumento(documentoHistorico.getIds());
		return (List<String>) listaErros;
	}

	/**
	 * Executa a a��o "Colocar em cart�rio" em uma transaction.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#carregaDocumentos(String)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#gerarHistoricoDocumento(Documentohistorico, Documentoacao, Documento)
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#saveOrUpdateNoUseTransaction(Documentohistorico)
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#updateCartorio(String, Date)
	 * @param documentoHistorico
	 * @return
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public String doEmCartorio(final Documentohistorico documentoHistorico) {
		Object nomes = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Date dtcartorio = documentoHistorico.getDtcartorio();
				String whereIn = documentoHistorico.getIds();
				String obs = documentoHistorico.getObservacao();
				Documentoacao acaoProtesto = documentoHistorico.getDocumentoacao();
				if(acaoProtesto == null) acaoProtesto = Documentoacao.PROTESTADA;
				Boolean fromAcaoProtestar = documentoHistorico.getFromAcaoProtestar();
				String nomes = "";
				Documentohistorico dh = null;

				List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);

				for (Documento documento: listaDocumento) {
					if(documento.getDtcartorio() != null && documento.getDocumentoacao() == null){
						documento.setDocumentoacao(Documentoacao.PROTESTADA);
					}
					//S� podem ser protestadas se tiver a dtcartorio diferente de null.
					//ou for trocar a situa��o do protesto 
					if (documento.getDtcartorio() != null && acaoProtesto.getCddocumentoacao().equals(documento.getDocumentoacaoprotesto().getCddocumentoacao())) {
						whereIn = whereIn.replace(documento.getCddocumento()+",", "");
						whereIn = whereIn.replace(","+documento.getCddocumento(), "");
						nomes += documento.getCddocumento()+", ";
						continue;
					}
					
					documento.setDocumentoacaoprotesto(acaoProtesto);
					documento.setFromAcaoProtestar(fromAcaoProtestar);
					dh = new Documentohistorico();
					dh.setObservacao(obs);


					dh = documentohistoricoService.gerarHistoricoDocumento(dh, acaoProtesto, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
				}

				if (!whereIn.equals("")) {
					contapagarService.updateCartorio(whereIn, dtcartorio, acaoProtesto);
				}
				return nomes;
			}
		});
		callProcedureAtualizaDocumento(documentoHistorico.getIds());
		return (String) nomes;
	}

	/**
	 * M�todo que gera um relat�rio de Conta a Pagar OU Conta a Receber.
	 * A gera��o dos mesmos foi agrupada nesta fun��o em 18/07/2008 por possuir v�rias
	 * coisas em comum. O tratamento de coisas espec�ficas s�o feitas dentro do primeiro IF.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForReport(DocumentoFiltro)
	 * @param filtro
	 * @return
	 * @author Flavio Tavares
	 * @author Andr� Brunelli
	 * @author Hugo Ferreira
	 * @author Leandro Lima
	 */
	public Report gerarRelarorioDocumento(DocumentoFiltro filtro) {
		//Este peda�o faz os tratamentos particulares a cada um dos tipos de relat�rio: 
		Report report = new Report("/financeiro/conta");
		Report subReport = new Report("/financeiro/conta_subreport");
		Report subReportCentrocusto = new Report("/financeiro/contaCentrocusto_subreport");
		Report subReportTotalPorTipo = new Report("/financeiro/contaTotalPorTipo_subreport");
		addInfRelatorioDocumento(filtro, report);
		
		String labeldescricao = "Recebimento de";
		Money totalCorrigido = new Money();
		Money total = new Money();
		Money totalPagto = new Money();
		Money totalRestante = new Money();
		List<ContaReceberPagarBeanReport> listaContaDetalhada = montaRelatorioDocumento(filtro, labeldescricao);
		verificaQuantidadeRegistrosForPdf(listaContaDetalhada);
		
		for (ContaReceberPagarBeanReport c : listaContaDetalhada) {
			total = total.add(c.getValorAtual());
			c.setValorrestante(new Money());
			boolean ratearValorProporcionalmente = ((filtro.getListaCentrocusto() != null && filtro.getListaCentrocusto().size() > 0) ||
													(filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0) ||
													(filtro.getListaContagerencial() != null && filtro.getListaContagerencial().size() > 0)) &&
													c.getPercentualRateio() < 100;
			if(c.getSituacao().equalsIgnoreCase("baixada")){
				c.setValorPago(c.getValorAtual());
				totalPagto = totalPagto.add(c.getValorPago());
			}else if(c.getSituacao().equalsIgnoreCase("baixada parcial")){
				Money valorpago = c.getValorMovimentacoes();
				if(c.getValorValecompra() != null){
					valorpago = valorpago != null ? valorpago.add(c.getValorValecompra()) : c.getValorValecompra();
				}
				if(c.getValorAntecipado() != null){
					valorpago = valorpago != null ? valorpago.add(c.getValorAntecipado()) : c.getValorAntecipado();
				}
				if(ratearValorProporcionalmente){
					valorpago = valorpago.divide(new Money(100)).multiply(new Money(c.getPercentualRateio()));
				}
				
				c.setValorPago(valorpago);
				c.setValorrestante(c.getValorOriginal().add(c.getTaxa()).subtract(c.getValorPago()));
				totalRestante = totalRestante.add(c.getValorrestante());
				totalPagto = totalPagto.add(c.getValorPago());
			}
			if(!filtro.getTipoConta().equals("PAGAR") && c.getCdindicecorrecao() != null && c.getSituacao().equals("PREVISTA")){
				c.setValorCorrigido(indicecorrecaoService.calculaIndiceCorrecao(
																	new Indicecorrecao(c.getCdindicecorrecao()), 
																	c.getValorAtual(), 
																	new java.sql.Date(c.getDtVencimento().getTime()), 
																	new java.sql.Date(c.getDtEmissao().getTime())));
				 
				c.setValorrestante(ratearValorProporcionalmente? c.getValorCorrigido().divide(new Money(100)).multiply(new Money(c.getPercentualRateio())):
									c.getValorCorrigido());

				totalRestante = totalRestante.add(c.getValorrestante());
			} else if(c.getValor().compareTo(c.getValorAtual()) != 0){
				c.setValorCorrigido(c.getValorAtual());
				
			}
			
			if(!c.getSituacao().equalsIgnoreCase("baixada") && !c.getSituacao().equalsIgnoreCase("baixada parcial")){
				c.setValorPago(new Money());
				Money valorAux = c.getValorCorrigido() != null? c.getValorCorrigido(): c.getValorAtual();
				c.setValorrestante(ratearValorProporcionalmente? valorAux.divide(new Money(100)).multiply(new Money(c.getPercentualRateio())):
									valorAux);
				totalRestante = totalRestante.add(c.getValorrestante());
			}
			
			if(c.getValorCorrigido() != null) totalCorrigido = totalCorrigido.add(c.getValorCorrigido());
		}
		if(filtro.getTipoConta().equals("PAGAR")){
			labeldescricao = "Pagamento a";			
		}
		
		if(listaContaDetalhada.size()==0){
			report.addParameter("LISTA_VAZIA", true);
		}else{
			report.addParameter("LISTA_VAZIA", false);
		}
		
		report.addParameter("TOTALREGISTROS", listaContaDetalhada.size());
		report.addParameter("VALORTOTALCORRIGIDO", totalCorrigido.getValue());
		report.addParameter("VALORTOTAL", total.getValue());
		report.addParameter("VALORTOTALPAGTO", totalPagto);
		report.addParameter("VALORTOTALRESTANTE", totalRestante);
		report.addParameter("LABELDESCRICAO", labeldescricao);
		report.addParameter("LISTACENTROCUSTO", this.gerarResumoCentrocusto(listaContaDetalhada));
		report.addParameter("LISTATOTALPORTIPO", this.gerarTotalPorTipoDeConta(listaContaDetalhada));
		report.setDataSource(listaContaDetalhada);
		report.addParameter("isContaReceber", !filtro.getTipoConta().equals("PAGAR"));
		report.addSubReport("CONTA_SUBREPORT", subReport);
		report.addSubReport("CONTACENTROCUSTO_SUBREPORT", subReportCentrocusto);
		report.addSubReport("CONTATOTALPORTIPO_SUBREPORT", subReportTotalPorTipo);
		
		return report;
	}
	
	private void addInfRelatorioDocumento(DocumentoFiltro filtro, Report report){
		String listaEmpresas = "";
		Empresa empresa = new Empresa();
		if(filtro.getListaEmpresa() != null && filtro.getListaEmpresa().size() == 1){
			for (ItemDetalhe item : filtro.getListaEmpresa()) {
				empresa.setCdpessoa(item.getEmpresa().getCdpessoa());
				empresa = empresaService.loadForEntrada(item.getEmpresa());
				//Seta a empresa no filtro pois o SinedReport procura este atributo no filtro para carregar a logomarca e nome
				filtro.setEmpresa(empresa);
			}
		}else if(filtro.getEmpresa() != null){
			empresa = empresaService.loadForEntrada(filtro.getEmpresa());
			listaEmpresas += empresaService.getEmpresaRazaosocialOuNome(empresa) +  ","; 
		}else{
			empresa = empresaService.loadPrincipal();
		}
		if (filtro.getListaEmpresa()!=null && !filtro.getListaEmpresa().isEmpty()){
			for (ItemDetalhe item : filtro.getListaEmpresa()) {
				empresa.setCdpessoa(item.getEmpresa().getCdpessoa());
				empresa = empresaService.load(item.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial");
				listaEmpresas += empresaService.getEmpresaRazaosocialOuNome(empresa) +  ",";
			}
		}
		
		if(StringUtils.isNotBlank(listaEmpresas)){
			listaEmpresas = listaEmpresas.trim().substring(0, listaEmpresas.length()-1);
		}
		
		report.addParameter("EMPRESATITULO", empresa.getRazaosocialOuNome());
		report.addParameter("empresaFiltro", listaEmpresas);
		
	}
	
	public void verificaQuantidadeRegistrosForPdf(List<ContaReceberPagarBeanReport> listaContaDetalhada){
		if(listaContaDetalhada.size() > 10000){
			boolean isListagem = NeoWeb.getRequestContext().getParameter("fromListagem") != null && NeoWeb.getRequestContext().getParameter("fromListagem").equals("true");
			String link = "aqui";
			if(isListagem){
				String classe = NeoWeb.getRequestContext().getParameter("classe");
				if(classe != null && classe.equals("PAGAR")){
					link = "<a href=\"#\" onclick=\"form.ACAO.value ='gerar';if (mensagemConfirmacao('0')) {form.action = '/w3erp/financeiro/relatorio/Conta?classe=PAGAR&fromListagem=true'; submitForm()}\">aqui</a>";
				} else {
					link = "<a href=\"#\" onclick=\"form.ACAO.value ='gerar';if (mensagemConfirmacao('0')) {form.action = '/w3erp/financeiro/relatorio/Conta?classe=RECEBER&fromListagem=true'; submitForm()}\">aqui</a>";
				}
			} else {
				link = "<a href=\"#\" onclick=\"form.ACAO.value ='gerar';if (filtroValido('1')) {form.action = '/w3erp/financeiro/relatorio/ContaCSV?clearFilter=true'; form.validate = 'true'; submitForm()}\">aqui</a>";
			}
			throw new SinedException("Quantidade de registros acima do permitido para gerar PDF.<BR>Clique " + link + " para gerar CSV.");
		}
	}
	
//	asd
	public Report gerarRelarorioDocumentoSimplificado(DocumentoFiltro filtro) {
		//Este peda�o faz os tratamentos particulares a cada um dos tipos de relat�rio: 
		Report report = new Report("/financeiro/contasimplificado");
		addInfRelatorioDocumento(filtro, report);

		String labeldescricao = "Recebimento de";
		Money total = new Money();
		Money totalAtrasada = new Money();
		Money totalRestante = new Money();
		Money totalPagto = new Money();
		Money totalJurosMulta = new Money();
		Report subReportTotalPorTipo = new Report("/financeiro/contaTotalPorTipo_subreport");
		List<ContaReceberPagarBeanReport> listaContaDetalhada = montaRelatorioDocumento(filtro, labeldescricao);
		verificaQuantidadeRegistrosForPdf(listaContaDetalhada);
		
		HashMap<String, Money> mapFormaPagamento = new HashMap<String, Money>();	
		HashMap<String, Money> mapSituacao = new HashMap<String, Money>();
		
		for (ContaReceberPagarBeanReport c : listaContaDetalhada) {
			total = total.add(c.getValorAtual());
			
		
			if(c.getValorJurosMulta() != null){
				totalJurosMulta = totalJurosMulta.add(c.getValorJurosMulta());
			}
			c.setValorrestante(new Money());
			if(c.getSituacao().equalsIgnoreCase("baixada")){
				c.setValorPago(c.getValorAtual());
				totalPagto = totalPagto.add(c.getValorPago());
			}else if(c.getSituacao().equalsIgnoreCase("baixada parcial")){
				Money valorpago = c.getValorMovimentacoes();
				if(c.getValorValecompra() != null){
					valorpago = valorpago != null ? valorpago.add(c.getValorValecompra()) : c.getValorValecompra();
				}
				if(c.getValorAntecipado() != null){
					valorpago = valorpago != null ? valorpago.add(c.getValorAntecipado()) : c.getValorAntecipado();
				}
				
				c.setValorPago(valorpago);
				c.setValorrestante(c.getValorOriginal().add(c.getTaxa()).subtract(c.getValorPago()));
				totalRestante = totalRestante.add(c.getValorrestante());
				totalPagto = totalPagto.add(c.getValorPago());
			}
			if(!filtro.getTipoConta().equals("PAGAR") && c.getCdindicecorrecao() != null && c.getSituacao().equals("PREVISTA")){
				c.setValorCorrigido(indicecorrecaoService.calculaIndiceCorrecao(
																	new Indicecorrecao(c.getCdindicecorrecao()), 
																	c.getValorAtual(), 
																	new java.sql.Date(c.getDtVencimento().getTime()), 
																	new java.sql.Date(c.getDtEmissao().getTime())));
				 
				c.setValorrestante(c.getValorCorrigido());

				totalRestante = totalRestante.add(c.getValorrestante());
			} else if(c.getValor().compareTo(c.getValorAtual()) != 0){
				c.setValorCorrigido(c.getValorAtual());
				
			}
			
			if(!c.getSituacao().equalsIgnoreCase("baixada") && !c.getSituacao().equalsIgnoreCase("baixada parcial")){
				c.setValorPago(new Money());
				c.setValorrestante(c.getValorCorrigido() != null? c.getValorCorrigido(): c.getValorAtual());
				
				totalRestante = totalRestante.add(c.getValorrestante());
			}
			
			if(c.getDtVencimento() != null && SinedDateUtils.beforeIgnoreHour(c.getDtVencimento(), new Date(System.currentTimeMillis()))){
				if((c.getSituacao().equalsIgnoreCase("Prevista") || c.getSituacao().equalsIgnoreCase("Definitiva")) && 
						(c.getValorMovimentacoes() == null || c.getValorMovimentacoes().getValue().doubleValue() == 0)){
					totalAtrasada = totalAtrasada.add(c.getValorAtual());
				}else if(c.getSituacao().equalsIgnoreCase("baixada parcial") && c.getValorrestante() != null){
					totalAtrasada = totalAtrasada.add(c.getValorrestante());
				}
			}
			
			Money valorTotalSituacao = mapSituacao.get(c.getSituacao());
			if(valorTotalSituacao != null){
				mapSituacao.put(c.getSituacao(), valorTotalSituacao.add(c.getValorAtual()));
			}else {
				mapSituacao.put(c.getSituacao(), c.getValorAtual());
			}
			if(c.getSituacao().equalsIgnoreCase("baixada")){
				Documento doc = new Documento(c.getCdconta());
				
				List<Movimentacaoorigem> mov = movimentacaoorigemService.buscarMovimentacaoDocumento(doc);
				if(mov != null){
					for (Movimentacaoorigem origem : mov) {
						if(origem.getMovimentacao().getListaMovimentacaoorigem().size() == 1){
							if(origem.getMovimentacao()!=null && origem.getMovimentacao().getFormapagamento() != null){	
								Money valorTotalPagemento = mapFormaPagamento.get(origem.getMovimentacao().getFormapagamento().getNome());
								if(valorTotalPagemento != null && c.getValorAtual() != null){
									mapFormaPagamento.put(origem.getMovimentacao().getFormapagamento().getNome(), valorTotalPagemento.add(origem.getMovimentacao().getValor()));	
								}else if(c.getValorAtual() != null){
									mapFormaPagamento.put(origem.getMovimentacao().getFormapagamento().getNome(), origem.getMovimentacao().getValor());
								}
							}
						}else
							if(origem.getMovimentacao()!=null && origem.getMovimentacao().getFormapagamento() != null){	
								Money valorTotalPagemento = mapFormaPagamento.get(origem.getMovimentacao().getFormapagamento().getNome());
								Documento valorAtual = buscarValorAtual(origem.getDocumento());
								if(valorTotalPagemento != null && c.getValorAtual() != null){
									mapFormaPagamento.put(origem.getMovimentacao().getFormapagamento().getNome(),valorTotalPagemento.add(valorAtual.getAux_documento().getValoratual()));	
								}else if(c.getValorAtual() != null){
									mapFormaPagamento.put(origem.getMovimentacao().getFormapagamento().getNome(),valorAtual.getAux_documento().getValoratual());
								}
						}
					}
				}
			}
		}
		
		List<ContaReceberPagarBeanReport> listaContaResumoSituacao = new ArrayList<ContaReceberPagarBeanReport>();
		
		if(mapSituacao != null && mapSituacao.size() > 0){
			for(String situacao : mapSituacao.keySet()){
				ContaReceberPagarBeanReport c = new ContaReceberPagarBeanReport();
				c.setSituacao(situacao);
				c.setValor(mapSituacao.get(situacao));
				listaContaResumoSituacao.add(c);
			}
		}
		
		
		List<ContaReceberPagarBeanReport> listaContaResumoFormapagamento = new ArrayList<ContaReceberPagarBeanReport>();
		if(mapFormaPagamento != null && mapFormaPagamento.size() > 0){
			for(String pagamento : mapFormaPagamento.keySet()){
				ContaReceberPagarBeanReport c = new ContaReceberPagarBeanReport();
				if(mapFormaPagamento.get(pagamento) !=null){
					c.setValor(mapFormaPagamento.get(pagamento));
					if(pagamento ==null){
						pagamento = "N�O INFORMADO";
					}
					c.setFormapagamento(pagamento);
					listaContaResumoFormapagamento.add(c);
				}
			}
				
		}
		
		//ordena listas
		Collections.sort(listaContaResumoSituacao,new Comparator<ContaReceberPagarBeanReport>() {		
			public int compare(ContaReceberPagarBeanReport o1, ContaReceberPagarBeanReport o2) {
				return o1.getSituacao().toUpperCase().compareTo(o2.getSituacao().toUpperCase());
			}
		});
		
		Collections.sort(listaContaResumoFormapagamento,new Comparator<ContaReceberPagarBeanReport>() {		
			public int compare(ContaReceberPagarBeanReport o1, ContaReceberPagarBeanReport o2) {
				return o1.getFormapagamento().toUpperCase().compareTo(o2.getFormapagamento().toUpperCase());
			}
		});
		
		
		report.addParameter("TOTALREGISTROS", listaContaDetalhada.size());
		report.addParameter("VALORTOTALATRASADAS", totalAtrasada);
		
		Report subReportResumoSituacao = new Report("/financeiro/sub_contaresumosituacao");
		report.addParameter("LISTARESUMOSITUACAO", listaContaResumoSituacao);
		report.addSubReport("CONTARESUMOSITUACAO_SUBREPORT", subReportResumoSituacao);
		
		Report subReportResumoFormapagamento = new Report("/financeiro/sub_contaresumoformapagamento");
		report.addParameter("LISTARESUMOFORMAPAGAMENTO", listaContaResumoFormapagamento);
		report.addSubReport("CONTARESUMOFORMAPAGAMENTO_SUBREPORT", subReportResumoFormapagamento);

		report.addParameter("isContaReceber", !filtro.getTipoConta().equals("PAGAR"));
		
		report.addParameter("LISTATOTALPORTIPO", this.gerarTotalPorTipoDeConta(listaContaDetalhada));
		report.addSubReport("CONTATOTALPORTIPO_SUBREPORT", subReportTotalPorTipo);
		
		
		if(filtro.getTipoConta().equals("PAGAR")){
			labeldescricao = "Pagamento a";			
		}
		
		report.addParameter("VALORTOTAL", total.getValue());
		report.addParameter("VALORTOTALPAGTO", totalPagto);
		report.addParameter("VALORTOTALRESTANTE", totalRestante);
		report.addParameter("VALORTOTALJUROSMULTA", totalJurosMulta);
		report.addParameter("LABELDESCRICAO", labeldescricao);
		report.setDataSource(listaContaDetalhada);
		
		return report;
	}
	
	
	public Documento buscarValorAtual(Documento doc) {
		List<Documento> docAux = documentoDAO.obterListaValorAtual(doc.getCddocumento().toString()); 
		return docAux.get(0);
	}
	/**
	 * Faz uma compara��o por meio de uma pesquisa bin�ria 
	 * e agrupa os centros de custo e soma os seus valores
	 * @param listaContaDetalhada
	 * @return
	 * @author Taidson
	 * @since 12/11/2010
	 */
	@SuppressWarnings("unchecked")
	public List<Centrocusto> gerarResumoCentrocusto(List<ContaReceberPagarBeanReport> listaContaDetalhada){
		
		List<Centrocusto> listaCentrocusto = new ArrayList<Centrocusto>();
		
		for (ContaReceberPagarBeanReport item : listaContaDetalhada) {
			if(item.getListaRateioitem() != null && item.getListaRateioitem().size() > 0){
				Centrocusto aux = null;
				for (Rateioitem ri : item.getListaRateioitem()) {
					if(ri.getCentrocusto() != null && ri.getCentrocusto().getNome()!= null){
						int index = Collections.binarySearch(listaCentrocusto, ri.getCentrocusto(), new BeanComparator("nome"));
						if (index >= 0) {
							aux = listaCentrocusto.get(index);
							aux.setValorTotal(aux.getValorTotal().add(ri.getValor()));
						}else{
							aux = ri.getCentrocusto();
							aux.setValorTotal(ri.getValor());
							listaCentrocusto.add((index * -1)-1, aux);
						}
					}
					
				}
			}
		}
		return listaCentrocusto;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Documentotipo> gerarTotalPorTipoDeConta(List<ContaReceberPagarBeanReport> listaContaDetalhada){
		
		List<Documentotipo> listaCentrocusto = new ArrayList<Documentotipo>();
		
		for (ContaReceberPagarBeanReport item : listaContaDetalhada) {
			Documentotipo aux = null;
			if(item.getDocumentotipo() != null && item.getDocumentotipo().getNome() == null){
				item.getDocumentotipo().setNome("N�O INFORMADO");
			}
			
			if(item.getDocumentotipo() != null && item.getDocumentotipo().getNome()!= null){
				int index = Collections.binarySearch(listaCentrocusto, item.getDocumentotipo(), new BeanComparator("nome"));
				if (index >= 0) {
					aux = listaCentrocusto.get(index);
					aux.setValorTotal(aux.getValorTotal().add(item.getValorAtual()));
				}else{
					aux = item.getDocumentotipo();
					aux.setValorTotal(item.getValorAtual());
					listaCentrocusto.add((index * -1)-1, aux);
				}
			}
		
		}
		
		Collections.sort(listaCentrocusto,new Comparator<Documentotipo>() {		
			public int compare(Documentotipo o1, Documentotipo o2) {
				return o1.getNome().toUpperCase().compareTo(o2.getNome().toUpperCase());
			}
		});
		
		return listaCentrocusto;
	}
	
	
	public List<ContaReceberPagarBeanReport> findForReport(DocumentoFiltro filtro) {
		filtro.setPermitirFiltroDocumentoDeAte(false);
		if(filtro.getDocumentoDe() != null || filtro.getDocumentoAte() != null){
			filtro.setPermitirFiltroDocumentoDeAte(documentoService.permitirFiltroDocumentoDeAte());
			if(filtro.getPermitirFiltroDocumentoDeAte() == null || !filtro.getPermitirFiltroDocumentoDeAte()){
				if(filtro.getDocumentoDe() == null || filtro.getDocumentoAte() == null || !filtro.getDocumentoDe().equals(filtro.getDocumentoAte())){
					filtro.setDocumentoDe(null);
					filtro.setDocumentoAte(null);
				}
			}
		}
		List<ContaReceberPagarBeanReport> lista = documentoDAO.findForReport(filtro);
		boolean listarRazaosocialAndNomefantasia = parametrogeralService.getBoolean(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA);
		boolean listarRazaosocial = Documentoclasse.OBJ_RECEBER.equals(filtro.getDocumentoclasse()) &&
									parametrogeralService.getBoolean(Parametrogeral.LISTARRAZAOSOCIALCLIENTE);
		for(ContaReceberPagarBeanReport bean: lista){
			if(StringUtils.isNotBlank(bean.getOutrospagamento())){
				bean.setDescricaoPagamento(bean.getOutrospagamento());
			}else{
				if(listarRazaosocialAndNomefantasia){
					String razaoSocialAndNome = bean.getRazaosocialPessoa();
					razaoSocialAndNome = (Util.strings.isNotEmpty(razaoSocialAndNome)? razaoSocialAndNome + " - " : "") + Util.strings.emptyIfNull(bean.getNomePessoa());
					bean.setDescricaoPagamento(razaoSocialAndNome);
				}else if(listarRazaosocial){
					boolean isClienteAndPJ = Tipopagamento.CLIENTE.ordinal() == bean.getTipopagamento().intValue() && bean.getTipopessoa() != null && bean.getTipopessoa().intValue() == Tipopessoa.PESSOA_JURIDICA.ordinal();
					if(isClienteAndPJ){
						bean.setDescricaoPagamento(bean.getRazaosocialPessoa());
					}else{
						bean.setDescricaoPagamento(bean.getNomePessoa());
					}
				}else{
					bean.setDescricaoPagamento(bean.getNomePessoa());
				}
			}
		}
		return lista;
	}
	
	private List<ContaReceberPagarBeanReport> montaRelatorioDocumento(DocumentoFiltro filtro, String labeldescricao) {
		List<ContaReceberPagarBeanReport> listaContaDetalhada = null;	

		if(filtro.getTipoConta().equals("PAGAR")) {
			labeldescricao = "Pagamento a";
			filtro.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		} else { //Conta a receber:
			//Remove as situa��es que n�o s�o possiveis. � necess�rio pois os dois relat�rios est�o sendo gerados na mesma tela.
			if(filtro.getListaDocumentoacao() != null){
				List<Documentoacao> documentoRetirados = new ListSet<Documentoacao>(Documentoacao.class);
				for (Documentoacao acao : filtro.getListaDocumentoacao()) {
					if(acao.equals(Documentoacao.AUTORIZADA) || acao.equals(Documentoacao.AUTORIZADA_PARCIAL) || 
							acao.equals(Documentoacao.NAO_AUTORIZADA))
						documentoRetirados.add(acao);
				}

				if(documentoRetirados != null && !documentoRetirados.isEmpty())
					for (Documentoacao documento : documentoRetirados)
						filtro.getListaDocumentoacao().remove(documento);
			}
			labeldescricao = "Recebimento de";
			filtro.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		}
		
		listaContaDetalhada = this.agrupaRateioitemReport(this.findForReport(filtro));
		if(!filtro.getTipoConta().equals("PAGAR")){
			String whereInDocumento = CollectionsUtil.listAndConcatenate(listaContaDetalhada, "cdconta", ",");
			List<Vdocumentonegociado> listaNegociacaoTodos = vdocumentonegociadoService.findByDocumentosnegociadosRaiz(whereInDocumento);
			List<Vdocumentonegociado> listaNegociacao;
			for(ContaReceberPagarBeanReport item: listaContaDetalhada){
				listaNegociacao = getVdocumentonegociado(listaNegociacaoTodos, item.getCdconta());
				String whereInDocumentosForNota = SinedUtil.isListNotEmpty(listaNegociacao) ? CollectionsUtil.listAndConcatenate(listaNegociacao, "cddocumento", ","): item.getCdconta().toString();

				List<NotaDocumento> listaNotadocumento = notaDocumentoService.findByDocumento(whereInDocumentosForNota);

				List<String> notasProduto = new ArrayList<String>();
				List<String> notasServico = new ArrayList<String>(); 
				for(NotaDocumento notadocumento: listaNotadocumento){
					if(NotaTipo.NOTA_FISCAL_PRODUTO.equals(notadocumento.getNota().getNotaTipo())){
						if(notadocumento.getNota().getNumero() != null && !notasProduto.contains(notadocumento.getNota().getNumero()))
							notasProduto.add(notadocumento.getNota().getNumero());
					}else if(NotaTipo.NOTA_FISCAL_SERVICO.equals(notadocumento.getNota().getNotaTipo())){
						Nota nota = notadocumento.getNota();
						if(nota.getCdNota() != null){
							Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nota);
							if(arquivonfnota != null && arquivonfnota.getNumeronfse() != null &&
								!notasServico.contains(arquivonfnota.getNumeronfse())){
								notasServico.add(arquivonfnota.getNumeronfse());
							}else if(nota.getNumero() != null && !notasServico.contains(nota.getNumero())){
								notasServico.add(nota.getNumero());
							}
						}
					}
				}
				
				item.setNumeroNota(notasServico.isEmpty()? "": ("Nfs-e: "+CollectionsUtil.concatenate(notasServico, ", ")));
				if(!notasProduto.isEmpty()){
					item.setNumeroNota((Util.strings.isEmpty(item.getNumeroNota())? "": ", ")+
									"Nf-e: "+CollectionsUtil.concatenate(notasProduto, ", "));
				}
			}
		}
		
		if(filtro.getAscReport() != null){
				String campoDocumento = "";
				if(filtro.getTipoConta().equals("PAGAR")) {
					campoDocumento = OrdenacaoContapagarReport.values()[filtro.getOrdenacaoContapagarReport().ordinal()].getCampo();
				}else {
					campoDocumento = OrdenacaoContareceberReport.values()[filtro.getOrdenacaoContareceberReport().ordinal()].getCampo();
				}
				final String campo = campoDocumento;
				final Boolean ascReport = filtro.getAscReport();
				
				if(campo.equals("numero")){
					for (ContaReceberPagarBeanReport item : listaContaDetalhada) {
						if(item.getNumero() == null){
							item.setNumero("");
						}
					}
				}
				
				Collections.sort(listaContaDetalhada,new Comparator<ContaReceberPagarBeanReport>(){
					@SuppressWarnings("unused")
					public int compare(ContaReceberPagarBeanReport o1, ContaReceberPagarBeanReport o2) {
						
						Method getterMethod = Util.beans.getGetterMethod(ContaReceberPagarBeanReport.class, campo);
						try {
							Object obj1 = getterMethod.invoke(o1);
							Object obj2 = getterMethod.invoke(o2);
							
							if (obj1 instanceof Integer) {
								Integer num1 = (Integer) obj1;
								Integer num2 = (Integer) obj2;
								
								if(num1 == null) return -1;
								if(num2 == null) return -1;
								
								if(ascReport)
									return num1.compareTo(num2);
								else
									return num2.compareTo(num1);
								
							} else if (obj1 instanceof Date) {
								Date num1 = (Date) obj1;
								Date num2 = (Date) obj2;
								
								if(num1 == null) return -1;
								if(num2 == null) return -1;
								
								if(ascReport)
									return num1.compareTo(num2);
								else
									return num2.compareTo(num1);
								
							} else if (obj1 instanceof String) {
								String num1 = (String) obj1;
								String num2 = (String) obj2;
								
								if(num1 == null) return -1;
								if(num2 == null) return -1;
								
								if(ascReport)
									return num1.toUpperCase().compareTo(num2.toUpperCase());
								else
									return num2.toUpperCase().compareTo(num1.toUpperCase());
								
							} else if (obj1 instanceof Money) {
								Money num1 = (Money) obj1;
								Money num2 = (Money) obj2;
								
								if(num1 == null) return -1;
								if(num2 == null) return -1;
								
								if(ascReport)
									return num2.compareTo(num1);
								else
									return num1.compareTo(num2);
								
							}
						} 
						catch (IllegalArgumentException e) {} 
						catch (IllegalAccessException e) {} 
						catch (InvocationTargetException e) {}
						
						return 0;
					}
				});
				
		}
		
		return listaContaDetalhada;
	}
	
	private List<Vdocumentonegociado> getVdocumentonegociado(List<Vdocumentonegociado> listaNegociacaoTodos, Integer cdconta) {
		if(cdconta == null || SinedUtil.isListEmpty(listaNegociacaoTodos)) return null;
		
		List<Vdocumentonegociado> lista = new ArrayList<Vdocumentonegociado>();
		for(Vdocumentonegociado v : listaNegociacaoTodos){
			if(cdconta.equals(v.getCddocumentonegociado())){
				lista.add(v);
			}
		}
		return lista;
	}
	
	/**
	 * M�todo para agrupar o itens de rateio para o relat�rio de contas a pagar/receber.
	 * 
	 * @param listaContaDetalhada
	 * @return
	 * @author Fl�vio Tavares
	 */
	private List<ContaReceberPagarBeanReport> agrupaRateioitemReport(List<ContaReceberPagarBeanReport> listaContaDetalhada){
		List<ContaReceberPagarBeanReport> listaConta = new ArrayList<ContaReceberPagarBeanReport>();
		
		ContaReceberPagarBeanReport aux = null;
		for (ContaReceberPagarBeanReport bean : listaContaDetalhada) {
			if (listaConta.contains(bean)) {
				aux = listaConta.get(listaConta.indexOf(bean));
				
				boolean adicionarRateioItem = true;
				for(Rateioitem rateioitemAux : aux.getListaRateioitem()){
					if(rateioitemAux.getCdrateioitem() != null && rateioitemAux.getCdrateioitem().equals(bean.getCdRateioItem())){
						adicionarRateioItem = false;
						break;
					}
				}
				if(adicionarRateioItem){
					Rateioitem rateioitem = new Rateioitem(bean.getContagerencial(), bean.getIdentificador(), bean.getCentrocusto(), bean.getProjeto(),null, bean.getValorRateio());
					rateioitem.setCdrateioitem(bean.getCdRateioItem());
					rateioitem.setObservacao(bean.getObservacaoItemRateio());
					rateioitem.setPercentual(bean.getPercentualRateio());
					aux.getListaRateioitem().add(rateioitem);
				}
			}else{
				Rateioitem rateioitem = new Rateioitem(bean.getContagerencial(), bean.getIdentificador(), bean.getCentrocusto(), bean.getProjeto(),null, bean.getValorRateio());
				rateioitem.setCdrateioitem(bean.getCdRateioItem());
				rateioitem.setObservacao(bean.getObservacaoItemRateio());
				rateioitem.setPercentual(bean.getPercentualRateio());
				bean.getListaRateioitem().add(rateioitem);
				listaConta.add(bean);
			}
			
		}
		
	//	Movimentacao chequeDocumento;
		for (ContaReceberPagarBeanReport beanPagRec : listaConta) {
		/*	chequeDocumento = movimentacaoService.getChequeDocumento(beanPagRec.getCdconta());
			beanPagRec.setCheque(chequeDocumento != null ? chequeDocumento.getCheque() : null);
			beanPagRec.setDtbaixa(chequeDocumento != null ? chequeDocumento.getDataMovimentacao() : null);*/
//			beanPagRec.setValorAtual(this.somaItens(beanPagRec.getListaRateioitem()));
			beanPagRec.setValorOriginal(this.somaItens(beanPagRec.getListaRateioitem()));
			beanPagRec.setPercentualRateio(this.somaPercents(beanPagRec.getListaRateioitem()));
		}
		
		return listaConta;
	}
	
	private Double somaPercents(List<Rateioitem> listaRateioitem) {
		Double soma = 0d;
		
		if(listaRateioitem != null && listaRateioitem.size() > 0){
			for (Rateioitem rateioitem : listaRateioitem) {
				soma += rateioitem.getPercentual();
			}
		}
		
		return soma;		
	}
	
	private Money somaItens(List<Rateioitem> listaRateioitem) {
		Money soma = new Money();
		
		if(listaRateioitem != null && listaRateioitem.size() > 0){
			for (Rateioitem rateioitem : listaRateioitem) {
				soma = soma.add(rateioitem.getValor());
			}
		}
		
		return soma;
	}
	/**
	 * Seta dados para exibi��o da tela de baixar conta a pagar e receber.
	 * 
	 * @param request
	 * @param baixarContaBean
	 * @author Rodrigo Freitas
	 * @param qtdePessoas 
	 */
	@SuppressWarnings("unchecked")
	public void setarDados(WebRequestContext request, BaixarContaBean baixarContaBean, Boolean mesmoCliente, Integer qtdePessoas) {
		List<Contagerencial> listaContagerencial = null;
		String descricao = null;
		Tipooperacao tipooperacao = null;
		List<Formapagamento> listaFormapagamento = null;
		if (baixarContaBean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
			tipooperacao = Tipooperacao.TIPO_DEBITO;
			listaContagerencial = contagerencialService.findAnaliticas(tipooperacao);
			descricao = "BAIXAR CONTA(S) A PAGAR";
			listaFormapagamento = formapagamentoService.findDebito();
			
			if (Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
				listaFormapagamento.remove(Formapagamento.CHEQUE);
				listaFormapagamento.remove(Formapagamento.VALECOMPRA);
				listaFormapagamento.remove(Formapagamento.ANTECIPACAO);
			}
		} else {
			tipooperacao = Tipooperacao.TIPO_CREDITO;
			listaContagerencial = contagerencialService.findAnaliticas(tipooperacao);
			descricao = "BAIXAR CONTA(S) A RECEBER";
			listaFormapagamento = formapagamentoService.findCredito();
			List<Contatipo> listaTipos = new ArrayList<Contatipo>();
			listaTipos.add(new Contatipo(Contatipo.CAIXA,"Caixa"));
			listaTipos.add(new Contatipo(Contatipo.CONTA_BANCARIA,"Conta banc�ria"));
			request.setAttribute("listaTipos", listaTipos);
			
			if(mesmoCliente != null && mesmoCliente){
				Formapagamento valecompra = Formapagamento.VALECOMPRA;
				listaFormapagamento.add(valecompra);
			}
			
			if (Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
				listaFormapagamento.remove(Formapagamento.CHEQUE);
			}
		}
		if(qtdePessoas != null && qtdePessoas > 1){
			listaFormapagamento.remove(Formapagamento.ANTECIPACAO);
		}
		
		for (Documento documento : baixarContaBean.getListaDocumento()) {	
				if (documento.getDocumentotipo() != null && documento.getDocumentotipo().getAntecipacao() != null && documento.getDocumentotipo().getAntecipacao()) {
					listaFormapagamento.remove(Formapagamento.ANTECIPACAO);
				}
		}
		
		List<Centrocusto> listaCentroCusto = (List<Centrocusto>) CollectionsUtil.getListProperty(baixarContaBean.getRateio().getListaRateioitem(), "centrocusto");

		request.setAttribute("tipooperacao", tipooperacao);
		request.setAttribute("descricaotela", descricao);
		request.setAttribute("listaContagerencial", listaContagerencial);
		request.setAttribute("listaFormapagamento", listaFormapagamento);
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
		request.setAttribute("listaVinculo", new ArrayList<Contatipo>());
		
		if (baixarContaBean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR) && SinedUtil.isListNotEmpty(listaFormapagamento)) {
			request.setAttribute("comboFormapagamento", criaHtmlComboFormapgamento(listaFormapagamento, false));
			request.setAttribute("comboFormapagamentoSemCheque", criaHtmlComboFormapgamento(listaFormapagamento, true));
		}

		if (request.getBindException().hasErrors()) {
			Conta vinculo = null;
			if(baixarContaBean.getVinculo() != null)
				vinculo = baixarContaBean.getVinculo();
			else if(baixarContaBean.getListaBaixarContaBeanMovimentacao() != null && baixarContaBean.getListaBaixarContaBeanMovimentacao().size() == 1){
				vinculo = baixarContaBean.getListaBaixarContaBeanMovimentacao().get(0).getVinculo();
			}
			if(vinculo != null)
				request.setAttribute("vinculoid", vinculo.getCdconta());
		}
	}

	private String criaHtmlComboFormapgamento(List<Formapagamento> listaFormapagamento, boolean removerCheque) {
		StringBuilder html = new StringBuilder();
		
		html.append("<option value='<null>'></option>");
		for(Formapagamento formapagamento : listaFormapagamento){
			if(removerCheque && formapagamento.equals(Formapagamento.CHEQUE)) continue;
			
			html.append("<option value='br.com.linkcom.sined.geral.bean.Formapagamento[cdformapagamento=")
				.append(formapagamento.getCdformapagamento())
				.append("]'>")
				.append(formapagamento.getNome())
				.append("</option>");
		}
		
		return html.toString();
	}
	
	/**
	 * Define a partir do escopo de requisi��o qual a classe do documento.
	 * 
	 * @param request
	 * @param baixarContaBean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documentoclasse defineDocumentoClasse(WebRequestContext request, BaixarContaBean baixarContaBean) {
		if(baixarContaBean.getDocumentoclasse() == null){
			if (request.getParameter("tipo") == null) {
				throw new SinedException("Indicar qual tipo de documento a ser baixado.");
			}
			if ("pagar".equals(request.getParameter("tipo"))) {
				return Documentoclasse.OBJ_PAGAR;
			} else if ("receber".equals(request.getParameter("tipo"))) {
				return Documentoclasse.OBJ_RECEBER;
			}
		}

		return baixarContaBean.getDocumentoclasse();
	}
	
	/**
	 * M�todo para realizar valida��es de inser��o de documentos.
	 * 
	 * @param bean
	 * @param errors
	 * @author Fl�vio Tavares
	 */
	public void validateDocumento(Documento bean, BindException errors) {
		
		if(bean.getTipopagamento().equals(Tipopagamento.CLIENTE)){
			if(bean.getCliente() == null){
				errors.reject("001","O campo Cliente � obrigat�rio.");
			}
		}
		if(bean.getTipopagamento().equals(Tipopagamento.FORNECEDOR)){
			if(bean.getFornecedor() == null){
				errors.reject("001","O campo Fornecedor � obrigat�rio.");
			}
		}
		if(bean.getTipopagamento().equals(Tipopagamento.COLABORADOR)){
			if(bean.getColaborador() == null){
				errors.reject("001","O campo Colaborador � obrigat�rio.");
			}
		}
		if(bean.getTipopagamento().equals(Tipopagamento.OUTROS)){
			if(StringUtils.isBlank(bean.getOutrospagamento())){
				errors.reject("001","O campo Outros � obrigat�rio.");
			}
		}
		if(bean.getValor() != null && bean.getValor().compareTo(new Money()) == 0){
			errors.reject("001","O campo Valor n�o pode ser zerado.");
		}
		if(StringUtils.isEmpty(bean.getNumero()) &&
			Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_DOCUMENTO_CR_CP))){
			errors.reject("001","O campo Documento � obrigat�rio.");
		}
		Documentotipo documentoTipo = documentotipoService.loadWithPrazoApropriacao(bean.getDocumentotipo());
		if(documentoTipo != null && documentoTipo.getPrazoApropriacao() != null && !Prazopagamento.UNICA.equals(documentoTipo.getPrazoApropriacao())){
			if(SinedUtil.isListEmpty(bean.getListaApropriacao())){
				errors.reject("001","A aba Apropria��o de ser preenchida.");
			}else{
				Money totalApropriacao = new Money();
				for(DocumentoApropriacao apropriacao: bean.getListaApropriacao()){
					if(apropriacao.getValor() != null){
						totalApropriacao = totalApropriacao.add(apropriacao.getValor());
					}
				}
				if(totalApropriacao.compareTo(bean.getValor()) != 0){
					errors.reject("001", "O total da Aba Apropria��o deve ser igual ao valor do documento.");
				}
			}
		}
	}
	
	/**
	 * M�todo que pega os c�digos das contas geradas.
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String getCodigosContasPagar(Documento form) {
		StringBuilder codigos = new StringBuilder();
		codigos.append(form.getCddocumento()+", ");
		if(form.getListaDocumento() != null  && form.getListaDocumento().size() > 0){
			for (Documento documento : form.getListaDocumento()) {
				if(codigos.indexOf(documento.getCddocumento().toString()) == -1){
					codigos.append(documento.getCddocumento()+", ");
				}
			}
		}
		return codigos.delete(codigos.length()-2, codigos.length()).toString();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#deleteDocumentoFromDespesa
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void deleteDocumentoFromDespesa(String whereIn) {
		documentoDAO.deleteDocumentoFromDespesa(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#callProcedureAtualizaDocumento
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaDocumento(Documento documento){
		documentoDAO.callProcedureAtualizaDocumento(documento);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaDocumento(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Documento documento;
			for (int i = 0; i < ids.length; i++) {
				documento = new Documento(Integer.parseInt(ids[i].trim()));
				this.callProcedureAtualizaDocumento(documento);
			}
		}
	}
	
	/**
	 * Atualiza a tabela auxiliar de documento.
	 *
	 * @author Rodrigo Freitas
	 */
	public void atualizaAux(){
		documentoDAO.atualizaAux();
	}

	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#loadForReceita(Documento)
	 * @author Fl�vio Tavares
	 */
	public Documento loadForReceita(Documento documento){
		return documentoDAO.loadForReceita(documento);
	}
	
	private static DocumentoService instance;
	public static DocumentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForBoleto
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForBoleto(String whereIn) {
		return documentoDAO.findForBoleto(whereIn);
	}
	
	public Documento loadForBoleto(Documento documento) {
		List<Documento> lista = documento != null && documento.getCddocumento() != null ? findForBoleto(documento.getCddocumento().toString()) : null;
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#isDocumentoNotDefitivaNotPrevista
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isDocumentoNotDefitivaNotPrevista(String whereIn) {
		return documentoDAO.isDocumentoNotDefitivaNotPrevista(whereIn);
	}
	
	public boolean isDocumentoNotBaixadaOrDefinitiva(String whereIn) {
		return documentoDAO.isDocumentoNotBaixadaOrDefinitiva(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#isDocumentoContaNotGeraBoleto
	 * 
	 * @param whereIn
	 * @return
	 * @author Marden Silva
	 */
	public boolean isDocumentoContaNotGeraBoleto(String whereIn) {
		return documentoDAO.isDocumentoContaNotGeraBoleto(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findDocumentoContaNotGeraBoleto
	 * 
	 * @param whereIn
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Documento> findDocumentoContaNotGeraBoleto(String whereIn) {
		return documentoDAO.findDocumentoContaNotGeraBoleto(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#existDocumentoContaNotNossonumero(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existDocumentoContaNotNossonumero(String whereIn) {
		return documentoDAO.existDocumentoContaNotNossonumero(whereIn);
	}
	
	

	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de remessa 
	 * 
	 * @param banco
	 * @param mes
	 * @param anoRemessa
	 * @return List<Documento>
	 * @author Jo�o Paulo Zica
	 */
	public List<Documento> findDadosArquivoRemessa(GerarArquivoRemessaBean filtro){
		return documentoDAO.findDadosArquivoRemessa(filtro);
	}
	
	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de remessa
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Jo�o Paulo Zica
	 */
	public List<Documento> findDadosForListagemRemessa(GerarArquivoRemessaBean filtro){
		return documentoDAO.findDadosForListagemRemessa(filtro);
	}
	
	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de remessa
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Marden Silva
	 */
	public List<Documento> findDadosForListagemRemessaConfiguravel(ArquivoConfiguravelRemessaBean filtro){
		List<Documento> lista = documentoDAO.findDadosForListagemRemessaConfiguravel(filtro);
		if(SinedUtil.isListEmpty(lista)){
			return lista;
		}
		
		return documentoDAO.findDadosForListagemRemessaConfiguravel(CollectionsUtil.listAndConcatenate(lista, "cddocumento", ",")); 
	}
	
	
	public List<Documento> findDadosForListagemRemessaBoletoDigitalConfiguravel(BoletoDigitalRemessaBean filtro){
		return documentoDAO.findDadosForListagemRemessaBoletoDigitalConfiguravel(filtro); 
	}
	
	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de remessa
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Rafael Salvio
	 */
	public List<Documento> findDadosForListagemRemessaPagamentoConfiguravel(ArquivoConfiguravelRemessaPagamentoBean filtro){
		return documentoDAO.findDadosForListagemRemessaPagamentoConfiguravel(filtro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 *		  
	 * @param resource
	 * @param mes
	 * @param ano
	 * @param listaDocumento
	 * 
	 * @author Bruno Eust�quio
	 */
	public void	saveAndUpdateArquivoRemessa(final Resource resource,final List<Documento> listaDocumento, String descricao, String sequencial) {
		documentoDAO.saveAndUpdateArquivoRemessa(resource, listaDocumento, descricao, sequencial, null);
	}
	
	public void	saveAndUpdateArquivoRemessa(final Resource resource,final List<Documento> listaDocumento, String descricao, String sequencial, List<Movimentacao> listaMovimentacao) {
		documentoDAO.saveAndUpdateArquivoRemessa(resource, listaDocumento, descricao, sequencial, listaMovimentacao);
	}
	/**
	 *  M�todo que faz refer�ncia ao DAO
	 * @param empresa
	 * @param categoria
	 * @param documentoclasse
	 * @return
	 * @author Ramon Brazil
	 * @param contatotipo 
	 * @param filtro 
	 */
	public List<Documento> findByContasAtrasadas (Empresa empresa, Categoria categoria, Contatotipo contatotipo, Date dtvencimento1, Date dtvencimento2, Documentoclasse documentoclasse, MailingDevedorBean filtro){
		return documentoDAO.findByContasAtrasadas(empresa, categoria, contatotipo, dtvencimento1, dtvencimento2, documentoclasse, filtro);
	}

	/**
	 *  M�todo que faz refer�ncia ao DAO
	 * @param empresa
	 * @param categoria
	 * @param documentoclasse
	 * @param wherein
	 * @return
	 * @author Ramon Brazil
	 * @param contatotipo 
	 */
	public List<Documento> findByIdForMaillingDevedor (Empresa empresa, Categoria categoria, Contatotipo contatotipo, Date dtvencimento1, Date dtvencimento2, Documentoclasse documentoclasse, String wherein, MailingDevedorBean filtro){
		return documentoDAO.findByIdForMaillingDevedor(empresa, categoria, contatotipo, dtvencimento1, dtvencimento2, documentoclasse, wherein, filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param classe
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro(Documentoclasse classe) {
		return documentoDAO.getDtUltimoCadastro(classe);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param classe
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeAtrasadas(Documentoclasse classe) {
		return documentoDAO.getQtdeAtrasadas(classe);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param classe
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAbertas(Documentoclasse classe) {
		return documentoDAO.getQtdeAbertas(classe);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param classe
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalContas(Documentoclasse classe) {
		return documentoDAO.getQtdeTotalContas(classe);
	}
	
	@SuppressWarnings("unchecked")
	public Resource createRecibo(WebRequestContext request) throws Exception {
		MergeReport mergeReport = new MergeReport("recibo_"+SinedUtil.datePatternForReport()+".pdf");
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean isContaReceber = true;
		String from = request.getParameter("conta");
		if(from != null && from.toUpperCase().equals("PAGAR"))
			isContaReceber = false;
		
		if(parametrogeralService.getBoolean(Parametrogeral.RECIBO_APENASBAIXADAS)){
			if(documentoService.validaDocumentoSituacao(whereIn, true, Documentoacao.BAIXADA, Documentoacao.BAIXADA_PARCIAL)){
				throw new SinedException("S� � permitido emitir recibo para contas com as situa��es Baixada ou Baixada Parcialmente.");
			}
		}
		
		if(isContaReceber && documentoService.validaDocumentoSituacao(whereIn, true, 
													Documentoacao.BAIXADA,
													Documentoacao.BAIXADA_PARCIAL,
													Documentoacao.PREVISTA,
													Documentoacao.DEFINITIVA,
													Documentoacao.AUTORIZADA,
													Documentoacao.AUTORIZADA_PARCIAL)){
			throw new SinedException("S� pode emitir recibo de uma conta a receber que estiver na situa��o diferente de Cancelada ou Negociada.");
		}
		
		if((!isContaReceber) &&
			documentoService.isDocumentoNotBaixadaOrDefinitiva(whereIn)){
			throw new SinedException("S� pode emitir recibo de uma conta a pagar que estiver diferente da situa��o Cancelada e N�o Autorizada.");
		}
		
		Boolean projeto = false;
		projeto = Boolean.parseBoolean(request.getParameter("projeto"));  
		
		
		List<Documento> listaDocumento = this.findForRecibo(whereIn);
		List<Documento> novaListaDocumento = new ListSet<Documento>(Documento.class);
		novaListaDocumento.addAll(listaDocumento);

		if(projeto && !isContaReceber){
			for (Documento item : listaDocumento) {
				item.setExisteProjetoRateio(false);
				int p = 0;
				for (Rateioitem r : item.getRateio().getListaRateioitem()) {
					if(r.getProjeto() != null && r.getProjeto().getNome() != null){
						p++;
					}
				}
				if(p > 0) item.setExisteProjetoRateio(true);
				
				if(item.getRateio().getListaRateioitem().size() > 1 && item.getExisteProjetoRateio()){
					Double valor = 0.0;
					for(int i=0; i < item.getRateio().getListaRateioitem().size(); i++){
						if(i > 0 && item.getRateio().getListaRateioitem().get(i).getProjeto().getCnpj() != null){
							Documento doc = new Documento();
							if(item.getEmpresa() != null && item.getEmpresa().getCdpessoa() != null){
								doc.setEmpresa(item.getEmpresa());
							}
							Rateioitem rateioitem = new Rateioitem();
							rateioitem = item.getRateio().getListaRateioitem().get(i);
							
							List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
							listaRateioitem.add(rateioitem);
							Rateio rateio = new Rateio();
							rateio.setListaRateioitem(listaRateioitem);
							
							doc.setListaMovimentacaoOrigem(item.getListaMovimentacaoOrigem());
							doc.setDescricao(item.getDescricao());
							doc.setPessoa(item.getPessoa());
							doc.setTipopagamento(item.getTipopagamento());
							doc.setAux_documento(item.getAux_documento());
							doc.setRateio(rateio);
							doc.setCddocumento(item.getCddocumento());
							doc.setExisteProjetoRateio(item.getExisteProjetoRateio());
							doc.setEndereco(item.getEndereco());
							novaListaDocumento.add(doc);
						}else if(i > 0){
							valor = valor + item.getRateio().getListaRateioitem().get(i).getValor().getValue().doubleValue();
						}
					}
					Documento doc = new Documento();
					if(valor > 0.0){
						doc.setListaMovimentacaoOrigem(item.getListaMovimentacaoOrigem());
						doc.setDescricao(item.getDescricao());
						doc.setPessoa(item.getPessoa());
						doc.setTipopagamento(item.getTipopagamento());
						doc.setAux_documento(item.getAux_documento());
						doc.getAux_documento().setValoratual(new Money(valor));
						doc.setCddocumento(item.getCddocumento());
						doc.setExisteProjetoRateio(false);
						doc.setEndereco(item.getEndereco());
						novaListaDocumento.add(doc);
					}
				}
			}	
		}
		
		Collections.sort(novaListaDocumento, new BeanComparator("cddocumento"));
		String whereInMovimentacao = request.getParameter("whereInMovimentacao");
		for (Documento documento : novaListaDocumento) {
			documento.setReciboProjeto(projeto);
			mergeReport.addReport(geraRecibo(documento, isContaReceber, whereInMovimentacao));
		}
		
		return mergeReport.generateResource();
	}
	
	public List<Documento> findForRecibo(String whereIn) {
		return documentoDAO.findForRecibo(whereIn);
	}
	
	private Report geraRecibo(Documento documento, boolean isContaReceber, String whereInMovimentacao) {
		Report report = new Report("/financeiro/recibo");
		Report primeiraVia = new Report("/financeiro/recibo_sub");
		Report segundaVia = new Report("/financeiro/recibo_sub");
		
		Empresa empresa = documento.getEmpresa();
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadForRecibo(empresa);
		} else {
			empresa = empresaService.loadForRecibo(empresaService.loadPrincipal());
		}

		Pessoa pessoa = null;
		Pessoa pessoaEndereco = null;
		String pessoaAux = null;
		if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
			if(documento.getTipopagamento().equals(Tipopagamento.COLABORADOR)){
				pessoa = colaboradorService.loadForBoleto(documento.getPessoa().getCdpessoa());
			}else{
				pessoa = pessoaService.carregaPessoa(documento.getPessoa());
				pessoaEndereco = pessoaService.carregaPessoa(documento.getPessoa());
			}
			
			Cliente responsavel = new Cliente();
			if(documento.getPessoa().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				responsavel = clienteService.carregaDadosResponsavelFinanceiro(documento.getPessoa());
			}
			if(responsavel != null && responsavel.getResponsavelFinanceiro() != null){
				pessoaAux = pessoa.getNome();
				pessoa = responsavel.getResponsavelFinanceiro();
				pessoa.setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.findByCliente((Cliente)pessoa)));
			}
			
			if(documento.getTipopagamento().equals(Tipopagamento.CLIENTE)){
				Cliente cliAux = clienteService.load(new Cliente(pessoa.getCdpessoa()), "cliente.razaosocial");
				if(cliAux != null)
					pessoa.setRazaosocialTrans(cliAux.getRazaosocial());
			}else if(documento.getTipopagamento().equals(Tipopagamento.FORNECEDOR)){
				Fornecedor forAux = fornecedorService.load(new Fornecedor(pessoa.getCdpessoa()), "fornecedor.razaosocial");
				if(forAux != null)
					pessoa.setRazaosocialTrans(forAux.getRazaosocial());
			}
			
//			if(isContaReceber && pessoa.getCpf() == null && pessoa.getCnpj() == null)
//				throw new SinedException("A pessoa cadastrada para emiss�o de recibo n�o possui CPF ou CNPJ cadastrado no sistema.");
		}
		
		String msg_corpo = this.makeMsgCorpo(documento, empresa, pessoa, pessoaAux, isContaReceber, whereInMovimentacao);
		
		if(empresa.getEndereco() == null || empresa.getEndereco().getMunicipio() == null){
			throw new SinedException("Favor cadastrar endere�o completo para a empresa " + empresa.getNome() + ".");
		}
		
		String cidade_empresa = empresa.getEndereco().getMunicipio().getNome() + " - " + empresa.getEndereco().getMunicipio().getUf().getSigla();
		
		StringBuilder msg_rodape = new StringBuilder();
		msg_rodape.append(cidade_empresa);
		
		Movimentacao mov = movimentacaoService.findByDocumento(documento, whereInMovimentacao);
		if(mov != null && mov.getDataMovimentacao() != null){
			String[] meses = new String[]{"Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"};
			
			int dia = SinedDateUtils.getDateProperty(mov.getDataMovimentacao(), Calendar.DAY_OF_MONTH);
			String mes = meses[SinedDateUtils.getDateProperty(mov.getDataMovimentacao(), Calendar.MONTH)];
			int ano = SinedDateUtils.getDateProperty(mov.getDataMovimentacao(), Calendar.YEAR);
			
			msg_rodape.append(", "); 
			msg_rodape.append(dia); 
			msg_rodape.append(" de "); 
			msg_rodape.append(mes); 
			msg_rodape.append(" de "); 
			msg_rodape.append(ano);
			msg_rodape.append(".");
		}else {
			if(!Documentoacao.BAIXADA.equals(documento.getDocumentoacao())){
				String[] meses = new String[]{"Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"};
				Date dataatual = new Date(System.currentTimeMillis());
				int dia = SinedDateUtils.getDateProperty(dataatual, Calendar.DAY_OF_MONTH);
				String mes = meses[SinedDateUtils.getDateProperty(dataatual, Calendar.MONTH)];
				int ano = SinedDateUtils.getDateProperty(dataatual, Calendar.YEAR);
				
				msg_rodape.append(", "); 
				msg_rodape.append(dia); 
				msg_rodape.append(" de "); 
				msg_rodape.append(mes); 
				msg_rodape.append(" de "); 
				msg_rodape.append(ano);
				msg_rodape.append(".");
			}
		}
		
		String nomeAssinatura = this.getNomeAssinaturaRecibo(documento, empresa, pessoa, isContaReceber);
		
		report.addParameter("DADOS_RECIBO", documento.getNumero());
		
		if(pessoa != null){
			if(isContaReceber){
				report.addParameter("ENDERECO_EMPRESA", empresa.getEndereco()!= null? empresa.getEndereco().getDescricaoCombo() : "");
			}else{
				//report.addParameter("ENDERECO_EMPRESA", pessoa.getEndereco()!= null?  empresa.getEndereco().getDescricaoCombo() : "");
				report.addParameter("ENDERECO_EMPRESA", pessoaEndereco!=null && pessoaEndereco.getEndereco()!= null?  pessoaEndereco.getEndereco().getDescricaoCombo() : "");
			}
		} else {
			report.addParameter("ENDERECO_EMPRESA", "");
		}
		report.addParameter("MSG_CORPO", msg_corpo);
		report.addParameter("MSG_RODAPE", msg_rodape.toString());
		report.addParameter("NOME_EMPRESA", nomeAssinatura);
		report.addParameter("OBSERVACAO_RECIBO", this.getObservacaoRecibo(empresa, isContaReceber));
		report.addParameter("ISCONTARECEBER", isContaReceber);
		
		if(!isContaReceber){
			if (documento.getTipopagamento().equals(Tipopagamento.OUTROS)){
				report.addParameter("CNPJ_EMPRESA", "");
			}else if (documento.getPessoa().getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)) {
				report.addParameter("CNPJ_EMPRESA", pessoa.getCpf() != null ? pessoa.getCpf().toString() : "");
			}else if (documento.getPessoa().getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)) {
				report.addParameter("CNPJ_EMPRESA", pessoa.getCnpj() != null ? pessoa.getCnpj().toString() : "");
			}
		} else {
			report.addParameter("CNPJ_EMPRESA", empresa.getCnpj() != null ? empresa.getCnpj().toString() : "");
		}
		
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
			report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
		}
		
		report.addSubReport("SUB_RECIBO_1", primeiraVia);
		report.addSubReport("SUB_RECIBO_2", segundaVia);
		return report;
	}
	
	private String getNomeAssinaturaRecibo(Documento documento,	Empresa empresa, Pessoa pessoa, boolean isContaReceber) {
		if(!isContaReceber){
			if(documento.getTipopagamento().equals(Tipopagamento.OUTROS))
				return documento.getOutrospagamento();
			else 
				return parametrogeralService.getBoolean(Parametrogeral.RECIBO_RAZAOSOCIAL)? pessoa.getRazaosocialTrans(): pessoa.getNome();
			
		}else{
			return empresa.getRazaosocialOuNome();
		}
	}
	
	private String getObservacaoRecibo(Empresa empresa, boolean isContaReceber){
		if(!isContaReceber){
			return empresa.getObservacaorecibopagar();
		}
		return empresa.getObservacaoreciboreceber();
	}
	
	@SuppressWarnings("unchecked")
	private String makeMsgCorpo(Documento documento, Empresa empresa, Pessoa pessoa, String pessoaAux, boolean isContaReceber, String whereInMovimentacao) {
		StringBuilder msg_corpo = new StringBuilder();
		
		msg_corpo.append("Declaro o recebimento de R$ ");
		
		if(documento.getReciboProjeto() == true && documento.getRateio() != null && !isContaReceber && documento.getExisteProjetoRateio()){
			for (Rateioitem ri : documento.getRateio().getListaRateioitem()) {
				msg_corpo.append(ri.getValor().toString());
				msg_corpo.append(" (");
				msg_corpo.append(new Extenso(ri.getValor()).toString());
				msg_corpo.append("), ");
				
				break;
			}
		}else{
			Money valorTotal = documento.getAux_documento().getValoratual();
			documento.setListaMovimentacaoOrigem(SinedUtil.listToSet(getListaMovimentacaoorigem(documento.getListaMovimentacaoOrigem(), whereInMovimentacao), Movimentacaoorigem.class));
			Money valorTotalMovimentacoes = documento.getValorTotalMovimentacoes();
			if(isContaReceber && 
					documento.getDocumentoacao() != null && 
					(documento.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL) || (documento.getDocumentoacao().equals(Documentoacao.BAIXADA) && StringUtils.isNotBlank(whereInMovimentacao))) &&  
					valorTotalMovimentacoes != null && 
					valorTotalMovimentacoes.getValue().doubleValue() > 0){
				valorTotal = valorTotalMovimentacoes;
			}
			msg_corpo.append(valorTotal.toString());
			msg_corpo.append(" (");
			msg_corpo.append(new Extenso(valorTotal).toString());
			msg_corpo.append("), ");
		}
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(documento.getListaMovimentacaoOrigem() != null && 
				documento.getListaMovimentacaoOrigem().size()>0){
			for (Movimentacaoorigem item : getListaMovimentacaoorigem(documento.getListaMovimentacaoOrigem(), whereInMovimentacao)) {
				if(item.getMovimentacao() != null && 
						item.getMovimentacao().getMovimentacaoacao() != null &&
						!item.getMovimentacao().getMovimentacaoacao().equals(Movimentacaoacao.CANCELADA)){
					if(item.getMovimentacao().getFormapagamento().equals(Formapagamento.CHEQUE) ){
						msg_corpo.append(" Cheque: "); 
						msg_corpo.append((item.getConta() != null && item.getConta().getBanco() != null && item.getConta().getBanco().getNumeroString() != null ? (item.getConta().getBanco().getNumeroString() + "/") : ""));
						msg_corpo.append(item.getMovimentacao() != null && item.getMovimentacao().getCheque() != null ? item.getMovimentacao().getCheque().getNumero() : "");
						msg_corpo.append(", ");
					}
					if(item.getMovimentacao().getDtmovimentacao() != null){
						msg_corpo.append("em ");
						msg_corpo.append(format.format(item.getMovimentacao().getDtmovimentacao()));
//						if(item.getMovimentacao().getValor() != null){
//							msg_corpo.append(" (R$ " + item.getMovimentacao().getValor() + ")");
//						}
						msg_corpo.append(", ");
					}
				}
			}
		}
		
		msg_corpo.append("de ");

		//Se for conta a receber coloca o nome da pessoa se n�o � o inverso
		if(isContaReceber){
			if(documento.getTipopagamento().equals(Tipopagamento.OUTROS)){
				msg_corpo.append(documento.getOutrospagamento());
			} else {
				Endereco endereco = documento.getEndereco() != null ? documento.getEndereco() : pessoa.getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.COBRANCA, Enderecotipo.FATURAMENTO, Enderecotipo.CORRESPONDENCIA);
				
				msg_corpo.append(parametrogeralService.getBoolean(Parametrogeral.RECIBO_RAZAOSOCIAL)? pessoa.getRazaosocialTrans():
																									pessoa.getNome());
				if(pessoa.getCnpj() != null){
					msg_corpo.append(", CNPJ: ");
					msg_corpo.append(pessoa.getCnpj());
				}else if(pessoa.getCpf() != null){
					msg_corpo.append(", CPF: ");
					msg_corpo.append(pessoa.getCpf());
				}

				if(endereco != null){
					concatenaEnderecoRecibo(endereco, pessoa.getTipopessoa(), msg_corpo);
				}
			}
		}else{
			if(documento.getReciboProjeto() == true && documento.getRateio() != null){
				for (Rateioitem item : documento.getRateio().getListaRateioitem()) {
					if(item.getProjeto() != null && item.getProjeto().getCnpj() != null){
						msg_corpo.append(item.getProjeto().getNome()+", CNPJ: "+item.getProjeto().getCnpj());
					}else{
						if(empresa.getCnpj() != null){
							msg_corpo.append(empresa.getRazaosocialOuNome()+", CNPJ: "+empresa.getCnpj());
						}else{
							msg_corpo.append(empresa.getRazaosocialOuNome());
						}
					}
					break;
				}
			}else{
				if(empresa.getCnpj() != null){
					msg_corpo.append(empresa.getRazaosocialOuNome()+", CNPJ: "+empresa.getCnpj());
				}else{
					msg_corpo.append(empresa.getRazaosocialOuNome());
				}
			}
			concatenaEnderecoRecibo(empresa.getEndereco(), Tipopessoa.PESSOA_JURIDICA, msg_corpo);
		}
		msg_corpo.append(", referente a ");
		msg_corpo.append(documento.getDescricao());
		if(parametrogeralService.getBoolean(Parametrogeral.RECIBO_PRODUTOS)){
			msg_corpo.append(dadosVenda(this.docOrigemVenda(documento), isContaReceber));
		}
		if(isContaReceber && pessoaAux != null && !pessoaAux.equals(""))
			msg_corpo.append(" (").append(pessoaAux).append(")");
		msg_corpo.append(".");
		
		return msg_corpo.toString();
	}
	
	private List<Movimentacaoorigem> getListaMovimentacaoorigem(Set<Movimentacaoorigem> listaMovimentacaoOrigem, String whereInMovimentacao) {
		List<Movimentacaoorigem> lista = new ArrayList<Movimentacaoorigem>();
		if(SinedUtil.isListNotEmpty(listaMovimentacaoOrigem)){
			for(Movimentacaoorigem movimentacaoorigem : listaMovimentacaoOrigem){
				if(existeMovimentacao(movimentacaoorigem.getMovimentacao(), whereInMovimentacao)){
					lista.add(movimentacaoorigem);
				}
			}
		}
		
		return lista;
	}
	
	private boolean existeMovimentacao(Movimentacao movimentacao, String whereInMovimentacao) {
		if(StringUtils.isNotBlank(whereInMovimentacao) && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			for(String id : whereInMovimentacao.split(",")){
				if(id.equals(movimentacao.getCdmovimentacao().toString())){
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * Caso a conta a receber for originada de uma venda, ser� impresso no recibo
	 * os produtos desta venda.
	 * @param listaDocsVenda
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public String dadosVenda(List<Documento> listaDocsVenda, boolean isContaReceber){
		StringBuilder produtos = new StringBuilder();
		if(listaDocsVenda != null && listaDocsVenda.size() > 0 && isContaReceber){
			for (Documento doc : listaDocsVenda) {
				for (Vendamaterial vm : doc.getListaDocumentoOrigem().get(0).getVenda().getListavendamaterial()) {
					if(!produtos.toString().contains(vm.getMaterial().getNome())){
						produtos.append(", ").append(vm.getMaterial().getNome());
					}
				}
			}
		}
		return produtos.toString();
	}
	
	
	/**
	 * M�todo que concatena o endere�o da pessoa ou empresa para emitir recibo
	 * 
	 * @param endereco
	 * @param tipopessoa
	 * @param msg_corpo
	 */
	private void concatenaEnderecoRecibo(Endereco endereco,	Tipopessoa tipopessoa, StringBuilder msg_corpo) {
		if(endereco != null){
			if(endereco.getLogradouro() != null && !endereco.getLogradouro().equals("")){
				if(tipopessoa != null){
					if(tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)){
						msg_corpo.append(", com sede na");
					} else {
						msg_corpo.append(", residente na");
					}
				}
				msg_corpo.append(" ").append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					msg_corpo.append(", ");
					msg_corpo.append(endereco.getNumero());
					if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
						msg_corpo.append("/");
						msg_corpo.append(endereco.getComplemento());
					}
				} else {
					if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
						msg_corpo.append(", ");
						msg_corpo.append(endereco.getComplemento());
					}
				}
			}
			if(endereco.getCep() != null && endereco.getCep().getValue() != null){
				msg_corpo.append(", CEP: ");
				msg_corpo.append(endereco.getCep().getValue());
			}
			
			if(endereco.getBairro() != null && !endereco.getBairro().equals("")){
				msg_corpo.append(", no bairro ");
				msg_corpo.append(endereco.getBairro());
			}
			
			if(endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null){
				msg_corpo.append(", na cidade de ");
				msg_corpo.append(endereco.getMunicipio().getNome());
				msg_corpo.append(" - ");
				msg_corpo.append(endereco.getMunicipio().getUf().getSigla());
			}
		}
	}
	
	public Money getValorAtual(Integer cddocumento, Date dtcredito) {
		return getValorAtual(cddocumento, dtcredito, false);
	}
	
	public Money getValorAtual(Integer cddocumento, Date dtcredito, boolean verificarTaxaMovSeparada) {
		Tipotaxa tipotaxaignorar = null;
		if(cddocumento != null && verificarTaxaMovSeparada){
			Documento documento = loadForMovimentacaoTaxa(new Documento(cddocumento));
			tipotaxaignorar = documento != null ? Tipotaxa.TAXAMOVIMENTO : null;
		}
		return documentoDAO.getValorAtual(cddocumento, dtcredito, tipotaxaignorar);
	}
	
	/**
	 * M�todo que monta os documentos a partir da tela de Agendamento servi�o FLEX
	 * 
	 * @param listaAgenda
	 * @param agendamentoservicoapoioFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documento> montaListaDocumentoAgendamentoServico(List<Agendamentoservicoprazopagamentoapoio> listaAgenda, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro) {
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		if(listaAgenda != null && !listaAgenda.isEmpty()){
			Material material = materialService.loadForEntrada(agendamentoservicoapoioFiltro.getMaterial());
			
			for (Agendamentoservicoprazopagamentoapoio agendamentoservicoprazopagamentoapoio : listaAgenda) {
				int parcela = 0;
				Documento documentoPrincipal = new Documento();
				for (Agendamentoservicoprazopagamentoitemapoio agendamentoservicoprazopagamentoitemapoio : agendamentoservicoprazopagamentoapoio.getListaparcelas()) {
					if(parcela == 0){
						documentoPrincipal = setaValoresDocumento(agendamentoservicoprazopagamentoapoio, agendamentoservicoprazopagamentoitemapoio, agendamentoservicoapoioFiltro, material);
						documentoPrincipal.setListaDocumento(new ArrayList<Documento>());
					}else{
						documentoPrincipal.getListaDocumento().add(setaValoresDocumento(agendamentoservicoprazopagamentoapoio, agendamentoservicoprazopagamentoitemapoio, agendamentoservicoapoioFiltro, material));
					}
					parcela++;
				}
				listaDocumento.add(documentoPrincipal);
			}
		}
			
		return listaDocumento;
	}
	
	/**
	 *
	 * M�todo que seta valores dos documentos do agendamento servi�o
	 * 
	 * @param agendamentoservicoprazopagamentoapoio
	 * @param agendamentoservicoprazopagamentoitemapoio
	 * @param agendamentoservicoapoioFiltro
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Documento setaValoresDocumento(Agendamentoservicoprazopagamentoapoio agendamentoservicoprazopagamentoapoio, Agendamentoservicoprazopagamentoitemapoio agendamentoservicoprazopagamentoitemapoio, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro, Material material) {
		Documento documentoAux = new Documento();
		documentoAux.setDocumentoacao(Documentoacao.PREVISTA);
		documentoAux.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documentoAux.setPrazo(agendamentoservicoprazopagamentoapoio.getPrazopagamento());
		
		documentoAux.setEmpresa(agendamentoservicoapoioFiltro.getEmpresa());
		documentoAux.setTipopagamento(Tipopagamento.CLIENTE);
		documentoAux.setPessoa(agendamentoservicoprazopagamentoapoio.getCliente());
		documentoAux.setDocumentotipo(agendamentoservicoprazopagamentoitemapoio.getDocumentotipo());
		documentoAux.setDescricao(material.getNome());
		documentoAux.setDtemissao(new Date(System.currentTimeMillis()));
		documentoAux.setDtvencimento(agendamentoservicoprazopagamentoitemapoio.getDtvencimento());
		documentoAux.setValor(new Money(agendamentoservicoprazopagamentoitemapoio.getValor()));
		documentoAux.setReferencia(Referencia.MES_ANO_CORRENTE);
		
		documentoAux.setNumero((agendamentoservicoprazopagamentoitemapoio.getBanco() != null ? agendamentoservicoprazopagamentoitemapoio.getBanco()+" ": "") + (agendamentoservicoprazopagamentoitemapoio.getConta() != null ? agendamentoservicoprazopagamentoitemapoio.getConta()+" ": "") + 
							   (agendamentoservicoprazopagamentoitemapoio.getAgencia() != null ? agendamentoservicoprazopagamentoitemapoio.getAgencia()+" ": "") + (agendamentoservicoprazopagamentoitemapoio.getCheque() != null ? agendamentoservicoprazopagamentoitemapoio.getCheque()+" ": ""));
		
		Rateio rateio = new Rateio();
		List<Rateioitem> lista = new ArrayList<Rateioitem>();
		Rateioitem rateioitem = new Rateioitem(material.getContagerencialvenda(), material.getCentrocustovenda(), null, new Money(agendamentoservicoprazopagamentoitemapoio.getValor()), 100.0);
		lista.add(rateioitem);
		rateio.setListaRateioitem(lista);
		documentoAux.setRateio(rateio);
		
		return documentoAux;
	}
	
	/**
	 * M�todo que gera relat�rio CSV para, conta receber e conta a pagar, com os mesmos dados da listagem 
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Resource gerarRelatorioDocumentoCSV(DocumentoFiltro filtro) {
		String labeldescricao = "Recebimento de";
		if(filtro.getTipoConta().equals("PAGAR")){
			labeldescricao = "Pagamento a";			
		}
		
		Money totalCorrigido = new Money();
		Money total = new Money();
		List<ContaReceberPagarBeanReport> listaContaDetalhada = montaRelatorioDocumento(filtro, labeldescricao);
		
		List<Taxaitem> listaTaxaitem = null;
		String listaMaterialConcatenado = null;
		Controleorcamentoitem coitem = null;
		StringBuilder taxaSb = null;
		StringBuilder csv = new StringBuilder();
		
		// CABE�ALHO
		csv.append("Conta;Venc.;Primeiro Venc.;Emiss�o;Compet�ncia;Parc.;");
		if(!filtro.getTipoConta().equals("PAGAR")){
			csv.append("Nota Fiscal;");
		}
		if(Boolean.TRUE.equals(filtro.getExibeRateio())){
			csv.append(labeldescricao).append(" (Identificador);").append(labeldescricao).append(" (Nome);Descri��o;Doc.;Cheque;C�digo de Barras;Tipo de Documento;Situa��o;Data Pagto.;Valor;Valor corrigido;Valor Atual;Valor Movimenta��o;Valor Item;Projeto;Centro de custo;Conta gerencial;Or�ado por M�s;Recebimento;Taxas;");
			csv.append("\n");
		}else{
			csv.append(labeldescricao).append(" (Identificador);").append(labeldescricao).append(" (Nome);Descri��o;Doc.;Cheque;C�digo de Barras;Tipo de Documento;Situa��o;Data Pagto.;Valor;Valor corrigido;Valor Atual;Valor Movimenta��o;Recebimento;Taxas;");
			csv.append("\n");	
		}
		
		if(listaContaDetalhada != null && !listaContaDetalhada.isEmpty()){
			for (ContaReceberPagarBeanReport bean : listaContaDetalhada) {
				total = total.add(bean.getValorAtual());
				if(!filtro.getTipoConta().equals("PAGAR") && bean.getCdindicecorrecao() != null && bean.getSituacao().equals("PREVISTA")){
					bean.setValorCorrigido(indicecorrecaoService.calculaIndiceCorrecao(
																		new Indicecorrecao(bean.getCdindicecorrecao()), 
																		bean.getValorAtual(), 
																		new java.sql.Date(bean.getDtVencimento().getTime()), 
																		new java.sql.Date(bean.getDtEmissao().getTime())));
					if(bean.getValorCorrigido() != null) totalCorrigido = totalCorrigido.add(bean.getValorCorrigido());
				}
				
				
				// CONTA
				csv.append(bean.getCdconta()).append(";");
				
				// VENCIMENTO
				csv.append(SinedDateUtils.toString(bean.getDtVencimento())).append(";");
				
				// PRIMEIRO VENCIMENTO
				if(bean.getDtPrimeiroVencimento() != null){
					csv.append(SinedDateUtils.toString(bean.getDtPrimeiroVencimento()));
				} 
				csv.append(";");
				
				// EMISS�O
				csv.append(SinedDateUtils.toString(bean.getDtEmissao())).append(";");
				
				// COMPET�NCIA
				csv.append(bean.getDtcompetencia() == null ? "" : SinedDateUtils.toString(bean.getDtcompetencia())).append(";");
				
				// PARCELAS
				csv.append(bean.getParcelas() == null ? "" : " " + bean.getParcelas() + " ").append(";");
				
				// NUM�RO DA NF
				if(!filtro.getTipoConta().equals("PAGAR")){
					csv.append(Util.strings.isEmpty(bean.getNumeroNota()) ? "" : bean.getNumeroNota()).append(";");
				}
				
				// RECEBIMENTO DE OU PAGAMENTO A (IDENTIFICADOR)
				csv.append(bean.getClienteIdentificador() == null ? "" : bean.getClienteIdentificador()).append(";");
				
				// RECEBIMENTO DE OU PAGAMENTO A (NOME)
				csv.append(bean.getDescricaoPagamento() == null ? "" : bean.getDescricaoPagamento()).append(";");
				
				// DESCRI��O
				csv.append(bean.getDescricao() == null ? "" : "\"" + bean.getDescricao().replace("\"", "") + "\"").append(";");
				
				// DOCUMENTO
				csv.append(bean.getNumero() == null ? "" : bean.getNumero()).append(";");
				
				// CHEQUE
				csv.append(bean.getCheque() == null ? "" : bean.getCheque()).append(";");
				
				// C�DIGO DE BARRAS
				csv.append(bean.getCodigobarras() != null ? "'" + bean.getCodigobarras() : "").append(";");
				
				// TIPO DE DOCUMENTO
				csv.append(bean.getDocumentotipo() != null ? bean.getDocumentotipo().getNome() : "").append(";");
				
				// SITUA��O
				csv.append(bean.getSituacao()).append(";");
				
				// DATA DO PAGAMENTO
				csv.append(bean.getDatasPagto() != null ? bean.getDatasPagto() : "").append(";");
				
				// VALOR
				csv.append(bean.getValor()).append(";");
				
				// VALOR CORRIGIDO
				csv.append(bean.getValorCorrigido() == null ? "" : bean.getValorCorrigido()).append(";");
				
				// VALOR ATUAL
				csv.append(bean.getValorAtual()).append(";");
				
				// VALOR MOVIMENTA��O
				Money valorMovimentacoes = bean.getValorMovimentacoes();
				
				if(bean.getValorValecompra() != null){
					valorMovimentacoes = valorMovimentacoes.add(bean.getValorValecompra());
				}
				if(bean.getValorAntecipado() != null){
					valorMovimentacoes = valorMovimentacoes.add(bean.getValorAntecipado());
				}
				
				csv.append(valorMovimentacoes).append(";");
				
				if(Boolean.TRUE.equals(filtro.getExibeRateio())){
					// VALOR ITEM
					// PROJETO
					// CENTRO DE CUSTO
					// CONTA GERENCIAL
					// OR�ADO POR M�S
					csv.append(";;;;;");
				}
				
				// RECEBIMENTO
				if(filtro.getTipoConta().equals("PAGAR")){
					listaMaterialConcatenado =  documentoDAO.buscaMateriaisByDocumento(bean.getCdconta());
					if(listaMaterialConcatenado != null && !listaMaterialConcatenado.equals(""))
						csv.append(listaMaterialConcatenado);
				}
				csv.append(";");
				
				
				// TAXAS
				if(bean.getTaxaConta() != null){
					listaTaxaitem = taxaitemService.findForTaxa(bean.getTaxaConta());
					if (SinedUtil.isListNotEmpty(listaTaxaitem)){
						taxaSb = new StringBuilder();
						
						for (Taxaitem ti : listaTaxaitem) {
							if(taxaSb.length() > 0) taxaSb.append(", ");
							
							taxaSb.append(ti.getTipotaxa().getNome());
							if (ti.getMotivo() != null){
								taxaSb.append(" - Motivo: ");
								taxaSb.append(ti.getMotivo());
							}
							taxaSb.append(" - R$");
							taxaSb.append(ti.getValor());
						}
						
						csv.append("\"").append(taxaSb.toString()).append("\"");
					} 
				}
				csv.append(";");
				
				if(Boolean.TRUE.equals(filtro.getExibeRateio())){	
					for (Rateioitem rateioitem : bean.getListaRateioitem()) {
						csv.append("\n");
						csv.append(";;;;;;;;;;;;;;;");
						if(!filtro.getTipoConta().equals("PAGAR")){
							csv.append(";");
						}
						csv.append(";;;;");
						// VALOR
						csv.append(rateioitem.getValor()).append(";");
						// PROJETO
						csv.append(rateioitem.getProjeto() != null && rateioitem.getProjeto().getNome() != null ? rateioitem.getProjeto() : "").append(";");
						// CENTRO DE CUSTO
						csv.append(rateioitem.getCentrocusto() != null && rateioitem.getCentrocusto().getNome() != null ? rateioitem.getCentrocusto().getNome() : "").append(";");
						// CONTA GERENCIAL
						csv.append(rateioitem.getContagerencial() != null && rateioitem.getContagerencial().getNome() != null ? (rateioitem.getIdentificador() != null ? rateioitem.getIdentificador() + " " : "") + rateioitem.getContagerencial().getNome() : "").append(";");;
						
						// OR�ADO POR M�S
						if(filtro.getDocumentoclasse() != null && bean.getDtVencimento() != null){
							coitem = controleorcamentoitemService.findForValorOr�adoByContagerencial(
																	rateioitem.getContagerencial(), 
																	rateioitem.getCentrocusto() != null && rateioitem.getCentrocusto().getCdcentrocusto() != null ? rateioitem.getCentrocusto() : null,
																	filtro.getEmpresa(), 
//																	Integer.parseInt(new SimpleDateFormat("yyyy").format(bean.getDtVencimento())))
																	bean.getDtVencimento(),
																	filtro.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR) ? Tipooperacao.TIPO_DEBITO : Tipooperacao.TIPO_CREDITO,
																	rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null ? rateioitem.getProjeto() : null);						
												
							if (coitem != null && coitem.getControleorcamento() != null && coitem.getControleorcamento().getTipolancamento().getValue() == 0){
								if(coitem != null && coitem.getControleorcamento() != null && coitem.getControleorcamento().getQtnMesesExercicio() !=null && coitem.getControleorcamento().getQtnMesesExercicio() > 0){
									csv.append(coitem != null && coitem.getValor() != null ? "R$ " + coitem.getValor().divide(new Money(coitem.getControleorcamento().getQtnMesesExercicio())) : "");
								}else{
									csv.append(coitem != null && coitem.getValor() != null ? "R$ " + coitem.getValor().divide(new Money(1.00)) : "");
								}								
							} else {
								csv.append(coitem != null && coitem.getValor() != null ? "R$ " + coitem.getValor() : "");
							}					
						}
					}
				}
				csv.append("\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "documento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Documento contaReceberDevolucao (Integer cddocumento){
		return documentoDAO.contaReceberDevolucao(cddocumento);
	}
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @param documento
	 * @return
	 * @author Taidson
	 * @since 10/06/2010
	 */
	public Documento verificaDocumentoacao(Documento documento){
		return documentoDAO.verificaDocumentoacao(documento);
	}
	
	public void cancelaDocumentosAtrasados() {
		String cancelamentoAutomatico = parametrogeralService.getValorPorNome(PARAMETRO_CANCELAMENTO_CONTA_RECEBER);
		if(cancelamentoAutomatico != null && "TRUE".equals(cancelamentoAutomatico.trim().toUpperCase())){
			Date data = new Date(System.currentTimeMillis());
			Integer numDias = parametrogeralService.getInteger(PARAMETRO_DIAS_CANCELAMENTO_CONTA_RECEBER);
			data = SinedDateUtils.addDiasData(data, numDias*(-1));
			
			List<Documento> listaDocumento = this.findForCancelamentoAutomatico(data);
			if(listaDocumento != null && listaDocumento.size() > 0){			
				String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
				listaDocumento = documentoService.carregaDocumentos(whereIn);
				for (Documento documento : listaDocumento) {
					contapagarService.updateStatusConta(Documentoacao.CANCELADA, documento.getCddocumento().toString());
					documentoDAO.cancelaContaDevolvida(documento);
				}
				
				listaDocumento = documentoService.carregaDocumentos(whereIn);
				Documentohistorico dh;
				for (Documento documento : listaDocumento) {
					dh = new Documentohistorico();
					dh.setObservacao("Cancelamento autom�tico.");
					
					dh = documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.CANCELADA, documento);
					documentohistoricoService.saveOrUpdate(dh);
				}
			}
		}
	}
	
	private List<Documento> findForCancelamentoAutomatico(Date data) {
		return documentoDAO.findForCancelamentoAutomatico(data);
	}
	
	/**
	 * Faz refer�ncia a DAO
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 30/06/2010
	 */
	public Date getPrimeiroVencimentoDocumentoAtrasado(Cliente cliente){
		return documentoDAO.getPrimeiroVencimentoDocumentoAtrasado(cliente);
	}
	
	public Date getPrimeiroVencimentoContasEmAbertoByCliente(Cliente cliente){
		return documentoDAO.getPrimeiroVencimentoContasEmAbertoByCliente(cliente);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @param wherein
	 * @return
	 */
	public boolean haveProjetoCNPJ(String whereIn){
		return documentoDAO.haveProjetoCNPJ(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 20/07/2010
	 */
	public List<Documento> loadDocumentos(String whereIn){
		return documentoDAO.loadDocumentos(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param documento
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public List<Documento> docOrigemVenda(Documento documento) {
		return documentoDAO.docOrigemVenda(documento);
	}
	
	public void atualizaDtvencimento(Documento documento, Date dtvencimento) {
		documentoDAO.atualizaDtvencimento(documento, dtvencimento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 17/09/2010
	 */
	public List<Documento> findPagamentosByCliente(Cliente cliente, Empresa empresa) {
		return documentoDAO.findPagamentosByCliente(cliente, empresa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public List<Documento> contasAtrasadasByContrato(String whereIn){
		return documentoDAO.contasAtrasadasByContrato(whereIn);
	}
	
	public List<Documento> contasAtrasadasByCliente(Cliente cliente) {
		return documentoDAO.contasAtrasadasByCliente(cliente);
	}
	
	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAnaliseRecDespesaFlex
	 *
	 * @param identificador
	 * @param max
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForAnaliseRecDespesaFlex(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		return documentoDAO.findForAnaliseRecDespesaFlex(filtro, identificador, max, campo, ordenacao);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Verifica se existe contas a receber que est�o vinculadas a um contrato
	* 
	* @see	br.com.linkcom.sined.geral.dao.DocumentoDAO#findForContasContrato(Contrato contrato)
	*
	* @param contrato
	* @return
	* @since Jul 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public boolean findForContasContrato(Contrato contrato){
		return documentoDAO.findForContasContrato(contrato);
	}
	
	public void atualizaFaturamento(Integer cdcliente, Integer cddocumento) {
		List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(new Pessoa(cdcliente));
		Endereco endereco = contareceberService.enderecoSugerido(listaEndereco);
		documentoDAO.updateTipoPagamentoCliente(cddocumento, cdcliente);
		documentoDAO.updateMsg2Msg3(cddocumento);
		if(endereco != null){
			documentoDAO.updateEndereco(cddocumento, endereco);
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os dados da conta a receber para criar a nota fiscal
	* 
	* @see	br.com.linkcom.sined.geral.dao.DocumentoDAO#findForCriarnotafiscal(Documento documento)
	*
	* @param documento
	* @return
	* @since Oct 18, 2011
	* @author Luiz Fernando F Silva
	*/
	public Documento findForCriarnotafiscal(Documento documento) {
		return documentoDAO.findForCriarnotafiscal(documento);
	}
	
	public Documento criarCopia(Documento origem) {
		return criarCopia(origem, true);
	}
	
	public Documento criarCopia(Documento origem, boolean carregarDocumento) {
		origem = carregarDocumento ? loadForEntrada(origem) : origem;
		
		if(carregarDocumento){
			origem.setTaxa(taxaService.findByDocumento(origem));
		}
		
		Documento copia = new Documento();
		copia.setCopiadocumeto(Boolean.TRUE);
		copia.setEmpresa(origem.getEmpresa());
		copia.setNumero(origem.getNumero());
		copia.setDocumentotipo(origem.getDocumentotipo());
		copia.setTipopagamento(origem.getTipopagamento());
		copia.setDocumentoclasse(origem.getDocumentoclasse());
		copia.setOutrospagamento(origem.getOutrospagamento());
		copia.setMensagem1(origem.getMensagem1());
		copia.setMensagem2(origem.getMensagem2());
		copia.setMensagem3(origem.getMensagem3());
		copia.setMensagem4(origem.getMensagem4());
		copia.setMensagem5(origem.getMensagem5());
		copia.setMensagem6(origem.getMensagem6());
		copia.setConta(origem.getConta() != null ? contaService.load(origem.getConta()) : null);
		copia.setContacarteira(origem.getContacarteira() != null ? contacarteiraService.load(origem.getContacarteira()) : null);
		copia.setOperacaocontabil(origem.getOperacaocontabil());
		copia.setEndereco(origem.getEndereco());
		
		Pessoa pessoa = carregarDocumento ? pessoaService.load(origem.getPessoa()) : origem.getPessoa();
		copia.setPessoa(pessoa);
		
		if (pessoa instanceof Cliente)
			copia.setCliente((Cliente) pessoa);
		if (pessoa instanceof Fornecedor)
			copia.setFornecedor((Fornecedor) pessoa);
		if (pessoa instanceof Colaborador)
			copia.setColaborador((Colaborador) pessoa);
		
		copia.setDescricao(origem.getDescricao());
		copia.setDtemissao(origem.getDtemissao());
		copia.setDtvencimento(origem.getDtvencimento());
		copia.setValor(origem.getValor());
		copia.setIndicecorrecao(origem.getIndicecorrecao());
		copia.setDocumentoacao(origem.getDocumentoacao());
		copia.setReferencia(origem.getReferencia());
		
		copia.setRateio(new Rateio());
		copia.getRateio().setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
		
		if(origem.getRateio() != null && origem.getRateio().getListaRateioitem() != null){
			for (Rateioitem item : origem.getRateio().getListaRateioitem()){
				Rateioitem novoItem = new Rateioitem();
				novoItem.setContagerencial(new Contagerencial(item.getContagerencial().getCdcontagerencial()));
				novoItem.setCentrocusto(item.getCentrocusto());
				novoItem.setProjeto(item.getProjeto());
				novoItem.setValor(item.getValor());
				novoItem.setPercentual(item.getPercentual());
				
				copia.getRateio().getListaRateioitem().add(novoItem);
			}
		}
		
		copia.setTaxa(new Taxa());
		copia.getTaxa().setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class));
		
		if(origem.getTaxa() != null && origem.getTaxa().getListaTaxaitem() != null){
			for (Taxaitem taxaitem : origem.getTaxa().getListaTaxaitem()) {
				Taxaitem novoItem = new Taxaitem();
				novoItem.setTipotaxa(taxaitem.getTipotaxa());
				novoItem.setValor(taxaitem.getValor());
				novoItem.setPercentual(taxaitem.isPercentual());
				novoItem.setDtlimite(taxaitem.getDtlimite());
				novoItem.setAodia(taxaitem.getAodia());
				novoItem.setGerarmensagem(taxaitem.getGerarmensagem());
				novoItem.setMotivo(taxaitem.getMotivo());
				novoItem.setTipocalculodias(taxaitem.getTipocalculodias());
				novoItem.setDias(taxaitem.getDias());
				
				copia.getTaxa().getListaTaxaitem().add(novoItem);
			}
		}
		
		return copia;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findByPessoaDocumentoclasse(Pessoa pessoa, Documentoclasse classe)
	 *
	 * @param pessoa
	 * @param classe
	 * @param listagemTotal 
	 * @return
	 * @since 10/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findByPessoaDocumentoclasse(Pessoa pessoa, Documentoclasse classe, boolean listagemTotal) {
		return documentoDAO.findByPessoaDocumentoclasse(pessoa, classe, listagemTotal);
	}

	/**M�todo que busca os documentos de Conta a Pagar
	 * @author Thiago Augusto
	 * @param filtro
	 * @return
	 */
	public List<Documento> findDocumentosRemessa(GerarArquivoRemessaBean filtro){
		return documentoDAO.findDocumentosRemessa(filtro);
	}
	
	/**M�todo que padroniza as datas (ddMMyyyy) para a gera��o do arquivo
	 * @author Thiago Augusto
	 * @param data
	 * @return
	 */
	public String padronizaData(Date data){
		if (data != null){
			SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
			return format.format(data);
		} else 
			return "";
	}
		
	public Resource createDuplicata(WebRequestContext request) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Documento> listaDocumento = documentoDAO.carregaDocumentos(whereIn);
		for (Documento documento : listaDocumento) {
			if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) 
					&& !documento.getDocumentoacao().equals(Documentoacao.PREVISTA)){
				request.addError("S� � poss�vel gerar duplicatas para as situa��es Prevista e Definitiva!");
				return null;
			} 
		}
		MergeReport mergeReport = new MergeReport("duplicata_"+SinedUtil.datePatternForReport()+".pdf");
		for (Documento documento : listaDocumento) {
			mergeReport.addReport(gerarDuplicata(documento));
		}
		return mergeReport.generateResource();
	}
	
	public Report gerarDuplicata(Documento documento){
		Report report = new Report("/financeiro/duplicata");
		Empresa empresa = documento.getEmpresa();
		Cliente cliente = new Cliente();
		empresa = empresaService.loadForEntrada(empresa);
		cliente.setCdpessoa(documento.getPessoa().getCdpessoa());
		cliente = clienteService.loadForEntrada(cliente);
		report.addParameter("LOGO_EMPRESA", SinedUtil.getLogo(empresa));
		report.addParameter("RAZAO_SOCIAL", empresa.getRazaoSocial());
		report.addParameter("ENDERECO_EMPRESA", empresa.getEnderecosSemQuebraLinha());
		report.addParameter("CNPJ_EMPRESA", empresa.getCnpj().getValue());
		report.addParameter("INSCRICAO_EMPRESA", empresa.getInscricaoestadual());
		report.addParameter("TELEFONE_EMPRESA", empresa.getTelefonesSemQuebraLinha());
		report.addParameter("EMAIL_EMPRESA", empresa.getEmail());
		report.addParameter("NOTA_FISCAL", documento.getNumero());
		report.addParameter("VALOR_NOTA", documento.getValor().toString());
		report.addParameter("NUMERO_DUPLICATA", documento.getCddocumento().toString());
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		report.addParameter("VENCIMENTO", format.format(documento.getDtvencimento()));
		report.addParameter("CLIENTE", cliente.getNome());
		report.addParameter("ENDERECO_CLIENTE", documento.getEndereco() != null ? documento.getEndereco().getLogradouroCompletoComBairroCEP() : "");
		if (cliente.getCnpj() != null)
			report.addParameter("CNPJ_CPF_CLIENTE", cliente.getCnpj().getValue());
		else 
			report.addParameter("CNPJ_CPF_CLIENTE", cliente.getCpf().getValue());
			
		report.addParameter("INSCRICAO_CLIENTE", cliente.getInscricaoestadual());
		report.addParameter("RAZAO_EMPRESA", empresa.getRazaoSocial());
		report.addParameter("DATA_ATUAL", format.format(new java.util.Date()));
		report.addParameter("VALOR_EXTENSO", new Extenso(documento.getValor()).toString().toUpperCase());
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.DocumentoDAO#updateNumeroNFDocumentoVenda(Documento documento, String numeroNF)
	 *
	 * @param documento
	 * @param numeroNF
	 * @author Luiz Fernando
	 */
	public void updateNumeroNFDocumentoVenda(Documento documento, String numeroNF) {
		documentoDAO.updateNumeroNFDocumentoVenda(documento, numeroNF);
	}
	
	public void updateCdRateio(Integer cdDoc, Integer cdrateio) {
		documentoDAO.updateCdRateio(cdDoc,cdrateio);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateDtvencimentoNFDocumentoVenda(Documento documento, Date dtvencimento)
	 *
	 * @param documento
	 * @param dtvencimento
	 * @author Luiz Fernando
	 * @since 22/05/2014
	 */
	public void updateDtvencimentoNFDocumentoVenda(Documento documento, Date dtvencimento) {
		documentoDAO.updateDtvencimentoNFDocumentoVenda(documento, dtvencimento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateMesangem1And2NFDocumentoVenda(Documento documento, String msg1, String msg2)
	 *
	 * @param documento
	 * @param msg1
	 * @param msg2
	 * @author Luiz Fernando
	 */
	public void updateMesangem1And2NFDocumentoVenda(Documento documento, String msg1, String msg2) {
		documentoDAO.updateMesangem1And2NFDocumentoVenda(documento, msg1, msg2);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateNumerosNFDocumento(String documentos, String numeroNF)
	 *
	 * @param documentos
	 * @param numeroNF
	 * @since 15/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumerosNFDocumento(String documentos, String numeroNF) {
		documentoDAO.updateNumerosNFDocumento(documentos, numeroNF);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @param numero
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumerosNFDocumentoFaturamento(Documento documento, String numero) {
		documentoDAO.updateNumerosNFDocumentoFaturamento(documento, numero);
	}
	
	/**
	 * M�todo que concatena a identifica��o ao n�mero, caso seja nota de produto ou servi�o
	 *
	 * @param documento
	 * @param numero
	 * @param notaTipo
	 * @author Luiz Fernando
	 * @since 08/05/2014
	 */
	public void updateNumerosNFDocumentoFaturamento(Documento documento, String numero, NotaTipo notaTipo) {
		if(notaTipo != null && numero != null && !numero.equals("") && 
				parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_PREFIXO_NFE_NFSE)){
			if(NotaTipo.NOTA_FISCAL_SERVICO.equals(notaTipo)){
				numero = "NFS-e " + numero;
			}else if(NotaTipo.NOTA_FISCAL_PRODUTO.equals(notaTipo)){
				numero = "NF-e " + numero;
			}
		}
		this.updateNumerosNFDocumentoFaturamento(documento, numero);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 *
	 * @author Thiago Augusto
	 * @date 02/04/2012
	 * @param bean
	 * @return
	 */
	public Documento getDocumentoComArquivo(Documento bean){
		return documentoDAO.getDocumentoComArquivo(bean);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Documento> findDocumentosSemMovimentacao(Tipooperacao tipooperacao){
		
		List<Integer> documentoAcao = new ArrayList<Integer>();
			documentoAcao.add(Documentoacao.PREVISTA.getCddocumentoacao());
			documentoAcao.add(Documentoacao.DEFINITIVA.getCddocumentoacao());
			documentoAcao.add(Documentoacao.AUTORIZADA.getCddocumentoacao());
			documentoAcao.add(Documentoacao.AUTORIZADA_PARCIAL.getCddocumentoacao());
		
		return documentoDAO.findDocumentosSemMovimentacao(documentoAcao,tipooperacao);
	}
	
	/**
	 * 
	 * @param documento
	 */
	public void atualizaDocumentoacao(Documento documento){
		documentoDAO.atualizaDocumentoacao(documento);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Rafael Salvio
	 *@date 10/05/2012
	 * @param movimentacao
	 * @return Documento
	 */
	public List<Documento> findDocumentoByMovimentacao(Movimentacao movimentacao){
		return documentoDAO.findDocumentoByMovimentacao(movimentacao);
	}
	
	/**
	 * 
	 * M�todo que faz a valida��o da Forma de Pagamento x Tipo de pagamento.
	 *
	 * @name validaPagamentos
	 * @param formaPagamento
	 * @param tipoPagamento
	 * @return void
	 * @author Thiago Augusto
	 * @date 28/05/2012
	 *
	 */
	public void validaPagamentosSispag(BancoFormapagamento bancoFormaPagamento, BancoTipoPagamento bancoTipoPagamento){
		
		Integer[] salario = {1,60,2,3,10,41};
		Integer[] dividendos = {1,5,2,3,7,41};
		Integer[] juros_viajante_vendedor = {1,2,3,10,41};
		Integer[] fornecedores = {1,6,5,2,3,7,10,30,31,32,13,41,43};
		Integer[] beneficios_sinistros = {1,5,2,3,10,41};
		Integer[] fundoinvestimento = {1,2,10};
		Integer[] diversos = {1,6,5,2,3,7,10,30,31,32,13,41,43};
		Integer[] tributos = {16,18,21,22,91,17,35,19,25,27};
			
		if ((bancoTipoPagamento.getIdentificador() == 30 && !Arrays.asList(salario).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 10 && !Arrays.asList(dividendos).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 15 && !Arrays.asList(juros_viajante_vendedor).contains(bancoFormaPagamento.getIdentificador())) ||		
			(bancoTipoPagamento.getIdentificador() == 60 && !Arrays.asList(juros_viajante_vendedor).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 80 && !Arrays.asList(juros_viajante_vendedor).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 20 && !Arrays.asList(fornecedores).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 90 && !Arrays.asList(beneficios_sinistros).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 50 && !Arrays.asList(beneficios_sinistros).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 40 && !Arrays.asList(fundoinvestimento).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 98 && !Arrays.asList(diversos).contains(bancoFormaPagamento.getIdentificador())) ||
			(bancoTipoPagamento.getIdentificador() == 22 && !Arrays.asList(tributos).contains(bancoFormaPagamento.getIdentificador()))){
			throw new SinedException("N�o � possivel a utiliza��o da forma de pagamento '" + bancoFormaPagamento.getDescricao() + "' para o tipo de pagamento '" + bancoTipoPagamento.getDescricao() + "'.\n");
		}	
	}
	
	/**
	 * M�todo com refer�ncia ao DAO.
	 * @param documento
	 * @return Boolean
	 * @author Rafael Salvio
	 */
	public Boolean verificaDuplicidade(Documento documento){
		return documentoDAO.verificaDuplicidade(documento);
	}
	
	public List<Documento> calculaDocumentoValorByDataRef(List<Documento> listaDocumento, Date dataRef, boolean verificarTaxaMovSeparada){
		return calculaDocumentoValorByDataRef(listaDocumento, dataRef, null, verificarTaxaMovSeparada);
	}
	
	/**
	 * 
	 * M�todo que busca o valor atual de uma lista de documentos com base na data de refer�ncia
	 *
	 * @name calculaDocumentoValorByDataRef
	 * @param listaDocumento
	 * @param dataRef
	 * @return listaDocumento
	 * @author Marden Silva
	 * @date 03/07/2012
	 *
	 */
	public List<Documento> calculaDocumentoValorByDataRef(List<Documento> listaDocumento, Date dataRef, List<Documento> listaDocumentoForValecompra, boolean verificarTaxaMovSeparada){
		if (listaDocumento == null || dataRef == null)
			throw new SinedException("O par�metro documento ou data de refer�ncia n�o pode ser nulo.");
		
		for (Documento documento : listaDocumento){
			if (Documentoacao.BAIXADA_PARCIAL.equals(documento.getDocumentoacao())){
				Money valorrestante = movimentacaoService.calcularMovimentacoesByDocumento(documento, dataRef != null ? dataRef : new Date(System.currentTimeMillis()));
				documento.setValoratual(valorrestante);
			}else {
				Money valoratual = null; 
				
				if(SinedUtil.isListNotEmpty(listaDocumentoForValecompra)){
					for(Documento d : listaDocumentoForValecompra){
						if(documento.equals(d)){
							if(d.getValoratualbaixa() != null){
								valoratual = d.getValoratualbaixa();
							}
							break;
						}
					}
				}
				
				if(valoratual == null){
					valoratual = this.getValorAtual(documento.getCddocumento(), dataRef);
				}
				documento.setValoratual(valoratual);
				if(verificarTaxaMovSeparada){
					Documento documentoTaxa = documentoService.loadForMovimentacaoTaxa(documento);
					Taxaitem taxaitem = documentoService.getTaxaitemByTipo(documentoTaxa, Tipotaxa.TAXAMOVIMENTO);
					
					if (documentoTaxa!=null && taxaitem != null && taxaitem.getValor() != null){
						documento.setValoratualbaixaSemTaxa(this.getValorAtual(documento.getCddocumento(), dataRef, true));
					}
				}
			}
			
			Money taxa = new Money();
			if(documento.getDocumentoclasse() != null && documento.getDocumentoclasse().getCddocumentoclasse() != null && 
					Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse())){
				taxa = documento.getValoratual().subtract(documento.getValorautorizado());
			}else{
				taxa = documento.getValoratual().subtract(documento.getValor());
			}
			documento.setMoneyTaxas(taxa);			
		}
		
		return listaDocumento;
	}	
	
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		return documentoDAO.verificaListaFechamento(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereInConta 
	 * 
	 * @param whereIn, dtpagamento
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn, Date dtpagamento, String whereInConta){
		return documentoDAO.verificaListaFechamento(whereIn, dtpagamento, whereInConta);
	}
	
	/**
	 * Executa a a��o "Retirar protesto" em uma transaction.
	 *
	 * @param documentoHistorico
	 * @return
	 * @author Rafael Salvio
	 */
	public String retirarProtesto(final Documentohistorico documentoHistorico) {
		Object nomes = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				String whereIn = documentoHistorico.getIds();
				String obs = documentoHistorico.getObservacao();
				Documentoacao acao = documentoHistorico.getDocumentoacao();
				String nomes = "";
				Documentohistorico dh = null;

				List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);

				for (Documento documento: listaDocumento) {

					//S� � poss�vel retirar protesto se tiver a dtcartorio diferente de null.
					if (documento.getDtcartorio() == null) {
						whereIn = whereIn.replace(documento.getCddocumento()+",", "");
						whereIn = whereIn.replace(","+documento.getCddocumento(), "");
						nomes += documento.getCddocumento()+", ";
						continue;
					}

					dh = new Documentohistorico();
					dh.setObservacao(obs);


					dh = documentohistoricoService.gerarHistoricoDocumento(dh, acao, documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
				}

				if (!whereIn.equals("")) {
					updateRetirarCartorio(whereIn);
				}
				return nomes;
			}
		});
		callProcedureAtualizaDocumento(documentoHistorico.getIds());
		return (String) nomes;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param whereIn
	 * @param documentoacao
	 * @author Rafael Salvio
	 */
	public void updateRetirarCartorio(String whereIn){
		documentoDAO.updateRetirarCartorio(whereIn);
	}
	
	public void updateDocumentoNumeroByNota(String numeroNfse, Nota nota) {
		documentoDAO.updateDocumentoNumeroByNota(numeroNfse, nota);
	}
	
	/**
	 * 
	 * M�todo que atualiza o valor final pago na conta a pagar para o lote.
	 *
	 * @name atualizaValorFinal
	 * @param lote
	 * @param listDetalhe
	 * @return void
	 * @author Thiago Augusto
	 * @date 31/08/2012
	 *
	 */
	public void atualizaValorFinal(ArquivoRemessaItauTrailerLote lote, List<ArquivoRemessaItauLT> listDetalhe){
		Money valorFinal = new Money(Long.parseLong(lote.getValorPagamento()), true);
		for (ArquivoRemessaItauLT detalhe : listDetalhe) {
			if (detalhe.getAcrescimos() != null && !detalhe.getAcrescimos().equals(""))
				valorFinal = valorFinal.add(new Money(Long.parseLong(detalhe.getAcrescimos()), true));
			if (detalhe.getDescontos() != null && !detalhe.getDescontos().equals(""))
				valorFinal = valorFinal.subtract(new Money(Long.parseLong(detalhe.getDescontos()), true));
		}
		lote.setValorPagamento(valorFinal.toString().replace(".", "").replace(",", ""));
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#isChequeAssociado(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isChequeAssociado(Documento documento) {
		return documentoDAO.isChequeAssociado(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForContasVenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean findForContasVenda(Venda venda) {
		return documentoDAO.findForContasVenda(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#getTotalAntecipacaoByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getTotalAntecipacaoByCliente(Cliente cliente){
		return documentoDAO.getTotalAntecipacaoByCliente(cliente);
	}
	
	public List<Documento> findDocumentosForRemessaConfiguravel(String whereIn) {
		return documentoDAO.findDocumentosForRemessaConfiguravel(whereIn);
	}
		
	/**
	 * M�todo que preenche as informa��es dos documentos
	 * utilizados na gera��o do arquivo de remessa configur�vel
	 *
	 * @param listaDocumento
	 * @param baixarContaBean
	 * @return List<DocumentoVO>
	 * @author Marden Silva
	 * @param dtpagamento 
	 * @param agrupamentoFornecedor 
	 */
	public List<DocumentoVO> getListaDocumentoForRemessaConfiguravel(List<Documento> listaDocumento, Date dtpagamento, boolean agrupamentoFornecedor){
		List<DocumentoVO> listaDocumentoVO_ANTIGO = new ArrayList<DocumentoVO>();
		
		if (listaDocumento != null){
			List<Integer> listaCddocumento = new ArrayList<Integer>();
			for (Documento doc : listaDocumento){
				if(listaCddocumento.contains(doc.getCddocumento())) throw new SinedException("Documento duplicado na geracao do arquivo de remessa: " + doc.getCddocumento());
				
				DocumentoVO docVO = new DocumentoVO();
					
				docVO.setCddocumento(doc.getCddocumento());
				docVO.setValor(doc.getValor());
				docVO.setValoratual(doc.getAux_documento().getValoratual());
				
				docVO.setDescricao(doc.getDescricao());
				docVO.setDocumento(doc.getNumero());
				docVO.setNossonumero(doc.getNossonumero());
				docVO.setDtemissao(doc.getDtemissao());
				docVO.setDtvencimento(doc.getDtvencimento());
				docVO.setMensagem1(doc.getMensagem1());
				docVO.setMensagem2(doc.getMensagem2());
				docVO.setMensagem3(doc.getMensagem3());
				docVO.setMensagem4(doc.getMensagem4());
				docVO.setMensagem5(doc.getMensagem5());
				docVO.setMensagem6(doc.getMensagem6());
				docVO.setCodigobarra(doc.getCodigobarras());
				docVO.setCddocumentotipo(doc.getDocumentotipo() != null ? doc.getDocumentotipo().getCddocumentotipo() : null);
				if(doc.getDocumentotipo() != null){
					docVO.setDocumentotipo(doc.getDocumentotipo().getNome());
					docVO.setTipotributo(doc.getDocumentotipo().getTipotributo() != null ? doc.getDocumentotipo().getTipotributo().getDescricao() : null);
				}
				
				if (doc.getTaxa() != null && doc.getTaxa().getListaTaxaitem() != null){
					for (Taxaitem ti : doc.getTaxa().getListaTaxaitem()){						
						
						Money percentual = ti.getValor();
						Money valor = ti.getValor();
						
						if (ti.isPercentual()){								
							valor = valor.multiply(doc.getValor());
							valor = valor.divide(new Money(100));
						} else {
							percentual = percentual.divide(doc.getValor());
							percentual = percentual.multiply(new Money(100));
						}
						
						if (Tipotaxa.DESAGIO.equals(ti.getTipotaxa())){
							
							if (docVO.getDesagiopercentual() != null)
								docVO.setDesagiopercentual(docVO.getDesagiopercentual().add(percentual));
							else
								docVO.setDesagiopercentual(percentual);
							
							if (docVO.getDesagiovalor() != null)
								docVO.setDesagiovalor(docVO.getDesagiovalor().add(valor));
							else
								docVO.setDesagiovalor(valor);
							
							docVO.setDesagiotipo(ti.isPercentual() ? "P" : "V");
							docVO.setDesagiodtlimite(ti.getDtlimite());			
							
							if(dtpagamento != null && SinedDateUtils.afterIgnoreHour(ti.getDtlimite(), dtpagamento)){
								int difdias = SinedDateUtils.diferencaDias(ti.getDtlimite(), dtpagamento);
								docVO.setDesagiovalorcalculado(valor.multiply(new Money(new Double(difdias))));
							}
						} else if (Tipotaxa.DESCONTO.equals(ti.getTipotaxa())){
							
							if (docVO.getDescontopercentual() != null)
								docVO.setDescontopercentual(docVO.getDescontopercentual().add(percentual));
							else
								docVO.setDescontopercentual(percentual);
							
							if (docVO.getDescontovalor() != null)
								docVO.setDescontovalor(docVO.getDescontovalor().add(valor));
							else
								docVO.setDescontovalor(valor);							
							
							docVO.setDescontotipo(ti.isPercentual() ? "P" : "V");
							docVO.setDescontodtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.afterIgnoreHour(ti.getDtlimite(), dtpagamento)){
								docVO.setDesagiovalorcalculado(valor);
							}
						} else if (Tipotaxa.JUROS.equals(ti.getTipotaxa()) || Tipotaxa.JUROS_MES.equals(ti.getTipotaxa())) {
							
							if (docVO.getJurospercentual() != null)
								docVO.setJurospercentual(docVO.getJurospercentual().add(percentual));
							else
								docVO.setJurospercentual(percentual);
							
							if (docVO.getJurosvalor() != null)
								docVO.setJurosvalor(docVO.getJurosvalor().add(valor));
							else
								docVO.setJurosvalor(valor);							
							
							docVO.setJurostipo(ti.isPercentual() ? "P" : "V");
							docVO.setJurosdtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), dtpagamento)){
								int difdias = SinedDateUtils.diferencaDias(dtpagamento, ti.getDtlimite());
								docVO.setJurosvalorcalculado(valor.multiply(new Money(new Double(difdias))));
							}
							
							if(Tipotaxa.JUROS.equals(ti.getTipotaxa())){
								docVO.setCodigojurosmora(1);
							} else if(Tipotaxa.JUROS_MES.equals(ti.getTipotaxa())){
								docVO.setCodigojurosmora(2);
							}
						} else if (Tipotaxa.MULTA.equals(ti.getTipotaxa())){
							
							if (docVO.getMultapercentual() != null)
								docVO.setMultapercentual(docVO.getMultapercentual().add(percentual));
							else
								docVO.setMultapercentual(percentual);
							
							if (docVO.getMultavalor() != null)
								docVO.setMultavalor(docVO.getMultavalor().add(valor));
							else
								docVO.setMultavalor(valor);							
							
							docVO.setMultatipo(ti.isPercentual() ? "P" : "V");
							docVO.setMultadtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), dtpagamento)){
								docVO.setMultavalorcalculado(valor);
							}
						} else if (Tipotaxa.TAXABOLETO.equals(ti.getTipotaxa())){
							
							if (docVO.getTaxaboletopercentual() != null)
								docVO.setTaxaboletopercentual(docVO.getTaxaboletopercentual().add(percentual));
							else
								docVO.setTaxaboletopercentual(percentual);
							
							if (docVO.getTaxaboletovalor() != null)
								docVO.setTaxaboletovalor(docVO.getTaxaboletovalor().add(valor));
							else
								docVO.setTaxaboletovalor(valor);							
							
							docVO.setTaxaboletotipo(ti.isPercentual() ? "P" : "V");
							docVO.setTaxaboletodtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), dtpagamento)){
								docVO.setTaxaboletovalorcalculado(valor);
							}
						} else if (Tipotaxa.TERMO.equals(ti.getTipotaxa())){

							if (docVO.getTermopercentual() != null)
								docVO.setTermopercentual(docVO.getTermopercentual().add(percentual));
							else
								docVO.setTermopercentual(percentual);
							
							if (docVO.getTermovalor() != null)
								docVO.setTermovalor(docVO.getTermovalor().add(valor));
							else
								docVO.setTermovalor(valor);							
							
							docVO.setTermotipo(ti.isPercentual() ? "P" : "V");
							docVO.setTermodtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), dtpagamento)){
								docVO.setTermovalorcalculado(valor);
							}
						} else if (Tipotaxa.TAXAMOVIMENTO.equals(ti.getTipotaxa())){

							if (docVO.getTaxamovimentopercentual() != null)
								docVO.setTaxamovimentopercentual(docVO.getTaxamovimentopercentual().add(percentual));
							else
								docVO.setTaxamovimentopercentual(percentual);
							
							if (docVO.getTaxamovimentovalor() != null)
								docVO.setTaxamovimentovalor(docVO.getTaxamovimentovalor().add(valor));
							else
								docVO.setTaxamovimentovalor(valor);							
							
							docVO.setTaxamovimentotipo(ti.isPercentual() ? "P" : "V");
							docVO.setTaxamovimentodtlimite(ti.getDtlimite());
							
							if(dtpagamento != null && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), dtpagamento)){
								docVO.setTaxamovimentovalorcalculado(valor);
							}
						}						
					}
				}
				
				if (doc.getPessoa() != null){
					PessoaVO pessoaVO = new PessoaVO();	
					
					pessoaVO.setCdpessoa(doc.getPessoa().getCdpessoa());
					
					if (doc.getPessoa() != null && doc.getPessoa().getCliente() != null &&
							doc.getPessoa().getCliente().getRazaosocial() != null)
						pessoaVO.setNome(doc.getPessoa().getCliente().getRazaosocial());
					else
						pessoaVO.setNome(doc.getPessoa().getNome());
					
					pessoaVO.setCnpj(doc.getPessoa().getCnpj() != null ? doc.getPessoa().getCnpj().getValue() : null);
					pessoaVO.setCpf(doc.getPessoa().getCpf() != null ? doc.getPessoa().getCpf().getValue() : null);
					pessoaVO.setEmail(doc.getPessoa().getEmail());
					
					if (doc.getPessoa().getCnpj() != null)
						pessoaVO.setCnpjcpf(doc.getPessoa().getCnpj().getValue());
					else if (doc.getPessoa().getCpf() != null)
						pessoaVO.setCnpjcpf(doc.getPessoa().getCpf().getValue());
					
					if (doc.getPessoa().getTipopessoa() != null)
						pessoaVO.setTipopessoa(doc.getPessoa().getTipopessoa().getValue());
					
					if (doc.getPessoa().getDadobancario() != null){
						if(doc.getPessoa().getDadobancario().getBanco() != null &&
								doc.getPessoa().getDadobancario().getBanco().getNumero() != null){
							pessoaVO.setBanconumero(doc.getPessoa().getDadobancario().getBanco().getNumero().toString());
						}
						pessoaVO.setAgencia(doc.getPessoa().getDadobancario().getAgencia());
						pessoaVO.setDvagencia(doc.getPessoa().getDadobancario().getDvagencia());
						pessoaVO.setConta(doc.getPessoa().getDadobancario().getConta());
						pessoaVO.setDvconta(doc.getPessoa().getDadobancario().getDvconta());
						if (doc.getPessoa().getDadobancario().getTipoconta() != null)
							pessoaVO.setCdtipoconta(doc.getPessoa().getDadobancario().getTipoconta().getCdtipoconta());
					}
					Endereco endereco = new Endereco();
					if (doc.getEndereco() != null){
						endereco = doc.getEndereco();
					}else if(SinedUtil.isListNotEmpty(doc.getPessoa().getListaEndereco())){
						endereco = doc.getPessoa().getEnderecoWithPrioridade();
					}
					
					if (endereco != null){
						pessoaVO.setLogradouro(SinedUtil.removeQuebraLinha(endereco.getLogradouro()));
						pessoaVO.setNumero(endereco.getNumero());
						pessoaVO.setComplemento(SinedUtil.removeQuebraLinha(endereco.getComplemento()));
						if(endereco.getBairro() != null){
							pessoaVO.setBairro(SinedUtil.removeQuebraLinha(endereco.getBairro()));							
						}
						if (endereco.getCep() != null){
							pessoaVO.setCep(endereco.getCep().getValue());							
						}
						if (endereco.getMunicipio() != null){
							pessoaVO.setMunicipio(endereco.getMunicipio().getNome());
							if (endereco.getMunicipio().getUf() != null){
								pessoaVO.setUf(endereco.getMunicipio().getUf().getSigla());								
							}
						}
					}
					
					docVO.setPessoaVO(pessoaVO);
				}
				
				if(doc.getEmpresa() != null){
					Empresa empresa = doc.getEmpresa(); 
					if(doc.getConta() != null && doc.getConta().getEmpresatitular() != null && 
							!doc.getEmpresa().equals(doc.getConta().getEmpresatitular())){
						docVO.setSacadoravalista(true);
						EmpresaVO empresaVO = empresaService.getEmpresaForRemessaConfiguravel(doc.getConta().getEmpresatitular());					
						docVO.setEmpresaTITULAR(empresaVO);
					}
					
					EmpresaVO empresaVO = empresaService.getEmpresaForRemessaConfiguravel(empresa);					
					docVO.setEmpresaDOC(empresaVO);
				}
				
				docVO.setCodigoreceita(doc.getCodigoreceita());
				docVO.setCompetencia(doc.getMesanoAux());
				docVO.setValoroutrasentidades(doc.getValoroutrasentidades());
				docVO.setAtualizacaomonetaria(doc.getValoratualizacaomonetaria());
				docVO.setPeriodoapuracao(doc.getDtapuracao());
				docVO.setNumeroreferencia(doc.getNumeroreferencia());
				docVO.setValorreceita(doc.getValorreceitabrutaacumulada());
				docVO.setPercentualreceita(doc.getPercentualreceitabrutaacumulada());
				docVO.setIdentificacaotributo(doc.getIdentificacaotributo() != null ? doc.getIdentificacaotributo().getDescricao() : null);
				docVO.setInscestcodmunicipnumdec(doc.getIecodmunnumdec());
				docVO.setDividaativanumetiqueta(doc.getDividaativanumetiqueta());
				docVO.setNumparcelanotificacao(doc.getNumparcnotificacao());
				docVO.setAnobase(doc.getAnobase());
				docVO.setMesano(doc.getMesano());
				docVO.setRenavam(doc.getRenavam());
				docVO.setSiglauf(doc.getUf() != null ? doc.getUf().getSigla() : null);
				docVO.setCodigomunicipio(doc.getMunicipio() != null ? doc.getMunicipio().getCdibge() : null);
				docVO.setPlaca(doc.getPlacaveiculo());
				
				docVO.setIdentificadordofgts(doc.getNfgts());
				docVO.setLacreconectividadesocial(doc.getLacreconectividade());
				docVO.setDigitolacreconectividadesocial(doc.getDigitolacre());
				
				
				docVO.setOpcaopagamento(doc.getOpcaopagamento() != null ? doc.getOpcaopagamento().getDescricao() : "");
				
				listaDocumentoVO_ANTIGO.add(docVO);
				listaCddocumento.add(doc.getCddocumento());
			}
		}
		
		List<DocumentoVO> listaDocumentoVO_NOVO = new ArrayList<DocumentoVO>();
		if(agrupamentoFornecedor){
			Collections.sort(listaDocumentoVO_ANTIGO,new Comparator<DocumentoVO>(){
				public int compare(DocumentoVO o1, DocumentoVO o2) {
					int compareToDataEmissao = o1.getDtemissao().compareTo(o2.getDtemissao());
					if(compareToDataEmissao == 0){
						return o1.getCddocumento().compareTo(o2.getCddocumento());
					}
					return compareToDataEmissao;
				}
			});
			
			
			for (DocumentoVO documentoVO : listaDocumentoVO_ANTIGO) {
				boolean achou = false;
				for (DocumentoVO documentoVO_CONF : listaDocumentoVO_NOVO) {
					if(documentoVO_CONF.getPessoaVO() != null && 
						documentoVO_CONF.getPessoaVO().getCdpessoa() != null &&
						documentoVO.getPessoaVO() != null && 
						documentoVO.getPessoaVO().getCdpessoa() != null &&
						documentoVO.getPessoaVO().getCdpessoa().equals(documentoVO_CONF.getPessoaVO().getCdpessoa())){
						
						documentoVO_CONF.getIdsAgrupamento().add(documentoVO.getCddocumento());
						documentoVO_CONF.setValor(documentoVO_CONF.getValor().add(documentoVO.getValor()));
						documentoVO_CONF.setValoratual(documentoVO_CONF.getValoratual().add(documentoVO.getValoratual()));
						
						documentoVO_CONF.setDesagiovalor(addValor(documentoVO_CONF.getDesagiovalor(), documentoVO.getDesagiovalor()));
						documentoVO_CONF.setDescontovalor(addValor(documentoVO_CONF.getDescontovalor(), documentoVO.getDescontovalor()));
						documentoVO_CONF.setJurosvalor(addValor(documentoVO_CONF.getJurosvalor(), documentoVO.getJurosvalor()));
						documentoVO_CONF.setMultavalor(addValor(documentoVO_CONF.getMultavalor(), documentoVO.getMultavalor()));
						documentoVO_CONF.setTaxaboletovalor(addValor(documentoVO_CONF.getTaxaboletovalor(), documentoVO.getTaxaboletovalor()));
						documentoVO_CONF.setTermovalor(addValor(documentoVO_CONF.getTermovalor(), documentoVO.getTermovalor()));
						documentoVO_CONF.setTaxamovimentovalor(addValor(documentoVO_CONF.getTaxamovimentovalor(), documentoVO.getTaxamovimentovalor()));
						
						documentoVO_CONF.setDesagiovalorcalculado(addValor(documentoVO_CONF.getDesagiovalorcalculado(), documentoVO.getDesagiovalorcalculado()));
						documentoVO_CONF.setDescontovalorcalculado(addValor(documentoVO_CONF.getDescontovalorcalculado(), documentoVO.getDescontovalorcalculado()));
						documentoVO_CONF.setJurosvalorcalculado(addValor(documentoVO_CONF.getJurosvalorcalculado(), documentoVO.getJurosvalorcalculado()));
						documentoVO_CONF.setMultavalorcalculado(addValor(documentoVO_CONF.getMultavalorcalculado(), documentoVO.getMultavalorcalculado()));
						documentoVO_CONF.setTaxaboletovalorcalculado(addValor(documentoVO_CONF.getTaxaboletovalorcalculado(), documentoVO.getTaxaboletovalorcalculado()));
						documentoVO_CONF.setTermovalorcalculado(addValor(documentoVO_CONF.getTermovalorcalculado(), documentoVO.getTermovalorcalculado()));
						documentoVO_CONF.setTaxamovimentovalorcalculado(addValor(documentoVO_CONF.getTaxamovimentovalorcalculado(), documentoVO.getTaxamovimentovalorcalculado()));
						
						documentoVO_CONF.setValoroutrasentidades(addValor(documentoVO_CONF.getValoroutrasentidades(), documentoVO.getValoroutrasentidades()));
						documentoVO_CONF.setValorreceita(addValor(documentoVO_CONF.getValorreceita(), documentoVO.getValorreceita()));
						
						achou = true;
						break;
					}
				}
				
				if(!achou){
					listaDocumentoVO_NOVO.add(documentoVO);
				}
			}
			
			for (DocumentoVO documentoVO : listaDocumentoVO_NOVO) {
				List<Integer> idsAgrupamento = documentoVO.getIdsAgrupamento();
				if(idsAgrupamento != null && idsAgrupamento.size() > 0){
					Integer identificador = documentoService.getNextSequenceDocumento();
					
					BancoConfiguracaoRemessaAgrupamento agrupamento = new BancoConfiguracaoRemessaAgrupamento();
					agrupamento.setDocumento(new Documento(documentoVO.getCddocumento()));
					agrupamento.setIdentificador(identificador);
					bancoConfiguracaoRemessaAgrupamentoService.saveOrUpdate(agrupamento);
					
					for (Integer cddocumento : idsAgrupamento) {
						agrupamento = new BancoConfiguracaoRemessaAgrupamento();
						agrupamento.setDocumento(new Documento(cddocumento));
						agrupamento.setIdentificador(identificador);
						bancoConfiguracaoRemessaAgrupamentoService.saveOrUpdate(agrupamento);
					}
					
					documentoVO.setCddocumento(identificador);
				}
			}
			
		} else {
			listaDocumentoVO_NOVO.addAll(listaDocumentoVO_ANTIGO);
		}
		
		return listaDocumentoVO_NOVO;
	}
	
	private Money addValor(Money valor1, Money valor2) {
		if(valor1 == null && valor2 == null) return null;
		
		if(valor1 == null) valor1 = new Money();
		if(valor2 == null) valor2 = new Money();
		return valor1.add(valor2);
	}
	
	
	public List<Documento> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto,Date dtInicio,Date dtFim, Empresa empresa){
		return documentoDAO.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/09/2014
	 */
	private Integer getNextSequenceDocumento() {
		return documentoDAO.getNextSequenceDocumento();
	}
	
	/**
	 * M�todo que verifica os documentos cancelados para desmcar parcela cobrada no contrato
	 *
	 * @param ids
	 * @author Luiz Fernando
	 */
	public void desmarcarParcelacobradaOrigemContrato(String ids) {
		if(ids != null && !"".equals(ids)){
			List<Documento> listaDocumentos = this.findforDesmarcarParcelacobradaOrigemContrato(ids);
			if(listaDocumentos != null && !listaDocumentos.isEmpty()){
				for(Documento d : listaDocumentos){
					if(d.getDtvencimento() != null && d.getListaDocumentoOrigem() != null && !d.getListaDocumentoOrigem().isEmpty()){
						for(Documentoorigem doco : d.getListaDocumentoOrigem()){
							if(doco.getContrato() != null && doco.getContrato().getListaContratoparcela() != null && 
									!doco.getContrato().getListaContratoparcela().isEmpty()){
								for(Contratoparcela cp : doco.getContrato().getListaContratoparcela()){
									if(cp.getCdcontratoparcela() != null && cp.getDtvencimento() != null && SinedDateUtils.equalsIgnoreHour(cp.getDtvencimento(), d.getDtvencimento())){
										contratoparcelaService.updateDesmarcarParcelacobrada(cp, false);
									}
								}
							}
						}
					}
				}
			}
		}		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findforDesmarcarParcelacobradaOrigemContrato(String ids)
	 *
	 * @param ids
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findforDesmarcarParcelacobradaOrigemContrato(String ids) {
		return documentoDAO.findforDesmarcarParcelacobradaOrigemContrato(ids);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findByDespesaviagem(Despesaviagem despesaviagem, Documentoclasse documentoclasse)
	 *
	 * @param despesaviagem
	 * @param documentoclasse
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public List<Documento> findByDespesaviagem(Despesaviagem despesaviagem, Documentoclasse documentoclasse) {
		return documentoDAO.findByDespesaviagem(despesaviagem, documentoclasse);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findForMovimentacaocontabil(String whereInDocumento)
	 *
	 * @param whereInDocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForMovimentacaocontabil(String whereInDocumento) {
		return documentoDAO.findForMovimentacaocontabil(whereInDocumento);
	}
	
	/**
	 * Faz a valida��o para o cancelamento de documento.
	 *
	 * @param request
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 06/12/2012
	 */
	public void validacaoCancelamentoDocumento(WebRequestContext request, String whereIn, boolean webservice) {
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			throw new SinedException("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
		}
		
		String paramPemitirCancelarOrigemVenda = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_CANCELAR_CONTARECEBER);
		boolean permiteCancelarOrigemVenda = paramPemitirCancelarOrigemVenda != null && paramPemitirCancelarOrigemVenda.trim().toUpperCase().equals("TRUE");
		
		List<Documento> listaConta = contapagarService.findContas(whereIn);
		for (Documento documento : listaConta) {
			Documentoorigem documentoorigem = documentoorigemService.findDocumentoOrigemByDocumento(documento);
//			boolean documentoOrigundoDeNegociacao = !vdocumentonegociadoService.findByDocumentosnegociados(documento.getCddocumento().toString()).isEmpty();
			if(!permiteCancelarOrigemVenda &&
//					!documentoOrigundoDeNegociacao &&
					documentoorigem != null && 
					documentoorigem.getVenda() != null && 
					documentoorigem.getVenda().getVendasituacao() != null && 
					!documentoorigem.getVenda().getVendasituacao().equals(Vendasituacao.CANCELADA) && 
					!materialdevolucaoService.existDevolucaoByVenda(documentoorigem.getVenda())){
				throw new SinedException("Para cancelar esta conta � preciso cancelar a venda ou registrar uma devolu��o da venda.");
			} else if (!documento.getDocumentoacao().equals(Documentoacao.PREVISTA)) {
				if(webservice){
					if(!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
						throw new SinedException("Registro(s) com situa��o diferente de 'prevista' e 'definitiva'.");
					}
				} else {
					throw new SinedException("Registro(s) com situa��o diferente de 'prevista'.");
				}
			} /*else if(notaDocumentoService.validaEstornoContaWebserviceTrue(request, documento)){
				throw new SinedException();
			}*/
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findByVenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/12/2012
	 */
	public List<Documento> findByVenda(Venda venda) {
		return documentoDAO.findByVenda(venda);
	}
	
	public List<Documento> findVendaDiferenteCanceladaNegociada(Venda venda) {
		return documentoDAO.findVendaDiferenteCanceladaNegociada(venda);
	}
	
	/**
	 * Adiciona informa��es da ordem de compra no documento
	 *
	 * @param documento
	 * @author Luiz Fernando
	 */
	public void adicionaInformacoesByEntrega(Documento documento) {
		List<Ordemcompra> listaOrdemcompra;
		StringBuilder descricaooc = new StringBuilder();
		Money valortotalordemcompra = new Money();
		if(documento.getCddocumento() != null){
			listaOrdemcompra = ordemcompraService.findByDocumento(documento);
			if(listaOrdemcompra != null && !listaOrdemcompra.isEmpty()){
				for(Ordemcompra oc : listaOrdemcompra){
					if(!"".equals(descricaooc.toString())) descricaooc.append("\n");
					valortotalordemcompra = new Money(oc.getValorTotalOrdemCompra());
					descricaooc.append("O.C.: ").append(oc.getCdordemcompra());
					descricaooc.append(" | Valor: R$ ").append(valortotalordemcompra);
					descricaooc.append(" | Diferen�a: R$ ").append(valortotalordemcompra.subtract(documento.getValor()));
				}
			}
		}		
		documento.setDescricaoOrdemcompraEntrega(!"".equals(descricaooc.toString()) ? descricaooc.toString() : null);
	}

	/**
	 * M�todo que atualiza dados do documento com informa��es da nota
	 *
	 * @param arquivonfnota
	 * @param chaveacesso
	 * @author Luiz Fernando
	 */
	public void updateInfDocumentoByNota(Arquivonfnota arquivonfnota, String chaveacesso) {
		try {
			if(arquivonfnota != null && arquivonfnota.getNota() != null && arquivonfnota.getNota().getCdNota() != null){
				String msg1 = null;
				String msg2 = null;
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
					List<NotaVenda> listaNotavenda = notaVendaService.findByNota(arquivonfnota.getNota());
					if(listaNotavenda != null && !listaNotavenda.isEmpty()){
						for(NotaVenda notavenda : listaNotavenda){
							if(notavenda.getVenda() != null && notavenda.getVenda().getCdvenda() != null){
								List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(notavenda.getVenda());
								for (Documentoorigem documentoorigem : listadocumentoorigem) {
									Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
									msg1 = documento.getMensagem1();
									msg2 = documento.getMensagem2();
									
									String numero = null;
									String chave = null;
									
									if(arquivonfnota.getNota().getNumero() != null && !"".equals(arquivonfnota.getNota().getNumero())){
										numero = "NF: " +arquivonfnota.getNota().getNumero();
									}
									
									if(chaveacesso != null && !"".equals(chaveacesso)){
										chave = "Chave: " + chaveacesso;
									}
									
									if(numero != null && chave != null){
										msg1 = numero + " - " + chave;
									} else if(numero != null){
										msg1 = numero;
									} else if(chave != null){
										msg1 = chave;
									}
									
									if(msg1 != null || msg2 != null){
										documentoService.updateMesangem1And2NFDocumentoVenda(documento, msg1, msg2);
									}
								}
							}
						}
					}
				
					List<Documento> listaDocumento = this.findByNota(arquivonfnota.getNota());
					if(listaDocumento != null && !listaDocumento.isEmpty()){
						for(Documento documento : listaDocumento){
							msg1 = documento.getMensagem1();
							msg2 = documento.getMensagem2();
							
							String numero = null;
							String chave = null;
							
							if(arquivonfnota.getNota().getNumero() != null && !"".equals(arquivonfnota.getNota().getNumero())){
								numero = "NF: " +arquivonfnota.getNota().getNumero();
							}
							
							if(chaveacesso != null && !"".equals(chaveacesso)){
								chave = "Chave: " + chaveacesso;
							}
							
							if(numero != null && chave != null){
								msg1 = numero + " - " + chave;
							} else if(numero != null){
								msg1 = numero;
							} else if(chave != null){
								msg1 = chave;
							}
							
							if(msg1 != null || msg2 != null){
								documentoService.updateMesangem1And2NFDocumentoVenda(documento, msg1, msg2);
							}
						}
					}
				}
			}		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForQtdeParcelasComissionamento(Documento documento)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isComissionamentoValorbrutoConsiderandodiferencavalorpago(String whereIn) {
		return documentoDAO.isComissionamentoValorbrutoConsiderandodiferencavalorpago(whereIn);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForQtdeParcelasComissionamento(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForQtdeParcelasComissionamento(Documento documento) {
		return documentoDAO.findForQtdeParcelasComissionamento(documento);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForDocumentoNegociadoWithNota(Documento documento, Contrato contrato)
	 *
	 * @param documento
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForDocumentoNegociadoWithNota(Documento documento, Contrato contrato) {
		return documentoDAO.findForDocumentoNegociadoWithNota(documento, contrato);
	}
	
	/**
	 * M�todo para carregar os dados da pessoa para o documento
	 *
	 * @param documento
	 * @author Luiz Fernando
	 */
	public void carregaDadosPessoaDocumentoeForEntrada(Documento documento){
		if(documento != null && documento.getCddocumento() != null){
			if(documento.getEmpresa() != null && documento.getEmpresa().getCdpessoa() != null){
				documento.setEmpresa(empresaService.loadForDocumento(documento.getEmpresa()));
			}
			if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
				documento.setPessoa(pessoaService.load(documento.getPessoa(), "pessoa.cdpessoa, pessoa.nome"));
			}
		}
	}
	
	public Documento criarPagamentoWebservice(CriarPagamentoBean bean){
		if(bean.getCdcliente() == null && bean.getCdcolaborador() == null && bean.getCdfornecedor() == null){
			throw new SinedException("Cliente/Colaborador/Fornecedor n�o encontrado.");
		}
		
		Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
		Date dtemissao = new Date(System.currentTimeMillis());
		Date dtvencimento = SinedDateUtils.addDiasUteisData(dtemissao, numDiasVencimento);
		
		Empresa empresa = null;
		if(bean.getCdempresa() != null){
			empresa = empresaService.loadWithEndereco(new Empresa(bean.getCdempresa()));
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
		}
		
		Documento documento = new Documento();
		if(bean.getCdcliente() != null){
			documento.setPessoa(new Pessoa(bean.getCdcliente()));
			documento.setTipopagamento(Tipopagamento.CLIENTE);
		} else if(bean.getCdcolaborador() != null){
			documento.setPessoa(new Pessoa(bean.getCdcolaborador()));
			documento.setTipopagamento(Tipopagamento.COLABORADOR);
		} else if(bean.getCdfornecedor() != null){
			documento.setPessoa(new Pessoa(bean.getCdfornecedor()));
			documento.setTipopagamento(Tipopagamento.FORNECEDOR);
		}
		
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDocumentotipo(new Documentotipo(bean.getCddocumentotipo()));
		documento.setDescricao(bean.getDescricao());
		documento.setDtemissao(dtemissao);
		documento.setDtvencimento(dtvencimento);
		documento.setValor(bean.getValor());
		documento.setEmpresa(empresa);
		documento.setObservacaoHistorico("Conta a pagar criada via webservice.");
		
		//RATEIO
		List<Rateioitem> listaItens = new ArrayList<Rateioitem>();
		
		Rateioitem rateioitem = new Rateioitem();
		if(bean.getCdprojeto() != null){
			rateioitem.setProjeto(new Projeto(bean.getCdprojeto()));
		}
		rateioitem.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
		rateioitem.setContagerencial(new Contagerencial(bean.getCdcontagerencial()));
		rateioitem.setPercentual(100d);
		rateioitem.setValor(bean.getValor());
		listaItens.add(rateioitem);
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listaItens);
		documento.setRateio(rateio);
		
		// HIST�RICO
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		listaDocumentohistorico.add(documentohistorico);
		
		documento.setListaDocumentohistorico(listaDocumentohistorico);
		
		documentoService.saveOrUpdate(documento);
		
		return documento;
	}
	
	/**
	 * M�todo que cria o faturamento (Conta a receber/nota) atrav�s de webservice.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public Documento criarFaturaWebservice(CriarFaturaBean bean) {
		if(bean.getCdcliente() == null){
			throw new SinedException("Cliente n�o encontrado.");
		}
		
		if(bean.getMensagem1() != null && bean.getMensagem1().length() > 80){
			throw new SinedException("O tamanho da Mensagem 1 n�o pode ultrapassar 80 caracteres.");
		}
		if(bean.getMensagem2() != null && bean.getMensagem2().length() > 80){
			throw new SinedException("O tamanho da Mensagem 2 n�o pode ultrapassar 80 caracteres.");
		}
		if(bean.getMensagem3() != null && bean.getMensagem3().length() > 80){
			throw new SinedException("O tamanho da Mensagem 3 n�o pode ultrapassar 80 caracteres.");
		}
		
		/**
		 * valida��es de rateio
		 */
		boolean usaPropriedadesPrincipaisRateio = bean.getCdcentrocusto() != null || bean.getCdcontagerencial() != null || bean.getCdprojeto() != null;
		boolean usaListaRateio = bean.getRateioitem() != null && bean.getRateioitem().size() > 0;
		if(usaPropriedadesPrincipaisRateio && usaListaRateio){
			throw new SinedException("S� � permitido usar as propriedades principais (cdcentrocusto, cdcontagerencial e [cdprojeto]) para criar um item de rateio OU usar a lista rateioitem para criar mais de um rateio. N�o � permitido usar as duas formas concomitantemente.");
		}else if(!usaPropriedadesPrincipaisRateio && !usaListaRateio){
			throw new SinedException("� necess�rio enviar as propriedades principais (cdcentrocusto, cdcontagerencial e [cdprojeto]) para criar um item de rateio OU usar a lista rateioitem para criar mais de um rateio.");
		}
		
		if(usaListaRateio){
			StringBuilder msgErroRateio = new StringBuilder();
			Double totalPercentual = 0D;
			List<String> listaCentrocustoContagerencial = new ArrayList<String>();
			
			for(int i = 0; i < bean.getRateioitem().size(); i++){
				RateioitemBean ri = bean.getRateioitem().get(i);
				if(ri.getCdcentrocusto()==null) {
					msgErroRateio.append("Rateioitem[").append(i).append("].cdcentrocusto � obrigat�rio.\n");
				}
				
				if(ri.getCdcontagerencial()==null) {
					msgErroRateio.append("Rateioitem[").append(i).append("].cdcontagerencial � obrigat�rio.\n");
				}
				//validando itens com centro de custo/conta gerencial duplicados.
				String centrocustoContagerencial = ri.getCdcentrocusto()+"-"+ ri.getCdcontagerencial();
				if(listaCentrocustoContagerencial.contains(centrocustoContagerencial)){
					msgErroRateio.append("Voc� n�o pode enviar dois itens de rateio com o mesmo centro de custo e conta gerencial.\n");
				}
				
				//percentual
				if(ri.getPercentual()==null) {
					msgErroRateio.append("Rateioitem[").append(i).append("].percentual � obrigat�rio.\n");
				}else if(ri.getPercentual()<=0D){
					msgErroRateio.append("Rateioitem[").append(i).append("].percentual n�o pode ser menor ou igual a zero.");
				}else{
					totalPercentual += ri.getPercentual();
				}
			}
			
			if(!totalPercentual.equals(100D)){
				msgErroRateio.append("Soma de percentual de rateioitem � diferente de 100% (").append(totalPercentual).append(")");
			}
			
			if(msgErroRateio.length()>0){
				throw new SinedException(msgErroRateio.toString());
			}
		}
		
		Cliente cliente = clienteService.loadForTributacao(new Cliente(bean.getCdcliente()));
		
		Date dtemissao = new Date(System.currentTimeMillis());
		Date dtvencimento = bean.getDtvencimento();
		if(dtvencimento == null){
			Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
			dtvencimento = SinedDateUtils.addDiasUteisData(dtemissao, numDiasVencimento);
		}
		
		Empresa empresa = null;
		if(bean.getCdempresa() != null){
			empresa = empresaService.loadWithEndereco(new Empresa(bean.getCdempresa()));
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
		}
		
		
		Endereco enderecoPessoa = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			enderecoPessoa = enderecoService.getEnderecoPrincipalPessoa(empresa);
		}
		Conta conta = contaService.carregaContaComMensagem(new Conta(bean.getCdconta()), null);
		Contacarteira contacarteira = conta.getContacarteira();
		
		Documento documento = new Documento();
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDocumentotipo(new Documentotipo(bean.getCddocumentotipo()));
		documento.setDescricao(bean.getDescricao());
		documento.setPessoa(cliente);
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setDtemissao(dtemissao);
		documento.setDtvencimento(dtvencimento);
		documento.setValor(bean.getValor());
		documento.setEmpresa(empresa);
		documento.setConta(conta);
		documento.setObservacaoHistorico("Conta a receber criada via webservice.");
		documento.setMensagem1(bean.getMensagem1());
		documento.setMensagem2(bean.getMensagem2());
		documento.setMensagem3(bean.getMensagem3());
		
		if(contacarteira != null && contacarteira.getCdcontacarteira() != null){
			documento.setMensagem4(contacarteira.getMsgboleto1());
			documento.setMensagem5(contacarteira.getMsgboleto2());
			
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(contacarteira);
		}
		
		// Endere�o
		Endereco endereco = null;
		if(bean.getCdendereco() != null){
			endereco = new Endereco(bean.getCdendereco());
			documento.setEndereco(endereco);
		} else {
			List<Endereco> listaEnderecos = new ArrayList<Endereco>();
			listaEnderecos = contareceberService.carregaEnderecosAPartirTipoPagamento(documento);
			endereco = contareceberService.enderecoSugerido(listaEnderecos);
			documento.setEndereco(endereco);
		}
		
		//RATEIO
		List<Rateioitem> listaItens = new ArrayList<Rateioitem>();
		if(usaPropriedadesPrincipaisRateio){
			Rateioitem rateioitem = new Rateioitem();
			if(bean.getCdprojeto() != null){
				rateioitem.setProjeto(new Projeto(bean.getCdprojeto()));
			}
			rateioitem.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
			rateioitem.setContagerencial(new Contagerencial(bean.getCdcontagerencial()));
			rateioitem.setPercentual(100d);
			rateioitem.setValor(bean.getValor());
			listaItens.add(rateioitem);
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaItens);
			documento.setRateio(rateio);
		}else{//usa rateioitem

			for(RateioitemBean rateioitemBean : bean.getRateioitem()){
				Rateioitem rateioitem = new Rateioitem();
				if(rateioitemBean.getCdprojeto() != null){
					rateioitem.setProjeto(new Projeto(rateioitemBean.getCdprojeto()));
				}
				rateioitem.setCentrocusto(new Centrocusto(rateioitemBean.getCdcentrocusto()));
				rateioitem.setContagerencial(new Contagerencial(rateioitemBean.getCdcontagerencial()));
				rateioitem.setPercentual(rateioitemBean.getPercentual());
				listaItens.add(rateioitem);
			}	
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaItens);
			documento.setRateio(rateio);
			rateioService.atualizaValorRateio(rateio, bean.getValor());
		}
		// HIST�RICO
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		listaDocumentohistorico.add(documentohistorico);
		
		documento.setListaDocumentohistorico(listaDocumentohistorico);
		
		conta = contaService.loadWithTaxa(conta);
		if(conta != null && 
				conta.getTaxa() != null && 
				conta.getTaxa().getListaTaxaitem() != null &&
				conta.getTaxa().getListaTaxaitem().size() > 0){
			
			Set<Taxaitem> listaTaxaitem = contacorrenteService.getTaxas(conta, documento.getDtvencimento(), enderecoPessoa);
			if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
				StringBuilder msg2 = new StringBuilder();
				StringBuilder msg3 = new StringBuilder();
				String preposicao = "";
				String msg = "";
				
				Taxa taxa = new Taxa();
				taxa.setListaTaxaitem(listaTaxaitem);
				documento.setTaxa(taxa);
				
				if(documento.getMensagem2() != null)
					msg2.append(documento.getMensagem2());
				if(documento.getMensagem3() != null)
					msg3.append(documento.getMensagem3());
				
				for(Taxaitem txitem : listaTaxaitem){
					if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
						if(tipotaxaService.isApos(txitem.getTipotaxa())) {
							preposicao = " ap�s ";
						}else {
							preposicao = " at� ";
						}
						msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + SinedDateUtils.toString(txitem.getDtlimite()) : "") + ". ";
						if((msg2.length() + msg.length()) <= 80){
							msg2.append(msg);
						}else if((msg3.length() + msg.length()) <= 80){
							msg3.append(msg);
						}
					}
				}
				if(!msg2.toString().equals("")){
					documento.setMensagem2(msg2.toString());
				}
				if(!msg3.toString().equals("")){
					documento.setMensagem3(msg3.toString());
				}
			}
			
		}
		
		if(bean.getCriarnota() != null && bean.getCriarnota()){
			boolean situacaoEmitida = bean.getSituacaonota() != null && bean.getSituacaonota().equals(2);
			
			NotaFiscalServico nota = new NotaFiscalServico();
			if(bean.getCdgrupotributacao() != null){
				nota.setGrupotributacao(grupotributacaoService.loadForEntrada(new Grupotributacao(bean.getCdgrupotributacao())));
			}
			nota.setCliente(cliente);
			nota.setEnderecoCliente(endereco);
			nota.setBasecalculo(bean.getValor());
			nota.setBasecalculocofins(bean.getValor());
			nota.setBasecalculocsll(bean.getValor());
			nota.setBasecalculoicms(bean.getValor());
			nota.setBasecalculoinss(bean.getValor());
			nota.setBasecalculoir(bean.getValor());
			nota.setBasecalculoiss(bean.getValor());
			nota.setBasecalculopis(bean.getValor());
			
			Integer cdprojeto = null;
			if(usaPropriedadesPrincipaisRateio){
				cdprojeto = bean.getCdprojeto();
			} else {
				for(RateioitemBean rateioitemBean : bean.getRateioitem()){
					if(cdprojeto == null){
						cdprojeto = rateioitemBean.getCdprojeto();
					} else if(rateioitemBean.getCdprojeto() != null && !cdprojeto.equals(rateioitemBean.getCdprojeto())){
						cdprojeto = null;
						break;
					}
				}	
			}
			
			if(cdprojeto != null){
				nota.setProjeto(new Projeto(cdprojeto));
			}
			
			nota.setDtVencimento(dtvencimento);
			if(situacaoEmitida){
				nota.setNotaStatus(NotaStatus.EMITIDA);
			} else {
				nota.setNotaStatus(NotaStatus.EM_ESPERA);
			}
			nota.setDtEmissao(SinedDateUtils.currentDate());
			nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
			nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			
			notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, nota);
			
			if(empresa != null && empresa.getCdpessoa() != null){
				nota.setEmpresa(empresa);
				if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
					if(empresa.getEndereco().getMunicipio() != null)
						nota.setMunicipioissqn(empresa.getEndereco().getMunicipio());
				}
			}
			
			NotaFiscalServicoItem item = new NotaFiscalServicoItem();
			item.setDescricao(bean.getDescricao()); 
			item.setQtde(1d);
			item.setPrecoUnitario(bean.getValor().getValue().doubleValue());
			
			List<NotaFiscalServicoItem> listaIt = new ArrayList<NotaFiscalServicoItem>();
			listaIt.add(item);
			nota.setListaItens(listaIt);
			
			List<FaturamentoConjuntoBean> listaFaturamentoConjunto = new ArrayList<FaturamentoConjuntoBean>();
			
			FaturamentoConjuntoBean faturamentoConjuntoBean = new FaturamentoConjuntoBean();
			faturamentoConjuntoBean.setDocumento(documento);
			faturamentoConjuntoBean.setNotaFiscalServico(nota);
			
			listaFaturamentoConjunto.add(faturamentoConjuntoBean);
			
			contratoService.salvaListaFaturamentoConjunto(listaFaturamentoConjunto, !situacaoEmitida);

			if(documento.getListaDocumentohistorico() != null && !documento.getListaDocumentohistorico().isEmpty()){
				for(Documentohistorico dh : documento.getListaDocumentohistorico()){
					dh.setObservacao(dh.getObservacao()+" Vinculado a nota <a href='javascript:visualizarNotaFiscalServico("+nota.getCdNota()+")'>"+nota.getCdNota()+"</a>."); 
					documentohistoricoService.saveOrUpdate(dh);
					break;
				}
				
			}
			
		} else {
			documentoService.saveOrUpdate(documento);
		}
		return documento;
	}
	
	/**
	 * Baixa o pagamento atrav�s do webservice 
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public void baixarPagamentoWebservice(BaixarPagamentoBean bean) {
		Integer cddocumento = bean.getCddocumento();
		
		if(cddocumento == null && bean.getId() != null && !bean.getId().equals("")){
			TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
			if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
				List<Venda> listaVenda = vendaService.findByEmpresaIdentificador(bean.getEmpresa_cnpj(), bean.getId());
				
				if(listaVenda == null || listaVenda.size() == 0){
					throw new SinedException("Nenhuma venda encontrada com o identificador informado.");
				} else if(listaVenda.size() > 1){
					throw new SinedException("Foi encontrada mais de uma venda com o identificador informado.");
				}
				
				List<Documento> listaDocumento = contareceberService.findByVendas(CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ","));
				if(listaDocumento == null || listaDocumento.size() == 0){
					throw new SinedException("Nenhum documento encontrado para a venda.");
				}
				if(listaDocumento.size() > 1){
					throw new SinedException("Foi encontrada mais de um documento com o identificador informado.");
				}
				
				cddocumento = listaDocumento.get(0).getCddocumento();
			} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA)){
				List<Pedidovenda> listaPedidovenda = pedidovendaService.findByEmpresaIdentificador(bean.getEmpresa_cnpj(), bean.getId());
				
				if(listaPedidovenda == null || listaPedidovenda.size() == 0){
					throw new SinedException("Nenhum pedido encontrada com o identificador informado.");
				} else if(listaPedidovenda.size() > 1){
					throw new SinedException("Foi encontrada mais de um pedido com o identificador informado.");
				}
				
				List<Documento> listaDocumento = contareceberService.findByPedidovenda(CollectionsUtil.listAndConcatenate(listaPedidovenda, "cdpedidovenda", ","));
				if(listaDocumento == null || listaDocumento.size() == 0){
					throw new SinedException("Nenhum documento encontrado para o pedido de venda.");
				}
				if(listaDocumento.size() > 1){
					throw new SinedException("Foi encontrada mais de um documento com o identificador informado.");
				}
				
				cddocumento = listaDocumento.get(0).getCddocumento();
			} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)){
				throw new SinedException("N�o existe documento vinculado ao pedido de venda de bonifica��o.");
			}
		}
		
		Date dtpagamento = bean.getDtpagamento();
		Double valorDbl = bean.getValor();
		Integer cdconta = bean.getCdconta();
		
		Boolean incluirTaxaVenda = bean.getIncluir_taxa_venda();
		Boolean considerarValorAtual = bean.getConsiderar_valor_atual();
		
		if(cddocumento == null){
			throw new SinedException("O campo cddocumento � obrigat�rio.");
		}
		
		if(dtpagamento == null){
			throw new SinedException("O campo dtpagamento � obrigat�rio.");
		}
		
		if(valorDbl == null && !considerarValorAtual){
			throw new SinedException("O campo valor � obrigat�rio.");
		}
		
		if(cdconta == null){
			throw new SinedException("O campo cdconta � obrigat�rio.");
		}
		
		Conta conta = new Conta(cdconta);
		conta = contaService.loadForEntrada(conta);
		
		if(conta == null){
			throw new SinedException("Conta n�o encontrada.");
		}
		
		Documento documento = new Documento(cddocumento);
		documento = documentoService.loadForEntrada(documento);	
		
		if(documento == null){
			throw new SinedException("Documento n�o encontrado.");
		}
		
		Formapagamento formapagamento = Formapagamento.CREDITOCONTACORRENTE;
		if(bean.getCddocumentotipo() != null){
			Documentotipo documentotipo = new Documentotipo(bean.getCddocumentotipo());
			documentotipo = documentotipoService.load(documentotipo);
			
			if(documentotipo != null){
				documentoService.updateDocumentotipo(documento, documentotipo);
				documento.setDocumentotipo(documentotipo);
				if(documentotipo.getFormapagamentocredito() != null){
					formapagamento = formapagamentoService.load(documentotipo.getFormapagamentocredito());
				}
			}
		}
		
		if(incluirTaxaVenda && documento.getDocumentotipo() != null){
			Taxa taxa = taxaService.findByDocumento(documento);
			if(taxa == null){
				taxa = new Taxa();
				taxaService.saveOrUpdate(taxa);
				documento.setTaxa(taxa);
				documentoService.updateTaxa(documento);
			}
			
			Documentotipo documentotipo = documentotipoService.load(documento.getDocumentotipo());
			Double taxaVenda = documentotipo.getTaxavenda();
			
			if(taxaVenda != null && taxaVenda > 0.0){
				try{
					Taxaitem taxaitem = new Taxaitem();
					taxaitem.setTipotaxa(Tipotaxa.TAXAMOVIMENTO);
					taxaitem.setPercentual(documentotipo.getPercentual() != null ? documentotipo.getPercentual() : true);
					taxaitem.setValor(new Money(taxaVenda));
					taxaitem.setDtlimite(documento.getDtemissao());
					taxaitem.setTaxa(taxa);
					
					taxaitemService.saveOrUpdate(taxaitem);
					
					documentoService.callProcedureAtualizaDocumento(documento);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		Money valor = new Money();
		if(considerarValorAtual){
			Documento documento_aux = documentoService.loadWithValoratual(documento);
			if(documento_aux != null && 
					documento_aux.getAux_documento() != null && 
					documento_aux.getAux_documento().getValoratual() != null){
				valor = documento_aux.getAux_documento().getValoratual();
			}
		} else {
			valor = new Money(valorDbl);
		}
		
		BaixarContaBean baixarContaBean = new BaixarContaBean();
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		Rateio rateio;
		
		if(documento.getValor().getValue().doubleValue() == 0d){
			rateio = contareceberService.atualizaValorRateio(documento, valor);
			documento = contareceberService.loadForEntradaWithDadosPessoa(new Documento(cddocumento));
		} else {
			rateio = rateioService.findByDocumento(documento);
			rateioService.atualizaValorRateio(rateio, valor);
		}
		
		documento.setValoratual(valor);
		documento.setRateio(rateio);
		
		listaDocumento.add(documento);
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		
		baixarContaBean.setListaDocumento(listaDocumento);
		baixarContaBean.setRateio(documento.getRateio());
		baixarContaBean.setDtpagamento(dtpagamento);
		baixarContaBean.setDtcredito(dtpagamento);
		baixarContaBean.setFormapagamento(formapagamento);
		baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
		baixarContaBean.setVinculo(conta);
		baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		baixarContaBean.setChecknum(sdf.format(dtpagamento));
		baixarContaBean.setFromWebservice(true);
		
		Movimentacao movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
		if(baixarContaBean.getIsContaBaixada() == null || !baixarContaBean.getIsContaBaixada()){
			notaDocumentoService.liberaNotaDocumentoWebservice(null, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//			contareceberService.incrementaNumDocumento(documento);
			documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
			
			documentoService.callProcedureAtualizaDocumento(documento);
			movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
			movimentacaoService.updateHistoricoWithDescricao(movimentacao);
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @param documentotipo
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public void updateDocumentotipo(Documento documento, Documentotipo documentotipo) {
		documentoDAO.updateDocumentotipo(documento, documentotipo);
	}
	
	/**
	 * Cria o faturamento e o cliente juntos no webservice
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public Documento criarFaturaClienteWebservice(CriarFaturaClienteBean bean) {
		InserirClienteBean clienteBean = new InserirClienteBean();
		
		clienteBean.setNome(bean.getCliente_nome());
		clienteBean.setEmail(bean.getCliente_email());
		clienteBean.setCdcategoria(bean.getCliente_cdcategoria());
		clienteBean.setCpf(bean.getCliente_cpf());
		clienteBean.setSexo(bean.getCliente_sexo());
		clienteBean.setRazaosocial(bean.getCliente_razaosocial());
		clienteBean.setCnpj(bean.getCliente_cnpj());
		clienteBean.setInscricaomunicipal(bean.getCliente_inscricaomunicipal());
		clienteBean.setInscricaoestadual(bean.getCliente_inscricaoestadual());
		clienteBean.setSite(bean.getCliente_site());
		clienteBean.setNomecontato(bean.getCliente_nomecontato());
		clienteBean.setTelefonecontato(bean.getCliente_telefonecontato());
		clienteBean.setCelularcontato(bean.getCliente_celularcontato());
		clienteBean.setTelefone(bean.getCliente_telefone());
		clienteBean.setCelular(bean.getCliente_celular());
		clienteBean.setEnderecologradouro(bean.getCliente_enderecologradouro());
		clienteBean.setEndereconumero(bean.getCliente_endereconumero());
		clienteBean.setEnderecocomplemento(bean.getCliente_enderecocomplemento());
		clienteBean.setEnderecobairro(bean.getCliente_enderecobairro());
		clienteBean.setEnderecocep(bean.getCliente_enderecocep());
		clienteBean.setEnderecomunicipio(bean.getCliente_enderecomunicipio());
		clienteBean.setEnderecouf(bean.getCliente_enderecouf());
		
		Integer cdcliente = clienteService.criarClienteWebservice(clienteBean);
		bean.setCdcliente(cdcliente);
		bean.setCdendereco(clienteBean.getCdendereco());
		
		return this.criarFaturaWebservice(bean);
	}
	
	/**
	 * Verifica o status do documento para o webservice.
	 *
	 * @param cddocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public String verificaStatusPagamentoWebservice(Integer cddocumento) {
		String cache = CacheWebserviceUtil.getConsultaPagamento(cddocumento);
		if(cache != null){
			return cache;
		}
		
		Documento documento = new Documento(cddocumento);
		documento = documentoService.load(documento, "documento.cddocumento, documento.documentoacao, documento.dtvencimento");	
		
		if(documento == null){
			throw new SinedException("Documento n�o encontrado");
		}
		
		String statusdocumento = "0";
		if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA))
			statusdocumento = "1";
		else if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA))
			statusdocumento = "2";
		else if(documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
			List<Vdocumentonegociado> lista = vdocumentonegociadoService.findByDocumentos(cddocumento.toString());
			boolean haveBaixada = false;
			boolean haveCancelada = false;
			boolean haveEmaberto = false;
			
			for (Vdocumentonegociado vdocumentonegociado : lista) {
				if(vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.BAIXADA.getCddocumentoacao())){
					haveBaixada = true;
				} else if(vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.CANCELADA.getCddocumentoacao())){
					haveCancelada = true;
				} else if(!vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.NEGOCIADA.getCddocumentoacao())){
					haveEmaberto = true;
				}
			}
			
			if(haveBaixada && !haveEmaberto){
				statusdocumento = "1";
			} else if(haveCancelada && !haveEmaberto){
				statusdocumento = "2";
			}
		}
		
		CacheWebserviceUtil.putConsultaPagamento(cddocumento, statusdocumento);
		return statusdocumento;
	}
	
	/**
	 * 
	 * @param listaDocumento
	 * @author Thiago Clemente
	 * 
	 */
	public void gerarHistoricoBoletoPrimeiraVez(List<Documento> listaDocumento){
		for (Documento documento: listaDocumento){
			if (!Boolean.TRUE.equals(documento.getBoletogerado())){
				Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
				documentohistorico.setObservacao("Boleto gerado pela primeira vez");
				documentohistorico.setDocumentoacao(Documentoacao.BOLETO_GERADO);
				documentohistoricoService.saveOrUpdate(documentohistorico);
				documentoDAO.updateBoletoGerado(documento);
			}
		}
	}
	
	/**
	 * 
	 * @param listaDocumento
	 * @author Thiago Clemente
	 * 
	 */
	public void gerarHistoricoArquivoRemessa(List<Documento> listaDocumento){
		for (Documento documento: listaDocumento){
			Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
			documentohistorico.setObservacao("Arquivo de remessa gerado");
			documentohistoricoService.saveOrUpdate(documentohistorico);
		}
	}
	
	/**
	 * M�todo que abre popup para leitura de c�digo de barras de cheque
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupCodigobarrascheque(WebRequestContext request) {
		Documento bean = new Documento();
		
		String indexcodigobarrascheque = "";
		if(request.getParameter("indexcodigobarrascheque") != null){
			indexcodigobarrascheque = request.getParameter("indexcodigobarrascheque"); 
		}
		request.setAttribute("descricao", "C�DIGO DE BARRAS");
		request.setAttribute("indexcodigobarrascheque", indexcodigobarrascheque);
		return new ModelAndView("direct:/crud/popup/codigobarrachequepopup", "bean", bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateNumeroByEntradafsical(Documento documento, String numero)
	 *
	 * @param documento
	 * @param numero
	 * @author Luiz Fernando
	 */
	public void updateNumeroByEntradafsical(Documento documento, String numero) {
		documentoDAO.updateNumeroByEntradafsical(documento, numero);
	}
	
	/**
	 * Busca as contas a receber que tenha o cddocumento ou nossonumero e a conta do filtro.
	 *
	 * @param listaNossoNumero
	 * @param conta
	 * @param documentoclasse
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForRetornoConfiguravel(List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento, Conta conta, Documentoclasse documentoclasse) {
		return documentoDAO.findForRetornoConfiguravel(listaDocumento, conta, documentoclasse);
	}
	
	/**
	 * Carrega um documento
	 *
	 * @param documento
	 * @return
	 * @author Marden Silva
	 */
	public Documento loadForRetornoConfiguravel(Documento documento) {
		return documentoDAO.loadForRetornoConfiguravel(documento);
	}
	
	
	/**
	 * Busca os documentos para a tela de sele��o de documento no retorno configur�vel
	 *
	 * @param filtro
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForSelecaoDocumentoListagem(SelecionarDocumentoBean filtro){
		return documentoDAO.findForSelecaoDocumentoListagem(filtro);
	}
	
	/**
	 *  Atualiza o campo de rejeitado pelo banco do documento
	 * 
	 * @param documento
	 * @author Marden Silva
	 */
	public void updateRejeitado(Documento documento) {
		documentoDAO.updateRejeitado(documento);
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemnente
	 * 
	 */
	public void alterarContaReceberLote(String selectedItens, DocumentoFiltro filtro){
		documentoDAO.alterarContaReceberLote(selectedItens, filtro);
	}
	
	/**
	 * Atualiza as taxas e mensagem do documento de acordo com  a conta-corrente.
	 * @param documentosIDs - ids dos documentos que ser�o atualizados.(separados por ',').
	 * @param contacorrente - conta corrente com a taxa e seus respectivos itens.
	 */
	public void updateBoletoETaxa(String whereIn, Conta contacorrente, Contacarteira contacarteira){
		
		if(StringUtils.isBlank(whereIn) || contacorrente == null || contacorrente.getCdconta() == null) return;
		
		contacorrente = contacorrenteService.loadContaAlterContaReceberLote(contacorrente);
		contacarteira = contacarteira!=null && contacarteira.getCdcontacarteira()!=null? contacarteiraService.load(contacarteira): null;
		List<Documento> listDocumentos = documentoDAO.findDocumentoUpdateBoletoETaxa(whereIn);
		for(Documento d: listDocumentos){
			Taxa taxa = contacorrente != null ? contacorrente.getTaxa() : null;
			try{
				if(taxa != null){
					taxa = (Taxa) taxa.clone();
					for(Taxaitem ti: taxa.getListaTaxaitem()){
						if(Tipocalculodias.ANTES_VENCIMENTO.equals(ti.getTipocalculodias())){
							ti.setDtlimite(SinedDateUtils.addDiasData(d.getDtvencimento(), -ti.getDias()));
						}else {
							ti.setDtlimite(SinedDateUtils.addDiasData(d.getDtvencimento(), ti.getDias()));
						}
					}
					
					taxa.setListaDocumento(new ListSet<Documento>(Documento.class));
					taxaService.saveOrUpdate(taxa);
				}else{
					taxa = null;
				}
				
				Map<String, String> mapMsg = taxaService.criarMensagemPadraoTaxa(taxa);
				mapMsg.put("mensagem4", contacarteira!=null && contacarteira.getMsgboleto1() != null? contacarteira.getMsgboleto1(): "");
				mapMsg.put("mensagem5", contacarteira!=null && contacarteira.getMsgboleto2() != null? contacarteira.getMsgboleto2(): "");
				documentoDAO.updateMsgBoleto(d, mapMsg, taxa);
			}catch (CloneNotSupportedException e) {
					e.printStackTrace();
			}
		}
	}
	public void updatePessoaEndereco(Documento documento, Pessoa pessoa, Endereco endereco) {
		documentoDAO.updatePessoaEndereco(documento, pessoa, endereco);
	}	
	
	/**
	 * N�O UTILIZAR ESTE M�TODO!
	 * @author Rafael Salvio
	 */
	@Deprecated
	public List<Documento> getDocumentosForCorrecaoRateio(){
		return documentoDAO.getDocumentosForCorrecaoRateio();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/10/2013
	 */
	public Documento loadContareceberByNumeroEmpresa(Empresa empresa, String numero) {
		return documentoDAO.loadContareceberByNumeroEmpresa(empresa, numero, null);
	}
	
	public Documento loadContareceberByNumeroEmpresa(Empresa empresa, String numero, String doc) {
		return documentoDAO.loadContareceberByNumeroEmpresa(empresa, numero, doc);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#loadWithValoratual(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/10/2013
	 */
	public Documento loadWithValoratual(Documento documento) {
		return documentoDAO.loadWithValoratual(documento);
	}
	
	public Documento loadWithEmpresa(Documento documento) {
		return documentoDAO.loadWithEmpresa(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoDAO#permitirFiltroDocumentoDeAte()
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 02/05/2014
	 */
	public Boolean permitirFiltroDocumentoDeAte() {
		return documentoDAO.permitirFiltroDocumentoDeAte();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param dtvencimento
	 * @param valor
	 * @return
	 * @author Rodrigo Freitas
	 * @param date 
	 * @since 30/06/2014
	 */
	public List<Documento> findBaixadasByEmpresaDtemissaoDtvencimentoValor(Empresa empresa, Date dtemissao, Date dtvencimento, Money valor, Money valorTaxa, String numero, String doc) {
		return documentoDAO.findBaixadasByEmpresaDtemissaoDtvencimentoValor(empresa, dtemissao, dtvencimento, valor, valorTaxa, numero, doc);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findBaixadasByEmpresaDtemissaoValorCdvenda(Empresa empresa, Date dtemissao, Money valor, String numero, Integer cdvenda)
	*
	* @param empresa
	* @param dtemissao
	* @param valor
	* @param numero
	* @param cdvenda
	* @return
	* @since 29/08/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findBaixadasByEmpresaDtemissaoValorCdvenda(Empresa empresa, Date dtemissao, Money valor, Money valorTaxa, String numero, Integer cdvenda) {
		return documentoDAO.findBaixadasByEmpresaDtemissaoValorCdvenda(empresa, dtemissao, valor, valorTaxa, numero, cdvenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param dtemissao
	 * @return
	 * @author Rodrigo Freitas
	 * @param dtemissao2 
	 * @since 20/08/2014
	 */
	public List<Documento> findBaixadasByEmpresaDtemissaoPeriodo(Empresa empresa, Date dtemissao1, Date dtemissao2) {
		return documentoDAO.findBaixadasByEmpresaDtemissaoPeriodo(empresa, dtemissao1, dtemissao2);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateContaChequeSubstituto(String whereIn, Cheque chequeSubstituto)
	*
	* @param whereIn
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateContaChequeSubstituto(String whereIn, Cheque chequeSubstituto) {
		documentoDAO.updateContaChequeSubstituto(whereIn, chequeSubstituto);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#updateContaRemoveCheque(String whereIn)
	*
	* @param whereIn
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateContaRemoveCheque(String whereIn) {
		documentoDAO.updateContaRemoveCheque(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.DocumentoService#findContaForSubstituicaoByCheque(String whereInCheque, Documentoclasse documentoclasse, Documentoacao... documentoacao)
	*
	* @param whereInCheque
	* @param documentoclasse
	* @param documentoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Documento> findContaForSubstituicaoByCheque(String whereInCheque, Documentoclasse documentoclasse, Documentoacao... documentoacao) {
		return documentoDAO.findContaForSubstituicaoByCheque(whereInCheque, documentoclasse, documentoacao);
	}
	
	/**
	* M�todo que seta informa��es do cheque ao criar o documento
	*
	* @param request
	* @param documento
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void setInfDevolucaoCheque(WebRequestContext request, Documento documento) {
		if(request != null){
			String whereInChequeParam = request.getParameter("whereInCheque");
			String valorParam = request.getParameter("valor");
			String numeroParam = request.getParameter("numero");
			if(StringUtils.isNotEmpty(valorParam)){
				try {
					documento.setValor(new Money(valorParam));
				} catch (NumberFormatException e) {
					String valor = valorParam.replace(".", "");
					if(StringUtils.isEmpty(valor)) valor = valorParam;
					documento.setValor(new Money(valor.replace(",", ".")));
				}
			}
			if(StringUtils.isNotEmpty(numeroParam)){
				documento.setNumero(numeroParam);
			}
			if(StringUtils.isNotEmpty(whereInChequeParam)){
				documento.setWhereInChequeDevolucao(whereInChequeParam);
			}
		}
	}
	
	/**
	 * Realiza a baixa do documento baseado no v�nculo provisionado dele.
	 *
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 25/09/2014
	 */
	public void baixaAutomaticaByVinculoProvisionado(Documento documento) {
		try{
			if(documento.getVinculoProvisionado() != null){
				Conta vinculoProvisionado = contaService.loadConta(documento.getVinculoProvisionado());
				if(vinculoProvisionado != null && 
						vinculoProvisionado.getBaixaautomaticafaturamento() != null && 
						vinculoProvisionado.getBaixaautomaticafaturamento()){
					
					BaixarContaBean baixarContaBean = new BaixarContaBean();
					List<Documento> listaDocumento = new ArrayList<Documento>();
					
					
					listaDocumento.add(documento);
					listaDocumento = documentoService.calculaDocumentoValorByDataRef(listaDocumento, baixarContaBean.getDtpagamento(), true);

					SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
					
					baixarContaBean.setListaDocumento(listaDocumento);
					baixarContaBean.setRateio(documento.getRateio());
					baixarContaBean.setDtpagamento(documento.getDtvencimento());
					baixarContaBean.setDtcredito(documento.getDtvencimento());
					baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
					
					if(documento.getDocumentotipo() != null && documento.getDocumentotipo().getCddocumentotipo() != null){
						Documentotipo documentotipo = new Documentotipo(documento.getDocumentotipo().getCddocumentotipo());
						documentotipo = documentotipoService.load(documentotipo);
						
						if(documentotipo != null && documentotipo.getFormapagamentocredito() != null){
							baixarContaBean.setFormapagamento(formapagamentoService.load(documentotipo.getFormapagamentocredito()));
						}
					}
					
					baixarContaBean.setContatipo(vinculoProvisionado.getContatipo());
					baixarContaBean.setVinculo(vinculoProvisionado);
					baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
						baixarContaBean.setChecknum(documento.getNumero());
					} else {
						baixarContaBean.setChecknum(sdf.format(documento.getDtvencimento()));
					}
					baixarContaBean.setFromBaixaautomatica(true);
					baixarContaBean.setEmpresa(documento.getEmpresa());
					
					Movimentacao movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
					if(baixarContaBean.getIsContaBaixada() == null || !baixarContaBean.getIsContaBaixada()){
//						contareceberService.incrementaNumDocumento(documento);
						documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
						
						documentoService.callProcedureAtualizaDocumento(documento);
						movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
						
						//AT 155864
						//Criar uma segunda movimenta��o, de d�bito, com o valor da taxa e considerando no rateio a conta gerencial e centro de custo de taxa informado no tipo de documento.
						//Vincular a movimenta��o da taxa com a movimenta��o de origem, da conta a receber.
						if (movimentacao.getListaMovimentacaoorigem()!=null){
							for (Movimentacaoorigem movimentacaoorigem: movimentacao.getListaMovimentacaoorigem()){
								if (movimentacaoorigem.getMovimentacaorelacionada()!=null 
										&& movimentacaoorigem.getMovimentacaorelacionada().getCdmovimentacao()!=null){
									movimentacaoService.callProcedureAtualizaMovimentacao(movimentacaoorigem.getMovimentacaorelacionada());
								}
							}
						}
						
						movimentacaoService.updateHistoricoWithDescricao(movimentacao);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(NeoWeb.isInRequestContext()){
				NeoWeb.getRequestContext().addError("N�o foi poss�vel realizar a baixa do documento " + documento.getCddocumento() + ": " + e.getMessage());
			}
		}
	}
	
	/**
	 * M�todo que seta em um documento a data da �ltima cobran�a
	 * @param cddocumento 
	 * @param currentDate
	 */
	public void atualizaDataUltimaCobranca(Date data, Documento documento) {
		documentoDAO.atualizaDataUltimaCobranca(data, documento);	
	}
	
	/**
	 * Busca os documento para a a��o de enviar mailing passando os cddocumentos(whereIn)
	 * @param whereIn
	 * @return
	 */
	public List<Documento> findByWhereInForMaillingDevedor(String whereIn) {
		return documentoDAO.findByWhereInForMaillingDevedor(whereIn);
	}
	
	public void criarHistoricoERegistrarUltimaCobranca(DevedoresBean devedores, Documento documento, String observacao) {
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setDocumento(documento);
		documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		documentohistorico.setDocumentoacao(Documentoacao.COBRANCA_DEBITO);
		documentohistorico.setObservacao(observacao);
		
		documentoService.atualizaDataUltimaCobranca(SinedDateUtils.currentDate(), documento);
		documentohistoricoService.saveOrUpdate(documentohistorico);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @param filtro 
	 * @since 08/04/2015
	 */
	public List<Documento> loadForListagem(String whereIn, DocumentoFiltro filtro, String orderBy, boolean asc) {
		return documentoDAO.loadForListagem(whereIn, filtro, orderBy, asc);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 26/05/2015
	 */
	public void updateVencimento(Documento documento) {
		documentoDAO.updateVencimento(documento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findWithDatapagamento(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 03/06/2015
	* @author Luiz Fernando
	*/
	public List<Documento> findWithDatapagamento(String whereIn) {
		return documentoDAO.findWithDatapagamento(whereIn);
	}
	
	/**
	 * @param entrega
	 * @return
	 * @since 16/06/2015
	 * @author Andrey Leonardo
	 */
	public Boolean existeContaPagarByEntrega(Entrega entrega){
		return documentoDAO.existeContaPagarByEntrega(entrega);
	}
	
	public List<String> verificaSituacaoDocumento(String whereIn, boolean envioBoleto){
		
		List<String> listaErro = new ArrayList<String>();
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(!envioBoleto && fechamentofinanceiroService.verificaListaFechamento(aux)){
			listaErro.add("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
		}
		
		if(documentoService.isDocumentoNotDefitivaNotPrevista(whereIn)){
			listaErro.add("S� pode imprimir boleto de uma conta a receber que estiver na situa��o prevista ou definitiva.");
		}
		
		List<Documento> listaDocumento = documentoService.findForBoleto(whereIn);
		
		String paraEnvioEmailCliente = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.ENVIOFINANCEIROMAILPRINCIPALCLIENTE);
		
		for (Documento documentoAux : listaDocumento) {
			if(documentoAux.getConta() == null){
				listaErro.add("A conta a receber "+documentoAux.getCddocumento()+" n�o possui conta banc�ria cadastrada.");
			} else if(documentoAux.getPessoa() == null){
				listaErro.add("A conta a receber "+documentoAux.getCddocumento()+" n�o possui cliente ou colaborador cadastrado.");
			} else {
				boolean encontrouEmail = false;
				String msgErro = ""; 
				if (!paraEnvioEmailCliente.equals("FALSE")){
					if(documentoAux.getPessoa().getEmail() == null || documentoAux.getPessoa().getEmail().equals("")){
						msgErro += "O cliente "+documentoAux.getPessoa().getNome()+" n�o possui e-mail cadastrado. ";
					}else {
						encontrouEmail = true;
					}
				}
				if(!encontrouEmail){
					List<Contato> listaContato = ContatoService.getInstance().findByPessoa(documentoAux.getPessoa(), true);
					for (Contato contato : listaContato){
						if(contato.getEmailcontato() != null && !contato.getEmailcontato().equals("")){
							encontrouEmail = true;
						}
					}
					msgErro += "O cliente "+documentoAux.getPessoa().getNome()+" n�o possui contatos para receber boleto com e-mail cadastrado. ";
				}
				
				if(!encontrouEmail){
					listaErro.add(msgErro);
				}		
			}
		}
		return listaErro;
	}
		
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#validaDocumentoSituacao(String whereIn, boolean diferente, Documentoacao... documentoacao)
	*
	* @param whereIn
	* @param diferente
	* @param documentoacao
	* @return
	* @since 05/10/2015
	* @author Luiz Fernando
	*/
	public boolean validaDocumentoSituacao(String whereIn, boolean diferente, Documentoacao... documentoacao) {
		return documentoDAO.validaDocumentoSituacao(whereIn, diferente, documentoacao);
	}
	
	/**
	* M�todo que estorna o documento baixado parcial (Caso exista vale compra, eles ser�o exclu�dos)
	*
	* @param bean
	* @throws Exception
	* @since 15/12/2015
	* @author Luiz Fernando
	*/
	public void doEstornarContasBaixadasParcial(final EstornaContaBean bean) throws Exception{
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(SinedUtil.isListNotEmpty(bean.getListaDocumentoAEstornar())){
					for(Documento documento : bean.getListaDocumentoAEstornar()){
						List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByDocumento(documento);
						for (Valecompraorigem valecompraorigem : listaValecompraorigem) {
							Valecompra valecompra = valecompraorigem.getValecompra();
							valecompraorigemService.delete(valecompraorigem);
							if(valecompra != null){
								valecompraService.delete(valecompra);
							}
						}
						List<HistoricoAntecipacao> listaHistoricoanAntecipacao = historicoAntecipacaoService.findByDocumentoReferencia(documento);
						if(SinedUtil.isListNotEmpty(listaHistoricoanAntecipacao)){
							for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoanAntecipacao) {
								historicoAntecipacaoService.delete(historicoAntecipacao);
							}
						}
						documentoService.retornaDocumentosAcaoAnt(documento, Documentoacao.ESTORNADA, null, true);
					}
				}
				
				return null;
			}
		});
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/04/2016
	 */
	public List<Documento> findForWSSOAP(Empresa empresa, Cliente cliente) {
		return documentoDAO.findForWSSOAP(empresa, cliente);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.DocumentoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<DocumentoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<DocumentoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<DocumentoRESTModel> lista = new ArrayList<DocumentoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Documento bean : documentoDAO.findForAndroid(whereIn, sincronizacaoInicial)){
				if(Documentoacao.BAIXADA_PARCIAL.equals(bean.getDocumentoacao())){
					bean.setValorRestante(movimentacaoService.calcularMovimentacoesByDocumento(bean, new Date(System.currentTimeMillis())));
				}
				lista.add(new DocumentoRESTModel(bean));
			}
		}
		
		return lista;
	}
	
	/**
	* M�todo que atualiza o item do rateio de acordo com o novo valor
	*
	* @param whereIn
	* @param valorNovo
	* @since 16/09/2016
	* @author Luiz Fernando
	*/
	public void atualizaValorRateio(String whereIn, Money valorNovo) {
		List<Documento> lista = documentoDAO.findForAtualizarRateio(whereIn);
		if(SinedUtil.isListNotEmpty(lista) && valorNovo != null){
			for(Documento d : lista){
				if(d.getRateio() != null && SinedUtil.isListNotEmpty(d.getRateio().getListaRateioitem())){
					rateioService.atualizaValorRateio(d.getRateio(), valorNovo);

					Double totalRateioItem = 0d;
					for(Rateioitem rateioitem : d.getRateio().getListaRateioitem()){
						totalRateioItem += SinedUtil.round(rateioitem.getValor().getValue().doubleValue(), 10);
					}
					
					Double diferenca = valorNovo.subtract(new Money(SinedUtil.round(totalRateioItem, 2))).getValue().doubleValue();
					if(diferenca != 0){
						Rateioitem rateioitem = d.getRateio().getListaRateioitem().get(0);
						rateioitem.setValor(rateioitem.getValor().add(new Money(diferenca)));
						double percentualAux = rateioitem.getValor().getValue().doubleValue()/valorNovo.getValue().doubleValue() * 100d;
						percentualAux = SinedUtil.round(percentualAux, 10);
						rateioitem.setPercentual(percentualAux);
					}
					
					for(Rateioitem rateioitem : d.getRateio().getListaRateioitem()){
						rateioitemService.updateValorRateioItem(rateioitem);
					}
				}
			}
		}
		
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoDAO#isOrigemContratoNotaAposRecebimento(String contas)
	*
	* @param contas
	* @return
	* @since 19/10/2016
	* @author Luiz Fernando
	*/
	public boolean isOrigemContratoNotaAposRecebimento(String contas) {
		return documentoDAO.isOrigemContratoNotaAposRecebimento(contas);
	}
	
	/**
	* M�todo respons�vel por validar se o limite do nosso n�mero excedeu ou est� sendo excedido.
	* Retorna um HashMap com as mensagens de "Excedendo" ou "Excedido", que s�o armazenadas no retorno
	* respectivamente com os tipos MessageType.WARN ou MessageType.ERROR
	*
	* @param listaDocumento, contacarteira
	* @return HashMap<MessageType, String>
	* @since 03/10/2016
	* @author Mairon Cezar
	*/
	public HashMap<MessageType, String> validaLimiteSequencialNossoNum(List<Documento> listaDocumento, Contacarteira contacarteira){
		HashMap<MessageType, String> retorno = new HashMap<MessageType, String>();
		if(listaDocumento != null){
			BigInteger limiteParaAlertaNossoNumero = BigInteger.valueOf(1000);
			HashMap<Conta, Contacarteira> mapaConveniosExcedendo = new HashMap<Conta, Contacarteira>();
			HashMap<Conta, Contacarteira> mapaConveniosExcedidos = new HashMap<Conta, Contacarteira>();
			for(Documento bean: listaDocumento){
				Contacarteira contaCart = (contacarteira != null? contacarteira: bean.getContacarteira());
				
				if(contaCart != null && StringUtils.isNotBlank(contaCart.getLimiteposicoesintervalonossonumero())){
					BigInteger limiteNossoNumero =  new BigInteger(contaCart.getLimiteposicoesintervalonossonumero());
					BigInteger numeroDocumento = new BigInteger(bean.getCddocumento().toString());
					if(limiteNossoNumero.compareTo(numeroDocumento)<0){
						Conta conta = contaService.findContaByContacarteira(contaCart);
						if(!mapaConveniosExcedidos.containsKey(conta)){
							mapaConveniosExcedidos.put(conta, contaCart);
						}
						retorno.put(MessageType.ERROR, "O limite do nosso n�mero (sequencial do boleto) da conta "+conta.getNome()+" foi excedido. Entre em contato com o seu banco para libera��o de mais nosso n�mero (sequencial do boleto).");
					}else if(limiteNossoNumero.subtract(numeroDocumento).compareTo(limiteParaAlertaNossoNumero)<=0){
						Conta conta = contaService.findContaByContacarteira(contaCart);
						if(!mapaConveniosExcedendo.containsKey(conta)){
							mapaConveniosExcedendo.put(conta, contaCart);
						}
					}
				}
			}
			
			if(!mapaConveniosExcedendo.isEmpty()){
				StringBuilder alertas = new StringBuilder();
				String conveniosExcedendo = CollectionsUtil.listAndConcatenate(mapaConveniosExcedendo.keySet(), "nome", ",");
				if(mapaConveniosExcedendo.keySet().size() > 1){
					alertas.append("Quantidade de nosso n�mero permitido para as contas banc�rias "+conveniosExcedendo+" est� acabando. Procure o seu banco para aumentar a quantidade.\n");
				}else{
					alertas.append("Quantidade de nosso n�mero permitido para a conta banc�ria "+conveniosExcedendo+" est� excedendo. Procure o seu banco para aumentar a quantidade.\n");
				}
				retorno.put(MessageType.WARN, alertas.toString());
			}
			
			if(!mapaConveniosExcedidos.isEmpty()){
				StringBuilder alertas = new StringBuilder();
				String conveniosExcedidos = CollectionsUtil.listAndConcatenate(mapaConveniosExcedidos.keySet(), "nome", ",");
				if(mapaConveniosExcedidos.keySet().size() > 1){
					alertas.append("O limite do nosso n�mero (sequencial do boleto) das contas "+conveniosExcedidos+" foi excedido. Entre em contato com o seu banco para libera��o de mais nosso n�mero (sequencial do boleto).");
				}else{
					alertas.append("O limite do nosso n�mero (sequencial do boleto) da conta "+conveniosExcedidos+" foi excedido. Entre em contato com o seu banco para libera��o de mais nosso n�mero (sequencial do boleto).");
				}
				retorno.put(MessageType.ERROR, alertas.toString());
			}	
		}
		return retorno;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param documento
	 * @since 17/11/2016
	 * @author C�sar
	 */
	public void updateDtVencimentoAntiga(Documento documento){
		documentoDAO.updateDtVencimentoAntiga(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param wherein
	 * @return
	 * @since 17/11/2016
	 * @author C�sar
	 */
	public List<Documento> findDtVencimentoByWhereIn (String wherein){
		return documentoDAO.findDtVencimentoByWhereIn(wherein);
	}
	
	/**
	* M�todo que preenche o n�mero da fatura de loca��o no campo numero fatura do documento
	*
	* @param listaDocumento
	* @since 28/11/2016
	* @author Luiz Fernando
	*/
	public void preencherNumeroFaturalocacao(List<Documento> listaDocumento) {
		if(SinedUtil.isListNotEmpty(listaDocumento) && parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO)){
			List<Integer> listaNumero;
			for(Documento documento : listaDocumento){
				if(SinedUtil.isListNotEmpty(documento.getListaContratofaturalocacaodocumento())){
					listaNumero = new ArrayList<Integer>();
					for(Contratofaturalocacaodocumento cfld : documento.getListaContratofaturalocacaodocumento()){
						if(cfld.getContratofaturalocacao() != null && cfld.getContratofaturalocacao().getNumero() != null && 
								!listaNumero.contains(cfld.getContratofaturalocacao().getNumero())){
							listaNumero.add(cfld.getContratofaturalocacao().getNumero());
						}
					}
					if(SinedUtil.isListNotEmpty(listaNumero)){
						documento.setNumerofaturalocacao(CollectionsUtil.concatenate(listaNumero, ","));
					}
				}
			}
		}
	}
	
	public List<Documento> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, Formapagamento formaPagamento){
		return documentoDAO.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, formaPagamento);
	}
	
	public List<Documento> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return this.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, null);
	}
	
	public List<Documento> findForGerarLancamentoContabilAntecipacao(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return documentoDAO.findForGerarLancamentoContabilAntecipacao(filtro, movimentacaocontabilTipoLancamento);
	}
	
	public boolean isMesmaPessoa(List<Documento> listaDocumento){
		Set<Integer> listaCdpessoa = new HashSet<Integer>();
		
		if (listaDocumento!=null){
			for (Documento documento: listaDocumento){
				if (documento.getPessoa()!=null){
					listaCdpessoa.add(documento.getPessoa().getCdpessoa());
				}
			}
		}
		
		return listaCdpessoa.size()==1;
	}
	
	public void setDatasBaixaContaListagem(DocumentoFiltro filtro, List<Documento> listaDocumento, String whereIn){
		if ((filtro.getListaDocumentoacao()!=null &&
			(filtro.getListaDocumentoacao().contains(Documentoacao.BAIXADA) || filtro.getListaDocumentoacao().contains(Documentoacao.BAIXADA_PARCIAL))) &&
			!listaDocumento.isEmpty()){
			String whereInCddocumento = (StringUtils.isNotBlank(whereIn) ? whereIn : CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ","));
			List<Movimentacao> listaMovimentacao = movimentacaoService.findForContaListagem(whereInCddocumento);
			List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByWhereInCddocumento(whereInCddocumento);
			
			for (Documento documento: listaDocumento){
				Set<String> listaDataStr = new HashSet<String>();
				List<Date> listaData = new ArrayList<Date>();
				
				for (Movimentacao movimentacao: listaMovimentacao){
					for (Movimentacaoorigem movimentacaoorigem: movimentacao.getListaMovimentacaoorigem()){
						if (documento.equals(movimentacaoorigem.getDocumento())){
							listaData.add(movimentacao.getDataMovimentacao());
						}
					}
				}
				
				for (Valecompraorigem valecompraorigem: listaValecompraorigem){
					if (valecompraorigem.getValecompra().getTipooperacao().equals(Tipooperacao.TIPO_DEBITO) && documento.equals(valecompraorigem.getDocumento())){
						listaData.add(valecompraorigem.getValecompra().getData());
					}
				}
				
				String parametro = parametrogeralService.getValorPorNome(Parametrogeral.DATA_PAGAMENTO_ANTECIPACAO);
				parametro = parametro != null ? br.com.linkcom.utils.StringUtils.tiraAcento(parametro).toUpperCase() : "";
				
				if ("ANTECIPACAO".equalsIgnoreCase(parametro)) {
					documento.setListaHistoricoAntecipacao(historicoAntecipacaoService.findByDocumentoReferencia(documento));
					if (documento.getListaHistoricoAntecipacao() != null && documento.getListaHistoricoAntecipacao().size() > 0) {
						String whereInCddocumentoAntecipacao = CollectionsUtil.listAndConcatenate(documento.getListaHistoricoAntecipacao(), "documento.cddocumento", ",");
						if(StringUtils.isNotBlank(whereInCddocumentoAntecipacao)){
							List<Movimentacao> listaMovimentacaoAntecipacao = movimentacaoService.findForContaListagem(whereInCddocumentoAntecipacao);
							for (HistoricoAntecipacao histAntecipacao : documento.getListaHistoricoAntecipacao()) {
								for (Movimentacao movimentacao: listaMovimentacaoAntecipacao) {
									for (Movimentacaoorigem movimentacaoorigem: movimentacao.getListaMovimentacaoorigem()) {
										if (histAntecipacao.getDocumento().equals(movimentacaoorigem.getDocumento())) {
											listaData.add(movimentacao.getDataMovimentacao());
										}
									}
								}
							}
						}
					}
				} else if ("BAIXA".equalsIgnoreCase(parametro)) {
					documento.setListaHistoricoAntecipacao(historicoAntecipacaoService.findByDocumentoReferencia(documento));
					if (documento.getListaHistoricoAntecipacao() != null && documento.getListaHistoricoAntecipacao().size() > 0) {
						for (HistoricoAntecipacao histAntecipacao : documento.getListaHistoricoAntecipacao()) {
							listaData.add(new java.sql.Date(histAntecipacao.getDataHora().getTime()));
						}
					}
				}
				
				if (!listaDataStr.isEmpty()){
					documento.setDatasBaixa(CollectionsUtil.concatenate(listaDataStr, "<BR>"));
				}
				
				if (!listaData.isEmpty()){
					Collections.sort(listaData, new Comparator<Date>() {
						public int compare(Date d1, Date d2){
							return d1.compareTo(d2);
						}
					});

					for(Date data: listaData){
						Integer difDias = SinedDateUtils.diferencaDias(data, documento.getDtvencimento());
						String dataStr = NeoFormater.getInstance().format(data)+" ("+difDias.toString()+")";
						if(data.compareTo(documento.getDtvencimento()) > 0){
							dataStr = dataStr.replace("(", "<font color=\"red\"> (").concat("</font>");
							listaDataStr.add(dataStr);
						}else if(data.compareTo(documento.getDtvencimento()) < 0){
							dataStr = dataStr.replace("(", "<font color=\"blue\"> (").concat("</font>");
							listaDataStr.add(dataStr);
						}else{
							listaDataStr.add(dataStr);
						}
					}
					documento.setDatasBaixa(CollectionsUtil.concatenate(listaDataStr, "<BR>"));
				}
			}
		}
	}
	
	public void updateCancelamentocobranca(String whereIn, boolean cancelamentocobranca){
		documentoDAO.updateCancelamentocobranca(whereIn, cancelamentocobranca);
	}
	
	public void updateEmissaoboletobloqueado(Documento documento){
		documentoDAO.updateEmissaoboletobloqueado(documento);
	}
	
	public boolean isPossuiEmissaoBoletoBloqueado(String whereIn){
		return documentoDAO.isPossuiEmissaoBoletoBloqueado(whereIn);
	}
	
	public void updateCheque(Documento documento, Cheque cheque) {
		documentoDAO.updateCheque(documento, cheque);
	}
	
	public boolean isTodasContasCanceladasOrigemNegociacao(Integer cddocumento){
		return documentoDAO.isTodasContasCanceladasOrigemNegociacao(cddocumento);
	}
	
	public void updateDocumentoacaoanteriornegociacao(Documento documento, Documentoacao documentoacaoanteriornegociacao){
		documentoDAO.updateDocumentoacaoanteriornegociacao(documento, documentoacaoanteriornegociacao);
	}
	
	public void alteraSituacaoAnteriorNegociada(String ids){
		for (Documento documento1: carregaDocumentos(ids)){
			if (isTodasContasCanceladasOrigemNegociacao(documento1.getCddocumento())){
				for (Vdocumentonegociado vdocumentonegociado: vdocumentonegociadoService.findByDocumentosnegociados(documento1.getCddocumento().toString())){
					Documento documento2 = load(new Documento(vdocumentonegociado.getCddocumento()), "documento.cddocumento, documento.documentoacao, documento.documentoacaoanteriornegociacao");
					if (documento2!=null && Documentoacao.NEGOCIADA.equals(documento2.getDocumentoacao())){
						contapagarService.updateStatusConta(documento2.getDocumentoacaoanteriornegociacao(), documento2.getCddocumento().toString());						
					}
				}
			}
		}
	}
	
	public MailingDevedorRetornoBean montaListagemDevedores(MailingDevedorBean filtro) {
		MailingDevedorRetornoBean retornoBean = new MailingDevedorRetornoBean();
		
		Categoria categoria = null;
		if(filtro.getCategoria() != null){
			categoria = categoriaService.loadWithIdentificador(filtro.getCategoria());
		}
		
		List<Documento> listaDocumento = documentoService.findByContasAtrasadas(filtro.getEmpresa(),categoria,filtro.getContatotipo(), filtro.getDtvencimento1(), filtro.getDtvencimento2(), Documentoclasse.OBJ_RECEBER, filtro);
		StringBuilder sbEmail, sbContato, sbTelefone;	
		
		StringBuilder whereInPessoa = new StringBuilder();
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			for(Documento documento : listaDocumento){
				if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
					whereInPessoa.append(documento.getPessoa().getCdpessoa().toString()).append(",");
				}
				documento.setValorRestante(movimentacaoService.calcularMovimentacoesByDocumento(documento, new Date(System.currentTimeMillis())));
			}
		}
		
		List<Contato> listacontatoPessoa = null;
		List<Agendainteracao> listaAgendainteracao = null;
		if(StringUtils.isNotEmpty(whereInPessoa.toString())){
			listacontatoPessoa = contatoService.findByPessoaContatotipo(whereInPessoa.substring(0, whereInPessoa.length()-1), filtro.getContatotipo());
			listaAgendainteracao = agendainteracaoService.findProximaCobranca(filtro.getEmpresa(), whereInPessoa.substring(0, whereInPessoa.length()-1));
		}
		
		List<Contato> listacontato;
		for (Documento documento : listaDocumento) {
			
			DevedoresBean devedoresBean = getDevedoresBeanByPessoa(retornoBean.getListaDevedores(), documento);
			if(devedoresBean == null){
				devedoresBean = new DevedoresBean();
				
				sbContato = new StringBuilder();
				sbEmail = new StringBuilder();
				sbTelefone = new StringBuilder();
				
				if(documento.getPessoa().getEmail() != null){
					sbEmail.append(documento.getPessoa().getEmail());
				}
							
				listacontato = getListacontato(listacontatoPessoa, documento.getPessoa());
				
				if(documento.getPessoa() != null && documento.getPessoa().getTelefonesComQuebraLinha() != null){
					sbTelefone.append(documento.getPessoa().getTelefonesComQuebraLinha());				
				}
	
				if (listacontato != null){			
					for (int i = 0; i < listacontato.size(); i++) {
						if (listacontato.get(i).getNome() != null && !"".equals(listacontato.get(i).getNome())){
							if(sbContato.length() > 0)
								sbContato.append("\n");
							
							if(listacontato.get(i).getContatotipo() != null && !"".equals(listacontato.get(i).getContatotipo().getNome())){
								sbContato.append(listacontato.get(i).getNome() + " - " + listacontato.get(i).getContatotipo().getNome());							
							}else{
								sbContato.append(listacontato.get(i).getNome());
							}		
						}
						if(listacontato.get(i).getEmailcontato() != null && !"".equals(listacontato.get(i).getEmailcontato())){
							if(sbEmail.length() > 0)
								sbEmail.append(",\n ");
							sbEmail.append(listacontato.get(i).getEmailcontato());
						}
						if(listacontato.get(i).getTelefones() != null && !"".equals(listacontato.get(i).getTelefones())){
							if(sbTelefone.length() > 0){
								sbTelefone.append("\n");
								sbTelefone.append(listacontato.get(i).getTelefones("\n"));							
							}
						}
					}
				}
				
				devedoresBean.setContato(sbContato.toString());
				devedoresBean.setEmail(sbEmail.toString());
				devedoresBean.setEmpresa(documento.getEmpresa());
				devedoresBean.setPessoa(documento.getPessoa());
				devedoresBean.setDocumento(documento);
				devedoresBean.setWhereInDocumento(documento.getCddocumento().toString());
				devedoresBean.setContaatrasada(
								"<a href=\"#\" onclick=\"redirecionaPaginaContaReceber("+ documento.getCddocumento()+")\"" + 
								devedoresBean.getStyleLinkContaAtrasada(documento) + 
								devedoresBean.getOnmouseoverDtvencimentoAlteracao(documento) + ">" + 
								documento.getNumeroCddocumento() +
								"(" + 
								SinedDateUtils.toString(documento.getDtvencimento()) + 
								devedoresBean.getAsterisco(documento) + 
								(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null ? " - " + documento.getAux_documento().getValoratual() : "") +
								(documento.getDtcartorio() != null ? " - <font color=\"red\">P</font>" : "") +
								")</a>");
				
				devedoresBean.setContaatrasadareport(
						documento.getNumeroCddocumento() +  
						"(" +  
						SinedDateUtils.toString(documento.getDtvencimento()) +  
						devedoresBean.getAsterisco(documento) + 
						(documento.getAux_documento().getValoratual() != null ? " - " + documento.getAux_documento().getValoratual() : "") +
						(documento.getDtcartorio() != null ? " - P" : "") +
						")");
				devedoresBean.setCliente(documento.getPessoa().getNome());
				devedoresBean.setValor(documento.getValor());
				if(Documentoacao.BAIXADA_PARCIAL.equals(documento.getDocumentoacao())){
					devedoresBean.setValoratual(documento.getValorRestante());
				}else if(documento.getAux_documento() != null){
					devedoresBean.setValoratual(documento.getAux_documento().getValoratual());						
				}
				
				devedoresBean.setTelefone(sbTelefone.toString());
				
				if(documento.getDtultimacobranca() != null){
					devedoresBean.setUltimaCobranca(documento.getDtultimacobranca());				
				}
				devedoresBean.setProximaCobranca(getProximaCobranca(documento.getPessoa(), listaAgendainteracao));
				retornoBean.getListaDevedores().add(devedoresBean);
				if(devedoresBean.getValor() != null){
					retornoBean.setValorTotal(retornoBean.getValorTotal().add(devedoresBean.getValor()));
				}
				if(devedoresBean.getValoratual() != null){
					retornoBean.setValorAtualTotal(retornoBean.getValorAtualTotal().add(devedoresBean.getValoratual()));					
				}
			} else {
				Money valorAcumulado = devedoresBean.getValor() != null ? devedoresBean.getValor() : new Money(0);
				Money valorAtualAcumulado = devedoresBean.getValoratual() != null ? devedoresBean.getValoratual() : new Money(0);
				devedoresBean.addDocumento(documento);
				if(devedoresBean.getValor() != null){
					retornoBean.setValorTotal(retornoBean.getValorTotal().add(devedoresBean.getValor().subtract(valorAcumulado)));
				}
				if(devedoresBean.getValoratual() != null){
					retornoBean.setValorAtualTotal(retornoBean.getValorAtualTotal().add(devedoresBean.getValoratual().subtract(valorAtualAcumulado)));					
				}
				if(SinedUtil.isListEmpty(devedoresBean.getListaDocumento()) || !devedoresBean.getListaDocumento().contains(documento)){
					if(StringUtils.isNotEmpty(devedoresBean.getWhereInDocumento())){
						devedoresBean.setWhereInDocumento(devedoresBean.getWhereInDocumento() + "," + documento.getCddocumento().toString());
					}else {
						devedoresBean.setWhereInDocumento(documento.getCddocumento().toString());
					}
				}
			}
		}
		
		if(filtro.getCampoOrdenacao() != null && OrdenacaoMailingDevedor.VALOR.equals(filtro.getCampoOrdenacao())){
			final boolean tipoordenacao = filtro.getTipoOrdenacao() != null && filtro.getTipoOrdenacao();
			Collections.sort(retornoBean.getListaDevedores(), new Comparator<DevedoresBean>(){
				public int compare(DevedoresBean a1, DevedoresBean a2){
					try {
						return a1.getValor().compareTo(a2.getValor()) * (tipoordenacao ? -1 : 1);
					} catch (Exception e) { 
						return -1;
					}
				}
			});
		}
		return retornoBean;
	}
	
	/**
	* M�todo que retorna a pr�xima da de cobran�a de acordo com a agenda de intera��o do cliente
	*
	* @param pessoa
	* @param listaAgendainteracao
	* @return
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	private Date getProximaCobranca(Pessoa pessoa, List<Agendainteracao> listaAgendainteracao) {
		if(pessoa != null && pessoa.getCdpessoa() != null && SinedUtil.isListNotEmpty(listaAgendainteracao)){
			for(Agendainteracao agendainteracao : listaAgendainteracao){
				if(agendainteracao.getCliente() != null && pessoa.getCdpessoa().equals(agendainteracao.getCliente().getCdpessoa())){
					return agendainteracao.getDtproximainteracao();
				}
			}
		}
		return null;
	}
	
	private List<Contato> getListacontato(List<Contato> listacontato, Pessoa pessoa) {
		List<Contato> lista = new ArrayList<Contato>();
		if(SinedUtil.isListNotEmpty(listacontato) && pessoa != null){
			for(Contato contato : listacontato){
				if(contato.getListaContato() != null && contato.getListaContato().size() > 0) {
					for (PessoaContato pessoaContato : contato.getListaContato()) {
						if (pessoaContato.getPessoa().equals(pessoa)) {
							lista.add(contato);
						}
					}
				}
			}
		}
		return lista;
	}
	
	private DevedoresBean getDevedoresBeanByPessoa(List<DevedoresBean> listaDevedores, Documento documento) {
		if(SinedUtil.isListNotEmpty(listaDevedores) && documento != null && documento.getPessoa() != null){
			for(DevedoresBean devedoresBean : listaDevedores){
				boolean equalsPessoa = devedoresBean.getPessoa() != null && devedoresBean.getPessoa().equals(documento.getPessoa());
				
				boolean equalsEmpresa = (devedoresBean.getEmpresa() != null && documento.getEmpresa() != null && devedoresBean.getEmpresa().equals(documento.getEmpresa())) ||
										(devedoresBean.getEmpresa() == null && documento.getEmpresa() == null);
				
				if(equalsPessoa && equalsEmpresa &&
					(
						(documento.getDtultimacobranca() != null && devedoresBean.getUltimaCobranca() != null) || 
						(documento.getDtultimacobranca() == null && devedoresBean.getUltimaCobranca() == null)
					)){
					return devedoresBean;
				}
			}
		}
		return null;
	}
	
	public void updateTaxa(Documento documento, Taxa taxa){
		documentoDAO.updateTaxa(documento, taxa);
	}
	
	public ModelAndView abrirPopupAlterarTipoDocumento(WebRequestContext request) throws Exception {
		boolean contapagar = Boolean.valueOf(request.getParameter("contapagar") != null ? request.getParameter("contapagar") : "false"); 
		String selectedItens = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(selectedItens)){
			request.addError("Nenhum item selecionado");
			SinedUtil.redirecionamento(request, "/financeiro/crud/" + (contapagar ? "Contapagar" : "Contareceber"));
			return null;
		}
		
		boolean entrada = "true".equalsIgnoreCase(request.getParameter("entrada"));
		
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("entrada", entrada);
		
		//N�o � poss�vel fazer a altera��o. O tipo de documento atual � de antecipa��o e o documento j� est� associado a outras opera��es.
		if(isPossuiDocumentotipoAntecipacao(selectedItens)){
			request.addError("N�o � poss�vel fazer a altera��o. O tipo de documento atual � de adiantamento e o documento j� est� associado a outras opera��es.");
			SinedUtil.redirecionamento(request, "/financeiro/crud/" + (contapagar ? "Contapagar" : "Contareceber") + (entrada ? "?ACAO=consultar&cddocumento=" + selectedItens : ""));
			return null;
		}
		
		return new ModelAndView("direct:/crud/popup/alterarContaTipoDocumento");
	}
	
	public void salvarPopupAlterarTipoDocumento(WebRequestContext request, DocumentoFiltro filtro, String url) throws Exception {
		if(filtro.getDocumentotipo() != null){
			Documentotipo documentotipo = documentotipoService.load(filtro.getDocumentotipo(), "documentotipo.cddocumentotipo, documentotipo.nome");
			
			String whereIn = request.getParameter("selectedItens");
			List<Documento> listaDocumento = documentoService.carregaDocumentos(whereIn);
			documentoDAO.updateDocumentotipo(whereIn, filtro.getDocumentotipo());
			if(SinedUtil.isListNotEmpty(listaDocumento)){
				String observacao;
				for (Documento documento: listaDocumento){
					observacao = "";
					Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
					if(documento.getDocumentotipo() != null && documento.getDocumentotipo().getNome() != null){
						observacao += " de " + documento.getDocumentotipo().getNome();
					}
					observacao += " para " + documentotipo.getNome();
					documentohistorico.setObservacao("Altera��o do tipo de documento" + observacao + ".");
					documentohistoricoService.saveOrUpdate(documentohistorico);
				}
			}
		}
		request.addMessage("Registros alterados com sucesso.", MessageType.INFO);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent()
				.eval("<script type='text/javascript'>")
				.eval("parent.location = '" + request.getServletRequest().getContextPath() + url + "';")
				.eval("</script>")
				.flush();
	}
	
	public boolean isPossuiDocumentotipoAntecipacao(String whereIn){
		return documentoDAO.isPossuiDocumentotipoAntecipacao(whereIn);
	}
	
	public boolean isMovimentacaoTaxa(Documento documento){
		return documentoDAO.isMovimentacaoTaxa(documento);
	}
	
	public Documento loadForMovimentacaoTaxa(Documento documento) {
		return documentoDAO.loadForMovimentacaoTaxa(documento);
	}
	
	public Taxaitem getTaxaitemByTipo(Documento documento, Tipotaxa tipotaxa){
		if (documento != null && documento.getTaxa() != null && documento.getTaxa().getListaTaxaitem() != null && tipotaxa != null){
			for (Taxaitem taxaitem : documento.getTaxa().getListaTaxaitem()){
				if (tipotaxa.equals(taxaitem.getTipotaxa())){
					return taxaitem;
				}
			}
		}
		return null;
	}
	
	public boolean isPodeAlterarValorConta(WebRequestContext request, Documento documento){
		if (documento==null || documento.getCddocumento()==null){
			List<String> listaNomeParametro = Arrays.asList(new String[]{"cdcontrato", "gerarreceitafaturalocacao", "faturarPorMedicao", "faturarlocacao", "finalizarContrato"});
			List<String> listaNomeAcao = Arrays.asList(new String[]{"gerarReceita", "criarGerarreceitaServico", "criarFaturamentoEntrega"});
			
			for (String nomeParametro: listaNomeParametro){
				String valorParametro = request.getParameter(nomeParametro);
				if (valorParametro!=null){
					return false;
				}
			}
			
			for (String nomeAcao: listaNomeAcao){
				if (nomeAcao.equalsIgnoreCase(request.getParameter("ACAO"))){
					return false;
				}
			}
			
			return true;
		}else {
			return documentoDAO.isPodeAlterarValorConta(documento);
		}
	}
	
	public List<Documento> findDocsCopiaChequeForTemplate(String whereIn){
		return documentoDAO.findDocsCopiaChequeForTemplate(whereIn);
	}
	
	public LinkedList<EmitirCopiaChequeDocumentoBean> geraListaCopiaChequeDocumentoBean(List<Documento> listaDocumento){
		LinkedList<EmitirCopiaChequeDocumentoBean> retorno = new LinkedList<EmitirCopiaChequeDocumentoBean>();
		for(Documento documento: listaDocumento){
			retorno.add(new EmitirCopiaChequeDocumentoBean(documento));
		}
		return retorno;
	}
	
	public ModelAndView ajaxBuscaHistoricoOperacaoForBaixarConta(WebRequestContext request, BaixarContaBean bean){
		String whereIn = request.getParameter("selectedItens");
		Integer index = Integer.parseInt(request.getParameter("indice"));
		BaixarContaBeanMovimentacao beanMov = bean.getListaBaixarContaBeanMovimentacao().get(index);
		Historicooperacao historicoOperacao = null;
		if(beanMov.getFormapagamento() != null && beanMov.getFormapagamento().getCdformapagamento() != null){
			Tipohistoricooperacao historicotipo = Documentoclasse.OBJ_PAGAR.equals(bean.getDocumentoclasse())? Tipohistoricooperacao.CONTA_PAGAR_BAIXADA_FORNECEDOR:
				historicooperacaoService.findHistoricooperacaoTipoReceberByFormapagamento(beanMov.getFormapagamento());
			historicoOperacao = historicooperacaoService.findByTipo(historicotipo);
		}

		String historicoStr = montaHistoricoOperacaoForBaixarConta(whereIn, bean, beanMov);
		return new JsonModelAndView().addObject("historicoStr", historicoStr)
									.addObject("possuiHistoricoOperacao", historicoOperacao != null);
	}
	
	private String montaHistoricoOperacaoForBaixarConta(String whereIn, BaixarContaBean bean, BaixarContaBeanMovimentacao beanMov){
		String historicoAux = "";

		Money valor = beanMov.getValor();
		Conta conta = null;
		if(beanMov.getVinculo() != null && beanMov.getVinculo().getCdconta() != null){
			conta = contaService.load(beanMov.getVinculo());
		}
		Formapagamento formapagamento = null;
		if(beanMov.getFormapagamento() != null && beanMov.getFormapagamento().getCdformapagamento() != null){
			formapagamento = formapagamentoService.load(beanMov.getFormapagamento());
		}

		Documentoclasse documentoclasse = bean.getDocumentoclasse();

		Tipohistoricooperacao historicotipo = Documentoclasse.OBJ_PAGAR.equals(documentoclasse)? Tipohistoricooperacao.CONTA_PAGAR_BAIXADA_FORNECEDOR:
											historicooperacaoService.findHistoricooperacaoTipoReceberByFormapagamento(formapagamento);
		
		List<Documento> listaConta = contapagarService.findContasPreenchidas(whereIn);
		if(listaConta != null && listaConta.size() == 1){
			historicoAux = listaConta.get(0).getDescricao();
		}
		if(historicotipo != null){
			//Se gerar movimenta��o separada, o hist�rico de cada movimenta��o ser� montado ao gravar a baixa
			if(listaConta.size() > 1 && Boolean.TRUE.equals(bean.getGerarmovimentacaoparacadacontapagar())){
				return "";
				//return "* Ser� preenchido automaticamente";
			}
			
			for(Documento doc: listaConta){
				if(Tipopagamento.CLIENTE.equals(doc.getTipopagamento())){
					doc.setCliente(new Cliente(doc.getPessoa().getCdpessoa(), doc.getPessoa().getNome()));
				}else if(Tipopagamento.FORNECEDOR.equals(doc.getTipopagamento())){
					doc.setFornecedor(new Fornecedor(doc.getPessoa().getCdpessoa(), doc.getPessoa().getNome()));
				}else if(Tipopagamento.COLABORADOR.equals(doc.getTipopagamento())){
					doc.setColaborador(new Colaborador(doc.getPessoa().getCdpessoa(), doc.getPessoa().getNome()));
				}
				documentoService.ajustaNumeroParcelaDocumento(doc);
			}
			
			Historicooperacao historicooperacao = historicooperacaoService.findByTipo(historicotipo);
			if(historicooperacao != null){
				Movimentacao movimentacao = new Movimentacao();
				
				if(Util.strings.isNotEmpty(beanMov.getNumerocheque())){
					movimentacao.setCheque(preencheChequeForHistoricoOperacao(beanMov, whereIn));
				}
				
				movimentacao.setConta(conta);
				movimentacao.setFormapagamento(formapagamento);
				movimentacao.setValor(valor);
				movimentacao.setDtmovimentacao(bean.getDtpagamento());
				historicoAux = historicooperacaoService.findHistoricoByMovimentofinanceiro(movimentacao, listaConta, historicotipo, documentoclasse);					
			}
		}
		return historicoAux;
	}
	
	private Cheque preencheChequeForHistoricoOperacao(BaixarContaBeanMovimentacao bean, String whereInDocumentos){
		Cheque cheque = new Cheque();
		Empresa empresa = empresaService.getEmpresaByDocumentos(whereInDocumentos);		
		if(bean.getCdchequecheque() != null){
			cheque.setCdcheque(bean.getCdchequecheque());
			cheque.setResgatado(true);
			cheque.setValor(bean.getValorcheque());
			Cheque chequeBean = chequeService.loadForEntrada(cheque);
			if(chequeBean != null){
				cheque.setEmpresa(chequeBean.getEmpresa());
				cheque.setLinhanumerica(chequeBean.getLinhanumerica());
				cheque.setChequesituacao(chequeBean.getChequesituacao());
			}
		}else {
			cheque.setValor(bean.getValor());
		}
		cheque.setBanco(bean.getBancocheque());
		cheque.setAgencia(bean.getAgenciacheque());
		cheque.setConta(bean.getContacheque());
		cheque.setNumero(bean.getNumerocheque());
		cheque.setEmitente(bean.getEmitentecheque());
		cheque.setCpfcnpj(bean.getCpfcnpjcheque());
		cheque.setDtbompara(bean.getDtbomparacheque());
		cheque.setEmpresa(empresa);
		if(cheque.getChequesituacao() == null){
			cheque.setChequesituacao(Chequesituacao.PREVISTO);
		}
		return cheque;
	}
	
	public void insertUpdateValortaxa(Money valortaxa, Date data, List<Documento> listaDocumento, Tipotaxa tipotaxa, boolean estorno) {
		if(valortaxa != null && SinedUtil.isListNotEmpty(listaDocumento) && tipotaxa != null){
			for(Documento documento : listaDocumento){
				insertUpdateValortaxa(valortaxa, data, documento, tipotaxa, estorno);
			}
		}
	}
	
	public void insertUpdateValortaxa(Money valortaxa, Date data, Documento documento, Tipotaxa tipotaxa, boolean estorno) {
		if(valortaxa != null && valortaxa.getValue().doubleValue() > 0 && documento != null && documento.getCddocumento() != null && tipotaxa != null){
			Taxaitem taxaitem = taxaitemService.loadTaxaitemByDocumento(documento, tipotaxa);
			if(taxaitem != null){
				Money valor = null;
				if(estorno){
					if(taxaitem.getValor() != null && taxaitem.getValor().getValue().doubleValue() >= valortaxa.getValue().doubleValue()){
						valor = new Money(taxaitem.getValor().getValue().doubleValue() - valortaxa.getValue().doubleValue());
					}
				}else {
					if(taxaitem.getValor() != null){
						valor = new Money(taxaitem.getValor().getValue().doubleValue() + valortaxa.getValue().doubleValue());
					}else {
						valor = valortaxa;
					}
				}
				
				Date dataNova = data;
				if(data != null && !SinedDateUtils.beforeOrEqualIgnoreHour(data, new Date(System.currentTimeMillis()))){
					dataNova = new Date(System.currentTimeMillis());
				}
				
//				String motivo = taxaitemService.criaMotivoValorTaxa(dataNova, valortaxa);
//				if(motivo != null && StringUtils.isNotBlank(taxaitem.getMotivo())){
//					if(estorno){
//						motivo = taxaitem.getMotivo().replace(motivo, "");
//					}else if((taxaitem.getMotivo().length() + motivo.length()) <= 100){
//						motivo = taxaitem.getMotivo() + " " + motivo;
//					}
//				}
				if(valor != null){
					if(valor != null) taxaitem.setValor(valor);
					if(dataNova != null && !estorno) taxaitem.setDtlimite(dataNova);
//					taxaitemService.updateValorMotivo(taxaitem, valor, dataNova, motivo);
					taxaitemService.saveOrUpdate(taxaitem);
					updateObservacaoHistoricoBaixaTaxa(documento, valortaxa, dataNova, estorno);
				}
			}else if(!estorno){
				Taxa taxa = taxaService.findByDocumento(documento);
				if(taxa == null){
					taxa = new Taxa();
					taxaService.saveOrUpdate(taxa);
					
					documento.setTaxa(taxa);
					documentoService.updateTaxa(documento);
				}
				
				Taxaitem taxaitemNovo = new Taxaitem(tipotaxa, false, valortaxa, false);
				if(SinedDateUtils.beforeOrEqualIgnoreHour(data, new Date(System.currentTimeMillis()))){
					taxaitemNovo.setDtlimite(data);
				}else {
					taxaitemNovo.setDtlimite(new Date(System.currentTimeMillis()));
				}
				taxaitemNovo.setTaxa(taxa);
				taxaitemNovo.setValor(valortaxa);
//				taxaitemNovo.setMotivo(taxaitemService.criaMotivoValorTaxa(data, valortaxa));
				taxaitemService.saveOrUpdate(taxaitemNovo);
				
				updateObservacaoHistoricoBaixaTaxa(documento, valortaxa, data, estorno);
			}
		}
	}
	
	private void updateObservacaoHistoricoBaixaTaxa(Documento documento, Money valortaxa, Date data, boolean estorno) {
		if(valortaxa != null && valortaxa.getValue().doubleValue() > 0 && documento != null){
			Documentohistorico documentohistorico = documentohistoricoService.loadUltimoHistorico(documento);
			if(documentohistorico != null){
				if(!estorno){
					String texto = "Baixa parcial realizada com inclus�o de taxa";
					String observacao = "";
					if(StringUtils.isNotBlank(documentohistorico.getObservacao())){
						observacao = documentohistorico.getObservacao();
					}
					if(observacao.contains(texto)){
						int index = observacao.lastIndexOf(".");
						if(index != -1){
							observacao = observacao.substring(0, index);
						}
						observacao += ", valor de R$ " + valortaxa.toString() + (data != null ? " no dia " + new SimpleDateFormat("dd/MM/yyyy").format(data) : "") + ".";
					}else {
						observacao += texto + " no valor de R$ " + valortaxa.toString() + (data != null ? " no dia " + new SimpleDateFormat("dd/MM/yyyy").format(data) : "") + ".";
					}
					
					documentohistoricoService.updateObservacaoDocumento(documentohistorico, observacao);
				}else {
					String observacao = "";
					if(StringUtils.isNotBlank(documentohistorico.getObservacao())){
						observacao = documentohistorico.getObservacao() + " ";
					}
					documentohistoricoService.updateObservacaoDocumento(documentohistorico, observacao + "Taxa de baixa (acr�scimo) no valor de R$ " + valortaxa.toString() + " removida da conta a receber.");
				}
			}else if(estorno){
				Usuario usuario = SinedUtil.getUsuarioLogado();
				documentohistorico = new Documentohistorico();
				documentohistorico.setDocumento(documento);
				documentohistorico.setDocumentoacao(Documentoacao.CANCEL_MOVIMENTACAO);
				documentohistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				documentohistorico.setObservacao("Taxa de baixa (acr�scimo) no valor de R$ " + valortaxa.toString() + " removida da conta a receber.");
				documentohistoricoService.saveOrUpdate(documentohistorico);
			}
		}
	}
	
	public List<Documento> findForFormapagamento(String whereIn, String whereInMovimentacoes){
		return documentoDAO.findForFormapagamento(whereIn, whereInMovimentacoes);
	}
	
	public List<Documento> findForUpdateTaxaitem(String whereIn){
		return documentoDAO.findForUpdateTaxaitem(whereIn);
	}
	
	public List<Documento> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Contagerencial contagerencial, String whereNotIn, String whereNotInRatioitem){
		return documentoDAO.findForGerarLancamentoContabilContaGerencial(filtro, contagerencial, whereNotIn, whereNotInRatioitem);
	}

	/**
	 * Busca as contas a receber filhas(ou "netas" e etc...) de uma conta que foi negociada
	 *
	 * @param documentonegociado
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Documento> findByDocumentonegociado(Documento documentonegociado){
		List<Documento> listaFilhos = new ArrayList<Documento>();
		for(Vdocumentonegociado doc: vdocumentonegociadoService.findByDocumentos(documentonegociado.getCddocumento().toString())){
			listaFilhos.add(carregaDocumento(new Documento(doc.getCddocumentonegociado())));
		}
		return listaFilhos;
	}

	/**
	 * Retorna true se um documento que foi negociado possui um ou mais filhos(ou "netos" e etc...) com situa��o baixada ou baixada parcialmente 
	 *
	 * @param documentonegociado
	 * @return
	 * @author Mairon Cezar
	 */
	public boolean isDocumentonegociadoComFilhoBaixado(Documento documentonegociado){
		List<Documento> docsFilhos = documentoService.findByDocumentonegociado(documentonegociado);
		boolean exists = false;
		for(Documento doc: docsFilhos){
			if((Documentoacao.BAIXADA.equals(doc.getDocumentoacao()) || Documentoacao.BAIXADA_PARCIAL.equals(doc.getDocumentoacao()))){
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	public List<Documento> findForValidationDtbanco(String whereIn){
		return documentoDAO.findForValidationDtbanco(whereIn);
	}
	
	public Date calculaDtbancoForBaixa(Date dtmovimentacao, String whereIn, Cheque chequePagamento){
		Date dtbanco = null;
		boolean preencherdatabanco = false; 
		Integer maxDiasreceber = 0;
		Empresa empresa = null;
		if(Util.strings.isNotEmpty(whereIn)){
			List<Documento> listaDocumento = documentoService.findForValidationDtbanco(whereIn);
			for(Documento doc: listaDocumento){
				if(doc.getDocumentotipo() != null){
					if(Boolean.TRUE.equals(doc.getDocumentotipo().getPreencherdatabanco()) && !preencherdatabanco){
						preencherdatabanco = true;
						empresa = doc.getEmpresa();
					}
					if(doc.getDocumentotipo().getDiasreceber() != null && doc.getDocumentotipo().getDiasreceber() > maxDiasreceber){
						maxDiasreceber = doc.getDocumentotipo().getDiasreceber();
					}
				}
			}
			if(preencherdatabanco){
				if(chequePagamento != null && chequePagamento.getDtbompara() != null){
					dtmovimentacao = chequePagamento.getDtbompara();
				}
				dtmovimentacao = SinedDateUtils.addDiasData(dtmovimentacao, maxDiasreceber);
				Endereco enderecoempresa = null;
				if(empresa != null){
					enderecoempresa = enderecoService.getEnderecoPrincipalPessoa(empresa);
				}
				dtbanco = calendarioService.proximoDiaUtil(enderecoempresa, dtmovimentacao);
			}
		}
		return dtbanco;
	}
	
	public ModelAndView ajaxValidaDocumentotipoForBaixa(WebRequestContext request){
		ModelAndView retorno = new JsonModelAndView();
		String whereIn = request.getParameter("selectedItens");
		boolean preencherdatabanco = false; 
		List<Integer> listaDiasreceber = new ArrayList<Integer>();
		String mensagem = "";
		if(Util.strings.isNotEmpty(whereIn)){
			List<Documento> listaDocumento = documentoService.findForValidationDtbanco(whereIn);
			for(Documento doc: listaDocumento){
				if(doc.getDocumentotipo() != null){
					if(Boolean.TRUE.equals(doc.getDocumentotipo().getPreencherdatabanco())){
						preencherdatabanco = true;
					}
					if(doc.getDocumentotipo().getDiasreceber() != null && !listaDiasreceber.contains(doc.getDocumentotipo().getDiasreceber()))
						listaDiasreceber.add(doc.getDocumentotipo().getDiasreceber());
				}
			}
			Collections.sort(listaDiasreceber);
			if(preencherdatabanco && !listaDiasreceber.isEmpty() && listaDocumento.size() > 1){
				mensagem = "Foram selecionadas contas que possuem valores diferentes no campo 'Dias para receber(previs�o)' no cadastro de Tipo de Documento. Ser� utilizado o maior valor para o registro da movimenta��o.";
			}
		}
		return retorno.addObject("mensagem", mensagem);
	}
	
	public void defineDocumentoacaoParametroGeral(Documento documento, Documentoacao documentoacao, Boolean NOTACOMPRA_GERA_CONTADEFINITIVA) {
		if(NOTACOMPRA_GERA_CONTADEFINITIVA)
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
		else
			documento.setDocumentoacao(documentoacao);
	}
	
	public List<Documento> findForValiationProcessamentoExtratocartao(String whereInMovimentacoes){
		return documentoDAO.findForValiationProcessamentoExtratocartao(whereInMovimentacoes);
	}
	
	public List<Documento> findDocumentoByMovimentacao(String whereInMovimentacao, Documentoclasse documentoclasse, Tipopagamento tipopagamento){
		return documentoDAO.findDocumentoByMovimentacao(whereInMovimentacao, documentoclasse, tipopagamento);
	}
	
	public Documento findEmpresaByDocumento(Integer cddocumento) {
		return documentoDAO.findEmpresaByDocumento(cddocumento);
	}
	
	public List<Documento> findByDocumentoacao(Documentoacao documentoacao, Cliente cliente, Boolean atrasadas) {
		return documentoDAO.findByDocumentoacao(documentoacao, cliente, atrasadas);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @since 16/08/2019
	 * @author Rodrigo Freitas
	 */
	public boolean isOrigemNegociacao(Documento documento) {
		return documentoDAO.isOrigemNegociacao(documento);
	}
	
	public List<Documento> findForCompensarSaldo(String whereIn) {
		return documentoDAO.findForCompensarSaldo(whereIn);
	}
	
	public Boolean possuiContasComSaldoNaoCompensado(Documento documento) {
//		List<Documento> listaDocumentos = documentoDAO.buscarDocumentosNaoCompensadosPorPessoa(documento);
//		
//		for(Documento doc : listaDocumentos) {
//			Money valorJaUtilizado = new Money();
//			for(HistoricoAntecipacao historico : doc.getListaHistoricoAntecipacao()) {
//				valorJaUtilizado = valorJaUtilizado.add(historico.getValor());
//			}
//			
//			if(doc.getValorDouble() - valorJaUtilizado.getValue().doubleValue() > 0) {
//				return true;
//			}
//		}
		
		if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
			List<Documento> listaDocumentos = findForAntecipacaoConsideraPedidoVenda(documento.getPessoa(), null, null, documento.getDocumentoclasse());
			return SinedUtil.isListNotEmpty(listaDocumentos);
		}
		return false;
	}
	
	public boolean isDocumentoAntecipacaoComSaldo(Pessoa pessoa, Documentotipo documentotipo, Documento documento, Documentoclasse documentoClasse){
		if (!SinedUtil.isObjectValid(pessoa)) return false;
		
		List<Documento> lista = documentoDAO.findForAntecipacao(pessoa, documentotipo, true, null, documento, documentoClasse);
		return SinedUtil.isListNotEmpty(lista);
	}
	
	public void verificarSaldoAdiantamentoPorDocumento(Documento documento, Documentoclasse documentoClasse) {
		documento.setPossuiSaldoAdiantamento(documentoService.isDocumentoAntecipacaoComSaldo(documento.getPessoa(), documento.getDocumentotipo(), documento, documentoClasse));
	}
	
	public void verificarDocumentosAntesDeCompensar(List<Documento> listaDocumentos, Documentoclasse classeDaCompensacao) throws SinedException {
		if(SinedUtil.isListNotEmpty(listaDocumentos)) {
			Empresa empresa = listaDocumentos.get(0).getEmpresa();
			Pessoa pessoa = listaDocumentos.get(0).getPessoa();
			String recebimentoDe = listaDocumentos.get(0).getRecebimentode();
			
			String whereInDocumentos = CollectionsUtil.listAndConcatenate(listaDocumentos, "cddocumento", ",");
			List<Vdocumentonegociado> listaNegociadasNaoCanceladas = vdocumentonegociadoService.findByDocumentos(whereInDocumentos);
			
			String classeStr = "", campoStr = "";
			if(classeDaCompensacao.equals(Documentoclasse.OBJ_PAGAR)) {
				classeStr = "pagar";
				campoStr = "Pagamento A";
			} else {
				classeStr = "receber";
				campoStr = "Recebimento De";
			}
			
			for(Documento documento : listaDocumentos) {
				if(documento.getDocumentotipo() == null || !Boolean.TRUE.equals(documento.getDocumentotipo().getAntecipacao())) {
					throw new SinedException("S� � permitido realizar Compensa��o de adiantamento de contas a "+ classeStr +" de adiantamento do tipo de documento de adiantamento.");
				}
				if(empresa != null && !documento.getEmpresa().equals(empresa)) {
					throw new SinedException("S� � permitido realizar Compensa��o de adiantamento de contas a "+ classeStr +" de adiantamento da mesma empresa.");
				}
				if(recebimentoDe != null && !documento.getRecebimentode().equals(recebimentoDe)) {
					throw new SinedException("S� � permitido realizar Compensa��o de adiantamento de contas a "+ classeStr +" de adiantamento do mesmo \""+ campoStr +"\" (Cliente, Colaborador ou Fornecedor).");
				}
				if(pessoa != null && !documento.getPessoa().equals(pessoa)) {
					throw new SinedException("S� � permitido realizar Compensa��o de adiantamento de contas a "+ classeStr +" de adiantamento da mesma pessoa (Cliente, Colaborador ou Fornecedor).");
				}
				if(!documento.getDocumentoacao().equals(Documentoacao.BAIXADA)) {
					throw new SinedException("S� � permitido realizar Compensa��o de adiantamento de contas a "+ classeStr +" de adiantamento com situa��o Baixada.");
				}
				
				Money somaValoresAntecipados = new Money();
				for(HistoricoAntecipacao historico : documento.getListaHistoricoAntecipacao()) {
					if(listaNegociadasNaoCanceladas != null && !listaNegociadasNaoCanceladas.isEmpty()) {
						String cdDocumentosNegociados = CollectionsUtil.listAndConcatenate(listaNegociadasNaoCanceladas, "cddocumentonegociado", ",");
						
						throw new SinedException("N�o � poss�vel gerar Compensa��o de adiantamento da(s) conta(s) a "+ classeStr + " " + whereInDocumentos +
								", ela j� foi compensada na(s) conta(s) a "+ (classeStr.equals("receber") ? "pagar" : "receber") +" " + cdDocumentosNegociados + ".");
					}
					
					somaValoresAntecipados = somaValoresAntecipados.add(historico.getValor());
				}
				
				Double documentoValor = documento.getValorDouble() != null ? documento.getValorDouble() : 0D;
				if(documentoValor - somaValoresAntecipados.getValue().doubleValue() <= 0) {
					throw new SinedException("Saldo das contas a "+ classeStr + " de adiantamento � insuficiente para gerar Compensa��o de adiantamento.");
				}
			}
		}
	}
	
	public void preencherDocumentoCompensacao(List<Documento> listaDocumentosACompensar, Documento documento) {
		if(documento == null) {
			documento = new Documento();
		}
		
		documento.setEmpresa(listaDocumentosACompensar.get(0).getEmpresa());
		documento.setPessoa(listaDocumentosACompensar.get(0).getPessoa());
		documento.setOutrospagamento(listaDocumentosACompensar.get(0).getOutrospagamento());
		documento.setTipopagamento(listaDocumentosACompensar.get(0).getTipopagamento());
		
		if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null) {
			if (Tipopagamento.CLIENTE.equals(documento.getTipopagamento())) {
				documento.setCliente(ClienteService.getInstance().load(new Cliente(documento.getPessoa().getCdpessoa())));
			} else if (Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())) {
				documento.setFornecedor(FornecedorService.getInstance().load(new Fornecedor(documento.getPessoa().getCdpessoa())));
			} else if (Tipopagamento.COLABORADOR.equals(documento.getTipopagamento())) {
				documento.setColaborador(ColaboradorService.getInstance().load(new Colaborador(documento.getPessoa().getCdpessoa())));
			}
		}
		
		Money somaValoresDisponiveisParaCompensacao = new Money(0D);
		StringBuilder valoresStr = new StringBuilder();
		
		for(Documento d : listaDocumentosACompensar) {
			Money valorJaUtilizado = new Money(0D);
			Money valorDisponivel = new Money(0D);
			
			for(HistoricoAntecipacao ha : d.getListaHistoricoAntecipacao()) {
				valorJaUtilizado = valorJaUtilizado.add(ha.getValor());
			}
			
			valorDisponivel = d.getValor().subtract(valorJaUtilizado);
			somaValoresDisponiveisParaCompensacao = somaValoresDisponiveisParaCompensacao.add(valorDisponivel);
			
			if(listaDocumentosACompensar.indexOf(d) < (listaDocumentosACompensar.size() - 1)) {
				valoresStr.append(d.getCddocumento() + ":" + valorJaUtilizado.getValue().doubleValue() + "|");
			} else {
				valoresStr.append(d.getCddocumento() + ":" + valorJaUtilizado.getValue().doubleValue());
			}
		}
		
		documento.setValor(somaValoresDisponiveisParaCompensacao);
		documento.setDocumentoAdiantamentoValores(valoresStr.toString());
		
		String whereInDocumento = CollectionsUtil.listAndConcatenate(listaDocumentosACompensar, "cddocumento", ",");
		documento.setDescricao("Compensa��o de saldo da(s) conta(s) "+ whereInDocumento +".");
		documento.setDtemissao(new Date(System.currentTimeMillis()));
		documento.setWhereInDocumentoAdiantamento(whereInDocumento);
		documento.setIsCompensarSaldo(true);
	}
	
	public void gerarHistoricoCompensacao(Documento bean) {
		String classeStr = bean.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR) ? "pagar" : "receber";
		Map<String, Money> mapaValoresDocumentosAdiantados = new HashMap<String, Money>();
		
		if(StringUtils.isNotBlank(bean.getDocumentoAdiantamentoValores())) {
			for(String contas : bean.getDocumentoAdiantamentoValores().split("\\|")) {
				String conta = contas.split(":")[0];
				Money valor = new Money(Double.parseDouble(contas.split(":")[1]));
				
				mapaValoresDocumentosAdiantados.put(conta, valor);
			}
		}
		
		for(String codigo : bean.getWhereInDocumentoAdiantamento().split(",")) {
			Documentohistorico dh = new Documentohistorico();
			dh.setDocumento(new Documento(Integer.parseInt(codigo)));
			dh.setDocumentoacao(DocumentoacaoService.getInstance().load(Documentoacao.COMPENSACAO_SALDO));
			dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
			dh.setObservacao("Conta a "+ classeStr +" de compensa��o: <a href=\"javascript:visualizaConta"+ classeStr +
					"("+ bean.getCddocumento() +");\">"+ bean.getCddocumento() +"</a>");
			
			HistoricoAntecipacao ha = new HistoricoAntecipacao();
			ha.setDescricao("Valor descontado na Conta "+ classeStr +" de compensa��o: " + bean.getCddocumento());
			ha.setDocumento(new Documento(Integer.parseInt(codigo)));
			ha.setDocumentoReferencia(bean);
			ha.setValor(bean.getValor());
			ha.setDataHora(new Date(System.currentTimeMillis()));
			ha.setCompensacao(true);
			
			documentohistoricoService.saveOrUpdate(dh);
			historicoAntecipacaoService.saveOrUpdate(ha);
		}
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Documento documento){
		return new JsonModelAndView().addObject("isApropriacao", documentotipoService.isApropriacao(request, documento.getDocumentotipo()));
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Documento documento){
		Documentotipo documentoTipo = documentotipoService.loadWithPrazoApropriacao(documento.getDocumentotipo());
		Prazopagamento prazo = documentoTipo!= null && documentoTipo.getPrazoApropriacao() != null? prazopagamentoService.loadForEntrada(documentoTipo.getPrazoApropriacao()): null;
		List<DocumentoApropriacao> lista = new ArrayList<DocumentoApropriacao>();
		Integer numParcelas = 0;
		if(documento.getDtemissao() != null && prazo != null && prazo.getListaPagamentoItem() != null){
			numParcelas = prazo.getListaPagamentoItem().size();
			for(Prazopagamentoitem item: prazo.getListaPagamentoItem()){
				Integer dias = item.getDias();
				DocumentoApropriacao bean = new DocumentoApropriacao();
				if(dias != null){
					Date dataApropriacao = SinedDateUtils.addDiasData(documento.getDtemissao(), dias);
					dataApropriacao = SinedDateUtils.firstDateOfMonth(dataApropriacao);
					bean.setMesAno(dataApropriacao);
				}else{
					if(item.getMeses() != null){
						Date dataApropriacao = SinedDateUtils.addMesData(documento.getDtemissao(), item.getMeses());					
						dataApropriacao = SinedDateUtils.firstDateOfMonth(dataApropriacao);
						bean.setMesAno(dataApropriacao);
					}
				}
				
				lista.add(bean);
			}
		}
		View.getCurrent().convertObjectToJs(lista, "listaApropriacao");
		View.getCurrent().println("var parcelas = " +numParcelas+";");
	}
	
	public List<Documento> findWithDespesaColaborador(String whereIn) {
		return documentoDAO.findWithDespesaColaborador(whereIn);
	}
	
	public void ajustaValoresApropriacao(Documento doc, Money valorOriginal){
		List<DocumentoApropriacao> listaApropriacao = doc.getListaApropriacao();
		Money total = new Money();
		
		// Percorre a lista de apropria��es para somar o valor total
		for(DocumentoApropriacao apropriacao : listaApropriacao){
			total = total.add(apropriacao.getValor());
		}
		
		// Reajusta o valor de cada parcela para o equivalente do novo valor total definido na tela
		if(total.compareTo(doc.getValor())!=0){
			for(DocumentoApropriacao apropriacao : listaApropriacao){
				apropriacao.setValor(doc.getValor().multiply((apropriacao.getValor().divide(total))));
			}
		}
	}

	public boolean existeContaPagarEntradaFiscal(String whereIn) {
		return documentoDAO.existeContaPagarEntradaFiscal(whereIn);
	}
	public List<Documento> findForNotaPromissoria(String whereIn) {
		return documentoDAO.findForNotaPromissoria(whereIn);
	}
	public Report gerarNotaPromissoria(Documento documento) {
		Report report = new Report("/financeiro/notaPromissoria");

		String nomeEmpresa = null;
		String cnpjEmpresa = null;
		String telefoneEmpresa = "";
		String enderecoEmpresa = null;
		String municipioEmpresa = null;
		String nomePagador = null;
		String cpfCnpjPagador = null;
		String telefonePagador = "";
		String enderecoPagador = null;
		String dtVencimento = null;
		Money valor = null;
		String valorExtenso = "";
		String promissoria = null;
		String origem = "";
		
		if(documento.getEmpresa() != null){
			if(documento.getEmpresa().getNomefantasia() != null){
				nomeEmpresa = documento.getEmpresa().getNomefantasia();
			}else if(documento.getEmpresa().getRazaosocial() != null){
				nomeEmpresa = documento.getEmpresa().getRazaosocial();
			}else if(documento.getEmpresa().getNome() != null){
				nomeEmpresa = documento.getEmpresa().getNome();
			}
			if(documento.getEmpresa().getCnpj() != null){
				cnpjEmpresa = documento.getEmpresa().getCnpj().toString();
			}
			if(documento.getEmpresa().getListaTelefone() != null && !documento.getEmpresa().getListaTelefone().isEmpty()){
				for (Telefone telefone : documento.getEmpresa().getListaTelefone()) {
					telefoneEmpresa = telefoneEmpresa + telefone.getTelefone() + " | ";
				}
				
				telefoneEmpresa = telefoneEmpresa.substring(0, telefoneEmpresa.length()-3);
			}
			if(SinedUtil.isListNotEmpty(documento.getEmpresa().getListaEndereco())){
				enderecoEmpresa = documento.getEmpresa().getEnderecoWithPrioridade(Enderecotipo.FATURAMENTO, Enderecotipo.COBRANCA, Enderecotipo.CORRESPONDENCIA, Enderecotipo.ENTREGA, Enderecotipo.INSTALACAO, Enderecotipo.OUTRO).getLogradouroCompletoComBairro();
				municipioEmpresa = documento.getEmpresa().getEnderecoWithPrioridade(Enderecotipo.FATURAMENTO, Enderecotipo.COBRANCA, Enderecotipo.CORRESPONDENCIA, Enderecotipo.ENTREGA, Enderecotipo.INSTALACAO, Enderecotipo.OUTRO).getMunicipio().getNome();
			}
		}
		
		if(documento.getPessoa() != null){
			if(documento.getPessoa().getNome() != null){
				nomePagador = documento.getPessoa().getNome();
			}
			if(documento.getPessoa().getCnpj() != null){
				cpfCnpjPagador = documento.getPessoa().getCnpj().toString();
			}else if(documento.getPessoa().getCpf() != null){
				cpfCnpjPagador = documento.getPessoa().getCpf().toString();
			}
			if(documento.getPessoa().getListaTelefone() != null && !documento.getPessoa().getListaTelefone().isEmpty()){
				for (Telefone telefone : documento.getPessoa().getListaTelefone()) {
					telefonePagador = telefonePagador + telefone.getTelefone() + " | ";
				}
				
				telefonePagador = telefonePagador.substring(0, telefonePagador.length()-3);
			}
			if(documento.getEndereco() != null){
				enderecoPagador = documento.getEndereco().getLogradouroCompletoComBairro();
			}else if(SinedUtil.isListNotEmpty(documento.getPessoa().getListaEndereco())){				
				enderecoPagador = documento.getPessoa().getEnderecoWithPrioridade(Enderecotipo.FATURAMENTO, Enderecotipo.ENTREGA, Enderecotipo.CORRESPONDENCIA, Enderecotipo.INSTALACAO, Enderecotipo.OUTRO).getLogradouroCompletoComBairro();
			}	
		}
		
		if(documento.getDtvencimento() != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			dtVencimento = sdf.format(documento.getDtvencimento());
		}
		if(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null){
			valor = documento.getAux_documento().getValoratual();
			Extenso extenso = new Extenso(documento.getAux_documento().getValoratual());
			valorExtenso = extenso.toString();
		}
		if(documento.getCddocumento() != null){
			promissoria = documento.getCddocumento().toString();
		}
		if(SinedUtil.isListNotEmpty(documento.getListaDocumentoOrigem())){
			for (Documentoorigem documentoorigem : documento.getListaDocumentoOrigem()) {
				if(documentoorigem.getVenda() != null){
					origem = origem + documentoorigem.getVenda().getCdvenda().toString() + ", ";
				}else if(documentoorigem.getPedidovenda() != null){
					origem = origem + documentoorigem.getPedidovenda().getCdpedidovenda().toString() + ", ";
				}else if(documentoorigem.getDocumento() != null){
					origem = origem + documentoorigem.getDocumento().getNumero();
				}
			}
			if(origem.endsWith(", ")){
				origem = origem.substring(0, origem.length() -2);
			}
		}else {
			origem = documento.getNumero();
		}
		
		report.addParameter("NOME_EMPRESA", nomeEmpresa);
		report.addParameter("CNPJ_EMPRESA", cnpjEmpresa);
		report.addParameter("TELEFONE_EMPRESA", telefoneEmpresa);
		report.addParameter("ENDERECO_EMPRESA", enderecoEmpresa);
		report.addParameter("MUNICIPIO_EMPRESA", municipioEmpresa);
		report.addParameter("NOME_PAGADOR", nomePagador);
		report.addParameter("CPF_CNPJ_PAGADOR", cpfCnpjPagador);
		report.addParameter("TELEFONE_PAGADOR", telefonePagador);
		report.addParameter("ENDERECO_PAGADOR", enderecoPagador);
		report.addParameter("DTVENCIMENTO", dtVencimento);
		report.addParameter("VALOR", valor);
		report.addParameter("VALOR_EXTENSO", valorExtenso);
		report.addParameter("PROMISSORIA", promissoria);
		report.addParameter("ORIGEM", origem);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		report.addParameter("DATA_ATUAL", format.format(new java.util.Date()));

		return report;
	}
	
	public void criarRegistroHistorico(Documento documento) {
		boolean criarHistorico = true;
		if(SinedUtil.isListNotEmpty(documento.getListaDocumentohistorico())){
			for (Documentohistorico documentohistorico : documento.getListaDocumentohistorico()) {
				if(Documentoacao.NOTA_PROMISSORIA_GERADA.equals(documentohistorico.getDocumentoacao())){
					criarHistorico = false;
				}
			}			
		}
		
		if(criarHistorico){
			documento.setAcaohistorico(Documentoacao.NOTA_PROMISSORIA_GERADA);
			documento.setObservacaoHistorico("Nota promiss�ria gerada pela primeira vez.");
			Documentohistorico documentohistorico = new Documentohistorico(documento);
			documentohistoricoService.saveOrUpdate(documentohistorico);
		}
		
	}
	
}
	
