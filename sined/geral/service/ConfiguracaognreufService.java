package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognreuf;
import br.com.linkcom.sined.geral.dao.ConfiguracaognreufDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaognreufService extends GenericService<Configuracaognreuf>{
	
	private ConfiguracaognreufDAO configuracaognreufDAO;

	public void setConfiguracaognreufDAO(
			ConfiguracaognreufDAO configuracaognreufDAO) {
		this.configuracaognreufDAO = configuracaognreufDAO;
	}
	
	public List<Configuracaognreuf> findByConfiguracaognre(Configuracaognre configuracaognre) {
		return configuracaognreufDAO.findByConfiguracaognre(configuracaognre);
	}

}
