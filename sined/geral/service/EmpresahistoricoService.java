package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresahistorico;
import br.com.linkcom.sined.geral.dao.EmpresahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresahistoricoService extends GenericService<Empresahistorico> {

	protected EmpresahistoricoDAO empresahistoricoDAO;
	
	public void setEmpresahistoricoDAO(EmpresahistoricoDAO empresahistoricoDAO) {
		this.empresahistoricoDAO = empresahistoricoDAO;
	}
	
	/**
	 * Busca todos os registros de histórico de uma determinada {@link Empresa}
	 * 
	 * @author Giovane Freitas
	 * @param empresa
	 * @return
	 */
	public List<Empresahistorico> findByEmpresa(Empresa empresa) {
		
		if (empresa == null || empresa.getCdpessoa() == null)
			return Collections.emptyList();
		
		return empresahistoricoDAO.findByEmpresa(empresa);
	}

	/**
	 * Carrega um histórico para visualização.
	 * 
	 * @author Giovane Freitas
	 * @param empresahistorico
	 * @return
	 */
	public Empresahistorico loadForVisualizacao(Empresahistorico empresahistorico) {

		if (empresahistorico == null || empresahistorico.getCdempresahistorico() == null)
			return null;
		
		return empresahistoricoDAO.loadForVisualizacao(empresahistorico);
	}



}
