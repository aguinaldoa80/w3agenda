package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.Arquivotefhistorico;
import br.com.linkcom.sined.geral.dao.ArquivotefhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivotefhistoricoService extends GenericService<Arquivotefhistorico> {
	
	private ArquivotefhistoricoDAO arquivotefhistoricoDAO;
	
	public void setArquivotefhistoricoDAO(
			ArquivotefhistoricoDAO arquivotefhistoricoDAO) {
		this.arquivotefhistoricoDAO = arquivotefhistoricoDAO;
	}

	public List<Arquivotefhistorico> findByArquivotef(Arquivotef arquivotef) {
		return arquivotefhistoricoDAO.findByArquivotef(arquivotef);
	}

}
