package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ConfiguracaoecomDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaoecomService extends GenericService<Configuracaoecom>{

	private ConfiguracaoecomDAO configuracaoecomDAO;
	
	public void setConfiguracaoecomDAO(ConfiguracaoecomDAO configuracaoecomDAO) {
		this.configuracaoecomDAO = configuracaoecomDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaoecomDAO#haveConfiguracaoPrincipalEmpresa(Empresa empresa, Integer cdconfiguracaoecom)
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/01/2014
	 */
	public boolean haveConfiguracaoPrincipalEmpresa(Empresa empresa, Integer cdconfiguracaoecom) {
		return configuracaoecomDAO.haveConfiguracaoPrincipalEmpresa(empresa, cdconfiguracaoecom);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaoecomDAO#findPrincipalByEmpresa(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/01/2014
	 */
	public Configuracaoecom findPrincipalByEmpresa(Empresa empresa) {
		return configuracaoecomDAO.findPrincipalByEmpresa(empresa);
	}
	

}
