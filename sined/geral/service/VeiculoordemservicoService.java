package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoferiado;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.dao.VeiculoordemservicoDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AgendamentoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.BaixaManutencaoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.BaixaManutencaoSubReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FormularioInspecaoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.VeiculoagendamentoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.BaixamanutencaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.VeiculoagendamentoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculoordemservicoService extends GenericService<Veiculoordemservico>{

	private VeiculoordemservicoDAO veiculoordemservicoDAO;
	private VeiculoferiadoService veiculoferiadoService;
	private VeiculoordemservicoitemService veiculoordemservicoitemService;
	private ParametrogeralService parametrogeralService;
	private EnvioemailService envioemailService;
	private VeiculousoService veiculousoService;
	private VeiculoService veiculoService;
	
	public void setVeiculoferiadoService(
			VeiculoferiadoService veiculoferiadoService) {
		this.veiculoferiadoService = veiculoferiadoService;
	}
	public void setVeiculoordemservicoDAO(
			VeiculoordemservicoDAO veiculoordemservicoDAO) {
		this.veiculoordemservicoDAO = veiculoordemservicoDAO;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setVeiculoordemservicoitemService(
			VeiculoordemservicoitemService veiculoordemservicoitemService) {
		this.veiculoordemservicoitemService = veiculoordemservicoitemService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	/**
	 * <b>M�todo respons�vel por calcular data padrao de uma ordem de servi�o manuten��o, onde<br>
	 * a data padr�o s�o 10 dias ap�s a data atual</b> 
	 * @author Biharck
	 * @param form Ordem de servi�o
	 */
	public Veiculoordemservico calculaDataPadrao(Veiculoordemservico form) {
		Date atual = new Date(System.currentTimeMillis());
		//recebe a data atual
		form.setDtrealizada(atual);
		form.setDiasPrevistos(10);
		return form;
	}

	/**
	 * <b>M�todo respons�vel em tratar soma em datas</b>
	 * @param context
	 * @param form Ordemservico
	 * @author Biharck
	 * @throws ParseException 
	 */
	public String validaData(WebRequestContext context, Veiculoordemservico form, Boolean mode, Long dias) throws ParseException{
		
		Integer aux = null;
		Date date = null;
		if(mode == null){
			date = new Date(System.currentTimeMillis());
			aux = dias.intValue();
		}
		else if(mode) {
			aux = Integer.parseInt(context.getParameter("valor"));
			date = SinedDateUtils.stringToDate(context.getParameter("auxiliar"));
		}
		else{
			aux = dias.intValue();
			date = SinedDateUtils.stringToDate(context.getParameter("auxiliar"));
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		for (int i = 0; i < aux; i++) {
			if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ||
					calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY	|| !veiculoferiadoService.isFeriado(date)){
				date = SinedDateUtils.incrementDate(date, 1, Calendar.DAY_OF_WEEK);
				calendar.setTime(date);
				aux++;
			}else{
				date = SinedDateUtils.incrementDate(date, 1, Calendar.DAY_OF_WEEK);
				calendar.setTime(date);
			}
		}
		date = SinedDateUtils.incrementDate(date, -1, Calendar.DAY_OF_WEEK);
		String dataFinal = SinedDateUtils.toString(date);
		return dataFinal;
	}

	/**
	 * <b>M�todo respons�vel em tratar as opera��es aritm�ticas com datas<br>
	 * calculando assim os dias �teis em rela��o a uma data X</b>
	 * @author Biharck
	 * @param context
	 * @param ordemservico
	 * @param tipo onde � determinado como ser� usado o m�todo, com quais par�metros
	 */
	public Long calculaDias(Veiculoordemservico veiculoordemservico){
		String msg = "A data informada � um feriado";
		
		if(!veiculoferiadoService.isFeriado(new java.sql.Date(veiculoordemservico.getDtrealizada().getTime()))){
			throw new SinedException(msg);
		}
		long diferencaEntraDatas = 0;
		
		if(veiculoordemservico.getDtprevista() != null)
			diferencaEntraDatas = arithmeticOperationsWithDates(new java.sql.Date(veiculoordemservico.getDtrealizada().getTime()),new java.sql.Date(veiculoordemservico.getDtprevista().getTime()));
		return diferencaEntraDatas + 1;
	}
//
//	/**
//	 * <b>M�todo respons�vel por caregar uma lista de item de inspe��o atrav�s de uma ordem de servi�o, onde<br>
//	 * a lista de item de isnpe��o est� relacionada � uma ordem de servi�o</b>
//	 * @author Biharck
//	 * @param form
//	 */
//	public Ordemservico loadListaItemInspecao(Ordemservico form) {
//		//cria uma lista de ordemservicoitem
//		List<Ordemservicoitem> listaOrdemServicoItem = form.getListaOrdemServicoItem();
//		//se for a primeira vez que entrar
//		if((listaOrdemServicoItem != null && listaOrdemServicoItem.isEmpty()) || listaOrdemServicoItem == null){
//			//carrega listaiteminspecao
//			List<Iteminspecao> listaItemInspecao = iteminspecaoService.findAll();
//			//inicializa a lista de ordemservicoitem
//			listaOrdemServicoItem = new ArrayList<Ordemservicoitem>();
//			//iterar para cada item de inspecao
//			for (Iteminspecao iteminspecao : listaItemInspecao) {
//				//nova ordemservicoitem
//				Ordemservicoitem os = new Ordemservicoitem();
//				//add item de inspecao
//				os.setIteminspecao(iteminspecao);
//				//for default o status e false
//				os.setStatus(Boolean.FALSE);
//				//adiciona na lista
//				listaOrdemServicoItem.add(os);
//			}
//			//seta lista ordemservicoitem
//			form.setListaOrdemServicoItem(listaOrdemServicoItem);
//		}
//		return form;
//	}
//	
//	/**
//	 * <b> M�todo respons�vel em verificar a quantidade de itens de inspe��o que foram geradas<br>
//	 * manuten��es</b>
//	 * @param request
//	 * @param bean
//	 * @param listaOrdemServicoItem
//	 * @return quantidade de itens que foram geradaos manuten��es
//	 * @author Biharck
//	 */
//	public String qtdElementosInspecao(WebRequestContext request,
//			Ordemservico bean, List<Ordemservicoitem> listaOrdemServicoItem) {
//		Integer qtdElementos =0;
//		String valor="";
//		if (bean.getCdordemservico()!=null) {
//			qtdElementos = ordemservicoitemService.loadListaOrdemServicoItemByOrdemServico(bean,Boolean.TRUE).size(); 
//		}else {
//			qtdElementos = listaOrdemServicoItem.size();
//		}
//		if(qtdElementos>0){ 
//			valor  = ("Foi(ram) gerado(s) " + qtdElementos + " item(ns) pendente(s) neste lan�amento de inspe��o");
//		}else{
//			valor = ("N�o foram geradas manuten��es neste lan�amento de inspe��o");
//			valor += "\n" + ("Todos os itens est�o ok.");
//		}
//		return valor;
//	}
//	
//	/**
//	 * <b> M�todo respons�vel em verificar se existem elementos selecionados</b>
//	 * @param request
//	 * @param bean
//	 * @param listaOrdemServicoItem
//	 * @return quantidade de itens que foram geradaos manuten��es
//	 * @author Biharck
//	 */
//	public Boolean temElementos(WebRequestContext request,
//			Ordemservico bean, List<Ordemservicoitem> listaOrdemServicoItem) {
//		Integer qtdElementos =0;
//		Boolean valor = null;
//		if (bean.getCdordemservico()!=null) {
//			qtdElementos = ordemservicoitemService.loadListaOrdemServicoItemByOrdemServico(bean,Boolean.TRUE).size(); 
//		}else {
//			qtdElementos = listaOrdemServicoItem.size();
//		}
//		if(qtdElementos>0){ 
//			valor = Boolean.FALSE;
//		}else{
//			valor = Boolean.TRUE;
//		}
//		return valor;
//	}
//
//	/**
//	 *<b>M�todo respons�vel em carregar uma lista de Itens de inspe��o atrav�s de um status<br>
//	 * fornecido pela ordem de servi�o que pode ser pendente ou ok</b>
//	 * @param form
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#findByVeiculo(Veiculo veiculo)
//	 * @author Biharck
//	 */
//	public Ordemservico loadListaItemInspecaoByStatus(Ordemservico form) {
//		//cria uma lista de ordemservicoitem
//		List<Ordemservicoitem> listaOrdemServicoItem = form.getListaOrdemServicoItem();
//		//se for a primeira vez que entrar
//		if((listaOrdemServicoItem != null && listaOrdemServicoItem.isEmpty()) || listaOrdemServicoItem == null){
//			//carrega listaiteminspecao
//			List<Iteminspecao> listaItemInspecao = iteminspecaoService.findAll();
//			//inicializa a lista de ordemservicoitem
//			listaOrdemServicoItem = new ArrayList<Ordemservicoitem>();
//			//iterar para cada item de inspecao
//			for (Iteminspecao iteminspecao : listaItemInspecao) {
//				//nova ordemservicoitem
//				Ordemservicoitem os = new Ordemservicoitem();
//				//add item de inspecao
//				os.setIteminspecao(iteminspecao);
//				//for default o status e false
//				os.setStatus(Boolean.FALSE);
//				//adiciona na lista
//				listaOrdemServicoItem.add(os);
//			}
//			//seta lista ordemservicoitem
//			form.setListaOrdemServicoItem(listaOrdemServicoItem);
//		}
//		return form;
//	}
//
	/**
	 * <b>M�todo respons�vel em calcular a diferen�a entre duas datas onde s�o considerados<br>
	 * feriados, s�bados e domigos</b>
	 * @param data
	 * @param dataPrevista
	 * @return qtd de dias �teis
	 * @author Biharck
	 */
	public long arithmeticOperationsWithDates(Date data, Date dataPrevista) {
		long diferencaEntraDatas = dataPrevista.getTime() - data.getTime();
		diferencaEntraDatas = (diferencaEntraDatas/1000/60/60/24);
		int cont = 0;
		int contFeriados = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		List<Veiculoferiado> listaFeriado = veiculoferiadoService.listaIntervaloFeriado(data, dataPrevista);
		if (!listaFeriado.isEmpty()) {
			contFeriados = listaFeriado.size();
		}
		for (int i = 0; i <= diferencaEntraDatas; i++) {
			if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ||
					calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY	|| !veiculoferiadoService.isFeriado(data)){
				data = SinedDateUtils.incrementDate(data, 1, Calendar.DAY_OF_WEEK);
				calendar.setTime(data);
				cont++;
			}else{
				data = SinedDateUtils.incrementDate(data, 1, Calendar.DAY_OF_WEEK);
				calendar.setTime(data);
			}
		}
		cont = cont + contFeriados;
		diferencaEntraDatas = diferencaEntraDatas - cont;
		return diferencaEntraDatas;
	}

//	public Veiculoordemservico carregaOrdemServicoCompleto(Veiculoordemservico ordemservico){
//		return veiculoordemservicoDAO.carregaOrdemServicoCompleto(ordemservico);
//	}

	/**
	 * <b>M�todo de refer�ncia ao DAO</b>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#saveOrdemServico(Integer, Date)
	 * @author Jo�o Paulo Zica
	 * @author Jo�o Paulo Zica
	 * @param cdordemservico
	 * @param dtrealizada
	 */
	public void saveOrdemServico(Integer cdordemservico, Date dtrealizada){
		veiculoordemservicoDAO.saveOrdemServico(cdordemservico, dtrealizada);
	}
	/**
	 * M�todo que faz referencia ao DAO para buscar as ordens de sevi�os do tipo Agendamento 
	 * @author Ramon Brazil
	 * @param OrdemservicoFiltro
	 * @return List<Ordemservico>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#carregaOrdemServico(filtro)
	 */
	public List<Veiculoordemservico> carregaOrdemServico(VeiculoagendamentoFiltro filtro){
		return veiculoordemservicoDAO.carregaOrdemServico(filtro);
	}
	/**
	 * M�todo para gerar o relatorio Baixa de Manute��o 
	 * @author Ramon Brazil
	 * @param filtro
	 * @return report
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoitemService#findByItensForStatus(BaixamanutencaoFiltro)
	 */
	public Report createReportBaixaManutencao(BaixamanutencaoFiltro filtro) {
		List<BaixaManutencaoReportBean> listaBaixa = new ArrayList<BaixaManutencaoReportBean>();
		List<Veiculoordemservicoitem> listaordem = veiculoordemservicoitemService.findByItensForStatus(filtro);
		BaixaManutencaoReportBean beanBaixa = null;
		BaixaManutencaoSubReportBean beanItem = null;
		List<BaixaManutencaoSubReportBean> listaItem = null;
		for (Veiculoordemservicoitem ordemservicoitem : listaordem) {
			beanBaixa = new BaixaManutencaoReportBean();
			beanBaixa.setCooperado(ordemservicoitem.getOrdemservico().getVeiculo().getColaborador() != null ? ordemservicoitem.getOrdemservico().getVeiculo().getColaborador().getNome() : "");
			beanBaixa.setPlaca(ordemservicoitem.getOrdemservico().getVeiculo().getPlaca());
			beanBaixa.setPrefixo(ordemservicoitem.getOrdemservico().getVeiculo().getPrefixo());
			listaItem = new ArrayList<BaixaManutencaoSubReportBean>();
			beanItem = new BaixaManutencaoSubReportBean();
			beanItem.setTipoitens(ordemservicoitem.getInspecaoitem().getInspecaoitemtipo().getNome());
			beanItem.setDescricaoitens(ordemservicoitem.getInspecaoitem().getNome());
			beanItem.setLocal(ordemservicoitem.getLocalmanut());
			if(filtro.getStatus()){
				beanItem.setDtmanutencao(ordemservicoitem.getOrdemservico().getDtprevista());
			}
			else{
				beanItem.setDtmanutencao(ordemservicoitem.getOrdemservico().getDtrealizada());	
			}
			beanItem.setStatus(filtro.getStatus());
			beanItem.setObservacao(ordemservicoitem.getObservacao());
			listaItem.add(beanItem);
			beanBaixa.setListaManutencao(listaItem);
			listaBaixa.add(beanBaixa);
		}
		if (listaBaixa == null || listaBaixa.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report= new Report("veiculo/relBaixaManutencao");
			report.setDataSource(listaBaixa);
			report.addSubReport("SUBBAIXAMANUTENCAO", new Report("veiculo/relBaixaManutencaoSub"));	                     
			return report;
		}
	}
	/**
	 * M�todo para gerar o relatorio de agendamento
	 * @author Ramon Brazil
	 * @param OrdemservicoFiltro
	 * @return report
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoService#carregaOrdemServico(filtro)
	 */
	public Report createReportAgendamento(VeiculoagendamentoFiltro filtro) {
		List<AgendamentoReportBean> listaAgendamento = new ArrayList<AgendamentoReportBean>();
		List<Veiculoordemservico> listaOrdem = this.carregaOrdemServico(filtro);
		AgendamentoReportBean agendamento = null;
		
		for (Veiculoordemservico ordemservico : listaOrdem) {
			agendamento = new AgendamentoReportBean();
			agendamento.setCooperado(ordemservico.getVeiculo().getColaborador().getNome());
			agendamento.setModelo(ordemservico.getVeiculo().getVeiculomodelo().getNome());
			agendamento.setPlaca(ordemservico.getVeiculo().getPlaca());
			agendamento.setPrefixo(ordemservico.getVeiculo().getPrefixo());
			agendamento.setDtagendamento(ordemservico.getDtprevista());
			listaAgendamento.add(agendamento);
		}
		if (listaAgendamento == null || listaAgendamento.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report= new Report("veiculo/relAgendamento");
			report.addParameter("PERIODODATA", SinedUtil.getDescricaoPeriodo(new Date(filtro.getDtagendamento().getTime()), new Date(filtro.getDtfim().getTime())));
			report.setDataSource(listaAgendamento);	                     
			return report;
		}
	}
	/**
	 * M�todo para salvar uma ordemdeservico do tipo agendamento
	 * e o tipo de email enviado ou n�o
	 * @author Rafael odon, Biharck
	 * @param ordemservico
	 */
	public void saveAgendamento(Veiculoordemservico ordemservico, Boolean tipo){

		if(ordemservico == null){ //o cd pode ser nulo pois ele salva uma nova...
			throw new SinedException("Ordem de servico n�o pode ser nula.");
		}

		//configura a ordem de servi�o		
		Calendar dtRealizada = Calendar.getInstance();
		dtRealizada.setTimeInMillis(ordemservico.getDtprevista().getTime());
		ordemservico.setDtrealizada(dtRealizada.getTime());		
		ordemservico.setOrdemservicotipo(Ordemservicotipo.AGENDAMENTO);
		//salva a ordem de servi�o
		saveOrUpdate(ordemservico);
	}

	/**
	 * M�todo para gerar o relatorio de Comprovante de agendamento
	 * @author Ramon Brazil
	 * @param OrdemservicoFiltro
	 * @return report
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoService#carregaOrdemServico(filtro)
	 */
	public Report createReportComprovanteAgendamento(VeiculoagendamentoFiltro filtro) {
		List<VeiculoagendamentoReportBean> listaAgendamento = new ArrayList<VeiculoagendamentoReportBean>();
		List<Veiculoordemservico> listaOrdem = this.carregaOrdemServico(filtro);
		VeiculoagendamentoReportBean agendamento = null;
		for (Veiculoordemservico ordemservico : listaOrdem) {
			agendamento = new VeiculoagendamentoReportBean();
			agendamento.setCooperado(ordemservico.getVeiculo().getColaborador().getNome());
			agendamento.setModelo(ordemservico.getVeiculo().getVeiculomodelo().getNome());
			agendamento.setPlaca(ordemservico.getVeiculo().getPlaca());
			agendamento.setPrefixo(ordemservico.getVeiculo().getPrefixo());
			agendamento.setDtagendamento(ordemservico.getDtprevista());
			listaAgendamento.add(agendamento);
		}
		if (listaAgendamento == null || listaAgendamento.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report= new Report("veiculo/relComprovanteAgendamento");
			report.addParameter("LISTA",listaAgendamento);
			report.addSubReport("SUBCOMPROVANTE", new Report("veiculo/relComprovanteAgendamentoSub"));		                     
			return report;
		}	
	}

	/**
	 * <b>M�todo respons�vel em enviar e-mail para o associado que possuir itens de inspecao em atraso</b>
	 * @author Biharck
	 * @see br.com.linkcom.w3auto.geral.service.ParametroService#getValorParametro(Parametro paramtro)
	 * @param ordemservico
	 * @throws br.com.linkcom.w3auto.util.W3AutoException.W3AutoException
	 */
	public void enviaEmailAgendamento(Veiculoordemservico ordemservico){
		if(ordemservico == null)throw new SinedException("O objeto ordemservico n�o pode ser nulo");
		String template = null;
		String valor =  parametrogeralService.getValorPorNome("emailRespAgendVeiculo");
		String telPrincipal =  parametrogeralService.getValorPorNome("telRespAgendVeiculo");
		try {
			java.util.Date dt = ordemservico.getDtprevista();
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			template = new TemplateManager("/WEB-INF/template/templateEnvioEmailAgendamento.tpl")
			.assign("nome", ordemservico.getVeiculo().getColaborador().getNome())
			.assign("email", ordemservico.getVeiculo().getColaborador().getEmail())
			.assign("dtprevista", format.format(dt))
			.assign("emailPricipal", valor)
			.assign("telPrincipal", telPrincipal)
			.assign("placa", ordemservico.getVeiculo().getPlaca())
			.getTemplate();
		} catch (IOException e) {
			throw new SinedException("Erro de processamento de template :" + e);
		}
		EmailManager emailManager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		try {
			String assunto = "W3ERP - Agendamento dos itens de inspe��o";

			Envioemail envioemail = envioemailService.registrarEnvio(valor, assunto, template, Envioemailtipo.AVISO_AGENDAMENTO_INSPECAO, 
					ordemservico.getVeiculo().getColaborador(), ordemservico.getVeiculo().getColaborador().getEmail(), 
					ordemservico.getVeiculo().getColaborador().getNome(), emailManager);
			
			emailManager
			.setFrom(valor)
			.setSubject(assunto)
			.setTo(ordemservico.getVeiculo().getColaborador().getEmail())
			.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, ordemservico.getVeiculo().getColaborador().getEmail()))
			.sendMessage();
		} catch (Exception e) {
			throw new SinedException("Erro ao enviar e-mail :" + e);
		}
	}

//	/**
//	 * <b>M�todo respons�vel em criar uma nova ordem de servico em uma lista de OrdemServicoItemInspecao</b>
//	 * @param listaOrdemServicoItemInspecao
//	 * @param cds
//	 * @param listaCategoriaItemInspecao
//	 * @throws NumberFormatException
//	 * @author Biharck
//	 * @return java.util.list
//	 */
//	public List<Ordemservicoitem> preencheListaOrdemServicoItemInspecao(
//			List<Ordemservicoitem> listaOrdemServicoItemInspecao, String[] cds,
//			List<Categoriaiteminspecao> listaCategoriaItemInspecao)
//	throws NumberFormatException {
//		for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
//			//a lista recebe uma ordem de servico para cada �tem de inspe��o
//			listaOrdemServicoItemInspecao.add(new Ordemservicoitem(categoriaiteminspecao.getIteminspecao()));
//		}
//		for(Ordemservicoitem ordemservicoitem : listaOrdemServicoItemInspecao){
//			for(String cd : cds){
//				if(ordemservicoitem.getIteminspecao().getCditeminspecao().equals(Integer.parseInt(cd))){
//					ordemservicoitem.setStatus(Boolean.TRUE);
//				}
//			}
//		}
//		return listaOrdemServicoItemInspecao;
//	}
//
//	/**
//	 * <b>M�todo respons�vel em redirecionar para o jsp detalheOrdemServico uma lista de categoriaItemInspe��o<br>
//	 * a qual conter� uma lista de categoriaItemInspe��o</b>
//	 * @param bean
//	 * 
//	 * @param listaCategoriaItemInspecao
//	 * @return
//	 * @author Biharck
//	 */
//	public ModelAndView redirecionaJspCasoCriar(Ordemservico bean,List<Categoriaiteminspecao> listaCategoriaItemInspecao) {
//		Queue<Integer> objectIndexVisual = new PriorityQueue<Integer>();
//		int index ;
//		if(!bean.getOpcaoVisual()){
//			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
//				if(categoriaiteminspecao.getIteminspecao().getVisual()==false){
//					index =  listaCategoriaItemInspecao.indexOf(categoriaiteminspecao);
//					//aloca em uma fila
//					objectIndexVisual.add(index);
//				}
//			}
//		}
//
//		//percorre a fila at� achar a posi��o a qual � passada para ser retirada da lista...
//		for (Integer integer : objectIndexVisual) {
//			Integer posicao = objectIndexVisual.element();
//			listaCategoriaItemInspecao.remove(posicao.intValue());
//		}
//		bean.setListaCategoriaItemInspecao(listaCategoriaItemInspecao);
//		return new ModelAndView("direct:crud/detalheOrdemServico", "listaCategoriaItemInspecao", listaCategoriaItemInspecao);
//	}
//
	/**
	 * <b>M�todo de refer�ncia ao DAO</b>
	 * 
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#loadWithVeiculo(Ordemservico)
	 * @author Jo�o Paulo Zica
	 * @param ordemservico
	 * @return
	 */
	public Veiculoordemservico loadWithVeiculo(Veiculoordemservico ordemservico){
		return veiculoordemservicoDAO.loadWithVeiculo(ordemservico);
	}

	/**
	 * <b>M�todo respons�vel em retornar uma data prevista a partir de uma ordem servico</b>
	 * @see  br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#loadDtPrevista(Ordemservico ordemservico)
	 * @param ordemservico
	 * @author Biharck
	 * @return
	 */
	public Veiculoordemservico loadDtPrevista(Veiculoordemservico ordemservico){
		return veiculoordemservicoDAO.loadDtPrevista(ordemservico);
	}
	
//	/**
//	 * <b>M�todo respons�vel em retonrar uma ordem de servico do tipo manuten��o a partir de uma inspe��o</b>
//	 * @param ordemservico
//	 * @author Biharck
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoDAO#retornoManutencaoViaInspecao(Ordemservico ordemservico)
//	 * @return Ordemservico
//	 */
//	public Ordemservico retornoManutencaoViaInspecao(Ordemservico ordemservico){
//		return ordemservicoDAO.retornoManutencaoViaInspecao(ordemservico);
//	}
//	
	/**
	 * Gera uma nova ordem de manuten��o a partir da ordem de servi�o de inspe��o.
	 * 
	 * @see #findOsManutencaoByOSInspecao(Ordemservico)
	 * 
	 * @param bean
	 * @author Pedro Gon�alves
	 */
	public void generateOrdemManutencao(Veiculoordemservico bean){
		if (bean==null) {
			throw new SinedException("A propriedade n�o deve ser nula");
		}
		List<Veiculoordemservicoitem> listaOSIManutencao = new ArrayList<Veiculoordemservicoitem>();
		if (bean.getListaVeiculoordemservicoitem()!=null && !bean.getListaVeiculoordemservicoitem().isEmpty()) {
			for (Veiculoordemservicoitem ordemservicoitem : bean.getListaVeiculoordemservicoitem()) {
				if(ordemservicoitem.getStatus() != null && ordemservicoitem.getStatus()){
					Integer cdordemservicoitem = ordemservicoitem.getCdveiculoordemservicoitem();
					ordemservicoitem.setCdveiculoordemservicoitem(null);
					ordemservicoitem.setStatus(Boolean.FALSE);
					ordemservicoitem.setAtivo(Boolean.TRUE);
					ordemservicoitem.setOrdemservicoiteminspecao(new Veiculoordemservicoitem(cdordemservicoitem));
					
					listaOSIManutencao.add(ordemservicoitem);
				}
			}
		}
		
		Veiculoordemservico ordemservicoManutencao = null;
		
		if(bean != null && bean.getCdveiculoordemservico()!= null)
			ordemservicoManutencao = findOsManutencaoByOSInspecao(bean);
		
		if(ordemservicoManutencao == null)
			ordemservicoManutencao = new Veiculoordemservico();
		
		
		
		
		ordemservicoManutencao.setVeiculo(bean.getVeiculo());
		ordemservicoManutencao.setDtprevista(bean.getDtprevista());
		ordemservicoManutencao.setDtrealizada(bean.getDtrealizada());
		ordemservicoManutencao.setListaVeiculoordemservicoitem(listaOSIManutencao);
		ordemservicoManutencao.setOrdemservicotipo(Ordemservicotipo.MANUTENCAO);
		ordemservicoManutencao.setKm(bean.getKm());
		
		this.saveOrUpdate(ordemservicoManutencao);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * Encontra a ordem de servi�o do tipo manuten��o a partir de uma ordem de inspe��o.
	 * 
	 * @param ordemservico
	 * @author Pedro Gon�alves
	 * @return
	 */
	public Veiculoordemservico findOsManutencaoByOSInspecao(Veiculoordemservico ordemservico){
		return veiculoordemservicoDAO.findOsManutencaoByOSInspecao(ordemservico);
	}
	
	
	private static VeiculoordemservicoService instance;
	public static VeiculoordemservicoService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(VeiculoordemservicoService.class);
		}
		return instance;
	}
	/**
	 * M�todo para localizar agendamentos de inspe��o num intervalo de data.
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @author Fernando Boldrini
	 * @return
	 */
	public List<Veiculoordemservico> listIntervaloAgendamento(Date dtReferencia,
			Date dtReferenciaLimite, Veiculo veiculo) {
		return veiculoordemservicoDAO.listIntervaloAgendamento(dtReferencia, dtReferenciaLimite, veiculo);
	}
	/**
	 * M�todo para verificar se h� uso do ve�culo na data escolhida para a ordem de servi�o
	 * @param bean
	 * @return
	 */
	public boolean existeOrdemServicoMesmaData(Veiculouso bean) {
		return this.veiculoordemservicoDAO.existeOrdemServicoMesmaData(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean inspecaoPossuiManutencoesEmAberto(Veiculoordemservico form) {
		return veiculoordemservicoDAO.inspecaoPossuiManutencoesEmAberto(form);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeInspecaoData(Veiculoordemservico bean) {
		return veiculoordemservicoDAO.existeInspecaoData(bean);
	}
	
	public Veiculoordemservico criaVeiculoordemservicoByVeiculouso(Veiculouso veiculouso) {
		Veiculoordemservico bean = new Veiculoordemservico();
		if(veiculouso != null && veiculouso.getCdveiculouso() != null){
			veiculouso = veiculousoService.loadForInspecao(veiculouso);
			bean.setVeiculouso(veiculouso);
			bean.setVeiculo(veiculouso.getVeiculo());
			bean.setFromVeiculouso(true);
			Veiculo veiculo = veiculoService.loadForAgendamento(veiculouso.getVeiculo());
			if(veiculo != null && Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && veiculo.getVwveiculo() != null && veiculo.getVwveiculo().getKmatual() != null){
				bean.setKm(veiculo.getVwveiculo().getKmatual().longValue());
			}else if(veiculo != null && Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && veiculo.getVwveiculo() != null && veiculo.getVwveiculo().getHorimetroatual() != null){
				bean.setHorimetro(veiculo.getVwveiculo().getHorimetroatual());
			}
		}
		return bean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoDAO#findByVeiculouso(Veiculouso veiculouso)
	 *
	 * @param veiculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculoordemservico> findByVeiculouso(Veiculouso veiculouso) {
		return veiculoordemservicoDAO.findByVeiculouso(veiculouso);
	}
	
	/**
	 * M�todo que cria o formul�rio de inspe��o
	 *
	 * @param whereInInspecao
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport createReportFormularioInspecao(String whereInInspecao) {
		FormularioinspecaoFiltro filtro = new FormularioinspecaoFiltro();
		Report report= new Report("veiculo/relInspecaoVeicular");
		report.addSubReport("SUBFOMULARIOINSPECAO", new Report("veiculo/relInspecaoVeicularSub"));	 
		
		report.addParameter("VISUAL", false);
		report.addParameter("PREVENTIVA", true);
		report.addParameter("USOVEICULO", false);
		
		List<Veiculoordemservico> listaveVeiculoordemservico = this.findForInspecao(whereInInspecao);
		StringBuilder whereInVeiculo = new StringBuilder();
		for(Veiculoordemservico veiculoordemservico : listaveVeiculoordemservico){
			if(veiculoordemservico.getVeiculo() != null && veiculoordemservico.getVeiculo().getCdveiculo() != null){
				if(!whereInVeiculo.toString().equals("")) whereInVeiculo.append(",");
				whereInVeiculo.append(veiculoordemservico.getVeiculo().getCdveiculo());
			}
		}
		
		if(whereInVeiculo.toString().equals("")) throw new SinedException("N�o existe itens para emitir inspe��o.");
		
		filtro.setWhereInVeiculo(whereInVeiculo.toString());
		List<FormularioInspecaoReportBean> listaFormulario = veiculousoService.createListForInspecaoByVeiculouso(filtro);
		
		if(listaFormulario == null || listaFormulario.isEmpty()) 
			throw new SinedException("N�o existem itens para emitir inspe��o.");
		
		try {
			Image image = null;
			URL arqImagem = new URL(SinedUtil.getUrlWithContext() + "/imagens/veiculo/relatorio/inspecao_visual.gif");
			image = ImageIO.read(arqImagem.openStream());
			report.addParameter("IMAGEMINSPECAOVISUAL", image);
		} catch (Exception e) {}
		
		report.setDataSource(listaFormulario);
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoDAO#findForInspecao(String whereInInspecao)
	 *
	 * @param whereInInspecao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculoordemservico> findForInspecao(String whereInInspecao) {
		return veiculoordemservicoDAO.findForInspecao(whereInInspecao);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoDAO#findByVeiculo(Veiculo veiculo)
	 *
	 * @param veiculo
	 * @return Veiculoordemservico
	 * @author Jo�o Vitor
	 */
	public Veiculoordemservico findByVeiculo(Veiculo veiculo){
		return veiculoordemservicoDAO.findByVeiculo(veiculo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoDAO#findByVeiculo(Veiculo veiculo)
	 *
	 * @param veiculo
	 * @return Veiculoordemservico
	 * @author Jo�o Vitor
	 */
	public List<Veiculoordemservico> findUltimaManutencaoByVeiculoAndItemInspecao(Veiculo veiculo, String whereIn){
		return veiculoordemservicoDAO.findUltimaManutencaoByVeiculoAndItemInspecao(veiculo, whereIn);
	}
}

