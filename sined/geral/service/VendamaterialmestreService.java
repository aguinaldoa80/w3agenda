package br.com.linkcom.sined.geral.service;


import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.dao.VendamaterialmestreDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaMaterialMestreWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendamaterialmestreService extends GenericService<Vendamaterialmestre>{

	private VendamaterialmestreDAO vendamaterialmestreDAO;
	
	public void setVendamaterialmestreDAO(VendamaterialmestreDAO vendamaterialmestreDAO) {this.vendamaterialmestreDAO = vendamaterialmestreDAO;}


	public List<Vendamaterialmestre> findByVenda(Venda venda) {
		return vendamaterialmestreDAO.findByVenda(venda);
	}
	
	public void updateNomeAlternativo(Vendamaterialmestre vmm) {
		vendamaterialmestreDAO.updateNomeAlternativo(vmm);
	}
	
	public List<VendaMaterialMestreWSBean> toWSList(List<Vendamaterialmestre> listaVendaMaterialMestre){
		List<VendaMaterialMestreWSBean> lista = new ArrayList<VendaMaterialMestreWSBean>();
		if(SinedUtil.isListNotEmpty(listaVendaMaterialMestre)){
			for(Vendamaterialmestre vmm: listaVendaMaterialMestre){
				VendaMaterialMestreWSBean bean = new VendaMaterialMestreWSBean();
				bean.setAliquotaReaisIpi(vmm.getAliquotaReaisIpi());
				bean.setAltura(vmm.getAltura());
				bean.setCdvendamaterialmestre(vmm.getCdvendamaterialmestre());
				bean.setComprimento(vmm.getComprimento());
				bean.setDesconto(vmm.getDesconto());
				bean.setDtprazoentrega(new java.util.Date(vmm.getDtprazoentrega().getTime()));
				bean.setExibiritenskitflexivel(vmm.getExibiritenskitflexivel());
				bean.setGrupoTributacao(ObjectUtils.translateEntityToGenericBean(vmm.getGrupotributacao()));
				bean.setIdentificadorespecifico(vmm.getIdentificadorespecifico());
				bean.setIdentificadorinterno(vmm.getIdentificadorinterno());
				bean.setIpi(vmm.getIpi());
				bean.setLargura(vmm.getLargura());
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(vmm.getMaterial()));
				bean.setNomealternativo(vmm.getNomealternativo());
				bean.setPeso(vmm.getPeso());
				bean.setPreco(vmm.getPreco());
				bean.setQtde(vmm.getQtde());
				bean.setSaldo(vmm.getSaldo());
				bean.setTipoCalculoIpi(EnumUtils.translateEnum(vmm.getTipoCalculoIpi()));
				bean.setTipoCobrancaIpi(EnumUtils.translateEnum(vmm.getTipocobrancaipi()));
				bean.setUnidademedida(ObjectUtils.translateEntityToGenericBean(vmm.getUnidademedida()));
				bean.setValorIpi(vmm.getValoripi());
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vmm.getVenda()));
				
				lista.add(bean);
			}
		}
		return lista;
	}
}
