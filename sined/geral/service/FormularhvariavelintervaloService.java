package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.bean.Formularhvariavelintervalo;
import br.com.linkcom.sined.geral.dao.FormularhvariavelintervaloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormularhvariavelintervaloService extends GenericService<Formularhvariavelintervalo>{

	protected FormularhvariavelintervaloDAO formularhvariavelintervaloDAO;		
	public void setFormularhvariavelintervaloDAO(FormularhvariavelintervaloDAO formularhvariavelintervaloDAO) {this.formularhvariavelintervaloDAO = formularhvariavelintervaloDAO;}
	
	public List<Formularhvariavelintervalo> carregaIntervalos(Formularhvariavel formularhvariavel){
		return formularhvariavelintervaloDAO.carregaIntervalos(formularhvariavel);
	}
	
	private static FormularhvariavelintervaloService instance;
	public static FormularhvariavelintervaloService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FormularhvariavelintervaloService.class);
		}
		return instance;
	}
}
