package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulacusto;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulacusto.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulacusto.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.FormulatabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.TabelavalorVO;
import br.com.linkcom.sined.geral.dao.MaterialformulacustoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialformulacustoService extends GenericService<Materialformulacusto>{

	private MaterialService materialService;
	private MaterialformulacustoDAO materialformulacustoDAO;
	private TabelavalorService tabelavalorService;
	
	public void setMaterialformulacustoDAO(MaterialformulacustoDAO materialformulacustoDAO) {
		this.materialformulacustoDAO = materialformulacustoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	
	/**
	* M�todo que calcula o custo do material
	*
	* @param material
	* @return
	* @throws ScriptException
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public Double calculaCustoMaterial(Material material) throws ScriptException {
		Double custo = 0d;
		
		if(material != null){
			if(material.getListaMaterialformulacusto() != null && 
				material.getListaMaterialformulacusto().size() > 0){
				List<Materialformulacusto> listaMaterialformulacusto = material.getListaMaterialformulacusto();
				List<String> idsMateriais = new ArrayList<String>();
				idsMateriais.add(material.getCdmaterial().toString());
				
				Pattern pattern_tabelavalor = Pattern.compile("tabelavalor\\.[a-zA-Z]+\\('([a-zA-Z0-9_]+)'\\)");
				List<String> identificadoresTabelavalor = new ArrayList<String>();
				
				Pattern pattern_material = Pattern.compile("material\\.[a-zA-Z]+\\(([0-9]+)\\)");
				for (Materialformulacusto materialformulacusto : listaMaterialformulacusto) {
					if(materialformulacusto.getOrdem() == null ||
							materialformulacusto.getIdentificador() == null ||
							materialformulacusto.getFormula() == null){
						return 0d;
					}else {
						Matcher m_material = pattern_material.matcher(materialformulacusto.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
						
						Matcher m_tabelavalor = pattern_tabelavalor.matcher(materialformulacusto.getFormula());
						while(m_tabelavalor.find()){
							identificadoresTabelavalor.add(m_tabelavalor.group(1));
						}
					}
				}
				
				FormulaVO formulaVO = new FormulaVO();
				formulaVO.setCdmaterial(material.getCdmaterial());
				
				
				if(idsMateriais.size() > 0){
					List<Material> listaMaterial = materialService.findForCalcularValorcusto(CollectionsUtil.concatenate(idsMateriais, ","));
					for (Material m : listaMaterial) {
						MaterialVO materialVO = new MaterialVO();
						materialVO.setCdmaterial(m.getCdmaterial());
						materialVO.setValorcustomaterialprima(m.getValorcustomateriaprima() != null ? m.getValorcustomateriaprima().getValue().doubleValue() : 0.0);
						if(m.getMaterialgrupo() != null){
							materialVO.setRateiooperacional(m.getMaterialgrupo().getRateiocustoproducao() != null ? m.getMaterialgrupo().getRateiocustoproducao() : 0d);
						}
						if(m.getCdmaterial() != null && m.getCdmaterial().equals(material.getCdmaterial())){
							materialVO.setValorcustomaterialprima(material.getValorcustomateriaprima() != null ? material.getValorcustomateriaprima().getValue().doubleValue() : 0);
							formulaVO.setAux_materialVO(materialVO);
						}
						formulaVO.addMaterial(materialVO);
					}
				}
				
				List<Tabelavalor> listaTabelavalor = tabelavalorService.findByIdentificador(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO + "," + Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO);
				formulaVO.setCustooperacionaladministrativo(tabelavalorService.getValor(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO, listaTabelavalor));
				formulaVO.setCustocomercialadministrativo(tabelavalorService.getValor(Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO, listaTabelavalor));
				
				FormulatabelavalorVO formulatabelavalorVO = new FormulatabelavalorVO();
				if(identificadoresTabelavalor.size() > 0){
					listaTabelavalor = tabelavalorService.findByIdentificador(CollectionsUtil.concatenate(identificadoresTabelavalor, ","));
					for (Tabelavalor tabelavalor : listaTabelavalor) {
						formulatabelavalorVO.addTabelavalor(new TabelavalorVO(tabelavalor));
					}
				}
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				engine.put("material", formulaVO);
				engine.put("tabelavalor", formulatabelavalorVO);
				
				Collections.sort(listaMaterialformulacusto, new Comparator<Materialformulacusto>(){
					public int compare(Materialformulacusto o1, Materialformulacusto o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Materialformulacusto materialformulacusto : listaMaterialformulacusto) {
					Object obj = engine.eval(materialformulacusto.getFormula());
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(materialformulacusto.getIdentificador(), resultado);
					custo = resultado;
				}
			}  else if(material.getValorcusto() != null){
				custo = material.getValorcusto();
			}
		}
		
		return custo;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialformulacustoDAO#findByMaterial(Material material)
	*
	* @param material
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulacusto> findByMaterial(Material material) {
		return materialformulacustoDAO.findByMaterial(material);
	}
	
	public void saveReplicarFormulacusto(List<Materialformulacusto> listaMaterialformulacusto, Material material) {
		if(material != null && material.getCdmaterial() != null && listaMaterialformulacusto != null && 
				!listaMaterialformulacusto.isEmpty()){
			if(material.getListaMaterialformulacusto() != null && !material.getListaMaterialformulacusto().isEmpty()){
				deleteFormulacustoByMaterial(material);
			}
			List<Materialformulacusto> listaMfc = new ArrayList<Materialformulacusto>();
			for(Materialformulacusto materialformulacusto : listaMaterialformulacusto){
				Materialformulacusto mfc = new Materialformulacusto();
				mfc.setCdmaterialformulacusto(null);
				mfc.setOrdem(materialformulacusto.getOrdem());
				mfc.setIdentificador(materialformulacusto.getIdentificador());
				mfc.setFormula(materialformulacusto.getFormula());
				mfc.setMaterial(material);
				saveOrUpdate(mfc);
				listaMfc.add(mfc);
			}
			material.setListaMaterialformulacusto(listaMfc);
		}
		
	}
	
	private void deleteFormulacustoByMaterial(Material material) {
		materialformulacustoDAO.deleteFormulacustoByMaterial(material);
	}
	
	/**
	 * Calcula o custo do material que tenha usado na f�rmula o rateio operacional
	 *
	 * @author Rodrigo Freitas
	 * @since 27/10/2015
	 */
	public void calculaCustoMaterialWithRateiooperacional(WebRequestContext request) {
		List<Material> listaMaterial = materialService.findForCalculocustoRateiooperacional();
		for (Material material : listaMaterial) {
			try{
				Double custo = this.calculaCustoMaterial(material);
				if(custo != null){
					materialService.updateValorCusto(material, custo);
				}
			} catch (Exception e) {
				e.printStackTrace();
				if(request != null){
					request.addError("Erro ao calcular o custo do material " + material.getCdmaterial() + ": " + e.getMessage());
				}
			}
		}
	}
}
