package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovenda;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovendaitem;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendafinalizadora;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.PrazoPagamentoECF;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.EmporiumvendaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.DeleteCupomCancelVendaBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.EmporiumAcertoExclusaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumvendaService extends GenericService<Emporiumvenda> {

	private EmporiumvendaDAO emporiumvendaDAO;
	private ParametrogeralService parametrogeralService;
	private EmpresaService empresaService;
	private VendaService vendaService;
	private VendahistoricoService vendahistoricoService;
	private MaterialService materialService;
	private DocumentotipoService documentotipoService;
	private ColaboradorService colaboradorService;
	private ClienteService clienteService;
	private PrazopagamentoService prazopagamentoService;
	private ContaService contaService;
	private DocumentoService documentoService;
	private RateioService rateioService;
	private MovimentacaoService movimentacaoService;
	private PedidovendaService pedidovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumvendaitemService emporiumvendaitemService;
	private EmporiumvendafinalizadoraService emporiumvendafinalizadoraService;
	private NotaVendaService notaVendaService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private PedidovendamaterialmestreService pedidovendamaterialmestreService;
	private NotaService notaService;
	private NotaHistoricoService notaHistoricoService;
	private PedidovendamaterialService pedidovendamaterialService;
	private FormapagamentoService formapagamentoService;
	private EmporiumpedidovendaService emporiumpedidovendaService;
	private ReservaService reservaService;
	private UnidademedidaService unidademedidaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ConfiguracaonfeService configuracaonfeService;
	
	public void setPedidovendamaterialService(
			PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setPedidovendapagamentoService(
			PedidovendapagamentoService pedidovendapagamentoService) {
		this.pedidovendapagamentoService = pedidovendapagamentoService;
	}
	public void setNotaVendaService(NotaVendaService notaVendaService) {
		this.notaVendaService = notaVendaService;
	}
	public void setEmporiumvendafinalizadoraService(
			EmporiumvendafinalizadoraService emporiumvendafinalizadoraService) {
		this.emporiumvendafinalizadoraService = emporiumvendafinalizadoraService;
	}
	public void setEmporiumvendaitemService(
			EmporiumvendaitemService emporiumvendaitemService) {
		this.emporiumvendaitemService = emporiumvendaitemService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setPedidovendahistoricoService(
			PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setPrazopagamentoService(
			PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setVendahistoricoService(
			VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEmporiumvendaDAO(EmporiumvendaDAO emporiumvendaDAO) {
		this.emporiumvendaDAO = emporiumvendaDAO;
	}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}
	
	public List<Emporiumvenda> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		return emporiumvendaDAO.findForSped(emporiumpdv, dtinicio, dtfim);
	}
	public void setPedidovendamaterialmestreService(PedidovendamaterialmestreService pedidovendamaterialmestreService) {
		this.pedidovendamaterialmestreService = pedidovendamaterialmestreService;
	}
	public void setEmporiumpedidovendaService(EmporiumpedidovendaService emporiumpedidovendaService) {
		this.emporiumpedidovendaService = emporiumpedidovendaService;
	}
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmporiumvendaDAO.loadTicketCadastrado(String loja, String pdv, String numeroTicket)
	 *
	 * @param numeroTicket
	 * @return
	 * @author Rodrigo Freitas
	 * @param string2 
	 * @param string 
	 * @since 15/04/2013
	 */
	public Emporiumvenda loadTicketCadastrado(String loja, String pdv, String numeroTicket) {
		return emporiumvendaDAO.loadTicketCadastrado(loja, pdv, numeroTicket);
	}
	
	public void processaVendasPendentes(Arquivoemporium arquivoemporium, boolean job, String loja_pendente, String whereInEmporiumvenda) {
		if(whereInEmporiumvenda == null && !parametrogeralService.getBoolean(Parametrogeral.EMPORIUM_VENDA_CRIAR)){
			return;
		}
		
		Integer cdempresa = parametrogeralService.getInteger(Parametrogeral.EMPORIUM_VENDA_EMPRESA);
		Integer cdcolaborador = parametrogeralService.getInteger(Parametrogeral.EMPORIUM_VENDA_COLABORADOR);
		Integer cdcliente = parametrogeralService.getInteger(Parametrogeral.EMPORIUM_VENDA_CLIENTE);
		Integer cdprazopagamento = parametrogeralService.getInteger(Parametrogeral.EMPORIUM_VENDA_PRAZOPAGAMENTO);
		Integer cdconta = parametrogeralService.getInteger(Parametrogeral.EMPORIUM_VENDA_CONTA);
		
		Empresa empresa = empresaService.load(new Empresa(cdempresa), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.integracaopdv");
		Colaborador colaborador = colaboradorService.load(new Colaborador(cdcolaborador));
		Cliente cliente = clienteService.load(new Cliente(cdcliente));
		Prazopagamento prazopagamento = prazopagamentoService.load(new Prazopagamento(cdprazopagamento));
		Conta conta = contaService.loadForEntrada(new Conta(cdconta));
		
		if(empresa == null){
			throw new SinedException("N�o foi encontrado o empresa com o id " + cdempresa);
		}
		if(colaborador == null){
			throw new SinedException("N�o foi encontrado o colaborador com o id " + cdcolaborador);
		}
		if(cliente == null){
			throw new SinedException("N�o foi encontrado o cliente com o id " + cdcliente);
		}
		if(prazopagamento == null){
			throw new SinedException("N�o foi encontrado o prazo de pagamento com o id " + cdprazopagamento);
		}
		if(conta == null){
			throw new SinedException("N�o foi encontrada a conta com o id " + cdconta);
		}
		
		List<Colaborador> listaColaborador = colaboradorService.findForIntegracaoEmporium();
		List<Emporiumpdv> listaPdv = emporiumpdvService.findForIntegracao();
		List<Empresa> listaEmpresa = empresaService.findForIntegracaoEmporium();
		List<Emporiumvenda> lista = this.findPendentes(arquivoemporium, loja_pendente, whereInEmporiumvenda);
		Calendario calendario = CalendarioService.getInstance().getCalendarioUnificado(false, null, null);
		
		Map<String, Conta> mapConta = new HashMap<String, Conta>();
		Map<Integer, Material> mapMaterial = new HashMap<Integer, Material>();
		Map<Integer, Documentotipo> mapDocumentotipo = new HashMap<Integer, Documentotipo>();
		
		for (Emporiumvenda emporiumvenda : lista) {
			try {
				String numero_ticket = emporiumvenda.getNumero_ticket();
				String pdv = emporiumvenda.getPdv();
				String loja = emporiumvenda.getLoja();
				Nota nota = this.getNotaFromAcertoByTicket(numero_ticket, pdv, loja);
				
				List<Emporiumvendafinalizadora> listaEmporiumvendafinalizadora = emporiumvendafinalizadoraService.findByEmporiumvenda(emporiumvenda);
				List<Emporiumvendaitem> listaEmporiumvendaitem = emporiumvendaitemService.findByEmporiumvenda(emporiumvenda);
				
				Integer idMaterialFreteInt = null;
				String idMaterialFrete = parametrogeralService.getValorPorNome(Parametrogeral.EMPORIUM_VENDA_FRETEMATERIAL);
				if(idMaterialFrete != null && !idMaterialFrete.trim().equals("")){
					try{
						idMaterialFreteInt = Integer.parseInt(idMaterialFrete);
					} catch (Exception e) {}
				}
				
				Double valorFrete = 0.0;
				Double valorDesconto = emporiumvenda.getValor_desconto();
				if(valorDesconto == null){
					valorDesconto = 0d;
				}
				for (Emporiumvendaitem it : listaEmporiumvendaitem) {
					if(it.getDesconto() != null && it.getDesconto() > 0){
						valorDesconto -= it.getDesconto();
					}
					if(idMaterialFreteInt != null && it.getProduto_id().equals(idMaterialFreteInt)){
						valorFrete = it.getValortotal();
					}
				}
				
				Date data = new Date(emporiumvenda.getData_hora().getTime());
				
				Empresa empresavenda = IntegracaoEmporiumUtil.util.getEmpresaEmporium(loja, listaEmpresa);
				
				if(existeVendaCupom(numero_ticket, loja, pdv, empresavenda, emporiumvenda.getValor())){
					continue;
				}
				
	//			CRIAR VENDA
				Venda venda = new Venda();
				
				
				
			
				venda.setValorfrete(new Money(valorFrete));
				venda.setPresencacompradornfe(Presencacompradornfe.PRESENCIAL);
				
				if(empresavenda != null){
					venda.setEmpresa(empresavenda);
				} else {
					venda.setEmpresa(empresa);
				}
				
				Conta contavenda = conta;
				if(!mapConta.containsKey(pdv)){
					Emporiumpdv emporiumpdv = IntegracaoEmporiumUtil.util.getPDVEmporium(loja, pdv, listaPdv);
					if(emporiumpdv != null && emporiumpdv.getConta() != null){
						contavenda = contaService.loadForEntrada(emporiumpdv.getConta());
					}
					mapConta.put(pdv, contavenda);
				} else {
					contavenda = mapConta.get(pdv);
				}
				
				Colaborador colaboradorvenda = colaborador;
				String vendedor = emporiumvenda.getVendedor_id();
				if(vendedor != null && !vendedor.trim().equals("")){
					Colaborador colaborador_aux = IntegracaoEmporiumUtil.util.getColaboradorEmporium(vendedor, listaColaborador);
					if(colaborador_aux != null){
						colaboradorvenda = colaborador_aux;
					}
				}
				
				if(parametrogeralService.getBoolean(Parametrogeral.EMPORIUM_VENDA_VENDEDOR)){
					if(listaEmporiumvendaitem != null && listaEmporiumvendaitem.size() >0){
						Emporiumvendaitem itemVenda = listaEmporiumvendaitem.get(0);
						if(itemVenda.getVendedor_id() != null){
							Colaborador colaborador_aux = IntegracaoEmporiumUtil.util.getColaboradorEmporium(itemVenda.getVendedor_id(), listaColaborador);
							if(colaborador_aux != null){
								colaboradorvenda = colaborador_aux;
							}
						}
					}
				}	
				venda.setColaborador(colaboradorvenda);
				
				venda.setCliente(cliente);
				if(emporiumvenda.getCliente_id() != null){
					try{
						Cliente clientevenda = new Cliente(Integer.parseInt(emporiumvenda.getCliente_id()));
						clientevenda = clienteService.load(clientevenda);
						
						if(clientevenda != null){
							venda.setCliente(clientevenda);
						}
					} catch (Exception e) {
		//				e.printStackTrace();
					}
				}
				if(emporiumvenda.getCliente_cpfcnpj() != null && !"".equals(emporiumvenda.getCliente_cpfcnpj().trim())){
					try{
						String cpfcnpj = emporiumvenda.getCliente_cpfcnpj();
						if(cpfcnpj.length() == 11){
							Cpf cpf = new Cpf(cpfcnpj);
							Cliente clientevenda = clienteService.findByCpf(cpf);
							if(clientevenda != null){
								venda.setCliente(clientevenda);
							}
						} else if(cpfcnpj.length() == 14){
							Cnpj cnpj = new Cnpj(cpfcnpj);
							Cliente clientevenda = clienteService.findByCnpj(cnpj);
							if(clientevenda != null){
								venda.setCliente(clientevenda);
							}
						}
					} catch (Exception e) {
		//				e.printStackTrace();
					}
				}
				
				venda.setFrete(ResponsavelFrete.SEM_FRETE);
				venda.setDtvenda(data);
				venda.setIdentificadorexterno(emporiumvenda.getNumero_ticket());
				venda.setPrazomedio(true);
				
			
				if(SinedUtil.isListNotEmpty(emporiumvenda.getListaEmporiumvendafinalizadora())){
					for(Emporiumvendafinalizadora finalizadora : emporiumvenda.getListaEmporiumvendafinalizadora()){	
						List<Documentotipo> listaDocTipo = documentotipoService.findByCodECF(finalizadora.getFinalizadora_id(), null);
						if(listaDocTipo.size() == 1){
							Documentotipo documentoTipo = listaDocTipo.get(0);
							if(SinedUtil.isListNotEmpty(documentoTipo.getListaPrazoPagamentoECF())){
								for (PrazoPagamentoECF prazoECF : documentoTipo.getListaPrazoPagamentoECF()) {
									if(prazoECF.getCodECF().equals(finalizadora.getFinalizadora_id()) && prazoECF.getPrazoPagamento() !=null){
										prazopagamento = prazopagamentoService.load(prazoECF.getPrazoPagamento());
									}	
								}
							}		
						}
					}	
				}
				venda.setPrazopagamento(prazopagamento);
				
				venda.setQtdeParcelas(listaEmporiumvendafinalizadora.size());
				venda.setDesconto(new Money(valorDesconto));
				venda.setContaboleto(contavenda);
				
				if(nota != null){
					venda.setVendasituacao(Vendasituacao.FATURADA);
				} else {
					venda.setVendasituacao(Vendasituacao.REALIZADA);
				}
				
				List<Pedidovendamaterial> listaPedidovendamaterial = null;
				List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre = null;
				List<Integer> idsItensUsados = new ArrayList<Integer>();
				List<Integer> idsEmporiumpedidovendaitemUsados = new ArrayList<Integer>();
				Pedidovenda pedidovenda = null;
				Emporiumpedidovenda emporiumpedidovenda = null;;
				boolean naoGerarReceita = false;
				boolean ajusteEstoque = true;
				Integer dias_apos = 0;
				
				if(emporiumvenda.getPedidovenda() != null && emporiumvenda.getPedidovenda() > 0){
					pedidovenda = new Pedidovenda(emporiumvenda.getPedidovenda());
					pedidovenda = pedidovendaService.loadForEntrada(pedidovenda);
					if(pedidovenda != null){
						emporiumpedidovenda = emporiumpedidovendaService.findByPedidovenda(pedidovenda);
						dias_apos = SinedDateUtils.diferencaDias(SinedDateUtils.dateToBeginOfDay(data), SinedDateUtils.dateToBeginOfDay(pedidovenda.getDtpedidovenda()));
						
						listaPedidovendamaterial = pedidovenda.getListaPedidovendamaterial();
						listaPedidovendamaterialmestre = pedidovendamaterialmestreService.findByPedidovenda(pedidovenda);
						List<Pedidovendapagamento> listaPedidovendapagamento = pedidovendapagamentoService.findByPedidovenda(pedidovenda);
						for (Pedidovendapagamento pedidovendapagamento : listaPedidovendapagamento) {
							if(pedidovendapagamento != null && pedidovendapagamento.getDocumento() != null){
								naoGerarReceita = true;
							}
						}
						pedidovenda.setListaPedidovendapagamento(listaPedidovendapagamento);
						
						venda.setColaborador(pedidovenda.getColaborador());
						venda.setCliente(pedidovenda.getCliente());
						venda.setEndereco(pedidovenda.getEndereco());
						venda.setFrete(pedidovenda.getFrete());
						venda.setProjeto(pedidovenda.getProjeto());
						venda.setPedidovendatipo(pedidovenda.getPedidovendatipo());
						venda.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
						venda.setValorusadovalecompra(pedidovenda.getValorusadovalecompra());
//						venda.setDesconto(pedidovenda.getDesconto());
						
						if(pedidovenda.getPedidovendatipo() != null){
							if(pedidovenda.getPedidovendatipo().getBaixaestoqueEnum() != null && (BaixaestoqueEnum.APOS_EMISSAONOTA.equals(pedidovenda.getPedidovendatipo().getBaixaestoqueEnum()) || 
									BaixaestoqueEnum.NAO_BAIXAR.equals(pedidovenda.getPedidovendatipo().getBaixaestoqueEnum()) ||
									BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(pedidovenda.getPedidovendatipo().getBaixaestoqueEnum()))){
								ajusteEstoque = false;
							}
							
							if(pedidovenda.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
								!GeracaocontareceberEnum.APOS_VENDAREALIZADA.equals(pedidovenda.getPedidovendatipo().getGeracaocontareceberEnum())){
								naoGerarReceita = true;
							}
						}
					}
				}
				venda.setPedidovenda(pedidovenda);
				
				Emporiumpdv emporiumpdv = IntegracaoEmporiumUtil.util.getPDVEmporium(loja, pdv, listaPdv);
				if(emporiumpdv != null && emporiumpdv.getLocalarmazenagem() != null){
					venda.setLocalarmazenagem(emporiumpdv.getLocalarmazenagem());
				}
				
//				Double valorTotalDesconto = 0d;
				if(listaPedidovendamaterial != null && listaPedidovendamaterial.size() > 0){
					if((pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0) ||
							(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0)){
						Double valorvalecompra = pedidovenda.getValorusadovalecompra() != null ? pedidovenda.getValorusadovalecompra().getValue().doubleValue() : 0d;
						Double valordesconto = pedidovenda.getDesconto() != null ? pedidovenda.getDesconto().getValue().doubleValue() : 0d;
						
						Double desconto = valordesconto + valorvalecompra;
						Double valortotal = pedidovenda.getTotalvenda().getValue().doubleValue();
						Double descontoRestante = desconto;
						Double valorvalecompraRestante = valorvalecompra;
						
						// FEITO ISSO POIS O VALOR VEM COM O DESCONTO APLICADO
						valortotal += desconto != null ? desconto : 0d;
						
						for (int i = 0; i < listaPedidovendamaterial.size(); i++) {
							Pedidovendamaterial it = listaPedidovendamaterial.get(i);
							
							Double valor_it = it.getTotalproduto().getValue().doubleValue();
							if(it.getDesconto() != null){
								valor_it -= it.getDesconto().getValue().doubleValue();
							}
							
							Double percent = (valor_it * 100d)/valortotal;
							Double desconto_it = SinedUtil.round((percent * desconto)/100d, 2);
							Double valorvalecompra_it = SinedUtil.round((percent * valorvalecompra)/100d, 2);
							
							descontoRestante -= desconto_it;
							if((i+1) == listaPedidovendamaterial.size()){
//								desconto_it += descontoRestante;
							}
							
							valorvalecompraRestante -= valorvalecompra_it;
							if((i+1) == listaPedidovendamaterial.size()){
//								valorvalecompra_it = valorvalecompraRestante;
							}
							
							it.setDesconto_it(desconto_it);
							it.setValorvalecomprapropocional(valorvalecompra_it);
							
//							if((i+1) == listaPedidovendamaterial.size() && valorvalecompra_it > 0){
//								valorTotalDesconto = SinedUtil.round((desconto_it > valorvalecompra_it ? desconto_it - valorvalecompra_it : valorvalecompra_it - desconto_it), 2);
//							}
						}
					}
				}
				
	//			CRIAR ITENS DA VENDA
				List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
				List<Pedidovendamaterialmestre> listaPVMM = new ArrayList<Pedidovendamaterialmestre>();
				Double valorUsadoValeCompra = 0d;
				boolean verificarDesconto = false;
				Double valorTotalDescsontoCalculado = 0d;
				boolean existeValorUsadoValecompraPedidovenda = getExisteValorUsadoValecompra(emporiumpedidovenda); 
				for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitem) {
					if(idMaterialFreteInt != null && emporiumvendaitem.getProduto_id().equals(idMaterialFreteInt)){
						continue;
					}
					
					Material material = null;
					if(!mapMaterial.containsKey(emporiumvendaitem.getProduto_id())){
						material = materialService.carregaMaterial(new Material(emporiumvendaitem.getProduto_id()));
						mapMaterial.put(emporiumvendaitem.getProduto_id(), material);
					} else {
						material = mapMaterial.get(emporiumvendaitem.getProduto_id());
					}
					if(material == null){
						throw new SinedException("N�o foi encontrado o material com o id " + emporiumvendaitem.getProduto_id());
					}
					
					Vendamaterial vendamaterial = new Vendamaterial();
					vendamaterial.setMaterial(material);
					vendamaterial.setUnidademedida(material.getUnidademedida());
					vendamaterial.setPreco(emporiumvendaitem.getValorunitario());
					vendamaterial.setQuantidade(emporiumvendaitem.getQuantidade());
					vendamaterial.setDesconto(emporiumvendaitem.getDesconto() != null ? new Money(emporiumvendaitem.getDesconto()) : null);
					vendamaterial.setTotal(new Money(emporiumvendaitem.getValortotal()));
					vendamaterial.setPrazoentrega(data);
					
					try{
						if(emporiumvendaitem.getKit() != null && !"".equals(emporiumvendaitem.getKit()) && !"0".equals(emporiumvendaitem.getKit())){
							String kit = emporiumvendaitem.getKit();
							kit = kit.split(" ")[0];
							
							Integer cdmaterialmestre = Integer.parseInt(kit.trim());
							Material materialmestre =  materialService.load(new Material(cdmaterialmestre), "material.cdmaterial, material.nome");
							
							if(materialmestre == null){
								throw new SinedException("N�o foi encontrado o material mestre '" + emporiumvendaitem.getKit() + "'");
							}
							vendamaterial.setMaterialmestre(materialmestre);
						}
					} catch (Exception e) {
						throw new SinedException("Problema na identifica��o do Kit do item", e);
					}
					
					try{
						if(listaPedidovendamaterial != null && listaPedidovendamaterial.size() > 0){
							for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
								boolean materialIgual = pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().equals(material);
								boolean qtdeIgual = pedidovendamaterial.getQuantidade() != null && (pedidovendamaterial.getQuantidade().equals(emporiumvendaitem.getQuantidade()) || qtdeIgualComDiferencaMinima(listaPedidovendamaterial, material, materialIgual, pedidovendamaterial.getQuantidade(), emporiumvendaitem.getQuantidade())) ;
								boolean naoAdicionado = idsItensUsados.size() == 0 || !idsItensUsados.contains(pedidovendamaterial.getCdpedidovendamaterial());
								Double valorvalecompraItem = 0d;
								
								if(materialIgual && qtdeIgual && naoAdicionado){
									verificarDesconto = true;
									Emporiumpedidovendaitem emporiumpedidovendaitem = getEmporiumpedidovendaitem(emporiumpedidovenda, pedidovendamaterial.getMaterial().getCdmaterial(), pedidovendamaterial.getQuantidade(), idsEmporiumpedidovendaitemUsados);
									if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){
										if(existeValorUsadoValecompraPedidovenda){
											if(emporiumpedidovendaitem != null){
												if(emporiumpedidovendaitem.getValorvalecompra() != null){
													valorvalecompraItem = SinedUtil.round(emporiumpedidovendaitem.getValorvalecompra(), 2);
													valorUsadoValeCompra += SinedUtil.round(emporiumpedidovendaitem.getValorvalecompra(), 2);
													if(emporiumvendaitem.getDesconto() != null && emporiumvendaitem.getDesconto() > 0){
														if(emporiumvendaitem.getDesconto() >= emporiumpedidovendaitem.getValorvalecompra()){
															pedidovendamaterial.setDesconto_it(emporiumvendaitem.getDesconto() - emporiumpedidovendaitem.getValorvalecompra());
															vendamaterial.setDesconto(new Money(pedidovendamaterial.getDesconto_it()));
														}else if(valorvalecompraItem > 0){
															pedidovendamaterial.setDesconto_it(valorvalecompraItem - emporiumvendaitem.getDesconto());
															valorUsadoValeCompra -= pedidovendamaterial.getDesconto_it();
															vendamaterial.setDesconto(new Money(pedidovendamaterial.getDesconto_it()));
														}else {
															pedidovendamaterial.setDesconto_it(0d);
														}
														
													}
												}
												idsEmporiumpedidovendaitemUsados.add(emporiumpedidovendaitem.getCdemporiumpedidovendaitem());
											}
										}else if(pedidovendamaterial.getValorvalecomprapropocional() != null && pedidovendamaterial.getValorvalecomprapropocional() > 0){
											valorvalecompraItem = SinedUtil.round(pedidovendamaterial.getValorvalecomprapropocional(), 2);
											valorUsadoValeCompra += SinedUtil.round(pedidovendamaterial.getValorvalecomprapropocional(), 2);
											if(emporiumvendaitem.getDesconto() != null && emporiumvendaitem.getDesconto() > 0){
												if(emporiumvendaitem.getDesconto() >= valorvalecompraItem){
													pedidovendamaterial.setDesconto_it(emporiumvendaitem.getDesconto() - valorvalecompraItem);
													vendamaterial.setDesconto(new Money(pedidovendamaterial.getDesconto_it()));
												}else if(valorvalecompraItem > 0){
													pedidovendamaterial.setDesconto_it(valorvalecompraItem - emporiumvendaitem.getDesconto());
													valorUsadoValeCompra -= pedidovendamaterial.getDesconto_it();
													vendamaterial.setDesconto(new Money(pedidovendamaterial.getDesconto_it()));
												}else {
													pedidovendamaterial.setDesconto_it(0d);
												}
												
											}
										}
									}else {
										pedidovendamaterial.setDesconto_it(emporiumvendaitem.getDesconto());
									}
									if(pedidovendamaterial.getQuantidade() != null 
											&& !pedidovendamaterial.getQuantidade().equals(emporiumvendaitem.getQuantidade()) 
											&& qtdeIgualComDiferencaMinima(listaPedidovendamaterial, material, materialIgual, pedidovendamaterial.getQuantidade(), emporiumvendaitem.getQuantidade())){
										vendamaterial.setQuantidade(pedidovendamaterial.getQuantidade());
									}
									
									if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0){
										valorTotalDescsontoCalculado += vendamaterial.getDesconto().getValue().doubleValue();
									}
									vendamaterial.setPedidovendamaterial(pedidovendamaterial);
                                    if(pedidovendamaterial.getUnidademedida() != null){
                                        vendamaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
                                    }
									vendamaterial.setFatorconversao(pedidovendamaterial.getFatorconversao());
									vendamaterial.setQtdereferencia(pedidovendamaterial.getQtdereferencia());
									vendamaterial.setLoteestoque(pedidovendamaterial.getLoteestoque());
									if(pedidovendamaterial.getMaterialmestre() != null && listaPedidovendamaterialmestre != null && 
											!listaPedidovendamaterialmestre.isEmpty()){
										vendamaterial.setMaterialmestre(pedidovendamaterial.getMaterialmestre());
										vendamaterial.setIdentificadorinterno(pedidovendamaterial.getIdentificadorinterno());
										Pedidovendamaterialmestre pvmm = pedidovendaService.getPVMForVenda(listaPedidovendamaterialmestre, pedidovendamaterial.getMaterialmestre());
										if(pvmm != null && !listaPVMM.contains(pvmm)){
											listaPVMM.add(pvmm);
										}
									}
									
									idsItensUsados.add(pedidovendamaterial.getCdpedidovendamaterial());
									break;
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro na verifica��o do pedido de venda.");
					}
					
					if(vendamaterial.getValorcustomaterial() == null && material != null){
						vendamaterial.setValorcustomaterial(material.getValorcusto());
					}
					listaVendamaterial.add(vendamaterial);
				}
				venda.setListavendamaterial(listaVendamaterial);
				if(verificarDesconto){
					Double totalDescontoComValeCompra = valorTotalDescsontoCalculado + valorUsadoValeCompra;
					if(emporiumvenda.getValor_desconto() != null && ((emporiumvenda.getValor_desconto() - totalDescontoComValeCompra) < -0.05 || (emporiumvenda.getValor_desconto() - totalDescontoComValeCompra) > 0.05)){
						venda.setDesconto(new Money(emporiumvenda.getValor_desconto()));
						valorUsadoValeCompra = 0d;
						for(Vendamaterial vendamaterial : listaVendamaterial){
							vendamaterial.setDesconto(new Money());
						}
					}
//					else if(valorTotalDesconto != null && valorTotalDesconto > 0 && valorTotalDescsontoCalculado > 0){
//						Double diferenca = SinedUtil.round(SinedUtil.round(valorTotalDescsontoCalculado, 2) - SinedUtil.round(valorTotalDesconto, 2), 2);
//						if(diferenca != 0){
//							for(Vendamaterial vendamaterial : listaVendamaterial){
//								if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0){
//									if(vendamaterial.getDesconto().getValue().doubleValue() >= diferenca){
//										vendamaterial.setDesconto(new Money(vendamaterial.getDesconto().getValue().doubleValue() - diferenca));
//										break;
//									}else {
//										diferenca -= vendamaterial.getDesconto().getValue().doubleValue();
//										vendamaterial.setDesconto(new Money());
//									}
//									if(diferenca == 0){
//										break;
//									}
//								}
//							}
//						}
//					}
				}
				
				try{
					if(listaPVMM != null && !listaPVMM.isEmpty()){
						venda.setListaVendamaterialmestre(pedidovendaService.preecheVendaMaterialmestre(listaPVMM));
					}
				} catch (Exception e) {
					e.printStackTrace();
					IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro ao preencher a lista dos materiais mestre do pedido na venda.");
				}
				
				venda.setValorusadovalecompra(new Money(valorUsadoValeCompra));
				
	//			CRIAR PARCELAS DA VENDA
				boolean pedidoParcela = parametrogeralService.getBoolean(Parametrogeral.EMPORIUM_VENDA_CONSIDERAR_PEDIDO_PARCELA);
				if(venda.getPedidovenda() != null && pedidoParcela){
					venda.setPrazomedio(venda.getPedidovenda().getPrazomedio());
					venda.setPrazopagamento(venda.getPedidovenda().getPrazopagamento());
					venda.setContaboleto(venda.getPedidovenda().getConta());
					venda.setDocumentotipo(venda.getPedidovenda().getDocumentotipo());
					
					List<Vendapagamento> listaVendapagamento = pedidovendaService.preencheListaVendaPagamento(venda.getPedidovenda().getListaPedidovendapagamento());
					if(dias_apos > 0){
						for (Vendapagamento vendapagamento : listaVendapagamento) {
							vendapagamento.setDataparcela(SinedDateUtils.addDiasData(vendapagamento.getDataparcela(), dias_apos));
						}
					}
					venda.setListavendapagamento(listaVendapagamento);
				} else {
					Map<Integer, Double> mapFinalizadora = new HashMap<Integer, Double>();
					Double troco = 0d;
					
					for (Emporiumvendafinalizadora emporiumvendafinalizadora : listaEmporiumvendafinalizadora) {
						String flag = emporiumvendafinalizadora.getFlag();
						if(flag == null) flag = "+";
						boolean soma = flag.trim().equals("+");
						
						if(soma){
							Double valor = 0d;
							if(mapFinalizadora.containsKey(emporiumvendafinalizadora.getFinalizadora_id())){
								valor = mapFinalizadora.get(emporiumvendafinalizadora.getFinalizadora_id());
							}
							valor += emporiumvendafinalizadora.getValor();
							mapFinalizadora.put(emporiumvendafinalizadora.getFinalizadora_id(), valor);
						} else {
							troco += emporiumvendafinalizadora.getValor();
						}
					}
					
					List<Vendapagamento> listaVendapagamento = new ArrayList<Vendapagamento>();
					Set<Entry<Integer, Double>> entrySet = mapFinalizadora.entrySet();
					for (Entry<Integer, Double> entry : entrySet) {
						Double valor = entry.getValue();
						Integer finalizadora_id = entry.getKey();
						
						if(valor <= 0d) continue;
						
						if(troco >= valor){
							troco -= valor;
							continue;
						} else {
							valor -= troco;
							troco = 0d;
						}
						
						Documentotipo documentoTipo = null;
						if(!mapDocumentotipo.containsKey(finalizadora_id)){
							List<Documentotipo> listaDocumentoTipo = documentotipoService.findByCodECF(finalizadora_id,null);
							if(SinedUtil.isListEmpty(listaDocumentoTipo)){
								System.out.println("banco_dados "+SinedUtil.getStringConexaoBancoIntegracao());
								throw new SinedException("N�o foi encontrado o tipo de documento com o id da finalizadora " + finalizadora_id);
							}
							documentoTipo = listaDocumentoTipo.get(0);
							mapDocumentotipo.put(finalizadora_id, documentoTipo);
						} else {
							documentoTipo = mapDocumentotipo.get(finalizadora_id);
						}
						
						if(documentoTipo == null){
							throw new SinedException("N�o foi encontrado o tipo de documento com o id da finalizadora " + finalizadora_id);
						}
						
						Date data_aux = new Date(data.getTime());
						if(documentoTipo.getDiasreceber() != null && documentoTipo.getDiasreceber() > 0){
							data_aux = SinedDateUtils.incrementaDia(data_aux, documentoTipo.getDiasreceber(), calendario);
						}
						Conta contaDestino = documentoTipo.getContadestino();
						Venda vendaParaCalculo = new Venda();
						
						Prazopagamento prazoParcelas = venda.getPrazopagamento();
						for (PrazoPagamentoECF prazoECF : documentoTipo.getListaPrazoPagamentoECF()) {
							if(prazoECF.getCodECF().equals(finalizadora_id)){
								prazoParcelas = prazoECF.getPrazoPagamento();
							}
						}
						
						vendaParaCalculo.setPrazopagamento(prazoParcelas);
						vendaParaCalculo.setValor(valor);
						vendaParaCalculo.setQtdeParcelas(null);
						vendaParaCalculo.setDtvenda(data_aux);
						vendaParaCalculo.setEmpresa(venda.getEmpresa());
						
						if(SinedUtil.isListNotEmpty(documentoTipo.getListaPrazoPagamentoECF())) {
							for (PrazoPagamentoECF prazoECF : documentoTipo.getListaPrazoPagamentoECF()) {
								if(prazoECF.getCodECF().equals(finalizadora_id)){
									Prazopagamento pagamentos = prazopagamentoService.loadAll(prazoECF.getPrazoPagamento());
//									if(Boolean.TRUE.equals(pagamentos.getAvista())){
//										listaVendapagamento.add(crirarVendaPagamento(contaDestino,documentoTipo,data_aux,new Money(valor)));
//									}else 
									if(SinedUtil.isListNotEmpty(pagamentos.getListaPagamentoItem())){
											venda.setPrazopagamento(pagamentos);
											AjaxRealizarVendaPagamentoBean retorno = vendaService.pagamento(vendaParaCalculo,null);
											if(retorno !=null && SinedUtil.isListNotEmpty(retorno.getLista())){
												for (AjaxRealizarVendaPagamentoItemBean itemPagamento : retorno.getLista()) {
													String valorPorParcela = itemPagamento.getValor().replace(".", "").replace(",", ".");
													double valorP = Double.parseDouble(valorPorParcela);
													listaVendapagamento.add(crirarVendaPagamento(contaDestino,documentoTipo,itemPagamento.getDtparceladuplicata(),new Money(valorP)));
												}
											}else{
												listaVendapagamento.add(crirarVendaPagamento(contaDestino,documentoTipo,data_aux,new Money(valor)));
											}
									} else{
										listaVendapagamento.add(crirarVendaPagamento(contaDestino,documentoTipo,data_aux,new Money(valor)));
									}
									
								}
							}
						}else{
							listaVendapagamento.add(crirarVendaPagamento(contaDestino,documentoTipo,data_aux,new Money(valor)));
						}
					}
					venda.setListavendapagamento(listaVendapagamento);
				}
				vendaService.saveOrUpdate(venda);
				/*if(StringUtils.isNotBlank(emporiumvenda.getXml_env())){
					String string = new String(Util.strings.tiraAcento(emporiumvenda.getXml_env()).getBytes(), "UTF-8");
					Arquivo arquivoXmlNota = new Arquivo(string.getBytes(), "nfceenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
					emporiumvenda.setArquivoXmlNota(arquivoXmlNota);
					ImportarXMLEstadoBean beanNfe = notafiscalprodutoService.processarXmlNfceEmporium(emporiumvenda.getXml_env());
					Configuracaonfe configuracaoNfe = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR, beanNfe.getEmpresa());
					
					notafiscalprodutoService.importarArquivo(null, beanNfe.getNotaXML(), emporiumvenda.getArquivoXmlNota(), beanNfe, configuracaoNfe, venda);
				}*/
				
				if(pedidovenda != null){
					boolean confirmado_total = true;
					for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
						Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
						if(qtdeRestante == null || qtdeRestante > 0){
							confirmado_total = false;
							break;
						}
					}
					
					Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
					pedidovendahistorico.setPedidovenda(pedidovenda);
					pedidovendahistorico.setObservacao("Venda criada via Emporium: <a href=\"javascript:visualizarVenda(" + 
							venda.getCdvenda() + 
							");\">" + 
							venda.getCdvenda() + 
							"</a>");
					if(confirmado_total){
						pedidovendahistorico.setAcao("Confirmado");
						pedidovendaService.updateConfirmacao(venda.getPedidovenda().getCdpedidovenda().toString());
					} else {
						pedidovendahistorico.setAcao("Confirmado parcialmente");
						pedidovendaService.updateConfirmacaoParcial(venda.getPedidovenda().getCdpedidovenda().toString());
					}
					pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
					if(SinedUtil.isListNotEmpty(venda.getListavendamaterial())){
						for(Vendamaterial vm: venda.getListavendamaterial()){
							if(vm.getPedidovendamaterial() != null){
								Double quantidade = (vm.getFatorconversao() != null && vm.getFatorconversao() > 0) ? (vm.getQuantidade() / vm.getFatorconversaoQtdereferencia()) : 
															unidademedidaService.getQtdeUnidadePrincipal(vm.getMaterial(),
																	vm.getUnidademedida(),
																	vm.getQuantidade());
								reservaService.desfazerReserva(vm.getPedidovendamaterial(), vm.getMaterial(), quantidade);
							}
						}
					}
				}
				
				venda.setEmporiumDocumentoacao(Documentoacao.DEFINITIVA);
	//			CRIAR AS CONTAS A RECEBER DAS PARCELAS DA VENDA
				List<Rateioitem> listarateioitem = vendaService.prepareAndSaveVendaMaterial(venda, ajusteEstoque, venda.getEmpresa(), false, null);
				List<Integer> listaIdsDocumentos = vendaService.prepareAndSaveReceita(venda, listarateioitem, venda.getEmpresa(), naoGerarReceita);
				
	//			CRIAR O HIST�RICO DA VENDA
				Vendahistorico vendahistorico = new Vendahistorico();
				vendahistorico.setAcao("Cria��o venda");
				vendahistorico.setObservacao("Criado a partir da integra��o com o ECF." + 
						(pedidovenda != null ? (" Referente ao pedido de venda <a href=\"javascript:visualizarPedidovenda(" + 
								pedidovenda.getCdpedidovenda() + 
								")\">" + 
								pedidovenda.getCdpedidovenda() + 
								"</a>") : ""));
				vendahistorico.setVenda(venda);
				vendahistoricoService.saveOrUpdate(vendahistorico);
				
				if(nota != null){
					nota = notaService.carregaDadosNota(nota);
					
					Vendahistorico vendahistoricoNota = new Vendahistorico();
					vendahistoricoNota.setAcao("Nota vinculada");
					vendahistoricoNota.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNota("+nota.getCdNota()+");\">"+(nota.getNumero() != null ? nota.getNumero() : nota.getCdNota() )+"</a>.");
					vendahistoricoNota.setVenda(venda);
					vendahistoricoService.saveOrUpdate(vendahistoricoNota);
					
					NotaVenda notavenda = new NotaVenda();
					notavenda.setNota(nota);
					notavenda.setVenda(venda);
					notaVendaService.saveOrUpdate(notavenda);
					
					String obs = "Venda criada novamente: <a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>.";
					NotaHistorico notaHistorico = new NotaHistorico(nota, obs, nota.getNotaStatus());
					notaHistoricoService.saveOrUpdate(notaHistorico);
				}
				
	//			ATUALIZA A VENDA
				this.updateVenda(emporiumvenda, venda);
				
				
				List<Integer> idsFaturado = new ArrayList<Integer>();
				try {
					String paramFaturado = parametrogeralService.getValorPorNome(Parametrogeral.EMPORIUM_VENDA_FATURADO);
					if(paramFaturado != null && !paramFaturado.trim().equals("")){
						String[] idsFaturadoArray = paramFaturado.split(";");
						for (String id : idsFaturadoArray) {
							try {
								idsFaturado.add(Integer.parseInt(id.trim()));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
	//			BAIXAR AS CONTAS A RECEBER DAS PARCELAS DA VENDA
				for (Integer cddocumento : listaIdsDocumentos) {
					Documento documento = new Documento(cddocumento);
					documento = documentoService.loadForEntrada(documento);	
					
					if(documento != null){
						// SE TIVER O ID IGUAL AO PAR�METRO N�O BAIXAR A CONTA
						Documentotipo documentotipo = documento.getDocumentotipo();
						if(idsFaturado.size() > 0 && 
								documentotipo != null && 
								documentotipo.getCddocumentotipo() != null &&
								idsFaturado.contains(documentotipo.getCddocumentotipo())){
							continue;
						}
						
						Conta contabaixa = null;
						if(documentotipo != null && 
								documentotipo.getContadestino() != null && 
								documentotipo.getContadestino().getCdconta() != null){
							contabaixa = documentotipo.getContadestino(); 
						}
						
						Date dtvencimento = documento.getDtvencimento();
						Money valor = documentoService.getValorAtual(cddocumento, dtvencimento);
						if(valor == null){
							valor = documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null ? documento.getAux_documento().getValoratual() : documento.getValor();
						}
						
						BaixarContaBean baixarContaBean = new BaixarContaBean();
						List<Documento> listaDocumento = new ArrayList<Documento>();
						
						Rateio rateio = rateioService.findByDocumento(documento);
						rateioService.atualizaValorRateio(rateio, documento.getValor());
						
						documento.setRateio(rateio);
						documento.setValoratual(valor);
						
						listaDocumento.add(documento);
						
						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						
						baixarContaBean.setListaDocumento(listaDocumento);
						baixarContaBean.setRateio(documento.getRateio());
						baixarContaBean.setDtpagamento(dtvencimento);
						baixarContaBean.setDtcredito(dtvencimento);
						baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
						
						if(documento.getDocumentotipo() != null && documento.getDocumentotipo().getCddocumentotipo() != null){
							Documentotipo documentotipoBean = new Documentotipo(documento.getDocumentotipo().getCddocumentotipo());
							documentotipoBean = documentotipoService.load(documentotipoBean);
							
							if(documentotipoBean != null && documentotipoBean.getFormapagamentocredito() != null){
								baixarContaBean.setFormapagamento(formapagamentoService.load(documentotipoBean.getFormapagamentocredito()));
							}
						}
						
						baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
						baixarContaBean.setVinculo(contabaixa != null ? contabaixa : contavenda);
						baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						baixarContaBean.setChecknum(sdf.format(dtvencimento));
						baixarContaBean.setFromWebservice(true);
						
						Movimentacao movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
//						contareceberService.incrementaNumDocumento(documento);
						
						documentoService.callProcedureAtualizaDocumento(documento);
						if(movimentacao != null){
							movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
							movimentacaoService.updateHistoricoWithDescricao(movimentacao);
						}
					} else {
						throw new SinedException("Documento " + cddocumento + " n�o encontrado para fazer a baixa.");
					}
				}
				
				boolean registrarValecompra = true;
				String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
				if (!"VENDA".equalsIgnoreCase(param_utilizacao_valecompra) || (venda.getVendasituacao() != null && Vendasituacao.AGUARDANDO_APROVACAO.equals(venda.getVendasituacao()))) {
					registrarValecompra = false;
				}

				if (registrarValecompra) {
					vendaService.registrarUsoValecompra(venda, false);
				}
			} catch (Exception e) {
				e.printStackTrace();
				if(job){
					IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro no Job para cria��o de vendas.");
				} else {
					NeoWeb.getRequestContext().addError(e.getMessage());
				}
			}
		}
	}
	
	private boolean qtdeIgualComDiferencaMinima(List<Pedidovendamaterial> listaPedidovendamaterial, Material material, boolean materialIgual, Double quantidadePVM, Double quantidadeEmporium) {
		if(materialIgual && SinedUtil.isListNotEmpty(listaPedidovendamaterial) && material != null && quantidadePVM != null && quantidadeEmporium != null){
			int countMaterial = 0;
			for(Pedidovendamaterial pvm : listaPedidovendamaterial){
				if(pvm.getMaterial() != null && pvm.getMaterial().equals(material)){
					countMaterial++;
				}
			}
			if(countMaterial == 1){
				Double diferenca = quantidadePVM - quantidadeEmporium;
				System.out.println(SinedUtil.round(diferenca, 3));
				if(SinedUtil.round(diferenca, 3) <= 0.01 && SinedUtil.round(diferenca, 3) >= -0.01){
					return true;
				}
			}
		}
		return false;
	}
	private Vendapagamento crirarVendaPagamento(Conta contaDestino,Documentotipo documentoTipo, Date data_aux, Money valor) {
		Vendapagamento vendaPagamento = new Vendapagamento();
		vendaPagamento.setContadestino(documentoTipo.getContadestino());
		vendaPagamento.setDocumentotipo(documentoTipo);
		vendaPagamento.setDataparcela(data_aux);
		vendaPagamento.setValororiginal(valor);
		return vendaPagamento;
	}
	private boolean existeVendaCupom(String numeroTicket, String loja, String pdv, Empresa empresavenda, Double valor) {
		return emporiumvendaDAO.existeVendaCupom(numeroTicket, loja, pdv, empresavenda, valor);
	}
	
	private boolean getExisteValorUsadoValecompra(Emporiumpedidovenda emporiumpedidovenda) {
		if(emporiumpedidovenda != null && SinedUtil.isListNotEmpty(emporiumpedidovenda.getListaEmporiumpedidovendaitem())){
			for(Emporiumpedidovendaitem item : emporiumpedidovenda.getListaEmporiumpedidovendaitem()){
				if(item.getValorvalecompra() != null){
					return true;
				}
			}
		}
		return false;
	}
	
	private Emporiumpedidovendaitem getEmporiumpedidovendaitem(Emporiumpedidovenda emporiumpedidovenda, Integer cdmaterial, Double quantidade, List<Integer> idsEmporiumpedidovendaitemUsados) {
		if(emporiumpedidovenda != null && SinedUtil.isListNotEmpty(emporiumpedidovenda.getListaEmporiumpedidovendaitem()) &&
				cdmaterial != null && quantidade != null){
			for(Emporiumpedidovendaitem item : emporiumpedidovenda.getListaEmporiumpedidovendaitem()){
				if(item.getCdemporiumpedidovendaitem() != null && cdmaterial.equals(item.getProduto_id()) && quantidade.equals(item.getQuantidade()) &&
						!idsEmporiumpedidovendaitemUsados.contains(item.getCdemporiumpedidovendaitem())){
					idsEmporiumpedidovendaitemUsados.add(item.getCdemporiumpedidovendaitem());
					return item;
				}
			}
		}
		return null;
	}
	
	private Nota getNotaFromAcertoByTicket(String numeroTicket, String pdv, String loja) {
		return emporiumvendaDAO.getNotaFromAcertoByTicket(numeroTicket, pdv, loja);
	}
	
	private List<Emporiumvenda> findPendentes(Arquivoemporium arquivoemporium, String loja, String whereInEmporiumvenda) {
		return emporiumvendaDAO.findPendentes(arquivoemporium, loja, whereInEmporiumvenda);
	}
	
	public List<Emporiumvenda> findForValidacao(String whereIn) {
		return emporiumvendaDAO.findForValidacao(whereIn);
	}
	
	private void updateVenda(Emporiumvenda emporiumvenda, Venda venda) {
		emporiumvendaDAO.updateVenda(emporiumvenda, venda);
	}

	/* singleton */
	private static EmporiumvendaService instance;
	public static EmporiumvendaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EmporiumvendaService.class);
		}
		return instance;
	}
	
	public boolean haveTicketDia(Emporiumpdv emporiumpdv, Date dataInicio) {
		return emporiumvendaDAO.haveTicketDia(emporiumpdv, dataInicio);
	}
	
	public boolean haveVenda(Venda venda) {
		return emporiumvendaDAO.haveVenda(venda);
	}
	
	public boolean fromVendaemporium(Nota nota) {
		return emporiumvendaDAO.fromVendaemporium(nota);
	}
	
	public void makeExclusao(final EmporiumAcertoExclusaoFiltro filtro) {
		emporiumvendaDAO.getTransactionTemplate().execute(new TransactionCallback() {
			@Override
			public Object doInTransaction(TransactionStatus status) {
				// CRIA TABELA TEMPOR�RIA DE VINCULA��O DE NOTA COM A VENDA
				try{
					emporiumvendaDAO.getJdbcTemplate().execute("create table emporiumnotavenda (cdnota integer, numero_ticket varchar);");
				} catch (Exception e) {}
				try{
					emporiumvendaDAO.getJdbcTemplate().execute("alter table emporiumnotavenda add pdv varchar;");
				} catch (Exception e) {}
				try{
					emporiumvendaDAO.getJdbcTemplate().execute("alter table emporiumnotavenda add loja varchar;");
				} catch (Exception e) {}
				
				emporiumvendaDAO.getJdbcTemplate().execute("insert into emporiumnotavenda (cdnota, numero_ticket, pdv, loja) " +
															"select nv.cdnota, ev.numero_ticket, ev.pdv, ev.loja " +
															"from notavenda nv " +
															"join emporiumvenda ev on ev.cdvenda = nv.cdvenda " +
															"join nota n on n.cdnota = nv.cdnota " +
															"where ev.numero_ticket is not null " +
															"and ev.numero_ticket <> '' " +
															"and n.cdnotastatus not in (3, 13, 14); ");
				emporiumvendaDAO.getJdbcTemplate().execute("ALTER TABLE public.vendapagamento DROP CONSTRAINT fk_vendapagamento_1 RESTRICT;");
				emporiumvendaDAO.getJdbcTemplate().execute("ALTER TABLE public.vendapagamento ADD CONSTRAINT fk_vendapagamento_1 FOREIGN KEY (cddocumento) REFERENCES public.documento(cddocumento) ON DELETE SET NULL ON UPDATE NO ACTION NOT DEFERRABLE;");
				try{
					emporiumvendaDAO.getJdbcTemplate().execute("create table vendadeletada (cdvenda integer primary key);");
				} catch (Exception e) {}
				
				String filtroStr = "where 1 = 1 ";
				if(filtro.getEmpresa() != null){
					filtroStr += "and v.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ";
				}
				if(filtro.getData1() != null){
					filtroStr += "and v.dtvenda >= '" + SinedDateUtils.toStringPostgre(filtro.getData1()) + "' ";
				}
				if(filtro.getData2() != null){
					filtroStr += "and v.dtvenda <= '" + SinedDateUtils.toStringPostgre(filtro.getData2()) + "' ";
				}
				if(filtro.getVendas() != null && !filtro.getVendas().equals("")){
					while(filtro.getVendas().indexOf(",,") != -1){
						filtro.setVendas(filtro.getVendas().replaceAll(",,", ","));
					}
					if(filtro.getVendas().substring(0, 1).equals(",")){
						filtro.setVendas(filtro.getVendas().substring(1, filtro.getVendas().length()));
					}
					if(filtro.getVendas().substring(filtro.getVendas().length() - 1, filtro.getVendas().length()).equals(",")){
						filtro.setVendas(filtro.getVendas().substring(0, filtro.getVendas().length()-1));
					}
					
					filtroStr += "and v.cdvenda in (" + filtro.getVendas() + ") ";
				}
				emporiumvendaDAO.getJdbcTemplate().execute("insert into vendadeletada select v.cdvenda from venda v " + filtroStr + ";");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from movimentacao where cdmovimentacao in (select m.cdmovimentacao from movimentacao m join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao join documento d on d.cddocumento = mo.cddocumento join vendapagamento vp on vp.cddocumento = d.cddocumento where vp.cdvenda in (select vd.cdvenda from vendadeletada vd));");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from documentoorigem where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from arquivobancariodocumento where cddocumento in (select d.cddocumento from documento d join vendapagamento vp on vp.cddocumento = d.cddocumento where vp.cdvenda in (select vd.cdvenda from vendadeletada vd));");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from documento where cddocumento in (select d.cddocumento from documento d join vendapagamento vp on vp.cddocumento = d.cddocumento where vp.cdvenda in (select vd.cdvenda from vendadeletada vd));");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from vendahistorico where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from vendapagamento where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from materialdevolucao where cdvendamaterial in (select vm.cdvendamaterial from vendamaterial vm join vendadeletada vd on vd.cdvenda = vm.cdvenda);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from expedicaoitem where cdvendamaterial in (select vm.cdvendamaterial from vendamaterial vm join vendadeletada vd on vd.cdvenda = vm.cdvenda);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from vendamaterial where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from movimentacaoestoque where cdmovimentacaoestoque in (select me.cdmovimentacaoestoque from movimentacaoestoque me join movimentacaoestoqueorigem mo on mo.cdmovimentacaoestoqueorigem = me.cdmovimentacaoestoqueorigem where mo.cdvenda in (select vd.cdvenda from vendadeletada vd));");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from movimentacaoestoqueorigem where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from notavenda where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from emporiumvenda where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("delete from venda where cdvenda in (select vd.cdvenda from vendadeletada vd);");
				emporiumvendaDAO.getJdbcTemplate().execute("drop table vendadeletada;");
				
				return null;
			}
		});
	}
	
	public List<Emporiumvenda> findForAcerto(Date dtinicio, Date dtfim) {
		return emporiumvendaDAO.findForAcerto(dtinicio, dtfim);
	}
	
	public List<Emporiumvendaitem> getEmporiumvendaitemForSpedAcerto(List<Emporiumvenda> listaEmporiumvenda) {
		List<Emporiumvendaitem> listaEmporiumvendaitem = new ArrayList<Emporiumvendaitem>();
		for (Emporiumvenda emporiumvenda : listaEmporiumvenda) {
			if(emporiumvenda.getListaEmporiumvendaitem() != null && emporiumvenda.getListaEmporiumvendaitem().size() > 0){
				Double valortotal = emporiumvenda.getValor();
				
				Double desconto = emporiumvenda.getValor_desconto();
				for (Emporiumvendaitem it : emporiumvenda.getListaEmporiumvendaitem()) {
					if(it.getDesconto() != null && it.getDesconto() > 0){
						desconto -= it.getDesconto();
					}
				}
				
				Double descontoRestante = desconto != null ? new Double(desconto) : 0d;
				
				// FEITO ISSO POIS O VALOR VEM COM O DESCONTO APLICADO
				valortotal += desconto != null ? desconto : 0d;
				
				for (int i = 0; i < emporiumvenda.getListaEmporiumvendaitem().size(); i++) {
					Emporiumvendaitem it = emporiumvenda.getListaEmporiumvendaitem().get(i);
					it.setNumero_ticket(emporiumvenda.getNumero_ticket());
					
					if(desconto != null && desconto > 0){
						Double valor_it = it.getValortotal();
						Double percent = (valor_it * 100d)/valortotal;
						Double desconto_it = (percent * desconto)/100d;
						
						descontoRestante -= desconto_it;
						if((i+1) == emporiumvenda.getListaEmporiumvendaitem().size()){
							desconto_it += descontoRestante;
						}
						
						it.setDesconto_it(desconto_it);
					}
					listaEmporiumvendaitem.add(it);
				}
			}
		}
		return listaEmporiumvendaitem;
	}
	
	public List<Emporiumvendaitem> getEmporiumvendaitemByDataPDV(List<Emporiumvendaitem> listaEmporiumvendaitem, Emporiumpdv emporiumpdv, Date data) {
		List<Emporiumvendaitem> lista = new ArrayList<Emporiumvendaitem>();
		
		Integer codigoemporium = null;
		String loja = emporiumpdv.getEmpresa() != null ? emporiumpdv.getEmpresa().getIntegracaopdv() : null;
		try{
			codigoemporium = Integer.parseInt(emporiumpdv.getCodigoemporium());
		} catch (Exception e) {}
		
		if(codigoemporium != null){
			for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitem) {
				if(emporiumvendaitem.getEmporiumvenda() != null &&
						emporiumvendaitem.getEmporiumvenda().getPdv() != null){
					String pdv = emporiumvendaitem.getEmporiumvenda().getPdv();
					try{
						if(codigoemporium.equals(Integer.parseInt(pdv)) &&
								emporiumvendaitem.getEmporiumvenda().getLoja() != null && emporiumvendaitem.getEmporiumvenda().getLoja().equals(loja) &&
								emporiumvendaitem.getEmporiumvenda().getData_hora() != null &&
								SinedDateUtils.equalsIgnoreHour(SinedDateUtils.timestampToDate(emporiumvendaitem.getEmporiumvenda().getData_hora()), data)){
							lista.add(emporiumvendaitem);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return lista;
	}
	
	public void deleteCupomCancelVenda(DeleteCupomCancelVendaBean bean) {
		Emporiumvenda emporiumvenda = this.loadTicketCadastrado(bean.getLoja(), bean.getPdv(), bean.getNumero_ticket());
		if(emporiumvenda != null){
			if(emporiumvenda.getVenda() != null){
				Venda venda = emporiumvenda.getVenda();
				if(venda != null){
					vendaService.cancelamentoVenda(venda, true, Boolean.FALSE);
				}
			}
			this.delete(emporiumvenda);
		}
	}
	public boolean isExisteECF(String ids,String origem) {
		List<Emporiumvenda> emporiumvenda = emporiumvendaDAO.isExisteECF(ids,origem);
		if(emporiumvenda == null || emporiumvenda.isEmpty()){
			if(origem.equals("pedidovenda")){
				for(String id : ids.split(",")){
					Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(id));
					if(!emporiumpedidovendaService.havePedidovendaEnviado(pedidovenda)){
						return false;
					}else{
						return true;
					}
				}
			}else{return false;}
			
		}
		return true;
	}
	
	public List<Emporiumvenda> findForCriarNotas(){
		return emporiumvendaDAO.findForCriarNotas();
	}
	
	public void updateNotaGerada(Emporiumvenda emporiumVenda){
		emporiumvendaDAO.updateNotaGerada(emporiumVenda);
	}
}
