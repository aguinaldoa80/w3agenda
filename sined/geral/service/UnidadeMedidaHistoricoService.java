package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.UnidadeMedidaHistorico;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.enumeration.UnidadeMedidaacao;
import br.com.linkcom.sined.geral.dao.UnidadeMedidaHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class UnidadeMedidaHistoricoService extends GenericService<UnidadeMedidaHistorico>{
	
	private UnidadeMedidaHistoricoDAO unidadeMedidaHistoricoDAO;

	public void setUnidadeMedidaHistoricoDAO(
			UnidadeMedidaHistoricoDAO unidadeMedidaHistoricoDAO) {
		this.unidadeMedidaHistoricoDAO = unidadeMedidaHistoricoDAO;
	}
	
	/**
	 * Seta os valores antigos no hist�rico de Unidademedida.
	 *
	 * @param unidadeMedidaHistorico
	 * @param Unidademedida
	 * @author Diogo Souza
	 * @since 20/04/2020
	 */
	public void preencherHistorico(Unidademedida unidademedida, Unidademedida unidadeMedidaAntiga, boolean criar) {
		
		UnidadeMedidaHistorico novoHistorico = new UnidadeMedidaHistorico();
		
		novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		if(criar){
			novoHistorico.setAcao(UnidadeMedidaacao.CRIADO);
			novoHistorico.setUnidadeMedida(unidademedida);
			novoHistorico.setObservacao("Unidade de Medida Criada.");
		}else {
			novoHistorico.setAcao(UnidadeMedidaacao.ALTERADO);
			novoHistorico.setAtivo(unidadeMedidaAntiga.getAtivo());
			novoHistorico.setUnidadeMedida(unidadeMedidaAntiga);
			novoHistorico.setCasasdecimaisestoque(unidadeMedidaAntiga.getCasasdecimaisestoque());
			novoHistorico.setEtiquetaunica(unidadeMedidaAntiga.getEtiquetaunica());
			novoHistorico.setNome(unidadeMedidaAntiga.getNome());
			novoHistorico.setSimbolo(unidadeMedidaAntiga.getSimbolo());
			
			if(unidadeMedidaAntiga.getListaUnidademedidaconversao().size()>0){
				String unidadeMedidaConversao = "";
				for(Unidademedidaconversao conversao : unidadeMedidaAntiga.getListaUnidademedidaconversao()){
					unidadeMedidaConversao += "Unidade relacionada: "+ conversao.getUnidademedidarelacionada().getNome() +"   Fra��o: "+ conversao.getFracao()+"   Qtde Refer�ncia: "+ conversao.getQtdereferencia()+"\n";
				}
			novoHistorico.setUnidadeMedidaConversaoHistorico(unidadeMedidaConversao);	
			}
		}
		saveOrUpdate(novoHistorico);
	}

	
	/**
	 * Busca hist�rico atrav�s do Unidade de Medida.
	 *
	 * @param unidadeMedidaHistorico
	 * @param Unidademedida
	 * @return
	 * @author Diogo Souza
	 * @since 20/04/2020
	 */
	
	public List<UnidadeMedidaHistorico> findByUnidadeMedidaHistorico(Unidademedida unidademedida) {
		return unidadeMedidaHistoricoDAO.findByUnidadeMedidaHistorico(unidademedida);
	}
	
}
