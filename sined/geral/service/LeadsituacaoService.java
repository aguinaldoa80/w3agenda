package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.dao.LeadsituacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadsituacaoService extends GenericService<Leadsituacao>{
	
	private LeadsituacaoDAO leadsituacaoDAO;
	
	public void setLeadsituacaoDAO(LeadsituacaoDAO leadsituacaoDAO) {this.leadsituacaoDAO = leadsituacaoDAO;}

	/* singleton */
	private static LeadsituacaoService instance;
	
	public static LeadsituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LeadsituacaoService.class);
		}
		return instance;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LeadsituacaoDAO#findForListagem()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Leadsituacao> findForListagem() {
		return leadsituacaoDAO.findForListagem();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2016
	 */
	public Leadsituacao findByNome(String nome) {
		return leadsituacaoDAO.findByNome(nome);
	}
	
}
