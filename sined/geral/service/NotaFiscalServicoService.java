package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.hibernate.Hibernate;
import org.jdom.Element;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.lkbanco.arquivoiss.ArquivoIss;
import br.com.linkcom.lkbanco.arquivoiss.ArquivoIssProcess;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresacodigocnae;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.auxiliar.ImpostoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadortipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Retencao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ParcelasCobrancaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportadorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaItemReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ApuracaoimpostoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ArquivoIssFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.NotaFiscalServicoTemplateReport;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.InformacaoContribuinteNfseBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GrupoTributacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.NaturezaOperacaoBean;
import br.com.linkcom.sined.util.ArquivoImportacao;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.util.rest.TLSSocketConnectionFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;

public class NotaFiscalServicoService extends GenericService<NotaFiscalServico> {
	
	private static final int[] COLUMNS_EXCEL = new int[]{80,100,200,100,100,150,450,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,
			150,150,150,150,150,150,150, 150};
	private static final String[] TITLES_EXCEL = new String[]{"Emiss�o", "Nota Fiscal", "N NFS-e", "ID do contrato",
			"Data de vencimento", "CNPJ/CPF", "Cliente", "Inscri��o Municipal", "Valor bruto", 
			"Base de c�lculo Geral", "ISS (%)", "Valor ISS", "Valor ISS Retido", "IR (%)", "Valor IR",
			"Valor IR Retido", "PIS (%)", "Valor PIS", "Valor PIS Retido", "COFINS (%)", "Valor COFINS",
			"Valor COFINS Retido", "CSLL (%)", "Valor CSLL", "Valor CSLL Retido", "INSS (%)", "Valor INSS",
			"Valor INSS Retido", "ICMS (%)", "Valor ICMS", "Valor ICMS Retido", "Valor l�quido"};
	private NotaFiscalServicoDAO notaFiscalServicoDAO;
	private NotaFiscalServicoItemService notaFiscalServicoItemService;
	private NotaService notaService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private ArquivoService arquivoService;
	private ArquivonfnotaService arquivonfnotaService;
	private GrupotributacaoimpostoService grupotributacaoimpostoService;
	private FaixaimpostoService faixaimpostoService;
	private ConfiguracaonfeService configuracaonfeService;
	private NotaDocumentoService notaDocumentoService;
	private NotaHistoricoService notaHistoricoService;
	private DocumentohistoricoService documentohistoricoService;
	private VendaService vendaService;
	private ColaboradorService colaboradorService;
	private ClienteService clienteService;
	private VendapagamentoService vendapagamentoService;
	private VendamaterialService vendamaterialService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private DocumentoorigemService documentoorigemService;
	private DocumentoService documentoService;
	private VendahistoricoService vendahistoricoService;
	private NotaVendaService notaVendaService;
	private ItemlistaservicoService itemlistaservicoService;
	private CodigocnaeService codigocnaeService;
	private CodigotributacaoService codigotributacaoService;
	private DocumentotipoService documentotipoService;
	private ParametrogeralService parametrogeralService;
	private GrupotributacaoService grupotributacaoService;
	private RequisicaoService requisicaoService;
	private CategoriaService categoriaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ColetaMaterialService coletaMaterialService;
	private NotaFiscalServicoRPSService notaFiscalServicoRPSService;
	private ArquivoDAO arquivoDAO;
	private ReportTemplateService reportTemplateService;
	private ColaboradorcargoService colaboradorcargoService;
	private ContacarteiraService contacarteiraService;
	private VendamaterialmestreService vendamaterialmestreService;
	private NotaFiscalServicoTemplateReport notaFiscalServicoTemplateReport;
	private PedidovendaService pedidovendaService;
	private ProjetoService projetoService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private RomaneioService romaneioService;
	private ArquivonfService arquivonfService;
	
	public void setNotaFiscalServicoRPSService(NotaFiscalServicoRPSService notaFiscalServicoRPSService) {this.notaFiscalServicoRPSService = notaFiscalServicoRPSService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setFaixaimpostoService(FaixaimpostoService faixaimpostoService) {this.faixaimpostoService = faixaimpostoService;}
	public void setGrupotributacaoimpostoService(GrupotributacaoimpostoService grupotributacaoimpostoService) {this.grupotributacaoimpostoService = grupotributacaoimpostoService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotaFiscalServicoDAO(NotaFiscalServicoDAO notaFiscalServicoDAO) {this.notaFiscalServicoDAO = notaFiscalServicoDAO;}
	public void setNotaFiscalServicoItemService(NotaFiscalServicoItemService notaFiscalServicoItemService) {this.notaFiscalServicoItemService = notaFiscalServicoItemService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setItemlistaservicoService(ItemlistaservicoService itemlistaservicoService) {this.itemlistaservicoService = itemlistaservicoService;}
	public void setCodigocnaeService(CodigocnaeService codigocnaeService) {this.codigocnaeService = codigocnaeService;}
	public void setCodigotributacaoService(CodigotributacaoService codigotributacaoService) {this.codigotributacaoService = codigotributacaoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setVendamaterialmestreService(VendamaterialmestreService vendamaterialmestreService) {this.vendamaterialmestreService = vendamaterialmestreService;}
	public void setNotaFiscalServicoTemplateReport(NotaFiscalServicoTemplateReport notaFiscalServicoTemplateReport) {this.notaFiscalServicoTemplateReport = notaFiscalServicoTemplateReport;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}

	/* singleton */
	private static NotaFiscalServicoService instance;

	public static NotaFiscalServicoService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(NotaFiscalServicoService.class);
		}
		return instance;
	}

	/**
	 * M�todo de refer�ncia ao DAO. Carrega as informa��es necess�rias para
	 * calcular o total de cada nota fiscal de Servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#carregarInfoParaCalcularTotal(List)
	 * @param listaNfProduto
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarInfoParaCalcularTotal(List<Nota> listaNfServico) {
		return notaFiscalServicoDAO.carregarInfoParaCalcularTotal(listaNfServico);
	}
	
	public List<NotaFiscalServico> carregarInfoParaCalcularTotalServico(String whereIn, String orderBy, Boolean asc) {
		return notaFiscalServicoDAO.carregarInfoParaCalcularTotalServico(whereIn,orderBy,asc);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForReport(NotaFiltro)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<NotaFiscalServico> findForReport(NotaFiltro filtro){
		return notaFiscalServicoDAO.findForReport(filtro);
	}
	
	/**
	 * Carrega uma Nota fiscal de servi�o e associa � lista de Produ��o di�ria.
	 * Para cada tarefa da produ��o di�ria � criado um Item e adicionado na nota fiscal de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoItemService#criaListaNotaItem(List)
	 * @param notaFiscalServico
	 * @throws SinedException - Se o projeto da nota existir e for diferente dos projetos da lista.
	 * @param listaMedicao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public NotaFiscalServico carregaAssociaNotaServico(NotaFiscalServico notaFiscalServico, List<Producaodiaria> listaMedicao){
		NotaFiscalServico nfs = this.loadForEntrada(notaFiscalServico);
		
		if(SinedUtil.isListNotEmpty(listaMedicao)){
			
			Projeto projetoMedicoes = listaMedicao.get(0).getTarefa().getPlanejamento().getProjeto();
			
			if(nfs.getProjeto() != null && !nfs.getProjeto().equals(projetoMedicoes)){
				throw new SinedException("O projeto da nota fiscal de servi�o deve ser o mesmo das medi��es selecionadas.");
			}
			
			List<NotaFiscalServicoItem> listaNotaItem = nfs.getListaItens();
			if(SinedUtil.isListEmpty(listaNotaItem)) 
				listaNotaItem = new ArrayList<NotaFiscalServicoItem>();
			
			@SuppressWarnings("unchecked")
			List<Tarefa> listaTarefa = (List<Tarefa>)CollectionsUtil.getListProperty(listaMedicao, "tarefa");
			listaNotaItem.addAll(notaFiscalServicoItemService.criaListaNotaItem(listaTarefa));
			nfs.setListaItens(listaNotaItem);
			nfs.setCodProducoesDiaria(CollectionsUtil.listAndConcatenate(listaMedicao, "cdproducaodiaria", ","));
		}
		
		return nfs;
	}
	
	/**
	 * Cria uma nova NotaFiscalServico com base em produ��es di�rias.
	 * A Nota possui como projeto o primeiro projeto da listaMedicao, j� que � esperado que todos os objetos da lista
	 * tenham o mesmo projeto. Para cada tarefa da produ��o di�ria � criado um Item na nota fiscal de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoItemService#criaListaNotaItem(List)
	 * 
	 * @param listaMedicao
	 * @return Nota fiscal de servi�o
	 * @author Fl�vio Tavares
	 */
	public NotaFiscalServico criaNotaServico(List<Producaodiaria> listaMedicao){
		NotaFiscalServico nfs = new NotaFiscalServico();
		if(SinedUtil.isListNotEmpty(listaMedicao)){
			@SuppressWarnings("unchecked")
			List<Tarefa> listaTarefa = (List<Tarefa>)CollectionsUtil.getListProperty(listaMedicao, "tarefa");
			List<NotaFiscalServicoItem> listaNotaItem = notaFiscalServicoItemService.criaListaNotaItem(listaTarefa);
			nfs.setListaItens(listaNotaItem);
			nfs.setProjeto(listaMedicao.get(0).getTarefa().getPlanejamento().getProjeto());
			nfs.setCodProducoesDiaria(CollectionsUtil.listAndConcatenate(listaMedicao, "cdproducaodiaria", ","));
		}
		return nfs;
	}
	
	/**
	 * Refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForSelect()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<NotaFiscalServico> findForSelect(String projetos) {
		return notaFiscalServicoDAO.findForSelect(projetos);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#loadForReceita
	 * 
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public NotaFiscalServico loadForReceita(Integer cdNota) {
		return notaFiscalServicoDAO.loadForReceita(cdNota);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#loadForReceita(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> loadForReceita(String whereIn) {
		return notaFiscalServicoDAO.loadForReceita(whereIn);
	}
	
	/**
	 * Gera todas as GPSs em um pdf.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaService#haveNFProduto
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#findForGPS
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#gerarGPS
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public Resource gerarGPS(String whereIn) throws Exception {
		
		if(notaService.haveNFProduto(whereIn)){
			throw new SinedException("S� � poss�vel gerar a GPS de nota fiscal de servi�o.");
		}
		
		MergeReport mergeReport = new MergeReport("gps_"+SinedUtil.datePatternForReport()+".pdf");
		
		List<NotaFiscalServico> listaNota = this.findForGPS(whereIn);
		
		for (NotaFiscalServico nfs : listaNota) {
			mergeReport.addReport(this.gerarGPS(nfs));
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * Gera o report do GPS.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#geraStrEmpresa
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#geraStrCliente
	 * 
	 * @param nfs
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Report gerarGPS(NotaFiscalServico nfs) {
		
		Report report = new Report("faturamento/gps");
		
		Image image = null;		
		try {
			String url = SinedUtil.getUrlWithContext() + "/imagens/faturamento/logo_gps.jpg";
			SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Content-type", "image/jpg");
			HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpGet);
			HttpEntity entity = response.getEntity();
			image = ImageIO.read(entity.getContent());
		} catch (Exception e) {
			throw new SinedException("Erro na obten��o do logo da previd�ncia social.");
		}
		
		List<NotaContrato> listaContrato = nfs.getListaNotaContrato();
		if(listaContrato != null && 
				listaContrato.size() > 0 && 
				listaContrato.get(0) != null && 
				listaContrato.get(0).getContrato() != null && 
				listaContrato.get(0).getContrato().getCodigogps() != null){
			report.addParameter("CODPAGAMENTO", listaContrato.get(0).getContrato().getCodigogps().toString());
		}
		
		Empresa empresa = nfs.getEmpresa();
		if(empresa == null)
			empresa = empresaService.loadPrincipal();
		
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		empresa.setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(empresa)));
		report.addParameter("NOMEFONEENDERECO", this.geraStrEmpresa(empresa));
		
		report.addParameter("COMPETENCIA", new SimpleDateFormat("MM/yyyy").format(nfs.getDtEmissao()));
		report.addParameter("IDENTIFICADOR", StringUtils.soNumero(empresa.getCpfCnpjProprietarioPrincipalOuEmpresa().toString()));
		report.addParameter("LOGOGPS", image);
		
		Money valorInss = nfs.getValorInss();
		report.addParameter("VALORINSS", valorInss != null ? valorInss.toString() : "");
		report.addParameter("TOTAL", valorInss != null ? valorInss.toString() : "");
		
		report.addParameter("NOMEFONEENDERECOCLIENTE", this.geraStrCliente(nfs));
		
		java.sql.Date dtEmissao = new java.sql.Date(nfs.getDtEmissao().getTime());
		java.sql.Date dtVencimento = null;
		
		if (parametrogeralService.getBoolean(Parametrogeral.VENCIMENTO_GUIAGPS_EMPRESA)) {
			dtVencimento = SinedDateUtils.setDateProperty(SinedDateUtils.addMesData(dtEmissao, 1), 20, Calendar.DAY_OF_MONTH);
			
			empresa = empresaService.loadWithEndereco(empresa);
			
			nfs.setDtVencimento(SinedDateUtils.getAnteriorDataUtil(dtVencimento, empresa));
		}
		
		report.addParameter("DTVENCIMENTO", (dtVencimento != null ? SinedDateUtils.toString(nfs.getDtVencimento()) : ""));
		
		return report;
	}
	
	/**
	 * Gera a string de exibi��o do cliente.
	 *
	 * @param nfs
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String geraStrCliente(NotaFiscalServico nfs) {
		StringBuilder str = new StringBuilder();
		
		str.append(nfs.getCliente().getNome() + "\n" + (nfs.getCliente().getCnpj() != null ?  nfs.getCliente().getCnpj().toString() : "") + "\n");
		str.append("N� Nota Fiscal: " + nfs.getNumero() +
					"    Emiss�o: " + new SimpleDateFormat("dd/MM/yyyy").format(nfs.getDtEmissao()) + 
					"    Valor: " + nfs.getValorTotalServicos());
		
		return str.toString();
	}
	
	/**
	 * Gera a string de exibi��o da empresa.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String geraStrEmpresa(Empresa empresa) {
		StringBuilder str = new StringBuilder();
		
		str.append(empresa.getRazaosocialOuNome() + "\n");
		Endereco endereco = empresa.getEndereco();
		if(endereco != null){
			if(endereco.getLogradouro() != null){
				str.append(endereco.getLogradouroCompleto() + "\n");
			}
			str.append((endereco.getBairro() != null ? endereco.getBairro() : "") + "\n");
			str.append((endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "") + (endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? " - " + endereco.getMunicipio().getUf().getSigla() : "") + "\n");
			str.append((endereco.getCep() != null ? endereco.getCep().toString() : ""));
		}
		
		return str.toString();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForGPS
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<NotaFiscalServico> findForGPS(String whereIn) {
		return notaFiscalServicoDAO.findForGPS(whereIn);
	}
	
	public Resource gerarArquivoIss(ArquivoIssFiltro filtro) {
		
		Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cnpj, empresa.inscricaomunicipal");
		Cnpj cnpj = empresa.getCnpj();
		String inscMunicipal = empresa.getInscricaomunicipal();
		
		List<? extends ArquivoIss> listaNota = this.findForArquivoIss(filtro);
		
		String arquivoString  = ArquivoIssProcess.geraArquivo(listaNota, inscMunicipal);
		
		Resource resource = new Resource("text/plain", "ISSBH" + (cnpj != null ? cnpj.getValue() : "")  + ".txt", arquivoString.getBytes());
		return resource;
	}
	
	private List<NotaFiscalServico> findForArquivoIss(ArquivoIssFiltro filtro) {
		return notaFiscalServicoDAO.findForArquivoIss(filtro);
	}
	
	public List<NotaFiscalServico> findForNFe(String whereIn) {
		List<NotaFiscalServico> lista =  notaFiscalServicoDAO.findForNFe(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			for(NotaFiscalServico nfs : lista){
				try {
					notaFiscalServicoItemService.ordenaListaItens(nfs);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return lista;
	}
	
	public Resource gerarRelatorioNfse(String whereIn, boolean rps) throws Exception {
		
		Map<NotaFiscalEletronicaReportBean, Arquivonfnota> listaReportBean = this.prepareListBeansForNfse(whereIn, rps);
		List<Object> listaPdfs = new ArrayList<Object>();
		String resourcename = rps ? "rps_"+SinedUtil.datePatternForReport()+".pdf" : "nfse_"+SinedUtil.datePatternForReport()+".pdf";
		MergeReport mergeReport = new MergeReport(resourcename);
		
		for (NotaFiscalEletronicaReportBean bean : listaReportBean.keySet()) {
			Prefixowebservice prefixowebservice = null;
			Configuracaonfe configuracaonfe = null;
			if(bean.getCdconfiguracaonfe() != null){
				configuracaonfe = new Configuracaonfe();
				configuracaonfe.setCdconfiguracaonfe(bean.getCdconfiguracaonfe());
				configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe);
			}
			
			if(configuracaonfe != null){
				if(!rps)
					prefixowebservice = configuracaonfe.getPrefixowebservice();

				ReportTemplateBean template = rps && configuracaonfe.getTemplateemissaonfseespelho() != null? configuracaonfe.getTemplateemissaonfseespelho():
											!rps && configuracaonfe.getTemplateemissaonfse() != null? configuracaonfe.getTemplateemissaonfse(): null;
				if(template != null){
					WebRequestContext request = (WebRequestContext)Neo.getRequestContext();
					request.setAttribute("bean", bean);
					ReportTemplateFiltro filtro = new ReportTemplateFiltro();
					filtro.setTemplate(template);
					Resource pdf = notaFiscalServicoTemplateReport.getPdfResource(request, filtro);
					listaPdfs.add(pdf);
					continue;
				}	
			}
			
			boolean salvador = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD));
			boolean votorantim = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD));
			String modeloRPS = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.LAYOUT_EMISSAO_RPS);
			String modeloNFSE = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.LAYOUT_EMISSAO_NFSE);
			
			Report report;
			if(rps && modeloRPS != null && !modeloRPS.isEmpty()){
				report = new Report("faturamento/" + modeloRPS);
			}else if(!rps && modeloNFSE != null && !modeloNFSE.isEmpty() && !votorantim){
				report = new Report("faturamento/" + modeloNFSE);
			}else{
				if(salvador){
					report = new Report("faturamento/notaFiscalEletronica_salvador"); 
				} else {
					report = new Report("faturamento/notaFiscalEletronica"); 
				}
			}
			
			List<NotaFiscalEletronicaReportBean> lista = new ArrayList<NotaFiscalEletronicaReportBean>();
			lista.add(bean);
			report.setDataSource(lista);
			
			report.addParameter("TITULO", rps ? "RPS - RECIBO PROVIS�RIO DE SERVI�O" : "NFS-e - NOTA FISCAL DE SERVI�O ELETR�NICA");
			
			if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
				report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
			}
			
			listaPdfs.add(report);
			mergeReport.addReport(report);
		}
		
		return this.generateResource(listaPdfs, resourcename);
	}
	
	public Resource generateResource(List<Object> listaNotas, String nomePdf) throws Exception {
		List<PdfReader> listaPdf = new ArrayList<PdfReader>();

		for(Object nota: listaNotas){
			PdfReader pdf = null;
			if(nota instanceof Report){
				pdf = new PdfReader(getReportBytes((Report)nota));
			}else{
				pdf = new PdfReader(((Resource)nota).getContents());
			}
			
			listaPdf.add(pdf);
		}
		Document document = new Document(listaPdf.get(0).getPageSizeWithRotation(1));
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PdfCopy writer = new PdfCopy(document, output);
		document.open();
		
		for (PdfReader pdfReader : listaPdf) {
			convertToDocument(pdfReader,writer);
		}
		
		document.close();
		
		byte[] bytes = output.toByteArray();
		
		return getPdfResource(nomePdf, bytes);
	}
	
	private byte[] getReportBytes(IReport report) {
		return Neo.getApplicationContext().getReportGenerator().toPdf(report);
	}
	
	private Resource getPdfResource(String name, byte[] bytes) {
		Resource resource = new Resource();
        resource.setContentType("application/pdf");
        resource.setFileName(name);
        resource.setContents(bytes);
		return resource;
	}
	
	private void convertToDocument(PdfReader pdfReader,PdfCopy writer) throws DocumentException {
		pdfReader.consolidateNamedDestinations();
		PdfImportedPage page;
		int n = pdfReader.getNumberOfPages();
		for (int i = 0; i < n; ) {
		    ++i;
		    page = writer.getImportedPage(pdfReader, i);
		    try {
				writer.addPage(page);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public Map<NotaFiscalEletronicaReportBean, Arquivonfnota> prepareListBeansForNfse(String whereIn, boolean rps) throws Exception {
		if(rps){
			if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.EMITIDA, NotaStatus.LIQUIDADA, NotaStatus.CANCELADA)){
				throw new SinedException("S� se pode emitir rps com situa��es 'EMITIDA', 'LIQUIDADA' ou 'CANCELADA'.");
			}
		} else {
			if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA, NotaStatus.CANCELADA)){
				throw new SinedException("S� se pode emitir nfse com situa��es 'NFS-e EMITIDA', 'NFS-e LIQUIDADA' ou 'CANCELADA PELA PREFEITURA'.");
			}
		}
		
		List<NotaFiscalServico> listaNota = this.findForNFe(whereIn);
		
		if(rps){
			for (NotaFiscalServico nfs : listaNota) {
				boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
				if(!haveVotorantim){
					throw new SinedException("S� � poss�vel emitir RPS de empresas com configura��o do fornecedor GeisWeb cadastrada.");
				}
			}
		}
		boolean forTemplate = false;
		List<GenericBean> listaBean = new ArrayList<GenericBean>();
		for (NotaFiscalServico nfs : listaNota) {
			NotaFiscalServicoRPS notaFiscalServicoRPS = null;
			Configuracaonfe configuracaonfe = null;
			Arquivonfnota arquivonfnota = null;
			
			if(rps){
				notaFiscalServicoRPS = notaFiscalServicoRPSService.findByNota(nfs);
				List<Configuracaonfe> ativosByTipoEmpresa = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, nfs.getEmpresa());
				configuracaonfe = ativosByTipoEmpresa != null && ativosByTipoEmpresa.size() > 0 ? ativosByTipoEmpresa.get(0) : null;
				configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe);
				forTemplate = configuracaonfe.getTemplateemissaonfseespelho() != null;
				if(notaFiscalServicoRPS == null){
					throw new SinedException("N�o existe RPS para a nota com id " + nfs.getCdNota() + ".");
				}
			} else {
				arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
				//Esta valida��o � para emiss�o de NFS que n�o foram canceladas na prefeitura eletronicamente
				if (arquivonfnota==null){
					arquivonfnota = arquivonfnotaService.findByNotaCanceladaNaPrefeitura(nfs);
					if (arquivonfnota==null){
						throw new SinedException("S� se pode emitir nfse com situa��es 'CANCELADA PELA PREFEITURA'" + ".");
					}
				}
				arquivonfnota.setCliente(nfs.getCliente());
				configuracaonfe = configuracaonfeService.loadForEntrada(arquivonfnota.getArquivonf().getConfiguracaonfe());
				forTemplate = configuracaonfe.getTemplateemissaonfse() != null;
			}
			
			
			String modeloRPS = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.LAYOUT_EMISSAO_RPS);
			String modeloNFSE = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.LAYOUT_EMISSAO_NFSE);
			
			NotaFiscalEletronicaReportBean bean = makePdfNfse(nfs, arquivonfnota, configuracaonfe, notaFiscalServicoRPS, forTemplate, rps, modeloRPS, modeloNFSE);
			listaBean.add(new GenericBean(arquivonfnota, bean));
		}
		
		//Ordena��o pela ordem: Arquivonfnota.numeronfse, Nota.numero, Cliente.razaosocial, Cliente.nome
		if(listaBean != null && !listaBean.isEmpty()){
			if(!rps){
				Collections.sort(listaBean, new Comparator<GenericBean>(){
					
					public int compare(GenericBean o1, GenericBean o2) {
						Arquivonfnota a1 = (Arquivonfnota) o1.getId();
						Arquivonfnota a2 = (Arquivonfnota) o2.getId();
						
						if(a1.getNumero_filtro() == null || a2.getNumero_filtro() == null){
							
							if(a1.getNota() != null && a1.getNota().getNumeronota() != null && a1.getNota() != null && a2.getNota().getNumeronota() != null){
								return a1.getNota().getNumeronota().compareTo(a2.getNota().getNumeronota());
							} else{
								String s1 = a1.getCliente().getRazaosocial() != null && !a1.getCliente().getRazaosocial().equals("") 
											? a1.getCliente().getRazaosocial() : a1.getCliente().getNome();
								String s2 = a2.getCliente().getRazaosocial() != null && !a2.getCliente().getRazaosocial().equals("") 
											? a2.getCliente().getRazaosocial() : a2.getCliente().getNome();
								return s1.compareTo(s2);
							}
						} else{
							return a1.getNumero_filtro().compareTo(a2.getNumero_filtro());
						}
					}
					
				});
			}
		}
		Map<NotaFiscalEletronicaReportBean, Arquivonfnota> retorno = new LinkedHashMap<NotaFiscalEletronicaReportBean, Arquivonfnota>();
		for(GenericBean genericBean: listaBean){
			retorno.put((NotaFiscalEletronicaReportBean)genericBean.getValue(), (Arquivonfnota)genericBean.getId());
		}
		return retorno;
	}
	
	public NotaFiscalEletronicaReportBean makePdfNfse(NotaFiscalServico nfs, Arquivonfnota arquivonfnota, Configuracaonfe configuracaonfe, NotaFiscalServicoRPS notaFiscalServicoRPS, boolean forTemplate, boolean rps, String modeloRPS, String modeloNFSE) throws IOException {
		boolean temModelo = ((rps && modeloRPS != null && !modeloRPS.isEmpty()) || (!rps && modeloNFSE != null && !modeloNFSE.isEmpty()));
		
		Prefixowebservice prefixowebservice = configuracaonfe != null ? configuracaonfe.getPrefixowebservice() : null;
		Empresa empresa = empresaService.loadComArquivo(nfs.getEmpresa());
		empresaService.setInformacoesPropriedadeRural(empresa);
		Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());
		
		boolean smarapd = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SERRAISS_HOM) || 
														prefixowebservice.equals(Prefixowebservice.SERRAISS_PROD) ||
														prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) ||
														prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
														prefixowebservice.equals(Prefixowebservice.JANDIRA_HOM) || 
														prefixowebservice.equals(Prefixowebservice.JANDIRA_PROD));
		boolean saopaulo = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SPISS_HOM) || prefixowebservice.equals(Prefixowebservice.SPISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_PROD));
		boolean salvador = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD));
		boolean lavras = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.LAVRASISS_HOM) || prefixowebservice.equals(Prefixowebservice.LAVRASISS_PROD));
		boolean ginfes = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) || prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
									prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_PROD) ||
									prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD));
		boolean santaLuzia = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD));
		boolean itabirito = prefixowebservice != null && (prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD));
		
		boolean modeloItabirito = ((rps && modeloRPS != null && modeloRPS.equals("notaFiscalEletronica_itabirito")) || (!rps && modeloNFSE != null && modeloNFSE.equals("notaFiscalEletronica_itabirito")));
		boolean modeloMorecap = ((rps && modeloRPS != null && modeloRPS.equals("notaFiscalEletronica_morecap")) || (!rps && modeloNFSE != null && modeloNFSE.equals("notaFiscalEletronica_morecap")));
		boolean modeloMorecapRps = ((rps && modeloRPS != null && modeloRPS.equals("notaFiscalEletronica_morecap_rps")) || (!rps && modeloNFSE != null && modeloNFSE.equals("notaFiscalEletronica_morecap_rps")));
		
		String paramAnotacaocorpoContrato = parametrogeralService.getValorPorNome(Parametrogeral.CONTRATO_ANOTACAOCORPO_CONCATENAR);
		boolean concatenarAnotacaocorpo = paramAnotacaocorpoContrato != null && paramAnotacaocorpoContrato.toUpperCase().trim().equals("TRUE");
		
		StringBuilder discriminacao;
		StringBuilder discriminacaoComValor;
		NotaFiscalEletronicaReportBean bean = new NotaFiscalEletronicaReportBean();
		
		bean.setSerie("A");
		bean.setEntradaSaida("1");
		bean.setValorIR(nfs.getValorIr() != null ? nfs.getValorIr().toString() : "0,00");
		bean.setValorPis(nfs.getValorPis() != null ? nfs.getValorPis().toString() : "0,00");
		bean.setValorConfis(nfs.getValorCofins() != null ? nfs.getValorCofins().toString() : "0,00");
		bean.setValorCsll(nfs.getValorCsll() != null ? nfs.getValorCsll().toString() : "0,00");
		bean.setValorInss(nfs.getValorInss() != null ? nfs.getValorInss().toString() : "0,00");
			
		if(notaFiscalServicoRPS != null){
			bean.setNumNfse(notaFiscalServicoRPS.getNumero().toString());
			bean.setDtEmissao(notaFiscalServicoRPS.getDataemissao());
			bean.setDataHoraEmissao(notaFiscalServicoRPS.getDataemissao()!=null ? new SimpleDateFormat("HH:mm").format(notaFiscalServicoRPS.getDataemissao()) : "00:00");
			bean.setDtCompetencia(nfs.getDtEmissao());
			bean.setRps(Boolean.TRUE);
			bean.setNumeroNfse(notaFiscalServicoRPS.getNumero().toString());
		} else {
			// CABE�ALHO
			if(salvador){
				bean.setNumNfse(arquivonfnota.getNumeronfse());
			} else {
				bean.setNumNfse(this.formataNumNfse(arquivonfnota.getNumeronfse()));
			}
			bean.setNumeroNfse(arquivonfnota.getNumeronfse());
			
			if(!lavras && !saopaulo && !smarapd){
				bean.setDtEmissao(arquivonfnota.getDtemissao() != null ? new Date(arquivonfnota.getDtemissao().getTime()) : new Date(nfs.getDtEmissao().getTime()));
				bean.setDtCompetencia(arquivonfnota.getDtcompetencia() != null ? new Date(arquivonfnota.getDtcompetencia().getTime()) : nfs.getDtEmissao());
			} else {
				bean.setDtEmissao(nfs.getDtEmissao());
				bean.setDtCompetencia(nfs.getDtEmissao());
			}
			bean.setDataHoraEmissao(NeoFormater.getInstance().format(nfs.getDtEmissao()) + " " + (nfs.getHremissao()!=null ? new SimpleDateFormat("HH:mm").format(nfs.getHremissao()) : "00:00"));
			bean.setCodVerificacao(arquivonfnota.getCodigoverificacao());
		}
		
		bean.setVariasParcelas(nfs.getListaDuplicata() != null && nfs.getListaDuplicata().size() > 1);
		bean.setPrimeiroVencimentoParcela(nfs.getListaDuplicata() != null && nfs.getListaDuplicata().size() > 0 ? nfs.getListaDuplicata().get(0).getDtvencimento() : nfs.getDtVencimento());
		bean.setDtVencimento(nfs.getDtVencimento());
		bean.setNumRPS(nfs.getNumero());
		bean.setCdconfiguracaonfe(configuracaonfe != null? configuracaonfe.getCdconfiguracaonfe(): null);
		
		if(saopaulo){
			bean.setPatternDataemissao("dd/MM/yyyy");
		}
		//Logo Prefeitura NFSe
		try{
			if(configuracaonfe != null && configuracaonfe.getLogoNfse() != null){
				bean.setLogoNFse(arquivoService.loadAsImage(configuracaonfe.getLogoNfse()));
			}else if(salvador){
				URL arqImagem = new URL(SinedUtil.getUrlWithContext() + "/imagens/faturamento/logoPrefeituraSalvador.png");					
				Image image = ImageIO.read(arqImagem.openStream());
				bean.setLogoNFse(image);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
		if(configuracaonfe != null){
			bean.setNomePrefeitura(configuracaonfe.getNomeprefeitura() != null ? configuracaonfe.getNomeprefeitura() : "");
			bean.setEnderecoPrefeitura(configuracaonfe.getEnderecoprefeitura() != null ? configuracaonfe.getEnderecoprefeitura() : "");
		}
		
		// PRESTADOR
		try{
			if (empresa.getLogomarca() != null)
				bean.setLogoPrestador(ImageIO.read(new ByteArrayInputStream(arquivoService.loadWithContents(empresa.getLogomarca()).getContent())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		bean.setMostrarAtividade(lavras);
		bean.setPrestadorNome(empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
		bean.setPrestadorNomeFantasia(empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa());
		bean.setPrestadorCpfCnpj(empresa.getCpfCnpjProprietarioPrincipalOuEmpresa().toString());
		bean.setPrestadorInscEst(empresa.getInscricaoestadual());
		bean.setPrestadorInscMun(this.maskInscricaoMunicipal((StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()))));
		bean.setPrestadorEndereco(configuracaonfe.getEndereco().getLogradouroCompletoComBairro() + " - Cep: " + configuracaonfe.getEnderecoCep());
		bean.setPrestadorEnderecoSemMunicipioSemComplemento(configuracaonfe.getEndereco().getLogradouroCompletoComBairroSemMunicipioSemComplemento() + " - Cep: " + configuracaonfe.getEnderecoCep());
		bean.setPrestadorEnderecoComplemento(configuracaonfe.getEndereco().getComplemento());
//		bean.setPrestadorCidade(empresa.getEnderecoCidade());
//		bean.setPrestadorEstado(empresa.getEnderecoSiglaEstado());
		bean.setPrestadorTelefone(configuracaonfe.getTelefone() != null ? configuracaonfe.getTelefone() : "N�o informado");
		bean.setPrestadorEmail(empresa.getEmail() != null ? empresa.getEmail(): "N�o informado");
		bean.setPrestadorSimplesNacional(Codigoregimetributario.SIMPLES_NACIONAL.equals(empresa.getCrt()) || Codigoregimetributario.SIMPLES_NACIONAL_EXCESSO.equals(empresa.getCrt()));
		
		// TOMADOR
		String tomadorNome;
		if(nfs.getNomefantasiaCliente() != null && !"".equals(nfs.getNomefantasiaCliente())){
			tomadorNome = nfs.getNomefantasiaCliente();
		}else if(nfs.getCliente().getRazaosocial() != null && !nfs.getCliente().getRazaosocial().equals("")){
			tomadorNome = nfs.getCliente().getRazaosocial();
		}else {
			tomadorNome = nfs.getCliente().getNome();
		}
		bean.setTomadorNome(tomadorNome);
		bean.setTomadorCpfCnpj(nfs.getCliente().getCpfOuCnpj());
		bean.setTomadorInscEst(org.apache.commons.lang.StringUtils.isNotBlank(nfs.getCliente().getInscricaoestadual()) ? nfs.getCliente().getInscricaoestadual() : "N�o informado");
		if(nfs.getInscricaomunicipal() != null)
			bean.setTomadorInscMun(!nfs.getInscricaomunicipal().trim().equals("") ? nfs.getInscricaomunicipal() : null);
		else
			bean.setTomadorInscMun(nfs.getCliente().getInscricaomunicipal() != null && !nfs.getCliente().getInscricaomunicipal().equals("") ? this.maskInscricaoMunicipal(StringUtils.soNumero(nfs.getCliente().getInscricaomunicipal())) : "N�o informado");
		
		bean.setTomadorEndereco(enderecoCliente.getLogradouroCompletoComBairroSemMunicipio() + " - Cep: " + (enderecoCliente.getCep() != null ? enderecoCliente.getCep().toString() : ""));
		bean.setTomadorEnderecoSemMunicipioSemComplemento(enderecoCliente.getLogradouroCompletoComBairroSemMunicipioSemComplemento() + " - Cep: " + (enderecoCliente.getCep() != null ? enderecoCliente.getCep().toString() : ""));
		
		bean.setTomadorEnderecoLogradouro(enderecoCliente.getLogradouro() != null ? enderecoCliente.getLogradouro() : "");
		bean.setTomadorEnderecoNumero(enderecoCliente.getNumero() != null ? enderecoCliente.getNumero() : "");
		bean.setTomadorEnderecoBairro(enderecoCliente.getBairro() != null ? enderecoCliente.getBairro() : "");
		bean.setTomadorEnderecoCep(enderecoCliente.getCep() != null ? enderecoCliente.getCep().toString() : "");
		bean.setTomadorEnderecoComplemento(enderecoCliente.getComplemento() != null ? enderecoCliente.getComplemento() : "");
		
		if(enderecoCliente.getMunicipio() != null){
			bean.setTomadorCidade(enderecoCliente.getMunicipio().getNome());
			if(enderecoCliente.getMunicipio().getUf() != null){
				bean.setTomadorEstado(enderecoCliente.getMunicipio().getUf().getSigla());
			}
		}
		
		if(nfs.getCliente().getListaTelefone() != null && !nfs.getCliente().getListaTelefone().isEmpty()){
			for(Telefone t : nfs.getCliente().getListaTelefone()){
				bean.setTomadorTelefone(t.getTelefone() != null ? t.getTelefone():"N�o informado");
				break;
			}
		}else
			bean.setTomadorTelefone("N�o informado");
		bean.setTomadorEmail(nfs.getCliente().getEmail() != null && !nfs.getCliente().getEmail().equals("") ? nfs.getCliente().getEmail() : "N�o informado");
		
		// DISCRIMINA��O DOS SERVI�OS
		discriminacao = new StringBuilder();
		discriminacaoComValor = new StringBuilder();
		StringBuilder qtdItem = new StringBuilder();
		StringBuilder valoresun = new StringBuilder();
		StringBuilder valortotal = new StringBuilder();
		StringBuilder desconto = new StringBuilder();
		StringBuilder aliqiss = new StringBuilder();
		
		int qtdeQuebraTotal = 0;
		
		boolean discriminarQtdeValorItem = empresa.getDiscriminarservicoqtdevalor() != null ? empresa.getDiscriminarservicoqtdevalor() : false;
		if(salvador){
			boolean putQuebraLinha = false;
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				if(putQuebraLinha) discriminacao.append("\n");
				if(putQuebraLinha) discriminacaoComValor.append("\n");
				
				StringBuilder descricao = new StringBuilder();
				if (it.getDescricao() != null && !it.getDescricao().equals("")) {
					descricao.append(it.getDescricao());
				} else {
					if (it.getMaterial() != null && it.getMaterial().getNome() != null && !it.getMaterial().getNome().equals("")) {
						descricao.append(it.getMaterial().getNome());
					}
				}
				
				discriminacao.append(SinedUtil.retiraZeroDireita(it.getQtde()) + "  " + descricao.toString() + "  R$" + it.getTotalParcial() + "\n");
				putQuebraLinha = true;
			}
			
			discriminacao.append("\n\nValor do servi�o prestado: R$ " + nfs.getValorBruto());
			discriminacao.append("\nValor l�quido a pagar desta NF: R$ " + nfs.getValorNota());
		} else {
			boolean putQuebraLinha = false;
			boolean putQuebralinhaNomeGrande = false;
			StringBuilder quebras = new StringBuilder();
			
			int limiteCaracteresDescricao = modeloItabirito ? 98 : modeloMorecap ? 130 : modeloMorecapRps ? 78 : 90;
			String pontos = " ";
			for (int i=1; i<25; i++){
				pontos += ".";
			}
			pontos += " ";
			
			LinkedList<NotaFiscalEletronicaItemReportBean> listaItens = new LinkedList<NotaFiscalEletronicaItemReportBean>();
			for (NotaFiscalServicoItem it : nfs.getListaItens()) {
				Double desc = it.getDesconto() != null? it.getDesconto().getValue().doubleValue(): 0d;
				listaItens.add(new NotaFiscalEletronicaItemReportBean(it.getQtde(), it.getDescricao(), nfs.getIss(), it.getPrecoUnitario(), it.getTotalParcial() != null ? it.getTotalParcial().getValue().doubleValue() : null, desc));
				
				String descricaoMaterial = org.apache.commons.lang.StringUtils.isNotBlank(it.getDescricao()) ?  it.getDescricao() : it.getMaterial() != null ? it.getMaterial().getNome() : null;
				
				if(putQuebraLinha) discriminacao.append("\n");
				if(putQuebraLinha) discriminacaoComValor.append("\n");
				if(putQuebraLinha) qtdItem.append("\n");
				if(putQuebraLinha) valoresun.append("\n");
				if(putQuebraLinha) valortotal.append("\n");
				if(putQuebraLinha) desconto.append("\n");
				if(putQuebraLinha) aliqiss.append("\n");
				qtdeQuebraTotal++;

				if(discriminarQtdeValorItem && !temModelo){
					discriminacao.append(it.getQtde() + " - " + descricaoMaterial + "     R$ " + it.getPrecoUnitario() + 
							(it.getDesconto() != null && it.getDesconto().getValue().doubleValue() > 0 ? " Desconto R$ " + it.getDesconto() : ""));
				}else {
					quebras.delete(0, quebras.length());
					if((descricaoMaterial != null && descricaoMaterial.length() > limiteCaracteresDescricao)){
						/* M�ximo de 90 caracteres por linha, quebrando linha caso ultrapasse.
						 * Como j� existe uma quebra logo abaixo, verifico (getDescricao().length() % 90). Se igual a 0, retiro uma linha da quebra
						 * para n�o quebrar duas vezes e deixar um espa�o indevido entre os itens 
						 * */
						int qtdeQuebras = (descricaoMaterial.length() % limiteCaracteresDescricao) == 0?
										 (descricaoMaterial.length() / limiteCaracteresDescricao) - 1:
										 (descricaoMaterial.length() / limiteCaracteresDescricao); 
						for(int i = 0; i < qtdeQuebras; i++){
							quebras.append('\n');
							qtdeQuebraTotal++;
						}					
					}
					int qtdeQuebraDescricao = descricaoMaterial != null ? org.apache.commons.lang.StringUtils.countMatches(descricaoMaterial, "\n") : 0;
					for(int i = 0; i < qtdeQuebraDescricao-1; i++){
						quebras.append('\n');
						qtdeQuebraTotal++;
					}
					
//					String descricaoMaterialWithQuebra = descricaoMaterial != null ? org.apache.commons.lang.StringUtils.join(descricaoMaterial.split("(?<=\\G.{" + limiteCaracteresDescricao + "})"), "\n") : null;
					String descricaoMaterialWithQuebra = descricaoMaterial != null ? SinedUtil.putBreakLineAtLimit(descricaoMaterial, limiteCaracteresDescricao) : null;
					putQuebralinhaNomeGrande = quebras.length() > 0;	
					//altera��o feita para o cliente 3rlab - concatena a quantidade e os valores unitarios e total, na descri��o dos servi�os
					if(Boolean.TRUE.equals((parametrogeralService.getBoolean(Parametrogeral.DETALHAMENTO_SERVICO_NFSE)))){
						discriminacao.append("||" + (descricaoMaterialWithQuebra != null ? descricaoMaterialWithQuebra : "") + " | Quantidade: " + (it.getQtde() != null ? it.getQtde().toString() : "0") + " | Valor Unit�rio: R$ " + (it.getPrecoUnitario() != null ? new Money(it.getPrecoUnitario()) : "0,00") + " | Valor Total: R$ " + it.getTotalParcial() + "||");
					}else{
						discriminacao.append(descricaoMaterialWithQuebra != null ? descricaoMaterialWithQuebra : "");						
					}
					discriminacaoComValor.append((descricaoMaterial != null ? descricaoMaterial : "") + pontos + "R$ " + it.getTotalParcial());
					qtdItem.append(it.getQtde() != null ? it.getQtde().toString() : "1");
					if(putQuebralinhaNomeGrande){
						qtdItem.append(quebras.toString());
					}
					valoresun.append("R$ " + new Money(it.getPrecoUnitario() !=null ? it.getPrecoUnitario() : 0).toString());
					
					if(putQuebralinhaNomeGrande){
						valoresun.append(quebras.toString());
					}
					if(it.getPrecoUnitario() != null && it.getQtde() != null){
						valortotal.append("R$ " + new Money(it.getPrecoUnitario()*it.getQtde()));
						if(putQuebralinhaNomeGrande){
							valortotal.append(quebras.toString());
						}
					}
					desconto.append("R$ " + it.getDesconto());
					if(putQuebralinhaNomeGrande){
						desconto.append(quebras.toString());
					}
					aliqiss.append((nfs.getIss() != null ? SinedUtil.descriptionDecimal(nfs.getIss()) : "0"));
					if(putQuebralinhaNomeGrande){
						aliqiss.append(quebras.toString());
					}
				}
				putQuebraLinha = true;
				
			}

			if(putQuebralinhaNomeGrande) qtdItem.append("\n");
			if(putQuebralinhaNomeGrande) valoresun.append("\n");
			if(putQuebralinhaNomeGrande) valortotal.append("\n");
			if(putQuebralinhaNomeGrande) desconto.append("\n");
			if(putQuebralinhaNomeGrande) aliqiss.append("\n");
			
			bean.setListaItens(listaItens);
			
			if(nfs.getGrupotributacao() != null){
				Codigotributacao codigotributacao = nfs.getCodigotributacao();
				
				List<Grupotributacaoimposto> listaGrupotributacaoimposto = grupotributacaoimpostoService.findByGrupotributacao(nfs.getGrupotributacao());
				for (Grupotributacaoimposto gti : listaGrupotributacaoimposto){
					Municipio municipio = null;
					if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
						if (nfs.getEnderecoCliente() != null && nfs.getEnderecoCliente().getMunicipio() != null)
							municipio = nfs.getEnderecoCliente().getMunicipio();
						else if (nfs.getCliente() != null && nfs.getCliente().getEndereco() != null)
							municipio = nfs.getCliente().getEndereco().getMunicipio();
					} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
						if(nfs.getMunicipioissqn() != null)
							municipio = nfs.getMunicipioissqn(); 
						else if (nfs.getEmpresa() != null && nfs.getEmpresa().getEndereco() != null)
							municipio = nfs.getEmpresa().getEndereco().getMunicipio(); 
					}
	
					Faixaimposto fi = faixaimpostoService.findByControleMunicipio(empresa, nfs.getCliente(), gti.getControle(), municipio, codigotributacao);
					
					if(fi != null && fi.getExibiremissaonfse() != null && fi.getExibiremissaonfse()){
						Money basecalculo = null;
						Money valorimposto = null;
						Boolean incide = false;
						
						if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoicms();
							valorimposto = nfs.getValorIcms();
							incide = nfs.getIncideicms();
						} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoiss();
							valorimposto = nfs.getValorIss();
							incide = nfs.getIncideiss();
						} else if (Faixaimpostocontrole.IR.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoir();
							valorimposto = nfs.getValorIr();
							incide = nfs.getIncideir();
						} else if (Faixaimpostocontrole.INSS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoinss();
							valorimposto = nfs.getValorInss();
							incide = nfs.getIncideinss();
						} else if (Faixaimpostocontrole.CSLL.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculocsll();
							valorimposto = nfs.getValorCsll();
							incide = nfs.getIncidecsll();
						} else if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculopis();
							valorimposto = nfs.getValorPis();
							incide = nfs.getIncidepis();
						} else if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculocofins();
							valorimposto = nfs.getValorCofins();
							incide = nfs.getIncidecofins();
						}
						
						if(incide != null && incide){
							discriminacao.append("\nBase de c�lculo do ").append(gti.getControle().toString()).append(": R$ ").append(basecalculo != null ? basecalculo : new Money()).append(" ");
							discriminacao.append("Valor do ").append(gti.getControle().toString()).append(": R$ ").append(valorimposto != null ? valorimposto : new Money());
							qtdeQuebraTotal++;
						}
					}
				}
			}
		}

		if(concatenarAnotacaocorpo && nfs.getInfocomplementar() != null && !nfs.getInfocomplementar().trim().equals("")){
			discriminacao.append("- ").append(nfs.getInfocomplementar());
		}
		
		bean.setDiscriminacaoServico(discriminacao.toString() + "\n");			
		bean.setDiscriminacaoComValorServico(discriminacaoComValor.toString() + "\n");
		qtdeQuebraTotal++;
		
		boolean considerarListaDocumento = true;
		if(smarapd && SinedUtil.isListNotEmpty(nfs.getListaDuplicata()) && qtdeQuebraTotal < 30){
			considerarListaDocumento = false;
			List<StringBuilder> listaStrDuplicata = new ArrayList<StringBuilder>();
			String strDuplicata;
			boolean discriminacaoDuplicataPorLinha = nfs.getListaDuplicata().size() + qtdeQuebraTotal <= 30;
			int limiteCaracteresDescricao = 119;
			int index = 0;
			
			for(Notafiscalservicoduplicata duplicata : nfs.getListaDuplicata()){
				strDuplicata = duplicata.getNumero() != null ? duplicata.getNumero() : "";
				if(duplicata.getDtvencimento() != null){
					if(org.apache.commons.lang.StringUtils.isNotBlank(strDuplicata)){
						strDuplicata += (" - ");
					}
					strDuplicata += SinedDateUtils.toString(duplicata.getDtvencimento());
				}
				if(duplicata.getValor() != null){
					if(org.apache.commons.lang.StringUtils.isNotBlank(duplicata.getNumero()) || duplicata.getDtvencimento() != null){
						strDuplicata += " - ";
					}
					strDuplicata += "R$ " + duplicata.getValor();
				}
				
				if(listaStrDuplicata.size() == 0){
					listaStrDuplicata.add(new StringBuilder(strDuplicata));
				}else {
					if(discriminacaoDuplicataPorLinha){
						listaStrDuplicata.get(index).append("\n").append(strDuplicata);
					}else {
						if((listaStrDuplicata.get(index).length() + strDuplicata.length()) > limiteCaracteresDescricao){
							listaStrDuplicata.add(new StringBuilder("\n").append(strDuplicata));
							index++;
						}else {
							listaStrDuplicata.get(index).append(" | ").append(strDuplicata);
						}
					}
				}
			}
			
			StringBuilder discriminacaoDuplicatas = new StringBuilder("Vencimentos:\n");
			for(StringBuilder str : listaStrDuplicata) discriminacaoDuplicatas.append(str);
			
			int qtdeQuebraDuplicatas = org.apache.commons.lang.StringUtils.countMatches(discriminacaoDuplicatas.toString(), "\n");
			StringBuilder quebras = new StringBuilder();
			for(int i = 0; i < qtdeQuebraTotal + ((25 - qtdeQuebraTotal) - qtdeQuebraDuplicatas ); i++){
				quebras.append('\n');
			}
			bean.setDiscriminacaoDuplicatas(quebras.toString() + discriminacaoDuplicatas.toString());
		}

		bean.setQuantidadeServico(qtdItem.toString());
		bean.setValorLiquido(nfs.getValorNotaMunicipal() != null ? nfs.getValorNotaMunicipal().toString() : "0,00");
		bean.setValorUnitario(valoresun.toString());
		bean.setTotalprodutos(valortotal.toString());
		bean.setValorTotal(nfs.getValorNota().toString());
		bean.setTotalDescontos((nfs.getDescontocondicionado().add(nfs.getDescontoincondicionado()).toString()));
		bean.setValorDescontoNotaMunicipal(nfs.getValorDescontoNotaMunicipal() != null? nfs.getValorDescontoNotaMunicipal().getValue().doubleValue(): 0d);
		if(temModelo && !forTemplate){
			bean.setDescontoServicos(desconto.toString());
			bean.setAliquotaServicos(aliqiss.toString());
		}else{
			bean.setDescontoServicos(nfs.getValorDescontoNotaMunicipal().toString());
			bean.setAliquotaServicos(nfs.getIss() != null ? nfs.getIss().toString() : "0");
		}
		
		// C�DIGO CNAE E ITEM LISTA DE SERVI�O
		if(salvador){
			if(nfs.getCodigocnaeBean() != null){
				bean.setCnaeDescricao(nfs.getCodigocnaeBean().getCnae() + " - " + nfs.getCodigocnaeBean().getDescricaocnae());
			}
			
			if(nfs.getItemlistaservicoBean() != null){
				bean.setItemListaDescricao(nfs.getItemlistaservicoBean().getCodigo() + " - " + nfs.getItemlistaservicoBean().getDescricao());
			}
		} else {
			if(nfs.getCodigotributacao() != null){
				bean.setCnaeDescricao(nfs.getCodigotributacao().getCodigo() + " / " + nfs.getCodigotributacao().getDescricao());
			}
			
			if(nfs.getItemlistaservicoBean() != null){
				bean.setItemListaDescricao(nfs.getItemlistaservicoBean().getCodigo() + " / " + nfs.getItemlistaservicoBean().getDescricao());
			}
		}
		
		// MUNIC�PIO DA PRESTA��O DE SERVI�COS
		bean.setCodMunicipioPrestacao(nfs.getMunicipioissqn() != null ? nfs.getMunicipioissqn().getCdibge() + " / " + nfs.getMunicipioissqn().getNome() : "");
		bean.setNaturezaOperacao(nfs.getNaturezaoperacao() != null ? nfs.getNaturezaoperacao().getDescricao() : "");
		bean.setCodigoNfeNaturezaOperacao((nfs.getNaturezaoperacao() != null && nfs.getNaturezaoperacao().getCodigonfse()!=null && !nfs.getNaturezaoperacao().getCodigonfse().trim().equals("") ? nfs.getNaturezaoperacao().getCodigonfse() + " - " : "") + (nfs.getNaturezaoperacao() != null ? nfs.getNaturezaoperacao().getDescricao() : ""));
		bean.setRegimeEspecial(nfs.getRegimetributacao() != null ? nfs.getRegimetributacao().getDescricao() : "");
		
		boolean considerarBaseIss = false;
		try {
			considerarBaseIss = Boolean.valueOf(parametrogeralService.getValorPorNome(Parametrogeral.CONSIDERAR_BASE_ISS));
		} catch (Exception e) {}

		Extenso valorExtenso = new Extenso(nfs.getValorBruto() != null ? nfs.getValorBruto().getValue() : BigDecimal.ZERO);
		bean.setValorPorExtenso(valorExtenso.toString());
		
		Extenso valorLiquidoExtenso = new Extenso(nfs.getValorNotaMunicipal() != null ? nfs.getValorNotaMunicipal().getValue() : BigDecimal.ZERO);
		bean.setValorLiquidoPorExtenso(valorLiquidoExtenso.toString());
		
		// VALORES
		bean.setValorServicos(nfs.getValorBruto() != null ? nfs.getValorBruto().toString() : "0,00");
		bean.setBaseCalculoServicos(considerarBaseIss ? (nfs.getBasecalculoiss() != null ? nfs.getBasecalculoiss().toString() : "0,00") : nfs.getBaseCalculoNotaMunicipal().toString());
		bean.setDescontoIncServicos(nfs.getDescontoincondicionado() != null ? nfs.getDescontoincondicionado().toString() : "0,00");
		bean.setDescontoCondicionadoServicos(nfs.getDescontocondicionado() != null ? nfs.getDescontocondicionado().toString() : "0,00");
		bean.setDeducoesServicos(nfs.getDeducao() != null ? nfs.getDeducao().toString() : "0,00");
		bean.setValorIss(nfs.getValorIss() != null ? nfs.getValorIss().toString() : "0,00");
		bean.setRetencoesServicos(nfs.getValorImpostosFederais() != null ? nfs.getValorImpostosFederais().toString() : "0,00");
		bean.setPercentualIss((nfs.getIss() != null? String.format("%.2f", nfs.getIss()): "0,00") + "%");
		
		bean.setValorIssRetido(Boolean.TRUE.equals(nfs.getIncideiss()) && nfs.getValorIss() != null ? nfs.getValorIss().toString(): "0,00");
		bean.setValorIRRetido(Boolean.TRUE.equals(nfs.getIncideir()) && nfs.getValorIr() != null ? nfs.getValorIr().toString(): "0,00");
		bean.setValorPisRetido(Boolean.TRUE.equals(nfs.getIncidepis()) && nfs.getValorPis() != null ? nfs.getValorPis().toString(): "0,00");
		bean.setValorConfisRetido(Boolean.TRUE.equals(nfs.getIncidecofins()) && nfs.getValorCofins() != null ? nfs.getValorCofins().toString(): "0,00");
		bean.setValorCsllRetido(Boolean.TRUE.equals(nfs.getIncidecsll()) && nfs.getValorCsll() != null ? nfs.getValorCsll().toString(): "0,00");
		bean.setValorInssRetido(Boolean.TRUE.equals(nfs.getIncideinss()) && nfs.getValorInss() != null ? nfs.getValorInss().toString(): "0,00");
		bean.setValorOutrasRetencoes(nfs.getOutrasretencoes() != null? nfs.getOutrasretencoes().toString(): "0,00");
		bean.setIncideIss(Boolean.TRUE.equals(nfs.getIncideiss()));
		
		if(nfs.getIncideiss() == null || !nfs.getIncideiss()){
			bean.setIssRetidoServicos("0,00");
			
			if(ginfes && 
					nfs.getRegimetributacao() != null && 
					(nfs.getRegimetributacao().equals(Regimetributacao.ME_EPP_SIMPLES_NACIONAL) || nfs.getRegimetributacao().equals(Regimetributacao.MEI_SIMPLES_NACIONAL))) {
				bean.setValorIss("0,00");
			}
		} else {
			bean.setIssRetidoServicos(nfs.getValorIss().toString());
		}
		
		StringBuilder observacaoitabirito = new StringBuilder();
		observacaoitabirito.append("RPS N� ").append(nfs.getNumero()).append("\n");
		if(configuracaonfe.getExigibilidadeisslavras() != null){
			observacaoitabirito.append("C�digo da natureza da opera��o: ").append((configuracaonfe.getExigibilidadeisslavras().getValue() + 1) + " - " + configuracaonfe.getExigibilidadeisslavras().getNome()).append("\n");
		}
		if(nfs.getCodigotributacao() != null){
			observacaoitabirito.append(nfs.getCodigotributacao().getDescricaoCombo()).append("\n");
		}
		if(nfs.getItemlistaservicoBean() != null){
			observacaoitabirito.append(nfs.getItemlistaservicoBean().getDescricaoCombo()).append("\n");
		}
		bean.setObservacaoitabirito(observacaoitabirito.toString());
		bean.setObservacaonota(nfs.getDadosAdicionais());
		
		if(salvador){
			StringBuilder infos = new StringBuilder();
			
			if(nfs.getRegimetributacao() != null && nfs.getRegimetributacao().equals(Regimetributacao.ME_EPP_SIMPLES_NACIONAL)){
				if(infos.length() > 0) infos.append("\n");
				infos.append("- DOCUMENTO EMITIDO POR ME OU EPP OPTANTE PELO SIMPLES NACIONAL");
			}
			
			if(bean.getDtCompetencia() != null){
				if(infos.length() > 0) infos.append("\n");
				infos.append("- COMPET�NCIA " + new SimpleDateFormat("MM/yyyy").format(bean.getDtCompetencia()));
			}
			
			if(nfs.getDadosAdicionais()!=null && !nfs.getDadosAdicionais().isEmpty()){
				if(infos.length() > 0) infos.append("\n");
				infos.append(nfs.getDadosAdicionais());
			}
			int qtdQuebraLinhaDadosAdicionais;
			qtdQuebraLinhaDadosAdicionais = org.apache.commons.lang.StringUtils.countMatches(infos.toString(), "\n");
			qtdQuebraLinhaDadosAdicionais += org.apache.commons.lang.StringUtils.countMatches(infos.toString(), "\r");
			if(qtdQuebraLinhaDadosAdicionais > 5){
				String aux = infos.toString().replaceAll("\n", " ");
				aux = aux.replaceAll("\r", " ");
				bean.setOutrasinformacoes(aux);
			}
			else{
				bean.setOutrasinformacoes(infos.toString());
			}
		} else {
			StringBuilder infos = new StringBuilder();
			
			if(!temModelo && notaFiscalServicoRPS == null && nfs.getNumero() != null && !nfs.getNumero().equals("")){
				infos.append("NFS-e Gerada a Partir do RPS ").append(nfs.getNumero()).append("\n");
			}
			
			boolean impressoFaixaImposto = false;
			if(nfs.getGrupotributacao() != null){
				Codigotributacao codigotributacao = nfs.getCodigotributacao();
				
				List<Grupotributacaoimposto> listaGrupotributacaoimposto = grupotributacaoimpostoService.findByGrupotributacao(nfs.getGrupotributacao());
				for (Grupotributacaoimposto gti : listaGrupotributacaoimposto){
					Municipio municipio = null;
					if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
						if (nfs.getEnderecoCliente() != null && nfs.getEnderecoCliente().getMunicipio() != null)
							municipio = nfs.getEnderecoCliente().getMunicipio();
						else if (nfs.getCliente() != null && nfs.getCliente().getEndereco() != null)
							municipio = nfs.getCliente().getEndereco().getMunicipio();
					} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
						if(nfs.getMunicipioissqn() != null)
							municipio = nfs.getMunicipioissqn(); 
						else if (nfs.getEmpresa() != null && nfs.getEmpresa().getEndereco() != null)
							municipio = nfs.getEmpresa().getEndereco().getMunicipio(); 
					}
	
					Faixaimposto fi = faixaimpostoService.findByControleMunicipio(empresa, nfs.getCliente(), gti.getControle(), municipio, codigotributacao);
					
					if(fi != null && fi.getExibiremissaonfse() != null && fi.getExibiremissaonfse()){
						Money basecalculo = null;
						Money valorimposto = null;
						Boolean incide = false;
						
						if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoicms();
							valorimposto = nfs.getValorIcms();
							incide = nfs.getIncideicms();
						} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoiss();
							valorimposto = nfs.getValorIss();
							incide = nfs.getIncideiss();
						} else if (Faixaimpostocontrole.IR.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoir();
							valorimposto = nfs.getValorIr();
							incide = nfs.getIncideir();
						} else if (Faixaimpostocontrole.INSS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculoinss();
							valorimposto = nfs.getValorInss();
							incide = nfs.getIncideinss();
						} else if (Faixaimpostocontrole.CSLL.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculocsll();
							valorimposto = nfs.getValorCsll();
							incide = nfs.getIncidecsll();
						} else if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculopis();
							valorimposto = nfs.getValorPis();
							incide = nfs.getIncidepis();
						} else if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
							basecalculo = nfs.getBasecalculocofins();
							valorimposto = nfs.getValorCofins();
							incide = nfs.getIncidecofins();
						}
						
						if(incide != null && incide){
							infos.append("Base de c�lculo do ").append(gti.getControle().toString()).append(": R$ ").append(basecalculo != null ? basecalculo : new Money()).append(" ");
							infos.append("Valor do ").append(gti.getControle().toString()).append(": R$ ").append(valorimposto != null ? valorimposto : new Money()).append("\n");
						}
						impressoFaixaImposto = true;
					}
				}
			}
			
			if(!impressoFaixaImposto && !smarapd){
				if (nfs.getOutrasretencoes() != null && nfs.getOutrasretencoes().getValue().doubleValue() > 0
						|| nfs.getIncidecofins() != null && nfs.getIncidecofins()
						|| nfs.getIncidepis() != null && nfs.getIncidepis()
						|| nfs.getIncideinss() != null && nfs.getIncideinss()
						|| nfs.getIncideir() != null && nfs.getIncideir()
						|| nfs.getIncidecsll() != null && nfs.getIncidecsll()){
						infos.append("Reten��es Federais: \n");
					}
				
				if(nfs.getIncidecofins() != null && nfs.getIncidecofins()){
					if(nfs.getValorCofinsRetido() != null && nfs.getValorCofinsRetido().getValue().doubleValue() > 0){
						infos.append("COFINS: ").append(nfs.getValorCofinsRetido()).append("; ");
					} else {
						infos.append("COFINS: ").append(nfs.getValorCofins()).append("; ");
					}
				}
				
				if(nfs.getIncidepis() != null && nfs.getIncidepis()){
					if(nfs.getValorPisRetido() != null && nfs.getValorPisRetido().getValue().doubleValue() > 0){
						infos.append("PIS: ").append(nfs.getValorPisRetido()).append("; ");
					} else {
						infos.append("PIS: ").append(nfs.getValorPis()).append("; ");
					}
				}
				
				if(nfs.getIncideinss() != null && nfs.getIncideinss()){
					infos.append("INSS: ").append(nfs.getValorInss()).append("; ");
				}
				
				if(nfs.getIncideir() != null && nfs.getIncideir()){
					infos.append("IR: ").append(nfs.getValorIr()).append("; ");
				}
				
				if(nfs.getIncidecsll() != null && nfs.getIncidecsll()){
					infos.append("CSLL: ").append(nfs.getValorCsll()).append("; ");
				}
				
				if (nfs.getOutrasretencoes() != null && nfs.getOutrasretencoes().getValue().doubleValue() > 0){
					infos.append("Outras reten��es: ").append(nfs.getOutrasretencoes());
				}
			}
			
			List<Documento> listaDocumento = documentoService.findNotaDiferenteCanceladaNegociada(nfs);
			if(listaDocumento == null) listaDocumento = new ArrayList<Documento>();
			
			StringBuilder observacoesvenda = new StringBuilder();
			List<NotaVenda> listaVenda = notaVendaService.findByNota(nfs);
			if(listaVenda != null && listaVenda.size() > 0){
				for (NotaVenda notaVenda : listaVenda) {
					if(notaVenda.getVenda().getObservacao() != null){
						observacoesvenda.append(notaVenda.getVenda().getObservacao()).append(" ");
					}
					List<Documento> listaAux = documentoService.findVendaDiferenteCanceladaNegociada(notaVenda.getVenda());
					if(listaAux != null && listaAux.size() > 0) listaDocumento.addAll(listaAux);
				}
			}
			bean.setObservacaovenda(observacoesvenda.toString());
			
			StringBuilder vencimentosObs = new StringBuilder();
			if(considerarListaDocumento && listaDocumento != null && listaDocumento.size() > 0){
				if(infos.length() > 0 && !infos.substring(infos.length()-1).equals("\n")) infos.append("\n");
				infos.append("Vencimento(s): ");
				vencimentosObs.append("Vencimento(s): ");
				
				StringBuilder vencimentos = new StringBuilder();
				for (Documento documento : listaDocumento) {
					if(documento.getDtvencimento() != null){
						String stringDate = SinedDateUtils.toString(documento.getDtvencimento());
						if(vencimentos.indexOf(stringDate) == -1){
							vencimentos.append(stringDate).append("; ");
						}
					}
				}
				infos.append(vencimentos.toString());
				vencimentosObs.append(vencimentos.toString());
			}
			bean.setVencimentos(vencimentosObs.toString());
			
			if(nfs.getDadosAdicionais()!=null && !nfs.getDadosAdicionais().isEmpty()){
				if(infos.length() > 0) infos.append("\n");
				infos.append(nfs.getDadosAdicionais());
			}
			int qtdQuebraLinhaDadosAdicionais;
			qtdQuebraLinhaDadosAdicionais = org.apache.commons.lang.StringUtils.countMatches(infos.toString(), "\n");
			qtdQuebraLinhaDadosAdicionais += org.apache.commons.lang.StringUtils.countMatches(infos.toString(), "\r");
			if(qtdQuebraLinhaDadosAdicionais > 5){
				String aux = infos.toString().replaceAll("\n", " ");
				aux = aux.replaceAll("\r", " ");
				bean.setOutrasinformacoes(aux);
			}
			else{
				bean.setOutrasinformacoes(infos.toString());
			}
			
			if(santaLuzia){
				String codigoControle = getCodigoControleNfe(arquivonfnota);
				if (codigoControle!=null){
					bean.setOutrasinformacoes(bean.getOutrasinformacoes() + "\nC�digo de controle: " + codigoControle);
				}
			}
			if(itabirito){
				String codigoControle = getCodigoControleNfe(arquivonfnota);
				if (codigoControle!=null){
					bean.setCodigocontrole(codigoControle);
				}
			}
		}
		
		bean.setStatus(nfs.getNotaStatus()!=null ? nfs.getNotaStatus().getCdNotaStatus() : null);
		bean.setCdnota(nfs.getCdNota().toString());
		if(bean.getLogoNFse() != null){
			bean.setLogoNfseBase64(SinedUtil.convertImageToBase64(bean.getLogoNFse()));
		}
		if(bean.getLogoPrestador() != null){
			bean.setLogoPrestadorBase64(SinedUtil.convertImageToBase64(bean.getLogoPrestador()));
		}
		
		return bean;
	}
	
	@SuppressWarnings("unchecked")
	private String getCodigoControleNfe(Arquivonfnota arquivonfnota){
		String codigoControleString = null;
		
		try {
			if (arquivonfnota.getArquivoxml()!=null){
				Arquivo arquivo = arquivoDAO.loadWithContents(arquivonfnota.getArquivoxml());
				String xml = new String(arquivo.getContent(), "UTF-8");
				String regex = "(\\<CodigoControle\\>)(.*)(\\<\\/CodigoControle\\>)";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(xml);
				
				if (matcher.find()){
					codigoControleString = matcher.group(2);
				}
			} else if(arquivonfnota.getArquivonf() != null && arquivonfnota.getArquivonf().getArquivoretornoconsulta() != null){
				Arquivo arquivo = arquivoDAO.loadWithContents(arquivonfnota.getArquivonf().getArquivoretornoconsulta());
				Element consultarLoteRpsResposta = SinedUtil.getRootElementXML(arquivo.getContent());
				
				Element listaNfse = SinedUtil.getChildElement("ListaNfse", consultarLoteRpsResposta.getContent());
				List<Element> listaCompNfse = SinedUtil.getListChildElement("CompNfse", listaNfse.getContent());
				for (Element compNfse : listaCompNfse) {
					Element nfse = SinedUtil.getChildElement("Nfse", compNfse.getContent());
					Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
					
					Element numero = SinedUtil.getChildElement("Numero", infNfse.getContent());
					if(numero.getTextTrim().equalsIgnoreCase(arquivonfnota.getNumeronfse())){
						Element codigoControle = SinedUtil.getChildElement("CodigoControle", infNfse.getContent());
						if(codigoControle != null){
							codigoControleString = codigoControle.getTextTrim();
						}
						break;
					}
				}
			}
			return codigoControleString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String formataNumNfse(String numeronfse) {
		if(numeronfse != null && !numeronfse.equals("") && numeronfse.trim().length() == 15){
			int ano = Integer.parseInt(numeronfse.substring(0, 4));
			int num = Integer.parseInt(numeronfse.substring(4, numeronfse.length()));
			
			return ano + "/" + num;
		} else return numeronfse;
	}
	
	private String maskInscricaoMunicipal(String string){
		if(string != null && string.length() >= 10){
			String format = string.substring(0, 6) + "/" + string.substring(6, 9) + "-" + string.substring(9, string.length());
			return format;
		} else return string;
	}
	
	/**
	 * M�todo que calcula total das notas
	 * 
	 * @param filtro
	 * @param list
	 * @author Tom�s Rabelo
	 */
	public void ajustaValoresListagem(NotaFiscalServicoFiltro filtro, List<NotaFiscalServico> list) {
		if(SinedUtil.isListNotEmpty(list)){
			Money total = new Money(0.0);
			for (NotaFiscalServico n : list) 
				total = total.add(n.getValorNota());
			filtro.setTotal(total);
		} else {
			filtro.setTotal(new Money(0.0));
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findByNumNfseCNPJProcessoNovo(String num, Cnpj cnpj)
	 *
	 * @param num
	 * @param cnpj
	 * @return
	 * @since 12/03/2012
	 * @author Rodrigo Freitas
	 */
	public NotaFiscalServico findByNumNfseCNPJProcessoNovo(String num, Cnpj cnpj) {
		return notaFiscalServicoDAO.findByNumNfseCNPJProcessoNovo(num, cnpj);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param num
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/05/2015
	 */
	public List<NotaFiscalServico> findByNumCNPJ(String num, Cnpj cnpj) {
		return notaFiscalServicoDAO.findByNumCNPJ(num, cnpj);
	}
	
	public List<NotaFiscalServico> findEmitidasSubstituicao(Empresa empresa) {
		return notaFiscalServicoDAO.findEmitidasSubstituicao(empresa);
	}
	
	public List<NotaFiscalServico> carregarInfoCalcularTotal(List<NotaFiscalServico> lista) {
		return notaFiscalServicoDAO.carregarInfoCalcularTotal(lista, null, null);
	}
	
	public List<NotaFiscalServico> carregarInfoCalcularTotalForComissao(List<NotaFiscalServico> lista) {
		return notaFiscalServicoDAO.carregarInfoCalcularTotal(lista, "notaFiscalServico.dtEmissao, notaFiscalServico.cdNota", true);
	}
	
	public NotaFiscalServico carregarInfoCalcularTotal(NotaFiscalServico nota) {
		List<NotaFiscalServico> lista = new ArrayList<NotaFiscalServico>();
		lista.add(nota);
		return notaFiscalServicoDAO.carregarInfoCalcularTotal(lista, null, null).get(0);
	}
	
	public String getNextNumero() {
		return notaFiscalServicoDAO.getNextNumero();
	}
	
//	/**
//	 * Faz refer�ncia ao DAO.
//	 * 
//	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#isNotaFiscalServico
//	 *
//	 * @param cdNota
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public boolean isNotaFiscalServico(int cdNota) {
//		return notaFiscalServicoDAO.isNotaFiscalServico(cdNota);
//	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public List<NotaFiscalServico> loadNotasServico() {
		return notaFiscalServicoDAO.loadNotasServico();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<NotaFiscalServico> findByWhereIn(String whereIn) {
		return notaFiscalServicoDAO.findByWhereIn(whereIn);
	}
	
	/**
	 * @param filtro
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public List<NotaFiscalServico> carregarInfoParaCalcularTotalNew(NotaFiltro filtro) {
		return notaFiscalServicoDAO.carregarInfoParaCalcularTotalNew(filtro);
	}
	
	/**
	 * 
	 * Metodo de referencia ao DAO.
	 * Carrega os dados para montagem do relat�rio de Resumo de Faturamento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#buscarParaRelatorioResumoFaturamento(NotaFiltro)
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 * @author Hugo Ferreira
	 */
	public List<NotaFiscalServico> buscarParaRelatorioResumoFaturamento(NotaFiscalServicoFiltro filtro){		
		return notaFiscalServicoDAO.buscarParaRelatorioResumoFaturamento(filtro);
	}
	
	/**Prepara dados para exporta��o excel.
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Taidson
	 * @since 08/09/2010
	 * 
	 */
	public Resource preparaArquivoAcompanhamentoTarefaExcel(NotaFiscalServicoFiltro filtro) throws IOException{
		
		List<NotaFiscalServico> notas = this.buscarParaRelatorioResumoFaturamento(filtro);
		String whereIn = CollectionsUtil.listAndConcatenate(notas, "cdNota", ",");
		List<NotaFiscalServico> listaNotas = this.carregarInfoParaCalcularTotalServico(whereIn, null, null);
		
		//FOR PARA COLOCAR PREENCHER A STRING COM O N�MERO DAS NOTAS FICAIS ELETR�NICAS
		for (NotaFiscalServico nfs : listaNotas) {
			if(nfs.getListaNotaContrato() != null && nfs.getListaNotaContrato().size() > 0){
				List<Integer> listaIds = new ArrayList<Integer>();
				for (NotaContrato notaContrato : nfs.getListaNotaContrato()) {
					if(notaContrato != null && notaContrato.getContrato() != null && notaContrato.getContrato().getCdcontrato() != null){
						listaIds.add(notaContrato.getContrato().getCdcontrato());
					}
				}
				nfs.setIdcontrato(CollectionsUtil.concatenate(listaIds, ","));
			}
			
			if(nfs.getListaNotaDocumento() != null && nfs.getListaNotaDocumento().size() > 0){
				for (NotaDocumento notaDocumento : nfs.getListaNotaDocumento()) {
					if(notaDocumento != null && 
							notaDocumento.getDocumento() != null && 
							notaDocumento.getDocumento().getDtvencimento() != null){
						nfs.setDtvencimentocontareceber(notaDocumento.getDocumento().getDtvencimento());
						break;
					}
				}
			}
		}
		
		List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			for (NotaFiscalServico nf : listaNotas) {
				if(nf.getCdNota().equals(arquivonfnota.getNota().getCdNota())){
					nf.setHaveArquivonfnota(Boolean.TRUE);
					if(arquivonfnota.getDtcancelamento() == null && 
							arquivonfnota.getArquivonf() != null && 
							arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
							arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO)){
						nf.setNumeroNfse(arquivonfnota.getNumeronfse() + " / " + arquivonfnota.getCodigoverificacao());
					}
					break;
				}
			}
		}
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 35);
        
        HSSFRow row = null;
		HSSFCell cell = null;
        
		row = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) 31));
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cell.setCellValue("RELAT�RIO DE RESUMO DE FATURAMENTO");
		
		for(int i = 0; i < COLUMNS_EXCEL.length; i++)
			planilha.setColumnWidth((short) i, (short) (COLUMNS_EXCEL[i]*35));
		
		row = planilha.createRow(4);
		
		for(int i = 0; i < TITLES_EXCEL.length; i++)
			addCell(row, TITLES_EXCEL[i], i, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		for (int i = 0; i < listaNotas.size(); i++) {
			NotaFiscalServico nota = listaNotas.get(i);
			
			row = planilha.createRow(i + 5);
			
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(nota.getDtEmissao()));
			
			cell = row.createCell((short) 1);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(nota.getNumeroFormatado());
			
			cell = row.createCell((short) 2);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(nota.getNumeroNfse());
			
			cell = row.createCell((short) 3);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(nota.getIdcontrato() != null)
				cell.setCellValue(nota.getIdcontrato());		
			
			cell = row.createCell((short) 4);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(nota.getDtvencimentocontareceber() != null)
				cell.setCellValue(SinedDateUtils.toString(nota.getDtvencimentocontareceber()));			
			
			cell = row.createCell((short) 5);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(nota.getCliente().getCpfOuCnpj());
			
			cell = row.createCell((short) 6);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(nota.getCliente().getNome());
			
			cell = row.createCell((short) 7);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(nota.getInscricaomunicipal());
			
			cell = row.createCell((short) 8);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorBruto() != null)
			cell.setCellValue(nota.getValorBruto().getValue().doubleValue());
			
			cell = row.createCell((short) 9);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getBasecalculoiss() != null)
				cell.setCellValue(nota.getBasecalculoiss().getValue().doubleValue());
			else if(nota.getBasecalculo() != null)
				cell.setCellValue(nota.getBasecalculo().getValue().doubleValue());
			else if (nota.getValorBruto() != null)
				cell.setCellValue(nota.getValorBruto().getValue().doubleValue());
			
			cell = row.createCell((short) 10);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getIss() != null)
				cell.setCellValue(nota.getIss()/100d);
			
			cell = row.createCell((short) 11);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorIss() != null && (nota.getIncideiss() == null || !nota.getIncideiss()))
				cell.setCellValue(nota.getValorIss().getValue().doubleValue());
			
			cell = row.createCell((short) 12);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncideiss() != null && nota.getIncideiss() && nota.getValorIss() != null)
				cell.setCellValue(nota.getValorIss().getValue().doubleValue());
			
			cell = row.createCell((short) 13);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getIr() != null)
				cell.setCellValue(nota.getIr()/100d);
			
			cell = row.createCell((short) 14);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorIr() != null)
				cell.setCellValue(nota.getValorIr().getValue().doubleValue());
			
			cell = row.createCell((short) 15);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncideir() != null && nota.getIncideir() && nota.getValorIr() != null)
				cell.setCellValue(nota.getValorIr().getValue().doubleValue());
			
			cell = row.createCell((short) 16);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getPis() != null)
				cell.setCellValue(nota.getPis()/100d);
			
			cell = row.createCell((short) 17);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorPis() != null)
				cell.setCellValue(nota.getValorPis().getValue().doubleValue());
			
			cell = row.createCell((short) 18);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncidepis() != null && nota.getIncidepis() && nota.getValorPis() != null)
				cell.setCellValue(nota.getValorPis().getValue().doubleValue());
			
			cell = row.createCell((short) 19);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getCofins() != null)
				cell.setCellValue(nota.getCofins()/100d);
			
			cell = row.createCell((short) 20);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorCofins() != null)
				cell.setCellValue(nota.getValorCofins().getValue().doubleValue());
			
			cell = row.createCell((short) 21);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncidecofins() != null && nota.getIncidecofins() && nota.getValorCofins() != null)
				cell.setCellValue(nota.getValorCofins().getValue().doubleValue());
			
			cell = row.createCell((short) 22);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getCsll() != null)
				cell.setCellValue(nota.getCsll()/100d);
			
			cell = row.createCell((short) 23);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorCsll() != null)
				cell.setCellValue(nota.getValorCsll().getValue().doubleValue());
			
			cell = row.createCell((short) 24);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncidecsll() != null && nota.getIncidecsll() && nota.getValorCsll() != null)
				cell.setCellValue(nota.getValorCsll().getValue().doubleValue());
			
			cell = row.createCell((short) 25);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getInss() != null)
				cell.setCellValue(nota.getInss()/100d);
			
			cell = row.createCell((short) 26);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorInss() != null)
				cell.setCellValue(nota.getValorInss().getValue().doubleValue());
			
			cell = row.createCell((short) 27);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncideinss() != null && nota.getIncideinss() && nota.getValorInss() != null)
				cell.setCellValue(nota.getValorInss().getValue().doubleValue());
			
			cell = row.createCell((short) 28);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			if(nota.getIcms() != null)
				cell.setCellValue(nota.getIcms()/100d);
			
			cell = row.createCell((short) 29);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorIcms() != null)
				cell.setCellValue(nota.getValorIcms().getValue().doubleValue());
			
			cell = row.createCell((short) 30);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getIncideicms() != null && nota.getIncideicms() && nota.getValorIcms() != null)
				cell.setCellValue(nota.getValorIcms().getValue().doubleValue());
			
			cell = row.createCell((short) 31);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(nota.getValorNota() != null)
				cell.setCellValue(nota.getValorNota().getValue().doubleValue());
		}
		
		row = planilha.createRow(planilha.getLastRowNum() +1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("Emiss�o");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("Nota Fiscal");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("N NFS-e");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("ID do contrato");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("Data de vencimento");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("CNPJ/CPF");
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("Cliente");
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("Inscri��o Municipal");
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor bruto");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Base de c�lculo Geral");
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("ISS (%)");
		
		cell = row.createCell((short) 11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor ISS");
		
		cell = row.createCell((short) 12);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor ISS Retido");
		
		cell = row.createCell((short) 13);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("IR (%)");
		
		cell = row.createCell((short) 14);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor IR");
		
		cell = row.createCell((short) 15);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor IR Retido");
		
		cell = row.createCell((short) 16);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("PIS (%)");
		
		cell = row.createCell((short) 17);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor PIS");
		
		cell = row.createCell((short) 18);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor PIS Retido");
		
		cell = row.createCell((short) 19);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("COFINS (%)");
		
		cell = row.createCell((short) 20);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor COFINS");
		
		cell = row.createCell((short) 21);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor COFINS Retido");
		
		cell = row.createCell((short) 22);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("CSLL (%)");
		
		cell = row.createCell((short) 23);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor CSLL");
		
		cell = row.createCell((short) 24);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor CSLL Retido");
		
		cell = row.createCell((short) 25);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("INSS (%)");
		
		cell = row.createCell((short) 26);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor INSS");
		
		cell = row.createCell((short) 27);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor INSS Retido");
		
		cell = row.createCell((short) 28);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
//		cell.setCellValue("ICMS (%)");
		
		cell = row.createCell((short) 29);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor ICMS");
		
		cell = row.createCell((short) 30);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor ICMS Retido");
		
		cell = row.createCell((short) 31);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor l�quido");
		
		
		//Cria totaliza��o
		row = planilha.createRow(planilha.getLastRowNum() +1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("Total");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("");
		
		int coluna = 8;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 9;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 10;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 11;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 12;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 13;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 14;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 15;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 16;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 17;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 18;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 19;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 20;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 21;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 22;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 23;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");

		coluna = 24;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 25;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 26;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 27;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 28;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_PERCENT);
//		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 29;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 30;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		coluna = 31;
		cell = row.createCell((short) coluna);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(coluna)+6+":"+SinedExcel.getAlgarismoColuna(coluna)+(planilha.getLastRowNum())+")");
		
		
		return wb.getWorkBookResource("resumofaturamento_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	private void addCell(HSSFRow row, String title, int i, HSSFCellStyle style){
		HSSFCell cell = row.createCell((short) i);
		cell.setCellStyle(style);
		cell.setCellValue(title);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca os dados na Nota Fiscal de Servi�o para Integra��o com Dom�nio
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForDominiointegracaoSaida(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForDominiointegracaoSaida(Empresa empresa, java.sql.Date dtinicio, java.sql.Date dtfim) {		
		return notaFiscalServicoDAO.findForDominiointegracaoSaida(empresa, dtinicio, dtfim);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO.findForSpedPiscofinsRegA0100(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSpedPiscofinsRegA0100(SpedpiscofinsFiltro filtro, Empresa empresa, boolean haveNfse) {
		return notaFiscalServicoDAO.findForSpedPiscofinsRegA0100(filtro, empresa, haveNfse);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForSagefiscal(SagefiscalFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<NotaFiscalServico> findForSagefiscal(SagefiscalFiltro filtro) {
		return notaFiscalServicoDAO.findForSagefiscal(filtro);
	}
	
	public boolean isExisteNota(Empresa empresa, String numeroNota) {
		return notaFiscalServicoDAO.isExisteNota(empresa, numeroNota);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForCancelamentoNfse(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaFiscalServico> findForCancelamentoNfse(String whereIn) {
		return notaFiscalServicoDAO.findForCancelamentoNfse(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForSIntegraRegistro75(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSIntegraRegistro75(SintegraFiltro filtro) {
		return notaFiscalServicoDAO.findForSIntegraRegistro75(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForSIntegraRegistro50(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSIntegraRegistro50(SintegraFiltro filtro) {
		return notaFiscalServicoDAO.findForSIntegraRegistro50(filtro);
	}

//	/**
//	 * M�todo com refer�ncia no DAO
//	 *
//	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForEnviarXmlParaCliente(String whereIn)
//	 *
//	 * @param whereIn
//	 * @return
//	 * @author Luiz Fernando
//	 */
//	public List<NotaFiscalServico> findForEnviarXmlParaCliente(String whereIn) {
//		return notaFiscalServicoDAO.findForEnviarXmlParaCliente(whereIn);
//	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 28/03/2012
	 * @param nota
	 * @return
	 */
	public NotaFiscalServico findDescontosByNota(Nota nota){
		return notaFiscalServicoDAO.findDescontosByNota(nota);
	}
	
	/**
	 * Calcula os impostos a partir de um grupo de tributa��o.
	 *
	 * @param nota
	 * @param grupotributacao
	 * @since 23/05/2012
	 * @author Rodrigo Freitas
	 */
	public void calculaTributacaoNota(NotaFiscalServico nota, Grupotributacao grupotributacao, Money basecalculo){
		if(grupotributacao != null){
			nota.setCstcofins(grupotributacao.getTipocobrancacofins());
			nota.setCstpis(grupotributacao.getTipocobrancapis());
			
			Empresa empresa = nota.getEmpresa();
			Cliente cliente = nota.getCliente();
			
			Endereco enderecoCliente = cliente != null ? cliente.getEndereco() : null;
			Endereco enderecoNota = nota.getEnderecoCliente();
			Endereco enderecoEmpresa = empresa.getEndereco();
			
			Municipio municipioNota = nota.getMunicipioissqn();
			Municipio municipioProjeto = nota.getProjeto() != null ? nota.getProjeto().getMunicipio() : null;
			Municipio municipioCliente = enderecoNota != null ? enderecoNota.getMunicipio() : (enderecoCliente != null ? enderecoCliente.getMunicipio() : null);
			Municipio municipioEmpresa = enderecoEmpresa != null ? enderecoEmpresa.getMunicipio() : null;
			
			boolean tributacaomunicipiocliente = grupotributacao.getTributacaomunicipiocliente() != null && grupotributacao.getTributacaomunicipiocliente();
			boolean tributacaomunicipioprojeto = grupotributacao.getTributacaomunicipioprojeto() != null && grupotributacao.getTributacaomunicipioprojeto();
			
			if(tributacaomunicipioprojeto && municipioProjeto != null){
				nota.setMunicipioissqn(municipioProjeto);
				municipioNota = municipioProjeto;
				nota.setNaturezaoperacao(!municipioProjeto.equals(municipioEmpresa) ? Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO : Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			} else if(tributacaomunicipiocliente && municipioCliente != null){
				nota.setMunicipioissqn(municipioCliente);
				municipioNota = municipioCliente;
				nota.setNaturezaoperacao(!municipioCliente.equals(municipioEmpresa) ? Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO : Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			}
			
//			String paramInveterRetencaoPISCOFINS = parametrogeralService.getValorPorNome(Parametrogeral.INVERTER_RETENCAO_PISCOFINS);
			
			List<Grupotributacaoimposto> listaGrupotributacaoimposto = grupotributacaoimpostoService.findByGrupotributacao(grupotributacao);
			for (Grupotributacaoimposto gti : listaGrupotributacaoimposto){
				
				boolean incideImposto = false;
				boolean buscaraliquotafaixaimposto = gti.getBuscaraliquotafixafaixaimposto() != null && gti.getBuscaraliquotafixafaixaimposto();
				
				boolean clienteJuridico = cliente != null && cliente.getTipopessoa() != null && cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA);
				boolean clienteCrtNormal = (cliente == null || cliente.getCrt() == null) || (cliente.getCrt() != null && cliente.getCrt().equals(Codigoregimetributario.NORMAL));
				boolean clienteForcarIncide = cliente != null && cliente.getIncidiriss() != null &&  cliente.getIncidiriss();
				
				if (clienteJuridico && clienteCrtNormal)
					incideImposto = true;
				
				Municipio municipio = null;
				if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
					if(municipioNota != null)
						municipio = municipioNota;
					else if (enderecoNota != null && enderecoNota.getMunicipio() != null)
						municipio = enderecoNota.getMunicipio();
					else if (cliente != null && enderecoCliente != null)
						municipio = enderecoCliente.getMunicipio();
				} else if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
					if(municipioNota != null)
						municipio = municipioNota; 
					else if (empresa != null && enderecoEmpresa != null)
						municipio = municipioEmpresa; 
				}else if (buscaraliquotafaixaimposto){
					if(municipioNota != null)
						municipio = municipioNota; 
					else if (empresa != null && enderecoEmpresa != null)
						municipio = municipioEmpresa; 
				}
	
				Codigotributacao codigotributacao = nota.getCodigotributacao();
				
				Faixaimposto fi = null;
				if(buscaraliquotafaixaimposto){
					fi = faixaimpostoService.findByControleMunicipio(empresa, cliente, gti.getControle(), municipio, codigotributacao);
				}
				ImpostoVO imposto = faixaimpostoService.calculaValorFaixaImposto(cliente, NotaTipo.NOTA_FISCAL_SERVICO, fi, basecalculo, gti.getBasecalculopercentual(), codigotributacao, nota);
				
				Boolean retencao = null;
				if(gti.getRetencao() != null) {
					if(gti.getRetencao().equals(Retencao.SEMPRE_RETER)){
						retencao = Boolean.TRUE;
					}else if(gti.getRetencao().equals(Retencao.NUNCA_RETER)){
						retencao = Boolean.FALSE;
					}
				}
				
				if (imposto != null){
					double aliquota = gti.getAliquotafixa() != null && gti.getAliquotafixa().getValue().doubleValue() > 0 && !buscaraliquotafaixaimposto ? gti.getAliquotafixa().getValue().doubleValue() : imposto.getAliquota().getValue().doubleValue();										
					if (Faixaimpostocontrole.ISS.equals(gti.getControle())){
						
						//Se o munic�pio da empresa for diferente do munic�pio do cliente n�o incide ISS
						if (empresa != null && enderecoEmpresa != null && municipioEmpresa != null &&
								enderecoNota != null && enderecoNota.getMunicipio() != null &&
							!municipioEmpresa.equals(enderecoNota.getMunicipio()))
							incideImposto = false;
						else if (empresa != null && enderecoEmpresa != null && municipioEmpresa != null &&
								cliente != null && enderecoCliente != null && enderecoCliente.getMunicipio() != null &&
								!municipioEmpresa.equals(enderecoCliente.getMunicipio()))
							incideImposto = false;
						
						nota.setBasecalculoiss(imposto.getBasecalculo());
						nota.setIss(aliquota);
						nota.setIncideiss(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativoiss(imposto.getCumulativoCobrado());
						
						// Se estiver marcado o check de incidir ISS no cliente � para incidir independente do valor.
						if(clienteForcarIncide)
							nota.setIncideiss(Boolean.TRUE);
						
						if(retencao != null){
							nota.setIncideiss(retencao);
						}
						
					}
					if (Faixaimpostocontrole.IR.equals(gti.getControle())){
						nota.setBasecalculoir(imposto.getBasecalculo());
						nota.setIr(aliquota);
						nota.setIncideir(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativoir(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncideir(retencao);
						}
					}	
					if (Faixaimpostocontrole.INSS.equals(gti.getControle())){
						nota.setBasecalculoinss(imposto.getBasecalculo());
						nota.setInss(aliquota);
						nota.setIncideinss(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativoinss(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncideinss(retencao);
						}
					}
					if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
						nota.setBasecalculopis(imposto.getBasecalculo());
						nota.setPis(aliquota);
						nota.setPisretido(aliquota);
						nota.setIncidepis(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativopis(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncidepis(retencao);
						}
						
						if(imposto.getAliquotaretencao() != null && imposto.getAliquotaretencao().getValue().doubleValue() > 0){
							nota.setPisretido(imposto.getAliquotaretencao().getValue().doubleValue());
						}else {
							nota.setPisretido(aliquota);
						}
					}
					if (Faixaimpostocontrole.ICMS.equals(gti.getControle())){
						nota.setBasecalculoicms(imposto.getBasecalculo());
						nota.setIcms(aliquota);
						nota.setIncideicms(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativoicms(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncideicms(retencao);
						}
					}
					if (Faixaimpostocontrole.CSLL.equals(gti.getControle())){
						nota.setBasecalculocsll(imposto.getBasecalculo());
						nota.setCsll(aliquota);
						nota.setIncidecsll(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativocsll(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncidecsll(retencao);
						}
					}
					if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
						nota.setBasecalculocofins(imposto.getBasecalculo());
						nota.setCofinsretido(aliquota);
						nota.setCofins(aliquota);
						nota.setIncidecofins(incideImposto ? imposto.getIncide() : false);
						nota.setImpostocumulativocofins(imposto.getCumulativoCobrado());
						
						if(retencao != null){
							nota.setIncidecofins(retencao);
						}
						
						if(imposto.getAliquotaretencao() != null && imposto.getAliquotaretencao().getValue().doubleValue() > 0){
							nota.setCofinsretido(imposto.getAliquotaretencao().getValue().doubleValue());
						}else {
							nota.setCofinsretido(aliquota);
						}
					}
				}				
			}
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#registrarHistoricoEnvioEmailXmlCliente(NotaFiscalServico nfs, String historico)
	 *
	 * @param nfs
	 * @param historico
	 * @author Luiz Fernando
	 */
	public void registrarHistoricoEnvioEmailXmlCliente(Nota nfs, String historico) {
		notaFiscalServicoDAO.registrarHistoricoEnvioEmailXmlCliente(nfs, historico);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte)
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaFiscalServico> findForRecalculoComissao(String whereIn, java.sql.Date dtrecalculoDe, java.sql.Date dtrecalculoAte) {
		return notaFiscalServicoDAO.findForRecalculoComissao(whereIn, dtrecalculoDe, dtrecalculoAte);
	}
	
	/**
	 * Preenche com valores padr�es para a tributa��o a partir de uma empresa.
	 *
	 * @param empresa
	 * @param bean
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public void preencheValoresPadroesEmpresa(Empresa empresa, NotaFiscalServico bean) {
		empresa = empresaService.loadForEntrada(empresa);
		if(empresa != null){
			Endereco endereco = empresa.getEndereco();
			
			bean.setEmpresa(empresa);
			
			if(endereco != null && endereco.getMunicipio() != null) bean.setMunicipioissqn(endereco.getMunicipio());
			
			if(empresa.getNaturezaoperacao() != null) bean.setNaturezaoperacao(empresa.getNaturezaoperacao());
			else {
				Naturezaoperacao naturezaoperacao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_SERVICO);
				bean.setNaturezaoperacao(naturezaoperacao != null ? naturezaoperacao : Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			}
			
			if(empresa.getRegimetributacao() != null) bean.setRegimetributacao(empresa.getRegimetributacao());
			if(empresa.getCodigotributacao() != null) bean.setCodigotributacao(empresa.getCodigotributacao());
			if(empresa.getItemlistaservico() != null) bean.setItemlistaservicoBean(empresa.getItemlistaservico());
			
			if(empresa.getListaEmpresacodigocnae() != null){
				Codigocnae codigocnae = null;
				for (Empresacodigocnae ec : empresa.getListaEmpresacodigocnae()) {
					if(codigocnae == null) codigocnae = ec.getCodigocnae();
					if(ec.getPrincipal() != null && ec.getPrincipal()){
						codigocnae = ec.getCodigocnae();
						break;
					}
				}
				bean.setCodigocnaeBean(codigocnae);
			}
			
			if(empresa.getCstpisnfs() != null){
				bean.setCstpis(empresa.getCstpisnfs());
			}
			if(empresa.getCstcofinsnfs() != null){
				bean.setCstcofins(empresa.getCstcofinsnfs());
			}
			
			if (empresa.getIncentivoFiscalIss() != null && empresa.getIncentivoFiscalIss() > 0) {
 				bean.setIncentivoFiscalIss(empresa.getIncentivoFiscalIss());
 			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro) {
		return notaFiscalServicoDAO.findforApuracaopiscofins(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		return notaFiscalServicoDAO.verificaListaFechamento(whereIn);
	}
	public boolean isExisteNotaByNumeronfse(Empresa empresa, String numeronfse, Configuracaonfe configuracaonfe) {
		return notaFiscalServicoDAO.isExisteNotaByNumeronfse(empresa, numeronfse, configuracaonfe);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#loadNotasServicoAssociar(String whereInDocumentos)
	 *
	 * @param whereInDocumentos
	 * @return
	 * @author Luiz Fernando
	 * @param list 
	 * @param date2 
	 * @param date 
	 */
	public List<NotaFiscalServico> loadNotasServicoAssociar(String whereInDocumentos, java.sql.Date dtEmissao1, java.sql.Date dtEmissao2, List<NotaStatus> listaSituacao) {
		return notaFiscalServicoDAO.loadNotasServicoAssociar(whereInDocumentos, dtEmissao1, dtEmissao2, listaSituacao);
	}
	
	/**
	 * M�todo que busca a menor data de vencimento
	 *
	 * @param listaEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public java.sql.Date getDtemissaoMenor(List<NotaFiscalServico> lisNotaFiscalServico) {
		java.sql.Date dtvencimento = null;
		if(lisNotaFiscalServico != null && !lisNotaFiscalServico.isEmpty()){
			for(NotaFiscalServico nfs : lisNotaFiscalServico){
				if(dtvencimento == null){
					dtvencimento = nfs.getDtEmissao();
				}else if(nfs.getDtEmissao() != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtvencimento, new java.sql.Date(nfs.getDtEmissao().getTime()))){
					dtvencimento = nfs.getDtEmissao();
				}
			}
		}
		return dtvencimento;
	}
	
	public void gerarHistoricoLiquidarNota(Documento documento, String whereIn, boolean gerarHistoricoConta) {		
		List<NotaFiscalServico> lista = this.loadForReceita(whereIn);
		
		if(gerarHistoricoConta){
			if(lista.size() > 1){
				StringBuilder obs = new StringBuilder();
				for(NotaFiscalServico nota : lista){
					obs.append("<a href=\"javascript:visualizarNotaFiscalServico("+nota.getCdNota()+")\">"+(nota.getNumero() != null ? nota.getNumero() : "sem n�mero")+"</a>,");
				}
				documento.setObservacaoHistorico("Origem da(s) nota(s): " + obs.substring(0, obs.length()-1));
			}else {
				NotaFiscalServico notafiscalservico = lista.get(0);
				documento.setObservacaoHistorico("Origem da nota <a href=\"javascript:visualizarNotaFiscalServico("+notafiscalservico.getCdNota()+")\">"+(notafiscalservico.getNumero() != null ? notafiscalservico.getNumero() : "sem n�mero")+"</a>.");
			}
			Documentohistorico documentohistorico = new Documentohistorico(documento);
			documentohistoricoService.saveOrUpdate(documentohistorico);
		}
		
		for(NotaFiscalServico nfs : lista){
			notaService.liquidarNota(nfs);
			
			if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
				StringBuilder obs = new StringBuilder();
				for(Documento doc : documento.getListaDocumento()){
					NotaDocumento notaDocumento = new NotaDocumento();
					notaDocumento.setDocumento(doc);
					notaDocumento.setNota(nfs);
					notaDocumentoService.saveOrUpdate(notaDocumento);
					
					obs.append("<a href=\"javascript:visualizarContaReceber("+doc.getCddocumento()+")\">"+doc.getCddocumento()+"</a>,");
				}
				
				NotaHistorico notaHistorico = new NotaHistorico(null, 
						nfs, 
						nfs.getNotaStatus(), 
						"Gerada a(s) conta(s) a receber " + obs.substring(0, obs.length()-1), 
						SinedUtil.getUsuarioLogado().getCdpessoa(), 
						new Timestamp(System.currentTimeMillis()));
				
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}else {
				NotaDocumento notaDocumento = new NotaDocumento();
				notaDocumento.setDocumento(documento);
				notaDocumento.setNota(nfs);
				notaDocumentoService.saveOrUpdate(notaDocumento);
				
				NotaHistorico notaHistorico = new NotaHistorico(null, 
						nfs, 
						nfs.getNotaStatus(), 
						"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
						SinedUtil.getUsuarioLogado().getCdpessoa(), 
						new Timestamp(System.currentTimeMillis()));
				
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		}		
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/12/2014
	 */
	public List<NotaFiscalServico> findForConferenciaNfse() {
		return notaFiscalServicoDAO.findForConferenciaNfse();
	}
	
	/**
	 * M�todo que cria a nota a partir da proposta
	 *
	 * @param proposta
	 * @return
	 * @author Luiz Fernando
	 */
	public NotaFiscalServico gerarNotaByProposta(Proposta proposta) {
		NotaFiscalServico form = new NotaFiscalServico();
		
		if(proposta != null && proposta.getCdproposta() != null){
			form.setFromProposta(true);
			form.setWhereIn(proposta.getCdproposta().toString());
			form.setCliente(proposta.getCliente());
			
			List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
			NotaFiscalServicoItem item = new NotaFiscalServicoItem();
			item.setDescricao(proposta.getDescricao());
			item.setPrecoUnitario(proposta.getAux_proposta().getValor().getValue().doubleValue());
			item.setQtde(1.0);
			listaItens.add(item);
			
			form.setListaItens(listaItens);
		}
		
		return form;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForAtualizarRequisicao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForAtualizarRequisicao(String whereIn){
		return notaFiscalServicoDAO.findForAtualizarRequisicao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForApuracaoimposto(ApuracaoimpostoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForApuracaoimposto(ApuracaoimpostoFiltro filtro) {
		return notaFiscalServicoDAO.findForApuracaoimposto(filtro);
	}
	
	/**
	 * 
	 * @param whereInCddocumento
	 * @param notaStatus
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPossuiNotaFiscalServico(String whereInCddocumento, NotaStatus... notaStatus){
		return notaFiscalServicoDAO.isPossuiNotaFiscalServico(whereInCddocumento, notaStatus);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#findForAtualizacaoData(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public List<NotaFiscalServico> findForAtualizacaoData(String whereIn) {
		return notaFiscalServicoDAO.findForAtualizacaoData(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nfs
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public void updateDataemissaoAtual(NotaFiscalServico nfs) {
		notaFiscalServicoDAO.updateDataemissaoAtual(nfs);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nfs
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public void updateDatavencimentoAtual(NotaFiscalServico nfs) {
		notaFiscalServicoDAO.updateDatavencimentoAtual(nfs);
	}
	
	/**
	 * M�todo que gera nota de servi�o a partir da venda
	 *
	 * @param nf
	 * @param venda
	 * @param expedicoes
	 * @param somenteservico
	 * @return
	 * @author Luiz Fernando
	 */
	public NotaFiscalServico geraNotaVenda(NotaFiscalServico nf, Venda venda, String expedicoes, Boolean somenteservico) {
		venda = vendaService.loadForEntrada(venda);
		if (venda.getColaborador() != null && venda.getColaborador().getCdpessoa() != null){
			venda.setColaborador(colaboradorService.load(venda.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
		}
		if (venda.getCliente() != null && venda.getCliente().getCdpessoa() != null){
			venda.setCliente(clienteService.load(venda.getCliente(), "cliente.cdpessoa, cliente.nome"));
		}
		if (venda.getEmpresa()!=null && venda.getEmpresa().getCdpessoa()!=null){
			venda.setEmpresa(empresaService.loadForVenda(venda.getEmpresa()));
		}
		
		venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
		venda.setListavendamaterial(vendamaterialService.findByVenda(venda));
		venda.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(venda));
		vendamaterialService.verificaVendaKitFlexivelExibir(venda);

		if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
			if(venda.getListavendapagamento().size() == 1){
				Vendapagamento vendapagamento = venda.getListavendapagamento().get(0);
				nf.setDtVencimento(vendapagamento.getDataparcela());
			}else {
				java.sql.Date dtvencimento = null;
				for(Vendapagamento vendapagamento : venda.getListavendapagamento()){
					if(vendapagamento.getDataparcela() != null){
						if(dtvencimento == null){
							dtvencimento = vendapagamento.getDataparcela();
						}else if(SinedDateUtils.afterIgnoreHour(vendapagamento.getDataparcela(), dtvencimento)){
							dtvencimento = vendapagamento.getDataparcela();
						}
					}
				}
				if(dtvencimento != null){
					nf.setDtVencimento(dtvencimento);
				}
			}
		}
		
		nf.setEmpresa(venda.getEmpresa());
		nf.setNumero("");
		if(somenteservico != null && somenteservico) nf.setUsarSequencial(true);
		
		if(venda.getEmpresa() != null && venda.getEmpresa().getCdpessoa() != null && venda.getDocumentotipo() != null &&
				venda.getDocumentotipo().getCddocumentotipo() != null){
			Empresa empresa = venda.getEmpresa();
			if(empresa != null && empresa.getDiscriminarservicoqtdevalor() != null && empresa.getDiscriminarservicoqtdevalor()){
				Documentotipo documentotipo = documentotipoService.load(venda.getDocumentotipo(), "documentotipo.cddocumentotipo, documentotipo.nome");
				if(documentotipo != null && documentotipo.getNome() != null && !"".equals(documentotipo.getNome())){
					if(nf.getDadosAdicionais() != null && !"".equals(nf.getDadosAdicionais())){
						if(!nf.getDadosAdicionais().contains(("Forma de pagamento: " + venda.getDocumentotipo().getNome()))){
							nf.setDadosAdicionais(" Forma de pagamento: " + venda.getDocumentotipo().getNome());
						}
					}else {
						nf.setDadosAdicionais("Forma de pagamento: " + venda.getDocumentotipo().getNome());
					}
				}
			}
		}
		
		Cliente cliente = clienteService.carregarDadosCliente(venda.getCliente());
		Endereco enderecoCliente = null;
		Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);
		
		Set<Endereco> listaEndereco = cliente.getListaEndereco();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for (Endereco endereco : listaEndereco) {
				if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
					enderecoCliente = endereco;
					break;
				}
			}
		}
		
		nf.setCliente(cliente);
		nf.setInscricaomunicipal(cliente.getInscricaomunicipal());
		nf.setEnderecoCliente(enderecoCliente != null ? enderecoCliente : venda.getEndereco());
		if(nf.getEnderecoCliente() != null && nf.getEnderecoCliente().getMunicipio() != null){
			nf.setMunicipioissqn(nf.getEnderecoCliente().getMunicipio());
		}
		nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
		nf.setTelefoneClienteTransiente(telefoneCliente);
		nf.setProjeto(venda.getProjeto());
		nf.setContaboleto(venda.getContaboleto());
		if(nf.getContaboleto() != null){
			nf.setContacarteira(contacarteiraService.loadPadraoBoleto(nf.getContaboleto()));
		}
		
		if(nf.getCliente() != null && nf.getCliente().getCpfOuCnpj() != null){
			nf.getCliente().setCpfcnpj(nf.getCliente().getCpfOuCnpj());
		}
		
		List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
		List<Vendamaterial> listavendamaterial = venda.getListavendamaterial();
		
		Money precoTotalComArredondamento = new Money();
		Money precoTotalReal = new Money();
		
		for (Vendamaterial vendamaterial : listavendamaterial) {
			boolean servico = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getServico() != null && vendamaterial.getMaterial().getServico();
			boolean tributacaomunicipal = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaomunicipal() != null && vendamaterial.getMaterial().getTributacaomunicipal();
			
			if(somenteservico != null && somenteservico && vendamaterial.getMaterial() != null && (servico || tributacaomunicipal)){
				Double quantidade = vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador();
				if(multiplicador == null) multiplicador = 1.0;
				
				Material material = vendamaterial.getMaterial();
				Material materialbanda = vendamaterial.getPneu() != null ? vendamaterial.getPneu().getMaterialbanda() : null;
				NotaFiscalServicoItem item = new NotaFiscalServicoItem();
				
				item.setNotaFiscalServico(nf);
				String nomeComAlturaLarguraComprimento = vendaService.getAlturaLarguraComprimentoConcatenados(vendamaterial, material);
				item.setMaterial(material);
				item.setDescricao(getDescricaoItemNfs(material, materialbanda, nomeComAlturaLarguraComprimento, vendamaterial.getPneu(), false, false, vendamaterial.getObservacao(), venda.getIdentificadorexternoAndIdentificador()));
				item.setQtde(quantidade);
				if(vendamaterial.getPreco() != null){
					item.setPrecoUnitario(new Money(vendamaterial.getPreco()).getValue().doubleValue());
				}
				
				Money valorDesconto = new Money();
				Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
				
				if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
					item.setDesconto(valorDescontoItem);
					
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(listavendamaterial, vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
					if(valorDesconto != null){
						nf.setDescontoincondicionado(nf.getDescontoincondicionado() != null ? nf.getDescontoincondicionado().add(valorDesconto) : valorDesconto);
					}
				} else {
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(listavendamaterial, vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
	
					if(quantidade != null){
						valorDescontoItem = valorDescontoItem.divide(new Money(quantidade*multiplicador));
					}
					valorDesconto = valorDesconto.add(valorDescontoItem);
					if(multiplicador != null){
						if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
							if(item.getQtde() != null)
								item.setQtde(item.getQtde() * multiplicador); 
						} else {
							if(item.getPrecoUnitario() != null)
								item.setPrecoUnitario(new Money(item.getPrecoUnitario() * multiplicador).getValue().doubleValue());
						}
					}
					
					if(item.getPrecoUnitario() != null){
						if(valorDesconto != null && valorDesconto.getValue().doubleValue() > 0 && parametrogeralService.getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO)){
							precoTotalComArredondamento = precoTotalReal.add(new Money(SinedUtil.roundByParametro(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue())));
							precoTotalReal = precoTotalReal.add(new Money(item.getPrecoUnitario()).subtract(valorDesconto));
							
							item.setPrecoUnitario(SinedUtil.roundByParametro(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue()));
						}else {
							item.setPrecoUnitario(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue());
						}
					}
				}
				
				if(item.getPrecoUnitario() != null && item.getQtde() != null){
					item.setPrecoTotal(new Money(item.getPrecoUnitario() * item.getQtde()));
					if(item.getDesconto() != null){
						item.setPrecoTotal(item.getPrecoTotal().subtract(item.getDesconto()));
					}
				}
				
				if (vendamaterial.getCdvendamaterial() != null) {
					item.setVendamaterial(vendamaterial);
				}
				
				listaItens.add(item);
			}
		}
		
		NotaFiscalServicoItem notaFiscalServicoItem = null;
		for (NotaFiscalServicoItem nfpi : listaItens) {
			if(notaFiscalServicoItem == null && nfpi.getPrecoUnitario() != null && nfpi.getQtde() != null && nfpi.getQtde() == 1){
				notaFiscalServicoItem = nfpi;
			}
		}
		
		if(notaFiscalServicoItem != null && precoTotalComArredondamento != null && precoTotalComArredondamento.getValue().doubleValue() > 0 && 
				precoTotalReal != null && precoTotalReal.getValue().doubleValue() > 0){
			double descontoRealDbl = SinedUtil.roundByParametro(precoTotalComArredondamento.getValue().doubleValue());
			double descontoTotalDbl = SinedUtil.roundByParametro(precoTotalReal.getValue().doubleValue());
			if(descontoRealDbl != descontoTotalDbl){
				double diferenca = descontoTotalDbl - descontoRealDbl;
				if(diferenca != 0d){
					notaFiscalServicoItem.setPrecoUnitario(SinedUtil.roundByParametro(notaFiscalServicoItem.getPrecoUnitario() + new Double(diferenca)));
				}
			}
		}
		
		addItensInterrompidos(venda, listaItens, nf);
		vendaService.verificaTemplateDiscriminacao(nf, listaItens, venda);
		nf.setListaItens(listaItens);
		
		Money valorTotalServicos = new Money(0.0);
		for (NotaFiscalServicoItem nfpi : nf.getListaItens()) {
			valorTotalServicos = valorTotalServicos.add(nfpi.getPrecoTotal());
		}
		
		Venda vendaaux = vendaService.loadWithPedidovenda(venda);
		if(vendaaux != null && 
				vendaaux.getPedidovendatipo() != null && 
				vendaaux.getPedidovendatipo().getNaturezaoperacaoservico() != null &&
				NotaTipo.NOTA_FISCAL_SERVICO.equals(vendaaux.getPedidovendatipo().getNaturezaoperacaoservico().getNotaTipo())){ 
			nf.setNaturezaoperacao(vendaaux.getPedidovendatipo().getNaturezaoperacaoservico());
		}else {
			nf.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_SERVICO));
		}
		
		if(nf.getDescontoincondicionado() != null && nf.getDescontoincondicionado().getValue().doubleValue() > 0){
			nf.setBasecalculo(valorTotalServicos.subtract(nf.getDescontoincondicionado()));
		}else {
			nf.setBasecalculo(valorTotalServicos);
		}
		
		nf.setBasecalculoir(nf.getBasecalculo());
		nf.setBasecalculoiss(nf.getBasecalculo());
		nf.setBasecalculopis(nf.getBasecalculo());
		nf.setBasecalculocofins(nf.getBasecalculo());
		nf.setBasecalculocsll(nf.getBasecalculo());
		nf.setBasecalculoinss(nf.getBasecalculo());
		nf.setBasecalculoicms(nf.getBasecalculo());
		
		nf.setNotaStatus(NotaStatus.EMITIDA);	
		nf.setCdvendaassociada(venda.getCdvenda());
		nf.setPrazopagamentofatura(venda.getPrazopagamento());
		nf.setDocumentotipo(venda.getDocumentotipo());
		
		if(vendaaux == null || vendaaux.getPedidovendatipo() == null || vendaaux.getPedidovendatipo().getBonificacao() == null ||
				!vendaaux.getPedidovendatipo().getBonificacao()){
			List<Documentoorigem> listaDocumentoOrigem = documentoorigemService.buscarListaParaDuplicatas(venda);
			List<Notafiscalservicoduplicata> listaDuplicata = new ArrayList<Notafiscalservicoduplicata>();
			if(listaDocumentoOrigem != null && listaDocumentoOrigem.size() > 0) {
				Notafiscalservicoduplicata nfDuplicata;
				for (Documentoorigem docOrigem : listaDocumentoOrigem) {
					nfDuplicata = new Notafiscalservicoduplicata();
					nfDuplicata.setDtvencimento(docOrigem.getDocumento().getDtvencimento());
					nfDuplicata.setValor(docOrigem.getDocumento().getValor());
					nfDuplicata.setDocumentotipo(docOrigem.getDocumento().getDocumentotipo()); 
					listaDuplicata.add(nfDuplicata);
				}
			}else if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
				Notafiscalservicoduplicata nfDuplicata;
				Money valorTotalPagamentos = new Money();
				for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
					nfDuplicata = new Notafiscalservicoduplicata();
					nfDuplicata.setDtvencimento(vendapagamento.getDataparcela());
					nfDuplicata.setValor(vendapagamento.getValororiginal());
					nfDuplicata.setDocumentotipo(vendapagamento.getDocumentotipo());
					listaDuplicata.add(nfDuplicata);
					
					if(vendapagamento.getValororiginal() != null){
						valorTotalPagamentos = valorTotalPagamentos.add(vendapagamento.getValororiginal());
					}
				}
				
				if(vendaaux != null && vendaaux.getPedidovendatipo() != null && !Boolean.TRUE.equals(vendaaux.getPedidovendatipo().getNaoatualizarvencimento()) && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(vendaaux.getPedidovendatipo().getGeracaocontareceberEnum())){
					Venda v = vendaService.loadForEntrada(venda);
					if (v.getCliente() != null && v.getCliente().getCdpessoa() != null) v.setCliente(clienteService.load(v.getCliente(), "cliente.cdpessoa, cliente.nome"));
					v.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(v));
					v.setListavendamaterial(vendamaterialService.findByVenda(v));
					v.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(v));
					v.setDtvendaEmissaonota(nf.getDtEmissao());
					vendaService.ajustaValorParcelaVenda(v, valorTotalPagamentos, listaDuplicata, false, false, true, v.getPrazopagamento(), true);
				}
			}
			nf.setListaDuplicata(listaDuplicata);
		}
		
		return nf;
	}
	
	/**
	 * M�todo que gera nota a partir das vendas
	 *
	 * @param listaVendas
	 * @param agrupamento
	 * @param somenteservico
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> gerarFaturamentoVenda(WebRequestContext request, List<Venda> listaVendas, Boolean agrupamento, Boolean somenteservico, Boolean notaseparada) {
		Boolean exibirMensagemInfTributacao = false;
		List<NotaFiscalServico> notas = this.getListaNotaFiscalServico(request, listaVendas, agrupamento, somenteservico, notaseparada, exibirMensagemInfTributacao);
		
		for (NotaFiscalServico notafiscalservico : notas){ 
			if(notafiscalservico.getListaItens() != null && !notafiscalservico.getListaItens().isEmpty()){
				String infoContribuinteTemplate = montaInformacoesContribuinteTemplate(notafiscalservico.getListaVenda(), notafiscalservico);
				if(!org.apache.commons.lang.StringUtils.isEmpty(infoContribuinteTemplate)){
					notafiscalservico.setDadosAdicionais(infoContribuinteTemplate);
				}
			}
			notafiscalservico.setDocumentotipo(vendaService.getDocumentotipoVenda(notafiscalservico.getListaVenda()));
			notafiscalservico.setContaboleto(vendaService.getContaboletoVenda(notafiscalservico.getListaVenda()));
			if(notafiscalservico.getContaboleto() != null){
				notafiscalservico.setContacarteira(contacarteiraService.loadPadraoBoleto(notafiscalservico.getContaboleto()));
			}
		}

		boolean gerarNfSeparadaTipoPedidoVenda = notaseparada && parametrogeralService.getBoolean(Parametrogeral.GERAR_NF_SEPARADA_TIPOPEDIDOVENDA);
		for (NotaFiscalServico notafiscalservico : notas){ 
			if(notafiscalservico.getListaItens() != null && !notafiscalservico.getListaItens().isEmpty()){
				boolean haveVotorantim = configuracaonfeService.havePrefixoativo(notafiscalservico.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
				
				if(!haveVotorantim){
					Integer proximoNumero = empresaService.carregaProxNumNF(notafiscalservico.getEmpresa());
					if(proximoNumero == null){
						proximoNumero = 1;
					}
					notafiscalservico.setNumero(proximoNumero.toString());
					proximoNumero++;
		
					empresaService.updateProximoNumNF(notafiscalservico.getEmpresa(), proximoNumero);
				}
				
				notafiscalservico.setValororiginalfatura(notafiscalservico.getValorNota());
				Money valororiginal = notafiscalservico.getValororiginalfatura();
				if(valororiginal == null){
					valororiginal = new Money();
				}
				
				Money valordesconto = notafiscalservico.getValordescontofatura();
				if(valordesconto == null){
					valordesconto = new Money();
				}
				
				notafiscalservico.setValorliquidofatura(valororiginal.subtract(valordesconto));

				verificaDuplicatasVariasVenda(notafiscalservico);
				saveOrUpdate(notafiscalservico);
				
				List<Venda> listaVendaNota = notafiscalservico.getListaVenda();
				
				vendaService.adicionarParcelaNumeroDocumento(listaVendaNota, notafiscalservico, notafiscalservico.getNumero(), notaseparada, true, true, false);
				
				if(listaVendaNota != null && listaVendaNota.size() > 0){
					boolean recalcularValoresParcelas = listaVendaNota.size() == 1;
					GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
					List<Documento> listaContareceber = new ArrayList<Documento>();
					for (Venda venda : listaVendaNota) {
						notaVendaService.saveOrUpdate(new NotaVenda(notafiscalservico, venda));
						
						Vendahistorico vendahistorico = new Vendahistorico();
						vendahistorico.setVenda(venda);
						String numero = notafiscalservico.getNumero() == null || notafiscalservico.getNumero().equals("") ? "sem n�mero" : notafiscalservico.getNumero();
						vendahistorico.setAcao("Gera��o da nota " + numero);
						vendahistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNotaServico("+notafiscalservico.getCdNota()+");\">"+numero+"</a>.");
						vendahistoricoService.saveOrUpdate(vendahistorico);
						
						geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
						if(venda.getPedidovendatipo() != null){
							geracaocontareceberEnum = venda.getPedidovendatipo().getGeracaocontareceberEnum();
						}
						if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum) && !gerarNfSeparadaTipoPedidoVenda){
							vendaService.gerarContareceberAposEmissaonota(request, venda, notafiscalservico.getDtEmissao(), null, null, notafiscalservico.getValorNota(), null, (somenteservico ? NotaTipo.NOTA_FISCAL_SERVICO : null), null, somenteservico != null && somenteservico, recalcularValoresParcelas, listaContareceber, null, venda.getPrazopagamento(), true);
						}
						
						List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(venda);
						if(SinedUtil.isListNotEmpty(listadocumentoorigem)){
							for (Documentoorigem documentoorigem : listadocumentoorigem) {
								Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
								if(documento != null){
									vendaService.gerarMensagensDocumento(venda, documento, notafiscalservico, true, notaseparada);
									
									NotaDocumento notaDocumento = new NotaDocumento();
									notaDocumento.setDocumento(documento);
									notaDocumento.setNota(notafiscalservico);
									notaDocumentoService.saveOrUpdate(notaDocumento);
								}
							}
						}
						
						if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getGerarnotaretornovenda()) && venda.getPedidovenda() != null){
							try {
								pedidovendaService.gerarNotaFiscalRetorno(request, pedidovendaService.findForNotaRetorno(venda.getPedidovenda()), venda, true);
							} catch (IOException e) {
								request.addError("N�o foi poss�vel gerar nota de retorno. " + venda.getCdvenda());
								e.printStackTrace();
							}
						}
						
						if (org.apache.commons.lang.StringUtils.isNotEmpty(notafiscalservico.getPneusJaAssociado())) {
							vendahistoricoService.salvarHistoricoPneuJaAssociado(notafiscalservico, venda, notafiscalservico.getPneusJaAssociado());
						}
					}
					
					if(SinedUtil.isListNotEmpty(listaContareceber) && parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA)){
						for(Documento documento : listaContareceber){
							documentoService.baixaAutomaticaByVinculoProvisionado(documento);
						}
					}
				}
			}
		}
		
		if(exibirMensagemInfTributacao){
			StringBuilder linkNota = new StringBuilder();
			for (NotaFiscalServico notafiscalservico : notas){ 
				if(notafiscalservico.getCdNota() != null){
					String numero = org.apache.commons.lang.StringUtils.isNotEmpty(notafiscalservico.getNumero()) ? notafiscalservico.getNumero() : notafiscalservico.getCdNota().toString(); 
					linkNota.append("<a href=/"+ SinedUtil.getContexto() + "/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=" + notafiscalservico.getCdNota() +">"+numero+"</a>").append(",");
				}
			}
			if(linkNota != null && linkNota.length() > 0){
				request.addMessage("Favor conferir as informa��es de tributa��o da nota. " + linkNota.substring(0, linkNota.length()-1), MessageType.WARN);
			}
		}
		return notas;
	}
	
	public List<NotaFiscalServico> getListaNotaFiscalServico(WebRequestContext request, List<Venda> listaVendas, Boolean agrupamento, Boolean somenteservico, Boolean notaseparada, Boolean exibirMensagemInfTributacao) {
		List<NotaFiscalServico> notas = new ArrayList<NotaFiscalServico>();
//		boolean agrupado = false;
		for (Venda venda : listaVendas) {
			vendamaterialService.verificaVendaKitFlexivelExibir(venda);
			
			if(notaVendaService.existNotaEmitida(venda, NotaTipo.NOTA_FISCAL_SERVICO)){
				continue;
			}
//			agrupado = false;
			
			boolean vendaNaoPossuiServicos = true;
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				boolean servico = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getServico() != null && vendamaterial.getMaterial().getServico();
				boolean tributacaomunicipal = vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getTributacaomunicipal() != null && vendamaterial.getMaterial().getTributacaomunicipal();
				if(somenteservico != null && somenteservico && vendamaterial.getMaterial() != null && !servico && !tributacaomunicipal){
					continue;
				}
				vendaNaoPossuiServicos = false;
			}
			if(vendaNaoPossuiServicos){
				continue;
			}
			
			List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(venda);
			for (Documentoorigem documentoorigem : listadocumentoorigem) {
				Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
				if (documento.getDocumentoacao().equals(Documentoacao.PREVISTA)){
					documento.setDocumentoacao(Documentoacao.DEFINITIVA);
					documentoService.updateDocumentoacao(documento);
				}
			}
			
			venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
			if(SinedUtil.isListNotEmpty(venda.getListavendapagamento()) && venda.getQtdeParcelas() == null){
				venda.setQtdeParcelas(venda.getListavendapagamento().size());
			}
			
			NotaFiscalServico nf = null;
			if(agrupamento){
				for (NotaFiscalServico notaFiscalServico : notas) {
					//Condi��o para agrupamento
					boolean cliente = (notaFiscalServico.getCliente().equals(venda.getCliente()) || clienteService.isMatrizCnpjIgual(notaFiscalServico.getCliente(), venda.getCliente()));
					
					if(cliente){
						nf = notaFiscalServico;
						nf.getListaNotaHistorico().get(0).setObservacao(nf.getListaNotaHistorico().get(0).getObservacao().replace(".", ", ")+"<a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>.");
//						agrupado = true;
						break;
					}
				}
			}
			if(nf == null){
				nf = new NotaFiscalServico();
				nf.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);

				if(somenteservico != null && somenteservico) nf.setUsarSequencial(true);
				
				NotaHistorico notaHistorico = new NotaHistorico(nf, "Visualizar venda: <a href=\"javascript:visualizaVenda("+venda.getCdvenda()+");\">"+venda.getCdvenda()+"</a>.", NotaStatus.EMITIDA, ((Usuario)Neo.getUser()).getCdpessoa(), new Timestamp(System.currentTimeMillis()));
				List<NotaHistorico> lista = new ArrayList<NotaHistorico>();
				lista.add(notaHistorico);
				nf.setListaNotaHistorico(lista);
				
				nf.setListaItens(new ArrayList<NotaFiscalServicoItem>());
				notas.add(nf);
			}
			nf.getListaVenda().add(venda);
			
			Empresa empresa = venda.getEmpresa();
			if(empresa == null) empresa = empresaService.loadPrincipal();
			
//			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(empresa, Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
//			
//			if(!agrupado && !haveVotorantim){
//				Integer proximoNumero = empresaService.carregaProxNumNF(empresa);
//				if(proximoNumero == null){
//					proximoNumero = 1;
//				}
//				nf.setNumero(proximoNumero.toString());
//				proximoNumero++;
//	
//				empresaService.updateProximoNumNF(empresa, proximoNumero);
//			}
			
			empresa = empresaService.loadWithEndereco(empresa);
			
			nf.setEmpresa(empresa);
			
			Venda venda_aux = vendaService.loadWithPedidovenda(venda);
			Naturezaoperacao naturezaoperacao = null;
			
			if (venda_aux != null && venda_aux.getPedidovendatipo() != null && venda_aux.getPedidovendatipo().getNaturezaoperacaoservico() != null &&
						NotaTipo.NOTA_FISCAL_SERVICO.equals(venda_aux.getPedidovendatipo().getNaturezaoperacaoservico().getNotaTipo())) {
				naturezaoperacao = venda_aux.getPedidovendatipo().getNaturezaoperacaoservico();
			}
				
			nf.setIndicadortipopagamento(Indicadortipopagamento.A_PRAZO);
			this.preencheValoresPadroesEmpresa(empresa, nf);
			
			if(naturezaoperacao == null && nf.getNaturezaoperacao() == null){
				naturezaoperacao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_SERVICO);
			}
			if(naturezaoperacao != null){
				nf.setNaturezaoperacao(naturezaoperacao);
			}
			
			if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
				if(venda.getListavendapagamento().size() == 1){
					Vendapagamento vendapagamento = venda.getListavendapagamento().get(0);
					nf.setDtVencimento(vendapagamento.getDataparcela());
				}else {
					java.sql.Date dtvencimento = nf.getDtVencimento();
					for(Vendapagamento vendapagamento : venda.getListavendapagamento()){
						if(vendapagamento.getDataparcela() != null){
							if(dtvencimento == null){
								dtvencimento = vendapagamento.getDataparcela();
							}else if(SinedDateUtils.afterIgnoreHour(vendapagamento.getDataparcela(), dtvencimento)){
								dtvencimento = vendapagamento.getDataparcela();
							}
						}
					}
					if(dtvencimento != null){
						nf.setDtVencimento(dtvencimento);
					}
				}
			}
			
			Cliente cliente = clienteService.carregarDadosCliente(venda.getCliente());
			Endereco enderecoCliente = venda.getEnderecoFaturamento();
			Telefone telefoneCliente = clienteService.getTelefoneForNota(cliente);

			if(Util.objects.isNotPersistent(enderecoCliente)){
				Set<Endereco> listaEndereco = cliente.getListaEndereco();
				if(listaEndereco != null && listaEndereco.size() > 0){
					for (Endereco end : listaEndereco) {
						if(end.getEnderecotipo() != null && end.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
							enderecoCliente = end;
							break;
						}
					}
				}
			}
			
			nf.setCliente(cliente);
			nf.setEnderecoCliente(enderecoCliente != null ? enderecoCliente : venda.getEndereco());
			nf.setTelefoneCliente(telefoneCliente != null ? telefoneCliente.getTelefone() : null);
			nf.setProjeto(venda.getProjeto());
			
			Money precoTotalComArredondamento = new Money();
			Money precoTotalReal = new Money();
			
			List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();			
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {				
				if(Boolean.TRUE.equals(somenteservico) && !vendamaterialService.isVendaServico(vendamaterial)){
					continue;
				}
					
				Double quantidade = vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador();
				if(multiplicador == null) multiplicador = 1.0;
				
				Material material = vendamaterial.getMaterial();
				Material materialbanda = vendamaterial.getPneu() != null ? vendamaterial.getPneu().getMaterialbanda() : null;
				NotaFiscalServicoItem item = new NotaFiscalServicoItem();
				
				item.setNotaFiscalServico(nf);
				String nomeComAlturaLarguraComprimento = vendaService.getAlturaLarguraComprimentoConcatenados(vendamaterial, material);
				item.setMaterial(material);
				item.setDescricao(getDescricaoItemNfs(material, materialbanda, nomeComAlturaLarguraComprimento, vendamaterial.getPneu(), false, false, vendamaterial.getObservacao(), venda.getIdentificadorexternoAndIdentificador()));
				item.setQtde(quantidade);
				if(vendamaterial.getPreco() != null){
					item.setPrecoUnitario(new Money(vendamaterial.getPreco()).getValue().doubleValue());
				}
				
				Money valorDesconto = new Money();
				Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
				
				if (cliente.getDiscriminardescontonota() != null && cliente.getDiscriminardescontonota()){
					item.setDesconto(valorDescontoItem);
					
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
					if(valorDesconto != null){
						nf.setDescontoincondicionado(nf.getDescontoincondicionado() != null ? nf.getDescontoincondicionado().add(valorDesconto) : valorDesconto);
					}
				} else {
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(venda.getListavendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
	
					if(quantidade != null){
						valorDescontoItem = valorDescontoItem.divide(new Money(quantidade*multiplicador));
					}
					valorDesconto = valorDesconto.add(valorDescontoItem);
					if(multiplicador != null){
						if(material.getMetrocubicovalorvenda() != null && material.getMetrocubicovalorvenda()){
							if(item.getQtde() != null)
								item.setQtde(item.getQtde() * multiplicador); 
						} else {
							if(item.getPrecoUnitario() != null)
								item.setPrecoUnitario(new Money(item.getPrecoUnitario() * multiplicador).getValue().doubleValue());
						}
					}
					
					if(item.getPrecoUnitario() != null){
						if(valorDesconto != null && valorDesconto.getValue().doubleValue() > 0 && parametrogeralService.getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO)){
							precoTotalComArredondamento = precoTotalReal.add(new Money(SinedUtil.roundByParametro(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue())));
							precoTotalReal = precoTotalReal.add(new Money(item.getPrecoUnitario()).subtract(valorDesconto));
							
							item.setPrecoUnitario(SinedUtil.roundByParametro(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue()));
						}else {
							item.setPrecoUnitario(new Money(item.getPrecoUnitario()).subtract(valorDesconto).getValue().doubleValue());
						}
					}
				}
				
				if(item.getPrecoUnitario() != null && item.getQtde() != null){
					item.setPrecoTotal(new Money(item.getPrecoUnitario() * item.getQtde()));
					if(item.getDesconto() != null){
						item.setPrecoTotal(item.getPrecoTotal().subtract(item.getDesconto()));
					}
				}
				
				if (vendamaterial.getCdvendamaterial() != null) {
					item.setVendamaterial(vendamaterial);
				}
				
				listaItens.add(item);
			}
			
			NotaFiscalServicoItem notaFiscalServicoItem = null;
			for (NotaFiscalServicoItem nfpi : listaItens) {
				if(notaFiscalServicoItem == null && nfpi.getPrecoUnitario() != null && nfpi.getQtde() != null && nfpi.getQtde() == 1){
					notaFiscalServicoItem = nfpi;
				}
			}
			
			if(notaFiscalServicoItem != null && precoTotalComArredondamento != null && precoTotalComArredondamento.getValue().doubleValue() > 0 && 
					precoTotalReal != null && precoTotalReal.getValue().doubleValue() > 0){
				double descontoRealDbl = SinedUtil.roundByParametro(precoTotalComArredondamento.getValue().doubleValue());
				double descontoTotalDbl = SinedUtil.roundByParametro(precoTotalReal.getValue().doubleValue());
				if(descontoRealDbl != descontoTotalDbl){
					double diferenca = descontoTotalDbl - descontoRealDbl;
					if(diferenca != 0d){
						notaFiscalServicoItem.setPrecoUnitario(SinedUtil.roundByParametro(notaFiscalServicoItem.getPrecoUnitario() + new Double(diferenca)));
					}
				}
			}
			
			addItensInterrompidos(venda, listaItens, nf);
			
			vendaService.verificaTemplateDiscriminacao(nf, listaItens, venda);
			
			nf.getListaItens().addAll(listaItens);			
			
//			Venda vendaaux = vendaService.loadWithPedidovenda(venda);
//			if(vendaaux != null && 
//					vendaaux != null && 
//					vendaaux.getPedidovendatipo() != null && 
//					vendaaux.getPedidovendatipo().getBonificacao() != null &&
//					vendaaux.getPedidovendatipo().getBonificacao()){
//				nf.setNaturezaoperacao(naturezaoperacaoService.loadPadrao());
//			}
			if(nf.getNaturezaoperacao() == null){
				nf.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_SERVICO));
			}
			
			if(venda.getEmpresa() != null && venda.getEmpresa().getDiscriminarservicoqtdevalor() != null &&
					venda.getEmpresa().getDiscriminarservicoqtdevalor() &&
					venda.getDocumentotipo() != null && venda.getDocumentotipo().getNome() != null && 
					!"".equals(venda.getDocumentotipo().getNome())){
				if(nf.getDadosAdicionais() != null && !"".equals(nf.getDadosAdicionais())){
					if(!nf.getDadosAdicionais().contains(("Forma de pagamento: " + venda.getDocumentotipo().getNome()))){
						nf.setDadosAdicionais("Forma de pagamento: " + venda.getDocumentotipo().getNome());
					}
				}else {
					nf.setDadosAdicionais("Forma de pagamento: " + venda.getDocumentotipo().getNome());
				}
			}
			
			if(nf.getGrupotributacao() == null || nf.getGrupotributacao().getCdgrupotributacao() == null){
				Endereco enderecoDestinoCliente = null;
				if(nf.getEnderecoCliente() != null && nf.getEnderecoCliente().getCdendereco() != null){
					enderecoDestinoCliente = enderecoService.carregaEnderecoComUfPais(nf.getEnderecoCliente());
				}
				
				List<Categoria> listaCategoriaCliente = null;
				if(cliente != null){
					listaCategoriaCliente = categoriaService.findByPessoa(cliente);
				}
				
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
						grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
																				empresa, 
																				nf.getNaturezaoperacao(), 
																				null,
																				cliente,
																				false, 
																				null, 
																				null,
																				enderecoDestinoCliente, 
																				null,
																				listaCategoriaCliente,
																				null,
																				null,
																				null,
																				null,
																				nf.getDtEmissao()));
				
				
				if(nf.getGrupotributacao() == null || nf.getGrupotributacao().getCdgrupotributacao() == null){
					if(listaGrupotributacao != null && listaGrupotributacao.size() > 1){
						exibirMensagemInfTributacao = true;
					}
					nf.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
				}
			}

			Money baseCalculo = nf.getValorTotalServicos();
			if(baseCalculo != null){
				if(nf.getDeducao() != null) baseCalculo = baseCalculo.subtract(nf.getDeducao());
				if(nf.getDescontoincondicionado() != null) baseCalculo = baseCalculo.subtract(nf.getDescontoincondicionado());
				
				nf.setBasecalculo(baseCalculo);
				nf.setBasecalculocofins(baseCalculo);
				nf.setBasecalculocsll(baseCalculo);
				nf.setBasecalculoicms(baseCalculo);
				nf.setBasecalculoinss(baseCalculo);
				nf.setBasecalculoir(baseCalculo);
				nf.setBasecalculoiss(baseCalculo);
				nf.setBasecalculopis(baseCalculo);
			}
			if(nf.getGrupotributacao() != null){
				if(baseCalculo != null){
					calculaTributacaoNota(nf, nf.getGrupotributacao(), nf.getBasecalculo());
				}
				
				if(org.apache.commons.lang.StringUtils.isNotBlank(nf.getGrupotributacao().getInfoadicionalfisco()) && 
						(nf.getInfocomplementar() == null || !nf.getInfocomplementar().contains(nf.getGrupotributacao().getInfoadicionalfisco()))){
					String infoComplementar = nf.getInfocomplementar()!=null?nf.getInfocomplementar():"";
					nf.setInfocomplementar(infoComplementar+" "+nf.getGrupotributacao().getInfoadicionalfisco());
				}
				
				if(org.apache.commons.lang.StringUtils.isNotBlank(nf.getGrupotributacao().getInfoadicionalcontrib()) &&
						(nf.getDadosAdicionais() == null || !nf.getDadosAdicionais().contains(nf.getGrupotributacao().getInfoadicionalcontrib()))){
					String infoDadosAdicionais = nf.getDadosAdicionais()!=null?nf.getDadosAdicionais():"";
					nf.setDadosAdicionais(infoDadosAdicionais + " " + nf.getGrupotributacao().getInfoadicionalcontrib());
				}
			}
		}
		
		return notas;
	}
	
	private void verificaDuplicatasVariasVenda(NotaFiscalServico nota) {
		if(nota != null && SinedUtil.isListNotEmpty(nota.getListaVenda()) && SinedUtil.isListEmpty(nota.getListaDuplicata())){
			List<Documentotipo> listaDocumentotipo = new ArrayList<Documentotipo>();
			List<Prazopagamento> listaPrazopagamento = new ArrayList<Prazopagamento>();
			List<Vendapagamento> listaVendapgamento = new ArrayList<Vendapagamento>();
			HashMap<Integer, Date> mapParcelaData = new HashMap<Integer, Date>();
			HashMap<Integer, Money> mapParcelaValor = new HashMap<Integer, Money>();
			HashMap<Integer, Documentotipo> mapParcelaDocumentotipo = new HashMap<Integer, Documentotipo>();
			boolean verificarVencimento = false;
			boolean vencimentoIgual = true;
			Integer qtdeParcelas = null;
			Date dataparcela;
			
			Money totalPagamentoVenda = new Money();
			for(Venda venda : nota.getListaVenda()){
				if(venda.getDocumentotipo() != null && !listaDocumentotipo.contains(venda.getDocumentotipo())){
					listaDocumentotipo.add(venda.getDocumentotipo());
				}
				
				listaVendapgamento = SinedUtil.isListNotEmpty(venda.getListavendapagamento()) ? venda.getListavendapagamento() : vendapagamentoService.findVendaPagamentoByVenda(venda); 
				if(SinedUtil.isListNotEmpty(listaVendapgamento)){
					Integer i = 0; 
					for(Vendapagamento vendapagamento : listaVendapgamento){
						totalPagamentoVenda = totalPagamentoVenda.add(MoneyUtils.zeroWhenNull(vendapagamento.getValororiginal()));
						
						if(vendapagamento.getDocumentotipo() != null && !listaDocumentotipo.contains(vendapagamento.getDocumentotipo())){
							listaDocumentotipo.add(vendapagamento.getDocumentotipo());
						}
						
						mapParcelaDocumentotipo.put(i, vendapagamento.getDocumentotipo());
						mapParcelaValor.put(i, vendapagamento.getValororiginal());
						
						if(vencimentoIgual){
							if(!verificarVencimento){
								mapParcelaData.put(i, vendapagamento.getDataparcela());
							}else if(mapParcelaData.size() != listaVendapgamento.size()){
								vencimentoIgual = false;
							}else {
								dataparcela = mapParcelaData.get(i);
								if(dataparcela == null || vendapagamento.getDataparcela() == null || !SinedDateUtils.equalsIgnoreHour(vendapagamento.getDataparcela(), dataparcela)){
									vencimentoIgual = false;
								}
							}
						}
						i++;
					}
					verificarVencimento = true;
				}
				if(venda.getPrazopagamento() != null && !listaPrazopagamento.contains(venda.getPrazopagamento())){
					listaPrazopagamento.add(venda.getPrazopagamento());
				}
				if(qtdeParcelas == null && venda.getQtdeParcelas() != null && venda.getQtdeParcelas() > 0){
					qtdeParcelas = venda.getQtdeParcelas();
				}
			}
			
			if(listaDocumentotipo.size() == 1){
				nota.setDocumentotipo(listaDocumentotipo.get(0));
			}
			if(listaPrazopagamento.size() == 1){
				nota.setPrazopagamentofatura(listaPrazopagamento.get(0));
				if (nota.getPrazopagamentofatura().getAvista() != null && nota.getPrazopagamentofatura().getAvista()){
					nota.setIndicadortipopagamento(Indicadortipopagamento.A_VISTA);
				}else {
					nota.setIndicadortipopagamento(Indicadortipopagamento.A_PRAZO);
				}
				List<Notafiscalservicoduplicata> listaDuplicata = gerarParcelasCobranca(nota, true, qtdeParcelas != null ? qtdeParcelas : 1);
				Money valorTotalDuplicata = getValorTotalDuplicata(listaDuplicata);
				boolean documentoTipoUnico = listaDocumentotipo.size() == 1;
				boolean vendaUnica = nota.getListaVenda().size() == 1;
				if(SinedUtil.isListNotEmpty(listaDuplicata) && (documentoTipoUnico || vendaUnica)){
					if(vencimentoIgual && (documentoTipoUnico || vendaUnica) && listaDuplicata.size() == mapParcelaData.size()){
						for(Integer posicao : mapParcelaData.keySet()){
							try {
								listaDuplicata.get(posicao).setDtvencimento(new java.sql.Date(mapParcelaData.get(posicao).getTime()));
							} catch (Exception e) {}
							
							try {
								if(listaDocumentotipo.size() > 1 && nota.getListaVenda().size() == 1){
									listaDuplicata.get(posicao).setDocumentotipo(mapParcelaDocumentotipo.get(posicao));
								}
							} catch (Exception e) {}
							
							if(vendaUnica && MoneyUtils.isMaiorQueZero(valorTotalDuplicata) && MoneyUtils.isMaiorQueZero(totalPagamentoVenda)){
								try {
									listaDuplicata.get(posicao).setValor(mapParcelaValor.get(posicao).divide(totalPagamentoVenda).multiply(valorTotalDuplicata));
								} catch (Exception e) {}
							}
						}
					}
					nota.setListaDuplicata(listaDuplicata);
				}
				
			}
		}
	}
	
	private Money getValorTotalDuplicata(List<Notafiscalservicoduplicata> listaDuplicata) {
		Money valorTotal = new Money();
		if(SinedUtil.isListNotEmpty(listaDuplicata)){
			for(Notafiscalservicoduplicata notafiscalservicoduplicata : listaDuplicata){
				valorTotal = valorTotal.add(MoneyUtils.zeroWhenNull(notafiscalservicoduplicata.getValor()));
			}
		}
		return valorTotal;
	}
	
	public List<Notafiscalservicoduplicata> getDuplicatasAgrupadas(List<NotaFiscalServico> listaNota) {
		List<Notafiscalservicoduplicata> listaDuplicata = null;
		if(SinedUtil.isListNotEmpty(listaNota)){
			int i = 0;
			Notafiscalservicoduplicata duplicataNova;
			for(NotaFiscalServico nota : listaNota){
				if(listaDuplicata == null){
					listaDuplicata = new ArrayList<Notafiscalservicoduplicata>();
					if(SinedUtil.isListNotEmpty(nota.getListaDuplicata())){
						for(Notafiscalservicoduplicata duplicata : nota.getListaDuplicata()){
							listaDuplicata.add(new Notafiscalservicoduplicata(duplicata));
						}
					}
				}else {
					if(SinedUtil.isListNotEmpty(nota.getListaDuplicata()) && nota.getListaDuplicata().size() == listaDuplicata.size()){
						i = 0;
						for(Notafiscalservicoduplicata duplicata : nota.getListaDuplicata()){
							duplicataNova = listaDuplicata.get(i);
							if(duplicata.getValor() == null) duplicata.setValor(new Money());
							if(duplicataNova.getValor() == null) duplicataNova.setValor(new Money());
							duplicataNova.setValor(duplicataNova.getValor().add(duplicata.getValor()));
							i++;
						}
					}
				}
			}
		}
		return listaDuplicata;
	}
	
	public List<Notafiscalservicoduplicata> gerarParcelasCobranca(NotaFiscalServico nota, boolean calculoVenda, Integer qtdeParcelas){	
		ParcelasCobrancaBean bean = new ParcelasCobrancaBean();
		bean.setPrazopagamento(nota.getPrazopagamentofatura());
		bean.setData(nota.getDtEmissao());
		bean.setValor(nota.getValorliquidofaturaTransient() != null && nota.getValorliquidofaturaTransient().getValue().doubleValue() > 0 ? nota.getValorliquidofaturaTransient() : nota.getValorliquidofatura());
		bean.setEmpresa(nota.getEmpresa());
		
		Venda venda = new Venda();
		if(calculoVenda){
			venda.setPrazopagamento(bean.getPrazopagamento());
			venda.setValor(bean.getValor() != null ? bean.getValor().getValue().doubleValue() : 0d);
			venda.setQtdeParcelas(qtdeParcelas);
			venda.setDtvenda(bean.getData());
			venda.setEmpresa(bean.getEmpresa());
		}
		
		List<Notafiscalservicoduplicata> listaDuplicata = null;
		if(bean.getPrazopagamento() != null && bean.getData() != null && bean.getValor() != null && bean.getValor().getValue().doubleValue() > 0 && bean.getEmpresa() != null){
			AjaxRealizarVendaPagamentoBean ajaxRealizarVendaPagamentoBean = null;
			if(calculoVenda){
				ajaxRealizarVendaPagamentoBean = vendaService.pagamento(venda, null);
			}else {
				ajaxRealizarVendaPagamentoBean = notafiscalprodutoService.gerarParcelasCobranca(bean);
			}
			if(ajaxRealizarVendaPagamentoBean != null && SinedUtil.isListNotEmpty(ajaxRealizarVendaPagamentoBean.getLista())){
				listaDuplicata = new ArrayList<Notafiscalservicoduplicata>();
				Notafiscalservicoduplicata duplicata;
				int i = 1;
				for(AjaxRealizarVendaPagamentoItemBean itemBean : ajaxRealizarVendaPagamentoBean.getLista()){
					duplicata = new Notafiscalservicoduplicata();
					duplicata.setDocumentotipo(nota.getDocumentotipo());
					duplicata.setNumero(i + "");
					duplicata.setDtvencimento(itemBean.getDtparceladuplicata());
					duplicata.setValor(itemBean.getValorduplicata());
					i++;
					
					listaDuplicata.add(duplicata);
				}
			}
		}
		
		return listaDuplicata;
	}
	
	/**
	 * 
	 * @param idnota
	 * @return
	 */
	public boolean validaNotaStatus(String idnota){
		List<NotaFiscalServico> lista = notaFiscalServicoDAO.validaNotaStatus(idnota);
		for (NotaFiscalServico notaFiscalServico : lista) {
			if(!notaFiscalServico.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) && !notaFiscalServico.getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA))
				return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param idnota
	 * @param notaFiscalServico
	 */
	public void atualizaDados(String idnota, NotaFiscalServico notaFiscalServico) {
		notaFiscalServicoDAO.atualizaDados(idnota,notaFiscalServico.getCstcofins(),notaFiscalServico.getCstpis());
	}
	
	/**
	 * Faz o processamento do arquivo de importa��o das notas fiscais de servi�o.
	 * 
	 *
	 * @param request
	 * @param stringArquivo
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<ImportadorBean> processarArquivo(WebRequestContext request, String stringArquivo, Empresa empresa) {
		String[] linha = stringArquivo.split("\\r?\\n");
		String regA, regB, regC;
		
		List<ArquivoImportacao> lista = new ArrayList<ArquivoImportacao>();
		
		for (int i = 0; i < linha.length; i=i+3) {
			try{
				regA = linha[i];
				regB = linha[i+1];
				regC = linha[i+2];
				
				lista.add(new ArquivoImportacao(regA, regB, regC, request));
			} catch (SinedException e) {
				e.printStackTrace();
				List<ImportadorBean> list = new ArrayList<ImportadorBean>();
				list.add(new ImportadorBean(e.getMessage(), true, null));
				return list;
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
		
		List<ImportadorBean> listErro = new ArrayList<ImportadorBean>();
		
		if (lista.size() > 0) {
			try{
				empresa = empresaService.loadForEntrada(empresa);
				
				Cliente cliente;
				NotaFiscalServico nf;
				
				for (ArquivoImportacao ai : lista) {
					if(notaFiscalServicoDAO.isExisteNota(empresa, ai.getNumero().toString())){
						listErro.add(new ImportadorBean("Nota com n�mero " + ai.getNumero() + " n�o importada, registro j� existe no sistema.", true, ai.getNumero()));
					} else {
						cliente = clienteService.findForImportacao(ai);
						nf = this.createForImportacao(ai, cliente, empresa);			
						
						String fromTDrive = request.getParameter("origem");
						NotaHistorico notaHistorico;
						
						if(fromTDrive != null && fromTDrive.equals("tdriveweb")) {
							notaHistorico =  new NotaHistorico(nf, "Nota importada via TDriveweb", NotaStatus.EMITIDA);
							notaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						} else {
							notaHistorico =  new NotaHistorico(nf, "Nota importada via arquivo TXT.", NotaStatus.EMITIDA, SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
						}
						
						List<NotaHistorico> listaNotaHistorico = new ArrayList<NotaHistorico>();
						listaNotaHistorico.add(notaHistorico);
						nf.setListaNotaHistorico(listaNotaHistorico);
						this.saveOrUpdate(nf);
						listErro.add(new ImportadorBean("Nota com n�mero " + ai.getNumero() + " importada com sucesso.", false, ai.getNumero()));
					}
				}
			} catch (SinedException e) {
				e.printStackTrace();
				listErro.add(new ImportadorBean(e.getMessage(), true, null));
				return listErro;
			}
			
		} else return null;
		
		return listErro;
	}

	
	/**
	 * Cria uma nota fiscal de servi�o a partir das informa��es do arquivo de importa��o.
	 *
	 * @param ai
	 * @param cliente
	 * @param empresa
	 * @return notafiscalServico
	 * @author Rodrigo Freitas
	 */
	private NotaFiscalServico createForImportacao(ArquivoImportacao ai, Cliente cliente, Empresa empresa) {
		NotaFiscalServico notafiscalServico = new NotaFiscalServico();
		
		this.preencheValoresPadroesEmpresa(empresa, notafiscalServico);
		
		notafiscalServico.setNumero(ai.getNumero().toString());
		notafiscalServico.setDtEmissao(ai.getData_de_emissao());
		notafiscalServico.setEmpresa(empresa);
		notafiscalServico.setCliente(cliente);
		notafiscalServico.setEnderecoCliente(cliente.getEnderecoAux());
		notafiscalServico.setTelefoneCliente(ai.getTelefone_cliente());
		
		notafiscalServico.setNaturezaoperacao(ai.getNatureza_operacao() != null ? new Naturezaoperacao(ai.getNatureza_operacao()) : null);
		notafiscalServico.setRegimetributacao(ai.getRegime_especial() != null ? new Regimetributacao(ai.getRegime_especial()) : null);
		
		if (ai.getCodigo_cnae() != null && !ai.getCodigo_cnae().equals("")){
			List <Codigocnae> listCodigocnae = codigocnaeService.findByCodigocnae(ai.getCodigo_cnae());
			
			if (listCodigocnae != null && listCodigocnae.size() > 0) notafiscalServico.setCodigocnaeBean(listCodigocnae.get(0));
			else notafiscalServico.setCodigocnaeBean(null);
		} else notafiscalServico.setCodigocnaeBean(null);
				
		if (ai.getCodigo_tributacao() != null && !ai.getCodigo_tributacao().equals("")){
			notafiscalServico.setCodigotributacao(codigotributacaoService.findByCodigo(ai.getCodigo_tributacao()));
		} else notafiscalServico.setCodigotributacao(null);
		
		if(ai.getItem_lista_de_servico() != null && !ai.getItem_lista_de_servico().equals("")){
			String codigoItemListaDeServico = ai.getItem_lista_de_servico();
			
			try{
				Integer intAux = Integer.parseInt(codigoItemListaDeServico);
				codigoItemListaDeServico = intAux.toString();
			} catch (Exception e) {}
		
			Itemlistaservico itemlistaservico = itemlistaservicoService.loadByCodigo(codigoItemListaDeServico);
			notafiscalServico.setItemlistaservicoBean(itemlistaservico);
		} else notafiscalServico.setItemlistaservicoBean(null);
		
		NotaFiscalServicoItem notaFiscalServicoItem = new NotaFiscalServicoItem();
		
		notaFiscalServicoItem.setPrecoUnitario(new Money(ai.getValor_servico()).getValue().doubleValue());
		notaFiscalServicoItem.setQtde(1.0);
		notaFiscalServicoItem.setDescricao(ai.getDiscriminacao_servico());
		
		List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
		listaItens.add(notaFiscalServicoItem);
		notafiscalServico.setListaItens(listaItens);
		
		notafiscalServico.setIncideiss(ai.getIncide_iss());
		notafiscalServico.setIncideir(ai.getIncide_ir());
		notafiscalServico.setIncideinss(ai.getIncide_inss());
		notafiscalServico.setIncidepis(ai.getIncide_pis());
		notafiscalServico.setIncidecofins(ai.getIncide_cofins());
		notafiscalServico.setIncidecsll(ai.getIncide_csll());
		
		notafiscalServico.setIss(ai.getAliquota_iss());
		notafiscalServico.setIr(ai.getAliquota_ir());
		notafiscalServico.setInss(ai.getAliquota_inss());
		notafiscalServico.setPis(ai.getAliquota_pis());
		notafiscalServico.setCofins(ai.getAliquota_cofins());
		notafiscalServico.setCsll(ai.getAliquota_csll());
		
		notafiscalServico.setDescontocondicionado(ai.getDescontocondicionado() != null ? new Money(ai.getDescontocondicionado()) : new Money());
		notafiscalServico.setDescontoincondicionado(ai.getDescontoincondicionado() != null ? new Money(ai.getDescontoincondicionado()) : new Money());
		notafiscalServico.setDeducao(ai.getDeducao() != null ? new Money(ai.getDeducao()) : new Money());
		notafiscalServico.setOutrasretencoes(ai.getOutrasretencoes() != null ? new Money(ai.getOutrasretencoes()) : new Money());
		
		Money baseCalc = new Money();
		baseCalc = baseCalc.add(notafiscalServico.getValorTotalServicos());
		baseCalc = baseCalc.subtract(notafiscalServico.getDescontoincondicionado());
		baseCalc = baseCalc.subtract(notafiscalServico.getDeducao());
		
		notafiscalServico.setBasecalculo(baseCalc);
		notafiscalServico.setBasecalculoiss(baseCalc);
		notafiscalServico.setBasecalculoir(baseCalc);
		notafiscalServico.setBasecalculoinss(baseCalc);
		notafiscalServico.setBasecalculopis(baseCalc);
		notafiscalServico.setBasecalculocofins(baseCalc);
		notafiscalServico.setBasecalculocsll(baseCalc);
		
		return notafiscalServico;
	}
	
	public NotaFiscalServico geraNotaRomaneios(NotaFiscalServico nf, String romaneios) {		
		nf.setUsarSequencial(Boolean.TRUE);
		nf.setNotaStatus(NotaStatus.EMITIDA);
		nf.setRomaneios(romaneios);
		
		if (romaneios != null && !romaneios.equals("")) {
			List<NotaFiscalServicoItem> nfsiList = new ArrayList<NotaFiscalServicoItem>();
			List<Romaneio> romaneioList = romaneioService.findForGerarNota(romaneios);
			
			for (Romaneio romaneio : romaneioList) {
				for (Romaneioitem romaneioItem : romaneio.getListaRomaneioitem()) {
					if (romaneioItem.getMaterial() != null && ((romaneioItem.getMaterial().getServico() != null && romaneioItem.getMaterial().getServico()) || 
							(romaneioItem.getMaterial().getProduto() != null && romaneioItem.getMaterial().getProduto() && 
							romaneioItem.getMaterial().getTributacaomunicipal() != null && romaneioItem.getMaterial().getTributacaomunicipal()))) {
						Integer posicaoNfsi = 0;
						
						if (nfsiList.size() > 0) {
							for (int i = 0; i < nfsiList.size(); i++) {
								if (nfsiList.get(i).getMaterial() != null && nfsiList.get(i).getMaterial().getCdmaterial().equals(romaneioItem.getMaterial().getCdmaterial())) {
									posicaoNfsi = i;
									break;
								}
							}
							
							if (posicaoNfsi != 0) {
								nfsiList.get(posicaoNfsi).setQtde(nfsiList.get(posicaoNfsi).getQtde() + romaneioItem.getQtde());
							} else {
								nfsiList.add(new NotaFiscalServicoItem(romaneioItem.getMaterial(), romaneioItem.getQtde()));
							}
							
							posicaoNfsi = 0;
						} else {
							nfsiList.add(new NotaFiscalServicoItem(romaneioItem.getMaterial(), romaneioItem.getQtde()));
						}
					}
				}
			}
			
			nf.setListaItens(nfsiList);
		}
		
		return nf;
	}
	
	
	/**
	* M�todo que verifica se a data atual � maior que a data de emiss�o da nota
	*
	* @param whereIn
	* @return
	* @since 13/10/2014
	* @author Luiz Fernando
	*/
	public boolean existeDataEmissaoAnteriorDataAtual(String whereIn){
		boolean exibirAlerta = false;
		try {
			if(org.apache.commons.lang.StringUtils.isNotEmpty(whereIn)){
				List<NotaFiscalServico> listaNota = this.findForAtualizacaoData(whereIn);
				if(SinedUtil.isListNotEmpty(listaNota)){
					for(NotaFiscalServico nfs : listaNota){
						if(nfs.getDtEmissao() != null && SinedDateUtils.afterIgnoreHour(new java.sql.Date(System.currentTimeMillis()), new java.sql.Date(nfs.getDtEmissao().getTime()))){
							exibirAlerta = true;
							break;
						}
					}
				}		
			}
		} catch (Exception e) {}
		return exibirAlerta;
	}
	
	public List<NotaFiscalServico> findByCdNota(String whereIn) {
		return notaFiscalServicoDAO.findByCdNota(whereIn);
	}
	public List<NotaFiscalServico> existeEndere�oCadastrado(String whereIn){	
		try {
			if(org.apache.commons.lang.StringUtils.isNotEmpty(whereIn)){
				List<NotaFiscalServico> listaNota = this.findByCdNota(whereIn);
				return listaNota; 
			}
		}	catch (Exception e) {}
		return null;
	}
	
	/**
	* M�todo para recalcular os impostos das notas
	*
	* @param whereInNota
	* @since 18/11/2014
	* @author Luiz Fernando
	*/
	public void recalcularImpostos(String whereInNota) {
		if(org.apache.commons.lang.StringUtils.isNotEmpty(whereInNota) && "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECALCULAR_IMPOSTO_CRIARNFSE))){
			List<NotaFiscalServico> listaNfs = buscarNotaRecalcularImposto(whereInNota);
			if(SinedUtil.isListNotEmpty(listaNfs)){
				int mesAtual = SinedDateUtils.getDateProperty(SinedDateUtils.currentDate(), Calendar.MONTH);
				for(NotaFiscalServico nfs : listaNfs){
						Date dtemissao = nfs.getDtEmissao();
						boolean atualizaEmissao = false;
						if(dtemissao == null){
							atualizaEmissao = true;
						} else {
							int mesEmissao = SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH);
							if(mesEmissao != mesAtual){
								atualizaEmissao = true;
							}
						}
						if(atualizaEmissao){
							updateDataemissaoAtual(nfs);
						}
					notaDocumentoService.atualizaValoresBasecalculoNotaFiscalServico(nfs);
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaFiscalServicoDAO#buscarNotaRecalcularImposto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 18/11/2014
	* @author Luiz Fernando
	*/
	public List<NotaFiscalServico> buscarNotaRecalcularImposto(String whereIn){
		return notaFiscalServicoDAO.buscarNotaRecalcularImposto(whereIn);
	}
	
	public boolean podeEditarNotaNumeroPosterior(NotaFiscalServico notaFiscalServico) {
		String param_recalcularimpostonfse = parametrogeralService.getValorPorNome(Parametrogeral.RECALCULAR_IMPOSTO_CRIARNFSE);
		if(!"TRUE".equalsIgnoreCase(param_recalcularimpostonfse))
			return true;		
		
		if(notaFiscalServico == null || notaFiscalServico.getCdNota() == null || org.apache.commons.lang.StringUtils.isEmpty(notaFiscalServico.getNumero()) ||
				notaFiscalServico.getCliente() == null || notaFiscalServico.getCliente().getCdpessoa() == null)
			return true;
		
		try {
			Cliente cliente = clienteService.load(notaFiscalServico.getCliente(), "cliente.cdpessoa, cliente.cnpj, cliente.contabilidadecentralizada");
			List<NotaFiscalServico> listaNfs = buscarNotasValidacaoEdicaoNumeroPosterior(cliente, notaFiscalServico.getNumero());
			if(SinedUtil.isListNotEmpty(listaNfs)){
				for(NotaFiscalServico nfs : listaNfs){
					NotaFiscalServico nota = loadForEntrada(new NotaFiscalServico(nfs.getCdNota()));
					if(nota != null){
						if(nota.getGrupotributacao() != null){
							nota.setGrupotributacao(grupotributacaoService.loadForEntrada(nota.getGrupotributacao()));
						}
						Grupotributacao grupotributacao = nota.getGrupotributacao();
						if(grupotributacao != null){
							calculaTributacaoNota(nota, grupotributacao, nota.getValorTotalServicos());
						}
						if(nota.getExisteImpostoCumulativo() != null && nota.getExisteImpostoCumulativo()){
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public List<NotaFiscalServico> buscarNotasValidacaoEdicaoNumeroPosterior(Cliente cliente, String numero) {
		return notaFiscalServicoDAO.buscarNotasValidacaoEdicaoNumeroPosterior(cliente, numero);
	}
	
	/**
	 * Carrega as informa��es necess�rias para Emiss�o de Nota Fiscal de Servi�o, baseada no Faturamento de Requisi��o(OS)
	 * 
	 * @author Rafael Salvio
	 */
	public NotaFiscalServico preencheNotaForFaturarServicosRequisiscao(NotaFiscalServico bean, String whereIn) {
		bean.setWhereInRequisicao(whereIn);
		bean.setCadastrarcobranca(Boolean.FALSE);
		List<Requisicao> listRequisicao = requisicaoService.loadForEmitirNotaFiscalProduto(whereIn, "servico");
		List<NotaFiscalServicoItem> listProdutos = new ArrayList<NotaFiscalServicoItem>();
		if(listRequisicao != null){
			for (Requisicao requisicao : listRequisicao) {
				bean.setRequisicao(requisicao);
				if (requisicao.getEmpresa() != null){
					bean.setEmpresa(requisicao.getEmpresa());
				}else if(bean.getEmpresa() == null){
					bean.setEmpresa(empresaService.loadPrincipal());
				}
				
				if(bean.getMunicipioissqn() == null && bean.getEmpresa() != null && bean.getEmpresa().getEndereco() == null){
					Empresa empresa = empresaService.loadForEntrada(bean.getEmpresa());
					if(empresa != null){
						Endereco endereco = empresa.getEndereco();
						if(endereco != null && endereco.getMunicipio() != null) bean.setMunicipioissqn(endereco.getMunicipio());
					}
				}
				
				if (requisicao.getCliente() != null){
					bean.setCliente(requisicao.getCliente());
					bean.setInscricaomunicipal(requisicao.getCliente().getInscricaomunicipal());
					if(requisicao.getEndereco() != null){
						bean.setEnderecoCliente(requisicao.getEndereco());
					}
				}
				if(requisicao.getListaMateriaisrequisicao() != null){
					for (Materialrequisicao requisicaoMaterial : requisicao.getListaMateriaisrequisicao()) {
						if(requisicaoMaterial.getMaterial() != null){
							NotaFiscalServicoItem item = new NotaFiscalServicoItem();
							item.setMaterial(requisicaoMaterial.getMaterial());
							item.setDescricao(requisicaoMaterial.getMaterial().getNome());
							item.setQtde(requisicaoMaterial.getQuantidade());
							Double valorvenda = requisicaoMaterial.getValorunitario();
							if(valorvenda == null) {
								valorvenda = requisicaoMaterial.getMaterial().getValorvenda();
							}
							item.setPrecoUnitario(new Money(valorvenda != null ? valorvenda: 0d).getValue().doubleValue());
							item.setPrecoTotal(new Money((requisicaoMaterial.getQuantidade() != null ? requisicaoMaterial.getQuantidade() : 0d) * (valorvenda != null ? valorvenda: 0d)));
							if(requisicaoMaterial.getObservacao() != null){
								item.setDescricao(item.getDescricao() + "\n" + requisicaoMaterial.getObservacao());
							}
							
							item.setCdmaterialrequisicao(requisicaoMaterial.getCdmaterialrequisicao());
							item.setItem(requisicaoMaterial.getItem());
							item.setNomematerial(requisicaoMaterial.getMaterial().getNome());
							item.setNomematerialnf(requisicaoMaterial.getMaterial().getNomenf());
							
							listProdutos.add(item);
							
							if(bean.getBasecalculo() == null) bean.setBasecalculo(new Money());
							bean.setBasecalculo(item.getPrecoTotal().add(bean.getBasecalculo()));
							if(requisicaoMaterial.getMaterial() != null && requisicaoMaterial.getMaterial().getGrupotributacao() != null){
								if(bean.getEnderecoCliente() != null){
									Endereco enderecoCliente = enderecoService.loadEndereco(bean.getEnderecoCliente());
									bean.setEnderecoCliente(enderecoCliente);
								}
								
								Grupotributacao grupotributacao = grupotributacaoService.loadForEntrada(requisicaoMaterial.getMaterial().getGrupotributacao());
								bean.setGrupotributacao(grupotributacao);
								if(bean.getGrupotributacao() != null){
									if(bean.getGrupotributacao().getCodigocnae() != null){
										bean.setCodigocnaeBean(bean.getGrupotributacao().getCodigocnae());
									}
									if(bean.getGrupotributacao().getCodigotributacao() != null){
										bean.setCodigotributacao(bean.getGrupotributacao().getCodigotributacao());
									}
									if(bean.getGrupotributacao().getItemlistaservico() != null){
										bean.setItemlistaservicoBean(bean.getGrupotributacao().getItemlistaservico());
									}
									if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getGrupotributacao().getInfoadicionalcontrib())){
										if(org.apache.commons.lang.StringUtils.isBlank(bean.getDadosAdicionais()) || !bean.getDadosAdicionais().contains(bean.getGrupotributacao().getInfoadicionalcontrib())){
											if(org.apache.commons.lang.StringUtils.isBlank(bean.getDadosAdicionais())){
												bean.setDadosAdicionais(bean.getGrupotributacao().getInfoadicionalcontrib()); 
											}else if(!bean.getDadosAdicionais().contains(bean.getGrupotributacao().getInfoadicionalcontrib())){
												bean.setDadosAdicionais(bean.getDadosAdicionais() + ". " + bean.getGrupotributacao().getInfoadicionalcontrib());
											}
										}
									}
									if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getGrupotributacao().getInfoadicionalfisco())){
										if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfocomplementar()) || !bean.getDadosAdicionais().contains(bean.getGrupotributacao().getInfoadicionalfisco())){
											if(org.apache.commons.lang.StringUtils.isBlank(bean.getInfocomplementar())){
												bean.setInfocomplementar(bean.getGrupotributacao().getInfoadicionalfisco()); 
											}else if(!bean.getInfocomplementar().contains(bean.getGrupotributacao().getInfoadicionalfisco())){
												bean.setInfocomplementar(bean.getInfocomplementar() + ". " + bean.getGrupotributacao().getInfoadicionalfisco());
											}
										}
									}
								}
								if(bean.getEmpresa() != null){
									calculaTributacaoNota(bean, grupotributacao, bean.getBasecalculo());
								}
							}
						}
					}
				}
			}
		}
		bean.setListaItens(listProdutos);
		
		if(bean.getCliente() != null){
			Cliente cliente = clienteService.load(bean.getCliente(), "cliente.cdpessoa, cliente.incidiriss");
			if(cliente != null && cliente.getIncidiriss() != null && cliente.getIncidiriss()){
				bean.setIncideiss(Boolean.TRUE);
			}
		}
		
		requisicaoService.verificaTemplateDiscriminacao(bean);
		
		if(bean.getBasecalculoir() == null) bean.setBasecalculoir(bean.getBasecalculo());
		if(bean.getBasecalculoinss() == null) bean.setBasecalculoinss(bean.getBasecalculo());
		if(bean.getBasecalculoiss() == null) bean.setBasecalculoiss(bean.getBasecalculo());
		if(bean.getBasecalculocsll() == null) bean.setBasecalculocsll(bean.getBasecalculo());
		if(bean.getBasecalculocofins() == null) bean.setBasecalculocofins(bean.getBasecalculo());
		if(bean.getBasecalculopis() == null) bean.setBasecalculopis(bean.getBasecalculo());
		if(bean.getBasecalculoicms() == null) bean.setBasecalculoicms(bean.getBasecalculo());
		
		return bean;
	}
	
	/**
	* M�todo que adiciona os itens interrompidos da coleta na nota fiscal
	*
	* @param venda
	* @param listaItens
	* @param nf
	* @since 08/01/2016
	* @author Luiz Fernando
	*/
	public void addItensInterrompidos(Venda venda,	List<NotaFiscalServicoItem> listaItens, NotaFiscalServico nf) {
		if(venda != null && venda.getPedidovenda() != null){
			if(listaItens == null) listaItens = new ArrayList<NotaFiscalServicoItem>();
			List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findMaterialproducaoInterrompido(venda.getPedidovenda());			

			if(SinedUtil.isListNotEmpty(listaPedidovendamaterial)){								
				String WhereIn = CollectionsUtil.listAndConcatenate(listaPedidovendamaterial, "cdpedidovendamaterial", ",");
				List<ColetaMaterial> listaColetaMaterial =  coletaMaterialService.findByIensInterropidos(WhereIn);
				List<Pneu> listaPneusJaAssociado = new ArrayList<Pneu>();
				
				for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
					Double qtdTotalDevolvida = 0.0;
					if(SinedUtil.isListNotEmpty(listaColetaMaterial) && SinedUtil.isRecapagem()){
						for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
							for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
								if(itemServico.getPedidovendamaterial() != null
										&& itemServico.getPedidovendamaterial().equals(pedidovendamaterial)												
										&& coletaMaterial.getQuantidadedevolvida() != null){
									
									qtdTotalDevolvida += coletaMaterial.getQuantidadedevolvida();
									break;
								}
							}

//							if(coletaMaterial.getPedidovendamaterial() != null
//									&& coletaMaterial.getPedidovendamaterial().equals(pedidovendamaterial)												
//									&& coletaMaterial.getQuantidadedevolvida() != null){
//								
//								qtdTotalDevolvida += coletaMaterial.getQuantidadedevolvida();
//							}
						}
					}
					
					if (qtdTotalDevolvida > 0 && notaFiscalServicoItemService.existePedidoVendaMaterial(pedidovendamaterial)) {
						listaPneusJaAssociado.add(pedidovendamaterial.getPneu());
						
						continue;
					}
					
					Material material = pedidovendamaterial.getMaterial();
					Material materialbanda = pedidovendamaterial.getPneu() != null ? pedidovendamaterial.getPneu().getMaterialbanda() : null;
					NotaFiscalServicoItem item = new NotaFiscalServicoItem();	
												
					boolean itemInterrompidoParcial = false;
					//Valida��o para encontrar a Devolu��o parcial ou total
					if(pedidovendamaterial.getQuantidade().equals(qtdTotalDevolvida)){
						item.setQtde(pedidovendamaterial.getQuantidade());
					}else if(pedidovendamaterial.getQuantidade() > qtdTotalDevolvida){
						item.setQtde(qtdTotalDevolvida);
						itemInterrompidoParcial = true;
					}
					
					Double multiplicador = pedidovendamaterial.getMultiplicador();
					if(multiplicador == null) multiplicador = 1.0;
					
					item.setPedidovendamaterial(pedidovendamaterial);
					item.setNotaFiscalServico(nf);
					item.setDescricao(getDescricaoItemNfs(material, materialbanda, null, pedidovendamaterial.getPneu(), true, itemInterrompidoParcial, pedidovendamaterial.getObservacao(), venda.getIdentificadorexternoAndIdentificador()));
					item.setPrecoUnitario(0D);
					item.setPrecoTotal(new Money());
					listaItens.add(item);
				}
				
				nf.setPneusJaAssociado(SinedUtil.listAndConcatenateIDs(listaPneusJaAssociado));
			}
		}
	}
	
	/**
	* M�todo que retorna a descri��o do item da venda para a nota
	*
	* @param material
	* @param materialbanda
	* @param dimensoes
	* @param pneu
	* @param itemInterrompido
	* @return
	* @since 08/01/2016
	* @author Luiz Fernando
	*/
	public String getDescricaoItemNfs(Material material, Material materialbanda, String dimensoes, Pneu pneu, boolean itemInterrompido, boolean itemInterrompidoParcial, String observacao, String identificadorvenda){
		StringBuilder descricao = new StringBuilder();
		descricao.append(material.getNomenfOuNome());
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(dimensoes)){
			descricao.append(" " + dimensoes);
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_VENDA_NOTA) && org.apache.commons.lang.StringUtils.isNotBlank(identificadorvenda)){
			descricao.append(" " + identificadorvenda);
		}
		
		if(materialbanda != null){
			descricao.append(" " + materialbanda.getNome());
		}
		if(pneu != null && 
				pneu.getDot() != null && 
				!pneu.getDot().trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append("DOT: " + pneu.getDot());
		}
		if(pneu != null && 
				pneu.getSerie() != null && 
				!pneu.getSerie().trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append("S�rie/Fogo: " + pneu.getSerie());
		}
		if(pneu != null && 
				pneu.getPneumarca() != null && 
				pneu.getPneumarca().getNome() != null &&
				!pneu.getPneumarca().getNome().trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append("Marca: " + pneu.getPneumarca().getNome());
		}
		if(pneu != null && 
				pneu.getPneumodelo() != null && 
				pneu.getPneumodelo().getNome() != null &&
				!pneu.getPneumodelo().getNome().trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append("Modelo: " + pneu.getPneumodelo().getNome());
		}
		if(pneu != null && 
				pneu.getPneumedida() != null && 
				pneu.getPneumedida().getNome() != null &&
				!pneu.getPneumedida().getNome().trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append("Medida: " + pneu.getPneumedida().getNome());
		}
		if(observacao != null && 
				!observacao.trim().equals("")){
			if(descricao.length() > 0) descricao.append(" ");
			descricao.append(observacao);
		}
		
		if(itemInterrompido){
			if(itemInterrompidoParcial)
				descricao.append(" - Servi�o recusado parcialmente");
			else
				descricao.append(" - Servi�o recusado");
		}
		return descricao.toString();
	}
	
	public List<NotaFiscalServico> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String whereNotIn, String whereInNaturezaOperacao){
		return notaFiscalServicoDAO.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, whereNotIn, whereInNaturezaOperacao);
	}
	
	public String montaInformacoesContribuinteTemplate(String whereInVenda, NotaFiscalServico bean){
		List<Venda> listaVenda = new ArrayList<Venda>();
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInVenda)){
			for(String id : whereInVenda.split(",")){
				listaVenda.add(new Venda(Integer.parseInt(id)));
			}
		}
		
		return montaInformacoesContribuinteTemplate(listaVenda, bean);
	}
	
	public String montaInformacoesContribuinteTemplate(List<Venda> listaVenda, NotaFiscalServico bean){
		try {
			if(bean.getNaturezaoperacao() == null){
				return null;
			}
			bean.setNaturezaoperacao(naturezaoperacaoService.loadForEntrada(bean.getNaturezaoperacao()));
			
			if(bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getTemplateinfcontribuinte() != null){
				Cliente cliente = bean.getCliente();
				
				List<Venda> listaVendas = new ArrayList<Venda>();
				if(SinedUtil.isListNotEmpty(listaVenda)){
					listaVendas = vendaService.findForCobranca(CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ","));
				}
				
				ReportTemplateBean reportTemplate = reportTemplateService.load(bean.getNaturezaoperacao().getTemplateinfcontribuinte());
				ITemplateEngine engine = new GroovyTemplateEngine().build(reportTemplate.getLeiaute());
				InformacaoContribuinteNfseBean beanInfo = new InformacaoContribuinteNfseBean();
				
				Map<String,Object> datasource = new HashMap<String, Object>();
				
				Venda venda = SinedUtil.isListNotEmpty(listaVendas) ? listaVendas.iterator().next() : null;
				
				cliente = cliente != null ? clienteService.loadForInfoContribuinte(cliente): null;
				ClienteBean clientereportBean = new ClienteBean(cliente);
				
				GrupoTributacaoBean grupotributacaoreportBean = new GrupoTributacaoBean();
				if(bean.getGrupotributacao() != null){
					grupotributacaoreportBean = new GrupoTributacaoBean(grupotributacaoService.load(bean.getGrupotributacao()));
				}
				
				Empresa empresa = bean.getEmpresa();
				if(empresa == null && venda != null && venda.getEmpresa() != null){
					empresa = venda.getEmpresa();
				}
				
				empresaService.setInformacoesPropriedadeRural(empresa);
				
				EmpresaBean empresareportBean = empresa != null ? new EmpresaBean(empresaService.loadForInfoContribuinte(empresa)) : new EmpresaBean(null);
				
				NaturezaOperacaoBean naturezaoperacaoreportBean = new NaturezaOperacaoBean(bean.getNaturezaoperacao());
				
				LinkedList<VendaInfoContribuinteBean> listaBeanVendas = new LinkedList<VendaInfoContribuinteBean>();
				if(SinedUtil.isListNotEmpty(listaVendas)){
					Colaboradorcargo colaboradorcargo;
					for(Venda v: listaVendas){
						colaboradorcargo = null;
						if(v.getColaborador() != null){
							colaboradorcargo = colaboradorcargoService.findCargoAtual(v.getColaborador());
						}
						listaBeanVendas.add(new VendaInfoContribuinteBean(v, colaboradorcargo));				
					}
				}
				
				beanInfo.setCliente(clientereportBean);
				beanInfo.setEmpresa(empresareportBean);
				beanInfo.setGrupoTributacao(grupotributacaoreportBean);
				beanInfo.setNaturezaoperacao(naturezaoperacaoreportBean);
				beanInfo.setVendas(listaBeanVendas);
				
				datasource.put("bean", beanInfo);
				String retorno = engine.make(datasource);
				if(retorno != null && retorno.length() > 500){
					retorno = retorno.substring(0, 499);
				}
				return retorno;
			}
			return null;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public NotaFiscalServico criaCopia(NotaFiscalServico origem) {
		NotaFiscalServico copia = this.loadForEntrada(origem);
		copia.setCdNota(null);

		if(SinedUtil.isListNotEmpty(copia.getListaItens())){
			for(NotaFiscalServicoItem item : copia.getListaItens()){
				item.setCdNotaFiscalServicoItem(null);
				item.setNotaFiscalServico(null);
			}
		}
		if(SinedUtil.isListNotEmpty(copia.getListaDuplicata())){
			for(Notafiscalservicoduplicata item : copia.getListaDuplicata()){
				item.setCdnotafiscalservicoduplicata(null);
				item.setNotafiscalservico(null);
			}
		}
		if(copia.getProjeto() != null && copia.getProjeto().getCdprojeto() != null){
			Projeto projetoCopiado = projetoService.load(copia.getProjeto());
			if(Situacaoprojeto.CANCELADO.equals(projetoCopiado.getSituacao()) || Situacaoprojeto.CONCLUIDO.equals(projetoCopiado.getSituacao())){
				copia.setProjeto(null);
			}
		}
		return copia;
	}
	
	public List<NotaFiscalServico> findNotaSemContaBoleto(String whereIn) {
		return notaFiscalServicoDAO.findNotaSemContaBoleto(whereIn);
	}
	
	public void setPrazogapamentoUnico(NotaFiscalServico nfs) {
		if(nfs != null && nfs.getPrazopagamentofatura() == null && Hibernate.isInitialized(nfs.getListaDuplicata()) && 
				nfs.getListaDuplicata() != null && nfs.getListaDuplicata().size() == 1){
			nfs.setPrazopagamentofatura(Prazopagamento.UNICA);
		}
	}
	
	public void setDescricaoObservacaoDocumentoItensNota(NotaFiscalServico nfs, Documento documento, String descricao) {
		if(documento != null && nfs != null && SinedUtil.isListNotEmpty(nfs.getListaItens())){
			StringBuilder sb = new StringBuilder();
			for(NotaFiscalServicoItem item : nfs.getListaItens()){
				if(org.apache.commons.lang.StringUtils.isNotBlank(item.getDescricao()) && !sb.toString().contains(item.getDescricao())){
					sb.append(item.getDescricao()).append("\n");
				} else {
					if (item.getMaterial() != null && org.apache.commons.lang.StringUtils.isNotBlank(item.getMaterial().getNome()) && !sb.toString().contains(item.getMaterial().getNome())) {
						sb.append(item.getMaterial().getNome() + "\n");
					}
				}
			}
			if(sb.length() > 150 || descricao != null){
				Integer limiteSplit = descricao == null ? sb.substring(0, 150).lastIndexOf(" ")+1 : 0;
				if(limiteSplit <= 0 && sb.length() > 150 && descricao == null){
					limiteSplit = 150;
				}
				if(descricao != null){
					documento.setDescricao(descricao);
				}else {
					documento.setDescricao(sb.substring(0, limiteSplit));
				}
				String obs = documento.getObservacao() != null ? documento.getObservacao() : "";
				if(limiteSplit < sb.length()){
					documento.setObservacao(sb.substring(limiteSplit) + obs);
				}
				if(documento.getObservacao().length() > 2000){
					documento.setObservacao(documento.getObservacao().substring(0, 1999));
				}
			} else {
				documento.setDescricao(sb.toString());
			}
		}
	}
	
	public List<Rateioitem> getListaRateioitemFaturamento(NotaFiscalServico nfs) {
		List<NotaFiscalServicoItem> lista = notaFiscalServicoItemService.findForRateioFaturamento(nfs);
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		if(SinedUtil.isListNotEmpty(lista)){
			Vendamaterial vendamaterial;
			Empresa empresa;
			Contagerencial contagerencial;
			Centrocusto centrocusto;
			
			Money valorTotal = new Money();
			Money totalItem;
			for (NotaFiscalServicoItem it : lista) {
				totalItem = it.getTotalParcial();
				if(it.getVendamaterial() != null && it.getTotalParcial() != null){
					valorTotal = valorTotal.add(it.getTotalParcial());
				}
			}
			
			for (NotaFiscalServicoItem it : lista) {
				vendamaterial = it.getVendamaterial();
				totalItem = it.getTotalParcial();
				if(vendamaterial == null || totalItem == null) continue;
				
				empresa = it.getNotaFiscalServico() != null ? it.getNotaFiscalServico().getEmpresa() : null;
				
				Rateioitem rateioitem = new Rateioitem();
				
				contagerencial = empresa != null ? empresa.getContagerencial() : null;
				if (vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().getContagerencialvenda() != null && vendamaterial.getMaterialmestre().getContagerencialvenda().getCdcontagerencial() != null) {
					contagerencial = vendamaterial.getMaterialmestre().getContagerencialvenda();
				} else if (vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getContagerencialvenda() != null && vendamaterial.getMaterial().getContagerencialvenda().getCdcontagerencial() != null) {
					contagerencial = vendamaterial.getMaterial().getContagerencialvenda();
				}
				rateioitem.setContagerencial(contagerencial);
				
				if (vendamaterial.getVenda() != null && vendamaterial.getVenda().getCentrocusto() != null) {
					centrocusto = vendamaterial.getVenda().getCentrocusto();
				} else {
					centrocusto = empresa != null ? empresa.getCentrocusto() : null;
					if (vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().getCentrocustovenda() != null && vendamaterial.getMaterialmestre().getCentrocustovenda().getCdcentrocusto() != null) {
						centrocusto = vendamaterial.getMaterialmestre().getCentrocustovenda();
					} else if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCentrocustovenda() != null && vendamaterial.getMaterial().getCentrocustovenda().getCdcentrocusto() != null) {
						centrocusto = vendamaterial.getMaterial().getCentrocustovenda();
					}
				}

				rateioitem.setCentrocusto(centrocusto);
				rateioitem.setProjeto(vendamaterial.getVenda() != null ? vendamaterial.getVenda().getProjeto() : null);
				rateioitem.setValor(totalItem);
				rateioitem.setPercentual(0d);
				if (valorTotal.getValue().doubleValue() > 0) {
					rateioitem.setPercentual(totalItem.getValue().doubleValue() / valorTotal.getValue().doubleValue() * 100);
				}
				
				listaRateioitem.add(rateioitem);
			}
		}
		
		return listaRateioitem;
	}
	
	public String validaDivergenciaValoresParcela(WebRequestContext request){
		request.setAttribute("tipoNota", "servico");
		return notaService.validaDivergenciaValoresParcela(request);
	}
	
	public List<NotaFiscalServico> findByWhereInForCalculototal(String whereIn) {
		return notaFiscalServicoDAO.findByWhereInForCalculototal(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param empresa
	 * @param numeronota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public NotaFiscalServico findNotaByNumeroEmpresa(Empresa empresa, String numeronota) {
		return notaFiscalServicoDAO.findNotaByNumeroEmpresa(empresa, numeronota);
	}

	public List<NotaFiscalServico> findNotaWithNaturezaoperacao(String whereIn){
		return notaFiscalServicoDAO.findNotaWithNaturezaoperacao(whereIn);
	}
	
	public void criarAvisoNotaSemContaReceberNaoLiquidada(Motivoaviso m, java.sql.Date data, java.sql.Date dateToSearch) {
		List<NotaFiscalServico> notaList = notaFiscalServicoDAO.findByNotaSemContaReceber(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Nota n : notaList) {
			Aviso aviso = new Aviso("Nota fiscal sem v�nculo a uma conta a receber", "N�mero da nota fiscal: " + (!org.apache.commons.lang.StringUtils.isEmpty(n.getNumero()) ?  n.getNumero() : "Sem n�mero"), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), n.getCdNota(), n.getEmpresa() != null ? n.getEmpresa() : empresa, SinedDateUtils.currentDate(), m,
					Boolean.FALSE);
			java.sql.Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, n.getCdNota());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, n.getCdNota(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public List<NotaFiscalServicoItem> getListaItensAjustadaSped(NotaFiscalServico nfs) {
		if(nfs != null && SinedUtil.isListNotEmpty(nfs.getListaItens())){
			setValorProporcionalImpostoItem(nfs);
			for(NotaFiscalServicoItem item : nfs.getListaItens()){
				if(nfs.getDeducao() != null && nfs.getDeducao().getValue().doubleValue() > 0){
					item.setDeducao(getValorDeducaoItem(nfs.getListaItens(), item, nfs.getDeducao(), false));
				}
			}
		}
		return nfs.getListaItens();
	}
	
	private Money getValorDeducaoItem(List<NotaFiscalServicoItem> listaItens, NotaFiscalServicoItem item, Money deducao, boolean calcularDescontoUnitario) {
		Money valorDesconto = new Money(0);
		
		if (deducao != null && deducao.getValue().doubleValue() > 0 && item.getQtde() != null && item.getPrecoUnitario() != null){
			Money totalMaterial = new Money(0);
			if(SinedUtil.isListNotEmpty(listaItens)){
				for (NotaFiscalServicoItem it : listaItens) {
					if(it.getQtde() != null){
						totalMaterial = totalMaterial.add(new Money(it.getPrecoUnitario() * it.getQtde()));
						if(it.getDesconto() != null){
							totalMaterial = totalMaterial.subtract(it.getDesconto());
						}
					}
				}
			}
			
			Money valorMaterial = new Money(item.getPrecoUnitario() *  item.getQtde());
			
			if(item.getDesconto() != null){
				valorMaterial = valorMaterial.subtract(item.getDesconto());
			}
			
			//Calcula o valor total de desconto proporcional para o produto
			if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
				totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
				valorDesconto = deducao.multiply(valorMaterial.divide(totalMaterial));
			}		
			
			//Calcula o valor do desconto unit�rio
			if (calcularDescontoUnitario){
				valorDesconto = valorDesconto.divide(new Money(item.getQtde()));
			}
		}		
		
		return valorDesconto;
	}
	
	private void setValorProporcionalImpostoItem(NotaFiscalServico nfs) {
		if (nfs.getBasecalculocofins() != null || nfs.getBasecalculopis() != null){
			Money totalMaterial = nfs.getValorTotalServicos();
			Money perc100 = new Money(100);
			if(totalMaterial != null && totalMaterial.getValue().doubleValue() > 0 && SinedUtil.isListNotEmpty(nfs.getListaItens())){
				for (NotaFiscalServicoItem it : nfs.getListaItens()) {
					if(it.getTotalParcial() != null){
						if(nfs.getBasecalculocofins() != null){
							it.setBasecalculoCofins(it.getTotalParcial().divide(totalMaterial).multiply(nfs.getBasecalculocofins()));
							if(nfs.getCofins() != null){
								it.setValorCofins(new Money(SinedUtil.roundFloor(it.getBasecalculoCofins().multiply(new Money(nfs.getCofins()).divide(perc100)).getValue(), 2)));
							}
						}
						if(nfs.getBasecalculopis() != null){
							it.setBasecalculoPis(it.getTotalParcial().divide(totalMaterial).multiply(nfs.getBasecalculopis()));
							if(nfs.getPis() != null){
								it.setValorPis(new Money(SinedUtil.roundFloor(it.getBasecalculoPis().multiply(new Money(nfs.getPis()).divide(perc100)).getValue(), 2)));
							}
						}
					}
				}
			}
		}		
	}
	
    public void setValorIssqnProporcionalItem(NotaFiscalServico nfs) {
        if (nfs.getBasecalculoiss() != null) {
            Money totalMaterial = nfs.getValorTotalServicos();
            Money perc100 = new Money(100);
            
            if (totalMaterial != null && totalMaterial.getValue().doubleValue() > 0 && SinedUtil.isListNotEmpty(nfs.getListaItens())) {
                for (NotaFiscalServicoItem it : nfs.getListaItens()) {
                    if (it.getTotalParcial() != null) {
                        if (nfs.getBasecalculoiss() != null) {
                            it.setBaseCalculoIss(it.getTotalParcial().divide(totalMaterial).multiply(nfs.getBasecalculoiss()));
                            if (nfs.getIss() != null) {
                                it.setValorIss(new Money(SinedUtil.roundFloor(it.getBaseCalculoIss().multiply(new Money(nfs.getIss()).divide(perc100)).getValue(), 2)));
                            }
                        }
                    }
                }
            }
        }        
    }
	
	public List<NotaFiscalServico> findForConferenciaNfseSigBancos(Empresa empresa) {
		return notaFiscalServicoDAO.findForConferenciaNfseSigBancos(empresa);
	}
	
	public NotaFiscalServico buscarNumeroNota(Integer cdNota) {
		return notaFiscalServicoDAO.buscarNumeroNota(cdNota);
	}
	
	public void teste(WebRequestContext request, List<NotaFiscalServico> notas, boolean notaseparada){
		if(!parametrogeralService.getBoolean(Parametrogeral.GERAR_NF_SEPARADA_TIPOPEDIDOVENDA)){
			return;
		}
		for (NotaFiscalServico notafiscalservico : notas){ 
			if(notafiscalservico.getListaItens() != null && !notafiscalservico.getListaItens().isEmpty()){
				List<Venda> listaVendaNota = notafiscalservico.getListaVenda();
				
//				vendaService.adicionarParcelaNumeroDocumento(listaVendaNota, notafiscalservico, notafiscalservico.getNumero(), notaseparada, true, true, false);
				
				if(listaVendaNota != null && listaVendaNota.size() > 0){
					boolean recalcularValoresParcelas = listaVendaNota.size() == 1;
					GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
					List<Documento> listaContareceber = new ArrayList<Documento>();
					List<Documento> listaContasCriadas = new ArrayList<Documento>();
					for (Venda venda : listaVendaNota) {
	/*					notaVendaService.saveOrUpdate(new NotaVenda(notafiscalservico, venda));
						
						Vendahistorico vendahistorico = new Vendahistorico();
						vendahistorico.setVenda(venda);*/
						String numero = notafiscalservico.getNumero() == null || notafiscalservico.getNumero().equals("") ? "sem n�mero" : notafiscalservico.getNumero();
						/*vendahistorico.setAcao("Gera��o da nota " + numero);
						vendahistorico.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNotaServico("+notafiscalservico.getCdNota()+");\">"+numero+"</a>.");
						vendahistoricoService.saveOrUpdate(vendahistorico);*/
						
						geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
						if(venda.getPedidovendatipo() != null){
							geracaocontareceberEnum = venda.getPedidovendatipo().getGeracaocontareceberEnum();
						}
						if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum)){
//							vendaService.gerarContareceberAposEmissaonota(request, venda, notafiscalservico.getDtEmissao(), null, null, notafiscalservico.getValorNota(), null, (somenteservico ? NotaTipo.NOTA_FISCAL_SERVICO : null), null, somenteservico != null && somenteservico, recalcularValoresParcelas, listaContareceber, null, venda.getPrazopagamento());
							//listaContasCriadas = vendaService.gerarContareceberAposEmissaonota2(request, venda, notafiscalservico.getDtEmissao(), null, null, notafiscalservico.getValorNota(), null, NotaTipo.NOTA_FISCAL_SERVICO, null, true, recalcularValoresParcelas, listaContareceber, null, venda.getPrazopagamento(), "S", true);
						}
						
						for(Documento documento: listaContasCriadas){
							documento = documentoService.carregaDocumento(documento);
							documento.setAcaohistorico(Documentoacao.ALTERADA);
							Documentohistorico documentohistorico = new Documentohistorico(documento);
							String obs = "Vinculado a nota <a href=\"javascript:visualizarNotaFiscalServico("
										+ notafiscalservico.getCdNota()
										+ ")\">"
										+ notafiscalservico.getCdNota() + "</a>. ";
							documentohistorico.setObservacao(obs);
							documentohistoricoService.saveOrUpdate(documentohistorico);
						}
						
						String whereInDocumento = CollectionsUtil.listAndConcatenate(listaContasCriadas, "cddocumento", ",");
						List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDeVenda(venda, whereInDocumento);
						if(SinedUtil.isListNotEmpty(listadocumentoorigem)){
							for (Documentoorigem documentoorigem : listadocumentoorigem) {
								Documento documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
								if(documento != null){
									vendaService.gerarMensagensDocumento(venda, documento, notafiscalservico, true, notaseparada);
									
									NotaDocumento notaDocumento = new NotaDocumento();
									notaDocumento.setDocumento(documento);
									notaDocumento.setNota(notafiscalservico);
									notaDocumentoService.saveOrUpdate(notaDocumento);
								}
							}
						}
					}
				}
			}
		}
	}
}
