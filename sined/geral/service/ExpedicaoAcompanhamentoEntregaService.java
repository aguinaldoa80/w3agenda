package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.correios.webservice.resource.Objeto;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.ExpedicaoAcompanhamentoEntrega;
import br.com.linkcom.sined.geral.dao.ExpedicaoAcompanhamentoEntregaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExpedicaoAcompanhamentoEntregaService extends GenericService<ExpedicaoAcompanhamentoEntrega>{

	private ExpedicaoAcompanhamentoEntregaDAO expedicaoAcompanhamentoEntregaDAO;
	
	private static ExpedicaoAcompanhamentoEntregaService instance;
	
	public static ExpedicaoAcompanhamentoEntregaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExpedicaoAcompanhamentoEntregaService.class);
		}
		return instance;
	}
	
	public void setExpedicaoAcompanhamentoEntregaDAO(ExpedicaoAcompanhamentoEntregaDAO expedicaoAcompanhamentoEntregaDAO) {
		this.expedicaoAcompanhamentoEntregaDAO = expedicaoAcompanhamentoEntregaDAO;
	}
	
	public List<ExpedicaoAcompanhamentoEntrega> findByExpedicao(Expedicao expedicao){
		return expedicaoAcompanhamentoEntregaDAO.findByExpedicao(expedicao);
	}
	
	public void saveList(final List<ExpedicaoAcompanhamentoEntrega> lista){
		getGenericDAO().getTransactionTemplate().execute(
			new TransactionCallback() {
					@Override
					public Object doInTransaction(TransactionStatus status) {
						for(ExpedicaoAcompanhamentoEntrega acompanhamento: lista){
							ExpedicaoAcompanhamentoEntregaService.getInstance().saveOrUpdateNoUseTransaction(acompanhamento);
						}
						return null;
					}
				}
			);
	}
	
	public List<ExpedicaoAcompanhamentoEntrega> findAcompanhamentosOriginadosDosCorreios(Expedicao expedicao){
		return expedicaoAcompanhamentoEntregaDAO.findAcompanhamentosOriginadosDosCorreios(expedicao);
	}
	
	public void createCompanhamentoEntregaByObjetoCorreios(Objeto objeto){
		
	}
}
