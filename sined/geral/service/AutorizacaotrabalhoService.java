package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Autorizacaotrabalho;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.dao.AutorizacaotrabalhoDAO;
import br.com.linkcom.sined.geral.dao.RequisicaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AutorizacaotrabalhoService extends GenericService<Autorizacaotrabalho> {
	protected AutorizacaotrabalhoDAO autorizacaotrabalhoDAO;
	protected RequisicaoDAO requisicaoDAO;
	
	public void setAutorizacaotrabalhoDAO(
			AutorizacaotrabalhoDAO autorizacaotrabalhoDAO) {
		this.autorizacaotrabalhoDAO = autorizacaotrabalhoDAO;
	}
	public void setRequisicaoDAO(RequisicaoDAO requisicaoDAO) {
		this.requisicaoDAO = requisicaoDAO;
	}

	/* singleton */
	private static AutorizacaotrabalhoService instance;
	public static AutorizacaotrabalhoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AutorizacaotrabalhoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @author Marden
	 * @param requisicao
	 */	
	public List<Autorizacaotrabalho> findByRequisicao(Requisicao requisicao){
		return autorizacaotrabalhoDAO.findByRequisicao(requisicao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @author Taidson
	 * @since 24/05/2010
	 */
	public Double calculaHoras (Requisicao requisicao){
		return autorizacaotrabalhoDAO.calculaHoras(requisicao);
	}
	
	
}
