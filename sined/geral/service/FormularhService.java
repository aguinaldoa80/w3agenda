package br.com.linkcom.sined.geral.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhcargo;
import br.com.linkcom.sined.geral.bean.Formularhitem;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.geral.dao.FormularhDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.funcao.Funcao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormularhService extends GenericService<Formularh>{
	
	private FormularhDAO formularhDAO;
	private FormularhcargoService formularhcargoService;
	private ColaboradorcargoService colaboradorcargoService;
	private FormularhvariavelService formularhvariavelService;
	private ColaboradordespesaService colaboradordespesaService;
	private AusenciacolaboradorService ausenciacolaboradorService;
	private DocumentocomissaoService documentocomissaoService;
	private ContratoService contratoService;
	private ColaboradordependenteService colaboradordependenteService;
	private DespesaviagemService despesaviagemService;
	private ApontamentoService apontamentoService;
	
	protected static final String[] VARIAVEIS_BASE = {"SALARIO", "VALE_TRANSPORTE", "DIAS_AUSENTES", "COMISSAO_VENCIMENTO", "COMISSAO_PAGAMENTO", "COMISSAO_EMISSAO", "COMISSAO_CONTRATO", "QTD_DEPENDENTES", "ATIVIDADES", "ATIVIDADE_HORA", "DESPESA_VIAGEM"};
	
	public void setAusenciacolaboradorService(AusenciacolaboradorService ausenciacolaboradorService) {this.ausenciacolaboradorService = ausenciacolaboradorService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {this.colaboradordespesaService = colaboradordespesaService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setFormularhvariavelService(FormularhvariavelService formularhvariavelService) {this.formularhvariavelService = formularhvariavelService;}	
	public void setFormularhDAO(FormularhDAO formularhDAO) {this.formularhDAO = formularhDAO;}
	public void setFormularhcargoService(FormularhcargoService formularhcargoService) {this.formularhcargoService = formularhcargoService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setColaboradordependenteService(ColaboradordependenteService colaboradordependenteService) {this.colaboradordependenteService = colaboradordependenteService;}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}

	private static FormularhService instance;
	public static FormularhService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FormularhService.class);
		}
		return instance;
	}
	
	/**
	 *
	 * @param cdcargos
	 * @return
	 */
	public Formularh findByCargo(Cargo cdcargos){
		return formularhDAO.findByCargo(cdcargos);
	}	
	
	class PeriodoHolerite {
		public PeriodoHolerite(Date dtinicio, Date dtfim) {
			this.dtinicio = dtinicio;
			this.dtfim = dtfim;
		}
		
		private Date dtinicio;
		private Date dtfim;
		
		public Date getDtinicio() {
			return dtinicio;
		}
		
		public Date getDtfim() {
			return dtfim;
		}
		
		public void setDtinicio(Date dtinicio) {
			this.dtinicio = dtinicio;
		}
		
		public void setDtfim(Date dtfim) {
			this.dtfim = dtfim;
		}
		
	}

	/**
	 * M�todo que carrega todas as fun��es das form�las e calcula na ordem definida para a gera��o do 
	 * @author Filipe Santos
	 * @param empresa 
	 * @param formulas
	 * @param cdpessoas
	 * @throws EvaluationException 
	 */
	public void calcularAndGerarHolerite(Empresa empresa, String cdpessoasString, Date dtreferencia1, Date dtreferencia2, Projeto projeto, Boolean holeritePorProjeto) throws EvaluationException{
		Evaluator evaluator = getEvaluator();
		
		List<Colaboradorcargo> listaColaboradorcargo = colaboradorcargoService.findByCdpessoa(cdpessoasString);
		
		for (Colaboradorcargo cc : listaColaboradorcargo) {
			if(cc.getColaborador() != null && cc.getColaborador().getDtadmissao() != null && SinedDateUtils.afterIgnoreHour(cc.getColaborador().getDtadmissao(), dtreferencia1)){
				throw new SinedException("N�o � poss�vel gerar holerite com per�odo anterior a data de admiss�o do colaborador "+cc.getColaborador().getNome());
			}
			
			Formularh formularh = formularhDAO.findByCargo(cc.getCargo());
			if (formularh==null){
				throw new SinedException("N�o existe f�rmula cadastrada para: <ul><li>Cargo: "+cc.getCargo().getNome()+" &nbsp;&nbsp;</li><li>Colaborador: "+cc.getColaborador().getNome()+"</li></ul>");
			}
			cc.setFormularh(formularh);
		}
		
		Map<Colaboradorcargo, List<PeriodoHolerite>> mapGeracaoHolerite = new HashMap<Colaboradorcargo, List<PeriodoHolerite>>();
		
		for (Colaboradorcargo cc : listaColaboradorcargo) {
			List<Date> listDate = SinedDateUtils.getListDateByPeriodo(dtreferencia1, dtreferencia2);
			
			List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForListagemHolerite(dtreferencia1, dtreferencia2, empresa, projeto, cc.getColaborador(), holeritePorProjeto);
			for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
				for (Iterator<Date> iterator = listDate.iterator(); iterator.hasNext();) {
					Date data = (Date) iterator.next();
					if(SinedDateUtils.beforeOrEqualIgnoreHour(colaboradordespesa.getDtholeriteinicio(), data) &&
						SinedDateUtils.afterOrEqualsIgnoreHour(colaboradordespesa.getDtholeritefim(), data)){
						iterator.remove();
					}
				}
			}	
			
			List<PeriodoHolerite> listaPeriodoHolerite = new ArrayList<PeriodoHolerite>();
			if(listaColaboradordespesa.size() > 0 && listDate.size() > 0){
				Date dtinicio_aux = null;
				Date dt_aux = null;
				
				for (Date data : listDate) {
					if(dtinicio_aux == null) {
						dtinicio_aux = new Date(data.getTime());
						dt_aux = new Date(data.getTime());
						continue;
					} 
					
					if(!SinedDateUtils.equalsIgnoreHour(SinedDateUtils.incrementDate(dt_aux, 1, Calendar.DAY_OF_MONTH), data)){
						listaPeriodoHolerite.add(new PeriodoHolerite(dtinicio_aux, dt_aux));
						
						dtinicio_aux = new Date(data.getTime());
						dt_aux = new Date(data.getTime());
						continue;
					}
					
					dt_aux = SinedDateUtils.incrementDate(dt_aux, 1, Calendar.DAY_OF_MONTH);
				}
				
				if(dtinicio_aux != null){
					listaPeriodoHolerite.add(new PeriodoHolerite(dtinicio_aux, dt_aux));
				}
			} else {
				listaPeriodoHolerite.add(new PeriodoHolerite(dtreferencia1, dtreferencia2));
			}
			mapGeracaoHolerite.put(cc, listaPeriodoHolerite);
		}
		
		for (Entry<Colaboradorcargo, List<PeriodoHolerite>> entry : mapGeracaoHolerite.entrySet()) {
			Colaboradorcargo cc = entry.getKey();
			List<PeriodoHolerite> listaPeriodoHolerite = entry.getValue();
			
			if(holeritePorProjeto){
				for (PeriodoHolerite periodoHolerite : listaPeriodoHolerite) {
					List<Projeto> listaProjeto = this.getListaProjeto(empresa, cc.getColaborador(), periodoHolerite.getDtinicio(), periodoHolerite.getDtfim(), projeto);
					for (Projeto projetoHolerite : listaProjeto) {
						this.gerarHolerite(empresa, periodoHolerite.getDtinicio(), periodoHolerite.getDtfim(), projetoHolerite, evaluator, cc);
					}
				}
			} else {
				for (PeriodoHolerite periodoHolerite : listaPeriodoHolerite) {
					this.gerarHolerite(empresa, periodoHolerite.getDtinicio(), periodoHolerite.getDtfim(), projeto, evaluator, cc);
				}
			}
		}
	}
	
	private List<Projeto> getListaProjeto(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2, Projeto projeto) {
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		List<Apontamento> listaApontamento = apontamentoService.findByColaborador(empresa, colaborador, dtreferencia1, dtreferencia2, null, projeto, ApontamentoSituacaoEnum.AUTORIZADO);
		for (Apontamento apontamento : listaApontamento) {
			if(apontamento.getProjeto() != null && !listaProjeto.contains(apontamento.getProjeto())){
				listaProjeto.add(apontamento.getProjeto());
			}
		}
		return listaProjeto;
	}
	
	private void gerarHolerite(Empresa empresa, Date dtreferencia1, Date dtreferencia2, Projeto projeto, Evaluator evaluator, Colaboradorcargo cc) throws EvaluationException {
		List<Colaboradordespesaitem> cdi = new ArrayList<Colaboradordespesaitem>();
		List<Apontamento> listaApontamento = null;
		
		List<Formularhitem> listaFormularhitens = FormularhitemService.getInstance().findByHolerite(cc.getFormularh());
		
		if(this.haveVariavelFormula("SALARIO", listaFormularhitens)) 
			evaluator.putVariable("SALARIO", this.formatoEvaluator(cc.getSalario()));
		
//		if(this.haveVariavelFormula("VALE_TRANSPORTE", listaFormularhitens)) 
//			evaluator.putVariable("VALE_TRANSPORTE", this.formatoEvaluator(cc.getColaborador().getValorvaletransporte()));
		
		if(this.haveVariavelFormula("DIAS_AUSENTES", listaFormularhitens)) 
			evaluator.putVariable("DIAS_AUSENTES", ausenciacolaboradorService.getNumeroDiasAusenciaColaborador(cc.getColaborador(), dtreferencia1, dtreferencia2).toString());
		
		if(this.haveVariavelFormula("COMISSAO_VENCIMENTO", listaFormularhitens)) 
			evaluator.putVariable("COMISSAO_VENCIMENTO", this.formatoEvaluator(documentocomissaoService.getTotalComissaoColaboradorMes(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2, "Vencimento")));
		
		if(this.haveVariavelFormula("COMISSAO_PAGAMENTO", listaFormularhitens)) 
			evaluator.putVariable("COMISSAO_PAGAMENTO", this.formatoEvaluator(documentocomissaoService.getTotalComissaoColaboradorMes(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2, "Pagamento")));
		
		if(this.haveVariavelFormula("COMISSAO_EMISSAO", listaFormularhitens)) 
			evaluator.putVariable("COMISSAO_EMISSAO", this.formatoEvaluator(documentocomissaoService.getTotalComissaoColaboradorMes(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2, "Emissao")));
		
		if(this.haveVariavelFormula("COMISSAO_CONTRATO", listaFormularhitens)) 
			evaluator.putVariable("COMISSAO_CONTRATO", this.formatoEvaluator(contratoService.getCalculaTotalComissaoColaboradorMesDtassinatura(cc.getColaborador(), dtreferencia1, dtreferencia2)));
		
		if(this.haveVariavelFormula("QTD_DEPENDENTES", listaFormularhitens)) {
			Integer qtdedependentes = colaboradordependenteService.getQtdeDependentes(cc.getColaborador());
			evaluator.putVariable("QTD_DEPENDENTES", qtdedependentes != null ? qtdedependentes.toString() : "0");
		}
		
		if(this.haveVariavelFormula("ATIVIDADES", listaFormularhitens)) {
			listaApontamento = apontamentoService.findByColaborador(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2, ApontamentoTipo.FUNCAO_ATIVIDADE, projeto, ApontamentoSituacaoEnum.AUTORIZADO);
			evaluator.putVariable("ATIVIDADES",  this.formatoEvaluator(apontamentoService.getValortotalAtividadeByColaborador(listaApontamento, cc.getColaborador())));
		}
		if(this.haveVariavelFormula("ATIVIDADE_HORA", listaFormularhitens)) {
			listaApontamento = apontamentoService.findByColaborador(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2, ApontamentoTipo.FUNCAO_HORA, projeto, ApontamentoSituacaoEnum.AUTORIZADO);
			evaluator.putVariable("ATIVIDADE_HORA",  this.formatoEvaluator(apontamentoService.getValortotalAtividadeByColaborador(listaApontamento, cc.getColaborador())));
		}
		
		if(this.haveVariavelFormula("DESPESA_VIAGEM", listaFormularhitens)) {
			evaluator.putVariable("DESPESA_VIAGEM",  this.formatoEvaluator(despesaviagemService.getTotalDespesaviagemByColaborador(empresa, cc.getColaborador(), dtreferencia1, dtreferencia2)));
		}
		
		Double calculoSomatorio = new Double(0);
		
		this.substituiIdentificadorPorFormula(listaFormularhitens);
		
		for (Formularhitem frhi : listaFormularhitens){
			Colaboradordespesaitem colaboradordespesaitem = new Colaboradordespesaitem();
			
			String salarioTexto = evaluator.evaluate(formatoFuncao(frhi.getFormula()));
			Double formulaResultado = Double.parseDouble(salarioTexto);
			
			calculoSomatorio += formulaResultado;
			
			colaboradordespesaitem.setMotivo(frhi.getIdentificador());
			
			if(formulaResultado > 0){
				colaboradordespesaitem.setValor(new Money(formulaResultado));
				colaboradordespesaitem.setTipo(Colaboradordespesaitemtipo.CREDITO);
			} else {
				colaboradordespesaitem.setValor(new Money(formulaResultado*-1d));
				colaboradordespesaitem.setTipo(Colaboradordespesaitemtipo.DEBITO);
			}
			
			
			cdi.add(colaboradordespesaitem);
		}			
		
		colaboradordespesaService.gerarHolerite(new Money(calculoSomatorio), cc.getColaborador(), cdi, dtreferencia1, dtreferencia2, empresa, projeto);
		
		if(listaApontamento != null && !listaApontamento.isEmpty()){
			apontamentoService.updateSituacao(ApontamentoSituacaoEnum.PAGO, SinedUtil.listAndConcatenate(listaApontamento, "cdapontamento", ","));
		}
	}
	
	/**
	 * M�todo que retorna um array com os identificadores de cada f�rmula
	 *
	 * @param formularh
	 * @return
	 * @author Luiz Fernando
	 */
	public String[] getIdentificadores(Formularh formularh) {
		return CollectionsUtil.listAndConcatenate(formularh.getFormularhitem(), "identificador", ",").split(",");
	}
	
	/**
	 * M�todo que substitui o identificador pela f�rmula
	 *
	 * @param listaFormularhitem
	 * @author Luiz Fernando
	 */
	public void substituiIdentificadorPorFormula(List<Formularhitem> listaFormularhitem) {
		if(listaFormularhitem != null && !listaFormularhitem.isEmpty()){
			for(Formularhitem item : listaFormularhitem){
				if(item.getFormula() != null){
					for(Formularhitem item2 : listaFormularhitem){
						if(item2.getFormula() != null && item.getIdentificador() != null && item2.getIdentificador() != null){
							item.setFormula(item.getFormula().replaceAll("<" + item2.getIdentificador() + ">", "("+item2.getFormula()+")"));
						}
					}
				}
			}
		}
	}
	
	private boolean haveVariavelFormula(String variavel, List<Formularhitem> listaFormularhitens) {
		for (Formularhitem it : listaFormularhitens) {
			if(it.getFormula().contains(variavel)) return true;
		}
		return false;
	}

	/**
	 * Inst�ncia um evaluator com todas as fun��es cadastradas no sistema
	 * @author Igor Silv�rio Costa
	 * @return
	 * @throws ClassNotFoundException Caso a classe cadastrada na tabela n�o exista no W3 
	 * @throws d 
	 * @throws InstantiationException 
	 */
	@SuppressWarnings("unchecked")
	public Evaluator getEvaluator() {
		Evaluator evaluator = new Evaluator();
		List<Funcao> listaFuncoes = new ArrayList<Funcao>();
		List<Formularhvariavel> lista = formularhvariavelService.findAll();
		for (Formularhvariavel frv : lista) {
			try {
				Constructor<Funcao> construtor = (Constructor<Funcao>) Class.forName("br.com.linkcom.sined.util.funcao.PERCENTUAL").getConstructor(Formularhvariavel.class, String.class);
				Funcao funcao = construtor.newInstance(frv, frv.getNome());
				
				evaluator.putFunction(funcao);
				listaFuncoes.add(funcao);
			} catch (InstantiationException e) {
				e.printStackTrace();
				throw new SinedException("Erro ao inicializar a f�rmula.");				
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				throw new SinedException("Ouve um erro ao tentar executar a f�rmula.");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new SinedException("N�o foi poss�vel encontrar a f�rmula solicitada.");
			} catch (SecurityException e) {				
				e.printStackTrace();
				throw new SinedException("Erro de Seguran�a.");
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
				throw new SinedException("N�o existe a a��o solicitada.");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				throw new SinedException("Ouve um erro ao tentar executar a f�rmula.");
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				throw new SinedException("N�o foi possivel executar a a��o solicitada.");
			}
		}
		return evaluator;
	}
	
	/**
	 * @author Filipe Santos 
	 * @param salario
	 * @return valor - O valor do salario ajustado para o formato do evaluator
	 */
	public String formatoEvaluator(Money salario){
		String valor = salario.toString();		
		return valor.replaceAll("\\." , "").replaceAll("\\," , "\\.");
	}

	/**
	 * @author Filipe Santos
	 * @param funcao
	 * @return funcao - A fun��o formatada para o formato correto do evaluator
	 */
	public String formatoFuncao(String funcao){		
		for (String variavel : VARIAVEIS_BASE) {
			funcao = funcao.replace(variavel, "#{"+variavel+"}");
		}
		return funcao;
	}
	
	/**
	 * M�todo que formata o identificador para valida��o
	 *
	 * @param funcao
	 * @param identificador
	 * @return
	 * @author Luiz Fernando
	 */
	public String formataIdentificador(String funcao, String identificador[]){		
		for (String variavel : identificador) {
			funcao = funcao.replace("<"+variavel+">", "#{"+variavel+"}");
		}
		return funcao;
	}
	
	/**
	 * Verifica se a alguma formula j� possui o cargo selecionado durante a hora de salvar a form�la
	 * @param whereIn
	 * @author Filipe Santos
	 * @return Boolean
	 */
	public Boolean validaCargo (String whereIn, String cargos){
		List<Formularhcargo> validaCargo = formularhcargoService.validaCargo(whereIn,cargos);
		if(validaCargo == null || validaCargo.isEmpty()){
			return true;
		} else {
			return false;			
		}	
	}
}