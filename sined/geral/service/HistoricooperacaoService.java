package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.GeraMovimentacao;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.dao.HistoricooperacaoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.HistoricooperacaoBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class HistoricooperacaoService extends  GenericService<Historicooperacao>{

	private HistoricooperacaoDAO historicooperacaoDAO;
	private ArquivonfnotaService arquivonfnotaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaService notaService;
	private ChequeService chequeService;
	private ContaService contaService;
	
	public static final String VARIAVEL_HISTORICO_NUMDOCUMENTO = "$NUMDOCUMENTO";
	public static final String VARIAVEL_HISTORICO_DESCRICAO = "$DESCRICAO";
	public static final String VARIAVEL_HISTORICO_PAGAMENTOA = "$PAGAMENTOA";
	public static final String VARIAVEL_HISTORICO_CLIENTE = "$CLIENTE";
	public static final String VARIAVEL_HISTORICO_FORNECEDOR = "$FORNECEDOR";
	public static final String VARIAVEL_HISTORICO_PARCELA = "$PARCELA";
	public static final String VARIAVEL_HISTORICO_CONTAORIGEM = "$CONTAORIGEM";
	public static final String VARIAVEL_HISTORICO_CONTADESTINO = "$CONTADESTINO";
	public static final String VARIAVEL_HISTORICO_NOMECONTAORIGEM = "$NOMECONTAORIGEM";
	public static final String VARIAVEL_HISTORICO_NOMECONTADESTINO = "$NOMECONTADESTINO";
	public static final String VARIAVEL_HISTORICO_DATA = "$DATA";
	public static final String VARIAVEL_HISTORICO_CHEQUE = "$CHEQUE";
	public static final String VARIAVEL_HISTORICO_VALORVINCULADO = "$VALORVINCULADO";
	public static final String VARIAVEL_HISTORICO_DATAVENCIMENTO = "$DATAVENCIMENTO";
	public static final String VARIAVEL_HISTORICO_DATABANCO = "$DATABANCO";
	public static final String VARIAVEL_HISTORICO_RECEBIMENTODE = "$RECEBIMENTODE";
	public static final String VARIAVEL_HISTORICO_VALOR = "$VALOR";
	public static final String VARIAVEL_HISTORICO_NUMERONOTA = "$NUMERONOTA";
	public static final String VARIAVEL_HISTORICO_DATAENTRADA = "$DATAENTRADA";
	public static final String VARIAVEL_HISTORICO_DATAEMISSAO = "$DATAEMISSAO";
	public static final String VARIAVEL_HISTORICO_NUMERO = "$NUMERO";
	public static final String VARIAVEL_HISTORICO_VALORDOCUMENTO = "$VALORDOCUMENTO"; 
	public static final String VARIAVEL_HISTORICO_SITUACAO = "$SITUACAO";
	
	public void setHistoricooperacaoDAO(
			HistoricooperacaoDAO historicooperacaoDAO) {
		this.historicooperacaoDAO = historicooperacaoDAO;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setChequeService(ChequeService chequeService) {
		this.chequeService = chequeService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}

	public Historicooperacao findByTipo(Tipohistoricooperacao tipo){
		return historicooperacaoDAO.findByTipo(tipo); 
	}
	
	public List<Tipohistoricooperacao> listaTipoForContareceber(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CARTAO);
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CHEQUE);
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CRED_EM_CONTA);
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_DINHEIRO);
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO);
		retorno.add(Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CARTAO);
		
		return retorno;
	}
	
	public List<Tipohistoricooperacao> listaTipoForNotafiscal(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.EMISSAO_NF);
		
		return retorno;
	}
	
	public List<Tipohistoricooperacao> listaTipoForTransferenciainterna(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);
		
		return retorno;
	}
	
	public List<Tipohistoricooperacao> listaTipoForTaxarecebimento(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.TAXA_RECEBIMENTO);
		
		return retorno;
	}
	
	public List<Tipohistoricooperacao> listaTipoForContapagar(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.CONTA_PAGAR_BAIXADA_FORNECEDOR);
		return retorno;
	}
	
	public List<Tipohistoricooperacao> listaTipoForRequisitoEntradaFiscal(){
		List<Tipohistoricooperacao> retorno = new ArrayList<Tipohistoricooperacao>();
		retorno.add(Tipohistoricooperacao.REGISTRO_ENTRADA_NF);
		
		return retorno;
	}
	
	public List<GenericBean> findVariaveisByTipolancamento(Tipohistoricooperacao tipolancamento){
		List<GenericBean> retorno = new ArrayList<GenericBean>();
		
		GenericBean varNumDocumento = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_NUMDOCUMENTO, "N�mero do Documento (" + HistoricooperacaoService.VARIAVEL_HISTORICO_NUMDOCUMENTO + ")");
		GenericBean varDescricaoConta = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DESCRICAO, " Descri��o da conta (" + HistoricooperacaoService.VARIAVEL_HISTORICO_DESCRICAO + ")");
		GenericBean varPagamentoA = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_PAGAMENTOA, "Pagamento a (" + HistoricooperacaoService.VARIAVEL_HISTORICO_PAGAMENTOA + ")");
		GenericBean varRecebimentoDe = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_RECEBIMENTODE, "Recebimento de (" + HistoricooperacaoService.VARIAVEL_HISTORICO_RECEBIMENTODE + ")");
		GenericBean varCliente = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_CLIENTE, "Cliente (" + HistoricooperacaoService.VARIAVEL_HISTORICO_CLIENTE + ")");
		GenericBean varFornecedor = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_FORNECEDOR, "Fornecedor (" + HistoricooperacaoService.VARIAVEL_HISTORICO_FORNECEDOR + ")");
		GenericBean varParcela = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_PARCELA, "Parcela (" + HistoricooperacaoService.VARIAVEL_HISTORICO_PARCELA + ")");
		GenericBean varContaOrigem = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_CONTAORIGEM, "C�digo da conta de origem (" + HistoricooperacaoService.VARIAVEL_HISTORICO_CONTAORIGEM + ")");
		GenericBean varContaDestino = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_CONTADESTINO, "C�digo da conta de destino (" + HistoricooperacaoService.VARIAVEL_HISTORICO_CONTADESTINO + ")");
		GenericBean varNomeContaOrigem = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_NOMECONTAORIGEM, "Nome da conta de origem (" + HistoricooperacaoService.VARIAVEL_HISTORICO_NOMECONTAORIGEM + ")");
		GenericBean varNomeContaDestino = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_NOMECONTADESTINO, "Nome da conta de destino (" + HistoricooperacaoService.VARIAVEL_HISTORICO_NOMECONTADESTINO + ")");
		GenericBean varData = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DATA, "Data (" + HistoricooperacaoService.VARIAVEL_HISTORICO_DATA + ")");
		GenericBean varCheque = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE, "Cheque (" + HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE + ")");
		GenericBean varDataBanco = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DATABANCO, "Data do banco(" + HistoricooperacaoService.VARIAVEL_HISTORICO_DATABANCO + ")");
		GenericBean varDataVencimento = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DATAVENCIMENTO, "Data de vencimento(" + HistoricooperacaoService.VARIAVEL_HISTORICO_DATAVENCIMENTO + ")");
		GenericBean varValorVinculado = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO, "Valor vinculado(" + HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO + ")");
		GenericBean varValor = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_VALOR, "Valor (" + HistoricooperacaoService.VARIAVEL_HISTORICO_VALOR + ")");
		GenericBean varNumeroNota = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_NUMERONOTA, "N�mero da nota (" + HistoricooperacaoService.VARIAVEL_HISTORICO_NUMERONOTA + ")");
		GenericBean varDataEntrada = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DATAENTRADA, "Data de entrada (" + HistoricooperacaoService.VARIAVEL_HISTORICO_DATAENTRADA + ")");
		GenericBean varDataEmissao = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_DATAEMISSAO, "Data de Emiss�o (" + HistoricooperacaoService.VARIAVEL_HISTORICO_DATAEMISSAO + ")");
		GenericBean varNumero = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_NUMERO, "N�mero (" + HistoricooperacaoService.VARIAVEL_HISTORICO_NUMERO + ")");
		GenericBean varDocumento = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORDOCUMENTO, "Valor do Documento (" + HistoricooperacaoService.VARIAVEL_HISTORICO_VALORDOCUMENTO + ")");
		GenericBean varSituacao = new GenericBean(HistoricooperacaoService.VARIAVEL_HISTORICO_SITUACAO, "Situa��o (" + HistoricooperacaoService.VARIAVEL_HISTORICO_SITUACAO + ")");

		
		if(tipolancamento != null){
			if(listaTipoForContapagar().contains(tipolancamento)){
				retorno.add(varNumDocumento);
				retorno.add(varDescricaoConta);
				retorno.add(varPagamentoA);
				retorno.add(varParcela);
				retorno.add(varValor);
				retorno.add(varCheque);
				retorno.add(varData);
				retorno.add(varDataBanco);
				retorno.add(varDataVencimento);
			}else if(listaTipoForContareceber().contains(tipolancamento)){
				retorno.add(varNumDocumento);
				retorno.add(varDescricaoConta);
				retorno.add(varRecebimentoDe);
				retorno.add(varParcela);
				retorno.add(varValor);
				retorno.add(varCheque);
				retorno.add(varData);
				retorno.add(varDataBanco);
				retorno.add(varDataVencimento);
			}else if(listaTipoForNotafiscal().contains(tipolancamento)){
				retorno.add(varNumDocumento);
				retorno.add(varNumeroNota);
				retorno.add(varCliente);
				retorno.add(varParcela);
				retorno.add(varValor);
				retorno.add(varData);				
			}else if(listaTipoForTransferenciainterna().contains(tipolancamento)){
				retorno.add(varDescricaoConta);
				retorno.add(varValor);
				retorno.add(varCheque);
				retorno.add(varData);
				retorno.add(varContaOrigem);
				retorno.add(varContaDestino);
				retorno.add(varNomeContaOrigem);
				retorno.add(varNomeContaDestino);
			}else if(listaTipoForTaxarecebimento().contains(tipolancamento)){
				retorno.add(varNumDocumento);
				retorno.add(varDescricaoConta);
				retorno.add(varRecebimentoDe);
				retorno.add(varParcela);
				retorno.add(varValor);
				retorno.add(varCheque);
				retorno.add(varData);
				retorno.add(varDataVencimento);
				retorno.add(varValorVinculado);
			}else if(listaTipoForRequisitoEntradaFiscal().contains(tipolancamento)){
				retorno.add(varNumDocumento);
				retorno.add(varFornecedor);
				retorno.add(varDataEntrada);
				retorno.add(varDataEmissao);
				retorno.add(varNumero);
				retorno.add(varValor);
				retorno.add(varDocumento);
				retorno.add(varSituacao);
			}
		}
		
		return retorno;
	}

	private String getDescricao(String str, HistoricooperacaoBean bean, Integer limite, String... variaveisNaoPreencher) {
		List<String> varNaoPreencher = new ArrayList<String>();
		for(String var: variaveisNaoPreencher){
			varNaoPreencher.add(var);
		}
		
		String string = str
				.replace(VARIAVEL_HISTORICO_NUMDOCUMENTO, varNaoPreencher.contains(VARIAVEL_HISTORICO_NUMDOCUMENTO)? VARIAVEL_HISTORICO_NUMDOCUMENTO: bean.getNumdocumentoReplace())
				.replace(VARIAVEL_HISTORICO_DESCRICAO, varNaoPreencher.contains(VARIAVEL_HISTORICO_DESCRICAO)? VARIAVEL_HISTORICO_DESCRICAO: bean.getDescricaoReplace())
				.replace(VARIAVEL_HISTORICO_PAGAMENTOA, varNaoPreencher.contains(VARIAVEL_HISTORICO_PAGAMENTOA)? VARIAVEL_HISTORICO_PAGAMENTOA: bean.getPagamentoaReplace())
				.replace(VARIAVEL_HISTORICO_CLIENTE, varNaoPreencher.contains(VARIAVEL_HISTORICO_CLIENTE)? VARIAVEL_HISTORICO_CLIENTE: bean.getClienteReplace())
				.replace(VARIAVEL_HISTORICO_FORNECEDOR, varNaoPreencher.contains(VARIAVEL_HISTORICO_FORNECEDOR)? VARIAVEL_HISTORICO_FORNECEDOR: bean.getFornecedorReplace())
				.replace(VARIAVEL_HISTORICO_PARCELA, varNaoPreencher.contains(VARIAVEL_HISTORICO_PARCELA)? VARIAVEL_HISTORICO_PARCELA: bean.getParcelaReplace())
				.replace(VARIAVEL_HISTORICO_CONTAORIGEM, varNaoPreencher.contains(VARIAVEL_HISTORICO_CONTAORIGEM)? VARIAVEL_HISTORICO_CONTAORIGEM: bean.getContaorigemReplace())
				.replace(VARIAVEL_HISTORICO_CONTADESTINO, varNaoPreencher.contains(VARIAVEL_HISTORICO_CONTADESTINO)? VARIAVEL_HISTORICO_CONTADESTINO: bean.getContadestinoReplace())
				.replace(VARIAVEL_HISTORICO_NOMECONTAORIGEM, varNaoPreencher.contains(VARIAVEL_HISTORICO_NOMECONTAORIGEM)? VARIAVEL_HISTORICO_NOMECONTAORIGEM: bean.getNomecontaorigemReplace())
				.replace(VARIAVEL_HISTORICO_NOMECONTADESTINO, varNaoPreencher.contains(VARIAVEL_HISTORICO_NOMECONTADESTINO)? VARIAVEL_HISTORICO_NOMECONTADESTINO: bean.getNomecontadestinoReplace())
				.replace(VARIAVEL_HISTORICO_VALORVINCULADO, varNaoPreencher.contains(VARIAVEL_HISTORICO_VALORVINCULADO)? VARIAVEL_HISTORICO_VALORVINCULADO: bean.getValorvinculadoReplace())
				.replace(VARIAVEL_HISTORICO_VALORDOCUMENTO, varNaoPreencher.contains(VARIAVEL_HISTORICO_VALORDOCUMENTO)? VARIAVEL_HISTORICO_VALORDOCUMENTO : bean.getValordocumentoReplace())
				.replace(VARIAVEL_HISTORICO_VALOR, varNaoPreencher.contains(VARIAVEL_HISTORICO_VALOR)? VARIAVEL_HISTORICO_VALOR: bean.getValorReplace())
				.replace(VARIAVEL_HISTORICO_DATAENTRADA, varNaoPreencher.contains(VARIAVEL_HISTORICO_DATAENTRADA)? VARIAVEL_HISTORICO_DATAENTRADA : bean.getDataentradaReplace())
				.replace(VARIAVEL_HISTORICO_DATAEMISSAO, varNaoPreencher.contains(VARIAVEL_HISTORICO_DATAEMISSAO)? VARIAVEL_HISTORICO_DATAEMISSAO : bean.getDataemissaoReplace())
				.replace(VARIAVEL_HISTORICO_DATABANCO, varNaoPreencher.contains(VARIAVEL_HISTORICO_DATABANCO)? VARIAVEL_HISTORICO_DATABANCO: bean.getDatabancoReplace())
				.replace(VARIAVEL_HISTORICO_DATAVENCIMENTO, varNaoPreencher.contains(VARIAVEL_HISTORICO_DATAVENCIMENTO)? VARIAVEL_HISTORICO_DATAVENCIMENTO: bean.getDatavencimentoReplace())
				.replace(VARIAVEL_HISTORICO_DATA, varNaoPreencher.contains(VARIAVEL_HISTORICO_DATA)? VARIAVEL_HISTORICO_DATA: bean.getDataReplace())
				.replace(VARIAVEL_HISTORICO_CHEQUE, varNaoPreencher.contains(VARIAVEL_HISTORICO_CHEQUE)? VARIAVEL_HISTORICO_CHEQUE: bean.getChequeReplace())
				.replace(VARIAVEL_HISTORICO_RECEBIMENTODE, varNaoPreencher.contains(VARIAVEL_HISTORICO_RECEBIMENTODE)? VARIAVEL_HISTORICO_RECEBIMENTODE: bean.getRecebimentodeReplace())
				.replace(VARIAVEL_HISTORICO_NUMERONOTA, varNaoPreencher.contains(VARIAVEL_HISTORICO_NUMERONOTA)? VARIAVEL_HISTORICO_NUMERONOTA: Util.strings.emptyIfNull(bean.getNumeronota()))
				.replace(VARIAVEL_HISTORICO_NUMERO, varNaoPreencher.contains(VARIAVEL_HISTORICO_NUMERO)? VARIAVEL_HISTORICO_NUMERO : bean.getNumeroReplace())
				.replace(VARIAVEL_HISTORICO_SITUACAO, varNaoPreencher.contains(VARIAVEL_HISTORICO_SITUACAO)? VARIAVEL_HISTORICO_SITUACAO : bean.getSituacaoReplace());
				
		return limite != null && limite < string.length() ? string.substring(0, limite) : string;
	}
	
	public void preencherDocumento(HistoricooperacaoBean bean, Documento documento, Documento docTemp){
		if(documento != null && bean != null){
			bean.setNumdocumento(documento.getNumero());
			bean.setDescricao(documento.getDescricao());
			bean.setPagamentoa(docTemp != null && docTemp.getTipopagamento() != null ? docTemp.getTipopagamento().getNome() : documento.getTipopagamento() != null ? documento.getTipopagamento().getNome() : null);
			bean.setCliente(docTemp != null && docTemp.getCliente() != null ? docTemp.getCliente().getNome() : documento.getCliente() != null ? documento.getCliente().getNome() : null);
			bean.setFornecedor(docTemp != null && docTemp.getFornecedor() != null ? docTemp.getFornecedor().getNome() : documento.getFornecedor() != null ? documento.getFornecedor().getNome() : null);
		}
	}
	
	public void preencherNota(HistoricooperacaoBean bean, List<Nota> listaNota){
		if(SinedUtil.isListNotEmpty(listaNota)){
			StringBuilder numeroNota = new StringBuilder();
			String numero = null;
			List<String> listaDtEmissao = new ArrayList<String>();
			String dtemissaoStr = null;
			for(Nota nota : listaNota){
				if(nota != null && nota.getCdNota() != null && bean != null){
					Nota notaConta = notaService.load(nota, "nota.cdNota, nota.dtEmissao, nota.numero, nota.notaTipo");
					if(notaConta != null){
						numero = null;
						if(notaConta.getDtEmissao() != null){
							dtemissaoStr = NeoFormater.getInstance().format(notaConta.getDtEmissao());
							if(!listaDtEmissao.contains(dtemissaoStr)){
								listaDtEmissao.add(dtemissaoStr);
							}
						}
						if(NotaTipo.NOTA_FISCAL_SERVICO.equals(notaConta.getNotaTipo())){
							Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nota);
							if(arquivonfnota != null){
								numero = notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse());
							}
						}
						if(StringUtils.isBlank(numero) && StringUtils.isNotBlank(notaConta.getNumero())){
							numero = notaConta.getNumero();
						}
						
						if(StringUtils.isNotBlank(numero)){
							numeroNota.append(numero).append(",");
						}
					}
				}
			}
			if(numeroNota.length() > 0){
				bean.setNumeronota(numeroNota.substring(0, numeroNota.length()-1));
			}
			if(listaDtEmissao.size() == 1){
				bean.setData(listaDtEmissao.get(0));
			}
		}
	}
	
	public void preencherMovimentofinanceiro(HistoricooperacaoBean bean, Movimentacao movimentacao, List<Documento> listadocumento, Documentoclasse documentoclasse){
		if(movimentacao != null && bean != null && listadocumento != null){
			List<String> listaDtVencimento = new ArrayList<String>();
			List<String> listaPagamentoA = new ArrayList<String>();
			List<String> listaRecebimentoDe = new ArrayList<String>();
			List<String> listaDescricao = new ArrayList<String>();
			List<String> listaNumDocumento = new ArrayList<String>();
			List<String> listaValorVinculado = new ArrayList<String>();
			List<String> listaParcelaDoc = new ArrayList<String>();

			for(Documento documento: listadocumento){
				if(documento.getDtvencimento() != null){
					String data = new SimpleDateFormat("dd/MM/yyyy").format(documento.getDtvencimento());
					if(!listaDtVencimento.contains(data))
						listaDtVencimento.add(data);
				}
				if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
					if(documento.getPagamentoa() != null && !listaPagamentoA.contains(documento.getPagamentoa()))
						listaPagamentoA.add(documento.getPagamentoa());					
				}else if(Documentoclasse.OBJ_RECEBER.equals(documentoclasse)){
					if(documento.getPagamentoa() != null && !listaRecebimentoDe.contains(documento.getPagamentoa()))
						listaRecebimentoDe.add(documento.getRecebimentode());
				}
				if(documento.getNumero() != null && !listaNumDocumento.contains(documento.getNumero()))
					listaNumDocumento.add(documento.getNumero());
							
				if(documento.getDescricao() != null && !listaDescricao.contains(documento.getDescricao()))
					listaDescricao.add(documento.getDescricao());
				
				if(documento.getNumParcela() != null)
					listaParcelaDoc.add(documento.getNumParcela());
				
				if(documento.getValor() != null)
					listaValorVinculado.add(documento.getValor().toString());
			}
			bean.setDatabanco(movimentacao.getDtbanco() != null? new SimpleDateFormat("dd/MM/yyyy").format(movimentacao.getDtbanco()): "");
			bean.setValor(movimentacao.getValor() != null? movimentacao.getValor().toString(): "");
			bean.setCheque(movimentacao.getCheque() != null? Util.strings.emptyIfNull(movimentacao.getCheque().getNumero()): "");
			bean.setData(movimentacao.getDtmovimentacao() != null? new SimpleDateFormat("dd/MM/yyyy").format(movimentacao.getDtmovimentacao()): "");

			if(movimentacao.getConta() != null){
				if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
					bean.setContaorigem(movimentacao.getConta().getNumero());
					bean.setNomecontaorigem(movimentacao.getConta().getDescricao());					
				}else{
					bean.setContadestino(movimentacao.getConta().getNumero());
					bean.setNomecontadestino(movimentacao.getConta().getDescricao());
				}
			}
			bean.setNumdocumento(CollectionsUtil.concatenate(listaNumDocumento, ","));
			bean.setDatavencimento(CollectionsUtil.concatenate(listaDtVencimento, ","));
			bean.setDescricao(CollectionsUtil.concatenate(listaDescricao, ","));
			bean.setPagamentoa(CollectionsUtil.concatenate(listaPagamentoA, ","));
			bean.setRecebimentode(CollectionsUtil.concatenate(listaRecebimentoDe, ","));
			bean.setValorvinculado(CollectionsUtil.concatenate(listaValorVinculado, ","));
			bean.setParcela(CollectionsUtil.concatenate(listaParcelaDoc, ","));
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean addDescricaoDocumentoByHistoricooperacao(Documento documento, Documento docTemp, List listaNota, String parcela){
		if(documento != null){
			Historicooperacao historicooperacao = findByTipo(Tipohistoricooperacao.EMISSAO_NF);
			if(historicooperacao != null && StringUtils.isNotBlank(historicooperacao.getHistorico())){
				HistoricooperacaoBean bean = new HistoricooperacaoBean();
				bean.setParcela(parcela != null ? parcela : null);
				preencherDocumento(bean, documento, docTemp);
				try {
					preencherNota(bean, (List<Nota>) listaNota);
				} catch (Exception e) {e.printStackTrace();}
				
				if(docTemp != null){
					docTemp.setDescricao(getDescricao(historicooperacao.getHistorico(), bean, 150));
					docTemp.setObservacao(null);
				}else {
					documento.setDescricao(getDescricao(historicooperacao.getHistorico(), bean, 150));
					documento.setObservacao(null);
				}
				return true;
			}
		}
		return false;
	}
	
	public boolean addDescricaoDocumentoByHistoricooperacao(Documento documento, Documento docTemp, Nota nota, String parcela){
		List<Nota> listaNota = null;
		if(nota != null){
			listaNota = new ArrayList<Nota>();
			listaNota.add(nota);
		}
		return addDescricaoDocumentoByHistoricooperacao(documento, docTemp, listaNota, parcela);
	}
	
	public boolean addDescricaoEntregaByHistoricooperacao(Documento documento, Entregadocumento entregadocumento, Entregapagamento entregapagamento) {
		if(entregadocumento != null){
			Historicooperacao historicooperacao = findByTipo(Tipohistoricooperacao.REGISTRO_ENTRADA_NF);
			if(historicooperacao != null && StringUtils.isNotBlank(historicooperacao.getHistorico())){
				HistoricooperacaoBean bean = new HistoricooperacaoBean();
				preencherDadosEntregaDocumento(bean, documento, entregadocumento, entregapagamento);
				documento.setDescricao(getDescricao(historicooperacao.getHistorico(), bean, 150));
			}
			return true;
		}
		return false;
	}
	
	private void preencherDadosEntregaDocumento(HistoricooperacaoBean bean, Documento documento, Entregadocumento entregadocumento, Entregapagamento entregapagamento) {
		bean.setNumdocumento(entregapagamento.getNumero() != null ? entregapagamento.getNumero() : "");
		bean.setFornecedor(entregadocumento.getFornecedor() != null ? entregadocumento.getFornecedor().getNome() : "");
		bean.setDataentrada(entregadocumento.getDtentradaStr() != null ? entregadocumento.getDtentradaStr() : "");
		bean.setDataemissao(entregadocumento.getDtemissaoStr() != null ? entregadocumento.getDtemissaoStr() : "");
		bean.setNumero(entregadocumento.getNumero() != null ? entregadocumento.getNumero() : "");
		bean.setValordocumento(documento.getValor() != null ? documento.getValor().toString() : "");
		bean.setValor(entregadocumento.getValor() != null ? entregadocumento.getValor().toString() : "");
		bean.setSituacao(entregadocumento.getEntregadocumentosituacao() != null ? Entregadocumentosituacao.FATURADA.getNome() : "");
		
	}
	public String findHistoricoByMovimentofinanceiro(Movimentacao movimentacao, List<Documento> listadocumento, Tipohistoricooperacao historicooperacaoTipo, Documentoclasse documentoclasse, String... variaveisNaoPreencher){
		if(listadocumento != null){
			if(historicooperacaoTipo != null){
				Historicooperacao historicooperacao = findByTipo(historicooperacaoTipo);
				if(historicooperacao != null && StringUtils.isNotBlank(historicooperacao.getHistorico())){
					HistoricooperacaoBean bean = new HistoricooperacaoBean();
					preencherMovimentofinanceiro(bean, movimentacao, listadocumento, documentoclasse);
					return getDescricao(historicooperacao.getHistorico(), bean, 150, variaveisNaoPreencher);
				}				
			}
		}
		return null;
	}

	public Tipohistoricooperacao findHistoricooperacaoTipoReceberByFormapagamento(Formapagamento formapagamento){
		if(formapagamento == null){
			return null;
		}
		return Formapagamento.CHEQUE.equals(formapagamento)? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CHEQUE:
					Formapagamento.CREDITOCONTACORRENTE.equals(formapagamento)? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CRED_EM_CONTA:
						Formapagamento.DINHEIRO.equals(formapagamento)? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_DINHEIRO:
							Formapagamento.CAIXA.equals(formapagamento)? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_DINHEIRO:
								Formapagamento.CARTAOCREDITO.equals(formapagamento)? Tipohistoricooperacao.CONTA_RECEBER_BAIXADA_CARTAO:
									null;
			
	}
	
	public ModelAndView findHistoricoByTransferenciaInterna(WebRequestContext request, GeraMovimentacao geraMovimentacao, Historicooperacao historicoOperacao){
		String strHistorico = Util.strings.emptyIfNull(historicoOperacao.getHistorico());
		String strHistoricoComplementar = Util.strings.emptyIfNull(historicoOperacao.getHistoricocomplementar());
		
		if((strHistorico.contains(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE) ||
			strHistoricoComplementar.contains(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE))){
			String whereInCheques = Util.strings.emptyIfNull(request.getParameter("whereInCheques"));
			String chequeStr = "";
			if(!whereInCheques.equals("")){
				List<Cheque> cheques = chequeService.findCheques(whereInCheques);
				chequeStr = CollectionsUtil.listAndConcatenate(cheques, "numero", " / ");
			}
			strHistorico = strHistorico.replace(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE, chequeStr);
			strHistoricoComplementar = strHistoricoComplementar.replace(HistoricooperacaoService.VARIAVEL_HISTORICO_CHEQUE, chequeStr);
		}
		Conta contaOrigem = geraMovimentacao.getMovimentacaoorigem().getConta() != null && geraMovimentacao.getMovimentacaoorigem().getConta().getCdconta() != null?
								contaService.loadForTransferenciaInterna(geraMovimentacao.getMovimentacaoorigem().getConta()): null;
		Conta contaDestino = geraMovimentacao.getMovimentacaodestino().getConta() != null && geraMovimentacao.getMovimentacaodestino().getConta().getCdconta() != null?
								contaService.loadForTransferenciaInterna(geraMovimentacao.getMovimentacaodestino().getConta()): null;
		
		HistoricooperacaoBean bean = new HistoricooperacaoBean();
		
		String numeroContaOrigem = (contaOrigem != null && contaOrigem.getNumero() != null) ? contaOrigem.getNumero(): "";
		String numeroContaDestino = (contaDestino != null && contaDestino.getNumero() != null) ? contaDestino.getNumero(): "";
		String nomeContaOrigem = (contaOrigem != null && contaOrigem.getNome() != null) ? contaOrigem.getNome(): "";
		String nomeContaDestino = (contaDestino != null && contaDestino.getNome() != null) ? contaDestino.getNome(): "";
		String descricaoOrigem = (contaOrigem != null && contaOrigem.getDescricao() != null) ? contaOrigem.getDescricao(): "";
		String dataMov = geraMovimentacao.getMovimentacaoorigem().getDataMovimentacao() != null? new SimpleDateFormat("dd/MM/yyyy").format(geraMovimentacao.getMovimentacaoorigem().getDataMovimentacao()): "";
		String valorMov = geraMovimentacao.getMovimentacaoorigem().getValor() != null? geraMovimentacao.getMovimentacaoorigem().getValor().toString(): "";
		
		bean.setContaorigem(numeroContaOrigem);
		bean.setContadestino(numeroContaDestino);
		bean.setNomecontaorigem(nomeContaOrigem);
		bean.setNomecontadestino(nomeContaDestino);
		bean.setDescricao(descricaoOrigem);
		bean.setData(dataMov);
		bean.setValor(valorMov);
		
		strHistorico = this.getDescricao(strHistorico, bean, 150);
		strHistoricoComplementar = this.getDescricao(strHistoricoComplementar, bean, 150);

		ModelAndView retorno = new JsonModelAndView()
										.addObject("historico", strHistorico)
										.addObject("historicocomplementar", strHistoricoComplementar);
		
		return retorno;
	}
	
	public boolean isPossuiTipo(Tipohistoricooperacao tipo){
		return historicooperacaoDAO.isPossuiTipo(tipo);
	}
}