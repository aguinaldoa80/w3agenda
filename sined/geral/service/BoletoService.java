package br.com.linkcom.sined.geral.service;

import java.io.IOException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Boleto;
import br.com.linkcom.sined.geral.dao.BoletoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.util.BoletoManager;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BoletoService extends GenericService<Boleto> {

	private BoletoDAO boletoDAO;
	
	public void setBoletoDAO(BoletoDAO boletoDAO) {
		this.boletoDAO = boletoDAO;
	}
	
	private static BoletoService instance;
	public static BoletoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(BoletoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que troca os c�digos pelos valores corretos do template
	 * 
	 * @param boletotemplate
	 * @param boletoBean
	 * @return
	 * @author Tom�s Rabelo
	 * @throws IOException 
	 */
	public String generateTemplate(Boleto boletotemplate, BoletoBean boletoBean) throws IOException {
		BoletoManager boletoManager = new BoletoManager(boletotemplate.getLayout());

		boletoManager.assign("codigoBanco", boletoBean.getCodigoBanco() != null ? boletoBean.getCodigoBanco() : "");
		boletoManager.assign("cdBanco", boletoBean.getCdBanco() != null ? boletoBean.getCdBanco() : "");
		boletoManager.assign("localPagamento", boletoBean.getLocalPagamento() != null ? boletoBean.getLocalPagamento() : "");
		boletoManager.assign("dtVencimento", boletoBean.getDtVencimento() != null ? boletoBean.getDtVencimento() : "");
		boletoManager.assign("cedente", boletoBean.getCedente() != null ? boletoBean.getCedente() : "");
		boletoManager.assign("agCdCedente", boletoBean.getAgCdCedente() != null ? boletoBean.getAgCdCedente() : "");
		boletoManager.assign("dtDocumento", boletoBean.getDtDocumento() != null ? boletoBean.getDtDocumento() : "");
		boletoManager.assign("numDocumento", boletoBean.getNumDocumento() != null ? boletoBean.getNumDocumento() : "");
		boletoManager.assign("espDocumento", boletoBean.getEspDocumento() != null ? boletoBean.getEspDocumento() : "");
		boletoManager.assign("aceite", boletoBean.getAceite() != null ? boletoBean.getAceite() : "");
		boletoManager.assign("dtProcessamento", boletoBean.getDtProcessamento() != null ? boletoBean.getDtProcessamento() : "");
		boletoManager.assign("nossoNumero", boletoBean.getNossoNumero() != null ? boletoBean.getNossoNumero() : "");
		boletoManager.assign("usoBanco", boletoBean.getUsoBanco() != null ? boletoBean.getUsoBanco() : "");
		boletoManager.assign("carteira", boletoBean.getCarteira() != null ? boletoBean.getCarteira() : "");
		boletoManager.assign("especie", boletoBean.getEspecie() != null ? boletoBean.getEspecie() : "");
		boletoManager.assign("valorDocumento", boletoBean.getValorDocumento() != null ? boletoBean.getValorDocumento() : "");
		boletoManager.assign("instrucoes1", boletoBean.getInstrucoes1() != null ? boletoBean.getInstrucoes1() : "");
		boletoManager.assign("instrucoes2", boletoBean.getInstrucoes2() != null ? boletoBean.getInstrucoes2() : "");
		boletoManager.assign("instrucoes3", boletoBean.getInstrucoes3() != null ? boletoBean.getInstrucoes3() : "");
		boletoManager.assign("instrucoes4", boletoBean.getInstrucoes4() != null ? boletoBean.getInstrucoes4() : "");
		boletoManager.assign("instrucoes5", boletoBean.getInstrucoes5() != null ? boletoBean.getInstrucoes5() : "");
		boletoManager.assign("instrucoes6", boletoBean.getInstrucoes6() != null ? boletoBean.getInstrucoes6() : "");
		boletoManager.assign("instrucoes7", boletoBean.getInstrucoes7() != null ? boletoBean.getInstrucoes7() : "");
		boletoManager.assign("desconto", boletoBean.getDesconto() != null ? boletoBean.getDesconto() : "");
		boletoManager.assign("outrasDeducoes", boletoBean.getOutrasDeducoes() != null ? boletoBean.getOutrasDeducoes() : "");
		boletoManager.assign("moraMulta", boletoBean.getMoraMulta() != null ? boletoBean.getMoraMulta() : "");
		boletoManager.assign("outrosAcrescimos", boletoBean.getOutrosAcrescimos() != null ? boletoBean.getOutrosAcrescimos() : "");
		boletoManager.assign("valorCobrado", boletoBean.getValorCobrado() != null ? boletoBean.getValorCobrado() : "");
		boletoManager.assign("nomeSacado", boletoBean.getNomeSacado() != null ? boletoBean.getNomeSacado() : "");
		boletoManager.assign("enderecoBairroSacado", boletoBean.getEnderecoBairroSacado() != null ? boletoBean.getEnderecoBairroSacado() : "");
		boletoManager.assign("cidadeEstadoCepSacado", boletoBean.getCidadeEstadoCepSacado() != null ? boletoBean.getCidadeEstadoCepSacado() : "");
		boletoManager.assign("caixaPostalSacado", boletoBean.getCaixaPostalSacado() != null ? boletoBean.getCaixaPostalSacado() : "");
		boletoManager.assign("cdBaixa", boletoBean.getCdBaixa() != null ? boletoBean.getCdBaixa() : "");
		boletoManager.assign("cpfCnpj", boletoBean.getCpfCnpj() != null ? boletoBean.getCpfCnpj() : "");
//		boletoManager.assign("numCodBarras", boletoBean.getNumCodBarras() != null ? boletoBean.getNumCodBarras() : "");
		boletoManager.assign("valor", boletoBean.getValor() != null ? boletoBean.getValor() : "");
		boletoManager.assign("quantidade", boletoBean.getQuantidade() != null ? boletoBean.getQuantidade() : "");
		boletoManager.assign("clienteIdentificador", boletoBean.getClienteIdentificador() != null ? boletoBean.getClienteIdentificador() : "");
		boletoManager.assign("contratoId", boletoBean.getContratoId() != null ? boletoBean.getContratoId() : "");
		boletoManager.assign("barcode", "<img src=\""+SinedUtil.getUrlWithContext()+"/BARCODE/"+boletoBean.getNumCodBarrasSemFormatacao()+"\"></img>");
		
		return boletoManager.getTemplate();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeBoletos() {
		return boletoDAO.existeBoletos();
	}

}
