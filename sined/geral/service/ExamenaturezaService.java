package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Examenatureza;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExamenaturezaService extends GenericService<Examenatureza> {	
	
	/* singleton */
	private static ExamenaturezaService instance;
	public static ExamenaturezaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExamenaturezaService.class);
		}
		return instance;
	}
	
}
