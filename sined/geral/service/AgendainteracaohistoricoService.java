package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.dao.AgendainteracaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AgendainteracaohistoricoService extends GenericService<Agendainteracaohistorico>{
	
	private AgendainteracaohistoricoDAO agendainteracaohistoricoDAO;

	public void setAgendainteracaohistoricoDAO(AgendainteracaohistoricoDAO agendainteracaohistoricoDAO) {
		this.agendainteracaohistoricoDAO = agendainteracaohistoricoDAO;
	}
	
	public List<Agendainteracaohistorico> findByAgendainteracao(Agendainteracao agendainteracao){
		return agendainteracaohistoricoDAO.findByAgendainteracao(agendainteracao);
	}
	
	public List<Agendainteracaohistorico> findForAndroidHistorico(String whereIn, String orderBy, boolean asc) {
		return agendainteracaohistoricoDAO.findForAndroidHistorico(whereIn, orderBy, asc);
	}
	
}
