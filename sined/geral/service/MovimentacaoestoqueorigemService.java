package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.dao.MovimentacaoestoqueorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoestoqueorigemService extends GenericService<Movimentacaoestoqueorigem> {

	private MovimentacaoestoqueorigemDAO movimentacaoestoqueorigemDAO;
	
	public void setMovimentacaoestoqueorigemDAO(
			MovimentacaoestoqueorigemDAO movimentacaoestoqueorigemDAO) {
		this.movimentacaoestoqueorigemDAO = movimentacaoestoqueorigemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeMovEstoqueComOrigemEntrega(Entrega entrega) {
		return movimentacaoestoqueorigemDAO.existeMovEstoqueComOrigemEntrega(entrega);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueorigemDAO#existeMovEstoqueComOrigemRequisicaomaterial(String whereIn, somenteOrigemRequisicao)
	*
	* @param whereIn
	* @param somenteOrigemRequisicao
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public Boolean existeMovEstoqueComOrigemRequisicaomaterial(String whereIn, Boolean somenteOrigemRequisicao){
		return movimentacaoestoqueorigemDAO.existeMovEstoqueComOrigemRequisicaomaterial(whereIn, somenteOrigemRequisicao);
	}
}
