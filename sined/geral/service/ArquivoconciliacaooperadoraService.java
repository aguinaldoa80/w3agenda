package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.dao.ArquivoconciliacaooperadoraDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivoconciliacaooperadoraFiltro;
import br.com.linkcom.sined.util.SinedException;

public class ArquivoconciliacaooperadoraService extends GenericService<Arquivoconciliacaooperadora> {
	
	private ArquivoconciliacaooperadoraDAO arquivoconciliacaooperadoraDAO;
	private MovimentacaoService movimentacaoService;
	
	private static ArquivoconciliacaooperadoraService instance;
	
	public void setArquivoconciliacaooperadoraDAO(ArquivoconciliacaooperadoraDAO arquivoconciliacaooperadoraDAO) {
		this.arquivoconciliacaooperadoraDAO = arquivoconciliacaooperadoraDAO;
	}
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	public Arquivoconciliacaooperadora findForAtulizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		return arquivoconciliacaooperadoraDAO.findForAtulizaStatusPagamento(arquivoconciliacaooperadora);
	}

	public void atualizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadoraVenda, Arquivoconciliacaooperadora arquivoconciliacaooperadoraPagamento){
		arquivoconciliacaooperadoraDAO.atualizaStatusPagamento(arquivoconciliacaooperadoraVenda, arquivoconciliacaooperadoraPagamento);
	} 
	
	public List<Arquivoconciliacaooperadora> findForCalculoValorTotalLiquidoVenda(ArquivoconciliacaooperadoraFiltro filtro){
	 return arquivoconciliacaooperadoraDAO.findForCalculoValorTotalLiquidoVenda(filtro);
	}
	
	public Arquivoconciliacaooperadora findArquivoForAssociacaoArquivoOperadora(Integer cdarquivoconciliacaooperadora){
		return arquivoconciliacaooperadoraDAO.findArquivoForAssociacaoArquivoOperadora(cdarquivoconciliacaooperadora);
	}
	
	public void associaArquivoConciliacaoAutomatica(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		arquivoconciliacaooperadoraDAO.associaArquivoConciliacaoAutomatica(arquivoconciliacaooperadora);
	}
	
	public Arquivoconciliacaooperadora findArquivoForConciliacaoArquivoOperadora(Integer cdarquivoconciliacaooperadora){
		return arquivoconciliacaooperadoraDAO.findArquivoForConciliacaoArquivoOperadora(cdarquivoconciliacaooperadora);
	}
	
	public static ArquivoconciliacaooperadoraService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(ArquivoconciliacaooperadoraService.class);
		}
		return instance;
	}
	
    public void conciliar(String cdarquivoconciliacaooperadora){
    	if (StringUtils.isBlank(cdarquivoconciliacaooperadora)) {
			throw new SinedException("Par�metro Inv�lido");
		}
		Arquivoconciliacaooperadora arquivoconciliacaooperadora = arquivoconciliacaooperadoraDAO.findArquivoForConciliacaoArquivoOperadora(Integer.valueOf(cdarquivoconciliacaooperadora));
		if (arquivoconciliacaooperadora != null 
				&& arquivoconciliacaooperadora.getDocumento() != null 
				&& arquivoconciliacaooperadora.getDocumento().getCddocumento() != null
				&& arquivoconciliacaooperadora.getDocumento().getDocumentoacao() != null
				&& arquivoconciliacaooperadora.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA)) {

			Movimentacao movimentacao = movimentacaoService.findByDocumento(arquivoconciliacaooperadora.getDocumento());
			
			if (movimentacao != null && movimentacao.getValor() != null && 
					arquivoconciliacaooperadora.getValorbruto() != null && 
					arquivoconciliacaooperadora.getValorbruto() >= movimentacao.getValor().getValue().doubleValue()) {
				if(arquivoconciliacaooperadora.getValorbruto() > movimentacao.getValor().getValue().doubleValue()){
					Double valorTaxaparcela = 0d;
					if(arquivoconciliacaooperadora.getTaxaparcela() != null && 
						arquivoconciliacaooperadora.getTaxaparcela().getTaxa() != null &&
						arquivoconciliacaooperadora.getTaxaparcela().getTaxa() > 0){
						valorTaxaparcela = arquivoconciliacaooperadora.getValorbruto() * arquivoconciliacaooperadora.getTaxaparcela().getTaxa() / 100;
					}
					movimentacao.setValor(new Money(arquivoconciliacaooperadora.getValorbruto() + valorTaxaparcela));
					Movimentacao mov = movimentacaoService.carregaMovimentacaoComRateio(movimentacao);
					if(mov != null){
						movimentacaoService.atualizaValorMovimentacaoERateio(mov, movimentacao.getValor());
					}
				}
				movimentacao.setDtbanco(arquivoconciliacaooperadora.getDataenviobanco());
				movimentacao.setHistorico("Concilia��o de pagamento do extrato do cart�o.");
				StringBuilder historicoMovimentacao = new StringBuilder("Referente ao extrato do cart�o ");
				historicoMovimentacao
				.append("<a href=\"javascript:visualizaArquivoConciliacaoOperadora(")
				.append(arquivoconciliacaooperadora.getCdarquivoconciliacaooperadora())
				.append(")\">")
				.append(arquivoconciliacaooperadora.getCdarquivoconciliacaooperadora())
				.append("</a>");
				movimentacaoService.salvaMovimentacaoConciliacao(movimentacao, historicoMovimentacao.toString());
			}
		}
    }
    
    public boolean isArquivoConciliacaoOperadoraPago (Integer cdarquivoconciliacaooperadora){
    	return arquivoconciliacaooperadoraDAO.isArquivoConciliacaoOperadoraPago(cdarquivoconciliacaooperadora);
    }
    
    public void associar(String cdarquivoconciliacaooperadora, String cddocumento){
    	if (StringUtils.isBlank(cdarquivoconciliacaooperadora) || StringUtils.isBlank(cddocumento)) {
    		throw new SinedException("Par�metro Inv�lido");
    	}
		Arquivoconciliacaooperadora arquivoconciliacaooperadora = new Arquivoconciliacaooperadora();
		Documento documento = new Documento();
		arquivoconciliacaooperadora.setCdarquivoconciliacaooperadora(Integer.parseInt(cdarquivoconciliacaooperadora));
		documento.setCddocumento(Integer.parseInt(cddocumento));
		arquivoconciliacaooperadora.setDocumento(documento);
		arquivoconciliacaooperadoraDAO.associaArquivoConciliacaoAutomatica(arquivoconciliacaooperadora);
		if (arquivoconciliacaooperadoraDAO.isArquivoConciliacaoOperadoraPago(arquivoconciliacaooperadora.getCdarquivoconciliacaooperadora())) {
			conciliar(cdarquivoconciliacaooperadora);
		}
    }

	public boolean haveArquivoconciliacaooperadoraByNsudoc(Arquivoconciliacaooperadora arquivoconciliacaooperadora) {
		return arquivoconciliacaooperadoraDAO.haveArquivoconciliacaooperadoraByNsudoc(arquivoconciliacaooperadora);
	}
}
