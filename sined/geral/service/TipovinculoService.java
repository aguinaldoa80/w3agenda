package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipovinculo;
import br.com.linkcom.sined.geral.dao.TipovinculoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipovinculoService extends GenericService<Tipovinculo> {

	private TipovinculoDAO tipoVinculoDAO;
	
	public void setTipoVinculoDAO(TipovinculoDAO tipoVinculoDAO) {
		this.tipoVinculoDAO = tipoVinculoDAO;
	}

	/**
	 * <p>M�todo de refer�ncia ao DAO. Recupera uma lista de Tipovinculo cujo
	 * Tipooperacao seja igual ao passado como par�metro.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TipovinculoDAO#buscarPor(Tipooperacao)
	 * @param tipoOperacao
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Tipovinculo> buscarPor(Tipooperacao tipoOperacao) {
		return tipoVinculoDAO.buscarPor(tipoOperacao);
	}
}
