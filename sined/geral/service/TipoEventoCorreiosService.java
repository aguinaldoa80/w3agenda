package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
import br.com.linkcom.sined.geral.dao.TipoEventoCorreiosDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoEventoCorreiosService extends GenericService<TipoEventoCorreios>{

	private TipoEventoCorreiosDAO tipoEventoCorreiosDAO;
	private static TipoEventoCorreiosService instance;
	
	public static TipoEventoCorreiosService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipoEventoCorreiosService.class);
		}
		return instance;
	}
	
	public void setTipoEventoCorreiosDAO(
			TipoEventoCorreiosDAO tipoEventoCorreiosDAO) {
		this.tipoEventoCorreiosDAO = tipoEventoCorreiosDAO;
	}
	
	public TipoEventoCorreios findByNome(String nome){
		return tipoEventoCorreiosDAO.findByNome(nome);
	}
}
