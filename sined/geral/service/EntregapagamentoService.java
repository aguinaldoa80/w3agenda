package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.dao.EntregapagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregapagamentoService extends GenericService<Entregapagamento> {

	private EntregapagamentoDAO entregapagamentoDAO;
	
	public void setEntregapagamentoDAO(EntregapagamentoDAO entregapagamentoDAO) {
		this.entregapagamentoDAO = entregapagamentoDAO;
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregapagamentoDAO#findByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @since 07/12/2011
	 * @author Rodrigo Freitas
	 */
	public List<Entregapagamento> findByEntregadocumento(Entregadocumento entregadocumento) {
		return entregapagamentoDAO.findByEntregadocumento(entregadocumento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		entregapagamentoDAO.deleteAllFromEntregaNotInWhereIn(cdentrega, whereNotIn);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregapagamentoDAO#getTotalpagamento(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public Money getTotalpagamento(String whereIn) {
		return entregapagamentoDAO.getTotalpagamento(whereIn);
	}
}
