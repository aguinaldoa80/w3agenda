package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.Ecom_PedidoVendaDAO;
import br.com.linkcom.sined.util.SinedUtil;

public class Ecom_PedidoVendaService extends GenericService<Ecom_PedidoVenda>{

	private Ecom_PedidoVendaDAO ecom_PedidoVendaDAO;
	private Ecom_PedidoVendaPagamentoService ecom_PedidoVendaPagamentoService;
	private Ecom_ClienteService ecom_ClienteService;
	private Ecom_PedidoVendaSituacaoService ecom_PedidoVendaSituacaoService;
	
	
	private static Ecom_PedidoVendaService instance;
	public static Ecom_PedidoVendaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(Ecom_PedidoVendaService.class);
		}
		return instance;
	}
	public void setEcom_ClienteService(Ecom_ClienteService ecom_ClienteService) {
		this.ecom_ClienteService = ecom_ClienteService;
	}
	
	public void setEcom_PedidoVendaDAO(Ecom_PedidoVendaDAO ecom_PedidoVendaDAO) {
		this.ecom_PedidoVendaDAO = ecom_PedidoVendaDAO;
	}
	public void setEcom_PedidoVendaSituacaoService(Ecom_PedidoVendaSituacaoService ecom_PedidoVendaSituacaoService) {
		this.ecom_PedidoVendaSituacaoService = ecom_PedidoVendaSituacaoService;
	}
	public void setEcom_PedidoVendaPagamentoService(Ecom_PedidoVendaPagamentoService ecom_PedidoVendaPagamentoService) {
		this.ecom_PedidoVendaPagamentoService = ecom_PedidoVendaPagamentoService;
	}
	
	public List<Ecom_PedidoVenda> findForCriarPedido(){
		List<Ecom_PedidoVenda> lista = ecom_PedidoVendaDAO.findForCriarPedido();
		for(Ecom_PedidoVenda pedido: lista){
			pedido.setPedidoCliente(ecom_ClienteService.findForPedido(pedido.getPedidoCliente()).get(0));
			pedido.setPedidoSituacoes(ecom_PedidoVendaSituacaoService.findByEcomPedidoVenda(pedido));
			pedido.setPagamentos(ecom_PedidoVendaPagamentoService.findByPedidoVenda(pedido));
		}
		return lista;
	}
	
	public void processarPedidosPendentes(){
		
	}
	public void updateProcessado(Integer idEcomPedidoVenda, Integer cdpedidovenda){
		ecom_PedidoVendaDAO.updateProcessado(idEcomPedidoVenda, cdpedidovenda);
	}
	
	public Ecom_PedidoVenda findWithPedidoCriado(Ecom_PedidoVenda ecom_PedidoVenda){
		return ecom_PedidoVendaDAO.findWithPedidoCriado(ecom_PedidoVenda);
	}
	
	public Ecom_PedidoVenda loadByCdPedidoVenda(Integer cdpedidovenda) {
		return ecom_PedidoVendaDAO.loadByCdPedidoVenda(cdpedidovenda);
	}
	
	public void updateRastreamento(Ecom_PedidoVenda bean) {
		ecom_PedidoVendaDAO.updateRastreamento(bean);
	}
	public void updateRastreamentoExpedicao(Ecom_PedidoVenda bean) {
		ecom_PedidoVendaDAO.updateRastreamentoExpedicao(bean);
	}
	public void insertTabelaSincronizacaForRastreamento(Ecom_PedidoVenda bean) {
		ecom_PedidoVendaDAO.insertTabelaSincronizacaForRastreamento(bean);
	}
	public void insertTabelaSincronizacaForRastreamentoForExpedicao(Ecom_PedidoVenda bean) {
		ecom_PedidoVendaDAO.insertTabelaSincronizacaForRastreamentoForExpedicao(bean);
	}
	public Ecom_PedidoVenda loadByVenda(Venda venda){
		return ecom_PedidoVendaDAO.loadByVenda(venda);
	}
	public void updateCampo(Ecom_PedidoVenda ecom_PedidoVenda, String nomeCampo, Object valor) {
		ecom_PedidoVendaDAO.updateCampo(ecom_PedidoVenda, nomeCampo, valor);
	}
	public List<Ecom_PedidoVenda> findByWhereInPedidovenda(String whereInPedidovenda) {
		return ecom_PedidoVendaDAO.findByWhereInPedidovenda(whereInPedidovenda);
	}
	public List<Ecom_PedidoVenda> findByWhereInVenda(String whereInVenda) {
		return ecom_PedidoVendaDAO.findByWhereInVenda(whereInVenda);
	}
	
	public List<Ecom_PedidoVenda> findWithContasEmAberto(){
		return ecom_PedidoVendaDAO.findWithContasEmAberto();
	}
	public boolean isPedidoEcompletoPago(Pedidovenda pedidovenda){
		Ecom_PedidoVenda ecom_PedidoVenda = this.loadByCdPedidoVenda(pedidovenda.getCdpedidovenda());
		return ecom_PedidoVenda != null && ecom_PedidoVenda.getId_situacao() != null && SinedUtil.STATUS_PAGO_ECOMPLETO.equals(ecom_PedidoVenda.getId_situacao().toString());
	}
}
