package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorexame;
import br.com.linkcom.sined.geral.dao.ColaboradorexameDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorexameService extends GenericService<Colaboradorexame> {	

	private ColaboradorexameDAO colaboradorexameDAO;
	
	public void setColaboradorexameDAO(ColaboradorexameDAO colaboradorexameDAO) {
		this.colaboradorexameDAO = colaboradorexameDAO;
	}

	/* singleton */
	private static ColaboradorexameService instance;
	public static ColaboradorexameService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorexameService.class);
		}
		return instance;
	}
	
	public List<Colaboradorexame> findExamesByColaborador(Colaborador colaborador){
		return colaboradorexameDAO.findExamesByColaborador(colaborador);
	}
	
}
