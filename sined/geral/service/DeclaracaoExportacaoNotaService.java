package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoNota;
import br.com.linkcom.sined.geral.dao.DeclaracaoExportacaoNotaDAO;

public class DeclaracaoExportacaoNotaService extends GenericService<DeclaracaoExportacaoNota> {

	private DeclaracaoExportacaoNotaDAO declaracaoExportacaoNotaDAO;	
	public void setDeclaracaoExportacaoNotaDAO(DeclaracaoExportacaoNotaDAO declaracaoExportacaoNotaDAO) {this.declaracaoExportacaoNotaDAO = declaracaoExportacaoNotaDAO;}
	
	
	public List<DeclaracaoExportacaoNota> buscarNotasVinculadas(String whereInNotas){
		return declaracaoExportacaoNotaDAO.buscarNotasVinculadas(whereInNotas);
	}
	
}
