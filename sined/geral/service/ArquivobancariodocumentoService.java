package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.dao.ArquivobancariodocumentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivobancariodocumentoService extends GenericService<Arquivobancariodocumento> {

	private ArquivobancariodocumentoDAO arquivobancariodocumentoDAO;
	
	public void setArquivobancariodocumentoDAO(ArquivobancariodocumentoDAO arquivobancariodocumentoDAO) {
		this.arquivobancariodocumentoDAO = arquivobancariodocumentoDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListaForArquivoBancario
	 * @param arquivobancario
	 * @return
	 * @return List<Arquivobancariodocumento>
	 * @author Thiago Augusto
	 * @date 07/05/2012
	 *
	 */
	public List<Arquivobancariodocumento> findListaForArquivoBancario(Arquivobancario arquivobancario){
		return arquivobancariodocumentoDAO.findListaForArquivoBancario(arquivobancario);
	}
	
	/**
	 * 
	 * M�todo que busca documentos que ainda n�o foram baixados
	 *
	 * @name findListaForArquivoBancario
	 * @param arquivobancario
	 * @return
	 * @return List<Arquivobancariodocumento>
	 * @author Marden Silva
	 * @date 30/07/2103
	 *
	 */
	public List<Arquivobancariodocumento> findDocumentosNaoBaixado(Arquivobancario arquivobancario, String whereIn){
		return arquivobancariodocumentoDAO.findDocumentosNaoBaixado(arquivobancario, whereIn);
	}
	
	public Boolean existeProcessamentoArquivoRetorno(Documento documento){
		return arquivobancariodocumentoDAO.existeProcessamentoArquivoRetorno(documento);
	}
	
	public void saveProcessamentoArquivoRetorno(Arquivobancario arquivobancario, Documento documento){
		if (arquivobancario!=null && documento!=null){
			this.saveOrUpdate(new Arquivobancariodocumento(arquivobancario, documento));
		}
	}
}
