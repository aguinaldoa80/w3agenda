package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.UfDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.UfOfflineJSON;
import br.com.linkcom.sined.util.rest.android.uf.UfRESTModel;

public class UfService extends GenericService<Uf> {

	private UfDAO ufDAO;
	
	public void setUfDAO(UfDAO ufDAO) {
		this.ufDAO = ufDAO;
	}
	
	/**
	 * Carrega todos os UF para exibi��o no combo do flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Uf> findForComboFlex(){
		return this.findAll();
	}	
	
	/* singleton */
	private static UfService instance;
	public static UfService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UfService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca a UF pela sigla.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UfDAO#findBySigla(String sigla)
	 *
	 * @param sigla
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Uf findBySigla(String sigla) {
		return ufDAO.findBySigla(sigla);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForUf
	 * @param uf
	 * @return
	 * @return Uf
	 * @author Thiago Augusto
	 * @date 16/04/2012
	 *
	 */
	public Uf findForCodigo(Integer cdibge){
		return ufDAO.findForCodigo(cdibge);
	}

	

	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<UfOfflineJSON> findForPVOffline() {
		List<UfOfflineJSON> lista = new ArrayList<UfOfflineJSON>();
		for(Uf uf : ufDAO.findForPVOffline())
			lista.add(new UfOfflineJSON(uf));
		
		return lista;
	}
	
	public List<UfRESTModel> findForAndroid() {
		return findForAndroid(null, true); 
	}
	
	public List<UfRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<UfRESTModel> lista = new ArrayList<UfRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Uf m : ufDAO.findForAndroid(whereIn))
				lista.add(new UfRESTModel(m));
		}
		
		return lista;
	}

}
