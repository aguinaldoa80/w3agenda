package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.lkbanco.boleto.BoletoBancario;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.BoletoCobrancaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ContacorrenteDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.GerarboletohomologacaoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.Modulos;

public class ContacorrenteService extends GenericService<Conta>{

	private ContacorrenteDAO contacorrenteDAO;
	private BancoService bancoService;
	private ParametrogeralService parametrogeralService;
	private ContareceberService contareceberService;
	private ContacarteiraService contacarteiraService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private ContaService contaService;
	
	public void setContacorrenteDAO(ContacorrenteDAO contacorrenteDAO) {
		this.contacorrenteDAO = contacorrenteDAO;
	}

	public void setBancoService(BancoService bancoService) {
		this.bancoService = bancoService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {
		this.contacarteiraService = contacarteiraService;
	}
	
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
	@Override
	public Conta loadForEntrada(Conta bean) {
		Conta conta = super.loadForEntrada(bean);
		conta.setDescricaobancaria(conta.getNome());
		return conta;
	}
	
	@Override
	public void saveOrUpdate(Conta bean) {
		// AJUSTA O NOME DO BANCO PARA A ORDENA��O NA LISTAGEM
		Banco banco = bancoService.load(bean.getBanco());
		bean.setBanco(banco);
		if(bean.getDescricaobancaria() == null || "".equals(bean.getDescricaobancaria())){
			bean.setNome(bean.getDescricao());
		}else{
			bean.setNome(bean.getDescricaobancaria());
		}
		bean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
		super.saveOrUpdate(bean);
	}
	
	/**
	 * M�todo para verificar se uma conta corrente pode ser inserida.
	 * Ou seja, se possui banco, agencia e numero em outra conta corrente j� cadastrada.
	 *
	 * @see #findTotalContasIguais(Conta)
	 * @param conta
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean validateContacorrente(Conta conta){
		Long count = this.findTotalContasIguais(conta);
		return count==0;
	}
	
	/**
	 * M�todo para obter o n�mero de contas corrente iguais.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContacorrenteDAO#findTotalContasIguais(Contacorrente)
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Long findTotalContasIguais(Conta conta){
		return contacorrenteDAO.findTotalContasIguais(conta);
	}
	
	public Conta findByAgenciaContaBanco(String cdbanco, String cdagencia, String cdcedente){
		return contacorrenteDAO.findByAgenciaContaBanco(cdbanco, cdagencia, cdcedente);
	}
	
	public Conta findByAgenciaNumeroBanco(String cdbanco, String cdagencia, String cdconta) {
		return contacorrenteDAO.findByAgenciaNumeroBanco(cdbanco, cdagencia, cdconta);
	}
	
	public Conta findForRetorno(String cdbanco, String cdagencia, String cdconta) {
		return contacorrenteDAO.findForRetorno(cdbanco, cdagencia, cdconta);
	}
	
	/* singleton */
	private static ContacorrenteService instance;
	public static ContacorrenteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContacorrenteService.class);
		}
		return instance;
	}
	
	public Money getValorTaxaBoleto(Conta conta){
		return contacorrenteDAO.getValorTaxaBoleto(conta);
	}
	
	/**
	 * M�todo gera as contas a receber para homologa��o do boleto
	 *
	 * @param gerarboletohomologacaoBean
	 * @author Luiz Fernando
	 */
	public void gerarBoletoHomologacao(GerarboletohomologacaoBean gerarboletohomologacaoBean) throws Exception{
		gerarboletohomologacaoBean.setConta(this.load(gerarboletohomologacaoBean.getConta()));
		gerarboletohomologacaoBean.setContacarteira(contacarteiraService.load(gerarboletohomologacaoBean.getContacarteira()));
		
		String sicob11 = parametrogeralService.getValorPorNome("LAYOUT_BOLETO_CAIXA");
		BoletoCobrancaEnum boletoCobrancaEnum = null;
		if(gerarboletohomologacaoBean.getContacarteira() != null){
			boletoCobrancaEnum = gerarboletohomologacaoBean.getContacarteira().getBoletocobranca();
		}
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Documento documento = null;
		Taxa taxa = null;
		Taxaitem taxaitem = null;
		Set<Taxaitem> listaTaxaitem = null;
		Integer numero;
		boolean geracaoimcompleta = true;
		BoletoBancario bb = null;
		
		Date data = new Date(System.currentTimeMillis());
		if(gerarboletohomologacaoBean.getDtemissao() != null){
			data = new Date(gerarboletohomologacaoBean.getDtemissao().getTime());
		}
		int qtdedias = 1;
		for(int i = 0; i < 20; i++){
			documento = new Documento();
			//varia��o da data de vencimento
			qtdedias = qtdedias + ((int) (Math.random() * 20));
			data = SinedDateUtils.incrementaDia(data, qtdedias, null);
			documento.setDtvencimento(data);
			
			//varia��o do valor
			documento.setValor(new Money(((Double) (Math.random() * 5000))));
			documento.setValorboleto(documento.getValor());
			
			//varia��o do nosso n�mero
			numero = ((int) (Math.random() * 1000));
			documento.setCddocumento(numero);
			documento.setNumero(numero.toString());
			documento.setNossonumero(numero.toString());
			
			documento.setEmpresa(gerarboletohomologacaoBean.getEmpresa());
			documento.setCliente(gerarboletohomologacaoBean.getCliente());
			documento.setPessoa(gerarboletohomologacaoBean.getCliente());
			documento.setEndereco(gerarboletohomologacaoBean.getEndereco());
			documento.setRateio(new Rateio(gerarboletohomologacaoBean.getRateio()));
			rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
			documento.setConta(gerarboletohomologacaoBean.getConta());
			documento.setContacarteira(gerarboletohomologacaoBean.getContacarteira());
			if(gerarboletohomologacaoBean.getDtemissao() != null){
				documento.setDtemissao(new Date(gerarboletohomologacaoBean.getDtemissao().getTime()));
			}else {
				documento.setDtemissao(new Date(System.currentTimeMillis()));
			}
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setTipopagamento(Tipopagamento.CLIENTE);
			documento.setDocumentotipo(Documentotipo.BOLETO);
			documento.setDocumentoacao(Documentoacao.PREVISTA);
			documento.setMensagem1(gerarboletohomologacaoBean.getContacarteira().getMsgboleto1());
			documento.setMensagem2(gerarboletohomologacaoBean.getContacarteira().getMsgboleto2());
			
			listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
			if(gerarboletohomologacaoBean.getValorjuros() != null && gerarboletohomologacaoBean.getValorjuros().getValue().doubleValue() > 0){
				taxaitem = new Taxaitem();
				if(gerarboletohomologacaoBean.isPercentualJuros()){
					taxaitem.setValor(gerarboletohomologacaoBean.getValor().multiply(gerarboletohomologacaoBean.getValorjuros()).divide(new Money(100)));
				}else {
					taxaitem.setValor(gerarboletohomologacaoBean.getValorjuros());
				}
				taxaitem.setPercentual(gerarboletohomologacaoBean.isPercentualJuros());
				taxaitem.setTipotaxa(Tipotaxa.JUROS);
				taxaitem.setDtlimite(documento.getDtvencimento());
				taxaitem.setGerarmensagem(true);
				
				listaTaxaitem.add(taxaitem);
			}
			if(gerarboletohomologacaoBean.getValordesconto() != null && gerarboletohomologacaoBean.getValordesconto().getValue().doubleValue() > 0){
				if(gerarboletohomologacaoBean.getValordesconto().getValue().doubleValue() < documento.getValor().getValue().doubleValue()){
					taxaitem = new Taxaitem();
					if(gerarboletohomologacaoBean.isPercentualDesconto()){
						taxaitem.setValor(gerarboletohomologacaoBean.getValor().multiply(gerarboletohomologacaoBean.getValordesconto()).divide(new Money(100)));
					}else {
						taxaitem.setValor(gerarboletohomologacaoBean.getValordesconto());
					}
					taxaitem.setPercentual(gerarboletohomologacaoBean.isPercentualDesconto());
					taxaitem.setTipotaxa(Tipotaxa.DESCONTO);
					taxaitem.setDtlimite(documento.getDtvencimento());
					taxaitem.setGerarmensagem(true);
					
					listaTaxaitem.add(taxaitem);
				}
			}
			if(gerarboletohomologacaoBean.getValormulta() != null && gerarboletohomologacaoBean.getValormulta().getValue().doubleValue() > 0){
				taxaitem = new Taxaitem();
				if(gerarboletohomologacaoBean.isPercentualMulta()){
					taxaitem.setValor(gerarboletohomologacaoBean.getValor().multiply(gerarboletohomologacaoBean.getValormulta()).divide(new Money(100)));
				}else {
					taxaitem.setValor(gerarboletohomologacaoBean.getValormulta());
				}
				taxaitem.setTipotaxa(Tipotaxa.MULTA);
				taxaitem.setPercentual(gerarboletohomologacaoBean.isPercentualMulta());
				taxaitem.setDtlimite(documento.getDtvencimento());
				taxaitem.setGerarmensagem(true);
				
				listaTaxaitem.add(taxaitem);
			}
			
			if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
				taxa = new Taxa();
				taxa.setListaTaxaitem(listaTaxaitem);
				documento.setTaxa(taxa);
			}
			
			if(gerarboletohomologacaoBean.getDescricao() != null && !"".equals(gerarboletohomologacaoBean.getDescricao())){
				documento.setDescricao(gerarboletohomologacaoBean.getDescricao());
			}else {
				documento.setDescricao("Documento gerado para homologa��o (teste)");
			}
			
			listaDocumento.add(documento);
		}
		
		int indexDocumento = 0;
//		String paramBoletoCaixa = ParametrogeralService.getInstance().getValorPorNome("LAYOUT_BOLETO_CAIXA");
		String parametroSantander = parametrogeralService.buscaValorPorNome(Parametrogeral.NOVO_BOLETO_SANTANDER);
		String cdBanco = "";
		boolean fatorvencimentozerado = false;
		int countDVNossonumero = 0;
		int countDVLinhadigitavel = 0;
		int countDVCampolivre = 0;
		String dvNossonumero = null;
		String dvLinhadigitavel = null;
		String dvCampolivre = null;
		List<String> listaDVNossonumero = new ArrayList<String>();
		List<String> listaDVLinhadigitavel = new ArrayList<String>();
		List<String> listaDVCampolivre = new ArrayList<String>();
		boolean achou = false;
		int contadorgeral = 0;
		do {	
			fatorvencimentozerado = listaDocumento.get(indexDocumento).getContacarteira().getFatorvencimentozerado() != null ? documento.getContacarteira().getFatorvencimentozerado() : false;
			bb = contareceberService.criaBoletoBancario(listaDocumento.get(indexDocumento), cdBanco, fatorvencimentozerado, parametroSantander);
			
			if(bb == null) throw new SinedException("Identificador da Empresa no Banco inv�lido.");
			
			dvNossonumero = getDVNossonumero(bb);
			dvLinhadigitavel = getDVGeralLinhaDigitavel(bb);
			dvCampolivre = getDVGeralCampolivreLinhaDigitavel(bb);
			
			if(dvNossonumero != null && (!existDigito(dvNossonumero, listaDVNossonumero) || (countDVNossonumero > 8 && existDigitosvalidos(listaDVNossonumero)))){
				if(countDVNossonumero < 10){
					countDVNossonumero++;
					listaDVNossonumero.add(dvNossonumero);
					achou = true;
				}
				if(dvLinhadigitavel != null && !existDigito(dvLinhadigitavel, listaDVLinhadigitavel)){
					countDVLinhadigitavel++;
					listaDVLinhadigitavel.add(dvLinhadigitavel);
					achou = true;
				}
				if(dvCampolivre != null && !existDigito(dvCampolivre, listaDVCampolivre)){
					countDVCampolivre++;
					listaDVCampolivre.add(dvCampolivre);
					achou = true;
				}
				
				if(!achou){
					if(countDVNossonumero > 8 && countDVLinhadigitavel > 7 && countDVCampolivre <= 9){
						numero = ((int) (Math.random() * 1000));
						listaDocumento.get(indexDocumento).setNumero(numero.toString());
						listaDocumento.get(indexDocumento).setCddocumento(numero);
						listaDocumento.get(indexDocumento).setNossonumero(numero.toString());
					}
					listaDocumento.get(indexDocumento).setValor(new Money(((Double) (Math.random() * 5000))));
					listaDocumento.get(indexDocumento).setValorboleto(listaDocumento.get(indexDocumento).getValor());
					listaDocumento.get(indexDocumento).setDtvencimento(SinedDateUtils.incrementaDia(new Date(System.currentTimeMillis()), ((int) (Math.random() * 100)), null));
				}else {
					indexDocumento++;
				}
				
			}else {
				numero = ((int) (Math.random() * 1000));
				listaDocumento.get(indexDocumento).setNumero(numero.toString());
				listaDocumento.get(indexDocumento).setCddocumento(numero);
				listaDocumento.get(indexDocumento).setNossonumero(numero.toString());
			}
			
			achou = false;
			//linha digitavel n�o cont�m 0
			if(countDVNossonumero > 8 && countDVLinhadigitavel > 8 && (countDVCampolivre > 9 || ("sicob_11".equalsIgnoreCase(sicob11) ||
					(boletoCobrancaEnum != null && (BoletoCobrancaEnum.SICOB_11.equals(boletoCobrancaEnum) ||
							BoletoCobrancaEnum.SICOB_11CR.equals(boletoCobrancaEnum)))))){ 
				geracaoimcompleta = false;
			}
			
			if(contadorgeral > 500){
				throw new SinedException("N�o foi poss�vel gerar a sequen�ncia necess�ria (o n�mero de tentativa ultrapassou o limite). Favor gerar novamente.");
			}
			contadorgeral++;
			
		}while(geracaoimcompleta);
		
//		if(SinedUtil.isAmbienteDesenvolvimento()){
//			NeoWeb.getRequestContext().addMessage("DV Nosso numero: " + listaDVNossonumero, MessageType.INFO);
//			NeoWeb.getRequestContext().addMessage("DV Campo livre: " + listaDVLinhadigitavel, MessageType.INFO);
//			NeoWeb.getRequestContext().addMessage("DV Geral: " + listaDVCampolivre, MessageType.INFO);
//		}
		
		//salva os documentos
		for(int i = 0; i < indexDocumento; i++){
			listaDocumento.get(i).setCddocumento(null);
			contareceberService.saveOrUpdate(listaDocumento.get(i));
		}
		
	}
	
	/**
	 * verifica se existe todos os d�gitos necess�rios do nosso n�mero para homologa��o (d�gito 0 s� � valido para alguns bancos)
	 *
	 * @param listaDV
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean existDigitosvalidos(List<String> listaDV) {
		boolean exist = false;
		Integer i = 1;
		if(listaDV != null && !listaDV.isEmpty()){
			for(; i <= 9; i++){
				exist = false;
				for(String digito : listaDV){
					if (digito.equals(i.toString())){
						exist = true;
						break;
					}
				}
				if(!exist) break;
			}
		}
		return exist;
	}

	/**
	 * verifica se existe o d�gito na lista, se exite retorna true, sen�o false
	 *
	 * @param dv
	 * @param listaDV
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean existDigito(String dv, List<String> listaDV) {
		boolean exist = false;
		if(listaDV != null && !listaDV.isEmpty()){
			for(String digito : listaDV){
				if(digito.equals(dv)){
					exist = true;
					break;
				}
			}
		}
		return exist;
	}
	
	/**
	 * retorna o D�gito Verificador do Campo Livre da Linha Digit�vel
	 *
	 * @param bb
	 * @return
	 * @author Luiz Fernando
	 */
	private String getDVGeralCampolivreLinhaDigitavel(BoletoBancario bb){
		String dv = null;
		dv = bb.getCdBarraSemDV().substring(33,43);
//		dv = String.valueOf(Modulos.modulo10(dv));
		dv = dv.substring(dv.length()-1);
		return dv;
	}
	
	/**
	 * retorna o D�gito Verificador Geral da Linha Digit�vel
	 *
	 * @param bb
	 * @return
	 * @author Luiz Fernando
	 */
	private String getDVGeralLinhaDigitavel(BoletoBancario bb){
		String dv = null;
		dv = String.valueOf(Modulos.modulo11(bb.getCdBarraSemDV(),1));
		return dv;
	}
	
	/**
	 * retorna o D�gito Verificador do nosso n�mero
	 *
	 * @param bb
	 * @return
	 * @author Luiz Fernando
	 */
	private String getDVNossonumero(BoletoBancario bb){
		String dvnossonumero = null;
		if(bb != null && bb.getNossoNumero() != null && !"".equals(bb.getNossoNumero())){
			dvnossonumero = bb.getNossoNumero().substring(bb.getNossoNumero().indexOf("-")+1);
		}
		return dvnossonumero;
	}
	
	public String Modulo11Bradesco(String numero) {
		int indexador;
		int soma      = 0;
		int mult      = 0;
		String result = "";		
		int tamanho = numero.length();
	
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 6) + 2; //Sequ�ncia de 2 a 7
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = String.valueOf(11 - (soma % 11));
		if (result.equals("10")) result = "P";
		else if (result.equals("11")) result = "0";			
		return result;
	} //Modulo11	
	
	public String modulo11(String numero) {
		int indexador;
		int soma    = 0;
		int mult    = 0;
		int result  = 0;		
		int tamanho = numero.length();
		
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = 9 - (indexador % 8); // De 9 a 2.
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = soma % 11;
		
		if (result == 10) {
			return "X"; 
		}
		else {
			return Integer.toString(result);
		}
	}

	/**
	 * M�todo que busca a taxa da conta e calcula a data limite de acordo com a data da parcela
	 *
	 * @param contaboleto
	 * @param dataparcela
	 * @return
	 * @author Luiz Fernando
	 */
	public Set<Taxaitem> getTaxas(Conta contaboleto, Date dataparcela, Endereco endereco) {
		Set<Taxaitem> listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
		
		if(contaboleto != null && contaboleto.getCdconta() != null){
			Conta bean = contaService.loadWithTaxa(contaboleto);
			if(bean != null && bean.getTaxa() != null && bean.getTaxa().getListaTaxaitem() != null && !bean.getTaxa().getListaTaxaitem().isEmpty()){
				if(dataparcela != null){
					for(Taxaitem taxaitem : bean.getTaxa().getListaTaxaitem()){
						if(taxaitem.getDias() != null){
							if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
								taxaitem.setDtlimite(SinedDateUtils.addDiasData(dataparcela, -taxaitem.getDias()));
							}else {
								taxaitem.setDtlimite(SinedDateUtils.addDiasData(dataparcela, taxaitem.getDias()));
							}
						}
					}
				}
				
				listaTaxaitem.addAll(bean.getTaxa().getListaTaxaitem());
				taxaService.limpaReferenciasTaxaitem(listaTaxaitem);
			}
		}
		
		return listaTaxaitem;
	}

	/**
	* M�todo que retorna o nome do cedente. Caso exista empresa titular na conta, o cedente � a empresa titular.
	*
	* @param conta
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	public String getNomeCedente(Conta conta) {
		String nomeCedente = null;
		Empresa empresa = conta.getEmpresa();
		
		if(empresa != null && conta.getEmpresatitular() != null && 
				!empresa.equals(conta.getEmpresatitular())){
			empresa = conta.getEmpresatitular();
		}
		
		if(empresa != null){
			if(StringUtils.isNotBlank(empresa.getRazaosocialOuNome())){
				nomeCedente = empresa.getRazaosocialOuNome().toUpperCase();
			}else {
				nomeCedente = empresa.getNome().toUpperCase();
			}
		}
		return nomeCedente;
	}
	
	public void preencherTaxasByConta(List<Taxaitem> listaTaxaitem, Conta bean) {
		if(bean != null && bean.getCdconta() != null){
			Conta conta = contaService.loadWithTaxa(bean);
			if(conta != null && conta.getTaxa() != null && SinedUtil.isListNotEmpty(conta.getTaxa().getListaTaxaitem())){
				for(Taxaitem taxaitemAux : conta.getTaxa().getListaTaxaitem()){
					boolean adicionar = true;
					if(SinedUtil.isListNotEmpty(listaTaxaitem)){
						for(Taxaitem taxaitem : listaTaxaitem){
							if(taxaitem.getTipotaxa() != null && taxaitemAux.getTipotaxa().equals(taxaitem.getTipotaxa())){
								adicionar = false;
								break;
							}
						}
					}
					
					if(adicionar){
						taxaitemAux.setCdconta(conta.getCdconta());
						listaTaxaitem.add(taxaitemAux);
					}
				}
			}
		}
	}
	
	/**
	 * Carregar a conta com os dados necess�rios para fazer a atualiza��o da Taxa e mensagens do
	 * boleto da conta a receber.
	 * @param contacorrente - conta a ser carregada.
	 * @return conta com taxa e itens da taxa.
	 */
	public Conta loadContaAlterContaReceberLote(Conta contacorrente) {
		return contacorrenteDAO.loadContaAlterContaReceberLote(contacorrente);
	}
}
