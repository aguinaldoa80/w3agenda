package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CfopescopoService extends GenericService<Cfopescopo>{
	
	private EnderecoService enderecoService;
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	public Cfopescopo loadByEndereco(Endereco endereco1, Endereco endereco2){
		
		Uf uf1 = null;
		Uf uf2 = null;
		
		if (endereco1!=null && endereco1.getCdendereco()!=null){
			endereco1 = enderecoService.carregaEnderecoComUfPais(endereco1);
			if (endereco1.getMunicipio()!=null){
				uf1 = endereco1.getMunicipio().getUf();
			}
		}
		
		if (endereco2!=null){
			if(endereco2.getCdendereco() != null){
				endereco2 = enderecoService.carregaEnderecoComUfPais(endereco2);
			}
			if (endereco2 != null && endereco2.getMunicipio()!=null){
				uf2 = endereco2.getMunicipio().getUf();
			}
		}
		
		Cfopescopo cfopescopo = null;
		if (uf1!=null && uf2!=null){
			if (uf1.getCduf().equals(uf2.getCduf())){
				cfopescopo = Cfopescopo.ESTADUAL;
			}else {
				cfopescopo = Cfopescopo.FORA_DO_ESTADO;
			}
		}
		
		return cfopescopo;
	}	
}