package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CotacaofornecedorService extends GenericService<Cotacaofornecedor> {

	private CotacaofornecedorDAO cotacaofornecedorDAO;
	private CotacaofornecedoritemService cotacaofornecedoritemService;
	private LocalarmazenagemService localarmazenagemService;
	private EnderecoService enderecoService;
	
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setCotacaofornecedoritemService(
			CotacaofornecedoritemService cotacaofornecedoritemService) {
		this.cotacaofornecedoritemService = cotacaofornecedoritemService;
	}
	public void setCotacaofornecedorDAO(CotacaofornecedorDAO cotacaofornecedorDAO) {
		this.cotacaofornecedorDAO = cotacaofornecedorDAO;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	@Override
	public void saveOrUpdate(Cotacaofornecedor bean) {
		List<Cotacaofornecedoritem> listaCotacaofornecedoritem = bean.getListaCotacaofornecedoritem();
		bean.setListaCotacaofornecedoritem(null);
		
		super.saveOrUpdate(bean);
		
		String string = "";
		for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaofornecedoritem) {
			if (cotacaofornecedoritem.getCdcotacaofornecedoritem() != null) {
				string += cotacaofornecedoritem.getCdcotacaofornecedoritem();
				string += ",";
			}
		}
		
		if (!string.equals("")) {
			string = string.substring(0, string.length()-1);
			cotacaofornecedoritemService.deleteNaoExistentes(bean,string);
		}
		
		for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaofornecedoritem) {
			cotacaofornecedoritem.setCotacaofornecedor(bean);
			cotacaofornecedoritemService.saveOrUpdateNoUseTransaction(cotacaofornecedoritem);
		}
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Cotacaofornecedor bean) {
		List<Cotacaofornecedoritem> listaCotacaofornecedoritem = bean.getListaCotacaofornecedoritem();
		bean.setListaCotacaofornecedoritem(null);
		
		super.saveOrUpdateNoUseTransaction(bean);
		
		String string = "";
		for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaofornecedoritem) {
			if (cotacaofornecedoritem.getCdcotacaofornecedoritem() != null) {
				string += cotacaofornecedoritem.getCdcotacaofornecedoritem();
				string += ",";
			}
		}
		
		if (!string.equals("")) {
			string = string.substring(0, string.length()-1);
			cotacaofornecedoritemService.deleteNaoExistentes(bean,string);
		}
		
		for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaofornecedoritem) {
			cotacaofornecedoritem.setCotacaofornecedor(bean);
			cotacaofornecedoritemService.saveOrUpdateNoUseTransaction(cotacaofornecedoritem);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO#deleteNaoExistentes
	 * @param bean
	 * @param whereNotIn
	 * @author Rodrigo Freitas
	 */
	public void deleteNaoExistentes(Cotacao bean, String whereNotIn) {
		cotacaofornecedorDAO.deleteNaoExistentes(bean,whereNotIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO#findFornCotacoes
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaofornecedor> findFornCotacoes(String whereIn) {
		return cotacaofornecedorDAO.findFornCotacoes(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO#findFornecedorCotacao
	 * @param where
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Cotacaofornecedor findFornecedorCotacao(String where) {
		Cotacaofornecedor cotacaofornecedor = cotacaofornecedorDAO.findFornecedorCotacao(where);
		if(cotacaofornecedor.getFornecedor() != null && cotacaofornecedor.getFornecedor().getCdpessoa() != null){
			cotacaofornecedor.getFornecedor().setListaEndereco(SinedUtil.listToSet(enderecoService.findByFornecedor(cotacaofornecedor.getFornecedor()), Endereco.class));
		}
		return cotacaofornecedor;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO#findWithLista
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaofornecedor> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return cotacaofornecedorDAO.loadWithLista(whereIn, orderBy, asc);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void doUpdateConviteEnviado(final String whereIn) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				cotacaofornecedorDAO.doUpdateConviteEnviado(whereIn);
				return null;
			}
		});
	}

	/**
	 * Gera o arquivo xls para enviar ao fornecedor.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeRelatorio
	 * 
	 * @param cotacaofornecedor
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoCotacaoExcelFormatado(Cotacaofornecedor cotacaofornecedor) throws IOException {
		
		SinedExcel e = new SinedExcel();
		
		HSSFSheet p = e.createSheet("Cota��o");
		p.getPrintSetup().setLandscape(true);
		
		this.makeRelatorio(cotacaofornecedor, p);
		
		return e.getWorkBookResource("cotacaofornecedor_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * M�todo que agrupa todas as fun��es para a gera��o do relat�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCabecalhoRel
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCabecalhoDataGrid
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedoritemService#findByCotacaofornecedor
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeLinhaDetalhe
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCondicoesPag
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeLocaisEntrega
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeObservacoes
	 * 
	 * @param cotacaofornecedor
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void makeRelatorio(Cotacaofornecedor cotacaofornecedor, HSSFSheet p) {
		p.setDefaultColumnWidth((short) 8);
		p.setDisplayGridlines(false);
		
//		SETANDO OS TAMANHOS DAS COLUNAS
		p.setColumnWidth((short) 0, (short) (5*35));
		p.setColumnWidth((short) 1, (short) (50*35));
		p.setColumnWidth((short) 2, (short) (130*35));
		p.setColumnWidth((short) 3, (short) (40*35));
		p.setColumnWidth((short) 4, (short) (130*35));
		
		p.setColumnWidth((short) 5, (short) (40*35));
		p.setColumnWidth((short) 6, (short) (50*35));
		p.setColumnWidth((short) 7, (short) (65*35));
		p.setColumnWidth((short) 8, (short) (50*35));
		p.setColumnWidth((short) 9, (short) (40*35));
		
		p.setColumnWidth((short) 10, (short) (130*35));
		p.setColumnWidth((short) 11, (short) (65*35));
		p.setColumnWidth((short) 12, (short) (90*35));
		
		this.makeCabecalhoRel(p);
		
		this.makeCabecalhoDataGrid(p);
		
		List<Cotacaofornecedoritem> lista = cotacaofornecedoritemService.findByCotacaofornecedor(cotacaofornecedor);
		List<Localarmazenagem> listaLocais = new ArrayList<Localarmazenagem>();
		
		int i = 0;
		for (i = 0; i < lista.size(); i++) {
			this.makeLinhaDetalhe(p, lista, listaLocais, i);
		}
		
		Integer abaixo = 2;
		
		abaixo = this.makeCondicoesPag(p, i, abaixo);
		
		abaixo = this.makeLocaisEntrega(p, listaLocais, i, abaixo);
		
		this.makeObservacoes(p, i, abaixo);
	}
	
	/**
	 * Cria a parte abaixo do relat�rio, especificamente a parte de OBSERVA��ES.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCellWithStyle
	 * 
	 * @param p
	 * @param i
	 * @param abaixo
	 * @author Rodrigo Freitas
	 */
	private void makeObservacoes(HSSFSheet p, int i, Integer abaixo) {
		abaixo += 2;
		
		HSSFRow row = p.createRow(i + 3 + abaixo);
		row.setHeightInPoints(20);
		
		p.addMergedRegion(new Region(i + 3 + abaixo,(short)1, i + 3 + abaixo,(short)11));
		
		HSSFCell cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_FILTRO);
		cell.setCellValue("OBSERVA��ES");
		
		this.makeCellWithStyle(row, 2, 11, SinedExcel.STYLE_FILTRO);
		
		abaixo += 1;
		
		row = p.createRow(i + 3 + abaixo);
		row.setHeightInPoints(20);
		
		p.addMergedRegion(new Region(i + 3 + abaixo,(short)2, i + 3 + abaixo,(short)11));
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_RESUMO);
		cell.setCellValue("espa�o reservado para o fornecedor");
		
		this.makeCellWithStyle(row, 3, 11, SinedExcel.STYLE_RESUMO);
	}

	/**
	 * Cria a parte abaixo do relat�rio, especificamente a parte de LOCAIS DE ENTREGA.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCellWithStyle
	 * @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#loadWithEndereco
	 * 
	 * @param p
	 * @param listaLocais
	 * @param i
	 * @param abaixo
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Integer makeLocaisEntrega(HSSFSheet p, List<Localarmazenagem> listaLocais, int i, Integer abaixo) {
		abaixo += 2;
		
		HSSFRow row = p.createRow(i + 3 + abaixo);
		row.setHeightInPoints(20);
		
		p.addMergedRegion(new Region(i + 3 + abaixo,(short)1, i + 3 + abaixo,(short)11));
		
		HSSFCell cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_FILTRO);
		cell.setCellValue("ENDERE�O DE ENTREGA");
		
		this.makeCellWithStyle(row, 2, 11, SinedExcel.STYLE_FILTRO);
		
		listaLocais = localarmazenagemService.loadWithEndereco(CollectionsUtil.listAndConcatenate(listaLocais, "cdlocalarmazenagem", ","));
		
		for (int j = 0; j < listaLocais.size(); j++) {
			abaixo += 1;
			
			row = p.createRow(i + 3 + abaixo);
			row.setHeightInPoints(20);
			
			p.addMergedRegion(new Region(i + 3 + abaixo,(short)2, i + 3 + abaixo,(short)11));
			
			cell = row.createCell((short)2);
			cell.setCellStyle(SinedExcel.STYLE_RESUMO);
			cell.setCellValue(listaLocais.get(j).getNome() + ": " + listaLocais.get(j).getEndereco().getDescricaoCombo());
			
			this.makeCellWithStyle(row, 3, 11, SinedExcel.STYLE_RESUMO);
		}
		
		return abaixo;
	}
	/**
	 * Cria a parte abaixo do relat�rio, especificamente a parte de CONDI�OES DE PAGAMENTO.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCellWithStyle
	 * 
	 * @param p
	 * @param i
	 * @param abaixo
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Integer makeCondicoesPag(HSSFSheet p, int i, Integer abaixo) {
		HSSFRow row = p.createRow(i + 3 + abaixo);
		row.setHeightInPoints(20);
		
		p.addMergedRegion(new Region(i + 3 + abaixo,(short)1, i + 3 + abaixo,(short)11));
		
		HSSFCell cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_FILTRO);
		cell.setCellValue("INFORMAR");
		
		this.makeCellWithStyle(row, 2, 11, SinedExcel.STYLE_FILTRO);
		
		abaixo += 1;
		
		row = p.createRow(i + 3 + abaixo);
		row.setHeightInPoints(20);
		
		p.addMergedRegion(new Region(i + 3 + abaixo,(short)2, i + 3 + abaixo,(short)11));
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_RESUMO);
		cell.setCellValue("Condi��es de pagamento:");
		
		this.makeCellWithStyle(row, 3, 11, SinedExcel.STYLE_RESUMO);
		
		return abaixo;
	}
	
	/**
	 * Cria uma linha no detalhe do relatorio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCellWithStyle
	 * 
	 * @param p
	 * @param lista
	 * @param listaLocais
	 * @param i
	 * @author Rodrigo Freitas
	 */
	private void makeLinhaDetalhe(HSSFSheet p, List<Cotacaofornecedoritem> lista, List<Localarmazenagem> listaLocais, int i) {
		HSSFRow row = p.createRow(i+3);
		row.setHeightInPoints(40);
		
		Cotacaofornecedoritem it = lista.get(i);
		
		listaLocais.add(it.getLocalarmazenagem());
		
		HSSFCell cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(i+1);
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(it.getMaterial().getIdentificacao()+" - "+it.getMaterial().getNome());
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(it.getMaterial().getUnidademedida().getSimbolo());
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(it.getObservacao());
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(it.getQtdesol());
		
		this.makeCellWithStyle(row, 6, 9, SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(it.getLocalarmazenagem().getNome());
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		
		cell = row.createCell((short)12);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
	}
	
	/**
	 * Cria o cabe�alho do data grid.
	 *
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void makeCabecalhoDataGrid(HSSFSheet p) {
		HSSFRow row = p.createRow(2);
		row.setHeightInPoints(60);
		
		HSSFCell cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("ITEM");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("DESCRI��O");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("U.M.");
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("OBSERVA��O");
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("QTD");
		
		cell = row.createCell((short)6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("VR. UNIT. R$");
		
		cell = row.createCell((short)7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("VR. TOTAL R$");
		
		cell = row.createCell((short)8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("ICMS");
		
		cell = row.createCell((short)9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("IPI");
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("LOCAL DE ENTREGA");
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("FRETE R$");
		
		cell = row.createCell((short)12);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("PRAZO ENTREGA");
	}
	
	/**
	 * Cria o cabe�alho do relat�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#makeCellWithStyle
	 * 
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void makeCabecalhoRel(HSSFSheet p) {
		p.addMergedRegion(new Region(1,(short)1, 1,(short)12));
		
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow(1);
		row.setHeightInPoints(40);
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("MATERIAIS SOLICITADOS");
		
		this.makeCellWithStyle(row, 2, 12, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
	}
	
	/**
	 * Cria as c�lulas de uma linha com o mesmo style.
	 *
	 * @param row
	 * @param colFrom - A partir desta coluna
	 * @param colTo - At� esta coluna, inclusive.
	 * @param style - Estilo a ser atribuido �s colunas.
	 * @author Rodrigo Freitas
	 */
	private void makeCellWithStyle(HSSFRow row, Integer colFrom, Integer colTo, HSSFCellStyle style) {
		HSSFCell cell;
		for (int i = colFrom; i <= colTo; i++) {
			cell = row.createCell((short)i);
			cell.setCellStyle(style);
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @author Tom�s Rabelo
	 * @param cotacaofornecedor 
	 */
	public void updatePrazoPagamento(Cotacaofornecedor cotacaofornecedor) {
		cotacaofornecedorDAO.updatePrazoPagamento(cotacaofornecedor);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os 3 �ltimos fornecedores utilizados na cota��o
	*
	* @see	br.com.linkcom.sined.geral.dao.CotacaofornecedorDAO#find3FornecedoresMaisUsadosByMaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Nov 4, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Fornecedor> find3FornecedoresMaisUsadosByMaterial(String whereIn){
		return cotacaofornecedorDAO.find3FornecedoresMaisUsadosByMaterial(whereIn);
	}
}
