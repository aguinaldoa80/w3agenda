package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioitem;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorescala;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorescalaSituacao;
import br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorescalaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorescalaService extends GenericService<Colaboradorescala> {

	private ColaboradorService colaboradorService;
	private ColaboradorescalaDAO colaboradorescalaDAO;
	private ContratocolaboradorService contratocolaboradorService;
	private CalendarioService calendarioService;
	private ApontamentoService apontamentoService;
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setContratocolaboradorService(ContratocolaboradorService contratocolaboradorService) {this.contratocolaboradorService = contratocolaboradorService;}
	public void setColaboradorescalaDAO(ColaboradorescalaDAO colaboradorescalaDAO) {this.colaboradorescalaDAO = colaboradorescalaDAO;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	
	
	/**
	 * Cria o relat�rio de listagem da escala do colaborador.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorescalaService#findForReport
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createRelatorioListagem(ColaboradorescalaFiltro filtro) {
		Report report = new Report("/rh/colaboradorescala");
		
		filtro.setOrderBy(null);
		
		List<Colaboradorescala> listaReport = this.findForReport(filtro);
		report.setDataSource(listaReport);
		
		report.addParameter("COLABORADOR", colaboradorService.load(filtro.getColaborador(), "colaborador.nome"));
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtescala1(), filtro.getDtescala2()));
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#findForReport
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaboradorescala> findForReport(ColaboradorescalaFiltro filtro){
		return colaboradorescalaDAO.findForReport(filtro);
	}


	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#atualizaSituacao
	 *  
	 * @param load
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void atualizaSituacao(Colaboradorescala load, ColaboradorescalaSituacao situacao) {
		colaboradorescalaDAO.atualizaSituacao(load, situacao);
	}
	
	/**
	 * Carrega o bean <code>Colaboradorescala</code> e retorna o colaborador.
	 *
	 * @param colaboradorescala
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Colaborador getColaborador(Colaboradorescala colaboradorescala) {
		return this.loadForEntrada(colaboradorescala).getColaborador();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#getBeanFalta
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Colaboradorescala getBeanFalta(Colaboradorescala bean) {
		return colaboradorescalaDAO.getBeanFalta(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#deletaSubstitutos
	 * 
	 * @param colaboradorescala
	 * @author Rodrigo Freitas
	 */
	public void deletaSubstitutos(Colaboradorescala colaboradorescala) {
		colaboradorescalaDAO.deletaSubstitutos(colaboradorescala);
	}
	
	/**
	 * M�todo que faz a gera��o das escalas e atualiza��o das j� existentes.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratocolaboradorService#findByColaboradorContrato
	 * @see br.com.linkcom.sined.geral.service.ColaboradorescalaService#geraEscalas
	 * @see br.com.linkcom.sined.geral.service.ColaboradorescalaService#findByColaboradorData
	 * @see br.com.linkcom.sined.geral.service.ColaboradorescalaService#atualizaHorasRealizadas
	 * 
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void gerarEscala(ColaboradorescalaFiltro filtro) {
		
		List<Colaboradorescala> listaEscala = new ArrayList<Colaboradorescala>();
		List<Contratocolaborador> listaPrevisao  = contratocolaboradorService.findByColaboradorContrato(filtro.getColaborador(), filtro.getContrato());

		for (Contratocolaborador cc : listaPrevisao) {
			listaEscala.addAll(this.geraEscalas(cc, filtro.getDtescala1(), filtro.getDtescala2()));
		}
		
		List<Colaboradorescala> listaEscalaNova = this.findByColaboradorData(filtro.getColaborador(), filtro.getDtescala1(), filtro.getDtescala2(), filtro.getContrato());
		
		for (Colaboradorescala ce : listaEscala) {
			if(!containsEscala(ce, listaEscalaNova)){
				listaEscalaNova.add(ce);
			}
		}
		
		this.atualizaHorasRealizadas(listaEscalaNova);
		
		for (Colaboradorescala colaboradorescala : listaEscalaNova) {
			this.saveOrUpdate(colaboradorescala);
		}
	}
	
	/**
	 * Atualiza as horas realizadas de uma lista de Escala do colaborador
	 * 
	 * @see br.com.linkcom.sined.geral.service.ApontamentoService#getTotalHoras
	 * 
	 * @param listaEscalaNova
	 * @author Rodrigo Freitas
	 */
	private void atualizaHorasRealizadas(List<Colaboradorescala> listaEscalaNova) {
		
		for (Colaboradorescala colaboradorescala : listaEscalaNova) {
			colaboradorescala.setHorarealizada(apontamentoService.getTotalHoras(colaboradorescala));
		}
		
	}
	/**
	 * Veirifica se cont�m a escala na lista de Escalas do colaborador
	 *
	 * @param ce
	 * @param listaEscalaNova
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean containsEscala(Colaboradorescala ce, List<Colaboradorescala> listaEscalaNova){
		
		boolean achou = false;
		for (Colaboradorescala ce2 : listaEscalaNova) {
			if(ce2.getColaborador().getCdpessoa().equals(ce.getColaborador().getCdpessoa()) &&
					ce2.getContrato().getCdcontrato().equals(ce.getContrato().getCdcontrato()) &&
					SinedDateUtils.equalsIgnoreHour(ce2.getDtescala(), ce.getDtescala())){
				achou = true;
			}
		}
		return achou;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#findByColaboradorData
	 * 
	 * @param colaborador
	 * @param dtescala1
	 * @param dtescala2
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Colaboradorescala> findByColaboradorData(Colaborador colaborador, Date dtescala1, Date dtescala2, Contrato contrato) {
		return colaboradorescalaDAO.findByColaboradorData(colaborador, dtescala1, dtescala2, contrato);
	}
	/**
	 * Gera as escalas de acordo com as datas e o <code>Contratocolaborador</code> que foi passado por par�metro.
	 *
	 * @param cc
	 * @param dtescala1
	 * @param dtescala2
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Colaboradorescala> geraEscalas(Contratocolaborador cc, Date dtescala1, Date dtescala2) {
		
		Escala escala = cc.getEscala();
		Contrato contrato = cc.getContrato();
		Colaborador colaborador = cc.getColaborador();
		
		if(cc.getHorainicio() == null && cc.getHorafim() == null) throw new SinedException("Contrato " + contrato.getCdcontrato() + " com hora in�cio/fim n�o cadastrada para o colaborador selecionado.");
		if(cc.getHorainicio() == null) throw new SinedException("Contrato " + contrato.getCdcontrato() + " com hora in�cio n�o cadastrada para o colaborador selecionado.");
		if(cc.getHorafim() == null) throw new SinedException("Contrato " + contrato.getCdcontrato() + " com hora fim n�o cadastrada para o colaborador selecionado.");
		
		int numTrabalho = escala.getDiastrabalho();
		int numFolga = escala.getDiasfolga();
		
		Empresa empresa = contrato != null ? contrato.getEmpresa() : null;
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		List<Calendario> listaCalendario = null;
		if(cc.getEscala().getNaotrabalhaferiado()){
			listaCalendario = calendarioService.findCalendarioGeral(uf, municipio);
		}
		
		boolean trabalho = true;
		
		Date data = dtescala1;
		
		Colaboradorescala ce = null;
		List<Colaboradorescala> lista = new ArrayList<Colaboradorescala>();
		
		while(SinedDateUtils.beforeIgnoreHour(data, dtescala2) || SinedDateUtils.equalsIgnoreHour(data, dtescala2)){
						
			if(this.validaGeracao(cc, data, listaCalendario)){
				
				ce = new Colaboradorescala();
				
				ce.setColaborador(colaborador);
				ce.setContrato(contrato);
				ce.setDtescala(data);
				ce.setHorainicio(cc.getHorainicio());
				ce.setHorafim(cc.getHorafim());
				ce.setHorarealizada(0.0);
				
				if(trabalho){
					ce.setSituacao(ColaboradorescalaSituacao.TRABALHO);
					numTrabalho--;
					if(numTrabalho == 0){
						numTrabalho = escala.getDiastrabalho();
						trabalho = false;
					}
				} else {
					ce.setSituacao(ColaboradorescalaSituacao.FOLGA);
					numFolga--;
					if(numFolga == 0){
						numFolga = escala.getDiasfolga();
						trabalho = true;
					}
				}
				
				lista.add(ce);
			}
			
			data = SinedDateUtils.incrementDate(data, 1, Calendar.DAY_OF_MONTH);
		}
		
		return lista;
	}
	
	/**
	 * Faz a valida��o se vai ser gerada ou n�o a escala.
	 *
	 * @param cc
	 * @param data
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean validaGeracao(Contratocolaborador cc, Date data, List<Calendario> listaCalendario) {
		boolean valido = true;
		
		// VERIFICA SE A DATA QUE SER� GERADA A ESCALA EST� DEPOIS OU NO DIA DO IN�CIO DO COLABORADOR NO CONTRATO
		if (cc.getDtinicio() != null && !SinedDateUtils.beforeIgnoreHour(cc.getDtinicio(), data) && !SinedDateUtils.equalsIgnoreHour(cc.getDtinicio(), data)){
			valido = false;
		}
		
		// VERIFICA SE A DATA QUE SER� GERADA A ESCALA EST� ANTES OU NO DIA DO T�RMINO DO COLABORADOR NO CONTRATO
		if(cc.getDtfim() != null && !SinedDateUtils.afterIgnoreHour(cc.getDtfim(), data) && !SinedDateUtils.equalsIgnoreHour(cc.getDtfim(), data)){
			valido = false;
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		
		// VERIFICA SE O COLABORADOR N�O TRABALHA NOS DOMINGOS
		if(cc.getEscala().getNaotrabalhadomingo() && calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			valido = false;
		}
		
		// VERIFICA SE O COLABORADOR N�O TRABALHA NOS FERIADOS
		if(cc.getEscala().getNaotrabalhaferiado() && listaCalendario != null){
			List<Calendarioitem> lista = new ArrayList<Calendarioitem>();
			for (Calendario c : listaCalendario) {
				lista.addAll(c.getListaCalendarioitem());
			}
			if(lista != null && lista.size() > 0){
				if(SinedDateUtils.verificaContains(calendar, lista)){
					valido = false;
				}
			}
		}
		
		
		return valido;
	}
	
	/**
	 *  Faz a totaliza��o das horas das escalas do colaborador.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorescalaDAO#totalizarListagem
	 *
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void totalizarListagem(ColaboradorescalaFiltro filtro) {
		
		if(filtro.getColaborador() != null && filtro.getDtescala1() != null && filtro.getDtescala2() != null){
			colaboradorescalaDAO.totalizarListagem(filtro);
		} else {
			filtro.setHoraPrevista(0.0);
			filtro.setHoraRealizada(0.0);
		}
		
	}

}
