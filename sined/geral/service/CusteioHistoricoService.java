package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.CusteioHistorico;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.dao.CusteioHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CusteioHistoricoService extends GenericService<CusteioHistorico> {

	private static CusteioHistoricoService instance;
	private CusteioHistoricoDAO custeioHistoricoDAO;

	public void setCusteioHistoricoDAO(CusteioHistoricoDAO custeioHistoricoDAO) {this.custeioHistoricoDAO = custeioHistoricoDAO;}
	public static CusteioHistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CusteioHistoricoService.class);
		}
		return instance;
	}
	public List<CusteioHistorico> findByCusteioProcessado(CusteioProcessado bean) {
		return custeioHistoricoDAO.findByCusteioProcessado(bean);
	}

}
