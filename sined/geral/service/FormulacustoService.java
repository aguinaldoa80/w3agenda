package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Formulacusto;
import br.com.linkcom.sined.geral.dao.FormulacustoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormulacustoService extends GenericService<Formulacusto> {
	
	private FormulacustoDAO formulacustoDAO;

	public void setFormulacustoDAO(FormulacustoDAO formulacustoDAO) {
		this.formulacustoDAO = formulacustoDAO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.FormulacustoDAO#existsFormulaAtiva(Formulacusto formulacusto)
	*
	* @param formulacusto
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public boolean existsFormulaAtiva(Formulacusto formulacusto){
		return formulacustoDAO.existsFormulaAtiva(formulacusto);
	}
	
	/**
	* M�todo que verifica se existe uma f�rmula ativa.
	* 
	* @see br.com.linkcom.sined.geral.service.FormulacustoService#existsFormulaAtiva()
	*
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public boolean existsFormulaAtiva(){
		return existsFormulaAtiva(null);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.FormulacustoDAO#getFormulacustoForcalcularCustoEntradaMaterial()
	*
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Formulacusto getFormulacustoForcalcularCustoEntradaMaterial() {
		return formulacustoDAO.getFormulacustoForcalcularCustoEntradaMaterial();
	}
}
