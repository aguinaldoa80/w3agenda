package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Servico;
import br.com.linkcom.sined.geral.dao.ServicoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicoService extends GenericService<Servico> {

	private ServicoDAO servicoDAO;
	
	public void setServicoDAO(ServicoDAO servicoDAO) {
		this.servicoDAO = servicoDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#deleteServico(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteServico(Material material) {
		servicoDAO.deleteServico(material);
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#insertServico(Servico)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void insertServico(Servico servico) {
		servicoDAO.insertServico(servico);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#carregaServico(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Servico carregaServico(Material material){
		return servicoDAO.carregaServico(material);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#updateServico(Servico)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateServico(Servico servico){
		servicoDAO.updateServico(servico);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#countServico(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Long countServico(Material material){
		return servicoDAO.countServico(material);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ServicoDAO#verificaServico
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaServico(Material bean){
		return servicoDAO.verificaServico(bean);
	}
	



}
