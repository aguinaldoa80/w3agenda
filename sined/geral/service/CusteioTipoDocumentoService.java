package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.CusteioTipoDocumento;
import br.com.linkcom.sined.geral.dao.CusteioTipoDocumentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CusteioTipoDocumentoService extends GenericService<CusteioTipoDocumento> {
	
	private static CusteioTipoDocumentoService instance;
	private CusteioTipoDocumentoDAO custeioTipoDocumentoDAO;
	
	public void setCusteioTipoDocumentoDAO(CusteioTipoDocumentoDAO custeioTipoDocumentoDAO) {this.custeioTipoDocumentoDAO = custeioTipoDocumentoDAO;}
	
	public static CusteioTipoDocumentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CusteioTipoDocumentoService.class);
		}
		return instance;
	}

	public List<CusteioTipoDocumento> findByCusteioProcessado(CusteioProcessado bean) {
		return custeioTipoDocumentoDAO.findByCusteioProcessado(bean);
	}

}
