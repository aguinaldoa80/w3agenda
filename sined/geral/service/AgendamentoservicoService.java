package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicoacao;
import br.com.linkcom.sined.geral.bean.Agendamentoservicocancela;
import br.com.linkcom.sined.geral.bean.Agendamentoservicodocumento;
import br.com.linkcom.sined.geral.bean.Agendamentoservicohistorico;
import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamentoServico;
import br.com.linkcom.sined.geral.dao.AgendamentoservicoDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoapoioFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.ColaboradoragendaservicoReportFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoapoioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.text.SimpleDateFormat;

public class AgendamentoservicoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Agendamentoservico> {
	
	public final String MSG_DATA_GERAR_PDF_CSV = "� obrigat�ria a escolha do Cliente e/ou intervalo da Data do Agendamento com no m�ximo 1 ano.";
	public final String MSG_QTDE_GERAR_PDF_CSV = "Quantidade de registros acima do permitido, favor realizar um filtro mais detalhado.";
	private AgendamentoservicoDAO agendamentoservicoDAO;
	private EscalaService escalaService;
	private EscalahorarioService escalahorarioService;
	private DocumentoService documentoService;
	private ParcelamentoService parcelamentoService;
	private AgendamentoservicohistoricoService agendamentoservicohistoricoService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private AusenciacolaboradorService ausenciacolaboradorService;
	private EscalacolaboradorService escalacolaboradorService;
	private AgendamentoservicodocumentoService agendamentoservicodocumentoService;
	private AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro;
	private MaterialService materialService;
	private CalendarioService calendarioService;
	private ParametrogeralService parametrogeralService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setAgendamentoservicodocumentoService(
			AgendamentoservicodocumentoService agendamentoservicodocumentoService) {
		this.agendamentoservicodocumentoService = agendamentoservicodocumentoService;
	}
	public void setAgendamentoservicoDAO(
			AgendamentoservicoDAO agendamentoservicoDAO) {
		this.agendamentoservicoDAO = agendamentoservicoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAusenciacolaboradorService(
			AusenciacolaboradorService ausenciacolaboradorService) {
		this.ausenciacolaboradorService = ausenciacolaboradorService;
	}
	public void setEscalacolaboradorService(
			EscalacolaboradorService escalacolaboradorService) {
		this.escalacolaboradorService = escalacolaboradorService;
	}
	public void setCalendarioService(CalendarioService calendarioService) {
		this.calendarioService = calendarioService;
	}
	
	/* singleton */
	private static AgendamentoservicoService instance;
	public static AgendamentoservicoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AgendamentoservicoService.class);
		}
		return instance;
	}
	
	public void setEscalaService(EscalaService escalaService) {
		this.escalaService = escalaService;
	}
	public void setEscalahorarioService(
			EscalahorarioService escalahorarioService) {
		this.escalahorarioService = escalahorarioService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setParcelamentoService(ParcelamentoService parcelamentoService) {
		this.parcelamentoService = parcelamentoService;
	}
	public void setAgendamentoservicohistoricoService(
			AgendamentoservicohistoricoService agendamentoservicohistoricoService) {
		this.agendamentoservicohistoricoService = agendamentoservicohistoricoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @Author Tom�s Rabelo
	 */
	public void updateAux() {
		agendamentoservicoDAO.updateAux();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> loadForAcoes(String whereIn) {
		return agendamentoservicoDAO.loadForAcoes(whereIn);
	}

	/**
	 * M�todo que valida para cancelar
	 * 
	 * @param list
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public void validaCancelar(List<Agendamentoservico> list, BindException errors) {
		for (Agendamentoservico agendamentoservico : list) {
			if(agendamentoservico.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.CANCELADO) || 
			   agendamentoservico.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.REALIZADO)){
				errors.reject("001", "O(s) agendamentos deve(m) ter a situa��o igual a 'Agendado', 'Em andamento' ou 'Aten��o' para ser cancelado.");
			}
		}
		
	}

	/**
	 * Valida os agendamentos para finalizar
	 * 
	 * @param list
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public void validaFinalizar(List<Agendamentoservico> list, BindException errors) {
		for (Agendamentoservico agendamentoservico : list) {
			if(agendamentoservico.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.CANCELADO) || 
					agendamentoservico.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.REALIZADO)){
				errors.reject("001", "O(s) agendamentos deve(m) ter a situa��o igual a 'Agendado', 'Em andamento' ou 'Aten��o' para ser finalizado.");
			}
		}
	}

	/**
	 * M�todo que cancela os agendamentos de servi�o
	 * 
	 * @param list
	 * @author Tom�s Rabelo
	 */
	public void cancelar(String whereIn, Agendamentoservicocancela agendamentoservicocancela) {
		agendamentoservicoDAO.cancelar(whereIn, agendamentoservicocancela);
	}

	/**
	 * M�todo que finaliza os agendamentos de servi�o
	 * 
	 * @param list
	 * @author Tom�s Rabelo
	 */
	public void realizado(String whereIn) {
		agendamentoservicoDAO.realizado(whereIn);
	}
	
	/**
	 * M�todo que monta os beans necess�rios para montar o datagrid de aloca��o de ve�culo flex
	 * 
	 * @see #findAlocacaoVeiculoData(Date, Date, VeiculousoapoioFiltro)
	 * @see br.com.linkcom.sined.geral.service.EscalaService#findEscalaDoVeiculo(br.com.linkcom.sined.geral.bean.Veiculo)
	 * @see br.com.linkcom.sined.geral.service.VeiculoferiadoService
	 * @see #montaListaAlocacaoVeiculo(Escala, Date, Date, List, List)
	 * @param veiculousoapoioFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservicoapoio> montaAgendaServicoFlex(AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro){
		Date dtReferencia = SinedDateUtils.stringToSqlDate(agendamentoservicoapoioFiltro.getDataReferencia());
		Date dtReferenciaAte = SinedDateUtils.stringToSqlDate(agendamentoservicoapoioFiltro.getDataReferenciaAte());
		
		List<Agendamentoservico> listaAgendaServico = findAgendamentoServicoData(dtReferencia, dtReferenciaAte, agendamentoservicoapoioFiltro);
		
		return montaAgendaServico(listaAgendaServico, agendamentoservicoapoioFiltro.getColaborador(), dtReferencia, dtReferenciaAte, agendamentoservicoapoioFiltro.getEmpresa());
	}
	
	public List<Agendamentoservicoapoio> buildScheduleService(AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro){
		List<Agendamentoservico> listaAgendaServico = findAgendamentoServicoData(agendamentoservicoapoioFiltro.getDateFrom(), agendamentoservicoapoioFiltro.getDateTo(), agendamentoservicoapoioFiltro);
		return montaAgendaServico(listaAgendaServico, agendamentoservicoapoioFiltro.getColaborador(), agendamentoservicoapoioFiltro.getDateFrom(), agendamentoservicoapoioFiltro.getDateTo(), agendamentoservicoapoioFiltro.getEmpresa());
	}
	
	/**
	 * M�todo que monta escala de hor�rio do colaborador
	 * 
	 * @param listaAgendaServico
	 * @param colaborador
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservicoapoio> montaAgendaServico(List<Agendamentoservico> listaAgendaServico, Colaborador colaborador, Date dtReferencia, Date dtReferenciaAte, Empresa empresa) {
		verificaPendenciasRestricoesCliente(listaAgendaServico);
		List<Escalacolaborador> listaEscalaHorario = escalacolaboradorService.findListaEscalaHorarioDoColaboradorDoPeriodo(colaborador, dtReferencia, dtReferenciaAte);
		List<Ausenciacolaborador> listaAusencias = ausenciacolaboradorService.findAusenciasDoColaborador(colaborador, dtReferencia, dtReferenciaAte);
		List<Agendamentoservicoapoio> lista = new ArrayList<Agendamentoservicoapoio>();
		
		String whereIn = "-1";
		
		for (Escalacolaborador escalahorario : listaEscalaHorario) {
			whereIn += "," + escalahorario.getEscala().getCdescala();
		}
		
		List<Escala> listaEscala = escalaService.carregaListaEscala(whereIn);
		
		//Carrega escalas de hor�rio
		for (Escalacolaborador escalahorario : listaEscalaHorario) {
//			Escala escala = escalaService.carregaEscala(escalahorario.getEscala());
			Escala escala = listaEscala.get(listaEscala.indexOf(escalahorario.getEscala()));
			escalahorario.getEscala().setListaescalahorario(escala.getListaescalahorario());
			montaListaAgendaServico(lista, dtReferencia, dtReferenciaAte, listaAgendaServico, escala, listaAusencias, escalahorario.getDtinicio(), escalahorario.getDtfim(), empresa);
		}
		
		verificaExclusividadeEscalas(listaEscalaHorario, lista);
		
		Collections.sort(lista, new Comparator<Agendamentoservicoapoio>(){
			public int compare(Agendamentoservicoapoio o1, Agendamentoservicoapoio o2) {
				return o1.getEscalahorario().getHorainicio().compareTo(o2.getEscalahorario().getHorainicio());
			}
		});
		return lista;
	}
	
	/**
	 * M�todo que anula c�lulas da tela do flex caso a escala de hor�rio do colaborador seja EXCLUSIVA
	 * 
	 * @param listaEscalaHorario
	 * @param lista
	 * @author Tom�s Rabelo
	 */
	private void verificaExclusividadeEscalas(List<Escalacolaborador> listaEscalaHorario, List<Agendamentoservicoapoio> lista) {
		for (Escalacolaborador escalahorario : listaEscalaHorario) {
			if(escalahorario.getExclusivo() != null && escalahorario.getExclusivo()){
				for (Agendamentoservicoapoio agendamentoservicoapoio : lista) {
					if(!escalahorario.getEscala().getListaescalahorario().contains(agendamentoservicoapoio.getEscalahorario())){
						for (Agendamentoservicoitemapoio agendamentoservicoitemapoio : agendamentoservicoapoio.getListaagendamentoservicoitemapoio()) {
							/*Todos os hor�rios que estiverem no intervalo de tempo dos hor�rios exclusivos ir�o se tornar indisponiveis.
							 * Deve atender � regra: estar dentro da data, disponivel, n�o pode ser domingo ou feriado.*/
							if(((escalahorario.getDtinicio().getTime() <= agendamentoservicoitemapoio.getData().getTime() && escalahorario.getDtfim() == null) ||
							   (escalahorario.getDtinicio().getTime() <= agendamentoservicoitemapoio.getData().getTime() && escalahorario.getDtfim().getTime() >= agendamentoservicoitemapoio.getData().getTime())) && 
							   agendamentoservicoitemapoio.getIsHorarioDisponivel() && !agendamentoservicoitemapoio.getIsFeriado()&& !agendamentoservicoitemapoio.getIsDomingo()){
								agendamentoservicoitemapoio.setIsHorarioDisponivel(Boolean.FALSE);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que verifica se os clientes que est�o na agenda possuem alguma restri��o.
	 * As restri��es est�o em cadastro de cliente ou contas a receber atrasadas
	 * 
	 * @param listaAgendaServico
	 * @author Tom�s Rabelo
	 */
	private void verificaPendenciasRestricoesCliente(List<Agendamentoservico> listaAgendaServico) {
		if(listaAgendaServico != null && !listaAgendaServico.isEmpty()){
			List<Cliente> listaClientes = retornaListaClientesAgendados(listaAgendaServico);
			if(listaClientes != null && !listaClientes.isEmpty()){
				List<Cliente> clientesRestricoes = clienteService.findClientesContaReceberAtrasadaOuRestricoesEmAberto(listaClientes);
				if(clientesRestricoes != null && !clientesRestricoes.isEmpty()){
					for (Cliente cliente : clientesRestricoes) 
						for (Agendamentoservico agendamentoservico : listaAgendaServico)
							if(agendamentoservico.getCliente().equals(cliente))
								agendamentoservico.setPossuiPendenciaRestricao(true);
				}
			}
		}
	}

	/**
	 * M�todo que retorna lista de clientes diferentes contidos no agendamento
	 * 
	 * @param listaAgendaServico
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Cliente> retornaListaClientesAgendados(List<Agendamentoservico> listaAgendaServico) {
		List<Cliente> clientes = new ArrayList<Cliente>();
		for (Agendamentoservico agendamentoservico : listaAgendaServico) 
			if(!clientes.contains(agendamentoservico.getCliente()))
				clientes.add(agendamentoservico.getCliente());
		
		return clientes;
	}

	/**
	 * M�todo que monta escala para tela FLEX
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param listaAgendaServico
	 * @param escala
	 * @return
	 * @author Tom�s Rabelo
	 * @param list 
	 * @param listaAusencias 
	 * @param dataFimEscala 
	 * @param dataInicioEscala 
	 */
	private void montaListaAgendaServico(List<Agendamentoservicoapoio> list, Date dataDeFiltro, Date dataAteFiltro, List<Agendamentoservico> listaAgendaServico, Escala escala, List<Ausenciacolaborador> listaAusencias, Date dataInicioEscala, Date dataFimEscala, Empresa empresa) {
		Date dataInicialReferencia = new Date(dataDeFiltro.getTime());
		Calendar dataInicioEscalaAux = Calendar.getInstance();
		dataInicioEscalaAux.setTimeInMillis(dataInicioEscala.getTime());
		Calendar dataFimEscalaAux = null;
		if(dataFimEscala != null){
			dataFimEscalaAux = Calendar.getInstance();
			dataFimEscalaAux.setTimeInMillis(dataFimEscala.getTime());
		}
		int dias = SinedDateUtils.diferencaDias(dataAteFiltro, dataDeFiltro)+1;
		
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		Escalahorario escalahorarioAux = null;
		Agendamentoservicoapoio agendamentoservicoapoio = null;
		List<Agendamentoservico> lista = null;
		
		List<Escalahorario> listHorariosDiferentes = escalahorarioService.getHorariosDiferentes(escala);
		for (Escalahorario escalahorario : listHorariosDiferentes) {
			agendamentoservicoapoio = new Agendamentoservicoapoio(escalahorario);
			//1�x ir� adicionar todos os dias e poder� colocar alguns dias indispon�veis
			if(!list.contains(agendamentoservicoapoio)){
				List<Agendamentoservicoitemapoio> listItemApoio = new ArrayList<Agendamentoservicoitemapoio>();
				for (int i = 0; i < dias; i++) {
					Date data = SinedDateUtils.addDiasData(dataInicialReferencia, i);
					Calendar dataAux = Calendar.getInstance();
					dataAux.setTimeInMillis(data.getTime());

					boolean domingo = false;
					if(escala.getNaotrabalhadomingo())
						domingo = SinedDateUtils.isDaySunday(data);
					
					boolean feriado = false;
					feriado = calendarioService.isFeriado(data, uf, municipio);
					
					lista = new ArrayList<Agendamentoservico>();
					boolean horarioDisponivel = true;
					String motivo = "";
					for (int j = 0; j < lista.size()+1; j++) {
						Agendamentoservico agendamentoservico = new Agendamentoservico();
						
						//Colocado esse m�todo devido ao fato de ter mudado a forma de criar escala de hor�rio
						escalahorarioAux = escalahorarioService.achaEscalaDeHorarioCorreta(data, escala, escalahorario);
						
						if(!domingo)
							agendamentoservico = findAgendamentoServicoNoDiaHora(data, escalahorarioAux, listaAgendaServico);
						
						//condi��o de parada de grupo
						if(domingo || (lista.size() > 0 && agendamentoservico.getCdagendamentoservico() == null))
							break;
		
						verificaSeHorarioDisponivelNoDia(escala, escalahorario, dataAux, agendamentoservico, listaAusencias, dataInicioEscalaAux, dataFimEscalaAux);
						horarioDisponivel = agendamentoservico.getIsHorarioDisponivel();
						motivo += agendamentoservico.getMotivo();
						
						lista.add(agendamentoservico);
					}
					Agendamentoservicoitemapoio agendamentoservicoitemapoio = new Agendamentoservicoitemapoio(data, lista, domingo, horarioDisponivel, escalahorario, feriado, motivo);
					listItemApoio.add(agendamentoservicoitemapoio);
				}
				agendamentoservicoapoio.getListaagendamentoservicoitemapoio().addAll(listItemApoio);
				list.add(agendamentoservicoapoio);
			}
//			else{//A partir da 2�x torna dias dispon�veis
//				agendamentoservicoapoio = findAgendamentoServicoApoio(list, escala, escalahorario);
//				dias = SinedDateUtils.diferencaDias(dataFimEscala != null && dataFimEscala.before(dataAteFiltro) ? dataFimEscala : dataAteFiltro, dataInicioEscala)+1;
//				dataInicialReferencia = new Date(dataInicioEscala.getTime());
//				
//				for (int i = 0; i < dias; i++) {
//					Date data = SinedDateUtils.addDiasData(dataInicialReferencia, i);
//					Calendar dataAux = Calendar.getInstance();
//					dataAux.setTimeInMillis(data.getTime());
//					
//					makeAgendamentoServicoDisponivel(agendamentoservicoapoio, data);
//				}
//			}
		}
	}

//	/**
//	 * Esse m�todo torna dispon�vel Agendamento de Servi�os. Utilizado para escalas intercaladas que ficam na mesma linha na tela do flex
//	 * 
//	 * @param agendamentoservicoapoio
//	 * @param data
//	 * @authro Tom�s Rabelo
//	 */
//	private void makeAgendamentoServicoDisponivel(Agendamentoservicoapoio agendamentoservicoapoio, Date data) {
//		if(agendamentoservicoapoio.getListaagendamentoservicoitemapoio() != null && !agendamentoservicoapoio.getListaagendamentoservicoitemapoio().isEmpty()){
//			for (Agendamentoservicoitemapoio agendamentoservicoitemapoio : agendamentoservicoapoio.getListaagendamentoservicoitemapoio()) {
//				if(agendamentoservicoitemapoio.getData().getTime() == data.getTime() && !agendamentoservicoitemapoio.getIsFeriado()){
//					agendamentoservicoitemapoio.setIsHorarioDisponivel(true);
////					for (Agendamentoservico agendamentoservico : agendamentoservicoitemapoio.getListaAgendamentos()) 
////						agendamentoservico.setIsHorarioDisponivel(true);
//					break;
//				}
//			}
//		}
//	}
	
	/**
	 * M�todo que retorna o agendamento servi�o do dia e hora
	 * 
	 * @param currentDay
	 * @param escalahorarioAux
	 * @param listaAgendaServico
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Agendamentoservico findAgendamentoServicoNoDiaHora(Date currentDay, Escalahorario escalahorarioAux, List<Agendamentoservico> listaAgendaServico) {
		Agendamentoservico agendamentoservico;
		for (int i = 0; i < listaAgendaServico.size(); i++) {
			agendamentoservico = listaAgendaServico.get(i);
			if(SinedDateUtils.equalsIgnoreHour(currentDay, agendamentoservico.getData())){
				if(agendamentoservico.getEscalahorario() != null && agendamentoservico.getEscalahorario().getCdescalahorario() != null){
					if(agendamentoservico.getEscalahorario().equals(escalahorarioAux)){
						listaAgendaServico.remove(i);
						return agendamentoservico;
					}
				}else{
					listaAgendaServico.remove(i);
					return agendamentoservico;
				}
			}
		}
		return new Agendamentoservico(escalahorarioAux);
	}

//	/**
//	 * M�todo que acha o agendamento servi�o apoio correto ou ent�o cria um novo se n�o existir
//	 * 
//	 * @param list
//	 * @param escala
//	 * @param escalahorario
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	private Agendamentoservicoapoio findAgendamentoServicoApoio(List<Agendamentoservicoapoio> list, Escala escala, Escalahorario escalahorario) {
//		if(list != null && !list.isEmpty()){
//			for (Agendamentoservicoapoio agendamentoservicoapoioAux : list) {
//				Escalahorario escalahorarioAux = agendamentoservicoapoioAux.getEscalahorario();
//				if(((escala.getHorariotodosdias() != null && escala.getHorariotodosdias()) || 
//				   (escala.getHorariotodosdias() != null && !escala.getHorariotodosdias())) &&
//				   escalahorario.getHorainicio().getTime() == escalahorarioAux.getHorainicio().getTime() && escalahorario.getHorafim().getTime() == escalahorarioAux.getHorafim().getTime()){
//					return agendamentoservicoapoioAux;
//				}
//			}
//		}
//		return new Agendamentoservicoapoio(escalahorario);
//	}

	/**
	 * M�todo que verifica se o hor�rio est� disponivel
	 * 
	 * @param escala
	 * @param escalahorario
	 * @param dataAux
	 * @param agendamentoservico
	 * @author Tom�s Rabelo
	 * @param listaAusencias 
	 * @param dataFimEscalaAux 
	 * @param dataInicioEscalaAux 
	 */
	private void verificaSeHorarioDisponivelNoDia(Escala escala, Escalahorario escalahorario, Calendar dataAux,	Agendamentoservico agendamentoservico, List<Ausenciacolaborador> listaAusencias, Calendar dataInicioEscalaAux, Calendar dataFimEscalaAux) {
		boolean horarioDisponivel = false;
		StringBuilder motivo = new StringBuilder();
		if(escala.getHorariotodosdias() != null && !escala.getHorariotodosdias()){
			for (Escalahorario escalahorarioAux : escala.getListaescalahorario()) {
				if(dataAux.get(Calendar.DAY_OF_WEEK) == escalahorarioAux.getDiasemana().ordinal()+1){
					if(escalahorarioAux.getHorainicio().getTime() == escalahorario.getHorainicio().getTime() && 
					   escalahorarioAux.getHorafim().getTime() == escalahorario.getHorafim().getTime() && 
					   ausenciacolaboradorService.verificaColaboradorAusente(dataAux, escalahorario, listaAusencias, motivo) && 
					   ((dataFimEscalaAux == null && dataAux.getTimeInMillis() >= dataInicioEscalaAux.getTimeInMillis()) || 
						(dataAux.getTimeInMillis() >= dataInicioEscalaAux.getTimeInMillis() && dataAux.getTimeInMillis() <= dataFimEscalaAux.getTimeInMillis()))){
						horarioDisponivel = true;
						break;
					}
				}
			}
		}else{
			if((dataFimEscalaAux == null && dataAux.getTimeInMillis() >= dataInicioEscalaAux.getTimeInMillis()) || 
			   (dataAux.getTimeInMillis() >= dataInicioEscalaAux.getTimeInMillis() && dataAux.getTimeInMillis() <= dataFimEscalaAux.getTimeInMillis()))
			   horarioDisponivel = ausenciacolaboradorService.verificaColaboradorAusente(dataAux, escalahorario, listaAusencias, motivo);
		}
		agendamentoservico.setIsHorarioDisponivel(horarioDisponivel);
		agendamentoservico.setMotivo(motivo.toString());
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param material
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendaServicoData(Date dtReferencia, Date dtReferenciaLimite, Material material, Colaborador colaborador) {
		return agendamentoservicoDAO.findAgendaServicoData(dtReferencia, dtReferenciaLimite, material, colaborador);
	}

	/**
	 * M�todo que reestrutura os registros para realizar as aloca��es de veiculo.
	 * Toda vez que o usu�rio manda salvar, ele busca os registros antigos e faz uma compara��o de qual foi modificado ou criado novo.
	 * Todos que foram modificados e/ou retirados ser�o excluidos. Os modificados s�o gerados novos registros.
	 * 
	 * @see #findAlocacaoVeiculoData(Date, Date, VeiculousoapoioFiltro)
	 * @see br.com.linkcom.sined.geral.service.VeiculousotipoService#veiculoUsoTipoNormal()
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findContaGerencialVeiculo(br.com.linkcom.sined.geral.bean.Veiculo)
	 * @see #saveDeleteVeiculoUso(List, List)
	 * @param lista
	 * @param veiculousoapoioFiltro
	 * @param empresa
	 * @author Tom�s Rabelo
	 */
	public void prepareToSaveAgendaServicos(List<Agendamentoservicoapoio> lista, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro, List<Agendamentoservicoprazopagamentoapoio> listaDocumento){
		List<Agendamentoservico> listaAgendaNovos = new ArrayList<Agendamentoservico>();
		montaListaAgendamentos(agendamentoservicoapoioFiltro, lista, listaAgendaNovos);
		List<Documento> listaDocumentos = documentoService.montaListaDocumentoAgendamentoServico(listaDocumento, agendamentoservicoapoioFiltro);		
		saveDeleteAgendaServico(listaAgendaNovos, listaDocumentos);
	}
	
	public void prepareToSaveAgendaNovosServicos(List<Agendamentoservico> listaAgendaNovos, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro, List<Agendamentoservicoprazopagamentoapoio> listaDocumento){
		List<Documento> listaDocumentos = documentoService.montaListaDocumentoAgendamentoServico(listaDocumento, agendamentoservicoapoioFiltro);
		saveDeleteAgendaServico(listaAgendaNovos, listaDocumentos);
	}
	
	/**
	 * M�todo que monta os agendamentos de servi�o para save
	 * 
	 * @param agendamentoservicoapoioFiltro
	 * @param lista
	 * @param listaAgendaNovos
	 * @param listaAgendaExcluidos
	 * @author Tom�s Rabelo
	 */
	private void montaListaAgendamentos(AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro, List<Agendamentoservicoapoio> lista, List<Agendamentoservico> listaAgendaNovos) {
//		Date dtReferencia = SinedDateUtils.stringToSqlDate(agendamentoservicoapoioFiltro.getDataReferencia());
//		Date dtReferenciaAte = SinedDateUtils.stringToSqlDate(agendamentoservicoapoioFiltro.getDataReferenciaAte());
		
//		List<Agendamentoservico> listaAgendaServicoAntigos = findAgendamentoServicoData(dtReferencia, dtReferenciaAte, agendamentoservicoapoioFiltro);
		List<Agendamentoservico> listaAgendaAtulizados = new ArrayList<Agendamentoservico>();
		
		for (Agendamentoservicoapoio agendamentoservicoapoio  : lista) {
			for (Agendamentoservicoitemapoio agendamentoservicoitemapoio : agendamentoservicoapoio.getListaagendamentoservicoitemapoio()) {
				List<Agendamentoservico> listaAux = agendamentoservicoitemapoio.getListaAgendamentos();
				for (Agendamentoservico agendamentoservico : listaAux) {
					//Se tiver ID setado so atualiza
					if(agendamentoservico.getCdagendamentoservico() == null || agendamentoservico.getCdagendamentoservico() == 0){
						agendamentoservico.setCdagendamentoservico(null);
						agendamentoservico.setEmpresa(agendamentoservicoapoioFiltro.getEmpresa());
						agendamentoservico.setMaterial(agendamentoservicoapoioFiltro.getMaterial());
						agendamentoservico.setData(agendamentoservicoitemapoio.getData());
						
						if(agendamentoservico.getCliente() != null && agendamentoservico.getCliente().getCdpessoa() != null){
							agendamentoservico.setColaborador(agendamentoservicoapoioFiltro.getColaborador());
							listaAgendaNovos.add(agendamentoservico);
						}
					}else{
						listaAgendaAtulizados.add(new Agendamentoservico(agendamentoservico.getCdagendamentoservico()));
					}
				}
				
			}
		}
			
//		for (Agendamentoservico antigo : listaAgendaServicoAntigos) 
//			if(!listaAgendaAtulizados.contains(antigo)){
//				antigo.setDtcancelado(new Date(System.currentTimeMillis()));
//				antigo.getAux_agendamentoservico().setSituacao(SituacaoAgendamentoServico.CANCELADO);
//				listaAgendaExcluidos.add(antigo);
//			}
	}

	/**
	 * M�todo que salva e/ou exclui registros da lista da tela do FLEX
	 * 
	 * @param listaAgendaNovos
	 * @param listaAgendaExcluidos
	 * @author Tom�s Rabelo
	 * @param listaDocumentos 
	 */
	private void saveDeleteAgendaServico(final List<Agendamentoservico> listaAgendaNovos, final List<Documento> listaDocumentos) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				Agendamentoservicodocumento agendamentoservicodocumento = null;
				if(listaDocumentos != null && listaDocumentos.size() > 0){
					agendamentoservicodocumento = new Agendamentoservicodocumento();
					agendamentoservicodocumentoService.saveOrUpdateNoUseTransaction(agendamentoservicodocumento);
				}
				
				List<Agendamentoservicohistorico> lista = null;
				for (Agendamentoservico agendamentoservico : listaAgendaNovos) {
					lista = new ArrayList<Agendamentoservicohistorico>();
					lista.add(new Agendamentoservicohistorico(Agendamentoservicoacao.AGENDADO));
					agendamentoservico.setAgendamentoservicodocumento(agendamentoservicodocumento);
					saveOrUpdateNoUseTransaction(agendamentoservico);
				}
				
//				for (Agendamentoservico agendamentoservico : listaAgendaExcluidos) 
//					saveOrUpdateNoUseTransaction(agendamentoservico);
				
				for (Documento documento : listaDocumentos) {
					documento.setAgendamentoservicodocumento(agendamentoservicodocumento);
					
					for (Agendamentoservico agendamentoservico : listaAgendaNovos) 
						if(agendamentoservico.getCliente().equals(documento.getPessoa()))
							documento.setNumero(agendamentoservico.getCdagendamentoservico()+", "+(documento.getNumero() != null ? documento.getNumero() : ""));
					
					if(documento.getNumero() != null && !documento.getNumero().equals("") && documento.getNumero().indexOf(", ") != -1)
						documento.setNumero(documento.getNumero().substring(0, documento.getNumero().lastIndexOf(",")) + documento.getNumero().substring(documento.getNumero().lastIndexOf(",")+1, documento.getNumero().length()));
					
					Parcelamento parcelamento = documentoService.criarEstruturaParcelamento(documento);
					parcelamentoService.saveOrUpdateNoUseTransaction(parcelamento);
//					documentoService.saveOrUpdateNoUseTransaction(documento);
				}
				
				return status;
			}

		});
		for (Agendamentoservico agendamentoservico : listaAgendaNovos) {
			callProcedureAtualizaSessaoAgendamentoServico(agendamentoservico.getColaborador(), agendamentoservico.getMaterial(), agendamentoservico.getCliente());
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param material
	 * @author Tom�s Rabelo
	 */
	public void callProcedureAtualizaSessaoAgendamentoServico(Colaborador colaborador, Material material, Cliente cliente) {
		agendamentoservicoDAO.callProcedureAtualizaSessaoAgendamentoServico(colaborador, material, cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @param agendamentoservicoapoioFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendamentoServicoData(Date dtReferencia, Date dtReferenciaAte, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro) {
		return agendamentoservicoDAO.findAgendamentoServicoData(dtReferencia, dtReferenciaAte, agendamentoservicoapoioFiltro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param agendamentoservico
	 * @return
	 * @author Tom�s Rabelo
	 * @param material 
	 */
	public Long qtdeAgendamentoServicoMaterial(Agendamentoservico agendamentoservico) {
		return agendamentoservicoDAO.qtdeAgendamentoServicoMaterial(agendamentoservico);
	}

	/**
	 * M�todo que valida as a��es
	 * 
	 * @param agendamentoservicohistorico
	 * @param bindException
	 * @author Tom�s Rabelo
	 */
	public void validaAcoes(Agendamentoservicohistorico agendamentoservicohistorico, BindException bindException) {
		List<Agendamentoservico> list = new ArrayList<Agendamentoservico>();
		if(agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.CANCELADO) || agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.REALIZADO)){
			list = loadForAcoes(agendamentoservicohistorico.getWhereIn());
			
			if(agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.CANCELADO))
				validaCancelar(list, bindException);
			else if(agendamentoservicohistorico.getAgendamentoservicoacao().equals(Agendamentoservicoacao.REALIZADO))
				validaFinalizar(list, bindException);
		}
	}
	
	/**
	 * M�todo que desaloca um agendamento servi�o pela tela do FLEX.
	 * S� � poss�vel desalocar um registro por vez e toda vez que este m�todo for chamado necessariamente o registro foi gravado no banco.
	 * 
	 * @param agendamentoservico
	 * @param motivo
	 * @author Tom�s Rabelo
	 */
	public void desalocarClienteFlex(Agendamentoservico agendamentoservico, String motivo){
		Agendamentoservicohistorico agendamentoservicohistorico = new Agendamentoservicohistorico(motivo, agendamentoservico.getCdagendamentoservico().toString(), Agendamentoservicoacao.CANCELADO);
		List<Agendamentoservico> listaAgendamentos = findByWhereIn(agendamentoservico.getCdagendamentoservico().toString());
		agendamentoservicohistoricoService.saveHistoricos(agendamentoservicohistorico, listaAgendamentos);
	}
	
	public void remarcarClienteFlex(Agendamentoservico agendamentoservico, Date data, Escalahorario escalahorario){
		agendamentoservicoDAO.atualizaDataEscalahorario(agendamentoservico, data, escalahorario);
		List<Agendamentoservico> listaAgendamentos = findByWhereIn(agendamentoservico.getCdagendamentoservico().toString());
		callProcedureAtualizaSessaoAgendamentoServico(listaAgendamentos);
	}
	
	/**
	 * M�todo que atualiza as sess�es dos agendamento
	 * 
	 * @param listaAgendamentos
	 * @author Tom�s Rabelo
	 */
	public void callProcedureAtualizaSessaoAgendamentoServico(List<Agendamentoservico> listaAgendamentos) {
		if(listaAgendamentos != null && !listaAgendamentos.isEmpty())
			for (Agendamentoservico agendamentoservico : listaAgendamentos) 
				callProcedureAtualizaSessaoAgendamentoServico(agendamentoservico.getColaborador(), agendamentoservico.getMaterial(), agendamentoservico.getCliente());
	}
	
	public List<Escalahorario> buscarHorariosEntrada(Agendamentoservico agendamentoservico){
		List<Escalahorario> listaNova = new ArrayList<Escalahorario>();
		Date data;
		
		if(agendamentoservico.getCdagendamentoservico() != null){
			data = agendamentoservico.getDataAux();
			agendamentoservico = this.loadForEntrada(agendamentoservico);
		} else {
			data = agendamentoservico.getData();
			agendamentoservico.setMaterial(materialService.load(agendamentoservico.getMaterial(), "material.cdmaterial, material.qtdeunidade"));
		}
		
		Empresa empresa = agendamentoservico.getEmpresa();
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		List<Escalahorario> lista = escalahorarioService.findEscalaHorario(agendamentoservico.getColaborador(), data);
		for (Escalahorario escalahorario : lista) {
			if(this.validaEscalaHorario(escalahorario, agendamentoservico, data, uf, municipio)){
				listaNova.add(escalahorario);
			}
		}
		
		return escalahorarioService.limpaHorasConvertString(listaNova);
	}
	
	public List<Escalahorario> buscarHorariosFlex(Agendamentoservico agendamentoservico, Date data){
		agendamentoservico = this.loadForEntrada(agendamentoservico);
		
		Date dataAgend = agendamentoservico.getData();
		Escalahorario escalahorarioAgend = agendamentoservico.getEscalahorario();
		
		Empresa empresa = agendamentoservico.getEmpresa();
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		List<Escalahorario> lista = escalahorarioService.findEscalaHorario(agendamentoservico.getColaborador(), agendamentoservico.getData());
		List<Escalahorario> listaNova = new ArrayList<Escalahorario>();
		for (Escalahorario escalahorario : lista) {
			if(this.validaEscalaHorario(escalahorario, agendamentoservico, data, uf, municipio)){
				if(!SinedDateUtils.equalsIgnoreHour(data, dataAgend) || (SinedDateUtils.equalsIgnoreHour(data, dataAgend) && !escalahorario.equals(escalahorarioAgend))){
					listaNova.add(escalahorario);
				}
			}
		}
		
		return escalahorarioService.limpaHorasConvertString(listaNova);
	}
	
	private boolean validaEscalaHorario(Escalahorario escalahorario, Agendamentoservico agendamentoservico, Date data, Uf uf, Municipio municipio){
		boolean valido = true;
		boolean isFeriado = false;
		
		isFeriado = calendarioService.isFeriado(data, uf, municipio);
		
		agendamentoservico.setData(data);
		agendamentoservico.setEscalahorario(escalahorario);
		
		Material material = agendamentoservico.getMaterial();
		material.setQtdeunidade(material.getQtdeunidade() == null ? 1 : material.getQtdeunidade());
		if(material.getQtdeunidade() <= this.qtdeAgendamentoServicoMaterial(agendamentoservico).intValue())
			valido = false;
	
		List<Agendamentoservico> listaAgendamentos = this.verificaAgendamentoClienteHorario(agendamentoservico.getColaborador(), null, escalahorario, agendamentoservico.getData(), agendamentoservico);
		if(listaAgendamentos != null && !listaAgendamentos.isEmpty())
			for (Agendamentoservico aux : listaAgendamentos) 
				if(aux != null && aux.getCdagendamentoservico() != null && ((!aux.getMaterial().equals(material)) || (aux.getCliente().equals(agendamentoservico.getCliente()))))
					valido = false;
		
		boolean naotrabalhadomingo = escalahorario.getEscala().getNaotrabalhadomingo();
		int diaSemana = SinedDateUtils.getDateProperty(data, Calendar.DAY_OF_WEEK);
		if(diaSemana == Calendar.SUNDAY && naotrabalhadomingo || isFeriado)
			valido = false;
		
		return valido;
	}

	/**
	 * M�todo que gera o relat�rio de agendamento de servi�os do cliente ou do colaborador dependendo do filtro selecionado
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 * @throws Exception 
	 */
	public Resource gerarRelatorio(AgendamentoservicoFiltro filtro) throws Exception {
		if (filtro.getCliente()==null && (filtro.getDtde()==null || filtro.getDtate()==null || SinedDateUtils.diferencaDias(filtro.getDtate(), filtro.getDtde())>365)){
			throw new SinedException(MSG_DATA_GERAR_PDF_CSV);
		}
			
		if (getCountListagem(filtro)>10000){
			throw new SinedException(MSG_QTDE_GERAR_PDF_CSV);
		}
		
		if(getCountListagem(filtro) > 10000) throw new SinedException("Quantidade de registros acima do permitido favor realizar um filtro mais detalhado.");
		
		MergeReport mergeReport = new MergeReport("agendamentoservico_"+SinedUtil.datePatternForReport()+".pdf");
		List<Agendamentoservico> lista = this.findAgendamentosServicosReport(filtro);
		
//		if(lista.size() > 10000) throw new SinedException("Quantidade de registros acima do permitido favor realizar um filtro mais detalhado.");
		
		Empresa empresaAux = filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? empresaService.loadComArquivo(filtro.getEmpresa()) : empresaService.loadPrincipal();
		Image image = SinedUtil.getLogo(empresaAux);
		
		if(lista == null || lista.isEmpty())
			throw new SinedException("Dados insuficientes para gera��o do relat�rio.");
		
		criaRelatorios(mergeReport, lista, filtro, empresaAux, image);
		return mergeReport.generateResource();
	}

	/**
	 * M�todo que cria os relat�rios de acordo com Cliente ou Colaborador
	 * 
	 * @param mergeReport
	 * @param lista
	 * @param filtro.getTipovisaoreport()
	 * @param empresaAux
	 * @param image
	 * @author Tom�s Rabelo
	 */
	private void criaRelatorios(MergeReport mergeReport, List<Agendamentoservico> lista, AgendamentoservicoFiltro filtro, Empresa empresaAux, Image image) {
		Integer posicaoAtual = 0;
		Integer posicaoAnterior = 0;
		Report report;
		
		while (posicaoAtual < lista.size()) {
			posicaoAnterior = posicaoAtual;
			Pessoa pessoa = filtro.getTipovisaoreport().equals(AgendamentoservicoFiltro.CLIENTE) ? ((Pessoa)lista.get(posicaoAtual).getCliente()) : ((Pessoa)lista.get(posicaoAtual).getColaborador());
			for (int i = posicaoAtual; i < lista.size(); i++) {
				posicaoAtual++;
				if((pessoa instanceof Cliente && !lista.get(i).getCliente().equals((Cliente)pessoa)) || 
				   (pessoa instanceof Colaborador && !lista.get(i).getColaborador().equals((Colaborador)pessoa))){
					posicaoAtual--;
					break;
				}
			}
			report = new Report(filtro.getTipovisaoreport().equals(AgendamentoservicoFiltro.CLIENTE) ? "/servicointerno/agendamentoservicocliente" : "/servicointerno/agendamentoservicocolaborador");
			adicionaDadosReport(report, empresaAux, image);
			if(filtro.getTipovisaoreport().equals(AgendamentoservicoFiltro.CLIENTE)){
				report.addParameter("identificador", ((Cliente)pessoa).getIdentificador());
				report.addParameter("cli", ((Cliente)pessoa).getFullNome());
			}else{
				report.addParameter("codigo", ((Colaborador)pessoa).getCdpessoa());
				report.addParameter("col", ((Colaborador)pessoa).getNome());
			}
			
			if(filtro.getTipovisaoreport().equals(AgendamentoservicoFiltro.COLABORADOR_COMPLETO)){
				report.setDataSource(montaRelatorioColaboradorComplet(lista.subList(posicaoAnterior, posicaoAtual), filtro));
			}else{
				report.setDataSource(lista.subList(posicaoAnterior, posicaoAtual));
			}
				
			report.addParameter("AGENDAR_SERVICO_POR", parametrogeralService.getValorPorNome(Parametrogeral.AGENDAR_SERVICO_POR));
			mergeReport.addReport(report);
		}
	}
	
	/**
	 * M�todo que chama o processo que gera a agenda de atendimento e depois trata esta lista para ser exibida no relat�rio de Colaborador completo
	 * 
	 * @param lista
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Agendamentoservico> montaRelatorioColaboradorComplet(List<Agendamentoservico> lista, AgendamentoservicoFiltro filtro) {
		List<Agendamentoservicoapoio> listaCompleta = montaAgendaServico(lista, filtro.getColaborador(), SinedDateUtils.dateToBeginOfDay(filtro.getDtde()), SinedDateUtils.dataToEndOfDay(filtro.getDtate()), filtro.getEmpresa());
		List<Agendamentoservico> listaNova = new ArrayList<Agendamentoservico>();
		Agendamentoservicoapoio agendamentoservicoapoioAux = listaCompleta.iterator().next();
		for (int i = 0; i < agendamentoservicoapoioAux.getListaagendamentoservicoitemapoio().size(); i++) {
			for (Agendamentoservicoapoio agendamentoservico : listaCompleta) {
				Agendamentoservicoitemapoio agendamentoservicoitemapoio = agendamentoservico.getListaagendamentoservicoitemapoio().get(i);
				if(agendamentoservicoitemapoio.getIsDomingo()){
					listaNova.add(new Agendamentoservico(agendamentoservicoitemapoio.getData(), agendamentoservico.getEscalahorario(), agendamentoservicoitemapoio.getIsDomingo()));
				}else{
					for (Agendamentoservico agendamentoservico2 : agendamentoservicoitemapoio.getListaAgendamentos()) {
						agendamentoservico2.setData(agendamentoservicoitemapoio.getData());
						agendamentoservico2.setIsDomingo(agendamentoservicoitemapoio.getIsDomingo());
						agendamentoservico2.setIsFeriado(agendamentoservicoitemapoio.getIsFeriado());
						agendamentoservico2.setIsHorarioDisponivel(agendamentoservicoitemapoio.getIsHorarioDisponivel());
						agendamentoservico2.setMotivo(agendamentoservicoitemapoio.getMotivo());
						listaNova.add(agendamentoservico2);
					}
				}
			}
		}
		
		return listaNova;
	}
	
	/**
	 * M�todo que adiciona cabe�alho no relat�rio de Agendamento servi�o
	 * 
	 * @param report
	 * @param empresa
	 * @param image
	 * @author Tom�s Rabelo
	 */
	private void adicionaDadosReport(Report report, Empresa empresa, Image image) {
		report.addParameter("TITULO", "AGENDA DE ATENDIMENTO");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		report.addParameter("LOGO", image);
		report.addParameter("EMPRESA", empresaService.getEmpresaRazaosocialOuNome(empresa));
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendamentosServicosReport(AgendamentoservicoFiltro filtro) {
		return agendamentoservicoDAO.findAgendamentosServicosReport(filtro);
	}

	/**
	 * Faz refer�ncia ao DAO
	 * @author Taidson
	 * @since 10/05/2010
	 */
	public List<Agendamentoservico> findForRelatorioAgendaServico(ColaboradoragendaservicoReportFiltro filtro) {
		return agendamentoservicoDAO.findForRelatorioAgendaServico(filtro);
	}
	
	@Override
	public void saveOrUpdate(Agendamentoservico bean) {
		super.saveOrUpdate(bean);
		this.callProcedureAtualizaSessaoAgendamentoServico(bean.getColaborador(), bean.getMaterial(), bean.getCliente());
	}
	
	/**
	 * M�todo que verifica se o o cliente j� est� alocado no ho�rario para outro Colaborador.
	 * Chamado via FLEX
	 * 
	 * @param cliente
	 * @param lista
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String verificaAgendamentoClienteHorario(Cliente cliente, List<Agendamentoservicoitemapoio> lista){
		String datasAlocadas = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for (Agendamentoservicoitemapoio agendamentoservicoitemapoio : lista) {
			Escalahorario escalahorario = escalahorarioService.load(agendamentoservicoitemapoio.getEscalahorarioAux());
			List<Agendamentoservico> listaAgendamentos = verificaAgendamentoClienteHorario(null, cliente, escalahorario, agendamentoservicoitemapoio.getData(), null);
			if(listaAgendamentos != null && !listaAgendamentos.isEmpty())
				for (Agendamentoservico aux : listaAgendamentos) 
					if(aux != null && aux.getCdagendamentoservico() != null)
						datasAlocadas += "O cliente j� est� alocado na data "+dateFormat.format(aux.getData())+" hor�rio "+aux.getEscalahorario().getDescriptionProperty()+" para colaborador "+aux.getColaborador().getNome()+".\n";
		}
		return datasAlocadas;
	}
	
	/**
	 * M�todo que verifica se o o cliente j� est� alocado no ho�rario para outro Colaborador.
	 * Chamado via FLEX
	 * 
	 * @param cliente
	 * @param lista
	 * @return List<String>
	 * @author Tom�s Rabelo
	 */
	public List<String> verificaAgendamentoClienteHorarioAlocado(Cliente cliente, List<Agendamentoservicoitemapoio> lista){
		List<String> datasAlocadas = new ArrayList<String>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for (Agendamentoservicoitemapoio agendamentoservicoitemapoio : lista) {
			Escalahorario escalahorario = escalahorarioService.load(agendamentoservicoitemapoio.getEscalahorarioAux());
			List<Agendamentoservico> listaAgendamentos = verificaAgendamentoClienteHorario(null, cliente, escalahorario, agendamentoservicoitemapoio.getData(), null);
			if(listaAgendamentos != null && !listaAgendamentos.isEmpty())
				for (Agendamentoservico aux : listaAgendamentos) 
					if(aux != null && aux.getCdagendamentoservico() != null)
						datasAlocadas.add("O cliente j� est� alocado na data "+dateFormat.format(aux.getData())+" hor�rio "+aux.getEscalahorario().getDescriptionProperty()+" para colaborador "+aux.getColaborador().getNome());
		}
		return datasAlocadas;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cliente
	 * @param escalahorarioAux
	 * @param data
	 * @author Tomas Rabelo
	 * @param agendamentoservico 
	 * @return
	 */
	public List<Agendamentoservico> verificaAgendamentoClienteHorario(Colaborador colaborador, Cliente cliente, Escalahorario escalahorarioAux, Date data, Agendamentoservico agendamentoservico) {
		return agendamentoservicoDAO.verificaAgendamentoClienteHorario(colaborador, cliente, escalahorarioAux, data, agendamentoservico);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param escala
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean verificaAgendamentoColaboradorEmAbertoOuAgendado(Colaborador colaborador, Escala escala) {
		return agendamentoservicoDAO.verificaAgendamentoColaboradorEmAbertoOuAgendado(colaborador, escala);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @param escalacolaborador
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public boolean isEscalacolaboradorForaPeriodo(Colaborador colaborador, Escalacolaborador escalacolaborador) {
		return agendamentoservicoDAO.isEscalacolaboradorForaPeriodo(colaborador, escalacolaborador);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findByWhereIn(String whereIn) {
		return agendamentoservicoDAO.findByWhereIn(whereIn);
	}
	
	public List<Agendamentoservico> findForAgenda(Date data) {
		return agendamentoservicoDAO.findForAgenda(data);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoservicoDAO#findForCsv(AgendamentoservicoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 02/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Agendamentoservico> findForCsv(AgendamentoservicoFiltro filtro) {
		return agendamentoservicoDAO.findForCsv(filtro);
	}
	
	public Long getCountListagem(AgendamentoservicoFiltro filtro){
		return agendamentoservicoDAO.getCountListagem(filtro);
	}
}