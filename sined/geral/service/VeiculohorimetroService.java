package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculohorimetro;
import br.com.linkcom.sined.geral.dao.VeiculohorimetroDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.AnaliticoSinteticoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.AnaliticoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculohorimetroService extends GenericService<Veiculohorimetro> {

	private VeiculohorimetroDAO veiculohorimetroDAO;
	
	public void setVeiculohorimetroDAO(VeiculohorimetroDAO veiculohorimetroDAO) {
		this.veiculohorimetroDAO = veiculohorimetroDAO;
	}
	
	public void adicionaHorimetroAtualRegistros(List<Veiculohorimetro> lista) {
		if(lista != null && lista.size() > 0){
			for (Veiculohorimetro veiculohorimetro: lista) 
				veiculohorimetro.setVeiculohorimetroatual(getHorimetroAtualDoVeiculo(veiculohorimetro.getVeiculo()));
		}
	}
	
	public Veiculohorimetro getHorimetroAtualDoVeiculo(Veiculo veiculo) {
		return veiculohorimetroDAO.getHorimetroAtualDoVeiculo(veiculo);
	}
	
	public Double calculaDiferenca(Veiculohorimetro horimetroNovo, Veiculohorimetro horimetroAntigo){
		//se houver ultima importa��o...
		if (horimetroAntigo != null)
			return horimetroNovo.getHorimetronovo() - horimetroAntigo.getHorimetronovo();
		else
			//sen�o, a diferen�a � a partir do zero
			return horimetroNovo.getHorimetronovo();
	}

	public Veiculohorimetro findByVeiculo(Veiculo veiculo){
		return veiculohorimetroDAO.findByVeiculo(veiculo);
	}	
	
	public Report createReportAnalitico(AnaliticoFiltro filtro) {
		List<AnaliticoSinteticoReportBean> lista = new ArrayList<AnaliticoSinteticoReportBean>();
		List<Veiculohorimetro>listaajuste = null; 
		listaajuste = this.findAnalitico(filtro); 
		AnaliticoSinteticoReportBean beanAnalitico = null;
		for (Veiculohorimetro ajuste : listaajuste) {
			beanAnalitico = new AnaliticoSinteticoReportBean();
			if(ajuste.getVeiculo().getColaborador() != null){
				beanAnalitico.setCooperado(ajuste.getVeiculo().getColaborador().getNome());
			}
			if(ajuste.getVeiculo().getVeiculomodelo() != null){
				beanAnalitico.setModelo(ajuste.getVeiculo().getVeiculomodelo().getNome());
			}
			beanAnalitico.setPlaca(ajuste.getVeiculo().getPlaca());
			beanAnalitico.setPrefixo(ajuste.getVeiculo().getPrefixo() != null ? ajuste.getVeiculo().getPrefixo() : " ");
			beanAnalitico.setTipoajuste(ajuste.getHorimetrotipoajuste().getNome());
			beanAnalitico.setDtajuste(ajuste.getDtentrada());
			beanAnalitico.setHorimetroanterior(this.getHorimetroanterior(ajuste));
			beanAnalitico.setHorimetroposterior(ajuste.getHorimetronovo());
			beanAnalitico.setHorimetroatual((ajuste.getVeiculo().getVwveiculo().getHorimetroatual())== null? 0:ajuste.getVeiculo().getVwveiculo().getHorimetroatual());
			beanAnalitico.setHorimetroentrada((ajuste.getVeiculo().getHorimetroinicial() == null? 0: ajuste.getVeiculo().getHorimetroinicial()));
			beanAnalitico.setHorimetroreal(ajuste.getVeiculo().getVwveiculo().getHorimetroreal());
			beanAnalitico.setHorimetrorodado(ajuste.getVeiculo().getVwveiculo().getHorimetrorodado());
			lista.add(beanAnalitico);
		}
		if (lista == null || lista.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report = new Report("veiculo/relAnalitico");
			report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
			report.addParameter("LISTA", lista);
			report.addSubReport("SUBANALITICO", new Report("veiculo/relAnaliticoSub"));	                     
			return report;
		}
	}
	
	private Double getHorimetroanterior(Veiculohorimetro ajuste) {
		return veiculohorimetroDAO.getHorimetroanterior(ajuste);
	}
	
	public Report createReportSintetico(AnaliticoFiltro filtro) {
		List<AnaliticoSinteticoReportBean> lista = new ArrayList<AnaliticoSinteticoReportBean>();
		List<Veiculohorimetro>listaajuste = null;	
		listaajuste = this.findAnalitico(filtro); 				
		Integer veiculo = 0;
		AnaliticoSinteticoReportBean beanAnalitico = null;
		for (Veiculohorimetro ajuste : listaajuste) {
			if(!veiculo.equals(ajuste.getVeiculo().getCdveiculo())){
				beanAnalitico = new AnaliticoSinteticoReportBean();
				if(ajuste.getVeiculo().getColaborador() != null){
					beanAnalitico.setCooperado(ajuste.getVeiculo().getColaborador().getNome());
				}
				if(ajuste.getVeiculo().getVeiculomodelo() != null){
					beanAnalitico.setModelo(ajuste.getVeiculo().getVeiculomodelo().getNome());
				}
				beanAnalitico.setPlaca(ajuste.getVeiculo().getPlaca());
				beanAnalitico.setPrefixo(ajuste.getVeiculo().getPrefixo());
				beanAnalitico.setHorimetroatual((ajuste.getVeiculo().getVwveiculo().getHorimetroatual()) == null? 0:ajuste.getVeiculo().getVwveiculo().getHorimetroatual());
				beanAnalitico.setHorimetroentrada((ajuste.getVeiculo().getHorimetroinicial() == null? 0: ajuste.getVeiculo().getHorimetroinicial()));
				beanAnalitico.setHorimetroreal(ajuste.getVeiculo().getVwveiculo().getHorimetroreal());
				beanAnalitico.setHorimetrorodado(ajuste.getVeiculo().getVwveiculo().getHorimetrorodado());
				lista.add(beanAnalitico);
				veiculo = ajuste.getVeiculo().getCdveiculo();
			}
		}
		if (lista == null || lista.size() == 0) {
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		}else{
			Report report = new Report("veiculo/relSintetico");
			report.addParameter("PERIODO",SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
			report.addParameter("LISTA", lista);
			report.addSubReport("SUBSINTETICO", new Report("veiculo/relSinteticoSub"));	                     
			return report;
		}
	}
	
	public List<Veiculohorimetro> findAnalitico(AnaliticoFiltro filtro){
		return veiculohorimetroDAO.findAnalitico(filtro);
	}

	@Override
	protected ListagemResult<Veiculohorimetro> findForExportacao(
			FiltroListagem filtro) {
		ListagemResult<Veiculohorimetro> listagemResult = veiculohorimetroDAO.findForExportacao(filtro);
		List<Veiculohorimetro> lista = listagemResult.list();
		
		adicionaHorimetroAtualRegistros(lista);
		
		return listagemResult;
	}
}
