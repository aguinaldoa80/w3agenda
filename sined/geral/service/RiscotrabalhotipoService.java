package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Riscotrabalhotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RiscotrabalhotipoService extends GenericService<Riscotrabalhotipo> {	
	
	/* singleton */
	private static RiscotrabalhotipoService instance;
	public static RiscotrabalhotipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RiscotrabalhotipoService.class);
		}
		return instance;
	}
	
}
