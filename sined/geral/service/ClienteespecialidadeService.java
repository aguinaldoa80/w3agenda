package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clienteespecialidade;
import br.com.linkcom.sined.geral.dao.ClienteespecialidadeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class ClienteespecialidadeService extends GenericService<Clienteespecialidade> {
	
	private ClienteespecialidadeDAO clienteespecialidadeDAO;
	
	public void setClienteespecialidadeDAO(ClienteespecialidadeDAO clienteespecialidadeDAO) {
		this.clienteespecialidadeDAO = clienteespecialidadeDAO;
	}
	
	public List<Clienteespecialidade> findByCliente(Cliente cliente){
		return clienteespecialidadeDAO.findByCliente(cliente);
	}
}