package br.com.linkcom.sined.geral.service;

import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Expedicaoiteminspecao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExpedicaoitemService extends GenericService<Expedicaoitem> {

	private ExpedicaoitemDAO expedicaoitemDAO;
	private ExpedicaoiteminspecaoService expedicaoiteminspecaoService;
	
	private static ExpedicaoitemService instance;
	
	public static ExpedicaoitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExpedicaoitemService.class);
		}
		return instance;
	}
	
	public void setExpedicaoitemDAO(ExpedicaoitemDAO expedicaoitemDAO) {
		this.expedicaoitemDAO = expedicaoitemDAO;
	}
	public void setExpedicaoiteminspecaoService(ExpedicaoiteminspecaoService expedicaoiteminspecaoService) {
		this.expedicaoiteminspecaoService = expedicaoiteminspecaoService;
	}


	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#findByVendaNotFaturada(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/02/2013
	 */
	public List<Expedicaoitem> findByVendaNotFaturada(Venda venda) {
		return expedicaoitemDAO.findByVendaNotFaturada(venda);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#findForFaturamento(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/02/2013
	 */
	public List<Expedicaoitem> findForFaturamento(String whereIn) {
		return expedicaoitemDAO.findForFaturamento(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#updateFaturada(Expedicaoitem expedicaoitem, Boolean faturada)
	 *
	 * @param expedicaoitem
	 * @param faturada
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public void updateFaturada(Expedicaoitem expedicaoitem, Boolean faturada) {
		expedicaoitemDAO.updateFaturada(expedicaoitem, faturada);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#haveNotFaturada(Expedicao expedicao)
	 *
	 * @param expedicao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveNotFaturada(Expedicao expedicao) {
		return expedicaoitemDAO.haveNotFaturada(expedicao);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#haveFaturada(String whereInVenda)
	 *
	 * @param whereInVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveFaturada(String whereInVenda) {
		return expedicaoitemDAO.haveFaturada(whereInVenda);
	}

	/**
	 * M�todo que salva os items da expedi��o com a inspe��o
	 *
	 * @param expedicaoitem
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void saveExpedicaoitemMaisListaInspecao(Expedicaoitem expedicaoitem) {
		saveOrUpdateNoUseTransaction(expedicaoitem);
		if(expedicaoitem.getListaInspecao() != null && expedicaoitem.getListaInspecao().size() > 0)
			for (Expedicaoiteminspecao expedicaoiteminspecao : expedicaoitem.getListaInspecao()) 
				expedicaoiteminspecaoService.saveOrUpdateNoUseTransaction(expedicaoiteminspecao);
		
	}
	
	/**
	 * 
	 * @param listaExpedicaoitem
	 */
	public void confirmarAdicaoVenda(List<Expedicaoitem> listaExpedicaoitem) {
		expedicaoitemDAO.confirmarAdicaoVenda(listaExpedicaoitem);
	}
	
	/**
	 * M�todo que traz a matriz de expedicoes itens separada por clientes para compor o relat�rio de expedicaoitem
	 *
	 * @param expedicao
	 * @author Rafael Patr�cio
	 * @since 18/01/2014
	 */
	public LinkedList<LinkedList<Expedicaoitem>> carregaDadosEmitirExpedicao(Expedicao expedicao){
		
		//Recupero os dados do relat�rio no DAO
		List<Expedicaoitem> listaBean = expedicaoitemDAO.carregaDadosEmitirExpedicao(expedicao);
		
		//Como ser� gerado uma caixa com informa��es separadas de cada cliente eu farei a separa��o colocando Expedicaoitem de clientes diferentes em 
		//listas diferentes
		LinkedList<LinkedList<Expedicaoitem>> listasPorCliente= new LinkedList<LinkedList<Expedicaoitem>>();
		
		for(Expedicaoitem expedicaoitem : listaBean){
			
			boolean inserido = false;
			
			//Se j� existe uma lista pra esse cliente eu insiro a expedicaoitem nela
			if(listasPorCliente!=null && !listasPorCliente.isEmpty()){
				for(List<Expedicaoitem> lista : listasPorCliente){
					if(expedicaoitem.getCliente().equals(lista.get(0).getCliente())){
							lista.add(expedicaoitem);
							inserido = true;
					}
				}
			}
			
			//Se n�o eu estr�io uma lista nova
			if(!inserido){
				LinkedList<Expedicaoitem> novaLista = new LinkedList<Expedicaoitem>();
				novaLista.add(expedicaoitem);
				listasPorCliente.add(novaLista);
			}
				
		}
		return listasPorCliente;
	}
	
	public List<Expedicaoitem> findByWhereInNota(String whereInNota,boolean isBuscaCancelados){
		return expedicaoitemDAO.findByWhereInNota(whereInNota,isBuscaCancelados);
	}
	
	public List<Expedicaoitem> findByWhereInExpedicao(String whereInExpedicao){
		return expedicaoitemDAO.findByWhereInExpedicao(whereInExpedicao);
	}
	
	public List<Expedicaoitem> findByCdExpedicao(Integer cdExpedicao){
		return expedicaoitemDAO.findByCdExpedicao(cdExpedicao);
	}
	
	/**
	 * 
	 * @param listaExpedicaoitem 
	 * @param bean
	 * @return
	 */
	public List<Expedicaoitem> findByExpedicaoForDelete(Expedicao expedicao, List<Expedicaoitem> listaExpedicaoitem) {
		return expedicaoitemDAO.findByExpedicao(expedicao,listaExpedicaoitem);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ExpedicaoitemDAO#findForConferenciaMateriaisFromExpedicao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 17/10/2016
	* @author Luiz Fernando
	*/
	public List<Expedicaoitem> findForConferenciaMateriaisFromExpedicao(String whereIn) {
		return expedicaoitemDAO.findForConferenciaMateriaisFromExpedicao(whereIn);
	}
	
	/**
	 * @return retorla lista de expedicao diferente do id passado
	 */
	public List<Expedicaoitem> findByColetaMaterial(Integer cdColetaMaterial, Material material, Integer cdExpeticaoItem, String whereInExcludeExpedicao) {
		return expedicaoitemDAO.findByColetaMaterial(cdColetaMaterial, material, cdExpeticaoItem, whereInExcludeExpedicao);
	}
	public List<Expedicaoitem> findByColeta(Coleta coleta, Expedicao expedicao) {
		return expedicaoitemDAO.findByColetaMaterial(coleta.getCdcoleta(),expedicao.getCdexpedicao());
	}
	public List<Expedicaoitem> findForConferenciaPneuFromExpedicao(String whereIn) {
		return expedicaoitemDAO.findForConferenciaPneuFromExpedicao(whereIn);
	}
	public List<Expedicaoitem> findForFinalizaExpedicao(String whereIn) {
		return expedicaoitemDAO.findForFinalizaExpedicao(whereIn);
	}

	public boolean existeVendasDistintas(String whereIn) {
		return expedicaoitemDAO.existeVendasDistintas(whereIn);
	}
}
