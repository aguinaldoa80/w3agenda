package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.auxiliar.ImpostoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.dao.FaixaimpostoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FaixaimpostoService extends GenericService<Faixaimposto> {

	private FaixaimpostoDAO faixaimpostoDAO;
	private NotaService notaService;
	private ClienteService clienteService;
	private ParametrogeralService parametrogeralService;
	
	public void setFaixaimpostoDAO(FaixaimpostoDAO faixaimpostoDAO) {this.faixaimpostoDAO = faixaimpostoDAO;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FaixaimpostoDAO#findByControleMunicipio
	 * 
	 * @param controle
	 * @param municipio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Faixaimposto findByControleMunicipio(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle, Municipio municipio, Codigotributacao codigotributacao) {
		return faixaimpostoDAO.findByControleMunicipio(empresa, cliente, controle, municipio, codigotributacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FaixaimpostoDAO#findByControle
	 * 
	 * @param controle
	 * @return
	 * @author Marden Silva
	 */
	public List<Faixaimposto> findByControle(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle){
		return faixaimpostoDAO.findByControle(empresa, cliente, controle);
	}		
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FaixaimpostoDAO#findByControleAndUf
	 * 
	 * @param controle
	 * @param ufOrigem
	 * @param ufDestino
	 * @return
	 * @author Marden Silva
	 * @param empresa 
	 */
	public Faixaimposto findByControleAndUf(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle, Uf ufOrigem, Uf ufDestino){
		return faixaimpostoDAO.findByControleAndUf(empresa, cliente, controle, ufOrigem, ufDestino);
	}
	
	public ImpostoVO calculaValorFaixaImposto(Cliente cliente, NotaTipo notaTipo, Faixaimposto faixaImposto, Money baseCalculo, Double basecalculopercentual, Codigotributacao codigotributacao) {
		return calculaValorFaixaImposto(cliente, notaTipo, faixaImposto, baseCalculo, basecalculopercentual, codigotributacao, null);
	}
	
	/**
	 * Calcula o imposto com base nas informa��es do Faixaimposto
	 * 
	 * @param notaTipo
	 * @param faixaImposto
	 * @param baseCalculo
	 * @param basecalculopercentual 
	 * @return ImpostoVO
	 * @author Marden Silva
	 */
	public ImpostoVO calculaValorFaixaImposto(Cliente cliente, NotaTipo notaTipo, Faixaimposto faixaImposto, Money baseCalculo, Double basecalculopercentual, Codigotributacao codigotributacao, Nota bean) {		
		
		ImpostoVO imposto = null;
		
		if(faixaImposto != null){
			double baseImposto = baseCalculo.getValue().doubleValue();
			double aliquotaImposto = faixaImposto.getValoraliquota().getValue().doubleValue();
			double valorImposto = 0d;
			boolean incide = false;
			boolean cumulativoCobrado = false;	
			
			if(faixaImposto.getBasecalculopercentual() != null && faixaImposto.getBasecalculopercentual() != 100.0){
				baseImposto = (baseImposto * faixaImposto.getBasecalculopercentual()) / 100.0;
			} else if(basecalculopercentual != null && basecalculopercentual != 100.0){
				baseImposto = (baseImposto * basecalculopercentual) / 100.0;
			}
			
			if(faixaImposto.getCumulativo() != null && faixaImposto.getCumulativo()){
				if(bean != null){
					bean.setExisteImpostoCumulativo(true);
				}
				Cliente clienteTrib = clienteService.loadForCalculoTributacao(cliente); 
				boolean valorCumulativoCobrado = notaService.isValorCumulativoCobrado(faixaImposto.getControle(), notaTipo, clienteTrib, faixaImposto.getCumulativotipo(), bean);
				if (!valorCumulativoCobrado){	
					Money somaNotas = notaService.getValorCumulativoMesCorrente(faixaImposto.getControle(), notaTipo, clienteTrib, faixaImposto.getCumulativotipo(), codigotributacao, bean);
					somaNotas = somaNotas.add(new Money(baseImposto));
					
					if (faixaImposto.getValorminimoincidir() != null &&
						somaNotas.getValue().doubleValue() >= faixaImposto.getValorminimoincidir().getValue().doubleValue()){
						baseImposto = somaNotas.getValue().doubleValue();
						cumulativoCobrado = true;
					}
				} else {
					valorImposto = (baseImposto*aliquotaImposto)/100;
					incide = true;
				}
			}
			
			if (faixaImposto.getValorminimoincidir() != null &&
				baseImposto >= faixaImposto.getValorminimoincidir().getValue().doubleValue())
					incide = true;
			
			if (faixaImposto.getValorminimocobrar() == null ||
					("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INCIDIR_CLIENTE_PREVALECE_MINIMO_COBRAR)) && Boolean.TRUE.equals(cliente.getIncidiriss())) ||
					(faixaImposto.getValorminimocobrar() != null && baseImposto >= faixaImposto.getValorminimocobrar().getValue().doubleValue())){
				valorImposto = (baseImposto*aliquotaImposto)/100;			
			}

			if (valorImposto > 0){
				imposto = new ImpostoVO();
				imposto.setBasecalculo(new Money(baseImposto));
				imposto.setAliquota(new Money(aliquotaImposto));
				imposto.setValor(new Money(valorImposto));
				imposto.setIncide(incide);
				imposto.setCumulativoCobrado(cumulativoCobrado);
				
				if (faixaImposto.getValorminimoincidir() != null &&
						baseImposto >= faixaImposto.getValorminimoincidir().getValue().doubleValue() && faixaImposto.getAliquotaretencao() != null && 
						faixaImposto.getAliquotaretencao().getValue().doubleValue() > 0){
					imposto.setAliquotaretencao(faixaImposto.getAliquotaretencao());
				}
			}
		} else {
			double baseImposto = baseCalculo.getValue().doubleValue();
			if(basecalculopercentual != null && basecalculopercentual != 100.0){
				baseImposto = (baseImposto * basecalculopercentual) / 100.0;
			}
			
			imposto = new ImpostoVO();
			imposto.setBasecalculo(new Money(baseImposto));
			imposto.setAliquota(new Money());
			imposto.setValor(new Money());
			imposto.setIncide(false);
			imposto.setCumulativoCobrado(false);
		}
		
		return imposto;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdfaixaimposto
	 * @param controle
	 * @param uf
	 * @param ufdestino
	 * @param municipio 
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/07/2016
	 */
	public boolean haveFaixaimpostoIgualCliente(Integer cdfaixaimposto, Faixaimpostocontrole controle, Uf uf, Uf ufdestino, Municipio municipio, Cliente cliente) {
		return faixaimpostoDAO.haveFaixaimpostoIgualCliente(cdfaixaimposto, controle, uf, ufdestino, municipio, cliente);
	}

}
