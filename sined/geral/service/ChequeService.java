package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequedevolucaomotivo;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Chequerelacao;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.dao.ChequeDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.Aux_Chequeorigem;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ProtocoloRetiradaChequeBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ProtocoloRetiradaChequeFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.ChequeBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ChequeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ChequeService extends GenericService<Cheque>{

	private ChequeDAO chequeDAO;
	private VendapagamentoService vendapagamentoService;
	private MovimentacaoService movimentacaoService;
	private ChequehistoricoService chequehistoricoService;
	private DocumentoService documentoService;
	private ChequerelacaoService chequerelacaoService;
	private ContaService contaService;
	private EmpresaService empresaService;
	private ReportTemplateService reporttemplateService;
	
	public void setChequeDAO(ChequeDAO chequeDAO) {this.chequeDAO = chequeDAO;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setChequerelacaoService(ChequerelacaoService chequerelacaoService) {this.chequerelacaoService = chequerelacaoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setReporttemplateService(ReportTemplateService reporttemplateService) {this.reporttemplateService = reporttemplateService;}
	
	public Boolean verificarDuplicidadeCheque(Cheque cheque){
		return chequeDAO.verificarDuplicidadeCheque(cheque);
	}
	
	/**
	 * M�todo que verifica se em uma lista existe cheque duplicado
	 *
	 * @param listaCheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isDuplicidadeCheque(List<Cheque> listaCheque) {
		if(listaCheque != null && !listaCheque.isEmpty()){
			Integer i = 0;
			Integer j = 0;
			for(Cheque bean : listaCheque){
				j = 0;
				for(Cheque cheque : listaCheque){
					if(i != j){
						boolean agencia = 
							(cheque.getAgencia() == null && bean.getAgencia() == null) ||
							(cheque.getAgencia() != null && bean.getAgencia() != null && cheque.getAgencia().equals(bean.getAgencia()));
						boolean banco = 
							(cheque.getBanco() == null && bean.getBanco() == null) ||
							(cheque.getBanco() != null && bean.getBanco() != null && cheque.getBanco().equals(bean.getBanco()));
						boolean conta = 
							(cheque.getConta() == null && bean.getConta() == null) ||
							(cheque.getConta() != null && bean.getConta() != null && cheque.getConta().equals(bean.getConta()));
						boolean numero = 
							(cheque.getNumero() == null && bean.getNumero() == null) ||
							(cheque.getNumero() != null && bean.getNumero() != null && cheque.getNumero().equals(bean.getNumero()));
						boolean empresa = 
							(cheque.getEmpresa() == null && bean.getEmpresa() == null) ||
							(cheque.getEmpresa() != null && bean.getEmpresa() != null && cheque.getEmpresa().equals(bean.getEmpresa()));
						
						
						if(agencia && conta && banco && numero && empresa){
							return Boolean.TRUE;
						}
					}
					j++;
				}
				i++;
			}
		}
		
		return Boolean.FALSE;
	}

	/**
	 * M�todo que monta a rastreabilidade do cheque
	 *
	 * @see br.com.linkcom.sined.geral.service.VendapagamentoService#findByCheque(Cheque cheque
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findByCheque(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Aux_Chequeorigem> montaRastreabilidade(Cheque cheque) {
		List<Aux_Chequeorigem> lista = new ArrayList<Aux_Chequeorigem>();
		if(cheque.getCdcheque() != null){
			List<Vendapagamento> listaVendapagamento = vendapagamentoService.findByCheque(cheque);
			List<Movimentacao> listaMovimentacao = movimentacaoService.findByCheque(cheque);
			
			Aux_Chequeorigem auxChequeorigem;
			if(listaVendapagamento != null && !listaVendapagamento.isEmpty()){
				for(Vendapagamento vendapagamento : listaVendapagamento){
					auxChequeorigem = new Aux_Chequeorigem();
					auxChequeorigem.setTipo(Aux_Chequeorigem.VENDA);
					auxChequeorigem.setCdvenda(vendapagamento.getVenda().getCdvenda());
					auxChequeorigem.setData(vendapagamento.getDataparcela());
					lista.add(auxChequeorigem);
				}
			}
			if(listaMovimentacao != null && !listaMovimentacao.isEmpty()){
				for(Movimentacao movimentacao : listaMovimentacao){
					auxChequeorigem = new Aux_Chequeorigem();
					auxChequeorigem.setTipo(Aux_Chequeorigem.MOVIMENTACAO_FINANCEIRA);
					auxChequeorigem.setCdmovimentacao(movimentacao.getCdmovimentacao());
					auxChequeorigem.setData(movimentacao.getDtmovimentacao());
					lista.add(auxChequeorigem);
				}
			}			
		}
		return lista;
	}
	
	public List<Cheque> findForAutocompleteCompleto(String q){
		return chequeDAO.findForAutocompleteCompleto(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#findForAutocompleteCompletoGerarMovimentacao(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findForAutocompleteCompletoGerarMovimentacao(String q){
		WebRequestContext request = NeoWeb.getRequestContext(); 
		Conta conta = null;
		Contatipo contatipo = null;
		if(request.getSession().getAttribute("contaGerarmovimentacaoCheque") != null){
			conta  = (Conta) request.getSession().getAttribute("contaGerarmovimentacaoCheque");
		}
		if(request.getSession().getAttribute("contatipoGerarmovimentacaoCheque") != null){
			contatipo  = (Contatipo) request.getSession().getAttribute("contatipoGerarmovimentacaoCheque");
		}
		if(conta == null || contatipo == null) return new ArrayList<Cheque>();
		return chequeDAO.findForAutocompleteCompletoGerarMovimentacao(q, conta, contatipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#findForAutocompleteCompletoContapagar(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findForAutocompleteCompletoContapagar(String q){
		return chequeDAO.findForAutocompleteCompletoContapagar(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Cheque> findForAutocompleteCompletoContareceber(String q){
		return chequeDAO.findForAutocompleteCompletoContareceber(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete(), SinedUtil.getParametroRequisicaoAutocomplete("cddocumento"));
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#getValor(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValor(Cheque cheque) {
		return chequeDAO.getValor(cheque);
	}
	
	/**
	 * M�todo gera relat�rio de cheques de acordo com filtro
	 *
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public IReport gerarPDF(ChequeFiltro filtro) {
		Report report = new Report("/financeiro/cheque");
		report.addSubReport("CHEQUE_HISTORICO_SUB", new Report("/financeiro/cheque_historico_sub"));
		
		List<Cheque> lista = this.findForReport(filtro);
		
		report.setDataSource(lista);
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#findForReport(ChequeFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findForReport(ChequeFiltro filtro) {
		return chequeDAO.findForReport(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#isResgatado(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isResgatado(Cheque cheque) {
		return chequeDAO.isResgatado(cheque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequeDAO#findCheques(String idsCheques)
	 *
	 * @param idsCheques
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findCheques(String idsCheques) {
		return chequeDAO.findCheques(idsCheques);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#findChequeWithEmpresa(String whereIn)
	*
	* @param idsCheques
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Cheque> findChequeWithEmpresa(String idsCheques) {
		return chequeDAO.findChequeWithEmpresa(idsCheques);
	}
	
	/**
	 * 
	 * @param q
	 * @author Thiago Clemente
	 * 
	 */
	public List<Cheque> findForAutocompleteMovimentacao(String q) {
		return chequeDAO.findForAutocompleteMovimentacao(q);
	}
	
	public Cheque carregaCheque(Cheque cheque) {
		return chequeDAO.carregaCheque(cheque);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#isNotSituacao(String whereIn, Chequesituacao chequesituacao)
	*
	* @param whereIn
	* @param chequesituacao
	* @return
	* @since 22/07/2014
	* @author Luiz Fernando
	*/
	public boolean isNotSituacao(String whereIn, Chequesituacao... chequesituacao) {
		return chequeDAO.isNotSituacao(whereIn, chequesituacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#isVinculoCredito(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoCredito(String whereIn) {
		return chequeDAO.isVinculoCredito(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#isVinculoDebito(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 25/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoDebito(String whereIn) {
		return chequeDAO.isVinculoDebito(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#updateSituacao(String whereIn, Chequesituacao chequesituacao)
	*
	* @param whereIn
	* @param chequesituacao
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public void updateSituacao(String whereIn, Chequesituacao chequesituacao) {
		chequeDAO.updateSituacao(whereIn, chequesituacao);
	}
	/**
	* M�todo que salva os cheques substitutos
	* atualiza a situa��o das movimenta��es relacionada aos cheques antigos para CANCELADA
	* cria novas movimenta��es com as origens das movimenta��o antigas
	*
	* @param whereInChequeAntigo
	* @param listaChequesubstituicao
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void saveAndExecuteSubstituicaoCheque(final String whereInChequeAntigo, final List<Cheque> listaChequesubstituicao) {
		if(SinedUtil.isListNotEmpty(listaChequesubstituicao) || StringUtils.isNotEmpty(whereInChequeAntigo)){
			final Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
			final Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			
			List<Cheque> listaChequeAntigo = findCheques(whereInChequeAntigo);
			if(SinedUtil.isListEmpty(listaChequeAntigo))
				throw new SinedException("Nehum cheque encontrado.");
			
			List<Movimentacao> listaMovimentacaoChequeAntigo = movimentacaoService.findForSubistituirByCheque(whereInChequeAntigo);
			if(SinedUtil.isListNotEmpty(listaMovimentacaoChequeAntigo)){
				for(Movimentacao movimentacao : listaMovimentacaoChequeAntigo){
					if(movimentacao.getCheque() != null){
						for(Cheque cheque : listaChequeAntigo){
							if(cheque.equals(movimentacao.getCheque())){
								if(cheque.getListaMovimentacaoSubstituicao() == null)
									cheque.setListaMovimentacaoSubstituicao(new ArrayList<Movimentacao>());
								cheque.getListaMovimentacaoSubstituicao().add(movimentacao);
							}
						}
					}
				}
			}
			
			final List<Documento> listaContapagar = documentoService.findContaForSubstituicaoByCheque(whereInChequeAntigo, Documentoclasse.OBJ_PAGAR,  Documentoacao.BAIXADA, Documentoacao.CANCELADA);
			final List<Vendapagamento> listaVendapagamento = vendapagamentoService.findForSubstituicaoByCheque(whereInChequeAntigo, Documentoacao.DEFINITIVA, Documentoacao.PREVISTA);
			
			final List<Cheque> listaChequeWithMovimentacao = listaChequeAntigo;			
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
				public Object doInTransaction(TransactionStatus status) {
					Cheque[] cheques = new Cheque[listaChequesubstituicao.size()];
					String linkObsChequeAntigo = getLinkChequeHistorico(whereInChequeAntigo, "Cheque");
					String linkObsRelChequeAntigo = getLinkChequeHistoricoRelatorio(whereInChequeAntigo, "Cheque");
					
					int i = 0;
					for (Cheque chequeSubstituto : listaChequesubstituicao){
						chequeSubstituto.setChequesituacao(Chequesituacao.PREVISTO);
						saveOrUpdateNoUseTransaction(chequeSubstituto);
						
						cheques[i] = chequeSubstituto;
						i++;
						
						for(Cheque chequeAntigo : listaChequeWithMovimentacao){
							Chequerelacao chequerelacao = new Chequerelacao();
							chequerelacao.setChequenovo(chequeSubstituto);
							chequerelacao.setChequeantigo(chequeAntigo);
							chequerelacaoService.saveOrUpdateNoUseTransaction(chequerelacao);
						}
					}
					
					for(Cheque cheque : listaChequeWithMovimentacao){
						if(SinedUtil.isListNotEmpty(cheque.getListaMovimentacaoSubstituicao())){
							String whereInMov = CollectionsUtil.listAndConcatenate(cheque.getListaMovimentacaoSubstituicao(), "cdmovimentacao", ",");
							movimentacaoService.updateSituacaoCanceladaBySubstituicaoCheque(whereInMov);
							for (Cheque chequeSubstituto : listaChequesubstituicao){
								Movimentacao movimentacaoNovo = movimentacaoService.createMovimentacaoBySubstituicaoCheque(cheque.getListaMovimentacaoSubstituicao(), chequeSubstituto);
								movimentacaoService.saveOrUpdateNoUseTransaction(movimentacaoNovo);
								updateSituacao(chequeSubstituto.getCdcheque().toString(), Chequesituacao.BAIXADO);
								chequeSubstituto.setMovimentacaoNovo(movimentacaoNovo);
							}
						}
					}
					
					updateSituacao(whereInChequeAntigo, Chequesituacao.SUBSTITUIDO);
					
					StringBuilder whereInChequeSubstituto = new StringBuilder();
					if(listaChequesubstituicao.size() > 1){
						if(SinedUtil.isListNotEmpty(listaContapagar)){
							String whereInContapagar = CollectionsUtil.listAndConcatenate(listaContapagar, "cddocumento", ",");
							documentoService.updateContaRemoveCheque(whereInContapagar);
						}
						if(SinedUtil.isListNotEmpty(listaVendapagamento)){
							String whereInVendapagamento = CollectionsUtil.listAndConcatenate(listaVendapagamento, "cdvendapagamento", ",");
							vendapagamentoService.updateVendapagamentoRemoveCheque(whereInVendapagamento);
						}
						for(Cheque chequeSubstituto : cheques){
							whereInChequeSubstituto.append(chequeSubstituto.getCdcheque()).append(",");
							
							Chequehistorico chequehistorico = new Chequehistorico();
							chequehistorico.setCheque(chequeSubstituto);
							chequehistorico.setChequesituacao(chequeSubstituto.getChequesituacao());
							chequehistorico.setCdusuarioaltera(cdusuarioaltera);
							chequehistorico.setDtaltera(dtaltera);
							chequehistorico.setObservacao("Substitui��o do cheque: " + linkObsChequeAntigo);
							chequehistorico.setObservacaorelatorio("Substitui��o do cheque: " + linkObsRelChequeAntigo);
							chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
						}
					}else {
						for(Cheque chequeSubstituto : cheques){
							StringBuilder obsChequeSubstituto = new StringBuilder();
							StringBuilder obsRelChequeSubstituto = new StringBuilder();
							whereInChequeSubstituto.append(chequeSubstituto.getCdcheque()).append(",");
							if(SinedUtil.isListNotEmpty(listaContapagar)){
								String whereInContapagar = CollectionsUtil.listAndConcatenate(listaContapagar, "cddocumento", ",");
								documentoService.updateContaChequeSubstituto(whereInContapagar, chequeSubstituto);
								obsChequeSubstituto.append("<br>Associa��o a Conta a Pagar: " + getLinkChequeHistorico(whereInContapagar, "Contapagar"));
								obsRelChequeSubstituto.append("\nAssocia��o a Conta a Pagar: " + getLinkChequeHistoricoRelatorio(whereInContapagar, "Contapagar"));
								
							}
							if(SinedUtil.isListNotEmpty(listaVendapagamento)){
								String whereInVendapagamento = CollectionsUtil.listAndConcatenate(listaVendapagamento, "cdvendapagamento", ",");
								String whereInVenda = CollectionsUtil.listAndConcatenate(listaVendapagamento, "venda.cdvenda", ",");
								vendapagamentoService.updateVendapagamentoChequeSubstituto(whereInVendapagamento, chequeSubstituto);
								obsChequeSubstituto.append("<br>Associa��o a Venda: " + getLinkChequeHistorico(whereInVenda, "Venda"));
								obsRelChequeSubstituto.append("\nAssocia��o a Venda: " + getLinkChequeHistoricoRelatorio(whereInVenda, "Venda"));
							}
							if(chequeSubstituto.getMovimentacaoNovo() != null && chequeSubstituto.getMovimentacaoNovo().getCdmovimentacao() != null){
								obsChequeSubstituto.append("<br>Movimenta��es: " + getLinkChequeHistorico(chequeSubstituto.getMovimentacaoNovo().getCdmovimentacao().toString(), "Movimentacao"));
								obsRelChequeSubstituto.append("\nMovimenta��es: " + getLinkChequeHistoricoRelatorio(chequeSubstituto.getMovimentacaoNovo().getCdmovimentacao().toString(), "Movimentacao"));
							}
							
							Chequehistorico chequehistorico = new Chequehistorico();
							chequehistorico.setCheque(chequeSubstituto);
							chequehistorico.setChequesituacao(chequeSubstituto.getChequesituacao());
							chequehistorico.setCdusuarioaltera(cdusuarioaltera);
							chequehistorico.setDtaltera(dtaltera);
							chequehistorico.setObservacao("Substitui��o do cheque: " + linkObsChequeAntigo + ". " + obsChequeSubstituto);
							chequehistorico.setObservacaorelatorio("Substitui��o do cheque: " + linkObsRelChequeAntigo + ". " + obsRelChequeSubstituto);
							chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
						}
					}
					
					String linkObsChequeSubstituto = getLinkChequeHistorico(whereInChequeSubstituto.toString().substring(0, whereInChequeSubstituto.toString().length()-1), "Cheque");
					
					for(Cheque chequeAntigo : listaChequeWithMovimentacao){
						Chequehistorico chequehistorico = new Chequehistorico();
						chequehistorico.setCheque(chequeAntigo);
						chequehistorico.setChequesituacao(chequeAntigo.getChequesituacao());
						chequehistorico.setCdusuarioaltera(cdusuarioaltera);
						chequehistorico.setDtaltera(dtaltera);
						chequehistorico.setObservacao("Novo(s) cheque(s): " + linkObsChequeSubstituto);
						chequehistoricoService.saveOrUpdate(chequehistorico);
					}
					return cheques;
				}
			});		
		}
	}
	
	private String getLinkChequeHistorico(String whereIn, String nomeFuncao){
		return getLinkChequeHistorico(whereIn, nomeFuncao, false);
	}
	
	private String getLinkChequeHistoricoRelatorio(String whereIn, String nomeFuncao){
		return getLinkChequeHistorico(whereIn, nomeFuncao, true);
	}
	
	/**
	* M�todo que cria o link de hist�rico do cheque
	*
	* @param whereIn
	* @param nomeFuncao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	private String getLinkChequeHistorico(String whereIn, String nomeFuncao, boolean obsRelatorio){
		String linkObs = "";
		if(StringUtils.isNotEmpty(whereIn) && StringUtils.isNotEmpty(nomeFuncao)){
			String[] ids = null;
			ids = whereIn.split(",");
			if(ids.length > 0){
				StringBuilder linkHtml = new StringBuilder();
				for(String id : ids){
					if(nomeFuncao.equals("Venda") && linkHtml.toString().contains(id)){
						continue;
					}
					if(obsRelatorio){
						linkHtml.append(id).append(",");
					}else {
						linkHtml.append("<a href=\"javascript:visualiza" + nomeFuncao + "(")
								.append(id)
								.append(");\">")
								.append(id)
								.append("</a>,");
					}
				}
				linkObs = linkHtml.toString().substring(0, linkHtml.toString().length()-1);
			}
		}
		return linkObs;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#isClienteDiferente(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 29/07/2014
	* @author Luiz Fernando
	*/
	public boolean isClienteDiferente(String whereIn) {
		return chequeDAO.isClienteDiferente(whereIn);
	}
	
	/**
	 * Estorna o cheque das movimenta��es passadas por par�metro.
	 *
	 * @param whereInMovimentacao
	 * @author Rodrigo Freitas
	 * @param documentoclasse 
	 * @param list 
	 * @since 05/02/2015
	 */
	public void estornaChequeByMovimentacao(Documentoclasse documentoclasse, List<Documento> listaDocumento, String whereInMovimentacao) {
		List<Cheque> listaCheque = this.findByMovimentacao(whereInMovimentacao);
		if(listaCheque != null && listaCheque.size() > 0){
			String whereInDocumento = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
			this.updateSituacao(CollectionsUtil.listAndConcatenate(listaCheque, "cdcheque", ","), Chequesituacao.PREVISTO);
			for (Cheque cheque : listaCheque) {
				if(cheque.getListaMovimentacao() != null && cheque.getListaMovimentacao().size() > 0){
					String documentoFuncao = "Contareceber";
					if(documentoclasse != null && documentoclasse.equals(Documentoclasse.OBJ_PAGAR)){
						documentoFuncao = "Contapagar";
					}
					String observacao = "Conta(s) estornada(s): " + this.getLinkChequeHistorico(whereInDocumento, documentoFuncao) + "<BR>" +
										"Movimenta��o cancelada: " + this.getLinkChequeHistorico(CollectionsUtil.listAndConcatenate(cheque.getListaMovimentacao(), "cdmovimentacao", ","), "Movimentacao");
					
					Chequehistorico chequehistorico = new Chequehistorico();
					chequehistorico.setCheque(cheque);
					chequehistorico.setChequesituacao(Chequesituacao.PREVISTO);
					chequehistorico.setObservacao(observacao);
					chequehistoricoService.saveOrUpdateNoUseTransaction(chequehistorico);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInMovimentacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/02/2015
	 */
	private List<Cheque> findByMovimentacao(String whereInMovimentacao) {
		return chequeDAO.findByMovimentacao(whereInMovimentacao);
	}
	
	/**
	* M�todo que retorna um cheque de acordo com banco, agencia, conta e numero
	*
	* @param cheque
	* @return
	* @since 13/03/2015
	* @author Luiz Fernando
	*/
	public Cheque getChequeForVenda(Cheque cheque) {
		if(cheque != null && cheque.getCdcheque() == null){
			List<Cheque> listaCheque = findChequeForVenda(cheque.getBanco(), cheque.getAgencia(), cheque.getConta(), cheque.getNumero());
			if(listaCheque != null && listaCheque.size() > 0){
				return listaCheque.get(0);
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#findChequeForVenda(Integer banco, Integer agencia, Integer conta, String numero)
	*
	* @param banco
	* @param agencia
	* @param conta
	* @param numero
	* @return
	* @since 13/03/2015
	* @author Luiz Fernando
	*/
	private List<Cheque> findChequeForVenda(Integer banco, Integer agencia,	Integer conta, String numero) {
		return chequeDAO.findChequeForVenda(banco, agencia,	conta, numero);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#getWhereInChequeClienteVendaNota(ChequeFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 01/06/2015
	* @author Luiz Fernando
	*/
	public String getWhereInChequeClienteVendaNota(ChequeFiltro filtro) {
		return chequeDAO.getWhereInChequeClienteVendaNota(filtro);
	}
	
	/**
	* M�todo que faz o update da situa��o do cheque e registra no hist�rico a justificativa
	*
	* @param whereIn
	* @param chequesituacao
	* @param justificativa
	* @since 30/10/2015
	* @author Luiz Fernando
	*/
	public void registrarCancelamentoOuEstorno(String whereIn, Chequesituacao chequesituacao, String justificativa){
		updateSituacao(whereIn, chequesituacao);
		
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			Chequehistorico chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(new Cheque(Integer.parseInt(id)));
			chequehistorico.setChequesituacao(chequesituacao);
			chequehistorico.setObservacao(justificativa);
			chequehistorico.setObservacaorelatorio(justificativa);
			chequehistoricoService.saveOrUpdate(chequehistorico);	
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ChequeDAO#getValorTotalCheques(Cliente cliente, Chequesituacao[] situacao)
	*
	* @param cliente
	* @param situacao
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public Money getValorTotalCheques(Cliente cliente, Chequesituacao... situacao) {
		return chequeDAO.getValorTotalCheques(cliente, situacao);
	}
	
	public boolean possuiChequePendente(Cliente cliente, Chequesituacao... situacao) {
		Money valor = getValorTotalCheques(cliente, situacao);
		return valor != null && valor.getValue().doubleValue() > 0;
	}
	
	/**
	* M�todo que seta as informa��es de 'recebido de' e 'pago a' do cheque
	*
	* @param listaCheque
	* @since 12/09/2016
	* @author Luiz Fernando
	*/
	public void setInformacoesRecebidoPago(List<Cheque> listaCheque) {
		if(SinedUtil.isListNotEmpty(listaCheque)){
			List<ChequeBean> lista = chequeDAO.findWithInformacoesRecebidoPago(CollectionsUtil.listAndConcatenate(listaCheque, "cdcheque", ","));
			if(SinedUtil.isListNotEmpty(lista)){
				for(ChequeBean bean : lista){
					if(bean.getCdcheque() != null){
						for(Cheque cheque : listaCheque){
							if(bean.getCdcheque().equals(cheque.getCdcheque())){
								cheque.setRecebidode(bean.getRecebidode());
								cheque.setPagoa(bean.getPagoa());
							}
						}
					}
				}
			}
		}
	}
	
	public void buscarInfoConta(WebRequestContext request, Conta conta) throws Exception{
		conta = contaService.loadConta(conta);
		View view = View.getCurrent();
		view.println("var chequebanco = '" + (conta.getBanco() != null && conta.getBanco().getNumero() != null ? conta.getBanco().getNumero() : "") + "';");
		view.println("var chequeagencia = '" + conta.getAgenciaComDVSomenteNumero() + "';");
		view.println("var chequeconta = '" + conta.getNumeroComDVSomenteNumero() + "';");
		
		String empresaWhereIn = request.getParameter("empresaWhereIn");
		String emitente = "";
		String cpfcnpj = "";
		Empresa empresa = null;
		if(StringUtils.isNotBlank(empresaWhereIn)){
			 String[] ids = empresaWhereIn.split(",");
			 if(ids.length == 1){
				 empresa = new Empresa(Integer.parseInt(ids[0]));
			 }
		}else if(conta.getListaContaempresa() != null && conta.getListaContaempresa().size() == 1){
			empresa = conta.getListaContaempresa().iterator().next().getEmpresa();
		}
		if(empresa != null){
			empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.razaosocial, empresa.cpf, empresa.cnpj");
			emitente = empresa != null && empresa.getRazaosocial() != null ? empresa.getRazaosocial() : "";
			cpfcnpj = empresa != null && empresa.getCpfOuCnpjValue() != null ? empresa.getCpfOuCnpjValue() : "";
		}
		view.println("var chequeemitente = '" + emitente + "';");
		view.println("var cpfcnpj = '" + cpfcnpj + "';");
	}
	
	public List<Cheque> findForCopiaChequeTemplate(String whereInCheques, String whereInMovimentacoes){
		return chequeDAO.findForCopiaChequeByMovimentacoes(whereInCheques, whereInMovimentacoes);
	}
	
	public ModelAndView ajaxTemplatesForCopiaCheque(WebRequestContext requestContext){
		List<ReportTemplateBean> templates = reporttemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_MOVIMENTACAO_CHEQUE);
		
		return new JsonModelAndView()
				.addObject("cdtemplate", templates.size() == 1? templates.get(0).getCdreporttemplate(): null)
				.addObject("qtdeTemplates", templates.size());
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaCheque(WebRequestContext request){
		String whereIn = request.getParameter("whereInCheques");
		String whereInMovimentacoes = request.getParameter("whereInMovimentacoes");
		String origemChamada = request.getParameter("origemChamada");
		
		if(!StringUtils.isEmpty(whereInMovimentacoes)){
			List<Cheque> cheques = this.findForCopiaChequeTemplate(null, whereInMovimentacoes);
			if(cheques == null || cheques.size() == 0){
				request.addError("Nenhum cheque encontrado para a(s) movimenta��o(�es) informada(s).");			
				SinedUtil.redirecionamento(request,"/financeiro/crud/Movimentacao");
				return null;
			}
		}
		
		if(!StringUtils.isEmpty(whereIn)){
			List<Cheque> listaCheques = this.findForCopiaChequeTemplate(whereIn, whereInMovimentacoes);
			if(SinedUtil.isListEmpty(listaCheques)){
				request.addError("Cheque(s) selecionado(s) n�o possui(em) movimenta��o(�es) relacionada(s).");
				if(origemChamada.equals("TRANSFERENCIA_INTERNA")){
					SinedUtil.redirecionamento(request,"/financeiro/process/GerarMovimentacao");
				}else if(origemChamada.equals("CHEQUE")){
					SinedUtil.redirecionamento(request,"/financeiro/crud/Cheque");					
				}
				
				return null;
			}			
		}
		
		List<ReportTemplateBean> templates = reporttemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_MOVIMENTACAO_CHEQUE);
		request.setAttribute("whereInCheques", whereIn);
		request.setAttribute("whereInMovimentacoes", whereInMovimentacoes);
		request.setAttribute("whereInDocumentos", "");
		request.setAttribute("listaTemplatesCopiaCheque", templates);
		return new ModelAndView("direct:relatorio/popup/selecaoTemplateCopiaCheque");
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaChequeDocumento(WebRequestContext request){
		String whereInDocumentos = request.getParameter("whereInDocumentos");
		
		List<ReportTemplateBean> templates = reporttemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_DOCUMENTO);
		request.setAttribute("whereInDocumentos", whereInDocumentos);
		request.setAttribute("listaTemplatesCopiaCheque", templates);
		return new ModelAndView("direct:relatorio/popup/selecaoTemplateCopiaChequeDocumento");
	}
	
	public LinkedList<EmitirCopiaChequeBean> geraListaCopiaChequeByDocumentoBean(List<Cheque> cheques){
		LinkedList<EmitirCopiaChequeBean> retorno = new LinkedList<EmitirCopiaChequeBean>();
		for(Cheque cheque: cheques){
			retorno.add(new EmitirCopiaChequeBean(cheque));
		}
		
		return retorno; 
	}
	
	public Cheque findForEmitenteCheque(Integer banco, Integer agencia,	Integer conta) {
		return chequeDAO.findForEmitenteCheque(banco, agencia, conta);
	}
	
	public ModelAndView ajaxBuscaDadosCheque(WebRequestContext request, Cheque cheque) {
		cheque = findForEmitenteCheque(cheque.getBanco(), cheque.getAgencia(), cheque.getConta());
		return new JsonModelAndView()
			.addObject("emitente", cheque != null ? cheque.getEmitente() : "")
			.addObject("cpfcnpj", cheque != null ? cheque.getCpfcnpj() : "");
	}
	
	public ModelAndView abrirPopupProtocoloRetiradaCheque(WebRequestContext request, ProtocoloRetiradaChequeFiltro filtro){
		List<ProtocoloRetiradaChequeBean> listaBean = new ListSet<ProtocoloRetiradaChequeBean>(ProtocoloRetiradaChequeBean.class);
		Set<Empresa> listaEmpresa = new HashSet<Empresa>();
		
		for (Cheque cheque: chequeDAO.findForProtocoloRetiradaCheque(filtro)){
			Movimentacao primeiraMovimentacao = cheque.getPrimeiraMovimentacao();
			Chequedevolucaomotivo ultimoChequedevolucaomotivo = cheque.getUltimoChequedevolucaomotivo();
			
			Set<String> listaCodigosCliente = new HashSet<String>();
			Set<String> listaNomesCliente = new HashSet<String>();
			Set<String> listaCodigosNomesCliente = new HashSet<String>();
			Set<String> listaCidadesCliente = new HashSet<String>();
			
			for (Movimentacaoorigem movimentacaoorigem: primeiraMovimentacao.getListaMovimentacaoorigem()){
				String codigosNomesCliente = "";
				Cliente cliente = movimentacaoorigem.getDocumento().getPessoa().getCliente();
				
				if (cliente.getIdentificador()!=null){
					listaCodigosCliente.add(cliente.getIdentificador());
					codigosNomesCliente = cliente.getIdentificador() + " ";
				}else {
					listaCodigosCliente.add("-");
				}
				
				if (SinedUtil.isListNotEmpty(cliente.getListaEndereco())){
					for (Endereco endereco: cliente.getListaEndereco()){
						if (endereco.getMunicipio()!=null){
							listaCidadesCliente.add(endereco.getMunicipio().getNome());
						}
					}
				}
				
				listaNomesCliente.add(cliente.getNome());
				listaCodigosNomesCliente.add(codigosNomesCliente + cliente.getNome());
			}
			
			ProtocoloRetiradaChequeBean bean = new ProtocoloRetiradaChequeBean();
			bean.setCodigosCliente(CollectionsUtil.concatenate(listaCodigosCliente, ", "));
			bean.setNomesCliente(CollectionsUtil.concatenate(listaNomesCliente, ", "));
			bean.setCidadesCliente(CollectionsUtil.concatenate(listaCidadesCliente, ", "));
			bean.setCodigosNomesCliente(CollectionsUtil.concatenate(listaCodigosNomesCliente, ", "));
			bean.setCheque(cheque);
			bean.setMotivoDevolucao(ultimoChequedevolucaomotivo!=null ? ultimoChequedevolucaomotivo.getDescricao() : null);
			
			if (cheque.getEmpresa()!=null){
				Empresa empresa = cheque.getEmpresa();
				filtro.setEmpresaCheques(empresa.getRazaosocial());
				
				if (SinedUtil.isListNotEmpty(empresa.getListaEndereco())){
					Endereco endereco = new ListSet<Endereco>(Endereco.class, empresa.getListaEndereco()).get(0);
					if (endereco.getMunicipio()!=null){
						filtro.setCidadeCheques(endereco.getMunicipio().getNome());
					}
				}
				
				listaEmpresa.add(cheque.getEmpresa());
			}
			
			listaBean.add(bean);
		}
		
		filtro.setListaBean(listaBean);
		
		request.setAttribute("listaTipoDeclarante", Arrays.asList(ProtocoloRetiradaChequeFiltro.TIPO_DECLARANTE_CLIENTE, ProtocoloRetiradaChequeFiltro.TIPO_DECLARANTE_COLABORADOR, ProtocoloRetiradaChequeFiltro.TIPO_DECLARANTE_OUTRO));
		
		if (filtro.getListaBean().isEmpty()){
			request.setAttribute("msgRegistroCheque", "Nenhum registro encontrado.");
		}else if (listaEmpresa.size()>1){
			request.setAttribute("msg001", "Os cheques selecionados pertencem a empresas diferentes, favor realizar o filtro por empresa.");			
		}else if (listaEmpresa.size()==1){
			Empresa empresa = new ListSet<Empresa>(Empresa.class, listaEmpresa).get(0);
			filtro.setEmpresa(empresa);
			filtro.setEmpresaCheques(empresa.getRazaosocial());
			if (SinedUtil.isListNotEmpty(empresa.getListaEndereco())){
				Endereco endereco = new ListSet<Endereco>(Endereco.class, empresa.getListaEndereco()).get(0);
				if (endereco.getMunicipio()!=null){
					filtro.setCidadeCheques(endereco.getMunicipio().getNome());
				}
			}
		}
		
		return new ModelAndView("direct:/crud/popup/protocoloRetiradaCheque", "filtro", filtro);
	}
	
	public IReport createReportProtocoloRetiradaCheque(ProtocoloRetiradaChequeFiltro filtro) {
		Report report = new Report("/financeiro/protocoloretiradacheque");
		report.setDataSource(filtro.getListaBean());
		report.addParameter("EMPRESA_CHEQUES", filtro.getEmpresaCheques());
		report.addParameter("CIDADE_CHEQUES", filtro.getCidadeCheques());
		report.addParameter("REFERENTE", filtro.getReferente());
		report.addParameter("DECLARANTE", filtro.getDeclarante());
		report.addParameter("LOGO_CHEQUES", SinedUtil.getLogo(filtro.getEmpresa()));
		
		chequeDAO.updateProtocoloretiradachequeemitida(filtro.getWhereInCdchequeLista(), Boolean.TRUE);
		
		filtro.setListaBean(null); //Cache a partir da emiss�o do segundo relat�rio
		
		return report;
	}
	
	
	public void updateEmpresaCheque(Movimentacao moDestino) {
		chequeDAO.updateEmpresaCheque(moDestino);
	}
}