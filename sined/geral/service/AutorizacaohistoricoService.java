package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Autorizacaohistorico;
import br.com.linkcom.sined.geral.bean.Autorizacaotrabalho;
import br.com.linkcom.sined.geral.dao.AutorizacaohistoricoDAO;

public class AutorizacaohistoricoService extends GenericService<Autorizacaohistorico>{
	
	public List<Autorizacaohistorico> findBy(Autorizacaotrabalho autorizacaotrabalho){
		return AutorizacaohistoricoDAO.getInstance().findBy(autorizacaotrabalho);
	}
	
	public static AutorizacaohistoricoService instance;
	
	public static AutorizacaohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AutorizacaohistoricoService.class);
		}
		return instance;
	}	
}
