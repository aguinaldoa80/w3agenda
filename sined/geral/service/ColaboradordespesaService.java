package br.com.linkcom.sined.geral.service;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesahistoricoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipovalorreceber;
import br.com.linkcom.sined.geral.dao.ColaboradordespesaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ColaboradorPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorPagamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradordespesaService extends GenericService<Colaboradordespesa>{

	private ColaboradordespesaDAO colaboradordespesaDAO;	
	private ColaboradordespesahistoricoService colaboradordespesahistoricoService;
	private DocumentoService documentoService;
	private DocumentoorigemService documentoorigemService;	
	private ColaboradordespesamotivoService colaboradordespesamotivoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private ProvisaoService provisaoService;

	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setColaboradordespesamotivoService(
			ColaboradordespesamotivoService colaboradordespesamotivoService) {
		this.colaboradordespesamotivoService = colaboradordespesamotivoService;
	}
	public void setColaboradordespesahistoricoService(ColaboradordespesahistoricoService colaboradordespesahistoricoService) {
		this.colaboradordespesahistoricoService = colaboradordespesahistoricoService;
	}
	public void setColaboradordespesaDAO(ColaboradordespesaDAO colaboradordespesaDAO) {
		this.colaboradordespesaDAO = colaboradordespesaDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setProvisaoService(ProvisaoService provisaoService) {
		this.provisaoService = provisaoService;
	}

	/* singleton */
	private static ColaboradordespesaService instance;
	public static ColaboradordespesaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordespesaService.class);
		}
		return instance;
	}
	
	/**
	* Faz refer�ncia ao DAO.
	* Busca lista de Colaboradordespesa para o processo de autoriza��o, cancelamento e estorno 
	* 
	* @see	br.com.linkcom.sined.geral.dao.ColaboradordespesaDAO#findColaboradordespesa(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaboradordespesa> findForAutorizarCancelarEstornar(String whereIn){
		return colaboradordespesaDAO.findForAutorizarEstornarCancelar(whereIn);
	}
	
	public List<Colaboradordespesa> findForProcessar(String whereIn){
		return colaboradordespesaDAO.findForProcessar(whereIn);
	}
	
	
	/**
	* Faz refer�ncia ao DAO.
	* Atualiza a situa��o do Colaboradordespesa de acordo com o par�mettro Colaboradordespesasituacao.
	* 
	* @see	br.com.linkcom.sined.geral.dao.ColaboradordespesaDAO#updateSituacao(Colaboradordespesasituacao colaboradordespesasituacao, Colaboradordespesa colaboradordespesa)
	*
	* @param colaboradordespesasituacao
	* @param colaboradordespesa
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateSituacao(Colaboradordespesasituacao colaboradordespesasituacao, Colaboradordespesa colaboradordespesa){
		colaboradordespesaDAO.updateSituacao(colaboradordespesasituacao, colaboradordespesa);
		
	}	
	
	/**
	* M�todo que realiza a autoriza��o da despesa do colaborador
	* 
	* @see	br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdate(Colaboradordespesahistorico bean)
	*
	* @param colaboradordespesa
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void autorizar (Colaboradordespesa colaboradordespesa) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(Colaboradordespesasituacao.AUTORIZADO, colaboradordespesa);
		
		// REGISTRA NO HIST�RICO
		Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
		colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.AUTORIZADO);
		colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);		
		colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
	}	
	
	
	/**
	* M�todo que realiza o cancelamento da despesa do colaborador
	* 
	* @see	br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdate(Colaboradordespesahistorico bean)
	*
	* @param colaboradordespesa
	* @param obs
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void cancelar(Colaboradordespesa colaboradordespesa, String obs) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(Colaboradordespesasituacao.CANCELADO, colaboradordespesa);
		
		// REGISTRA NO HIST�RICO
		Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
		colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.CANCELADO);
		colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
		colaboradordespesahistorico.setObservacao(obs);
		colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
	}
	
	
	/**
	* M�todo que realiza o estorno da despesa do colaborador
	* 
	* @see	br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdate(Colaboradordespesahistorico bean)
	*
	* @param colaboradordespesa
	* @param observacao
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void estornar (Colaboradordespesa colaboradordespesa, String observacao) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(Colaboradordespesasituacao.EM_ABERTO, colaboradordespesa);
		
		// REGISTRA NO HIST�RICO
		Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
		colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.ESTORNADO);
		colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
		colaboradordespesahistorico.setObservacao(observacao);
		colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
	}
	
	
	/**
	* M�todo que realiza o processamento da despesa do colaborador
	* 
	* @see	br.com.linkcom.sined.util.neo.persistence.GenericService.saveOrUpdate(Colaboradordespesahistorico bean)
	*
	* @param colaboradordespesa
	* @param obs
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	 * @param documento2 
	*/
	public void processar(List<Colaboradordespesa> listaDespesa, Documento documento) {
		
		Documentohistorico documentohistorico;
		Set<Documentohistorico> listaHistorico;
		documento.setDocumentoclasse(getDocumentoclasse(listaDespesa));
		documento.setDtemissao(new Date(System.currentTimeMillis()));
		documento.setDtcompetencia(new Date(System.currentTimeMillis()));
		documento.setDescricao("Despesa Pessoal");
		if(!ParametrogeralService.getInstance().getBoolean(Parametrogeral.AGRUPAR_GERACAO_CONTAS_DESPESA_COLABORADOR)){
			documento.setTipopagamento(Tipopagamento.COLABORADOR);
		}
		documento.setDocumentoacao(Documentoacao.PREVISTA);
				
		documentohistorico = new Documentohistorico(documento);
		documentohistorico.setObservacao("Criada a partir do processamento da despesa do colaborador.");
		listaHistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		listaHistorico.add(documentohistorico);
		documento.setListaDocumentohistorico(listaHistorico);
		
		rateioService.ajustaPercentualRateioitem(documento.getValor(), documento.getRateio().getListaRateioitem());
		
		if(documento.getValortaxadespesa() != null && documento.getValortaxadespesa().getValue().doubleValue() > 0){
			Taxa taxa = new Taxa();
			taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class));
			Taxaitem taxaitem = new Taxaitem(Tipotaxa.TAXAMOVIMENTO, false, documento.getValortaxadespesa(), false);
			taxaitem.setDtlimite(documento.getDtemissao());
			taxa.getListaTaxaitem().add(taxaitem);
			documento.setTaxa(taxa);
		}
		
		documentoService.saveOrUpdate(documento);
		documentoService.callProcedureAtualizaDocumento(documento);
		
		for (Colaboradordespesa despesa : listaDespesa){
			Documentoorigem documentoorigem = new Documentoorigem();
			documentoorigem.setColaboradordespesa(despesa);
			documentoorigem.setDocumento(documento);
			
			documentoorigemService.saveOrUpdate(documentoorigem);		
			
			this.processarUtualizacaohistorico(despesa, documento);
		}
		
	}
	
	public Documentoclasse getDocumentoclasse(List<Colaboradordespesa> listaDespesa) {
		boolean gerarContapagar = false;
		boolean gerarContareceber = false;
		
		if(SinedUtil.isListNotEmpty(listaDespesa)){
			for (Colaboradordespesa despesa : listaDespesa){
				if(despesa.getTotal() != null && despesa.getTotal().getValue().doubleValue() < 0 && despesa.getTipovalorreceber() != null && Tipovalorreceber.CONTA_RECEBER.equals(despesa.getTipovalorreceber())){
					gerarContareceber = true;
				}else {
					gerarContapagar = true;
				}
			}
		}
		
		Documentoclasse documentoclasse = null;
		if(gerarContareceber && !gerarContapagar){
			documentoclasse = Documentoclasse.OBJ_RECEBER;
		}else if(!gerarContareceber && gerarContapagar){
			documentoclasse = Documentoclasse.OBJ_PAGAR;
		}
		return documentoclasse;
	}
	/**
	* Atualiza a situa��o do Colaboradordespesa e o hist�rico do colaboradordespesahist�rico
	*
	* @param colaboradordespesa
	* @since Jul 5, 2011
	* @author Luiz Fernando F Silva
	*/
	public void processarUtualizacaohistorico(Colaboradordespesa colaboradordespesa, Documento documento){
		// ATUALIZA A SITUA��O DO COLABORADOR
		this.updateSituacao(Colaboradordespesasituacao.PROCESSADO, colaboradordespesa);
		
		String observacao = "Criada a partir do processamento da despesa do colaborador";
		if(documento != null){
			observacao += " <a href=\"javascript:visualizar" +
				(Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse()) ? "Contapagar" : "Contareceber") + 
				"(" + documento.getCddocumento() + ")\">" + documento.getCddocumento() + "</a>";
		}
		
		// REGISTRA NO HIST�RICO
		Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
		colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.PROCESSADO);
		colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
		colaboradordespesahistorico.setObservacao(observacao);
		colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
	}
	
	/**
	 * Gera��o da despesa do colaborador em cima do sal�rio calculado
	 * 
	 * @author Filipe Santos
	 * @param salarioLiquido
	 */
	public void gerarHolerite(Money salarioLiquido, Colaborador colaborador, List<Colaboradordespesaitem> cdi, Date dtreferencia1, Date dtreferencia2, Empresa empresa, Projeto projeto){
		try {
			Colaboradordespesa colaboradordespesa = new Colaboradordespesa();
			colaboradordespesa.setColaborador(colaborador);
			colaboradordespesa.setDtinsercao(SinedDateUtils.currentDate());
			colaboradordespesa.setDtholeriteinicio(dtreferencia1);
			colaboradordespesa.setDtholeritefim(dtreferencia2);
			colaboradordespesa.setProjeto(projeto);
			colaboradordespesa.setEmpresa(empresa);
			colaboradordespesa.setSituacao(Colaboradordespesasituacao.EM_ABERTO);
			colaboradordespesa.setValor(new Money());
			colaboradordespesa.setTotal(salarioLiquido);
			colaboradordespesa.setListaColaboradordespesaitem(cdi);
			colaboradordespesa.setColaboradordespesamotivo(colaboradordespesamotivoService.motivoHolerite());
			
			colaboradordespesa.setBloqueardebitocredito(Boolean.TRUE);

			this.saveOrUpdate(colaboradordespesa);
			
			Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
			colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
			colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.CRIADO);
			colaboradordespesahistorico.setObservacao("Gerada atrav�s de holerite.");
			
			colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar a holerite.");
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradordespesaDAO#findForConsultarColaboradordespesa(Integer cdcolaborador, Date referencia)
	 * 
	 * @param cdcolaborador
	 * @param referencia
	 * @return
	 * @author Luiz Fernando F Silva
	 */
	public List<Colaboradordespesa> findForConsultarColaboradordespesa(Integer cdcolaborador, Date dtreferencia1, Date dtreferencia2) {
		return colaboradordespesaDAO.findForConsultarColaboradordespesa(cdcolaborador, dtreferencia1, dtreferencia2);
	} 
	
	public Resource gerarRelarorioCSV(ColaboradordespesaFiltro filtro){
		
		StringBuilder csv = new StringBuilder();
		
		//CRIAR O CSV DINAMICAMENTE DEPENDENDO DA QUANTIDADE DE MOTIVOS
		List<Colaboradordespesa> listaColaboradorDespesa = this.findForCSV(filtro);
		List<String> stringCabecalho = new ArrayList<String>();
		for (Colaboradordespesa cd : listaColaboradorDespesa) {
			for (Colaboradordespesaitem item : cd.getListaColaboradordespesaitem()) {
				if (item.getMotivo() != null && !item.getMotivo().equals("")){
					if (stringCabecalho.size() == 0){
						stringCabecalho.add(item.getMotivo());
					} else {
						boolean encontrou =  false;
						for (String string : stringCabecalho) {
							if (item.getMotivo().equals(string)){
								encontrou = true;
							}
						}
						if(!encontrou)
							stringCabecalho.add(item.getMotivo());
					}
				}
			}
		}
		
		csv.append("MATRICULA;");
		csv.append("NOME;");
		csv.append("TOTAL;");
		csv.append("DATA DE REFER�NCIA;");
		csv.append("DATA DE PAGAMENTO;");
		for (String string : stringCabecalho) {
			csv.append(string + ";");
		}
		csv.append("\n");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		for (Colaboradordespesa cd : listaColaboradorDespesa) {
			if(cd.getColaborador().getListaColaboradorcargo() != null && !cd.getColaborador().getListaColaboradorcargo().isEmpty()){
				for (Colaboradorcargo colaboradorcargo : cd.getColaborador().getListaColaboradorcargo()) {
					if(colaboradorcargo.getDtfim() == null){
						if(colaboradorcargo.getMatricula() != null){
							cd.getColaborador().setMatricula(colaboradorcargo.getMatricula());
						}
						break;
					}
				}
			}
			csv.append((cd.getColaborador().getMatricula() != null ? cd.getColaborador().getMatricula() : "") + ";");
			csv.append(cd.getColaborador().getNome() + ";");
			csv.append(cd.getTotal() + ";");
			csv.append((cd.getDtinsercao() != null ? format.format(cd.getDtinsercao()) : "") + ";");
			csv.append((cd.getDtdeposito() != null ? format.format(cd.getDtdeposito()) : "") + ";");
			
			for(String cabecalho : stringCabecalho){
				boolean encontrouMotivo = false;
				for (Colaboradordespesaitem item : cd.getListaColaboradordespesaitem()) {
					if (item.getMotivo().equals(cabecalho)){
						csv.append(item.getValor() + ";");
						encontrouMotivo = true;
					}
				}
				if(!encontrouMotivo)
					csv.append(";");
			}
			
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "colaboradordespesa_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForCSV
	 * @param filtro
	 * @return
	 * @return List<Colaboradordespesa>
	 * @author Thiago Augusto
	 * @date 04/05/2012
	 *
	 */
	public List<Colaboradordespesa> findForCSV(ColaboradordespesaFiltro filtro){
		return colaboradordespesaDAO.findForCSV(filtro);
	}
	
	/**
	 * M�todo que copia o pedido de venda
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaboradordespesa criarCopia(Colaboradordespesa origem) {
		origem = loadForEntrada(origem);
		
		Colaboradordespesa copia = new Colaboradordespesa();
		copia = origem;
		copia.setCdcolaboradordespesa(null);
		copia.setDtcancelado(null);
		copia.setDtinsercao(new Date(System.currentTimeMillis()));
		copia.setSituacao(Colaboradordespesasituacao.EM_ABERTO);
		if(copia.getRateio() != null && copia.getRateio().getCdrateio() != null){
			copia.getRateio().setListaRateioitem(rateioitemService.findByRateio(copia.getRateio()));
			copia.getRateio().setCdrateio(null);
			for(Rateioitem ri: copia.getRateio().getListaRateioitem()){
				ri.setCdrateioitem(null);
				ri.setCdusuarioaltera(null);
				ri.setDtaltera(null);
			}
		}
		

		if(origem.getListaColaboradordespesaitem() != null && !origem.getListaColaboradordespesaitem().isEmpty()){
			for(Colaboradordespesaitem item : origem.getListaColaboradordespesaitem()){
				item.setCdcolaboradordespesaitem(null);
			}
		}
		copia.setListaColaboradordespesahistorico(null);
		return copia;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradordespesaService#findForReportFluxocaixa(FluxocaixaFiltroReport filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaboradordespesa> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return colaboradordespesaDAO.findForReportFluxocaixa(filtro, false);
	}
	
	public List<Colaboradordespesa> findAnterioresForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return colaboradordespesaDAO.findForReportFluxocaixa(filtro, true);
	}
	
	/**
	 * M�todo que adicionar as despesas com colaboadores na lista de fluxo de caixa
	 *
	 * @param filtro
	 * @param listaFluxo
	 * @param listaColaboradordespesa
	 * @author Luiz Fernando
	 * @param boolean1 
	 * @param date 
	 */
	public void adicionaColaboradordespesaFluxocaixa(FluxocaixaFiltroReport filtro,	List<FluxocaixaBeanReport> listaFluxo, List<Colaboradordespesa> listaColaboradordespesa, Date periodoDe, Boolean valoresAnteriores) {
		listaFluxo.addAll(this.criaListaFluxocaixa(listaColaboradordespesa, periodoDe, valoresAnteriores));
	}
	
	/**
	 * M�todo que cria a lista de fluxo de caixa com as despesas com pessoal
	 *
	 * @param listaColaboradordespesa
	 * @return
	 * @author Luiz Fernando
	 */
	private List<FluxocaixaBeanReport> criaListaFluxocaixa(List<Colaboradordespesa> listaColaboradordespesa, Date periodoDe, Boolean valoresAnteriores) {
		List<FluxocaixaBeanReport> listaFluxo = new ArrayList<FluxocaixaBeanReport>();
		
		for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
			FluxocaixaBeanReport fluxocaixaBeanReport = new FluxocaixaBeanReport(colaboradordespesa);
			if(valoresAnteriores != null && valoresAnteriores && colaboradordespesa.getDtdeposito().before(periodoDe)){
				fluxocaixaBeanReport.setDataAtrasada(colaboradordespesa.getDtdeposito());
				fluxocaixaBeanReport.setData(SinedDateUtils.remoteDate());
			}
			listaFluxo.add(fluxocaixaBeanReport);
		}
		return listaFluxo;
	}
	
	public List<Colaboradordespesa> findForArquivoDIRF(Empresa empresa, Date dtinicio, Date dtfim, Colaboradordespesamotivo colaboradordespesamotivo) {
		return colaboradordespesaDAO.findForArquivoDIRF(empresa, dtinicio, dtfim, colaboradordespesamotivo);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param dtreferencia1
	 * @param dtreferencia2
	 * @param empresa
	 * @param projeto
	 * @param colaborador
	 * @param holeritePorProjeto 
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/12/2015
	 */
	public List<Colaboradordespesa> findForListagemHolerite(Date dtreferencia1, Date dtreferencia2, Empresa empresa, Projeto projeto, Colaborador colaborador, Boolean holeritePorProjeto) {
		return colaboradordespesaDAO.findForListagemHolerite(dtreferencia1, dtreferencia2, empresa, projeto, colaborador, holeritePorProjeto);
	}
	
	public Colaboradordespesa loadWithRateio(Colaboradordespesa bean) {
		return colaboradordespesaDAO.loadWithRateio(bean);
	}
	
	public void saveOrUpdateList(final List<Colaboradordespesa> lista){
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			
			@Override
			public Object doInTransaction(TransactionStatus status) {
				for(Colaboradordespesa bean: lista){
					ColaboradordespesaService.getInstance().saveOrUpdateNoUseTransaction(bean);
					
					Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
					colaboradordespesahistorico.setColaboradordespesa(bean);
					
					colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.CRIADO);
					colaboradordespesahistorico.setObservacao("Criado via importa��o de folha de pagamento.");
					
					colaboradordespesahistoricoService.saveOrUpdateNoUseTransaction(colaboradordespesahistorico);
				}
				return null;
			}
		});
	}
	
	public void verificaDespesaColaboradorAposCancelamentoDocumento(String whereIdDocumentos){
		List<Documento> listaDocumento = documentoService.findWithDespesaColaborador(whereIdDocumentos);
		
		for(Documento doc: listaDocumento){
			String labelDocumento = Documentoclasse.OBJ_RECEBER.equals(doc.getDocumentoclasse())? "conta a receber <a href=\"javascript:visualizarContareceber("+doc.getCddocumento()+")\">"+doc.getCddocumento()+"</a>": "conta a pagar <a href=\"javascript:visualizarContapagar("+doc.getCddocumento()+")\">"+doc.getCddocumento()+"</a>";
			for(Documentoorigem origem: doc.getListaDocumentoOrigem()){
				this.updateSituacao(Colaboradordespesasituacao.AUTORIZADO, origem.getColaboradordespesa());
				
				Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
				colaboradordespesahistorico.setColaboradordespesa(origem.getColaboradordespesa());
				colaboradordespesahistorico.setObservacao("Cancelamento de "+labelDocumento+".");
				colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.ALTERADO);
				colaboradordespesahistoricoService.saveOrUpdateNoUseTransaction(colaboradordespesahistorico);
				
				List<Provisao> listaProvisao = provisaoService.findByColaboradorDespesaOrigem(origem.getColaboradordespesa());
				if(SinedUtil.isListNotEmpty(listaProvisao)){
					provisaoService.deleteProvisoes(SinedUtil.listAndConcatenate(listaProvisao, "cdProvisao", ","));
				}
			}
		}
	}
	
	public List<Colaboradordespesa> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, boolean considerarEvento, String whereInEventos){
		return colaboradordespesaDAO.findForGerarLancamentoContabil(filtro, considerarEvento, whereInEventos);
	}
	
	public List<ColaboradorPagamentoBean> findForReportPagamentoColaborador(ColaboradorPagamentoFiltro filtro){
		return colaboradordespesaDAO.findForReportPagamentoColaborador(filtro);
	}
	public Money calcularValorTotalDespesaColaborador(Empresa empresa, String whereInEventoDespesaColaborador, Date dtInicio, Date dtFim) {
		return colaboradordespesaDAO.calcularValorTotalDespesaColaborador(empresa, whereInEventoDespesaColaborador, dtInicio, dtFim);
	}
	public List<Colaboradordespesa> findByIds(String whereIn) {
		return colaboradordespesaDAO.findByIds(whereIn);
	}
}