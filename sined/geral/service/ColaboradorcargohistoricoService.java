package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargohistorico;
import br.com.linkcom.sined.geral.dao.ColaboradorcargohistoricoDAO;

public class ColaboradorcargohistoricoService extends GenericService<Colaboradorcargohistorico> {
	
	protected ColaboradorcargohistoricoDAO colaboradorcargohistoricoDAO;

	
	public ColaboradorcargohistoricoDAO getColaboradorcargohistoricoDAO() {
		return colaboradorcargohistoricoDAO;
	}

	public void setColaboradorcargohistoricoDAO(
			ColaboradorcargohistoricoDAO colaboradorcargohistoricoDAO) {
		this.colaboradorcargohistoricoDAO = colaboradorcargohistoricoDAO;
	}

	public List<Colaboradorcargohistorico> findByColaboradorcargo(Colaboradorcargo colaboradorcargo){
		return colaboradorcargohistoricoDAO.findByColaboradorcargo(colaboradorcargo);
	}
	
	

}
