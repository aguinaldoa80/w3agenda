package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefarequisicao;
import br.com.linkcom.sined.geral.dao.TarefarequisicaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefarequisicaoService extends GenericService<Tarefarequisicao>{

	private TarefarequisicaoDAO tarefarequisicaoDAO;
		
	public void setTarefarequisicaoDAO(TarefarequisicaoDAO tarefarequisicaoDAO) {
		this.tarefarequisicaoDAO = tarefarequisicaoDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefarequisicaoDAO.findByTarefa(Tarefa tarefa)
	 *
	 * @param tarefa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Tarefarequisicao> findByTarefa(Tarefa tarefa){
		return tarefarequisicaoDAO.findByTarefa(tarefa);
	}
}
