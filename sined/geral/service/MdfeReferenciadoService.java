package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeReferenciado;
import br.com.linkcom.sined.geral.dao.MdfeReferenciadoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeReferenciadoService extends GenericService<MdfeReferenciado>{

	private MdfeReferenciadoDAO mdfeReferenciadoDAO;
	public void setMdfeReferenciadoDAO(MdfeReferenciadoDAO mdfeReferenciadoDAO) {
		this.mdfeReferenciadoDAO = mdfeReferenciadoDAO;
	}
	
	public List<MdfeReferenciado> findByMdfe(Mdfe mdfe){
		return  mdfeReferenciadoDAO.findByMdfe(mdfe);
	}
}
