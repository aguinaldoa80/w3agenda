package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Dependenciacargo;
import br.com.linkcom.sined.geral.bean.Dependenciafaixa;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Relacaocargo;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.RelacaocargoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.RelacaoCargoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RelacaocargoService extends GenericService<Relacaocargo>{
	
	private RelacaocargoDAO relacaocargoDAO;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private RecursocomposicaoService recursocomposicaoService;
	
	public void setRecursocomposicaoService(
			RecursocomposicaoService recursocomposicaoService) {
		this.recursocomposicaoService = recursocomposicaoService;
	}
	public void setOrcamentorecursohumanoService(
			OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	
	public void setRelacaocargoDAO(RelacaocargoDAO relacaocargoDAO) {
		this.relacaocargoDAO = relacaocargoDAO;
	}
	
	public void setPeriodoorcamentocargoService(PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	
	/**
	 * Busca as rela��es entre os cargos a partir de um filtro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RelacaocargoDAO#findForListagemFlex(RelacaoCargoFiltro)
	 * 
	 * @param filtro
	 * @return List<Relacaocargo>
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Relacaocargo> findForListagemFlex(RelacaoCargoFiltro filtro) {
		return relacaocargoDAO.findForListagemFlex(filtro);
	}
	
	/* 
	 * Deixar sobrescrito mesmo sem nenhuma implementa��o para ser usado
	 * na parte em flex da aplica��o.
	 */
	@Override
	public Relacaocargo loadForEntrada(Relacaocargo bean) {
		return super.loadForEntrada(bean);
	}
	
	/**
	 * Salva o objeto do tipo Relacaocargo no banco de dados e recalcula o histograma,
	 * caso o par�metro recalcularHistograma seja verdadeiro
	 *
	 * @see br.com.linkcom.sined.geral.service.RelacaocargoService#existeDependenciaCiclica(List)
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdate(Object)
	 * @see br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService#atualizaValoresHistograma(Orcamento)
	 * 
	 * @param orcamento
	 * @param recalcularHistograma
	 * @return
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveListaRelacaoCargoFlex(final Relacaocargo bean, final Boolean recalcularHistograma) {
		
		if(bean != null && bean.getCdrelacaocargo().intValue() == 0) {
			bean.setCdrelacaocargo(null);
		}
		
		//Como no flex os valores nulos para tipo number s�o automaticamente convertidos para o valor default do primitivo (0),
		//deve-se fazer a convers�o aqui para que o valor correto seja cadastrado no banco
		if (bean.getTiporelacaorecurso().equals(Tiporelacaorecurso.FAIXA_DE_VALORES)) {
			bean.setQuantidade(null);
			
			for (Dependenciafaixa dependenciaFaixa : bean.getListaDependenciafaixa()) {
				//Como no flex os valores nulos para tipo inteiro s�o automaticamente convertidos para o valor default do primitivo (0),
				//deve-se fazer a convers�o aqui para que o registro seja inclu�do no banco e n�o atualizado...					
				if (dependenciaFaixa.getCddependenciafaixa().intValue() == 0) {
					dependenciaFaixa.setCddependenciafaixa(null);
				}
				
				//Como no flex os valores nulos para tipo number s�o automaticamente convertidos para o valor default do primitivo (0),
				//deve-se fazer a convers�o aqui para que o valor correto seja cadastrado no banco				
				if (dependenciaFaixa.getFaixaate().intValue() == 0) {
					dependenciaFaixa.setFaixaate(null);
				}
			}
		}

		if (bean.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
			for (Dependenciacargo dependenciaCargo : bean.getListaDependenciacargo()) {
				//Como no flex os valores nulos para tipo inteiro s�o automaticamente convertidos para o valor default do primitivo (0),
				//deve-se fazer a convers�o aqui para que o registro seja inclu�do no banco e n�o atualizado...					
				if (dependenciaCargo.getCddependenciacargo().intValue() == 0) {
					dependenciaCargo.setCddependenciacargo(null);
				}
			}
		}
		
		//Verifica se existe algum tipo de depend�ncia c�clica entre os cargos
		List<Relacaocargo> listaRelacaoCargo = findByOrcamento(bean.getOrcamento());
		List<Relacaocargo> listaRelacaoCargoAlterada = new ArrayList<Relacaocargo>();
		
		if (listaRelacaoCargo != null) {
			for (Relacaocargo relacaoCargoBanco : listaRelacaoCargo) {
				if (!relacaoCargoBanco.getCdrelacaocargo().equals(bean.getCdrelacaocargo())) {
					listaRelacaoCargoAlterada.add(relacaoCargoBanco);
				}
			}
			listaRelacaoCargoAlterada.add(bean);
			
			String msg = existeDependenciaCiclica(listaRelacaoCargoAlterada);
			if (!msg.equals("")) {
				throw new SinedException(msg);
			}			
		}
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				saveOrUpdateNoUseTransaction(bean);
				
				if (recalcularHistograma) {
					if(bean.getOrcamento().getCalcularhistograma()){
						periodoorcamentocargoService.atualizaValoresHistograma(bean.getOrcamento());
					} else {
						periodoorcamentocargoService.deleteByOrcamento(bean.getOrcamento());
						orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoSemHistogramaFlex(bean.getOrcamento());
						recursocomposicaoService.atualizaOrcamentoRecursoGeralSemHistogramaFlex(bean.getOrcamento());
					}
				}
				
				return null;
			}
		});		
		
	}
	
	/**
	 * Deleta a rela��o entre os cargos e recalcula o histograma de m�o-de-obra, caso o
	 * par�metro recalcularHistograma seja verdadeiro
	 *
	 * @param relacaocargo
	 * @param recalcularHistograma
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public void deleteFlex(Relacaocargo relacaocargo, Boolean recalcularHistograma){
		delete(relacaocargo);
		
		if (recalcularHistograma) {
			if(relacaocargo.getOrcamento().getCalcularhistograma()){
				periodoorcamentocargoService.atualizaValoresHistograma(relacaocargo.getOrcamento());
			} else {
				periodoorcamentocargoService.deleteByOrcamento(relacaocargo.getOrcamento());
				orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoSemHistogramaFlex(relacaocargo.getOrcamento());
				recursocomposicaoService.atualizaOrcamentoRecursoGeralSemHistogramaFlex(relacaocargo.getOrcamento());
			}
		}		
	}
	
	/**
	 * Carrega a lista de rela��es entre os cargos de um determinado or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RelacaocargoDAO#findByOrcamento(Orcamento)
	 *
	 * @param orcamento
	 * @return List<Relacaocargo>
	 * @throws SinedException - caso o par�metro orcamento seja nulo
	 * @author Rodrigo Alvarenga
	 */
	public List<Relacaocargo> findByOrcamento(Orcamento orcamento){
		return relacaocargoDAO.findByOrcamento(orcamento);
	}
	
	
	/***
	 * M�todo que verifica se existe alguma depend�ncia c�clica nos cargos presentes na lista de rela��es passada como par�metro
	 * 
	 * @see br.com.linkcom.sined.geral.service.RelacaocargoService#verificaDependenciaCiclica(Relacaocargo, List, List)
	 * 	 
	 * @param listaRelacaoCargo
	 * @return string contendo uma mensagem de erro ou vazia, caso n�o haja depend�ncia c�clica
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public String existeDependenciaCiclica(List<Relacaocargo> listaRelacaoCargo) {
		String msg = "";
		//Para cada rela��o entre os cargos, verifica se existe depend�ncia c�clica entre os mesmos
		for (Relacaocargo relacaoCargo : listaRelacaoCargo) {
			//if (relacaoCargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
				msg = verificaDependenciaCiclica(relacaoCargo, listaRelacaoCargo, null);
				if (!msg.equals("")) {
					return msg;
				}
			//}
		}
		return msg;
	}
	
	/***
	 * M�todo recursivo que verifica se existe alguma depend�ncia c�clica entre os cargos 
	 * 	 
	 * @param relacaoCargoOrigem
	 * @param listaRelacaoCargo
	 * @param listaCargoDependenciaCiclica
	 * @return string contendo uma mensagem de erro ou vazia, caso n�o haja depend�ncia c�clica
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private String verificaDependenciaCiclica(Relacaocargo relacaoCargoOrigem, List<Relacaocargo> listaRelacaoCargo, List<Cargo> listaCargoDependenciaCiclica) {
		Boolean existeRelacaoCargoDependente;
		String msg = "";
			
		if (listaCargoDependenciaCiclica == null) {
			listaCargoDependenciaCiclica = new ArrayList<Cargo>();
		}
		
		//Verifica se o cargo presente em relacaoCargoOrigem j� est� presente na lista que mapeia o caminho da depend�ncia.
		//Caso afirmativo, existe a depend�ncia c�clica.
		if (listaCargoDependenciaCiclica.contains(relacaoCargoOrigem.getCargo())) {			
			int indexCargoDependencia = listaCargoDependenciaCiclica.indexOf(relacaoCargoOrigem.getCargo());			
			listaCargoDependenciaCiclica.add(relacaoCargoOrigem.getCargo());
			String msgErro = "";
			Cargo cargoDepCiclica;
			Iterator<Cargo> itCargoDependenciaCiclica = listaCargoDependenciaCiclica.iterator();
			int i = 0;
			while (itCargoDependenciaCiclica.hasNext()) {
				cargoDepCiclica = itCargoDependenciaCiclica.next();
				
				//S� imprime a lista a partir do cargo que gerou a depend�ncia c�clica
				if (i >= indexCargoDependencia) {
					msgErro += cargoDepCiclica.getNome();
				
					if (itCargoDependenciaCiclica.hasNext()) {
						msgErro += " -> ";
					}
				}
				i++;
			}
			return "Foi encontrada uma depend�ncia c�clica: " + msgErro + ".";
		}
		listaCargoDependenciaCiclica.add(relacaoCargoOrigem.getCargo());		

		if (relacaoCargoOrigem.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
			for (Dependenciacargo dependenciaCargo : relacaoCargoOrigem.getListaDependenciacargo()) {
				
				int index = listaCargoDependenciaCiclica.size();
				
				existeRelacaoCargoDependente = false;
							
				for (Relacaocargo relacaoCargoDependente : listaRelacaoCargo) {
					if (relacaoCargoDependente.getCargo().equals(dependenciaCargo.getCargo())) {
						
						//Verifica se um cargo dependende de outro que, por sua vez, depende da soma de TODOS os cargos
						if (relacaoCargoDependente.getTipodependenciarecurso().equals(Tipodependenciarecurso.TODOS_CARGOS)) {
							msg = "O cargo " + relacaoCargoOrigem.getCargo().getNome() + " depende do cargo " + dependenciaCargo.getCargo().getNome() + " que, por sua vez, depende do somat�rio de todos os cargos.";
							return msg;
						}
						
						msg = verificaDependenciaCiclica(relacaoCargoDependente, listaRelacaoCargo, listaCargoDependenciaCiclica);
						if (!msg.equals("")) {
							return msg;
						}
						existeRelacaoCargoDependente = true;
					}
				}
				
				if (!existeRelacaoCargoDependente) {
					listaCargoDependenciaCiclica.add(dependenciaCargo.getCargo());
				}
				
				for (int i = listaCargoDependenciaCiclica.size() - 1; i >= index ; i--) {
					listaCargoDependenciaCiclica.remove(i);				
				}
			}
		}
		return msg;
	}	
}
