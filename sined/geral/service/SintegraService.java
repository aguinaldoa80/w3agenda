package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Sintegra;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadesped;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraNaturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopessoainventario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.SintegraDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sintegrafiscal.registros.Registro10;
import br.com.linkcom.sintegrafiscal.registros.Registro11;
import br.com.linkcom.sintegrafiscal.registros.Registro50;
import br.com.linkcom.sintegrafiscal.registros.Registro51;
import br.com.linkcom.sintegrafiscal.registros.Registro53;
import br.com.linkcom.sintegrafiscal.registros.Registro54;
import br.com.linkcom.sintegrafiscal.registros.Registro60A;
import br.com.linkcom.sintegrafiscal.registros.Registro60D;
import br.com.linkcom.sintegrafiscal.registros.Registro60I;
import br.com.linkcom.sintegrafiscal.registros.Registro60M;
import br.com.linkcom.sintegrafiscal.registros.Registro61;
import br.com.linkcom.sintegrafiscal.registros.Registro61R;
import br.com.linkcom.sintegrafiscal.registros.Registro70;
import br.com.linkcom.sintegrafiscal.registros.Registro71;
import br.com.linkcom.sintegrafiscal.registros.Registro74;
import br.com.linkcom.sintegrafiscal.registros.Registro75;
import br.com.linkcom.sintegrafiscal.registros.Registro76;
import br.com.linkcom.sintegrafiscal.registros.Registro77;
import br.com.linkcom.sintegrafiscal.registros.Registro90;
import br.com.linkcom.sintegrafiscal.registros.Totalizador;
import br.com.linkcom.sintegrafiscal.sintegra.SIntegra;
import br.com.linkcom.utils.SIntegraUtil;

public class SintegraService extends GenericService<Sintegra>{
	
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	private SintegraDAO sintegraDAO;
	private EmpresaService empresaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EntradafiscalService entradafiscalService;
	private InventarioService inventarioService;
	private ArquivonfnotaService arquivonfnotaService;
	private InutilizacaonfeService inutilizacaonfeService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumtotalizadoresService emporiumtotalizadoresService;
	private EmporiumvendaService emporiumvendaService;
	private EmporiumvendaitemService emporiumvendaitemService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	
	public void setEmporiumvendaitemService(EmporiumvendaitemService emporiumvendaitemService) {this.emporiumvendaitemService = emporiumvendaitemService;}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {this.emporiumpdvService = emporiumpdvService;}
	public void setEmporiumreducaozService(EmporiumreducaozService emporiumreducaozService) {this.emporiumreducaozService = emporiumreducaozService;}
	public void setEmporiumtotalizadoresService(EmporiumtotalizadoresService emporiumtotalizadoresService) {this.emporiumtotalizadoresService = emporiumtotalizadoresService;}
	public void setEmporiumvendaService(EmporiumvendaService emporiumvendaService) {this.emporiumvendaService = emporiumvendaService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setSintegraDAO(SintegraDAO sintegraDAO) {this.sintegraDAO = sintegraDAO;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {this.inutilizacaonfeService = inutilizacaonfeService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	
	public Resource gerarArquivo(SintegraFiltro filtro, WebRequestContext request) {
				
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		boolean saidasServico = filtro.getBuscarsaidasServico() != null && filtro.getBuscarsaidasServico();
		boolean cupomfiscal = filtro.getGerardadoscupomfiscal() != null && filtro.getGerardadoscupomfiscal();
		
		SIntegra sIntegra = new SIntegra();		
		filtro.setEmpresa(empresaService.findForSintegra(filtro.getEmpresa()));
//		filtro.inicializarRegistros();
		
		List<NotaFiscalServico> listaNfs = new ArrayList<NotaFiscalServico>();
		List<Arquivonfnota> listaArquivonfnotaNFS = new ArrayList<Arquivonfnota>();
		
		if(saidasServico){
			if(filtro.getNaturezaoperacao() != null && SIntegraNaturezaoperacao.TOTALIDADE_OPERACOES_INFORMANTE.equals(filtro.getNaturezaoperacao())){
				listaNfs = notaFiscalServicoService.findForSIntegraRegistro50(filtro);
				String whereInNFS = CollectionsUtil.listAndConcatenate(listaNfs, "cdNota", ",");
				listaArquivonfnotaNFS = arquivonfnotaService.findByNotaNotCancelada(whereInNFS);
			}
		}
		
		List<Notafiscalproduto> listaNfp = notafiscalprodutoService.findForSIntegraRegistro50(filtro);
		List<Notafiscalproduto> listaNfpRegistro54 = notafiscalprodutoService.findForSIntegraRegistro50(filtro);
		List<Inutilizacaonfe> listainuInutilizacaonfe = inutilizacaonfeService.findForSIntegraRegistro50(filtro);
		String whereInNFP = CollectionsUtil.listAndConcatenate(listaNfp, "cdNota", ",");
		List<Arquivonfnota> listaArquivonfnotaNFP = new ArrayList<Arquivonfnota>();
		if(whereInNFP != null && !"".equals(whereInNFP)){
			listaArquivonfnotaNFP = arquivonfnotaService.findByNotaProduto(whereInNFP);
		}
		
		List<Entregadocumento> listaEntregadocumento = new ArrayList<Entregadocumento>();
		List<Entregadocumento> listaEntregadocumentoRegistro54 = new ArrayList<Entregadocumento>();
		
		if(entradas){
			listaEntregadocumento = entradafiscalService.findForSIntegraRegistro50(filtro);
			listaEntregadocumentoRegistro54 = entradafiscalService.findForSIntegraRegistro50(filtro);
		}
		
		sIntegra.setRegistro10(this.createRegistro10(filtro));
		sIntegra.setListaRegistro50(this.createRegistro50(filtro, listaNfs, listaNfp, listaEntregadocumento, listaArquivonfnotaNFS, listaArquivonfnotaNFP, listainuInutilizacaonfe));
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getIndiceatividadesped() != null && 
				Indiceatividadesped.INDUSTRIAL.equals(filtro.getEmpresa().getIndiceatividadesped())){
			sIntegra.setListaRegistro51(this.createRegistro51(filtro, listaNfp, listaEntregadocumento, listaArquivonfnotaNFP));
		}
		sIntegra.setListaRegistro53(this.createRegistro53(filtro, listaNfpRegistro54, listaEntregadocumentoRegistro54, listaArquivonfnotaNFP));
		sIntegra.setListaRegistro54(this.createRegistro54(filtro, listaNfpRegistro54, listaEntregadocumentoRegistro54, listaArquivonfnotaNFP));
//		sIntegra.setListaRegistro55(this.createRegistro55(filtro));
		
		List<Emporiumvenda> listaEmporiumvenda = null;
		if(cupomfiscal){
			listaEmporiumvenda = emporiumvendaService.findForSped(null, filtro.getDtinicio(), filtro.getDtfim());
			this.createRegistro60(sIntegra, filtro);
		}
		
		List<Notafiscalproduto> listaNfpRegistro61 = null;
		if(filtro.getGerarRegistro61()){
			listaNfpRegistro61 = notafiscalprodutoService.findForSIntegraRegistro61(filtro);
			sIntegra.setListaRegistro61(this.createRegistro61(listaNfpRegistro61, filtro));			
			if(filtro.getGerarRegistro61R()){
				sIntegra.setListaRegistro61R(this.createRegistro61R(listaNfpRegistro61, filtro));
			}			
		}
		
		if(entradas){
			listaEntregadocumento = entradafiscalService.findForSIntegraRegistro70(filtro);
		}
		sIntegra.setListaRegistro70(this.createRegistro70(filtro, listaEntregadocumento));
		if(filtro.getGerarRegisro71() != null && filtro.getGerarRegisro71())
			sIntegra.setListaRegistro71(this.createRegistro71(filtro, listaEntregadocumento));
		
		Inventario inventario = null;
		if (filtro.getGerarRegisro74()){
			inventario = inventarioService.findForSIntegraRegistro74(filtro);
			sIntegra.setListaRegistro74(this.createRegistro74(filtro, inventario));
		}
		
		sIntegra.setListaRegistro75(this.createRegistro75(filtro, listaNfpRegistro54, listaEntregadocumentoRegistro54, inventario, listaEmporiumvenda, listaNfpRegistro61));
		
		if(entradas){
			listaEntregadocumento = entradafiscalService.findForSIntegraRegistro76(filtro);
		}
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			sIntegra.setListaRegistro76(this.createRegistro76(filtro, listaEntregadocumento));
			sIntegra.setListaRegistro77(this.createRegistro77(filtro, listaEntregadocumento));
		}
		sIntegra.setListaRegistro90(this.createRegistro90(filtro, sIntegra));
						
		List<String> listaErro = filtro.getListaErro();					
		if(listaErro != null && listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			throw new SinedException("A gera��o do arquivo cont�m erros. Favor corrig�-los.");
		}
		
		String nome = "sintegra" + SinedUtil.datePatternForReport() + ".txt";
		byte[] bytes = sIntegra.toString().getBytes();
		
		this.saveArquivoSintegra(filtro, nome, bytes);
		
		return new Resource("text/plain", nome, bytes);
	}

	private void saveArquivoSintegra(SintegraFiltro filtro, String nome, byte[] bytes) {
		Arquivo arquivo = new Arquivo(bytes, nome, "text/plain");
		
		this.deleteByEmpresaDtinicioDtfim(filtro);
		
		Sintegra sintegra = new Sintegra();
		sintegra.setDtinicio(filtro.getDtinicio());
		sintegra.setDtfim(filtro.getDtfim());
		sintegra.setConvenio(filtro.getConvenio());
		sintegra.setNaturezaoperacao(filtro.getNaturezaoperacao());
		sintegra.setFinalidadearquivo(filtro.getFinalidadearquivo());
		sintegra.setArquivo(arquivo);
		sintegra.setEmpresa(filtro.getEmpresa());
		
		arquivoDAO.saveFile(sintegra, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		this.saveOrUpdate(sintegra);
		
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SintegraDAO#deleteByEmpresaDtinicioDtfim(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @author Luiz Fernando
	 */
	private void deleteByEmpresaDtinicioDtfim(SintegraFiltro filtro) {
		sintegraDAO.deleteByEmpresaDtinicioDtfim(filtro);
	}
	
	private void createRegistro60(SIntegra sIntegra, SintegraFiltro filtro) {
		List<Registro60M> listaRegistro60M = new ArrayList<Registro60M>();
		
		List<Emporiumpdv> listaEmporiumpdv = emporiumpdvService.findByEmpresa(filtro.getEmpresa());
		
		for(Emporiumpdv emporiumpdv: listaEmporiumpdv){
			List<Emporiumreducaoz> listaEmporiumreducaoz = emporiumreducaozService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
			List<Emporiumtotalizadores> listaEmporiumtotalizadores = emporiumtotalizadoresService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
			List<Emporiumvenda> listaEmporiumvenda = emporiumvendaService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
			List<Emporiumvendaitem> listaEmporiumvendaitem = emporiumvendaService.getEmporiumvendaitemForSpedAcerto(listaEmporiumvenda);
			
			for (Emporiumreducaoz emporiumreducaoz : listaEmporiumreducaoz) {
				Integer numerocooinicial = 0;
				Emporiumreducaoz emporiumreducaoz_ultima = emporiumreducaozService.findLastReducaoz(emporiumreducaoz.getData(), emporiumpdv);
				if(emporiumreducaoz_ultima != null && emporiumreducaoz_ultima.getNumerocoofinal() != null){
					numerocooinicial = emporiumreducaoz_ultima.getNumerocoofinal() + 1;
				}
				
				List<Registro60A> listaRegistro60A = new ArrayList<Registro60A>();
				List<Registro60I> listaRegistro60I = new ArrayList<Registro60I>();
				List<Registro60D> listaRegistro60D = new ArrayList<Registro60D>();
				
				Registro60M registro60M = new Registro60M();
				
				registro60M.setTipo(Registro60M.TIPO);
				registro60M.setDataemissao(emporiumreducaoz.getData());
				registro60M.setNumeroserie(emporiumpdv.getNumeroserie());
				registro60M.setNumeropdv(Integer.parseInt(StringUtils.soNumero(emporiumpdv.getCodigoemporium())));
				registro60M.setModelodocumento(emporiumpdv.getModelodocfiscal());
				registro60M.setNumerocooinicial(numerocooinicial);
				registro60M.setNumerocoofinal(emporiumreducaoz.getNumerocoofinal());
				registro60M.setNumerocrz(emporiumreducaoz.getCrz());
				registro60M.setNumerocro(emporiumreducaoz.getCro());
				registro60M.setValorvendabruta(emporiumreducaoz.getValorvendabruta());
				registro60M.setValortotalizadorgeral(emporiumreducaoz.getGtfinal());
				
				if(emporiumreducaoz.getValorcancelados() != null && emporiumreducaoz.getValorcancelados() > 0){
					Registro60A registro60A = new Registro60A();
					
					registro60A.setTipo(Registro60A.TIPO);
					registro60A.setDataemissao(emporiumreducaoz.getData());
					registro60A.setNumeroserie(emporiumpdv.getNumeroserie());
					registro60A.setCodigototalizador("CANC");
					registro60A.setValortotalizador(emporiumreducaoz.getValorcancelados());
					
					filtro.addRegistro(Registro60A.TIPO_TOTALIZADOR);
					listaRegistro60A.add(registro60A);
				}
				
				if(emporiumreducaoz.getValordesconto() != null && emporiumreducaoz.getValordesconto() > 0){
					Registro60A registro60A = new Registro60A();
					
					registro60A.setTipo(Registro60A.TIPO);
					registro60A.setDataemissao(emporiumreducaoz.getData());
					registro60A.setNumeroserie(emporiumpdv.getNumeroserie());
					registro60A.setCodigototalizador("DESC");
					registro60A.setValortotalizador(emporiumreducaoz.getValordesconto());
					
					filtro.addRegistro(Registro60A.TIPO_TOTALIZADOR);
					listaRegistro60A.add(registro60A);
				}
				
				List<Emporiumtotalizadores> listaEmporiumtotalizadoresData = emporiumtotalizadoresService.getEmporiumtotalizadoresByDataMovimento(emporiumreducaoz.getData(), listaEmporiumtotalizadores);
				
				for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadoresData) {
					Tributacaoecf tributacaoecf = emporiumtotalizadores.getTributacaoecf();
					
					Registro60A registro60A = new Registro60A();
					
					registro60A.setTipo(Registro60A.TIPO);
					registro60A.setDataemissao(emporiumtotalizadores.getData());
					registro60A.setNumeroserie(emporiumpdv.getNumeroserie());
					
					String legenda = emporiumtotalizadores.getLegenda();
					if(legenda != null && !legenda.equals("F") && !legenda.equals("I") && !legenda.equals("N") && emporiumtotalizadores.getAliqicms() != null){
						registro60A.setCodigototalizador(emporiumtotalizadores.getAliqicms().intValue() + "");
					} else {
						registro60A.setCodigototalizador(legenda);
					}
					registro60A.setValortotalizador(emporiumtotalizadores.getValorbcicms());
					
					filtro.addRegistro(Registro60A.TIPO_TOTALIZADOR);
					listaRegistro60A.add(registro60A);
					
					List<Emporiumvendaitem> listaEmporiumvendaitemData = emporiumvendaitemService.getEmporiumvendaitemByDataMovimento(emporiumreducaoz.getData(), listaEmporiumvendaitem, emporiumpdv.getEmpresa());
					
					final int POS_QTDE = 0;
					final int POS_VALOR = 1;
					
					Map<Material, Double[]> mapMaterial = new HashMap<Material, Double[]>();
					for(Emporiumvendaitem it: listaEmporiumvendaitemData){
						Material material = it.getMaterial();
						if(material == null || it.getLegenda() == null){
							continue;
						}
						
						
						Registro60I registro60I = new Registro60I();
						registro60I.setTipo(Registro60I.TIPO);
						registro60I.setDataemissao(emporiumtotalizadores.getData());
						registro60I.setNumeroserie(emporiumpdv.getNumeroserie());
						registro60I.setModelodocumento(emporiumpdv.getModelodocfiscal());
						registro60I.setNumerocupom(Integer.parseInt(StringUtils.soNumero(it.getNumero_ticket())));
						registro60I.setNumeroitem(it.getSequencial());
						registro60I.setCodigoproduto(material.getCdmaterial() != null ? StringUtils.stringCheia(material.getCdmaterial().toString(), "0", 14, false) : null);
						registro60I.setQuantidade(it.getQuantidade());
						registro60I.setValorunitario(it.getValorunitario());
						registro60I.setValorbcicms(it.getValorComDesconto());
						
						if(legenda != null && !legenda.equals("F") && !legenda.equals("I") && !legenda.equals("N") && emporiumtotalizadores.getAliqicms() != null){
							registro60I.setCodigototalizador(StringUtils.stringCheia((emporiumtotalizadores.getAliqicms().intValue() * 100) + "", "0", 4, false));
							registro60I.setValoricms(it.getValorComDesconto() * (emporiumtotalizadores.getAliqicms() / 100d));
						} else {
							registro60I.setCodigototalizador(legenda);
							registro60I.setValoricms(0d);
						}
						
						filtro.addRegistro(Registro60I.TIPO_TOTALIZADOR);
						listaRegistro60I.add(registro60I);
						
						if(it.getLegenda() != null && tributacaoecf != null && it.getLegenda().equals(tributacaoecf.getCodigoecf())){
							if(mapMaterial.containsKey(material)){
								Double[] valores = mapMaterial.get(material);
								
								Double qtde = valores[POS_QTDE] + it.getQuantidade();
								Double valor = valores[POS_VALOR] + it.getValorComDesconto();
								
								mapMaterial.put(material, new Double[]{qtde, valor});
							} else {
								mapMaterial.put(material, new Double[]{it.getQuantidade(), it.getValorComDesconto()});
							}
						}
					}
					
					Set<Entry<Material, Double[]>> entrySetMaterial = mapMaterial.entrySet();
					
					List<Entry<Material, Double[]>> listaEntryMaterial = new ArrayList<Entry<Material,Double[]>>(entrySetMaterial);
					Collections.sort(listaEntryMaterial,new Comparator<Entry<Material, Double[]>>(){
						public int compare(Entry<Material, Double[]> o1, Entry<Material, Double[]> o2) {
							return o1.getKey().getNome().compareTo(o2.getKey().getNome());
						}
					});
						
					for (Entry<Material, Double[]> entry : listaEntryMaterial) {
						Material material = entry.getKey();
						Double[] valores = entry.getValue();
						
						Double qtde = valores[POS_QTDE];
						Double valor = valores[POS_VALOR];
						
						if(valor > 0){
							Registro60D registro60D = new Registro60D();
							
							registro60D.setTipo(Registro60D.TIPO);
							registro60D.setDataemissao(emporiumtotalizadores.getData());
							registro60D.setNumeroserie(emporiumpdv.getNumeroserie());
							registro60D.setCodigoproduto(material.getCdmaterial() != null ? StringUtils.stringCheia(material.getCdmaterial().toString(), "0", 14, false) : null);
							registro60D.setQuantidade(qtde);
							registro60D.setValor(valor);
							registro60D.setValorbcicms(valor);
							
							if(legenda != null && !legenda.equals("F") && !legenda.equals("I") && !legenda.equals("N") && emporiumtotalizadores.getAliqicms() != null){
								registro60D.setCodigototalizador(StringUtils.stringCheia((emporiumtotalizadores.getAliqicms().intValue() * 100)+ "", "0", 4, false));
								registro60D.setValoricms(valor * (emporiumtotalizadores.getAliqicms() / 100d));
							} else {
								registro60D.setCodigototalizador(legenda);
								registro60D.setValoricms(0d);
							}
							
							filtro.addRegistro(Registro60D.TIPO_TOTALIZADOR);
							listaRegistro60D.add(registro60D);
						}
					}
				}
				
				
				registro60M.setListaRegistro60A(listaRegistro60A);
				registro60M.setListaRegistro60I(listaRegistro60I);
				registro60M.setListaRegistro60D(listaRegistro60D);
				
				filtro.addRegistro(Registro60M.TIPO_TOTALIZADOR);
				listaRegistro60M.add(registro60M);
			}
		}
		
		sIntegra.setListaRegistro60M(listaRegistro60M);
		
	}
	
	private Registro10 createRegistro10(SintegraFiltro filtro) {
		
		Registro10 registro10 = new Registro10();
		
		if(filtro.getEmpresa() == null){
			filtro.addError("Empresa n�o pode ser nula.");
		}else{		
			registro10.setTipo(Registro10.TIPO);
			registro10.setCgcmf(filtro.getEmpresa().getCpfOuCnpj());
			registro10.setInscricaoestadual(filtro.getEmpresa().getInscricaoestadual());
			registro10.setNomecontribuinte(filtro.getEmpresa().getRazaosocialOuNome());
			if(filtro.getEmpresa().getMunicipiosped() != null){
				registro10.setMunicipio(filtro.getEmpresa().getMunicipiosped().getNomecompleto());
				if(filtro.getEmpresa().getMunicipiosped().getUf() != null)
					registro10.setUnidadefederacao(filtro.getEmpresa().getMunicipiosped().getUf().getSigla());
			}
			if(filtro.getEmpresa().getTelefoneFax() != null)
				registro10.setFax(filtro.getEmpresa().getTelefoneFax().getTelefone());
			
			registro10.setDatainicial(filtro.getDtinicio());
			registro10.setDatafinal(filtro.getDtfim());
			registro10.setCodigoidentificacaoconvenio(filtro.getConvenio().getCdsintegra());
			registro10.setCodigonaturezaoperacao(filtro.getNaturezaoperacao().getCdsintegra());
			registro10.setCodigofinalidadearquivo(filtro.getFinalidadearquivo().getCdsintegra());
			
			registro10.setRegistro11(createRegistro11(filtro));
		}	
		
		return registro10;
	}
	
	private Registro11 createRegistro11(SintegraFiltro filtro) {
		
		Registro11 registro11 = new Registro11();
		
		registro11.setTipo(Registro11.TIPO);
				
		registro11.setLogradouro(filtro.getEmpresa().getEnderecoLogradouro());
		registro11.setNumero(filtro.getEmpresa().getEnderecoNumero());
		registro11.setComplemento(filtro.getEmpresa().getEnderecoComplemento());
		registro11.setBairro(filtro.getEmpresa().getEnderecoBairro());
		registro11.setCep(filtro.getEmpresa().getEnderecoCep());
		if(filtro.getEmpresa().getResponsavel() != null && filtro.getEmpresa().getResponsavel().getNome() != null && !"".equals(filtro.getEmpresa().getResponsavel().getNome())){
			registro11.setNomecontato(filtro.getEmpresa().getResponsavel().getNome());
		}else {
			registro11.setNomecontato(filtro.getEmpresa().getRazaosocialOuNome());
		}
		registro11.setTelefone(filtro.getEmpresa().getTelefones());
		
		return registro11;
	}
	
	private List<Registro50> createRegistro50(SintegraFiltro filtro, List<NotaFiscalServico> listaNfs, List<Notafiscalproduto> listaNfp, 
			List<Entregadocumento> listaEntregadocumento, List<Arquivonfnota> listaArquivonfnotaNFS, List<Arquivonfnota> listaArquivonfnotaNFP, List<Inutilizacaonfe> listainuInutilizacaonfe) {
		
		List<Registro50> listaRegistro50 = new ArrayList<Registro50>();
		List<Registro50> listaRegistro50aux;
		Registro50 registro50;
				
		if(listaNfs != null && !listaNfs.isEmpty()){
			for(NotaFiscalServico nfs : listaNfs){				
				registro50 = new Registro50();	
				registro50.setTipo(Registro50.TIPO);
				
				if(nfs.getCliente() != null){
					registro50.setCnpj(nfs.getCliente().getCpfOuCnpj());	
					registro50.setInscricaoestadual(nfs.getCliente().getInscricaoestadual());
				}
				
				registro50.setDataemissaorecebimento(nfs.getDtEmissao());
				if(filtro.getEmpresa() != null && filtro.getEmpresa().getUfsped() != null)
					registro50.setUnidadefederacao(filtro.getEmpresa().getUfsped().getSigla());
				if(nfs.getCliente() != null){
					if(nfs.getCliente().getUfinscricaoestadual() != null)
						registro50.setUnidadefederacao(nfs.getCliente().getUfinscricaoestadual().getSigla());
					else if(nfs.getEnderecoCliente() != null && nfs.getEnderecoCliente().getPais() != null && !Pais.BRASIL.equals(nfs.getEnderecoCliente().getPais())){
						registro50.setUnidadefederacao("EX");
					}else {
						registro50.setUnidadefederacao(nfs.getCliente().getEnderecoSiglaEstadoIntegracaoDominio());
					}
				}
				
				if(NotaStatus.EMITIDA.equals(nfs.getNotaStatus()) || NotaStatus.LIQUIDADA.equals(nfs.getNotaStatus())){
					registro50.setModelo(filtro.getModelo());
				}else {
					// MODELO PADR�O NOTA FISCAL ELETR�NICA
					registro50.setModelo("55");
				}
				
				boolean achou = false;
				for (Arquivonfnota aux : listaArquivonfnotaNFS) {
					if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfs.getCdNota())){
						if(aux.getArquivonf() != null && aux.getArquivonf().getConfiguracaonfe() != null){
							registro50.setSerie(aux.getArquivonf().getConfiguracaonfe().getSerienfse());
							achou = true;
						}
						break;
					}
				}
				
				if(!achou){
					registro50.setSerie("1");
				}
				
				registro50.setNumero(nfs.getNumero());
				registro50.setCfop(adicionarCfopservico(nfs.getEnderecoCliente(), nfs.getEmpresa()));
				registro50.setEmitente("P");
				registro50.setValortotal(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue() : 0d);
				registro50.setBcicms(nfs.getBasecalculoicms()!= null ? nfs.getBasecalculoicms().getValue().doubleValue() : 0d);
				registro50.setValoricms(nfs.getValorIcms() != null ? nfs.getValorIcms().getValue().doubleValue() : 0d);
				registro50.setValorisentonaotributada(0d);
				registro50.setOutras(0d);
				registro50.setAliquotaicms(nfs.getIcms() != null ? nfs.getIcms() : null);
				registro50.setSituacao(printSituacao(nfs.getNotaStatus()));
						
				filtro.addRegistro(Registro50.TIPO);
				listaRegistro50.add(registro50);				
			}
		}
		
		if(listaNfp != null && !listaNfp.isEmpty()){
			
			for(Notafiscalproduto nfp : listaNfp){
				String serie = "1";
				String modelo = "55";
				if(NotaStatus.EMITIDA.equals(nfp.getNotaStatus()) || NotaStatus.LIQUIDADA.equals(nfp.getNotaStatus())){
					modelo = filtro.getModelo();
				}else {
					for (Arquivonfnota aux : listaArquivonfnotaNFP) {
						if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfp.getCdNota())){
							if(aux.getArquivonf() != null && aux.getArquivonf().getConfiguracaonfe() != null){
								modelo = aux.getArquivonf().getConfiguracaonfe().getModelonfe();	
							}
							if(aux.getNota() != null){
								serie = aux.getNota().getSerienfe() != null ? aux.getNota().getSerienfe().toString() : null;
							}
							break;
						}
					}
				}
				
				if(nfp.getNotaStatus() != null && nfp.getNotaStatus().equals(NotaStatus.NFE_DENEGADA)){
					registro50 = new Registro50();
					registro50.setTipo(Registro50.TIPO);
					registro50.setSituacao(printSituacao(nfp.getNotaStatus()));
					registro50.setDataemissaorecebimento(nfp.getDtEmissao());
					
					registro50.setSerie(serie);
					registro50.setModelo(modelo);
					registro50.setNumero(nfp.getNumero());
					registro50.setEmitente("P");
					
					registro50.setCnpj("");
					registro50.setCfop("");
					registro50.setValortotal(0d);
					registro50.setBcicms(0d);
					registro50.setValoricms(0d);
					registro50.setValorisentonaotributada(0d);
					registro50.setAliquotaicms(0d);
					registro50.setInscricaoestadual("");
					registro50.setOutras(0d);
					registro50.setUnidadefederacao("");
					
					filtro.addRegistro(Registro50.TIPO);
					listaRegistro50.add(registro50);
				}else {
					listaRegistro50aux = new ArrayList<Registro50>();
					nfp.setListaItens(agruparCfopAliquotaNfp(nfp.getListaItens()));
					for(Notafiscalprodutoitem item : nfp.getListaItens()){
						registro50 = new Registro50();	
						registro50.setTipo(Registro50.TIPO);
						if(nfp.getCliente() != null){
							registro50.setCnpj(nfp.getCliente().getCpfOuCnpj());
							registro50.setInscricaoestadual(nfp.getInscricaoestadual());
						}
						registro50.setDataemissaorecebimento(nfp.getDtEmissao());
	//					registro50.setUnidadefederacao(nfp.getCliente() != null && nfp.getCliente().getUfinscricaoestadual() != null ? 
	//							nfp.getCliente().getUfinscricaoestadual().getSigla() : "");	
						if((nfp.getCliente() != null && (nfp.getCliente().getInscricaoestadual() == null || 
													     "".equals(nfp.getCliente().getInscricaoestadual()) || 
													     "ISENTO".equalsIgnoreCase(nfp.getCliente().getInscricaoestadual()))) && 
								nfp.getEnderecoCliente() != null && nfp.getEnderecoCliente().getMunicipio() != null && 
								nfp.getEnderecoCliente().getMunicipio().getUf() != null && nfp.getEnderecoCliente().getMunicipio().getUf().getSigla() != null &&
								!"".equals(nfp.getEnderecoCliente().getMunicipio().getUf().getSigla())){
							registro50.setUnidadefederacao(nfp.getEnderecoCliente().getMunicipio().getUf().getSigla());
						}else if(nfp.getCliente() != null){
							if(nfp.getCliente().getUfinscricaoestadual() != null)
								registro50.setUnidadefederacao(nfp.getCliente().getUfinscricaoestadual().getSigla());
							else if(nfp.getEnderecoCliente() != null && nfp.getEnderecoCliente().getPais() != null && !Pais.BRASIL.equals(nfp.getEnderecoCliente().getPais())){
								registro50.setUnidadefederacao("EX");
							}else {
								registro50.setUnidadefederacao(nfp.getCliente().getEnderecoSiglaEstadoIntegracaoDominio());
							}
						}
						
						registro50.setSerie(serie);
						registro50.setModelo(modelo);
						
						registro50.setNumero(nfp.getNumero());
						if(item.getCfop() != null && item.getCfop().getCodigo() != null){
							registro50.setCfop(item.getCfop().getCodigo());						
						}
						registro50.setEmitente("P");
						
						double valortotal = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
						double valorOutras = valortotal;
						valortotal -= item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
						valortotal += item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
						valortotal += item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
						valortotal += item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : 0d;
						if(item.getTipocobrancaicms() != null && !item.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE) &&
								!item.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO)){
							valortotal += item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d;
						}
						valortotal += item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
						valortotal += item.getValorii() != null ? item.getValorii().getValue().doubleValue() : 0d;
						registro50.setValortotal(valortotal);
						
						registro50.setBcicms(item.getValorbcicms() != null ? nfp.getValorbcicms().getValue().doubleValue() : 0d);
						if(item.getValoricms() != null)
							registro50.setValoricms(item.getValoricms().getValue().doubleValue());
						else if(item.getValoricmsst() != null)
							registro50.setValoricms(item.getValoricmsst().getValue().doubleValue());
						else
							registro50.setValoricms(0d);					
						if(item.getTipocobrancaicms() == null || !Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms())){
							registro50.setValorisentonaotributada(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():0d);
						}else {
							registro50.setValorisentonaotributada(0d);
						}
						if(item.getTipocobrancaicms() != null && Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms())){
							registro50.setOutras(valorOutras+(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d)-registro50.getBcicms());
						}else if(item.getTipotributacaoicms() != null && item.getTipotributacaoicms().equals(Tipotributacaoicms.SIMPLES_NACIONAL)){
							registro50.setOutras(valorOutras);
						} else {
							registro50.setOutras(0d);
						}
						registro50.setAliquotaicms(item.getIcms() != null ? item.getIcms():0d);
						registro50.setSituacao(printSituacao(nfp.getNotaStatus()));
						
						filtro.addRegistro(Registro50.TIPO);
						listaRegistro50aux.add(registro50);
					}
					if(listaRegistro50aux != null && !listaRegistro50aux.isEmpty()){
						for(Registro50 r50 : listaRegistro50aux) listaRegistro50.add(r50);					
					}
				}
			}
			
		}

		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			boolean simplesnacional = false;
			for(Entregadocumento entregadocumento : listaEntregadocumento){
				simplesnacional = false;
				if(entregadocumento.getEmpresa() != null && entregadocumento.getEmpresa().getCrt() != null && 
						Codigoregimetributario.SIMPLES_NACIONAL.equals(entregadocumento.getEmpresa().getCrt())){
					simplesnacional = true;
				}
				listaRegistro50aux = new ArrayList<Registro50>();
				for(Entregamaterial em : entregadocumento.getListaEntregamaterial()){
					em.setValorproduto(em.getQtde() * em.getValorunitario());
				}
				entregadocumento.setListaEntregamaterial(agruparCfopAliquotaEntrega(entregadocumento.getListaEntregamaterial()));
				
				for(Entregamaterial em : entregadocumento.getListaEntregamaterial()){
					registro50 = new Registro50();				
					registro50.setTipo(Registro50.TIPO);
					if(entregadocumento.getFornecedor() != null){
						registro50.setCnpj(entregadocumento.getFornecedor().getCpfOuCnpj());
						registro50.setInscricaoestadual(entregadocumento.getFornecedor().getInscricaoestadual());
					}
					registro50.setDataemissaorecebimento(entregadocumento.getDtentrada());
					if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getUfinscricaoestadual() != null){
						registro50.setUnidadefederacao(entregadocumento.getFornecedor().getUfinscricaoestadual().getSigla());
					}else if(entregadocumento.getFornecedor() != null){
						registro50.setUnidadefederacao(entregadocumento.getFornecedor().getEnderecoSiglaEstadoIntegracaoDominio());
					}
					
					registro50.setModelo(entregadocumento.getModelodocumentofiscal() != null && entregadocumento.getModelodocumentofiscal().getCodigo() != null ? entregadocumento.getModelodocumentofiscal().getCodigo() : null);	
					registro50.setSerie(entregadocumento.getSerie() != null ? entregadocumento.getSerie().toString():null);
					registro50.setNumero(entregadocumento.getNumero());
					registro50.setCfop(em.getCfop() != null ? em.getCfop().getCodigo() : null);
					registro50.setEmitente("T");
					
					double valortotal = em.getValorproduto() != null ? em.getValorproduto() : 0d;
					valortotal += em.getValorfrete() != null ? em.getValorfrete().getValue().doubleValue() : 0d;
					valortotal += em.getValorseguro() != null ? em.getValorseguro().getValue().doubleValue() : 0d;
					valortotal += em.getValoricmsst() != null ? em.getValoricmsst().getValue().doubleValue() : 0d;
					valortotal += em.getValoripi() != null ? em.getValoripi().getValue().doubleValue() : 0d;
					valortotal += em.getValoroutrasdespesas() != null ? em.getValoroutrasdespesas().getValue().doubleValue() : 0d;
					valortotal -= em.getValordesconto() != null ? em.getValordesconto().getValue().doubleValue() : 0d;
					registro50.setValortotal(valortotal);
					
					registro50.setBcicms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue() : 0d);
					if(em.getValorbcicms() != null)
						registro50.setValoricms(em.getValoricms().getValue().doubleValue());
					else if(em.getValoricmsst() != null)
						registro50.setValoricms(em.getValoricmsst().getValue().doubleValue());
					else
						registro50.setValoricms(0d);
					registro50.setValorisentonaotributada(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue():0d);

					if(simplesnacional){
						registro50.setOutras(valortotal);
					} else {
						registro50.setOutras(0d);
					}
//					registro50.setAliquotaicms(em.getIcms() != null ? em.getIcms():0d);
					registro50.setAliquotaicms(em.getIcms() != null ? em.getIcms():0d);
					registro50.setSituacao("N");
					
					filtro.addRegistro(Registro50.TIPO);
					listaRegistro50aux.add(registro50);
				}
				
				if(listaRegistro50aux != null && !listaRegistro50aux.isEmpty()){
					for(Registro50 r50 : listaRegistro50aux) listaRegistro50.add(r50);					
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listainuInutilizacaonfe)){
			for(Inutilizacaonfe inutilizacaonfe : listainuInutilizacaonfe){
				if(inutilizacaonfe.getNuminicio() != null && inutilizacaonfe.getNumfim() != null){
					Integer tam = inutilizacaonfe.getNumfim()-inutilizacaonfe.getNuminicio()+1;
					for(int i = 0; i < tam; i++){
						registro50 = new Registro50();
						registro50.setTipo(Registro50.TIPO);
						registro50.setSituacao(printSituacao(inutilizacaonfe));
						registro50.setDataemissaorecebimento(inutilizacaonfe.getDtinutilizacao());
						registro50.setSerie("1");
						registro50.setModelo("55");
						registro50.setNumero("" + (inutilizacaonfe.getNuminicio()+i));
						registro50.setEmitente("P");
						
						registro50.setCnpj("");
						registro50.setCfop("");
						registro50.setValortotal(0d);
						registro50.setBcicms(0d);
						registro50.setValoricms(0d);
						registro50.setValorisentonaotributada(0d);
						registro50.setAliquotaicms(0d);
						registro50.setInscricaoestadual("");
						registro50.setOutras(0d);
						registro50.setUnidadefederacao("");
						
						filtro.addRegistro(Registro50.TIPO);
						listaRegistro50.add(registro50);
					}
				}
			}
		}
				
		return listaRegistro50;
	}	
			
	/**
	 * M�todo que verifica se a nota foi emitida dentro do estado ou para fora do estado
	 * isto � feito para escolher o CFOP da nota fiscal de servi�o
	 * CFOP 1933 - nota emitida para dentro do estado
	 * CFOP 2933 - nota emitida para fora do estado
	 *
	 * @param enderecoCliente
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	private String adicionarCfopservico(Endereco enderecoCliente, Empresa empresa) {
		String cfop = "";
		if(enderecoCliente != null && empresa != null){
			if(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null &&
					enderecoCliente.getMunicipio().getUf().getSigla() != null && empresa.getUfsped() != null &&
					empresa.getUfsped().getSigla() != null){
				if(enderecoCliente.getMunicipio().getUf().getSigla().equals(empresa.getUfsped().getSigla()))
					cfop = "1933";
				else
					cfop = "2933";
			}
		}
		return cfop;
	}
	
	private List<Registro51> createRegistro51(SintegraFiltro filtro, List<Notafiscalproduto> listaNfp, List<Entregadocumento> listaEntregadocumento, List<Arquivonfnota> listaArquivonfnotaNFP) {
		
		List<Registro51> listaRegistro51 = new ArrayList<Registro51>();
		Registro51 registro51;
				
		if(listaNfp != null && !listaNfp.isEmpty()){
			for(Notafiscalproduto nfp : listaNfp){	
				String modelo = "55";
				if(NotaStatus.EMITIDA.equals(nfp.getNotaStatus()) || NotaStatus.LIQUIDADA.equals(nfp.getNotaStatus())){
					modelo = filtro.getModelo();
				}else {
					for (Arquivonfnota aux : listaArquivonfnotaNFP) {
						if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfp.getCdNota())){
							if(aux.getArquivonf() != null && aux.getArquivonf().getConfiguracaonfe() != null){
								modelo = aux.getArquivonf().getConfiguracaonfe().getModelonfe();	
							}
							break;
						}
					}
				}
				
				if(!"55".equals(modelo) && !"01".equals(modelo)){
					continue;
				}
				
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
//					if(item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
						registro51 = new Registro51();
						registro51.setTipo(Registro51.TIPO);
						if(nfp.getCliente() != null){
							registro51.setCnpj(nfp.getCliente().getCpfOuCnpj());
							registro51.setInscricaoestadual(nfp.getCliente().getInscricaoestadual());
						}
						registro51.setDataemissaorecebimento(nfp.getDtEmissao());
						registro51.setUnidadefederacao(nfp.getCliente() != null && nfp.getCliente().getUfinscricaoestadual() != null ? 
								nfp.getCliente().getUfinscricaoestadual().getSigla() : "");
						
						boolean achou = false;
						for (Arquivonfnota aux : listaArquivonfnotaNFP) {
							if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfp.getCdNota())){
								registro51.setSerie(aux.getNota().getSerienfe() != null ? aux.getNota().getSerienfe().toString() : null);
								achou = true;
								break;
							}
						}
						if(!achou){
							registro51.setSerie("1");
						}
						
						registro51.setNumero(nfp.getNumero());
						registro51.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
						
						double valortotal = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
						valortotal -= item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
						valortotal += item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
						valortotal += item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
						valortotal += item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : 0d;
						if(item.getTipocobrancaicms() != null && !item.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE) &&
								!item.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO)){
							valortotal += item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d;
						}
						valortotal += item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
						valortotal += item.getValorii() != null ? item.getValorii().getValue().doubleValue() : 0d;
						registro51.setValortotal(valortotal);
						
//						registro51.setValoripi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : null);
//						registro51.setValorisentonaotributada(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():0d); 
//						registro51.setOutras(item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue():null);
						if(item.getTipocobrancaipi() != null && item.getValoripi() != null){
							if(item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_TRIBUTABA) || 
									item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_TRIBUTADA_ALIQUOTA_ZERO)){
								registro51.setValoripi(item.getValoripi().getValue().doubleValue());
							}else if(item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_ISENTA) || 
									item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_NAO_TRIBUTADA)){
								registro51.setValorisentonaotributada(item.getValoripi().getValue().doubleValue());
							}else if(item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_IMUNE) || 
									item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_SUSPENSAO) ||
									item.getTipocobrancaipi().equals(Tipocobrancaipi.SAIDA_OUTRAS)){
								registro51.setOutras(item.getValoripi().getValue().doubleValue());
							}
						}
	//					registro51.setBrancos();		
						registro51.setSituacao(printSituacao(nfp.getNotaStatus()));
						
						filtro.addRegistro(Registro51.TIPO);
						listaRegistro51.add(registro51);
//					}
				}
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				String modelo = ed.getModelodocumentofiscal() != null ? ed.getModelodocumentofiscal().getCodigo() : "";
				if((!"55".equals(modelo) && !"01".equals(modelo)) || (ed.getModelodocumentofiscal() != null && 
						(ed.getModelodocumentofiscal().getSintegra_51() == null || !ed.getModelodocumentofiscal().getSintegra_51()))){
					continue;
				}
				
				for(Entregamaterial item : ed.getListaEntregamaterial()){
//					if(item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
						registro51 = new Registro51();
						registro51.setTipo(Registro51.TIPO);
						
						if(ed.getFornecedor() != null){
							registro51.setCnpj(ed.getFornecedor().getCpfOuCnpj());
							registro51.setInscricaoestadual(ed.getFornecedor().getInscricaoestadual());
						}
						registro51.setDataemissaorecebimento(ed.getDtentrada());
						registro51.setUnidadefederacao(ed.getFornecedor() != null && ed.getFornecedor().getUfinscricaoestadual() != null ? 
								ed.getFornecedor().getUfinscricaoestadual().getSigla() : "");
						
						registro51.setSerie(ed.getSerie() != null ? ed.getSerie().toString() : null);
						registro51.setNumero(ed.getNumero());
						registro51.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
	
						double valortotal = item.getValorproduto() != null ? item.getValorproduto() : 0d;
						valortotal += item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
						valortotal += item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
						valortotal += item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d;
						valortotal += item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
						valortotal += item.getValoroutrasdespesas() != null ? item.getValoroutrasdespesas().getValue().doubleValue() : 0d;
	//					valortotal -= item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
						registro51.setValortotal(valortotal);
						
						registro51.setValoripi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : null);
						registro51.setValorisentonaotributada(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():0d);
						registro51.setOutras(item.getValoroutrasdespesas() != null ? item.getValoroutrasdespesas().getValue().doubleValue():0d);
	//					registro51.setBrancos();					
						registro51.setSituacao("N");
						
						filtro.addRegistro(Registro51.TIPO);
						listaRegistro51.add(registro51);
//					}
				}
			}
		}		
		return listaRegistro51;
	}
	
	private List<Registro53> createRegistro53(SintegraFiltro filtro, List<Notafiscalproduto> listaNfp, List<Entregadocumento> listaEntregadocumento, List<Arquivonfnota> listaArquivonfnotaNFP){
		
		List<Registro53> listaRegistro53 = new ArrayList<Registro53>();
		Registro53 registro53;		
		Money valordespesasacessorias = new Money();
		
		if(listaNfp != null && !listaNfp.isEmpty()){			
			for(Notafiscalproduto nfp : listaNfp){	
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
					if(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) || 
							Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
							Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ){
							
						registro53 = new Registro53();
						registro53.setTipo(Registro53.TIPO);
						if(nfp.getCliente() != null){
							registro53.setCnpj(nfp.getCliente() != null ? nfp.getCliente().getCpfOuCnpj() : "");
							registro53.setInscricaoestadual(nfp.getCliente().getInscricaoestadual());
						}
						registro53.setDataemissaorecebimento(nfp.getDtEmissao());
						registro53.setUnidadefederacao(nfp.getCliente() != null && nfp.getCliente().getUfinscricaoestadual() != null ? 
								nfp.getCliente().getUfinscricaoestadual().getSigla() : "");
						
	
						boolean achou = false;
						if(NotaStatus.EMITIDA.equals(nfp.getNotaStatus()) || NotaStatus.LIQUIDADA.equals(nfp.getNotaStatus())){
							registro53.setSerie("1");
							registro53.setModelo(filtro.getModelo());
							achou = true;
						}else {
							for (Arquivonfnota aux : listaArquivonfnotaNFP) {
								if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfp.getCdNota())){
									if(aux.getArquivonf() != null && aux.getArquivonf().getConfiguracaonfe() != null){
										registro53.setModelo(aux.getArquivonf().getConfiguracaonfe().getModelonfe());
										achou = true;
									}
									registro53.setSerie(aux.getNota().getSerienfe() != null ? aux.getNota().getSerienfe().toString() : null);
									break;
								}
							}
						}
						if(!achou){
							registro53.setSerie("1");
							registro53.setModelo("55");
						}
						
						registro53.setNumero(nfp.getNumero());
						registro53.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
						registro53.setEmitente("P");
						registro53.setBcicms(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue():null);
						registro53.setValoricmsretido(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : null);
						
						valordespesasacessorias = new Money();
						if(nfp.getOutrasdespesas() != null) 
							valordespesasacessorias = valordespesasacessorias.add(nfp.getOutrasdespesas());
						if(nfp.getValorfrete() != null)
							valordespesasacessorias = valordespesasacessorias.add(nfp.getValorfrete());
						if(nfp.getValorseguro() != null)
							valordespesasacessorias = valordespesasacessorias.add(nfp.getValorseguro());
						
						registro53.setDespesasacessorias(valordespesasacessorias != null ? valordespesasacessorias.getValue().doubleValue():null);
						registro53.setSituacao(printSituacao(nfp.getNotaStatus()));
	//					registro53.setCodigoantecipacao();
	//					registro53.setBrancos();					
						
						filtro.addRegistro(Registro53.TIPO);
						listaRegistro53.add(registro53);
					}
				}
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){	
				if(ed.getModelodocumentofiscal() != null && 
						(ed.getModelodocumentofiscal().getSintegra_53() == null || !ed.getModelodocumentofiscal().getSintegra_53())){
					continue;
				}
				for(Entregamaterial item : ed.getListaEntregamaterial()){
					if(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(item.getCsticms()) || 
							Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getCsticms()) ||
							Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getCsticms()) ||
							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getCsticms()) ||
							Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.equals(item.getCsticms()) ||
							Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getCsticms()) ||
//							Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getCsticms()) ||
							Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(item.getCsticms()) ){
						registro53 = new Registro53();
						registro53.setTipo(Registro53.TIPO);
						if(ed.getFornecedor() != null){
							registro53.setCnpj(ed.getFornecedor() != null ? ed.getFornecedor().getCpfOuCnpj() : "");
							registro53.setInscricaoestadual(ed.getFornecedor().getInscricaoestadual());
						}
						registro53.setDataemissaorecebimento(ed.getDtentrada());
						registro53.setUnidadefederacao(ed.getFornecedor() != null && ed.getFornecedor().getUfinscricaoestadual() != null ? 
								ed.getFornecedor().getUfinscricaoestadual().getSigla() : "");
											
						registro53.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
						registro53.setSerie(ed.getSerie() != null ? ed.getSerie().toString() : null);
						registro53.setNumero(ed.getNumero());
						registro53.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
						registro53.setEmitente("T");
						registro53.setBcicms(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue():null);
						registro53.setValoricmsretido(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue():null);
						
						valordespesasacessorias = new Money();
						if(item.getValoroutrasdespesas() != null)
							valordespesasacessorias = valordespesasacessorias.add(item.getValoroutrasdespesas());
						if(item.getValorfrete() != null)
							valordespesasacessorias = valordespesasacessorias.add(item.getValorfrete());
						if(item.getValorseguro() != null)
							valordespesasacessorias = valordespesasacessorias.add(item.getValorseguro());
						registro53.setDespesasacessorias(valordespesasacessorias != null ? valordespesasacessorias.getValue().doubleValue():null);
						registro53.setSituacao("N");
	//					registro53.setCodigoantecipacao();
	//					registro53.setBrancos();					
						
						filtro.addRegistro(Registro53.TIPO);
						listaRegistro53.add(registro53);
					}
				}
			}
		}		
		return listaRegistro53;		
	}
	
	private List<Registro54> createRegistro54(SintegraFiltro filtro, List<Notafiscalproduto> listaNfp, List<Entregadocumento> listaEntregadocumento, List<Arquivonfnota> listaArquivonfnotaNFP) {
		
		List<Registro54> listaRegistro54 = new ArrayList<Registro54>();		
		Integer ordemmaterial;
		if(listaNfp != null && !listaNfp.isEmpty()){
			for(Notafiscalproduto nfp : listaNfp){
				if(NotaStatus.CANCELADA.equals(nfp.getNotaStatus())){
					continue;
				}
				Boolean criarRegistroFrete = false;
				Boolean criarRegistroSeguro = false;
				Boolean criarRegistroDespesas = false;
				Money valorFrete = new Money();
				Money valorSeguro = new Money();
				Money valorDespesa = new Money();
				ordemmaterial = 1;
				Iterator<Notafiscalprodutoitem> listaNotaFiscalProdutoItem = nfp.getListaItens().iterator();
				while(listaNotaFiscalProdutoItem.hasNext()){
					Notafiscalprodutoitem item = listaNotaFiscalProdutoItem.next();
					createRegistro54(filtro, item, nfp, listaArquivonfnotaNFP, ordemmaterial, listaRegistro54, null, null, null);
					ordemmaterial++;
					
					if(item.getValorfrete() != null && item.getValorfrete().getValue().doubleValue() > 0){
						valorFrete = item.getValorfrete().add(valorFrete);
						criarRegistroFrete = true;
					}
					if(item.getValorseguro() != null && item.getValorseguro().getValue().doubleValue() > 0){
						valorSeguro = item.getValorseguro().add(valorSeguro);
						criarRegistroSeguro = true;
					}
					if(item.getOutrasdespesas() != null && item.getOutrasdespesas().getValue().doubleValue() > 0){
						valorDespesa = item.getOutrasdespesas().add(valorSeguro);
						criarRegistroDespesas = true;
					}
					if(criarRegistroFrete && !listaNotaFiscalProdutoItem.hasNext()){
						createRegistro54(filtro, item, nfp, listaArquivonfnotaNFP, ordemmaterial, listaRegistro54, valorFrete, null, null);						
						ordemmaterial++;
					}
					if(criarRegistroSeguro && !listaNotaFiscalProdutoItem.hasNext()){
						createRegistro54(filtro, item, nfp, listaArquivonfnotaNFP, ordemmaterial, listaRegistro54, null, valorSeguro, null);						
						ordemmaterial++;
					}
					if(criarRegistroDespesas && !listaNotaFiscalProdutoItem.hasNext()){
						createRegistro54(filtro, item, nfp, listaArquivonfnotaNFP, ordemmaterial, listaRegistro54, null, null, valorDespesa);						
						ordemmaterial++;
					}
				}
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				if((ed.getModelodocumentofiscal() != null && 
						(ed.getModelodocumentofiscal().getSintegra_54() == null || !ed.getModelodocumentofiscal().getSintegra_54()))){
					continue;
				}
				ordemmaterial = 1;
				Boolean criarRegistroFrete = false;
				Boolean criarRegistroSeguro = false;
				Boolean criarRegistroDespesas = false;
				Money valorFrete = new Money();
				Money valorSeguro = new Money();
				Money valorDespesa = new Money();
				Iterator<Entregamaterial> listaNotaFiscalEntregaMaterial = ed.getListaEntregamaterial().iterator();
				while(listaNotaFiscalEntregaMaterial.hasNext()){
					Entregamaterial item = listaNotaFiscalEntregaMaterial.next();
					createRegistro54(filtro, item, ed, ordemmaterial, listaRegistro54, null, null, null);
					ordemmaterial++;
					
					if(item.getValorfrete() != null && item.getValorfrete().getValue().doubleValue() > 0){
						valorFrete = item.getValorfrete().add(valorFrete);
						criarRegistroFrete = true;
					}
					if(item.getValorseguro() != null && item.getValorseguro().getValue().doubleValue() > 0){
						valorSeguro = item.getValorseguro().add(valorSeguro);
						criarRegistroSeguro = true;
					}
					if(item.getValoroutrasdespesas() != null && item.getValoroutrasdespesas().getValue().doubleValue() > 0){
						valorDespesa = item.getValoroutrasdespesas().add(valorSeguro);
						criarRegistroDespesas = true;
					}
					if(criarRegistroFrete && !listaNotaFiscalEntregaMaterial.hasNext()){
						createRegistro54(filtro, item, ed, ordemmaterial, listaRegistro54, valorFrete, null, null);
						ordemmaterial++;
					}
					if(criarRegistroSeguro && !listaNotaFiscalEntregaMaterial.hasNext()){
						createRegistro54(filtro, item, ed, ordemmaterial, listaRegistro54, null, valorSeguro, null);
						ordemmaterial++;
					}
					if(criarRegistroDespesas && !listaNotaFiscalEntregaMaterial.hasNext()){
						createRegistro54(filtro, item, ed, ordemmaterial, listaRegistro54, null, null, valorDespesa);
						ordemmaterial++;
					}
				}
			}
		}		
		
		return listaRegistro54;
	}
	
	private void createRegistro54(SintegraFiltro filtro, Entregamaterial item, Entregadocumento ed, Integer ordemmaterial, List<Registro54> listaRegistro54, Money valorFrete, Money valorSeguro, Money valorDespesaAcessorias) {
		Registro54 registro54 = new Registro54();
		registro54.setTipo(Registro54.TIPO);
		registro54.setCnpj(ed.getFornecedor() != null ? ed.getFornecedor().getCpfOuCnpj() : "");
		registro54.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
		registro54.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);
		registro54.setNumero(ed.getNumero());
		registro54.setCfop(item.getCfop() != null ? item.getCfop().getCodigo():null);
		
		if(valorFrete != null){
			registro54.setNumeroitem(991);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorFrete.getValue().doubleValue());
		}else if(valorSeguro != null){
			registro54.setNumeroitem(999);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorSeguro.getValue().doubleValue());
		}else if(valorDespesaAcessorias != null){
			registro54.setNumeroitem(999);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorDespesaAcessorias.getValue().doubleValue());
		}else {
			registro54.setCodigoprodutoservico(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(item.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
			registro54.setCst(adicionarCst((item.getMaterial() != null ? item.getMaterial().getOrigemproduto() : null), 
					item.getCsticms()));
			registro54.setNumeroitem(ordemmaterial);					
			registro54.setQuantidade(item.getQtde());
		
			item.setValorproduto(item.getQtde() * item.getValorunitario());
			
			double valortotal = item.getValorproduto() != null ? item.getValorproduto() : 0d;
	//		valortotal += item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
	//		valortotal += item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
	//		valortotal += item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d;
	//		valortotal += item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
	//		valortotal += item.getValoroutrasdespesas() != null ? item.getValoroutrasdespesas().getValue().doubleValue() : 0d;
	//		valortotal -= item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
			registro54.setValorproduto(valortotal);
			
			registro54.setValordescontodespesaacessoria(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : null);
			registro54.setBcicms(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue():null);
			registro54.setBcicmsretencao(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue():null);
			registro54.setValoripi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue():null);
			registro54.setAliquotaicms(item.getIcms() != null ? item.getIcms() : 0d);
		}
		
		filtro.addRegistro(Registro54.TIPO);
		listaRegistro54.add(registro54);
	}
	
	private void createRegistro54(SintegraFiltro filtro, Notafiscalprodutoitem item, Notafiscalproduto nfp, List<Arquivonfnota> listaArquivonfnotaNFP, Integer ordemmaterial, List<Registro54> listaRegistro54, Money valorFrete, Money valorSeguro, Money valorDespesaAcessorias) {
		Registro54 registro54 = new Registro54();
		registro54.setTipo(Registro54.TIPO);
		registro54.setCnpj(nfp.getCliente() != null ? nfp.getCliente().getCpfOuCnpj() : "");
		
		boolean achou = false;
		if(NotaStatus.EMITIDA.equals(nfp.getNotaStatus()) || NotaStatus.LIQUIDADA.equals(nfp.getNotaStatus())){
			registro54.setSerie("1");
			registro54.setModelo(filtro.getModelo());
			achou = true;
		}else {
			for (Arquivonfnota aux : listaArquivonfnotaNFP) {
				if(aux.getNota() != null && aux.getNota().getCdNota().equals(nfp.getCdNota())){
					if(aux.getArquivonf() != null && aux.getArquivonf().getConfiguracaonfe() != null){
						registro54.setModelo(aux.getArquivonf().getConfiguracaonfe().getModelonfe());
						achou = true;
					}
					registro54.setSerie(aux.getNota().getSerienfe() != null ? aux.getNota().getSerienfe().toString() : null);
					break;
				}
			}
		}
		if(!achou){
			registro54.setSerie("1");
			registro54.setModelo("55");
		}
		
		registro54.setNumero(nfp.getNumero());
		registro54.setCfop(item.getCfop() != null ? item.getCfop().getCodigo():null);
		
		
		if(valorFrete != null){
			registro54.setNumeroitem(991);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorFrete.getValue().doubleValue());
		}else if(valorSeguro != null){
			registro54.setNumeroitem(992);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorSeguro.getValue().doubleValue());
		}else if(valorDespesaAcessorias != null){
			registro54.setNumeroitem(999);
			registro54.setQuantidade(1d);
			registro54.setValorproduto(valorDespesaAcessorias.getValue().doubleValue());
		}else {
			registro54.setCodigoprodutoservico(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(item.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
			registro54.setCst(adicionarCst((item.getMaterial() != null ? item.getMaterial().getOrigemproduto() : null), 
					item.getTipocobrancaicms()));
			registro54.setNumeroitem(ordemmaterial);					
			registro54.setQuantidade(item.getQtde());
			registro54.setValorproduto(item.getValorunitario() != null ? 
				(item.getValorunitario() * (item.getQtde() != null ? item.getQtde() : 1.0)): null);
		
			Double valorDescontoDespesaAcessoria = null;
			if(item.getOutrasdespesas() != null){
				valorDescontoDespesaAcessoria = item.getOutrasdespesas().getValue().doubleValue();
			}
			if(item.getValordesconto() != null){
				if(valorDescontoDespesaAcessoria == null) valorDescontoDespesaAcessoria = 0d;
				valorDescontoDespesaAcessoria += item.getValordesconto().getValue().doubleValue();
			}
			
			registro54.setValordescontodespesaacessoria(valorDescontoDespesaAcessoria);
			registro54.setBcicms(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue():null);
			registro54.setBcicmsretencao(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():null);
			registro54.setValoripi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue():null);
			registro54.setAliquotaicms(item.getIcms() != null ? item.getIcms() : 0d);
		}
		
		filtro.addRegistro(Registro54.TIPO);
		listaRegistro54.add(registro54);
		
		ordemmaterial++;
	}
	
	private String adicionarCst(Origemproduto origemproduto, Tipocobrancaicms tipocobrancaicms) {
		String cst = "";
		
		if(origemproduto != null && (tipocobrancaicms == null || tipocobrancaicms.getCdnfe().length() < 3)) 
			cst += origemproduto.getValue();		
		if(tipocobrancaicms != null) cst += tipocobrancaicms.getCdnfe();
		
		if(cst != null && cst.length() == 2) cst = "0" + cst;
		
		return cst;
	}
	
//	private List<Registro55> createRegistro55(SintegraFiltro filtro) {
//		
//		List<Registro55> listaRegistro55 = new ArrayList<Registro55>();
//		List<Notafiscalproduto> listaNfp = notafiscalprodutoService.findForSIntegraRegistro50(filtro);
//		List<Entrega> listaEntrega = entregaService.findForSIntegraRegistro50(filtro);
//		Registro55 registro55;
//		
//		if(listaNfp != null && !listaNfp.isEmpty()){
//			for(Notafiscalproduto nfp : listaNfp){
//				for(Notafiscalprodutoitem item : nfp.getListaItens()){
//					registro55 = new Registro55();
//					registro55.setTipo(Registro55.TIPO);
//					if(nfp.getCliente() != null){
//						registro55.setCnpj(nfp.getCliente().getCpfOuCnpj());
//						registro55.setInscricaoestadual(nfp.getCliente().getInscricaoestadual());
//					}
//					registro55.setDatagnre();
//					registro55.setUnidadefederacaosubstituto();
//					registro55.setUnidadefederacaosubstituto();
//					registro55.setBancognre();
//					registro55.setAgenciagnre();
//					registro55.setNumerognre();
//					registro55.setValorgnre();
//					registro55.setDatavencimento();
//					registro55.setMesanoreferencia();
//					registro55.setNumeroconvenioprotocolomercadoria();		
//					
//					listaRegistro55.add(registro55);
//				}
//			}
//		}
//		
//		if(listaEntrega != null && !listaEntrega.isEmpty()){
//			for(Entrega e : listaEntrega){
//				for(Entregamaterial item : e.getListaEntregamaterial()){
//					registro55 = new Registro55();
//					registro55.setTipo(Registro55.TIPO);
//					if(e.getFornecedor() != null){
//						registro55.setCnpj(e.getFornecedor().getCpfOuCnpj());
//						registro55.setInscricaoestadual(e.getFornecedor().getInscricaoestadual());
//					}
//					registro55.setDatagnre();
//					registro55.setUnidadefederacaosubstituto();
//					registro55.setUnidadefederacaosubstituto();
//					registro55.setBancognre();
//					registro55.setAgenciagnre();
//					registro55.setNumerognre();
//					registro55.setValorgnre();
//					registro55.setDatavencimento();
//					registro55.setMesanoreferencia();
//					registro55.setNumeroconvenioprotocolomercadoria();		
//					
//					listaRegistro55.add(registro55);
//				}
//			}
//		}
//		
//		return listaRegistro55;		
//	}
	
	private List<Registro70> createRegistro70(SintegraFiltro filtro, List<Entregadocumento> listaEntregadocumento) {
		
		List<Registro70> listaRegistro70 = new ArrayList<Registro70>();				
		Registro70 registro70;
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					registro70 = new Registro70();
					registro70.setTipo(Registro70.TIPO);
					if(ed.getFornecedor() != null){
						registro70.setCnpj(ed.getFornecedor().getCpfOuCnpj());
						registro70.setInscricaoestadual(ed.getFornecedor().getInscricaoestadual());
					}
					registro70.setDataemissaoutilizacao(ed.getDtentrada());
					registro70.setUnidadefederacao(ed.getFornecedor() != null && ed.getFornecedor().getUfinscricaoestadual() != null ? 
							ed.getFornecedor().getUfinscricaoestadual().getSigla() : "");
					
					registro70.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
					registro70.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);
//					registro70.setSubserie();
					registro70.setNumero(ed.getNumero());
					registro70.setCfop(em.getCfop() != null ? em.getCfop().getCodigo():null);
					registro70.setValortotaldocumentofiscal(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue():null);
					registro70.setBcicms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue():null);
					registro70.setValoricms(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue():null);
					registro70.setValorisentanaotributada(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue():null);
					registro70.setOutras(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue():null);
//					registro70.setCiffoboutros();
					registro70.setSituacao("N");
					
					filtro.addRegistro(Registro70.TIPO);
					listaRegistro70.add(registro70);
				}
			}
		}
		
		return listaRegistro70;
	}
	
	private List<Registro71> createRegistro71(SintegraFiltro filtro, List<Entregadocumento> listaEntregadocumento) {
		
		List<Registro71> listaRegistro71 = new ArrayList<Registro71>();
		Registro71 registro71;
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){	
				if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getSintegra_71() != null && ed.getModelodocumentofiscal().getSintegra_71()){
					registro71 = new Registro71();
					registro71.setTipo(Registro71.TIPO);				
					if(ed.getFornecedor() != null){
						registro71.setCnpjtomador(ed.getFornecedor().getCpfOuCnpj());
						registro71.setCnpjtomador(ed.getFornecedor().getInscricaoestadual());
					}
						
					registro71.setDataemissao(ed.getDtentrada());
					if(ed.getFornecedor() != null && ed.getFornecedor().getUfinscricaoestadual() != null)
						registro71.setUnidadefederacaotomador(ed.getFornecedor().getUfinscricaoestadual().getSigla());
					
					registro71.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
					registro71.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);
//					registro71.setSubserie();
					registro71.setNumero(ed.getNumero());
//					registro71.setUnidadefederacaoremetentedestinatario();
//					registro71.setCnpjremetentedestinatario();
//					registro71.setDataemissaonota();
//					registro71.setModelonotafiscal();
//					registro71.setSerienotafiscal();
//					registro71.setNumeronotafiscal();
					registro71.setValortotalnotafiscal(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue():null);
//					registro71.setBrancos();
					
					filtro.addRegistro(Registro71.TIPO);
					listaRegistro71.add(registro71);
				}
			}
		}
		return listaRegistro71;
	}
	
	private List<Registro74> createRegistro74(SintegraFiltro filtro, Inventario inventario) {
		List<Registro74> listaRegistro74 = new ArrayList<Registro74>();

		Registro74 registro74;
		
		if(inventario != null && SinedUtil.isListNotEmpty(inventario.getListaInventariomaterial())){
			StringBuilder whereInCliente = new StringBuilder();
			StringBuilder whereInFornecedor = new StringBuilder();
			for(Inventariomaterial im : inventario.getListaInventariomaterial()){
				if(im.getPessoa() != null && im.getPessoa().getCdpessoa() != null){
					if(Tipopessoainventario.CLIENTE.equals(im.getTipopessoainventario())){
						whereInCliente.append(im.getPessoa().getCdpessoa()).append(",");
					}else if(Tipopessoainventario.FORNECEDOR.equals(im.getTipopessoainventario())){
						whereInFornecedor.append(im.getPessoa().getCdpessoa()).append(",");
					}
				}
			}
			
			List<Cliente> listaCliente = whereInCliente.length() > 0 ? clienteService.findForInventario(whereInCliente.substring(0, whereInCliente.length()-1)) : null;
			List<Fornecedor> listaFornecedor = whereInFornecedor.length() > 0 ? fornecedorService.findForInventario(whereInFornecedor.substring(0, whereInFornecedor.length()-1)) : null;
			Cliente cliente;
			Fornecedor fornecedor;
			Endereco endereco;
			for(Inventariomaterial im : inventario.getListaInventariomaterial()){
				registro74 = new Registro74();
				registro74.setTipo(Registro74.TIPO);
				registro74.setDatainventario(inventario.getDtinventario());
				registro74.setCodigoproduto(im.getMaterial().getCdmaterial());
				registro74.setQuantidade(im.getQtde());
				if(im.getValorUnitario() != null){
					registro74.setValorproduto(im.getValorUnitario() * (im.getQtde() != null ? im.getQtde() : 1));
				}else if(im.getMaterial().getValorcusto() != null){
					registro74.setValorproduto(im.getMaterial().getValorcusto() * (im.getQtde() != null ? im.getQtde() : 1));
				}
				registro74.setCodigopossemercadoriainventariada(im.getIndicadorpropriedade() != null ? 
						im.getIndicadorpropriedade().getCdSIntegra():null);
				if((im.getIndicadorpropriedade().equals(Indicadorpropriedade.PROPRIEDADE_DO_INFORMANTE__POSSE_TERCEIROS) || 
						im.getIndicadorpropriedade().equals(Indicadorpropriedade.PROPRIEDADE_DE_TERCEIROS__POSSE_INFORMANTE)) &&
						im.getPessoa() != null){
					registro74.setCnpjpossuidorproprietario(im.getPessoa().getCpfOuCnpj());
					if(Tipopessoainventario.CLIENTE.equals(im.getTipopessoainventario())){
						cliente = getClienteInventario(im.getPessoa(), listaCliente);
						if(cliente != null){
							registro74.setInscricaoestadualpossuidorproprietario(cliente.getInscricaoestadual());
							endereco = cliente.getEndereco();
							if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
								registro74.setUfpossuidorproprietario(endereco.getMunicipio().getUf().getSigla());
							}
						}
					}else if(Tipopessoainventario.FORNECEDOR.equals(im.getTipopessoainventario())){
						fornecedor = getFornecedorInventario(im.getPessoa(), listaFornecedor);
						if(fornecedor != null){
							registro74.setInscricaoestadualpossuidorproprietario(fornecedor.getInscricaoestadual());
							endereco = fornecedor.getEndereco();
							if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
								registro74.setUfpossuidorproprietario(endereco.getMunicipio().getUf().getSigla());
							}
						}
					}
				}
//					registro74.setBrancos();
				
				filtro.addRegistro(Registro74.TIPO);
				listaRegistro74.add(registro74);
			}
			
		}		
		return listaRegistro74;
	}
	
	private Fornecedor getFornecedorInventario(Pessoa pessoa, List<Fornecedor> listaFornecedor) {
		if(pessoa != null && SinedUtil.isListNotEmpty(listaFornecedor)){
			for(Fornecedor f : listaFornecedor){
				if(pessoa.equals(f)) return f;
			}
		}
		return null;
	}
	private Cliente getClienteInventario(Pessoa pessoa, List<Cliente> listaCliente) {
		if(pessoa != null && SinedUtil.isListNotEmpty(listaCliente)){
			for(Cliente c : listaCliente){
				if(pessoa.equals(c)) return c;
			}
		}
		return null;
	}
	
	private List<Registro75> createRegistro75(SintegraFiltro filtro, List<Notafiscalproduto> listaNfp, List<Entregadocumento> listaEntregadocumento, Inventario inventario, List<Emporiumvenda> listaEmporiumvenda, List<Notafiscalproduto> listaNfpConsumidor) {
		
		List<Registro75> listaRegistro75 = new ArrayList<Registro75>();	
		List<Registro75> listaRegistro75aux = new ArrayList<Registro75>();	
		Registro75 registro75 = new Registro75();
		HashMap<Integer, Integer> material = new HashMap<Integer, Integer>();	
		//HashMap<Registro75, Integer> mapaRegistro75 = new HashMap<Registro75, Integer>();
		
		if(listaNfp != null && !listaNfp.isEmpty()){
			for(Notafiscalproduto nfp : listaNfp){
				if(NotaStatus.CANCELADA.equals(nfp.getNotaStatus())){
					continue;
				}
				for(Notafiscalprodutoitem nfpi : nfp.getListaItens()){
					if(material.get(nfpi.getMaterial().getCdmaterial()) == null){
						registro75 = new Registro75();
						registro75.setTipo(Registro75.TIPO);
						registro75.setDatainicial(filtro.getDtinicio());
						registro75.setDatafinal(filtro.getDtfim());
						registro75.setCodigoprodutoservico(nfpi.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(nfpi.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
						registro75.setCodigoncm(nfpi.getMaterial().getNcmcompleto());		
						registro75.setDescricao(nfpi.getMaterial().getNome());	
						if(nfpi.getUnidademedida() == null){
							if(nfpi.getMaterial() != null && nfpi.getMaterial().getUnidademedida() != null){
								registro75.setUnidademedida(nfpi.getMaterial().getUnidademedida().getSimbolo());
							}
						} else {
							registro75.setUnidademedida(nfpi.getUnidademedida().getSimbolo());
						}
						registro75.setAliquotaipi(nfpi.getIpi());
						registro75.setAliquotaicms(nfpi.getIcms());
						registro75.setReducaobcicms(nfpi.getReducaobcicms() != null ? nfpi.getReducaobcicms():null);
						registro75.setBcicms(nfpi.getValorbcicms() != null ? nfp.getValorbcicms().getValue().doubleValue():null);
						
						filtro.addRegistro(Registro75.TIPO);
						material.put(nfpi.getMaterial().getCdmaterial(),nfpi.getMaterial().getCdmaterial());
						listaRegistro75.add(registro75);
					}
				}
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				if((ed.getModelodocumentofiscal() != null && 
						(ed.getModelodocumentofiscal().getSintegra_54() == null || !ed.getModelodocumentofiscal().getSintegra_54()))){
					continue;
				}
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					if(material.get(em.getMaterial().getCdmaterial()) == null){
						registro75 = new Registro75();
						registro75.setTipo(Registro75.TIPO);
						registro75.setDatainicial(filtro.getDtinicio());
						registro75.setDatafinal(filtro.getDtfim());
						registro75.setCodigoprodutoservico(em.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(em.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
						registro75.setCodigoncm(em.getMaterial().getNcmcompleto());		
						registro75.setDescricao(em.getMaterial().getNome());	
						if(em.getUnidademedidacomercial() == null){
							if(em.getMaterial() != null && em.getMaterial().getUnidademedida() != null){
								registro75.setUnidademedida(em.getMaterial().getUnidademedida().getSimbolo());
							}
						} else {
							registro75.setUnidademedida(em.getUnidademedidacomercial().getSimbolo());
						}
						registro75.setAliquotaipi(em.getIpi());
						registro75.setAliquotaicms(em.getIcms());
	//					registro75.setReducaobcicms();
						registro75.setBcicms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue():null);
						
						filtro.addRegistro(Registro75.TIPO);
						material.put(em.getMaterial().getCdmaterial(),em.getMaterial().getCdmaterial());
						listaRegistro75.add(registro75);
					}
				}
			}
		}
		
		if(inventario != null && SinedUtil.isListNotEmpty(inventario.getListaInventariomaterial())){
			for(Inventariomaterial im : inventario.getListaInventariomaterial()){
				if(material.get(im.getMaterial().getCdmaterial()) == null){
					registro75 = new Registro75();
					registro75.setTipo(Registro75.TIPO);
					registro75.setDatainicial(filtro.getDtinicio());
					registro75.setDatafinal(filtro.getDtfim());
					registro75.setCodigoprodutoservico(im.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(im.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
					registro75.setCodigoncm(im.getMaterial().getNcmcompleto());		
					registro75.setDescricao(im.getMaterial().getNome());	
					if(im.getMaterial() != null && im.getMaterial().getUnidademedida() != null){
						registro75.setUnidademedida(im.getMaterial().getUnidademedida().getSimbolo());
					}
//					registro75.setAliquotaipi();
//					registro75.setAliquotaicms();
//					registro75.setReducaobcicms();
//					registro75.setBcicms();
					
					filtro.addRegistro(Registro75.TIPO);
					material.put(im.getMaterial().getCdmaterial(),im.getMaterial().getCdmaterial());
					listaRegistro75.add(registro75);
				}
			}
		}
		
		if(listaEmporiumvenda != null && !listaEmporiumvenda.isEmpty()){
			Empresa empresaAux = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.integracaopdv");
			
			for (Emporiumvenda emporiumvenda : listaEmporiumvenda) {
				if(emporiumvenda.getLoja() != null && 
						empresaAux.getIntegracaopdv() != null &&
						emporiumvenda.getLoja().equalsIgnoreCase(empresaAux.getIntegracaopdv())){
					for (Emporiumvendaitem emporiumvendaitem : emporiumvenda.getListaEmporiumvendaitem()) {
						if(material.get(emporiumvendaitem.getMaterial().getCdmaterial()) == null){
							registro75 = new Registro75();
							registro75.setTipo(Registro75.TIPO);
							registro75.setDatainicial(filtro.getDtinicio());
							registro75.setDatafinal(filtro.getDtfim());
							registro75.setCodigoprodutoservico(emporiumvendaitem.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(emporiumvendaitem.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
							registro75.setCodigoncm(emporiumvendaitem.getMaterial().getNcmcompleto());		
							registro75.setDescricao(emporiumvendaitem.getMaterial().getNome());	
							if(emporiumvendaitem.getMaterial() != null && emporiumvendaitem.getMaterial().getUnidademedida() != null){
								registro75.setUnidademedida(emporiumvendaitem.getMaterial().getUnidademedida().getSimbolo());
							}
							filtro.addRegistro(Registro75.TIPO);
							material.put(emporiumvendaitem.getMaterial().getCdmaterial(), emporiumvendaitem.getMaterial().getCdmaterial());
							listaRegistro75.add(registro75);
						}
					}	
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaNfpConsumidor)){
			for (Notafiscalproduto nfpc : listaNfpConsumidor) {
				for (Notafiscalprodutoitem item : nfpc.getListaItens()) {
					if(material.get(item.getMaterial().getCdmaterial()) ==  null){
						
						registro75 = new Registro75();
						registro75.setTipo(Registro75.TIPO);
						registro75.setDatainicial(filtro.getDtinicio());
						registro75.setDatafinal(filtro.getDtfim());
						
						registro75.setCodigoprodutoservico(item.getMaterial().getCdmaterial() != null ? StringUtils.stringCheia(item.getMaterial().getCdmaterial().toString(), "0", 14, false) : null);
						registro75.setCodigoncm(item.getMaterial().getNcmcompleto());		
						registro75.setDescricao(item.getMaterial().getNome());	
						if(item.getUnidademedida() == null){
							if(item.getMaterial() != null && item.getMaterial().getUnidademedida() != null){
								registro75.setUnidademedida(item.getMaterial().getUnidademedida().getSimbolo());
							}
						}else {
							registro75.setUnidademedida(item.getUnidademedida().getSimbolo());
						}
						registro75.setAliquotaipi(item.getIpi());
						registro75.setAliquotaicms(item.getIcms());
						registro75.setReducaobcicms(item.getReducaobcicms() != null ? item.getReducaobcicms():null);
						registro75.setBcicms(item.getValorbcicms() != null ? nfpc.getValorbcicms().getValue().doubleValue():null);
						
						filtro.addRegistro(Registro75.TIPO);
						material.put(item.getMaterial().getCdmaterial(), item.getMaterial().getCdmaterial());
						listaRegistro75aux.add(registro75);	
					}
				}		
			}
		}
		
		listaRegistro75.addAll(listaRegistro75aux);
		
		return listaRegistro75;
	}
	
	private List<Registro76> createRegistro76(SintegraFiltro filtro, List<Entregadocumento> listaEntregadocumento) {
		
		List<Registro76> listaRegistro76 = new ArrayList<Registro76>();		
		Registro76 registro76;
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					registro76 = new Registro76();
					registro76.setTipo(Registro76.TIPO);
					if(ed.getFornecedor() != null){
						registro76.setCnpjcpf(ed.getFornecedor().getCpfOuCnpj());
						registro76.setInscricaoestadual(ed.getFornecedor().getInscricaoestadual());
					}					
					registro76.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
					registro76.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);		
//					registro76.setSubserie();		
					registro76.setNumero(ed.getNumero());
					registro76.setCfop(em.getCfop() != null ? em.getCfop().getCodigo():null);
					registro76.setTiporeceita(2);
					registro76.setDataemissaorecebimento(ed.getDtentrada());
					registro76.setUnidadefederacao(ed.getFornecedor() != null && ed.getFornecedor().getUfinscricaoestadual() != null ? 
							ed.getFornecedor().getUfinscricaoestadual().getSigla() : "");
					registro76.setValortotal(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue():0d);
					registro76.setBcicms(ed.getValortotalbcicms() != null ? ed.getValortotalbcicms().getValue().doubleValue():0d);
					registro76.setValoricms(ed.getValortotalicms() != null ? ed.getValortotalicms().getValue().doubleValue():0d);
					registro76.setValorisentanaotributada(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue():null);
					registro76.setOutras(ed.getValoroutrasdespesas() != null ? ed.getValoroutrasdespesas().getValue().doubleValue():null);
					registro76.setAliquota(em.getIcms());
					registro76.setSituacao("N");
					
					filtro.addRegistro(Registro76.TIPO);
					listaRegistro76.add(registro76);
				}
			}
		}
		
		return listaRegistro76;
	}
	
	private List<Registro77> createRegistro77(SintegraFiltro filtro, List<Entregadocumento> listaEntregadocumento) {
		
		List<Registro77> listaRegistro77 = new ArrayList<Registro77>();		
		Registro77 registro77;
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					registro77 = new Registro77();
					registro77.setTipo(Registro77.TIPO);
					registro77.setCnpjcpf(ed.getFornecedor() != null ? ed.getFornecedor().getCpfOuCnpj():null);
					registro77.setModelo(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
					registro77.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);		
//					registro77.setSubserie();		
					registro77.setNumero(ed.getNumero());
					registro77.setCfop(em.getCfop() != null ? em.getCfop().getCodigo():null);
					registro77.setTiporeceita(2);
					registro77.setNumeroitem(em.getCdentregamaterial());
					registro77.setCodigoservico(em.getMaterial().getCdmaterial() != null ? em.getMaterial().getCdmaterial().toString():null);
					registro77.setQuantidade(em.getQtde());
					registro77.setValorservico(em.getValorunitario());
					registro77.setValordescontodespesaacessoria(em.getValordesconto() != null ? em.getValordesconto().getValue().doubleValue() : null);
					registro77.setBcicms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue():null);
					registro77.setAliquotaicms(em.getIcms());
//					registro77.setCnpjmf();
//					registro77.setCodigonumeroterminal();
					
					filtro.addRegistro(Registro77.TIPO);
					listaRegistro77.add(registro77);
				}
			}
		}
		
		return listaRegistro77;
	}
	
	private List<Registro90> createRegistro90(SintegraFiltro filtro, SIntegra sintegra) {
		List<Registro90> listaRegistro90 = new ArrayList<Registro90>();
		
		Registro90 registro90 = new Registro90();		
		List<Totalizador> listaTotais = new ArrayList<Totalizador>();
		Totalizador totalizador;
		
		registro90.setTipo(Registro90.TIPO);
		registro90.setCnpjmf(filtro.getEmpresa().getCpfOuCnpj());
		registro90.setInscricaoestadual(filtro.getEmpresa().getInscricaoestadual());
		
		if(filtro.getMapTotalizadorRegistros() != null && filtro.getMapTotalizadorRegistros().size() > 0){
			int i = 1;
			for (String registro : filtro.getMapTotalizadorRegistros().keySet()){  
	        	if(i == 9){
	        		registro90.setTotalizador(listaTotais);
	        		if(registro90.toString().length() < 126){
	        			registro90.setBrancos(SIntegraUtil.geraStringVazia(126 - registro90.toString().length() + 2));
	        		}
	        		listaRegistro90.add(registro90);
	        		registro90 = new Registro90();
	        		registro90.setTipo(Registro90.TIPO);
	        		registro90.setCnpjmf(filtro.getEmpresa().getCpfOuCnpj());
	        		registro90.setInscricaoestadual(filtro.getEmpresa().getInscricaoestadual());
	        		listaTotais = new ArrayList<Totalizador>();
	        		i = 1;
	        	}else {	        		
	        		i++;
	        	}
	        	totalizador = new Totalizador();
        		totalizador.setTipototalizado(registro);
        		totalizador.setTotalregistros(filtro.getMapTotalizadorRegistros().get(registro));
        		listaTotais.add(totalizador);	            
	        }  
		}				
		
		registro90.setTotalizador(listaTotais);
		registro90.setTotalgeralRegistros(qtdeTotalRegistro(sintegra) + listaRegistro90.size() + 1);
		
		if(listaRegistro90 != null && !listaRegistro90.isEmpty()){			
			for(Registro90 r90 : listaRegistro90){
				r90.setNumeroregistrotipo90(listaRegistro90.size()+1);				
			}
		}
		
		registro90.setNumeroregistrotipo90(listaRegistro90.size()+1);
		if(registro90.toString().length() < 126){
			registro90.setBrancos(SIntegraUtil.geraStringVazia(126 - registro90.toString().length() + 2));
		}
		listaRegistro90.add(registro90);
		
		return listaRegistro90;
	}
	
	private Integer qtdeTotalRegistro(SIntegra sintegra) {
		Integer total = 2;
		if(sintegra != null){			
			total += sintegra.getListaRegistro50() != null ? sintegra.getListaRegistro50().size() : 0;
			total += sintegra.getListaRegistro51() != null ? sintegra.getListaRegistro51().size() : 0;
			total += sintegra.getListaRegistro53() != null ? sintegra.getListaRegistro53().size() : 0;
			total += sintegra.getListaRegistro54() != null ? sintegra.getListaRegistro54().size() : 0;
			total += sintegra.getListaRegistro55() != null ? sintegra.getListaRegistro55().size() : 0;
			total += sintegra.getListaRegistro60M() != null ? sintegra.getListaRegistro60M().size() : 0;
			total += sintegra.getListaRegistro61() != null ? sintegra.getListaRegistro61().size() : 0;
			total += sintegra.getListaRegistro61R() != null ? sintegra.getListaRegistro61R().size() : 0;
			total += sintegra.getListaRegistro70() != null ? sintegra.getListaRegistro70().size() : 0;
			total += sintegra.getListaRegistro71() != null ? sintegra.getListaRegistro71().size() : 0;
			total += sintegra.getListaRegistro74() != null ? sintegra.getListaRegistro74().size() : 0;
			total += sintegra.getListaRegistro75() != null ? sintegra.getListaRegistro75().size() : 0;
			total += sintegra.getListaRegistro76() != null ? sintegra.getListaRegistro76().size() : 0;
			total += sintegra.getListaRegistro77() != null ? sintegra.getListaRegistro77().size() : 0;				
			
			if(sintegra.getListaRegistro60M() != null){
				for (Registro60M registro60m : sintegra.getListaRegistro60M()) {
					total += registro60m.getListaRegistro60A() != null ? registro60m.getListaRegistro60A().size() : 0;
					total += registro60m.getListaRegistro60D() != null ? registro60m.getListaRegistro60D().size() : 0;
					total += registro60m.getListaRegistro60I() != null ? registro60m.getListaRegistro60I().size() : 0;
				}
			}
		}
		return total;
	}
	private String printSituacao(NotaStatus notaStatus){
		return printSituacao(notaStatus, null);
	}
	
	private String printSituacao(Inutilizacaonfe inutilizacaonfe){
		return printSituacao(null, inutilizacaonfe);
	}
	
	private String printSituacao(NotaStatus notaStatus, Inutilizacaonfe inutilizacaonfe) {
		String situacao = null;
		if(notaStatus != null){
			if(NotaStatus.NFSE_EMITIDA.equals(notaStatus) || NotaStatus.NFSE_LIQUIDADA.equals(notaStatus)) 
				situacao = "N";
			else if(NotaStatus.CANCELADA.equals(notaStatus))
				situacao = "S";
			if(NotaStatus.NFE_EMITIDA.equals(notaStatus) || NotaStatus.NFE_LIQUIDADA.equals(notaStatus) || NotaStatus.EMITIDA.equals(notaStatus) || NotaStatus.LIQUIDADA.equals(notaStatus)){
				situacao = "N";
			}else if(NotaStatus.NFE_DENEGADA.equals(notaStatus)){
				situacao = "2";
			}
		}else if(inutilizacaonfe != null){
			situacao = "4";
		}
		return situacao;
	}
	
	/**
	 * M�todo que agrupa os itens da nota fiscal de produto com CFOP e CSTPIS iguais
	 *
	 * @param listaItens
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalprodutoitem> agruparCfopAliquotaNfp(List<Notafiscalprodutoitem> listaItens) {
		HashMap<String, Integer> mapaCfopAliquota = new HashMap<String, Integer>();
		List<Notafiscalprodutoitem> listaAgrupada = new ArrayList<Notafiscalprodutoitem>();
		String cfopaliquota;
		Integer cdnfpi;
		
		if(listaItens != null && listaItens.size() > 1){
			for(Notafiscalprodutoitem notafiscalprodutoitem : listaItens){
				cfopaliquota = new String();
				if(notafiscalprodutoitem.getCfop() != null && notafiscalprodutoitem.getCfop() != null && notafiscalprodutoitem.getCfop().getCdcfop() != null){
					cfopaliquota += notafiscalprodutoitem.getCfop().getCdcfop(); 
				}
				if(notafiscalprodutoitem.getIcms() != null && notafiscalprodutoitem.getIcms() > 0){
					cfopaliquota += notafiscalprodutoitem.getIcms().toString();
				}
				if(mapaCfopAliquota.size() > 0){
					cdnfpi = mapaCfopAliquota.get(cfopaliquota);
					if(cdnfpi != null){
						for(Notafiscalprodutoitem nfp : listaItens){
							if(nfp.getCdnotafiscalprodutoitem() == cdnfpi){
								if(nfp.getValorbccofins() == null) nfp.setValorbccofins(new Money());
								if(nfp.getValoricms() == null) nfp.setValoricms(new Money());
								if(nfp.getValoricmsst() == null) nfp.setValoricmsst(new Money());
								if(nfp.getValoripi() == null) nfp.setValoripi(new Money());
								if(nfp.getValorii() == null) nfp.setValorii(new Money());
								if(nfp.getValorfrete() == null) nfp.setValorfrete(new Money());
								if(nfp.getValorseguro() == null) nfp.setValorseguro(new Money());
								if(nfp.getValordesconto() == null) nfp.setValordesconto(new Money());
								if(nfp.getOutrasdespesas() == null) nfp.setOutrasdespesas(new Money());
								
								if(notafiscalprodutoitem.getValorbccofins() == null) notafiscalprodutoitem.setValorbccofins(new Money());
								if(notafiscalprodutoitem.getValoricms() == null) notafiscalprodutoitem.setValoricms(new Money());
								if(notafiscalprodutoitem.getValoricmsst() == null) notafiscalprodutoitem.setValoricmsst(new Money());
								if(notafiscalprodutoitem.getValoripi() == null) notafiscalprodutoitem.setValoripi(new Money());
								if(notafiscalprodutoitem.getValorii() == null) notafiscalprodutoitem.setValorii(new Money());
								if(notafiscalprodutoitem.getValorfrete() == null) notafiscalprodutoitem.setValorfrete(new Money());
								if(notafiscalprodutoitem.getValorseguro() == null) notafiscalprodutoitem.setValorseguro(new Money());
								if(notafiscalprodutoitem.getValordesconto() == null) notafiscalprodutoitem.setValordesconto(new Money());
								if(notafiscalprodutoitem.getOutrasdespesas() == null) notafiscalprodutoitem.setOutrasdespesas(new Money());
								
								nfp.setValorbccofins(nfp.getValorbccofins().add(notafiscalprodutoitem.getValorbccofins()));
								nfp.setValoricms(nfp.getValoricms().add(notafiscalprodutoitem.getValoricms()));
								nfp.setValorbruto(nfp.getValorbruto().add(notafiscalprodutoitem.getValorbruto()));
								nfp.setValoricmsst(nfp.getValoricmsst().add(notafiscalprodutoitem.getValoricmsst()));
								nfp.setValoripi(nfp.getValoripi().add(notafiscalprodutoitem.getValoripi()));
								nfp.setValorii(nfp.getValorii().add(notafiscalprodutoitem.getValorii()));
								nfp.setValordesconto(nfp.getValordesconto().add(notafiscalprodutoitem.getValordesconto()));
								nfp.setValorfrete(nfp.getValorfrete().add(notafiscalprodutoitem.getValorfrete()));
								nfp.setValorseguro(nfp.getValorseguro().add(notafiscalprodutoitem.getValorseguro()));
								nfp.setOutrasdespesas(nfp.getOutrasdespesas().add(notafiscalprodutoitem.getOutrasdespesas()));
								break;
							}
						}
					}else{
						mapaCfopAliquota.put(cfopaliquota,notafiscalprodutoitem.getCdnotafiscalprodutoitem());
						listaAgrupada.add(notafiscalprodutoitem);
					}
				}else{
					mapaCfopAliquota.put(cfopaliquota, notafiscalprodutoitem.getCdnotafiscalprodutoitem());
					listaAgrupada.add(notafiscalprodutoitem);
				}
			}
			
			return listaAgrupada;
		}		
		
		return listaItens;
	}
	/**
	 * M�todo que agrupa os itens da entrega com CFOP e CSTPIS iguais
	 *
	 * @param listaEntregamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Set<Entregamaterial> agruparCfopAliquotaEntrega(Set<Entregamaterial> listaEntregamaterial) {
		HashMap<MultiKey, Integer> mapaCfopAliquota = new HashMap<MultiKey, Integer>();
		Set<Entregamaterial> listaAgrupada = new ListSet<Entregamaterial>(Entregamaterial.class);
		Integer cdentregamaterial;
		
		if(listaEntregamaterial != null && listaEntregamaterial.size() > 1){
			for(Entregamaterial entregamaterial : listaEntregamaterial){
				Integer cdcfop = null;
				Double aliqIcms = null;
				
				if(entregamaterial.getCfop() != null && entregamaterial.getCfop() != null && entregamaterial.getCfop().getCdcfop() != null){
					cdcfop = entregamaterial.getCfop().getCdcfop(); 
				}
				if(entregamaterial.getIcms() != null && entregamaterial.getIcms() > 0){
					aliqIcms = entregamaterial.getIcms();
				}
				
				MultiKey multiKey = new MultiKey(cdcfop, aliqIcms);
				if(mapaCfopAliquota.size() > 0){
					cdentregamaterial = mapaCfopAliquota.get(multiKey);
					if(cdentregamaterial != null){
						for(Entregamaterial em : listaEntregamaterial){
							if(em.getCdentregamaterial().equals(cdentregamaterial)){
								if(em.getValorbccofins() == null) em.setValorbccofins(new Money());
								if(em.getValorbcicms() == null) em.setValorbcicms(new Money());
								if(em.getValoricms() == null) em.setValoricms(new Money());
								if(em.getValoricmsst() == null) em.setValoricmsst(new Money());
								if(em.getValoripi() == null) em.setValoripi(new Money());
								if(em.getValorfrete() == null) em.setValorfrete(new Money());
								if(em.getValorseguro() == null) em.setValorseguro(new Money());
								if(em.getValoroutrasdespesas() == null) em.setValoroutrasdespesas(new Money());
								if(em.getValorproduto() == null) em.setValorproduto(0d);
								if(em.getValordesconto() == null) em.setValordesconto(new Money());
								
								if(entregamaterial.getValorbccofins() == null) entregamaterial.setValorbccofins(new Money());
								if(entregamaterial.getValorbcicms() == null) entregamaterial.setValorbcicms(new Money());
								if(entregamaterial.getValoricms() == null) entregamaterial.setValoricms(new Money());
								if(entregamaterial.getValoricmsst() == null) entregamaterial.setValoricmsst(new Money());
								if(entregamaterial.getValoripi() == null) entregamaterial.setValoripi(new Money());
								if(entregamaterial.getValorfrete() == null) entregamaterial.setValorfrete(new Money());
								if(entregamaterial.getValorseguro() == null) entregamaterial.setValorseguro(new Money());
								if(entregamaterial.getValoroutrasdespesas() == null) entregamaterial.setValoroutrasdespesas(new Money());
								if(entregamaterial.getValorproduto() == null) entregamaterial.setValorproduto(0d);
								if(entregamaterial.getValordesconto() == null) entregamaterial.setValordesconto(new Money());
								
								em.setValorbccofins(em.getValorbccofins().add(entregamaterial.getValorbccofins()));
								em.setValorbcicms(em.getValorbcicms().add(entregamaterial.getValorbcicms()));
								em.setValoricms(em.getValoricms().add(entregamaterial.getValoricms()));
								em.setValorproduto(em.getValorproduto() + entregamaterial.getValorproduto());
								em.setValoricmsst(em.getValoricmsst().add(entregamaterial.getValoricmsst()));
								em.setValoripi(em.getValoripi().add(entregamaterial.getValoripi()));
								em.setValorfrete(em.getValorfrete().add(entregamaterial.getValorfrete()));
								em.setValorseguro(em.getValorseguro().add(entregamaterial.getValorseguro()));
								em.setValoroutrasdespesas(em.getValoroutrasdespesas().add(entregamaterial.getValoroutrasdespesas()));
								em.setValordesconto(em.getValordesconto().add(entregamaterial.getValordesconto()));
								
								break;
							}
						}
					}else{
						mapaCfopAliquota.put(multiKey, entregamaterial.getCdentregamaterial());
						listaAgrupada.add(entregamaterial);
					}
				}else{
					mapaCfopAliquota.put(multiKey, entregamaterial.getCdentregamaterial());
					listaAgrupada.add(entregamaterial);
				}
			}
			
			return listaAgrupada;
		}		
		
		return listaEntregamaterial;		
	}
	
	private List<Registro61> createRegistro61(List<Notafiscalproduto> listaNFCe, SintegraFiltro filtro) {
		List<Registro61> listaRegistro61 = new ArrayList<Registro61>();
		Money valorTotalDiario = new Money();
		Money valorTotalBaseCalculoICMSDiario = new Money();
		Money valorTotalICMSDiario = new Money();
		Money valorTotalIsentaNaoTributadas = new Money();
		Money valorTotalOutras = new Money();
		Registro61 registro61 = new Registro61();
		
			for (int i = 0; i < listaNFCe.size(); i++) {
				Notafiscalproduto nfce = listaNFCe.get(i);
				
				//se for a primeira itera��o ou a serie ou a data forem diferentes, cria um novo objeto de registro61
				//a verifica��o da s�rie foi retirada, pois o validador s� aceita letras e no w3erp s�o usados n�meros
				//if(i == 0 || !nfce.getSerienfe().equals(listaNFCe.get(i-1).getSerienfe()) || SinedDateUtils.afterIgnoreHour(nfce.getDtEmissao(), listaNFCe.get(i-1).getDtEmissao())){
				if(i == 0 || SinedDateUtils.afterIgnoreHour(nfce.getDtEmissao(), listaNFCe.get(i-1).getDtEmissao())){
					//zera os totais
					valorTotalDiario = new Money();
					valorTotalBaseCalculoICMSDiario = new Money();
					valorTotalICMSDiario = new Money();
					 
					registro61 = new Registro61();
					
					registro61.setTipo(Registro61.TIPO);
					registro61.setDtEmissao(nfce.getDtEmissao());
					registro61.setModelo("65");
					//registro61.setSerie(nfce.getSerienfe().toString());			
					registro61.setNumeroInicialOrdem(nfce.getNumero());
					registro61.setNumeroFinalOrdem(nfce.getNumero());
					registro61.setDtEmissao(nfce.getDtEmissao());
					
					valorTotalDiario = nfce.getValor();
					if(nfce.getValorbcicms() != null){
						valorTotalBaseCalculoICMSDiario = nfce.getValorbcicms();						
					}
					if(nfce.getValoricms() != null){
						valorTotalICMSDiario = nfce.getValoricms();						
					}
					
					for (Notafiscalprodutoitem item : listaNFCe.get(i).getListaItens()) {
						if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIcms())){
							if(item.getValorbcicms() != null){
								valorTotalIsentaNaoTributadas = valorTotalIsentaNaoTributadas.add(item.getValorbcicms());
							}else if(item.getValorunitario() != null){
								Money valorUnitario = new Money(item.getValorunitario());
								valorTotalIsentaNaoTributadas = valorTotalIsentaNaoTributadas.add(valorUnitario);
							}
						}
						if(Tipocobrancaicms.OUTROS.equals(item.getTipocobrancaicms()) || Tipocobrancaicms.OUTROS_PARTILHA.equals(item.getTipocobrancaicms()) || Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms())){
							if(item.getValorbcicms() != null){
								valorTotalOutras = valorTotalOutras.add(item.getValorbcicms());
							}else if(item.getValorunitario() != null){
								Money valorUnitario = new Money(item.getValorunitario());
								valorTotalOutras = valorTotalOutras.add(valorUnitario);
							}
						}
					}	
					
					//se a data do pr�ximo registro for posterior ao registro atual ou a serie for diferente, adiciona ele na lista e na pr�xima itera��o cria um novo
					if(i == listaNFCe.size()-1 || !nfce.getSerienfe().equals(listaNFCe.get(i+1).getSerienfe()) || SinedDateUtils.beforeIgnoreHour(nfce.getDtEmissao(), listaNFCe.get(i+1).getDtEmissao())){
						registro61.setValorTotal(valorTotalDiario.getValue().doubleValue());
						registro61.setBaseCalculoICMS(valorTotalBaseCalculoICMSDiario.getValue().doubleValue());
						registro61.setValorICMS(valorTotalICMSDiario.getValue().doubleValue());					
						listaRegistro61.add(registro61);
					}	
				}else {
					valorTotalDiario = valorTotalDiario.add(nfce.getValor());
					valorTotalBaseCalculoICMSDiario = valorTotalBaseCalculoICMSDiario.add(nfce.getValorbcicms());
					valorTotalICMSDiario = valorTotalICMSDiario.add(nfce.getValoricms());
					
					//guarda os menores e maiores n�meros do dia
					BigDecimal numeroAtual = new BigDecimal(listaNFCe.get(i).getNumero());
					if(numeroAtual.compareTo(new BigDecimal(registro61.getNumeroInicialOrdem())) == -1){
						registro61.setNumeroInicialOrdem(numeroAtual.toString());
					}
					if(numeroAtual.compareTo(new BigDecimal(registro61.getNumeroFinalOrdem())) == 1){
						registro61.setNumeroFinalOrdem(numeroAtual.toString());
					}
		
					for (Notafiscalprodutoitem item : listaNFCe.get(i).getListaItens()) {
						if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIcms())){
							if(item.getValorbcicms() != null){
								valorTotalIsentaNaoTributadas = valorTotalIsentaNaoTributadas.add(item.getValorbcicms());
							}else if(item.getValorunitario() != null) {
								Money valorUnitario = new Money(item.getValorunitario());
								valorTotalIsentaNaoTributadas = valorTotalIsentaNaoTributadas.add(valorUnitario);
							}
						}
						if(Tipocobrancaicms.OUTROS.equals(item.getTipocobrancaicms()) || Tipocobrancaicms.OUTROS_PARTILHA.equals(item.getTipocobrancaicms()) || Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms())){
							if(item.getValorbcicms() != null){
								valorTotalOutras = valorTotalOutras.add(item.getValorbcicms());
							}else if(item.getValorunitario() != null){
								Money valorUnitario = new Money(item.getValorunitario());
								valorTotalOutras = valorTotalOutras.add(valorUnitario);
							}
						}
					}
					
					//se a data do pr�ximo registro for posterior ao registro atual ou a serie for diferente, adiciona ele na lista e na pr�xima itera��o cria um novo
					//a verifica��o da s�rie foi retirada, pois o validador s� aceita letras e no w3erp s�o usados n�meros
					//if(i == listaNFCe.size()-1 || !nfce.getSerienfe().equals(listaNFCe.get(i+1).getSerienfe()) || SinedDateUtils.beforeIgnoreHour(nfce.getDtEmissao(), listaNFCe.get(i+1).getDtEmissao())){
					if(i == listaNFCe.size()-1 || SinedDateUtils.beforeIgnoreHour(nfce.getDtEmissao(), listaNFCe.get(i+1).getDtEmissao())){
						registro61.setValorTotal(valorTotalDiario.getValue().doubleValue());
						registro61.setBaseCalculoICMS(valorTotalBaseCalculoICMSDiario.getValue().doubleValue());
						registro61.setValorICMS(valorTotalICMSDiario.getValue().doubleValue());
						listaRegistro61.add(registro61);
					}	
				}
			}
			
		for (int i=0; i < listaRegistro61.size(); i++){
			filtro.addRegistro(Registro61.TIPO);
		}
			
		return listaRegistro61;
	}
	
	private List<Registro61R> createRegistro61R(List<Notafiscalproduto> listaNFCe, SintegraFiltro filtro) {
		List<Registro61R> listaRegistro61R = new ArrayList<Registro61R>();
		
		HashMap<String, Registro61R> mapaMateriais = new HashMap<String, Registro61R>();
		
		if(SinedUtil.isListNotEmpty(listaNFCe)){
			for (Notafiscalproduto nf : listaNFCe) {
				if(SinedUtil.isListNotEmpty(nf.getListaItens())){
					for (Notafiscalprodutoitem item : nf.getListaItens()) {
						Registro61R registro61R;
						
						String periodoMaterial = SinedDateUtils.getMesAno(nf.getDtEmissao()) + " - " + item.getMaterial().getCdmaterial() + " - " + item.getIcms();
						if(mapaMateriais.containsKey(periodoMaterial)){
							registro61R = mapaMateriais.get(periodoMaterial);
							
							registro61R.setQuantidade(registro61R.getQuantidade() + item.getQtde());
							
							if(item.getValorbruto() != null){
								Money valorBruto = new Money(registro61R.getValorBrutoProduto());
								valorBruto = valorBruto.add(item.getValorbruto());
								registro61R.setValorBrutoProduto(valorBruto.getValue().doubleValue());								
							}
							
							if(item.getValorbcicms() != null){
								Money baseCalculoICMS = new Money(registro61R.getBaseCalculoICMS());
								baseCalculoICMS = baseCalculoICMS.add(item.getValorbcicms());
								registro61R.setBaseCalculoICMS(baseCalculoICMS.getValue().doubleValue());								
							}
							
							mapaMateriais.put(periodoMaterial, registro61R);
						}else {
							registro61R = new Registro61R();
							
							registro61R.setTipo(Registro61R.TIPO);
							registro61R.setMestreAnaliticoResumo(Registro61R.MESTREANALITICORESUMO);
							registro61R.setMesAnoEmissao(SinedDateUtils.getMesAno(nf.getDtEmissao()).replace("/", ""));
							registro61R.setCodigoProduto(item.getMaterial().getCdmaterial().toString());
							registro61R.setCodigoProduto(StringUtils.stringCheia(item.getMaterial().getCdmaterial().toString(), "0", 14, false));
							registro61R.setQuantidade(item.getQtde() != null ? item.getQtde() : 0.0d);
							registro61R.setValorBrutoProduto(item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0.0d);
							registro61R.setBaseCalculoICMS(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0.0d);
							registro61R.setAliquotaProduto(item.getIcms() != null ? item.getIcms() : 0.0d);
							
							mapaMateriais.put(SinedDateUtils.getMesAno(nf.getDtEmissao()) + " - " + item.getMaterial().getCdmaterial() + " - " + item.getIcms(), registro61R);
						}
					}					
				}	
			}
		}
		
		for (Registro61R registro61R : mapaMateriais.values()) {
			filtro.addRegistro(Registro61.TIPO);
			listaRegistro61R.add(registro61R);
		}
		
		return listaRegistro61R;
	}
	
}