package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocoluna;
import br.com.linkcom.sined.geral.dao.TelaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class TelaService extends GenericService<Tela> {
	private TelaDAO telaDAO;
	
	public void setTelaDAO(TelaDAO telaDAO) {
		this.telaDAO = telaDAO;
	}
	
	/**
	 * Refer�ncia ao m�todo no dao
	 * @param url
	 * @return 
	 * @author Pedro Gon�alves
	 * @see br.com.ideagri.web.geral.dao.TelaDAO#getTelaDescriptionByUrl
	 */
	public String getTelaDescriptionByUrl(String url) {
		return telaDAO.getTelaDescriptionByUrl(url);
	}
	
	/**
	 * Refer�ncia ao m�todo no dao
	 * @param url
	 * @return 
	 * @author Pedro Gon�alves
	 * @see br.com.linkcom.sined.geral.dao.TelaDAO#clearTelaCache()
	 */
	public void clearTelaCache(){
		telaDAO.clearTelaCache();
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TelaDAO#loadAll(String)
	 * @param whereIn
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tela> loadAll(String whereIn){
		return telaDAO.loadAll(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param pessoa
	 * @return
	 * @since 30/03/2011
	 */	
	public List<Tela> findAtalhoByPessoa(Pessoa pessoa) {
		return telaDAO.findAtalhoByPessoa(pessoa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param path
	 * @return
	 * @since 30/03/2011
	 */	
	public List<Tela> findByPath(String path) {
		return telaDAO.findByPath(path);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os campos da listagem
	* 
	* @see	br.com.linkcom.sined.geral.dao.TelaDAO#findForCampolistagemByPath(String path, Tipocoluna tipocoluna)
	*
	* @param path
	* @return
	* @since Oct 24, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findForCampolistagemByPath(String path, Tipocoluna tipocoluna) {
		return telaDAO.findForCampolistagemByPath(path, tipocoluna);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os campos da listagem
	*
	* @see br.com.linkcom.sined.geral.dao.TelaDAO#findForCampolistagemByPath(String path)
	* 
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findForCampolistagemByPath(String path) {
		return telaDAO.findForCampolistagemByPath(path);
	}
	
	
	/* singleton */
	private static TelaService instance;
	public static TelaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TelaService.class);
		}
		return instance;
	}

	/**
	* M�todo com refer�ncia no DAO
	* busca as colunas fixas da tela na listagem 
	* 
	* @see	br.com.linkcom.sined.geral.dao.TelaDAO#findColunaFixa(String path)
	* 
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findColunaFixa(String path) {
		return telaDAO.findColunaFixa(path);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca a tela
	* 
	* @see	br.com.linkcom.sined.geral.dao.TelaDAO#findTelaByPath(String path)
	*
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findTelaByPath(String path){
		return telaDAO.findTelaByPath(path);
	}
	
	/**
	 * M�todo com ref�ncia no DAO
	 * busca os campos da entrada de acordo com o par�metro ( url da tela )
	 *
	 * @see	br.com.linkcom.sined.geral.dao.TelaDAO.findForCampoentradaByPath(String path)
	 *
	 * @param path
	 * @return
	 * @author Luiz Fernando
	 */
	public Tela findForCampoentradaByPath(String path){
		return telaDAO.findForCampoentradaByPath(path);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * verifica se existe campos da entrada cadastrado para a tela (url da tela)
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.TelaDAO#isCampoentrada(String path)
	 *
	 * @param path
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isCampoentrada(String path){
		return telaDAO.isCampoentrada(path);
	}
	
	/**
	 * M�todo com refer�ncia no DAO 
	 * busca a descri��o da tela
	 * 
	 * @see	 br.com.linkcom.sined.geral.service.TelaService#getTituloMenuByUrl(String url)
	 *
	 * @param url
	 * @return
	 * @author Luiz Fernando
	 */
	public String getTituloMenuByUrl(String url){
		return telaDAO.getTituloMenuByUrl(url);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca as telas que utilizam a descri��o no menu
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.TelaDAO.findForTituloMenu()
	 * 
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Tela> findForTituloMenu(){
		return telaDAO.findForTituloMenu();
	}
}
