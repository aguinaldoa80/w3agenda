package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.dao.CboDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CboService extends GenericService<Cbo> {

	private CboDAO cboDAO;
	
	public void setCboDAO(CboDAO cboDAO) {
		this.cboDAO = cboDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CboDAO#insertCBO(Cbo cbo)
	 *
	 * @param cbo
	 * @since 02/05/2012
	 * @author Rodrigo Freitas
	 */
	public void insertCBO(Cbo cbo){
		cboDAO.insertCBO(cbo);
	}
}
