package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.dao.VeiculoordemservicoitemDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculomanutencaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.BaixamanutencaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculoordemservicoitemService extends GenericService<Veiculoordemservicoitem> {
	
	private VeiculoordemservicoitemDAO veiculoordemservicoitemDAO;
	
	public void setVeiculoordemservicoitemDAO(
			VeiculoordemservicoitemDAO veiculoordemservicoitemDAO) {
		this.veiculoordemservicoitemDAO = veiculoordemservicoitemDAO;
	}
	
//	/**
//	 * <b>M�todo respons�vel em apagar a referencia a uma ordem de servi�o item</b>
//	 * @author Biharck
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#deleteForIdOrdemServico(Ordemservico ordemservico)
//	 * @see findByItensForData(Veiculo veiculo, FormularioinspecaoFiltro filtro)
//	 * @param ordemservico
//	 */
//	public void deleteReferencia(Ordemservico ordemservico){
//		ordemservicoitemDAO.deleteForIdOrdemServico(ordemservico);
//	}
	/**
	 * M�todo para referenciar o DAO para gerar o relatorio Baixa de Manute��o 
	 * @author Ramon Brazil
	 * @param filtro 
	 * @return List <Ordemservicoitem>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findByItensForStatus(BaixamanutencaoFiltro)
	 */
	public List <Veiculoordemservicoitem> findByItensForStatus(BaixamanutencaoFiltro filtro ){
		return veiculoordemservicoitemDAO.findByItensForStatus(filtro);
	}
//	
//	/**
//	 * <b>M�todo respons�vel em carregar uma lista de ordem de servi�o item a partir de uma categoria<br>
//	 * contida em modelo</b>
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#loadListaOrdemServicoItem(Modelo modelo)
//	 * @author Biharck
//	 * @param modelo
//	 * @return uma lista de ordem de servi�o item
//	 */
//	public List<Ordemservicoitem> loadListaOrdemServicoItem(Modelo modelo){
//		return ordemservicoitemDAO.loadListaOrdemServicoItem(modelo);
//	}
//
	/**
	 * <b>M�todo de refer�ncia ao DAO <b>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findOrdemServicoItemByOrdemServico(Ordemservico)
	 * @author Jo�o Paulo Zica
	 * @param ordemservico
	 * @return uma lista de ordem de servi�o item
	 */
	public List<Veiculoordemservicoitem> findOrdemServicoItemByOrdemServico(Veiculoordemservico ordemservico) {
		return veiculoordemservicoitemDAO.findOrdemServicoItemByOrdemServico(ordemservico);
	}
	
	/**
	 * <b>M�todo de refer�ncia ao DAO <b>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findOrdemServicoItemByManutencaoFilter(ManutencaoFiltro)
	 * @author Jo�o Paulo Zica
	 * @param filtro
	 * @param status
	 * @return uma lista de ordem de servi�o item
	 */
	public List<Veiculoordemservicoitem> findOrdemServicoItemByManutencaoFilter(VeiculomanutencaoFiltro filtro) {
		return veiculoordemservicoitemDAO.findOrdemServicoItemByManutencaoFilter(filtro);
	}
//	
//	/**
//	 * <b>M�todo de refer�ncia ao DAO <b>
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findOrdemServicoItemByCodigoOrdemServico(Integer)
//	 * @author Jo�o Paulo Zica
//	 * @param filtro
//	 * @return uma ordem de servi�o item
//	 */
//	public Ordemservicoitem findOrdemServicoItemByCodigoOrdemServico(Integer id) {
//		return ordemservicoitemDAO.findOrdemServicoItemByCodigoOrdemServico(id);
//	}
	
	/**
	 * <b>M�todo de refer�ncia ao DAO <b>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#carregaOrdemServicoItem(Ordemservicoitem)
	 * @author Jo�o Paulo Zica
	 * @param ordemservicoitem
	 * @return uma ordem de servi�o item
	 */
	public Veiculoordemservicoitem carregaOrdemServicoItem(Veiculoordemservicoitem ordemservicoitem){
		return veiculoordemservicoitemDAO.carregaOrdemServicoItem(ordemservicoitem);
	}
//	
//	/**
//	 * <b>M�todo respons�vel em localizar uma lista de ordem de servico item a partir de uma ordem de servico</b>
//	 * @author Biharck
//	 * @param ordemservico
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#loadListaOrdemServicoItemByOrdemServico(Ordemservico ordemservico)
//	 * @return uma lista de ordem de servico item
//	 */
//	public List<Ordemservicoitem> loadListaOrdemServicoItemByOrdemServico(Ordemservico ordemservico,Boolean tipo){
//		return ordemservicoitemDAO.loadListaOrdemServicoItemByOrdemServico(ordemservico, tipo);
//	}
//	
	/**
	 * Carrega todas as OSI da ordem de servi�o que est�o marcadas como true no status.
	 * @author Pedro Gon�alves
	 * @param ordemservico
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#carregarOSIMarcados(Ordemservico)
	 * @return uma lista de ordem de servico item
	 */
	public List<Veiculoordemservicoitem> carregarOSIMarcados(Veiculoordemservico ordemservico){
		return veiculoordemservicoitemDAO.carregarOSIMarcados(ordemservico);
	}
//	
//	/**
//	 * <b>M�todo respons�vel em localizar um auto relacionamento</b>
//	 * @author Biharck
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findForId(Ordemservico ordemservico)
//	 * @param ordemservico
//	 * @return
//	 */
//	public Ordemservicoitem findForId(Ordemservico ordemservico){
//		return ordemservicoitemDAO.findForId(ordemservico);
//	}
	
	/**
	 * <b>M�todo de refer�ncia ao DAO</b>
	 * 
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#saveOrdemServicoItem(Integer)
	 * @author Jo�o Paulo Zica
	 * @param cdordemservicoitem
	 * @return
	 */
	public void saveOrdemServicoItem(Integer cdordemservicoitem){
		veiculoordemservicoitemDAO.saveOrdemServicoItem(cdordemservicoitem);
	}
	
	
	/**
	 * <b>M�todo de refer�ncia ao DAO.</b>
	 * 
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findOrdemServicoItemInspecao(Ordemservicoitem)
	 * @param ordemservicoitem
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Veiculoordemservicoitem findOrdemServicoItemInspecao(Veiculoordemservicoitem ordemservicoitem){
		return veiculoordemservicoitemDAO.findOrdemServicoItemInspecao(ordemservicoitem);
	}
	
//	/**
//	 * <b>M�todo de refer�ncia ao DAO</b>
//	 * 
//	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoordemservicoitemDAO#findOrdemServicoItemStatusByOrdemServico(Ordemservico)
//	 * @param ordemservico
//	 * @return List<Boolean>
//	 */
//	public List<Ordemservicoitem> findOrdemServicoItemStatusByOrdemServico(Ordemservico ordemservico) {
//		return ordemservicoitemDAO.findOrdemServicoItemStatusByOrdemServico(ordemservico);
//	}
	
	/* singleton */
	private static VeiculoordemservicoitemService instance;
	public static VeiculoordemservicoitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VeiculoordemservicoitemService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoitemDAO#getIdWhereNotIn
	 * 
	 * @param whereIn
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Integer> getIdWhereNotIn(String whereIn, Veiculoordemservico bean) {
		return veiculoordemservicoitemDAO.getIdWhereNotIn(whereIn, bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoitemDAO#carregaListaOrdemServicoItem
	 * 
	 * @param veiculoordemservico
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Veiculoordemservicoitem> carregaListaOrdemServicoItem(Veiculoordemservico veiculoordemservico) {
		return veiculoordemservicoitemDAO.carregaListaOrdemServicoItem(veiculoordemservico);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculoordemservicoitemDAO#carregaOrdemServicoByItem
	 * 
	 * @param veiculoordemservico
	 * @return
	 * @author Jo�o Vitor
	 */
	public Veiculoordemservicoitem carregaOrdemServicoItem(Veiculoordemservico veiculoordemservico, Categoriaiteminspecao categoriaiteminspecao) {
		return veiculoordemservicoitemDAO.carregaOrdemServicoItem(veiculoordemservico, categoriaiteminspecao);
	} 
}
