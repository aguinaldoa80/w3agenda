package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoetapanomecampo;
import br.com.linkcom.sined.geral.dao.ProducaoetapanomecampoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanomecampo.ProducaoEtapaNomeCampoW3producaoRESTModel;

public class ProducaoetapanomecampoService extends GenericService<Producaoetapanomecampo>{
	
	private ProducaoetapanomecampoDAO producaoetapanomecampoDAO;
	
	public void setProducaoetapanomecampoDAO(
			ProducaoetapanomecampoDAO producaoetapanomecampoDAO) {
		this.producaoetapanomecampoDAO = producaoetapanomecampoDAO;
	}

	public List<ProducaoEtapaNomeCampoW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	public List<ProducaoEtapaNomeCampoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ProducaoEtapaNomeCampoW3producaoRESTModel> lista = new ArrayList<ProducaoEtapaNomeCampoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Producaoetapanomecampo pen : producaoetapanomecampoDAO.findForW3Producao(whereIn))
				lista.add(new ProducaoEtapaNomeCampoW3producaoRESTModel(pen));
		}
		
		return lista;
	}
	
}
