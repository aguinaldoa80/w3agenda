package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentonegociado;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentonegociadoService extends GenericService<Documentonegociado> {	
	
	/* singleton */
	private static DocumentonegociadoService instance;
	public static DocumentonegociadoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentonegociadoService.class);
		}
		return instance;
	}
	
}
