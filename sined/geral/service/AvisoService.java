package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.Tipoaviso;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.bean.Usuarioprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.dao.AvisoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.OrigemavisoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GerenciaavisoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.thread.EmailManagerThreadAvisos;

public class AvisoService extends GenericService<Aviso> {

	
	private final String AVISO_STRING_SESSION = "AVISO_STRING_SESSION";
	private final String emailRemetente = "w3erp@w3erp.com.br";
	
	private AvisoDAO  avisoDAO;
	private AvisousuarioService avisousuarioService;
	private ParametrogeralService parametrogeralService;
	private UsuariopapelService usuariopapelService;
	private UsuarioService usuarioService;
	private MotivoavisoService motivoavisoService;
	private AgendamentoService agendamentoService;
	private ContapagarService contaPagarService;
	private ContareceberService contaReceberService;
	private VendaService vendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private SolicitacaocompraService solicitacaocompraService;
	private CotacaoService cotacaoService;
	private OrdemcompraService ordemcompraService;
	private EntregaService entregaService;
	private ColetaService coletaService;
	private LocacaomaterialService locacaomaterialService;
	private FornecimentoService fornecimentoService;
	private EntradafiscalService entradafiscalService;
	private AgendainteracaoService agendainteracaoService;
	private RequisicaoService requisicaoService;
	private SpedarquivoService spedArquivoService;
	private SpedpiscofinsService spedPisCofinsService;
	private MovimentacaoService movimentacaoService;
	private EmpresaService empresaService;
	private CalendarioService calendarioService;
	private FechamentofinanceiroService fechamentoFinanceiroService;
	private EnvioemailService envioEmailService;
	private SolicitacaoservicoService solicitacaoservicoService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setAvisoDAO(AvisoDAO avisoDAO) {
		this.avisoDAO = avisoDAO;
	}
	public void setAvisousuarioService(AvisousuarioService avisousuarioService) {
		this.avisousuarioService = avisousuarioService;
	}
	public void setUsuariopapelService(UsuariopapelService usuariopapelService) {
		this.usuariopapelService = usuariopapelService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setContaPagarService(ContapagarService contaPagarService) {
		this.contaPagarService = contaPagarService;
	}
	public void setContaReceberService(ContareceberService contaReceberService) {
		this.contaReceberService = contaReceberService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setLocacaomaterialService(LocacaomaterialService locacaomaterialService) {
		this.locacaomaterialService = locacaomaterialService;
	}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {
		this.agendainteracaoService = agendainteracaoService;
	}
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	public void setSpedArquivoService(SpedarquivoService spedArquivoService) {
		this.spedArquivoService = spedArquivoService;
	}
	public void setSpedPisCofinsService(SpedpiscofinsService spedPisCofinsService) {
		this.spedPisCofinsService = spedPisCofinsService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCalendarioService(CalendarioService calendarioService) {
		this.calendarioService = calendarioService;
	}
	public void setFechamentoFinanceiroService(FechamentofinanceiroService fechamentoFinanceiroService) {
		this.fechamentoFinanceiroService = fechamentoFinanceiroService;
	}
	public void setEnvioEmailService(EnvioemailService envioEmailService) {
		this.envioEmailService = envioEmailService;
	}
	public void setSolicitacaoservicoService(SolicitacaoservicoService solicitacaoservicoService) {
		this.solicitacaoservicoService = solicitacaoservicoService;
	}
	
	private static AvisoService instance;
	public static AvisoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AvisoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que faz listagem padr�o do flex
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> findForListagemFlex() {
		return this.findForListagemFlex(new GerenciaavisoFiltro());
	}
	
	/**
	 * M�todo que faz listagem padr�o do flex passando filtro
	 * 
	 * @param gerenciaavisoFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> findForListagemFlex(GerenciaavisoFiltro gerenciaavisoFiltro) {
		return this.findListagemFlex(gerenciaavisoFiltro, true);
	}
	
	public List<Aviso> findListagemFlex(GerenciaavisoFiltro gerenciaavisoFiltro, boolean flex){
		Usuario user = (Usuario)Neo.getUser();

		if(NeoWeb.getRequestContext().getSession().getAttribute(AVISO_STRING_SESSION) == null && flex){
			this.marcarAvisosAntigos(user);
			avisoDAO.deleteAvisosVelhos();
		}
		
		gerenciaavisoFiltro.setUsuario(user);
		return avisoDAO.findForListagemFlex(gerenciaavisoFiltro);
	}
	
	/**
	 * M�todo que marca os avisos antigos.
	 *
	 * @param user
	 * @author Rodrigo Freitas
	 */
	private void marcarAvisosAntigos(Usuario user) {
		Integer diasAvisoExpirar = 15;
		try {
			diasAvisoExpirar = Integer.parseInt(parametrogeralService.getValorPorNome("diasAvisoExpirar"));
		} catch (Exception e) {
			Parametrogeral parametrogeral = new Parametrogeral();
			parametrogeral.setNome("diasAvisoExpirar");
			parametrogeral.setValor("15");			
			parametrogeralService.saveOrUpdate(parametrogeral);
		}
		
		Date data = SinedDateUtils.currentDateToBeginOfDay();
		data = SinedDateUtils.incrementDate(data, (diasAvisoExpirar*(-1)), Calendar.DAY_OF_MONTH);
		
		GerenciaavisoFiltro filtro = new GerenciaavisoFiltro();
		filtro.setDtavisoate(data);
		filtro.setUsuario(user);

		List<Aviso> listaAviso = this.findListagemFlex(filtro, false);
		this.preparaMarcarLidoAvisoFlex(listaAviso);
		
		NeoWeb.getRequestContext().getSession().setAttribute(AVISO_STRING_SESSION, Boolean.TRUE);
	}
	
	/**
	 * M�todo que salva avisos como lidos do usu�rio
	 * 
	 * @param avisos
	 * @author Tom�s Rabelo
	 */
	public void preparaMarcarLidoAvisoFlex(List<Aviso> avisos) {
		avisousuarioService.marcaItensComoLidos(avisos);
	}

	/**
	 * M�todo que deleta avisos lidos do usu�rio
	 * 
	 * @param avisos
	 * @author Tom�s Rabelo
	 */
	public void preparaMarcarNaoLidoAvisoFlex(List<Aviso> avisos) {
		avisousuarioService.marcaItensComoNaoLidos(avisos);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @param cliente
	 * @param categoria
	 * @return
	 * @author Taidson
	 * @since 16/11/2010
	 */
	public List<Aviso> avisoBoleto(Cliente cliente, String whereIn){
		return avisoDAO.avisoBoleto(cliente, whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.AvisoDAO#findForAgenda(Date data, Usuario usuario)
	 *
	 * @param data
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Aviso> findForAgenda(Date data, Usuario usuario) {
		return avisoDAO.findForAgenda(data, usuario);
	}
	
	public List<OrigemavisoBean> montaDestino(Aviso aviso) {
		List<OrigemavisoBean> listaBean = new ArrayList<OrigemavisoBean>();
		if(aviso.getAvisoOrigem() != null && aviso.getIdOrigem() != null){
			if(OrigemavisoBean.SOLICITACAO_DE_COMPRA.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.SOLICITACAO_DE_COMPRA, "SOLICITACAO DE COMPRA", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.COTACAO.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.COTACAO, "COTA��O", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.ORDEM_DE_COMPRA.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ORDEM_DE_COMPRA, "ORDEM DE COMPRA", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.ENTREGA.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ENTREGA, "ENTREGA", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.ROMANEIO.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ROMANEIO, "ROMANEIO", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.AGENDAMENTO.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.AGENDAMENTO, "AGENDAMENTO", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.CONTA_A_PAGAR.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.CONTA_A_PAGAR, "CONTA A PAGAR", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.PLANEJAMENTO.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.PLANEJAMENTO, "PLANEJAMENTO", aviso.getIdOrigem().toString()));
			}else if(OrigemavisoBean.PEDIDOVENDA.equals(aviso.getAvisoOrigem().ordinal())){
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.PEDIDOVENDA, "PEDIDO DE VENDA", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.RECEBIMENTO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.RECEBIMENTO, "RECEBIMENTO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.LOTE_NOTA_FISCAL.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.LOTE_NOTA_FISCAL, "LOTE NOTA FISCAL", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.ESTOQUE.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ESTOQUE, "ESTOQUE", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.CONTA_A_RECEBER.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.CONTA_A_RECEBER, "CONTA A RECEBER", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.VENDA.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.VENDA, "VENDA", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.NOTA_FISCAL.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.NOTA_FISCAL, "NOTA FISCAL", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.COLETA.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.COLETA, "COLETA", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.FATURA_LOCACAO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.FATURA_LOCACAO, "FATURA LOCACAO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.FORNECIMENTO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.FORNECIMENTO, "FORNECIMENTO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.ENTRADA_FISCAL.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ENTRADA_FISCAL, "ENTRADA FISCAL", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.AGENDA_INTERACAO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.AGENDA_INTERACAO, "AGENDA INTERACAO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.ORDEM_SERVICO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.ORDEM_SERVICO, "ORDEM SERVICO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.SPED_FISCAL.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.SPED_FISCAL, "SPED FISCAL", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.EFD_CONTRIBUICOES.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.EFD_CONTRIBUICOES, "EFD CONTRIBUICOES", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.CONCILIACAO_BANCARIA.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.CONCILIACAO_BANCARIA, "CONCILIACAO BANCARIA", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.FECHAMENTO_FINANCEIRO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.FECHAMENTO_FINANCEIRO, "FECHAMENTO FINANCEIRO", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.REQUISICAO_MATERIAL.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.REQUISICAO_MATERIAL, "REQUISICAO MATERIAL", aviso.getIdOrigem().toString()));
			}else if (OrigemavisoBean.SOLICITACAO_SERVICO.equals(aviso.getAvisoOrigem().ordinal())) {
				listaBean.add(new OrigemavisoBean(OrigemavisoBean.SOLICITACAO_SERVICO, "SOLICITACAO SERVICO", aviso.getIdOrigem().toString()));
			}
		}else {
			listaBean.add(new OrigemavisoBean(OrigemavisoBean.CADASTRO, "CADASTRO", ""));
		}
		return listaBean;
	}
	
	private void enviaEmailAviso() {
		Avisousuario avisousuario = null;
		EmailManager email = null;
		Envioemail envioEmail = null;
		TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioEmailAviso.tpl");
		String template = null;
		Solicitacaoservico solicitacaoServico = null;
		
		for (int i = 0 ; i < 5 ; i++) {
			avisousuario = avisousuarioService.findAvisoNaoLidasEmail();
			

			
			if (avisousuario != null && avisousuario.getAviso() != null && avisousuario.getAviso().getMotivoaviso() != null && 
					avisousuario.getAviso().getMotivoaviso().getMotivo() != null && avisousuario.getUsuario() != null && 
					(avisousuario.getUsuario().getBloqueado() == null || !avisousuario.getUsuario().getBloqueado())) {
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(avisousuario.getAviso().getMotivoaviso().getMotivo());
				
				
				
				if (motivoAviso != null && Boolean.TRUE.equals(motivoAviso.getEmail())) {
					if (motivoAviso.equals(MotivoavisoEnum.SOLICITACAO_SERVICO)) {
						avisousuarioService.updatePreparadoEnvioEmail(avisousuario, Boolean.TRUE);
						
						try {					
							solicitacaoServico = solicitacaoservicoService.load(new Solicitacaoservico(avisousuario.getAviso().getIdOrigem()));
							
							if (avisousuario.getAviso().getAssunto().contains("Cancelar")) {
								solicitacaoservicoService.enviaEmailCancelar(solicitacaoServico, "");
							} else if (avisousuario.getAviso().getAssunto().contains("Concluir")) {
								solicitacaoservicoService.enviaEmailConcluir(solicitacaoServico);
							} else if (avisousuario.getAviso().getAssunto().contains("Criar")) {
								solicitacaoservicoService.enviaEmailCriar(solicitacaoServico);
							}
							
							avisousuarioService.updateAvisoComoEnviadoEmail(avisousuario);
						} catch (Exception e) {
							avisousuarioService.updatePreparadoEnvioEmail(avisousuario, Boolean.FALSE);
						}
					} else {
						if (avisousuario != null && avisousuario.getUsuario() != null && avisousuario.getUsuario().getEmail() != null) {
							try {
								avisousuarioService.updatePreparadoEnvioEmail(avisousuario, Boolean.TRUE);
								
								try {
									template = templateAux
											.assign("assunto", avisousuario.getAviso().getAssunto())
											.assign("dtaviso", SinedDateUtils.toString(avisousuario.getAviso().getDtaviso()))
											.assign("complemento", avisousuario.getAviso().getComplemento())
											.assign("urlWithoutContext", SinedUtil.getUrlWithoutContext())
											.assign("cdavisousuario", avisousuario.getCdavisousuario().toString())
											.assign("cdusuario", avisousuario.getUsuario().getCdpessoa().toString())
											.assign("empresa", avisousuario.getAviso().getEmpresa() != null ? avisousuario.getAviso().getEmpresa().getRazaosocial() : empresaService.loadPrincipal().getRazaosocial())
											.getTemplate();
								} catch (IOException e) {
									e.printStackTrace();
								}
								
								email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
								
								envioEmail = envioEmailService.registrarEnvio(emailRemetente, Envioemailtipo.ENVIO_AVISO.getNome(), template, Envioemailtipo.ENVIO_AVISO_DIARIO);
								
								try {			
									email
										.setFrom(emailRemetente)
										.setSubject(Envioemailtipo.ENVIO_AVISO_DIARIO.getNome())
										.setTo(avisousuario.getUsuario().getEmail())
										.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioEmail, avisousuario.getUsuario().getEmail()));
									
									if (SinedUtil.isAmbienteDesenvolvimento()) {
										Thread.sleep(1000);
									}
									if(!Boolean.TRUE.equals(avisousuario.getUsuario().getBloqueado())){
										new EmailManagerThreadAvisos(email, avisousuario.getCdavisousuario(), avisousuario.getUsuario().getCdpessoa(), SinedUtil.getStringConexaoBanco()).start();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} catch (Exception e) {
								avisousuarioService.updatePreparadoEnvioEmail(avisousuario, Boolean.FALSE);
								e.printStackTrace();
							}
						}
					}
				} else {
					avisousuarioService.updatePreparadoEnvioEmail(avisousuario, Boolean.TRUE);
				}
			} 
		}
	}
	
	public ModelAndView ajaxAvisoUsuario(WebRequestContext request) {
		List<Avisousuario> listaAvisousuarioNaoNotificado = new ArrayList<Avisousuario>();
		List<Avisousuario> listaAvisousuarioUltimos = avisousuarioService.findAvisousuario(SinedUtil.getUsuarioLogado(), new SinedUtil().getListaEmpresa(), false, 10, null, null);
		if(parametrogeralService.getBoolean(Parametrogeral.NOTIFICACAO_USUARIO_AVISO)){
			listaAvisousuarioNaoNotificado = avisousuarioService.findAvisousuario(SinedUtil.getUsuarioLogado(), new SinedUtil().getListaEmpresa(), true, 20, new Date(System.currentTimeMillis()), Boolean.FALSE);
			listaAvisousuarioUltimos = avisousuarioService.findAvisousuario(SinedUtil.getUsuarioLogado(), new SinedUtil().getListaEmpresa(), false, 10, null, null);
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.NOTIFICACAO_EMAIL_USUARIO_AVISO)){
			enviaEmailAviso();
		}
		
		Integer qtdeNaoLidos = 0;
		
		if(parametrogeralService.getBoolean(Parametrogeral.NOTIFICACAO_USUARIO_AVISO)){
			qtdeNaoLidos = avisousuarioService.getQtdeNaoLidos(SinedUtil.getUsuarioLogado(), new SinedUtil().getListaEmpresa());
		}
		
		Integer permanencianotificacao = 5000;
		try {
			permanencianotificacao = Integer.parseInt(parametrogeralService.getValorPorNome(Parametrogeral.PERMANENCIA_NOTIFICACAO)) * 1000;
		} catch (Exception e) {}
		
		return new JsonModelAndView()
				.addObject("qtdeAvisoNaoLido", qtdeNaoLidos)
				.addObject("listaAvisousuarioNaoNotificado", listaAvisousuarioNaoNotificado)
				.addObject("listaAvisousuario", listaAvisousuarioUltimos)
				.addObject("permanencianotificacao", permanencianotificacao);
	}
	
	public void ajaxAvisoUsuarioMarcarNotificado(WebRequestContext request) {
		try {
			avisousuarioService.updateNotificado(new Avisousuario(Integer.parseInt(request.getParameter("cdavisousuario"))));
		} catch (Exception e) {} 
	}
	
	public void ajaxAvisoUsuarioMarcarLido(WebRequestContext request) {
		try {
			avisousuarioService.updateLido(new Avisousuario(Integer.parseInt(request.getParameter("cdavisousuario"))));
		} catch (Exception e) {} 
	}
	
	public void ajaxAvisoUsuarioMarcarLidoTodos(WebRequestContext request) {
		try {
			avisousuarioService.updateLidoTodos(SinedUtil.getUsuarioLogado(), null);
		} catch (Exception e) {} 
	}
	
	public List<Usuario> salvarAvisos(List<Aviso> listaAvisos, boolean noUseTransaction) {
		List<Papel> listaPapeis = new ArrayList<Papel>();
		List<Usuario> listaUsuarioAviso = new ArrayList<Usuario>();
		List<Usuario> listaUsuario = null;
		
		
		for (Aviso aviso : listaAvisos){ 
			if(aviso.getListaEnvolvidos() !=null && !aviso.getListaEnvolvidos().isEmpty()){
				listaUsuarioAviso.addAll(aviso.getListaEnvolvidos());
			}else if(aviso.getPapel() != null){
				listaPapeis.add(aviso.getPapel());
			}else if(aviso.getUsuario() != null){
				listaUsuarioAviso.add(aviso.getUsuario());
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaAvisos) && (SinedUtil.isListNotEmpty(listaPapeis) || SinedUtil.isListNotEmpty(listaUsuarioAviso))){
			HashMap<Integer, List<Usuario>> mapIdUsuarioIncluido = new HashMap<Integer, List<Usuario>>();
			List<Usuario> listaUsuarioIncluido;
			List<Usuariopapel> listaUsuariopapel = usuariopapelService.findByPapeisTodos(listaPapeis);
			
			for (Aviso aviso : listaAvisos){ 
				listaUsuarioIncluido = mapIdUsuarioIncluido.get(aviso.getIdOrigem()); 
				if(listaUsuarioIncluido == null) listaUsuarioIncluido = new ArrayList<Usuario>();

				String whereInEmpresa = aviso.getEmpresa() != null && aviso.getEmpresa().getCdpessoa() != null ? aviso.getEmpresa().getCdpessoa().toString() : null;
				if(aviso.getListaEnvolvidos() !=null && !aviso.getListaEnvolvidos().isEmpty()){
					listaUsuario = aviso.getListaEnvolvidos();
				}
				else{
					listaUsuario = getUsuariosForAviso(listaUsuariopapel, aviso.getUsuario(), aviso.getPapel(), listaUsuarioIncluido, aviso.getWhereInProjeto(), whereInEmpresa);
				}
				if(SinedUtil.isListNotEmpty(listaUsuario)){
					if(noUseTransaction){
						saveOrUpdateNoUseTransaction(aviso);
					}else {
						saveOrUpdate(aviso);
					}
					for(Usuario usuario : listaUsuario){
						if(noUseTransaction){
							avisousuarioService.saveOrUpdateNoUseTransaction(new Avisousuario(aviso, usuario, false));
						}else {
							avisousuarioService.saveOrUpdate(new Avisousuario(aviso, usuario, false));
						}
					}
					listaUsuarioIncluido.addAll(listaUsuario);
					mapIdUsuarioIncluido.put(aviso.getIdOrigem(), listaUsuarioIncluido);
				}
			}
		}
		
		return listaUsuario;
	}
	
	public void salvarAvisos(Aviso aviso, List<Usuario> listaUsuario, boolean noUseTransaction) {
		if(SinedUtil.isListNotEmpty(listaUsuario)){
			if(noUseTransaction){
				saveOrUpdateNoUseTransaction(aviso);
			}else {
				saveOrUpdate(aviso);
			}
			for(Usuario usuario : listaUsuario){
				if(noUseTransaction){
					avisousuarioService.saveOrUpdateNoUseTransaction(new Avisousuario(aviso, usuario, false));
				}else {
					avisousuarioService.saveOrUpdate(new Avisousuario(aviso, usuario, false));
				}
			}
		}
	}
	
	public void salvarAvisos(Aviso aviso, boolean noUseTransaction) {
		if(aviso != null && aviso.getMotivoaviso() != null){
			List<Aviso> avisoList = new ArrayList<Aviso>();
			avisoList.add(aviso);
			salvarAvisos(avisoList, noUseTransaction);
		}
	}
	
	public void salvarAvisos(Aviso aviso, boolean noUseTransaction, List<Avisousuario> avisoUsuarioList) {
		if (SinedUtil.isListNotEmpty(avisoUsuarioList)){
			if (noUseTransaction){
				saveOrUpdateNoUseTransaction(aviso);
			} else {
				saveOrUpdate(aviso);
			}
			
			for(Avisousuario au : avisoUsuarioList){
				if (noUseTransaction){
					avisousuarioService.saveOrUpdateNoUseTransaction(new Avisousuario(aviso, au.getUsuario(), false));
				} else {
					avisousuarioService.saveOrUpdate(new Avisousuario(aviso, au.getUsuario(), false));
				}
			}
		}
	}

	public List<Usuario> getListaUsuarioForAviso(Aviso aviso, boolean noUseTransaction) {
		List<Papel> listaPapeis = new ArrayList<Papel>();
		List<Usuario> listaUsuarioAviso = new ArrayList<Usuario>();
		List<Usuario> listaUsuario = null;
		
		if (aviso != null){ 
			if(aviso.getPapel() != null){
				listaPapeis.add(aviso.getPapel());
			}else if(aviso.getUsuario() != null){
				listaUsuarioAviso.add(aviso.getUsuario());
			}
		}
		
		if(aviso != null && (SinedUtil.isListNotEmpty(listaPapeis) || SinedUtil.isListNotEmpty(listaUsuarioAviso))){
			HashMap<Integer, List<Usuario>> mapIdUsuarioIncluido = new HashMap<Integer, List<Usuario>>();
			List<Usuario> listaUsuarioIncluido;
			List<Usuariopapel> listaUsuariopapel = usuariopapelService.findByPapeisTodos(listaPapeis);
			
			listaUsuarioIncluido = mapIdUsuarioIncluido.get(aviso.getIdOrigem()); 
			if(listaUsuarioIncluido == null) listaUsuarioIncluido = new ArrayList<Usuario>();

			String whereInEmpresa = aviso.getEmpresa() != null && aviso.getEmpresa().getCdpessoa() != null ? aviso.getEmpresa().getCdpessoa().toString() : null;
			listaUsuario = getUsuariosForAviso(listaUsuariopapel, aviso.getUsuario(), aviso.getPapel(), listaUsuarioIncluido, aviso.getWhereInProjeto(), whereInEmpresa);
		}
		
		return listaUsuario;
	}
	
	public List<Usuario> getUsuariosForAviso(List<Usuariopapel> listaUsuariopapel, Usuario usuario, Papel papel, List<Usuario> listaUsuario, String whereInProjeto, String whereInEmpresa) {
		List<Usuario> lista = new ArrayList<Usuario>();
		if(listaUsuario == null) listaUsuario = new ArrayList<Usuario>();
		if(SinedUtil.isListNotEmpty(listaUsuariopapel) && papel != null){
			for(Usuariopapel usuariopapel : listaUsuariopapel){
				if(usuariopapel.getPessoa() != null && permissaoProjeto(usuariopapel.getPessoa(), whereInProjeto) && permissaoEmpresa(usuariopapel.getPessoa(), whereInEmpresa)){
					if((papel == null || (usuariopapel.getPapel() != null && usuariopapel.getPapel().getCdpapel() != null && usuariopapel.getPapel().getCdpapel().equals(papel.getCdpapel()))) && (listaUsuario == null || !listaUsuario.contains(usuariopapel.getPessoa())) && 
							!lista.contains(usuariopapel.getPessoa())){
						lista.add(usuariopapel.getPessoa());
					}
				}
			}
		}else if(usuario != null && permissaoProjeto(usuario, whereInProjeto) && !listaUsuario.contains(usuario)){ 
			lista.add(usuario);
		}
		return lista;
	}
	
	private boolean permissaoProjeto(Usuario usuario, String whereInProjeto) {
		if(StringUtils.isBlank(whereInProjeto)) return true;
		if(usuario == null) throw new SinedException("Usu�rio n�o pode ser nulo.");
		if(usuario.getTodosprojetos() != null && usuario.getTodosprojetos()) return true;
		
		if(SinedUtil.isListNotEmpty(usuario.getListaUsuarioprojeto())){
			String[] projetos = whereInProjeto.split(",");
			for(Usuarioprojeto usuarioprojeto : usuario.getListaUsuarioprojeto()){
				if(usuarioprojeto.getProjeto() != null && usuarioprojeto.getProjeto().getCdprojeto() != null){
					for(String id : projetos){
						if(usuarioprojeto.getProjeto().getCdprojeto().toString().equals(id)){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	private boolean permissaoEmpresa(Usuario usuario, String whereInEmpresa) {
		if(StringUtils.isBlank(whereInEmpresa)) return true;
		if(usuario == null) throw new SinedException("Usu�rio n�o pode ser nulo.");
		
		if(SinedUtil.isListNotEmpty(usuario.getListaUsuarioempresa())){
			String[] empresas = whereInEmpresa.split(",");
			for(Usuarioempresa usuarioempresa : usuario.getListaUsuarioempresa()){
				if(usuarioempresa.getEmpresa() != null && usuarioempresa.getEmpresa().getCdpessoa() != null){
					for(String id : empresas){
						if(usuarioempresa.getEmpresa().getCdpessoa().toString().equals(id)){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public void incluirESalvarAvisoUsuario(Aviso aviso) {
		if(aviso != null && aviso.getCdaviso() != null && aviso.getTipoaviso() != null){
			List<Usuario> listaUsuario = new ArrayList<Usuario>();
			
			if(Tipoaviso.GERAL.getCdtipoaviso().equals(aviso.getTipoaviso().getCdtipoaviso()) || 
					(Tipoaviso.NIVEL.getCdtipoaviso().equals(aviso.getTipoaviso().getCdtipoaviso()) && aviso.getPapel() == null)){
				listaUsuario = usuarioService.findUsuariosDesbloqueados(null);
			}else if((Tipoaviso.NIVEL.getCdtipoaviso().equals(aviso.getTipoaviso().getCdtipoaviso()) && aviso.getPapel() != null) || Tipoaviso.USUARIO.getCdtipoaviso().equals(aviso.getTipoaviso().getCdtipoaviso())){
				List<Papel> listaPapeis = new ArrayList<Papel>();
				if(aviso.getPapel() != null){
					listaPapeis.add(aviso.getPapel());
				}
			
				List<Usuariopapel> listaUsuariopapel = listaPapeis.size() > 0 ? usuariopapelService.findByPapeisTodos(listaPapeis) : new ArrayList<Usuariopapel>();
				listaUsuario = getUsuariosForAviso(listaUsuariopapel, aviso.getUsuario(), aviso.getPapel(), null, null, null);
			}
			
			if(SinedUtil.isListNotEmpty(listaUsuario)){
				for(Usuario usuario : listaUsuario){
					avisousuarioService.saveOrUpdate(new Avisousuario(aviso, usuario, false));
				}
			}
		}
	}
	
	public Date findLastAvisoDateByMotivoIdOrigem(Motivoaviso motivoaviso, Integer cdagendamento) {
		List<Aviso> avisoList = avisoDAO.findLastAvisoDateByMotivoIdOrigem(motivoaviso, cdagendamento);
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			return avisoList.get(0).getDtaviso();
		} else {
			return null;
		}
	}
	
	public List<Usuario> getListaCorrigidaUsuarioSalvarAviso(Aviso aviso, List<Avisousuario> avisoUsuarioList) {
		List<Usuario> listaUsuario = this.getListaUsuarioForAviso(aviso, true);
		for (Avisousuario avisoUsuario : avisoUsuarioList) {
			for (Usuario usuario : listaUsuario) {
				if (usuario.getCdpessoa().equals(avisoUsuario.getUsuario().getCdpessoa())) {
					listaUsuario.remove(usuario);
					break;
				}
			}
		}
		
		return listaUsuario;
	}
	
	public void criarAvisosControladosPorDia() throws ParseException {
		Empresa empresa = empresaService.loadPrincipal();
		Municipio municipio = null;
		Uf uf = null;
		Calendario calendario = null;
		Date data = null;
		List<Date> dataList = new ArrayList<Date>();
		Boolean gerarAviso = Boolean.TRUE;
		
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		if (municipio != null && uf != null) {
			calendario = calendarioService.getCalendarioUnificado(uf, municipio);
		}
		
		data = SinedDateUtils.currentDate();
		
		if (calendario != null) {
			if (SinedDateUtils.diaValidoCalendar(data, calendario)) {
				dataList.add(data);
				gerarAviso = Boolean.TRUE;
			} else {
				gerarAviso = Boolean.FALSE;
			}
		} else if (!SinedDateUtils.isFinalSemana(data)) {
			dataList.add(data);
			gerarAviso = Boolean.TRUE;
		} else {
			gerarAviso = Boolean.FALSE;
		}
		
		if (gerarAviso) {
			while (gerarAviso) {
				data = SinedDateUtils.addDiasData(data, 1);
				
				if (calendario != null) {
					if (SinedDateUtils.diaValidoCalendar(data, calendario)) {
						gerarAviso = Boolean.FALSE;
					} else {
						dataList.add(data);
						gerarAviso = Boolean.TRUE;
					}
				} else if (!SinedDateUtils.isFinalSemana(data)) {
					gerarAviso = Boolean.FALSE;
				} else {
					dataList.add(data);
					gerarAviso = Boolean.TRUE;
				}
			} 
		}
		
		Integer meses;
		try {
			meses = parametrogeralService.getInteger("QUANTIDADE_MESES_AVISOS_DIARIO") * -1;
		} catch (Exception e) {
			meses = -3;
			e.printStackTrace();
		}
		
		Date dateToSearch = SinedDateUtils.addMesData(SinedDateUtils.currentDate(), meses);
		Date dateToSearchIni = SinedDateUtils.stringToDate("01/06/2018");
		if(SinedDateUtils.beforeIgnoreHour(dateToSearch, dateToSearchIni)){
			dateToSearch = dateToSearchIni;
		}
		
		for (Date d : dataList) {
			List<Motivoaviso> motivoAvisoList = motivoavisoService.findAllForCreateAviso();
		
			for (Motivoaviso m : motivoAvisoList) {
				if (Boolean.TRUE.equals(m.getAtivo())) {
					try{
						if (m.getMotivo().equals(MotivoavisoEnum.AGENDAMENTO_PROX_VENCIMENTO)) {
							agendamentoService.criarAvisoAgendamentoProximo(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.AGENDAMENTO_ATRASADO)) {
							agendamentoService.criarAvisoAgendamentoAtrasado(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.CONTA_PAGAR_PROX_VENCIMENTO)) {
							contaPagarService.criarAvisoContaPagarProximo(m, d);
						}
						else if (m.getMotivo().equals(MotivoavisoEnum.CONTA_PAGAR_VENCIDA)) {
							contaPagarService.criarAvisoContaPagarAtrasado(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.CONTA_RECEBER_VENCIDA)) {
							contaReceberService.criarAvisoContaReceberAtrasado(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.VENDA_SEM_VINCULO_CONTA_RECEBER)) {
							vendaService.criarAvisoVendaSemContaReceber(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.NF_SEM_VINCULO_CONTA_RECEBER)) {
							notafiscalprodutoService.criarAvisoNotaSemContaReceberNaoLiquidada(m, d, dateToSearch);
							notaFiscalServicoService.criarAvisoNotaSemContaReceberNaoLiquidada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.SOLICITACAO_COMPRA_ATRASO_APROVACAO)) {
							solicitacaocompraService.criarAvisoSolicitacaoCompraAtrasoAprovacao(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.SOLICITACAO_COMPRA_ATRASO_ENTREGA)) {
							solicitacaocompraService.criarAvisoSolicitacaoCompraAtrasoEntrega(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.COTACAO_ATRASO)) {
							cotacaoService.criarAvisoCotacaoAtrasada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.ORDEM_COMPRA_ATRASO)) {
							ordemcompraService.criarAvisoOrdemCompraAtrasada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.ENVIAR_PEDIDO_COMPRA)) {
							ordemcompraService.criarAvisoEnvioOrdemCompra(m, d, dateToSearch);
						}else if (m.getMotivo().equals(MotivoavisoEnum.RECEBIMENTO_SEM_REGISTRO_FINANCEIRO)) {
							entregaService.criarAvisoEntregaSemRegistroFinanceiro(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.COLETA_DEVOLUCAO_NAO_CONFIRMADA)) {
							coletaService.criarAvisoColetaDevolucaoNaoConfirmada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.DATA_LOCACAO_MATERIAL_VENCIDA)) {
							locacaomaterialService.criarAvisoDataLocacaoMaterialVencida(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.CONTRATO_FORNECIMENTO_EXPIRADO)) {
							fornecimentoService.criarAvisoFornecimentoVencido(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.ENTRADA_FISCAL_SEM_VINCULO_REGISTRO_FINANCEIRO)) {
							entradafiscalService.criarAvisoEntradaFiscalSemVinculoFinanceiro(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.ATRASO_AGENDA_INTERACAO)) {
							agendainteracaoService.criarAvisoAgendaInteracaoAtrasada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.ORDEM_SERVICO_ATRASADA)) {
							requisicaoService.criarAvisoRequisicaoAtrasada(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.SPED_FISCAL_NAO_EMITIDO)) {
							spedArquivoService.criarAvisoSped(m, d);
						} else if (m.getMotivo().equals(MotivoavisoEnum.SPED_CONTRIBUICOES_NAO_EMITIDO)) {
							spedPisCofinsService.criarAvisoSped(m, d);
						} else if (m.getMotivo().equals(MotivoavisoEnum.CONCILIACAO_FINANCEIRA_NAO_REALIZADA)) {
								movimentacaoService.criarAvisoMovimentacao(m, d, dateToSearch);
						} else if (m.getMotivo().equals(MotivoavisoEnum.FECHAMENTO_FINANCEIRO_NAO_REALIZADO)) {
							if (SinedDateUtils.getDia(d) >= 5) {
								fechamentoFinanceiroService.criarAvisoFechamentoFinanceiro(m, d);
							}
						}
					}catch (Exception e){
						System.out.print("Erro ao gerar aviso \n" + e.getMessage());
					}
				}
			}
		}
	}
}