package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.dao.NotaDocumentoDAO;
import br.com.linkcom.sined.util.CriptografiaUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaDocumentoService extends GenericService<NotaDocumento> {
	
	private final static String PARAMETRO_SUPERLEILOES_INTEGRACAO = "SUPERLEILOES_INTEGRACAO";
	private final static String PARAMETRO_SUPERLEILOES_URL_INTEGRACAO = "SUPERLEILOES_URL_INTEGRACAO"; 
	
	private final static String PARAMETRO_W3NFE_INTEGRACAO = "W3NFE_INTEGRACAO";
	private final static String PARAMETRO_W3NFE_URL_INTEGRACAO = "W3NFE_URL_INTEGRACAO"; 
	
	private final static String PARAMETRO_CADEB_INTEGRACAO = "CADEB_INTEGRACAO";
	private final static String PARAMETRO_CADEB_URL_INTEGRACAO = "CADEB_URL_INTEGRACAO"; 

	private NotaDocumentoDAO notaDocumentoDAO;
	private NotaService notaService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private DocumentoorigemService documentoorigemService;
	private DocumentohistoricoService documentohistoricoService;
	private DocumentoService documentoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private GrupotributacaoService grupotributacaoService;
	private NotaVendaService notaVendaService;
	private VendaService vendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setNotaDocumentoDAO(NotaDocumentoDAO notaDocumentoDAO) {
		this.notaDocumentoDAO = notaDocumentoDAO;
	}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {
		this.grupotributacaoService = grupotributacaoService;
	}
	public void setNotaVendaService(NotaVendaService notaVendaService) {
		this.notaVendaService = notaVendaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	
	public void liberaNotaDocumentoWebservice(WebRequestContext request, List<Documento> lista, Date dtemissao) {
		String whereIn = CollectionsUtil.listAndConcatenate(lista, "cddocumento", ",");
		List<NotaDocumento> listaNotaDocumento = this.getListaNotaDocumentoWebserviceEmEspera(whereIn);	
		
		Integer proximoNumero;
		for (NotaDocumento notaDocumento : listaNotaDocumento) {
			Nota nota = notaDocumento.getNota();
			Empresa empresa = nota.getEmpresa();
			
			nota.setNotaStatus(NotaStatus.EMITIDA);
			notaService.alterarStatusAcao(nota, "Pagamento efetuado.", null);
			
			boolean servico = nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO);
			
			if(servico && request != null && nota != null && nota.getCdNota() != null &&
						notaFiscalServicoService.existeDataEmissaoAnteriorDataAtual(nota.getCdNota().toString())){
				request.addMessage("Aten��o! Os impostos acumulativos com outras notas devem ser revisados antes de gerar a nota.", MessageType.WARN);
			}
			
			notaService.atualizaDataEmissao(nota, dtemissao);
			
			if(empresa != null && empresa.getCdpessoa() != null){
				if(!servico){
					proximoNumero = empresaService.carregaProxNumNFProduto(empresa);
					if(proximoNumero == null){
						proximoNumero = 1;
					}
					notaService.updateNumeroNota(nota, proximoNumero);
					proximoNumero++;

					empresaService.updateProximoNumNFProduto(empresa, proximoNumero);
					
					notafiscalprodutoService.updateHoraEmissao(nota, new Hora(new Date(System.currentTimeMillis()).getTime()));
				} else {
					proximoNumero = empresaService.carregaProxNumNF(empresa);
					if(proximoNumero == null){
						proximoNumero = 1;
					}
					notaService.updateNumeroNota(nota, proximoNumero);
					proximoNumero++;
	
					empresaService.updateProximoNumNF(empresa, proximoNumero);
				}
			}
			
			try {
				//Setido inverso do par�metro pois a nota precisa ser atualizada antes do envio para a prefeitura.
				if(servico && !"TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECALCULAR_IMPOSTO_CRIARNFSE))){
					atualizaValoresBasecalculoNotaFiscalServico(nota);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	* M�todo para atualizar os valores de base de c�lculo da nota ap�s 
	*
	* @param bean
	* @since 19/08/2014
	* @author Luiz Fernando
	*/
	public void atualizaValoresBasecalculoNotaFiscalServico(Nota bean){
		if(bean != null && bean.getCdNota() != null){
			NotaFiscalServico nota = notaFiscalServicoService.loadForEntrada(new NotaFiscalServico(bean.getCdNota()));
			if(nota != null){
				if(nota.getGrupotributacao() != null){
					nota.setGrupotributacao(grupotributacaoService.loadForEntrada(nota.getGrupotributacao()));
				}
				Grupotributacao grupotributacao = nota.getGrupotributacao();
				if(grupotributacao != null){
					Money descontoincondicionado = nota.getDescontoincondicionado()!=null ? nota.getDescontoincondicionado() : new Money();
					Money basecalculo = nota.getValorTotalServicos().subtract(descontoincondicionado);
					notaFiscalServicoService.calculaTributacaoNota(nota, grupotributacao, basecalculo);
				}				
				Money retencao = new Money();
				
				if(nota.getIncideiss() != null && 
						nota.getIncideiss() && 
						nota.getIss() != null && 
						nota.getIss() > 0d){
					retencao = retencao.add(nota.getValorIss());
				}
				
				if(nota.getIncideir() != null && 
						nota.getIncideir() && 
						nota.getIr() != null && 
						nota.getIr() > 0d){
					retencao = retencao.add(nota.getValorIr());
				}
				
				if(nota.getIncidepis() != null && 
						nota.getIncidepis() && 
						nota.getPis() != null && 
						nota.getPis() > 0d){
					retencao = retencao.add(nota.getValorPis());
				}
				
				if(nota.getIncidecofins() != null && 
						nota.getIncidecofins() && 
						nota.getCofins() != null && 
						nota.getCofins() > 0d){
					retencao = retencao.add(nota.getValorCofins());
				}
				
				if(nota.getIncidecsll() != null && 
						nota.getIncidecsll() && 
						nota.getCsll() != null && 
						nota.getCsll() > 0d){
					retencao = retencao.add(nota.getValorCsll());
				}
				
				if(nota.getIncideinss() != null && 
						nota.getIncideinss() && 
						nota.getInss() != null && 
						nota.getInss() > 0d){
					retencao = retencao.add(nota.getValorInss());
				}
				
				if(nota.getIncideicms() != null && 
						nota.getIncideicms() && 
						nota.getIcms() != null && 
						nota.getIcms() > 0d){
					retencao = retencao.add(nota.getValorIcms());
				}
				
				notaFiscalServicoService.saveOrUpdate(nota);
			}
		}
	}
	
	public void estornaNotaDocumentoWebservice(Documento documento) {
		this.enviaRequisicaoEstornoDocumentoSuperLeiloes(documento);
		this.enviaRequisicaoEstornoDocumentoW3nfe(documento);
		this.enviaRequisicaoEstornoDocumentoCadeb(documento);
	}
	
	private void enviaRequisicaoEstornoDocumentoSuperLeiloes(Documento documento) {
		try{
			String param = parametrogeralService.getValorPorNome(PARAMETRO_SUPERLEILOES_INTEGRACAO);
			
			if("TRUE".equals(param.toUpperCase())){
				
				Integer cddocumento = documento.getCddocumento();
				String assinatura = CriptografiaUtil.createSignature(cddocumento.toString().getBytes());
				
				// Adicionando os par�metros
				String data = URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode("estornoDocumento", "LATIN1");
				data += "&" + URLEncoder.encode("cddocumento", "LATIN1") + "=" + URLEncoder.encode(cddocumento.toString(), "LATIN1"); 
				data += "&" + URLEncoder.encode("hash", "LATIN1") + "=" + assinatura; 
				
				String urlParametro = parametrogeralService.getValorPorNome(PARAMETRO_SUPERLEILOES_URL_INTEGRACAO);
				
				// Enviando requisi��o
				URL url = new URL("http://" + urlParametro + "/pub/EnvioInformacao");  
				URLConnection conn = url.openConnection(); 
				conn.setDoOutput(true); 
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
				wr.write(data); 
				wr.flush(); 
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
				String line; 
				String xml = ""; 
				
				while ((line = rd.readLine()) != null) { xml += line; } 
				
				wr.close(); 
				rd.close(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void enviaRequisicaoEstornoDocumentoW3nfe(Documento documento) {
		try{
			String param = parametrogeralService.getValorPorNome(PARAMETRO_W3NFE_INTEGRACAO);
			
			if("TRUE".equals(param.toUpperCase())){
				
				Integer cddocumento = documento.getCddocumento();
				String assinatura = CriptografiaUtil.createSignature(cddocumento.toString().getBytes());
				
				// Adicionando os par�metros
				String data = URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode("estornoDocumento", "LATIN1");
				data += "&" + URLEncoder.encode("cddocumento", "LATIN1") + "=" + URLEncoder.encode(cddocumento.toString(), "LATIN1"); 
				data += "&" + URLEncoder.encode("hash", "LATIN1") + "=" + assinatura; 
				
				String urlParametro = parametrogeralService.getValorPorNome(PARAMETRO_W3NFE_URL_INTEGRACAO);
				
				// Enviando requisi��o
				URL url = new URL("http://" + urlParametro + "/w3nfe/pub/EnvioInformacao");  
				URLConnection conn = url.openConnection(); 
				conn.setDoOutput(true); 
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
				wr.write(data); 
				wr.flush(); 
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
				String line; 
				String xml = ""; 
				
				while ((line = rd.readLine()) != null) { xml += line; } 
				
				wr.close(); 
				rd.close(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void enviaRequisicaoEstornoDocumentoCadeb(Documento documento) {
		try{
			String param = parametrogeralService.getValorPorNome(PARAMETRO_CADEB_INTEGRACAO);
			
			if("TRUE".equals(param.toUpperCase())){
				
				Integer cddocumento = documento.getCddocumento();
				String assinatura = CriptografiaUtil.createSignature(cddocumento.toString().getBytes());
				
				// Adicionando os par�metros
				String data = URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode("estornoDocumento", "LATIN1");
				data += "&" + URLEncoder.encode("cddocumento", "LATIN1") + "=" + URLEncoder.encode(cddocumento.toString(), "LATIN1"); 
				data += "&" + URLEncoder.encode("hash", "LATIN1") + "=" + assinatura; 
				
				String urlParametro = parametrogeralService.getValorPorNome(PARAMETRO_CADEB_URL_INTEGRACAO);
				
				// Enviando requisi��o
				URL url = new URL("http://" + urlParametro + "/cadeb/sistema/livre/EnvioInformacao");  
				URLConnection conn = url.openConnection(); 
				conn.setDoOutput(true); 
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
				wr.write(data); 
				wr.flush(); 
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
				String line; 
				String xml = ""; 
				
				while ((line = rd.readLine()) != null) { xml += line; } 
				
				wr.close(); 
				rd.close(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<NotaDocumento> getListaNotaDocumentoWebservice(String whereIn){
		return notaDocumentoDAO.getListaNotaDocumentoWebservice(whereIn);
	}
	
	public List<NotaDocumento> getListaNotaDocumentoWebserviceEmEspera(String whereIn){
		return notaDocumentoDAO.getListaNotaDocumentoWebserviceEmEspera(whereIn);
	}
	
	public boolean isNotaWebservice(Nota nota) {
		return notaDocumentoDAO.isNotaWebservice(nota);
	}
	
	public boolean isDocumentoWebservice(Documento documento) {
		return notaDocumentoDAO.isDocumentoWebservice(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<NotaDocumento> findByDocumento(Documento documento) {
		return notaDocumentoDAO.findByDocumento(documento);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/09/2015
	 */
	public List<NotaDocumento> findByDocumento(String whereIn) {
		return notaDocumentoDAO.findByDocumento(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @since 13/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaDocumento> findByDocumentoNotCancelada(Documento documento) {
		return notaDocumentoDAO.findByDocumentoNotCancelada(documento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael salvio
	 * @since 16/04/2012
	 */
	public boolean verificaContaNota(Integer cdconta) {
		return notaDocumentoDAO.verificaContaNota(cdconta, null);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param cdconta
	 * @param notaTipo
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public boolean verificaContaNota(Integer cdconta, NotaTipo notaTipo) {
		return notaDocumentoDAO.verificaContaNota(cdconta, notaTipo);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDocumentoDAO#haveDocumentoNaoCanceladoByNota(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/10/2012
	 */
	public boolean haveDocumentoNaoCanceladoByNota(Nota nota){
		return notaDocumentoDAO.haveDocumentoNaoCanceladoByNota(nota);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDocumentoDAO#findByNotasForValidacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public List<NotaDocumento> findByNotasForValidacao(String whereIn) {
		return notaDocumentoDAO.findByNotasForValidacao(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDocumentoDAO#findByDocumentoForValidacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public List<NotaDocumento> findByDocumentoForValidacao(String whereIn) {
		return notaDocumentoDAO.findByDocumentoForValidacao(whereIn);
	}
	
	/**
	 * Valida os v�nculos de nota e documento para o cancelamento de nota.
	 *
	 * @param request
	 * @param itensSelecionados
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public boolean validaCancelamentoNotaWebserviceFalse(WebRequestContext request, String itensSelecionados) {
		boolean erro = false;
		List<NotaDocumento> listaNotadocumento = this.findByNotasForValidacao(itensSelecionados);
		for (NotaDocumento notaDocumento : listaNotadocumento) {
			if(notaDocumento.getFromwebservice() == null || !notaDocumento.getFromwebservice()){
				Documento documento = notaDocumento.getDocumento();
				boolean origemVenda = documentoorigemService.isOrigemVenda(documento);
				
				if(!origemVenda){
					Nota nota = notaDocumento.getNota();
					if(documento != null && 
							documento.getDocumentoacao() != null && 
							!documento.getDocumentoacao().equals(Documentoacao.CANCELADA)){
						
						request.addError("Nota n� " + 
											SinedUtil.makeLinkNota(nota) + 
											": � necess�rio cancelar a conta a receber " + 
											SinedUtil.makeLinkContareceber(documento) + ".");
						erro = true;
					}
				}
			}
		}
		return erro;
	}
	
	/**
	 * Adiciona mensagens para avisar o usu�rio que est� dispon�vel o estorno das contas.
	 *
	 * @param request
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public void adicionaMensagensCancelamentoNotaWebserviceTrue(WebRequestContext request, String itensSelecionados, boolean avisoFuturo) {
		List<NotaDocumento> listaNotaDocumento = this.findByNotasForValidacao(itensSelecionados);
		if(listaNotaDocumento != null && listaNotaDocumento.size() > 0){
			Boolean origemvenda = false;
			Venda venda = null;
			String msgAvisofuturoDocumento = ": Conta a receber estar� dispon�vel para o estorno ";
			String msgAvisoDocumento = ": Conta a receber dispon�vel para o estorno ";
			String msgAvisoVenda = ": Venda dispon�vel para cancelamento ";
			String msgAvisofuturoVenda = ": Venda estar� dispon�vel para o cancelamento ";
			for (NotaDocumento notaDocumento : listaNotaDocumento) {
				Documento documento = notaDocumento.getDocumento();
				origemvenda = documentoorigemService.isOrigemVenda(documento);
				if(origemvenda != null && origemvenda){
					Documentoorigem documentoorigem = documentoorigemService.findDocumentoOrigemByDocumento(documento);
					if(documentoorigem != null && documentoorigem.getVenda() != null && documentoorigem.getVenda().getCdvenda() != null){
						venda = documentoorigem.getVenda();
					}
				}
				Nota nota = notaDocumento.getNota();
				if(documento != null && 
						notaDocumento.getFromwebservice() != null && 
						notaDocumento.getFromwebservice()){
					
					if(origemvenda != null && origemvenda){
						request.addMessage("Nota n� " +
								SinedUtil.makeLinkNota(nota) + 
								(avisoFuturo ? msgAvisofuturoVenda : msgAvisoVenda) + 
								SinedUtil.makeLinkVenda(venda) + ". Referente ao documento " + SinedUtil.makeLinkContareceber(documento));
					}else {
						request.addMessage("Nota n� " +
										SinedUtil.makeLinkNota(nota) + 
										(avisoFuturo ? msgAvisofuturoDocumento : msgAvisoDocumento) +
										SinedUtil.makeLinkContareceber(documento) + ".");
					}
				}
			}
		}
	}
	
	/**
	 * Valida os v�nculos de nota e documento para o estorno de conta a receber.
	 *
	 * @param errors
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public boolean validaEstornoContaWebserviceTrue(WebRequestContext request, Documento documento) {
		boolean erro = false;
		
		List<NotaDocumento> listaNotaDocumento = this.findByDocumentoForValidacao(documento.getCddocumento() + "");
		if(listaNotaDocumento != null && listaNotaDocumento.size() > 0){
			for (NotaDocumento notaDocumento : listaNotaDocumento) {
				if(notaDocumento.getFromwebservice() != null && notaDocumento.getFromwebservice()){
					Nota nota = notaDocumento.getNota();
					Documento doc = notaDocumento.getDocumento();
					
					if(nota != null && 
							nota.getNotaStatus() != null && 
							!nota.getNotaStatus().equals(NotaStatus.CANCELADA) && 
							!nota.getNotaStatus().equals(NotaStatus.EM_ESPERA)){
						String msgErro = "Conta a receber " + 
														SinedUtil.makeLinkContareceber(doc) +
														": A nota " +
														SinedUtil.makeLinkNota(nota) + " est� vinculada a esta conta a receber.";
						
						if(request != null)
							request.addError(msgErro);
						
						erro = true;
					}
				}
			}
		}
		
		return erro;
	}
	
	/**
	 * Adiciona mensagens para avisar o usu�rio que est� dispon�vel o cancelamento de notas.
	 *
	 * @param request
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public void adicionaMensagensCancelamentoContaWebserviceFalse(WebRequestContext request, String whereIn) {
		List<NotaDocumento> listaNotaDocumento = this.findByDocumentoForValidacao(whereIn);
		if(listaNotaDocumento != null && listaNotaDocumento.size() > 0){
			for (NotaDocumento notaDocumento : listaNotaDocumento) {
				Documento doc = notaDocumento.getDocumento();
				Nota nota = notaDocumento.getNota();
				
				if(doc != null && 
						(notaDocumento.getFromwebservice() == null || !notaDocumento.getFromwebservice())){
					
					request.addMessage("Conta a receber " +
										SinedUtil.makeLinkContareceber(doc) +
										(!haveDocumentoNaoCanceladoByNota(nota) ? 
										": Nota dispon�vel para o cancelamento " +
										SinedUtil.makeLinkNota(nota) : "") + ".");
				}
			}
		}
	}
	
	/**
	 * Cancela nota em espera a partir dos documentos.
	 *
	 * @param request
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 22/12/2016
	 */
	public void cancelaNotaEmespera(WebRequestContext request, String whereIn) {
		List<NotaDocumento> listaNotaDocumento = this.getListaNotaDocumentoWebserviceEmEspera(whereIn);
		if(listaNotaDocumento != null && listaNotaDocumento.size() > 0){
			for (NotaDocumento notaDocumento : listaNotaDocumento) {
				Nota nota = notaDocumento.getNota();
				Documento documento = notaDocumento.getDocumento();
				
				if(!documentoService.isOrigemNegociacao(documento)){
					String obs = "Cancelamento da conta a receber.";
					nota.setNotaStatus(NotaStatus.CANCELADA);
					notaService.alterarStatusAcao(nota, obs, null);
				}
			}
		}
	}
	
	public List<NotaDocumento> findForAlterarSituacaoVenda(String whereInNota) {
		return notaDocumentoDAO.findForAlterarSituacaoVenda(whereInNota);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaDocumentoDAO#findByNota(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 * @since 08/04/2014
	 */
	public List<NotaDocumento> findByNota(Nota nota) {
		return notaDocumentoDAO.findByNota(nota);
	}
	
	/**
	 * M�todo que atualiza o n�mero do documento
	 *
	 * @param bean
	 * @param numeroAnteriorNota
	 * @author Luiz Fernando
	 * @since 08/04/2014
	 */
	public void atualizarNumeroHistoricoDocumento(Nota bean, String numeroAnteriorNota) {
		if(bean != null && bean.getCdNota() != null && bean.getNumero() != null && !bean.getNumero().equals("")){
			List<NotaDocumento> listaNotaDocumento = findByNota(bean);
			if(listaNotaDocumento != null && !listaNotaDocumento.isEmpty()){
				for(NotaDocumento notaDocumento : listaNotaDocumento){
					if(notaDocumento.getDocumento() != null && notaDocumento.getDocumento().getCddocumento() != null && 
							(notaDocumento.getDocumento().getNumero() == null || !bean.getNumero().equals(notaDocumento.getDocumento().getNumero()))){
						documentoService.updateNumerosNFDocumento(notaDocumento.getDocumento().getCddocumento().toString(), bean.getNumero());
						
						Documentohistorico documentohistorico = new Documentohistorico();
						documentohistorico.setDocumento(notaDocumento.getDocumento());
						documentohistorico.setDocumentoacao(Documentoacao.ALTERADA);
						documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
						documentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						documentohistorico.setObservacao("N�mero da nota atualizado. Visualizar nota: <a href=\"javascript:visualizaNota("+bean.getCdNota()+");\">"+(bean.getNumero() != null ? bean.getNumero() : bean.getCdNota() )+"</a>.");
						documentohistoricoService.saveOrUpdate(documentohistorico);
					}
				}
			}
		}
	}
	
	public void verificarVendaAposCancelamentoOrigemNotaDocumento(Nota nota){
		if(nota != null && nota.getCdNota() != null){
			List<NotaDocumento> listaNotaDocumento = findForAlterarSituacaoVenda(nota.getCdNota().toString());
			List<Venda> listaVendaFaturada = new ArrayList<Venda>();
			if(listaNotaDocumento != null && !listaNotaDocumento.isEmpty()){
				for (NotaDocumento nd : listaNotaDocumento) {
					if (nd.getDocumento() != null && nd.getDocumento().getListaDocumentoOrigem() != null && 
							!nd.getDocumento().getListaDocumentoOrigem().isEmpty()){
						for(Documentoorigem documentoorigem : nd.getDocumento().getListaDocumentoOrigem()){
							if(documentoorigem.getVenda() != null){
								listaVendaFaturada.add(documentoorigem.getVenda());
							}
						}
					}
				}
			}
			
			if(listaVendaFaturada != null && !listaVendaFaturada.isEmpty()){
				String whereInVenda = CollectionsUtil.listAndConcatenate(listaVendaFaturada, "cdvenda", ",");
				if (whereInVenda != null && !whereInVenda.equals("")){
					StringBuilder whereInUpdate = new StringBuilder();
					for(String idVenda : whereInVenda.split(",")){
						if(!notaVendaService.existNotaEmitida(new Venda(Integer.parseInt(idVenda)), null)){
							whereInUpdate.append(idVenda).append(",");
						}
					}
					if(whereInUpdate.length() > 0){
						vendaService.updateSituacaoVenda(whereInUpdate.substring(0, whereInUpdate.length()-1), Vendasituacao.REALIZADA);
					}
				}
			}
		}
	}
	
	/**
	* M�todo que verifica o status da nota ap�s o cancelamento de uma conta a receber
	*
	* @param request
	* @param ids
	* @since 13/03/2017
	* @author Luiz Fernando
	*/
	public void verificaStatusNota(WebRequestContext request, String ids) {
		List<Nota> listaNota = notaService.findLiquidadaComDocumentoCancelado(ids);
		if(SinedUtil.isListNotEmpty(listaNota)){
			for(Nota nota : listaNota){
				if(NotaStatus.LIQUIDADA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.EMITIDA);
					notaService.alterarStatusAcao(nota, getObservacaoNotaDocumentoCancelado(nota, ids), null);
				}else if(NotaStatus.NFSE_LIQUIDADA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
					notaService.alterarStatusAcao(nota, getObservacaoNotaDocumentoCancelado(nota, ids), null);
				}else if(NotaStatus.NFE_LIQUIDADA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.NFE_EMITIDA);
					notaService.alterarStatusAcao(nota, getObservacaoNotaDocumentoCancelado(nota, ids), null);
				}
			}
		}
	}
	
	private String getObservacaoNotaDocumentoCancelado(Nota nota, String whereInDocumento) {
		StringBuilder sb = new StringBuilder();
		if(nota != null && SinedUtil.isListNotEmpty(nota.getListaNotaDocumento())){
			for(NotaDocumento notaDocumento : nota.getListaNotaDocumento()){
				if(notaDocumento.getDocumento() != null && notaDocumento.getDocumento().getCddocumento() != null &&
						(StringUtils.isBlank(whereInDocumento) || existeDocumento(notaDocumento.getDocumento().getCddocumento().toString(), whereInDocumento))){
					sb.append(" <a href=\"javascript:visualizarContaReceber("+notaDocumento.getDocumento().getCddocumento()+");\">"+notaDocumento.getDocumento().getCddocumento()+"</a>");
					sb.append(",");
				}
			}
		}
		
		String observacao = "";
		if(sb.length() > 0){
			observacao = "Conta(s) a receber cancelada(s):" + sb.substring(0, sb.length()-1) + ".";
		}
		return observacao;
	}
	
	private boolean existeDocumento(String id, String whereInDocumento) {
		if(id != null && StringUtils.isNotBlank(whereInDocumento)){
			for(String str : whereInDocumento.split(",")){
				if(str.equals(id)) return true;
			}
		}
		return false;
	}
	
	public boolean existeContaAssociadaNota(String whereIn) {
		return notaDocumentoDAO.existeContaAssociadaNota(whereIn);
	}
}
