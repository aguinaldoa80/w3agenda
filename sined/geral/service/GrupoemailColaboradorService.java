package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.geral.dao.GrupoemailColaboradorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupoemailColaboradorService extends GenericService<GrupoemailColaborador>{

	private GrupoemailColaboradorDAO grupoemailcolaboradorDAO;
	
	public void setGrupoemailcolaboradorDAO(GrupoemailColaboradorDAO grupoemailcolaboradorDAO) {
		this.grupoemailcolaboradorDAO = grupoemailcolaboradorDAO;
	}

	/**
	 * Retorna todos os colaboradores
	 * do grupoemail
	 * @param listagrupoemail
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.GrupoemailColaboradorDAO#find(Grupoemail)
	 * @author C�ntia Nogueira
	 */
	public List<GrupoemailColaborador> find(List<Grupoemail> listagrupoemail){
		return grupoemailcolaboradorDAO.find(listagrupoemail);
	}
	private GrupoemailColaboradorDAO grupoemailColaboradorDAO;
	public void setGrupoemailColaboradorDAO(GrupoemailColaboradorDAO grupoemailColaboradorDAO) {
		this.grupoemailColaboradorDAO = grupoemailColaboradorDAO;
	}
	
	/**
	 * M�todo para salvar uma lista de <code>GrupoemailColaborador</code>.
	 * Usa transaction pr�pria.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.GrupoemailColaboradorDAO#saveOrUpdateNoUseTransaction(GrupoemailColaborador)
	 * @param lista
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateListaProjetoColaborador(final Collection<GrupoemailColaborador> lista){
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (GrupoemailColaborador gce : lista) {
					grupoemailColaboradorDAO.saveOrUpdateNoUseTransaction(gce);
				}
				return null;
			}
		});
	}
}
