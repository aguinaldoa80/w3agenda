package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.EmporiumreducaozDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumreducaozService extends GenericService<Emporiumreducaoz>{
	
	private EmporiumreducaozDAO emporiumreducaozDAO;
	
	public void setEmporiumreducaozDAO(EmporiumreducaozDAO emporiumreducaozDAO) {
		this.emporiumreducaozDAO = emporiumreducaozDAO;
	}

	public List<Emporiumreducaoz> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		return emporiumreducaozDAO.findForSped(emporiumpdv, dtinicio, dtfim);
	}

	/* singleton */
	private static EmporiumreducaozService instance;
	public static EmporiumreducaozService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(EmporiumreducaozService.class);
		}
		return instance;
	}

	public boolean haveReducaozEmpresa(Empresa empresa, Date dataInicio, Date dataFim) {
		return emporiumreducaozDAO.haveReducaozEmpresa(empresa, dataInicio, dataFim);
	}

	public Emporiumreducaoz loadReducaoz(Integer crz, Date data, Emporiumpdv emporiumpdv) {
		return emporiumreducaozDAO.loadReducaoz(crz, data, emporiumpdv);
	}

	public List<Emporiumreducaoz> findForAcerto(Date dtinicio, Date dtfim) {
		return emporiumreducaozDAO.findForAcerto(dtinicio, dtfim);
	}

	public List<Emporiumreducaoz> getReducaozByPdv(List<Emporiumreducaoz> listaReducaoz, Emporiumpdv emporiumpdv) {
		List<Emporiumreducaoz> listaReducaozPDV = new ArrayList<Emporiumreducaoz>();
		for (Emporiumreducaoz emporiumreducaoz : listaReducaoz) {
			if(emporiumreducaoz.getEmporiumpdv() != null && 
					emporiumreducaoz.getEmporiumpdv().equals(emporiumpdv)){
				listaReducaozPDV.add(emporiumreducaoz);
			}
		}
		return listaReducaozPDV;
	}

	public Emporiumreducaoz findLastReducaoz(Date data, Emporiumpdv emporiumpdv) {
		return emporiumreducaozDAO.findLastReducaoz(data, emporiumpdv);
	}

}
