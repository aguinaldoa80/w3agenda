package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.TarefarecursogeralDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefarecursogeralService extends GenericService<Tarefarecursogeral>{

	private TarefarecursogeralDAO tarefarecursogeralDAO;
	
	public void setTarefarecursogeralDAO(
			TarefarecursogeralDAO tarefarecursogeralDAO) {
		this.tarefarecursogeralDAO = tarefarecursogeralDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefarecursogeralDAO#findByPlanejamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findByPlanejamento(Planejamento planejamento) {
		return tarefarecursogeralDAO.findByPlanejamento(planejamento);
	}

	/**
	 * Preenche a lista de valores de referÍncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#findByPlanejamento
	 * @param planejamento
	 * @param listaReferencia
	 * @param listaTodas
	 * @author Rodrigo Freitas
	 */
	public void valoresTarefaGeral(Planejamento planejamento, List<Valorreferencia> listaReferencia, List<Valorreferencia> listaTodas) {
		Valorreferencia valorreferencia;
		List<Tarefarecursogeral> listaTarefaGeral = this.findByPlanejamento(planejamento);
		for (Tarefarecursogeral tarefarecursogeral : listaTarefaGeral) {
			valorreferencia = new Valorreferencia();
			valorreferencia.setMaterial(tarefarecursogeral.getMaterial());
			valorreferencia.setPlanejamento(planejamento);
			valorreferencia.setValor(new Money(tarefarecursogeral.getMaterial().getValorvenda()));
			
			if (!listaTodas.contains(valorreferencia) && !listaReferencia.contains(valorreferencia)) {
				listaReferencia.add(valorreferencia);
			}
		}
	}

	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefarecursogeralDAO#findForOrcamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findForOrcamento(Planejamento planejamento) {
		return tarefarecursogeralDAO.findForOrcamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de referÍncia a partir de uma lista de tarefaRecursoGeral.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#findForOrcamento
	 * @see br.com.linkcom.sined.geral.bean.Valorreferencia#getValor
	 * @param filtro
	 * @param listaFolhas
	 * @param listaOutrosMaterial
	 * @param somamaterial
	 * @param listaValorReferencia
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money preencheListaVRTarefaMat(OrcamentoAnaliticoReportFiltro filtro, List<AnaliseRecDespBean> listaFolhas,
			List<AnaliseRecDespBean> listaOutrosMaterial, Money somamaterial, List<Valorreferencia> listaValorReferencia) {
		AnaliseRecDespBean bean;
		Money valor;
		List<Tarefarecursogeral> listaTarefaGeral = this.findForOrcamento(filtro.getPlanejamento());
		for (Tarefarecursogeral tarefarecursogeral : listaTarefaGeral) {
			bean = new AnaliseRecDespBean();
			if (tarefarecursogeral.getMaterial().getContagerencial() != null) {
				bean.setIdentificador(tarefarecursogeral.getMaterial().getContagerencial().getVcontagerencial().getIdentificador());
				bean.setNome(tarefarecursogeral.getMaterial().getContagerencial().getNome());
				bean.setOperacao(tarefarecursogeral.getMaterial().getContagerencial().getTipooperacao().getSigla());
				valor = Valorreferencia.getValor(tarefarecursogeral.getMaterial(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(tarefarecursogeral.getQtde())) : new Money());
				
				listaFolhas.add(bean);
			} else {
				bean.setIdentificador("XX.XX.XX");
				bean.setNome(tarefarecursogeral.getMaterial().getNome());
				bean.setOperacao("D");
				valor = Valorreferencia.getValor(tarefarecursogeral.getMaterial(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(tarefarecursogeral.getQtde())) : new Money());
				somamaterial = somamaterial.add(valor != null ? valor.multiply(new Money(tarefarecursogeral.getQtde())) : new Money());
				
				listaOutrosMaterial.add(bean);
			}
		}
		return somamaterial;
	}	
	
	public List<Tarefarecursogeral> preencheListaPlanejamentoRecGeral(Collection<Planejamentorecursogeral> lista){
		List<Tarefarecursogeral> listaRecGeral = new ArrayList<Tarefarecursogeral>();
		Tarefarecursogeral pg = null;
		for (Planejamentorecursogeral t : lista) {
			pg = new Tarefarecursogeral();
			pg.setCdtarefarecursogeral(t.getCdplanejamentorecursogeral());
			pg.setMaterial(t.getMaterial());
			pg.setQtde(t.getQtde());
			
			listaRecGeral.add(pg);
		}
		
		return listaRecGeral;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefarecursogeralDAO#findByTarefa
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findByTarefa(String whereIn) {
		return tarefarecursogeralDAO.findByTarefa(whereIn);
	}

	
}
