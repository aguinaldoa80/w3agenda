package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.geral.dao.ColaboradorequipamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorequipamentoService extends GenericService<Colaboradorequipamento> {	
	
	private ColaboradorequipamentoDAO colaboradorequipamentoDAO;

	public void setColaboradorequipamentoDAO(
			ColaboradorequipamentoDAO colaboradorequipamentoDAO) {
		this.colaboradorequipamentoDAO = colaboradorequipamentoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorequipamentoDAO#updateDtdevolucao(Colaboradorequipamento colaboradorequipamento)
	 *
	 * @param colaboradorequipamento
	 * @since 25/05/2012
	 * @author Rodrigo Freitas
	 */
	public void updateDtdevolucao(Colaboradorequipamento colaboradorequipamento) {
		colaboradorequipamentoDAO.updateDtdevolucao(colaboradorequipamento);
	}
	
	/**
	 * Faz ref�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorequipamentoDAO#loadWithEPI(Colaboradorequipamento colaboradorequipamento)
	 * 
	 * @param colaboradorequipamento
	 * @return
	 * @since 04/09/2013
	 * @author Luiz Romario Filho
	 */
	public Colaboradorequipamento loadWithEPI(Colaboradorequipamento colaboradorequipamento) {
		return colaboradorequipamentoDAO.loadWithEPI(colaboradorequipamento);
	}

	/* singleton */
	private static ColaboradorequipamentoService instance;
	public static ColaboradorequipamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorequipamentoService.class);
		}
		return instance;
	}
	
	/**
	 * 
	 * Busca uma lista com dados do equipamento com EPI
	 * 
	 * @param colaborador
	 * @return
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 */
	public List<Colaboradorequipamento> loadByColaborador(Colaborador colaborador){
		return colaboradorequipamentoDAO.loadByColaborador(colaborador);
	}
}
