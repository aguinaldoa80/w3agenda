package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacao;
import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacaohistorico;
import br.com.linkcom.sined.geral.dao.PedidovendasincronizacaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendasincronizacaohistoricoService extends GenericService<Pedidovendasincronizacaohistorico> {
	
	private PedidovendasincronizacaohistoricoDAO pedidovendasincronizacaohistoricoDAO;
	
	public void setPedidovendasincronizacaohistoricoDAO(PedidovendasincronizacaohistoricoDAO pedidovendasincronizacaohistoricoDAO) {
		this.pedidovendasincronizacaohistoricoDAO = pedidovendasincronizacaohistoricoDAO;
	}

	/**
	 * M�todo que cria/salva o hist�rico da sincroniza��o com o wms
	 *
	 * @param pedidovendasincronizacao
	 * @param observacao
	 * @author Luiz Fernando
	 */
	public void criarSalvarHistoricoSincronizacao(Pedidovendasincronizacao pedidovendasincronizacao, String observacao){
		if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
			Pedidovendasincronizacaohistorico historico = new Pedidovendasincronizacaohistorico();
		
			historico.setPedidovendasincronizacao(pedidovendasincronizacao);
			historico.setObservacao(observacao);
			this.saveOrUpdate(historico);
		}		
	}	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaohistoricoDAO#insertPedidovendasincronizacaohistorico(Pedidovendasincronizacao pedidovendasincronizacao, String obs, String log)
	 *
	 * @param pedidovendasincronizacao
	 * @param obs
	 * @param log
	 * @author Luiz Fernando
	 */
	public void insertPedidovendasincronizacaohistorico(Pedidovendasincronizacao pedidovendasincronizacao, String obs, String log) {
		pedidovendasincronizacaohistoricoDAO.insertPedidovendasincronizacaohistorico(pedidovendasincronizacao, obs, log);
	}
}
