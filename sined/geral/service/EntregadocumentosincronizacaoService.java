package br.com.linkcom.sined.geral.service;

import java.rmi.RemoteException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.AxisFault;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentosincronizacao;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamaterialsincronizacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.bean.Materiallegendamaterialtipo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.dao.EntregadocumentosincronizacaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialDevolucaoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.wms.WmsWebServiceStub;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ArrayOfProdutoNotaEntrada;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarClasseProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarFornecedor;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarNotaEntrada;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ClasseProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.NotaEntrada;
import br.com.linkcom.sined.wms.WmsWebServiceStub.Produto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ProdutoNotaEntrada;
import br.com.linkcom.sined.wms.WmsWebServiceStub.Produtos;

public class EntregadocumentosincronizacaoService extends GenericService<Entregadocumentosincronizacao> {
	
	private EntregadocumentosincronizacaoDAO entregadocumentosincronizacaoDAO;
	private MaterialService materialService;
	private FornecedorService fornecedorService;
	private EmpresaService empresaService;
	private EntregamaterialService entregamaterialService;
	private ClienteService clienteService;
	private ParametrogeralService parametrogeralService;
	
	public void setEntregadocumentosincronizacaoDAO(EntregadocumentosincronizacaoDAO entregadocumentosincronizacaoDAO) {
		this.entregadocumentosincronizacaoDAO = entregadocumentosincronizacaoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEntregamaterialService(
			EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	/* singleton */
	private static EntregadocumentosincronizacaoService instance;
	public static EntregadocumentosincronizacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EntregadocumentosincronizacaoService.class);
		}
		return instance;
	}
	
	
	public Boolean existEntregadocumentoNaoSincronizado(){
		return entregadocumentosincronizacaoDAO.existEntregadocumentoNaoSincronizado();
	}
	
	public List<Entregadocumentosincronizacao> findForSincronizacaoEntregadocumentoPendente(){
		return entregadocumentosincronizacaoDAO.findForSincronizacaoEntregadocumentoPendente();
	}
	
	public void updateEntregadocumentosincronizacaoSincronizado(Entregadocumentosincronizacao eds) {
		entregadocumentosincronizacaoDAO.updateEntregadocumentosincronizacaoSincronizado(eds);
	}
	
	public void updateEntregadocumentosincronizacaoTentativaDeSincronizacao(Entregadocumentosincronizacao eds) {
		entregadocumentosincronizacaoDAO.updateEntregadocumentosincronizacaoTentativaDeSincronizacao(eds);
	}
	
	public Entregadocumentosincronizacao findByEntregadocumentosincronizacao(Entregadocumentosincronizacao eds) {
		return entregadocumentosincronizacaoDAO.findByEntregadocumentosincronizacao(eds);
	}
	
	public Entregadocumentosincronizacao findForDevolucaoMaterial(Integer cdentregadocumentosincronizacao) {
		return entregadocumentosincronizacaoDAO.findForDevolucaoMaterial(cdentregadocumentosincronizacao);
	}

	public void updateEntregadocumentosincronizacaoDevolucaoefetuada(Entregadocumentosincronizacao eds) {
		entregadocumentosincronizacaoDAO.updateEntregadocumentosincronizacaoDevolucaoefetuada(eds);
	}
	
	public Entregadocumento getEntregaOrigemEntregasincronizacao(Integer cdentregadocumentosincronizacao) {
		Entregadocumentosincronizacao eds = findByEntregadocumentosincronizacao(new Entregadocumentosincronizacao(cdentregadocumentosincronizacao));
		Entregadocumento entregadocumento = new Entregadocumento();
		
		if(eds != null && eds.getEntregadocumento() != null){
			entregadocumento = eds.getEntregadocumento();
		} else {
			entregadocumento.setCdentregadocumento(cdentregadocumentosincronizacao);
		}
		
		entregadocumento.setEntregadocumentosincronizacao(eds);
		
		return entregadocumento;
	}
	
	/**
	 * M�todo que faz a sincroniza��o dos pedidos de vendas para o wms
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendasincronizacaoServic#findForSincronizacaoPedidoVendaPendente()
	 *
	 * @author Luiz Fernando
	 */
	public void sincronizarEntrega(){
		if(this.existEntregadocumentoNaoSincronizado()){
			try {
				List<Entregadocumentosincronizacao> lista = this.findForSincronizacaoEntregadocumentoPendente();				
				WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(empresaService.loadPrincipal()) + "/webservices/WmsWebService");
				
				if(lista != null && !lista.isEmpty()){
					Map<Integer, Integer> mapaCdarquivos = null;
					try {
						String param = parametrogeralService.buscaValorPorNome("GERAR_LEGENDAS_CONCATENADAS");
						if(param != null && param.equalsIgnoreCase("true")){
							mapaCdarquivos = generateImagensLegendas(lista, 20, 20);
						}
					} catch (Exception e) {
						e.printStackTrace();
						SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de gera��o de legendas)", 0, e);
					}
					for(Entregadocumentosincronizacao eds : lista){
						this.updateEntregadocumentosincronizacaoTentativaDeSincronizacao(eds);
						criarEnviarInfEntregadocumentoWms(eds, mapaCdarquivos, stub);
					}
				}
				
			} catch (AxisFault e) {
				e.printStackTrace();
				SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de busca de 'Entregadocumentosincronizacao')", 0, e);
			} catch (Exception e) {
				e.printStackTrace();
				SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de busca de 'Entregadocumentosincronizacao')", 0, e);
			}
		}
	}
	
	private Map<Integer, Integer> generateImagensLegendas(List<Entregadocumentosincronizacao> lista, Integer defaultWidth, Integer defaultHeight) throws Exception{		
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		Map<Integer, List<Arquivo>> mapaArquivos = new HashMap<Integer, List<Arquivo>>();
		Set<Materialtipo> listaMT = new ListSet<Materialtipo>(Materialtipo.class);
		for(Entregadocumentosincronizacao eds : lista){
			if(eds.getListaEntregamaterialsincronizacao() != null){
				for(Entregamaterialsincronizacao ems : eds.getListaEntregamaterialsincronizacao()){
					if(ems.getMaterial() != null && ems.getMaterial().getMaterialtipo() != null
							&& ems.getMaterial().getMaterialtipo().getCdmaterialtipo() != null
							&& !mapa.containsKey(ems.getMaterial().getMaterialtipo().getCdmaterialtipo())){
						listaMT.add(ems.getMaterial().getMaterialtipo());
					}
				}
			}
		}
		if(!listaMT.isEmpty()){
			String whereIn = SinedUtil.listAndConcatenate(listaMT, "cdmaterialtipo", ",");
			List<Materiallegenda> listaML = MateriallegendaService.getInstance().findByMaterialtipo(whereIn);
			if(listaML != null && !listaML.isEmpty()){
				for(Materiallegenda ml : listaML){
					if(ml.getSimbolo() != null && ml.getSimbolo().getCdarquivo() != null && ml.getListaMateriallegendamaterialtipo() != null){
						for(Materiallegendamaterialtipo mlmt : ml.getListaMateriallegendamaterialtipo()){
							if(!mapaArquivos.containsKey(mlmt.getMaterialtipo().getCdmaterialtipo())){
								mapaArquivos.put(mlmt.getMaterialtipo().getCdmaterialtipo(), new ListSet<Arquivo>(Arquivo.class));
							}
							mapaArquivos.get(mlmt.getMaterialtipo().getCdmaterialtipo()).add(ml.getSimbolo());
						}
					}
				}
			}
			mapa = SinedUtil.generateImagensByMaterialtipo(mapaArquivos, defaultWidth, defaultHeight);
		}
		return mapa;
	}
	
	/**
	 * M�todo que cria e envia as informa��es do Recebimento/Devolucao de Material 
	 * Envia para o WMS como um recebimento
	 *
	 * @param entregasincronizacao
	 * @param stub
	 * @author Luiz Fernando
	 */
	public void criarEnviarInfEntregadocumentoWms(Entregadocumentosincronizacao eds, Map<Integer, Integer> mapaCdarquivo, WmsWebServiceStub stub){
		try {
			String paramMaterialIdentificacaoWMS = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAR_IDENTIFICADOR_MATERIAL_WMS);
			
			br.com.linkcom.sined.wms.WmsWebServiceStub.Fornecedor fornecedor = new br.com.linkcom.sined.wms.WmsWebServiceStub.Fornecedor();
			fornecedor.setId(eds.getFornecedorId());
			fornecedor.setNome(eds.getFornecedorNome());
			fornecedor.setNatureza(eds.getFornecedorNatureza());
			fornecedor.setDocumento(StringUtils.soNumero(eds.getFornecedorDocumento()));
			
			CadastrarFornecedor cadastrarFornecedor = new CadastrarFornecedor();
			cadastrarFornecedor.setIn0(fornecedor);
			stub.cadastrarFornecedor(cadastrarFornecedor);
			
			List<ProdutoNotaEntrada> listaProdutos = new ArrayList<ProdutoNotaEntrada>();
			
			List<Entregamaterialsincronizacao> listaMaterial = eds.getListaEntregamaterialsincronizacao();
			for (Entregamaterialsincronizacao entregamaterialsincronizacao : listaMaterial) {
				
				ClasseProduto classeProduto = new ClasseProduto();
				classeProduto.setNumero(entregamaterialsincronizacao.getMaterialgrupoNumero());
				classeProduto.setNome(entregamaterialsincronizacao.getMaterialgrupoNome());
				
				CadastrarClasseProduto cadastrarClasseProduto = new CadastrarClasseProduto();
				cadastrarClasseProduto.setIn0(classeProduto);
				stub.cadastrarClasseProduto(cadastrarClasseProduto);
				
				Produto produto = new Produto();
				produto.setClasseProduto(entregamaterialsincronizacao.getMaterialClasseproduto());
				produto.setCodigo(entregamaterialsincronizacao.getMaterialCodigo());
				produto.setDescricao(entregamaterialsincronizacao.getMaterialDescricao());
				produto.setId(entregamaterialsincronizacao.getMaterial().getCdmaterial());
				produto.setCdprodutomestre(entregamaterialsincronizacao.getMaterialmestreId() != null ? entregamaterialsincronizacao.getMaterialmestreId() : 0);
				produto.setQuantidadeVolume(entregamaterialsincronizacao.getMaterialQuantidadeVolume());
				produto.setReferencia(entregamaterialsincronizacao.getMaterialReferencia());
				produto.setDescricaoEmbalagem(entregamaterialsincronizacao.getMaterialDescricaoEmbalagem());
				produto.setPeso(entregamaterialsincronizacao.getMaterialPeso() != null ? entregamaterialsincronizacao.getMaterialPeso() : 0.);
				produto.setAltura(entregamaterialsincronizacao.getMaterialAltura() != null ? entregamaterialsincronizacao.getMaterialAltura() : 0);
				produto.setLargura(entregamaterialsincronizacao.getMaterialLargura() != null ? entregamaterialsincronizacao.getMaterialLargura() : 0);
				produto.setProfundidade(entregamaterialsincronizacao.getMaterialComprimento() != null ? entregamaterialsincronizacao.getMaterialComprimento() : 0);
				produto.setCdmaterialcor(entregamaterialsincronizacao.getMaterialcorId() != null ? entregamaterialsincronizacao.getMaterialcorId() : 0);
				produto.setCdarquivolegenda(0);
				if(mapaCdarquivo != null && !mapaCdarquivo.isEmpty()
						&& entregamaterialsincronizacao.getMaterial() != null && entregamaterialsincronizacao.getMaterial().getMaterialtipo() != null 
						&& entregamaterialsincronizacao.getMaterial().getMaterialtipo().getCdmaterialtipo() != null){
					if(mapaCdarquivo.containsKey(entregamaterialsincronizacao.getMaterial().getMaterialtipo().getCdmaterialtipo()))
						produto.setCdarquivolegenda(mapaCdarquivo.get(entregamaterialsincronizacao.getMaterial().getMaterialtipo().getCdmaterialtipo()));
				}
				
				if(org.apache.commons.lang.StringUtils.isNotBlank(paramMaterialIdentificacaoWMS) &&
						"TRUE".equalsIgnoreCase(paramMaterialIdentificacaoWMS)){
					try {
						produto.setCodigo(entregamaterialsincronizacao.getMaterial().getIdentificacao());
					} catch (Exception e) {}
				}
				
				CadastrarProduto cadastrarProduto = new CadastrarProduto();
				cadastrarProduto.setIn0(produto);
				stub.cadastrarProduto(cadastrarProduto);
				
				ProdutoNotaEntrada produtoNotaEntrada = new ProdutoNotaEntrada();
				produtoNotaEntrada.setIdProduto(entregamaterialsincronizacao.getEntregamaterialProdutoId());
				produtoNotaEntrada.setNotaEntrada(eds.getCdentregadocumentosincronizacao());
				produtoNotaEntrada.setQuantidade(entregamaterialsincronizacao.getEntregamaterialQuantidade());
				produtoNotaEntrada.setValorUnitario(entregamaterialsincronizacao.getEntregamaterialValorUnitario());
				produtoNotaEntrada.setLote(entregamaterialsincronizacao.getEntregamaterialLote());
				
				listaProdutos.add(produtoNotaEntrada);
			}
			
			ArrayOfProdutoNotaEntrada arrayOfProdutoNotaEntrada = new ArrayOfProdutoNotaEntrada();
			ProdutoNotaEntrada[] arrayProdutos = new ProdutoNotaEntrada[listaProdutos.size()];		
			for (int i = 0; i < arrayProdutos.length; i++) {
				arrayProdutos[i] = listaProdutos.get(i);
			}
			arrayOfProdutoNotaEntrada.setProdutoNotaEntrada(arrayProdutos);
			
			Produtos produtos = new Produtos();
			produtos.setProdutos(arrayOfProdutoNotaEntrada);
						
			NotaEntrada notaEntrada = new NotaEntrada();
			notaEntrada.setListaProdutos(produtos);
			notaEntrada.setId(eds.getCdentregadocumentosincronizacao());
			notaEntrada.setDataChegada(SinedDateUtils.dateToCalendar(eds.getNotaEntradaDataChegada()));
			notaEntrada.setDataEmissao(SinedDateUtils.dateToCalendar(eds.getNotaEntradaDataEmissao()));
			notaEntrada.setDataLancamento(SinedDateUtils.dateToCalendar(eds.getNotaEntradaDataLancamento()));
			notaEntrada.setDeposito(StringUtils.soNumero(eds.getNotaEntradaDeposito()));
			if(eds.getDevolucao() != null && eds.getDevolucao()){
				notaEntrada.setFornecedor(eds.getFornecedorId());
			}else if(eds.getNotaEntradaFornecedor() != null)
				notaEntrada.setFornecedor(eds.getNotaEntradaFornecedor());
			else
				notaEntrada.setFornecedor(eds.getFornecedor().getCdpessoa());
			notaEntrada.setNumero(eds.getNotaEntradaNumero());
			notaEntrada.setPlacaVeiculo(eds.getNotaEntradaPlacaVeiculo());
			notaEntrada.setTipo(eds.getNotaEntradaTipo());
			notaEntrada.setTransportadora(eds.getNotaEntradaTransportadora());
			
			CadastrarNotaEntrada cadastrarNotaEntrada = new CadastrarNotaEntrada();
			cadastrarNotaEntrada.setIn0(notaEntrada);
			stub.cadastrarNotaEntrada(cadastrarNotaEntrada);			
			
			this.updateEntregadocumentosincronizacaoSincronizado(eds);
		
		} catch (AxisFault e) {
			e.printStackTrace();
//			System.out.println("erro no envio ao wms (entrada fiscal): " + e.getMessage());
			SinedUtil.enviaEmailErroSincronizacaoWMS("Entregadocumentosincronizacao", eds.getCdentregadocumentosincronizacao(), e);
		} catch (RemoteException e) {
			e.printStackTrace();
//			System.out.println("erro no envio ao wms (entrada fiscal): " + e.getMessage());
			SinedUtil.enviaEmailErroSincronizacaoWMS("Entregadocumentosincronizacao", eds.getCdentregadocumentosincronizacao(), e);
		}catch (Exception e) {
			e.printStackTrace();
//			System.out.println("erro no envio ao wms (entrada fiscal): " + e.getMessage());
			SinedUtil.enviaEmailErroSincronizacaoWMS("Entregadocumentosincronizacao", eds.getCdentregadocumentosincronizacao(), e);
		}
	}
	
	/**
	 * M�todo que cria a Entregasincronizacao atrav�s da entrega. 
	 * Os dados que eram enviados para o WMS direto, agora est�o sendo salvos na tabela Entregasincronizacao para ter um controle
	 * de possiv�is erros
	 *
	 * @param ed
	 * @param devolucao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentosincronizacao> criarEntregadocumentoSincronizacao(Entrega entrega, Boolean devolucao){
		List<Entregadocumentosincronizacao> lista = new ArrayList<Entregadocumentosincronizacao>();
		Entregadocumentosincronizacao eds;
		
		for(Entregadocumento ed : entrega.getListaEntregadocumento()){
			eds = new Entregadocumentosincronizacao();
			eds.setEntregadocumento(ed);
			eds.setDevolucao(devolucao);
			Empresa empresa = empresaService.loadPrincipal();
			
			//Fornecedor
			if(ed.getFornecedor() != null){
				eds.setFornecedorId(ed.getFornecedor().getCdpessoa());
				eds.setFornecedorNome(ed.getFornecedor().getNome());
				eds.setFornecedor(fornecedorService.load(ed.getFornecedor()));
				eds.setFornecedorNome(eds.getFornecedor().getNome());
				if(eds.getFornecedor().getTipopessoa() != null){
					if (eds.getFornecedor().getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
						eds.setFornecedorNatureza("F");
					} else {
						eds.setFornecedorNatureza("J");
					}
				}
				eds.setFornecedorDocumento(StringUtils.soNumero(eds.getFornecedor().getCpfCnpj()));
			}
			
			
			List<Entregamaterialsincronizacao> listaProdutos = new ArrayList<Entregamaterialsincronizacao>();		
			Set<Entregamaterial> listaMaterial = ed.getListaEntregamaterial();
			Entregamaterialsincronizacao entregamaterialsincronizacao;
			for (Entregamaterial entregamaterial : listaMaterial) {
				entregamaterialsincronizacao = new Entregamaterialsincronizacao();	
				if(entregamaterial.getConsiderarItemGradeForWms() == null || !entregamaterial.getConsiderarItemGradeForWms()){ 
					entregamaterial = entregamaterialService.carregaEntregamaterialWMS(entregamaterial);
				}else if(entregamaterial.getCdentregamaterial() != null){
					Entregamaterial em = entregamaterialService.carregaEntregamaterialWMS(entregamaterial);
					entregamaterial.setLoteestoque(em.getLoteestoque());
					entregamaterial.setListaEntregamateriallote(em.getListaEntregamateriallote());
					entregamaterial.setValorunitario(em.getValorunitario());
				}
				entregamaterialsincronizacao.setEntregamaterial(entregamaterial);
				entregamaterialsincronizacao.setMaterial(entregamaterial.getMaterial());
				
				//Grupo Material
				if(entregamaterialsincronizacao.getMaterial() != null && entregamaterialsincronizacao.getMaterial().getMaterialgrupo() != null){
					entregamaterialsincronizacao.setMaterialgrupo(entregamaterialsincronizacao.getMaterial().getMaterialgrupo());
					if(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						entregamaterialsincronizacao.setMaterialgrupoNumero(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					entregamaterialsincronizacao.setMaterialgrupoNome(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getNome());
				}
				
				//Material
				if(entregamaterialsincronizacao.getMaterial() != null){
					if(entregamaterialsincronizacao.getMaterial().getMaterialgrupo() != null && entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						entregamaterialsincronizacao.setMaterialClasseproduto(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					if(entregamaterialsincronizacao.getMaterial().getCdmaterial() != null)
						entregamaterialsincronizacao.setMaterialCodigo(entregamaterialsincronizacao.getMaterial().getCdmaterial().toString());
					entregamaterialsincronizacao.setMaterialDescricao(entregamaterialsincronizacao.getMaterial().getNome());
					entregamaterialsincronizacao.setMaterialId(entregamaterialsincronizacao.getMaterial().getCdmaterial());
					entregamaterialsincronizacao.setMaterialmestreId(entregamaterialsincronizacao.getMaterial().getMaterialmestregrade() != null
												? entregamaterialsincronizacao.getMaterial().getMaterialmestregrade().getCdmaterial() : 0);
					entregamaterialsincronizacao.setMaterialReferencia(entregamaterialsincronizacao.getMaterial().getCodigofabricante());
					entregamaterialsincronizacao.setMaterialPeso(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
							&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getPesobruto() != null
							? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getPesobruto()) : 0.);
					entregamaterialsincronizacao.setMaterialAltura(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
							&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getAltura() != null
							? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getAltura()/10) : 0);
					entregamaterialsincronizacao.setMaterialLargura(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
												&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getLargura() != null
												? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getLargura()/10) : 0);
					entregamaterialsincronizacao.setMaterialComprimento(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
												&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getComprimento() != null
												? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getComprimento()/10) : 0);
					entregamaterialsincronizacao.setMaterialcorId(entregamaterialsincronizacao.getMaterial().getMaterialcor() != null 
												&& entregamaterialsincronizacao.getMaterial().getMaterialcor().getCdmaterialcor() != null 
												? entregamaterialsincronizacao.getMaterial().getMaterialcor().getCdmaterialcor() : 0);
				}
				entregamaterialsincronizacao.setMaterialQuantidadeVolume(1);
				entregamaterialsincronizacao.setMaterialDescricaoEmbalagem(entregamaterial.getMaterial().getUnidademedida() != null 
																			? entregamaterial.getMaterial().getUnidademedida().getSimbolo() : "UN");
				if(entregamaterialsincronizacao.getMaterial() != null)
					entregamaterialsincronizacao.setEntregamaterialProdutoId(entregamaterialsincronizacao.getMaterial().getCdmaterial());
				entregamaterialsincronizacao.setEntregamaterialIdEntrega(ed.getCdentregadocumento());
				if(entregamaterial != null){					
					if(entregamaterial.getQtde() != null)
						entregamaterialsincronizacao.setEntregamaterialQuantidade(entregamaterial.getQtde().intValue());
					entregamaterialsincronizacao.setEntregamaterialValorUnitario(entregamaterial.getValorunitario());
					entregamaterialsincronizacao.setEntregamaterialLote(entregamaterial.getNumeroLotes());
				}
				
				listaProdutos.add(entregamaterialsincronizacao);
			}
			
			
			eds.setNotaEntradaId(ed.getCdentregadocumento());
			if(entrega.getDtentrega() != null){
				eds.setNotaEntradaDataChegada(entrega.getDtentrega());
			} else {
				eds.setNotaEntradaDataChegada(SinedDateUtils.currentDate());
			}
			if(ed.getDtemissao() != null){
				eds.setNotaEntradaDataEmissao(ed.getDtemissao());
			} else {
				eds.setNotaEntradaDataEmissao(SinedDateUtils.currentDate());
			}
			
			eds.setNotaEntradaDataLancamento(SinedDateUtils.currentDate());
			if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
				Empresa empresaNota = empresaService.load(ed.getEmpresa(), "empresa.cnpj, empresa.cpf");
				eds.setNotaEntradaDeposito(StringUtils.soNumero(empresaNota.getCpfCnpj()));
			}else {
				eds.setNotaEntradaDeposito(StringUtils.soNumero(empresa.getCpfCnpj()));
			}
			if(ed.getFornecedor() != null)
				eds.setNotaEntradaFornecedor(ed.getFornecedor().getCdpessoa());
			eds.setNotaEntradaNumero(ed.getNumero());
			if(ed.getPlacaveiculo() != null && !ed.getPlacaveiculo().equals("")){
				eds.setNotaEntradaPlacaVeiculo(ed.getPlacaveiculo().replace("-", ""));
			} else {
				eds.setNotaEntradaPlacaVeiculo("AAA9999");
			}
			eds.setNotaEntradaTipo("F");
			if(ed.getTransportadora() != null && !ed.getTransportadora().equals("")){
				eds.setNotaEntradaTransportadora(ed.getTransportadora());
			} else {
				eds.setNotaEntradaTransportadora("TRANSPORTADORA");
			}
			
			eds.setListaEntregamaterialsincronizacao(listaProdutos);
			
			lista.add(eds);
		}
		
		return lista;
	}	
	
	/**
	 * M�todo que cria uma entregasincronizacao atrav�s de uma devolu��o de mercadoria
	 *
	 * @param bean
	 * @param devolucao
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumentosincronizacao criarEntregasincronizacaoOrigemDevolucao(VendaDevolucaoBean bean, Boolean devolucao) {
		
		Entregadocumentosincronizacao eds = new Entregadocumentosincronizacao();
		eds.setDevolucao(devolucao);
		if(devolucao != null && devolucao)
			eds.setDevolucaoefetuada(Boolean.FALSE);
		eds.setVenda(bean.getVenda());
		eds.setPedidovenda(bean.getPedidovenda());
		Empresa empresa = empresaService.loadPrincipal();
		
		//Fornecedor
		if(bean.getCliente() != null){
			eds.setCliente(bean.getCliente());
			eds.setFornecedorId(bean.getCliente().getCdpessoa());
			Cliente cliente = clienteService.load(bean.getCliente());
			eds.setFornecedorNome(cliente.getNome());
			if(cliente.getTipopessoa() != null){
				if (cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
					eds.setFornecedorNatureza("F");
				} else {
					eds.setFornecedorNatureza("J");
				}
			}
			eds.setFornecedorDocumento(StringUtils.soNumero(cliente.getCpfCnpj()));
		}
		
		
		List<Entregamaterialsincronizacao> listaProdutos = new ArrayList<Entregamaterialsincronizacao>();		
		List<VendamaterialDevolucaoBean> listaMaterial = bean.getListaItens();
		Entregamaterialsincronizacao entregamaterialsincronizacao;
		for (VendamaterialDevolucaoBean itemBean : listaMaterial) {
			if(itemBean.getQtdedevolvida() != null && itemBean.getQtdedevolvida() > 0){
				entregamaterialsincronizacao = new Entregamaterialsincronizacao();			
//				entregamaterialsincronizacao.setEntregamaterial(vendamaterial);
				if(itemBean.getMaterialdevolucao() != null && itemBean.getMaterialdevolucao().getCdmaterialdevolucao() != null)
					entregamaterialsincronizacao.setMaterialdevolucao(itemBean.getMaterialdevolucao());
				
				//Grupo Material
				entregamaterialsincronizacao.setMaterial(materialService.carregaMaterialWMS(itemBean.getMaterial()));
				if(entregamaterialsincronizacao.getMaterial() != null && entregamaterialsincronizacao.getMaterial().getMaterialgrupo() != null){
					entregamaterialsincronizacao.setMaterialgrupo(entregamaterialsincronizacao.getMaterial().getMaterialgrupo());
					if(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						entregamaterialsincronizacao.setMaterialgrupoNumero(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					entregamaterialsincronizacao.setMaterialgrupoNome(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getNome());
				}
				
				//Material
				if(entregamaterialsincronizacao.getMaterial() != null){
					if(entregamaterialsincronizacao.getMaterial().getMaterialgrupo() != null && entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						entregamaterialsincronizacao.setMaterialClasseproduto(entregamaterialsincronizacao.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					if(entregamaterialsincronizacao.getMaterial().getCdmaterial() != null)
						entregamaterialsincronizacao.setMaterialCodigo(entregamaterialsincronizacao.getMaterial().getCdmaterial().toString());
					entregamaterialsincronizacao.setMaterialDescricao(entregamaterialsincronizacao.getMaterial().getNome());
					entregamaterialsincronizacao.setMaterialId(entregamaterialsincronizacao.getMaterial().getCdmaterial());
					entregamaterialsincronizacao.setMaterialReferencia(entregamaterialsincronizacao.getMaterial().getCodigofabricante());
					entregamaterialsincronizacao.setMaterialPeso(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
							&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getPeso() != null
							? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getPeso()) : 0.);
					entregamaterialsincronizacao.setMaterialAltura(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
																	&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getAltura() != null
																	? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getAltura()/10) : 0);
					entregamaterialsincronizacao.setMaterialLargura(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
																	&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getLargura() != null
																	? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getLargura()/10) : 0);
					entregamaterialsincronizacao.setMaterialComprimento(entregamaterialsincronizacao.getMaterial().getMaterialproduto() != null
																	&& entregamaterialsincronizacao.getMaterial().getMaterialproduto().getComprimento() != null
																	? Math.round(entregamaterialsincronizacao.getMaterial().getMaterialproduto().getComprimento()/10) : 0);
				}
				entregamaterialsincronizacao.setMaterialQuantidadeVolume(1);
				entregamaterialsincronizacao.setMaterialDescricaoEmbalagem(entregamaterialsincronizacao.getMaterial().getUnidademedida() != null 
																	? entregamaterialsincronizacao.getMaterial().getUnidademedida().getSimbolo() : "UN");
				
				if(entregamaterialsincronizacao.getMaterial() != null)
					entregamaterialsincronizacao.setEntregamaterialProdutoId(entregamaterialsincronizacao.getMaterial().getCdmaterial());
				entregamaterialsincronizacao.setEntregamaterialIdEntrega(eds.getCdentregadocumentosincronizacao());
				if(itemBean != null){
					Double qtdevolvida = null;
					if(itemBean.getQtdedevolvida() != null){
						if(itemBean.getMaterial() != null){ 
								Material m  = materialService.loadWithMaterialMestre(itemBean.getMaterial());
								if(m != null && m.getMaterialmestregrade() != null){
									qtdevolvida = 1.;
								} else{
									qtdevolvida = itemBean.getQtdedevolvida();
								}
						} else{
							qtdevolvida = itemBean.getQtdedevolvida();
						}
						
					}
					if(qtdevolvida == null){
						qtdevolvida = 0.;
					}
					entregamaterialsincronizacao.setEntregamaterialQuantidade(qtdevolvida.intValue());
					entregamaterialsincronizacao.setEntregamaterialValorUnitario(itemBean.getPreco());
					if(itemBean.getLoteestoque() != null && itemBean.getLoteestoque().getNumero() != null){
						entregamaterialsincronizacao.setEntregamaterialLote(itemBean.getLoteestoque().getNumero());
					}
				}
				
				listaProdutos.add(entregamaterialsincronizacao);
			}
		}
				
		eds.setNotaEntradaId(eds.getCdentregadocumentosincronizacao());
		if(bean.getDtchegada() != null){
			eds.setNotaEntradaDataChegada(new Date(bean.getDtchegada().getTime()));
		} else {
			eds.setNotaEntradaDataChegada(SinedDateUtils.currentDate());
		}
		if(bean.getDtemissao() != null){
			eds.setNotaEntradaDataEmissao(new Date(bean.getDtemissao().getTime()));
		} else {
			eds.setNotaEntradaDataEmissao(SinedDateUtils.currentDate());
		}
		
		eds.setNotaEntradaDataLancamento(SinedDateUtils.currentDate());
		if(bean.getEmpresa() != null){
			bean.setEmpresa(empresaService.load(bean.getEmpresa(), "empresa.cnpj, empresa.cpf"));
			eds.setNotaEntradaDeposito(StringUtils.soNumero(bean.getEmpresa().getCpfCnpj()));
		} else
			eds.setNotaEntradaDeposito(StringUtils.soNumero(empresa.getCpfCnpj()));
		
		if(bean.getCliente() != null)
			eds.setNotaEntradaFornecedor(bean.getCliente().getCdpessoa());
		eds.setNotaEntradaNumero(bean.getNumero());
		if(bean.getPlacaveiculo() != null && !bean.getPlacaveiculo().equals("")){
			eds.setNotaEntradaPlacaVeiculo(bean.getPlacaveiculo().replace("-", ""));
		} else {
			eds.setNotaEntradaPlacaVeiculo("AAA9999");
		}
		eds.setNotaEntradaTipo("F");
		if(bean.getTransportadora() != null && !bean.getTransportadora().equals("")){
			eds.setNotaEntradaTransportadora(bean.getTransportadora());
		} else {
			eds.setNotaEntradaTransportadora("TRANSPORTADORA");
		}
		
		eds.setListaEntregamaterialsincronizacao(listaProdutos);		
		
		return eds;
		
	}	
}
