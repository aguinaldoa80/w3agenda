package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.ControleOrcamentoHistorico;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalOrcamentoMes;
import br.com.linkcom.sined.geral.bean.enumeration.ControleOrcamentoHistoricoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipolancamento;
import br.com.linkcom.sined.geral.dao.ControleorcamentoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ControleOrcamentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ControleOrcamentoItemBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class ControleorcamentoService extends GenericService<Controleorcamento>{
	
	private ControleorcamentoDAO controleorcamentoDAO;	
	private ContagerencialService contagerencialService;
	private ControleOrcamentoHistoricoService controleOrcamentoHistoricoService;
	
	public void setControleOrcamentoHistoricoService(ControleOrcamentoHistoricoService controleOrcamentoHistoricoService) {this.controleOrcamentoHistoricoService = controleOrcamentoHistoricoService;}
	public void setControleorcamentoDAO(ControleorcamentoDAO controleorcamentoDAO) {
		this.controleorcamentoDAO = controleorcamentoDAO;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}

	public List<Controleorcamento> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return controleorcamentoDAO.loadWithLista(whereIn, orderBy, asc);
	}

	/**
	 * M�todo para criar uma c�pia do Controle de Or�amento
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Controleorcamento criarCopia(Controleorcamento origem) {
		origem = loadForEntrada(origem);
		Controleorcamento copia = new Controleorcamento();
		
		copia.setEmpresa(origem.getEmpresa());
		copia.setDescricao(origem.getDescricao());
		copia.setSituacao(Controleorcamentosituacao.ABERTO);
		
		copia.setListaControleorcamentoitem(origem.getListaControleorcamentoitem());
		if(copia.getListaControleorcamentoitem() != null && !copia.getListaControleorcamentoitem().isEmpty()){
			for(Controleorcamentoitem controleorcamentoitem : copia.getListaControleorcamentoitem()){
				controleorcamentoitem.setCdcontroleorcamentoitem(null);
			}
		}
		return copia;		
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * busca os Controle de Or�amento para serem atualizados
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamento> findForAprovar(String whereIn) {
		return controleorcamentoDAO.findForAprovar(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca os Controle de Or�amento para serem estornados - altera��o de status
	 *
	 * @param whereIn
	 * @return
	 * @author C�sar
	 */
	public List<Controleorcamento> findForEstornar(String whereIn) {
		return controleorcamentoDAO.findForEstornar(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * atualiza situa��o do Controle de Or�amento
	 *
	 * @param controleorcamento
	 * @author Luiz Fernando
	 */
	public void atualizarSituacao(Controleorcamento controleorcamento){
		controleorcamentoDAO.atualizarSituacao(controleorcamento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamento> findSituacaoByWhereIn(String whereIn){
		return controleorcamentoDAO.findSituacaoByWhereIn(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ControleorcamentoDAO#findForControleorcamentoMes
	 *
	 * @param whereInContagerencial
	 * @param whereInCentrocusto 
	 * @param vencimento
	 * @param cdcontapagar
	 * @return
	 * @author Luiz Fernando
	 */
	public List<TotalOrcamentoMes> findForControleorcamentoMes(String whereInContagerencial, String whereInCentrocusto, Date vencimento, Integer cdcontapagar, Integer cdempresa){
		return controleorcamentoDAO.findForControleorcamentoMes(whereInContagerencial, whereInCentrocusto, vencimento, cdcontapagar, cdempresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ControleorcamentoDAO#findForAnaliseRecDesp(int anoDe, int anoAte, String whereInEmpresa, String whereInCentrocusto)
	 *
	 * @param anoDe
	 * @param anoAte
	 * @param whereInEmpresa
	 * @param whereInCentrocusto
	 * @param whereInProjeto
	 * @return
	 * @since 10/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Controleorcamento> findForAnaliseRecDesp(Date anoDe,Date anoAte, String whereInEmpresa, String whereInCentrocusto, String whereInProjeto, String whereInProjetotipo) {
		return controleorcamentoDAO.findForAnaliseRecDesp(anoDe, anoAte, whereInEmpresa, whereInCentrocusto, whereInProjeto, whereInProjetotipo);
	}

	public Double getValorOrcamento(String identificador, int ano, int mes, List<Controleorcamento> listaOrcamentos) {
		double valor = 0.0;
		mes++;
		String mesano = (mes > 9 ? mes : ("0" + mes)) + "/" + ano;
		if(listaOrcamentos != null){
			for (Controleorcamento controleorcamento : listaOrcamentos) {
				if((Tipolancamento.ANUAL.equals(controleorcamento.getTipolancamento()) /*&& controleorcamento.getExercicio().equals(ano)*/ || 
						Tipolancamento.MENSAL.equals(controleorcamento.getTipolancamento()) )){
					List<Controleorcamentoitem> listaControleorcamentoitem = controleorcamento.getListaControleorcamentoitem();
					if(listaControleorcamentoitem != null){
						for (Controleorcamentoitem controleorcamentoitem : listaControleorcamentoitem) {
							if(controleorcamentoitem.getContagerencial().getVcontagerencial().getIdentificador().equals(identificador)){
								if(Tipolancamento.MENSAL.equals(controleorcamento.getTipolancamento())){
									if(controleorcamentoitem.getMesano() != null && mesano.equals(controleorcamentoitem.getMesanoAux())){
										valor += controleorcamentoitem.getValor().getValue().doubleValue();
									}
								}else {
									valor += controleorcamentoitem.getValor().getValue().doubleValue()/controleorcamento.getQtnMesesExercicio();
								}
							}
						}
					}
				}
			}
		}
		return valor;
	}
	
	/**
	 * M�todo que busca os dados que alimentar�o o relat�rio pdf de controle de or�amento
	 * 
	 * @param filtro
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Controleorcamento> findForReport(ControleorcamentoFiltro filtro){
		return controleorcamentoDAO.findForReport(filtro);
	}
	
	/**
	 * M�todo que gera o relat�rio de controle de or�amento 
	 * 
	 * @param filtro
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public IReport gerarRelatorioControleOrcamento(ControleorcamentoFiltro filtro){
		Report report = new Report("/financeiro/controleOrcamento");

		List<Contagerencial> listaCompleta = contagerencialService.findListaCompleta();
		List<Controleorcamento> listaCO = this.findForReport(filtro);
		
		List<ControleOrcamentoBean> dataSource = ajustaControleOrcamentoBean(filtro, listaCO, listaCompleta);
		report.setDataSource(dataSource);
		String strProjeto = "";
		if(filtro.getProjeto() != null && ProjetoService.getInstance().load(filtro.getProjeto()).getNome() !=null){
			strProjeto = ProjetoService.getInstance().load(filtro.getProjeto()).getNome();
		}else if (filtro.getProjetoTipo() != null && ProjetotipoService.getInstance().load(filtro.getProjetoTipo()).getNome() !=null){
			strProjeto = ProjetotipoService.getInstance().load(filtro.getProjetoTipo()).getNome();
		}
		String ano = "";
		if(filtro.getDtInicio() != null && filtro.getDtFim()!= null){
			ano = filtro.getDtInicio() + " at� " + filtro.getDtFim();
		}else if (filtro.getDtInicio() != null ){
			ano = "de" + filtro.getDtInicio(); 
		}else if (filtro.getDtFim()!= null){
			ano = "at� "+filtro.getDtFim();
		}
		if(filtro.getEmpresa() != null)
			report.addParameter("empresaFiltro", EmpresaService.getInstance().load(filtro.getEmpresa()).getRazaosocialOuNome());
		if(filtro.getCentrocusto() != null)
			report.addParameter("centroCusto", CentrocustoService.getInstance().load(filtro.getCentrocusto()).getNome());
		if(strProjeto != null && !strProjeto.equals(""))
			report.addParameter("projeto",strProjeto);
		if(filtro.getContagerencial() != null)
			report.addParameter("contaGerencial", contagerencialService.load(filtro.getContagerencial()).getNome());
		if(ano != null && !ano.equals(""))
			report.addParameter("ano", ano );
		if(filtro.getDescricao() != null && !filtro.getDescricao().isEmpty())
			report.addParameter("descricao", filtro.getDescricao());
		
		return report;
	}
	
	/**
	 * M�todo que ajusta uma lista de controle de or�amento para um bean padr�o para ser utilizado no relat�rio
	 * 
	 * @param listaCO
	 * @return
	 * 
	 * @author Rafael Salvio
	 * @param filtro 
	 */
	public List<ControleOrcamentoBean> ajustaControleOrcamentoBean(ControleorcamentoFiltro filtro, List<Controleorcamento> listaCO, List<Contagerencial> listaCompleta){
		Map<ControleOrcamentoBean, Map<Contagerencial, List<ControleOrcamentoItemBean>>> mapa = new HashMap<ControleOrcamentoBean, Map<Contagerencial, List<ControleOrcamentoItemBean>>>();
		
		if(listaCO != null && !listaCO.isEmpty()){
			for(Controleorcamento co : listaCO){
				if(co.getListaControleorcamentoitem() != null && !co.getListaControleorcamentoitem().isEmpty()){
					for(Controleorcamentoitem coi : co.getListaControleorcamentoitem()){
						ControleOrcamentoBean bean = new ControleOrcamentoBean();
						bean.setAno(co.getDtOrcamento());
						bean.setAnoInicio(co.getDtExercicioInicio());
						bean.setAnoFim(co.getDtExercicioFim());
						bean.setAnoFim(co.getDtExercicioFim());
						bean.setQtdMes(co.getQtnMesesExercicio());
						bean.setDescricao(co.getDescricao());
						bean.setCentrocusto(coi.getCentrocusto());
						bean.setProjeto(coi.getProjeto());
						bean.setProjetotipo(coi.getProjetoTipo());
						// Busca um controleorcamentobean cujo ano, centrocusto e projeto
						// sejam equivalentes ao item coi e repassa ao m�todo de preenchimento
						
						//O Metodo original foi comentado hoje 02/08/2019
						Map<Contagerencial, List<ControleOrcamentoItemBean>> itens = 
							preencheListaItens(mapa.get(bean), coi.getContagerencial(), coi.getMesanoAux(), coi.getValor(), bean);
						mapa.put(bean, itens);
						
						// Busca recursiva para preenchimento das contas gerencias 'pais'
//						if(coi.getContagerencial().getContagerencialpai() != null && coi.getContagerencial().getContagerencialpai().getCdcontagerencial() != null){
//							Integer indexCgPai = listaCompleta.indexOf(coi.getContagerencial().getContagerencialpai());
//							while(indexCgPai >= 0){
//								Contagerencial cgPai = listaCompleta.get(indexCgPai);
//								//O Metodo original foi comentado hoje 02/08/2019
//								itens =	preencheListaItens(mapa.get(bean), cgPai, coi.getMesano(), coi.getValor(), bean);
//								itens = preencheListaItens(mapa.get(bean), coi.getContagerencial(), coi.getMesano(), coi.getValor(), bean);
//								mapa.put(bean, itens);
//								
//								if(cgPai.getContagerencialpai() != null && cgPai.getContagerencialpai().getCdcontagerencial() != null){
//									indexCgPai = listaCompleta.indexOf(cgPai.getContagerencialpai());
//								} else{
//									indexCgPai = -1;
//								}
//							}
//						}
					
					}
				}
			}
		}
		
		// Transforma os mapas criados em uma listagem completa de bean para ser utilizada como DataSource.
		List<ControleOrcamentoBean> dataSource = new ListSet<ControleOrcamentoBean>(ControleOrcamentoBean.class);
		for(ControleOrcamentoBean bean : mapa.keySet()){
			if(bean.getListaItens() == null){
				bean.setListaItens(new ListSet<ControleOrcamentoItemBean>(ControleOrcamentoItemBean.class));
			}
			Map<Contagerencial, List<ControleOrcamentoItemBean>> itens = mapa.get(bean);
			for(Contagerencial cg : listaCompleta){
				List<ControleOrcamentoItemBean> lista = itens.get(cg);
				if(lista == null){
					lista = new ArrayList<ControleOrcamentoItemBean>();
					//Preenche os 12 meses referente ao item
					Calendar cal = Calendar.getInstance();
					for(int i=0; -1 != bean.getAnoFim().compareTo(cal.getTime()) ; i++){
						cal.setTime(bean.getAnoInicio());
						cal.add(Calendar.MONTH, i);	
						lista.add(new ControleOrcamentoItemBean(cg.getDescricaoInputPersonalizado(), new SimpleDateFormat("MM/yyyy").format(cal.getTime()), cal.getTime()));
					}
				}
				Collections.sort(lista,new Comparator<ControleOrcamentoItemBean>(){
					public int compare(ControleOrcamentoItemBean o1, ControleOrcamentoItemBean o2) {
						return o1.getData().compareTo(o2.getData());
					}
				});
				bean.getListaItens().addAll(lista);				
			}
			dataSource.add(bean);
		}
		
		if(filtro.getContaGerencialSemOrcamento() == null || !filtro.getContaGerencialSemOrcamento()){
			for (ControleOrcamentoBean controleOrcamentoBean : dataSource) {
				List<ControleOrcamentoItemBean> listaItens = controleOrcamentoBean.getListaItens();
				if(listaItens != null && listaItens.size() > 0){
					List<ControleOrcamentoItemBean> listaItensNovo = new ArrayList<ControleOrcamentoItemBean>();
					for (ControleOrcamentoItemBean controleOrcamentoItemBean : listaItens) {
						if(controleOrcamentoItemBean != null && controleOrcamentoItemBean.getValor() != null && controleOrcamentoItemBean.getValor().getValue().doubleValue() > 0){
							listaItensNovo.add(controleOrcamentoItemBean);
						}
					}
//					controleOrcamentoBean.setListaItens(listaItensNovo);
					Collections.sort(listaItensNovo,new Comparator<ControleOrcamentoItemBean>(){
						public int compare(ControleOrcamentoItemBean o1, ControleOrcamentoItemBean o2) {
							return o1.getData().compareTo(o2.getData());
						}
					});
					controleOrcamentoBean.setListaItens(listaItensNovo);
				}
			}
		}
		
		return dataSource;
	}
	

	
	private List<ControleOrcamentoItemBean> ordenarListaItens(List<ControleOrcamentoItemBean> list) {
		try{
			Collections.sort(list,new Comparator<ControleOrcamentoItemBean>(){
				public int compare(ControleOrcamentoItemBean o1, ControleOrcamentoItemBean o2) {
					return o1.getData().compareTo(o2.getData());
				}
			});
			return list;
		}catch (Exception e) {
			return list;
		}
	}
//	private Map<Contagerencial, List<ControleOrcamentoItemBean>> preencheListaItens(Map<Contagerencial, List<ControleOrcamentoItemBean>> itens, 
//			Contagerencial cg, Date mesano, Money valor, String ano){
//		Calendar cal = Calendar.getInstance();
//
//		if(itens == null){
//			itens = new HashMap<Contagerencial, List<ControleOrcamentoItemBean>>();
//		}
//		
//		// Busca a lista de controleorcamentoitembean cuja contagerencial seja equilavante ao item coi
//		List<ControleOrcamentoItemBean> lista = itens.get(cg);
//		if(lista == null){
//			lista = new ArrayList<ControleOrcamentoItemBean>(12);
//			//Preenche os 12 meses referente ao item
//			for(int i=1; i<=12; i++){
//				lista.add(new ControleOrcamentoItemBean(cg.getDescricaoInputPersonalizado(), (i < 10 ? "0"+i : i) +"/"+ano));
//			}
//		}
//		
//		// Caso tenha mes definido no coi, o valor ser� incrementado a este item
//		if(mesano != null){
//			cal.setTime(mesano);
//			Integer pos = cal.get(Calendar.MONTH);
//			if(pos >= 0 && pos < 12){
//				Money valorAux = lista.get(pos).getValor();
//				if(valorAux == null){
//					valorAux = new Money(valor);
//				} else{
//					valorAux = valorAux.add(valor);
//				}
//				
//				lista.get(pos).setValor(valorAux);
//			} 
//		//Caso contr�rio o valor � dividido entre os 12 meses do per�odo
//		} else{
//			Money valorDividido = valor.divide(new Money(12));
//			
//		
//			
//			for(int i=0; i<12; i++){
//				Money valorAux = lista.get(i).getValor();
//				if(valorAux == null){
//					valorAux = new Money(valorDividido);
//				} else{
//					valorAux = valorAux.add(valorDividido);
//				}
//				
//				lista.get(i).setValor(valorAux);
//			}
//		}
//		
//		itens.put(cg, lista);
//		return itens;
//	}

	
	private Map<Contagerencial, List<ControleOrcamentoItemBean>> preencheListaItens(Map<Contagerencial, List<ControleOrcamentoItemBean>> itens, Contagerencial cg, String mesano, Money valor, ControleOrcamentoBean bean){
		Calendar cal = Calendar.getInstance();

		if(itens == null){
			itens = new HashMap<Contagerencial, List<ControleOrcamentoItemBean>>();
		}
		
		// Busca a lista de controleorcamentoitembean cuja contagerencial seja equilavante ao item coi
		List<ControleOrcamentoItemBean> lista = itens.get(cg);
		if(lista == null){
			lista = new ArrayList<ControleOrcamentoItemBean>(bean.getQtdMes());
			//Preenche os 12 meses referente ao item
			Calendar calendario = Calendar.getInstance();
			for(int i=0; i < bean.getQtdMes(); i++){
				calendario.setTime(bean.getAnoInicio());
				calendario.add(Calendar.MONTH, i);	
				lista.add(new ControleOrcamentoItemBean(cg.getDescricaoInputPersonalizado(), new SimpleDateFormat("MM/yyyy").format(calendario.getTime()),calendario.getTime()));
			}
		}
		
		// Caso tenha mes definido no coi, o valor ser� incrementado a este item
		if(SinedUtil.isListNotEmpty(lista)){
			if(mesano != null){
				for (int i = 0; i < lista.size(); i++){
					if (mesano.equals(lista.get(i).getMesano())){
						Money valorAux = lista.get(i).getValor();
						if(valorAux == null){
							valorAux = new Money(valor);
						} else{
							valorAux = valorAux.add(valor);
						}
						lista.get(i).setValor(valorAux);						
					}
				}
				 
			//Caso contr�rio o valor � dividido entre os meses do per�odo
			} else{
				if(bean.getQtdMes() <= 0){
					bean.setQtdMes(1);
				}
				Money valorDividido = valor.divide(new Money(bean.getQtdMes()));
				for(int i=0; i<bean.getQtdMes(); i++){
					if(lista.size() > i){
						Money valorAux = lista.get(i).getValor();
						if(valorAux == null){
							valorAux = new Money(valorDividido);
						} else{
							valorAux = valorAux.add(valorDividido);
						}
						lista.get(i).setValor(valorAux);
					}
				}
			}
		}
		itens.put(cg, lista);
		return itens;
	}
	
	
	@Override
	protected ListagemResult<Controleorcamento> findForExportacao(
			FiltroListagem filtro) {
		ListagemResult<Controleorcamento> listagemResult = controleorcamentoDAO.findForExportacao(filtro);
		List<Controleorcamento> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontroleorcamento", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(controleorcamentoDAO.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}

		return listagemResult;
	}
	public void updateSituacaoAndSaveHistorico(Controleorcamentosituacao situacao,Integer cdControOrcamento,String obsParaHistroco){
		controleorcamentoDAO.updateSituacao(cdControOrcamento+"",situacao);
		
		ControleOrcamentoHistorico controleOrcamentoHistorico = new ControleOrcamentoHistorico();
		controleOrcamentoHistorico.setDataaltera(SinedDateUtils.currentTimestamp());
		controleOrcamentoHistorico.setResponsavel(SinedUtil.getUsuarioLogado().getNome());
		controleOrcamentoHistorico.setControleorcamento(new Controleorcamento(cdControOrcamento));
		controleOrcamentoHistorico.setObservacao(obsParaHistroco);
		Integer valorAcao = situacao.getValue();
		switch (valorAcao){
			case  0:
				controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.CRIADO);
				break;
			case 1:
				controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.ALTERADO);
				break;
			case 2:
				controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.REPLANEJADO);
				break;
			default: 
				throw new SinedException ("Ac�o N�o encontrada para registro de historico"); 
		}
		controleOrcamentoHistoricoService.saveOrUpdate(controleOrcamentoHistorico);
	}
	public List<Controleorcamento> findByEmpresaAndExercicio(Empresa empresa,Date exercicioInicio,Date exercicioFim,Integer cdControleOrcamento, Projeto projeto, Projetotipo projetoTipo) {
		return controleorcamentoDAO.findByEmpresaAndExercicio(empresa,exercicioInicio,exercicioFim,cdControleOrcamento, projeto, projetoTipo);
	}
	
	public Controleorcamento buscarValorFluxoCaixa(Controleorcamento controleorcamento) {
		return controleorcamentoDAO.buscarValorFluxoCaixa(controleorcamento);
	}
	
	public List<Controleorcamento> findOrcamentosForCsv(ControleorcamentoFiltro filtro) {
		return controleorcamentoDAO.findOrcamentosForCsv(filtro);
	}
	
	public Controleorcamento findOrcamentoById(Controleorcamento controleorcamento) {
		return controleorcamentoDAO.findOrcamentoById(controleorcamento);
	}
	
	public Controleorcamento loadOrcamentoForDateValidation(Controleorcamento controleorcamento) {
		return controleorcamentoDAO.loadOrcamentoForDateValidation(controleorcamento);
	}
	
	public boolean existeItemDataSuperiorOrcamento(Controleorcamento controleorcamento) {
		return controleorcamentoDAO.existeItemDataSuperiorOrcamento(controleorcamento);
	}
	
	
	
	
}

