package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentodespesa;
import br.com.linkcom.sined.geral.dao.PlanejamentodespesaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PlanejamentodespesaService extends GenericService<Planejamentodespesa>{

	private PlanejamentodespesaDAO planejamentodespesaDAO;
	
	public void setPlanejamentodespesaDAO(
			PlanejamentodespesaDAO planejamentodespesaDAO) {
		this.planejamentodespesaDAO = planejamentodespesaDAO;
	}
	
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentodespesaDAO#findForOrcamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentodespesa> findForOrcamento(Planejamento planejamento) {
		return planejamentodespesaDAO.findForOrcamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de refrÍncia a partir de uma lista de planejamentoRecursoGeral.
	 *
	 * @see PlanejamentodespesaService#findForOrcamento
	 * @see br.com.linkcom.sined.geral.bean.Valorreferencia#getValor
	 * @param filtro
	 * @param listaFolhas
	 * @param listaOutrosMaterial
	 * @param somamaterial
	 * @param listaValorReferencia
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money preencheListaVRPlanejamentoDespesa(OrcamentoAnaliticoReportFiltro filtro, List<AnaliseRecDespBean> listaFolhas,
			List<AnaliseRecDespBean> listaOutrosDespesa, Money somadespesa) {
		AnaliseRecDespBean bean;
		List<Planejamentodespesa> listaPlanejamentoDespesa = this.findForOrcamento(filtro.getPlanejamento());
		for (Planejamentodespesa planejamentodespesa : listaPlanejamentoDespesa) {
			bean = new AnaliseRecDespBean();
			if (planejamentodespesa.getContagerencial() != null) {
				bean.setIdentificador(planejamentodespesa.getContagerencial().getVcontagerencial().getIdentificador());
				bean.setNome(planejamentodespesa.getContagerencial().getNome());
				bean.setOperacao(planejamentodespesa.getContagerencial().getTipooperacao().getSigla());
				bean.setValor(planejamentodespesa.calculaTotal());
				
				listaFolhas.add(bean);
			} else {
				bean.setIdentificador("XX.XX.XX");
				bean.setNome(planejamentodespesa.getFornecimentotipo().getNome());
				bean.setOperacao("D");
				bean.setValor(planejamentodespesa.getTotal());
				somadespesa = somadespesa.add(planejamentodespesa.getTotal());
				
				listaOutrosDespesa.add(bean);
			}
		}
		return somadespesa;
	}

}
