package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulamargemcontribuicao;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao.FormulatabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao.OportunidadeVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao.TabelavalorVO;
import br.com.linkcom.sined.geral.dao.MaterialformulamargemcontribuicaoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialformulamargemcontribuicaoService extends GenericService<Materialformulamargemcontribuicao>{
	
	private MaterialformulamargemcontribuicaoDAO materialformulamargemcontribuicaoDAO;
	private MaterialService materialService;
	private TabelavalorService tabelavalorService;
	
	public void setMaterialformulamargemcontribuicaoDAO(MaterialformulamargemcontribuicaoDAO materialformulamargemcontribuicaoDAO) {
		this.materialformulamargemcontribuicaoDAO = materialformulamargemcontribuicaoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	
	/**
	 * M�todo calcula a margem de contribui��o
	 *
	 * @param material
	 * @return
	 * @throws ScriptException
	 * @author Luiz Fernando
	 * @since 24/06/2015
	 */
	public Double calculaMargemcontribuicaoMaterial(Material material, Oportunidadematerial oportunidadematerial) throws ScriptException {
		Double margemcontribuicao = 0d;
		if(material != null){
			if(material.getListaMaterialformulamargemcontribuicao() != null && !material.getListaMaterialformulamargemcontribuicao().isEmpty()){
				List<Materialformulamargemcontribuicao> listaMaterialformulavalorvenda = material.getListaMaterialformulamargemcontribuicao();
				
				Pattern pattern_material = Pattern.compile("material\\.[a-zA-Z]+\\(([0-9]+)\\)");
				List<String> idsMateriais = new ArrayList<String>();
				idsMateriais.add(material.getCdmaterial().toString());
				
				Pattern pattern_tabelavalor = Pattern.compile("tabelavalor\\.[a-zA-Z]+\\((['a-zA-Z0-9_']+)\\)");
				List<String> identificadoresTabelavalor = new ArrayList<String>();
				
				for (Materialformulamargemcontribuicao materialformulamargemcontribuicao : listaMaterialformulavalorvenda) {
					if(materialformulamargemcontribuicao.getOrdem() == null ||
							materialformulamargemcontribuicao.getIdentificador() == null ||
							materialformulamargemcontribuicao.getFormula() == null){
						return 0d;
					}else {
						Matcher m_material = pattern_material.matcher(materialformulamargemcontribuicao.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
						
						Matcher m_tabelavalor = pattern_tabelavalor.matcher(materialformulamargemcontribuicao.getFormula());
						while(m_tabelavalor.find()){
							identificadoresTabelavalor.add(m_tabelavalor.group(1));
						}
					}
				}
				
				FormulaVO formulaVO = new FormulaVO();
				
				if(idsMateriais.size() > 0){
					List<Material> listaMaterial = materialService.findForCalcularValorvenda(CollectionsUtil.concatenate(idsMateriais, ","));
					for (Material m : listaMaterial) {
						MaterialVO materialVO = new MaterialVO();
						materialVO.setCdmaterial(m.getCdmaterial());
						if(m.equals(material) && material.getValorcustoByCalculoProducao() != null){
							materialVO.setValorcusto(material.getValorcustoByCalculoProducao());
						}else {
							materialVO.setValorcusto(m.getValorcusto() != null ? m.getValorcusto() : 0.0);
						}
						materialVO.setValorvenda(m.getValorvenda() != null ? m.getValorvenda() : 0.0);
						formulaVO.addMaterial(materialVO);
						
						if(m.getCdmaterial().equals(material.getCdmaterial())){
							formulaVO.setAux_materialVO(materialVO);
							if(m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
								formulaVO.setCdgrupomaterial(m.getMaterialgrupo().getCdmaterialgrupo());
							}
							if(m.getOrigemproduto() != null){
								formulaVO.setOrigem(m.getOrigemproduto().ordinal());
							}
						}
					}
				}
				
				FormulatabelavalorVO formulatabelavalorVO = new FormulatabelavalorVO();
				if(identificadoresTabelavalor.size() > 0){
					List<Tabelavalor> listaTabelavalor = tabelavalorService.findForCalcularValorvenda(CollectionsUtil.concatenate(identificadoresTabelavalor, ","));
					for (Tabelavalor tabelavalor : listaTabelavalor) {
						formulatabelavalorVO.addTabelavalor(new TabelavalorVO(tabelavalor));
					}
				}
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				OportunidadeVO oportunidadeVO = new OportunidadeVO();
				oportunidadeVO.setRevendedor(material.getRevendedor() != null ? material.getRevendedor() : false);
				if(oportunidadematerial != null){
					oportunidadeVO.setQuantidade(oportunidadematerial.getQuantidade() != null ? oportunidadematerial.getQuantidade() : 0d);
					oportunidadeVO.setValorunitario(oportunidadematerial.getValorunitario() != null ? oportunidadematerial.getValorunitario() : 0d);
					oportunidadeVO.setComissao(oportunidadematerial.getComissao() != null ? oportunidadematerial.getComissao() : 0d);
				}
				
				engine.put("material", formulaVO);
				engine.put("tabelavalor", formulatabelavalorVO);
				engine.put("oportunidade", oportunidadeVO);
				
				Collections.sort(listaMaterialformulavalorvenda, new Comparator<Materialformulamargemcontribuicao>(){
					public int compare(Materialformulamargemcontribuicao o1, Materialformulamargemcontribuicao o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Materialformulamargemcontribuicao materialformulamargemcontribuicao : listaMaterialformulavalorvenda) {
					Object obj = engine.eval(materialformulamargemcontribuicao.getFormula());
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(materialformulamargemcontribuicao.getIdentificador(), resultado);
					margemcontribuicao = SinedUtil.roundByParametro(resultado);
				}
			}
		}
		
		return margemcontribuicao;
	}

	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialformulamargemcontribuicaoDAO#findByMaterial(Material material)
	*
	* @param material
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulamargemcontribuicao> findByMaterial(Material material) {
		return materialformulamargemcontribuicaoDAO.findByMaterial(material);
	}
	
	/**
	* M�todo que replica a formula de margem de contribui��o
	*
	* @param listaMaterialformulamargemcontribuicao
	* @param material
	* @since 06/07/2015
	* @author Luiz Fernando
	*/
	public void saveReplicarFormulamargemcontribuicao(List<Materialformulamargemcontribuicao> listaMaterialformulamargemcontribuicao, Material material) {
		if(material != null && material.getCdmaterial() != null && listaMaterialformulamargemcontribuicao != null && 
				!listaMaterialformulamargemcontribuicao.isEmpty()){
			if(material.getListaMaterialformulamargemcontribuicao() != null && !material.getListaMaterialformulamargemcontribuicao().isEmpty()){
				deleteFormulamargemcontribuicaoByMaterial(material);
			}
			for(Materialformulamargemcontribuicao materialformulamargemcontribuicao : listaMaterialformulamargemcontribuicao){
				materialformulamargemcontribuicao.setCdmaterialformulamargemcontribuicao(null);
				materialformulamargemcontribuicao.setMaterial(material);
				saveOrUpdate(materialformulamargemcontribuicao);
			}
		}
		
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialformulamargemcontribuicaoDAO#deleteFormulamargemcontribuicaoByMaterial(Material material)
	*
	* @param material
	* @since 06/07/2015
	* @author Luiz Fernando
	*/
	private void deleteFormulamargemcontribuicaoByMaterial(Material material) {
		materialformulamargemcontribuicaoDAO.deleteFormulamargemcontribuicaoByMaterial(material);
	}
}
