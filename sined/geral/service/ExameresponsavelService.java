package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.geral.dao.ExameresponsavelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExameresponsavelService extends GenericService<Exameresponsavel> {	
	
	private ExameresponsavelDAO exameresponsavelDAO;
	
	public void setExameresponsavelDAO(ExameresponsavelDAO exameresponsavelDAO) {
		this.exameresponsavelDAO = exameresponsavelDAO;
	}
	
	/* singleton */
	private static ExameresponsavelService instance;
	public static ExameresponsavelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExameresponsavelService.class);
		}
		return instance;
	}
	
	/**
	 * 
	 * @param campos
	 * @param orderBy
	 * @return
	 * @author Taidson
	 * @since 22/09/2010
	 */
	public List<Exameresponsavel> findAll(String campos, String orderBy) {
		return exameresponsavelDAO.findAll(campos, orderBy);
	}
}
