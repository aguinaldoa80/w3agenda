package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Dependenciacargocomposicao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DependenciacargocomposicaoService extends GenericService<Dependenciacargocomposicao>{
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos dispon�veis atrav�s do nome do cargo
	 *
	 * @see DependenciacargocomposicaoService#ordenaListaForFlex(List)
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargocomposicao> ordenaListaDepCargoDispForFlex(List<Dependenciacargocomposicao> listaDependenciaCargo){
		return ordenaListaForFlex(listaDependenciaCargo);
	}	
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos selecionados atrav�s do nome do cargo
	 *
	 * @see DependenciacargocomposicaoService#ordenaListaForFlex(List)
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargocomposicao> ordenaListaDepCargoSelForFlex(List<Dependenciacargocomposicao> listaDependenciaCargo){
		return ordenaListaForFlex(listaDependenciaCargo);
	}	
	
	/**
	 * Ordena a uma lista de depend�ncia entre cargos atrav�s do nome do cargo
	 *
	 * @param listaDependenciaCargo
	 * @return listaDependenciaCargo ordenada
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Dependenciacargocomposicao> ordenaListaForFlex(List<Dependenciacargocomposicao> listaDependenciaCargo){
		if (listaDependenciaCargo != null) {
			Collections.sort(listaDependenciaCargo, new Comparator<Dependenciacargocomposicao>() {
			    public int compare(Dependenciacargocomposicao c1, Dependenciacargocomposicao c2) {
			        return c1.getCargo().getNome().compareTo(c2.getCargo().getNome());
			    }
			});
		}
		return listaDependenciaCargo;
	}	
	
}
