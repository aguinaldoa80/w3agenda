package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.CfopDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CfopService extends GenericService<Cfop>{

	private CfopDAO cfopDAO;
	private FornecedorService fornecedorService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private PaisService paisService;
	
	public void setCfopDAO(CfopDAO cfopDAO) {this.cfopDAO = cfopDAO;}
	public FornecedorService getFornecedorService() {return fornecedorService;}
	public EmpresaService getEmpresaService() {return empresaService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setPaisService(PaisService paisService) {this.paisService = paisService;}
	public static CfopService instance;
	public static CfopService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CfopService.class);
		}
		return instance;
	}
	
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	public List<Cfop> findByTipoEscopo(Cfoptipo cfoptipo, Cfopescopo cfopescopo){
		return cfopDAO.findByTipoEscopo(cfoptipo, cfopescopo);
	}

	public Cfop findByCodigo(String codigo) {
		return cfopDAO.findByCodigo(codigo);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name carregarCfop
	 * @param cfop
	 * @return
	 * @return Cfop
	 * @author Thiago Augusto
	 * @date 05/04/2012
	 *
	 */
	public Cfop carregarCfop(Cfop cfop) {
		return cfopDAO.carregarCfop(cfop);
	}
	
	/**
	 * 
	 * M�todo que retorna o cfop do grupo material de acordo com o estado que a venda est� sendo realizada.
	 *
	 * @name findForNFProduto
	 * @param material
	 * @param enderecoTributacaoOrigem
	 * @param enderecoTributacaoDestino
	 * @return Cfop
	 * @author Marden Silva
	 * @date 11/04/2012
	 *
	 */
	public Cfop findForNFProduto(Material material, Endereco enderecoTributacaoOrigem, Endereco enderecoTributacaoDestino){
		if (material.getMaterialgrupo() != null && material.getMaterialgrupo().getCfop() != null){
			Cfop cfop = cfopDAO.carregarCfop(material.getMaterialgrupo().getCfop());
			if (enderecoTributacaoOrigem != null && enderecoTributacaoOrigem.getMunicipio() != null && enderecoTributacaoOrigem.getMunicipio().getUf() != null &&
				enderecoTributacaoDestino != null && enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null){
				String tipo = cfop.getCodigo().substring(0,1);
				String codigo = cfop.getCodigo().substring(1);				
				if (enderecoTributacaoOrigem.getMunicipio().getUf().getCduf().equals(enderecoTributacaoDestino.getMunicipio().getUf().getCduf()))
					tipo = tipo.replace("6", "5").replace("7", "5");
				else {
					if(enderecoTributacaoOrigem.getPais() == null || enderecoTributacaoOrigem.getPais().getCdpais() == null){
						enderecoTributacaoOrigem.setPais(Pais.BRASIL);
					}
					if(enderecoTributacaoDestino.getPais() == null || enderecoTributacaoDestino.getPais().getCdpais() == null){
						enderecoTributacaoDestino.setPais(Pais.BRASIL);
					}
					if(enderecoTributacaoOrigem.getPais() != null && enderecoTributacaoDestino.getPais() != null && 
							enderecoTributacaoOrigem.getPais().getCdpais() != null && enderecoTributacaoDestino.getPais().getCdpais() != null && 
							!enderecoTributacaoOrigem.getPais().getCdpais().equals(enderecoTributacaoDestino.getPais().getCdpais())){
						tipo = tipo.replace("6", "7").replace("5", "7");
					}else {
						tipo = tipo.replace("5", "6").replace("7", "6");
					}
				}
				cfop = cfopDAO.findByCodigo(tipo+codigo);
			}else if(enderecoTributacaoOrigem != null && enderecoTributacaoDestino != null){
				enderecoTributacaoOrigem.setPais(Pais.BRASIL);
				if(enderecoTributacaoDestino.getPais() != null && enderecoTributacaoOrigem.getPais() != null && 
						enderecoTributacaoDestino.getPais().getCdpais() != null && enderecoTributacaoOrigem.getPais().getCdpais() != null && 
						!enderecoTributacaoDestino.getPais().getCdpais().equals(enderecoTributacaoOrigem.getPais().getCdpais())){
					String tipo = cfop.getCodigo().substring(0,1);
					String codigo = cfop.getCodigo().substring(1);				
					if(enderecoTributacaoOrigem.getPais() != null && enderecoTributacaoDestino.getPais() != null && 
							enderecoTributacaoOrigem.getPais().getCdpais() != null && enderecoTributacaoDestino.getPais().getCdpais() != null && 
							!enderecoTributacaoOrigem.getPais().getCdpais().equals(enderecoTributacaoDestino.getPais().getCdpais())){
						tipo = tipo.replace("6", "7").replace("5", "7");
						cfop = cfopDAO.findByCodigo(tipo+codigo);
					}					
				}
			}
			return cfop;
		} else
			return null;
	}
	
	public Cfop findForEntradafiscal(Material material, Endereco enderecoTributacaoOrigem, Endereco enderecoTributacaoDestino){
		if (material.getMaterialgrupo() != null && material.getMaterialgrupo().getCfop() != null){
			Cfop cfop = cfopDAO.carregarCfop(material.getMaterialgrupo().getCfop());
			if (enderecoTributacaoOrigem != null && enderecoTributacaoOrigem.getMunicipio() != null && enderecoTributacaoOrigem.getMunicipio().getUf() != null &&
				enderecoTributacaoDestino != null && enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null){
				String tipo = cfop.getCodigo().substring(0,1);
				String codigo = cfop.getCodigo().substring(1);				
				if (enderecoTributacaoOrigem.getMunicipio().getUf().getCduf().equals(enderecoTributacaoDestino.getMunicipio().getUf().getCduf()))
					tipo = tipo.replace("2", "1");
				else {
					if(enderecoTributacaoOrigem.getPais() == null || enderecoTributacaoOrigem.getPais().getCdpais() == null){
						enderecoTributacaoOrigem.setPais(Pais.BRASIL);
					}
					if(enderecoTributacaoDestino.getPais() == null || enderecoTributacaoDestino.getPais().getCdpais() == null){
						enderecoTributacaoDestino.setPais(Pais.BRASIL);
					}
					if(enderecoTributacaoOrigem.getPais() != null && enderecoTributacaoDestino.getPais() != null && 
							enderecoTributacaoOrigem.getPais().getCdpais() != null && enderecoTributacaoDestino.getPais().getCdpais() != null && 
							!enderecoTributacaoOrigem.getPais().getCdpais().equals(enderecoTributacaoDestino.getPais().getCdpais())){
						tipo = tipo.replace("1", "3").replace("2", "3");
					}else {
						tipo = tipo.replace("3", "2").replace("1", "2");
					}
				}
				cfop = cfopDAO.findByCodigo(tipo+codigo);
			}else if(enderecoTributacaoOrigem != null && enderecoTributacaoDestino != null){
				enderecoTributacaoOrigem.setPais(Pais.BRASIL);
				if(enderecoTributacaoDestino.getPais() != null && enderecoTributacaoOrigem.getPais() != null && 
						enderecoTributacaoDestino.getPais().getCdpais() != null && enderecoTributacaoOrigem.getPais().getCdpais() != null && 
						!enderecoTributacaoDestino.getPais().getCdpais().equals(enderecoTributacaoOrigem.getPais().getCdpais())){
					String tipo = cfop.getCodigo().substring(0,1);
					String codigo = cfop.getCodigo().substring(1);				
					if(enderecoTributacaoOrigem.getPais() != null && enderecoTributacaoDestino.getPais() != null && 
							enderecoTributacaoOrigem.getPais().getCdpais() != null && enderecoTributacaoDestino.getPais().getCdpais() != null && 
							!enderecoTributacaoOrigem.getPais().getCdpais().equals(enderecoTributacaoDestino.getPais().getCdpais())){
						tipo = tipo.replace("1", "3").replace("2", "3");
						cfop = cfopDAO.findByCodigo(tipo+codigo);
					}					
				}
			}
			return cfop;
		} else
			return null;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name buscarReferenciasCFOP
	 * @param cfop
	 * @return
	 * @return Cfop
	 * @author Thiago Augusto
	 * @date 16/04/2012
	 *
	 */
	public Cfop buscarReferenciasCFOP(Cfop cfop){
		return cfopDAO.buscarReferenciasCFOP(cfop);
	}
	
	public Cfop verificaTrocaCfop(Cfop cfop){
		cfop = this.buscarReferenciasCFOP(cfop);
		if (cfop.getListacfoprelacionado() != null && cfop.getListacfoprelacionado().size() > 0){
			return cfop.getListacfoprelacionado().get(0).getCfopdestino();
		} else {
			return cfop;
		}
	}
	
	public List<Cfop> findAutoCompleteEntrada(String q){
		return cfopDAO.findAutoCompleteEntrada(q);
	}
	
	public List<Cfop> findAutoComplete(String q){
		return cfopDAO.findAutoComplete(q);
	}
	
	/**
	 * M�todo que verifica se o escopo do cfop est� de acordo com o endere�o do fornecedor e empresa 
	 *
	 * @param fornecedor
	 * @param empresa
	 * @param cfop
	 * @return
	 * @author Luiz Fernando
	 */
	public String verificaCfopescopo(Fornecedor fornecedor, Empresa empresa, Cfop cfop){
		String msg = "";
		if(cfop != null 
				&& cfop.getCdcfop() != null 
				&& fornecedor != null 
				&& fornecedor.getCdpessoa() != null 
				&& empresa != null 
				&& empresa.getCdpessoa() != null){
			
					Cfop beanCfop = this.loadForEntrada(cfop);
					Fornecedor beanFornecedor = fornecedorService.carregaFornecedor(fornecedor);
					Empresa beanEmpresa = empresaService.loadWithEndereco(empresa);
					Uf uffornecedor = null;
					Uf ufempresa = null;
					Endereco enderecofornecedor = null;
					Endereco enderecoempresa = null;
					
			if(beanFornecedor != null && beanFornecedor.getListaEndereco() != null && !beanFornecedor.getListaEndereco().isEmpty()){
				
						for (Endereco endereco : beanFornecedor.getListaEndereco()) {
						
							if (endereco.getEnderecotipo().equals(Enderecotipo.UNICO)) {
									enderecofornecedor = endereco;
								} else if(endereco.getEnderecotipo().equals(Enderecotipo.FATURAMENTO)){
									enderecofornecedor = endereco;
								} else if (endereco.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
									enderecofornecedor = endereco;
								}
						
						if (enderecofornecedor != null) {
							
							if(beanEmpresa != null && beanEmpresa.getListaEndereco() != null && !beanEmpresa.getListaEndereco().isEmpty()){
								
								enderecoempresa = beanEmpresa.getListaEndereco().iterator().next();
								
							} else {
								beanEmpresa = empresaService.loadPrincipalWithEndereco();
								if(empresa != null && empresa.getEndereco() != null){
									enderecoempresa = empresa.getEndereco();
									if(enderecoempresa != null && enderecoempresa.getCdendereco() != null){
										enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
									}
								}
							}
						
						if(enderecoempresa != null 
								&& enderecoempresa.getMunicipio() != null 
								&& enderecoempresa.getMunicipio().getUf() != null 
								&& enderecoempresa.getMunicipio().getUf().getCduf() != null 
								&& enderecofornecedor != null 
								&& enderecofornecedor.getMunicipio() != null 
								&& enderecofornecedor.getMunicipio().getUf() != null 
								&& enderecofornecedor.getMunicipio().getUf().getCduf() != null){
							
							uffornecedor = enderecofornecedor.getMunicipio().getUf();
							ufempresa = enderecoempresa.getMunicipio().getUf();
							
							enderecoempresa.setPais(Pais.BRASIL);
							
							if(beanCfop.getCfopescopo() != null && Cfopescopo.ESTADUAL.equals(beanCfop.getCfopescopo())){
								if (enderecoempresa.getPais() != null 
										&& enderecoempresa.getPais().getCdpais() != null 
										&& enderecofornecedor.getPais() != null 
										&& enderecofornecedor.getPais().getCdpais() != null 
										&& (!enderecofornecedor.getPais().getCdpais().equals(enderecoempresa.getPais().getCdpais()) 
										|| (uffornecedor.getCduf() != null && ufempresa.getCduf() != null && !uffornecedor.getCduf().equals(ufempresa.getCduf())) )) {
									
											msg = "N�o � permitido CFOP de escopo estadual para o fornecedor com UF diferente ao da empresa.";
								}
								
							} else if(beanCfop.getCfopescopo() != null && Cfopescopo.FORA_DO_ESTADO.equals(beanCfop.getCfopescopo())){
								
								if (enderecoempresa.getPais() != null 
										&& enderecoempresa.getPais().getCdpais() != null 
										&& enderecofornecedor.getPais() != null 
										&& enderecofornecedor.getPais().getCdpais() != null 
										&& (!enderecofornecedor.getPais().getCdpais().equals(enderecoempresa.getPais().getCdpais()) 
										|| (uffornecedor.getCduf() != null && ufempresa.getCduf() != null && uffornecedor.getCduf().equals(ufempresa.getCduf())) )) {
									
											msg = "N�o � permitido CFOP de escopo fora do estado para o fornecedor com UF igual a da empresa.";
								}
								
							} else if(beanCfop.getCfopescopo() != null && Cfopescopo.INTERNACIONAL.equals(beanCfop.getCfopescopo())){
								
								if (enderecoempresa.getPais() != null 
										&& enderecoempresa.getPais().getCdpais() != null 
										&& enderecofornecedor.getPais() != null 
										&& enderecofornecedor.getPais().getCdpais() != null 
										&& (enderecofornecedor.getPais().getCdpais().equals(enderecoempresa.getPais().getCdpais()) 
										|| uffornecedor.getSigla() != "EX")) {
									
											msg = "N�o � permitido CFOP de escopo dentro/fora do estado para o fornecedor com endere�o internacional.";
								}
							} 
						}
					}
				}
			}
		}
		return msg;
	}
	
	public Cfop getCfop(Endereco enderecoEmpresa, Endereco enderecoCliente, List<Cfop> listaCfop){
		if (enderecoEmpresa!=null && enderecoCliente!=null && listaCfop!=null && !listaCfop.isEmpty()){
			String prefixoCodigo = null;
			Cfopescopo cfopescopo = null;
			
			if (enderecoEmpresa!=null 
					&& enderecoEmpresa.getMunicipio()!=null 
					&& enderecoEmpresa.getMunicipio().getUf()!=null 
					&& enderecoEmpresa.getMunicipio().getUf().getCduf()!=null 
					&& enderecoCliente!=null 
					&& enderecoCliente.getMunicipio()!=null 
					&& enderecoCliente.getMunicipio().getUf()!=null 
					&& enderecoCliente.getMunicipio().getUf().getCduf()!=null
					&& enderecoEmpresa.getMunicipio().getUf().getCduf().equals(enderecoCliente.getMunicipio().getUf().getCduf())){
				prefixoCodigo = "5";
				cfopescopo = Cfopescopo.ESTADUAL;
			}else if (enderecoEmpresa!=null 
						&& enderecoEmpresa.getMunicipio()!=null 
						&& enderecoEmpresa.getMunicipio().getUf()!=null 
						&& enderecoEmpresa.getMunicipio().getUf().getCduf()!=null 
						&& enderecoCliente!=null 
						&& enderecoCliente.getMunicipio()!=null 
						&& enderecoCliente.getMunicipio().getUf()!=null 
						&& enderecoCliente.getMunicipio().getUf().getCduf()!=null
						&& !enderecoEmpresa.getMunicipio().getUf().getCduf().equals(enderecoCliente.getMunicipio().getUf().getCduf())){
				prefixoCodigo = "6";
				cfopescopo = Cfopescopo.FORA_DO_ESTADO;
			}else if (paisService.isInternacional(enderecoEmpresa.getPais(), enderecoCliente.getPais())){
				prefixoCodigo = "7";
				cfopescopo = Cfopescopo.INTERNACIONAL;
			}
			
			if (prefixoCodigo!=null){
				for (Cfop cfop: listaCfop){
					if (cfop.getCodigo()!=null && cfop.getCodigo().startsWith(prefixoCodigo)){
						return cfop;
					}
				}
			}
			
			if (cfopescopo!=null){
				for (Cfop cfop: listaCfop){
					if (cfopescopo.equals(cfop.getCfopescopo())){
						return cfop;
					}
				}
			}
		}
		
		return null;
	}
	
	
}