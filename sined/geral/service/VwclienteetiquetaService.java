package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwclienteetiqueta;
import br.com.linkcom.sined.geral.dao.VwclienteetiquetaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwclienteetiquetaService extends GenericService<Vwclienteetiqueta>{
	private VwclienteetiquetaDAO vwclienteetiquetaDAO;
	
	public void setVwclienteetiquetaDAO(VwclienteetiquetaDAO vwclienteetiquetaDAO) {
		this.vwclienteetiquetaDAO = vwclienteetiquetaDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 08/11/2010
	 */
	public List<Integer> novoWhereIn(String whereIn) {
		return vwclienteetiquetaDAO.novoWhereIn(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Integer> novoWhereInCdPessoa(String whereIn) {
		return vwclienteetiquetaDAO.novoWhereInCdPessoa(whereIn);
	}	
}
