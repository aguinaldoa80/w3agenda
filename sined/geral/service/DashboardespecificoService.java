package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Dashboardespecifico;
import br.com.linkcom.sined.geral.bean.Dashboardespecificopapel;
import br.com.linkcom.sined.geral.bean.Dashboardpapel;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.enumeration.CategoriaClienteLogin;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.menu.DashboardBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DashboardespecificoService extends GenericService<Dashboardespecifico> {

	private final static String LISTA_DASHBOARD_SESSION = "LISTA_DASHBOARD_SESSION"; 
	
	private UsuariopapelService usuariopapelService;
	
	public void setUsuariopapelService(UsuariopapelService usuariopapelService) {
		this.usuariopapelService = usuariopapelService;
	}
	
	/* singleton */
	private static DashboardespecificoService instance;
	public static DashboardespecificoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DashboardespecificoService.class);
		}
		return instance;
	}
	
	private Boolean havePermissionEspecifico(List<Papel> listaPapel, Dashboardespecifico dashboardespecifico){
		boolean permitido = false;
		
		if(listaPapel != null && listaPapel.size() > 0){
			for (Papel papel : listaPapel) {
				Set<Dashboardespecificopapel> listaDashboardespecificopapel = papel.getListaDashboardespecificopapel();
				if(listaDashboardespecificopapel != null && listaDashboardespecificopapel.size() > 0){
					for (Dashboardespecificopapel dashboardespecificopapel : listaDashboardespecificopapel) {
						if(dashboardespecifico.equals(dashboardespecificopapel.getDashboardespecifico())){
							permitido = true;
							break;
						}
					}
				}
				
				if(permitido) break;
			}
		}
		
		return permitido;
	}
	
	private Boolean havePermissionGeral(List<Papel> listaPapel, Integer cddashboard){
		boolean permitido = false;
		
		if(listaPapel != null && listaPapel.size() > 0){
			for (Papel papel : listaPapel) {
				Set<Dashboardpapel> listaDashboardpapel = papel.getListaDashboardpapel();
				if(listaDashboardpapel != null && listaDashboardpapel.size() > 0){
					for (Dashboardpapel dashboardpapel : listaDashboardpapel) {
						if(cddashboard.equals(dashboardpapel.getCddashboard())){
							permitido = true;
							break;
						}
					}
				}
				
				if(permitido) break;
			}
		}
		
		return permitido;
	}
	
	/**
	 * Busca todos os dashboards
	 *
	 * @return
	 * @since 02/04/2019
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<DashboardBean> findAllDashboard(boolean buscarDashboardGeral) {
		Object obj = NeoWeb.getRequestContext().getSession().getAttribute(LISTA_DASHBOARD_SESSION);
		List<Papel> listaPapel = usuariopapelService.carregaPapel(SinedUtil.getUsuarioLogado());
		
		if(obj == null){
			List<DashboardBean> listaDashboard = new ArrayList<DashboardBean>();
			
			if(buscarDashboardGeral){
				try {
					List<DashboardBean> listaDashboardW3controle = getDashboardInW3controle();
					for (DashboardBean dashboardBean : listaDashboardW3controle) {
						if(SinedUtil.isUsuarioLogadoAdministrador() || this.havePermissionGeral(listaPapel, dashboardBean.getCddashboard())){
							listaDashboard.add(dashboardBean);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					
					try {
						String nomeMaquina = "N�o encontrado.";
						try {  
							InetAddress localaddr = InetAddress.getLocalHost();  
							nomeMaquina = localaddr.getHostName();  
						} catch (UnknownHostException e3) {}  
						
						
						String url = "N�o encontrado";
						try {  
							url = SinedUtil.getUrlWithContext();
						} catch (Exception e3) {}
						
						
						StringWriter stringWriter = new StringWriter();
						e.printStackTrace(new PrintWriter(stringWriter));
						PedidovendaService.getInstance().enviarEmailErro("[W3ERP] Erro ao consultar lista de Dashboard.","Ocorreu um erro JSON de entidade. (M�quina: " + nomeMaquina + ", URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
					} catch (Exception e2) {
						e.printStackTrace();
					}
				}
			}
			
			List<Dashboardespecifico> listaDashboardespecifico = this.findAll();
			for (Dashboardespecifico dashboardespecifico : listaDashboardespecifico) {
				if(SinedUtil.isUsuarioLogadoAdministrador() || this.havePermissionEspecifico(listaPapel, dashboardespecifico)){
					DashboardBean bean = new DashboardBean();
					bean.setCddashboard(dashboardespecifico.getCddashboardespecifico());
					bean.setNome(dashboardespecifico.getNome());
					bean.setUrl(dashboardespecifico.getUrl());
					bean.setId(dashboardespecifico.getId());
					bean.setUsuariosistema(dashboardespecifico.getUsuariosistema());
					bean.setSenhasistema(dashboardespecifico.getSenhasistema());
					bean.setGeral(Boolean.FALSE);
					listaDashboard.add(bean);
				}
			}
			
			NeoWeb.getRequestContext().getSession().setAttribute(LISTA_DASHBOARD_SESSION, listaDashboard);
			return listaDashboard;
		} else {
			return (List<DashboardBean>) obj;
		}
	}
	
	public List<DashboardBean> getDashboardInW3controle() throws NamingException, IOException, JDOMException {
		CategoriaClienteLogin categoriaCliente = ControleAcessoUtil.util.getCategoriaCliente();
		
		URL url = montaURLRequisicao();
		String data = categoriaCliente != null ? ("categoriaClienteLogin=" + categoriaCliente.name()) : "";
		
		URLConnection conn = url.openConnection(); 
		conn.setDoOutput(true); 
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
		wr.write(data); 
		wr.flush(); 
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		String line; 
		String xml = ""; 
		
		while ((line = rd.readLine()) != null) { xml += line; } 
		
		wr.close(); 
		rd.close(); 
		
		return verificaXmlDashboard(xml);
	}
	
	@SuppressWarnings("unchecked")
	private List<DashboardBean> verificaXmlDashboard(String xml) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(Util.strings.tiraAcento(xml).getBytes());
		
		if(rootElement != null && rootElement.getContent().size() > 0){
			List<DashboardBean> listaDashboard = new ArrayList<DashboardBean>();
			List<Element> listaDashboardElement = SinedUtil.getListChildElement("dashboard", rootElement.getContent());
			
			for (Element dashboardElement : listaDashboardElement) {
				Element nomeElement = SinedUtil.getChildElement("nome", dashboardElement.getContent());
				Element cddashboardElement = SinedUtil.getChildElement("cddashboard", dashboardElement.getContent());
				Element urlElement = SinedUtil.getChildElement("url", dashboardElement.getContent());
				Element idElement = SinedUtil.getChildElement("id", dashboardElement.getContent());
				Element usuariosistemaElement = SinedUtil.getChildElement("usuariosistema", dashboardElement.getContent());
				Element senhasistemaElement = SinedUtil.getChildElement("senhasistema", dashboardElement.getContent());
				
				DashboardBean dashboardBean = new DashboardBean();
				dashboardBean.setCddashboard(SinedUtil.getElementInteger(cddashboardElement));
				dashboardBean.setNome(SinedUtil.getElementString(nomeElement));
				dashboardBean.setUrl(SinedUtil.getElementString(urlElement));
				dashboardBean.setId(SinedUtil.getElementInteger(idElement));
				dashboardBean.setUsuariosistema(SinedUtil.getElementString(usuariosistemaElement));
				dashboardBean.setSenhasistema(SinedUtil.getElementString(senhasistemaElement));
				dashboardBean.setGeral(Boolean.TRUE);
				listaDashboard.add(dashboardBean);
			}
			
			return listaDashboard;
		}
		
		return new ArrayList<DashboardBean>();
	}
	
	private URL montaURLRequisicao() throws MalformedURLException, NamingException{
		String urlString = "linkcom.w3erp.com.br";
		
		if(SinedUtil.isAmbienteDesenvolvimento()){
			urlString = InitialContext.doLookup("W3CONTROLE_URL");
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o dos dashboards.");
			}
		}
		
		return new URL("http://" + urlString + "/w3controle/pub/process/Dashboard");
	}

}
