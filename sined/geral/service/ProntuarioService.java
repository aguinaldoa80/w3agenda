package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Prontuario;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProntuarioService extends GenericService<Prontuario> {

	private static ProntuarioService instance;
	
	public static ProntuarioService getInstance() {
		if (instance==null){
			instance = Neo.getObject(ProntuarioService.class);
		}
		return instance;
	}	
}
