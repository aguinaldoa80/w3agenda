package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.ibm.icu.text.DecimalFormat;

import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAuxiliar;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaDocumentoacao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaFormaPagamento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.BancoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.ContaBancariaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.DocumentoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.GeralVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.RemessaVO;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaInstrucaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRemessaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaPagamentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bancoconfiguracao.BancoConfiguracaoRemessaUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.StringUtils;

public class BancoConfiguracaoRemessaService extends GenericService<BancoConfiguracaoRemessa>{
	private static BancoConfiguracaoRemessaService instance;
	public static BancoConfiguracaoRemessaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(BancoConfiguracaoRemessaService.class);
		}
		return instance;
	}
	
	private BancoConfiguracaoRemessaDAO bancoConfiguracaoRemessaDAO;
	private DocumentoService documentoService;
	private ContaService contaService;
	private EmpresaService empresaService;
	private BancoService bancoService;
	private BancoConfiguracaoRemessaInstrucaoService bancoConfiguracaoRemessaInstrucaoService;
	private BancoConfiguracaoRemessaSegmentoService bancoConfiguracaoRemessaSegmentoService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	private ArquivobancarioService arquivobancarioService;
	private BancotipopagamentoService bancotipopagamentoService;
	private BancoformapagamentoService bancoformapagamentoService;
	private BancoConfiguracaoRemessaAuxiliarService bancoConfiguracaoRemessaAuxiliarService;
	private BancoConfiguracaoRemessaDocumentoacaoService bancoConfiguracaoRemessaDocumentoacaoService;
	
	public void setBancoConfiguracaoRemessaDAO(BancoConfiguracaoRemessaDAO bancoConfiguracaoRemessaDAO) {this.bancoConfiguracaoRemessaDAO = bancoConfiguracaoRemessaDAO;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setBancoConfiguracaoRemessaInstrucaoService(BancoConfiguracaoRemessaInstrucaoService bancoConfiguracaoRemessaInstrucaoService) {this.bancoConfiguracaoRemessaInstrucaoService = bancoConfiguracaoRemessaInstrucaoService;}
	public void setBancoConfiguracaoRemessaSegmentoService(BancoConfiguracaoRemessaSegmentoService bancoConfiguracaoRemessaSegmentoService) {this.bancoConfiguracaoRemessaSegmentoService = bancoConfiguracaoRemessaSegmentoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setBancoformapagamentoService(BancoformapagamentoService bancoformapagamentoService) {this.bancoformapagamentoService = bancoformapagamentoService;}
	public void setBancotipopagamentoService(BancotipopagamentoService bancotipopagamentoService) {this.bancotipopagamentoService = bancotipopagamentoService;}
	public void setBancoConfiguracaoRemessaAuxiliarService(BancoConfiguracaoRemessaAuxiliarService bancoConfiguracaoRemessaAuxiliarService) {this.bancoConfiguracaoRemessaAuxiliarService = bancoConfiguracaoRemessaAuxiliarService;}
	public void setBancoConfiguracaoRemessaDocumentoacaoService(BancoConfiguracaoRemessaDocumentoacaoService bancoConfiguracaoRemessaDocumentoacaoService) {this.bancoConfiguracaoRemessaDocumentoacaoService = bancoConfiguracaoRemessaDocumentoacaoService;}
	
	public BancoConfiguracaoRemessa findByBancoAndTipo(Banco banco, BancoConfiguracaoRemessaTipoEnum tipo){
		return bancoConfiguracaoRemessaDAO.findByBancoAndTipo(banco, tipo);
	}
	
	public ModelAndView gerarArquivoRemessa(ArquivoConfiguravelRemessaBean filtro) throws Exception {	
		Conta conta = contaService.carregaConta(filtro.getConta(), filtro.getContacarteira());
		Contacarteira contacarteira = conta.getContacarteira();
		
		if (contacarteira.getBancoConfiguracaoRemessa() == null || (contacarteira.getBancoConfiguracaoRemessa() != null && contacarteira.getBancoConfiguracaoRemessa().getCdbancoconfiguracaoremessa() == null))
			throw new SinedException("A carteira da conta banc�ria n�o possui arquivo de remessa configur�vel.");		
		
		List<Documento> listaDocumento = documentoService.findDocumentosForRemessaConfiguravel(filtro.getCds());
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			for(Documento documento : listaDocumento){
				if(filtro.getConta() != null && !filtro.getConta().equals(documento.getConta())){ 
					throw new SinedException("N�o foi poss�vel gerar o arquivo de remessa. A listagem de documento � diferente do filtro. Favor gerar novamente.");	
				}
				if(filtro.getContacarteira() != null && filtro.getContacarteira().getCdcontacarteira() != null && documento.getContacarteira() != null && 
						!filtro.getContacarteira().getCdcontacarteira().equals(documento.getContacarteira().getCdcontacarteira())){
					throw new SinedException("N�o foi poss�vel gerar o arquivo de remessa. A listagem de documento � diferente do filtro. Favor gerar novamente.");	
				}
			}
		}
		HashMap<MessageType, String> mapaValidacaoLimiteNossoNumero = documentoService.validaLimiteSequencialNossoNum(listaDocumento, contacarteira);
		if(mapaValidacaoLimiteNossoNumero.containsKey(MessageType.ERROR)){
			throw new SinedException(mapaValidacaoLimiteNossoNumero.get(MessageType.ERROR));
		}
		BancoVO bancoVO = bancoService.getBancoForRemessaConfiguravel(conta.getBanco());
		ContaBancariaVO contaBancariaVO = contaService.getContaForRemessaConfiguravel(conta, contacarteira);
		EmpresaVO empresaVO = filtro.getEmpresa() != null ? empresaService.getEmpresaForRemessaConfiguravel(filtro.getEmpresa()) : new EmpresaVO();
		EmpresaVO empresatitularVO = conta.getEmpresatitular() != null ? empresaService.getEmpresaForRemessaConfiguravel(conta.getEmpresatitular()) : null;
		List<DocumentoVO> listaDocumentoVO = documentoService.getListaDocumentoForRemessaConfiguravel(listaDocumento, null, false);
		GeralVO geralVO = new GeralVO();			
		RemessaVO remessaVO = new RemessaVO();
		
		if (filtro.getInstrucaoCobranca() != null){
			BancoConfiguracaoRemessaInstrucao instrucao = bancoConfiguracaoRemessaInstrucaoService.load(filtro.getInstrucaoCobranca());
			geralVO.setInstrucaocobrancacodigo(instrucao.getCodigo());
			geralVO.setInstrucaocobrancanome(instrucao.getNome());
		}		
		
		if (conta.getSequencial() != null){
			geralVO.setSequencialarquivo(conta.getSequencial());
			remessaVO.setUtilizouSequencial(true);
		}
		
		remessaVO.setGeralVO(geralVO);
		remessaVO.setBancoVO(bancoVO);
		remessaVO.setContaBancariaVO(contaBancariaVO);
		remessaVO.setEmpresaVO(empresaVO);
		remessaVO.setEmpresatitularVO(empresatitularVO);
		remessaVO.setListaDocumentoVO(listaDocumentoVO);
		
		List<BancoConfiguracaoRemessaSegmento> segmentos = bancoConfiguracaoRemessaSegmentoService.findAllByBancoConfiguracaoRemessa(contacarteira.getBancoConfiguracaoRemessa());

		BancoConfiguracaoRemessaUtil bancoConfiguracaoRemessaUtil = new BancoConfiguracaoRemessaUtil();
		String arquivo = bancoConfiguracaoRemessaUtil.gerarDadosArquivoRemessaConfiguravel(segmentos, remessaVO);
		
		if (remessaVO.getListaErro() != null && remessaVO.getListaErro().size() > 0){
			StringBuilder erros = new StringBuilder();
			for (String erro : remessaVO.getListaErro()){
				erros.append(erro).append("; ");
			}	
			throw new SinedException(erros.toString());
		}
		
		// Cria o arquivo texto
//		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		SimpleDateFormat format = new SimpleDateFormat("ddMM");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
//		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String nomeArq = getNomeArquivoRemessaConfiguravel(conta);
//		String nomeArq = "CB"+data+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		
		if (remessaVO.isUtilizouSequencial())
			contaService.setProximoSequencial(conta);
		
		//Remove o status de rejeitado quando o documento � gerado novamente em outra remessa
		for (Documento doc : listaDocumento){
			if (doc.getRejeitadobanco() != null && doc.getRejeitadobanco()){
				doc.setRejeitadobanco(Boolean.FALSE);
				documentoService.updateRejeitado(doc);
			}
		}
		
		boolean cancelamentocobranca = false;
		
		if (filtro.getInstrucaoCobranca()!=null){
			BancoConfiguracaoRemessaInstrucao bancoConfiguracaoRemessaInstrucao = bancoConfiguracaoRemessaInstrucaoService.load(filtro.getInstrucaoCobranca());
			cancelamentocobranca = BancoConfiguracaoRemessaInstrucaoEnum.CANCELAR_COBRANCA.equals(bancoConfiguracaoRemessaInstrucao.getInstrucao());		
		}
		
		documentoService.updateCancelamentocobranca(CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ","), cancelamentocobranca);
		
		return resourceModelAndView;			
	}
	
	public Arquivo gerarArquivoRemessaSispag(BaixarContaBean baixarContaBean) throws Exception {
		
		Arquivo arquivoRemessa = null;
		Conta conta = contaService.carregaConta(baixarContaBean.getVinculo(), null);
		Contacarteira contacarteira = conta.getContacarteira();

		if (baixarContaBean.getBancoformapagamento() == null || baixarContaBean.getBancotipopagamento() == null)
			throw new SinedException("� obrigat�rio informar a forma e tipo de pagamento para gera��o do arquivo de remessa.");
		
		BancoConfiguracaoRemessa bancoConfiguracaoRemessa = this.findByBancoAndTipo(conta.getBanco(), BancoConfiguracaoRemessaTipoEnum.PAGAMENTO);		
		if (bancoConfiguracaoRemessa == null)
			throw new SinedException("N�o existe arquivo de remessa configurado para a forma e tipo de pagamento informado.");
					
		String cds = CollectionsUtil.listAndConcatenate(baixarContaBean.getListaDocumento(), "cddocumento", ",");	
		
		List<Documento> listaDocumento = documentoService.findDocumentosForRemessaConfiguravel(cds);			
		BancoVO bancoVO = bancoService.getBancoForRemessaConfiguravel(conta.getBanco());
		ContaBancariaVO contaBancariaVO = contaService.getContaForRemessaConfiguravel(conta, contacarteira);
		EmpresaVO empresaVO = empresaService.getEmpresaForRemessaConfiguravel(conta.getEmpresa());
		List<DocumentoVO> listaDocumentoVO = documentoService.getListaDocumentoForRemessaConfiguravel(listaDocumento, baixarContaBean.getDtpagamento(), false);
		GeralVO geralVO = new GeralVO();			
		RemessaVO remessaVO = new RemessaVO();
		
		if (conta.getSequencial() != null){
			geralVO.setSequencialarquivo(conta.getSequencial());
			remessaVO.setUtilizouSequencial(true);
		}
		
		geralVO.setDatapagamento(baixarContaBean.getDtpagamento());
		if (baixarContaBean.getBancotipopagamento() != null){
			BancoTipoPagamento bancoTipoPagamento = bancotipopagamentoService.load(baixarContaBean.getBancotipopagamento());
			if(bancoTipoPagamento != null){
				geralVO.setTipopagamento(bancoTipoPagamento.getIdentificador());
			}
		}
		if (baixarContaBean.getBancoformapagamento() != null){
			BancoFormapagamento bancoFormapagamento = bancoformapagamentoService.load(baixarContaBean.getBancoformapagamento());
			if(bancoFormapagamento != null){
				geralVO.setFormapagamento(bancoFormapagamento.getIdentificador());
			}
		}
		if (baixarContaBean.getFinalidadedoc() != null)
			geralVO.setFinalidadedoc(baixarContaBean.getFinalidadedoc().getCodigo());
		if (baixarContaBean.getFinalidadeted() != null)
			geralVO.setFinalidadeted(baixarContaBean.getFinalidadeted().getCodigo());
		
		if(SinedUtil.isListNotEmpty(listaDocumentoVO)){
			String documentotipo = null;
			String tipotributo = null;
			for(DocumentoVO documentoVO : listaDocumentoVO){
				if(org.apache.commons.lang.StringUtils.isNotBlank(documentoVO.getDocumentotipo())){
					if(documentotipo == null){
						documentotipo = documentoVO.getDocumentotipo();
						tipotributo = documentoVO.getTipotributo();
					}else if(!documentotipo.equals(documentoVO.getDocumentotipo())){
						documentotipo = null;
						tipotributo = null;
					}
				}
			}
			if(documentotipo != null){
				geralVO.setTipodocumento(documentotipo);
				geralVO.setTipotributo(tipotributo);
			}
		}
		
		remessaVO.setGeralVO(geralVO);
		remessaVO.setBancoVO(bancoVO);
		remessaVO.setContaBancariaVO(contaBancariaVO);
		remessaVO.setEmpresaVO(empresaVO);
		remessaVO.setListaDocumentoVO(listaDocumentoVO);
		
		List<BancoConfiguracaoRemessaSegmento> segmentos = bancoConfiguracaoRemessaSegmentoService.findAllByBancoConfiguracaoRemessa(bancoConfiguracaoRemessa);

		BancoConfiguracaoRemessaUtil bancoConfiguracaoRemessaUtil = new BancoConfiguracaoRemessaUtil();
		String arquivo = bancoConfiguracaoRemessaUtil.gerarDadosArquivoRemessaConfiguravel(segmentos, remessaVO);
		
		if (remessaVO.getListaErro() != null && remessaVO.getListaErro().size() > 0){
			StringBuilder erros = new StringBuilder();
			for (String erro : remessaVO.getListaErro()){
				erros.append(erro).append("; ");
			}	
			throw new SinedException(erros.toString());
		}
		
		// Cria o arquivo texto
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
//		String nomeArq = "CB"+data+".REM";
		
		arquivoRemessa = new Arquivo(arquivo.getBytes(), nomeArq , "text/plain");
		Arquivobancario arquivobancario = new Arquivobancario(nomeArq, Arquivobancariotipo.REMESSA, arquivoRemessa, new Date(System.currentTimeMillis()),Arquivobancariosituacao.GERADO);
		
		arquivoDAO.saveFile(arquivobancario, "arquivo");
		arquivoService.saveOrUpdate(arquivoRemessa);
		arquivobancarioService.saveOrUpdate(arquivobancario);
		
		if (remessaVO.isUtilizouSequencial())
			contaService.setProximoSequencial(conta);
		
		return arquivoRemessa;
	}
	
	public List<BancoConfiguracaoRemessa> loadRemessasForPagamento(ArquivoConfiguravelRemessaPagamentoBean filtro){
		return bancoConfiguracaoRemessaDAO.loadRemessasForPagamento(filtro);
	}
	
	public ModelAndView gerarArquivoRemessaPagamento(ArquivoConfiguravelRemessaPagamentoBean filtro) throws Exception {	
		if (filtro.getBancoConfiguracaoRemessa() == null){
			throw new SinedException("O arquivo de remessa configur�vel n�o foi localizado.");
		}
		
		Conta conta = contaService.carregaConta(filtro.getConta());
		Contacarteira contacarteira = conta.getContacarteira();
		BancoConfiguracaoRemessa bancoConfiguracaoRemessa = bancoConfiguracaoRemessaDAO.loadForEntrada(filtro.getBancoConfiguracaoRemessa());
		
		List<Documento> listaDocumento = documentoService.findDocumentosForRemessaConfiguravel(filtro.getCds());			
		BancoVO bancoVO = bancoService.getBancoForRemessaConfiguravel(conta.getBanco());
		ContaBancariaVO contaBancariaVO = contaService.getContaForRemessaConfiguravel(conta, contacarteira);
		EmpresaVO empresaVO = empresaService.getEmpresaForRemessaConfiguravel(filtro.getEmpresa());
		GeralVO geralVO = new GeralVO();			
		RemessaVO remessaVO = new RemessaVO();
		
		if (filtro.getInstrucaoPagamento() != null){
			if(bancoConfiguracaoRemessa.getListaBancoConfiguracaoRemessaInstrucao() != null){
				for(BancoConfiguracaoRemessaInstrucao bancoConfiguracaoRemessaInstrucao : bancoConfiguracaoRemessa.getListaBancoConfiguracaoRemessaInstrucao()){
					if(bancoConfiguracaoRemessaInstrucao.equals(filtro.getInstrucaoPagamento())){
						geralVO.setInstrucaocobrancacodigo(bancoConfiguracaoRemessaInstrucao.getCodigo());
						geralVO.setInstrucaocobrancanome(bancoConfiguracaoRemessaInstrucao.getNome());
					}
				}
			}
		}		
		
		if (filtro.getTipoPagamento() != null){
			BancoTipoPagamento btp = bancotipopagamentoService.load(filtro.getTipoPagamento());
			if (btp != null)
				geralVO.setTipopagamento(btp.getIdentificador());
		}
		
		boolean agrupamentoFornecedor = false;
		if (filtro.getFormaPagamento() != null){
			BancoFormapagamento bfp = bancoformapagamentoService.load(filtro.getFormaPagamento());
			if (bfp != null)
				geralVO.setFormapagamento(bfp.getIdentificador());
			
			if(bancoConfiguracaoRemessa.getListaBancoConfiguracaoRemessaFormaPagamento() != null && 
					bancoConfiguracaoRemessa.getListaBancoConfiguracaoRemessaFormaPagamento().size() > 0){
				for (BancoConfiguracaoRemessaFormaPagamento remessaFormaPagamento : bancoConfiguracaoRemessa.getListaBancoConfiguracaoRemessaFormaPagamento()) {
					if(remessaFormaPagamento.getBancoFormapagamento() != null && 
							remessaFormaPagamento.getBancoFormapagamento().getCdbancoformapagamento() != null &&
							remessaFormaPagamento.getBancoFormapagamento().getCdbancoformapagamento().equals(bfp.getCdbancoformapagamento())){
						agrupamentoFornecedor = remessaFormaPagamento.getAgrupamentofornecedor() != null && remessaFormaPagamento.getAgrupamentofornecedor();
						break;
					}
				}
			}
		}
		
		if (filtro.getAuxiliar1() != null){
			BancoConfiguracaoRemessaAuxiliar auxiliar1 = bancoConfiguracaoRemessaAuxiliarService.load(filtro.getAuxiliar1());
			geralVO.setCodigoauxiliar1(auxiliar1.getCodigo());
			geralVO.setNomeauxiliar1(auxiliar1.getNome());
		}
		
		if (filtro.getAuxiliar2() != null){
			BancoConfiguracaoRemessaAuxiliar auxiliar2 = bancoConfiguracaoRemessaAuxiliarService.load(filtro.getAuxiliar2());
			geralVO.setCodigoauxiliar2(auxiliar2.getCodigo());
			geralVO.setNomeauxiliar2(auxiliar2.getNome());
		}
		
		if(conta.getSequencialpagamento() != null){
			geralVO.setSequencialpagamento(conta.getSequencialpagamento());
			remessaVO.setUtilizouSequencialpagamento(true);
		}
		
		List<DocumentoVO> listaDocumentoVO = documentoService.getListaDocumentoForRemessaConfiguravel(listaDocumento, new Date(System.currentTimeMillis()), agrupamentoFornecedor);
		
		if(SinedUtil.isListNotEmpty(listaDocumentoVO)){
			String documentotipo = null;
			String tipotributo = null;
			for(DocumentoVO documentoVO : listaDocumentoVO){
				if(org.apache.commons.lang.StringUtils.isNotBlank(documentoVO.getDocumentotipo())){
					if(documentotipo == null){
						documentotipo = documentoVO.getDocumentotipo();
						tipotributo = documentoVO.getTipotributo();
					}else if(!documentotipo.equals(documentoVO.getDocumentotipo())){
						documentotipo = null;
						tipotributo = null;
					}
				}
			}
			if(documentotipo != null){
				geralVO.setTipodocumento(documentotipo);
				geralVO.setTipotributo(tipotributo);
			}
		}
		
		remessaVO.setGeralVO(geralVO);
		remessaVO.setBancoVO(bancoVO);
		remessaVO.setContaBancariaVO(contaBancariaVO);
		remessaVO.setEmpresaVO(empresaVO);
		remessaVO.setListaDocumentoVO(listaDocumentoVO);
		
		List<BancoConfiguracaoRemessaSegmento> segmentos = bancoConfiguracaoRemessaSegmentoService.findAllByBancoConfiguracaoRemessa(filtro.getBancoConfiguracaoRemessa());

		BancoConfiguracaoRemessaUtil bancoConfiguracaoRemessaUtil = new BancoConfiguracaoRemessaUtil();
		String arquivo = bancoConfiguracaoRemessaUtil.gerarDadosArquivoRemessaConfiguravel(segmentos, remessaVO);
		
		if (remessaVO.getListaErro() != null && remessaVO.getListaErro().size() > 0){
			StringBuilder erros = new StringBuilder();
			for (String erro : remessaVO.getListaErro()){
				erros.append(erro).append("; ");
			}	
			throw new SinedException(erros.toString());
		}
		
		// Cria o arquivo texto
		SimpleDateFormat format = new SimpleDateFormat("ddMM");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeArq = "PG"+data+StringUtils.stringCheia(conta.getSequencialpagamento() != null ? conta.getSequencialpagamento().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencialpagamento() != null ? conta.getSequencialpagamento().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		
		if (remessaVO.isUtilizouSequencialpagamento())
			contaService.setProximoSequencialpagamento(conta);
		
		//Remove o status de rejeitado quando o documento � gerado novamente em outra remessa
		for (Documento doc : listaDocumento){
			if (doc.getRejeitadobanco() != null && doc.getRejeitadobanco()){
				doc.setRejeitadobanco(Boolean.FALSE);
				documentoService.updateRejeitado(doc);
			}
		}
		
		return resourceModelAndView;			
	}
	
	/**
	* M�todo que cria a lista de BancoConfiguracaoRemessaDocumentoacao
	*
	* @param bean
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	public void criarListaBancoConfiguracaoRemessaDocumentoacao(BancoConfiguracaoRemessa bean) {
		List<Documentoacao> listaDocumentoacao = bean.getListaDocumentoacao();
		List<BancoConfiguracaoRemessaDocumentoacao> listaBancoConfiguracaoRemessaDocumentoacao = bancoConfiguracaoRemessaDocumentoacaoService.createListaBancoConfiguracaoRemessaDocumentoacao(listaDocumentoacao, bean);
		bean.setListaBancoConfiguracaoRemessaDocumentoacao(listaBancoConfiguracaoRemessaDocumentoacao);	
	}
	
	/**
	* M�todo que retorna a lista de Documentoacao da configuracao da remessa
	*
	* @param form
	* @return
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	public List<Documentoacao> getListDocumentoacao(BancoConfiguracaoRemessa form) {
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		if(form != null && SinedUtil.isListNotEmpty(form.getListaBancoConfiguracaoRemessaDocumentoacao())){
			for(BancoConfiguracaoRemessaDocumentoacao bancoRemessaDocumentoacao : form.getListaBancoConfiguracaoRemessaDocumentoacao()){
				if(bancoRemessaDocumentoacao.getDocumentoacao() != null){
					listaDocumentoacao.add(bancoRemessaDocumentoacao.getDocumentoacao());
				}
			}
		}
		return listaDocumentoacao;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.BancoConfiguracaoRemessaDAO#loadWithBancoConfiguracaoRemessaDocumentoacao(BancoConfiguracaoRemessa bancoConfiguracaoRemessa)
	*
	* @param bean
	* @return
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	public BancoConfiguracaoRemessa loadWithBancoConfiguracaoRemessaDocumentoacao(BancoConfiguracaoRemessa bean) {
		return bancoConfiguracaoRemessaDAO.loadWithBancoConfiguracaoRemessaDocumentoacao(bean);
	}
	
	/**
	 * M�todo que carrega um bancoconfiguracaoremessa de acordo com o banco da conta informada.
	 * Caso n�o seja informada conta, retorna null.
	 * 
	 * @param conta
	 * @return
	 * @author Rafael Salvio
	 */
	public List<BancoConfiguracaoRemessa> findByContabancaria(Conta conta){
		return bancoConfiguracaoRemessaDAO.findByContabancaria(conta, null);
	}
	
	public List<BancoConfiguracaoRemessa> findTipoChequeByContabancaria(Conta conta){
		return bancoConfiguracaoRemessaDAO.findByContabancaria(conta, BancoConfiguracaoRemessaTipoEnum.CHEQUE);
	}
	
	public boolean isPossuiBancoConfiguracaoRemessa(Conta conta){
		return bancoConfiguracaoRemessaDAO.isPossuiBancoConfiguracaoRemessa(conta);
	}
	
	private String getNomeArquivoRemessaConfiguravel(Conta conta){
		//SICRED
		if (conta.getBanco().getNumero()==748){
			String mes = String.valueOf(SinedDateUtils.getDateProperty(SinedDateUtils.currentDate(), Calendar.MONTH) + 1);
			if (mes.equals("10")){
				mes = "O";
			}else if (mes.equals("11")){
				mes = "N";
			}if (mes.equals("12")){
				mes = "D";
			}
			return SinedUtil.formataTamanho(conta.getNumero(), 5, '0', false) + mes + new DecimalFormat("00").format(SinedDateUtils.getDateProperty(SinedDateUtils.currentDate(), Calendar.DAY_OF_MONTH)) + ".CRM"; 
		}else {
			String data = new SimpleDateFormat("ddMM").format(new Date(System.currentTimeMillis()));
			return "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		}
	}
}