package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Obrigacaoicmsrecolher;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.ObrigacaoicmsrecolherDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ObrigacaoicmsrecolherService extends GenericService<Obrigacaoicmsrecolher> {

	private ObrigacaoicmsrecolherDAO obrigacaoicmsrecolherDAO;
	
	public void setObrigacaoicmsrecolherDAO(ObrigacaoicmsrecolherDAO obrigacaoicmsrecolherDAO) {
		this.obrigacaoicmsrecolherDAO = obrigacaoicmsrecolherDAO;
	}

	public List<Obrigacaoicmsrecolher> findForRegistroE116(SpedarquivoFiltro filtro, String siglaUf) {
		return obrigacaoicmsrecolherDAO.findForRegistroE116(filtro, siglaUf);
	}
	
	public List<Obrigacaoicmsrecolher> findForRegistroE316(Empresa empresa, String siglaUf, Date data) {
		return obrigacaoicmsrecolherDAO.findForRegistroE316(empresa, siglaUf, data);
	}

	public List<Obrigacaoicmsrecolher> findForRegistroE250(Empresa empresa, Uf uf, Date data) {
		return obrigacaoicmsrecolherDAO.findForRegistroE250(empresa, uf, data);
	}

}
