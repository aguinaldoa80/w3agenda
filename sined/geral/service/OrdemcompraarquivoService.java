package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.geral.dao.OrdemcompraarquivoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcompraarquivoService extends GenericService<Ordemcompraarquivo> {
	private OrdemcompraarquivoDAO ordemcompraarquivoDAO;
	
	public void setOrdemcompraarquivoDAO (OrdemcompraarquivoDAO ordemcompraarquivoDAO){this.ordemcompraarquivoDAO = ordemcompraarquivoDAO;}
	
	
	public Ordemcompraarquivo loadForEnvioPedidoCompra (Ordemcompraarquivo ordemcompraarquivo){
		return ordemcompraarquivoDAO.loadForEnvioPedidoCompra(ordemcompraarquivo);
	}
}
