package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.dao.FrequenciaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FrequenciaService extends GenericService<Frequencia>{

	private FrequenciaDAO frequenciaDAO;
	public void setFrequenciaDAO(FrequenciaDAO frequenciaDAO) {
		this.frequenciaDAO = frequenciaDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de frequencia com exce��o do registro "�nica".
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FrequenciaDAO#findForContas()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Frequencia> findForContas(){
		return frequenciaDAO.findForContas();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FrequenciaDAO#findForAgendamentoContrato
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Frequencia> findForAgendamentoContrato(){
		return frequenciaDAO.findForAgendamentoContrato();
	}
	
}
