package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.dao.FormularhvariavelDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhvariavelFiltro;

public class FormularhvariavelService extends GenericService<Formularhvariavel>{

	protected FormularhvariavelDAO formularhvariavelDAO;	
	
	public void setFormularhvariavelDAO(FormularhvariavelDAO formularhvariavelDAO) {this.formularhvariavelDAO = formularhvariavelDAO;}

	public List<Formularhvariavel> findByFitro(FormularhvariavelFiltro filtro) { 
		return formularhvariavelDAO.findByFiltro(filtro);
	}
}
