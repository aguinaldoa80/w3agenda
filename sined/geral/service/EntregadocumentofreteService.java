package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.dao.EntregadocumentofreteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregadocumentofreteService extends GenericService<Entregadocumentofrete> {
	
	private EntregadocumentofreteDAO entregadocumentofreteDAO;
	
	public void setEntregadocumentofreteDAO(EntregadocumentofreteDAO entregadocumentofreteDAO) {
		this.entregadocumentofreteDAO = entregadocumentofreteDAO;
	}
	
	public List<Entregadocumentofrete> findByEntregadocumento(Entregadocumento entregadocumento){
		return entregadocumentofreteDAO.findByEntregadocumento(entregadocumento);
	}
	
	public void delete(Integer cdentregadocumento, String whereNotIn){
		entregadocumentofreteDAO.delete(cdentregadocumento, whereNotIn);
	}
	
	public List<Entregadocumentofrete> findByEntregadocumentoForMemorandoexportacao(Entregadocumento entregadocumento){
		return entregadocumentofreteDAO.findByEntregadocumentoForMemorandoexportacao(entregadocumento);
	}
}
