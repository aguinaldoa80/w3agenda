package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.dao.TipoEventoNfeDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoEventoNfeService extends GenericService<TipoEventoNfe> {

	private TipoEventoNfeDAO tipoEventoNfeDAO;
	
	public void setTipoEventoNfeDAO(TipoEventoNfeDAO tipoEventoNfeDAO) {
		this.tipoEventoNfeDAO = tipoEventoNfeDAO;
	}
	
	public TipoEventoNfe procurarPorCodigo(String codigo) {
		return tipoEventoNfeDAO.procurarPorCodigo(codigo);
	}

}
