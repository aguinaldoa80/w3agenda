package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Comissionamentometa;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentocomissaodesempenho;
import br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentodesempenhoReportBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwdocumentocomissaodesempenhoService extends GenericService<Vwdocumentocomissaodesempenho>{

	private VwdocumentocomissaodesempenhoDAO vwdocumentocomissaodesempenhoDAO;
	private ComissionamentoService comissionamentoService;
	private VendaService vendaService;
		
	public void setVwdocumentocomissaodesempenhoDAO(VwdocumentocomissaodesempenhoDAO vwdocumentocomissaodesempenhoDAO) {
		this.vwdocumentocomissaodesempenhoDAO = vwdocumentocomissaodesempenhoDAO;
	}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	/**
	 * M�todo que faz os c�lculos necess�rios para exibi��o da tela de comissionamento por desempenho
	 *
	 * @see br.com.linkcom.sined.geral.service.VwdocumentocomissaodesempenhoService#findForTotalcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 * @see br.com.linkcom.sined.geral.service.VwdocumentocomissaodesempenhoService#findForTotalarepassarcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 * @see br.com.linkcom.sined.geral.service.VwdocumentocomissaodesempenhoService#findForTotalrepassadocomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 * @see br.com.linkcom.sined.geral.service.ComissionamentoService#findForCalcularmeta(Comissionamento Comissionamento)
	 *
	 * @param lista
	 * @param filtro
	 * @author Luiz Fernando
	 */
	public void ajustaLista(List<Vwdocumentocomissaodesempenho> lista, VwdocumentocomissaodesempenhoFiltro filtro) {
		filtro.setValorcomissao(null);
		filtro.setPercentualcomissao(null);
		filtro.setTotalvenda(null);
		if(lista != null && !lista.isEmpty() && filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			Money totalvenda = new Money();
			Money totalarepassar = new Money();
			Money totalrepassado = new Money();
			Comissionamento comissionamento = null;		
			if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
				addVendaForTotal(filtro, lista);
				totalvenda = filtro.getTotalvenda();
				totalarepassar = filtro.getTotalarepassar();
				totalrepassado = filtro.getTotalrepassado();
			} else {
				totalvenda = this.findForTotalcomissionamentodesempenho(filtro);
				totalarepassar = this.findForTotalarepassarcomissionamentodesempenho(filtro);
				totalrepassado = this.findForTotalrepassadocomissionamentodesempenho(filtro);
			}
			
			Double percentualcomissao = 0d;
			Double valorcomissao = 0d;
			
			if(lista.get(0).getCdcomissionamento() != null)
				comissionamento = comissionamentoService.findForCalcularmeta(new Comissionamento(lista.get(0).getCdcomissionamento()));
			
			if(comissionamento != null && comissionamento.getListaComissionamentometa() != null &&
					!comissionamento.getListaComissionamentometa().isEmpty()){
				Boolean acumulativo = comissionamento.getDesempenhocumulativo();
				Boolean diferencaentremetas = comissionamento.getConsiderardiferencaentremetas();
				
				if(acumulativo != null && acumulativo && diferencaentremetas != null && diferencaentremetas){
					
					//ordem deve ser por maior valor
					Collections.sort(comissionamento.getListaComissionamentometa(),new Comparator<Comissionamentometa>(){
						public int compare(Comissionamentometa o1, Comissionamentometa o2) {
							try {
								return o1.getMetavenda().compareTo(o2.getMetavenda());
							}catch (Exception e) {return 0;}
						}
					});
					
					Double valorMetaAlcancada = 0d;
					Double percentualMetaAlcancada = 0d;
					Double percentualMetaNaoAlcancada = 0d;
					Double valorRestanteMetaNaoAlcancada = 0d;
					
					for(Comissionamentometa cm : comissionamento.getListaComissionamentometa()){
						if(totalvenda != null && cm.getMetavenda() != null &&
								totalvenda.getValue().doubleValue() >= cm.getMetavenda().getValue().doubleValue()){
							valorMetaAlcancada = cm.getMetavenda().getValue().doubleValue();
							percentualMetaAlcancada = cm.getComissao();
							break;
						} else {
							percentualMetaNaoAlcancada = cm.getComissao();
						}
					}
					
					valorRestanteMetaNaoAlcancada = totalvenda.getValue().doubleValue() - valorMetaAlcancada;
					
					valorcomissao += valorRestanteMetaNaoAlcancada * percentualMetaNaoAlcancada;
					valorcomissao += valorMetaAlcancada * percentualMetaAlcancada;
				} else {
					for(Comissionamentometa cm : comissionamento.getListaComissionamentometa()){
						if(totalvenda != null && cm.getMetavenda() != null &&
							totalvenda.getValue().doubleValue() >= cm.getMetavenda().getValue().doubleValue()){
								if(acumulativo != null && acumulativo){
									percentualcomissao += cm.getComissao();
								} else {
									percentualcomissao = cm.getComissao();
								}
						}
					}
					if(percentualcomissao != null && percentualcomissao > 0){
						valorcomissao = totalarepassar.getValue().doubleValue() * percentualcomissao;
					}
				}
				
				if(valorcomissao != null && valorcomissao != 0){
					valorcomissao = valorcomissao/100d;
					filtro.setValorcomissao(new Money(valorcomissao));
				}
				
			}
			
			filtro.setGerarpagamento(valorcomissao != null && valorcomissao > 0 ? Boolean.TRUE : Boolean.FALSE);
			filtro.setTotalrepassado(totalrepassado);
			filtro.setTotalarepassar(totalarepassar);
			filtro.setPercentualcomissao(percentualcomissao);
			filtro.setTotalvenda(totalvenda);
			filtro.setExibirtotais(Boolean.TRUE);
		}else {
			filtro.setExibirtotais(Boolean.FALSE);
			filtro.setGerarpagamento(Boolean.FALSE);
		}
	}


	/**
	 * M�todo que adiciona venda no item da listagem
	 *
	 * @param filtro
	 * @param lista
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public void addVendaForTotal(VwdocumentocomissaodesempenhoFiltro filtro, List<Vwdocumentocomissaodesempenho> lista) {
		Money totalvenda = new Money();
		Money totalarepassar = new Money();
		Money totalrepassado = new Money();
		if(lista != null && !lista.isEmpty()){
			String whereInVenda = CollectionsUtil.listAndConcatenate(lista, "cdvenda", ",");
			if(whereInVenda != null && !whereInVenda.equals("")){
				List<Venda> listaVenda = vendaService.findForListagemComissionamentoDesempenho(whereInVenda);
				if(listaVenda != null && !listaVenda.isEmpty()){
					for(Vwdocumentocomissaodesempenho vwdocumentocomissaodesempenho : lista){
						if(vwdocumentocomissaodesempenho.getFromVenda() != null && vwdocumentocomissaodesempenho.getFromVenda() &&
								vwdocumentocomissaodesempenho.getCdvenda() != null){
							for(Venda venda : listaVenda){
								if(venda.getCdvenda() != null && venda.getCdvenda().equals(vwdocumentocomissaodesempenho.getCdvenda())){
									vwdocumentocomissaodesempenho.setVenda(venda);
									
									Money valorTotal = venda.getTotalvenda();
									totalvenda = totalvenda.add(valorTotal);
									
									if(vwdocumentocomissaodesempenho.getCdcolaboradorcomissao() != null){
										totalrepassado = totalrepassado.add(valorTotal);
									}else {
										totalarepassar = totalarepassar.add(valorTotal);
									}
									break;
								}
							}
						}
					}
				}
			}
		}
		filtro.setTotalvenda(totalvenda);
		filtro.setTotalarepassar(totalarepassar);
		filtro.setTotalrepassado(totalrepassado);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForTotalcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findForTotalcomissionamentodesempenho(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForTotalrepassadocomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalrepassadocomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findForTotalrepassadocomissionamentodesempenho(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForTotalarepassarcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalarepassarcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findForTotalarepassarcomissionamentodesempenho(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findDocumentoorigem(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public String findDocumentoorigem(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findDocumentoorigem(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findNotadocumento(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public String findNotadocumento(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findNotadocumento(filtro);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForReportComissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro _filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<ComissionamentodesempenhoReportBean> findForReportComissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findForReportComissionamentodesempenho(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForCalcularValorComissao(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vwdocumentocomissaodesempenho> findForCalcularValorComissao(VwdocumentocomissaodesempenhoFiltro filtro) {
		return vwdocumentocomissaodesempenhoDAO.findForCalcularValorComissao(filtro);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForReportDesempenho
	 * @param _filtro
	 * @return
	 * @return List<ComissionamentodesempenhoReportBean>
	 * @author Thiago Augusto
	 * @date 24/08/2012
	 *
	 */
	public List<Vwdocumentocomissaodesempenho> findForReportDesempenho(VwdocumentocomissaodesempenhoFiltro _filtro){
		return vwdocumentocomissaodesempenhoDAO.findForReportDesempenho(_filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VwdocumentocomissaodesempenhoDAO#findForContapagar(VwdocumentocomissaodesempenhoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public List<Vwdocumentocomissaodesempenho> findForContapagar(VwdocumentocomissaodesempenhoFiltro filtro){
		return vwdocumentocomissaodesempenhoDAO.findForContapagar(filtro);
	}

	public Resource gerarListagemCSV(WebRequestContext request, VwdocumentocomissaodesempenhoFiltro filtro) {
		ListagemResult<Vwdocumentocomissaodesempenho> listagemResult = findForExportacao(filtro);
		List<Vwdocumentocomissaodesempenho> lista = listagemResult.list();
		
		String labelConsiderar = (String) request.getAttribute("label_considerar");
		
		String cabecalho = "\"Colaborador\";\"Conta a Receber\";\"Venda\";\"Contrato\";\"" +
				(labelConsiderar != null ? labelConsiderar : "") + "\";\"Pago\";\"Repassado\";\n";
		
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Vwdocumentocomissaodesempenho bean : lista) {
			csv.append("\"" + bean.getNomecolaborador() + "\";");
			csv.append("\"");
			
			if(bean.getCddocumento() != null) {
				StringBuilder contaReceber = new StringBuilder("" + bean.getCddocumento());
				List<Documento> negociados = bean.getDocumentosNegociados();
				
				if(negociados != null && !negociados.isEmpty()) {
					contaReceber.append("\nNegocia��o:\n");
					
					for(Documento documento : negociados) {
						if(negociados.indexOf(documento) != negociados.size() - 1)
							contaReceber.append(documento.getCddocumento() + ", ");
						else
							contaReceber.append(documento.getCddocumento() + "");
					}
				}
				
				csv.append(contaReceber.toString());
			}
			
			if(bean.getFromVenda() != null && bean.getFromVenda() && bean.getWhereInDocumentoVenda() != null) {
				csv.append(bean.getWhereInDocumentoVenda());
			}
			
			csv.append("\";");
		    csv.append("\"" + (bean.getCdvenda() != null ? bean.getCdvenda() : "") + "\";");
		    csv.append("\"" + (bean.getCdcontrato() != null ? bean.getCdcontrato() : "") + "\";");
		    csv.append("\"");
		    
		    if(filtro.getConsiderar().getValue() == 0)
		    	csv.append(bean.getSaldofinal());
		    else if(filtro.getConsiderar().getValue() == 1)
		    	csv.append(bean.getVenda().getTotalvenda());
		    else if(filtro.getConsiderar().getValue() == 2)
		    	csv.append(bean.getValormarkupvenda());
		    else if(filtro.getConsiderar().getValue() == 3)
		    	csv.append(bean.getValoratualdocumento());
		    else
		    	csv.append("");
		    
		    csv.append("\";");
		    csv.append("\"" + (bean.getPago() != null && bean.getPago() ? "Sim" : "N�o") + "\";");
		    csv.append("\"" + (bean.getCdcolaboradorcomissao() != null ? "Sim" : "N�o") + "\";");
		    csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "controlar_comissionamento_desempenho.csv", csv.toString().getBytes());
		return resource;
		
	}
}
