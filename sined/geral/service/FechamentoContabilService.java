package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoContabil;
import br.com.linkcom.sined.geral.dao.FechamentoContabilDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FechamentoContabilService extends GenericService<FechamentoContabil> {
	private FechamentoContabilDAO fechamentoContabilDAO;
	
	public void setFechamentoContabilDAO(FechamentoContabilDAO fechamentoContabilDAO) {this.fechamentoContabilDAO = fechamentoContabilDAO;}
	
	public boolean existeFechamentoCadastradoMesmosDados(FechamentoContabil bean) {
		return fechamentoContabilDAO.existeFechamentoCadastradoMesmosDados(bean);
	}
	
	public Date carregarUltimaDataFechamento(Empresa empresa) {
		return fechamentoContabilDAO.carregarUltimaDataFechamento(empresa);
	}
	
	public List<FechamentoContabil> carregarUltimasDatasFechamentoPorEmpresa(String whereInEmpresa) {
		return fechamentoContabilDAO.carregarUltimasDatasFechamentoPorEmpresa(whereInEmpresa);
	}
	
	public List<FechamentoContabil> carregarFechamentosContabeisWhereIn(String whereIn) {
		return fechamentoContabilDAO.carregarFechamentosContabeisWhereIn(whereIn);
	}
	
	public Map<Integer, Date> montarMapaEmpresaDataFechamento(String whereInEmpresa) {
		Map<Integer, Date> mapa = new HashMap<Integer, Date>();
		
		if(StringUtils.isNotBlank(whereInEmpresa)) {
			List<FechamentoContabil> listaFechamentoContabil = carregarUltimasDatasFechamentoPorEmpresa(whereInEmpresa);
			
			if(SinedUtil.isListNotEmpty(listaFechamentoContabil)) {
				for(FechamentoContabil fechamentoContabil : listaFechamentoContabil) {
					mapa.put(fechamentoContabil.getEmpresa().getCdpessoa(), fechamentoContabil.getDtfechamento());
				}
			}
		}
		
		return mapa;
	}
} 
