package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialrelacionadoformula;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao.MaterialVO;
import br.com.linkcom.sined.geral.dao.MaterialrelacionadoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.MaterialrelacionadoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.materialrelacionado.MaterialrelacionadoRESTModel;

public class MaterialrelacionadoService extends GenericService<Materialrelacionado> {

	private MaterialrelacionadoDAO materialrelacionadoDAO;
	private ProdutoService produtoService;
	
	public void setMaterialrelacionadoDAO(MaterialrelacionadoDAO materialrelacionadoDAO) {this.materialrelacionadoDAO = materialrelacionadoDAO;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}


	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		materialrelacionadoDAO.deleteWhereNotInMaterial(material, whereIn);
	}
	
	/* singleton */
	private static MaterialrelacionadoService instance;
	public static MaterialrelacionadoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialrelacionadoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialrelacionadoDAO#findByPromocao
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialrelacionado> findByPromocao(Material material) {
		return materialrelacionadoDAO.findByPromocao(material);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialrelacionadoDAO#findForPVOffline()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<MaterialrelacionadoOfflineJSON> findForPVOffline() {
		List<MaterialrelacionadoOfflineJSON> lista = new ArrayList<MaterialrelacionadoOfflineJSON>();
		for(Materialrelacionado mr : materialrelacionadoDAO.findForPVOffline()){
			lista.add(new MaterialrelacionadoOfflineJSON(mr));
		}
		return lista;
	}

	/**
	 * M�todo que calcula a quantidade de acordo com a formula
	 *
	 * @param material
	 * @throws ScriptException
	 * @author Luiz Fernando
	 * @since 27/12/2013
	 */
	public void calculaQuantidadeKit(Material material) throws ScriptException{
		if(material.getListaMaterialrelacionado() != null){
			List<String> idsMateriais = new ArrayList<String>();
			Pattern pattern_material = Pattern.compile("formula\\.[a-zA-Z]+Material\\(([0-9]+)\\)");
			
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				if(materialrelacionado.getListaMaterialrelacionadoformula() != null){
					for (Materialrelacionadoformula materialrelacionadoformula : materialrelacionado.getListaMaterialrelacionadoformula()) {
						Matcher m_material = pattern_material.matcher(materialrelacionadoformula.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
					}
				}
			}
			
			FormulaVO formulaVO = new FormulaVO();
			
			if(idsMateriais.size() > 0){
				List<Produto> listaMaterial = produtoService.findForCalculoFormula(CollectionsUtil.concatenate(idsMateriais, ","));
				for (Produto p : listaMaterial) {
					MaterialVO materialVO = new MaterialVO();
					materialVO.setCdmaterial(p.getCdmaterial());
					materialVO.setAltura(p.getAltura() != null ? p.getAltura() : 0.0);
					materialVO.setLargura(p.getLargura() != null ? p.getLargura() : 0.0);
					materialVO.setPeso(p.getPeso() != null ? p.getPeso() : 0.0);
					formulaVO.addMaterial(materialVO);
				}
			}
			
			for (Materialrelacionado mr : material.getListaMaterialrelacionado()) {
				MaterialVO materialVO = new MaterialVO();
				if(mr.getMaterialpromocao() != null){
					materialVO.setCdmaterial(mr.getMaterialpromocao().getCdmaterial());
				}
//				materialVO.setAltura(mr.getAltura() != null ? mr.getAltura() : 0.0);
//				materialVO.setLargura(mr.getLargura() != null ? mr.getLargura() : 0.0);
				formulaVO.addMaterialProducao(materialVO);
			}
			
			if(material.getQuantidade() != null)
				formulaVO.setQuantidadeMaterialVenda(material.getQuantidade());
			if(material.getProduto_altura() != null)
				formulaVO.setAlturaMaterialVenda(material.getProduto_altura());
			if(material.getProduto_largura() != null)
				formulaVO.setLarguraMaterialVenda(material.getProduto_largura());
			
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				List<Materialrelacionadoformula> listaMaterialrelacionadoformula = materialrelacionado.getListaMaterialrelacionadoformula();
				if(listaMaterialrelacionadoformula != null && listaMaterialrelacionadoformula.size() > 0){
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine engine = manager.getEngineByName("JavaScript");
					
					engine.put("formula", formulaVO);
					
					Collections.sort(listaMaterialrelacionadoformula, new Comparator<Materialrelacionadoformula>(){
						public int compare(Materialrelacionadoformula o1, Materialrelacionadoformula o2) {
							return o1.getOrdem().compareTo(o2.getOrdem());
						}
					});
					
					Double consumo = 0.0;
					
					for (Materialrelacionadoformula materialrelacionadoformula : listaMaterialrelacionadoformula) {
						Object obj = engine.eval(materialrelacionadoformula.getFormula());

						Double resultado = 0d;
						if(obj != null){
							String resultadoStr = obj.toString();
							resultado = new Double(resultadoStr);
						}
						
						engine.put(materialrelacionadoformula.getIdentificador(), resultado);
						consumo = resultado;
					}
					
					materialrelacionado.setQuantidade(consumo);
				}
			}
			
		}
	}
	
	public Double getValorvendakit(Material material) throws ScriptException {
		if(material.getListaMaterialrelacionado() != null){
			this.calculaQuantidadeKit(material);
			
			Double valorconsumoTotal = 0.0;
			
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				if(materialrelacionado.getQuantidade() == null) materialrelacionado.setQuantidade(0d);
				
				Double valorconsumo = (materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0.0) * (materialrelacionado.getQuantidade() != null ? materialrelacionado.getQuantidade() : 0.0);
				
				if(material.getQuantidade() != null && material.getQuantidade() > 0 && 
						materialrelacionado.getListaMaterialrelacionadoformula() == null || 
						materialrelacionado.getListaMaterialrelacionadoformula().isEmpty()){
					valorconsumo = valorconsumo * material.getQuantidade();
				}
				materialrelacionado.setValorvenda(new Money(valorconsumo));
				valorconsumoTotal += valorconsumo;
			}
			
//			if(material.getLucro() != null){
//				Double lucroPercentual = material.getLucro().getValue().doubleValue();
//				if(lucroPercentual > 0){
//					Double lucroValor = (lucroPercentual * valorconsumoTotal) / 100d;
//					valorconsumoTotal += lucroValor;
//				}
//			}
			
			if(material.getFrete() != null){
				Double fretePercentual = material.getFrete().getValue().doubleValue();
				if(fretePercentual > 0){
					Double freteValor = (fretePercentual * valorconsumoTotal) / 100d;
					valorconsumoTotal += freteValor;
				}
			}
			
			return SinedUtil.roundByParametro(valorconsumoTotal);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Set<Materialrelacionado> ordernarListaKit(Set<Materialrelacionado> listaMaterialrelacionado) {
		if(listaMaterialrelacionado == null || listaMaterialrelacionado.size() < 2)
			return listaMaterialrelacionado;
		
		List<Materialrelacionado> listaOrdenada = new ArrayList<Materialrelacionado>();
		listaOrdenada.addAll(listaMaterialrelacionado);
		
		Collections.sort(listaOrdenada, new Comparator<Materialrelacionado>(){
			public int compare(Materialrelacionado mp1, Materialrelacionado mp2) {
				if(mp1.getOrdemexibicao() == null){
					return 1;
				}else if(mp2.getOrdemexibicao() == null){
					return -1;
				}
				return mp1.getOrdemexibicao().compareTo(mp2.getOrdemexibicao());
			}
		});
		
		return SinedUtil.listToSet(listaOrdenada, Materialrelacionado.class);
	}
	
	public List<MaterialrelacionadoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialrelacionadoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialrelacionadoRESTModel> lista = new ArrayList<MaterialrelacionadoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialrelacionado bean : materialrelacionadoDAO.findForAndroid(whereIn))
				lista.add(new MaterialrelacionadoRESTModel(bean));
		}
		
		return lista;
	}
	
	public Double getValorCustooTotal(Material material) throws ScriptException{
		if(material.getListaMaterialrelacionado() != null){
			Double valorCustoTotal = 0.0;
			
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				if(materialrelacionado.getQuantidade() == null || materialrelacionado.getMaterialpromocao() == null ||  
						materialrelacionado.getMaterialpromocao().getValorcusto() == null) continue;
				
				valorCustoTotal += (materialrelacionado.getMaterialpromocao().getValorcusto() * materialrelacionado.getQuantidade());
			}
			
			return valorCustoTotal;
		}
		return null;
	}

}
