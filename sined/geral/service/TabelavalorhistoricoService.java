package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tabelavalorhistorico;
import br.com.linkcom.sined.geral.dao.TabelavalorhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TabelavalorhistoricoService extends GenericService<Tabelavalorhistorico> {
	
	private TabelavalorhistoricoDAO tabelavalorhistoricoDAO;
	
	public void setTabelavalorhistoricoDAO(TabelavalorhistoricoDAO tabelavalorhistoricoDAO) {
		this.tabelavalorhistoricoDAO = tabelavalorhistoricoDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.TabelavalorhistoricoService#findByTabelavalor(Tabelavalor tabelavalor)
	*
	* @param tabelavalor
	* @return
	* @since 23/06/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalorhistorico> findByTabelavalor(Tabelavalor tabelavalor) {
		return tabelavalorhistoricoDAO.findByTabelavalor(tabelavalor);
	}
}
