package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.dao.ChequehistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ChequehistoricoService extends GenericService<Chequehistorico>{

	private ChequehistoricoDAO chequehistoricoDAO;
	private ChequeService chequeService;
	
	public void setChequehistoricoDAO(ChequehistoricoDAO chequehistoricoDAO) {this.chequehistoricoDAO = chequehistoricoDAO;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}

	/**
	 * M�todo cria e salva o hist�rico do cheque de acordo com o par�metro
	 *
	 * @param cheque
	 * @param venda
	 * @param contareceber
	 * @param movimentacao
	 * @author Luiz Fernando
	 */
	public void criaHistorico(Cheque cheque, Venda venda, Documento contareceber, Movimentacao movimentacao) {
		if(cheque != null && cheque.getCdcheque() != null){
			StringBuilder obs = new StringBuilder();
			StringBuilder obsrelatorio = new StringBuilder();
			
			Chequehistorico chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			if(SinedUtil.getUsuarioLogado() != null){
				chequehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			}
			
			if(!existHistoricoByCheque(cheque)){
				obs.append("Origem do Cheque: ");
				obsrelatorio.append("Origem do Cheque: ");
			}
			if(venda != null && venda.getCdvenda() != null){
				obs.append("Venda <a href=\"javascript:visualizaVenda("+ venda.getCdvenda()+");\">"+venda.getCdvenda() +"</a>.");
				obsrelatorio.append(" Venda " + venda.getCdvenda() + "");
			}
			if(contareceber != null && contareceber.getCddocumento() != null){
				obs.append("Documento <a href=\"javascript:visualizaContareceber("+ contareceber.getCddocumento()+");\">"+contareceber.getCddocumento() +"</a>.");
				obsrelatorio.append(" Documento " + contareceber.getCddocumento());
			}
			if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
				obs.append("Movimenta��o <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
				obsrelatorio.append(" Movimenta��o " +  movimentacao.getCdmovimentacao());
			}
			chequehistorico.setObservacao(obs.toString());
			chequehistorico.setObservacaorelatorio(obsrelatorio.toString());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
			this.saveOrUpdate(chequehistorico);
		}
	}
	
	/**
	* M�todo que retorna a situa��o do cheque. Caso a situa��o seja nula, carrega a informa��o do banco de dados
	*
	* @param cheque
	* @return
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	private Chequesituacao getChequeSituacao(Cheque cheque) {
		Chequesituacao chequesituacao = null;
		if(cheque != null && cheque.getCdcheque() != null){
			chequesituacao = cheque.getChequesituacao();
			if(chequesituacao == null){
				Cheque cq = chequeService.load(cheque, "cheque.cdcheque, cheque.chequesituacao");
				if(cq != null) 
					chequesituacao = cq.getChequesituacao();
			}
		}
		return chequesituacao;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequehistoricoDAO.existHistoricoByCheque(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existHistoricoByCheque(Cheque cheque) {
		return chequehistoricoDAO.existHistoricoByCheque(cheque);
	}

	/**
	 * Cria o hist�rico do cheque  
	 *
	 * @param cheque
	 * @param contareceber
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Chequehistorico criaHistorico(Cheque cheque, Documento contareceber, Movimentacao movimentacao, Documentoclasse documentoclasse) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null){
			chequehistorico = new Chequehistorico();
			StringBuilder obs = new StringBuilder();
			StringBuilder obsrelatorio = new StringBuilder();
			
			chequehistorico.setCheque(cheque);
			chequehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			chequehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			
			if(!existHistoricoByCheque(cheque)){
				obs.append("Origem do Cheque: ");
				obsrelatorio.append("Origem do Cheque: ");
			}
			if(contareceber != null && contareceber.getCddocumento() != null){
				if(movimentacao != null){
					obs.append("Baixa do documento ");
					obsrelatorio.append("Baixa do documento ");
				}else {
					if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
						obs.append("Associa��o a Conta a Pagar ");
						obsrelatorio.append("Associa��o a Conta a Pagar ");
					}else {
						obs.append("Associa��o a Conta a Receber ");
						obsrelatorio.append("Associa��o a Conta a Receber ");
					}
				}
				if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
					obs.append(" <a href=\"javascript:visualizaContapagar("+ contareceber.getCddocumento()+");\">"+contareceber.getCddocumento() +"</a>.");
				} else{
					obs.append(" <a href=\"javascript:visualizaContareceber("+ contareceber.getCddocumento()+");\">"+contareceber.getCddocumento() +"</a>.");
				}
				obsrelatorio.append(contareceber.getCddocumento());
			}
			if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
				obs.append("Movimenta��o <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
				obsrelatorio.append("Movimenta��o "+ movimentacao.getCdmovimentacao());
			}
			chequehistorico.setObservacao(obs.toString());
			chequehistorico.setObservacaorelatorio(obsrelatorio.toString());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}
		
		return chequehistorico;
	}
	
		
	/**
	 * Cria o hist�rico do cheque dos documentos baixados
	 *
	 * @param cheque
	 * @param listaDocumento
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Chequehistorico criaHistorico(Cheque cheque, List<Documento> listaDocumento, Movimentacao movimentacao, Documentoclasse documentoclasse) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null && listaDocumento != null && !listaDocumento.isEmpty()){
			chequehistorico = new Chequehistorico();
			StringBuilder obs = new StringBuilder();
			StringBuilder obsrelatorio = new StringBuilder();
			StringBuilder docs = new StringBuilder();
			StringBuilder docsrelatorio = new StringBuilder();
			
			if(!existHistoricoByCheque(cheque)){
				obs.append("Origem do Cheque: Baixa do(s) documento(s) ");
				obsrelatorio.append("Origem do Cheque: Baixa do(s) documento(s) ");
			}else if(SinedUtil.isListNotEmpty(listaDocumento)){
				obs.append("Documento(s) ");
				obsrelatorio.append("Documento(s) ");
			}
			
			chequehistorico.setCheque(cheque);
			chequehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			chequehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			
			for(Documento d : listaDocumento){
				if(!"".equals(docs.toString())) docs.append(",");
				if(!"".equals(docs.toString())) docsrelatorio.append(",");
				if(Documentoclasse.OBJ_RECEBER.equals(documentoclasse)){
					docs.append("<a href=\"javascript:visualizaContareceber("+ d.getCddocumento()+");\">"+d.getCddocumento() +"</a> ");
					docsrelatorio.append(d.getCddocumento());
				}else if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
					docs.append("<a href=\"javascript:visualizaContapagar("+ d.getCddocumento()+");\">"+d.getCddocumento() +"</a> ");
					docsrelatorio.append(d.getCddocumento());
				}
			}
			if(!"".equals(docs.toString())){
				obs.append(docs.toString());
				obsrelatorio.append(docsrelatorio.toString());
			}
			if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
				obs.append("<BR>Movimenta��o <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
				obsrelatorio.append("\nMovimenta��o "+ movimentacao.getCdmovimentacao());
			}
			
			if(obs.length() > 1000){
				if(movimentacao != null && movimentacao.getCdmovimentacao() != null){
					obs = new StringBuilder();
					obs.append("Origem do Cheque: Movimentacao <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
				}
			}
			chequehistorico.setObservacao(obs.toString());
			chequehistorico.setObservacaorelatorio(obsrelatorio.toString());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}		
		return chequehistorico;
	}

	/**
	 * Cria o hist�rico do cheque a partir da devolu��o de cheque 
	 *
	 * @param cheque
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Chequehistorico criaHistoricoDevolucao(Cheque cheque, Movimentacao movimentacao) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setObservacao("Devolu��o de Cheque <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
			chequehistorico.setObservacaorelatorio("Devolu��o de Cheque: " +  movimentacao.getCdmovimentacao());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}		
		return chequehistorico;
	}

	/**
	 * Cria o hist�rico do cheque a partir da conciliacao
	 * 
	 * @param cheque
	 * @param movimentacao
	 * @param observacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Chequehistorico criaHistoricoConciliacao(Cheque cheque, Movimentacao movimentacao, String observacao) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setObservacao(observacao + " <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
			chequehistorico.setObservacaorelatorio(observacao + " " + movimentacao.getCdmovimentacao());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}		
		return chequehistorico;
	}
	
	/**
	 * Cria hist�rico do cheque a partir de transfer�ncia entre contas
	 *
	 * @param cheque
	 * @param movimentacao
	 * @param observacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Chequehistorico criaHistoricoTransferencia(Cheque cheque, Movimentacao movimentacao, String observacao) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setObservacao(observacao + " <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
			chequehistorico.setObservacaorelatorio(observacao + " " + movimentacao.getCdmovimentacao());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}		
		return chequehistorico;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ChequehistoricoDAO#findByCheque(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Chequehistorico> findByCheque(Cheque cheque){
		return chequehistoricoDAO.findByCheque(cheque);
	}
	
	/**
	* Cria o hist�rico do cheque a partir do cancelamento da movimenta��o
	*
	* @param cheque
	* @param movimentacao
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	public Chequehistorico criaHistoricoCancelamentoMovimentacao(Cheque cheque, Movimentacao movimentacao) {
		Chequehistorico chequehistorico = null;
		if(cheque != null && cheque.getCdcheque() != null && movimentacao != null && movimentacao.getCdmovimentacao() != null){
			chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setObservacao("Cancelamento da Movimenta��o <a href=\"javascript:visualizaMovimentacao("+ movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a>.");
			chequehistorico.setObservacaorelatorio("Cancelamento da Movimenta��o: " +  movimentacao.getCdmovimentacao());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
		}		
		return chequehistorico;
	}
	
	/**
	 * M�todo cria e salva o hist�rico do cheque de acordo com o par�metro
	 *
	 * @param cheque
	 * @param pedidovenda
	 * @param contareceber
	 * @param movimentacao
	 * @author Mairon
	 */
	public void criaHistorico(Cheque cheque, Pedidovenda pedidovenda) {
		if(cheque != null && cheque.getCdcheque() != null){
			StringBuilder obs = new StringBuilder();
			StringBuilder obsrelatorio = new StringBuilder();
			
			Chequehistorico chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(cheque);
			chequehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			if(SinedUtil.getUsuarioLogado() != null){
				chequehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			}
			
			if(!existHistoricoByCheque(cheque)){
				obs.append("Origem do Cheque: ");
				obsrelatorio.append("Origem do Cheque: ");
			}
			if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
				obs.append("Pedido de venda <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
				obsrelatorio.append(" Pedido de venda " + pedidovenda.getCdpedidovenda() + "");
			}
			chequehistorico.setObservacao(obs.toString());
			chequehistorico.setObservacaorelatorio(obsrelatorio.toString());
			chequehistorico.setChequesituacao(getChequeSituacao(cheque));
			this.saveOrUpdate(chequehistorico);
		}
	}
	
	public Chequehistorico findHistoricoDevolvido(Cheque cheque, Movimentacao movimentacao) {
		return chequehistoricoDAO.findHistoricoDevolvido(cheque, movimentacao);
	}
}
