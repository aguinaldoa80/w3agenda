package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.dao.PessoacategoriaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.clientecategoria.ClientecategoriaRESTModel;

public class PessoacategoriaService extends GenericService<Pessoacategoria> {

	/* singleton */
	private static PessoacategoriaService instance;
	public static PessoacategoriaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PessoacategoriaService.class);
		}
		return instance;
	}
	
	private  PessoacategoriaDAO pessoacategoriaDAO;
	
	public void setPessoacategoriaDAO(PessoacategoriaDAO pessoacategoriaDAO) {
		this.pessoacategoriaDAO = pessoacategoriaDAO;
	}
	

	/**
	 * Faz refer�ncia ao DAO.
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 16/11/2010
	 */
	public List<Pessoacategoria> findByCliente(Cliente cliente){
		return pessoacategoriaDAO.findByCliente(cliente);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cliente
	 * @param categoria
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/03/2014
	 */
	public boolean haveCategoriaCliente(Cliente cliente, Categoria categoria) {
		return pessoacategoriaDAO.haveCategoriaCliente(cliente, categoria);
	}


	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cliente
	 * @author Rodrigo Freitas
	 * @since 07/04/2014
	 */
	public void deleteByCliente(Cliente cliente) {
		pessoacategoriaDAO.deleteByCliente(cliente);
	}
	
	public List<Pessoacategoria> findByFornecedor(Fornecedor fornecedor){
		return pessoacategoriaDAO.findByFornecedor(fornecedor);
	}
	
	public List<ClientecategoriaRESTModel> findClienteForAndroid() {
		return findClienteForAndroid(null, true);
	}
	
	public List<ClientecategoriaRESTModel> findClienteForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClientecategoriaRESTModel> lista = new ArrayList<ClientecategoriaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pessoacategoria c : pessoacategoriaDAO.findClienteForAndroid(whereIn))
				lista.add(new ClientecategoriaRESTModel(c));
		}
		
		return lista;
	}

}
