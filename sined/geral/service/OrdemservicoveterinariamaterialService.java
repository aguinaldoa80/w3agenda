package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariamaterial;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.dao.OrdemservicoveterinariamaterialDAO;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemservicoveterinariamaterialService extends GenericService<Ordemservicoveterinariamaterial> {

	private OrdemservicoveterinariamaterialDAO ordemservicoveterinariamaterialDAO;
	private OrdemservicoveterinariaService ordemservicoveterinariaService;
	private static OrdemservicoveterinariamaterialService instance;
	
	public void setOrdemservicoveterinariamaterialDAO(OrdemservicoveterinariamaterialDAO ordemservicoveterinariamaterialDAO) {	this.ordemservicoveterinariamaterialDAO = ordemservicoveterinariamaterialDAO;}
	public void setOrdemservicoveterinariaService(	OrdemservicoveterinariaService ordemservicoveterinariaService) { this.ordemservicoveterinariaService = ordemservicoveterinariaService;	}

	public static OrdemservicoveterinariamaterialService getInstance() {
		if (instance==null){
			instance = Neo.getObject(OrdemservicoveterinariamaterialService.class);
		}
		return instance;
	}
	
	public void updateFaturado(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial){
		if(ordemservicoveterinariamaterial != null){
			ordemservicoveterinariamaterialDAO.updateFaturado(ordemservicoveterinariamaterial);
		}
	}
	
	/**
	 * M�todo que persiste uma OrdemServicoVeterinariaMaterial com o Status de Faturado 
	 *
	 * @param OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public void updateFaturado(Ordemservicoveterinaria os) {
		if(os != null && os.getCdordemservicoveterinaria() != null && os.getListaOrdemservicoveterinariamaterial() != null && !os.getListaOrdemservicoveterinariamaterial().isEmpty()){
			for (Ordemservicoveterinariamaterial iterable : os.getListaOrdemservicoveterinariamaterial()) {
				if(iterable != null && iterable.getQtdeusada() != null && iterable.getQtdeusada() > 0 && (iterable.getFaturado() == null || iterable.getFaturado() == false))
					updateFaturado(iterable);
			}
		}
	}

	/**
	 * M�todo que persiste uma Lista de OrdemServicoVeterinaria com o Status de Faturado 
	 *
	 * @param whereIn OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public void updateFaturado(String whereInOSV) {
		if(whereInOSV != null && !whereInOSV.trim().isEmpty()){
			for (Ordemservicoveterinaria os : ordemservicoveterinariaService.carregaRequisicaoForFaturar(whereInOSV)) {
				updateFaturado(os);
			}
		}
		
	}	
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public List<Ordemservicoveterinariamaterial> findForIteracao(){
		return ordemservicoveterinariamaterialDAO.findForIteracao();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public void updateIteracao(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial, IteracaoVeterinariaRetorno retorno){
		ordemservicoveterinariamaterialDAO.updateIteracao(ordemservicoveterinariamaterial, retorno);
	}
	
	public List<Ordemservicoveterinariamaterial> findWithReserva(String whereInOSVM){
		return ordemservicoveterinariamaterialDAO.findWithReserva(whereInOSVM);
	}
	
	public String getWhereInReserva(List<Ordemservicoveterinariamaterial> lista) {
		StringBuilder sb = new StringBuilder();
		if(SinedUtil.isListNotEmpty(lista)){
			for(Ordemservicoveterinariamaterial bean : lista){
				if(SinedUtil.isListNotEmpty(bean.getListaReserva())){
					for(Reserva reserva : bean.getListaReserva()){
						if(reserva.getCdreserva() != null){
							sb.append(reserva.getCdreserva()).append(",");
						}
					}
				}
			}
		}
		return sb.length() > 0 ? sb.substring(0, sb.length()-1) : null;
	}
}