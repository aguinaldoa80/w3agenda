package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.dao.ColaboradordespesaitemDAO;

public class ColaboradordespesaitemService extends GenericService<Colaboradordespesaitem>{

	private ColaboradordespesaitemDAO colaboradordespesaitemDAO;
	public void setColaboradordespesaitemDAO(ColaboradordespesaitemDAO colaboradordespesaitemDAO) {this.colaboradordespesaitemDAO = colaboradordespesaitemDAO;}
	
	/* singleton */
	private static ColaboradordespesaitemService instance;
	public static ColaboradordespesaitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordespesaitemService.class);
		}
		return instance;
	}
	
	public List<Colaboradordespesaitem> findByColaboradordespesa(Integer cdcolaboradordespesa, Date dtreferencia1, Date dtreferencia2){
		return colaboradordespesaitemDAO.findByColaboradordespesa(cdcolaboradordespesa, dtreferencia1, dtreferencia2);
	}
}
