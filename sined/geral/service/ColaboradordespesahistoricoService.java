package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.geral.dao.ColaboradordespesahistoricoDAO;

public class ColaboradordespesahistoricoService extends GenericService<Colaboradordespesahistorico>{
	
	private ColaboradordespesahistoricoDAO colaboradordespesahistoricoDAO;
		

	public void setColaboradordespesahistoricoDAO(
			ColaboradordespesahistoricoDAO colaboradordespesahistoricoDAO) {
		this.colaboradordespesahistoricoDAO = colaboradordespesahistoricoDAO;
	}

	public List<Colaboradordespesahistorico> findByColaboradordespesahistorico(Colaboradordespesa colaboradordespesa){
		return colaboradordespesahistoricoDAO.finByColaboradordespesahistorico(colaboradordespesa);
	}
	
}
