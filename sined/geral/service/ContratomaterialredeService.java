package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialrede;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.dao.ContratomaterialredeDAO;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ContratomaterialredeFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratomaterialredeService extends GenericService<Contratomaterialrede>{

	private ContratomaterialredeDAO contratomaterialredeDAO;
	
	public void setContratomaterialredeDAO(
			ContratomaterialredeDAO contratomaterialredeDAO) {
		this.contratomaterialredeDAO = contratomaterialredeDAO;
	}	
	
	public int countContratomaterial(Integer cdcontratomaterialrede, Contratomaterial contratomaterial) {
		return contratomaterialredeDAO.countContratomaterial(cdcontratomaterialrede, contratomaterial);
	}
	

	/** 
	* Gerar arquivo CSV com os campos do filtro	
	*
	* @param filtro
	* @return
	* @since Jul 20, 2011
	* @author Luiz Fernando F Silva
	*/
	public Resource preparaArquivoContratomaterialredeCSV(ContratomaterialredeFiltro filtro) {
		List<Contratomaterialrede> listaContratomaterialrede = this.findForReport(filtro);
		
		StringBuilder csv = new StringBuilder("Id; Cliente; Contrato de Material; Endere�o de Instala��o; Tipo do Servi�o; Ambiente; Servidor; " +
				"Interface Preferencial; Restringir a esta interface; Rede; IP autom�tico; Endere�o IP; MAC; Usu�rio; Senha; Ativo\n");
		
		Servidor servidor = new Servidor();
		if(listaContratomaterialrede != null){
			
			for(Contratomaterialrede contratomaterialrede : listaContratomaterialrede){
				
				if (contratomaterialrede.getServidorinterfacepreferencial() != null){
					servidor = contratomaterialrede.getServidorinterfacepreferencial().getServidor();
					contratomaterialrede.setServidor(servidor);
					contratomaterialrede.setAmbienterede(servidor.getAmbienterede());
				}
				
				csv.append(contratomaterialrede.getCdcontratomaterialrede());
				csv.append(";");
				
				if(contratomaterialrede.getContratomaterial() != null && contratomaterialrede.getContratomaterial().getContrato() != null &&
						contratomaterialrede.getContratomaterial().getContrato().getCliente() != null){
					csv.append(contratomaterialrede.getContratomaterial().getContrato().getCliente().getNome() != null ? contratomaterialrede.getContratomaterial().getContrato().getCliente().getNome() : "");
				}else csv.append("");
				csv.append(";");
				
				if(contratomaterialrede.getContratomaterial() != null && contratomaterialrede.getContratomaterial().getServico() != null){
					csv.append(contratomaterialrede.getContratomaterial().getServico().getNome() != null ? contratomaterialrede.getContratomaterial().getServico().getNome() : "");
				}else csv.append("");				
				csv.append(";");	
				
				if(contratomaterialrede.getEnderecocliente() != null){
					String end = "";
					if(contratomaterialrede.getEnderecocliente().getLogradouro() != null){
						end += contratomaterialrede.getEnderecocliente().getLogradouro();
						if(contratomaterialrede.getEnderecocliente().getNumero() != null){
							end += ", " + contratomaterialrede.getEnderecocliente().getNumero();
						}
						if(contratomaterialrede.getEnderecocliente().getBairro() != null){
							end += ", " + contratomaterialrede.getEnderecocliente().getBairro();
						}
						if(contratomaterialrede.getEnderecocliente().getMunicipio() != null && contratomaterialrede.getEnderecocliente().getMunicipio().getNome() != null){
							end += ", " + contratomaterialrede.getEnderecocliente().getMunicipio().getNome();
						
							if(contratomaterialrede.getEnderecocliente().getMunicipio().getUf() != null && contratomaterialrede.getEnderecocliente().getMunicipio().getUf().getSigla() != null){
								end += "/" + contratomaterialrede.getEnderecocliente().getMunicipio().getUf().getSigla();
							}
						}
						
						csv.append(end);
					}
					
					
				}else csv.append("");
				csv.append(";");
				
				
				if(contratomaterialrede.getServicoservidortipo() != null){
					csv.append(contratomaterialrede.getServicoservidortipo().getNome() != null ? contratomaterialrede.getServicoservidortipo().getNome() : "");
				}else csv.append("");
				csv.append(";");
				
				if(contratomaterialrede.getAmbienterede() != null && contratomaterialrede.getAmbienterede().getCdambienterede() != null){
					csv.append(contratomaterialrede.getAmbienterede().getNome() != null ? contratomaterialrede.getAmbienterede().getNome() : "");
				} else csv.append("");
				csv.append(";");	
				
				if(contratomaterialrede.getServidor() != null && contratomaterialrede.getServidor().getCdservidor() != null){
					csv.append(contratomaterialrede.getServidor().getNome() != null ? contratomaterialrede.getServidor().getNome() : "");
				} else csv.append("");
				csv.append(";");
				
				if(contratomaterialrede.getServidorinterfacepreferencial() != null){
					csv.append(contratomaterialrede.getServidorinterfacepreferencial().getNome() != null ? contratomaterialrede.getServidorinterfacepreferencial().getNome() : "");
				} else csv.append("");
				csv.append(";");				
				
				if(contratomaterialrede.getRestringirinterface() != null){
					csv.append(contratomaterialrede.getRestringirinterface() ? "SIM" : "N�O");
				}else csv.append("");
				csv.append(";");
				if(contratomaterialrede.getRede() != null){
					csv.append(contratomaterialrede.getRede().getEnderecoip() != null ? contratomaterialrede.getRede().getEnderecoip() : "");
				} else csv.append("");
				csv.append(";");
				
				if(contratomaterialrede.getIpautomatico() != null){
					csv.append(contratomaterialrede.getIpautomatico() ? "SIM" : "N�O");
				} else csv.append("");
				csv.append(";");
				
				if(contratomaterialrede.getRedeip() != null){
					csv.append(contratomaterialrede.getRedeip().getIp() != null ? contratomaterialrede.getRedeip().getIp() : "");
				} else csv.append("");
				csv.append(";");
				
				
				csv.append(contratomaterialrede.getMac() != null ? contratomaterialrede.getMac() : "");
				csv.append(";");
				
				
				csv.append(contratomaterialrede.getUsuario() != null ? contratomaterialrede.getUsuario() : "");
				csv.append(";");
				csv.append(contratomaterialrede.getSenha() != null ? contratomaterialrede.getSenha() : "");
				csv.append(";");
				csv.append(contratomaterialrede.getAtivo() ? "SIM" : "N�O");
				csv.append("\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "contratomaterialrede" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Gerar relat�rio com os campos do filtro
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContratomaterialredeDAO#findForReport(ContratomaterialredeFiltro contratomaterialredeFiltro)
	*
	* @param contratomaterialredeFiltro
	* @return
	* @since Jul 20, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contratomaterialrede> findForReport(ContratomaterialredeFiltro contratomaterialredeFiltro){
		return contratomaterialredeDAO.findForReport(contratomaterialredeFiltro);
	}
	
}
