package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.geral.dao.DocumentodigitalusuarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentodigitalusuarioService extends GenericService<Documentodigitalusuario> {
	
	private DocumentodigitalusuarioDAO documentodigitalusuarioDAO;
	
	public void setDocumentodigitalusuarioDAO(
			DocumentodigitalusuarioDAO documentodigitalusuarioDAO) {
		this.documentodigitalusuarioDAO = documentodigitalusuarioDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param email
	 * @return
	 * @since 08/03/2019
	 * @author Rodrigo Freitas
	 */
	public Documentodigitalusuario loadByEmail(String email) {
		return documentodigitalusuarioDAO.loadByEmail(email);
	}

	
}