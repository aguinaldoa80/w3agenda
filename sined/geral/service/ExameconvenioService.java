package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.geral.dao.ExameconvenioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExameconvenioService extends GenericService<Exameconvenio>{
	
	private ExameconvenioDAO exameconvenioDAO;
	
	public void setExameconvenioDAO(ExameconvenioDAO exameconvenioDAO) {
		this.exameconvenioDAO = exameconvenioDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * @param campos
	 * @param orderBy
	 * @return
	 */
	public List<Exameconvenio> findAll(String campos, String orderBy) {
		return exameconvenioDAO.findAll(campos, orderBy);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * @param clinica
	 * @param responsavel
	 * @return
	 */
	public List<Exameconvenio> findByClinicaResponsavel(Exameclinica clinica, Exameresponsavel responsavel){
		return exameconvenioDAO.findByClinicaResponsavel(clinica, responsavel);
	}

}
