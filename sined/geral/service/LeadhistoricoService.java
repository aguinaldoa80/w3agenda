package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.dao.LeadhistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadhistoricoService extends GenericService<Leadhistorico>{
	
	private LeadhistoricoDAO leadhistoricoDAO;
	
	public void setLeadhistoricoDAO(LeadhistoricoDAO leadhistoricoDAO) {
		this.leadhistoricoDAO = leadhistoricoDAO;
	}
	
	public void saveOrUpdateHistoricoFromConta(Contacrm bean){
		Leadhistorico leadhistorico = new Leadhistorico();
		
		leadhistorico.setLead(bean.getLead());
		leadhistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		leadhistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		leadhistorico.setObservacao("Exporta��o para a conta " +
									"<a href=\"/w3erp/crm/crud/Contacrm?ACAO=consultar&cdcontacrm=" 
									+ bean.getCdcontacrm() + "\" >" + bean.getCdcontacrm() + "</a>");
		
		this.saveOrUpdateNoUseTransaction(leadhistorico);
	}

	public List<Leadhistorico> findByContacrm(Contacrm contacrm) {
		return leadhistoricoDAO.findByContacrm(contacrm, null);
	}

	public List<Leadhistorico> findByContacrm(Contacrm contacrm, Atividadetipo atividadetipo) {
		return leadhistoricoDAO.findByContacrm(contacrm, atividadetipo);
	}
	
	public boolean haveHistorico(Lead l, Timestamp dtenvio, String obs) {
		return leadhistoricoDAO.haveHistorico(l, dtenvio, obs);
	}

	/**
	* M�todo com refer�ncia ao DAO
	* Carrega a lista de hist�rico do Lead ordenada por data
	* 
	* @see	br.com.linkcom.sined.geral.dao.LeadhistoricoDAO#carregaListaleadhistorico(Lead lead)
	*
	* @param lead
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Leadhistorico> carregaListaleadhistorico(Lead lead) {
		return leadhistoricoDAO.carregaListaleadhistorico(lead);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LeadhistoricoDAO#findByLead(Lead lead)
	 *
	 * @param lead
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Leadhistorico> findByLead(Lead lead) {
		return leadhistoricoDAO.findByLead(lead);
	}

	/**
	 * 
	 * @param lead
	 * @param dthistoricoInicio
	 * @param dthistoricoFim
	 * @author Thiago Clemente
	 * 
	 */
	public Long getQtdeInteracoes(Lead lead, Date dthistoricoInicio, Date dthistoricoFim){
		return leadhistoricoDAO.getQtdeInteracoes(lead, dthistoricoInicio, dthistoricoFim);
	}
}
