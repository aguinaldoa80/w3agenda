package br.com.linkcom.sined.geral.service;
import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.OtrPneuTipoOtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.dao.OtrPneuTipoOtrClassificacaoCodigoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrPneuTipoOtrClassificacaoCodigoService extends GenericService<OtrPneuTipoOtrClassificacaoCodigo>{
	
	private OtrPneuTipoOtrClassificacaoCodigoDAO otrPneuTipoOtrClassificacaoCodigoDAO;

	public void setOtrPneuTipoOtrClassificacaoCodigoDAO(
			OtrPneuTipoOtrClassificacaoCodigoDAO otrPneuTipoOtrClassificacaoCodigoDAO) {
		this.otrPneuTipoOtrClassificacaoCodigoDAO = otrPneuTipoOtrClassificacaoCodigoDAO;
	}
	
	public List<OtrPneuTipoOtrClassificacaoCodigo> findListOtrPneuTipoOtrClassificacaoCodigo(OtrPneuTipo otrPneuTipo){
		return otrPneuTipoOtrClassificacaoCodigoDAO.findListOtrPneuTipoOtrClassificacaoCodigo(otrPneuTipo);
	}

}
