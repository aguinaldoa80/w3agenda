package br.com.linkcom.sined.geral.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;
import br.com.linkcom.sined.geral.bean.Clienteespecialidade;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Clienteprofissao;
import br.com.linkcom.sined.geral.bean.Clienterelacao;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.ContratosincronizacaoAA;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Documentotipopapel;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.auxiliar.DocumentotipoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.FormasPrazosPagamentoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.PrazopagamentoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.ClienteDAO;
import br.com.linkcom.sined.geral.dao.RestricaoDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclienteetiquetaFiltro;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclientehistoricoFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.ClienteCompletoReportBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.ClienteEtiqueta;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteCategoriaBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteContatoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteEnderecoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteReferenciaBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteRestricaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteTelefoneBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteVendedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteInfoFinanceiraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteVO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.DominiointegracaoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ClienteWSBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.ArquivoImportacao;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.ClienteOfflineJSON;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.sined.util.rest.android.cliente.ClienteRESTModel;
import br.com.linkcom.sined.util.rest.android.cliente.ClienteRESTWSBean;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTModel;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTWSBean;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTModel;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTWSBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.ContratoBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTClient;
import br.com.linkcom.sined.util.rest.arvoreacesso.EmpresaArvoreAcessoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.cliente.ClienteW3producaoRESTModel;
import br.com.linkcom.sined.util.tag.TagFunctions;


public class ClienteService extends GenericService<Cliente> {

	private ClienteDAO clienteDAO;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private ContareceberService contareceberService;
	private RequisicaoService requisicaoService;
	private CategoriaService categoriaService;
	private PessoacategoriaService pessoacategoriaService;
	private MunicipioService municipioService;
	private TelefoneService telefoneService;
	private ContatoService contatoService;
	private PessoaService pessoaService;
	private VwclienteetiquetaService vwclienteetiquetaService;
	private ParametrogeralService parametrogeralService;
	private UsuarioService usuarioService;
	private VendahistoricoService vendahistoricoService;
	private ClientehistoricoService clientehistoricoService;
	private ClientevendedorService clientevendedorService;
	private RestricaoDAO restricaoDao;
	private ArquivoService arquivoService;
	private ClienteDocumentoTipoService clienteDocumentoTipoService;
	private ClientePrazoPagamentoService clientePrazoPagamentoService;
	private DocumentotipoService documentotipoService;
	private PrazopagamentoService prazopagamentoService;
	private ValecompraService valecompraService;
	private ClienteService clienteService;
	private ContratosincronizacaoAAService contratosincronizacaoAAService;
	private ContratohistoricoService contratohistoricoService;
	private ContratoService contratoService;
	private DocumentoService documentoService;
	private UfService ufService;
	private ClienteEmpresaService clienteEmpresaService;
	private ClienteespecialidadeService clienteespecialidadeService;
	private GrauinstrucaoService grauinstrucaoService;
	private FornecedorService fornecedorService;
	private ClienteprofissaoService clienteprofissaoService;
	private EstadocivilService estadocivilService;
	private EspecialidadeService especialidadeService;
	private PessoaContatoService pessoaContatoService;
	private ChequeService chequeService;
	private ColaboradorService colaboradorService;
	private VendaService vendaService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setEspecialidadeService(EspecialidadeService especialidadeService) {this.especialidadeService = especialidadeService;}
	public void setEstadocivilService(EstadocivilService estadocivilService) {this.estadocivilService = estadocivilService;}
	public void setClienteprofissaoService(ClienteprofissaoService clienteprofissaoService) {this.clienteprofissaoService = clienteprofissaoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setGrauinstrucaoService(GrauinstrucaoService grauinstrucaoService) {this.grauinstrucaoService = grauinstrucaoService;}
	public void setClienteEmpresaService(ClienteEmpresaService clienteEmpresaService) {this.clienteEmpresaService = clienteEmpresaService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;	}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;	}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;	}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setClienteDAO(ClienteDAO clienteDAO) {this.clienteDAO = clienteDAO;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {this.pessoacategoriaService = pessoacategoriaService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setVwclienteetiquetaService(VwclienteetiquetaService vwclienteetiquetaService) {this.vwclienteetiquetaService = vwclienteetiquetaService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setClientehistoricoService(ClientehistoricoService clientehistoricoService) {this.clientehistoricoService = clientehistoricoService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;	}
	public void setRestricaoDao(RestricaoDAO restricaoDao) {this.restricaoDao = restricaoDao;}
	public void setClienteDocumentoTipoService(ClienteDocumentoTipoService clienteDocumentoTipoService) {this.clienteDocumentoTipoService = clienteDocumentoTipoService;	}
	public void setClientePrazoPagamentoService(ClientePrazoPagamentoService clientePrazoPagamentoService) {this.clientePrazoPagamentoService = clientePrazoPagamentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setContratosincronizacaoAAService(ContratosincronizacaoAAService contratosincronizacaoAAService) {this.contratosincronizacaoAAService = contratosincronizacaoAAService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setClienteespecialidadeService(ClienteespecialidadeService clienteespecialidadeService) {this.clienteespecialidadeService = clienteespecialidadeService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	
	/**
	 * M�todo que carrega o cliente com o metodo da entrada e carrega  a lista de clientevendedor
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente loadForEntradaWithDadosPessoa(Cliente cliente){
		Cliente cli = this.loadForEntradaWithoutWhereEmpresa(cliente);
		this.carregaDadosPessoaClienteForEntrada(cli);
		cli.setListaContato(pessoaContatoService.findByPessoa(cliente));
		cli.setListaValecompra(valecompraService.findByCliente(cli));
		return cli;
	}
	
	@SuppressWarnings("unchecked")
	public void carregaDadosPessoaClienteForEntrada(Cliente cliente) {
		if(cliente != null && cliente.getCdpessoa() != null){
			cliente.setListaClientevendedor(SinedUtil.listToSet(clientevendedorService.findByCliente(cliente), Clientevendedor.class));
			cliente.setListDocumentoTipo(clienteDocumentoTipoService.findByCliente(cliente));
			cliente.setListPrazoPagamento(clientePrazoPagamentoService.findByCliente(cliente));
			cliente.setListaClienteEmpresa(clienteEmpresaService.findByCliente(cliente));
			cliente.setListaClienteespecialidade(clienteespecialidadeService.findByCliente(cliente));
		}
	}
	/**
	 * M�todo para carregar os dados de um Cliente pelo c�digo.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#load(Cliente)
	 * @param cdpessoa
	 * @return
	 * @author Flavio Tavares
	 */
	public Cliente load(Integer cdpessoa){
		Cliente cliente = new Cliente();
		cliente.setCdpessoa(cdpessoa);
		return clienteDAO.load(cliente);
	}
	
	/**
	 * M�todo para verificar se existe cliente cadastrado com o cnpj.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#countClienteCnpj(Cliente)
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public boolean existeCnpjCliente(Cliente bean){
		Integer count=clienteDAO.countClienteCnpj(bean);
		return count != 0;
	}
	
	/**
	 * M�todo para verificar se existe cliente cadastrado com o cpf/cnpj.
	 *
	 * @param bean
	 * @return
	 * @author Giovane Freitas
	 */
	public boolean existeCpfCnpjCliente(String cpfCnpj){

		Cliente cliente = null;
		if (Cpf.cpfValido(cpfCnpj)){
			cliente = clienteDAO.findByCpf(new Cpf(cpfCnpj));
		} else if (Cnpj.cnpjValido(cpfCnpj)){
			cliente = clienteDAO.findByCnpj(new Cnpj(cpfCnpj));
		}
		
		return cliente != null;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. Carrega determinados campos de cliente.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#load(Cliente, String)
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Cliente load(Cliente bean, String campos){
		return clienteDAO.load(bean, campos);
	}
	
	/**
	 * Metodo para a criacao do relatorio de clientes
	 * @param filtro
	 * @see br.com.linkcom.sined.geral.service.ClienteService#findForReport(ClienteFiltro)
	 * @return
	 * @author Andre Brunelli
	 */
	public IReport createRelatorioCliente(ClienteFiltro filtro) {
		Report report = new Report("/crm/clientes");
		
		List<Cliente> lista = this.findForReport(filtro);
		
		for (Cliente cliente : lista) {
			if (cliente.getAtivo() == true) {
				cliente.setAtivosrelatorio("Ativo");
			}
			if (cliente.getAtivo() == false) {
				cliente.setAtivosrelatorio("Inativo");
			}
			
			if (cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)) {
				cliente.setTipopessoarel("F");
				if (cliente.getCpf() != null)
					cliente.setCpfcnpj(cliente.getCpf().toString());
			}
			else if (cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)) {
				cliente.setTipopessoarel("J");
				if (cliente.getCnpj() != null)
					cliente.setCpfcnpj(cliente.getCnpj().toString());
			}
		}
		
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * Metodo de referencia para o DAO.
	 * @param filtro
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForReport(ClienteFiltro)
	 * @return
	 * @author Andre
	 */
	public List<Cliente> findForReport(ClienteFiltro filtro){
		if(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado())){
			filtro.setLimitaracessoclientevendedor("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR)) ? Boolean.TRUE : Boolean.FALSE);
		}else {
			filtro.setLimitaracessoclientevendedor(false);
		}
		return clienteDAO.findForReport(filtro);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForPropostaReport
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findForPropostaReport(String whereIn) {
		return clienteDAO.findForPropostaReport(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param q
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findAtivosAutocompleteByIdentificador(String q){
		return clienteDAO.findAtivosAutocompleteByIdentificador(q);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * 
	 * @param q
	 * @return
	 * @author Dhiego Morais
	 */
	public List<Cliente> findClientesNomeCpfCnpjAtivos(String q) {
		return clienteDAO.findClientesNomeCpfCnpjAtivos(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificadorWithContrato(String q){
		return clienteDAO.findAutocompleteByNomeOrIdentificadorWithContrato(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificador(String q){
		return clienteDAO.findAutocompleteByNomeOrIdentificador(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificadorOrCpfOrCnpj(String q){
		return clienteDAO.findAutocompleteByNomeOrIdentificadorOrCpfOrCnpj(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public List<Cliente> findAutocompleteByRazaosocial(String q){
		return clienteDAO.findAutocompleteByRazaosocial(q);
	}
	
	public List<Cliente> findAutocompleteByRazaosocialWithContrato(String q){
		return clienteDAO.findAutocompleteByRazaosocialWithContrato(q);
	}
	
	public List<Cliente> findAutocompleteByNomeRazaosocial (String q){
		return clienteDAO.findAutocompleteByNomeRazaosocial(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAutocompleteByNomeIdentificador(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findAutocompleteByNomeIdentificador(String q){
		return clienteDAO.findAutocompleteByNomeIdentificador(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAllAutocompleteByNomeIdentificadorRazaoSocial(String q)
	 *
	 * @param q
	 * @return
	 * @author Filipe Santos
	 */
	public List<Cliente> findAllAutocompleteByNomeIdentificador(String q){
		return clienteDAO.findAllAutocompleteByNomeIdentificador(q);
	}
	
	public List<Cliente> findAutocompleteByNomeIdentificadorCnpj(String q){
		return clienteDAO.findAutocompleteByNomeIdentificadorCnpj(q);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findAtivosAutocomplete(String q){
		return clienteDAO.findAtivosAutocomplete(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAtivosEClienteDoUsuarioAutocomplete(String q, Usuario usuario)
	*
	* @param q
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public List<Cliente> findAtivosEClienteDoUsuarioAutocomplete(String q){
		Usuario usuario =  usuarioService.findForVerificarRestricaoClientevendedor(SinedUtil.getUsuarioLogado());
		return clienteDAO.findAtivosEClienteDoUsuarioAutocomplete(q, usuario, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
	 * Gera o Resource com os bytes dos relat�rios de todos os clientes.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ClienteService#findForReportCompleto
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public IReport createRelatorioClienteCompleto(WebRequestContext request) throws Exception {
		String contatosSelecionados = (String) request.getSession().getAttribute("lista_contatos_selecionados_pdf");
		request.getSession().removeAttribute("lista_contatos_selecionados_pdf");
		
		String whereIn = request.getParameter("selectedItens");
		if (StringUtils.isEmpty(whereIn) && StringUtils.isEmpty(contatosSelecionados)) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Report report = new Report("/crm/clientecompleto");
		List<ClienteCompletoReportBean> lista;
		if(StringUtils.isNotEmpty(whereIn)){		
			lista = this.findForReportCompleto(whereIn);
		}else {
			lista = this.findForReportCompleto(contatosSelecionados);
		}
		report.setDataSource(lista);
		
		return report;
		
//		MergeReport mergeReport = new MergeReport("clientecompleto_"+SinedUtil.datePatternForReport()+".pdf");
//		
//		Report report = null;
//		
//		List<Cliente> listaCliente = this.findForReportCompleto(whereIn);
//		
//		for (Cliente cliente : listaCliente) {
//			report = new Report("/crm/clientecompleto");
//			
//			report.addParameter("CLIENTE", cliente.getNome());
//			report.addParameter("CATEGORIA", cliente.getCategorias());
//			
//			report.setDataSource(cliente.getListaContato());
//			
//			report.addParameter("TITULO", "RELAT�RIO COMPLETO DE CLIENTE");
//			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
//			report.addParameter("DATA", new Date(System.currentTimeMillis()));
//						
//			Empresa empresa = empresaService.loadPrincipal();
//			report.addParameter("EMPRESA", empresa.getNome());
//			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
//			
//			mergeReport.addReport(report);
//		}
//
//		return mergeReport.generateResource();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ClienteDAO#findForReportCompleto(String)
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<ClienteCompletoReportBean> findForReportCompleto(String whereIn) {
		String whereInEmpresaUsuario = null;
		Usuario usuario = usuarioService.carregaUsuarioWithEmpresa(SinedUtil.getUsuarioLogado());
		if(usuario != null && usuario.getListaUsuarioempresa() != null && !usuario.getListaUsuarioempresa().isEmpty()){
			whereInEmpresaUsuario = SinedUtil.listAndConcatenate(usuario.getListaUsuarioempresa(), "empresa.cdpessoa", ",");
		}
		
		return clienteDAO.findForReportCompleto(whereIn, whereInEmpresaUsuario);
	}
	
	/**
	 * Renorna uma lista de cliente contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Cliente> findDescricao(String whereIn) {
		return clienteDAO.findDescricao(whereIn);
	}

	/**
	 * Retorna os nomes dos Clientes, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 *  
	 * @see #findDescricao(String)
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeClientes(List<ItemDetalhe> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(lista, "cliente.cdpessoa", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);
		
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/**
	 * Carrega o cliente associado ao projeto. Se nenhum projeto for passado,
	 * retorna todos os clientes.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#carregarClienteDoProjeto(Projeto)
	 * @param projeto
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Cliente> carregarClienteDoProjeto(Projeto projeto) {
		return clienteDAO.carregarClienteDoProjeto(projeto);
	}
	
	/**
	 * Carrega um cliente com os seguintes dados:
	 * 	- Raz�o social
	 * 	- CNPJ/CPF
	 * 	- Inscri��o Estadual
	 * 	- Todos os endere�os associados (logradouro, numero, complemento, bairro, munic�pio, UF, CEP)
	 * 	- Todos os telefones associados
	 * 
	 * @param cliente
	 * @return
	 * @author Hugo Ferreira
	 */
	public Cliente carregarDadosCliente(Cliente cliente) {
		if(cliente.getIncidiriss()==null)
			cliente.setIncidiriss(Boolean.FALSE);
		return clienteDAO.carregarDadosCliente(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#carregarInfNotaCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente carregarInfNotaCliente(Cliente cliente) {
		return clienteDAO.carregarInfNotaCliente(cliente);
	}
	
	/**
	 * Retorna uma lista de clientes que est�o vinculados a pelo menos um or�amento.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findWithOrcamentoForFlex
	 * @return lista de Cliente
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Cliente> findWithOrcamentoForFlex() {
		return clienteDAO.findWithOrcamentoForFlex();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForComboFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findForComboFlex() {
		return clienteDAO.findForComboFlex();
	}
	
	/* singleton */
	private static ClienteService instance;
	public static ClienteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClienteService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#loadForBoleto
	 * 
	 * @param cdpessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Cliente loadForBoleto(Integer cdpessoa) {
		return clienteDAO.loadForBoleto(cdpessoa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findListagem
	 * 
	 * @param whereIn
	 * @param orderby
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findListagem(String whereIn, String orderby, boolean asc){
		return clienteDAO.findListagem(whereIn, orderby, asc);
	}
	
	/**
	 * Cria o relat�rio de etiquetas de acordo com o filtro preenchido na tela. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#findForEtiqueta
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @throws Exception 
	 */
	public IReport createRelatorioEtiqueta(VwclienteetiquetaFiltro filtro, String whereIn) throws Exception {
		Report report = new Report("crm/relEtiqueta" + filtro.getTipoetiqueta().getSufix());


		if(filtro.getTipoetiqueta().equals(Tipoetiqueta.ETIQUETA_9X115)) { 

			List<Cliente> listaCliente;
			
			if (filtro.getIsSelectedItensCdCliente() != null && !filtro.getIsSelectedItensCdCliente()) {
				StringBuilder whereInAjustado = new StringBuilder();
				
				for (String id : whereIn.split(",")) {
					whereInAjustado.append(!"".equals(whereInAjustado.toString()) ? "," : "").append("'").append(id).append("'");
				}
	
				List<Integer> listaWhereIn = vwclienteetiquetaService.novoWhereInCdPessoa(whereInAjustado.toString());
				String novoWhereIn = StringUtils.join(listaWhereIn.iterator(), ",");
				listaCliente = clienteService.findClienteToEtiquetaBean(novoWhereIn);
			} else {
				listaCliente = clienteService.findClienteToEtiquetaBean(whereIn);
			}

			List<ClienteEtiqueta> listaClienteEtiqueta = new ArrayList<ClienteEtiqueta>();
			ClienteEtiqueta clienteEtiqueta;

			Empresa empresa = empresaService.loadForEntrada(empresaService.loadPrincipal());

			for (Cliente cliente : listaCliente) {
				clienteEtiqueta = new ClienteEtiqueta();
				if (empresa != null) {
					clienteEtiqueta.setLogomarca_principal_sistema(SinedUtil.getLogoURLForReport(empresa));
					clienteEtiqueta.setNomeEmpresa(empresa.getRazaosocialOuNome() != null ? empresa.getRazaosocialOuNome() : "");
					clienteEtiqueta.setRazao_social(empresa.getRazaoSocial() != null ? empresa.getRazaoSocial() : "");
				}

				clienteEtiqueta.setCpf(cliente.getCpf() != null ? cliente.getCpf().toString() : null);
				clienteEtiqueta.setCnpj(cliente.getCnpj() != null ? cliente.getCnpj().toString() : null);
				clienteEtiqueta.setData_de_nascimento(cliente.getDtnascimento() != null ? SinedDateUtils.toString(cliente.getDtnascimento()) : "");
				clienteEtiqueta.setEstado_civil(cliente.getEstadocivil() !=null ? cliente.getEstadocivil().getNome() : "");
				clienteEtiqueta.setCliente_profissao(cliente.getClienteprofissao() != null ? cliente.getClienteprofissao().getNome() : "");
				clienteEtiqueta.setNomea(cliente.getNome());
				if (!cliente.getListaTelefone().isEmpty()) {
					StringBuilder listaTelefone = new StringBuilder();
					for (Telefone telefone : cliente.getListaTelefone()) {
						listaTelefone.append(telefone.getTelefone()).append(" / ");
					}
					listaTelefone.delete(listaTelefone.length() - 3, listaTelefone.length());
					clienteEtiqueta.setListaTelefone(listaTelefone.toString());
				} else {
					clienteEtiqueta.setListaTelefone("");
				}
				clienteEtiqueta.setIdentificador(cliente.getIdentificador() != null ? cliente.getIdentificador() : "");
				clienteEtiqueta.setSexo(cliente.getSexo() != null ? cliente.getSexo().getNome() : "");

				listaClienteEtiqueta.add(clienteEtiqueta);
			}
			report.setDataSource(listaClienteEtiqueta);
			return report;
			
		} else {

			StringBuilder whereInAjustado = new StringBuilder();
			for(String id : whereIn.split(",")){
				whereInAjustado
				.append(!"".equals(whereInAjustado.toString()) ? "," : "")
				.append("'")
				.append(id).append("'");
			}

			List<Integer> novaListaWhereIn = vwclienteetiquetaService.novoWhereIn(whereInAjustado.toString());

			String novoWhereIn = StringUtils.join(novaListaWhereIn.iterator(), ",");

			List<Endereco> listaEndereco = enderecoService.findForEtiqueta(novoWhereIn);

			List<ClienteEtiqueta> listaClienteEtiqueta = new ArrayList<ClienteEtiqueta>();
			ClienteEtiqueta clienteEtiqueta = null;

			for (Endereco endereco : listaEndereco) {
				clienteEtiqueta = new ClienteEtiqueta();

				List<Contato> listaContato = contatoService.findListIdcontatotipo(endereco.getPessoa(), filtro.getContatotipo());		

				if(endereco.getCaixapostal() == null)
					clienteEtiqueta.setBairro(endereco.getBairro());
				else
					clienteEtiqueta.setBairro(endereco.getBairro() +" "+ endereco.getCaixapostal());
				clienteEtiqueta.setCep(endereco.getCep() != null ? endereco.getCep().toString() : null);
				clienteEtiqueta.setCidade(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : null);
				clienteEtiqueta.setUf(endereco.getMunicipio() != null ? endereco.getMunicipio().getUf().getSigla() : null);
				clienteEtiqueta.setNomea((filtro.getContatotipo() != null ? "A/C " : "") + endereco.getPessoa().getNome());						
				clienteEtiqueta.setEndereco(endereco.getLogradouroCompleto());
				clienteEtiqueta.setPontoreferencia(endereco.getPontoreferencia()!=null?endereco.getPontoreferencia():"");			
				if(listaContato != null && !listaContato.isEmpty()){
					for(Contato contato : listaContato){
						if(contato.getContatotipo() != null && filtro.getContatotipo() != null){
							if(contato.getContatotipo().getCdcontatotipo().equals(filtro.getContatotipo().getCdcontatotipo())){					
								clienteEtiqueta.setNomecontato("A/C " + contato.getNome());
								break;
							}
						}
					}				
				}			
				listaClienteEtiqueta.add(clienteEtiqueta);			
			}

			if(filtro.getApartircoluna() != null && 
					filtro.getApartirlinha() != null && 
					filtro.getTipoetiqueta().getColuna() > 0 && 
					filtro.getTipoetiqueta().getLinha() > 0){
				Integer colunas = filtro.getTipoetiqueta().getColuna();
				Integer numeroadicionar = 0;
				if(filtro.getApartirlinha() > 1){
					for (int i = 0; i < (filtro.getApartirlinha() - 1); i++) {
						numeroadicionar += colunas;
					}
				}
				if(filtro.getApartircoluna() > 1){
					for (int i = 0; i < (filtro.getApartircoluna() - 1); i++) {
						numeroadicionar += 1;
					}
				}
				for (int i = 0; i < numeroadicionar; i++) {
					listaClienteEtiqueta.add(0,new ClienteEtiqueta());
				}
			}


			if(filtro.getTipoetiqueta().equals(Tipoetiqueta.COMUNICADO)){
				Empresa empresa = empresaService.loadForEntrada(empresaService.loadPrincipal());
				String nomeEmp ="";
				String razaoEmp ="";
				String enderecoEmp ="";
				String bairroEmp ="";
				String municipioEmp ="";
				String ufEmp ="";
				String cepEmp ="";
				//setando dados de endere�o da empresa principal
				if (empresa != null && empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
					if (empresa.getRazaosocialOuNome() != null)
						razaoEmp += empresa.getRazaosocialOuNome();
					if (empresa.getListaEnderecoLIST().get(0).getLogradouro() != null)
						enderecoEmp += empresa.getListaEnderecoLIST().get(0).getLogradouro() +" ";
					if (empresa.getListaEnderecoLIST().get(0).getNumero() != null)
						enderecoEmp += empresa.getListaEnderecoLIST().get(0).getNumero()+" ";
					if (empresa.getListaEnderecoLIST().get(0).getComplemento() != null)
						enderecoEmp += empresa.getListaEnderecoLIST().get(0).getComplemento()+" ";

					if (empresa.getListaEnderecoLIST().get(0).getBairro() != null)
						bairroEmp += empresa.getListaEnderecoLIST().get(0).getBairro();
					if (empresa.getListaEnderecoLIST().get(0).getMunicipio() != null){
						municipioEmp += empresa.getListaEnderecoLIST().get(0).getMunicipio().getNome();
						ufEmp += empresa.getListaEnderecoLIST().get(0).getMunicipio().getUf().getSigla();
					}
					if (empresa.getListaEnderecoLIST().get(0).getCep() != null)
						cepEmp += empresa.getListaEnderecoLIST().get(0).getCep();					
					if (empresa.getListaEnderecoLIST().get(0).getCaixapostal() != null)
						bairroEmp += " "+empresa.getListaEnderecoLIST().get(0).getCaixapostal();					
				}

				report.addParameter("NOMEEMP", nomeEmp);
				report.addParameter("RAZAOEMP", razaoEmp);
				report.addParameter("ENDERECOEMP", enderecoEmp);
				report.addParameter("BAIRROEMP", bairroEmp);
				report.addParameter("MUNICIPIOEMP", municipioEmp);
				report.addParameter("UFEMP", ufEmp);
				report.addParameter("CEPEMP", cepEmp);
			}
			report.addParameter("FORMATADOR", new MessageFormat("{0}\n{1}\n{2}\n{3}\n{4} {5} {6}\n{7} {8}"));
			report.setDataSource(listaClienteEtiqueta);
			return report;
		}
	}
	
	/**
	 * M�todo que faz listagem padr�o do flex
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findForListagemFlex(ClienteFiltro clienteFiltro) {
		clienteFiltro.setPageSize(Integer.MAX_VALUE);
		return clienteDAO.findForListagem(clienteFiltro).list();
	}
	
	public List<Cliente> getClientesByEmpresa(Empresa empresa) {
		return clienteDAO.getClientesByEmpresa(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findClientesForBuscaGeral(String busca) {
		return clienteDAO.findClientesForBuscaGeral(busca);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findClientesAniversariantesDoMesFlex
	 * 
	 * @param mes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findClientesAniversariantesDoMesFlex(Integer mes){
		return clienteDAO.findClientesAniversariantesDoMesFlex(mes);
	}
	
	/**
	 * M�todo com ref�ncia no DAO
	 * @param whereLike
	 * @return
	 * @author Dhiego Morais
	 */
	public List<Cliente> findClientesNomeCnpj(String whereLike) {
		return clienteDAO.findClientesNomeCnpj(whereLike);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#insertSomenteCliente
	 *
	 * @param cliente
	 * @author Rodrigo Freitas
	 */
	public void insertSomenteCliente(Cliente cliente) {
		clienteDAO.insertSomenteCliente(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param listaClientes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findClientesContaReceberAtrasadaOuRestricoesEmAberto(List<Cliente> listaClientes) {
		String whereIn = CollectionsUtil.listAndConcatenate(listaClientes,"cdpessoa",",");
		return clienteDAO.findClientesContaReceberAtrasadaOuRestricoesEmAberto(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findClientesContaReceberAtrasadaOuRestricoesEmAberto(Cliente cliente) {
		return clienteDAO.findClientesContaReceberAtrasadaOuRestricoesEmAberto(cliente.getCdpessoa().toString());
	}
	
	/**
	 * M�todo que carrega restri��es do cliente para tela de agenda servi�o FLEX
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cliente carregaRestricoesPendenciasCliente(Cliente cliente){
		Cliente clienteAux = carregaRestricoesCliente(cliente);
		if(clienteAux == null)
			clienteAux = cliente;
		
		clienteAux.setPossuiContasAtrasadas(contareceberService.clientePossuiContasAtrasadas(cliente));
		return clienteAux; 
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Cliente carregaRestricoesCliente(Cliente cliente) {
		return clienteDAO.carregaRestricoesCliente(cliente);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param cnpj
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByCnpj(Cnpj cnpj) {
		return clienteDAO.findByCnpj(cnpj);
	}
	
	public Cliente findByCnpjSemEmpresa(Cnpj cnpj) {
		return clienteDAO.findByCnpjSemEmpresa(cnpj);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param cpf
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByCpf(Cpf cpf) {
		return clienteDAO.findByCpf(cpf);
	}
	
	public Cliente findByCpfSemEmpresa(Cpf cpf) {
		return clienteDAO.findByCpfSemEmpresa(cpf);
	}
	
	public boolean verificaOutrosRegistros(String itens) {
		return clienteDAO.verificaOutrosRegistros(itens);
	}
	
	/**
	 * M�todo respons�vel por carregar os dados necess�rio para o relat�rio Hist�rico de cliente
	 * @param selectedItens, filtro
	 * @return
	 * @author Taidson
	 * @since 26/08/2010
	 */ 
	public IReport createRelatorioHistoricoCliente(String selectedItens, VwclientehistoricoFiltro filtro) {
		Report report = new Report("/crm/historicoCliente");
		
		List<Cliente> lista = this.listaHistoricoCliente(selectedItens);
		
		this.makeDadosRelatorioClientehistorico(lista, filtro);
		
		report.addSubReport("SUBHISTCLIENTETEL", new Report("/crm/historicoClienteTelSub"));
		
		if(filtro.getExibirRequisicao()!= null  && filtro.getExibirRequisicao()){
			report.addSubReport("SUBREQUISICAO", new Report("/crm/historicoClienteReqSub"));
			report.addSubReport("SUBHISTORICOREQ", new Report("/crm/historicoClienteHistReqSub"));
			report.addSubReport("SUBMATERIALSERVICO", new Report("/crm/historicoClienteMatServSub"));
		}
		if(filtro.getExibirInteracao()!=null && filtro.getExibirInteracao()){
			report.addSubReport("SUBHISTORICOCLIENTE", new Report("/crm/historicoClienteHistSub"));
		}
		if(filtro.getExibirInteracaoVenda()!=null && filtro.getExibirInteracaoVenda()){
			report.addSubReport("SUBHISTORICOVENDA", new Report("/crm/historicoClienteHistVendaSub"));
		}
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * M�todo que ajusta os dados do cliente para o relat�rio
	 *
	 * @param lista
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void makeDadosRelatorioClientehistorico(List<Cliente> lista, VwclientehistoricoFiltro filtro) {
		StringBuilder telefones = new StringBuilder();
		
		for (Cliente cliente : lista) {
			if(filtro.getAtividadetipo() != null && filtro.getAtividadetipo().getCdatividadetipo() != null){
				cliente.setListClientehistorico(clientehistoricoService.findForRelatorioHistoricoCliente(cliente, filtro.getAtividadetipo(), filtro.getEmpresahistorico(), filtro.getDtinteracaoinicio(), filtro.getDtinteracaofim(), filtro.getSituacao()));
			}else if(filtro.getDtinteracaoinicio() != null || filtro.getDtinteracaofim() != null){
				cliente.setListClientehistorico(clientehistoricoService.findForRelatorioHistoricoCliente(cliente, null, filtro.getEmpresahistorico(), filtro.getDtinteracaoinicio(), filtro.getDtinteracaofim(), filtro.getSituacao()));
			}
			List<Requisicao> listaRequisicoes = requisicaoService.historicoCliente(cliente.getCdpessoa().toString(), filtro.getEmpresahistorico(), filtro.getDtinteracaoinicio(), filtro.getDtinteracaofim());
			cliente.setListaClienterequisicao(listaRequisicoes);
			
			if (cliente.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)) {
				if (cliente.getCpf() != null){
					cliente.setCpfcnpj(cliente.getCpf().toString());
					cliente.setListaVendahistorico(vendahistoricoService.findByCpfCnpjCliente(cliente.getCpf().getValue(), Tipopessoa.PESSOA_FISICA, filtro.getDtinteracaoinicio(), filtro.getDtinteracaofim(), filtro.getEmpresahistorico()));
				}
			}
			else if (cliente.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)) {
				if (cliente.getCnpj() != null){
					cliente.setCpfcnpj(cliente.getCnpj().toString());
					cliente.setListaVendahistorico(vendahistoricoService.findByCpfCnpjCliente(cliente.getCnpj().getValue(), Tipopessoa.PESSOA_JURIDICA, filtro.getDtinteracaoinicio(), filtro.getDtinteracaofim(), filtro.getEmpresahistorico()));
				}
			}
			
			if(SinedUtil.isListNotEmpty(cliente.getListaTelefone())){
				for (Telefone telefone : cliente.getListaTelefone()) {
					telefones.append(telefone.getTelefone() + "\\r\\n");
				}
			}
			cliente.setTelefones(telefones.toString());			
		}
	}
	/**
	 * Faz refer�ncia ao DAO.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 26/08/2010
	 */
	public List<Cliente>listaHistoricoCliente(String whereIn){
		List<Cliente> lista = clienteDAO.listaHistoricoCliente(whereIn);
		List<Cliente> listaTelefone = clienteDAO.listaTelefoneCliente(whereIn);
		List<Cliente> listaPessoaCategoria = clienteDAO.listaPessoaCategoria(whereIn);
		List<Cliente> listaClientehistorico = clienteDAO.listaClienteHistorico(whereIn);
		List<Cliente> listaClienterequisicao = clienteDAO.listaRequisicaoCliente(whereIn);
		
		for(int i=0; i<lista.size();i++){
			for(int j=0;j<lista.size();j++){
				if(listaTelefone.get(j)!=null && lista.get(i).getCdpessoa().equals(listaTelefone.get(j).getCdpessoa())){
					lista.get(i).setListaTelefone(listaTelefone.get(j)!=null?listaTelefone.get(j).getListaTelefone():null);
				}
				if(listaPessoaCategoria.get(j)!=null &&  lista.get(i).getCdpessoa().equals(listaPessoaCategoria.get(j).getCdpessoa())){
					lista.get(i).setListaPessoacategoria(listaPessoaCategoria.get(j)!=null?listaPessoaCategoria.get(j).getListaPessoacategoria():null);
				}
				if(listaClientehistorico.get(j)!=null &&  lista.get(i).getCdpessoa().equals(listaClientehistorico.get(j).getCdpessoa())){
					lista.get(i).setListClientehistorico(listaClientehistorico.get(j)!=null?listaClientehistorico.get(j).getListClientehistorico():null);
				}
				if(listaClienterequisicao.get(j)!=null &&  lista.get(i).getCdpessoa().equals(listaClienterequisicao.get(j).getCdpessoa())){
					lista.get(i).setListaClienterequisicao(listaClienterequisicao.get(j)!=null?listaClienterequisicao.get(j).getListaClienterequisicao():null);
				}
			}
		}
		return lista;
	}
	
	public Integer criaCliente(InserirClienteBean bean) {
		Cliente cliente = new Cliente();
		
		this.preencheClienteWebService(cliente, bean, null);
		this.saveOrUpdate(cliente);

		this.criaContatoClienteWebService(cliente, bean);
		this.criaCategoriaClienteWebService(cliente, bean);
		this.criaTelefonesClienteWebService(cliente, bean);
		this.criaEnderecoClienteWebService(cliente, bean);
		
		return cliente.getCdpessoa();
	}
	
	public Integer criaClienteJuridico(InserirClienteBean bean) {
		if(!Cnpj.cnpjValido(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getCnpj()))){
			throw new SinedException("N�o foi poss�vel converter " + bean.getCnpj() + " para um CNPJ v�lido.");
		}
		
		Cnpj cnpj = new Cnpj(bean.getCnpj());
		
		Cliente cliente = this.findByCnpjSemEmpresa(cnpj);
		
		boolean criadoPeloEcompleto = false;
		if(cliente == null){
			cliente = new Cliente();
			Pessoa pessoa = pessoaService.findPessoaByCnpj(cnpj);
			
			if(pessoa != null && pessoa.getCdpessoa() != null){
				cliente.setCdpessoa(pessoa.getCdpessoa());
				this.insertSomenteCliente(cliente);
			}
			if(bean.isFromEcompleto()){
				criadoPeloEcompleto = true;
			}
			
		}else {
			cliente = this.loadForEntradaWithDadosPessoa(cliente);
		}
			
		this.preencheClienteWebService(cliente, bean, Tipopessoa.PESSOA_JURIDICA);
		this.saveOrUpdate(cliente);
		if(criadoPeloEcompleto){
			Clientehistorico historico = new Clientehistorico();
			historico.setObservacao("Criado via integra��o com E-completo");
			historico.setCliente(cliente);
			clientehistoricoService.saveOrUpdate(historico);
		}
		this.setarListaClienteEmpresa(cliente);

		this.criaContatoClienteWebService(cliente, bean);
		this.criaCategoriaClienteWebService(cliente, bean);
		this.criaTelefonesClienteWebService(cliente, bean);
		this.criaEnderecoClienteWebService(cliente, bean);
		
		return cliente.getCdpessoa();
	}

	public Integer criaClienteFisica(InserirClienteBean bean) {
		if(!Cpf.cpfValido(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getCpf()))){
			throw new SinedException("N�o foi poss�vel converter " + bean.getCpf() + " para um CPF v�lido.");
		}
		
		Cpf cpf = new Cpf(bean.getCpf());
		boolean criadoPeloEcompleto = false;
		Cliente cliente = this.findByCpfSemEmpresa(cpf);
		if(cliente == null){
			cliente = new Cliente();
			Pessoa pessoa = pessoaService.findPessoaByCpf(cpf);
			
			if(pessoa != null && pessoa.getCdpessoa() != null){
				cliente.setCdpessoa(pessoa.getCdpessoa());
				this.insertSomenteCliente(cliente);
			}
			
			if(bean.isFromEcompleto()){
				criadoPeloEcompleto = true;
				
			}
		} else {
			cliente = this.loadForEntradaWithDadosPessoa(cliente);
		}
		
		this.preencheClienteWebService(cliente, bean, Tipopessoa.PESSOA_FISICA);
		this.saveOrUpdate(cliente);
		if(criadoPeloEcompleto){
			Clientehistorico historico = new Clientehistorico();
			if("ECOMPLETO".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE))){
				historico.setObservacao("Criado via integra��o com E-completo");
			}else{
				historico.setObservacao("Criado via integra��o com E-completo");
			}
			
			historico.setCliente(cliente);
			clientehistoricoService.saveOrUpdate(historico);
		}
		this.setarListaClienteEmpresa(cliente);

		this.criaContatoClienteWebService(cliente, bean);
		this.criaCategoriaClienteWebService(cliente, bean);
		this.criaTelefonesClienteWebService(cliente, bean);
		this.criaEnderecoClienteWebService(cliente, bean);
		
		return cliente.getCdpessoa();
	}
	
	/**
	 * Faz refer�nicia ao DAO.
	 * @return
	 * @author Taidson
	 * @since 09/11/2010
	 */
	public List<Cliente> findAllAtivos(){
		return clienteDAO.findAllAtivos();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param pessoa
	 * @return
	 * @author Taidson
	 * @since 05/01/2011
	 */
	public Cliente carregaDadosResponsavelFinanceiro(Pessoa pessoa) {
		return 	clienteDAO.carregaDadosResponsavelFinanceiro(pessoa);
	}
	
	/**
	 * M�todo que cria identificador para usu�rio
	 * 
	 * @author Tom�s Rabelo
	 */
	public String geraIdentificador() {
		String ultimo = parametrogeralService.getValorPorNome("ProximoIdentificadorCliente");
		String newNum = "";
		if(ultimo == null){
			newNum = "1";
		}else {
			String ultimoNum = ultimo+"";
			Long nextNum = Long.valueOf(ultimoNum);
			/*
			if(ultimoNum.length() > 1)
				nextNum = Long.valueOf(ultimoNum.substring(0, ultimoNum.length()-1));
			else
				nextNum = Long.valueOf(ultimoNum);
			*/
			nextNum++;
			newNum = nextNum+""+SinedUtil.modulo11(nextNum+"");
		}
		return newNum;
	}
	
	/**
	 * M�todo que cria identificador para usu�rio sem o digito verificador
	 * 
	 * @author Marden Silva
	 */
	public String geraIdentificadorSemDV() {
		String ultimo = parametrogeralService.getValorPorNome("ProximoIdentificadorCliente");
		String newNum = "";
		if(ultimo == null || ultimo.equals("")){
			newNum = "1";
		}else {
			String ultimoNum = ultimo;
			Long nextNum = Long.valueOf(ultimoNum);
			newNum = nextNum+"";
		}
		return newNum;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	
	public String getUltimoIdentificador() {
		return clienteDAO.getUltimoIdentificador();
	} */
	
	public Resource prepararArquivoClienteCSV(ClienteFiltro filtro) {
		List<Cliente> listaCliente = this.findForReport(filtro);
		
		StringBuilder csvCabecalho = new StringBuilder("\"C�digo\";\"Identificador/Nome\";\"Pessoa\";\"CPF/CNPJ\";\"Telefones\";\"E-Mail\";\"Categorias\";\"RG\";\"Data Nascimento\";");
		StringBuilder csvLinhas = new StringBuilder();
		String modeloColEndereco = "\"� Logradouro\";\"� N�mero \";\"� Complemento\";\"� Bairro\";\"� Munic�pio\";\"� UF\";\" � CEP\";";
		int qtMaxEnderecos = 0;
		for (Cliente cliente : listaCliente) {
			csvLinhas.append("\"" + (cliente.getCdpessoa() != null ? cliente.getCdpessoa() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getNome() != null ? cliente.getIdentificadorNome() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getTipopessoa() != null ? cliente.getTipopessoa() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getCpfCnpj() != null ? cliente.getCpfCnpj() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getTelefonesSemQuebraLinha() != null ? cliente.getTelefonesSemQuebraLinha() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getEmail() != null ? cliente.getEmail() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getCategorias() != null ? cliente.getCategorias() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getRg() != null ? cliente.getRg() : ""));
			csvLinhas.append("\";");
			csvLinhas.append("\"" + (cliente.getDtnascimento() != null ? cliente.getDtnascimento() : ""));
			csvLinhas.append("\";");
			
			for (Endereco endereco : cliente.getListaEndereco()) {
				csvLinhas.append("\"" + (endereco.getLogradouro() != null ? endereco.getLogradouro().replaceAll("\"", "\"\"") : ""));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getNumero() != null ? endereco.getNumero().replaceAll("\"", "\"\"") : ""));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getComplemento() != null ? endereco.getComplemento().replaceAll("\"", "\"\"") : ""));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getBairro() == null || endereco.getBairro().isEmpty() ? "" :  endereco.getBairro().replaceAll("\"", "\"\"")));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getMunicipio() == null || endereco.getMunicipio().getNome() == null ? "" : endereco.getMunicipio().getNome().replaceAll("\"", "\"\"")));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getMunicipio() == null || endereco.getMunicipio().getUf() == null || endereco.getMunicipio().getUf().getSigla()== null ? "" : endereco.getMunicipio().getUf().getSigla().replaceAll("\"", "\"\"")));
				csvLinhas.append("\";");
				csvLinhas.append("\"" + (endereco.getCep() != null ? endereco.getCep() : ""));
				csvLinhas.append("\";");
				
			}
			if(cliente.getListaEndereco().size()>qtMaxEnderecos)
				qtMaxEnderecos=cliente.getListaEndereco().size();
			
			//csvLinhas.append(cliente.getEnderecosSemQuebraLinha() != null ? cliente.getEnderecosSemQuebraLinha() : "");
			csvLinhas.append("\n");
		}
		for(int i=1; i <= qtMaxEnderecos;i++)
			csvCabecalho.append(modeloColEndereco.replaceAll("�", " "+(i)+"�"+" "));
		csvCabecalho.append("\n");
		
		Resource resource = new Resource("text/csv", "clientes_" + SinedUtil.datePatternForReport() + ".csv", csvCabecalho.append(csvLinhas).toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo que retorna o identificador do cliente sem o DV
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String getIdentificadorSemDV(Cliente bean) {
		if(bean.getIdentificador() != null && !bean.getIdentificador().equals("") && bean.getIdentificador().length() > 1)
			return bean.getIdentificador().substring(0, bean.getIdentificador().length()-1);
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param identificador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeIdentificadorCliente(String identificador, Integer cdpessoa) {
		return clienteDAO.existeIdentificadorCliente(identificador, cdpessoa);
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return clienteDAO.findForArquivoGerencial();
	}
	
		
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ClienteDAO#findClienteAtivosNomeRazaosocial(String q)
	 * 
	 * @param q
	 * @return
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocial(String q){
		return clienteDAO.findClienteAtivosNomeRazaosocial(q);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findByNome(String nome)
	 *
	 * @param nome
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByNome(String nome) {
		return clienteDAO.findByNome(nome);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro) {
		return clienteDAO.findForDominiointegracaoCadastro(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#isUsuarioPertenceRestricaoclientevendedor(Cliente cliente, Usuario usuario)
	 *
	 * @param cliente
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isUsuarioPertenceRestricaoclientevendedor(Cliente cliente, Usuario usuario) {
		return clienteDAO.isUsuarioPertenceRestricaoclientevendedor(cliente, usuario, false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#isUsuarioPertenceRestricaoclientevendedor(Cliente cliente, Usuario usuario)
	 *
	 * @param cliente
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isUsuarioPertenceRestricaoclientevendedorForCliente(Cliente cliente, Usuario usuario, Boolean limitarAcessoClienteVendedor) {
		return clienteDAO.isUsuarioPertenceRestricaoclientevendedor(cliente, usuario, limitarAcessoClienteVendedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findClienteAtivosNomeRazaosocialEClienteDoUsuario(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocialEClienteDoUsuario(String q){
		Usuario usuario =  usuarioService.findForVerificarRestricaoClientevendedor(SinedUtil.getUsuarioLogado());
		return clienteDAO.findClienteAtivosNomeRazaosocialEClienteDoUsuario(q, usuario);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findClienteAtivosNomeRazaosocialEClienteDoUsuario(String q)
	 *
	 * @param q
	 * @return
	 * @author Filipe Santos
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuario(String q){
		return clienteDAO.findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuario(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<ClienteOfflineJSON> findForPVOffline() {
		List<ClienteOfflineJSON> lista = new ArrayList<ClienteOfflineJSON>();
		for(Cliente c : clienteDAO.findForPVOffline())
			lista.add(new ClienteOfflineJSON(c));
		
		return lista;
	}
	
	public List<ClienteRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	public List<ClienteRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClienteRESTModel> lista = new ArrayList<ClienteRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			List<Cliente> listaCliente = clienteDAO.findForAndroid(whereIn);
			if(SinedUtil.isListNotEmpty(listaCliente)){
				preencherSaldoValecompra(listaCliente);
				preencherSaldoAntecipacao(listaCliente);
				for(Cliente c : listaCliente)
					lista.add(new ClienteRESTModel(c));
			}
		}
		
		return lista;
	}
	
	/**
	* M�todo que preenche o saldo de antecipa��o dos clientes
	*
	* @param lista
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	private void preencherSaldoAntecipacao(List<Cliente> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			List<GenericBean> listaClienteSaldoAntecipacao = documentoService.findSaldoAntecipacao(CollectionsUtil.listAndConcatenate(lista, "cdpessoa", ","), Documentoclasse.OBJ_RECEBER);
			if(SinedUtil.isListNotEmpty(listaClienteSaldoAntecipacao)){
				for(GenericBean genericBean : listaClienteSaldoAntecipacao){
					if(genericBean.getId() != null){
						for(Cliente cliente : lista){
							if(genericBean.getId().equals(cliente.getCdpessoa())){
								cliente.setSaldoAntecipacao(new Money((Double)genericBean.getValue()));
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo que preenche o saldo de vale compra dos clientes
	*
	* @param lista
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	private void preencherSaldoValecompra(List<Cliente> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			List<GenericBean> listaClienteSaldoValecompra = valecompraService.findSaldoValecompra(CollectionsUtil.listAndConcatenate(lista, "cdpessoa", ","));
			if(SinedUtil.isListNotEmpty(listaClienteSaldoValecompra)){
				for(GenericBean genericBean : listaClienteSaldoValecompra){
					if(genericBean.getId() != null){
						for(Cliente cliente : lista){
							if(genericBean.getId().equals(cliente.getCdpessoa())){
								cliente.setSaldoValecompra(new Money((Double)genericBean.getValue()));
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findVendedores(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente findVendedores(Cliente cliente) {
		return clienteDAO.findVendedores(cliente);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 * @param whereIn
	 * @return
	 * @author Thiers Euller
	 */
	public List<Cliente> findByWherein(String whereIn) {
		return clienteDAO.findByWherein(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ClienteDAO#loadForRequisicaoInfo(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente loadForRequisicaoInfo(Cliente cliente) {
		return clienteDAO.loadForRequisicaoInfo(cliente);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ClienteDAO#updateContabilidadeCentralizada()
	 *
	 * @return void
	 * @since 28/06/2012
	 * @author Marden Silva
	 */
	public void updateContabilidadeCentralizada(Cliente cliente){
		clienteDAO.updateContabilidadeCentralizada(cliente);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ClienteDAO#loadForCalculoTributacao(Cliente cliente)
	 * @param cliente
	 * @return Cliente
	 * @since 29/06/2012
	 * @author Marden Silva
	 */
	public Cliente loadForCalculoTributacao(Cliente cliente){
		return clienteDAO.loadForCalculoTributacao(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente) {
		return clienteDAO.carregarTaxapedidovendaCreditolimitecompraByCliente(cliente);
	}
	
	public Cliente carregarCreditolimitecompraByCnpj(VerificaPendenciaFinanceira bean) {
		return clienteDAO.carregarCreditolimitecompraByCnpj(bean);
	}
	
	/**
	 * Atualiza o CRT do cliente
	 * @param cliente
	 */
	public void updateCrt(Cliente cliente){
		clienteDAO.updateCrt(cliente);
	}
	
	/**
	 * M�todo que retorna o contato respons�vel do cliente, caso exista
	 *
	 * @see br.com.linkcom.sined.geral.service.ContatoService#getListContatoByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Contato getContatoresponsavelByCliente(Cliente cliente){
		Contato contato = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Contato> listaContato = contatoService.getListContatoByCliente(cliente);
			if(listaContato != null && !listaContato.isEmpty()){
				for(Contato c : listaContato){
					if(c.getResponsavelos() != null && c.getResponsavelos()){
						contato  = c;
					}
				}
			}
		}		
		return contato;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findByMatrizCNPJ
	 *
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/11/2012
	 */
	public List<ClienteVO> findByMatrizCNPJ(Cnpj cnpj) {
		return clienteDAO.findByMatrizCNPJ(cnpj);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#loadForMovimentacaocontabil(Pessoa pessoa)
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente loadForMovimentacaocontabil(Pessoa pessoa) {
		return clienteDAO.loadForMovimentacaocontabil(pessoa);
	}
	
	/**
	 * M�todo que preenche o cliente de acordo com o bean recebedido por webService.
	 * 
	 * @param cliente
	 * @param bean
	 * @param tipopessoa
	 * @author Rafael Salvio
	 */
	public void preencheClienteWebService(Cliente cliente, InserirClienteBean bean, Tipopessoa tipopessoa){
		if(bean.getNome() != null && !bean.getNome().equals(""))
			cliente.setNome(bean.getNome());
		
		if(bean.getRazaosocial() != null && !bean.getRazaosocial().equals(""))
			cliente.setRazaosocial(bean.getRazaosocial());
		
		if(bean.getEmail() != null && !bean.getEmail().equals(""))
			cliente.setEmail(bean.getEmail());
		
		if(bean.getCpf() != null && !bean.getCpf().equals(""))
			cliente.setCpf(new Cpf(bean.getCpf()));
		
		if(bean.getCnpj() != null && !bean.getCnpj().equals(""))
			cliente.setCnpj(new Cnpj(bean.getCnpj()));
		
		if(bean.getInscricaomunicipal() != null && !bean.getInscricaomunicipal().equals(""))
			cliente.setInscricaomunicipal(bean.getInscricaomunicipal());
		
		if(bean.getInscricaoestadual() != null && !bean.getInscricaoestadual().equals(""))
			cliente.setInscricaoestadual(bean.getInscricaoestadual());
		
		if(bean.getSite() != null && !bean.getSite().equals(""))
			cliente.setSite(bean.getSite());
		
		if(bean.getRg() != null && !bean.getRg().equals(""))
			cliente.setRg(bean.getRg());
		
		if(bean.getDtnascimento() != null)
			cliente.setDtnascimento(bean.getDtnascimento());
		
		if(tipopessoa != null){
			cliente.setTipopessoa(tipopessoa);
			if(tipopessoa.equals(Tipopessoa.PESSOA_FISICA) && bean.getSexo() != null && !bean.getSexo().equals("")){
				String sexo = bean.getSexo();
				char s = sexo.charAt(0);
				if(s == 'M' || s == 'm'){
					cliente.setSexo(new Sexo(Sexo.MASCULINO));
				} else {
					cliente.setSexo(new Sexo(Sexo.FEMININO));
				}
			}
		} else {
			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		}
		
		String enderecouf = bean.getEnderecouf();
		if(StringUtils.isNotBlank(enderecouf)){
			Uf ufinscricaoestadual = ufService.findBySigla(enderecouf);
			if(ufinscricaoestadual != null){
				cliente.setUfinscricaoestadual(ufinscricaoestadual);
			}
		}
		
		Integer cdgrauinstrucao = bean.getCdgrauinstrucao();
		if(cdgrauinstrucao != null){
			Grauinstrucao grauinstrucao = grauinstrucaoService.load(new Grauinstrucao(cdgrauinstrucao));
			cliente.setGrauinstrucao(grauinstrucao);
		}
		
		Integer cdassociacao = bean.getCdassociacao();
		if(cdassociacao != null){
			Fornecedor fornecedor = fornecedorService.load(new Fornecedor(cdassociacao));
			cliente.setAssociacao(fornecedor);
		}
		
		Integer cdclienteprofissao = bean.getCdclienteprofissao();
		if(cdclienteprofissao != null){
			Clienteprofissao clienteprofissao = clienteprofissaoService.load(new Clienteprofissao(cdclienteprofissao));
			cliente.setClienteprofissao(clienteprofissao);
		}
		
		Integer cdestadocivil = bean.getCdestadocivil();
		if(cdestadocivil != null){
			Estadocivil estadocivil = estadocivilService.load(new Estadocivil(cdestadocivil));
			cliente.setEstadocivil(estadocivil);
		}
		
		Especialidade especialidade = null;
		Integer cdespecialidade = bean.getCdespecialidade();
		String registro = bean.getRegistro();
		if(cdespecialidade != null){
			especialidade = especialidadeService.load(new Especialidade(cdespecialidade));
		}
		
		if(especialidade != null){
			List<Clienteespecialidade> listaClienteespecialidade = cliente.getListaClienteespecialidade();
			boolean achouEspecialidade = false;
			if(listaClienteespecialidade != null && listaClienteespecialidade.size() > 0){
				for (Clienteespecialidade clienteespecialidade : listaClienteespecialidade) {
					Especialidade especialidade_aux = clienteespecialidade.getEspecialidade();
					if(especialidade_aux != null && especialidade_aux.equals(especialidade)){
						achouEspecialidade = true;
						clienteespecialidade.setRegistro(registro);
					}
				}
			}
			
			if(!achouEspecialidade){
				if(listaClienteespecialidade == null){
					listaClienteespecialidade = new ArrayList<Clienteespecialidade>();
				}
				
				Clienteespecialidade clienteespecialidade = new Clienteespecialidade();
				clienteespecialidade.setEspecialidade(especialidade);
				clienteespecialidade.setRegistro(registro);
				listaClienteespecialidade.add(clienteespecialidade);
				
				cliente.setListaClienteespecialidade(listaClienteespecialidade);
			}
		}
		
		if(Tipopessoa.PESSOA_JURIDICA.equals(cliente.getTipopessoa())){
			if("ISENTO".equalsIgnoreCase(cliente.getInscricaoestadual()) || "ISENTA".equalsIgnoreCase(cliente.getInscricaoestadual())){
				cliente.setContribuinteicmstipo(Contribuinteicmstipo.ISENTO);
			}else if(StringUtils.isNotBlank(cliente.getInscricaoestadual())){
				cliente.setContribuinteicmstipo(Contribuinteicmstipo.CONTRIBUINTE);
			}else {
				cliente.setContribuinteicmstipo(Contribuinteicmstipo.NAO_CONTRIBUINTE);
			}
		}else if(Tipopessoa.PESSOA_FISICA.equals(cliente.getTipopessoa())){
			cliente.setContribuinteicmstipo(Contribuinteicmstipo.NAO_CONTRIBUINTE);
		}
	}
	
	/**
	 * M�todo que preenche o contato relacionado ao cliente inserido por webService.
	 * 
	 * @param cliente
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void criaContatoClienteWebService(Cliente cliente, InserirClienteBean bean){
		if(bean.getNomecontato() != null && !bean.getNomecontato().equals("")){
			Contato contato = null;
			PessoaContato pessoaContato = null;
			List<Contato> listaContatos = contatoService.findByPessoa(cliente);
			
			if(listaContatos != null && !listaContatos.isEmpty()){
				for(Contato c : listaContatos){
					if(c.getNome().trim().toUpperCase().equals(bean.getNomecontato().trim().toUpperCase()))
						contato = c;
					break;
				}
			}

			if(contato == null){
				contato = new Contato();
				pessoaContato = new PessoaContato();
				List<PessoaContato> listaPessoaContato = new ArrayList<PessoaContato>();
				
				contato.setNome(bean.getNomecontato());
				pessoaContato.setPessoa(cliente);
				contato.setListaContato(listaPessoaContato);

				contatoService.saveOrUpdate(contato);
			}

			List<Telefone> listaTelefones = telefoneService.findByPessoa(contato);
			if(bean.getTelefonecontato() != null && !bean.getTelefonecontato().equals("")){
				Telefone telefone = null;

				if(listaTelefones != null && !listaTelefones.isEmpty()){
					for(Telefone t: listaTelefones){
						if(t.getTelefone().equals(bean.getTelefonecontato()) && t.getTelefonetipo().equals(Telefonetipo.PRINCIPAL)){
							telefone = t;
							break;
						}
					}
				}

				if(telefone == null){
					telefone = new Telefone();
					telefone.setTelefone(bean.getTelefonecontato());
					telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
					telefone.setPessoa(contato);

					telefoneService.saveOrUpdate(telefone);
				}	
			}

			if(bean.getCelularcontato() != null && !bean.getCelularcontato().equals("")){
				Telefone celular = null;

				if(listaTelefones != null && !listaTelefones.isEmpty()){
					for(Telefone c: listaTelefones){
						if(c.getTelefone().equals(bean.getCelularcontato()) && c.getTelefonetipo().equals(Telefonetipo.CELULAR)){
							celular = c;
							break;
						}
					}
				}

				if(celular == null){
					celular = new Telefone();
					celular.setTelefone(bean.getCelularcontato());
					celular.setTelefonetipo(new Telefonetipo(Telefonetipo.CELULAR));
					celular.setPessoa(contato);

					telefoneService.saveOrUpdate(celular);
				}
			}
		}
	}
	
	/**
	 * M�todo que preenche a categoria relacionada ao cliente inserido por webService.
	 * 
	 * @param cliente
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void criaCategoriaClienteWebService(Cliente cliente, InserirClienteBean bean){
		if(bean.getCdcategoria() != null){		
			Categoria categoria = new Categoria(bean.getCdcategoria());
			
			if(bean.getSubstituircategoria() != null && bean.getSubstituircategoria()){
				pessoacategoriaService.deleteByCliente(cliente);
			}
			if(!pessoacategoriaService.haveCategoriaCliente(cliente, categoria)){
				categoria = categoriaService.load(categoria, "categoria.cdcategoria, categoria.nome");
				
				if(categoria != null){
					Pessoacategoria pessoacategoria = new Pessoacategoria();
					pessoacategoria.setCategoria(categoria);
					pessoacategoria.setPessoa(cliente);
	
					pessoacategoriaService.saveOrUpdate(pessoacategoria);
				}
			}
		}	
	}
	
	/**
	 * M�todo que preenche o telefone/celular relacionado ao cliente inserido por webService.
	 * 
	 * @param cliente
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void criaTelefonesClienteWebService(Cliente cliente, InserirClienteBean bean){
		List<Telefone> listaTelefones = telefoneService.findByPessoa(cliente);
		
		this.compareAndCreateTelefoneForClienteWebService(cliente, listaTelefones, bean.getTelefone(), new Telefonetipo(Telefonetipo.PRINCIPAL));
		this.compareAndCreateTelefoneForClienteWebService(cliente, listaTelefones, bean.getCelular(), new Telefonetipo(Telefonetipo.CELULAR));
		this.compareAndCreateTelefoneForClienteWebService(cliente, listaTelefones, bean.getCelular(), new Telefonetipo(Telefonetipo.COMERCIAL));
		this.compareAndCreateTelefoneForClienteWebService(cliente, listaTelefones, bean.getTelefonecomercial(), new Telefonetipo(Telefonetipo.COMERCIAL));
	}
	
	/**
	 * Verifica se existe o telefone j� cadastrado, caso n�o tenha cadastra um novo telefone.
	 * 	 
	 * @param cliente
	 * @param listaTelefones
	 * @param telefoneNovo
	 * @param telefonetipo
	 * @author Rodrigo Freitas
	 * @since 24/05/2018
	 */
	private void compareAndCreateTelefoneForClienteWebService(Cliente cliente, List<Telefone> listaTelefones, String telefoneNovo, Telefonetipo telefonetipo) {
		if(telefoneNovo != null && !telefoneNovo.equals("")){
			Telefone telefone = null;
			telefoneNovo = br.com.linkcom.neo.util.StringUtils.soNumero(telefoneNovo);
			
			if(listaTelefones != null && !listaTelefones.isEmpty()){
				for(Telefone t: listaTelefones){
					String telefoneBd = t.getTelefone();
					if(telefoneBd != null){
						telefoneBd = br.com.linkcom.neo.util.StringUtils.soNumero(telefoneBd);
						if(telefoneBd.equals(telefoneNovo)){
							telefone = t;
							break;
						}
					}
				}
			}

			if(telefone == null){
				telefone = new Telefone();
				telefone.setTelefone(telefoneNovo);
				telefone.setTelefonetipo(telefonetipo);
				telefone.setPessoa(cliente);

				telefoneService.saveOrUpdate(telefone);
			}	
		}
	}
		
	/**
	 * M�todo que preenche o endereco relacionado ao cliente inserido por webService.
	 * 
	 * @param cliente
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void criaEnderecoClienteWebService(Cliente cliente, InserirClienteBean bean){
		Municipio municipio = null;
		
		if(bean.getEnderecomunicipio() != null && !bean.getEnderecomunicipio().equals("") && bean.getEnderecouf() != null && !bean.getEnderecouf().equals(""))
			municipio = municipioService.findByNomeSigla(bean.getEnderecomunicipio(), bean.getEnderecouf());
		
		if(municipio == null && StringUtils.isNotBlank(bean.getEnderecocep())){
			try {
				Endereco endereco = enderecoService.findAddressWebServiceByCep(bean.getEnderecocep());
				if(endereco != null && endereco.getMunicipio() != null){
					municipio = endereco.getMunicipio();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		if(municipio != null || bean.isPermitirCriarEnderecoSemMunicipio()){
			Endereco endereco = null;
			Cep cep = null;
			if( bean.getEnderecocep() != null && !bean.getEnderecocep().equals(""))
				 cep = new Cep(bean.getEnderecocep());
			
			List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
			if(listaEndereco != null && !listaEndereco.isEmpty()){
				for(Endereco e : listaEndereco){
					if(e.getCep() != null && e.getCep().getValue() != null && cep != null && e.getCep().getValue().equals(cep.getValue())){
						endereco = e;
						break;
					}
					else if(bean.getEnderecologradouro() != null && e.getLogradouro() != null && !bean.getEnderecologradouro().equals("")
								&& bean.getEnderecologradouro().equalsIgnoreCase(e.getLogradouro()) ){
						endereco = e;
						break;
					}
				}
			}
			if(endereco == null){
				endereco = new Endereco();
			
				endereco.setPessoa(cliente);
				endereco.setLogradouro(bean.getEnderecologradouro());
				endereco.setNumero((bean.getEndereconumero() != null ? bean.getEndereconumero() : "") + "");
				endereco.setComplemento(bean.getEnderecocomplemento());
				endereco.setPontoreferencia((bean.getEnderecoreferencia() != null ? bean.getEnderecoreferencia() : "") + "");
				endereco.setBairro(bean.getEnderecobairro());
				if(bean.getEnderecocep() != null){
					endereco.setCep(new Cep(bean.getEnderecocep()));
				}
				endereco.setMunicipio(municipio);

				Integer cdenderecotipo = null;
				try {
					cdenderecotipo = ObjectUtils.coalesce(bean.getEnderecotipo(), parametrogeralService.getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_ENDERECOTIPO));
				} catch (Exception e) {	}
				
				if(cdenderecotipo == null) cdenderecotipo = Enderecotipo.UNICO.getCdenderecotipo();
				
				endereco.setEnderecotipo(new Enderecotipo(cdenderecotipo));
				endereco.setPais(Pais.BRASIL);
	
				enderecoService.saveOrUpdate(endereco);
			}

			if(endereco != null && endereco.getCdendereco() != null){
				bean.setCdendereco(endereco.getCdendereco());
			}
		}	
	}
	
	public boolean haveClienteIdentificadorNome(String identificador, String nome) {
		return clienteDAO.haveClienteIdentificadorNome(identificador, nome);
	}
	
	/**
	 * M�todo que gera o CSV - Hist�rico do cliente
	 *
	 * @param filtro
	 * @param selectedItens
	 * @return
	 * @author Luiz Fernando
	 */
	public Resource gerarRelatorioClientehistoricoCSV(VwclientehistoricoFiltro filtro, String selectedItens) {
		List<Cliente> lista = this.listaHistoricoCliente(selectedItens);
			
		this.makeDadosRelatorioClientehistorico(lista, filtro);
		
		StringBuilder csv = new StringBuilder();
		if(lista != null && !lista.isEmpty()){
			StringBuilder telefones;	
			Integer qtdelinhas;
			Integer contlinha;
			Requisicao requisicao;
			Clientehistorico clientehistorico;
			Vendahistorico vendahistorico;
			int qtderequisicao;
			int qtdeclientehistorico;
			int qtdevendahistorico;
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Cliente cliente : lista) {
				csv.append("Cliente;Email;CPF/CNPJ;Telefone(s);\n");
				
				csv
					.append(cliente.getNome()).append(";")
					.append(cliente.getEmail() != null ? cliente.getEmail() : "").append(";")
					.append(cliente.getCpfOuCnpj() != null ? cliente.getCpfOuCnpj() : "" ).append(";");
					
				if(cliente.getListaTelefone() != null && !cliente.getListaTelefone().isEmpty()){
					telefones = new StringBuilder();
					for(Telefone telefone : cliente.getListaTelefone()){
						telefones
							.append(!telefones.toString().equals("") ? "\n" : "")
							.append(telefone.getTelefone() + (telefone.getTelefonetipo() != null ? " " + telefone.getTelefonetipo().getNome() : ""))
							;
					}
					csv.append("\"" + telefones.toString() + "\"");
				}
				
				csv.append("\n;");
				
				qtderequisicao = 0;
				qtdeclientehistorico = 0;
				qtdevendahistorico = 0;
				qtdelinhas = 0;
				
				csv.append("ORDEM DE SERVI�O;DATA;DESCRI��O;SITUA��O;");
				if(cliente.getListaClienterequisicao() != null && !cliente.getListaClienterequisicao().isEmpty()){
					qtdelinhas = cliente.getListaClienterequisicao().size();
					qtderequisicao = cliente.getListaClienterequisicao().size();
				}
				csv.append("DATA;OBSERVA��O;RESPONS�VEL;TIPO;SITUA��O");
				if(cliente.getListClientehistorico() != null && !cliente.getListClientehistorico().isEmpty()){
					qtdeclientehistorico = cliente.getListClientehistorico().size();
					if(qtdelinhas < qtdeclientehistorico){
						qtdelinhas = qtdeclientehistorico;
					}
				}
				csv.append("DATA;A��O;OBSERVA��O;RESPONS�VEL");
				if(cliente.getListaVendahistorico() != null && !cliente.getListaVendahistorico().isEmpty()){
					qtdevendahistorico = cliente.getListaVendahistorico().size();
					if(qtdelinhas < qtdevendahistorico){
						qtdelinhas = qtdevendahistorico;
					}
				}
				
				for(contlinha = 0; contlinha < qtdelinhas; contlinha++){
					csv.append("\n;");
					if(contlinha < qtderequisicao){
						requisicao = cliente.getListaClienterequisicao().get(contlinha);
						csv
							.append(requisicao.getCdrequisicao()).append(";")
							.append(format.format(requisicao.getDtrequisicao())).append(";")
							.append(requisicao.getDescricao() != null ? "\"" + requisicao.getDescricao() + "\"" : "").append(";")
							.append(requisicao.getRequisicaoestado().getDescricao() != null ? requisicao.getRequisicaoestado().getDescricao() : "").append(";");
					}else {
						csv.append(";;;;");
					}
					if(contlinha < qtdeclientehistorico){
						clientehistorico = cliente.getListClientehistorico().get(contlinha);
						csv
							.append(clientehistorico.getDtaltera() != null ? format.format(clientehistorico.getDtaltera()) : "").append(";")
							.append(clientehistorico.getObservacao() != null ? "\"" + clientehistorico.getObservacao() + "\"" : "").append(";")
							.append(clientehistorico.getUsuarioaltera() != null ? clientehistorico.getUsuarioaltera().getNome() : "").append(";")
							.append(clientehistorico.getAtividadetipo()!=null ? clientehistorico.getAtividadetipo().getNome() : "").append(";")
							.append(clientehistorico.getSituacao()!=null ? clientehistorico.getSituacao().getNome() : "").append(";");
					}else {
						csv.append(";;;;;");
					}
					if(contlinha < qtdevendahistorico){
						vendahistorico = cliente.getListaVendahistorico().get(contlinha);
						csv
							.append(format.format(vendahistorico.getDtaltera())).append(";")
							.append(vendahistorico.getAcao() != null ? "\"" + vendahistorico.getAcao() + "\"" : "").append(";")
							.append(vendahistorico.getObservacao() != null ? "\"" + vendahistorico.getObservacao() + "\"" : "").append(";")
							.append(vendahistorico.getUsuarioaltera().getNome()).append(";");
					}else {
						csv.append(";;;;");
					}
				}
				csv.append("\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "historicocliente" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public List<Cliente> findForEnvioEmailCobranca(Contagerencial contagerencialMensalidade, Projeto projeto, SituacaoContrato[] situacoes) {
		return clienteDAO.findForEnvioEmailCobranca(contagerencialMensalidade, projeto, situacoes);
	}
	
	public List<Cliente> findForIntegracaoEmporium(Integer idCliente) {
		return clienteDAO.findForIntegracaoEmporium(idCliente);
	}
	
	/**
	 * M�todo que verifica se o cnpj de dois clientes tem a mesma matriz
	 *
	 * @param c1
	 * @param c2
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isMatrizCnpjIgual(Cliente c1, Cliente c2){
		boolean retorno = false;
		
		if(c1 != null && c1.getCdpessoa() != null && c2 != null && c2.getCdpessoa() != null){
			String cnpj1 = null;
			String cnpj2 = null;
			if(c1.getCnpj() != null) cnpj1 = c1.getCnpj().getValue();
			if(c2.getCnpj() != null) cnpj2 = c2.getCnpj().getValue();
			
			if(cnpj1 != null && cnpj2 != null){
				try {
					retorno = cnpj1.substring(0, 7).equals(cnpj2.substring(0, 7));
				} catch (Exception e) {
				}
			}
		}
		return retorno;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAutocompleteRestricaoClienteVendedor(String q, Colaborador colaborador)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findAutocompleteRestricaoClienteVendedor(String q){
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(usuario.getRestricaoclientevendedor() == null || !usuario.getRestricaoclientevendedor())
			colaborador = null;
		return clienteDAO.findAutocompleteRestricaoClienteVendedor(q, colaborador, false);
	}
	
	/**
	 * Criar cliente pelo webservice
	 * 
	 * @param bean
	 * @return
	 */
	public Integer criarClienteWebservice(InserirClienteBean bean) {
		Integer cdpessoa = null;
		if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
			cdpessoa = this.criaClienteJuridico(bean);
		} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
			cdpessoa = this.criaClienteFisica(bean);
		} else {
			cdpessoa = this.criaCliente(bean);
		}
		return cdpessoa;
	}
	
	/**
	 * M�todo que retorna o telefone do cliente.
	 * 1) Se o cliente possuir apenas um telefone retorna o telefone
	 * 2) Retorna o telefone do tipo Principal, se houver 2 ou mais, retorna null.
	 * 3) Se n�o tiver, telefone Principal, retorna o Telefone Comercial, se houver 2 ou mais retorna null.
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Telefone getTelefoneForNota(Cliente cliente){
		Telefone telefoneCliente = null;
		if(cliente != null){
			Set<Telefone> listaTelefone = cliente.getListaTelefone();
			
			if(listaTelefone != null && listaTelefone.size() > 0){
				if(listaTelefone.size() == 1){
					telefoneCliente = listaTelefone.iterator().next();
				}else {
					Telefone telefonePrincipal = null;
					Telefone telefoneComercial = null;
					boolean existTelefoneprincipal = false;
					boolean existTelefonecomercial = false;
					
					for (Telefone telefone : listaTelefone) {
						if(telefone.getTelefonetipo() != null && Telefonetipo.PRINCIPAL.equals(telefone.getTelefonetipo().getCdtelefonetipo())){
							if(existTelefoneprincipal){
								telefonePrincipal = null;
								break;
							}
							telefonePrincipal = telefone;
							existTelefoneprincipal = true;
						}else if(telefone.getTelefonetipo() != null && Telefonetipo.COMERCIAL.equals(telefone.getTelefonetipo().getCdtelefonetipo())){
							if(existTelefonecomercial){
								telefoneComercial = null;
								break;
							}
							telefoneComercial = telefone;
							existTelefonecomercial = true;
						}
					}
					
					if(existTelefoneprincipal){
						telefoneCliente = telefonePrincipal;
					}else if(existTelefonecomercial){
						telefoneCliente = telefoneComercial;
					}				
				}
			}
		}
		
		return telefoneCliente;
	}
	
	public Cliente findForContratoRtf(Cliente cliente) {
		return clienteDAO.findForContratoRtf(cliente);
	}
	
	/**
	 * Primeiro este m�todo busca o cliente pelo CNPJ ou CPF dele.
	 * Se n�o achar ele cria um cliente com as informa��es passadas no arquivo de importa��o.
	 * Se achar ele atualiza as informa��es do cliente no sistema.
	 * 
	 *
	 * @param ai
	 * @return cliente
	 * @author Rodrigo Freitas
	 */
	public Cliente findForImportacao(ArquivoImportacao ai) {
		if(ai.getNome_cliente() != null && !ai.getNome_cliente().equals("")){
			InserirClienteBean bean = new InserirClienteBean();
			
			bean.setNome(ai.getNome_cliente());
			bean.setCpf(ai.getCpf_cliente());
			bean.setCnpj(ai.getCnpj_cliente());
			bean.setInscricaomunicipal(ai.getIm_cliente());
			bean.setEmail(ai.getEmail_cliente());
			bean.setTelefone(ai.getTelefone_cliente());
			bean.setEnderecologradouro(ai.getLograouro_cliente());
			bean.setEndereconumero(ai.getNumero_cliente()==null ? null : ai.getNumero_cliente().toString());
			bean.setEnderecocomplemento(ai.getComplemento_cliente());
			bean.setEnderecobairro(ai.getBairro_cliente());
			bean.setEnderecocep(ai.getCep_cliente());
			
			Municipio municipio = municipioService.findByCdibge(ai.getCod_ibge_cliente());
			if(municipio != null){
				bean.setEnderecomunicipio(municipio.getNome());
				bean.setEnderecouf(municipio.getUf().getSigla());
			}
			
			Integer cdcliente = this.criarClienteWebservice(bean);
			return new Cliente(cdcliente, bean.getCdendereco());			
		}
		return null;
	}
	
	public Cliente loadForProcessFromPainelInteracao(Cliente cliente) {
		return clienteDAO.loadForProcessFromPainelInteracao(cliente);
	}
	
	public Cliente loadForTributacao(Cliente cliente) {
		return clienteDAO.loadForTributacao(cliente);
	}
	
	public List<Restricao> carregaRestricoesByCliente(Integer cdpessoa){
		return restricaoDao.carregaRestricoesByCliente(cdpessoa);
	}
	
	public List<Cliente> findClientesComReferencia(String query) {
		return clienteDAO.findClientesComReferencia(query);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAllAutocompleteByNomeIdCpfCnpj(String)
	 *
	 * @param q
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Cliente> findAllAutocompleteByNomeIdCpfCnpj(String q) {
		return clienteDAO.findAllAutocompleteByNomeIdCpfCnpj(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClienteDAO#findAllAutocompleteByAllFields(String)
	 *
	 * @param q
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Cliente> findAllAutocompleteByAllFields(String q) {
		return clienteDAO.findAllAutocompleteByAllFields(q, SinedUtil.getIdEmpresaParametroRequisicaoAutocomplete());
	}
	
	public Resource createAvaliarCliente(String whereIn) throws Exception{
		
		MergeReport mergeReport = new MergeReport("avaliarcliente"+SinedUtil.datePatternForReport()+".pdf");
		
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		listaCliente = this.carregarDadosRelatorioAvaliativo(whereIn);

		Empresa empresa = empresaService.loadPrincipal();
		
		for (Cliente cliente : listaCliente) {
			Report report = new Report("/crm/avaliacaocliente");
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			report.addParameter("TITULO", "AVALIA��ES DE CLIENTES");
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
/**/		report.addParameter("CLIENTE", cliente.getNome());
/**/		report.addParameter("ENDERECO", cliente.getEndereco() != null ? cliente.getEndereco().getLogradouroCompletoComCep() : "");
/**/		report.addParameter("TELEFONES", cliente.getTelefone() != null ? cliente.getTelefone().getTelefone() : "");
/**/		report.addParameter("CNPJCPF", cliente.getCpf() != null ? cliente.getCpf().getValue() : cliente.getCnpj() != null ? cliente.getCnpj().getValue() : "");
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			for (Pessoaquestionario pq : cliente.getListaQuestionario()) {
				pq.setAvaliadorString(TagFunctions.findUserByCd(pq.getCdusuarioaltera()));
			}
			if (cliente.getListaQuestionario().size() > 0 && !cliente.getListaQuestionario().isEmpty()){
				for (Pessoaquestionario pq : cliente.getListaQuestionario()) {
					if (pq.getQuestionario() != null && pq.getQuestionario().getArquivo() != null){
						Arquivo arquivo = null;
						try{
							arquivo = arquivoService.loadWithContents(pq.getQuestionario().getArquivo());
						} catch (Exception e) {
							e.printStackTrace();
						}
						pq.setArquivoString(new String(arquivo.getContent()));
					} else {
						pq.setArquivoString("");
					}
				}
				report.setDataSource(cliente.getListaQuestionario());
				Report subreport = new Report("/crm/avaliacaocliente_subreport");
				report.addSubReport("AVALIACAOCLIENTE_SUBREPORT", subreport);
			}
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	public List<Cliente> carregarDadosRelatorioAvaliativo(String whereIn){
		return clienteDAO.carregarDadosRelatorioAvaliativo(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForFaturalocacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 11/07/2014
	* @author Luiz Fernando
	*/
	public List<Cliente> findForFaturalocacao(String whereIn) {
		return clienteDAO.findForFaturalocacao(whereIn);
	}
	
	/**
	 * M�todo que gera a lista de beans para ser utilizado no template
	 * 
	 * @param whereIn
	 * @return lista
	 * @throws Exception
	 * @author Lucas Costa
	 */
	public LinkedList<EmitirClienteBean> gerarListaDadosForEmitirFichaCliente(String whereIn) throws Exception{
		LinkedList<EmitirClienteBean> lista = new LinkedList<EmitirClienteBean>();
		List<Cliente> listaCliente = clienteDAO.findForEmitirFichaCliente(whereIn);
		if(listaCliente != null && !listaCliente.isEmpty()){
			for(Cliente cliente : listaCliente){
				EmitirClienteBean emitirClienteBean = new EmitirClienteBean ();
				emitirClienteBean.setIdentificador(cliente.getIdentificador());
				emitirClienteBean.setNome(cliente.getNome());
				emitirClienteBean.setNacionalidade(cliente.getNacionalidade() != null ? cliente.getNacionalidade().getDescricao() : "");
				emitirClienteBean.setPessoatratamento(cliente.getPessoatratamento() != null ? cliente.getPessoatratamento().getNome() : "");
				emitirClienteBean.setEmail(cliente.getEmail());
				emitirClienteBean.setNaoconsiderartaxaboleto(cliente.getNaoconsiderartaxaboleto());
				emitirClienteBean.setDiscriminartaxaboleto(cliente.getDiscriminartaxaboleto());
				emitirClienteBean.setDiscriminardescontonota(cliente.getDiscriminardescontonota());
				emitirClienteBean.setTaxapedidovenda(cliente.getTaxapedidovenda());
				emitirClienteBean.setCreditolimitecompra(cliente.getCreditolimitecompra());
				emitirClienteBean.setSite(cliente.getSite());
				emitirClienteBean.setEstadocivil(cliente.getEstadocivil() != null ? cliente.getEstadocivil().getNome() : "");
				emitirClienteBean.setConjuge(cliente.getConjuge());
				emitirClienteBean.setProfissaoconjuge(cliente.getProfissaoconjuge() != null ? cliente.getProfissaoconjuge().getNome() : "");
				emitirClienteBean.setPai(cliente.getPai());
				emitirClienteBean.setProfissaopai(cliente.getProfissaopai() != null ? cliente.getProfissaopai().getNome() : "");
				emitirClienteBean.setMae(cliente.getMae());
				emitirClienteBean.setProfissaomae(cliente.getProfissaomae() != null ? cliente.getProfissaomae().getNome() : "");
				emitirClienteBean.setObservacao(cliente.getObservacao());
				emitirClienteBean.setInfoadicionalcontrib(cliente.getInfoadicionalcontrib());
				emitirClienteBean.setInfoadicionalfisco(cliente.getInfoadicionalfisco());
				emitirClienteBean.setObservacaoVenda(cliente.getObservacaoVenda());
				emitirClienteBean.setAtivo(cliente.getAtivo());
				emitirClienteBean.setTipopessoa(cliente.getTipopessoa() != null ? cliente.getTipopessoa().getTipo(): "");
				emitirClienteBean.setCnpj(cliente.getCnpj() != null ? cliente.getCnpj().toString() : "");
				emitirClienteBean.setRazaosocial(cliente.getRazaosocial());
				emitirClienteBean.setInscricaoestadual(cliente.getInscricaoestadual());
				emitirClienteBean.setUfinscricaoestadual(cliente.getUfinscricaoestadual()!=null ? cliente.getUfinscricaoestadual().getSigla() :"");
				emitirClienteBean.setCpf(cliente.getCpf() !=null ? cliente.getCpf().toString() : "");
				emitirClienteBean.setInscricaomunicipal(cliente.getInscricaomunicipal());
				emitirClienteBean.setCrt(cliente.getCrt() !=null ? cliente.getCrt().getDescricao() : "");
				emitirClienteBean.setIncidiriss(cliente.getIncidiriss());
				emitirClienteBean.setContabilidadecentralizada(cliente.getContabilidadecentralizada());
				emitirClienteBean.setTipoidentidade(cliente.getTipoidentidade() != null ? cliente.getTipoidentidade().getDescricao() : "");
				emitirClienteBean.setRg(cliente.getRg());
				emitirClienteBean.setOrgaoemissorrg(cliente.getOrgaoemissorrg());
				emitirClienteBean.setDtemissaorg(cliente.getDtemissaorg());
				emitirClienteBean.setPassaporte(cliente.getPassaporte());
				emitirClienteBean.setValidadepassaporte(cliente.getValidadepassaporte());
				emitirClienteBean.setSexo(cliente.getSexo() != null ? cliente.getSexo().getNome() : "");
				emitirClienteBean.setDtnascimento(cliente.getDtnascimento());
				if (cliente.getMunicipionaturalidade() != null && cliente.getMunicipionaturalidade().getUf() != null){
					emitirClienteBean.setMunicipionaturalidadeuf(cliente.getMunicipionaturalidade().getUf().getSigla());
				}
				else{
					emitirClienteBean.setMunicipionaturalidadeuf("");
				}
				emitirClienteBean.setMunicipionaturalidade(cliente.getMunicipionaturalidade()!=null ? cliente.getMunicipionaturalidade().getNome() : "");
				emitirClienteBean.setGrauinstrucao(cliente.getGrauinstrucao()!=null ? cliente.getGrauinstrucao().getNome() : "");
				emitirClienteBean.setClienteprofissao(cliente.getClienteprofissao() != null ? cliente.getClienteprofissao().getNome() : "");
				emitirClienteBean.setResponsavelFinanceiro(cliente.getResponsavelFinanceiro() != null ? cliente.getResponsavelFinanceiro().getNome() : "");
				emitirClienteBean.setClienteindicacao(cliente.getClienteindicacao() != null ? cliente.getClienteindicacao().getDescricao() : "");
				emitirClienteBean.setContagerencial(cliente.getContaContabil() != null ? cliente.getContaContabil().getNome() : "");
				emitirClienteBean.setOperacaocontabil(cliente.getOperacaocontabil() != null ? cliente.getOperacaocontabil().getNome() : "");
				emitirClienteBean.setCodigotributacao(cliente.getCodigotributacao() != null ? cliente.getCodigotributacao().getDescricaoCombo() : "");
				emitirClienteBean.setRamoatividade(cliente.getRamoatividade());
				emitirClienteBean.setObjetosocial(cliente.getObjetosocial());
				emitirClienteBean.setGrupotributacao(cliente.getGrupotributacao()!=null ? cliente.getGrupotributacao().getNome() : "");
				emitirClienteBean.setBanco(cliente.getDadobancario()!=null ? cliente.getDadobancario().getBanco().getNome() : "");
				emitirClienteBean.setAgencia(cliente.getDadobancario() !=null ? cliente.getDadobancario().getAgencia() : "");
				emitirClienteBean.setDvagencia(cliente.getDadobancario()!=null ? cliente.getDadobancario().getDvagencia() : "");
				emitirClienteBean.setConta(cliente.getDadobancario() != null ? cliente.getDadobancario().getConta() : "");
				emitirClienteBean.setDvconta(cliente.getDadobancario() != null ? cliente.getDadobancario().getDvconta() : "");
				emitirClienteBean.setOperacao(cliente.getDadobancario()!=null ? cliente.getDadobancario().getOperacao() : "");
				
				if(cliente.getListaTelefone() != null && !cliente.getListaTelefone().isEmpty()){
					for (Telefone telefone : cliente.getListaTelefone()){
						EmitirClienteTelefoneBean emitirClienteTelefoneBean = new EmitirClienteTelefoneBean();
						emitirClienteTelefoneBean.setTelefone(telefone.getTelefone());
						emitirClienteTelefoneBean.setTelefonetipo(telefone.getTelefonetipo()!=null ? telefone.getTelefonetipo().getNome() : "");
						emitirClienteBean.getListaClienteTelefone().add(emitirClienteTelefoneBean);
					}
				}
				
				List<PessoaContato> listaPessoaContato = pessoaContatoService.findByPessoa(cliente);
				cliente.setListaContato(listaPessoaContato);
				if(cliente.getListaContato() != null && !cliente.getListaContato().isEmpty()){
					for (PessoaContato pessoaContato : cliente.getListaContato()){
						Contato contato = pessoaContato.getContato();
						
						EmitirClienteContatoBean emitirClienteContatoBean = new EmitirClienteContatoBean();
						emitirClienteContatoBean.setContatotipo(contato.getContatotipo() !=null ? contato.getContatotipo().getNome() : "");
						emitirClienteContatoBean.setEmail(contato.getEmailcontato());
						emitirClienteContatoBean.setNome(contato.getNome());
						emitirClienteContatoBean.setReceberboleto(contato.getReceberboleto());
						emitirClienteContatoBean.setResponsavelos(contato.getResponsavelos());
						emitirClienteContatoBean.setTelefones(contato.getTelefones());
						emitirClienteBean.getListaClienteContato().add(emitirClienteContatoBean);
					}
				}
				
				if(cliente.getListaEndereco() != null && !cliente.getListaEndereco().isEmpty()){
					for (Endereco endereco : cliente.getListaEndereco()){
						EmitirClienteEnderecoBean emitirClienteEnderecoBean = new EmitirClienteEnderecoBean();
						emitirClienteEnderecoBean.setBairro(endereco.getBairro());
						emitirClienteEnderecoBean.setCaixapostal(endereco.getCaixapostal());
						emitirClienteEnderecoBean.setCep(endereco.getCep()!=null ? endereco.getCep().toString() : "");
						emitirClienteEnderecoBean.setComplemento(endereco.getComplemento());
						emitirClienteEnderecoBean.setEnderecotipo(endereco.getEnderecotipo()!=null ? endereco.getEnderecotipo().getNome() : "");
						emitirClienteEnderecoBean.setLogradouro(endereco.getLogradouro());
						emitirClienteEnderecoBean.setMunicipio(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "");
						emitirClienteEnderecoBean.setMunicipiouf(endereco.getMunicipioUf());
						emitirClienteEnderecoBean.setNumero(endereco.getNumero());
						emitirClienteEnderecoBean.setPais(endereco.getPais()!=null ? endereco.getPais().getNome() : "");
						emitirClienteEnderecoBean.setPontoreferencia(endereco.getPontoreferencia());
						emitirClienteEnderecoBean.setDistancia(endereco.getDistancia());
						emitirClienteBean.getListaClienteEndereco().add(emitirClienteEnderecoBean);
					}
				}
				
				if(cliente.getListaRestricao() != null && !cliente.getListaRestricao().isEmpty()){
					for (Restricao restricao : cliente.getListaRestricao()){
						EmitirClienteRestricaoBean emitirClienteRestricaoBean = new EmitirClienteRestricaoBean();
						emitirClienteRestricaoBean.setDtliberacao(restricao.getDtliberacao());
						emitirClienteRestricaoBean.setDtrestricao(restricao.getDtrestricao());
						emitirClienteRestricaoBean.setMotivoliberacao(restricao.getMotivoliberacao());
						emitirClienteRestricaoBean.setMotivorestricao(restricao.getMotivorestricao());
						emitirClienteBean.getListaClienteRestricao().add(emitirClienteRestricaoBean);
					}
				}
				
				if(cliente.getListaPessoacategoria() != null && !cliente.getListaPessoacategoria().isEmpty()){
					for (Pessoacategoria pessoacategoria : cliente.getListaPessoacategoria()){
						EmitirClienteCategoriaBean emitirClienteCategoriaBean = new EmitirClienteCategoriaBean();
						emitirClienteCategoriaBean.setCategoria(pessoacategoria.getCategoria()!=null ? pessoacategoria.getCategoria().getNome() : "");
						emitirClienteBean.getListaClienteCategoria().add(emitirClienteCategoriaBean);
					}
				}
				
				if(cliente.getListaClientevendedor() != null && !cliente.getListaClientevendedor().isEmpty()){
					for (Clientevendedor clientevendedor : cliente.getListaClientevendedor()){
						EmitirClienteVendedorBean emitirClienteVendedorBean = new EmitirClienteVendedorBean();
						emitirClienteVendedorBean.setVendedor(clientevendedor.getNomeColaborador());
						emitirClienteBean.getListaClienteVendedor().add(emitirClienteVendedorBean);
					}
				}
				
				if(cliente.getListaClienterelacao() != null && !cliente.getListaClienterelacao().isEmpty()){
					for (Clienterelacao clienterelacao : cliente.getListaClienterelacao()){
						EmitirClienteReferenciaBean emitirClienteReferenciaBean = new EmitirClienteReferenciaBean();
						emitirClienteReferenciaBean.setClienteoutro(clienterelacao.getClienteoutro() != null ? clienterelacao.getClienteoutro().getNome() : "");
						emitirClienteReferenciaBean.setClienterelacaotipo(clienterelacao.getClienterelacaotipo()!=null ? clienterelacao.getClienterelacaotipo().getDescricao() : "");
						emitirClienteBean.getListaClienteReferencia().add(emitirClienteReferenciaBean);
					}
				}
				
				
				lista.add(emitirClienteBean);
			}
		}
		return lista;
	}
	
	/**
	 * Lista as Formas de Pagamento de determinado Cliente. N�o havendo nenhuma forma de pagamento definida para o Cliente, ser�o retornadas todas as Formas de Pagamento v�lidas, presentes na Base de Dados atual.
	 *
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public List<Documentotipo> findDocumentotipo(Cliente cliente){
		List<ClienteDocumentoTipo> listClienteDocumentoTipo = clienteDocumentoTipoService.findByCliente(cliente);
		List<Documentotipo> listDocumentotipo = new ArrayList<Documentotipo>();

		if(listClienteDocumentoTipo != null && !listClienteDocumentoTipo.isEmpty()){
			for (ClienteDocumentoTipo it : listClienteDocumentoTipo) {
				it.getDocumentotipo().setListaDocumentotipopapel(new ArrayList<Documentotipopapel>());
				listDocumentotipo.add(it.getDocumentotipo());
			}
		}else{
			listDocumentotipo = documentotipoService.findForCombo("cddocumentotipo", "nome");
		}
		return listDocumentotipo;
	}
	
	public ClienteRESTModel salvarClienteAndroid(ClienteRESTWSBean command, Usuario usuario) {
		ClienteRESTModel clienteRESTModel = new ClienteRESTModel();
		try {
			
			Cliente bean = createClienteByAndroid(command);
			if(bean.getCdpessoa() == null){
				saveOrUpdate(bean);
				clienteRESTModel.setCdpessoa(bean.getCdpessoa());
				if(SinedUtil.isListNotEmpty(bean.getListaEndereco())){
					List<EnderecoRESTModel> lista = new ArrayList<EnderecoRESTModel>();
					for(Endereco endereco : bean.getListaEndereco()){
						if(endereco.getIdandroid() != null && endereco.getCdendereco() != null){
							EnderecoRESTModel model = new EnderecoRESTModel();
							model.setId(endereco.getIdandroid());
							model.setCdendereco(endereco.getCdendereco());
							lista.add(model);
						}
					}
					clienteRESTModel.setListaEnderecoRESTModel(lista);
				}
				
				if(SinedUtil.isListNotEmpty(bean.getListaContato())){
					for(PessoaContato pessoaContato : bean.getListaContato()){
						Contato contato = pessoaContato.getContato();
						contatoService.saveOrUpdate(contato);
					}
				}
				if(SinedUtil.isListNotEmpty(bean.getListaContato())){
					List<ContatoRESTModel> lista = new ArrayList<ContatoRESTModel>();
					for(PessoaContato pessoaContato : bean.getListaContato()){
						Contato contato = pessoaContato.getContato();
						if(contato.getIdandroid() != null && contato.getCdpessoa() != null){
							ContatoRESTModel model = new ContatoRESTModel();
							model.setId(contato.getIdandroid());
							model.setCdpessoa(contato.getCdpessoa());
							model.setCdpessoaligacao(bean.getCdpessoa());
							lista.add(model);
						}
					}
					clienteRESTModel.setListaContatoRESTModel(lista);
				}
				
				if(colaboradorService.isColaborador(usuario.getCdpessoa()) && "TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR))){
					Clientevendedor clientevendedor = new Clientevendedor();
					clientevendedor.setCliente(bean);
					clientevendedor.setColaborador(new Colaborador(usuario.getCdpessoa()));
					clientevendedor.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
					clientevendedorService.saveOrUpdate(clientevendedor);
				}
				
				Clientehistorico clientehistorico = new Clientehistorico();
				clientehistorico.setCliente(bean);
				clientehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				clientehistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				clientehistorico.setObservacao("Cliente criado pelo app.");
				clientehistoricoService.saveOrUpdate(clientehistorico);
			} else {
				command.setCdpessoa(bean.getCdpessoa());
				
				updateClienteAndroid(command);
				clienteRESTModel.setCdpessoa(bean.getCdpessoa());
				
				if(SinedUtil.isListNotEmpty(bean.getListaTelefone())){
					Cliente clienteBean = clienteService.carregarDadosCliente(bean);
					if(clienteBean != null && SinedUtil.isListNotEmpty(clienteBean.getListaTelefone())){
						for(Telefone telefone : bean.getListaTelefone()){
							Telefone telefoneSalvar = telefone;
							if(StringUtils.isNotEmpty(telefone.getTelefone())){
								for(Telefone telefoneBean : clienteBean.getListaTelefone()){
									if(StringUtils.isNotEmpty(telefoneBean.getTelefone()) && 
											telefone.getTelefone().equals(telefoneBean.getTelefone())){
										telefoneSalvar = null;
										break;
									}
								}
							}
							if(telefoneSalvar != null){
								telefoneSalvar.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
								telefoneSalvar.setPessoa(bean);
								telefoneService.saveOrUpdate(telefoneSalvar);
							}
						}
					}else {
						for(Telefone telefone : bean.getListaTelefone()){
							telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
							telefone.setPessoa(bean);
							telefoneService.saveOrUpdate(telefone);
						}
					}
				}
				
				if(SinedUtil.isListNotEmpty(bean.getListaContato())){
					List<ContatoRESTModel> lista = new ArrayList<ContatoRESTModel>();
					for(PessoaContato pessoaContatoFound : bean.getListaContato()){
						Contato contato = pessoaContatoFound.getContato();
						Contato contatoBean = null;
						if(contato.getCdpessoa() != null){
							contatoBean = contatoService.carregaContato(contato);
						}
						if(contatoBean != null){
							contatoService.updateContatoAndroid(contato);
							if(SinedUtil.isListNotEmpty(contato.getListaTelefone())){
								if(contatoBean != null && SinedUtil.isListNotEmpty(contatoBean.getListaTelefone())){
									for(Telefone telefone : contato.getListaTelefone()){
										Telefone telefoneSalvar = telefone;
										if(StringUtils.isNotEmpty(telefone.getTelefone())){
											for(Telefone telefoneBean : contatoBean.getListaTelefone()){
												if(StringUtils.isNotEmpty(telefoneBean.getTelefone()) && 
														telefone.getTelefone().equals(telefoneBean.getTelefone())){
													telefoneSalvar = null;
													break;
												}
											}
										}
										if(telefoneSalvar != null){
											telefoneSalvar.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
											telefoneSalvar.setPessoa(contato);
											telefoneService.saveOrUpdate(telefoneSalvar);
										}
									}
								}
							}
						}else {
							contato.setCdpessoa(null);
							PessoaContato pessoaContato = new PessoaContato();
							
							pessoaContato.setPessoa(bean);
							contatoService.saveOrUpdate(contato);
							pessoaContato.setContato(contato);
							
							pessoaContatoService.saveOrUpdate(pessoaContato);
						}
						if(contato.getIdandroid() != null && contato.getCdpessoa() != null){
							ContatoRESTModel model = new ContatoRESTModel();
							model.setId(contato.getIdandroid());
							model.setCdpessoa(contato.getCdpessoa());
							model.setCdpessoaligacao(bean.getCdpessoa());
							lista.add(model);
						}
					}
					clienteRESTModel.setListaContatoRESTModel(lista);
				}
				
				if(SinedUtil.isListNotEmpty(bean.getListaEndereco())){
					List<Endereco> listaEnderecoExcluir = enderecoService.findByCliente(bean);
					if(SinedUtil.isListNotEmpty(listaEnderecoExcluir)){
						for(Endereco enderecoExcluir : listaEnderecoExcluir){
							boolean excluir = true;
							for(Endereco endereco : bean.getListaEndereco()){
								if(endereco.getCdendereco() != null && 
										enderecoExcluir.getCdendereco().equals(endereco.getCdendereco())){
									excluir = false;
								}
							}
							if(excluir)
								try {enderecoService.delete(enderecoExcluir);} catch (Exception e) {e.printStackTrace();}
						}
					}
					
					List<EnderecoRESTModel> lista = new ArrayList<EnderecoRESTModel>();
					for(Endereco endereco : bean.getListaEndereco()){
						endereco.setPessoa(bean);
						if(endereco.getBairro() != null){
							endereco.setBairro(SinedUtil.removeQuebraLinha(endereco.getBairro()));
						}
						enderecoService.saveOrUpdate(endereco);
						if(endereco.getIdandroid() != null && endereco.getCdendereco() != null){
							EnderecoRESTModel model = new EnderecoRESTModel();
							model.setId(endereco.getIdandroid());
							model.setCdendereco(endereco.getCdendereco());
							lista.add(model);
						}
					}
					clienteRESTModel.setListaEnderecoRESTModel(lista);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			String nomeMaquina = "N�o encontrado.";
			try {  
	            InetAddress localaddr = InetAddress.getLocalHost();  
	            nomeMaquina = localaddr.getHostName();  
	        } catch (UnknownHostException e3) {}  

	        StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			
			String erro = "Erro ao salvar cliente (app android). (M�quina: " + nomeMaquina + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
			enviarEmailErro(erro);
			
			clienteRESTModel.setErroSincronizacao(SinedUtil.FORMATADOR_DATA_HORA.format(new Timestamp(System.currentTimeMillis())) + " - " + erro);
		}
		return clienteRESTModel;
	}
	
	private void enviarEmailErro(String mensagem) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro ao sincronizar cliente.")
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
	
	private void updateClienteAndroid(ClienteRESTWSBean command) {
		clienteDAO.updateClienteAndroid(command);
		
	}
	private Cliente createClienteByAndroid(ClienteRESTWSBean command) {
		Cliente cliente = new Cliente();
		
		String cpfString = command.getCpf();
		String cnpjString = command.getCnpj();
		
		Cpf cpf = null;
		Cnpj cnpj = null;
		
		if(StringUtils.isNotEmpty(cpfString)){
			try {
				cpf = new Cpf(cpfString);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(StringUtils.isNotEmpty(cnpjString)){
			try {
				cnpj = new Cnpj(cnpjString);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(command.getCdpessoa() == null){
			if(cpf != null){
				cliente = this.findByCpf(cpf);
				if(cliente == null){
					cliente = new Cliente();
					Pessoa pessoa = pessoaService.findPessoaByCpf(cpf);
					
					if(pessoa != null && pessoa.getCdpessoa() != null){
						cliente.setCdpessoa(pessoa.getCdpessoa());
						this.insertSomenteCliente(cliente);
					}
				} else {
					cliente = this.loadForEntradaWithDadosPessoa(cliente);
				}
			}
			if(cnpj != null){
				cliente = this.findByCnpj(cnpj);
				if(cliente == null){
					cliente = new Cliente();
					Pessoa pessoa = pessoaService.findPessoaByCnpj(cnpj);
					
					if(pessoa != null && pessoa.getCdpessoa() != null){
						cliente.setCdpessoa(pessoa.getCdpessoa());
						this.insertSomenteCliente(cliente);
					}
				}else {
					cliente = this.loadForEntradaWithDadosPessoa(cliente);
				}
			}
			cliente.setDtinsercao(new Timestamp(System.currentTimeMillis()));
		} else {
			cliente.setCdpessoa(command.getCdpessoa());
		}
		
		cliente.setNome(command.getNome());
		cliente.setRazaosocial(command.getRazaosocial());
		if(command.getTipopessoa() != null){
			cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA.ordinal() == command.getTipopessoa().intValue() ? 
					Tipopessoa.PESSOA_FISICA : Tipopessoa.PESSOA_JURIDICA);
		}
		cliente.setCpf(cpf);
		cliente.setCnpj(cnpj);
		cliente.setInscricaoestadual(command.getInscricaoestadual());
		cliente.setEmail(command.getEmail());
		cliente.setObservacao(command.getObservacao());
		
		if(StringUtils.isNotEmpty(command.getTelefone())){
			Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
			Telefone telefone = new Telefone();
			telefone.setTelefone(command.getTelefone());
			telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
			listaTelefone.add(telefone);
			cliente.setListaTelefone(listaTelefone);
		}
		
		if(SinedUtil.isListNotEmpty(command.getListaEnderecoRESTWSBean())){
			Set<Endereco> lista = new ListSet<Endereco>(Endereco.class);
			for(EnderecoRESTWSBean item : command.getListaEnderecoRESTWSBean()){
				Endereco endereco = new Endereco();
				endereco.setIdandroid(item.getId());
				endereco.setCdendereco(item.getCdendereco());
				endereco.setMunicipio(item.getCdmunicipio() != null ? new Municipio(item.getCdmunicipio()) : null);
				endereco.setLogradouro(item.getLogradouro());
				endereco.setBairro(item.getBairro());
				endereco.setCep(item.getCep() != null && item.getCep().matches("\\d{5}[-\\.]?\\d{3}") ? new Cep(item.getCep()) : null);
				endereco.setNumero(item.getNumero());
				endereco.setComplemento(item.getComplemento());
				endereco.setEnderecotipo(item.getCdenderecotipo() != null ? new Enderecotipo(item.getCdenderecotipo()) : null);
				lista.add(endereco);
			}
			cliente.setListaEndereco(lista);
		}
		
		if(SinedUtil.isListNotEmpty(command.getListaContatoRESTWSBean())){
			List<PessoaContato> lista = new ArrayList<PessoaContato>();
			
			for(ContatoRESTWSBean item : command.getListaContatoRESTWSBean()){
				Contato contato = new Contato();
				contato.setIdandroid(item.getId());
				contato.setCdpessoa(item.getCdpessoa());
				contato.setNome(item.getNome());
				
				if(StringUtils.isNotEmpty(item.getTelefone())){
					Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
					Telefone telefone = new Telefone();
					telefone.setTelefone(item.getTelefone());
					telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
					listaTelefone.add(telefone);
					contato.setListaTelefone(listaTelefone);
				}
				
				contato.setEmailcontato(item.getEmail());
				contato.setContatotipo(item.getCdcontatotipo() != null ? new Contatotipo(item.getCdcontatotipo()) : null);
				
				PessoaContato pessoaContato = new PessoaContato();
				pessoaContato.setPessoa(cliente);
				pessoaContato.setContato(contato);
				
				lista.add(pessoaContato);
			}
			cliente.setListaContato(lista);
		}
		return cliente;
	}
	
	/**
	 * Lista os Prazos de Pagamento de determinado Cliente. N�o havendo nenhum prazo de pagamento definido para o Cliente, ser�o retornados todos os Prazos de Pagamento v�lidos, presentes na Base de Dados atual.
	 *
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public List<Prazopagamento> findPrazoPagamento(Cliente cliente) {
		return this.findPrazoPagamento(cliente, null, false);
	}
	
	public List<Prazopagamento> findPrazoPagamentoOrcamento(Cliente cliente) {
		return this.findPrazoPagamento(cliente, null, true);
	}
	
	/**
	 * Lista os Prazos de Pagamento de determinado Cliente, considerando Prazo M�dio. N�o havendo nenhum prazo de pagamento definido para o Cliente, ser�o retornados todos os Prazos de Pagamento v�lidos, presentes na Base de Dados atual.
	 *
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public List<Prazopagamento> findPrazoPagamento(Cliente cliente, Boolean prazomedio, boolean orcamento) {
		List<Prazopagamento> listPrazopagamento = new ArrayList<Prazopagamento>();
		boolean possuiPrazoPagamentoPorCliente = false;
		if(cliente != null && cliente.getCdpessoa() != null){
			List<ClientePrazoPagamento> listClientePrazoPagamento = clientePrazoPagamentoService.findByCliente(cliente);
			if(SinedUtil.isListNotEmpty(listClientePrazoPagamento)){
				possuiPrazoPagamentoPorCliente = true;
				for (ClientePrazoPagamento it : listClientePrazoPagamento) {
					if (prazomedio != null && !orcamento) {
						if(prazomedio.equals(it.getPrazopagamento().getPrazomedio()))
							listPrazopagamento.add(it.getPrazopagamento());
					}else{
						listPrazopagamento.add(it.getPrazopagamento());
					}
				}
			}
		}
		if(!possuiPrazoPagamentoPorCliente) {
			if(orcamento){
				listPrazopagamento = prazopagamentoService.findByPrazomedioOrcamento();
			}else {
				listPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
			}
		}
		
		return listPrazopagamento;
	}
	/**
	* M�todo com refer�ncia no DAO
	* 
	* @param whereIn
	* @return
	* @since 14/10/2014
	* @author Jo�o Vitor
	*/
	public List<Cliente> findClienteToEtiquetaBean(String whereIn) throws Exception{
		return clienteDAO.findClienteToEtiquetaBean(whereIn);
	}
	
	public List<Cliente> findForControleInteracao(ControleInteracaoBean bean){
		List<Cliente> lista = clienteDAO.findForControleInteracao(bean);
		if(SinedUtil.isListNotEmpty(lista)){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdpessoa", ",");
			String whereInHistoricosituacao = CollectionsUtil.listAndConcatenate(bean.getListaSituacaohistorico(), "cdsituacaohistorico", ",");
			List<Cliente> listaCliente = clienteDAO.findHistoricoForControleInteracao(whereIn, whereInHistoricosituacao, bean.getDatade(), bean.getDataate());
			if(SinedUtil.isListNotEmpty(listaCliente)){
				for(Cliente clienteH : listaCliente){
					for(Cliente cliente : lista){
						if(clienteH.equals(cliente)){
							cliente.setListClientehistorico(clienteH.getListClientehistorico());
						}
					}
				}
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que retorna o hist�rico de cliente mais recente e que possua situa��o definida.
	 * 
	 * @param lista
	 * @param datainicio
	 * @param datafim
	 * @return
	 * @author Rafael Salvio
	 */
	public Clientehistorico buscaMaiorDataHistoricoCliente(List<Clientehistorico> lista, Timestamp datainicio, Timestamp datafim){
		Clientehistorico clientehistorico = null;

		for(Clientehistorico ch : lista){
			if(ch.getDtaltera().compareTo(datainicio) >= 0 && ch.getDtaltera().compareTo(datafim) < 0 && ch.getSituacaohistorico() != null
					&& ch.getAtividadetipo() != null && !(Boolean.TRUE.equals(ch.getAtividadetipo().getNaoexibirnopainel()))){
				if(clientehistorico == null){
					clientehistorico = ch;
				} else if(ch.getDtaltera().compareTo(clientehistorico.getDtaltera()) > 0){
					clientehistorico = ch;
				}
			}
		}
		return clientehistorico;
	}
	
	public Cliente loadForSincronizacaoAA(Cliente cliente) {
		Cliente clienteAux = clienteDAO.loadForSincronizacaoAA(cliente);
		return clienteAux;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDAO#findByIdentificador(String identificador)
	*
	* @param identificador
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public List<Cliente> findByIdentificador(String identificador){
		return clienteDAO.findByIdentificador(identificador);
	}
	
	/**
	* M�todo com refer�ncia no DAO.
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDAO#loadForMakeOportunidadeRTF(Cliente)
	*
	* @param cliente
	* @return
	* @author Matheus Santos
	*/
	public Cliente loadForMakeOportunidadeRTF(Cliente cliente){
		return clienteDAO.loadForMakeOportunidadeRTF(cliente);
	}
	
	public List<ClienteW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<ClienteW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ClienteW3producaoRESTModel> lista = new ArrayList<ClienteW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Cliente c : clienteDAO.findForW3Producao(whereIn))
				lista.add(new ClienteW3producaoRESTModel(c));
		}
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param dataReferencia
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/04/2016
	 */
	public List<ClienteWSBean> findForWSSOAP(Timestamp dataReferencia) {
		List<ClienteWSBean> lista = new ArrayList<ClienteWSBean>();
		for(Cliente c: clienteDAO.findForWSSOAP(dataReferencia))
			lista.add(new ClienteWSBean(c));
		return lista;
	}
	
	public void sincronizarClienteArvoreAcesso(Cliente cliente){
		EmpresaArvoreAcessoRESTModel model = null;
		Contratohistorico contratohistorico = null;
		List<ContratoBean> listaContratoBeanAux = new ArrayList<ContratoBean>();
		
		try {
			System.out.println("#### Altera��o de dados do cliente ::: Tentando sincronizar cdcliente=" + cliente.getCdpessoa());
			Cliente clienteSinc = clienteService.loadForSincronizacaoAA(cliente);
			validaDadosObrigatoriosSincronizacao(clienteSinc);
			
			EmpresaArvoreAcessoRESTClient clientWS = new EmpresaArvoreAcessoRESTClient();
			
			EmpresaArvoreAcessoRESTBean bean = new EmpresaArvoreAcessoRESTBean(clienteSinc);
			listaContratoBeanAux.addAll(bean.getLista_contrato());
			model = clientWS.enviaEmpresa(bean);
			if(model!=null && !model.isErro() && model.getId()!=null){
				contratosincronizacaoAAService.updateSincronizacao(clienteSinc.getCdpessoa(), model.getId());
				
				if(listaContratoBeanAux != null && !listaContratoBeanAux.isEmpty()){
					Timestamp hora = SinedDateUtils.currentTimestamp();
					for(ContratoBean item : listaContratoBeanAux){
						contratohistorico = new Contratohistorico();
						contratohistorico.setContrato(new Contrato(item.getId_contrato()));
						contratohistorico.setAcao(Contratoacao.ALTERADO);
						contratohistorico.setDthistorico(hora);
						contratohistorico.setObservacao("Contrato sincronizado com a �rvore de Acesso.");
						
						contratohistoricoService.saveOrUpdate(contratohistorico);
					}
				}
			}else{
				throw new RuntimeException("Servidor n�o retornou dados da requisi��o.");
			}
		} catch (Exception e) {
			System.out.println("ERRO ao sincronizar com a ArvoreAcesso o registro com id="+cliente.getCdpessoa());
			e.printStackTrace();
			String mensagemErro = (model==null || StringUtils.isBlank(model.getMensagem())) ? e.getMessage() : model.getMensagem();
			
			ContratosincronizacaoAA aa = contratosincronizacaoAAService.findByCdCliente(cliente.getCdpessoa());
			
			contratosincronizacaoAAService.updateError(cliente.getCdpessoa(), mensagemErro);
			
			if(aa == null || !mensagemErro.equals(aa.getMensagem())){
				if(listaContratoBeanAux != null && !listaContratoBeanAux.isEmpty()){
					for(ContratoBean item : listaContratoBeanAux){
						contratohistorico = new Contratohistorico();
						contratohistorico.setContrato(new Contrato(item.getId_contrato()));
						contratohistorico.setAcao(Contratoacao.ALTERADO);
						contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
						contratohistorico.setObservacao("ERRO ao sincronizar com a ArvoreAcesso: " + mensagemErro);
						
						contratohistoricoService.saveOrUpdate(contratohistorico);
					}
				} else {
					List<Contrato> listaContrato = contratoService.findByContrato(cliente);
					if(listaContrato != null && !listaContrato.isEmpty()){
						for(Contrato item : listaContrato){
							contratohistorico = new Contratohistorico();
							contratohistorico.setContrato(item);
							contratohistorico.setAcao(Contratoacao.ALTERADO);
							contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
							contratohistorico.setObservacao("ERRO ao sincronizar com a ArvoreAcesso: " + mensagemErro);
							
							contratohistoricoService.saveOrUpdate(contratohistorico);
						}
					}
				}
			}
		}
		System.out.println("#### Altera��o de dados do cliente ::: Fim da tentativa de sincroniza��o do cdcliente=" + cliente.getCdpessoa());
	}
	
	/**
	 * Valida��o de dados obrigat�rios para envio da sincroniza��o de cliente para o �rvore de Acesso
	 * @param cliente
	 */
	private void validaDadosObrigatoriosSincronizacao(Cliente cliente) {
		String msg = "";
		if(StringUtils.isBlank(cliente.getRazaosocial())){
			msg += "N�o possui razao social.\n";
		}
		
		for(PessoaContato pessoaContato : cliente.getListaContato()){
			Contato contato = pessoaContato.getContato();
			
			if(contato.getContatotipo()==null || StringUtils.isBlank(contato.getContatotipo().getNome()) ){
				msg += "Contato n�o possui contatotipo definido.\n";
			}
		}
		if(StringUtils.isNotBlank(msg)){
			throw new RuntimeException(msg);
		}
	}
	
	public ModelAndView getFormasPrazosPagamentoClienteAlertaModelAndView(WebRequestContext request, Cliente cliente, boolean addPrazo, boolean addForma){
		boolean mostrarAlerta = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.ALERTAR_CONDICOES_CLIENTE));
		List<DocumentotipoVO> listaDocumentotipoVO = new ArrayList<DocumentotipoVO>();
		List<PrazopagamentoVO> listaPrazopagamentoVO = new ArrayList<PrazopagamentoVO>();
		String formasPagamento = "";
		String prazosPagamento = "";
		
		if (mostrarAlerta){
			if (Util.objects.isPersistent(cliente)){
				if (addForma){
					List<Documentotipo> listaDocumentotipo = documentotipoService.findByCliente(cliente, false);
					for (Documentotipo documentotipo: listaDocumentotipo){
						listaDocumentotipoVO.add(new DocumentotipoVO(documentotipo.getCddocumentotipo(), documentotipo.getNome()));
					}
					formasPagamento = CollectionsUtil.listAndConcatenate(listaDocumentotipo, "nome", ", ");
				}
				if (addPrazo){
					List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByCliente(cliente);
					for (Prazopagamento prazopagamento: listaPrazopagamento){
						listaPrazopagamentoVO.add(new PrazopagamentoVO(prazopagamento.getCdprazopagamento(), prazopagamento.getNome()));
					}
					prazosPagamento = CollectionsUtil.listAndConcatenate(listaPrazopagamento, "nome", ", ");
				}
			}
		}
		
		FormasPrazosPagamentoVO formasPrazosPagamentoVO = new FormasPrazosPagamentoVO(mostrarAlerta, listaDocumentotipoVO, listaPrazopagamentoVO, formasPagamento, prazosPagamento);
		
		return new JsonModelAndView().addObject("formasPrazosPagamento", formasPrazosPagamentoVO);
	}
	
	public ModelAndView getFormasPrazosPagamentoClienteAlertaModelAndView(WebRequestContext request, boolean addForma, boolean addPrazo){
		String cdpessoaStr = request.getParameter("cdpessoa");
		Cliente cliente = null;
		if(StringUtils.isNotBlank(cdpessoaStr)){
			try {
				cliente = new Cliente(Integer.parseInt(cdpessoaStr));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return this.getFormasPrazosPagamentoClienteAlertaModelAndView(request, cliente, addForma, addPrazo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param cliente
	 * @return
	 * @since 13/07/2016
	 * @author C�sar
	 */
	public Cliente findForOportunidade(Cliente cliente){
		return clienteDAO.findForOportunidade(cliente);
	}
	
	/**
	* M�todo que cria observa��o referente as altera��es no limite e validade de cr�dito do cliente
	*
	* @param bean
	* @return
	* @since 12/08/2016
	* @author Luiz Fernando
	*/
	public String criaObservacaoAlteracaoLimiteValidadeCredito(Cliente bean) {
		String observacaoAlteracaoLimiteValidadeCredito = "";
		if(bean != null && bean.getCdpessoa() != null){
			Cliente cliente = clienteService.load(bean, "cliente.creditolimitecompra, cliente.dtvalidadelimitecompra");
			if(cliente != null){
				if(bean.getCreditolimitecompra() != null && cliente.getCreditolimitecompra() == null){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o do limite de cr�dito de 'vazio' para R$ " + new Money(bean.getCreditolimitecompra()) + ".<br>";
				}else if(bean.getCreditolimitecompra() == null && cliente.getCreditolimitecompra() != null){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o do limite de cr�dito de R$ " + new Money(cliente.getCreditolimitecompra()) + " para 'vazio'.<br>";
				}else if(bean.getCreditolimitecompra() != null && cliente.getCreditolimitecompra() != null &&bean.getCreditolimitecompra().compareTo(cliente.getCreditolimitecompra()) != 0){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o do limite de cr�dito de R$ " + new Money(cliente.getCreditolimitecompra()) + " para R$ " + new Money(bean.getCreditolimitecompra()) + ".<br>";
				}
				
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				if(bean.getDtvalidadelimitecompra() != null && cliente.getDtvalidadelimitecompra() == null){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o da validade do limite de compras de 'vazio' para " + format.format(bean.getDtvalidadelimitecompra()) + ".<br>";
				}else if(bean.getDtvalidadelimitecompra() == null && cliente.getDtvalidadelimitecompra() != null){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o da validade do limite de compras de " + format.format(cliente.getDtvalidadelimitecompra()) + " para 'vazio'.<br>";
				}else if(bean.getDtvalidadelimitecompra() != null && cliente.getDtvalidadelimitecompra() != null && !SinedDateUtils.equalsIgnoreHour(bean.getDtvalidadelimitecompra(), cliente.getDtvalidadelimitecompra())){
					observacaoAlteracaoLimiteValidadeCredito += "Altera��o da validade do limite de compras de " + format.format(cliente.getDtvalidadelimitecompra()) + " para " + format.format(bean.getDtvalidadelimitecompra()) + ".<br>";
				}
			}
		}
		
		return StringUtils.isNotBlank(observacaoAlteracaoLimiteValidadeCredito) ? observacaoAlteracaoLimiteValidadeCredito : null;
	}
	
	/**
	* M�todo que retorna true caso a validade do limite de cr�dito seja menor que a data atual
	* 
	* @param cliente
	* @return
	* @since 11/08/2016
	* @author Luiz Fernando
	*/
	public boolean getValidadeLimiteCreditoExpirada(Cliente cliente) {
		if(cliente != null && cliente.getCdpessoa() != null){
			Cliente bean = cliente.getDtvalidadelimitecompra() == null ? clienteService.load(cliente, "cliente.cdpessoa, cliente.dtvalidadelimitecompra") : cliente;
			if(bean != null && bean.getDtvalidadelimitecompra() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtvalidadelimitecompra(), new Date(System.currentTimeMillis()))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdcliente
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdcliente) {
		clienteDAO.atualizaW3producao(cdcliente);
	}
	
	private void setarListaClienteEmpresa(Cliente cliente){
		if (SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
			String whereInCdempresa = new SinedUtil().getListaEmpresa();
			if (whereInCdempresa != null){
				whereInCdempresa = whereInCdempresa.replace(" ", "");
				for (String id: whereInCdempresa.split("\\,")){
					ClienteEmpresa clienteEmpresa = new ClienteEmpresa();
					clienteEmpresa.setEmpresa(new Empresa(new Integer(id)));
					clienteEmpresa.setCliente(cliente);
					if(!clienteEmpresaService.haveClienteEmpresa(clienteEmpresa)){
						clienteEmpresaService.saveOrUpdate(clienteEmpresa);
					}
				}
			}
		}
	}
	
	public void updateContaContabil(Cliente cliente, ContaContabil contacontabil){
		clienteDAO.updateContaContabil(cliente, contacontabil);
	}
	
	public List<Cliente> findForSAGE(Date dataBase){
		return clienteDAO.findForSAGE(dataBase);
	}
	
	public Cliente loadForInfoContribuinte(Cliente cliente) {
		return clienteDAO.loadForInfoContribuinte(cliente);
	}
	
	public Cliente findForValeCompra(Cliente cliente){
		return clienteDAO.findForValeCompra(cliente);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDAO#findForSAGE(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Cliente> findForSAGE(String whereIn){
		return clienteDAO.findForSAGE(whereIn);
	}
	
	public List<Cliente> findForValidacaoSAGE(String whereIn){
		return clienteDAO.findForValidacaoSAGE(whereIn);
	}
	
	public boolean isClientePermissaoEmpresa(Cliente cliente, String whereInEmpresa){
		return clienteDAO.isClientePermissaoEmpresa(cliente, whereInEmpresa);
	}
	
	public ModelAndView verificaClienteEmpresa(WebRequestContext request) {
		boolean permissaoEmpresa = true;
		try {
			String id_cliente = request.getParameter("cliente");
			String whereInEmpresa = request.getParameter("whereInEmpresa");
			if(StringUtils.isNotBlank(id_cliente)){
				permissaoEmpresa = isClientePermissaoEmpresa(new Cliente(Integer.parseInt(id_cliente)), whereInEmpresa);
			}
		} catch (Exception e) {}
		
		return new JsonModelAndView().addObject("permissaoEmpresa", permissaoEmpresa);
	}
	
	public Cliente findForContato(Cliente cliente) {
		return clienteDAO.findForContato(cliente);
	}
	
	public List<Cliente> findWhereInCdvenda(String whereInCdvenda){
		return clienteDAO.findWhereInCdvenda(whereInCdvenda);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param whereInLead
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/10/2017
	 */
	public List<Cliente> findByLead(String whereInLead) {
		return clienteDAO.findByLead(whereInLead);
	}
	
	public List<Cliente> findForInventario(String whereIn) {
		return clienteDAO.findForInventario(whereIn);
	}
	/**
	 * 
	 * @param cliente
	 * @return
	 */
	public Cliente loadCliente(Cliente cliente) {
		return clienteDAO.loadClienteWithEndereco(cliente);
	}
	
	@Override
	public Cliente loadForEntrada(Cliente bean) {
		bean = super.loadForEntrada(bean);
		if(bean != null && bean.getAssociacao() != null){
			bean.setAssociacao(fornecedorService.load(bean.getAssociacao(), "fornecedor.cdpessoa, fornecedor.nome"));
		}
		return bean;
	}
	
	public List<Cliente> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco){
		return clienteDAO.findByMaterialtabelapreco(materialtabelapreco);
	}
	
	public List<Cliente> findClientesSemPermissaoClienteVendedor(String whereIn){
		return clienteDAO.findClientesSemPermissaoClienteVendedor(whereIn);
	}
	
	public List<Cliente> findByCategoria(Categoria categoria){
		return clienteDAO.findByCategoria(categoria);
	}
	
	public Money getValorDebitoByCliente(Cliente cliente){
		Money valor = contareceberService.getValorDebitoContasEmAbertoByCliente(cliente);
		if(parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_CHEQUE)){
			valor = valor.add(chequeService.getValorTotalCheques(cliente, Chequesituacao.DEVOLVIDO, Chequesituacao.PREVISTO, Chequesituacao.BAIXADO));
		}
		return valor;
	}
	
	public List<Cliente> findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(String q, String idEmpresa){
		return clienteDAO.findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuario(q, idEmpresa);
	}
	
	public List<Cliente> findAutocompleteRestricaoClienteVendedorWSVenda(String q, Usuario usuario){
		usuario = UsuarioService.getInstance().load(usuario, "usuario.cdpessoa, usuario.restricaoclientevendedor");
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador(usuario);
		if(usuario.getRestricaoclientevendedor() == null || !usuario.getRestricaoclientevendedor()){
			colaborador = null;
		}
		return clienteDAO.findAutocompleteRestricaoClienteVendedor(q, colaborador, true);
	}
	
	public Cliente loadForEmissaoNf(Cliente cliente){
		return clienteDAO.loadForEmissaoNf(cliente);
	}
	
	public Integer getNovosClientesForAndroid(Integer cdusuario) {
		boolean isColaborador = colaboradorService.isColaborador(cdusuario);
		if(!isColaborador) return 0;
		return clienteDAO.getNovosClientesForAndroid(cdusuario);
	}
	
	public boolean isExcedeLimiteCredito(Cliente cliente, Money valorValidar){
		ClienteInfoFinanceiraBean limiteCredido = vendaService.criarBeanClienteInfoFinanceira(null, cliente);
		if(limiteCredido != null && limiteCredido.getSaldolimite() != null){
			return limiteCredido.getSaldolimite().compareTo(valorValidar) > 0;
		}
		return false;
	}
	
	public Integer diasEmAtraso(Cliente cliente){
		Integer diasEmAtraso = 0;
		if(cliente != null){
			Date dtvencimento = documentoService.getPrimeiroVencimentoDocumentoAtrasado(cliente);	
			if(dtvencimento != null){
				diasEmAtraso = SinedDateUtils.diferencaDias(SinedDateUtils.currentDateToBeginOfDay(), dtvencimento);
			}
		}
		return diasEmAtraso;
	}
}