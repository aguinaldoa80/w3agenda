package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRetornoSegmentoDetalheDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoRetornoSegmentoDetalheService extends GenericService<BancoConfiguracaoRetornoSegmentoDetalhe>{
	BancoConfiguracaoRetornoSegmentoDetalheDAO bancoConfiguracaoRetornoSegmentoDetalheDAO;
	
	
	public void setBancoConfiguracaoRetornoSegmentoDetalheDAO(BancoConfiguracaoRetornoSegmentoDetalheDAO bancoConfiguracaoRetornoSegmentoDetalheDAO) {this.bancoConfiguracaoRetornoSegmentoDetalheDAO = bancoConfiguracaoRetornoSegmentoDetalheDAO;}


	public List<BancoConfiguracaoRetornoSegmentoDetalhe> findByBancoConfiguracaoRetorno(BancoConfiguracaoRetorno bcr){
		return bancoConfiguracaoRetornoSegmentoDetalheDAO.findByBancoConfiguracaoRetorno(bcr);
	}

}
