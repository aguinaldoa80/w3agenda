package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.geral.dao.GarantiatipopercentualDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipopercentual.GarantiaTipoPercentualW3producaoRESTModel;

public class GarantiatipopercentualService extends GenericService<Garantiatipopercentual>{

	protected GarantiatipopercentualDAO garantiatipopercentualDAO;
	
	public void setGarantiatipopercentualDAO(GarantiatipopercentualDAO garantiatipopercentualDAO) {
		this.garantiatipopercentualDAO = garantiatipopercentualDAO;
	}
	
	public List<GarantiaTipoPercentualW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<GarantiaTipoPercentualW3producaoRESTModel> lista = new ArrayList<GarantiaTipoPercentualW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Garantiatipopercentual garantiatipopercentual : garantiatipopercentualDAO.findForW3Producao(whereIn))
				lista.add(new GarantiaTipoPercentualW3producaoRESTModel(garantiatipopercentual));
		}
		
		return lista;
	}
	
	public List<Garantiatipopercentual> findByGarantiatipo(Garantiatipo garantiatipo){
		return garantiatipopercentualDAO.findByGarantiatipo(garantiatipo);
	}
}