package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.ContatoDAO;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTModel;

@DefaultOrderBy("upper(retira_acento(contato.nome))")
public class ContatoService extends GenericService<Contato>{

	private ContatoDAO contatoDAO;
	
	public void setContatoDAO(ContatoDAO contatoDAO) {
		this.contatoDAO = contatoDAO;
	}
	
	/* singleton */
	private static ContatoService instance;
	public static ContatoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContatoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Carrega a lista de Contatos de uma Pessoa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContatoDAO#findByPessoa
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contato> findByPessoa(Pessoa form) {
		return contatoDAO.findByPessoa(form);
	}

	public Contato carregaContato(Contato contato) {
		return contatoDAO.carregaContato(contato);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContatoDAO#findByPessoaCombo
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contato> findByPessoaCombo(Pessoa pessoa) {
		return contatoDAO.findByPessoaCombo(pessoa, null);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContatoDAO#findByPessoaCombo
	 * @param pessoa
	 * @return
	 * @author Thiago Clemente
	 */
	public List<Contato> findByPessoaCombo(Pessoa pessoa, Boolean ativo) {
		return contatoDAO.findByPessoaCombo(pessoa, ativo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contato getEmailContato(Contato contato) {
		return contatoDAO.getEmailContato(contato);
	}

	/**
	 * M�todo que verifica se h� contatos com emails iguais e retira da lista os duplicados
	 * 
	 * @param listaContatos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contato> retiraContatoComEmailDuplicados(List<Contato> listaContatos) {
		List<Contato> list = new ArrayList<Contato>();
		for (Contato contato : listaContatos) {
			boolean existe = false;
			for (Contato contato2 : list) {
				if(contato.getEmailcontato().equals(contato2.getEmailcontato())){
					existe = true;
					break;
				}
			}
			if(!existe)
				list.add(contato);
		}
		
		return list;
	}

	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * M�todo para obter a lista de contatos pelo cliente informado
	 * 
	 * @param contato
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Contato> getListContatoByCliente(Cliente cliente){
		return contatoDAO.getListContatoByCliente(cliente, null);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * M�todo para obter a lista de contatos pelo cliente informado
	 * 
	 * @param contato
	 * @return
	 * @author Thiago Clemente
	 */
	public List<Contato> getListContatoByCliente(Cliente cliente, Boolean ativo){
		return contatoDAO.getListContatoByCliente(cliente, ativo);
	}
	
	/**
	* Faz refer�ncia ao DAO
	* M�todo para obter a lista de contatos pela pessoa informado
	*
	* @param pessoa
	* @param contatotipo
	* @return
	* @since Jul 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contato> findListIdcontatotipo(Pessoa pessoa, Contatotipo contatotipo){
		return contatoDAO.findListIdcontatotipo(pessoa, contatotipo);
	}
	public List<Contato> findByPessoaContatotipo(Pessoa form, Contatotipo contatotipo) {
		if(form == null || form.getCdpessoa() == null)
			return new ArrayList<Contato>();
		return contatoDAO.findByPessoaContatotipo(form.getCdpessoa().toString(), contatotipo);
	}
	
	public List<Contato> findByPessoaContatotipo(String whereIn, Contatotipo contatotipo) {
		return contatoDAO.findByPessoaContatotipo(whereIn, contatotipo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContatoDAO#loadForOrdemcompraInfo(Contato contato)
	 *
	 * @param contato
	 * @return
	 * @author Luiz Fernando
	 */
	public Contato loadForOrdemcompraInfo(Contato contato) {
		return contatoDAO.loadForOrdemcompraInfo(contato);
	}

	/**
	 * 
	 * @param pessoa
	 * @return
	 */
	public Contato buscaEmailsTelefones(Pessoa pessoa) {
		List<Contato> listaContato = contatoDAO.buscaEmailsTelefones(pessoa);
		if(listaContato!= null && !listaContato.isEmpty())
			return listaContato.get(0);
		else
			return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContatoDAO#findByPessoa(Pessoa pessoa, Boolean receberBoleto)
	 *
	 * @param pessoa
	 * @param receberBoleto
	 * @return
	 * @author Luiz Fernando
	 * @since 18/11/2013
	 */
	public List<Contato> findByPessoa(Pessoa pessoa, Boolean receberBoleto) {
		return contatoDAO.findByPessoa(pessoa, receberBoleto);
	}
	
	public List<ContatoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ContatoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ContatoRESTModel> lista = new ArrayList<ContatoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Contato contato : contatoDAO.findForAndroid(whereIn))
				lista.add(new ContatoRESTModel(contato));
		}
		
		return lista;
	}
	
	public Contato load(Integer cdpessoa){
		if(cdpessoa==null){
			throw new SinedException("O par�metro cdpessoa n�o pode ser null.");
		}
		Contato contato = new Contato();
		contato.setCdpessoa(cdpessoa);
		return contatoDAO.load(contato);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param bean
	 * @param cdpessoaLigacao
	 */
	public void insertSomenteContato(Contato bean, Integer cdpessoaLigacao) {
		contatoDAO.insertSomenteContato(bean,cdpessoaLigacao);
		
	}

	public void updateContatoAndroid(Contato contato) {
		contatoDAO.updateContatoAndroid(contato);
		
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereInCliente
	 * @param ativo
	 * @return
	 */
	public List<Contato> findByPessoaComboWhereIn(String whereInCliente, Boolean ativo) {
		return contatoDAO.findByPessoaComboWhereIn(whereInCliente, ativo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param contato
	 * @author Andrey Leonardo
	 * @since 04/08/2015
	 */
	public void updateContatoEmail(Contato contato){
		contatoDAO.updateContatoEmail(contato);
	}

	public Contato getContatoByCdPessoaLigacao(Integer cdpessoaligacao) {
		return contatoDAO.getContatoByCdPessoaLigacao(cdpessoaligacao);
	}
	
	public List<GenericBean> findForWSVenda(Cliente cliente){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findByPessoaCombo(cliente));
	}
}
