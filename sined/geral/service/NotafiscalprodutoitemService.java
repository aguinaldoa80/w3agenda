package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoImpostoInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoitemDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistrosaidaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoitemService extends GenericService<Notafiscalprodutoitem>{

	private MaterialService materialService;
	private NotafiscalprodutoitemDAO notafiscalprodutoitemDAO;
	private CfopService cfopService;
	private ParametrogeralService parametrogeralService;
	
	public void setNotafiscalprodutoitemDAO(NotafiscalprodutoitemDAO notafiscalprodutoitemDAO) {this.notafiscalprodutoitemDAO = notafiscalprodutoitemDAO;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	
	public  <T extends CalculoImpostoInterface> T loadTributacaoMaterialByEmpresa(CalculoImpostoInterface item, Empresa empresa, Cliente cliente, Endereco enderecoOrigem, Endereco enderecoDestino, Cfop cfopitem){
		return this.loadTributacaoMaterialByEmpresa(item, empresa, cliente, enderecoOrigem, enderecoDestino, cfopitem, true);
	}
	
	public  <T extends CalculoImpostoInterface> T loadTributacaoMaterialByEmpresa(CalculoImpostoInterface item, Empresa empresa, Cliente cliente, Endereco enderecoOrigem, Endereco enderecoDestino, Cfop cfopitem, boolean carregarAliquotas){
		if (item.getMaterial() != null){		
			Material material = materialService.loadWithMatrialgrupo(item.getMaterial());
			material = materialService.loadTributacao(material, empresa, material.getMaterialgrupo(), cliente, 
					enderecoOrigem, enderecoDestino, item.getGrupotributacao());
			Cfop cfop = null;
			if (material.getGrupotributacaoNota() != null){
				item.setGrupotributacaotrans(material.getGrupotributacaoNota());
				if(cfopitem != null && cfopitem.getCdcfop() != null){
					material.getMaterialgrupo().setCfop(cfopitem);
					cfop = cfopService.findForNFProduto(material, material.getEnderecoTributacaoOrigem(), material.getEnderecoTributacaoDestino());
				}else if(material.getGrupotributacaoNota().getCfopgrupo() != null){
					material.getMaterialgrupo().setCfop(material.getGrupotributacaoNota().getCfopgrupo());
					cfop = cfopService.findForNFProduto(material, material.getEnderecoTributacaoOrigem(), material.getEnderecoTributacaoDestino());
				}
				preencherDadosTributacao(item, material.getGrupotributacaoNota());
			}else {
				item.setGrupotributacaotrans(item.getGrupotributacao());
				if(item.getTributadoicms() == null){
					if(material.getTributacaoestadual() != null && material.getTributacaoestadual() && material.getServico() != null && material.getServico()){
						item.setTributadoicms(false);
					}else {
						item.setTributadoicms(true);
					}
				}
			}
			item.setCfop(cfop);			

			if(carregarAliquotas){
				item.setIcms(material.getAliquotaicms() != null ? material.getAliquotaicms().getValue().doubleValue() : null);
				item.setIcmsst(material.getAliquotaicmsst() != null ? material.getAliquotaicmsst().getValue().doubleValue() : null);
				item.setIpi(material.getAliquotaipi() != null ? material.getAliquotaipi().getValue().doubleValue() : null);
				item.setPis(material.getAliquotapis() != null ? material.getAliquotapis().getValue().doubleValue() : null);
				item.setCofins(material.getAliquotacofins() != null ? material.getAliquotacofins().getValue().doubleValue() : null);
				item.setIss(material.getAliquotaiss() != null ? material.getAliquotaiss().getValue().doubleValue() : null);
				item.setCsll(material.getAliquotacsll() != null ? material.getAliquotacsll().getValue().doubleValue() : null);
				item.setIr(material.getAliquotair() != null ? material.getAliquotair().getValue().doubleValue() : null);
				item.setInss(material.getAliquotainss() != null ? material.getAliquotainss().getValue().doubleValue() : null);
				item.setFcp(material.getAliquotaFCP() != null ? material.getAliquotaFCP().getValue().doubleValue() : null);
				item.setFcpst(material.getAliquotaFCPST() != null ? material.getAliquotaFCPST().getValue().doubleValue() : null);
			}
			
			if(material.getAliquotamva() != null && material.getAliquotamva() > 0){
				item.setMargemvaloradicionalicmsst(material.getAliquotamva());
			}
			if(StringUtils.isNotBlank(material.getCest()) && item.getTipocobrancaicms() != null && exibirCST(item.getTipocobrancaicms())){
				item.setCest(material.getCest());
			}
			
			if(item.getMaterial() != null){
				item.getMaterial().setPercentualBcicms(material.getPercentualBcicms() != null ? material.getPercentualBcicms() : null);
				item.getMaterial().setPercentualBcipi(material.getPercentualBcipi() != null ? material.getPercentualBcipi() : null);
				item.getMaterial().setPercentualBcpis(material.getPercentualBcpis() != null ? material.getPercentualBcpis() : null);
				item.getMaterial().setPercentualBccofins(material.getPercentualBccofins() != null ? material.getPercentualBccofins() : null);
				item.getMaterial().setPercentualBciss(material.getPercentualBciss() != null ? material.getPercentualBciss() : null);
				item.getMaterial().setPercentualBccsll(material.getPercentualBccsll() != null ? material.getPercentualBccsll() : null);
				item.getMaterial().setPercentualBcir(material.getPercentualBcir() != null ? material.getPercentualBcir() : null);
				item.getMaterial().setPercentualBcinss(material.getPercentualBcinss() != null ? material.getPercentualBcinss() : null);
				item.getMaterial().setPercentualBcfcp(material.getPercentualBcfcp() != null ? material.getPercentualBcfcp() : null);
				item.getMaterial().setPercentualBcfcpst(material.getPercentualBcfcpst() != null ? material.getPercentualBcfcpst() : null);
				
				item.getMaterial().setBasecalculost(material.getBasecalculost());
				item.getMaterial().setValorst(material.getValorst());
			}
		}
		
		return (T)item;
	}
	
	private boolean exibirCST(Tipocobrancaicms tipocobrancaicms) {
		return tipocobrancaicms != null && (
				Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(tipocobrancaicms)
				|| Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(tipocobrancaicms)
				|| Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(tipocobrancaicms)
				|| Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO.equals(tipocobrancaicms)
				|| Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(tipocobrancaicms)
				|| Tipocobrancaicms.OUTROS.equals(tipocobrancaicms)
				|| Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(tipocobrancaicms)
				|| Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(tipocobrancaicms)
				|| Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(tipocobrancaicms)
				|| Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(tipocobrancaicms)
				|| Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(tipocobrancaicms));
	}
	
	private void preencherDadosTributacao(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		item.setIncluiricmsvalor(grupotributacao.getIncluiricmsvalor());
		item.setOrigemproduto(grupotributacao.getOrigemprodutoicms());
		item.setTributadoicms(grupotributacao.getTributadoicms());
		item.setTipotributacaoicms(grupotributacao.getTipotributacaoicms());
		item.setTipocobrancaicms(grupotributacao.getTipocobrancaicms());
		item.setModalidadebcicms(grupotributacao.getModalidadebcicms());
		item.setCest(grupotributacao.getCest());
		item.setModalidadebcicmsst(grupotributacao.getModalidadebcicmsst());
		item.setMargemvaloradicionalicmsst(grupotributacao.getMargemvaloradicionalicmsst());
		item.setReducaobcicms(grupotributacao.getReducaobcicms());
		item.setBcoperacaopropriaicms(grupotributacao.getBcoperacaopropriaicms());
		item.setUficmsst(grupotributacao.getUficmsst());
		item.setMotivodesoneracaoicms(grupotributacao.getMotivodesoneracaoicms());
		item.setPercentualdesoneracaoicms(grupotributacao.getPercentualdesoneracaoicms());
		item.setAbaterdesoneracaoicms(grupotributacao.getAbaterdesoneracaoicms());
		item.setAliquotacreditoicms(grupotributacao.getAliquotacreditoicms());
		item.setReducaobcicmsst(grupotributacao.getReducaobcicmsst());
		item.setValorFiscalIcms(grupotributacao.getValorFiscalIcms());
		item.setItemlistaservico(grupotributacao.getItemlistaservico());
		
		item.setIncluiripivalor(grupotributacao.getIncluiripivalor());
		item.setTipocobrancaipi(grupotributacao.getTipocobrancaipi());
		item.setTipocalculoipi(grupotributacao.getTipocalculoipi());
		item.setAliquotareaisipi(grupotributacao.getAliquotareaisipi());					
		item.setCodigoenquadramentoipi(grupotributacao.getCodigoenquadramentoipi());
		item.setValorFiscalIpi(grupotributacao.getValorFiscalIpi());

		item.setIncluirpisvalor(grupotributacao.getIncluirpisvalor());
		item.setTipocobrancapis(grupotributacao.getTipocobrancapis());
		item.setTipocalculopis(grupotributacao.getTipocalculopis());
		item.setAliquotareaispis(grupotributacao.getAliquotareaispis());					
		
		item.setIncluircofinsvalor(grupotributacao.getIncluircofinsvalor());
		item.setTipocobrancacofins(grupotributacao.getTipocobrancacofins());
		item.setTipocalculocofins(grupotributacao.getTipocalculocofins());
		item.setAliquotareaiscofins(grupotributacao.getAliquotareaiscofins());
		
		item.setIncluirissvalor(grupotributacao.getIncluirissvalor());					
		item.setTipotributacaoiss(grupotributacao.getTipotributacaoiss());
		item.setIncentivofiscal(grupotributacao.getIncentivofiscal());
	}
	
	public void preencherDadosTributacaoForDI(Notafiscalprodutoitem item, Grupotributacao grupotributacao){
		preencherDadosTributacao(item, grupotributacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoitemDAO#deleteNaoExistentes(Notafiscalproduto bean, String whereIn)
	 *
	 * @param bean
	 * @param string
	 * @since 10/09/2012
	 * @author Rodrigo Freitas
	 */
	public void deleteNaoExistentes(Notafiscalproduto bean, String string) {
		notafiscalprodutoitemDAO.deleteNaoExistentes(bean, string);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Notafiscalprodutoitem> findForPdfListagem(String whereIn){
		return notafiscalprodutoitemDAO.findForPdfListagem(whereIn);
	}
	
	/**
	 * 
	 * @param notafiscalproduto
	 * @return
	 */
	public List<Notafiscalprodutoitem> findByNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoitemDAO.findByNotafiscalproduto(notafiscalproduto);
	}
	
	/**
	 * Atualiza os dados da tabela Notafiscalprodutoitem, a priore somente o localarmazenagem ser� atualizado.
	 * 
	 * @param cdNota
	 * @param listaItens
	 */
	public void atualizaDados(Integer cdNota,List<Notafiscalprodutoitem> listaNotafiscalprodutoitem){
		if(cdNota==null) 
			throw new SinedException("N�o � poss�vel atualizar os dados do produto com uma nota nula.");
		
		if(listaNotafiscalprodutoitem==null || listaNotafiscalprodutoitem.isEmpty())
			throw new SinedException("N�o � poss�vel atualizar os dados do produto de uma nota sem os produtos vinculados a ela.");
		
		for (Notafiscalprodutoitem notafiscalprodutoitem : listaNotafiscalprodutoitem) {
			notafiscalprodutoitemDAO.atualizaDados(cdNota,notafiscalprodutoitem.getCdnotafiscalprodutoitem(),notafiscalprodutoitem.getLocalarmazenagem());	
		}
		
	}
	
	/**
	 * Busca todos os itens de nota fiscal para montagem do relat�rio de livro de registro de sa�da.
	 * 
	 * @param filtro
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Notafiscalprodutoitem> findForLivroSaida(LivroregistrosaidaFiltro filtro) {

		return notafiscalprodutoitemDAO.findForLivroSaida(filtro);
	}
	
	/**
	 * M�todo criado para atualizar o Cfop caso 
	 * o mesmo seja alterado em notas que possua carta de 
	 * corre��o
	 * @param cdnotafiscalprodutoitem
	 * @param cfop
	 */
	public void atualizaCfop(Integer cdnotafiscalprodutoitem, Cfop cfop) {
		notafiscalprodutoitemDAO.atualizaCfop(cdnotafiscalprodutoitem, cfop);
	}	
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.NotafiscalprodutoitemDAO#loadForReceita(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public List<Notafiscalprodutoitem> loadForReceita(String whereIn) {
		return notafiscalprodutoitemDAO.loadForReceita(whereIn);
	}
	
	public void ordenaListaItens(Notafiscalproduto notafiscalproduto){
		if(!parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_PRODUTOS_NFP)){
			Collections.sort(notafiscalproduto.getListaItens(), new Comparator<Notafiscalprodutoitem>() {
				@Override
				public int compare(Notafiscalprodutoitem o1, Notafiscalprodutoitem o2) {
					int comparisson = 0;
					
					if(o1.getVendamaterial() != null && o1.getVendamaterial().getVenda() != null &&
							o2.getVendamaterial() != null && o2.getVendamaterial().getVenda() != null){
						Venda v1 = o1.getVendamaterial().getVenda();
						Venda v2 = o2.getVendamaterial().getVenda();
						comparisson = v1.getDtvenda().compareTo(v2.getDtvenda());
						if(comparisson == 0){
							comparisson = v1.getCdvenda().compareTo(v2.getCdvenda());
							if(comparisson == 0){
								comparisson = o1.getVendamaterial().getOrdem().compareTo(o2.getVendamaterial().getOrdem());
							}
						}
					}
					return comparisson;
				}
			});
		}
	}
	
	public Notafiscalprodutoitem loadForPopupVincularNotaEntrada(Notafiscalprodutoitem notafiscalprodutoitem){
		return notafiscalprodutoitemDAO.loadForPopupVincularNotaEntrada(notafiscalprodutoitem);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param notafiscalprodutoitem
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	public void updateICMSInterestadual(Notafiscalprodutoitem notafiscalprodutoitem) {
		notafiscalprodutoitemDAO.updateICMSInterestadual(notafiscalprodutoitem);
	}
	
	public List<Notafiscalprodutoitem> findByWhereInCdnota(String whereInCdnota){
		if (whereInCdnota==null || whereInCdnota.trim().equals("")){
			return new ArrayList<Notafiscalprodutoitem>();
		}
		return notafiscalprodutoitemDAO.findByWhereInCdnota(whereInCdnota);
	}
	
	public List<Notafiscalprodutoitem> findForRateioFaturamento(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoitemDAO.findForRateioFaturamento(notafiscalproduto);
	}
	public void ordenarListaPorPedidoVenda(Notafiscalproduto notafiscalproduto) {
		if(parametrogeralService.getBoolean(Parametrogeral.ORDENAR_PRODUTOS_DANFE_ITEM_NF)){
			Collections.sort(notafiscalproduto.getListaItens(), new Comparator<Notafiscalprodutoitem>() {
				@Override
				public int compare(Notafiscalprodutoitem o1, Notafiscalprodutoitem o2) {
					int comparisson = 0;
					
					if (o1.getCdnotafiscalprodutoitem() != null && o2.getCdnotafiscalprodutoitem() != null){
						comparisson = o1.getCdnotafiscalprodutoitem().compareTo(o2.getCdnotafiscalprodutoitem());
					}
					return comparisson;
				}
			});
		}else if(parametrogeralService.getBoolean(Parametrogeral.ORDENAR_PRODUTOS_NF_ORDEM_PEDIDO_VENDA)){
			Collections.sort(notafiscalproduto.getListaItens(), new Comparator<Notafiscalprodutoitem>() {
				@Override
				public int compare(Notafiscalprodutoitem o1, Notafiscalprodutoitem o2) {
					int comparisson = 0;
					
					if(o1.getVendamaterial() != null &&
							o2.getVendamaterial() != null ){
						Vendamaterial v1 = o1.getVendamaterial();
						Vendamaterial v2 = o2.getVendamaterial();
						comparisson = v1.getCdvendamaterial().compareTo(v2.getCdvendamaterial());
						if(comparisson == 0){
							comparisson = o1.getVendamaterial().getOrdem().compareTo(o2.getVendamaterial().getOrdem());
						}
					}else if(o1.getPedidovendamaterial() != null &&
							o2.getPedidovendamaterial() != null){
						Pedidovendamaterial v1 = o1.getPedidovendamaterial();
						Pedidovendamaterial v2 = o2.getPedidovendamaterial();
						comparisson = v1.getCdpedidovendamaterial().compareTo(v2.getCdpedidovendamaterial());
						if(comparisson == 0 && o1.getPedidovendamaterial().getOrdem() != null && o2.getPedidovendamaterial().getOrdem() != null){
							comparisson = o1.getPedidovendamaterial().getOrdem().compareTo(o2.getPedidovendamaterial().getOrdem());
						}
					}else if (o1.getCdnotafiscalprodutoitem() != null && o2.getCdnotafiscalprodutoitem() != null){
						comparisson = o1.getCdnotafiscalprodutoitem().compareTo(o2.getCdnotafiscalprodutoitem());
					}
					return comparisson;
				}
			});
		}	
	}
	
	public Notafiscalprodutoitem findByVendaMaterial (Vendamaterial vendaMaterial){
		return notafiscalprodutoitemDAO.findByVendaMaterial(vendaMaterial);
	}
	public Notafiscalprodutoitem findByMaterialNotaFiscal (Notafiscalproduto nota, Material material){
		return notafiscalprodutoitemDAO.findByMaterialNotaFiscal(nota, material);
	}
}
