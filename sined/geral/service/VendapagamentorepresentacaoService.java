package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamentorepresentacao;
import br.com.linkcom.sined.geral.dao.VendapagamentorepresentacaoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaPagamentoRepresentacaoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendapagamentorepresentacaoService extends GenericService<Vendapagamentorepresentacao> {

	private VendapagamentorepresentacaoDAO vendapagamentorepresentacaoDAO;
	
	public void setVendapagamentorepresentacaoDAO(VendapagamentorepresentacaoDAO vendapagamentorepresentacaoDAO) {
		this.vendapagamentorepresentacaoDAO = vendapagamentorepresentacaoDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendapagamentorepresentacaoDAO#findVendaPagamentoRepresentacaoByVenda(Venda venda)
	*
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Vendapagamentorepresentacao> findVendaPagamentoRepresentacaoByVenda(Venda venda) {
		return vendapagamentorepresentacaoDAO.findVendaPagamentoRepresentacaoByVenda(venda);
	}
	
	public List<VendaPagamentoRepresentacaoWSBean> toWSList(List<Vendapagamentorepresentacao> listaVendaPagamentoRepresentacao){
		List<VendaPagamentoRepresentacaoWSBean> lista = new ArrayList<VendaPagamentoRepresentacaoWSBean>();
		if(SinedUtil.isListNotEmpty(listaVendaPagamentoRepresentacao)){
			for(Vendapagamentorepresentacao vpr: listaVendaPagamentoRepresentacao){
				VendaPagamentoRepresentacaoWSBean bean = new VendaPagamentoRepresentacaoWSBean();
				bean.setCdvendapagamentorepresentacao(vpr.getCdvendapagamentorepresentacao());
				//bean.setDataparcela(vpr.getDataparcela());
				bean.setDataparcela(new java.util.Date(vpr.getDataparcela().getTime()));
				bean.setDocumento(ObjectUtils.translateEntityToGenericBean(vpr.getDocumento()));
				bean.setDocumentotipo(ObjectUtils.translateEntityToGenericBean(vpr.getDocumentotipo()));
				bean.setFornecedor(ObjectUtils.translateEntityToGenericBean(vpr.getFornecedor()));
				bean.setValororiginal(vpr.getValororiginal());
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vpr.getVenda()));
				
				lista.add(bean);
			}
		}
		return lista;
	}
}
