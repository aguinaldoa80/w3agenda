package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.geral.dao.NotaerrocorrecaoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaNotaErroCorrecaoBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaerrocorrecaoService extends GenericService<Notaerrocorrecao>{

	private NotaerrocorrecaoDAO notaerrocorrecaoDAO;
	
	public void setNotaerrocorrecaoDAO(NotaerrocorrecaoDAO notaerrocorrecaoDAO) {this.notaerrocorrecaoDAO = notaerrocorrecaoDAO;}
	
	public String getMensagemErroCorrecao(Nota nota, Notaerrocorrecaotipo notaerrocorrecaotipo, String prefixo) {
		StringBuilder mensagem = new StringBuilder();
		List<Notaerrocorrecao> lista = notaerrocorrecaoDAO.findByNota(nota, notaerrocorrecaotipo, prefixo);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Notaerrocorrecao notaerrocorrecao : lista){
				if(StringUtils.isNotBlank(notaerrocorrecao.getCorrecao()) && mensagem.indexOf(notaerrocorrecao.getCorrecao()) == -1){
					mensagem.append(notaerrocorrecao.getCorrecao()).append("\n");
				}
			}
		}
		return mensagem.length() > 0 ? mensagem.substring(0, mensagem.length()-1) : null;
	}
	
	public boolean existeCodigoPrefixoNotaerrocorrecao(Notaerrocorrecao notaerrocorrecao){
		return notaerrocorrecaoDAO.existeCodigoPrefixoNotaerrocorrecao(notaerrocorrecao);    
	}
	
	public Notaerrocorrecao loadNotaErroCorrecao(VerificaNotaErroCorrecaoBean bean) {
		return notaerrocorrecaoDAO.loadNotaErroCorrecao(bean);
	}

}
