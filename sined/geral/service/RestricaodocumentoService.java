package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Restricaodocumento;
import br.com.linkcom.sined.geral.dao.RestricaodocumentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RestricaodocumentoService extends GenericService<Restricaodocumento> {

	private RestricaodocumentoDAO restricaodocumentoDAO;
	
	public void setRestricaodocumentoDAO(
			RestricaodocumentoDAO restricaodocumentoDAO) {
		this.restricaodocumentoDAO = restricaodocumentoDAO;
	}
	
	public List<Restricaodocumento> findByContratoUltimaRestricao(Contrato contrato) {
		return restricaodocumentoDAO.findByContratoUltimaRestricao(contrato);
	}

	public void deletaDocumentos(Restricao restricao) {
		restricaodocumentoDAO.deletaDocumentos(restricao);
	}

}
