package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaohistorico;
import br.com.linkcom.sined.geral.dao.GrupotributacaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaohistoricoService  extends GenericService<Grupotributacaohistorico>{

	protected GrupotributacaohistoricoDAO grupotributacaohistoricoDAO;
	
	public void setGrupotributacaohistoricoDAO(
			GrupotributacaohistoricoDAO grupotributacaohistoricoDAO) {
		this.grupotributacaohistoricoDAO = grupotributacaohistoricoDAO;
	}
	
	/**
	 * Busca todos os registros de histórico de um determinado {@link Grupotributacao}
	 * 
	 * @author Giovane Freitas
	 * @param grupotributacao
	 * @return
	 */
	public List<Grupotributacaohistorico> findByGrupotributacao(Grupotributacao grupotributacao) {

		if (grupotributacao == null || grupotributacao.getCdgrupotributacao() == null)
			return Collections.emptyList();
		
		return grupotributacaohistoricoDAO.findByGrupotributacao(grupotributacao);
	}

}
