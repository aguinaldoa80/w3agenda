package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.dao.GrupotributacaoimpostoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaoimpostoService extends GenericService<Grupotributacao>{

	private GrupotributacaoimpostoDAO grupotributacaoimpostoDAO;
	
	public void setGrupotributacaoimpostoDAO(GrupotributacaoimpostoDAO grupotributacaoimpostoDAO) {this.grupotributacaoimpostoDAO = grupotributacaoimpostoDAO;}
	
	public List<Grupotributacaoimposto> findByGrupotributacao(Grupotributacao grupotributacao){
		return grupotributacaoimpostoDAO.findByGrupotributacao(grupotributacao);
	}
		
	public Grupotributacaoimposto findByGrupotributacaoControle(Grupotributacao grupotributacao, Faixaimpostocontrole controle){
		return grupotributacaoimpostoDAO.findByGrupotributacaoControle(grupotributacao, controle);		
	}
	
}
