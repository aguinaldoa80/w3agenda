package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.dao.MeiocontatoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.meiocontato.MeiocontatoRESTModel;

public class MeiocontatoService extends GenericService<Meiocontato> {

	/* singleton */
	private static MeiocontatoService instance;
	protected MeiocontatoDAO meiocontatoDAO;
	
	public static MeiocontatoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MeiocontatoService.class);
		}
		return instance;
	}

	public void setMeiocontatoDAO(MeiocontatoDAO meiocontatoDAO) {
		this.meiocontatoDAO = meiocontatoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @return
	 */
	public boolean existeMeiocontatoPadrao() {
		return meiocontatoDAO.existeMeiocontatoPadrao();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param cdmeiocontato
	 * @return
	 */
	public boolean isMeiocontatoPadrao(Integer cdmeiocontato){
		return meiocontatoDAO.isMeiocontatoPadrao(cdmeiocontato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @return
	 */
	public Meiocontato findMeioContatoPadrao(){
		return meiocontatoDAO.findMeioContatoPadrao();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 */
	public List<Meiocontato> findAtivos(){
		return meiocontatoDAO.findAtivos();
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.MeiocontatoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<MeiocontatoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.MeiocontatoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<MeiocontatoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MeiocontatoRESTModel> lista = new ArrayList<MeiocontatoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Meiocontato bean : meiocontatoDAO.findForAndroid(whereIn))
				lista.add(new MeiocontatoRESTModel(bean));
		}
		
		return lista;
	}
}
