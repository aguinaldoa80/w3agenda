package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Prontuario;
import br.com.linkcom.sined.geral.bean.Prontuariohistorico;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProntuariohistoricoService extends GenericService<Prontuariohistorico> {
	
	public Prontuariohistorico saveProntuariohistorico(Prontuario prontuario){
		Prontuariohistorico prontuariohistorico = new Prontuariohistorico();
		prontuariohistorico.setProntuario(prontuario);
		prontuariohistorico.setData(SinedDateUtils.currentTimestamp());
		prontuariohistorico.setUsuario(SinedUtil.getUsuarioLogado());
		prontuariohistorico.setTerapeuta(prontuario.getTerapeuta());
		prontuariohistorico.setPaciente(prontuario.getPaciente());
		prontuariohistorico.setNumero(prontuario.getNumero());
		prontuariohistorico.setDtinicio(prontuario.getDtinicio());
		prontuariohistorico.setHrinicio(prontuario.getHrinicio());
		prontuariohistorico.setDtfim(prontuario.getDtfim());
		prontuariohistorico.setHrfim(prontuario.getHrfim());
		prontuariohistorico.setObservacao(prontuario.getObservacao());
		
		saveOrUpdate(prontuariohistorico);
		
		return prontuariohistorico;
	}
	
}
