package br.com.linkcom.sined.geral.service;

import java.awt.Color;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptException;

import net.sf.json.JSON;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.AtributosMaterial;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Embalagem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.MaterialEcomerceExcluido;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Materialcaracteristica;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialcolaborador;
import br.com.linkcom.sined.geral.bean.Materialempresa;
import br.com.linkcom.sined.geral.bean.Materialformulamargemcontribuicao;
import br.com.linkcom.sined.geral.bean.Materialformulapeso;
import br.com.linkcom.sined.geral.bean.Materialformulavalorvenda;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialhistorico;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.bean.Materialkitflexivel;
import br.com.linkcom.sined.geral.bean.Materialkitflexivelformula;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Materialpneumodelo;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialproducaoformula;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialrelacionadoformula;
import br.com.linkcom.sined.geral.bean.Materialsimilar;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Materialvenda;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Servico;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.Materialacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Retencao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEtiquetaMaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.dao.MaterialDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferenciaMateriaisVendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ImpostoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ServicoContratoJson;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAtualizacaoMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.MaterialEtiquetaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ResumoMaterialunidadeconversao;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.MaterialReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MaterialWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ProdutoWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ServicoWSBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.WmsBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.TransferenciaCategoriaMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AtualizacaoEstoqueFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.TransferenciaCategoriaFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.MaterialLoteBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.Materialajustarpreco;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltroReportTemplate;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SugestaocompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.MaterialHistoricoPrecoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.offline.MaterialOfflineJSON;
import br.com.linkcom.sined.util.rest.android.material.MaterialRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.material.MaterialW3producaoRESTModel;
import br.com.linkcom.sined.wms.WmsWebServiceStub;

import com.lowagie.text.pdf.BarcodeEAN;

public class MaterialService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Material> {

	public static String NOME_PARAM_MATERIALCLASSE_SIMILAR = "PARAM_MATERIALCLASSE_SIMILAR";
	
	private MaterialDAO materialDAO;
	private ServicoService servicoService;
	private ProdutoService produtoService;
	private EpiService epiService;
	private BempatrimonioService bempatrimonioService;
	private InspecaoitemService inspecaoitemService;
	private GrupotributacaoimpostoService grupotributacaoimpostoService;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private FaixaimpostoService faixaimpostoService;
	private MaterialService materialService;
	private MaterialgrupoService materialgrupoService;
	private UnidademedidaService unidademedidaService;
	private MaterialproducaoService materialproducaoService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private MaterialrelacionadoService materialrelacionadoService;
	private ParametrogeralService parametrogeralService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private VendaService vendaService;
	private LoteestoqueService loteestoqueService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private GerenciamentomaterialService gerenciamentomaterialService;
	private FormulacustoService formulacustoService;
	private LotematerialService lotematerialService;
	private MaterialkitflexivelService materialkitflexivelService;
	private LocalarmazenagemService localarmazenagemService;
	private MaterialformulacustoService materialformulacustoService;
	private PedidovendasincronizacaoService pedidovendasincronizacaoService;
	private MaterialhistoricoService materialhistoricoService;
	private MaterialformulapesoService materialformulapesoService;
	private PneumodeloService pneumodeloService;
	private MaterialformulavendamaximoService materialformulavendamaximoService;
	private MaterialformulavendaminimoService materialformulavendaminimoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialEcomerceExcluidoService materialEcomerceExcluidoService;
	
	public void setMaterialhistoricoService(
			MaterialhistoricoService materialhistoricoService) {
		this.materialhistoricoService = materialhistoricoService;
	}
	public void setUnidademedidaconversaoService(
			UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setMaterialproducaoService(
			MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public void setServicoService(ServicoService servicoService) {
		this.servicoService = servicoService;
	}
	public void setEpiService(EpiService epiService) {
		this.epiService = epiService;
	}
	public void setBempatrimonioService(BempatrimonioService bempatrimonioService) {
		this.bempatrimonioService = bempatrimonioService;
	}
	public void setMaterialDAO(MaterialDAO materialDAO) {
		this.materialDAO = materialDAO;
	}
	public void setInspecaoitemService(InspecaoitemService inspecaoitemService) {
		this.inspecaoitemService = inspecaoitemService;
	}
	public void setGrupotributacaoimpostoService(GrupotributacaoimpostoService grupotributacaoimpostoService) {
		this.grupotributacaoimpostoService = grupotributacaoimpostoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFaixaimpostoService(FaixaimpostoService faixaimpostoService) {
		this.faixaimpostoService = faixaimpostoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {
		this.materialrelacionadoService = materialrelacionadoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {
		this.materialformulavalorvendaService = materialformulavalorvendaService;
	}
	public void setGerenciamentomaterialService(GerenciamentomaterialService gerenciamentomaterialService) {
		this.gerenciamentomaterialService = gerenciamentomaterialService;
	}
	public void setFormulacustoService(FormulacustoService formulacustoService) {
		this.formulacustoService = formulacustoService;
	}
	public void setLotematerialService(LotematerialService lotematerialService) {
		this.lotematerialService = lotematerialService;
	}
	public void setMaterialkitflexivelService(MaterialkitflexivelService materialkitflexivelService) {
		this.materialkitflexivelService = materialkitflexivelService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMaterialformulacustoService(MaterialformulacustoService materialformulacustoService) {
		this.materialformulacustoService = materialformulacustoService;
	}
	public void setPedidovendasincronizacaoService(PedidovendasincronizacaoService pedidovendasincronizacaoService) {
		this.pedidovendasincronizacaoService = pedidovendasincronizacaoService;
	}
	public void setMaterialformulapesoService(MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setPneumodeloService(PneumodeloService pneumodeloService) {
		this.pneumodeloService = pneumodeloService;
	}
	public void setMaterialformulavendamaximoService(MaterialformulavendamaximoService materialformulavendamaximoService) {
		this.materialformulavendamaximoService = materialformulavendamaximoService;
	}
	public void setMaterialformulavendaminimoService(MaterialformulavendaminimoService materialformulavendaminimoService) {
		this.materialformulavendaminimoService = materialformulavendaminimoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) { this.materialCustoEmpresaService = materialCustoEmpresaService; }
	public void setMaterialFaixaMarkupService(
			MaterialFaixaMarkupService materialFaixaMarkupService) {
		this.materialFaixaMarkupService = materialFaixaMarkupService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setMaterialEcomerceExcluidoService(MaterialEcomerceExcluidoService materialEcomerceExcluidoService) {
		this.materialEcomerceExcluidoService = materialEcomerceExcluidoService;
	}

	/* singleton */
	private static MaterialService instance;
	public static MaterialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialService.class);
		}
		return instance;
	}
	
	@Override
	public void saveOrUpdate(Material bean) {
		Set<Materialproducao> listaProducao = bean.getListaProducao();
		bean.setListaProducao(null);
		Set<Materialrelacionado> listaMaterialrelacionado = bean.getListaMaterialrelacionado();
		bean.setListaMaterialrelacionado(null);
		Set<Materialkitflexivel> listaMaterialkitflexivel = bean.getListaMaterialkitflexivel();
		bean.setListaMaterialkitflexivel(null);
		
		super.saveOrUpdate(bean);
		
		if(bean.getCdmaterial() != null){
			materialproducaoService.deleteWhereNotInMaterial(bean, SinedUtil.listAndConcatenate(listaProducao, "cdmaterialproducao", ","));
			if(listaMaterialrelacionado != null && !listaMaterialrelacionado.isEmpty()){
				materialrelacionadoService.deleteWhereNotInMaterial(bean, SinedUtil.listAndConcatenate(listaMaterialrelacionado, "cdmaterialrelacionado", ","));
			}else {
				materialrelacionadoService.deleteWhereNotInMaterial(bean, null);
			}
			if(listaMaterialkitflexivel != null && !listaMaterialkitflexivel.isEmpty()){
				materialkitflexivelService.deleteWhereNotInMaterial(bean, SinedUtil.listAndConcatenate(listaMaterialkitflexivel, "cdmaterialkitflexivel", ","));
			}else {
				materialkitflexivelService.deleteWhereNotInMaterial(bean, null);
			}
		}
		if(listaProducao != null && listaProducao.size() > 0){
			for (Materialproducao materialproducao : listaProducao) {
				materialproducao.setMaterialmestre(bean);
				materialproducaoService.saveOrUpdate(materialproducao);
			}
		}
		if(listaMaterialrelacionado != null && listaMaterialrelacionado.size() > 0){
			for (Materialrelacionado materialrelacionado : listaMaterialrelacionado) {
				materialrelacionado.setMaterial(bean);
				materialrelacionadoService.saveOrUpdate(materialrelacionado);
			}
		}
		if(listaMaterialkitflexivel != null && listaMaterialkitflexivel.size() > 0){
			for (Materialkitflexivel materialkitflexivel : listaMaterialkitflexivel) {
				materialkitflexivel.setMaterial(bean);
				materialkitflexivelService.saveOrUpdate(materialkitflexivel);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.MaterialDAO#findForMaterialmestregrade(String s)
	 *
	 * @param s
	 * @return
	 * @author Luiz Fernando
	 * @since 27/12/2013
	 */
	public List<Material> findForMaterialmestregrade(String s){
		Integer cdmaterial = null;
		try {
			WebRequestContext request = NeoWeb.getRequestContext();
			cdmaterial = Integer.parseInt(request.getParameter("cdmaterial"));
		} catch (Exception e) {}
		return materialDAO.findForMaterialmestregrade(s, cdmaterial);
	}
	
	/**
	 * Verifica se um material � um patrim�nio.
	 * 
	 * @see #load(Material, String)
	 * @param material
	 * @return true - se o material for um patrim�nio
	 * 			false - caso contr�rio.
	 * @author Fl�vio Tavares
	 */
	public boolean verificaMaterialPatrimonio(Material material){
		Material load = this.load(material, "material.patrimonio");
		return BooleanUtils.isTrue(load.getPatrimonio());
	}
	
	/**
	 * M�todo que faz a verifica��o se insere ou faz update no Servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ServicoService#countServico
	 * @see br.com.linkcom.sined.geral.service.ServicoService#updateServico
	 * @see br.com.linkcom.sined.geral.service.ServicoService#insertServico
	 * @param bean
	 * @param update
	 * @param servico
	 * @author Rodrigo Freitas
	 */
	public void insereServico(final Material bean, Boolean update,
			Servico servico) {
		if (servico != null) {
			servico.setCdmaterial(bean.getCdmaterial());
			if (update) {
				Long countServico = servicoService.countServico(bean);
				if (countServico != null && countServico >  0) {
					servicoService.updateServico(servico);
				} else {
					servicoService.insertServico(servico);
				}
			} else {
				servicoService.insertServico(servico);
			}
		}
	}

	/**
	 * M�todo que faz a verifica��o se insere ou faz update no Epi.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EpiService#countEpi
	 * @see br.com.linkcom.sined.geral.service.EpiService#updateEpi
	 * @see br.com.linkcom.sined.geral.service.EpiService#insertEpi
	 * @param bean
	 * @param update
	 * @param epi
	 * @author Rodrigo Freitas
	 */
	public void insereEpi(final Material bean, Boolean update, Epi epi) {
		if (epi != null) {
			epi.setCdmaterial(bean.getCdmaterial());
			if (update) {
				Long countEpi = epiService.countEpi(bean);
				if (countEpi != null && countEpi >  0) {
					epiService.updateEpi(epi);
				} else {
					epiService.insertEpi(epi);
				}
			} else {
				epiService.insertEpi(epi);
			}
		}
	}
	
	/**
	 * M�todo que verifica se o material � um produto e se for preenche o produto, 
	 * se n�o deleta todos os registros da tabela Produto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#deleteProduto
	 * @param bean
	 * @param listaClasse
	 * @param produto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Produto preencheProduto(Material bean) {
		Produto produto = new Produto(bean.getProduto_nomereduzido() != null && !bean.getProduto_nomereduzido().equals("") ? bean.getProduto_nomereduzido() : null,
									  bean.getProduto_qtdmin(),
									  bean.getProduto_anotacoes(),
									  bean.getProduto_largura(),
									  bean.getProduto_comprimento(),
									  bean.getProduto_altura(),
									  bean.getProduto_materialcorte(),
									  bean.getProduto_fatorconversao(),
									  bean.getProduto_arredondamentocomprimento(),
									  bean.getProduto_margemarredondamento());
		produto.setFatorconversaoproducao(bean.getProduto_fatorconversaoproducao());
		return produto;
	}
	
	/**
	 * M�todo que verifica se o material � um bempatrimonio e se for preenche o bempatrimonio, 
	 * se n�o deleta todos os registros da tabela bempatrimonio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#deletePatrimonio
	 * @param bean
	 * @param listaClasse
	 * @param bempatrimonio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Bempatrimonio preenchePatrimonio(Material bean) {
		Bempatrimonio bempatrimonio = new Bempatrimonio(bean.getCdmaterial(),
														bean.getPatrimonio_nomereduzido() != null && !bean.getPatrimonio_nomereduzido().equals("") ? bean.getPatrimonio_nomereduzido() : null,
														bean.getPatrimonio_valor(),
														bean.getPatrimonio_valormercado(),
														bean.getPatrimonio_valorref(),
														bean.getPatrimonio_vida(),
														bean.getPatrimonio_depreciacao(),
														bean.getPatrimonio_anotacoes());
		
		return bempatrimonio;
	}
	
	/**
	 * M�todo que verifica se o material � um epi e se for preenche o epi, 
	 * se n�o deleta todos os registros da tabela epi.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EpiService#deleteEpi
	 * @param bean
	 * @param listaClasse
	 * @param epi
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Epi preencheEpi(Material bean) {
		Epi epi = new Epi(bean.getEpi_nomereduzido() != null && !bean.getEpi_nomereduzido().equals("") ? bean.getEpi_nomereduzido() : null,
						  bean.getEpi_duracao(), 
						  bean.getEpi_anotacoes(),
						  bean.getEpi_caepi());
		return epi;
	}
	
	/**
	 * M�todo que verifica se o material � um servico e se for preenche o servico, 
	 * se n�o deleta todos os registros da tabela servico.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ServicoService#deleteServico
	 * @param bean
	 * @param listaClasse
	 * @param servico
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Servico preencheServico(Material bean) {
		Servico servico = new Servico(bean.getServico_nomereduzido() != null && !bean.getServico_nomereduzido().equals("") ? bean.getServico_nomereduzido() : null,
									  bean.getServico_qtdmin(),
									  bean.getServico_anotacoes());
		return servico;
	}
	
	/**
	 * M�todo que faz a verifica��o se insere ou faz update no produto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#countProduto
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#updateProduto
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#insertProduto
	 * @param bean
	 * @param update
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void insereProduto(final Material bean, Boolean update, Produto produto) {
		if (produto != null) {
			produto.setCdmaterial(bean.getCdmaterial());
			if (update) {
				Long countProduto = produtoService.countProduto(bean);
				if (countProduto != null && countProduto > 0) {
					produtoService.updateProduto(produto);
				} else {
					produtoService.insertProduto(produto);
				}
			} else {
				produtoService.insertProduto(produto);
			}
		}
	}
	
	/**
	 * M�todo que faz a verifica��o se insere ou faz update no bempatrimonio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#countPatrimonio
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#updatePatrimonio
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#insertPatrimonio
	 * @param bean
	 * @param update
	 * @param bempatrimonio
	 * @author Rodrigo Freitas
	 */
	public void inserePatrimonio(final Material bean, Boolean update,
			Bempatrimonio bempatrimonio) {
		if (bempatrimonio != null) {
			bempatrimonio.setCdmaterial(bean.getCdmaterial());
			if (update) {
				Long count = bempatrimonioService.countPatrimonio(bean);
				if (count != null && count > 0) {
					bempatrimonioService.updatePatrimonio(bempatrimonio);
				} else {
					bempatrimonioService.insertPatrimonio(bempatrimonio);
				}
			} else {
				bempatrimonioService.insertPatrimonio(bempatrimonio);
			}
		}
	}
	
	/**
	 * Carrega o servi�o do material e seta no form.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ServicoService#carregaServico
	 * @param form
	 * @param listaClasse
	 * @author Rodrigo Freitas
	 */
	public void setaServico(Material form) {
		Servico servico = servicoService.carregaServico(form);
		if (servico != null) {
			form.setServicoObj(servico);
		}
	}
	
	/**
	 * Carrega o epi do material e seta no form.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EpiService#carregaEpi
	 * @param form
	 * @param listaClasse
	 * @author Rodrigo Freitas
	 */
	public void setaEpi(Material form) {
		Epi epi = epiService.carregaEpi(form);
		if (epi != null) {
			form.setEpiObj(epi);
		}
	}
	
	/**
	 * Carrega o patrimonio do material e seta no form.
	 * 
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#carregaPatrimonio
	 * @param form
	 * @param listaClasse
	 * @author Rodrigo Freitas
	 */
	public void setaPatrimonio(Material form) {
		Bempatrimonio bempatrimonio = bempatrimonioService.carregaPatrimonio(form);
		if (bempatrimonio != null) {
			form.setPatrimonioObj(bempatrimonio);
		}
	}
	
	/**
	 * Carrega o produto do material e seta no form.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#carregaProduto
	 * @param form
	 * @param listaClasse
	 * @author Rodrigo Freitas
	 */
	public void setaProduto(Material form) {
		Produto produto = produtoService.carregaProduto(form);
		if (produto != null) {
			form.setProdutoObj(produto);
		}
	}
	
	
	/**
	 * Preenche a lista de empresas do bean de material.
	 * 
	 * @param listaMaterialempresa
	 * @param findAtivos
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> preencheListaEmpresa(List<Materialempresa> listaMaterialempresa, List<Empresa> findAtivos, List<Empresa> lista) {
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		
		for (Materialempresa materialempresa : listaMaterialempresa) {
			if (!findAtivos.contains(materialempresa.getEmpresa())) {
				listaEmpresa.add(materialempresa.getEmpresa());
			} else {
				lista.add(materialempresa.getEmpresa());
			}
		}
		return listaEmpresa;
	}
	
	/**
	 * Preenche a lista de Materialempresa do bean de material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#adicionaEmpresa
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void preencheMaterialEmpresa(WebRequestContext request, Material bean) {
		
		Set<Materialempresa> listaMaterialempresa = new ListSet<Materialempresa>(Materialempresa.class);
		
		List<Empresa> listaEmpresa = bean.getListaEmpresa();
		this.adicionaEmpresa(listaMaterialempresa, listaEmpresa);
		
		listaEmpresa = (List<Empresa>) request.getSession().getAttribute("listaEmpresa");
		if (listaEmpresa != null) {
			this.adicionaEmpresa(listaMaterialempresa, listaEmpresa);
		}
		
		bean.setListaMaterialempresa(listaMaterialempresa);
	}
	
	/**
	 * Prrenche a lista de Materialempresa a partir de uma lista de Empresa.
	 * 
	 * @param listaMaterialempresa
	 * @param listaEmpresa
	 * @author Rodrigo Freitas
	 */
	private void adicionaEmpresa(Set<Materialempresa> listaMaterialempresa, List<Empresa> listaEmpresa) {
		Materialempresa materialempresa;
		for (Empresa empresa : listaEmpresa) {
			materialempresa = new Materialempresa();
			materialempresa.setEmpresa(empresa);
			listaMaterialempresa.add(materialempresa);
		}
	}
	
	/**
	 * Faz a verifica��o do material se os nomes reduzidos de 
	 * Produto, Epi, Patrim�nio e Servi�o j� existem no sistema.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProdutoService#verificaProduto(Material)
	 * @see br.com.linkcom.sined.geral.service.BempatrimonioService#verificaPatrimonio(Material)
	 * @see br.com.linkcom.sined.geral.service.EpiService#verificaEpi(Material)
	 * @see br.com.linkcom.sined.geral.service.ServicoService#verificaServico(Material)
	 * @param bean
	 * @param errors
	 * @param listaClasse
	 * @author Rodrigo Freitas
	 */
	public void verificaMaterial(Material bean, BindException errors) {
		Long count = null;
		if (bean.getProduto()) {
			count = produtoService.verificaProduto(bean);
			if (count != null && count > 0) {
				errors.reject("001","Produto j� cadastrado no sistema.");
			}
		}
		count = null;
		
		if (bean.getPatrimonio()) {
			count = bempatrimonioService.verificaPatrimonio(bean);
			if (count != null && count > 0) {
				errors.reject("001","Patrimonio j� cadastrado no sistema.");
			}
		}
		count = null;
		
		if (bean.getEpi()) {
			count = epiService.verificaEpi(bean);
			if (count != null && count > 0) {
				errors.reject("001","Epi j� cadastrado no sistema.");
			}
		}
		count = null;
		
		if (bean.getServico()) {
			count = servicoService.verificaServico(bean);
			if (count != null && count > 0) {
				errors.reject("001","Servi�o j� cadastrado no sistema.");
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findAutocompleteByTipoGrupo(String q, Materialgrupo materialgrupo, Materialtipo materialtipo)
	*
	* @param q
	* @return
	* @since 15/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findAutocompleteByTipoGrupo(String q){
		Materialgrupo materialgrupo = null; 
		Materialtipo materialtipo = null;
		
		try {
			materialgrupo = new Materialgrupo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("cdmaterialgrupo")));
		} catch (Exception e) {} 
		try {
			materialtipo = new Materialtipo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("cdmaterialtipo")));
		} catch (Exception e) {} 
		
		return materialDAO.findAutocompleteByTipoGrupo(q, materialgrupo, materialtipo);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findByTipoGrupo
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo){
		return materialDAO.findByTipoGrupo(materialgrupo, materialtipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.service.MaterialService#findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo, Empresa empresa)
	 *
	 * @param materialgrupo
	 * @param materialtipo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo, Empresa empresa){
		return materialDAO.findByTipoGrupo(materialgrupo, materialtipo, empresa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findByGrupo
	 * 
	 * @param materialgrupo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findByGrupoFlex(Materialgrupo materialgrupo){
		return materialDAO.findByGrupo(materialgrupo);
	}
	
	
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findByTipoGrupoProdutoEpiServico(Materialgrupo materialgrupo, Materialtipo materialtipo){
		return materialDAO.findByTipoGrupoProdutoEpiServico(materialgrupo, materialtipo);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. 
	 * Retorna o s�mbolo da unidade de medida do material.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#getSimboloUnidadeMedida(Material)
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material getSimboloUnidadeMedidaETempoEntrega(Material material) {
		return materialDAO.getSimboloUnidadeMedidaETempoEntrega(material);
	}
	

	/**
	 * Busca o nome da unidade de medida do material.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#getSimboloUnidadeMedidaETempoEntrega
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String getNomeUnidadeMedida(Material material) {
		return materialDAO.getSimboloUnidadeMedidaETempoEntrega(material).getUnidademedida().getNome();
	}
	
	/**
	 * Retorna a lista de classes de um material
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialclasse> findClasses(Material material){
		List<Materialclasse> lista = new ArrayList<Materialclasse>();
		if (material != null && material.getCdmaterial() != null) {
			material = load(material);
			if (material.getEpi()) {
				lista.add(Materialclasse.EPI);
			}	
			if (material.getPatrimonio()) {
				lista.add(Materialclasse.PATRIMONIO);
			}	
			if (material.getProduto()) {
				lista.add(Materialclasse.PRODUTO);
			}
			if (material.getServico()) {
				lista.add(Materialclasse.SERVICO);
			}	
		}
		return lista;
	}
	
	/**
	 * Retorna a lista de classes de um material sem dar um load no material
	 * 
	 * Para isso os campos booleans do material tem que vir preenchidos.
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialclasse> findClassesWithoutLoad(Material material){
		List<Materialclasse> lista = new ArrayList<Materialclasse>();
		if (material != null) {
			if (material.getProduto() != null && material.getProduto()) {
				lista.add(Materialclasse.PRODUTO);
			}
			if (material.getPatrimonio() != null && material.getPatrimonio()) {
				lista.add(Materialclasse.PATRIMONIO);
			}	
			if (material.getEpi() != null && material.getEpi()) {
				lista.add(Materialclasse.EPI);
			}	
			if (material.getServico() != null && material.getServico()) {
				lista.add(Materialclasse.SERVICO);
			}	
		}
		return lista;
	}
	
	/**
	* M�todo que retorna a classe do material caso exista apenas uma
	*
	* @param material
	* @param load
	* @return
	* @since 09/05/2017
	* @author Luiz Fernando
	*/
	public Materialclasse getMaterialClasse(Material material, boolean load){
		if(material != null && material.getCdmaterial() != null){
			List<Materialclasse> lista = load ? findClassesWithoutLoad(material) : findClasses(material);
			if(lista != null && lista.size() == 1){
				return lista.get(0);
			}
		}
		
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param materialclasse
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> getListaMateriaisProdutoEpiServicoByClasse(Materialclasse materialclasse) {
		return materialDAO.getListaMateriaisProdutoEpiServicoByClasse(materialclasse);
	}
	
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findByTarefa(Tarefa) 
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Material> findByTarefa(Tarefa tarefa){
		return materialDAO.findByTarefa(tarefa);
	}
	

	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findAllForFlex(){
		return materialDAO.findAllForFlex();
	}
	
	public List<MaterialVO> findAllForFlexVO(){
		List<Material> listaMaterial = this.findAllForFlex();
		List<MaterialVO> listaMaterialVO = new ArrayList<MaterialVO>();
		MaterialVO materialVO;
		
		for (Material material : listaMaterial) {
			materialVO = new MaterialVO();
			materialVO.setCdmaterial(material.getCdmaterial());
			materialVO.setNome(material.getNome());
			materialVO.setValorcusto(material.getValorcusto());
			materialVO.setValorvenda(material.getValorvenda());
			listaMaterialVO.add(materialVO);
		}	
		return listaMaterialVO;
	}
	
	public List<Material> findAutocompleteByMontarGrade(String q){
		return materialDAO.findAutocompleteByMontarGrade(q);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public String getSimboloUnidadeMedida(Material material) {
		return materialDAO.getSimboloUnidadeMedida(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param listaMateriais
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findMateriais(List<Material> listaMateriais) {
		return materialDAO.findMateriais(listaMateriais);
	}
	
	/**
	 * M�todo que verifica se a itens de inspe��o iguais na lista
	 * 
	 * @see br.com.linkcom.sined.geral.service.InspecaoitemService#findItens(List)
	 * @param list
	 * @param errors
	 * @author Tom�s Rabelo
	 */
	public void verificaItemInspecaoIgual(Set<Materialinspecaoitem> list, BindException errors) {
		List<Inspecaoitem> itensRepetidos = new ArrayList<Inspecaoitem>();
		
		for (Materialinspecaoitem materialinspecaoitem : list) {
			int qtde = 0;
			for (Materialinspecaoitem materialinspecaoitemaux : list) {
				if(materialinspecaoitem.getInspecaoitem().getCdinspecaoitem().equals(materialinspecaoitemaux.getInspecaoitem().getCdinspecaoitem())){
					qtde++;
					if(qtde > 1){
						itensRepetidos.add(materialinspecaoitem.getInspecaoitem());
						break;
					}
				}
			}
		}
		
		if(itensRepetidos != null && itensRepetidos.size() > 0){
			List<Inspecaoitem> itens = inspecaoitemService.findItens(itensRepetidos);
			for (Inspecaoitem inspecaoitem : itens) 
				errors.reject("001", "O item "+inspecaoitem.getNome()+" da inspe��o s� pode aparecer uma vez.");
		}
	}
	
	/**
	 * Gera relat�rio da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(MaterialFiltro filtro) {
		Report report = new Report("/suprimento/material");
		List<Material> listaMateriais = materialDAO.findForMaterialReport(filtro);
//		ListagemResult<Material> materiais = materialDAO.findForListagem(filtro);
		if(listaMateriais != null && listaMateriais.size() > 0)
			report.setDataSource(listaMateriais);
		return report;
	}
	
	public Resource preparaArquivoMaterialExcelFormatado(MaterialFiltro filtro) throws IOException {
		
		List<Material> listaMateriais = materialDAO.findForMaterialReport(filtro);
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 35);
        
        HSSFRow row = null;
		HSSFCell cell = null;
        
		row = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) 39));
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cell.setCellValue("Materiais e Servi�os");
		
		
		planilha.setColumnWidth((short) 0, (short) (80*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
//		planilha.setColumnWidth((short) 2, (short) (80*35));
		planilha.setColumnWidth((short) 3, (short) (150*35));
		planilha.setColumnWidth((short) 4, (short) (200*35));
		planilha.setColumnWidth((short) 5, (short) (200*35));
//		planilha.setColumnWidth((short) 6, (short) (80*35));
		planilha.setColumnWidth((short) 7, (short) (150*35));
//		planilha.setColumnWidth((short) 8, (short) (80*35));
		planilha.setColumnWidth((short) 9, (short) (200*35));
		planilha.setColumnWidth((short) 10, (short) (150*35));
		planilha.setColumnWidth((short) 11, (short) (200*35));
		planilha.setColumnWidth((short) 12, (short) (200*35));
		planilha.setColumnWidth((short) 13, (short) (200*35));
		planilha.setColumnWidth((short) 14, (short) (200*35));
		planilha.setColumnWidth((short) 15, (short) (200*35));
		planilha.setColumnWidth((short) 16, (short) (200*35));
		planilha.setColumnWidth((short) 17, (short) (200*35));
		planilha.setColumnWidth((short) 18, (short) (200*35));
		planilha.setColumnWidth((short) 19, (short) (200*35));
		planilha.setColumnWidth((short) 20, (short) (200*35));
		planilha.setColumnWidth((short) 21, (short) (200*35));
		planilha.setColumnWidth((short) 22, (short) (200*35));
		planilha.setColumnWidth((short) 23, (short) (200*35));
		planilha.setColumnWidth((short) 24, (short) (200*35));
		planilha.setColumnWidth((short) 25, (short) (200*35));
		planilha.setColumnWidth((short) 26, (short) (200*35));
		planilha.setColumnWidth((short) 27, (short) (200*35));
		planilha.setColumnWidth((short) 28, (short) (200*35));
		planilha.setColumnWidth((short) 29, (short) (200*35));
		planilha.setColumnWidth((short) 30, (short) (200*35));
		planilha.setColumnWidth((short) 31, (short) (200*35));
		planilha.setColumnWidth((short) 32, (short) (200*35));
		planilha.setColumnWidth((short) 33, (short) (200*35));
		planilha.setColumnWidth((short) 34, (short) (200*35));
		planilha.setColumnWidth((short) 35, (short) (200*35));
		planilha.setColumnWidth((short) 36, (short) (200*35));
		planilha.setColumnWidth((short) 37, (short) (200*35));
		planilha.setColumnWidth((short) 38, (short) (200*35));
		if(SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE)){
		planilha.setColumnWidth((short) 39, (short) (200*35));
		}

		
		
		
		
		row = planilha.createRow(4);
		int nLinha = 0;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("C�digo");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Identificador");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Nome");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Nome para NF");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Refer�ncia");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("C�digo do Fabricante");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Unidade de Medida");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Grupo do Material");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Tipo do material");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Categoria do Material");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Cor do Material");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Quantidade por Unidade");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Localiza��o Estoque");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Modelo de Contrato em RTF");
		nLinha +=1;
		
		//cell = row.createCell((short) 13);
		//cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		//cell.setCellValue("Venda Promocional");
		
		//cell = row.createCell((short) 13);
		//cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		//cell.setCellValue("Ativo");
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Qtde. Refer�ncia");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Tempo de Entrega");
		nLinha +=1;
		
		if(SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE)){
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue("Pre�o de Custo");
			nLinha +=1;
		}
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Pre�o de Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Pre�o M�nimo de Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Pre�o M�ximo de Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("ICMS(%)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("IPI(%)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Outros(%)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Outros(R$)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Frete(%)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Frete(R$)");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Grupo de Tributa��o");
		nLinha +=1;
		
		//cell = row.createCell((short) 25);
		//cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		//cell.setCellValue("Tributa��o Estadual");
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Conta Gerencial Compra");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Conta Gerencial Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Centro de Custo Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Tipo de Item");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("C�digo de barras");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Gen�ro do NCM");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("NCM completo");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("CEST");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("EX TIPI");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Item da Lista de Servi�o");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Classe(s)");
		nLinha +=1;
				
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Peso do Material");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Fra��o da Unidade Priorit�ria na Venda");
		nLinha +=1;
		
		cell = row.createCell((short) nLinha);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Pre�o Unit�rio de Venda da Unidade Pritorit�ria");
		nLinha +=1;
		
		for (int i = 0; i < listaMateriais.size(); i++) {
			Material material = listaMateriais.get(i);
			nLinha = 0;
			row = planilha.createRow(i + 5);
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
			cell.setCellValue(material.getCdmaterial());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getIdentificacao() != null)
				cell.setCellValue(material.getIdentificacao());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(material.getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getNomenf() != null){
				cell.setCellValue(material.getNomenf());
			}
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getReferencia() != null)
				cell.setCellValue(material.getReferencia());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getCodigofabricante() != null)
				cell.setCellValue(material.getCodigofabricante());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getUnidademedida() != null)
				cell.setCellValue(material.getUnidademedida().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getMaterialgrupo() != null)
				cell.setCellValue(material.getMaterialgrupo().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getMaterialtipo() != null)
				cell.setCellValue(material.getMaterialtipo().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getMaterialcategoria() != null)
				cell.setCellValue(material.getMaterialcategoria().getDescricao());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getMaterialcor() != null)
				cell.setCellValue(material.getMaterialcor().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getQtdeunidade() != null)
				cell.setCellValue(material.getQtdeunidade());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getLocalizacaoestoque() != null)
				cell.setCellValue(material.getLocalizacaoestoque().getDescricao());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getEmpresamodelocontrato() != null)
				cell.setCellValue(material.getEmpresamodelocontrato().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getQtdereferencia() != null)
				cell.setCellValue(material.getQtdereferencia());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getTempoentrega() != null)
				cell.setCellValue(material.getTempoentrega());
			nLinha +=1;
			
			if(SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE)){
				cell = row.createCell((short) nLinha);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				if(material.getValorcusto() != null){
					cell.setCellValue(material.getValorcusto());
				}
				nLinha +=1;
			}
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(material.getValorvenda() != null)
				cell.setCellValue(material.getValorvenda());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(material.getValorvendaminimo() != null)
				cell.setCellValue(material.getValorvendaminimo());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(material.getValorvendamaximo() != null)
				cell.setCellValue(material.getValorvendamaximo());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
//			if(material.getIcms() != null)
//				cell.setCellValue(material.getIcms() != null ? " " + material.getIcms().toString() : "");
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
//			if(material.getIpi() != null)
//				cell.setCellValue(material.getIpi() != null ? "" + material.getIpi().toString() : "");
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
//			if(material.getLucro() != null)
//				cell.setCellValue(material.getLucro() != null ? "" + material.getLucro().toString() : "");
			nLinha +=1;
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
//			if(material.getValorlucro() != null)
//				cell.setCellValue(material.getValorlucro() != null ? "R$" + material.getValorlucro().toString() : "");
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(material.getFrete() != null)
				cell.setCellValue(material.getFrete() != null ? "" + material.getFrete().toString() : "");
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			if(material.getValorfrete() != null)
				cell.setCellValue(material.getValorfrete().getValue().doubleValue());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getGrupotributacao() != null)
				cell.setCellValue(material.getGrupotributacao().getNome());
			nLinha +=1;
			
			//cell = row.createCell((short) 26);
			//cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			//if(material.getTributacaoestadual() != null)
				//cell.setCellValue(material.getTributacaoestadual());
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getContagerencial() != null)
				cell.setCellValue(material.getContagerencial().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getContagerencialvenda() != null)
				cell.setCellValue(material.getContagerencialvenda().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getCentrocustovenda() != null)
				cell.setCellValue(material.getCentrocustovenda().getNome());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getTipoitemsped() != null)
				cell.setCellValue(material.getTipoitemsped().getDescricao());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getCodigobarras() != null)
				cell.setCellValue(material.getCodigobarras());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getNcmcapitulo() != null)
				cell.setCellValue(material.getNcmcapitulo().getDescricao());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getNcmcompleto() != null)
				cell.setCellValue(material.getNcmcompleto());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getCest() != null)
				cell.setCellValue(material.getCest());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getExtipi() != null)
				cell.setCellValue(material.getExtipi());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			if (material.getCodlistaservico() != null)
				cell.setCellValue(material.getCodlistaservico());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(material.getClassestring().contains("<BR>") ? material.getClassestring().substring(0, material.getClassestring().length() - 4).replace("<BR>", "\n") : material.getClassestring());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getPesobruto() != null)
				cell.setCellValue(material.getPesobruto());
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
				cell.setCellValue(material.getListaMaterialunidademedida().iterator().next().getFracaoQtdereferencia());
			}
			nLinha +=1;
			
			cell = row.createCell((short) nLinha);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			if(material.getValorunitario() != null)
				cell.setCellValue(material.getValorunitario());
			nLinha +=1;
		}
		
		return wb.getWorkBookResource("material_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadWithCodFlex
	 * @param cdmaterial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadWithCodFlex(Integer cdmaterial) {
		return materialDAO.loadWithCodFlex(cdmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadWithProducaoetapa(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Material loadWithProducaoetapa(Material material) {
		return materialDAO.loadWithProducaoetapa(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadWithNumeroserie(Integer cdmaterial)
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadWithNumeroserie(Integer cdmaterial) {
		return materialDAO.loadWithNumeroserie(cdmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForRegistrarnumeroserie(Integer cdmaterial)
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForRegistrarnumeroserie(Integer cdmaterial) {
		return materialDAO.findForRegistrarnumeroserie(cdmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO.loadMaterialPromocionalComMaterialrelacionado(Integer cdmaterial)
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadMaterialPromocionalComMaterialrelacionado(Integer cdmaterial) {
		if(cdmaterial == null) throw new SinedException("Par�metro inv�lido");
		
		List<Material> lista = materialDAO.findMaterialPromocionalComMaterialrelacionado(cdmaterial.toString());
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	public List<Material> findMaterialPromocionalComMaterialrelacionado(String whereIn) {
		return materialDAO.findMaterialPromocionalComMaterialrelacionado(whereIn);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findByForApontamento(Planejamento, Tarefa, Materialgrupo, Materialtipo)
	 * @param planejamento
	 * @param tarefa
	 * @param grupo
	 * @param tipo
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Material> findByForApontamento(Planejamento planejamento, Tarefa tarefa, Materialgrupo grupo, Materialtipo tipo){
		return materialDAO.findByForApontamento(planejamento, tarefa, grupo, tipo);
	}
	
	/**
	 * Busca uma lista de material com base em alguns par�metros do filtro.
	 * 
	 * @see #findByForApontamento(Planejamento, Tarefa, Materialgrupo, Materialtipo)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Material> findByForApontamento(ApontamentoFiltro filtro){
		
		Planejamento planejamento = filtro.getPlanejamento();
		Tarefa tarefa = filtro.getTarefa();
		Materialtipo tipo = filtro.getMaterialtipo();
		Materialgrupo grupo = filtro.getMaterialgrupo();
		
		return this.findByForApontamento(planejamento, tarefa, grupo, tipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material getPrecoCustoProduto(Material material) {
		return materialDAO.getPrecoCustoProduto(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#isLocacaoByMaterial
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isLocacaoByMaterial(Material material) {
		return materialDAO.isLocacaoByMaterial(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see MaterialDAO#findByCodigoMaterial(String)
	 * @param nome
	 * @author Thiago Clemente, Ramon Brazil
	 * 
	 */
	public Material loadByCodigoMaterial(Integer cdmaterial){
		return materialDAO.loadByCodigoMaterial(cdmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findMateriaisOrdemCompraPeriodo(Empresa empresa, Date dtinicio, Date dtfim){
		return materialDAO.findMateriaisOrdemCompraPeriodo(empresa, dtinicio, dtfim);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForPromocional
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForPromocional(String s) {
		return materialDAO.findForPromocional(s);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForKitFlexivel(String s, Boolean produto, Boolean servico, Boolean epi, Boolean patrimonio)
	*
	* @param s
	* @return
	* @since 18/03/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForKitFlexivel(String s) {
		Material material = new Material();
		try {
			WebRequestContext request = NeoWeb.getRequestContext();
			if("true".equalsIgnoreCase(request.getParameter("produto"))) material.setProduto(Boolean.TRUE);
			if("true".equalsIgnoreCase(request.getParameter("servico"))) material.setServico(Boolean.TRUE);
			if("true".equalsIgnoreCase(request.getParameter("patrimonio"))) material.setPatrimonio(Boolean.TRUE);
			if("true".equalsIgnoreCase(request.getParameter("epi"))) material.setEpi(Boolean.TRUE);
		} catch (Exception e) {}
		
		return materialDAO.findForKitFlexivel(s, material.getProduto(), material.getServico(), material.getEpi(), material.getPatrimonio());
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findServicoPromocional
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicoPromocional() {
		return materialDAO.findServicoPromocional();
	}
	
	public String getOptionServico() {
		StringBuilder str = new StringBuilder();
		
		List<Material> listaMaterial = this.findServicosProdutos();
		str.append("<option value=\'<null>\'></option>");
		for (Material material : listaMaterial) {
			str.append("<option value='br.com.linkcom.sined.geral.bean.Material[cdmaterial=" + material.getCdmaterial() + "]'>" + Util.strings.addScapesToDescription(material.getNome()) + "</option>");
		}
		
		return str.toString();
	}
	
	public String getOptionServicoJson() {
		List<Material> listaMaterial = this.findServicosProdutos();
		
		List<ServicoContratoJson> listaJson = new ArrayList<ServicoContratoJson>();
		
		for (Material material : listaMaterial) {
			ServicoContratoJson json = new ServicoContratoJson();
			
			json.setId("br.com.linkcom.sined.geral.bean.Material[cdmaterial=" + material.getCdmaterial() + "]");
			json.setNome(material.getNome());
			
			listaJson.add(json);
		}
		
		StringWriter stringWriter = new StringWriter();
		
		JSON jsonObj = View.convertToJson(listaJson);
		jsonObj.write(stringWriter);
		
		return stringWriter.toString();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findServicos
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicos() {
		return materialDAO.findServicos();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findServicosProdutos
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicosProdutos() {
		return materialDAO.findServicosProdutos();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findWithContagerencial
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material findWithContagerencial(Material material){
		return materialDAO.findWithContagerencial(material);
	}
	
	/**
	 * Retorna lista de materiais de produ��o para serem associados ao material 
	 * @param material
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Material> findMaterialProducao(Material material) {
		return materialDAO.findMaterialProducao(material);
	}

	/**
	 * Retorna lista de materiais de produ��o para serem associados ao material 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findMaterialProducaoAutoComplete(String whereIn) {
		return materialDAO.findMaterialProducao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findMaterialProducaoAutoCompleteAtivos(String whereIn)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialProducaoAutoCompleteAtivos(String q) {
		return materialDAO.findMaterialProducaoAutoCompleteAtivos(q);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForOrdemProducao
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadForOrdemProducao(Material material) {
		return materialDAO.loadForOrdemProducao(material);
	}
	
//	public Double calculaPrecoVenda(Material material, Double qtdevendida){
//		material = loadForEntrada(material);
//		
//		Set<Materialproducao> listaProducao = material.getListaProducao();
//		Double valorvenda = material.getValorvenda();
//		if (listaProducao != null && listaProducao.size() > 0){
//			Double custo = 0D;
//			for (Materialproducao materialproducao : listaProducao) {
//				Double consumo = 0.0;
//				if(!materialproducao.getMaterial().getServico()){
//					consumo = materialproducaoService.calculaConsumo(materialproducao, qtdevendida);
//				}else{
//					consumo = materialproducaoService.calculaConsumoServico(materialproducao, qtdevendida, material.getQtdereferencia());
//				}
////				Double preco = new Double(materialproducao.getPreco().getValue().doubleValue() / 100);
//				custo += consumo * materialproducao.getPreco();
//			}
//			Money icms = material.getIcms();
//			Money ipi = material.getIpi();
//			Money lucro = material.getLucro();
//			Money frete = material.getFrete();
//			Double adicional = new Double((icms.toLong()/100) + (ipi.toLong()/100) + (lucro.toLong()/100) + (frete.toLong()/100));
//			
//			valorvenda = custo + (custo * adicional / 100) / qtdevendida;
//		} 
//		
//		return valorvenda;
//	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param materialtipo
	 * @return
	 * @auhtor Tom�s Rabelo
	 */
	public List<Material> findByTipoServico(Materialtipo materialtipo){
		return materialDAO.findByTipoServico(materialtipo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os servi�os relacionados a determinada empresa e a todas as empresas de acordo com o par�metro
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#findByTipoServico(Materialtipo materialtipo, Empresa empresa)
	*
	* @param materialtipo
	* @param empresa
	* @return
	* @since Oct 24, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findByTipoServico(Materialtipo materialtipo, Empresa empresa){
		return materialDAO.findByTipoServico(materialtipo, empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca os servi�os relacionados a determinada empresa e a todas as empresas de acordo com o par�metro
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#findByEmpresaFlex(Empresa empresa)
	*
	* @param empresa
	* @return
	* @since Nov 1, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findByEmpresaFlex(Empresa empresa){
		return materialDAO.findByEmpresaFlex(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material carregaMaterial(Material material) {
		return materialDAO.carregaMaterial(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#carregaMaterialForGerarGrade(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 28/01/2014
	 */
	public Material carregaMaterialForGerarGrade(Material material) {
		return materialDAO.carregaMaterialForGerarGrade(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material getCentroCustoContaGerencialVenda(Material material){
		return materialDAO.getCentroCustoContaGerencialVenda(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForNFProduto
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadForNFProduto(Material material) {
		return materialDAO.loadForNFProduto(material);
	}
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @author Taidson
	 * @since 10/05/2010
	 */
	public List<Material> findByTipoServico(){
		return materialDAO.findByTipoServico();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialProducaoAtivoAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Material> findMaterialProducaoAtivoAutocomplete(String q) {
		return materialDAO.findMaterialProducaoAtivoAutocomplete(q);
	}
	
	public List<Material> findMaterialAutocompleteAtivoNotProducao(String q){
		return materialDAO.findMaterialAutocomplete(q, Boolean.TRUE, Boolean.FALSE);
	}
	
	public List<Material> findMaterialAutocomplete(String q) {
		return materialDAO.findMaterialAutocomplete(q, null, null);
	}
	
	public List<Material> findMaterialAtivoAutocompleteWithoutLote(String q) {
		return materialDAO.findMaterialAutocomplete(q, Boolean.TRUE, null, true);
	}
	
	public List<Material> findMaterialAtivoAutocomplete(String q) {
		return materialDAO.findMaterialAutocomplete(q, Boolean.TRUE, null);
	}
	
	public List<Material> findMaterialEstoqueAutocomplete(String q) {
		Localarmazenagem localarmazenagem = (Localarmazenagem) NeoWeb.getRequestContext().getSession().getAttribute("localarmazenagemForAutocompleteMaterial");
//		String paramEstoquenegativo = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO);
		if(!localarmazenagemService.getPermitirestoquenegativo(localarmazenagem)){
			return materialDAO.findMaterialEstoqueAutocomplete(q, 
					(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? localarmazenagem : null));
		} else {
			return materialDAO.findMaterialAutocomplete(q, true, null, false);
		}
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findProdutoEpiServicoAtivoAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/12/2012
	 */
	public List<Material> findProdutoEpiServicoAtivoAutocomplete(String q) {
		return materialDAO.findProdutoEpiServicoAtivoAutocomplete(q, null);
	}
	
	public List<Material> findProdutoEpiServicoAtivoTipobuscaAutocomplete(String q) {
		String tipobuscamaterial = null; 
		try {
			tipobuscamaterial = (String) NeoWeb.getRequestContext().getSession().getAttribute("MOVIMENTACAOESTOQUE_TIPOBUSCAMATERIAL");
		} catch (Exception e) {}
		return materialDAO.findProdutoEpiServicoAtivoAutocomplete(q, tipobuscamaterial);
	}
	
	/**
	 * 
	 * @param material
	 * @return
	 * @author Taidson
	 * @since 11/06/2010
	 */
	public Material unidadeMedidaMaterial(Material material){
		return materialDAO.unidadeMedidaMaterial(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Material> findMaterialForBuscaGeral(String busca) {
		return materialDAO.findMaterialForBuscaGeral(busca);
	}
	
	/**
     * Faz refer�ncia ao DAO.
 	 * @param q
	 * @return
	 * author Taidson
	 * @since 24/09/2010
	 */
	public List<Material> findProdutoServico(String q){
		return materialDAO.findProdutoServico(q);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param material
	 * @return
	 * @author Taidson
	 * @since 28/12/2010
	 */
	public Integer getIdUnidadeMedida(Material material) {
		return materialDAO.getIdUnidadeMedida(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForSpedReg0200
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForSpedReg0200(String whereIn, String whereInMaterialproducao) {
		return materialDAO.findForSpedReg0200(whereIn, whereInMaterialproducao);
	}
	
	/**
	 * Atualiza o Tipo de item.
	 *
	 * @param tipoitemsped
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateTipoitemsped(Tipoitemsped tipoitemsped, Material material) {
		materialDAO.updateTipoitemsped(tipoitemsped, material);
	}
	
	/**
	 * Atualiza o G�nero do NCM.
	 *
	 * @param ncmcapitulo
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateNcmcapitulo(Ncmcapitulo ncmcapitulo, Material material) {
		materialDAO.updateNcmcapitulo(ncmcapitulo, material);
	}
	
	/**
	 * Atualiza o C�digo de barras.
	 *
	 * @param codigobarras
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateCodigobarras(String codigobarras, Material material) {
		materialDAO.updateCodigobarras(codigobarras, material);
	}
	
	/**
	 * Atualiza o NCM completo.
	 *
	 * @param ncmcompleto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateNcmcompleto(String ncmcompleto, Material material) {
		materialDAO.updateNcmcompleto(ncmcompleto, material);
	}
	
	/**
	 * Atualiza o Ex da TIPI.
	 *
	 * @param extipi
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateExtipi(String extipi, Material material) {
		materialDAO.updateExtipi(extipi, material);
	}
	
	/**
	 * Atualiza o C�digo do item na lista de servi�o.
	 *
	 * @param codlistaservico
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateCodlistaservico(String codlistaservico, Material material) {
		materialDAO.updateCodlistaservico(codlistaservico, material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadWithContagerencialCentrocusto
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadWithContagerencialCentrocusto(Material material) {
		return materialDAO.loadWithContagerencialCentrocusto(material);
	}
	
	public Material loadWithContagerencial(Material material) {
		return materialDAO.loadWithContagerencial(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findListagem(String whereIn, String orderBy, boolean asc,Localarmazenagem local) {
		return materialDAO.findListagem(whereIn, orderBy, asc,local);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findEmExpedicaoAndEmCompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findEmExpedicaoAndEmCompra(String whereIn) {
		return materialDAO.findEmExpedicaoAndEmCompra(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.MaterialDAO#findWithReserva(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findWithReserva(String whereIn) {
		return materialDAO.findWithReserva(whereIn);
	}
	
	public void updateIdentificacao(Material material, String identificacao) {
		materialDAO.updateIdentificacao(material, identificacao);
	}
	
	public Material loadForOrdemcompra(Material material) {
		return materialDAO.loadForOrdemcompra(material);
	}
	
	
	/**
	* Faz refer�ncia ao DAO
	* 
	* Busca material para atualiza��o
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#findForAtualizarmaterial(String whereIn)

	*
	* @param whereIn
	* @return
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findForAtualizarmaterial(String whereIn){
		return materialDAO.findForAtualizarmaterial(whereIn);
	}
	
	/**
	* Faz refer�ncia ao DAO
	* 
	* Busca material para atualiza��o
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#findForAtualizarmaterialmestre(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findForAtualizarmaterialmestre(String whereIn){
		return materialDAO.findForAtualizarmaterialmestre(whereIn);
	}
	
	/**
	* Faz refer�ncia ao DAO
	*
	*@see	br.com.linkcom.sined.geral.dao.MaterialDAO#updateMaterialvalor(Material material)
	*
	* @param material
	* @param valorpercentual
	* @since Jun 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateMaterialvalor(Material material, Materialajustarpreco valorpercentual){
		
		Double valorvenda = null, valorvendaminimo = null, valorvendamaximo = null, acrescimo = new Double(0);		
		
//		if(material.getIcms() != null){ acrescimo += material.getIcms().getValue().doubleValue();}
//		if(material.getIpi() != null){	acrescimo += material.getIpi().getValue().doubleValue();}
//		if(material.getLucro() != null ){ acrescimo += material.getLucro().getValue().doubleValue();}
		if(material.getFrete() != null){ acrescimo += material.getFrete().getValue().doubleValue();}
		
		if(acrescimo > 0 && material.getValorcusto() != null){
			valorvenda = material.getValorcusto() + ( material.getValorcusto() * acrescimo / 100 );
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvenda = valorvenda + (valorvenda * valorpercentual.getPercentual() / 100);
			}else {
				valorvenda = valorvenda * ((100 - valorpercentual.getPercentual()) / 100);
			}
		}else if(material.getValorvenda() != null){
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvenda = material.getValorvenda() + (material.getValorvenda() * valorpercentual.getPercentual() / 100);
			}else {
				valorvenda = material.getValorvenda() * (1 - (valorpercentual.getPercentual() / 100));
				valorvenda = new BigDecimal(valorvenda).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		
		if(material.getValorvendaminimo() != null){
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvendaminimo = material.getValorvendaminimo() + ( material.getValorvendaminimo() * valorpercentual.getPercentual() / 100 );
			}else {
				valorvendaminimo = material.getValorvendaminimo() * (1 - (valorpercentual.getPercentual() / 100));
				valorvendaminimo = new BigDecimal(valorvendaminimo).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		if(material.getValorvendamaximo() != null){
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvendamaximo = material.getValorvendamaximo() + ( material.getValorvendamaximo() * valorpercentual.getPercentual() / 100 );
			}else {
				valorvendamaximo = material.getValorvendamaximo() * (1 - (valorpercentual.getPercentual() / 100));
				valorvendamaximo = new BigDecimal(valorvendamaximo).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		
		material.setValorvenda(SinedUtil.roundByParametro(valorvenda));
		material.setValorvendaminimo(SinedUtil.roundByParametro(valorvendaminimo));
		material.setValorvendamaximo(SinedUtil.roundByParametro(valorvendamaximo));
		
		materialDAO.updateMaterialvalor(material);
	}
	
	/**
	* Faz refer�ncia ao DAO
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#updateMaterialmestrevalor(Material material)
	*
	* @param material
	* @param valorpercentual
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateMaterialmestrevalor(Material material, Materialajustarpreco valorpercentual){	
		
		Double valorvenda = null, valorvendaminimo = null, valorvendamaximo = null, acrescimo = new Double(0);		
		
//		if(material.getIcms() != null){ acrescimo += material.getIcms().getValue().doubleValue();}
//		if(material.getIpi() != null){	acrescimo += material.getIpi().getValue().doubleValue();}
//		if(material.getLucro() != null ){ acrescimo += material.getLucro().getValue().doubleValue();}
		if(material.getFrete() != null){ acrescimo += material.getFrete().getValue().doubleValue();}
		
		if(acrescimo > 0 && material.getValorcusto() != null){
			valorvenda = material.getValorcusto() + ( material.getValorcusto() * acrescimo  / 100 );			
		}else if(material.getValorvenda() != null) {
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvenda = material.getValorvenda() + ( material.getValorvenda() * valorpercentual.getPercentual() / 100 );
			}else {
				valorvenda = material.getValorvenda() * (1 - (valorpercentual.getPercentual() / 100));
				valorvenda = new BigDecimal(valorvenda).setScale(2, RoundingMode.HALF_UP).doubleValue();			
			}
		}
		
		if(material.getValorvendaminimo() != null){
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvendaminimo = material.getValorvendaminimo() + ( material.getValorvendaminimo() * valorpercentual.getPercentual() / 100 );
			}else {
				valorvendaminimo = material.getValorvendaminimo() * (1 - (valorpercentual.getPercentual() / 100));
				valorvendaminimo = new BigDecimal(valorvendaminimo).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		
		if (material.getValorvendamaximo() != null){
			if (Boolean.TRUE.equals(valorpercentual.getCalculo())){
				valorvendamaximo = material.getValorvendamaximo() + ( material.getValorvendamaximo() * valorpercentual.getPercentual() / 100 );
			}else {
				valorvendamaximo = material.getValorvendamaximo() * (1 - (valorpercentual.getPercentual() / 100));
				valorvendamaximo = new BigDecimal(valorvendamaximo).setScale(2, RoundingMode.HALF_UP).doubleValue();
			}
		}
		
		material.setValorvenda(valorvenda);
		material.setValorvendaminimo(valorvendaminimo);
		material.setValorvendamaximo(valorvendamaximo);	
		
		materialDAO.updateMaterialmestrevalor(material);
		
	}
	
	public Material getUnidademedidaValorvenda(Material material){		
		return materialDAO.getUnidademedidaValorvenda(material);
	}
	
	public Material findWithMaterialgrupo(Material material){
		return materialDAO.findWithMaterialgrupo(material);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* busca os materiais com pre�o de venda para o autocomplete
	* 
	* @see	br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteValorvenda(String q)
	*
	* @param q
	* @return
	* @since Sep 26, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findMaterialAutocompleteValorvenda(String q) {
		return materialDAO.findMaterialAutocompleteValorvenda(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material carregaMaterialWMS(Material material) {
		return materialDAO.carregaMaterialWMS(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.MaterialDAO#carregaValorVenda(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material carregaValorVenda(Material material){
		return materialDAO.carregaValorVenda(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteVenda(String q)
	 *
	 * @param nome
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteVenda(String q){
		String cdempresa = "";
		Empresa empresa = null;
		if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
			cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
			empresa = new Empresa(Integer.parseInt(cdempresa));
		}
		return materialDAO.findMaterialAutocompleteVenda(q, empresa);
	}
	
	public List<MaterialWSBean> findMaterialAutocompleteWSVenda(String q, Empresa empresa){
		List<MaterialWSBean> lista = new ArrayList<MaterialWSBean>();
		for(Material material: materialDAO.findMaterialAutocompleteVenda(q, empresa)){
			MaterialWSBean bean = new MaterialWSBean(material);
			lista.add(bean);
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialBtEmpresaAutocomplete(String q)
	 *
	 * @param nome
	 * @return
	 * @author Francisco Lourandes
	 */
	
	public List<Material> findByEmpresaAutocomplete(String q){
		String cdempresa = "";
		Empresa empresa = null;
		try{
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
			empresa = new Empresa(Integer.parseInt(cdempresa));
		}catch (Exception e) {}
		return materialDAO.findMaterialByEmpresaAutocomplete(q, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteVendaForPesquisa(String q, Empresa empresa)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 * @since 30/01/2014
	 */
	public List<Material> findMaterialAutocompleteVendaForPesquisa(String q){
		String cdempresa = "";
		Empresa empresa = null;
		if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
			cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
			empresa = new Empresa(Integer.parseInt(cdempresa));
		}
		
		List<Material> listaMaterial = materialDAO.findMaterialAutocompleteVendaForPesquisa(q, empresa, Boolean.FALSE);
		/*if(parametrogeralService.getBoolean(Parametrogeral.EXIBIR_VALOR_TABELAPRECO_AUTOCOMPLETE_MATERIAL)){
			Pedidovendatipo pedidovendatipo = null;
			String pedidovendatipoString = NeoWeb.getRequestContext().getParameter("pedidovendatipo");
			if(pedidovendatipoString != null){
				pedidovendatipo = new Pedidovendatipo(Integer.parseInt(pedidovendatipoString));			
			}
			
			Cliente cliente = null;
			String clienteString = NeoWeb.getRequestContext().getParameter("cliente");
			if(clienteString != null){
				cliente = new Cliente(Integer.parseInt(clienteString));	
			}
			
			Materialtabelapreco materialtabelapreco = null;
			String materialtabelaprecoString = NeoWeb.getRequestContext().getParameter("materialtabelapreco");
			if(StringUtils.isNotBlank(materialtabelaprecoString)){
				materialtabelapreco = new Materialtabelapreco(Integer.parseInt(materialtabelaprecoString));
			}
			
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				for (Material material : listaMaterial) {
					vendaService.preencheValorVendaComTabelaPreco(material, materialtabelapreco, pedidovendatipo, cliente, empresa, null, null);
				}
			}
		}*/
		
		return listaMaterial;
	}
	
	public List<MaterialWSBean> findMaterialAutocompleteVendaForPesquisaWSVenda(String q, Empresa empresa){
		List<MaterialWSBean> lista = new ArrayList<MaterialWSBean>();
		for(Material material: materialDAO.findMaterialAutocompleteVendaForPesquisa(q, empresa, Boolean.FALSE)){
			MaterialWSBean bean = new MaterialWSBean(material);
			
			lista.add(bean);
		}
		return lista;
	}
	
	public List<Material> findMaterialAutocompleteVendaForPesquisaKitFlexivel(String q){
		String cdempresa = "";
		Empresa empresa = null;
		if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
			cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
			empresa = new Empresa(Integer.parseInt(cdempresa));
		}
		return materialDAO.findMaterialAutocompleteVendaForPesquisa(q, empresa, Boolean.TRUE);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteFluxocompra(String q, Empresa empresa)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteRequisicaomaterial(String q){
		return materialDAO.findMaterialAutocompleteFluxocompra(q, empresaService.getEmpresaSessaoFluxocompraForBuscamaterial("REQUISICAOMATERIAL"), false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteFluxocompra(String q, Empresa empresa)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteSolicitacaocompra(String q){
		return materialDAO.findMaterialAutocompleteFluxocompra(q, empresaService.getEmpresaSessaoFluxocompraForBuscamaterial("SOLICITACAOCOMPRA"), false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteFluxocompra(String q, Empresa empresa)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteOrdemcompra(String q){
		return materialDAO.findMaterialAutocompleteFluxocompra(q, empresaService.getEmpresaSessaoFluxocompraForBuscamaterial("ORDEMCOMPRA"), false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteFluxocompra(String q, Empresa empresa)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteEntrega(String q){
		return materialDAO.findMaterialAutocompleteFluxocompra(q, empresaService.getEmpresaSessaoFluxocompraForBuscamaterial("ENTREGA"), true);
	}
	
	public List<Material> findMaterialAutocompleteContratoFornecimento(String q){
		return materialDAO.findMaterialAutocompleteFluxocompra(q, empresaService.getEmpresaSessaoFluxocompraForBuscamaterial("ENTREGA"), true, true);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteTabelapreco(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteTabelapreco(String q){
		return materialDAO.findMaterialAutocompleteTabelapreco(q);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @return
	 */
	public List<Material> findListMaterialOportunidade(){
		return materialDAO.findListMaterialOportunidade();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#isMaterialTarefa(Tarefa tarefa, Material material)
	 * 
	 * @param tarefa
	 * @param material
	 * @return
	 */
	public Boolean isMaterialTarefa(Tarefa tarefa, Material material) {
		return materialDAO.isMaterialTarefa(tarefa, material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO.findWithCod(String codigo)
	 * 
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findWithCod(String codigo) {
		return materialDAO.findWithCod(codigo);
	}
	
	public List<Material> findCdMaterialAndIdentificacao(String codigo) {
		return materialDAO.findCdMaterialAndIdentificacao(codigo);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 28/03/2012
	 * @param material
	 * @return
	 */
	public Material carregaMaterialServico(Material material) {
		return materialDAO.carregaMaterialServico(material);
	}
	
	public Material loadTributacao(	Material material, Empresa empresa, Cliente cliente, Materialgrupo materialgrupo, 
			Fornecedor fornecedor, Endereco enderecoTributacaoOrigem, Endereco enderecoTributacaoDestino,
			Grupotributacao grupotributacao){
		if (material != null){
			material = this.loadForNFProduto(material);
			
			Endereco enderecoOrigem = null;
			Uf ufOrigem = null;
			if (enderecoTributacaoOrigem != null && enderecoTributacaoOrigem.getCdendereco() != null){
				enderecoOrigem = enderecoService.loadEndereco(enderecoTributacaoOrigem);
				material.setEnderecoTributacaoOrigem(enderecoOrigem);
				if (enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getUf() != null)
					ufOrigem = enderecoOrigem.getMunicipio().getUf();
			}
			
			Endereco enderecoDestino = null;
			Uf ufDestino = null;
			if (enderecoTributacaoDestino != null){
				if(enderecoTributacaoDestino.getCdendereco() != null){
					enderecoDestino = enderecoService.loadEnderecoNota(enderecoTributacaoDestino);
					material.setEnderecoTributacaoDestino(enderecoDestino);
					if (enderecoDestino != null && enderecoDestino.getMunicipio() != null && enderecoDestino.getMunicipio().getUf() != null)
						ufDestino = enderecoDestino.getMunicipio().getUf();
				}else {
					material.setEnderecoTributacaoDestino(enderecoTributacaoDestino);
					if (enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null)
						ufDestino = enderecoTributacaoDestino.getMunicipio().getUf();
				}
			}
			
			Grupotributacao grupotributacaoMatrial = material.getGrupotributacao();
			if(empresa == null || (material.getListaMaterialempresa() == null || material.getListaMaterialempresa().isEmpty())){
				preencherDadosTributacao(material, empresa, cliente, materialgrupo, enderecoOrigem, enderecoDestino, ufOrigem, ufDestino, grupotributacao, grupotributacao != null ? null : grupotributacaoMatrial);
			}else {
				preencherDadosTributacao(material, empresa, cliente, materialgrupo, enderecoOrigem, enderecoDestino, ufOrigem, ufDestino, material.getGrupotributacaoForNota(empresa, grupotributacao),  grupotributacao != null ? null : grupotributacaoMatrial);
			}
			
		}
	
		return material;
	}
	
	public Material loadTributacao(	Material material, Empresa empresa, Materialgrupo materialgrupo, 
									Cliente cliente, Endereco enderecoTributacaoOrigem, Endereco enderecoTributacaoDestino,
									Grupotributacao grupotributacao){
		if (material != null){
			material = this.loadForNFProduto(material);
			
			Endereco enderecoOrigem = null;
			Uf ufOrigem = null;
			if (enderecoTributacaoOrigem != null && enderecoTributacaoOrigem.getCdendereco() != null){
				enderecoOrigem = enderecoService.loadEndereco(enderecoTributacaoOrigem);
				material.setEnderecoTributacaoOrigem(enderecoOrigem);
				if (enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getUf() != null)
					ufOrigem = enderecoOrigem.getMunicipio().getUf();
			}
			
			Endereco enderecoDestino = null;
			Uf ufDestino = null;
			if (enderecoTributacaoDestino != null){
				if(Boolean.TRUE.equals(enderecoTributacaoDestino.getEnderecoTransient())){
					material.setEnderecoTributacaoDestino(enderecoTributacaoDestino);
					if (enderecoTributacaoDestino != null && enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null)
						ufDestino = enderecoTributacaoDestino.getMunicipio().getUf();
				}else if(enderecoTributacaoDestino.getCdendereco() != null){
					enderecoDestino = enderecoService.loadEnderecoNota(enderecoTributacaoDestino);
					material.setEnderecoTributacaoDestino(enderecoDestino);
					if (enderecoDestino != null && enderecoDestino.getMunicipio() != null && enderecoDestino.getMunicipio().getUf() != null)
						ufDestino = enderecoDestino.getMunicipio().getUf();
				}else {
					material.setEnderecoTributacaoDestino(enderecoTributacaoDestino);
					if (enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null)
						ufDestino = enderecoTributacaoDestino.getMunicipio().getUf();
				}
			}
			
			Grupotributacao grupotributacaoMatrial = material.getGrupotributacao();
			if(empresa == null || (material.getListaMaterialempresa() == null || material.getListaMaterialempresa().isEmpty())){
				preencherDadosTributacao(material, empresa, cliente, materialgrupo, enderecoOrigem, enderecoDestino, ufOrigem, ufDestino, grupotributacao, grupotributacao != null ? null : grupotributacaoMatrial);
			}else {
				preencherDadosTributacao(material, empresa, cliente, materialgrupo, enderecoOrigem, enderecoDestino, ufOrigem, ufDestino, material.getGrupotributacaoForNota(empresa, grupotributacao), grupotributacao != null ? null : grupotributacaoMatrial);
			}
			
		}
	
		return material;
	}
	
	/**
	 * 
	 * M�todo que carrega as al�quotas de tributa��o do produto de acordo com o grupo de tributa��o e material grupo.
	 *
	 *@param material
	 *@return material
	 *@author Marden Silva
	 * @param grupotributacaoMatrial
	 *
	 */
//	public Material loadTributacao(Material material, Empresa empresa, Endereco enderecoTributacaoOrigem, Endereco enderecoTributacaoDestino){
//		
//		if (material != null){
//			
//			material = this.loadForNFProduto(material);
//			
//			Endereco enderecoOrigem = null;
//			Uf ufOrigem = null;
//			if (enderecoTributacaoOrigem != null && enderecoTributacaoOrigem.getCdendereco() != null){
//				enderecoOrigem = enderecoService.loadEndereco(enderecoTributacaoOrigem);
//				material.setEnderecoTributacaoOrigem(enderecoOrigem);
//				if (enderecoOrigem.getMunicipio() != null && enderecoOrigem.getMunicipio().getUf() != null)
//					ufOrigem = enderecoOrigem.getMunicipio().getUf();
//			}
//			
//			Endereco enderecoDestino = null;
//			Uf ufDestino = null;
//			if (enderecoTributacaoDestino != null){
//				if(enderecoTributacaoDestino.getCdendereco() != null){
//					enderecoDestino = enderecoService.loadEnderecoNota(enderecoTributacaoDestino);
//					material.setEnderecoTributacaoDestino(enderecoDestino);
//					if (enderecoDestino != null && enderecoDestino.getMunicipio() != null && enderecoDestino.getMunicipio().getUf() != null)
//						ufDestino = enderecoDestino.getMunicipio().getUf();
//				}else {
//					material.setEnderecoTributacaoDestino(enderecoTributacaoDestino);
//					if (enderecoTributacaoDestino.getMunicipio() != null && enderecoTributacaoDestino.getMunicipio().getUf() != null)
//						ufDestino = enderecoTributacaoDestino.getMunicipio().getUf();
//				}
//			}
//			
//			
//			if(empresa == null || (material.getListaMaterialempresa() == null || material.getListaMaterialempresa().isEmpty())){
//				preencherDadosTributacao(material, empresa, material.getGrupotributacao(), enderecoOrigem, enderecoDestino, ufOrigem, ufDestino);
//			}else {
//				boolean achou = false;
//				for(Materialempresa materialempresa : material.getListaMaterialempresa()){
//					if(materialempresa.getEmpresa() != null && materialempresa.getEmpresa().equals(empresa) && materialempresa.getGrupotributacao() != null){
//						preencherDadosTributacao(material, materialempresa.getEmpresa(), materialempresa.getGrupotributacao(), enderecoOrigem, enderecoDestino, ufOrigem, ufDestino);
//						achou = true;
//						break;
//					}
//				}
//				if(!achou){
//					preencherDadosTributacao(material, empresa, material.getGrupotributacao(), enderecoOrigem, enderecoDestino, ufOrigem, ufDestino);
//				}
//			}
//			
//		}
//	
//		return material;
//	}
	
	private void preencherDadosTributacao(Material material, Empresa empresa, Cliente cliente, Materialgrupo materialgrupo,
							Endereco enderecoOrigem, Endereco enderecoDestino, Uf ufOrigem, Uf ufDestino, Grupotributacao grupotributacao, Grupotributacao grupotributacaoMatrial){
		if (grupotributacao != null || grupotributacaoMatrial != null){			
			
			Money icms = null;
			Double percentualBcIcms = null;
			Money icmsst = null;
			Money ipi = null;
			Double percentualBcIpi = null;
			Money pis = null;
			Double percentualBcPis = null;
			Money cofins = null;
			Double percentualBcCofins = null;
			Money iss = null;
			Double percentualBcIss = null;
			Double mva = null;
			Money csll = null;
			Double percentualBcCsll = null;
			Money ir = null;
			Double percentualBcIr = null;
			Money inss = null;
			Double percentualBcInss = null;
			Money fcp = null;
			Double percentualBcFcp = null;
			Money fcpSt = null;
			Double percentualBcFcpSt = null;
			
			if(materialgrupo.getValormvaespecifico() != null /*&& materialgrupo.getValormvaespecifico().getValue().doubleValue() > 0*/){
				mva = materialgrupo.getValormvaespecifico();
			}
			
			//Carrega a tributa��o de ICMS por UF de origem e destino
			Grupotributacaoimposto impostoIcms = null;
			boolean buscaraliquotafixafaixaimpostoIcms = false;
			boolean impostoGrupotributacaoIcms = false;
			boolean existImpostoGrupoIcms = false;
			if(grupotributacaoMatrial != null){
				impostoIcms = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacaoMatrial, Faixaimpostocontrole.ICMS);
				if (impostoIcms != null){
					existImpostoGrupoIcms = true;
					buscaraliquotafixafaixaimpostoIcms = impostoIcms.getBuscaraliquotafixafaixaimposto() != null ? impostoIcms.getBuscaraliquotafixafaixaimposto() : false;
					if (impostoIcms.getAliquotafixa() != null/* && impostoIcms.getAliquotafixa().getValue().doubleValue() > 0*/){
						icms = impostoIcms.getAliquotafixa();
						impostoGrupotributacaoIcms = true;
					}
					if (impostoIcms.getBasecalculopercentual() != null/* && impostoIcms.getBasecalculopercentual() > 0*/){
						percentualBcIcms = impostoIcms.getBasecalculopercentual();
					}
				}
			}
			if(grupotributacao != null && !impostoGrupotributacaoIcms){
				impostoIcms = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, Faixaimpostocontrole.ICMS);
				if (impostoIcms != null){
					existImpostoGrupoIcms = true;
					buscaraliquotafixafaixaimpostoIcms = impostoIcms.getBuscaraliquotafixafaixaimposto() != null ? impostoIcms.getBuscaraliquotafixafaixaimposto() : false;
					if (impostoIcms.getAliquotafixa() != null/* && impostoIcms.getAliquotafixa().getValue().doubleValue() > 0*/){
						icms = impostoIcms.getAliquotafixa();
						impostoGrupotributacaoIcms = true;
					}
					if (impostoIcms.getBasecalculopercentual() != null/* && impostoIcms.getBasecalculopercentual() > 0*/){
						percentualBcIcms = impostoIcms.getBasecalculopercentual();
					}
				}
			}
			
			/*Grupotributacaoimposto impostoFCP = null;
			boolean impostoGrupotributacaoFCP = false;
			if(grupotributacaoMatrial != null){
				impostoFCP = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacaoMatrial, Faixaimpostocontrole.FCP);
				if (impostoFCP != null){
					if (impostoFCP.getAliquotafixa() != null){
						fcp = impostoFCP.getAliquotafixa();
						impostoGrupotributacaoFCP = true;
					}
				}
			}
			if(grupotributacao != null && !impostoGrupotributacaoFCP){
				impostoFCP = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, Faixaimpostocontrole.FCP);
				if (impostoFCP != null){
					if (impostoFCP.getAliquotafixa() != null){
						fcp = impostoFCP.getAliquotafixa();
						impostoGrupotributacaoFCP = true;
					}
				}
			}*/
			if(existImpostoGrupoIcms && (!impostoGrupotributacaoIcms || buscaraliquotafixafaixaimpostoIcms)){
				Faixaimposto faixaimposto = faixaimpostoService.findByControleAndUf(empresa, cliente, Faixaimpostocontrole.ICMS, ufOrigem, ufDestino);
				if (faixaimposto != null){
					if(faixaimposto.getBasecalculopercentual() != null){
						percentualBcIcms = faixaimposto.getBasecalculopercentual();
					}
					icms = faixaimposto.getValoraliquota() != null /*&& faixaimposto.getValoraliquota().getValue().doubleValue() > 0*/ ? faixaimposto.getValoraliquota() : null;
					icmsst = faixaimposto.getIcmsst() != null /*&& faixaimposto.getIcmsst().getValue().doubleValue() > 0*/ ? faixaimposto.getIcmsst() : null;
					if(grupotributacao == null)
						grupotributacao = material.getGrupotributacao();
					if(mva == null){
						if (grupotributacao != null && grupotributacao.getMargemvaloradicionalicmsst() != null /*&& grupotributacao.getMargemvaloradicionalicmsst().getValue().doubleValue() > 0*/)
							mva = grupotributacao.getMargemvaloradicionalicmsst();
						else if (faixaimposto.getMva() != null /*&& faixaimposto.getMva().getValue().doubleValue() > 0*/)
							mva = faixaimposto.getMva();
					}
				}
			}
			
			if(grupotributacaoMatrial != null && grupotributacaoMatrial.getAliquotaicmsst() != null && 
					(grupotributacaoMatrial.getBuscarfaixaimpostoaliquotaicmsst() == null || 
					!grupotributacaoMatrial.getBuscarfaixaimpostoaliquotaicmsst())){
				icmsst = new Money(grupotributacaoMatrial.getAliquotaicmsst());
			}else if(grupotributacao != null && grupotributacao.getAliquotaicmsst() != null && 
					(grupotributacao.getBuscarfaixaimpostoaliquotaicmsst() == null || 
					!grupotributacao.getBuscarfaixaimpostoaliquotaicmsst())){
				icmsst = new Money(grupotributacao.getAliquotaicmsst());
			}
			if(grupotributacaoMatrial != null && grupotributacaoMatrial.getMargemvaloradicionalicmsst() != null && 
					(grupotributacaoMatrial.getBuscarfaixaimpostomargemvaloradicionalicmsst() == null || 
					!grupotributacaoMatrial.getBuscarfaixaimpostomargemvaloradicionalicmsst())){
				mva = grupotributacaoMatrial.getMargemvaloradicionalicmsst();
			}else if(grupotributacao != null && grupotributacao.getMargemvaloradicionalicmsst() != null && 
					(grupotributacao.getBuscarfaixaimpostomargemvaloradicionalicmsst() == null || 
					!grupotributacao.getBuscarfaixaimpostomargemvaloradicionalicmsst())){
				mva = grupotributacao.getMargemvaloradicionalicmsst();
			}
			
			ImpostoBean impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.FCP);
			if(impostoBean != null){
				fcp = impostoBean.getAliquota();
				percentualBcFcp = impostoBean.getPercentualBC();
			}
			impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.ISS);
			if(impostoBean != null){
				iss = impostoBean.getAliquota();
				percentualBcIss = impostoBean.getPercentualBC();
			}
			impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.CSLL);
			if(impostoBean != null){
				csll = impostoBean.getAliquota();
				percentualBcCsll = impostoBean.getPercentualBC();
			}
			impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.IR);
			if(impostoBean != null){
				ir = impostoBean.getAliquota();
				percentualBcIr = impostoBean.getPercentualBC();
			}
			impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.INSS);
			if(impostoBean != null){
				inss = impostoBean.getAliquota();
				percentualBcInss = impostoBean.getPercentualBC();
			}
			impostoBean = buscaImposto(grupotributacaoMatrial, grupotributacao, empresa, cliente, enderecoDestino, Faixaimpostocontrole.FCPST);
			if(impostoBean != null){
				fcpSt = impostoBean.getAliquota();
				percentualBcFcpSt = impostoBean.getPercentualBC();
			}
			
			//Carrega os demais impostos: IPI, PIS e COFINS sem considerar estados (A query que n�o faz essa considera��o)
			List<Grupotributacaoimposto> listaImposto = null;
			boolean buscaraliquotafixafaixaimpostoIpi = false;
			boolean buscaraliquotafixafaixaimpostoPis = false;
			boolean buscaraliquotafixafaixaimpostoCofins = false;
			boolean impostoGrupotributacaoIpi = false;
			boolean impostoGrupotributacaoPis = false;
			boolean impostoGrupotributacaoCofins = false;
			boolean existImpostoGrupoIpi = false;
			boolean existImpostoGrupoPis = false;
			boolean existImpostoGrupoCofins = false;
			
			Retencao retencaoIpi = null;
			if(grupotributacaoMatrial != null){
				listaImposto = grupotributacaoimpostoService.findByGrupotributacao(grupotributacaoMatrial);
				if (listaImposto != null && !listaImposto.isEmpty()){
					for (Grupotributacaoimposto gti : listaImposto){
						Money aliquota = null;
						if (gti.getAliquotafixa() != null){
							aliquota = gti.getAliquotafixa();
						}					
						if (Faixaimpostocontrole.IPI.equals(gti.getControle())){
							existImpostoGrupoIpi = true;
							buscaraliquotafixafaixaimpostoIpi = gti.getBuscaraliquotafixafaixaimposto() != null ? gti.getBuscaraliquotafixafaixaimposto() : false;
							if (aliquota != null && aliquota.getValue().doubleValue() > 0){
								ipi = aliquota;
								impostoGrupotributacaoIpi = true;
							}
							if (gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
								percentualBcIpi = gti.getBasecalculopercentual();
								retencaoIpi = gti.getRetencao();
							}
						}else if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
							existImpostoGrupoPis = true;
							buscaraliquotafixafaixaimpostoPis = gti.getBuscaraliquotafixafaixaimposto() != null ? gti.getBuscaraliquotafixafaixaimposto() : false;
							if (aliquota != null && aliquota.getValue().doubleValue() > 0){
								pis = aliquota;
								impostoGrupotributacaoPis = true;
							}
							if (gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
								percentualBcPis = gti.getBasecalculopercentual();
							}
						}else if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
							existImpostoGrupoCofins = true;
							buscaraliquotafixafaixaimpostoCofins = gti.getBuscaraliquotafixafaixaimposto() != null ? gti.getBuscaraliquotafixafaixaimposto() : false;
							if (aliquota != null && aliquota.getValue().doubleValue() > 0){
								cofins = aliquota;
								impostoGrupotributacaoCofins = true;
							}
							if (gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
								percentualBcCofins = gti.getBasecalculopercentual();
							}
						}						
					}
				}
			}
			
			if((!impostoGrupotributacaoIpi || buscaraliquotafixafaixaimpostoIpi) || 
					(!impostoGrupotributacaoPis || buscaraliquotafixafaixaimpostoPis) || 
					(!impostoGrupotributacaoCofins || buscaraliquotafixafaixaimpostoCofins)){
				listaImposto = grupotributacaoimpostoService.findByGrupotributacao(grupotributacao);
				if (listaImposto != null && !listaImposto.isEmpty()){
					for (Grupotributacaoimposto gti : listaImposto){
						Money aliquota = null;
						Double percentualBc = null;
						if (gti.getAliquotafixa() != null && (gti.getBuscaraliquotafixafaixaimposto() == null || !gti.getBuscaraliquotafixafaixaimposto()))
							aliquota = gti.getAliquotafixa();
						else {
							Faixaimposto faixaimposto = faixaimpostoService.findByControleAndUf(empresa, cliente, gti.getControle(), ufDestino, null);
							if (faixaimposto != null && faixaimposto.getValoraliquota() != null) {					
								aliquota = faixaimposto.getValoraliquota();
							}
							if(faixaimposto != null && faixaimposto.getBasecalculopercentual() != null){
								percentualBc = faixaimposto.getBasecalculopercentual();
							}
						}					
						
						if (Faixaimpostocontrole.IPI.equals(gti.getControle())){
							existImpostoGrupoIpi = true;
							if (aliquota != null /*&& aliquota.getValue().doubleValue() > 0*/){
								if((!impostoGrupotributacaoIpi || buscaraliquotafixafaixaimpostoIpi)){
									ipi = aliquota;
									if(percentualBc != null){
										percentualBcIpi = percentualBc;
									}
									impostoGrupotributacaoIpi = true;
									if (percentualBcIpi == null && gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
										percentualBcIpi = gti.getBasecalculopercentual();
										retencaoIpi = gti.getRetencao();
									}
								}
							}
						}else if (Faixaimpostocontrole.PIS.equals(gti.getControle())){
							existImpostoGrupoPis = true;
							if (aliquota != null /*&& aliquota.getValue().doubleValue() > 0*/){
								if((!impostoGrupotributacaoPis || buscaraliquotafixafaixaimpostoPis)){
									pis = aliquota;
									if(percentualBc != null){
										percentualBcPis = percentualBc;
									}
									impostoGrupotributacaoPis = true;
									if (percentualBcPis == null && gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
										percentualBcPis = gti.getBasecalculopercentual();
									}
								}
							}
						}else if (Faixaimpostocontrole.COFINS.equals(gti.getControle())){
							existImpostoGrupoCofins = true;
							if (aliquota != null /*&& aliquota.getValue().doubleValue() > 0*/){
								if((!impostoGrupotributacaoCofins || buscaraliquotafixafaixaimpostoCofins)){
									cofins = aliquota;
									if(percentualBc != null){
										percentualBcCofins = percentualBc;
									}
									impostoGrupotributacaoCofins = true;
									if (percentualBcCofins == null && gti.getBasecalculopercentual() != null /*&& gti.getBasecalculopercentual() > 0*/){
										percentualBcCofins = gti.getBasecalculopercentual();
									}
								}
							}
						}
					}
				}	
			}
			
			if(existImpostoGrupoIpi && !impostoGrupotributacaoIpi){
				Faixaimposto faixaimposto = faixaimpostoService.findByControleMunicipio(empresa, cliente, Faixaimpostocontrole.IPI, enderecoDestino != null ? enderecoDestino.getMunicipio() : null, null);
				if (faixaimposto != null){
					ipi = faixaimposto.getValoraliquota() != null ? faixaimposto.getValoraliquota() : null;
					if(faixaimposto.getBasecalculopercentual() != null){
						percentualBcIpi = faixaimposto.getBasecalculopercentual();
					}
				}
			}
			if(existImpostoGrupoPis && !impostoGrupotributacaoPis){
				Faixaimposto faixaimposto = faixaimpostoService.findByControleMunicipio(empresa, cliente, Faixaimpostocontrole.PIS, enderecoDestino != null ? enderecoDestino.getMunicipio() : null, null);
				if (faixaimposto != null){
					pis = faixaimposto.getValoraliquota() != null ? faixaimposto.getValoraliquota() : null;
					if(faixaimposto.getBasecalculopercentual() != null){
						percentualBcPis = faixaimposto.getBasecalculopercentual();
					}
				}
			}
			if(existImpostoGrupoCofins && !impostoGrupotributacaoCofins){
				Faixaimposto faixaimposto = faixaimpostoService.findByControleMunicipio(empresa, cliente, Faixaimpostocontrole.COFINS, enderecoDestino != null ? enderecoDestino.getMunicipio() : null, null);
				if (faixaimposto != null){
					cofins = faixaimposto.getValoraliquota() != null ? faixaimposto.getValoraliquota() : null;
					if(faixaimposto.getBasecalculopercentual() != null){
						percentualBcCofins = faixaimposto.getBasecalculopercentual();
					}
				}
			}
			
			material.setAliquotaicms(icms);
			material.setAliquotaicmsst(icmsst);
			material.setAliquotaipi(ipi);
			material.setAliquotapis(pis);
			material.setAliquotacofins(cofins);
			material.setAliquotaiss(iss);			
			material.setAliquotamva(mva);
			material.setAliquotacsll(csll);
			material.setAliquotair(ir);
			material.setAliquotainss(inss);
			material.setAliquotaFCP(fcp);
			material.setAliquotaFCPST(fcpSt);
			
			material.setPercentualBcicms(percentualBcIcms);
			material.setPercentualBcipi(percentualBcIpi);
			material.setPercentualBcpis(percentualBcPis);
			material.setPercentualBccofins(percentualBcCofins);
			material.setPercentualBciss(percentualBcIss);
			material.setPercentualBccsll(percentualBcCsll);
			material.setPercentualBcir(percentualBcIr);
			material.setPercentualBcinss(percentualBcInss);
			material.setPercentualBcfcp(percentualBcFcp);
			material.setPercentualBcfcpst(percentualBcFcpSt);
			
			material.setRetencaoIpi(retencaoIpi);
			
			material.setGrupotributacaoNota(grupotributacao);
		}
	}
	
	public ImpostoBean buscaImposto(Grupotributacao grupotributacaoMatrial, Grupotributacao grupotributacao, Empresa empresa, Cliente cliente, Endereco enderecoDestino, Faixaimpostocontrole faixaimpostocontrole){
		Grupotributacaoimposto impostoIss = null;
		boolean existImpostoGrupoIss = false;
		boolean buscaraliquotafixafaixaimpostoIss = false;
		boolean impostoGrupotributacaoIss = false;
		
		Double percentualBcIss = null;
		Money iss = null;
		
		if(grupotributacaoMatrial != null){
			impostoIss = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacaoMatrial, faixaimpostocontrole);
			if (impostoIss != null){
				existImpostoGrupoIss = true;
				buscaraliquotafixafaixaimpostoIss = impostoIss.getBuscaraliquotafixafaixaimposto() != null ? impostoIss.getBuscaraliquotafixafaixaimposto() : false;
				if (impostoIss.getAliquotafixa() != null /*&& impostoIss.getAliquotafixa().getValue().doubleValue() > 0*/){
					iss = impostoIss.getAliquotafixa();
					impostoGrupotributacaoIss = true;
				} 
				if (impostoIss.getBasecalculopercentual() != null /*&& impostoIss.getBasecalculopercentual() > 0*/){
					percentualBcIss = impostoIss.getBasecalculopercentual();
				}
			}
		}
		if(grupotributacao != null && !impostoGrupotributacaoIss){
			impostoIss = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, faixaimpostocontrole);
			if (impostoIss != null){
				existImpostoGrupoIss = true;
				buscaraliquotafixafaixaimpostoIss = impostoIss.getBuscaraliquotafixafaixaimposto() != null ? impostoIss.getBuscaraliquotafixafaixaimposto() : false;
				if (impostoIss.getAliquotafixa() != null /*&& impostoIss.getAliquotafixa().getValue().doubleValue() > 0*/){
					iss = impostoIss.getAliquotafixa();
					impostoGrupotributacaoIss = true;
				}
				if (impostoIss.getBasecalculopercentual() != null /*&& impostoIss.getBasecalculopercentual() > 0*/){
					percentualBcIss = impostoIss.getBasecalculopercentual();
				}
			}	
		}
		
		if(existImpostoGrupoIss && (!impostoGrupotributacaoIss || buscaraliquotafixafaixaimpostoIss)){
			Faixaimposto faixaimposto = faixaimpostoService.findByControleMunicipio(empresa, cliente, faixaimpostocontrole, enderecoDestino != null ? enderecoDestino.getMunicipio() : null, null);
			if (faixaimposto != null){
				if(faixaimposto.getBasecalculopercentual() != null){
					percentualBcIss = faixaimposto.getBasecalculopercentual();
				}
				iss = faixaimposto.getValoraliquota() != null ? faixaimposto.getValoraliquota() : null;
			}
		}
		
		return new ImpostoBean(percentualBcIss, iss);
	}
	
//	private void preencherDadosTributacao(Material material, Empresa empresa, Grupotributacao grupotributacao, Endereco enderecoOrigem, Endereco enderecoDestino, Uf ufOrigem, Uf ufDestino ){
//		if (grupotributacao != null){			
//			
//			Money icms = null;
//			Money icmsst = null;
//			Money ipi = null;
//			Money pis = null;
//			Money cofins = null;
//			Money iss = null;
//			Money mva = null;
//			
//			//Carrega a tributa��o de ICMS por UF de origem e destino 
//			Grupotributacaoimposto impostoIcms = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, Faixaimpostocontrole.ICMS);
//			if (impostoIcms != null){
//				if (impostoIcms.getAliquotafixa() != null && impostoIcms.getAliquotafixa().getValue().doubleValue() > 0){
//					icms = impostoIcms.getAliquotafixa();
//				} else {												
//					Faixaimposto faixaimposto = faixaimpostoService.findByControleAndUf(empresa, Faixaimpostocontrole.ICMS, ufOrigem, ufDestino);
//					if (faixaimposto != null){
//						icms = faixaimposto.getValoraliquota() != null && faixaimposto.getValoraliquota().getValue().doubleValue() > 0 ? faixaimposto.getValoraliquota() : null;
//						icmsst = faixaimposto.getIcmsst() != null && faixaimposto.getIcmsst().getValue().doubleValue() > 0 ? faixaimposto.getIcmsst() : null;
//						if(grupotributacao == null)
//							grupotributacao = material.getGrupotributacao();
//						if (grupotributacao != null && grupotributacao.getMargemvaloradicionalicmsst() != null && grupotributacao.getMargemvaloradicionalicmsst().getValue().doubleValue() > 0)
//							mva = grupotributacao.getMargemvaloradicionalicmsst();
//						else if (faixaimposto.getMva() != null && faixaimposto.getMva().getValue().doubleValue() > 0)
//							mva = faixaimposto.getMva();
//					}						
//				}
//			}			
//			
//			//Carrega a tributa��o de ISS por munic�pio de destino
//			Grupotributacaoimposto impostoIss = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, Faixaimpostocontrole.ISS);
//			if (impostoIss != null){
//				if (impostoIss.getAliquotafixa() != null && impostoIss.getAliquotafixa().getValue().doubleValue() > 0){
//					icms = impostoIss.getAliquotafixa();
//				} else {												
//					Faixaimposto faixaimposto = faixaimpostoService.findByControleMunicipio(empresa, Faixaimpostocontrole.ISS, enderecoDestino != null ? enderecoDestino.getMunicipio() : null, null);
//					if (faixaimposto != null){
//						iss = faixaimposto.getValoraliquota() != null ? faixaimposto.getValoraliquota() : null;
//					}						
//				}
//			}				
//			
//			//Carrega os demais impostos: IPI, PIS e COFINS sem considerar estados (A query que n�o faz essa considera��o)
//			List<Grupotributacaoimposto> listaImposto = grupotributacaoimpostoService.findByGrupotributacao(grupotributacao);
//			if (listaImposto != null && !listaImposto.isEmpty()){
//				for (Grupotributacaoimposto gti : listaImposto){
//					Money aliquota = null;
//					if (gti.getAliquotafixa() != null && gti.getAliquotafixa().getValue().doubleValue() > 0)
//						aliquota = gti.getAliquotafixa();
//					else {
//						Faixaimposto faixaimposto = faixaimpostoService.findByControleAndUf(empresa, gti.getControle(), ufDestino, null);
//						if (faixaimposto != null && faixaimposto.getValoraliquota() != null) {					
//							aliquota = faixaimposto.getValoraliquota();
//						}
//					}					
//					if (aliquota != null){
//						if (Faixaimpostocontrole.IPI.equals(gti.getControle()))
//							ipi = aliquota;
//						if (Faixaimpostocontrole.PIS.equals(gti.getControle()))
//							pis = aliquota;
//						if (Faixaimpostocontrole.COFINS.equals(gti.getControle()))
//							cofins = aliquota;
//					}						
//				}
//			}		
//			
//			material.setAliquotaicms(icms);
//			material.setAliquotaicmsst(icmsst);
//			material.setAliquotaipi(ipi);
//			material.setAliquotapis(pis);
//			material.setAliquotacofins(cofins);
//			material.setAliquotaiss(iss);			
//			material.setAliquotamva(mva);
//		}
//	}
	
	
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<MaterialOfflineJSON> findForPVOffline() {
		List<MaterialOfflineJSON> lista = new ArrayList<MaterialOfflineJSON>();
//		Map<Integer, Double> mapEntrada = getMapQtdeDisponivel(Movimentacaoestoquetipo.ENTRADA);
//		Map<Integer, Double> mapSaida = getMapQtdeDisponivel(Movimentacaoestoquetipo.SAIDA);
		Map<Integer, List<Localarmazenagem>> mapEntrada = getMapQtdeDisponivelPorLocal(Movimentacaoestoquetipo.ENTRADA);
		Map<Integer, List<Localarmazenagem>> mapSaida = getMapQtdeDisponivelPorLocal(Movimentacaoestoquetipo.SAIDA);
		Map<Integer, Double> mapLocalEntradaSaida;
			
		Integer cdlocal;
		List<Localarmazenagem> listaLocalarmazenagem;
		List<Localarmazenagem> listaLocalarmazenagemEntrada;
		List<Localarmazenagem> listaLocalarmazenagemSaida;
		
		for(Material m : materialDAO.findForPVOffline()){
//			Double entrada = mapEntrada.get(m.getCdmaterial());
//			Double saida = mapSaida.get(m.getCdmaterial());
//			Double qtdisponivel = (entrada!=null?entrada:0d)-(saida!=null?saida:0d);

			boolean localnulo = true;
			Double qtdetotal = 0.0;
			listaLocalarmazenagemEntrada = mapEntrada.get(m.getCdmaterial());
			listaLocalarmazenagemSaida = mapSaida.get(m.getCdmaterial());			
			mapLocalEntradaSaida = new HashMap<Integer, Double>();
			if(listaLocalarmazenagemEntrada != null){
				for(Localarmazenagem entrada : listaLocalarmazenagemEntrada){
					if(entrada.getCdlocalarmazenagem() != null){
						mapLocalEntradaSaida.put(entrada.getCdlocalarmazenagem(), entrada.getQtde());
						qtdetotal += entrada.getQtde();
					}else {
						mapLocalEntradaSaida.put(0, entrada.getQtde());
						qtdetotal += entrada.getQtde();
						localnulo = false;
					}
				}
			}else {
				mapLocalEntradaSaida.put(0, 0.0);
			}
			if(listaLocalarmazenagemSaida != null){
				for(Localarmazenagem saida : listaLocalarmazenagemSaida){
					if(saida.getCdlocalarmazenagem() != null){
						if(mapLocalEntradaSaida.get(saida.getCdlocalarmazenagem()) != null){
							mapLocalEntradaSaida.put(saida.getCdlocalarmazenagem(), mapLocalEntradaSaida.get(saida.getCdlocalarmazenagem())-saida.getQtde());
							qtdetotal -= saida.getQtde();
						}else {
							mapLocalEntradaSaida.put(saida.getCdlocalarmazenagem(), saida.getQtde());
							qtdetotal -= saida.getQtde();
						}
					}else {
						if(mapLocalEntradaSaida.get(0) != null){
							mapLocalEntradaSaida.put(0, saida.getQtde()-mapLocalEntradaSaida.get(0));
							qtdetotal -= saida.getQtde();
						}else {
							mapLocalEntradaSaida.put(0, saida.getQtde());
							qtdetotal -= saida.getQtde();
						}
						localnulo = false;
					}
				}
			}
			
			listaLocalarmazenagem = new ArrayList<Localarmazenagem>();
			Iterator<Integer> itMapLocalES = mapLocalEntradaSaida.keySet().iterator();
			while (itMapLocalES.hasNext()) {
				Localarmazenagem localarmazenagem = new Localarmazenagem();
				cdlocal = itMapLocalES.next();
				localarmazenagem.setCdlocalarmazenagem(cdlocal);
				if(cdlocal == null || cdlocal == 0){
					localarmazenagem.setQtde(qtdetotal);
				}else {
					localarmazenagem.setQtde(mapLocalEntradaSaida.get(cdlocal));
				}
				listaLocalarmazenagem.add(localarmazenagem);
			}	
			if(localnulo){
				Localarmazenagem localarmazenagem = new Localarmazenagem();
				localarmazenagem.setCdlocalarmazenagem(0);
				localarmazenagem.setQtde(qtdetotal);
				listaLocalarmazenagem.add(localarmazenagem);
			}
			lista.add(new MaterialOfflineJSON(m, listaLocalarmazenagem));
		}
		return lista;
	}
	
	public List<MaterialRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		
		return findForAndroid(whereIn, sincronizacaoInicial, null);
	}
	public List<MaterialRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial, Timestamp dtUltimaSincronizacao) {
		List<MaterialRESTModel> lista = new ArrayList<MaterialRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Material m : materialDAO.findForAndroid(whereIn, dtUltimaSincronizacao))
				lista.add(new MaterialRESTModel(m));
		}
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.MaterialDAO#carregaMaterialForMovimentacaoVenda(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material carregaMaterialForMovimentacaoVenda(Material material) {
		return materialDAO.carregaMaterialForMovimentacaoVenda(material);
	}
	
	/**
     * Busca a quantidade de material disponivel por tipo de movimenta��o de todos os materiais cadastrados 
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * @param tipo Tipo de Movimenta��o estoque
	 */
//	private Map<Integer, Double> getMapQtdeDisponivel(Movimentacaoestoquetipo tipo){
//		Map<Integer, Double> mapa = new HashMap<Integer, Double>();
//		for (GenericBean genericBean : materialDAO.getListaMaterialQtdedisponivel(tipo)) 
//			mapa.put((Integer) genericBean.getId(), (Double) genericBean.getValue());
//		return mapa;
//	}
	
	private Map<Integer, List<Localarmazenagem>> getMapQtdeDisponivelPorLocal(Movimentacaoestoquetipo tipo){
		Map<Integer, List<Localarmazenagem>> mapa = new HashMap<Integer, List<Localarmazenagem>>();
		Localarmazenagem localarmazenagem;
		List<Localarmazenagem> listaLocalarmazenagem;
		for (Movimentacaoestoque movimentacaoestoque : materialDAO.getListaMaterialQtdedisponivelPorLocal(tipo)){ 
			if(mapa.get(movimentacaoestoque.getCdmaterial()) != null){
				listaLocalarmazenagem = mapa.get(movimentacaoestoque.getCdmaterial());
				localarmazenagem = new Localarmazenagem();
				localarmazenagem.setCdlocalarmazenagem(movimentacaoestoque.getCdlocalarmazenagem());
				localarmazenagem.setQtde(movimentacaoestoque.getQtde());
				listaLocalarmazenagem.add(localarmazenagem);
				mapa.put(movimentacaoestoque.getCdmaterial(), listaLocalarmazenagem);				
			}else {
				listaLocalarmazenagem = new ArrayList<Localarmazenagem>();
				localarmazenagem = new Localarmazenagem();
				localarmazenagem.setCdlocalarmazenagem(movimentacaoestoque.getCdlocalarmazenagem());
				localarmazenagem.setQtde(movimentacaoestoque.getQtde());
				listaLocalarmazenagem.add(localarmazenagem);
				mapa.put(movimentacaoestoque.getCdmaterial(), listaLocalarmazenagem);
			}
		}
		return mapa;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForComissaovendaGrupomaterial(Material material, Colaborador colaborador)
	 *
	 * @param material
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForComissaovendaGrupomaterial(Material material, Colaborador colaborador) {
		return materialDAO.findForComissaovendaGrupomaterial(material, colaborador);
	}
	
	public Material findForComissaovenda(Material material) {
		return materialDAO.findForComissaovenda(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForComissaovendaGrupomaterialByFornecedor(Material, Fornecedor)
	 *
	 * @param material
	 * @param fornecedor
	 * @return
	 * @author Jo�o Vitor
	 */
	public Material findForComissaovendaGrupomaterialByFornecedor(Material material, Fornecedor fornecedor) {
		return materialDAO.findForComissaovendaGrupomaterialByFornecedor(material, fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findListaMaterialunidademedida(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findListaMaterialunidademedida(Material material) {
		return materialDAO.findListaMaterialunidademedida(material);
	}
	
	public Material loadMaterialunidademedida(Material material) {
		return materialDAO.loadMaterialunidademedida(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadWithMaterialtipo(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public Material loadWithMaterialtipo(Material material) {
		return materialDAO.loadWithMaterialtipo(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadComContagerencialCentrocusto(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadComContagerencialCentrocusto(Material material) {
		return materialDAO.loadComContagerencialCentrocusto(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Material findByNome(String nome) {
		return materialDAO.findByNome(nome);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#isReferenciaDiferenteNomenf(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public  Boolean isReferenciaDiferenteNomenf(Material material) {
		return materialDAO.isReferenciaDiferenteNomenf(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForRelatorioComUnidademedidaconversao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findForRelatorioComUnidademedidaconversao(String whereIn, Boolean mostrarconversao) {
		return materialDAO.findForRelatorioComUnidademedidaconversao(whereIn,mostrarconversao);
	}
	
	public List<Material> findForComUnidademedidaconversao(String whereIn, Boolean mostrarconversao) {
		return materialDAO.findForComUnidademedidaconversao(whereIn,mostrarconversao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#isMaterialComAlturaLargura(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMaterialComAlturaLargura(Material material) {
		return materialDAO.isMaterialComAlturaLargura(material);
	}
	
	public Boolean isKit(Material material) {
		return materialDAO.isKit(material);
	}
	
	/**
	 * M�todo que verifica se existe duplicidade de n�mero de s�rie
	 *
	 * @param material
	 * @param errors 
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existDuplicidadenumeroserie(Material material, BindException errors) {
		boolean erro = false;
		if(material != null && material.getListaMaterialnumeroserie() != null && material.getListaMaterialnumeroserie().size() > 1){
			List<String> numerosSerie = new ArrayList<String>();
			Set<Materialnumeroserie> listaMaterialnumeroserie = material.getListaMaterialnumeroserie();
			for (Materialnumeroserie materialnumeroserie : listaMaterialnumeroserie) {
				
				if(materialnumeroserie.getNumero() != null && !"".equals(materialnumeroserie.getNumero())){
					if(numerosSerie.contains(materialnumeroserie.getNumero())){
						erro = true;
						if(errors != null){
							errors.reject("001", "N�mero de s�rie duplicado: " + materialnumeroserie.getNumero());
						}else {
							break;
						}
					} else numerosSerie.add(materialnumeroserie.getNumero());
				}
				
			}
		}
		
		return erro;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.MaterialDAO#findAutocompleteMaterialsimilar(String q, String classesting)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findAutocompleteMaterialsimilar(String q){
		List<Materialclasse> lista = new ArrayList<Materialclasse>();
		try {
			WebRequestContext request = NeoWeb.getRequestContext();
			if("true".equalsIgnoreCase(request.getParameter("produto"))) lista.add(Materialclasse.PRODUTO);
			if("true".equalsIgnoreCase(request.getParameter("servico"))) lista.add(Materialclasse.SERVICO);
			if("true".equalsIgnoreCase(request.getParameter("patrimonio"))) lista.add(Materialclasse.PATRIMONIO);
			if("true".equalsIgnoreCase(request.getParameter("epi"))) lista.add(Materialclasse.EPI);
		} catch (Exception e) {}	
		
		return materialDAO.findAutocompleteMaterialsimilar(q, lista);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialValorvendaAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Material> findMaterialValorvendaAutocomplete(String q){
		return materialDAO.findMaterialValorvendaAutocomplete(q);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForTabelaPreco(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForTabelaPreco(String whereIn){
		return materialDAO.findForTabelaPreco(whereIn);
	}
	
	public List<Material> findForRegistrarConsumno(String whereIn){
		return materialDAO.findForRegistrarConsumno(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadMaterialComMaterialproducao(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadMaterialComMaterialproducao(Material material) {
		if(material == null || material.getCdmaterial() == null)
			return null;
		List<Material> lista  = materialDAO.findMaterialComMaterialproducao(material.getCdmaterial().toString());
		return lista != null && !lista.isEmpty() ? lista.get(0) : null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForTrocaproducao(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Material loadForTrocaproducao(Material material) {
		return materialDAO.loadForTrocaproducao(material);
	}
	
	public List<Material> findMaterialComMaterialproducao(String whereIn) {
		return materialDAO.findMaterialComMaterialproducao(whereIn);
	}
	
	/**
	 * M�todo que limpa a refer�ncia das listas do material ao copiar
	 *
	 * @param material
	 * @author Luiz Fernando
	 */
	public void limparReferenciasForCopiar(Material material) {
		if(material != null){
			material.setCdmaterial(null);
			
			if(material.getListaCaracteristica() != null && !material.getListaCaracteristica().isEmpty()){
				for(Materialcaracteristica materialcaracteristica : material.getListaCaracteristica()){
					materialcaracteristica.setCdmaterialcaracteristica(null);
				}
			}
			if(material.getListaContratomaterial() != null && !material.getListaContratomaterial().isEmpty()){
				for(Contratomaterial contratomaterial : material.getListaContratomaterial()){
					contratomaterial.setCdcontratomaterial(null);
				}
			}			
			if(material.getListaFornecedor() != null && !material.getListaFornecedor().isEmpty()){
				for(Materialfornecedor materialfornecedor : material.getListaFornecedor()){
					materialfornecedor.setCdmaterialfornecedor(null);
				}
			}
			if(material.getListaMaterialcolaborador() != null && !material.getListaMaterialcolaborador().isEmpty()){
				for(Materialcolaborador materialcolaborador : material.getListaMaterialcolaborador()){
					materialcolaborador.setCdmaterialcolaborador(null);
				}
			}
			if(material.getListaMaterialempresa() != null && !material.getListaMaterialempresa().isEmpty()){
				for(Materialempresa materialempresa : material.getListaMaterialempresa()){
					materialempresa.setCdmaterialempresa(null);
				}
			}
			if(material.getListaMaterialinspecaoitem() != null && !material.getListaMaterialinspecaoitem().isEmpty()){
				for(Materialinspecaoitem materialinspecaoitem : material.getListaMaterialinspecaoitem()){
					materialinspecaoitem.setCdmaterialinspecaoitem(null);
				}
			}
			if(material.getListaMaterialnumeroserie() != null && !material.getListaMaterialnumeroserie().isEmpty()){
				material.setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class));
			}
			if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
				for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
					materialrelacionado.setCdmaterialrelacionado(null);
				}
			}
			if(material.getListaMaterialkitflexivel() != null && !material.getListaMaterialkitflexivel().isEmpty()){
				for (Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()) {
					materialkitflexivel.setCdmaterialkitflexivel(null);
					if(materialkitflexivel.getListaMaterialkitflexivelformula() != null && !materialkitflexivel.getListaMaterialkitflexivelformula().isEmpty()){
						for (Materialkitflexivelformula materialkitflexivelformula : materialkitflexivel.getListaMaterialkitflexivelformula()) {
							materialkitflexivelformula.setCdmaterialkitflexivelformula(null);
						}
					}
				}
			}
			if(material.getListaMaterialsimilar() != null && !material.getListaMaterialsimilar().isEmpty()){
				for(Materialsimilar materialsimilar : material.getListaMaterialsimilar()){
					materialsimilar.setCdmaterialsimilar(null);
				}
			}
			if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
				for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
					materialunidademedida.setCdmaterialunidademedida(null);
				}
			}
			if(material.getListaMaterialvenda() != null && !material.getListaMaterialvenda().isEmpty()){
				for(Materialvenda materialvenda : material.getListaMaterialvenda()){
					materialvenda.setCdmaterialvenda(null);
				}
			}
			if(material.getListaProducao() != null && !material.getListaProducao().isEmpty()){
				for(Materialproducao materialproducao : material.getListaProducao()){
					materialproducao.setCdmaterialproducao(null);
					if(materialproducao.getListaMaterialproducaoformula() != null && 
							!materialproducao.getListaMaterialproducaoformula().isEmpty()){
						for(Materialproducaoformula materialproducaoformula : materialproducao.getListaMaterialproducaoformula()){
							materialproducaoformula.setCdmaterialproducaoformula(null);
						}
					}
				}
			}
			if(material.getListaMaterialformulapeso() != null && !material.getListaMaterialformulapeso().isEmpty()){
				for(Materialformulapeso materialformulapeso : material.getListaMaterialformulapeso()){
					materialformulapeso.setCdmaterialformulapeso(null);
				}
			}
			if(material.getListaMaterialformulavalorvenda() != null && !material.getListaMaterialformulavalorvenda().isEmpty()){
				for(Materialformulavalorvenda materialformulavalorvenda : material.getListaMaterialformulavalorvenda()){
					materialformulavalorvenda.setCdmaterialformulavalorvenda(null);
				}
			}
			if(material.getListaMaterialformulamargemcontribuicao() != null && !material.getListaMaterialformulamargemcontribuicao().isEmpty()){
				for(Materialformulamargemcontribuicao materialformulamargemcontribuicao : material.getListaMaterialformulamargemcontribuicao()){
					materialformulamargemcontribuicao.setCdmaterialformulamargemcontribuicao(null);
				}
			}
			if(material.getMaterialRateioEstoque() != null && Hibernate.isInitialized(material.getMaterialRateioEstoque())){
				material.getMaterialRateioEstoque().setCdMaterialRateioEstoque(null);
			}
			if(material.getListaEmbalagem() != null && !material.getListaEmbalagem().isEmpty()){
				for(Embalagem embalagem : material.getListaEmbalagem()){
					embalagem.setCdembalagem(null);
				}
			}
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForComissaoRepresentante(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForComissaoRepresentante(Material material) {
		return materialDAO.findForComissaoRepresentante(material);
	}
	
	/**
	 * M�todo que busca os materiais similares
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService.findForTrocaMaterialsimilar(Material material, Unidademedida unidademedida)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForTrocaMaterialsimilar(Material material) {
		return findForTrocaMaterialsimilar(material, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForTrocaMaterialsimilar(Material material, Unidademedida unidademedida)
	*
	* @param material
	* @param unidademedida
	* @return
	* @since 08/08/2016
	* @author Luiz Fernando
	*/
	public Material findForTrocaMaterialsimilar(Material material, Unidademedida unidademedida) {
		return findForTrocaMaterialsimilar(material, unidademedida, null, null);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param material
	 * @param pneumedida
	 * @param pneumodelo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2016
	 */
	public Material findForTrocaMaterialsimilar(Material material, Pneumedida pneumedida, Pneumodelo pneumodelo) {
		return findForTrocaMaterialsimilar(material, null, pneumedida, pneumodelo);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param material
	 * @param unidademedida
	 * @param pneumedida
	 * @param pneumodelo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2016
	 */
	public Material findForTrocaMaterialsimilar(Material material, Unidademedida unidademedida, Pneumedida pneumedida, Pneumodelo pneumodelo) {
		return materialDAO.findForTrocaMaterialsimilar(material, unidademedida, pneumedida, pneumodelo);
	}
	
	/**
	 * M�todo que calcula o saldo do material
	 *
	 * @param min
	 * @param max
	 * @param preco
	 * @param qtde
	 * @return
	 * @author Luiz Fernando
	 */
	public Double calcularSaldoMaterial(Double min, Double max, Double preco, Double qtde){
		qtde = qtde != null ? qtde : 1.0;
		if(preco != null && max != null && preco > max)
			return (preco - max) * qtde;
		else if(preco != null && min != null && preco < min)
			return (preco - min) * qtde;
		else return 0.0;
	}
	
	/**
	 * M�todo que gerar o relat�rio de sugest�o de compra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarSugestaocompraRelatorio(SugestaocompraFiltro filtro) {
		List<Material> lista = ajustaListaSugestaocompra(this.findForSugestaocompra(filtro), filtro);
		
		if(SinedUtil.isListEmpty(lista)){
			throw new SinedException("N�o existe sugest�o de compra de acordo com o filtro selecionado.");
		}
		
		Report report = new Report("/suprimento/sugestaocompra");
		report.setDataSource(lista);
		return report;
	}
	
	public List<Material> ajustaListaSugestaocompra(List<Material> lista, SugestaocompraFiltro filtro) {
		if(lista == null || lista.isEmpty()) return lista;
		
		String whereInMaterial = CollectionsUtil.listAndConcatenate(lista, "cdmaterial", ",");
		List<Vgerenciarmaterial> listaVgerenciarmaterial = findEstoqueAtual(whereInMaterial, filtro);
		for(Material material : lista){
			if(listaVgerenciarmaterial != null && !listaVgerenciarmaterial.isEmpty()){
				for(Vgerenciarmaterial vgerenciarmaterial : listaVgerenciarmaterial){
					if(vgerenciarmaterial.getCdmaterial() != null && vgerenciarmaterial.getCdmaterial().equals(material.getCdmaterial())){
						material.setSugestaocompraestoqueatual(vgerenciarmaterial.getQtdedisponivel());
						break;
					}
				}
			}
			if(material.getSugestaocompraestoqueatual() != null){
				Double vendas = material.getSugestaocompravendas() != null && material.getSugestaocompravendas() > 0 ? material.getSugestaocompravendas() : 1d;
				
				material.setSugestaocompraciclos(material.getSugestaocompraestoqueatual()/vendas);
				material.setSugestaocomprasugestao(((vendas + (vendas * (filtro.getMargemcompra() != null ? filtro.getMargemcompra()/100 : 0d)) - material.getSugestaocompraestoqueatual())));
			}
			if(material.getSugestaocomprasugestao() == null || (material.getSugestaocomprasugestao().compareTo(0d) < 0)) material.setSugestaocomprasugestao(0d);
		}
		
		Collections.sort(lista,new Comparator<Material>(){
			public int compare(Material e1, Material e2) {
				return e2.getSugestaocomprasugestao().compareTo(e1.getSugestaocomprasugestao());
			}
		});
		
		return lista;
	}
	
	public List<Vgerenciarmaterial> findEstoqueAtual(String whereInMaterial, SugestaocompraFiltro filtro) {
		return materialDAO.findEstoqueAtual(whereInMaterial, filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Vgerenciarmaterial> consultarEstoqueAtualForWms(ConsultarEstoqueBean filtro) {
		return materialDAO.consultarEstoqueAtualForWms(filtro);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findForSugestaocompra(SugestaocompraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findForSugestaocompra(SugestaocompraFiltro filtro){
		return materialDAO.findForSugestaocompra(filtro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#haveMaterialNomeIdentificacao(Material material)
	 *
	 * @param identificacao
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 31/01/2013
	 */
	public boolean haveMaterialNomeIdentificacao(String identificacao, String nome) {
		return materialDAO.haveMaterialNomeIdentificacao(identificacao, nome);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialByIdentificacaoNome(String identificacao)
	 *
	 * @param identificacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialByIdentificacaoNome(String identificacao, String nome) {
		return materialDAO.findMaterialByIdentificacaoNome(identificacao, nome);
	}
	
	/**
	 * M�todo que salva os materiais do xmls que n�o constam no sistema
	 *
	 * @param listaMaterialNaoCadastrado
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Material> salvaMateriaisImportacaoproducao(final List<Material> listaMaterialNaoCadastrado) {
		List<Material> listaSalva = new ArrayList<Material>();
		if(listaMaterialNaoCadastrado != null && !listaMaterialNaoCadastrado.isEmpty()){
			listaSalva = (ArrayList<Material>)getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					HashMap<String, Materialgrupo> mapMaterialgrupo = new HashMap<String, Materialgrupo>();
					HashMap<String, Unidademedida> mapUnidademedida = new HashMap<String, Unidademedida>();
					HashMap<String, Material> mapMaterial = new HashMap<String, Material>();
					List<Material> lista = new ArrayList<Material>();
					
					for(Material material : listaMaterialNaoCadastrado){
						if(material.getListaProducao() != null && !material.getListaProducao().isEmpty()){
							for(Materialproducao materialproducao : material.getListaProducao()){
								if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() == null){
									if(mapMaterial.get(materialproducao.getMaterial().getNome()) == null){
										if(materialproducao.getMaterial().getMaterialgrupo() != null && materialproducao.getMaterial().getMaterialgrupo().getCdmaterialgrupo() == null){
											if(mapMaterialgrupo.get(materialproducao.getMaterial().getMaterialgrupo().getNome()) == null){
												materialgrupoService.saveOrUpdateNoUseTransaction(materialproducao.getMaterial().getMaterialgrupo());
												mapMaterialgrupo.put(materialproducao.getMaterial().getMaterialgrupo().getNome(), materialproducao.getMaterial().getMaterialgrupo());
											}else {
												materialproducao.getMaterial().setMaterialgrupo(mapMaterialgrupo.get(materialproducao.getMaterial().getMaterialgrupo().getNome()));
											}
										}
										if(materialproducao.getMaterial().getUnidademedida() != null && materialproducao.getMaterial().getUnidademedida().getCdunidademedida() == null){
											if(mapUnidademedida.get(materialproducao.getMaterial().getUnidademedida().getSimbolo()) == null){
												unidademedidaService.saveOrUpdateNoUseTransaction(materialproducao.getMaterial().getUnidademedida());
												mapUnidademedida.put(materialproducao.getMaterial().getUnidademedida().getSimbolo(), materialproducao.getMaterial().getUnidademedida());
											}else {
												materialproducao.getMaterial().setUnidademedida(mapUnidademedida.get(materialproducao.getMaterial().getUnidademedida().getSimbolo()));
											}
											
										}
										materialService.saveOrUpdateNoUseTransaction(materialproducao.getMaterial());
										mapMaterial.put(materialproducao.getMaterial().getNome(), materialproducao.getMaterial());
									}else {
										materialproducao.setMaterial(mapMaterial.get(materialproducao.getMaterial().getNome()));
									}
								
								}
							}
						}
						if(material.getCdmaterial() == null){
							if(mapMaterial.get(material.getNome()) == null){
								if(material.getMaterialgrupo() != null && material.getMaterialgrupo().getCdmaterialgrupo() == null){
									if(mapMaterialgrupo.get(material.getMaterialgrupo().getNome()) == null){
										materialgrupoService.saveOrUpdateNoUseTransaction(material.getMaterialgrupo());
										mapMaterialgrupo.put(material.getMaterialgrupo().getNome(), material.getMaterialgrupo());
									}else {
										material.setMaterialgrupo(mapMaterialgrupo.get(material.getMaterialgrupo().getNome()));
									}
								}
								if(material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() == null){
									if(mapUnidademedida.get(material.getUnidademedida().getSimbolo()) == null){
										unidademedidaService.saveOrUpdateNoUseTransaction(material.getUnidademedida());
										mapUnidademedida.put(material.getUnidademedida().getSimbolo(), material.getUnidademedida());
									}else {
										material.setUnidademedida(mapUnidademedida.get(material.getUnidademedida().getSimbolo()));
									}
								}
								
								Set<Materialproducao> listaProducao = material.getListaProducao();
								material.setListaProducao(null);
								
								materialService.saveOrUpdateNoUseTransaction(material);
								
								if(material.getCdmaterial() != null){
									materialproducaoService.deleteWhereNotInMaterial(material, SinedUtil.listAndConcatenate(listaProducao, "cdmaterialproducao", ","));
								}
								if(listaProducao != null && listaProducao.size() > 0){
									for (Materialproducao materialproducao : listaProducao) {
										materialproducao.setMaterialmestre(material);
										materialproducaoService.saveOrUpdateNoUseTransaction(materialproducao);
									}
								}
								material.setListaProducao(listaProducao);
								
								mapMaterial.put(material.getNome(), material);
							}else {
								material = mapMaterial.get(material.getNome());
							}
						}
						lista.add(material);
					}
					return lista;
				}
			});
		}
		return listaSalva;
	}
	
	public Double validaMultiplo(Double qtde, Integer qtdeunidade, Double fatorconversao, Double qtdereferencia){
		if(qtde != null && qtdeunidade != null){
			Double qtdeunidadeconvertida = qtdeunidade.doubleValue();
			if(fatorconversao != null && fatorconversao > 0){
				qtdeunidadeconvertida = qtdeunidadeconvertida / (fatorconversao / (qtdereferencia != null ? qtdereferencia : 1.0));
			}
			if((qtde % qtdeunidadeconvertida) != 0){
				return qtdeunidadeconvertida;
			}
		}
		return 0.0;
	}
	
	public List<Material> findByFornecedorCodigo(Fornecedor fornecedor, String cprod) {
		return materialDAO.findByFornecedorCodigo(fornecedor, cprod);
	}
	
	public Material findByNomeIdentificacao(String identificacao, String nome) {
		return materialDAO.findByNomeIdentificacao(identificacao, nome);
	}
	
	public Material findByIdentificacao(String identificacao, Boolean ativo) {
		return this.findByIdentificacao(identificacao, ativo, null);
	}
	
	public Material findByIdentificacao(String identificacao) {
		return this.findByIdentificacao(identificacao, null, null);
	}
	
	public Material findByIdentificacao(String identificacao, Boolean ativo, Integer cdMaterialExcecao) {
		return materialDAO.findByIdentificacao(identificacao, ativo, cdMaterialExcecao);
	}
	
	public Material findByCdmaterial(Integer cdmaterial) {
		return materialDAO.findByCdmaterial(cdmaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialByIdentificacao(String identificacao)
	 *
	 * @param identificacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialByIdentificacao(String identificacao) {
		return materialDAO.findMaterialByIdentificacao(identificacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findServicoByFornecedor(Fornecedor fornecedor)
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findServicoByFornecedor(Fornecedor fornecedor, Empresa empresa) {
		return materialDAO.findServicoByFornecedor(fornecedor, empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForHistoricoAlteracao(Material material)
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/04/2013
	 */
	public Material loadForHistoricoAlteracao(Material material) {
		return materialDAO.loadForHistoricoAlteracao(material);
	}
	
	public Material getMaterialByIdentificacao(String identificacao) {
		List<Material> listaMateriais = this.findMaterialByIdentificacao(identificacao);
		Material material = null;
		if(listaMateriais != null && !listaMateriais.isEmpty())
			material = listaMateriais.get(0);
		return material;
	}
	
	public List<ResumoMaterialunidadeconversao> preencheMaterialWithConversoesVenda(List<Vendamaterial> listaprodutos) {
		List<ResumoMaterialunidadeconversao> lista = new ArrayList<ResumoMaterialunidadeconversao>();
		if(listaprodutos != null && !listaprodutos.isEmpty()){
			Material material;
			ResumoMaterialunidadeconversao resumoMaterialunidadeconversao;
//			ResumoMaterialunidadeconversao resumoMaterialunidadeprincipal = null;
			
			for(Vendamaterial vendamaterial : listaprodutos){
				if(vendamaterial.getMaterial() != null){
					material = this.loadMaterialunidademedida(vendamaterial.getMaterial());
					if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
						if(material != null && !material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
							if(material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
								resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
								resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
								resumoMaterialunidadeconversao.setMaterial(material);
								resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
								
								addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
							}else {
								resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
								resumoMaterialunidadeconversao.setMaterial(material);
								resumoMaterialunidadeconversao.setUnidademedida(material.getUnidademedida());
								resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
																		vendamaterial.getQuantidade(), 
																		vendamaterial.getUnidademedida(), 
																		material,null,1.0));
								
//								resumoMaterialunidadeprincipal = resumoMaterialunidadeconversao;
								addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
							}
							
//							for(Materialunidademedida mUnidademedida : material.getListaMaterialunidademedida()){
//								if(mUnidademedida.getMostrarconversao() != null && mUnidademedida.getMostrarconversao()){
//									if(mUnidademedida.getUnidademedida().equals(vendamaterial.getUnidademedida())){
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}else {
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(mUnidademedida.getUnidademedida());
//										if(resumoMaterialunidadeprincipal != null){
//											Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, vendamaterial.getUnidademedida());
//											Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, mUnidademedida.getUnidademedida());
//											
//											if(fracao1 != null && fracao1 > 0 && fracao2 != null){
//												resumoMaterialunidadeconversao.setQtde((vendamaterial.getQuantidade() / fracao1) * fracao2);
//											}
//										}else {
//											resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(mUnidademedida.getUnidademedida(), 
//																				vendamaterial.getQuantidade(), 
//																				vendamaterial.getUnidademedida(), 
//																				material,null,1.0));
//										}
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}
//								}
//							}
						}
					}
				}
			}
		}
		return lista;
	}
	
	private void addResumomaterialunidadeconversao(List<ResumoMaterialunidadeconversao> lista, ResumoMaterialunidadeconversao resumoMaterialunidadeconversao) {
		if(resumoMaterialunidadeconversao != null && resumoMaterialunidadeconversao.getMaterial() != null &&
				resumoMaterialunidadeconversao.getUnidademedida() != null){
			if(lista == null)
				lista = new ArrayList<ResumoMaterialunidadeconversao>();
			
			boolean adicionar = true;
			
			for(ResumoMaterialunidadeconversao bean : lista){
				if(bean.equals(resumoMaterialunidadeconversao)){
					bean.setQtde((resumoMaterialunidadeconversao.getQtde() != null ? resumoMaterialunidadeconversao.getQtde() : 0d) +
								 (bean.getQtde() != null ? bean.getQtde() : 0d));
					adicionar = false;
					break;
				}
				
			}
			
			if(adicionar){
				lista.add(resumoMaterialunidadeconversao);
			}
		}		
	}
	
	public List<ResumoMaterialunidadeconversao> preencheMaterialWithConversoesVendaorcamento(List<Vendaorcamentomaterial> listaprodutos) {
		List<ResumoMaterialunidadeconversao> lista = new ArrayList<ResumoMaterialunidadeconversao>();
		if(listaprodutos != null && !listaprodutos.isEmpty()){
			Material material;
			ResumoMaterialunidadeconversao resumoMaterialunidadeconversao;
//			ResumoMaterialunidadeconversao resumoMaterialunidadeprincipal = null;
			
			for(Vendaorcamentomaterial vendamaterial : listaprodutos){
				if(vendamaterial.getMaterial() != null){
					material = this.loadMaterialunidademedida(vendamaterial.getMaterial());
					if(material != null && !material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
						if(material != null && material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
							resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
							resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
							resumoMaterialunidadeconversao.setMaterial(material);
							resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
							
							addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
						}else {
							resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
							resumoMaterialunidadeconversao.setMaterial(material);
							resumoMaterialunidadeconversao.setUnidademedida(material.getUnidademedida());
							resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
																	vendamaterial.getQuantidade(), 
																	vendamaterial.getUnidademedida(), 
																	material,null,1.0));
							
//							resumoMaterialunidadeprincipal = resumoMaterialunidadeconversao;
							addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
						}
						
//						if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
//							for(Materialunidademedida mUnidademedida : material.getListaMaterialunidademedida()){
//								if(mUnidademedida.getMostrarconversao() != null && mUnidademedida.getMostrarconversao()){
//									if(mUnidademedida.getUnidademedida().equals(vendamaterial.getUnidademedida())){
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}else {
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(mUnidademedida.getUnidademedida());
//										if(resumoMaterialunidadeprincipal != null){
//											Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, vendamaterial.getUnidademedida());
//											Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, mUnidademedida.getUnidademedida());
//											
//											if(fracao1 != null && fracao1 > 0 && fracao2 != null){
//												resumoMaterialunidadeconversao.setQtde((vendamaterial.getQuantidade() / fracao1) * fracao2);
//											}
//										}else {
//											resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(mUnidademedida.getUnidademedida(), 
//																				vendamaterial.getQuantidade(), 
//																				vendamaterial.getUnidademedida(), 
//																				material,null,1.0));
//										}
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}
//								}
//							}
//						}
					}
				}
			}
		}
		return lista;
	}
	
	public List<ResumoMaterialunidadeconversao> preencheMaterialWithConversoesPedidovenda(List<Pedidovendamaterial> listaprodutos) {
		List<ResumoMaterialunidadeconversao> lista = new ArrayList<ResumoMaterialunidadeconversao>();
		if(listaprodutos != null && !listaprodutos.isEmpty()){
			Material material;
			ResumoMaterialunidadeconversao resumoMaterialunidadeconversao;
//			ResumoMaterialunidadeconversao resumoMaterialunidadeprincipal = null;
			
			for(Pedidovendamaterial vendamaterial : listaprodutos){
				if(vendamaterial.getMaterial() != null){
					material = this.loadMaterialunidademedida(vendamaterial.getMaterial());
					if(material != null && !material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
						if(material != null && material.getUnidademedida().equals(vendamaterial.getUnidademedida())){
							resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
							resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
							resumoMaterialunidadeconversao.setMaterial(material);
							resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
							
							addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
						}else {
							resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
							resumoMaterialunidadeconversao.setMaterial(material);
							resumoMaterialunidadeconversao.setUnidademedida(material.getUnidademedida());
							resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
																	vendamaterial.getQuantidade(), 
																	vendamaterial.getUnidademedida(), 
																	material,null,1.0));
//							resumoMaterialunidadeprincipal = resumoMaterialunidadeconversao;
							addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
						}
						
//						if(material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
//							for(Materialunidademedida mUnidademedida : material.getListaMaterialunidademedida()){
//								if(mUnidademedida.getMostrarconversao() != null && mUnidademedida.getMostrarconversao()){
//									if(mUnidademedida.getUnidademedida().equals(vendamaterial.getUnidademedida())){
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setQtde(vendamaterial.getQuantidade());
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(vendamaterial.getUnidademedida());
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}else {
//										resumoMaterialunidadeconversao = new ResumoMaterialunidadeconversao();
//										resumoMaterialunidadeconversao.setMaterial(material);
//										resumoMaterialunidadeconversao.setUnidademedida(mUnidademedida.getUnidademedida());
//										if(resumoMaterialunidadeprincipal != null){
//											Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, vendamaterial.getUnidademedida());
//											Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, mUnidademedida.getUnidademedida());
//											
//											if(fracao1 != null && fracao1 > 0 && fracao2 != null){
//												resumoMaterialunidadeconversao.setQtde((vendamaterial.getQuantidade() / fracao1) * fracao2);
//											}
//										}else {
//											resumoMaterialunidadeconversao.setQtde(unidademedidaService.converteQtdeUnidademedida(mUnidademedida.getUnidademedida(), 
//																				vendamaterial.getQuantidade(), 
//																				vendamaterial.getUnidademedida(), 
//																				material,null,1.0));
//										}
//										
//										addResumomaterialunidadeconversao(lista, resumoMaterialunidadeconversao);
//									}
//								}
//							}
//						}
					}
				}
			}
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#updateInfByConferenciaEntradafical(Material material, ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean)
	 *
	 * @param material
	 * @param atualizaBean
	 * @author Luiz Fernando
	 */
	public void updateInfByConferenciaEntradafical(Material material, ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean) {
		materialDAO.updateInfByConferenciaEntradafical(material, atualizaBean);
	}
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForAutocompletePatrimonio(String q, Boolean patrimonio)
	 *
	 * @param q
	 * @param patrimonio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public List<Material> findForAutocompletePatrimonio(String q, Boolean patrimonio){
		return materialDAO.findForAutocompletePatrimonio(q, patrimonio);
	}
	
	/**
	 * Busca somente os materiais que s�o patrim�nios
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public List<Material> findForAutocompletePatrimonioTrue(String q){
		return this.findForAutocompletePatrimonio(q, true);
	}
	
	/**
	 * Busca os materiais que n�o s�o patrim�nios
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public List<Material> findForAutocompletePatrimonioFalse(String q){
		return this.findForAutocompletePatrimonio(q, false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForOrdenarMaterialproducao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/10/2013
	 */
	public List<Material> findForOrdenarMaterialproducao(String whereIn) {
		return materialDAO.findForOrdenarMaterialproducao(whereIn);
	}
	
	/**
	 * Retorna o fator de convers�o.
	 *
	 * @param material
	 * @param unidademedidaescolhida
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/11/2013
	 */
	public Double getFatorConversao(Material material, Unidademedida unidademedidaescolhida) {
		Double fatoraconversaoQtdereferencia = 1d;
		Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
		if(unidademedidaescolhida != null && !unidademedidaescolhida.equals(unidademedida)){
			
			material = materialService.loadMaterialunidademedida(material);
			Boolean achou = Boolean.FALSE;
			
			if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
				for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
					if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null && materialunidademedida.getFracao() != null){
						if(materialunidademedida.getUnidademedida().equals(unidademedidaescolhida)){
							fatoraconversaoQtdereferencia = materialunidademedida.getFracaoQtdereferencia();
							achou = Boolean.TRUE;
							break;
						}
					}
				}
			}
			
			if(!achou) {
				List<Unidademedidaconversao> lista = unidademedidaconversaoService.conversoesByUnidademedida(unidademedidaescolhida);
				if(lista != null && lista.size() > 0){
					for (Unidademedidaconversao item : lista) {
						if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
							if(item.getUnidademedidarelacionada().equals(unidademedidaescolhida)){
								fatoraconversaoQtdereferencia = item.getFracaoQtdereferencia();
								break;
							}
						}
					}
				}
			}
		}
		return fatoraconversaoQtdereferencia;
	}
	
	/**
	 * M�todo que retorna o material mestre ou o pr�prio item da grade para entrada/sa�da no estoque
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 30/12/2013
	 */
	public Material getMaterialmestreGradeOrItemGrade(Material material) {
		Material materialMestreItem = material;
		
		if(material != null && material.getCdmaterial() != null){
			Material mat = loadWithGrade(material);
			if(mat != null && mat.getMaterialmestregrade() != null && mat.getMaterialgrupo() != null && 
					mat.getMaterialgrupo().getGradeestoquetipo() != null && 
					Gradeestoquetipo.MESTRE.equals(mat.getMaterialgrupo().getGradeestoquetipo())){
				materialMestreItem = mat.getMaterialmestregrade();
			}
		}
		return materialMestreItem;
	}
	
	public Material loadWithMatrialgrupo(Material material){
		return materialDAO.loadWithMatrialgrupo(material);
	}
	
	public Material loadForTributacao(Material material){
		return materialDAO.loadForTributacao(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 30/12/2013
	 */
	public Material loadWithGrade(Material material) {
		return materialDAO.loadWithGrade(material);
	}
	
	public List<Material> findForCsvGerenciamentoMaterial(String whereInMat) {
		return materialDAO.findForCsvGerenciamentoMaterial(whereInMat);
	}
	
	public List<Material> findServicosAutocomplete(String parametro) {
		return materialDAO.findServicosAutocomplete(parametro);
	}
	
		/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#existProducaoetapa(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Boolean existProducaoetapa(Material material) {
		return materialDAO.existProducaoetapa(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#existMaterialitemByMaterialgrademestre(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public Boolean existMaterialitemByMaterialgrademestre(Material material) {
		return materialDAO.existMaterialitemByMaterialgrademestre(material);
	}
	
	public Boolean isControleGrade(Material material) {
		return materialDAO.isControleGrade(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#isControleMaterialgrademestre(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 30/01/2014
	 */
	public Boolean isControleMaterialgrademestre(Material material) {
		return materialDAO.isControleMaterialgrademestre(material);
	}
	
	public Boolean isControleMaterialitemgrade(Material material) {
		return materialDAO.isControleMaterialitemgrade(material);
	}
	
	public boolean isControleItemgradeSemMaterialgrademestre(Material material) {
		return materialDAO.isControleItemgradeSemMaterialgrademestre(material);
	}
	
	public Boolean isControleItemGradeEPesquisasomentemestre(Material material) {
		return materialDAO.isControleItemGradeEPesquisasomentemestre(material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialitemByMaterialmestregrade(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public List<Material> findMaterialitemByMaterialmestregrade(Material material) {
		return materialDAO.findMaterialitemByMaterialmestregrade(material, null, null);
	}
	
	public List<Material> findMaterialitemByMaterialmestregrade(Material material, String whereInMaterialItenGrade) {
		return materialDAO.findMaterialitemByMaterialmestregrade(material, null, whereInMaterialItenGrade);
	}
	
	public List<Material> findMaterialitemByMaterialmestregrade(Material material, Boolean ativo) {
		return materialDAO.findMaterialitemByMaterialmestregrade(material, ativo, null);
	}
	
	public List<Material> findMaterialitemByMaterialmestregrade(Material material, Boolean ativo, String whereInMaterialItenGrade) {
		return materialDAO.findMaterialitemByMaterialmestregrade(material, ativo, whereInMaterialItenGrade);
	}
	
	/**
	 * M�todo que retorna um whereIn dos itens da grade
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public String getWhereInItensgradeOrMaterialmestregrade(Material bean) {
		StringBuilder whereIn = new StringBuilder();
		if(bean != null && bean.getCdmaterial() != null){
			List<Material> lista = findMaterialitemByMaterialmestregrade(bean);
			if(lista != null && !lista.isEmpty()){
				for(Material m : lista){
					if(m.getCdmaterial() != null && m.getMaterialgrupo() != null && m.getMaterialgrupo().getGradeestoquetipo() != null){
						if(!whereIn.toString().equals("")) whereIn.append(",");
						whereIn.append(m.getCdmaterial());
					}
				}
			}
		}
		return whereIn.toString();
	}
	
	/**
	 * M�todo que salva a grade de materiais
	 *
	 * @param listaMaterial
	 * @author Luiz Fernando
	 * @since 20/01/2014
	 */
	public void salvaGradeMateriais(final List<Material> listaMaterial) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(listaMaterial != null && !listaMaterial.isEmpty()){
					for(Material material : listaMaterial){
						Set<Materialproducao> listaProducao = material.getListaProducao();
						material.setListaProducao(null);
						Set<Materialrelacionado> listaMaterialrelacionado = material.getListaMaterialrelacionado();
						material.setListaMaterialrelacionado(null);
						
						saveOrUpdateNoUseTransaction(material);
						
						if(listaProducao != null && listaProducao.size() > 0){
							for (Materialproducao materialproducao : listaProducao) {
								materialproducao.setMaterialmestre(material);
								materialproducaoService.saveOrUpdateNoUseTransaction(materialproducao);
							}
						}
						if(listaMaterialrelacionado != null && listaMaterialrelacionado.size() > 0){
							for (Materialrelacionado materialrelacionado : listaMaterialrelacionado) {
								materialrelacionado.setMaterial(material);
								materialrelacionadoService.saveOrUpdateNoUseTransaction(materialrelacionado);
							}
						}
					}
				}
				return null;
			}
		});		
	}
	
	public ModelAndView montarGrade(WebRequestContext request, Material material){
		request.setAttribute("DIMENSAOVENDA_1", parametrogeralService.getDimensaovenda(0));
		request.setAttribute("DIMENSAOVENDA_2", parametrogeralService.getDimensaovenda(1));
		request.setAttribute("DIMENSAOVENDA_3", parametrogeralService.getDimensaovenda(2));
		if("true".equalsIgnoreCase(request.getParameter("paramMaterial")) && material.getMaterialmestregrade() != null &&
				material.getMaterialmestregrade().getCdmaterial() != null){
			material.setMaterialmestregrade(materialService.load(material.getMaterialmestregrade(), "material.cdmaterial, material.nome, material.identificacao," +
					"material.valorvenda, material.valorvendaminimo, material.valorvendamaximo"));
			material.setNome(material.getMaterialmestregrade().getNome());
			material.setValorvenda(material.getMaterialmestregrade().getValorvenda());
			material.setValorvendaminimo(material.getMaterialmestregrade().getValorvendaminimo());
			material.setValorvendamaximo(material.getMaterialmestregrade().getValorvendamaximo());
		}
		if(request.getParameter("indexEntregamaterial") != null && !"".equals(request.getParameter("indexEntregamaterial"))){
			material.setIndexEntregamaterial(request.getParameter("indexEntregamaterial"));
		}
		if("true".equalsIgnoreCase(request.getParameter("entradafiscal")) || (material.getFromEntrafiscalMontarGrade() != null &&
				material.getFromEntrafiscalMontarGrade())){
			material.setFromEntrafiscalMontarGrade(true);
			request.setAttribute("urlOrigemGrade", "Entradafiscal");
			request.setAttribute("exibirBotaoFechar", true);
			return new ModelAndView("direct:/crud/popup/montarGrade","material", material);
		}else if("true".equalsIgnoreCase(request.getParameter("recebimento")) || (material.getFromEntrafiscalProcessMontarGrade() != null &&
				material.getFromEntrafiscalProcessMontarGrade())){
			material.setFromEntrafiscalProcessMontarGrade(true);
			request.setAttribute("urlOrigemGrade", "Entradafiscal");
			request.setAttribute("exibirBotaoFechar", true);
			return new ModelAndView("direct:/process/popup/montarGrade","material", material);
		}
		request.setAttribute("urlOrigemGrade", "/" + SinedUtil.getContexto() + "/suprimento/process/Montargrade");
		return new ModelAndView("/process/montarGrade","material", material);
	}
	
	/**
	* M�todo que exclui os materiais da grade criados pela entrada fiscal
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#findForExcluirGrade(String whereInMaterialItenGrade)
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 03/09/2014
	* @author Luiz Fernando
	*/
	public ModelAndView limparGrade(WebRequestContext request, Entregamaterial entregamaterial){
		StringBuilder nomeMaterial = new StringBuilder();
		int countSucesso = 0;
		int countErro = 0;
		boolean notExistsMaterial = true;
		
		if(entregamaterial != null && StringUtils.isNotEmpty(entregamaterial.getWhereInMaterialItenGrade())){
			List<Material> listaMaterial = findForExcluirGrade(entregamaterial.getWhereInMaterialItenGrade());
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				notExistsMaterial = false;
				for(Material materialExcluir : listaMaterial){
					try {
						delete(materialExcluir);
						countSucesso++;
					} catch (Exception e) {
						countErro++;
						nomeMaterial.append(materialExcluir.getNome() + "\n ");
					}
				}
			}
		}
		
		String msg = "";
		if(notExistsMaterial){
			msg += "N�o existe grade criada para o item informado.\n";
		}
		if(countSucesso > 0){
			msg += countSucesso + " iten(s) exclu�do(s) com sucesso.\n";
		}
		if(countErro > 0){
			msg += countErro + " iten(s) n�o pode(m) ser exclu�do(s) porque possui(em) refer�ncias em outros registros do sistema." +
					"\n " + nomeMaterial.toString();
		}
		
		return new JsonModelAndView()
			.addObject("msg", msg.toString())
			.addObject("gradeExcluida", countSucesso);
	}
	
	public ModelAndView salvarGrade(WebRequestContext request, Material material){
		if(material.getMaterialmestregrade() == null){
			request.addError("Para gerar a grade de produtos � preciso informar o material mestre");
			return montarGrade(request, material);
		}
			
		material.setMaterialmestregrade(materialService.loadForEntrada(material.getMaterialmestregrade()));
		if(material.getMaterialmestregrade() != null){
			materialService.setaProduto(material.getMaterialmestregrade());
		}
		
		if(material.getListaMaterialitemgrade() == null || material.getListaMaterialitemgrade().isEmpty()){
			request.addError("Para gerar a grade de produtos � preciso informar os itens");
			return montarGrade(request, material);
		}
		
		Loteestoque loteestoque = material.getLoteestoque();
		
		for(Material item : material.getListaMaterialitemgrade()){
			item.setProduto(true);
			item.setPatrimonio(false);
			item.setEpi(false);
			item.setServico(false);
			if(material.getMaterialmestregrade() != null){
				item.setUnidademedida(material.getMaterialmestregrade().getUnidademedida());
				item.setContagerencial(material.getMaterialmestregrade().getContagerencial());
				item.setMaterialgrupo(material.getMaterialmestregrade().getMaterialgrupo());
				item.setContagerencialvenda(material.getMaterialmestregrade().getContagerencialvenda());
				item.setTipoitemsped(material.getMaterialmestregrade().getTipoitemsped());
				item.setCentrocustovenda(material.getMaterialmestregrade().getCentrocustovenda());
				item.setGrupotributacao(material.getMaterialmestregrade().getGrupotributacao());
				item.setValorcusto(material.getMaterialmestregrade().getValorcusto());
//				item.setValorvenda(material.getMaterialmestregrade().getValorvenda());
//				item.setValorvendamaximo(material.getMaterialmestregrade().getValorvendamaximo());
//				item.setValorvendaminimo(material.getMaterialmestregrade().getValorvendaminimo());
				item.setValorindenizacao(material.getMaterialmestregrade().getValorindenizacao());
				item.setMaterialtipo(material.getMaterialmestregrade().getMaterialtipo());
				item.setTipoitemsped(material.getMaterialmestregrade().getTipoitemsped());
				item.setNcmcapitulo(material.getMaterialmestregrade().getNcmcapitulo());
				item.setNcmcompleto(material.getMaterialmestregrade().getNcmcompleto());
				item.setProducaoetapa(material.getMaterialmestregrade().getProducaoetapa());
				item.setProducao(material.getMaterialmestregrade().getProducao());
				item.setVendapromocional(material.getMaterialmestregrade().getVendapromocional());
				
				item.setProduto_nomereduzido(material.getMaterialmestregrade().getProduto_nomereduzido());
				item.setProduto_qtdmin(material.getMaterialmestregrade().getProduto_qtdmin());
//				item.setProduto_largura(material.getMaterialmestregrade().getProduto_largura());
//				item.setProduto_altura(material.getMaterialmestregrade().getProduto_altura());
//				item.setProduto_comprimento(material.getMaterialmestregrade().getProduto_comprimento());
//				item.setProduto_materialcorte(material.getMaterialmestregrade().getProduto_materialcorte());
				item.setProduto_fatorconversao(material.getMaterialmestregrade().getProduto_fatorconversao());
				item.setProduto_arredondamentocomprimento(material.getMaterialmestregrade().getProduto_arredondamentocomprimento());
				item.setProduto_margemarredondamento(material.getMaterialmestregrade().getProduto_margemarredondamento());
				
				if(material.getMaterialmestregrade().getListaProducao() != null && 
						!material.getMaterialmestregrade().getListaProducao().isEmpty()){
					Set<Materialproducao> listaMaterialproducao = new ListSet<Materialproducao>(Materialproducao.class);
					for(Materialproducao materialproducao : material.getMaterialmestregrade().getListaProducao()){
						Materialproducao beanMaterialproducao = new Materialproducao();
						beanMaterialproducao.setMaterial(materialproducao.getMaterial());
						beanMaterialproducao.setOrdemexibicao(materialproducao.getOrdemexibicao());
						beanMaterialproducao.setConsumo(materialproducao.getConsumo());
						beanMaterialproducao.setDiscriminarnota(materialproducao.getDiscriminarnota());
						beanMaterialproducao.setAltura(materialproducao.getAltura());
						beanMaterialproducao.setLargura(materialproducao.getLargura());
						beanMaterialproducao.setExibirvenda(materialproducao.getExibirvenda());
						beanMaterialproducao.setOpcional(materialproducao.getOpcional());
						beanMaterialproducao.setPreco(materialproducao.getPreco());
						beanMaterialproducao.setValorconsumo(materialproducao.getValorconsumo());
						
						if(materialproducao.getListaMaterialproducaoformula() != null && 
								!materialproducao.getListaMaterialproducaoformula().isEmpty()){
							List<Materialproducaoformula> listaMaterialproducaoformula = new ArrayList<Materialproducaoformula>();
							for(Materialproducaoformula materialproducaoformula : materialproducao.getListaMaterialproducaoformula()){
								Materialproducaoformula beanMaterialproducaoformula = new Materialproducaoformula();
								beanMaterialproducaoformula.setFormula(materialproducaoformula.getFormula());
								beanMaterialproducaoformula.setIdentificador(materialproducaoformula.getIdentificador());
								beanMaterialproducaoformula.setOrdem(materialproducaoformula.getOrdem());
								listaMaterialproducaoformula.add(beanMaterialproducaoformula);
							}
							beanMaterialproducao.setListaMaterialproducaoformula(listaMaterialproducaoformula);
						}
						listaMaterialproducao.add(beanMaterialproducao);
					}
					item.setListaProducao(listaMaterialproducao);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialrelacionado() != null && 
						!material.getMaterialmestregrade().getListaMaterialrelacionado().isEmpty()){
					Set<Materialrelacionado> listaMaterialrelacionado = new ListSet<Materialrelacionado>(Materialrelacionado.class);
					for(Materialrelacionado materialrelacionado : material.getMaterialmestregrade().getListaMaterialrelacionado()){
						Materialrelacionado beanMaterialrelacionado = new Materialrelacionado();
						beanMaterialrelacionado.setMaterialpromocao(materialrelacionado.getMaterialpromocao());
						beanMaterialrelacionado.setOrdemexibicao(materialrelacionado.getOrdemexibicao());
						beanMaterialrelacionado.setQuantidade(materialrelacionado.getQuantidade());
						beanMaterialrelacionado.setValorvenda(materialrelacionado.getValorvenda());
						
						if(materialrelacionado.getListaMaterialrelacionadoformula() != null && 
								!materialrelacionado.getListaMaterialrelacionadoformula().isEmpty()){
							List<Materialrelacionadoformula> listaMaterialrelacionadoformula = new ArrayList<Materialrelacionadoformula>();
							for(Materialrelacionadoformula materialrelacionadoformula : materialrelacionado.getListaMaterialrelacionadoformula()){
								Materialrelacionadoformula beanMaterialrelacionadoformula = new Materialrelacionadoformula();
								beanMaterialrelacionadoformula.setFormula(materialrelacionadoformula.getFormula());
								beanMaterialrelacionadoformula.setIdentificador(materialrelacionadoformula.getIdentificador());
								beanMaterialrelacionadoformula.setOrdem(materialrelacionadoformula.getOrdem());
								listaMaterialrelacionadoformula.add(beanMaterialrelacionadoformula);
							}
							beanMaterialrelacionado.setListaMaterialrelacionadoformula(listaMaterialrelacionadoformula);
						}
						listaMaterialrelacionado.add(beanMaterialrelacionado);
					}
					item.setListaMaterialrelacionado(listaMaterialrelacionado);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialsimilar() != null && 
						!material.getMaterialmestregrade().getListaMaterialsimilar().isEmpty()){
					Set<Materialsimilar> listaMaterialsimilar = new ListSet<Materialsimilar>(Materialsimilar.class);
					for(Materialsimilar materialsimilar : material.getMaterialmestregrade().getListaMaterialsimilar()){
						Materialsimilar beanMaterialsimilar = new Materialsimilar();
						beanMaterialsimilar.setMaterialsimilaritem(materialsimilar.getMaterialsimilaritem());
						listaMaterialsimilar.add(beanMaterialsimilar);
					}
					item.setListaMaterialsimilar(listaMaterialsimilar);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialvenda() != null && 
						!material.getMaterialmestregrade().getListaMaterialvenda().isEmpty()){
					Set<Materialvenda> listaMaterialvenda = new ListSet<Materialvenda>(Materialvenda.class);
					for(Materialvenda materialsimilar : material.getMaterialmestregrade().getListaMaterialvenda()){
						Materialvenda beanMaterialsimilar = new Materialvenda();
						beanMaterialsimilar.setMaterialvendaitem(materialsimilar.getMaterialvendaitem());
						listaMaterialvenda.add(beanMaterialsimilar);
					}
					item.setListaMaterialvenda(listaMaterialvenda);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialcolaborador() != null && 
						!material.getMaterialmestregrade().getListaMaterialcolaborador().isEmpty()){
					Set<Materialcolaborador> listaMaterialcolaborador = new ListSet<Materialcolaborador>(Materialcolaborador.class);
					for(Materialcolaborador materialcolaborador : material.getMaterialmestregrade().getListaMaterialcolaborador()){
						Materialcolaborador beanMaterialcolaborador = new Materialcolaborador();
						beanMaterialcolaborador.setAtivo(materialcolaborador.getAtivo());
						beanMaterialcolaborador.setColaborador(materialcolaborador.getColaborador());
						beanMaterialcolaborador.setDocumentotipo(materialcolaborador.getDocumentotipo());
						beanMaterialcolaborador.setPercentual(materialcolaborador.getPercentual());
						beanMaterialcolaborador.setValor(materialcolaborador.getValor());
						listaMaterialcolaborador.add(beanMaterialcolaborador);
					}
					item.setListaMaterialcolaborador(listaMaterialcolaborador);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialinspecaoitem() != null && 
						!material.getMaterialmestregrade().getListaMaterialinspecaoitem().isEmpty()){
					Set<Materialinspecaoitem> listaMaterialinspecaoitem = new ListSet<Materialinspecaoitem>(Materialinspecaoitem.class);
					for(Materialinspecaoitem materialinspecaoitem : material.getMaterialmestregrade().getListaMaterialinspecaoitem()){
						Materialinspecaoitem beanMaterialinspecaoitem = new Materialinspecaoitem();
						beanMaterialinspecaoitem.setInspecaoitem(materialinspecaoitem.getInspecaoitem());
						listaMaterialinspecaoitem.add(beanMaterialinspecaoitem);
					}
					item.setListaMaterialinspecaoitem(listaMaterialinspecaoitem);
				}
				
				if(material.getMaterialmestregrade().getListaCaracteristica() != null && 
						!material.getMaterialmestregrade().getListaCaracteristica().isEmpty()){
					Set<Materialcaracteristica> listaMaterialcaracteristica = new ListSet<Materialcaracteristica>(Materialcaracteristica.class);
					for(Materialcaracteristica materialcaracteristica : material.getMaterialmestregrade().getListaCaracteristica()){
						Materialcaracteristica beanMaterialcaracteristica = new Materialcaracteristica();
						beanMaterialcaracteristica.setNome(materialcaracteristica.getNome());
						beanMaterialcaracteristica.setValor(materialcaracteristica.getValor());
						listaMaterialcaracteristica.add(beanMaterialcaracteristica);
					}
					item.setListaCaracteristica(listaMaterialcaracteristica);
				}
				
				if(material.getMaterialmestregrade().getListaFornecedor() != null && 
						!material.getMaterialmestregrade().getListaFornecedor().isEmpty()){
					Set<Materialfornecedor> listaMaterialfornecedor = new ListSet<Materialfornecedor>(Materialfornecedor.class);
					for(Materialfornecedor materialfornecedor : material.getMaterialmestregrade().getListaFornecedor()){
						Materialfornecedor beanMaterialfornecedor= new Materialfornecedor();
						beanMaterialfornecedor.setFornecedor(materialfornecedor.getFornecedor());
						beanMaterialfornecedor.setCodigo(materialfornecedor.getCodigo());
						beanMaterialfornecedor.setFabricante(materialfornecedor.getFabricante());
						beanMaterialfornecedor.setTempoentrega(materialfornecedor.getTempoentrega());
						listaMaterialfornecedor.add(beanMaterialfornecedor);
					}
					item.setListaFornecedor(listaMaterialfornecedor);
				}
				
				if(material.getMaterialmestregrade().getListaMaterialempresa() != null && 
						!material.getMaterialmestregrade().getListaMaterialempresa().isEmpty()){
					Set<Materialempresa> listaMaterialempresa = new ListSet<Materialempresa>(Materialempresa.class);
					for(Materialempresa materialempresa : material.getMaterialmestregrade().getListaMaterialempresa()){
						Materialempresa beanMaterialempresa = new Materialempresa();
						beanMaterialempresa.setEmpresa(materialempresa.getEmpresa());
						beanMaterialempresa.setGrupotributacao(materialempresa.getGrupotributacao());
						listaMaterialempresa.add(beanMaterialempresa);
					}
					item.setListaMaterialempresa(listaMaterialempresa);
				}
				if(material.getMaterialmestregrade().getListaMaterialunidademedida() != null && 
						!material.getMaterialmestregrade().getListaMaterialunidademedida().isEmpty()){
					Set<Materialunidademedida> listaMaterialunidademedida = new ListSet<Materialunidademedida>(Materialunidademedida.class);
					for(Materialunidademedida materialunidademedida : material.getMaterialmestregrade().getListaMaterialunidademedida()){
						Materialunidademedida beanMaterialunidademedida = new Materialunidademedida();
						beanMaterialunidademedida.setUnidademedida(materialunidademedida.getUnidademedida());
						beanMaterialunidademedida.setFracao(materialunidademedida.getFracao());
						beanMaterialunidademedida.setQtdereferencia(materialunidademedida.getQtdereferencia());
						beanMaterialunidademedida.setMostrarconversao(materialunidademedida.getMostrarconversao());
						beanMaterialunidademedida.setPrioridadevenda(materialunidademedida.getPrioridadevenda());
						beanMaterialunidademedida.setValorunitario(materialunidademedida.getValorunitario());
						listaMaterialunidademedida.add(beanMaterialunidademedida);
					}
					item.setListaMaterialunidademedida(listaMaterialunidademedida);
				}
			}
			
			if(item.getMaterialmestregrade() == null){
				request.addError("Material mestre n�o informado no item da grade. " + (item.getNome() != null ? item.getNome() : ""));
				return montarGrade(request, material);
			}
			
			if(StringUtils.isNotBlank(material.getNomenf())){
				item.setNomenf(material.getNomenf());
			}
		}
		
		try {
			materialService.salvaGradeMateriais(material.getListaMaterialitemgrade());
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MATERIAL_NOME")) {
				request.addError("Material j� cadastrado no sistema. (N�o � permitido cadastrar material com o mesmo nome)");
				return montarGrade(request, material);
			}else {
				request.addError("Erro ao criar grade. " + e.getMessage());
				return montarGrade(request, material);
			}
		}catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao criar grade. " + e.getMessage());
			return montarGrade(request, material);
		}
		
		try {
			if(material.getListaMaterialitemgrade() != null && !material.getListaMaterialitemgrade().isEmpty()){
				for(Material item : material.getListaMaterialitemgrade()){
					if(item.getCdmaterial() != null){
						if(loteestoque != null && loteestoque.getCdloteestoque() != null){
							Lotematerial lotematerial = new Lotematerial();
							lotematerial.setMaterial(item);
							lotematerial.setLoteestoque(loteestoque);
							lotematerialService.saveOrUpdate(lotematerial);
						}
						
						if(item.getIdentificacao() == null || "".equals(item.getIdentificacao())){
							materialService.updateIdentificacao(item, item.getCdmaterial().toString());
						}
						
						Produto produto = materialService.preencheProduto(item);
						if(produto.getAltura() != null) produto.setAltura(produto.getAltura()*1000);
						if(produto.getLargura() != null) produto.setLargura(produto.getLargura()*1000);
						if(produto.getComprimento() != null) produto.setComprimento(produto.getComprimento()*1000);
						materialService.insereProduto(item, false, produto);
					}
				}
			}
		} catch (Exception e) {}
		
		if((material.getFromEntrafiscalMontarGrade() != null && material.getFromEntrafiscalMontarGrade()) ||
				material.getFromEntrafiscalProcessMontarGrade() != null && material.getFromEntrafiscalProcessMontarGrade()){
			
			if(loteestoque != null && loteestoque.getCdloteestoque() != null){
				loteestoque = loteestoqueService.load(loteestoque, "loteestoque.cdloteestoque, loteestoque.numero");
			}
			String whereInMaterialItenGrade = CollectionsUtil.listAndConcatenate(material.getListaMaterialitemgrade(), "cdmaterial", ",");
			String indexEntregamaterial = material.getIndexEntregamaterial();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(	"<script type=\"text/javascript\">" +
					"parent.setWhereInMaterialItenGrade('"+indexEntregamaterial+"','" + whereInMaterialItenGrade + "');" +
					(
							loteestoque != null && loteestoque.getCdloteestoque() != null ? 
									("parent.setLoteestoqueForGrade('"+indexEntregamaterial+"','" + loteestoque.getCdloteestoque() +
									"','" + loteestoque.getNumero() + "');"): ""
					) +
					"alert('Grade gerada com sucesso!');" +
					"parent.$.akModalRemove(true);</script>");
			return null;
		}
		request.addMessage("Grade gerada com sucesso.");
		return new ModelAndView("redirect:/suprimento/crud/Material");
	}
	
	public ModelAndView buscarInfMaterialGradeAJAX(WebRequestContext request, Material material){
		if(material.getCdmaterial() != null){
			material = materialService.load(material, "material.cdmaterial, material.nome, material.nomenf, material.valorvenda," +
					"material.valorvendaminimo, material.valorvendamaximo, material.obrigarlote");
		}		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("material", material);
		return json;
	}
	
	public ModelAndView buscarInfLoteestoqueGradeAJAX(WebRequestContext request, Loteestoque loteestoque){
		if(loteestoque.getCdloteestoque() != null){
			loteestoque = loteestoqueService.load(loteestoque, "loteestoque.cdloteestoque, loteestoque.numero");
		}		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("loteestoquenumero", loteestoque != null ? loteestoque.getNumero() : "");
		return json;
	}
	
	public Double getQtdeParamCalculometroquadrado(String param, Double altura, Double largura, Double comprimento){
		if(param == null || "".equals(param)) return null;
		
		if(altura == null && largura == null && comprimento == null) return 0d;
		
		Double valor = 0d;
		
		if(altura == null) altura = 1d;
		if(largura == null) largura = 1d;
		if(comprimento == null) comprimento = 1d;
		
		if("LxAxC".equalsIgnoreCase(param) || "CxAxL".equalsIgnoreCase(param) || "AxLxC".equalsIgnoreCase(param) ||
				"LxCxA".equalsIgnoreCase(param)){
			valor = altura * largura * comprimento;
		}else if("LxA".equalsIgnoreCase(param) || "AxL".equalsIgnoreCase(param)){
			valor = altura * largura;
		}else if("LxC".equalsIgnoreCase(param) || "CxL".equalsIgnoreCase(param)){
			valor = largura * comprimento;
		}else if("AxC".equalsIgnoreCase(param) || "CxA".equalsIgnoreCase(param)){
			valor = altura * comprimento;
		}
		return valor;
	}
	
	public void inutilizarMaterialSemEstoque(Material material,	Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque) {
		
		try {
			if("true".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INUTILIZARMATERIALSEMESTOQUE))){
				Material bean = loadWithGrade(material);
				if(bean != null && bean.getMaterialmestregrade() != null){
					Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(material,localarmazenagem,empresa,null,loteestoque);
					if (entrada == null){
						entrada = 0d;
					}
					Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(material,localarmazenagem,empresa,null,loteestoque);
					if (saida == null){
						saida = 0d;
					}
					Double qtdedisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
					if(qtdedisponivel == null || qtdedisponivel <= 0){
						inativarMaterial(material);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void inativarMaterial(Material material) {
		materialDAO.inativarMaterial(material);
	}
	
	private void ativarMaterial(Material material) {
		materialDAO.ativarMaterial(material);
	}
	
	public void reutilizarMaterialComEstoque(Material material,	Empresa empresa, Localarmazenagem localarmazenagem){
		try {
			if("true".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INUTILIZARMATERIALSEMESTOQUE))){
				Material bean = loadWithGrade(material);
				if(bean != null && bean.getMaterialmestregrade() != null){
					Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(material,localarmazenagem,empresa,null);
					if (entrada == null){
						entrada = 0d;
					}
					Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(material,localarmazenagem,empresa,null);
					if (saida == null){
						saida = 0d;
					}
					Double qtdedisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
					if(qtdedisponivel != null && qtdedisponivel > 0){
						ativarMaterial(material);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCopiarFormulapeso(Materialtipo materialtipo, Material material)
	 *
	 * @param materialtipo
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 10/02/2014
	 */
	public List<Material> findForCopiarFormulapeso(Materialtipo materialtipo, Material material) {
		return materialDAO.findForCopiarFormulapeso(materialtipo, material);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForAtualizacaoEstoque(AtualizacaoEstoqueFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/02/2014
	 */
	public List<AtualizacaoEstoqueMaterialBean> findForAtualizacaoEstoque(AtualizacaoEstoqueFiltro filtro, String whereInMaterial, String whereInCodigobarras) {
		return materialDAO.findForAtualizacaoEstoque(filtro, whereInMaterial, whereInCodigobarras);
	}
	
	public List<Material> findForSolicitacaoCompraFromGerenciamentomaterial(String whereIn) {
		return materialDAO.findForSolicitacaoCompraFromGerenciamentomaterial(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCopiarFormulavalorvenda(Materialgrupo materialgrupo, Material material)
	 *
	 * @param materialgrupo
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 21/05/2014
	 */
	public List<Material> findForCopiarFormulavalorvenda(Materialgrupo materialgrupo, Material material) {
		return materialDAO.findForCopiarFormulavalorvenda(materialgrupo, material);
	}
	
	public List<Material> findForCopiarFormulavalorvendaminimo(Materialgrupo materialgrupo, Material material) {
		return materialDAO.findForCopiarFormulavalorvendaminimo(materialgrupo, material);
	}
	
	public List<Material> findForCopiarFormulavalorvendamaximo(Materialgrupo materialgrupo, Material material) {
		return materialDAO.findForCopiarFormulavalorvendamaximo(materialgrupo, material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadForCalcularValorvenda(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 21/05/2014
	 */
	public Material loadForCalcularValorvenda(Material material) {
		return materialDAO.loadForCalcularValorvenda(material);
	}
	
	public List<Material> findForCalcularValorvenda(String whereIn) {
		return materialDAO.findForCalcularValorvenda(whereIn);
	}
	
	public List<Material> findForCalcularValorvendaminimo(String whereIn) {
		return materialDAO.findForCalcularValorvendaminimo(whereIn);
	}
	
	public List<Material> findForCalcularValorvendamaximo(String whereIn) {
		return materialDAO.findForCalcularValorvendamaximo(whereIn);
	}
	
	public Material loadForCalcularValorvendaminimo(Material material) {
		if(material != null && material.getCdmaterial() != null){
			List<Material> lista = findForCalcularValorvendaminimo(material.getCdmaterial().toString());
			return lista != null ? lista.get(0) : material;
		}
		return material;
	}
	
	public Material loadForCalcularValorvendamaximo(Material material) {
		if(material != null && material.getCdmaterial() != null){
			List<Material> lista = findForCalcularValorvendamaximo(material.getCdmaterial().toString());
			return lista != null ? lista.get(0) : material;
		}
		return material;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCalcularMargemcontribuicao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularMargemcontribuicao(String whereIn) {
		return materialDAO.findForCalcularMargemcontribuicao(whereIn);
	}
	
	/**
	* M�todo que carrega o material para calcular a margem de contribui��o
	*
	* @param material
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public Material loadForCalcularMargemcontribuicao(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
			
		List<Material> lista = findForCalcularMargemcontribuicao(material.getCdmaterial().toString());
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	/**
	* M�todo para ajustar NCM, Peso Liquido, Peso Bruto e Material Mestre da Grade
	*  
	*
	* @param material
	* @since Jun 13, 2014
	* @author Lucas Costa
	*/
	public void updateMaterialAtualizarLote(Material material, Materialajustarpreco materialajustarpreco) {
		materialDAO.updateMaterialAtualizarLote(material, materialajustarpreco);	
	}
	
	public Double getValorvendaByCadastroMaterial(Material material, boolean carregarMaterial) throws ScriptException {
		boolean isValorformula = materialformulavalorvendaService.setValorvendaByFormula(material, null, null, null, null, material.getValorcusto(), carregarMaterial);
		if(isValorformula){
			return material.getValorvenda();
		}
		return null;
	}
	
	public Material loadWithMaterialMestre(Material material){
		return materialDAO.loadWithMaterialMestre(material);
	}
	
	public Material loadWithMaterialMestre(Material material, boolean carregarValorMinimoMaximo){
		return materialDAO.loadWithMaterialMestre(material, carregarValorMinimoMaximo);
	}
	
	/**
	* M�todo que retorna o valor do custo m�dio de acordo com as entradas de estoque
	* 
	* @see br.com.linkcom.sined.geral.service.MaterialService#loadForCalcularCustoMedio(Material material)
	* @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getTotalQtdeValor(Material material, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial)
	*
	* @param bean
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Double getValorCustoMedio(Material bean, Movimentacaoestoque meReferencia, MaterialCustoEmpresa mce){
		if(bean == null || bean.getCdmaterial() == null)
			throw new SinedException("Material inv�lido.");
		
		Double valorCustoMedio = null;
		
		Material material = loadForCalcularCustoMedio(bean);
		
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		if(material.getValorcustoinicial() == null)
			throw new SinedException("O custo inicial do material n�o pode ser nulo. " + (StringUtils.isNotBlank(material.getNome()) ? material.getNome() : ""));
		
		Date dtInicio = null;
		Date dtInicioQtdeAnt = null;
		if(meReferencia != null && meReferencia.getDtmovimentacao() != null){
			dtInicio = meReferencia.getDtmovimentacao();
			dtInicioQtdeAnt = material.getDtcustoinicial();
			if(dtInicioQtdeAnt != null && SinedDateUtils.beforeIgnoreHour(meReferencia.getDtmovimentacao(), dtInicioQtdeAnt)){
				return null;
			}
		}else {
			dtInicio = material.getDtcustoinicial();
		}
		
		if(dtInicio == null)
			throw new SinedException("Data de in�cio n�o pode ser nula.");
		
		boolean decrementarData = true;
		Double vl_ant = material.getValorcustoinicial() != null ? material.getValorcustoinicial() : 0d;
		Movimentacaoestoque meAnterior = null;
		if(meReferencia != null){
			meAnterior = movimentacaoestoqueService.getMovimentacaoAnterior(material, meReferencia);
			if(meAnterior != null && meAnterior.getValorcusto() != null){
				vl_ant = meAnterior.getValorcusto();
				if(meAnterior.getDtmovimentacao() != null){ 
					dtInicio = meAnterior.getDtmovimentacao();
					decrementarData = false;
				}
			}
		}
		
		Date dtFim = decrementarData ? SinedDateUtils.addDiasData(dtInicio, -1) : dtInicio;
		Double qt_ant = gerenciamentomaterialService.getQtdeDisponivelCustoEntrada(material, dtInicioQtdeAnt, dtFim, meAnterior != null ? meAnterior : meReferencia);
		if(qt_ant != null && material.getQtdecustoinicial() != null && meReferencia != null){
			qt_ant += material.getQtdecustoinicial();
		}
		if(meAnterior != null && meAnterior.getQtde() != null && meAnterior.getDtmovimentacao() != null && dtInicioQtdeAnt != null && decrementarData && SinedDateUtils.equalsIgnoreHour(meAnterior.getDtmovimentacao(), dtInicioQtdeAnt)){
			qt_ant += meAnterior.getQtde();
		}
		
		List<Movimentacaoestoque> listaMovimentacaoestoque;
		if(mce != null && mce.getEmpresa() != null) {
			listaMovimentacaoestoque = movimentacaoestoqueService.findByMaterial(material, dtInicio, meAnterior, mce.getEmpresa());
		} else {
			listaMovimentacaoestoque = movimentacaoestoqueService.findByMaterial(material, dtInicio, meAnterior, null);
		}
		
		boolean valorCalculado = false;
		if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
			for(Movimentacaoestoque me : listaMovimentacaoestoque){
				if(me.getMovimentacaoestoquetipo() != null && me.getQtde() != null && me.getDtmovimentacao() != null){ 
					
					if(Movimentacaoestoquetipo.ENTRADA.equals(me.getMovimentacaoestoquetipo())){
						Double valorCustoMe = SinedUtil.roundByParametro(me.getValorcusto());
						
						if(valorCustoMedio == null){
							if(vl_ant != null) valorCustoMedio = vl_ant;
							else valorCustoMedio = 0d;
						}
						Double qt_ent = me.getQtde();
						Double vl_ent = me.getValor() != null ? me.getValor() : valorCustoMedio;
						
						Double valorCustoEntrada = vl_ent;
						if(qt_ant > 0){
							valorCustoEntrada = ((qt_ant *vl_ant) + (qt_ent*vl_ent)) / (qt_ant + qt_ent);
						}
						valorCustoEntrada = SinedUtil.roundByParametro(valorCustoEntrada);
						
						if(mce != null && mce.getEmpresa() != null && mce.getDtInicio() != null && mce.getCustoInicial() != null) {
							movimentacaoestoqueService.updateValorCustoPorEmpresaByMovimentacaoestoque(valorCustoEntrada, me);
						} else {
							movimentacaoestoqueService.updateValorCustoEntradaByMovimentacaoestoque(valorCustoEntrada, me);
						}
						
						String observacaoME = "Valor do custo m�dio anterior: " + (valorCustoMe != null ? valorCustoMe + " " : " ") +
											"Novo valor do custo m�dio: " + (valorCustoEntrada != null ? valorCustoEntrada + "" : "");
						if((valorCustoMe == null && valorCustoEntrada != null) || (valorCustoMe != null && valorCustoEntrada == null) 
								|| (valorCustoMe != null && valorCustoEntrada != null && valorCustoMe.compareTo(valorCustoEntrada) != 0)){
							movimentacaoestoqueService.salvarHisotirico(me, observacaoME, MovimentacaoEstoqueAcao.ATUALIZACAO_CUSTO_MEDIO);
						}
						me.setValorcusto(valorCustoEntrada); 
						
						if(SinedUtil.isRateioMovimentacaoEstoque()
								&& me.getValor() == null
								&& me.getValorcusto() != null
								&& Hibernate.isInitialized(me.getRateio()) && me.getRateio() != null 
								&& Hibernate.isInitialized(me.getRateio().getListaRateioitem()) && SinedUtil.isListNotEmpty(me.getRateio().getListaRateioitem())){
							
							rateioService.atualizaValorRateio(me.getRateio(), new Money(me.getQtde() * me.getValorcusto()));
							for(Rateioitem rateioitem : me.getRateio().getListaRateioitem()){
								rateioitemService.updateValorRateioItem(rateioitem);
							}
						}
						
						qt_ant += qt_ent;
						vl_ant = valorCustoEntrada;
						valorCustoMedio = valorCustoEntrada;
						valorCalculado = true;
						
						if(mce != null && meReferencia != null) {
							mce.setValorUltimaCompra(SinedUtil.round(meReferencia.getValor(), 2));
							mce.setQuantidadeReferencia(qt_ant >= 0 ? qt_ant : 0);
						}
					}else if(Movimentacaoestoquetipo.SAIDA.equals(me.getMovimentacaoestoquetipo())){
						qt_ant -= me.getQtde();
					}
				}
			}
		}
		if((mce == null || mce.getEmpresa() == null || mce.getCustoInicial() == null) 
				&& !valorCalculado 
				&& material.getValorcustoinicial() != null 
				&& material.getValorcustoinicial()>0 
				&& (meReferencia == null || !Movimentacaoestoquetipo.SAIDA.equals(meReferencia.getMovimentacaoestoquetipo()))){
			valorCustoMedio = material.getValorcustoinicial();
		} else if(mce != null && mce.getEmpresa() != null) {
			Double quantidadePorEmpresa = gerenciamentomaterialService
					.getQtdeDisponivelCustoEntradaPorEmpresa(material, mce.getDtInicio(), null, meAnterior != null ? meAnterior : meReferencia, mce.getEmpresa());
			
			materialCustoEmpresaService.updateQuantidadeReferencia(mce, quantidadePorEmpresa);
			
			if(mce.getCustoInicial() != null && !valorCalculado) {
				valorCustoMedio = mce.getCustoInicial();
			}
		}
		return valorCustoMedio;
	}
	
	public Double getValorCustoMedio(Material bean, Movimentacaoestoque meReferencia) {
		return getValorCustoMedio(bean, meReferencia, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#updateValorCusto(Material material, Double valorCustoMedio)
	*
	* @param bean
	* @param valorCustoMedio
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void updateValorCusto(Material bean, Double valorCustoMedio){
		materialDAO.updateValorCusto(bean, valorCustoMedio);
		
		materialService.recalcularValorvenda(bean.getCdmaterial().toString());
		materialService.recalcularMinioMaximo(bean.getCdmaterial().toString());
		
		// atualiza o percentual de markup ou o valor de venda de acordo com o novo valor de custo
		List<MaterialFaixaMarkup> listaFaixasMaterial = materialFaixaMarkupService.findByMaterial(bean);
		if(SinedUtil.isListNotEmpty(listaFaixasMaterial)) {
			String tipoAtualizacao = parametrogeralService.getValorPorNome(Parametrogeral.TIPO_ATUALIZACAO_PRECO_VENDA);
			
			for(MaterialFaixaMarkup faixa : listaFaixasMaterial) {
				if(tipoAtualizacao != null && tipoAtualizacao.equals("VALOR_VENDA")) {
					Double novoValor = materialFaixaMarkupService.calcularValorVendaPorMarkup(faixa.getValorCusto(), faixa.getMarkup());
					if(novoValor != null ) {
						materialFaixaMarkupService.updateValorVenda(faixa, novoValor);
						Double novoMarkup = materialFaixaMarkupService.calcularPercentualMarkupPorValorVenda(faixa.getValorCusto(), novoValor);
						if(novoMarkup != null) {
							materialFaixaMarkupService.updatePercentualMarkup(faixa, novoMarkup);
						}
					}
				} else if(tipoAtualizacao != null && tipoAtualizacao.equals("PERCENTUAL_MARKUP")) {
					Double novoMarkup = materialFaixaMarkupService.calcularPercentualMarkupPorValorVenda(faixa.getValorCusto(), faixa.getValorVenda());
					if(novoMarkup != null) {
						materialFaixaMarkupService.updatePercentualMarkup(faixa, novoMarkup);
					}
				}
			}
		}
	}
	
	public void updateValorVendaminimo(Material bean, Double valorCustoMedio){
		materialDAO.updateValorVendaminimo(bean, valorCustoMedio);
	}
	
	public void updateValorVendamaximo(Material bean, Double valorCustoMedio){
		materialDAO.updateValorVendamaximo(bean, valorCustoMedio);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForCalcularCustoMedio(Material material)
	*
	* @param material
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	private Material loadForCalcularCustoMedio(Material material) {
		return materialDAO.loadForCalcularCustoMedio(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#updateValoresIniciais(String whereIn, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial)
	*
	* @param whereIn
	* @param dtcustoinicial
	* @param qtdecustoinicial
	* @param valorcustoinicial
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void updateValoresIniciais(String whereIn, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial) {
		materialDAO.updateValoresIniciais(whereIn, dtcustoinicial, qtdecustoinicial, valorcustoinicial);
	}
	
	public void updateCustoPorEmpresaMaterial(MaterialCustoEmpresa custoEmpresa, Date dtInicial, Double qtdecustoinicialPorEmpresa, Double custoInicial) {
		materialDAO.updateCustoPorEmpresaMaterial(custoEmpresa, dtInicial, qtdecustoinicialPorEmpresa, custoInicial);
	}
	
	/**
	* M�todo que salva os valores dos custos iniciais
	*
	* @see br.com.linkcom.sined.geral.service.GerenciamentomaterialService#findForDefinirCustoInicial(String whereIn, Date dtcustoinicial)
	* @see br.com.linkcom.sined.geral.service.MaterialService#updateValoresIniciais(String whereIn, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial)
	* @see br.com.linkcom.sined.geral.service.MaterialService#recalcularCustoMedio(String whereIn)
	*
	* @param material
	* @param whereIn
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void saveDefinircustoinicial(Material bean, String whereIn) {
		if(bean != null && !StringUtils.isEmpty(whereIn)){
			SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
			List<Material> listaMaterial = gerenciamentomaterialService.findForDefinirCustoInicial(whereIn, bean.getDtcustoinicial(), null);
			
			for(String id : whereIn.split(",")){
				StringBuilder observacao = new StringBuilder();
				Double qtdecustoinicial = 0d;
				
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					for(Material m : listaMaterial){
						if(m.getCdmaterial() != null && id.equals(m.getCdmaterial().toString())){
							if(m.getQtdecustoinicial() == null || m.getQtdecustoinicial() < 0){
								qtdecustoinicial = 0d;
							}else {
								qtdecustoinicial = m.getQtdecustoinicial();
							}
							break;
						}
					}
				}
				
				materialService.updateValoresIniciais(id, bean.getDtcustoinicial(), qtdecustoinicial, bean.getValorcustoinicial());
				observacao.append("Custo do Material: Data de refer�ncia: " + pattern.format(bean.getDtcustoinicial()) +
						" Valor de Custo: " + new Money(bean.getValorcustoinicial()));
				
				Material m = new Material(Integer.parseInt(id));
				
				for(MaterialCustoEmpresa custoPorEmpresa : bean.getListaMaterialCustoEmpresa()) {
					List<MaterialCustoEmpresa> listaCustoEmpresaCadastrados = materialCustoEmpresaService.findByMaterial(m, custoPorEmpresa.getEmpresa());
					Double qtdecustoinicialPorEmpresa = 0D;
					
					List<Material> listaCustoInicialPorEmpresa = gerenciamentomaterialService.findForDefinirCustoInicial(id, custoPorEmpresa.getDtInicio(), null, custoPorEmpresa.getEmpresa());
					
					if(SinedUtil.isListNotEmpty(listaCustoInicialPorEmpresa)) {
						for(Material materialPorEmpresa : listaCustoInicialPorEmpresa) {
							if(id.equals(materialPorEmpresa.getCdmaterial().toString())) {
								if(materialPorEmpresa.getQtdecustoinicial() == null || materialPorEmpresa.getQtdecustoinicial() < 0){
									qtdecustoinicialPorEmpresa = 0d;
								}else {
									qtdecustoinicialPorEmpresa = materialPorEmpresa.getQtdecustoinicial();
								}
								break;
							}
						}
					}
					
					if(SinedUtil.isListNotEmpty(listaCustoEmpresaCadastrados)) {
						// a lista deve conter apenas um item 'custo por empresa' por empresa
						MaterialCustoEmpresa custoPorEmpresaAtualizado = listaCustoEmpresaCadastrados.get(0);
						materialService.updateCustoPorEmpresaMaterial(custoPorEmpresaAtualizado, 
								custoPorEmpresa.getDtInicio(), 
								qtdecustoinicialPorEmpresa,
								custoPorEmpresa.getCustoInicial());
						
						Empresa empresa = empresaService.load(custoPorEmpresa.getEmpresa(), "empresa.cdpessoa, empresa.nomefantasia");
						
						observacao.append("\nCusto Por Empresa:" +
								" Empresa: " + empresa.getNomefantasia() +
								" Data de refer�ncia: " + pattern.format(custoPorEmpresa.getDtInicio()) +
								" Valor de Custo: " + new Money(custoPorEmpresa.getCustoInicial()));
					} else {
						MaterialCustoEmpresa novo = new MaterialCustoEmpresa();
						novo.setEmpresa(custoPorEmpresa.getEmpresa());
						novo.setMaterial(m);
						novo.setQuantidadeReferencia(qtdecustoinicialPorEmpresa >= 0 ? qtdecustoinicialPorEmpresa : 0);
						novo.setCustoInicial(custoPorEmpresa.getCustoInicial());
						novo.setDtInicio(custoPorEmpresa.getDtInicio());
						materialCustoEmpresaService.saveOrUpdate(novo);
						
						Empresa empresa = empresaService.load(custoPorEmpresa.getEmpresa(), "empresa.cdpessoa, empresa.nomefantasia");
						
						observacao.append("\nCusto Por Empresa: " + 
								" Empresa: " + empresa.getNomefantasia() +
								" Data de refer�ncia: " + pattern.format(novo.getDtInicio()) +
								" Valor de Custo: " + new Money(novo.getCustoInicial()));
					}
				}
				
				registrarHistoricoMaterial(id, Materialacao.DEFINIDO_CUSTO_MEDIO, observacao.toString());
			}
			
			recalcularCustoMedio(whereIn, null);
		}
	}
	
	/**
	* M�todo que recalcula e atualiza o valor de custo do material 
	* 
	* @see br.com.linkcom.sined.geral.service.MaterialService#updateValorCusto(Material bean, Double valorCustoMedio)
	* @see br.com.linkcom.sined.geral.service.MaterialService#getValorCustoMedio(Material bean)
	* 
	* @param whereIn
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void recalcularCustoMedio(String whereIn, Movimentacaoestoque movimentacaoestoque) {
		if(!parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA) && formulacustoService.existsFormulaAtiva() && !StringUtils.isEmpty(whereIn) &&
				parametrogeralService.getBoolean(Parametrogeral.CALCULAR_CUSTO_MEDIO)){
			List<Material> listaMaterial = findMaterialSemFormulaCusto(whereIn);
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				StringBuilder whereInMaterialprima = new StringBuilder();
				for(Material material : listaMaterial){
					try {
						updateValorCusto(material, getValorCustoMedio(material, movimentacaoestoque));
						updateValorMaterialCustoEmpresa(material, movimentacaoestoque);

						whereInMaterialprima.append(material.getCdmaterial()).append(",");
					} catch (Exception e) {
	//					System.out.println("Erro ao calcular custo medio");
						e.printStackTrace();
						NeoWeb.getRequestContext().clearMessages();
						NeoWeb.getRequestContext().addError("Erro ao calcular custo m�dio: " + e.getMessage());
					}
				}
				
				if(StringUtils.isNotEmpty(whereInMaterialprima.toString())){
					try {
						recalcularConsumoAposAtualizacaoCusto(whereInMaterialprima.toString().substring(0, whereInMaterialprima.toString().length()-1));
						recalcularCustoKitAposAtualizacaoCusto(whereInMaterialprima.toString().substring(0, whereInMaterialprima.toString().length()-1));
					} catch (Exception e) {
	//					System.out.println("Erro ao calcular custo do material de produ��o");
						e.printStackTrace();
						NeoWeb.getRequestContext().clearMessages();
						NeoWeb.getRequestContext().addError("Erro ao calcular custo m�dio: " + e.getMessage());
					}
				}
				
				recalcularValorvenda(whereIn);
				recalcularMinioMaximo(whereIn);
			}
		}
	}
	
	public void recalcularValorvenda(String whereIn) {
		List<Material> listaMateriaisWithFaixa = new ArrayList<Material>();
		if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA) || parametrogeralService.getBoolean(Parametrogeral.CALCULAR_CUSTO_MEDIO)){
			List<MaterialFaixaMarkup> listaFaixa = materialFaixaMarkupService.findMarkupValorIdeal(whereIn, null);
			for(MaterialFaixaMarkup faixa: listaFaixa){
				if(faixa != null && faixa.getCdMaterialFaixaMarkup() != null){
					listaMateriaisWithFaixa.add(faixa.getMaterial());
					try {
						Double valorCustoAtual = this.getValorCustoAtual(faixa.getMaterial(), faixa.getUnidadeMedida(), faixa.getEmpresa());
					
						if(valorCustoAtual != null && faixa.getMarkup() != null && faixa.getMarkup() > 0){
							faixa.setValorCusto(valorCustoAtual);
							updateValorVenda(faixa.getMaterial(), faixa.getValorVenda());
						}
					} catch (Exception e) {
						e.printStackTrace();
						NeoWeb.getRequestContext().addError("Erro ao calcular valor de venda ap�s atualizar o custo: " + e.getMessage());
					}
				}
			}
		}
		List<Material> listaMaterial = findMaterialFormulavendaComCusto(whereIn);
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			for(Material material : listaMaterial){
				if(listaMateriaisWithFaixa.contains(material)){
					continue;
				}
				try {
					boolean isValorvendaFormula = materialformulavalorvendaService.setValorvendaByFormula(material, null, null, null, null);
					if(isValorvendaFormula && material.getValorvenda() != null){
						updateValorVenda(material, material.getValorvenda());
					}
				} catch (Exception e) {
					e.printStackTrace();
					NeoWeb.getRequestContext().addError("Erro ao calcular valor de venda ap�s atualizar o custo: " + e.getMessage());
				}
			}
		}
	}
	
	public void recalcularMinioMaximo(String whereIn) {
		List<Material> listaMateriaisWithFaixa = new ArrayList<Material>();
		if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA) || parametrogeralService.getBoolean(Parametrogeral.CALCULAR_CUSTO_MEDIO)){
			List<MaterialFaixaMarkup> listaFaixa = materialFaixaMarkupService.findMarkupValorMinimo(whereIn, null);
			for(MaterialFaixaMarkup faixa: listaFaixa){
				if(faixa != null && faixa.getCdMaterialFaixaMarkup() != null){
					listaMateriaisWithFaixa.add(faixa.getMaterial());
					try {
						Double valorCustoAtual = this.getValorCustoAtual(faixa.getMaterial(), faixa.getUnidadeMedida(), faixa.getEmpresa());
						if(valorCustoAtual != null && faixa.getMarkup() != null && faixa.getMarkup() > 0){
							faixa.setValorCusto(valorCustoAtual);
							updateValorVendaminimo(faixa.getMaterial(), faixa.getValorVenda());
						}
					} catch (Exception e) {
						e.printStackTrace();
						NeoWeb.getRequestContext().addError("Erro ao calcular valor de venda ap�s atualizar o custo: " + e.getMessage());
					}
				}
			}
		}
		List<Material> listaMaterial = findMaterialFormulavendaminimoComCusto(whereIn);
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			for(Material material : listaMaterial){
				if(listaMateriaisWithFaixa.contains(material)){
					continue;
				}
				try {
					boolean isValorFormula = materialformulavendaminimoService.setVendaminimoByFormula(material, null, true);
					if(isValorFormula && material.getValorvendaminimo() != null){
						updateValorVendaminimo(material, material.getValorvendaminimo());
					}
				} catch (Exception e) {
					e.printStackTrace();
					NeoWeb.getRequestContext().addError("Erro ao calcular valor m�nimo ap�s atualizar o custo: " + e.getMessage());
				}
			}
		}
		
		listaMaterial = findMaterialFormulavendamaximoComCusto(whereIn);
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			for(Material material : listaMaterial){
				try {
					boolean isValorFormula = materialformulavendamaximoService.setVendamaximoByFormula(material, null, true);
					if(isValorFormula && material.getValorvendamaximo() != null){
						updateValorVendamaximo(material, material.getValorvendamaximo());
					}
				} catch (Exception e) {
					e.printStackTrace();
					NeoWeb.getRequestContext().addError("Erro ao calcular valor �ximo ap�s atualizar o custo: " + e.getMessage());
				}
			}
		}
	}
	
	/**
	* M�todo que recalculo o custo dos materiais de produ��o de acordo com as mat�rias-primas
	*
	* @param whereInMateriaprima
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public void recalcularConsumoAposAtualizacaoCusto(String whereInMateriaprima){
		if(StringUtils.isNotEmpty(whereInMateriaprima)){
			List<Material> listaMaterialProducao = this.buscarMaterialProducaoAtualizarConsumo(whereInMateriaprima);
			if(SinedUtil.isListNotEmpty(listaMaterialProducao)){
				String whereIn = CollectionsUtil.listAndConcatenate(listaMaterialProducao, "cdmaterial", ",");
				this.recalcularCustoAndCustoMateriaprima(whereIn);
			}
			
		}
	}
	
	public void recalcularCustoKitAposAtualizacaoCusto(String whereInMateriaprima){
		if(StringUtils.isNotEmpty(whereInMateriaprima)){
			List<Material> listaMaterialrelacionado = this.buscarMaterialKitAtualizarConsumo(whereInMateriaprima);
			if(SinedUtil.isListNotEmpty(listaMaterialrelacionado)){
				String whereIn = CollectionsUtil.listAndConcatenate(listaMaterialrelacionado, "cdmaterial", ",");
				this.recalcularCustoKitAndCustoMateriaprima(whereIn);
			}
			
		}
	}
	
	/**
	 * Recalcular o custo de mat�ria prima
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 16/11/2015
	 */
	public void recalcularCustoAndCustoMateriaprima(String whereIn) {
		if(StringUtils.isNotEmpty(whereIn)){
			List<Material> listaMaterial = this.ordernarListaMaterialParaCalculoCusto(this.findMaterialComMaterialproducao(whereIn));
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				for(Material material : listaMaterial){
					if(SinedUtil.isListNotEmpty(material.getListaProducao())){
						for(Materialproducao mp : material.getListaProducao()){
							if(SinedUtil.isListEmpty(mp.getListaMaterialproducaoformula()) &&
									mp.getMaterial() != null && mp.getMaterial().getValorcusto() != null){
								mp.setPreco(mp.getMaterial().getValorcusto());
							}
						}
					}
					try {
						material.setQuantidade(1d);
						Double valorConsumoTotal = materialproducaoService.getValorConsumoTotal(material);
						if(SinedUtil.isListNotEmpty(material.getListaProducao())){
							for(Materialproducao mp : material.getListaProducao()){
								if(mp.getPreco() != null && mp.getValorconsumo() != null && mp.getCdmaterialproducao() != null){
									materialproducaoService.atualizaPrecoEValorConsumo(mp, mp.getPreco(), mp.getValorconsumo());
								}
							}
						}
						if(valorConsumoTotal != null){
							valorConsumoTotal = SinedUtil.round(valorConsumoTotal, 2);
							updateValorcustomateriaprima(material, new Money(valorConsumoTotal));
							try {
								material.setValorcustomateriaprima(new Money(valorConsumoTotal));
								material.setValorcusto(valorConsumoTotal);
								Double custo = materialformulacustoService.calculaCustoMaterial(material);
								if(Double.isNaN(custo)){
									throw new ScriptException("O resultado � NaN.");
								}
								updateValorCusto(material, custo);
							} catch (ScriptException e) {
								e.printStackTrace();
								throw new SinedException("Erro de script ao atualizar valor de custo do material " + material.getNome() + ": " +  e.getMessage());
							} catch (Exception e) {
								e.printStackTrace();
								throw new SinedException("Erro ao atualizar valor de custo do material " + material.getNome() + ": " +  e.getMessage());
							}
						}
					} catch (ScriptException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void recalcularCustoKitAndCustoMateriaprima(String whereIn) {
		if(StringUtils.isNotEmpty(whereIn)){
			List<Material> listaMaterial = this.ordernarListaMaterialParaCalculoCustoKit(this.findMaterialPromocionalComMaterialrelacionado(whereIn));
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				for(Material material : listaMaterial){
					try {
						material.setQuantidade(1d);
						Double valorCustoTotal = materialrelacionadoService.getValorCustooTotal(material);
						if(valorCustoTotal != null){
							valorCustoTotal = SinedUtil.round(valorCustoTotal, 2);
							try {
								material.setValorcusto(valorCustoTotal);
								Double custo = materialformulacustoService.calculaCustoMaterial(material);
								if(Double.isNaN(custo)){
									throw new ScriptException("O resultado � NaN.");
								}
								updateValorCusto(material, custo);
							} catch (ScriptException e) {
								e.printStackTrace();
								throw new SinedException("Erro de script ao atualizar valor de custo do material " + material.getNome() + ": " +  e.getMessage());
							} catch (Exception e) {
								e.printStackTrace();
								throw new SinedException("Erro ao atualizar valor de custo do material " + material.getNome() + ": " +  e.getMessage());
							}
						}
					} catch (ScriptException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	* M�todo que ordena os materiais para calcular o custo de produ��o. 
	* O material de produ��o que tem como materia-prima um material de produ��o ser� inserido no final da lista.
	*
	* @param lista
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	private List<Material> ordernarListaMaterialParaCalculoCusto(List<Material> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			List<Material> listaOrdenada = new ArrayList<Material>();
			List<Material> listaMaterialproducaoComItemProducao = new ArrayList<Material>();
			
			for(Material material : lista){
				if(material.getProducao() == null || !material.getProducao() || material.getListaProducao() == null || 
						material.getListaProducao().size() == 0){
					listaOrdenada.add(material);
				}else {
					boolean existeMaterialproducaoNoItem = false;
					for(Materialproducao mp : material.getListaProducao()){
						if(mp.getMaterial() != null && mp.getMaterial().getProducao() != null && mp.getMaterial().getProducao() && 
								SinedUtil.isListNotEmpty(mp.getMaterial().getListaProducao())){
							existeMaterialproducaoNoItem = true;
						}
					}
					if(existeMaterialproducaoNoItem){
						listaMaterialproducaoComItemProducao.add(material);
					}else {
						listaOrdenada.add(material);
					}
				}
			}
			
			listaOrdenada.addAll(listaMaterialproducaoComItemProducao);
			return listaOrdenada;
		}
		return lista;
	}
	
	private List<Material> ordernarListaMaterialParaCalculoCustoKit(List<Material> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			List<Material> listaOrdenada = new ArrayList<Material>();
			List<Material> listaMaterialrelacionadoComItemRelacionado = new ArrayList<Material>();
			
			for(Material material : lista){
				if(material.getVendapromocional() == null || !material.getVendapromocional() || material.getListaMaterialrelacionado() == null || 
						material.getListaMaterialrelacionado().size() == 0){
					listaOrdenada.add(material);
				}else {
					boolean existeMaterialrelacionadoNoItem = false;
					for(Materialrelacionado mp : material.getListaMaterialrelacionado()){
						if(mp.getMaterialpromocao() != null && mp.getMaterialpromocao().getVendapromocional() != null && mp.getMaterialpromocao().getVendapromocional() && 
								SinedUtil.isListNotEmpty(mp.getMaterialpromocao().getListaMaterialrelacionado())){
							existeMaterialrelacionadoNoItem = true;
						}
					}
					if(existeMaterialrelacionadoNoItem){
						listaMaterialrelacionadoComItemRelacionado.add(material);
					}else {
						listaOrdenada.add(material);
					}
				}
			}
			
			listaOrdenada.addAll(listaMaterialrelacionadoComItemRelacionado);
			return listaOrdenada;
		}
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#buscarMaterialProducaoAtualizarConsumo(String whereInMateriaprima)
	*
	* @param whereInMateriaprima
	* @return
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public List<Material> buscarMaterialProducaoAtualizarConsumo(String whereInMateriaprima) {
		return materialDAO.buscarMaterialProducaoAtualizarConsumo(whereInMateriaprima);
	}
	
	public List<Material> buscarMaterialKitAtualizarConsumo(String whereInMateriaprima) {
		return materialDAO.buscarMaterialKitAtualizarConsumo(whereInMateriaprima);
	}
	
	/**
	* M�todo que recalcula e atualiza o valor de custo do material 
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#recalcularCustoMedio(String whereIn, Movimentacaoestoque movimentacaoestoque)
	*
	* @param whereIn
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public void recalcularCustoMedio(String whereIn) {
		recalcularCustoMedio(whereIn, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForExcluirGrade(String whereIn)
	*
	* @param whereInMaterialItenGrade
	* @return
	* @since 03/09/2014
	* @author Luiz Fernando
	*/
	public List<Material> findForExcluirGrade(String whereInMaterialItenGrade) {
		return materialDAO.findForExcluirGrade(whereInMaterialItenGrade);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.service.MaterialService#buscarMaterialValidacaoCustomedio(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public List<Material> buscarMaterialValidacaoCustomedio(String whereIn) {
		return materialDAO.buscarMaterialValidacaoCustomedio(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param codigo
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Material> findByFornecedorForAutocomplete(String material) {
		Fornecedor fornecedor = (Fornecedor) NeoWeb.getRequestContext().getSession().getAttribute("fornecedorForAutocompleteMaterial");
		return materialDAO.findByFornecedorForAutocomplete(fornecedor, material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param q
	 * @return List<Material>
	 * @author Jo�o Vitor
	 * @since 14/10/2014
	 */
	public List<Material> findMaterialNotaFiscalProdutoAutocomplete(String q) {
		return materialDAO.findMaterialNotaFiscalProdutoAutocomplete(q, Boolean.TRUE);
	}
	
	public List<Material> findMaterialNotaFiscalServicoAutocomplete(String q) {
		return materialDAO.findMaterialNotaFiscalServicoAutocomplete(q, Boolean.TRUE);
	}
	
	public String calculoDigitoVerfifcadorEAN13(String codigoBarras){
		String digitoVerificadorString = "";
		if (codigoBarras.matches("^[0-9]+$") && codigoBarras.length() == 12) {
			
			Integer digitoVerificador = 0;
			
			Integer codigo[] = new Integer[codigoBarras.length()];  
			 
			for (int k = 0; k < codigo.length; k++){
		            codigo[k] = codigoBarras.charAt(k) - '0';
		    }
		        
			for (int i = 0; i < codigo.length; i++) {
					digitoVerificador = digitoVerificador + codigo[i] * ((i % 2) * 2 + 1); 
			}
			
			if (digitoVerificador % 10 > 0) {
				digitoVerificador = 10 - (digitoVerificador % 10);
			} else {
				digitoVerificador = 0;
			}
			
			digitoVerificadorString = digitoVerificador != null ? digitoVerificador.toString() : "";
		}
		return digitoVerificadorString;
	}
	
	
	/**
	 * M�todo que uma lista para gerar as etiquetas de modelo configur�vel.
	 * @param filtro
	 * @return IReport
	 * @author Murilo
	 */
	public LinkedList<MaterialReportBean> geraListaEtiquetaMaterialBean(MaterialFiltroReportTemplate filtro) throws Exception{
		LinkedList<MaterialReportBean> lst = geraListaEtiquetaMaterialBean(filtro.getListaetiquetamaterial(), TipoEtiquetaMaterial.ETIQUETA_CBEAN13);
		filtro.getListaetiquetamaterial().clear();
		return lst;
	}
	
	/**
	 * M�todo que retorna o IReport para gerar as etiquetas de modelo pr�-estabelecido.
	 * 
	 * @param filtro
	 * @return IReport
	 * @author Murilo
	 */
	public IReport gerarEtiqueta(MaterialFiltro filtro) throws Exception {
		LinkedList<MaterialReportBean> lst = geraListaEtiquetaMaterialBean(filtro.getListaetiquetamaterial(), filtro.getTipoetiquetamaterialpredifinido());
		Report report = new Report("/suprimento/"+ filtro.getTipoetiquetamaterialpredifinido().getSufix());
		report.setDataSource(lst);
		filtro.getListaetiquetamaterial().clear();
		return report;
	}
	
	
	/**
	 * Gera uma lista de materialreportbean de acordo com materialetiquetabean.
	 * 
	 * @param listamaterialetiqueta
	 * @param tipoetiquetamaterial
	 * @return List<Material>
	 * @author Jo�o Vitor
	 * @since 26/11/2014
	 */
	public LinkedList<MaterialReportBean> geraListaEtiquetaMaterialBean(List<MaterialEtiquetaBean> listamaterialetiqueta, TipoEtiquetaMaterial tipoetiquetamaterial) throws Exception {
		MaterialReportBean materialBean;

		LinkedList<MaterialReportBean> listaMaterialEtiqueta = new LinkedList<MaterialReportBean>();
		
		String VALIDAR_EAN13_ETIQUETAMATERIAL = parametrogeralService.buscaValorPorNome(Parametrogeral.VALIDAR_EAN13_ETIQUETAMATERIAL);

		for (MaterialEtiquetaBean materialEtq : listamaterialetiqueta) {
			materialBean = new MaterialReportBean();
			BarcodeEAN barcodeEAN;
			List<Material> listamaterial = materialService.buscarMaterialForEtiqueta(materialEtq.getMaterial().getCdmaterial().toString());
			Material material = listamaterial.get(0);
			
			String cdmaterial = material.getCdmaterial() != null ? material.getCdmaterial().toString() : "";
			
			materialBean.setCdmaterial(cdmaterial);
			materialBean.setIdentificacao(StringUtils.isNotBlank(material.getIdentificacao()) ? material.getIdentificacao() : "");
			if(tipoetiquetamaterial.equals(TipoEtiquetaMaterial.ETIQUETA_PIMACO) && material.getMaterialproduto() != null && material.getMaterialproduto().getNomereduzido() != null){
				materialBean.setNome(StringUtils.isNotBlank(material.getMaterialproduto().getNomereduzido()) ? material.getMaterialproduto().getNomereduzido() : "");
			}else{
				materialBean.setNome(StringUtils.isNotBlank(material.getNome()) ? material.getNome() : "");
			}
			materialBean.setValorvenda(material.getValorvenda() != null ? "R$ " + new Money(material.getValorvenda()).toString() : "");
			materialBean.setValorcusto(material.getValorcusto() != null ? material.getValorcusto().toString() : "");
			materialBean.setCor(material.getMaterialcor() != null && StringUtils.isNotBlank(material.getMaterialcor().getNome()) ? material.getMaterialcor().getNome() : "");
			materialBean.setMaterialgrupo_nome(material.getMaterialgrupo() != null && StringUtils.isNotBlank(material.getMaterialgrupo().getNome()) ? material.getMaterialgrupo().getNome() : "");
			materialBean.setUnidade_medida_simbolo(material.getUnidademedida() != null && StringUtils.isNotBlank(material.getUnidademedida().getSimbolo()) ? material.getUnidademedida().getSimbolo() : "");
			materialBean.setUnidade_medida_nome(material.getUnidademedida() != null && StringUtils.isNotBlank(material.getUnidademedida().getNome()) ? material.getUnidademedida().getNome() : "");
			materialBean.setNumero_lote(materialEtq.getLote());
			materialBean.setValidade_lote(materialEtq.getValidadeLote());
			materialBean.setEmpresa_recebimento(materialEtq.getEmpresaRecebimento());
			materialBean.setCdentrega(materialEtq.getCdEntrega());
			materialBean.setCdentregadocumento(materialEtq.getCdentregadocumento());
			materialBean.setCdentregamaterial(materialEtq.getCdEntregaMaterial());
			materialBean.setCdordemcompra(materialEtq.getCdordemcompra());
			materialBean.setCdmovimentacaoestoque(materialEtq.getCdmovimentacaoestoque());

			if(TipoEtiquetaMaterial.ETIQUETA_CBEAN13.equals(tipoetiquetamaterial) && !StringUtils.isBlank(VALIDAR_EAN13_ETIQUETAMATERIAL) && "TRUE".equals(VALIDAR_EAN13_ETIQUETAMATERIAL.toUpperCase())){
				if(parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_CODIGO_NA_CONFERENCIA_EXPEDICAO) && (material.getCodigobarras() == null || (material.getCodigobarras().toUpperCase().contains("GTIN")))){
					barcodeEAN = new BarcodeEAN();
					barcodeEAN.setCodeType(1);
					barcodeEAN.setCode(SinedUtil.formataTamanho(material.getCdmaterial().toString(), 13, '0', false));
					materialBean.setCodigobarras_imagem(barcodeEAN.createAwtImage(Color.BLACK, Color.WHITE));
					materialBean.setCodigobarras(SinedUtil.formataTamanho(material.getCdmaterial().toString(), 13, '0', false));
				}else if(StringUtils.isNotBlank(material.getCodigobarras()) && material.getCodigobarras().matches("^[0-9]+$") && material.getCodigobarras().length() == 13){
					barcodeEAN = new BarcodeEAN();
					barcodeEAN.setCodeType(1);
					barcodeEAN.setCode(material.getCodigobarras());	
					materialBean.setCodigobarras_imagem(barcodeEAN.createAwtImage(Color.BLACK, Color.WHITE));
					materialBean.setCodigobarras(material.getCodigobarras());					
				} else {
					throw new SinedException("N�o foi poss�vel emitir " + tipoetiquetamaterial.getDescricao() + " - o n�mero de c�digo de barras do produto "+cdmaterial+" - " + material.getNome() + " � inv�lido.");
				}
			} else {
				materialBean.setCodigobarras(material.getCodigobarras());
			}
			Integer numeroImpressoes = materialEtq.getNumerocopiasimpressaoetiqueta() != null && materialEtq.getNumerocopiasimpressaoetiqueta() > 0 ? materialEtq.getNumerocopiasimpressaoetiqueta() : 1;
			for (int i = 0; i < numeroImpressoes ; i++) {
				listaMaterialEtiqueta.add(materialBean);
			}
		}
		return listaMaterialEtiqueta; 
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param q
	 * @return List<Material>
	 * @author Jo�o Vitor
	 * @since 26/10/2014
	 */
	public List<Material> buscarMaterialForEtiqueta(String whereIn) {
		return materialDAO.buscarMaterialForEtiqueta(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param q
	 * @return List<Material>
	 * @author Jo�o Vitor
	 * @since 08/01/2015
	 */
	public List<Material> buscarMaterialForPopUpEtiqueta(String whereIn) {
		return materialDAO.buscarMaterialForPopUpEtiqueta(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @return
	 * @author Rafael Salvio
	 */
	public Material getMaterialForImportacaoCte(){
		return materialDAO.getMaterialForImportacaoCte();
	}
	
	/**
	 * M�todo que ativa o material caso ele esteja inativo ao efetuar
	 * devolu��o de estoque
	 */
	public void ativaMaterial(Material material) {
		materialDAO.ativaMaterial(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findAjusteicms(String s, String whereInMaterial)
	*
	* @param s
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Material> findEntradafiscalAjusteicms(String s){
		String whereInMaterial = null;
		try {
			List<Material> listaMaterial = (List<Material>)NeoWeb.getRequestContext().getSession().getAttribute("listaMaterialregistrarAjusteEntradaFiscal");
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				whereInMaterial = SinedUtil.listAndConcatenate(listaMaterial, "cdmaterial", ",");
			}
		} catch (Exception e) {}
		
		return materialDAO.findAjusteicms(s, whereInMaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findAjusteicms(String s, String whereInMaterial)
	*
	* @param s
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Material> findNotafiscalprodutoAjusteicms(String s){
		String whereInMaterial = null;
		try {
			List<Material> listaMaterial = (List<Material>)NeoWeb.getRequestContext().getSession().getAttribute("listaMaterialregistrarAjusteNotaFiscal");
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				whereInMaterial = SinedUtil.listAndConcatenate(listaMaterial, "cdmaterial", ",");
			}
		} catch (Exception e) {}
		
		return materialDAO.findAjusteicms(s, whereInMaterial);
	}
	
	public boolean existeRestricaoMaterialempresa(List<Solicitacaocompra> list) {
		boolean existeRestricao = false;
		if(SinedUtil.isListNotEmpty(list)){
			StringBuilder whereIn = new StringBuilder();
			for(Solicitacaocompra sc : list){
				if(sc.getMaterial() != null && sc.getMaterial().getCdmaterial() != null){
					whereIn.append(sc.getMaterial().getCdmaterial()).append(",");
				}
			}
			if(!"".equals(whereIn.toString())){
				existeRestricao = existeRestricaoMaterialempresa(whereIn.substring(0, whereIn.length()-1));
			}
		}
		return existeRestricao;
	}
	
	public boolean existeRestricaoMaterialempresa(String whereIn) {
		return materialDAO.existeRestricaoMaterialempresa(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadKitFlexivel(Material material)
	*
	* @param material
	* @return
	* @since 31/03/2015
	* @author Luiz Fernando
	*/
	public Material loadKitFlexivel(Material material) {
		return materialDAO.loadKitFlexivel(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCalcularValorcusto(String whereInMaterial)
	*
	* @param whereInMaterial
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularValorcusto(String whereInMaterial) {
		return materialDAO.findForCalcularValorcusto(whereInMaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCustoOperacional(String whereInMaterial)
	*
	* @param whereInMaterial
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCustoOperacional(String whereInMaterial) {
		return materialDAO.findForCustoOperacional(whereInMaterial);
	}
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForComissaoAgencia(String cdmaterial)
	*
	* @param cdmaterial
	* @return
	* @since 02/06/2015
	* @author Jo�o Vitor
	*/
	public Material loadForComissaoAgencia(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		List<Material> lista = findForComissaoAgencia(material.getCdmaterial().toString());
		return lista != null && lista.size() > 0 ? lista.get(0) : null; 
		
	}
	
	public List<Material> findForComissaoAgencia(String whereIn) {
		return materialDAO.findForComissaoAgencia(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findRecalcularCustoAposAlteracaoTabelavalorCustoComercial(String param)
	*
	* @param param
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findRecalcularCustoAposAlteracaoTabelavalorCustoComercial(String param){
		return materialDAO.findRecalcularCustoAposAlteracaoTabelavalorCustoComercial(param);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForCalcularCusto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularCusto(String whereIn) {
		return materialDAO.findForCalcularCusto(whereIn);
	}
	
	/**
	* M�todo que atualiza o valor de custo do material com formula
	*
	* @param request
	* @param param
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public void atualizarValorcusto(WebRequestContext request, String param) {
		if(StringUtils.isNotBlank(param) && (Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO.equalsIgnoreCase(param) ||
				Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO.equalsIgnoreCase(param))){
			List<Material> lista = this.findRecalcularCustoAposAlteracaoTabelavalorCustoComercial(param);
			if(SinedUtil.isListNotEmpty(lista)){
				String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdmaterial", ",");
				if(StringUtils.isNotBlank(whereIn)){
					this.recalcularCustoAndCustoMateriaprima(whereIn);
					
					try {
						this.recalcularConsumoAposAtualizacaoCusto(whereIn);
						this.recalcularCustoKitAposAtualizacaoCusto(whereIn);
					} catch (Exception e) {
						request.addError("Erro ao atualizar consumo ap�s atualiza��o de custo: " +  e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	* M�todo que sincroniza a lista de material para o wms
	*
	* @param empresa
	* @param lista
	* @return
	* @since 29/09/2015
	* @author Luiz Fernando
	*/
	public List<WmsBean> sincronizarProdutoWMS(Empresa empresa, List<Material> lista) {
		List<WmsBean> listaWmsBean = new ArrayList<WmsBean>();
		WmsWebServiceStub stub = null;
		
		try {
			stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(empresa) + "/webservices/WmsWebService");
		} catch (Exception e){
			WmsBean wmsBean = new WmsBean();
			wmsBean.setDescricaoErro("Erro url de integra��o: " + e.getMessage());
			wmsBean.setErro(Boolean.TRUE);
			listaWmsBean.add(wmsBean);
		}
		
		if(SinedUtil.isListNotEmpty(lista)){
			String paramMaterialIdentificacaoWMS = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAR_IDENTIFICADOR_MATERIAL_WMS.trim());
			for(Material material : lista){
				WmsBean wmsBean = new WmsBean();
				wmsBean.setMaterial(material);
				
				try {
					wmsBean = pedidovendasincronizacaoService.cadastrarProdutoWMS(material, stub, paramMaterialIdentificacaoWMS);
				} catch (Exception e) {
					wmsBean.setDescricaoErro("" + e.getMessage());
					wmsBean.setErro(Boolean.TRUE);
				}
				
				if(wmsBean.getErro() != null && wmsBean.getErro()){
					wmsBean.setMaterial(material);
					listaWmsBean.add(wmsBean);
				}
			}
		}
		
		return listaWmsBean;
	}
	
	/**
	* M�todo que calcula o valor de custo de materia prima
	*
	* @param listaMaterialproducao
	* @return
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public Money calcularValorcustomateriaprima(List<Materialproducao> listaMaterialproducao){
		Double valorcustomaterialprima = 0d;
		if(SinedUtil.isListNotEmpty(listaMaterialproducao)){
			for(Materialproducao materialproducao : listaMaterialproducao){
				if(materialproducao.getValorconsumo() != null){
					valorcustomaterialprima += materialproducao.getValorconsumo();
				}
			}
		}
		return new Money(SinedUtil.round(valorcustomaterialprima, 2));
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#updateValorcustomateriaprima(Material bean, Money valorcustomateriaprima)
	*
	* @param bean
	* @param valorcustomateriaprima
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public void updateValorcustomateriaprima(Material bean, Money valorcustomateriaprima){
		materialDAO.updateValorcustomateriaprima(bean, valorcustomateriaprima);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForSincronizacaoWms(Material material)
	*
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForSincronizacaoWms(Material material) {
		return materialDAO.findForSincronizacaoWms(material);
	}
	

	/**
	 * C�lcula o valor de venda a partir da entrada fiscal
	 *
	 * @param entregadocumento
	 * @author Rodrigo Freitas
	 * @since 09/10/2015
	 */
	public void calculaValorvendaByEntregadocumento(Entregadocumento entregadocumento) {
		String param = parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALORVENDA_MATERIAL_RECEBIMENTO);
		if(param != null && param.trim().toUpperCase().equals("TRUE")){
			if(entregadocumento != null && 
					entregadocumento.getListaEntregamaterial() != null &&
					!entregadocumento.getListaEntregamaterial().isEmpty()){
				for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
					if(entregamaterial != null &&
							entregamaterial.getMaterial() != null &&
							entregamaterial.getMaterial().getCdmaterial() != null){
						Material material = entregamaterial.getMaterial();
						boolean isValorformula = materialformulavalorvendaService.setValorvendaByFormula(material, null, null, null, null, null, true);
						if(isValorformula) {
							Material mat_aux = this.loadForHistoricoAlteracao(material);
							
							this.updateValorVenda(material, material.getValorvenda());
							
							Materialhistorico materialhistorico = new Materialhistorico();
							materialhistorico.setAcao(Materialacao.ALTERADO_ENTRADAFISCAL);
							materialhistoricoService.setValoresAntigos(materialhistorico, mat_aux);
							materialhistoricoService.saveOrUpdate(materialhistorico);
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 09/10/2015
	 */
	public void updateValorVenda(Material material, Double valorvenda) {
		materialDAO.updateValorVenda(material, valorvenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public List<Material> findForAtualizacaoTributacao() {
		return materialDAO.findForAtualizacaoTributacao();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#findCalcularImposto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public List<Material> findCalcularImposto(String whereIn) {
		return materialDAO.findCalcularImposto(whereIn);
	}
	
	/**
	* M�todo que retorna o material da lista
	*
	* @param material
	* @param listaMaterial
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public Material getMaterialByLista(Material material, List<Material> listaMaterial) {
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			Integer index = listaMaterial.indexOf(material);
			if(index != -1){
				return listaMaterial.get(index);
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialSemFormulaCusto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialSemFormulaCusto(String whereIn) {
		return materialDAO.findMaterialSemFormulaCusto(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialFormulavendaComCusto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 07/03/2017
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialFormulavendaComCusto(String whereIn) {
		return materialDAO.findMaterialFormulavendaComCusto(whereIn);
	}
	
	public List<Material> findMaterialFormulavendaminimoComCusto(String whereIn) {
		return materialDAO.findMaterialFormulavendaminimoComCusto(whereIn);
	}
	
	public List<Material> findMaterialFormulavendamaximoComCusto(String whereIn) {
		return materialDAO.findMaterialFormulavendamaximoComCusto(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/10/2015
	 */
	public List<Material> findForCalculocustoRateiooperacional() {
		return materialDAO.findForCalculocustoRateiooperacional();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#findWithCaracteristica(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 10/02/2016
	* @author Luiz Fernando
	*/
	public List<Material> findWithCaracteristica(String whereIn){
		return materialDAO.findWithCaracteristica(whereIn);
	}
	
	public List<Material> findWithClasse(String whereIn){
		return materialDAO.findWithClasse(whereIn);
	}
	
	public List<MaterialW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de materias a serem sincronizados com o W3Producao.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<MaterialW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialW3producaoRESTModel> lista = new ArrayList<MaterialW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Material m : materialDAO.findForW3Producao(whereIn))
				lista.add(new MaterialW3producaoRESTModel(m));
		}
		
		return lista;
	}
	
	/**
	* M�todo que seta o material com o bean materialunidademedida com prioridade na produ��o 
	*
	* @param material
	* @since 18/02/2016
	* @author Luiz Fernando
	*/
	public void preencherUnidademedidaPrioritariaProducao(Material material) {
		if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialsimilar())){
			for(Materialsimilar materialsimilar : material.getListaMaterialsimilar()){
				if(materialsimilar.getMaterialsimilaritem() != null && 
						SinedUtil.isListNotEmpty(materialsimilar.getMaterialsimilaritem().getListaMaterialunidademedida())){
					for(Materialunidademedida mum : materialsimilar.getMaterialsimilaritem().getListaMaterialunidademedida()){
						if(mum.getPrioridadeproducao() != null && mum.getPrioridadeproducao()){
							materialsimilar.getMaterialsimilaritem().setMaterialunidademedida(mum);
							break;
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForProducaoordemEquipamentos(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public List<Material> findForProducaoordemEquipamentos(String whereIn) {
		return materialDAO.findForProducaoordemEquipamentos(whereIn);
	}
	
	/**
	* M�todo que retorna o peso do material
	*
	* @param material
	* @param altura
	* @param largura
	* @param comprimento
	* @return
	* @since 10/05/2016
	* @author Luiz Fernando
	*/
	public Double getPesoMaterial(Material material, Double altura, Double largura, Double comprimento){
		Double peso = null;
		Unidademedida unidademedida = material.getVendaunidademedida() != null && material.getVendaunidademedida().getCdunidademedida() != null ? 
				unidademedidaService.load(material.getVendaunidademedida(), "unidademedida.cdunidademedida, unidademedida.simbolo") : null ;
		Material bean = materialService.loadForCalcularPeso(material);
		Double fatorconversao = bean.getMaterialproduto() != null && bean.getMaterialproduto().getFatorconversao() != null ? 
				bean.getMaterialproduto().getFatorconversao() : 1000d;
		
		if(altura != null){
			bean.setProduto_altura(new Double(new DecimalFormat("#.#######").format(altura*fatorconversao).replaceAll("\\.", "").replace(",", ".")));
		}
		if(largura != null){
			bean.setProduto_largura(new Double(new DecimalFormat("#.#######").format(largura*fatorconversao).replaceAll("\\.", "").replace(",", ".")));
		}
		if(comprimento != null){
			bean.setProduto_comprimento(new Double(new DecimalFormat("#.#######").format(comprimento*fatorconversao).replaceAll("\\.", "").replace(",", ".")));
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialformulapeso())){
			try {
				bean.setVendasimbolounidademedida(unidademedida != null ? unidademedida.getSimbolo() : "");
				peso = materialformulapesoService.calculaPesoMaterial(bean);
			} catch (ScriptException e) {}
			
			if(Double.isNaN(peso)){
				return null;
			}
			
			if(peso != null){
				peso = SinedUtil.roundByParametro(peso, 6);
			}
		}else {
			peso = bean.getPeso();
		}
		
		return peso;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForCalcularPeso(Material material)
	*
	* @param material
	* @return
	* @since 26/04/2016
	* @author Luiz Fernando
	*/
	public Material loadForCalcularPeso(Material material) {
		Material bean = materialDAO.loadForCalcularPeso(material);
		if(bean != null){
			bean.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(material));
		}
		return bean;
	}
	
	/**
	 * Busca os servi�os para o WS SOAP
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<ServicoWSBean> findServicoForWSSOAP() {
		List<ServicoWSBean> lista = new ArrayList<ServicoWSBean>();
		for(Material m: this.findForWSSOAP(false, true, null))
			lista.add(new ServicoWSBean(m));
		return lista;
	}
	
	/**
	 * Busca os produtos para o WS SOAP
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<ProdutoWSBean> findProdutoForWSSOAP(Timestamp dataReferencia) {
		List<ProdutoWSBean> lista = new ArrayList<ProdutoWSBean>();
		for(Material m: this.findForWSSOAP(true, false, dataReferencia))
			lista.add(new ProdutoWSBean(m));
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param produto
	 * @param servico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	private List<Material> findForWSSOAP(Boolean produto, Boolean servico, Timestamp dataReferencia){
		return materialDAO.findForWSSOAP(produto, servico, dataReferencia);
	}
	
	public Material loadSined(Material material){
		return materialDAO.loadSined(material, null);
	}
	
	public Material loadSined(Material material, String select){
		return materialDAO.loadSined(material, select);
	}
	
	public Material loadWithMaterialgrupoSined(Material material){
		return materialDAO.loadWithMaterialgrupoSined(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.MaterialDAO#existeFormulaValorVendaComPrecoBanda(Material material)
	*
	* @param material
	* @return
	* @since 14/07/2016
	* @author Luiz Fernando
	*/
	public boolean existeFormulaValorVendaComPrecoBanda(Material material) {
		return materialDAO.existeFormulaValorVendaComPrecoBanda(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#loadForIntegracaoSOAP(Material material)
	*
	* @param material
	* @return
	* @since 16/08/2016
	* @author Luiz Fernando
	*/
	public Material loadForIntegracaoSOAP(Material material){
		return materialDAO.loadForIntegracaoSOAP(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findMaterialAutocompleteEstoqueForPesquisa(String q)
	*
	* @param q
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialAutocompleteEstoqueForPesquisa(String q) {
		return materialDAO.findMaterialAutocompleteEstoqueForPesquisa(q);
	}
	
	public List<Material> findForAutocompleteRomaneio(String q) {
		return materialDAO.findForAutocompleteRomaneio(q);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#getQtdeMinimaMaterial(Integer cdmaterial, Localarmazenagem localarmazenagem)
	*
	* @param cdmaterial
	* @param localarmazenagem
	* @return
	* @since 09/09/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeMinimaMaterial(Integer cdmaterial, Localarmazenagem localarmazenagem){
		return materialDAO.getQtdeMinimaMaterial(cdmaterial, localarmazenagem);
	}
	
	/**
	 * M�todo que seta o atributo dos campos de dimens�es (altura,largura,comprimento)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void setAtributeDimensaomaterial(WebRequestContext request){
		request.setAttribute("DIMENSAOMATERIAL_1", parametrogeralService.getDimensaovenda(0));
		request.setAttribute("DIMENSAOMATERIAL_2", parametrogeralService.getDimensaovenda(1));
		request.setAttribute("DIMENSAOMATERIAL_3", parametrogeralService.getDimensaovenda(2));
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cdmaterial
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdmaterial) {
		materialDAO.atualizaW3producao(cdmaterial);
	}
	
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		String whereInMaterial = request.getParameter("whereInMaterial");
		List<MaterialLoteBean> lista = null;
		if(StringUtils.isNotEmpty(whereInMaterial)){
			lista = findForObrigatoriedadeLote(whereInMaterial);
		}
		return new JsonModelAndView().addObject("lista", lista);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForObrigatoriedadeLote(String whereInMaterial)
	*
	* @param whereInMaterial
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	public List<MaterialLoteBean> findForObrigatoriedadeLote(String whereInMaterial) {
		return materialDAO.findForObrigatoriedadeLote(whereInMaterial);
	}
	
	/**
	* M�todo que verifica se � obrigant�rio escolher lote para o material
	*
	* @param bean
	* @return
	* @since 28/12/2016
	* @author Luiz Fernando
	*/
	public boolean obrigarLote(Material bean){
		Material material = load(bean, "material.cdmaterial, material.obrigarlote");
		return material != null && material.getObrigarlote() != null && material.getObrigarlote(); 
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findForSagefiscal(String whereIn, boolean servico)
	*
	* @param whereIn
	* @param servico
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Material> findForSagefiscal(String whereIn) {
		return materialDAO.findForSagefiscal(whereIn);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialDAO#findByInfoContribuinte(Material material)
	*
	* @param material
	* @return
	* @since 31/01/2017
	* @author Mairon Cezar
	*/
	public Material findByInfoContribuinte(Material material){
		return materialDAO.findByInfoContribuinte(material);
	}

	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/03/2017
	 */
	public Material loadForIntegracaoECF(Material material) {
		return materialDAO.loadForIntegracaoECF(material);
	}
	
	/**
	* M�todo para adicionar atributos para a tela de cadastro de material 
	*
	* @param request
	* @param material
	* @since 31/03/2017
	* @author Luiz Fernando
	*/
	public void addInformacoesAtributoRequisicao(WebRequestContext request,	Material material) {
		Set<Pneumarca> pneumarcas = new HashSet<Pneumarca>();
		Map<Integer, List<Pneumodelo>> mapaModelos = new HashMap<Integer, List<Pneumodelo>>();
		if (material != null && Hibernate.isInitialized(material.getListaMaterialpneumodelo()) && SinedUtil.isListNotEmpty(material.getListaMaterialpneumodelo())) {
			Pneumodelo pneumodelo;
			for (Materialpneumodelo materialpneumodelo : material.getListaMaterialpneumodelo()) {
				if(materialpneumodelo.getPneumodelo() != null && materialpneumodelo.getPneumodelo() != null){
					if(materialpneumodelo.getPneumodelo().getPneumarca() != null && materialpneumodelo.getPneumodelo().getPneumarca().getCdpneumarca() != null){
						pneumarcas.add(materialpneumodelo.getPneumodelo().getPneumarca());
					}else {
						pneumodelo = pneumodeloService.load(materialpneumodelo.getPneumodelo(), "pneumodelo.cdpneumodelo, pneumodelo.pneumarca");
						if(pneumodelo != null && pneumodelo.getPneumarca() != null){
							pneumarcas.add(materialpneumodelo.getPneumodelo().getPneumarca());
						}
					}
				}
			}
		}
		if(pneumarcas != null){
			for (Pneumarca pneumarca : pneumarcas) {
				mapaModelos.put(pneumarca.getCdpneumarca(), pneumodeloService.findBy(pneumarca));
			}
		}
		request.setAttribute("mapaPneumodelo", mapaModelos);
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(Material material){
		ModelAndView retorno = new JsonModelAndView();
		if(material.getCdmaterial() == null){
			return retorno.addObject("registrarPesoMedio", false);
		}

		Material mat = materialService.load(material, "material.pesobruto, material.materialgrupo");
		
		if(mat != null && mat.getMaterialgrupo() != null && Boolean.TRUE.equals(mat.getMaterialgrupo().getRegistrarpesomedio())){
			retorno.addObject("registrarPesoMedio", true)
					.addObject("pesoBruto", mat.getPesobruto());
		}else{
			retorno.addObject("registrarPesoMedio", false);
		}
		return retorno;
	}
	
	public List<Material> findMaterialAutoCompleteByMaterialtipoEmpresa(String query){
		Empresa empresa = null;
		Materialtipo tipo = null;
		try{
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		}catch(Exception e){}
		try{
			tipo = new Materialtipo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("materialtipo")));
		}catch(Exception e){}
		return materialDAO.finMaterialAutoCompleteByMaterialtipoEmpresa(query, empresa, tipo);
	}
	
	public void updatePesoBruto(Material material, Double pesobruto) {
		materialDAO.updatePesoBruto(material, pesobruto);
	}
	
	public List<Material> findForRegitrarMovimentacaoAnimal(String whereIn){
		return materialDAO.findForRegitrarMovimentacaoAnimal(whereIn);
	}
	
	public List<TransferenciaCategoriaMaterialBean> findForTransferenciaCategoria(TransferenciaCategoriaFiltro filtro, String whereInMaterial, String whereInCodigobarras) {
		return materialDAO.findForTransferenciaCategoria(filtro, whereInMaterial, whereInCodigobarras);
	}
	
	public Double findEstoqueAtual(String cdmaterial, Localarmazenagem localarmazenagem, Empresa empresa) {
		return materialDAO.findEstoqueAtual(cdmaterial, localarmazenagem, empresa);
	}

	public Material findForReserva(Material material) {
		return materialDAO.findForReserva(material);
	}
	
	public List<Material> findMateriaisWithMaterialGrupoNotAnimal(String whereIn){
		return materialDAO.findMateriaisWithMaterialGrupoNotAnimal(whereIn);
	}
	
	public List<Material> findMateriaisWithMaterialGrupoAnimal(){
		return materialDAO.findMateriaisWithMaterialGrupoAnimal();
	}
	
	public void updateCategoriaMaterial(Materialcategoria materialcategoriaAntiga, Materialcategoria materialcategoriaNova){
		materialDAO.updateCategoriaMaterial(materialcategoriaAntiga, materialcategoriaNova);
	}
	
	public List<Material> findProdutoAtivoAutocomplete(String q) {
		return materialDAO.findProdutoAtivoAutocomplete(q,null);
	}

	public List<Material> findProdutoAcampanhaBandaAtivoAutocomplete(String q) {
		return materialDAO.findProdutoAtivoAutocomplete(q,true);
	}
	
	public List<Material> findMaterialEstoqueByCodigobarras(String codigobarras, Localarmazenagem localarmazenagem) {
		if(localarmazenagem != null && !localarmazenagemService.getPermitirestoquenegativo(localarmazenagem)){
			return materialDAO.findMaterialEstoqueByCodigobarras(codigobarras, 
					(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? localarmazenagem : null));
		} else {
			return materialDAO.findMaterialByCodigobarras(codigobarras, true, null);
		}
	}
	public Map<Integer, Material> carregarEstoqueDosMateriais(Map<Integer, Material> materiais, Empresa empresa) {
		List<Material> lst = materialDAO.carregarEstoqueDosMateriais(CollectionsUtil.concatenate(materiais.keySet(), ","), empresa);
		for(Material m: lst)
			materiais.get(m.getCdmaterial()).setMateriaisDisponiveisEmEstoque(m.getMateriaisDisponiveisEmEstoque());
		return materiais;
	}
	public Material loadMaterialWithMaterialgrupo(Material material) {
		return materialDAO.loadMaterialWithMaterialgrupo(material);
	}
	public Material loadMaterialWithMaterialtipo(Material material) {
		return materialDAO.loadMaterialWithMaterialtipoo(material);
	}
	public Material getUniqueMaterial(Material material, Materialtipo materialtipo, Empresa empresa) {
		return materialDAO.getUniqueMaterial(material, materialtipo, empresa);
	}
	
	public Boolean existMateriaprima(Material material) {
		return materialDAO.existMateriaprima(material);
	}
	
	public Material loadForUltimoValorVenda(Material material) {
		return materialDAO.loadForUltimoValorVenda(material);
	}
	public boolean isServico(Material material){
		Material bean = materialService.load(material, "material.servico");
		return bean != null && Boolean.TRUE.equals(bean.getServico());
	}
	
	public List<Material> findForRetornoContrato(String whereIn) {
		return materialDAO.findForRetornoContrato(whereIn);
	}
	
	public List<Material> findMaterialcoletaAtivoAutocomplete(String q){
		return materialDAO.findMaterialcoletaAutocomplete(q, Boolean.TRUE);
	}
	
	public Long getUltimoIdentificadorCdmaterial(){
		return materialDAO.getUltimoIdentificadorCdmaterial();
	}
	
	public Material getMaterialAcompanhaBanda(Material material, Material banda) {
		Material materialAcompanhaBanda = null;
		if(banda != null){
			Material mBanda = loadWithBanda(banda);
			if(mBanda != null && mBanda.getMaterialacompanhabanda() != null){
				materialAcompanhaBanda = mBanda.getMaterialacompanhabanda();
			}
		}
		if(materialAcompanhaBanda == null){
			Materialproducao materialproducao = materialproducaoService.findAcompanhabanda(material);
			if(materialproducao != null){
				materialAcompanhaBanda = materialproducao.getMaterial();
				materialAcompanhaBanda.setConsumoMaterialAcompanhaBanda(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 ? materialproducao.getConsumo() : null);
			}
		}
		return materialAcompanhaBanda;
	}
	
	public Material loadWithBanda(Material material) {
		return materialDAO.loadWithBanda(material);
	}
	
	public boolean isMaterialMestre(Material material) {
		return materialDAO.isMaterialMestre(material);
	}
	
	public boolean isMestreGrade(Material material) {
		return materialDAO.isMestreGrade(material);
	}
	
	public List<Material> findAutocompleteBanda(String q) {
		return materialDAO.findAtivosByNome(q);
	}
	
	public Material loadForMaterialRateioEstoque(Material material) {
		return materialDAO.loadForMaterialRateioEstoque(material);
	}
	
	public void preencherMaterialCustoPorEmpresa(Material material, Movimentacaoestoque movimentacaoestoque, Double quantidadeAnterior) {
		String acao = NeoWeb.getRequestContext().getParameter("ACAO");
		List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(material, movimentacaoestoque!=null?movimentacaoestoque.getEmpresa():null);
		if(SinedUtil.isListEmpty(listaMaterialCustoEmpresa) || movimentacaoestoque == null) {
			List<Empresa> listaEmpresas = empresaService.findAtivos();
			for(Empresa empresa : listaEmpresas){
				Boolean existeEmpresa = false;
				for(MaterialCustoEmpresa materialAux : listaMaterialCustoEmpresa){
					if(empresa.equals(materialAux.getEmpresa())){
						existeEmpresa= true;
						break;
					}
				}
				if(!existeEmpresa){
					MaterialCustoEmpresa aux = new MaterialCustoEmpresa();
					aux.setMaterial(material);
					aux.setEmpresa(empresa);
					aux.setCustoInicial(0D);
					aux.setQuantidadeReferencia(quantidadeAnterior);
					if(acao.equals("RecalcularCustoMedio")) {
						aux.setDtInicio(material.getDtcustoinicial());
						aux.setValorUltimaCompra(0D);
					} else {
						if(movimentacaoestoque==null){
							Movimentacaoestoque movimentacaoEstoqueAux = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(material, empresa);
							if(movimentacaoEstoqueAux!=null){
								aux.setDtInicio(movimentacaoEstoqueAux.getDtmovimentacao());
								aux.setValorUltimaCompra(movimentacaoEstoqueAux.getValor());
							}
						}else{
							aux.setDtInicio(movimentacaoestoque.getDtmovimentacao());
							aux.setValorUltimaCompra(movimentacaoestoque.getValor());							
						}
					}
					materialCustoEmpresaService.saveOrUpdate(aux);
				}
			}
		}
	}
	
	public void updateValorMaterialCustoEmpresa(Material material, Movimentacaoestoque movimentacaoestoque) {
		List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(material, null);
		
		if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)) {
			try {
				for(MaterialCustoEmpresa materialCustoPorEmpresa : listaMaterialCustoEmpresa) {
					if(materialCustoPorEmpresa.getEmpresa() != null){
						Double custoMedioEmpresa = getValorCustoMedio(material, movimentacaoestoque, materialCustoPorEmpresa);
						if(custoMedioEmpresa != null){
							materialCustoEmpresaService.updateValorCusto(materialCustoPorEmpresa, custoMedioEmpresa);
						}
					}
				}
				
				WebRequestContext request = NeoWeb.getRequestContext();
				String acao = request.getParameter("ACAO");
				
				if(!acao.equals("salvar")) {
					request.clearMessages();
					request.addMessage("O valor de custo/custo por empresa dos materiais selecionados foram atualizados.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				NeoWeb.getRequestContext().addError("Erro ao calcular custo m�dio: " + e.getMessage());
			}
		}
	}
	
	public void criarHistoricoRecalculoCustoMedio(String whereIn) {
		List<Material> listaComCustoPorEmpresa = findMaterialWithCustoPorEmpresa(whereIn);
		
		if(SinedUtil.isListNotEmpty(listaComCustoPorEmpresa)) {
			for(Material material : listaComCustoPorEmpresa) {
				Materialhistorico historico = new Materialhistorico();
				
				historico.setMaterial(material);
				historico.setAcao(Materialacao.RECALCULADO_CUSTO_MEDIO);
				historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				
				SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
				StringBuilder observacao = new StringBuilder();
				
				observacao.append("Custo do Material: Data de refer�ncia " + pattern.format(historico.getDtaltera()) + ", Valor de Custo: " + (material.getValorcusto() != null ? new Money(material.getValorcusto()) : "custo n�o informado"));
				for(MaterialCustoEmpresa m : material.getListaMaterialCustoEmpresa()) {
					observacao.append("\n ");
					observacao.append("Custo por Empresa: Empresa: " + (m.getEmpresa() != null ? m.getEmpresa().getNomefantasia() : "") + "," +
							" Data de referencia: " + pattern.format(historico.getDtaltera()) + "," +
							" Valor de Custo: " + (m.getCustoMedio() != null ? new Money(m.getCustoMedio()) : ""));
				}
				
				historico.setObservacao(observacao.toString());
				materialhistoricoService.saveOrUpdate(historico);
			}
		}
	}
	
	public List<Material> findMaterialWithCustoPorEmpresa(String whereIn) {
		return materialDAO.findMaterialWithCustoPorEmpresa(whereIn);
	}
	
	public void atualizarAbaMaterialCustoEmpresa(Material material) {
		if(SinedUtil.isListNotEmpty(material.getListaMaterialempresa())) {
			if(SinedUtil.isListEmpty(material.getListaMaterialCustoEmpresa())){
				material.setListaMaterialCustoEmpresa(new ListSet<MaterialCustoEmpresa>(MaterialCustoEmpresa.class));
			}
			for(Materialempresa me : material.getListaMaterialempresa()) {
				if(!isEmpresaCadastradaNaAbaCustoPorEmpresa(me.getEmpresa(), material)) {
					MaterialCustoEmpresa mce = new MaterialCustoEmpresa();
					mce.setEmpresa(me.getEmpresa());
					mce.setCustoInicial(material.getValorcustoinicial());
					mce.setDtInicio(material.getDtcustoinicial());
					material.getListaMaterialCustoEmpresa().add(mce);
				}
			}
		}
	}
	
	public Boolean isEmpresaCadastradaNaAbaCustoPorEmpresa(Empresa empresa, Material bean) {
		for(MaterialCustoEmpresa mce : bean.getListaMaterialCustoEmpresa()) {
			if(mce.getEmpresa().equals(empresa)) {
				return true;
			}
		}
		return false;
	}
	
	public void registrarHistoricoMaterial(String whereIn, Materialacao acao, String observacao) {
		if(!StringUtils.isBlank(whereIn)) {
			for(String id : whereIn.split(",")) {
				Materialhistorico historico = new Materialhistorico();
				historico.setMaterial(new Material(Integer.parseInt(id)));
				historico.setAcao(acao);
				historico.setObservacao(observacao);
				historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				materialhistoricoService.saveOrUpdate(historico);
			}
		}
	}
	
	public Double getValorCustoAtual(Material material, Unidademedida unidadeMedida, Empresa empresa){
		Double valorCusto = 0D;
		boolean usaFaixaMarkup = false;
		if(material != null && material.getCdmaterial() != null){
			List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = null;
			if(parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_CUSTO_POR_EMPRESA) && empresa != null && empresa.getCdpessoa() != null){
				listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(material, empresa);
			}
			Material bean = materialService.load(material, "material.cdmaterial, material.valorcusto, material.cadastrarFaixaMarkup");
			if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)){
				valorCusto = listaMaterialCustoEmpresa.get(0).getCustoMedio();
			}else{
				valorCusto = bean.getValorcusto();
			}
			usaFaixaMarkup = Boolean.TRUE.equals(bean.getCadastrarFaixaMarkup());
		}
		if(usaFaixaMarkup){
			Materialunidademedida materialunidademedida = materialunidademedidaService.getMaterialunidademedida(material, unidadeMedida);
			if(Util.objects.isPersistent(materialunidademedida) && materialunidademedida.getFracaoQtdereferencia() > 0){
				valorCusto = valorCusto / materialunidademedida.getFracaoQtdereferencia();
			}
		}
		return valorCusto;
	}
	
	public List<Material> buscarMaterialComFaixaMarkup(String whereIn) {
		return materialDAO.buscarMaterialComFaixaMarkup(whereIn);
	}
	
	public List<MaterialHistoricoPrecoBean> findHistoricoPrecos(MaterialHistoricoPrecoFiltro filtro){
		return materialDAO.findHistoricoPrecos(filtro);
	}
	
	public ModelAndView ajaxAbrePopupHistoricoPrecoMaterial(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		if(Util.objects.isPersistent(filtro.getMaterial())){
			filtro.setMaterial(materialService.load(filtro.getMaterial(), "material.cdmaterial, material.nome"));
		}
		return new ModelAndView("direct:crud/popup/popupHistoricoPrecoMaterial")
					.addObject("filtro", filtro)
					.addObject("lista", this.findHistoricoPrecos(filtro));
	}
	
	public List<MaterialHistoricoPrecoBean> getListaHistoricoPrecoMaterial(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		if(Util.objects.isPersistent(filtro.getMaterial())){
			filtro.setMaterial(materialService.load(filtro.getMaterial(), "material.cdmaterial, material.nome"));
		}
		return this.findHistoricoPrecos(filtro);
	}
	
	public ModelAndView ajaxMaterialProducaoOrKitOrGrade(WebRequestContext request, Material material){
		Material bean = this.load(material, "material.cdmaterial, material.vendapromocional, material.kitflexivel, material.producao");
		Material bean2 = this.loadWithMaterialMestre(bean);
		
		return new JsonModelAndView()
				.addObject("isMaterialProducao", Boolean.TRUE.equals(bean.getProducao()))
				.addObject("isKitFlexivel", Boolean.TRUE.equals(bean.getKitflexivel()))
				.addObject("isKit", Boolean.TRUE.equals(bean.getVendapromocional()))
				.addObject("isMestreGrade", this.isMaterialMestre(bean))
				.addObject("isItemGrade", bean2.getMaterialmestregrade() != null && bean2.getMaterialmestregrade().getCdmaterial() != null);
	}
	
	public void criarContabilEmpresa() {
		try {
			if(parametrogeralService.getBoolean(Parametrogeral.CRIAR_CONTABIL_EMPRESA_MATERIAL)){
				
				List<Integer> listaCdmaterial = materialDAO.findForCriarContabilEmpresaMaterial();
				
				if(SinedUtil.isListNotEmpty(listaCdmaterial)){
					for(Integer cdmaterial : listaCdmaterial){
						Material material = materialDAO.loadForCriarContabilEmpresa(new Material(cdmaterial));
						if(material != null){
							List<Empresa> listaEmpresa = empresaService.findForCriarContabilEmpresa(material);
							if(SinedUtil.isListNotEmpty(listaEmpresa)){
								for(Empresa empresa : listaEmpresa){
									MaterialCustoEmpresa materialCustoEmpresa = new MaterialCustoEmpresa();
									materialCustoEmpresa.setMaterial(material);
									materialCustoEmpresa.setContaContabil(material.getContaContabil());
									materialCustoEmpresa.setEmpresa(empresa);
									materialCustoEmpresa.setCustoInicial(null);
									materialCustoEmpresa.setQuantidadeReferencia(null);
									
									Movimentacaoestoque movimentacaoestoquePorEmpresa = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(material, materialCustoEmpresa.getEmpresa());
									if(movimentacaoestoquePorEmpresa != null){
										materialCustoEmpresa.setDtInicio(movimentacaoestoquePorEmpresa.getDtmovimentacao());
										materialCustoEmpresa.setValorUltimaCompra(movimentacaoestoquePorEmpresa.getValor());
									}
									materialCustoEmpresaService.saveOrUpdate(materialCustoEmpresa);
									
									if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA)){
										
										if(movimentacaoestoquePorEmpresa != null 
												&& movimentacaoestoquePorEmpresa.getValor() != null 
												&& movimentacaoestoquePorEmpresa.getMaterial() != null) {
											materialCustoEmpresaService.updateValorUltimaCompra(materialCustoEmpresa, movimentacaoestoquePorEmpresa.getValor());
											materialCustoEmpresaService.updateValorCusto(materialCustoEmpresa, movimentacaoestoquePorEmpresa.getValor());
										}
									} else {
										materialService.recalcularCustoMedio(material.getCdmaterial().toString(), null);
										if(movimentacaoestoquePorEmpresa != null){
											materialCustoEmpresaService.updateValorUltimaCompra(materialCustoEmpresa, movimentacaoestoquePorEmpresa.getValor());
										}
									}
								}
							}
						}
						
					}
				}
				
				parametrogeralService.updateValorPorNome(Parametrogeral.CRIAR_CONTABIL_EMPRESA_MATERIAL, "FALSE");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean isMaterialProducao(Material bean){
		Material material = this.load(bean, "material.cdmaterial, material.producao");
		return Util.objects.isPersistent(material) && Boolean.TRUE.equals(material.getProducao());
	}

	public boolean existMaterialPadraoParaMestreGrade(Material materialmestregrade, Material material) {
		return materialDAO.existMaterialPadraoParaMestreGrade(materialmestregrade, material);
	}
	
	public Material findItemGradePadrao(Material material) {
		return materialDAO.findItemGradePadrao(material);

	}
	public Material loadMaterialWithUnidadeMedida(Material material){
		return materialDAO.loadMaterialWithUnidadeMedida(material);
	}
	
	public boolean validateObrigarLote(ValidateLoteestoqueObrigatorioInterface bean){
		return validateObrigarLote(bean, null, null, null);
	}
	
	public boolean validateObrigarLote(ValidateLoteestoqueObrigatorioInterface bean, BindException errors){
		return validateObrigarLote(bean, errors, null, null);
	}
	
	public boolean validateObrigarLote(ValidateLoteestoqueObrigatorioInterface bean, WebRequestContext request){
		return validateObrigarLote(bean, null, request, null);
	}
	
	public boolean validateObrigarLote(ValidateLoteestoqueObrigatorioInterface bean, BindException errors, WebRequestContext request, Set<Entregamateriallote> listaEntregamateriallote){
		boolean isObrigaLote = this.obrigarLote(bean.getMaterial());
		if(isObrigaLote && Util.objects.isNotPersistent(bean.getLoteestoque())){
			boolean erro = true;
			
			if(SinedUtil.isListNotEmptyAndInitialized(listaEntregamateriallote)){
				erro = false;
				for(Entregamateriallote entregamateriallote : listaEntregamateriallote){
					if(Util.objects.isNotPersistent(entregamateriallote.getLoteestoque())){
						erro = true;
						break;
					}
				}
			}
			
			if(erro){
				Material material = materialService.load(bean.getMaterial(), "material.cdmaterial, material.nome");
				if(errors != null){
					errors.reject("001", "Favor informar o lote do item: "+material.getNome()+".");
				}else if(request != null){
		            request.addError("Favor informar o lote do item: "+material.getNome()+".");
				}else {
					throw new SinedException("Favor informar o lote do item: "+material.getNome()+".");
				}
				return false;
			}
		}
		return true;
	}
	
	public boolean validateLoteSemValidade(ValidateLoteestoqueObrigatorioInterface bean, BindException errors){
		boolean isObrigaLote = this.obrigarLote(bean.getMaterial());
		if(isObrigaLote && Util.objects.isPersistent(bean.getLoteestoque())){
			Loteestoque le = loteestoqueService.loadWithNumeroValidade(bean.getLoteestoque(), bean.getMaterial(), false);
			if(le.getValidade() == null){
				errors.reject("001", "O lote "+le.getNumero()+" n�o possui data de validade. Favor preencher a data de validade no lote.");
				return false;
			}
			
		}
		return true;
	}

	public Boolean isLoteObrigatorio(Material material) {
		return materialDAO.isLoteObrigatorio(material);
	}

	public List<Material> findMateriaisFabricante(Fornecedor bean) {
		return materialDAO.findMateriaisFabricante(bean);
	}
	public List<Material> findMateriaisFornecedor(Fornecedor bean) {
		return materialDAO.findMateriaisFornecedor(bean);
	}
	public boolean existsMaterialWithModeloDisponibilidade(String whereIn) {
		return materialDAO.existsMaterialWithModeloDisponibilidade(whereIn);
	}
	public Material loadByIdEcommerce(Integer idEcommerce){
		Material material = materialDAO.loadByIdEcommerce(idEcommerce);
		if(material == null){
			MaterialEcomerceExcluido materialEcomerceExcluido = materialEcomerceExcluidoService.loadByIdEcommerce(idEcommerce);
			if(materialEcomerceExcluido != null){
				material = materialEcomerceExcluido.getMaterial();
			}
		}
		return material;
	}
	public List<Material> loadWithIdEcommerce(String whereIn) {
		return materialDAO.loadWithIdEcommerce(whereIn);
	}
	public void insertMaterialTabelaSincronizacaoEcommerce(Material material) {
		materialDAO.insertMaterialTabelaSincronizacaoEcommerce(material);
	}
    public Integer prazoGarantiaConvertido(Integer prazoGarantia){
        if(prazoGarantia == null){
            return null;
        }
        if(parametrogeralService.getBoolean(Parametrogeral.GARANTIA_MATERIAL_MESES)){
            return prazoGarantia * 30;
        }
        return prazoGarantia;
    }

    public List<ConferenciaMateriaisVendaBean> findDuplicadosByCodigoBarras(String codigoBarras, String whereInMaterial){
    	return materialDAO.findDuplicadosByCodigoBarras(codigoBarras, whereInMaterial);
    }
    
    public Boolean existeAtributo (AtributosMaterial atributoMaterial, Material material, String valor){
    	return materialDAO.existeAtributo(atributoMaterial, material, valor);
    }
    
    public Material buscaAtributosMaterial (Material bean){
    	if(Util.objects.isNotPersistent(bean)){
    		return null;
    	}
    	return materialDAO.buscaAtributosMaterial(bean);
    }
    
    public void setaAtributosMestreTrayCorp(Material bean){
    	if(Util.objects.isPersistent(bean.getMaterialmestregrade())){
    		Material materialMestre = this.load(bean.getMaterialmestregrade(), "material.cdmaterial, material.marketplace, material.somenteParceiros, material.exibirMatrizAtributos, " +
    																			"material.idPaiExterno, material.contraProposta, material.urlVideo, material.maisInformacoes");
    		bean.setIdPaiExterno(materialMestre.getIdPaiExterno());
    		bean.setMarketplace(materialMestre.getMarketplace());
    		bean.setContraProposta(Boolean.TRUE.equals(materialMestre.getContraProposta()));
    		bean.setSomenteParceiros(materialMestre.getSomenteParceiros());
    		bean.setExibirMatrizAtributos(materialMestre.getExibirMatrizAtributos());
    		bean.setUrlVideo(materialMestre.getUrlVideo());
    		bean.setMaisInformacoes(materialMestre.getMaisInformacoes());
    	}
    }
    
    public void updateToForceTriggers(Material materialMestre){
    	materialDAO.updateToForceTriggers(materialMestre);
    }
    
    public Integer getNextIdPaiExterno(){
    	return materialDAO.generateNextSequence("sequence_idpaiexterno_traycorp");
    }
    
    public String getNextIdVinculoExterno(){
    	return "VINCULO_EXT_"+materialDAO.generateNextSequence("sequence_idvinculoexterno_traycorp");
    }
    
    public void updateIdPaiExternoItensDeGrade(Material materialMestre){
    	materialDAO.updateIdPaiExternoItensDeGrade(materialMestre);
    }
    
    public Material findForValidarTiketMedio(Material material) {
    	return materialDAO.findForValidarTiketMedio(material);
    }
    
    public boolean verificaRestricaoFornecedorColaborador(Material material, Fornecedor fornecedor){
    	return materialDAO.verificaRestricaoFornecedorColaborador(material, fornecedor);
    }
    
    public List<Material> translateVendaMaterialToMaterial(List<Vendamaterial> listaVm){
    	List<Material> lista = new ArrayList<Material>();
    	for(Vendamaterial vm: listaVm){
    		lista.add(vm.getMaterial());
    	}
    	return lista;
    }
    
    public List<Material> translatePedidoVendaMaterialToMaterial(List<Pedidovendamaterial> listaVm){
    	List<Material> lista = new ArrayList<Material>();
    	for(Pedidovendamaterial vm: listaVm){
    		lista.add(vm.getMaterial());
    	}
    	return lista;
    }
    
    public List<Material> translateVendaOrcamentoMaterialToMaterial(List<Vendaorcamentomaterial> listaVm){
    	List<Material> lista = new ArrayList<Material>();
    	for(Vendaorcamentomaterial vm: listaVm){
    		lista.add(vm.getMaterial());
    	}
    	return lista;
    }
    
    public boolean validateDadosForCalculoTicketMedio(List<Material> listaMaterial, BindException errors){
    	List<String> listaErros = this.haveCalculoTicketMedio(listaMaterial);
    	if(SinedUtil.isListNotEmpty(listaErros)){
    		//Dispara erro
    		for(String erro: listaErros){
    			errors.reject("001", erro);
    		}
    		return false;
    	}
    	return true;
    }
    
    public boolean validateDadosForCalculoTicketMedio(List<Material> listaMaterial, WebRequestContext request){
    	List<String> listaErros = this.haveCalculoTicketMedio(listaMaterial);
    	if(SinedUtil.isListNotEmpty(listaErros)){
    		//Dispara erro
    		for(String erro: listaErros){
    			request.addError(erro);
    		}
    		return false;
    	}
    	return true;
    }
 
    public List<String> haveCalculoTicketMedio(List<Material> listaMaterial){
    	StringBuilder messageProduto = new StringBuilder();
		StringBuilder messageFornecedor = new StringBuilder();
		StringBuilder messageSemFornecedor = new StringBuilder();
		int indexProduto = 0;
		int indexFornecedor = 0;
		int indexSemFornecedor = 0;
		
		List<String> listaErros = new ArrayList<String>();
		
		for(Material mat: listaMaterial){
			Material material = materialService.findForValidarTiketMedio(mat);
			// Valida��o de produto sem peso bruto.
			if(material.getPesobruto() == null || material.getPesobruto() == 0){
				if(indexProduto == 0)
					messageProduto.append("N�o foi poss�vel calcular o ticket m�dio por KG. O(s) Produto(s) ");							
				if(indexProduto > 1)
					messageProduto.append(", ");
				messageProduto.append("<b style='color: #fff'>" + material.getIdentificacao() + "</b>");
				indexProduto++;
			}
			
			if(material.getListaFornecedor().size() > 0){
				for(Materialfornecedor matFornecedor: material.getListaFornecedor()){
					if(matFornecedor.getFornecedor().getValorTicketMedio() == null){
						if(indexFornecedor == 0)
							messageFornecedor.append("N�o foi poss�vel calcular o ticket m�dio por KG. O(s) fornecedor(es) ");							
						if(indexFornecedor > 1)
							messageFornecedor.append(", ");
						messageFornecedor.append("<b style='color: #fff'>" + matFornecedor.getFornecedor().getNome()+ "</b>");
					}
					indexFornecedor++;
				}						
			} else {
				if(indexSemFornecedor == 0)
					messageSemFornecedor.append("N�o foi poss�vel calcular o ticket m�dio por KG. O(s) Produto(s) ");
					if(indexSemFornecedor > 1)
						messageSemFornecedor.append(", ");
					messageSemFornecedor.append("<b style='color: #fff'>" + material.getIdentificacao() + "</b>");
				
			}
		}							
		
		if(!messageProduto.toString().equals("")){
			messageProduto.append(" n�o possui(em) peso bruto.");
			listaErros.add(messageProduto.toString());
		}
		if(!messageFornecedor.toString().equals("")){
			messageFornecedor.append(" n�o possui(em) valor de ticket m�dio por KG.");
			listaErros.add(messageFornecedor.toString());
		}
		if(!messageSemFornecedor.toString().equals("")){
			messageSemFornecedor.append(" n�o possui(em) fornecedor.");
			listaErros.add(messageSemFornecedor.toString());
		}
		return listaErros;
    }
	
	public List<Material> findAutocompleteProcessoReferenciado(String nome){
		return materialDAO.findAutocompleteProcessoReferenciado(nome, SinedUtil.getNotaFiscalProdutoParametroRequisicaoAutocomplete(), SinedUtil.getEntradaFiscalParametroRequisicaoAutocomplete());
	}
}