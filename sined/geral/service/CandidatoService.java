package br.com.linkcom.sined.geral.service;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;

import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Candidato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.dao.CandidatoDAO;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CandidatoService extends GenericService<Candidato>{
	
	protected CandidatoDAO candidatoDAO;
	protected EmpresaService empresaService;
	protected EnvioemailService envioemailService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCandidatoDAO(CandidatoDAO candidatoDAO) {
		this.candidatoDAO = candidatoDAO;
	}
	
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	public Candidato carregaCandidatoContratado(Candidato candidato){
		return candidatoDAO.carregaCandidatoContratado(candidato);
	}
	
	
	public Candidato carregaCandidatoFromCpf(Cpf candidatoCpf){
		return candidatoDAO.carregaCandidatoFromCpf(candidatoCpf);
	}
	
	
	public void enviaEmailRecuperarSenha(Candidato candidato)throws Exception {
		if(candidato == null) throw new SinedException("O candidato n�o deve ser null");
		else if(candidato.getEmail() == null) throw new SinedException("O campo E-mail n�o pode ser null");
		
		this.gerarNovaSenhaUsuario(candidato);
		
		Empresa empresa = empresaService.loadPrincipal();
		
		String template = null;
		try{
			template = new TemplateManager("/WEB-INF/template/templateEnvioEmailCurriculo.tpl")
				.assign("nome", candidato.getNome())
				.assign("email", candidato.getEmail())
				.assign("senha", candidato.getSenha())
				.getTemplate();
		} catch (IOException e) {
			throw new SinedException("Erro no processamento do template.",e);
		}

		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			try {
				String assunto = "Envio de senha do Cadastro de Curr�culo";
				
				Envioemail envioemail = envioemailService.registrarEnvio(
						empresa.getEmail(),
						assunto,
						template,
						Envioemailtipo.ENVIO_SENHA_CADASTRO_CURRICULO,
						empresa,
						candidato.getEmail(), 
						candidato.getNome(),
						email);
				
				email
					.setFrom(empresa.getEmail())
					.setSubject(assunto)
					.setTo(candidato.getEmail())
					.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, candidato.getEmail()))
					.sendMessage();
				super.log.info("E-mail de recupera��o de senha enviado para <"+candidato.getEmail()+">");
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail.",e);
			}
	}
	
	public void gerarNovaSenhaUsuario(Candidato candidato){
		String senha = RandomStringUtils.randomAlphanumeric(6);
		candidato.setSenha(senha);
	}
	
	public void alterarSenhaCandidato(Candidato candidato){
		candidatoDAO.alterarSenhaCandidato(candidato);
	}
	
}
