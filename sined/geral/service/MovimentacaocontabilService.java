package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContaCompensacaoEnum;
import br.com.linkcom.sined.geral.dao.MovimentacaocontabilDAO;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemLancamentosBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.GerarDREFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirLivroRazaoBean;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirLivroRazaoItem;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirBalancoPatrimonialFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirLivroRazaoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MovimentacaocontabilFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaocontabilService extends GenericService<Movimentacaocontabil>{
	
	private MovimentacaocontabilDAO movimentacaocontabilDAO;
	private MovimentacaocontabildebitocreditoService movimentacaocontabildebitocreditoService;
	private ContacontabilService contaContabilService;

	public void setMovimentacaocontabilDAO(MovimentacaocontabilDAO movimentacaocontabilDAO) {this.movimentacaocontabilDAO = movimentacaocontabilDAO;}
	public void setMovimentacaocontabildebitocreditoService(MovimentacaocontabildebitocreditoService movimentacaocontabildebitocreditoService) {this.movimentacaocontabildebitocreditoService = movimentacaocontabildebitocreditoService;}
	public void setContacontabilService(ContacontabilService contaContabilService) {this.contaContabilService = contaContabilService;}
	
	public List<Movimentacaocontabil> findForCsv(MovimentacaocontabilFiltro filtro) {
		return movimentacaocontabilDAO.findForCsv(filtro);
	}
	
	public List<Movimentacaocontabil> findForListagem(String whereIn) {
		return movimentacaocontabilDAO.findForListagem(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaocontabilDAO#findForDominiointegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaocontabil> findForDominiointegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim) {
		return movimentacaocontabilDAO.findForDominiointegracaoLancamentocontabil(empresa, dtinicio, dtfim);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaocontabilDAO#findForALteradataintegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaocontabil> findForALteradataintegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim) {
		List<Movimentacaocontabil> lista = movimentacaocontabilDAO.findForALteradataintegracaoLancamentocontabil(empresa, dtinicio, dtfim);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Movimentacaocontabil mc : lista){
				mc.setListaDebito(movimentacaocontabildebitocreditoService.findWithIdentificadorByMovimentacaocontabil(mc, "movimentacaocontabildebito"));
				mc.setListaCredito(movimentacaocontabildebitocreditoService.findWithIdentificadorByMovimentacaocontabil(mc, "movimentacaocontabilcredito"));
			}
		}
		return lista;
	}
	
	public void transactionGerarLancamentoContabeis(List<Movimentacaocontabil> listaMovimentacaocontabil){
		movimentacaocontabilDAO.transactionGerarLancamentoContabeis(listaMovimentacaocontabil);
	}
	
	public List<Movimentacaocontabil> findForSAGE(MovimentacaocontabilFiltro filtro) {
		return movimentacaocontabilDAO.findForSAGE(filtro);
	}
	
	public Money findForTotaisValor(MovimentacaocontabilFiltro filtro) {
		return movimentacaocontabilDAO.findForResumoDocumentotipo(filtro);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @param configuracaodretipo
	 * @param configuracaodre
	 * @return
	 * @since 13/12/2019
	 * @author Rodrigo Freitas
	 */
	public Double getSumValorForDRE(Empresa empresa, Date dtinicio, Date dtfim, Configuracaodretipo configuracaodretipo, Configuracaodre configuracaodre, Centrocusto centrocusto, Projeto projeto) {
		return movimentacaocontabilDAO.getSumValorForDRE(empresa, dtinicio, dtfim, configuracaodretipo, configuracaodre, centrocusto, projeto);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @since 18/12/2019
	 * @author Rodrigo Freitas
	 */
	public List<GerarDREListagemLancamentosBean> findForDRE(GerarDREFiltro filtro) {
		return movimentacaocontabilDAO.findForDRE(filtro);
	}
	
	public List<Movimentacaocontabil> findForRelatorioLivroRazao(EmitirLivroRazaoFiltro filtro) {
		return movimentacaocontabilDAO.findForRelatorioLivroRazao(filtro);
	}

public IReport gerarRelatorioLivroRazao(EmitirLivroRazaoFiltro filtro) {
		
		List<Movimentacaocontabil> listaMovimentacaoContabil = this.findForRelatorioLivroRazao(filtro);
		HashMap<Integer, EmitirLivroRazaoBean> mapaPorContaGerencial =  new HashMap<Integer, EmitirLivroRazaoBean>();

		if (SinedUtil.isListNotEmpty(listaMovimentacaoContabil)) {
			for (Movimentacaocontabil movimentacaocontabil : listaMovimentacaoContabil) {
				
				//lista debito
				for (Movimentacaocontabildebitocredito mov : movimentacaocontabil.getListaDebito()) {
					
					EmitirLivroRazaoBean livroRazaoBean =  new EmitirLivroRazaoBean();
					//Se o mapa nao contem esse objeto, cria, acha o saldo aneterior e coloca no mapa 
					if(!mapaPorContaGerencial.containsKey(mov.getContaContabil().getCdcontacontabil())){
						livroRazaoBean.setContaContabil(mov.getContaContabil());
						livroRazaoBean.setIdentificadorNome(livroRazaoBean.getIdentificadorNome());
						Money saldoAnterior = new Money();
						if(filtro.getDtInicio() != null){
							if(livroRazaoBean.getContaContabil().getDtSaldoInicial() != null && filtro.getDtInicio().equals(livroRazaoBean.getContaContabil().getDtSaldoInicial())){
								saldoAnterior = livroRazaoBean.getContaContabil().getSaldoInicial();
							}else{
								saldoAnterior = calcularSaldoContaContabil(livroRazaoBean.getContaContabil().getCdcontacontabil(), SinedDateUtils.addDiasData(filtro.getDtInicio(), -1), filtro.getEmpresa(), filtro.getCentroCusto(), filtro.getProjeto());								
							}
						}
						livroRazaoBean.setSaldoInicial(saldoAnterior);
						livroRazaoBean.setSaldoInicialString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));
						livroRazaoBean.setSaldoAtual(saldoAnterior);
						livroRazaoBean.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));
						mapaPorContaGerencial.put(livroRazaoBean.getContaContabil().getCdcontacontabil(), livroRazaoBean);
					} else {
						livroRazaoBean = mapaPorContaGerencial.get(mov.getContaContabil().getCdcontacontabil());
					}
					
					List<EmitirLivroRazaoItem> listaLivroRazaoItens = new ArrayList<EmitirLivroRazaoItem>();
					EmitirLivroRazaoItem livroRazaoItem = new EmitirLivroRazaoItem();
					livroRazaoItem.setCdmovimentacaocontabil(movimentacaocontabil.getCdmovimentacaocontabil());					
					
					StringBuilder nomes = new StringBuilder();
					
					//percorre a lista de credito para buscar as contrapartidas
					for (Movimentacaocontabildebitocredito contrapartida : movimentacaocontabil.getListaCredito()) {
						if(nomes.indexOf(contrapartida.getContaContabil().getVcontacontabil().getIdentificador()) == -1){
							nomes.append(contrapartida.getContaContabil().getVcontacontabil().getIdentificador()+"\n");
						}
					}
					
					if(movimentacaocontabil.getListaDebito().size() == 1 && movimentacaocontabil.getListaCredito().size() > 1){
						livroRazaoItem.setContraPartidas("");
					}else{
						livroRazaoItem.setContraPartidas(nomes.toString());						
					}
					
					livroRazaoItem.setData(movimentacaocontabil.getDtlancamento());
					if(movimentacaocontabil.getNumero() != null){
						livroRazaoItem.setDocumento(movimentacaocontabil.getNumero());
					}
					if(movimentacaocontabil.getHistorico() != null){
						livroRazaoItem.setHistorico(movimentacaocontabil.getHistorico());
					}
					if(mov.getValor() != null){
						livroRazaoItem.setDebito(mov.getValor());													
					}
												
					//saldo
					livroRazaoItem.setSaldo(livroRazaoBean.getSaldoInicial());
					Money atualizaSaldo = new Money();
					//se for conta CREDORA: debito diminui 
					if(NaturezaContaCompensacaoEnum.CREDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().subtract(mov.getValor());								
					//se for conta DEVEDORA: debito aumenta
					}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().add(mov.getValor());
					//se for conta CREDORA RETIFICADORA: debito aumenta
					}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().add(mov.getValor());
					//se for conta DEVEDORA RETIFICADORA: debito diminui
					}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().subtract(mov.getValor());
					}
					
					livroRazaoItem.setSaldoAtual(atualizaSaldo);
					livroRazaoItem.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(atualizaSaldo));
					livroRazaoBean.setSaldoAtual(atualizaSaldo);
					livroRazaoBean.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(atualizaSaldo));
					//end saldo							
					
					//Total creditos e debitos
					Money totalDebitos = new Money();
					if(livroRazaoBean.getTotalDebitos() != null){
						totalDebitos = livroRazaoBean.getTotalDebitos().add(mov.getValor());
					}else {
						totalDebitos = mov.getValor();
					}
					livroRazaoBean.setTotalDebitos(totalDebitos);
					livroRazaoBean.setTotalDebitosString(SinedUtil.converteValorNegativoRelatorio(totalDebitos));
					livroRazaoItem.setTotalDebitos(totalDebitos);
					livroRazaoItem.setTotalDebitosString(SinedUtil.converteValorNegativoRelatorio(totalDebitos));
					//end total creditos e debitos
					
					if(mov.getCentrocusto() != null){
						livroRazaoItem.setCentroCusto(mov.getCentrocusto());
					}
					
					if(mov.getProjeto() != null){
						livroRazaoItem.setProjeto(mov.getProjeto());
					}
					
					boolean centroCusto = false;
					boolean projeto = false;
					
					if(filtro.getCentroCusto() == null || (livroRazaoItem.getCentroCusto() != null && filtro.getCentroCusto().getCdcentrocusto().equals(livroRazaoItem.getCentroCusto().getCdcentrocusto()))){
						centroCusto = true;
					}
					if(filtro.getProjeto() == null || (livroRazaoItem.getProjeto() != null && filtro.getProjeto().getCdprojeto().equals(livroRazaoItem.getProjeto().getCdprojeto()))){
						projeto = true;
					}
					
					if(centroCusto && projeto){
						listaLivroRazaoItens.add(livroRazaoItem);					
					}

					if(livroRazaoBean.getListaMovimentacoes() == null){
						livroRazaoBean.setListaMovimentacoes(listaLivroRazaoItens);							
					}else {
						livroRazaoBean.getListaMovimentacoes().addAll(listaLivroRazaoItens);
					}	
				}
				
				//lista credito
				for (Movimentacaocontabildebitocredito mov : movimentacaocontabil.getListaCredito()) {
					
					EmitirLivroRazaoBean livroRazaoBean =  new EmitirLivroRazaoBean();
					//Se o mapa nao contem esse objeto, cria, acha o saldo aneterior e coloca no mapa 
					if(!mapaPorContaGerencial.containsKey(mov.getContaContabil().getCdcontacontabil())){
						livroRazaoBean.setContaContabil(mov.getContaContabil());
						livroRazaoBean.setIdentificadorNome(livroRazaoBean.getIdentificadorNome());
						Money saldoAnterior = new Money();
						if(filtro.getDtInicio() != null){
							if(livroRazaoBean.getContaContabil().getDtSaldoInicial() != null && filtro.getDtInicio().equals(livroRazaoBean.getContaContabil().getDtSaldoInicial())){
								saldoAnterior = livroRazaoBean.getContaContabil().getSaldoInicial();
							}else{
								saldoAnterior = calcularSaldoContaContabil(livroRazaoBean.getContaContabil().getCdcontacontabil(), SinedDateUtils.addDiasData(filtro.getDtInicio(), -1), filtro.getEmpresa(), filtro.getCentroCusto(), filtro.getProjeto());								
							}
						}
						livroRazaoBean.setSaldoInicial(saldoAnterior);
						livroRazaoBean.setSaldoInicialString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));						
						livroRazaoBean.setSaldoAtual(saldoAnterior);
						livroRazaoBean.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));
						mapaPorContaGerencial.put(livroRazaoBean.getContaContabil().getCdcontacontabil(), livroRazaoBean);
					} else {
						livroRazaoBean = mapaPorContaGerencial.get(mov.getContaContabil().getCdcontacontabil());
					}
					
					List<EmitirLivroRazaoItem> listaLivroRazaoItens = new ArrayList<EmitirLivroRazaoItem>();
					EmitirLivroRazaoItem livroRazaoItem = new EmitirLivroRazaoItem();
					livroRazaoItem.setCdmovimentacaocontabil(movimentacaocontabil.getCdmovimentacaocontabil());					
					
					StringBuilder nomes = new StringBuilder();
					
					//percorre a lista de debito para buscar as contrapartidas
					for (Movimentacaocontabildebitocredito contrapartida : movimentacaocontabil.getListaDebito()) {
						if(nomes.indexOf(contrapartida.getContaContabil().getVcontacontabil().getIdentificador()) == -1){
							nomes.append(contrapartida.getContaContabil().getVcontacontabil().getIdentificador()+"\n");
						}
					}
					if(movimentacaocontabil.getListaCredito().size() == 1 && movimentacaocontabil.getListaDebito().size() > 1){
						livroRazaoItem.setContraPartidas("");
					}else {
						livroRazaoItem.setContraPartidas(nomes.toString());						
					}
					livroRazaoItem.setData(movimentacaocontabil.getDtlancamento());
					if(movimentacaocontabil.getNumero() != null){
						livroRazaoItem.setDocumento(movimentacaocontabil.getNumero());
					}
					if(movimentacaocontabil.getHistorico() != null){
						livroRazaoItem.setHistorico(movimentacaocontabil.getHistorico());
					}
					if(mov.getValor() != null){
						livroRazaoItem.setCredito(mov.getValor());												
					}
												
					//saldo
					livroRazaoItem.setSaldo(livroRazaoBean.getSaldoAtual());
					
					Money atualizaSaldo = new Money();
					//se for conta CREDORA: credito aumenta
					if(NaturezaContaCompensacaoEnum.CREDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().add(mov.getValor());					
					//se for conta DEVEDORA: credito diminui
					}else if (NaturezaContaCompensacaoEnum.DEVEDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().subtract(mov.getValor());
					//se for conta CREDORA RETIFICADORA: credito diminui
					}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().subtract(mov.getValor());
					//se for conta DEVEDORA RETIFICADORA: credito aumenta
					}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(livroRazaoBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(livroRazaoBean.getContaContabil().getContaretificadora())){
						atualizaSaldo = livroRazaoBean.getSaldoAtual().add(mov.getValor());
					}
					
					livroRazaoItem.setSaldoAtual(atualizaSaldo);
					livroRazaoItem.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(atualizaSaldo));
					livroRazaoBean.setSaldoAtual(atualizaSaldo);
					livroRazaoBean.setSaldoAtualString(SinedUtil.converteValorNegativoRelatorio(atualizaSaldo));
					//end saldo
					
					//Total creditos e debitos
					Money totalCreditos = new Money();
					if(livroRazaoBean.getTotalCreditos() != null){
						totalCreditos = livroRazaoBean.getTotalCreditos().add(mov.getValor());
					}else {
						totalCreditos = mov.getValor();
					}
					livroRazaoBean.setTotalCreditos(totalCreditos);
					livroRazaoBean.setTotalCreditosString(SinedUtil.converteValorNegativoRelatorio(totalCreditos));
					livroRazaoItem.setTotalCreditos(totalCreditos);
					livroRazaoItem.setTotalCreditosString(SinedUtil.converteValorNegativoRelatorio(totalCreditos));
					//end total debitos e creditos
					
					if(mov.getCentrocusto() != null){
						livroRazaoItem.setCentroCusto(mov.getCentrocusto());
					}
					
					if(mov.getProjeto() != null){
						livroRazaoItem.setProjeto(mov.getProjeto());
					}
					
					boolean centroCusto = false;
					boolean projeto = false;
					
					if(filtro.getCentroCusto() == null || (livroRazaoItem.getCentroCusto() != null && filtro.getCentroCusto().getCdcentrocusto().equals(livroRazaoItem.getCentroCusto().getCdcentrocusto()))){
						centroCusto = true;
					}
					if(filtro.getProjeto() == null || (livroRazaoItem.getProjeto() != null && filtro.getProjeto().getCdprojeto().equals(livroRazaoItem.getProjeto().getCdprojeto()))){
						projeto = true;
					}
					
					if(centroCusto && projeto){
						listaLivroRazaoItens.add(livroRazaoItem);						
					}
						
					if(livroRazaoBean.getListaMovimentacoes() == null){
						livroRazaoBean.setListaMovimentacoes(listaLivroRazaoItens);							
					}else {
						livroRazaoBean.getListaMovimentacoes().addAll(listaLivroRazaoItens);
					}
				}
			}
		}
		
		List<EmitirLivroRazaoBean> listaOrdenadaLivroRazao = new ArrayList<EmitirLivroRazaoBean>();
		
		if(!mapaPorContaGerencial.isEmpty()){
			EmitirLivroRazaoBean bean = new EmitirLivroRazaoBean();
			for (Integer chave: mapaPorContaGerencial.keySet()) {
				bean = mapaPorContaGerencial.get(chave);
				
				if((filtro.getContaContabil() == null || chave.equals(filtro.getContaContabil().getCdcontacontabil())) && SinedUtil.isListNotEmpty(bean.getListaMovimentacoes())){
					listaOrdenadaLivroRazao.add(bean);										
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaOrdenadaLivroRazao)){
			listaOrdenadaLivroRazao = ordenaPorIdentificadorContaGerencial(listaOrdenadaLivroRazao);			
		}
		
		return this.makeRelatorioLivroRazao(filtro, listaOrdenadaLivroRazao);
	}


	
	private IReport makeRelatorioLivroRazao(EmitirLivroRazaoFiltro filtro, List<EmitirLivroRazaoBean> listaOrdenadaLivroRazao) {
		Report report = new Report("/contabil/livrorazao");
		Report reportSub = new Report("/contabil/sub_livrorazao");
		
		report.setDataSource(listaOrdenadaLivroRazao);
		
		report.addSubReport("SUB_LIVRORAZAO", reportSub);
		
		return report;
	}
	
	/**
	 * Ordena a lista em razao do identificador da conta gerencial
	 */
	public List<EmitirLivroRazaoBean> ordenaPorIdentificadorContaGerencial(List<EmitirLivroRazaoBean> lista){
		
		Collections.sort(lista, new Comparator<EmitirLivroRazaoBean>() {
	        @Override
	        public int compare(EmitirLivroRazaoBean bean1, EmitirLivroRazaoBean bean2)
	        {

	            return bean1.getContaContabil().getVcontacontabil().getIdentificador().compareTo(bean2.getContaContabil().getVcontacontabil().getIdentificador());
	        }
	    });
		
		return lista;
	}
	
	public List<Movimentacaocontabil> findForBalancoPatrimonial(EmitirBalancoPatrimonialFiltro filtro){
		
		List<Movimentacaocontabil> listaMovimentacaoContabil = movimentacaocontabilDAO.findForBalancoPatrimonial(filtro);
		
		//remove as outras contas contabeis que trazidas na movimentacaoo contabil que naoo estao de acordo com o filtro
		for (Movimentacaocontabil movimentacaocontabil : listaMovimentacaoContabil) {
			List<Movimentacaocontabildebitocredito> listaRemoverCredito = new ArrayList<Movimentacaocontabildebitocredito>();
			List<Movimentacaocontabildebitocredito> listaRemoverDebito = new ArrayList<Movimentacaocontabildebitocredito>();
			
			//lista credito
			for (Movimentacaocontabildebitocredito mov : movimentacaocontabil.getListaCredito()) {
				Boolean centroCustoBool = false;
				Boolean projetoBool = false;
				Boolean contaGerencialBool = false;
				Boolean saldoInicialAnterior = false;
				if(filtro.getCentroCusto() != null && mov.getCentrocusto() != null && !mov.getCentrocusto().equals(filtro.getCentroCusto())){
					centroCustoBool = true;
				}
				if(filtro.getProjeto() != null && mov.getProjeto() != null && !mov.getProjeto().equals(filtro.getProjeto())){
					projetoBool = true;
				}
				if(filtro.getContaContabil() != null && !mov.getContaContabil().equals(filtro.getContaContabil())){
					contaGerencialBool = true;
				}
				if(mov.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.afterIgnoreHour(mov.getContaContabil().getDtSaldoInicial(), (movimentacaocontabil.getDtlancamento()))){
					saldoInicialAnterior = true;
				}
				
				if(centroCustoBool || projetoBool || contaGerencialBool || saldoInicialAnterior){
					listaRemoverCredito.add(mov);
				}
			}
			
			movimentacaocontabil.getListaCredito().removeAll(listaRemoverCredito);
			
			//lista debito
			for (Movimentacaocontabildebitocredito mov : movimentacaocontabil.getListaDebito()) {
				Boolean centroCustoBool = false;
				Boolean projetoBool = false;
				Boolean contaGerencialBool = false;
				Boolean saldoInicialAnterior = false;
				if(filtro.getCentroCusto() != null && mov.getCentrocusto() != null && !mov.getCentrocusto().equals(filtro.getCentroCusto())){
					centroCustoBool = true;
				}
				if(filtro.getProjeto() != null && mov.getProjeto() != null && !mov.getProjeto().equals(filtro.getProjeto())){
					projetoBool = true;
				}
				if(filtro.getContaContabil() != null && !mov.getContaContabil().equals(filtro.getContaContabil())){
					contaGerencialBool = true;
				}
				if(mov.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.afterIgnoreHour(mov.getContaContabil().getDtSaldoInicial(), (movimentacaocontabil.getDtlancamento()))){
					saldoInicialAnterior = true;
				}
				
				if(centroCustoBool || projetoBool || contaGerencialBool || saldoInicialAnterior){
					listaRemoverDebito.add(mov);
				}
			}
			
			movimentacaocontabil.getListaDebito().removeAll(listaRemoverDebito);			
		}
		
		return listaMovimentacaoContabil;
	}
	
	public IReport gerarRelatorioBalancoPatrimonial(EmitirBalancoPatrimonialFiltro filtro) {
		//lista com todas as contas filhas que tiveram movimentacoes contabeis
		List<Movimentacaocontabil> listaMovimentacaoContabil = this.findForBalancoPatrimonial(filtro);
		
		//lista com todas as contas gerenciais
		List<EmitirBalancoPatrimonialBean> listaCompleta = contaContabilService.buscarArvoreContaGerencialBalancoPatrimonial();
		
		if(SinedUtil.isListNotEmpty(listaMovimentacaoContabil)) {
			//itera sobre a lista de movimentacao contabil
			for (Movimentacaocontabil movimentacaoContabil : listaMovimentacaoContabil) {
				
				//listaCredito
				for (Movimentacaocontabildebitocredito mov : movimentacaoContabil.getListaCredito()) {
					EmitirBalancoPatrimonialBean emitirBalancoPatrimonialBean = new EmitirBalancoPatrimonialBean();
					emitirBalancoPatrimonialBean.setIsSintetico(filtro.getIsSintetico());
					if(mov.getContaContabil() != null){
						emitirBalancoPatrimonialBean.setContaContabil(mov.getContaContabil());
					}
					
					//se na lista completa tiver a conta contabil que teve movimentacaoo, essa sera substituida por essa conta preenchida
					if(listaCompleta.contains(emitirBalancoPatrimonialBean)) {
						int index = listaCompleta.indexOf(emitirBalancoPatrimonialBean);
						Money saldoAnterior = new Money();
						
						if (listaCompleta.get(index).getSaldoAnterior() == null) {
							//se o mes e ano da data do saldo inicial forem iguais aos da data dp filtro, o saldo anterior sera considerado o saldo inicial para os calculos, e depois zerado 
							if(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.equalsMesAno(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial(), filtro.getDtInicio())){
								saldoAnterior = emitirBalancoPatrimonialBean.getContaContabil().getSaldoInicial();
							}else {
								//Chama funcao no banco para calcular o saldo dessa conta gerencial	
								saldoAnterior = calcularSaldoContaContabil(emitirBalancoPatrimonialBean.getContaContabil().getCdcontacontabil(), SinedDateUtils.addDiasData(filtro.getDtInicio(), -1), filtro.getEmpresa(), filtro.getCentroCusto(), filtro.getProjeto());								
							}
							
							//Se for conta CREDORA: Credito aumenta
							if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.add(mov.getValor()));
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.add(mov.getValor())));
							//se for conta DEVEDORA: credito diminui
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.subtract(mov.getValor()));	
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.subtract(mov.getValor())));
							//se for conta CREDORA RETIFICADORA: credito diminui
							}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.subtract(mov.getValor()));	
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.subtract(mov.getValor())));
							//se for conta DEVEDORA RETIFICADORA: credito aumenta
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.add(mov.getValor()));
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.add(mov.getValor())));
							}
							
							//se o mes e ano da data do saldo inicial forem iguais aos da data do filtro, o saldo inicial � zerado, para que nao apareca no periodo anterior
							if(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.equalsMesAno(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial(), filtro.getDtInicio())){
								saldoAnterior = new Money();
							}
							
							emitirBalancoPatrimonialBean.setSaldoAnterior(saldoAnterior);
							emitirBalancoPatrimonialBean.setSaldoAnteriorString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));
							
							listaCompleta.set(index, emitirBalancoPatrimonialBean);
						}else {
							Money saldoFinal = listaCompleta.get(index).getSaldoFinal();
							//incrementa o valor da movimentacao contabil no saldo final
							//Se for conta CREDORA: Credito aumenta
							if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.add(mov.getValor());
							//se for conta DEVEDORA: credito diminui
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.subtract(mov.getValor());
							//se for conta CREDORA RETIFICADORA: credito diminui
							}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.subtract(mov.getValor());
							//se for conta DEVEDORA RETIFICADORA: credito aumenta
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.add(mov.getValor());
							}
							listaCompleta.get(index).setSaldoFinal(saldoFinal);
							listaCompleta.get(index).setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoFinal));
						}
					}
				}
				
				//listaDebito
				for (Movimentacaocontabildebitocredito mov : movimentacaoContabil.getListaDebito()) {					
					EmitirBalancoPatrimonialBean emitirBalancoPatrimonialBean = new EmitirBalancoPatrimonialBean();
					emitirBalancoPatrimonialBean.setIsSintetico(filtro.getIsSintetico());
					if(mov.getContaContabil() != null){	
						emitirBalancoPatrimonialBean.setContaContabil(mov.getContaContabil());					
					}
					
					//se na lista completa tiver a conta contabil que teve movimentacao, essa sera substituida por essa conta preenchida
					if(listaCompleta.contains(emitirBalancoPatrimonialBean)) {
						int index = listaCompleta.indexOf(emitirBalancoPatrimonialBean);
						Money saldoAnterior = new Money();
						
						if (listaCompleta.get(index).getSaldoAnterior() == null) {
							//se o mes e ano da data do saldo inicial forem iguais aos da data dp filtro, o saldo anterior sera considerado o saldo inicial para os calculos, e depois zerado 
							if(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.equalsMesAno(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial(), filtro.getDtInicio())){
								saldoAnterior = emitirBalancoPatrimonialBean.getContaContabil().getSaldoInicial();
							}else {
								//Chama funcao no banco para calcular o saldo dessa conta gerencial	
								saldoAnterior = calcularSaldoContaContabil(emitirBalancoPatrimonialBean.getContaContabil().getCdcontacontabil(), SinedDateUtils.addDiasData(filtro.getDtInicio(), -1), filtro.getEmpresa(), filtro.getCentroCusto(), filtro.getProjeto());								
							}

							//se for conta CREDORA: debito diminui
							if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.subtract(mov.getValor()));
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.subtract(mov.getValor())));
							//se for conta DEVEDORA: debito aumenta
							}else if (NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.add(mov.getValor()));							
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.add(mov.getValor())));
							//se for conta CREDORA RETIFICADORA: debito aumenta
							}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.add(mov.getValor()));							
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.add(mov.getValor())));
							//se for conta DEVEDORA RETIFICADORA: debito diminui
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								emitirBalancoPatrimonialBean.setSaldoFinal(saldoAnterior.subtract(mov.getValor()));
								emitirBalancoPatrimonialBean.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior.subtract(mov.getValor())));
							}
							
							//se o mes e ano da data do saldo inicial forem iguais aos da data do filtro, o saldo inicial � zerado, para que nao apareca no periodo anterior
							if(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial() != null && SinedDateUtils.equalsMesAno(emitirBalancoPatrimonialBean.getContaContabil().getDtSaldoInicial(), filtro.getDtInicio())){
								saldoAnterior = new Money();
							}
							emitirBalancoPatrimonialBean.setSaldoAnterior(saldoAnterior);
							emitirBalancoPatrimonialBean.setSaldoAnteriorString(SinedUtil.converteValorNegativoRelatorio(saldoAnterior));
							
							listaCompleta.set(index, emitirBalancoPatrimonialBean);
							
						}else {
							Money saldoFinal = listaCompleta.get(index).getSaldoFinal();
							//incrementa o valor da movimentacao contabil no saldo final
							//se for conta CREDORA: debito diminui
							if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.subtract(mov.getValor());
							//se for conta DEVEDORA: debito aumenta
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.FALSE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.add(mov.getValor());
							//se for conta CREDORA RETIFICADORA: debito aumenta
							}else if(NaturezaContaCompensacaoEnum.CREDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.add(mov.getValor());
							//se for conta DEVEDORA RETIFICADORA: debito diminui
							}else if(NaturezaContaCompensacaoEnum.DEVEDORA.equals(emitirBalancoPatrimonialBean.getContaContabil().getNaturezaContaCompensacao()) && Boolean.TRUE.equals(emitirBalancoPatrimonialBean.getContaContabil().getContaretificadora())){
								saldoFinal = saldoFinal.subtract(mov.getValor());
							}
							listaCompleta.get(index).setSaldoFinal(saldoFinal);
							listaCompleta.get(index).setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(saldoFinal));
						}		
					}
				}
			}
		}
		
		listaCompleta = calcularValores(listaCompleta, true, filtro);
		
		for (EmitirBalancoPatrimonialBean emitirBalancoPatrimonialBean : listaCompleta) {
			emitirBalancoPatrimonialBean.setIdentificadorNome(emitirBalancoPatrimonialBean.getContaContabil().getVcontacontabil().getIdentificador() + emitirBalancoPatrimonialBean.getContaContabil().getNome());
		}
		
		return this.makeRelatorioBalancoPatrimonial(filtro, listaCompleta);
	}
	
	public List<EmitirBalancoPatrimonialBean> calcularValores(List<EmitirBalancoPatrimonialBean> listaCompleta, Boolean contascomlancamentos, EmitirBalancoPatrimonialFiltro filtro) {
		List<EmitirBalancoPatrimonialBean> listaTrabalho = null;
		List<EmitirBalancoPatrimonialBean> listaIrmas = null;
		List<EmitirBalancoPatrimonialBean> listaZerados = new ArrayList<EmitirBalancoPatrimonialBean>();
		EmitirBalancoPatrimonialBean filho = null;
		EmitirBalancoPatrimonialBean pai = null;
		Boolean irmaTemMovimentacao = false;

		int nivel = this.encontrarMaiorNivel(listaCompleta);

		while (nivel > 1) {
			listaTrabalho = this.buscarTodasNivel(listaCompleta, nivel);
			while (!listaTrabalho.isEmpty()) {
				irmaTemMovimentacao = false;
				pai = null;
				filho = listaTrabalho.get(0);
				listaIrmas = this.buscarIrmas(listaTrabalho, filho);
				
				for (EmitirBalancoPatrimonialBean irma : listaIrmas) {
					if(irma.getSaldoAnterior() != null || irma.getSaldoFinal() != null){
						irmaTemMovimentacao = true;
					}
				}
				if (irmaTemMovimentacao){
					pai = this.buscarPai(listaCompleta, filho);					
				}
				if(pai != null){ 
					pai.setSaldoAnterior(this.somaSaldoAnterior(listaIrmas, pai));
					pai.setSaldoAnteriorString(SinedUtil.converteValorNegativoRelatorio(pai.getSaldoAnterior()));
					pai.setSaldoFinal(this.somaSaldoFinal(listaIrmas, pai));
					pai.setSaldoFinalString(SinedUtil.converteValorNegativoRelatorio(pai.getSaldoFinal()));
					pai.setTemFilho(true);
				}
				listaTrabalho.removeAll(listaIrmas);
			}
			nivel-=1;
		}
		
		if(contascomlancamentos){
			for (EmitirBalancoPatrimonialBean bean : listaCompleta) {
				if(bean.getSaldoFinal() == null){
					listaZerados.add(bean);					
				}
				if(Boolean.TRUE.equals(bean.getIsSintetico())){
					listaZerados.add(bean);
				}
			}
		}
				
		if(listaZerados != null && !listaZerados.isEmpty()){
			listaCompleta.removeAll(listaZerados);				
		}
		
		return listaCompleta;
	}	
	
	private int encontrarMaiorNivel(List<EmitirBalancoPatrimonialBean> listaCompleta) {
		if (listaCompleta == null || listaCompleta.isEmpty()) return -1;
		
		int tamMax = -1;
		for (EmitirBalancoPatrimonialBean bean : listaCompleta) {
			if (bean.getContaContabil().getVcontacontabil().getIdentificador().split("\\.").length > tamMax) {
				tamMax = bean.getContaContabil().getVcontacontabil().getIdentificador().split("\\.").length;
			}
		}
		return tamMax;
	}
	
	private List<EmitirBalancoPatrimonialBean> buscarTodasNivel(List<EmitirBalancoPatrimonialBean> listaCompleta, int nivel) {
		List<EmitirBalancoPatrimonialBean> listaMesmoNivel = new ArrayList<EmitirBalancoPatrimonialBean>();
		
		for (EmitirBalancoPatrimonialBean bean : listaCompleta) {
			if (bean.getContaContabil().getVcontacontabil().getIdentificador().split("\\.").length == nivel && ((bean.getContaContabil().getAtivo() != null && bean.getContaContabil().getAtivo()) || bean.getContaContabil().getAtivo() == null))
				listaMesmoNivel.add(bean);
		}
		
		return listaMesmoNivel;
	}
	
	private List<EmitirBalancoPatrimonialBean> buscarIrmas(List<EmitirBalancoPatrimonialBean> lista, EmitirBalancoPatrimonialBean bean) {
		String id = bean.getContaContabil().getVcontacontabil().getIdentificador();
		String prefixo = id.split("\\.").length > 1 ? StringUtils.substring(id, 0, (id.length()-(id.split("\\.")[id.split("\\.").length-1].length() + 1))) : "";
		if (StringUtils.isEmpty(prefixo)) return null;
		
		EmitirBalancoPatrimonialBean irma = null;
		List<EmitirBalancoPatrimonialBean> listaIrmas = new ArrayList<EmitirBalancoPatrimonialBean>();
		
		int index = lista.indexOf(bean);
		listaIrmas.add(lista.get(index));
		index++;
		
		while (lista.size() > index) {
			irma = lista.get(index);
			if (irma.getContaContabil().getVcontacontabil().getIdentificador().startsWith(prefixo)) {
				listaIrmas.add(irma);
			} else {
				break;
				//como a lista est� ordenada, se a CG atual n�o � irm� da CG do par�metro da fun��o,
				//as seguintes tamb�m n�o ser�o.
			}
			
			index++;
		}
		
		return listaIrmas;
	}

	private EmitirBalancoPatrimonialBean buscarPai(List<EmitirBalancoPatrimonialBean> lista, EmitirBalancoPatrimonialBean filho) {
		int indexPai = Collections.binarySearch(lista, filho, EmitirBalancoPatrimonialBean.PAI_COMPARATOR);

		if (indexPai < 0) {
			return null;
		}
		return lista.get(indexPai);
	}
	
	private Money somaSaldoAnterior(List<EmitirBalancoPatrimonialBean> lista, EmitirBalancoPatrimonialBean pai) {
		Money somatorio = new Money();
		for (EmitirBalancoPatrimonialBean bean : lista) {
			if(bean.getSaldoAnterior() != null && bean.getContaContabil().getContaretificadora().equals(pai.getContaContabil().getContaretificadora())){
				somatorio = somatorio.add(bean.getSaldoAnterior());
			}else if(bean.getSaldoAnterior() != null && bean.getContaContabil().getContaretificadora() && !pai.getContaContabil().getContaretificadora() && bean.getSaldoAnterior().toLong() < 0){
				Money saldo = bean.getSaldoAnterior().multiply(new Money(-1));
				somatorio = somatorio.add(saldo);
			}else if(bean.getSaldoAnterior() != null && bean.getContaContabil().getContaretificadora() && !pai.getContaContabil().getContaretificadora() && bean.getSaldoAnterior().toLong() > 0){
				somatorio = somatorio.subtract(bean.getSaldoAnterior());
			}
		}
		return somatorio;
	}
	
	private Money somaSaldoFinal(List<EmitirBalancoPatrimonialBean> lista, EmitirBalancoPatrimonialBean pai) {
		Money somatorio = new Money();
		for (EmitirBalancoPatrimonialBean bean : lista) {
			if(bean.getSaldoFinal() != null && bean.getContaContabil().getContaretificadora().equals(pai.getContaContabil().getContaretificadora())){
				somatorio = somatorio.add(bean.getSaldoFinal());
			}else if(bean.getSaldoFinal() != null && bean.getContaContabil().getContaretificadora() && !pai.getContaContabil().getContaretificadora() && bean.getSaldoFinal().toLong() < 0){
				Money saldo = bean.getSaldoFinal().multiply(new Money(-1));
				somatorio = somatorio.add(saldo);
			}else if(bean.getSaldoFinal() != null && bean.getContaContabil().getContaretificadora() && !pai.getContaContabil().getContaretificadora() && bean.getSaldoFinal().toLong() > 0){
				somatorio = somatorio.subtract(bean.getSaldoFinal());
			}
		}
		return somatorio;
	}
	
	private Money calcularSaldoContaContabil(Integer cdcontagerencial, Date dataReferencia, Empresa empresa, Centrocusto centrocusto, Projeto projeto) {
		return movimentacaocontabilDAO.calcularSaldoContaContabil(cdcontagerencial, dataReferencia, empresa, centrocusto, projeto);
	}
	
	public IReport makeRelatorioBalancoPatrimonial(EmitirBalancoPatrimonialFiltro filtro, List<EmitirBalancoPatrimonialBean> listaCompleta) {
		Report report = new Report("/contabil/balancopatrimonial");
		
		report.setDataSource(listaCompleta);
		
		return report;
	}
	public Boolean haveMovimentacaoContabil(String identificador) {
		return movimentacaocontabilDAO.haveMovimentacaoContabil(identificador);
	}
	
	public List<Movimentacaocontabil> carregarMovimentacoesContabeisParaValidarExclusao(String whereIn) {
		return movimentacaocontabilDAO.carregarMovimentacoesContabeisParaValidarExclusao(whereIn);
	}
	
	public List<Movimentacaocontabil> carregarMovimentacoesContabeisPorLote(String whereInLoteContabil) {
		return movimentacaocontabilDAO.carregarMovimentacoesContabeisPorLote(whereInLoteContabil);
	}

}