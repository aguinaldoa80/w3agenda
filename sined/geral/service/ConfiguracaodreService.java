package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.bean.Configuracaodreimpostofinal;
import br.com.linkcom.sined.geral.bean.Configuracaodreitem;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Configuracaodreoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Configuracaodreseparacao;
import br.com.linkcom.sined.geral.dao.ConfiguracaodreDAO;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREFormulaImpostoBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREImpostoBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREItemBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.GerarDREFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaodreService extends GenericService<Configuracaodre> {
	
	private ConfiguracaodreDAO configuracaodreDAO;
	private MovimentacaocontabilService movimentacaocontabilService;
	private EmpresaService empresaService;
	private CentrocustoService centrocustoService;
	private ProjetoService projetoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMovimentacaocontabilService(
			MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}
	public void setConfiguracaodreDAO(ConfiguracaodreDAO configuracaodreDAO) {
		this.configuracaodreDAO = configuracaodreDAO;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param configuracaodre
	 * @return
	 * @since 13/12/2019
	 * @author Rodrigo Freitas
	 */
	public Configuracaodre loadForDRE(Configuracaodre configuracaodre) {
		return configuracaodreDAO.loadForDRE(configuracaodre);
	}
	
	public ModelAndView gerarDREPDF(GerarDREFiltro filtro) {
		Configuracaodre configuracaodre = this.loadForDRE(filtro.getConfiguracaodre());
		GerarDREBean bean = this.gerarDRE(filtro, configuracaodre);
		
		List<GerarDREItemBean> lista = new ArrayList<GerarDREItemBean>();
		lista.addAll(bean.getListaOperacionalBruto());
		lista.add(new GerarDREItemBean(-1, null, bean.getReceitaliquida_label(), bean.getReceitaliquida()));
		lista.addAll(bean.getListaOperacionalLiquido());
		lista.add(new GerarDREItemBean(-1, null, bean.getResultadobruto_label(), bean.getResultadobruto()));
		lista.addAll(bean.getListaNaoOperacional());
		lista.add(new GerarDREItemBean(-1, null, bean.getResultadoliquidoantesimpostos_label(), bean.getResultadoliquidoantesimpostos()));
		
		List<GerarDREImpostoBean> listaImposto = bean.getListaImposto();
		for (GerarDREImpostoBean gerarDREImpostoBean : listaImposto) {
			lista.add(new GerarDREItemBean(0, null, gerarDREImpostoBean.getDescricao(), gerarDREImpostoBean.getValor()));
		}
		
		lista.add(new GerarDREItemBean(-1, null, bean.getResultadoliquido_label(), bean.getResultadoliquido()));
		
		Report report = new Report("contabil/dre");
		report.addParameter("filtro", filtro);
		report.setDataSource(lista);
		
		
		Empresa empresa = null;
		try {
			empresa = empresaService.loadComArquivo(filtro.getEmpresa());
		} catch (Exception e) {
			empresa = empresaService.loadPrincipal();
		}
		
		report.addParameter("TITULO", "DRE");
		report.addParameter("EMPRESA", empresa == null ? null : (empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa() != null ? empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa() : empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa()));
		report.addParameter("CENTROCUSTO", filtro.getCentrocusto() != null ?centrocustoService.load(filtro.getCentrocusto(), "centrocusto.nome").getNome() : null);
		report.addParameter("PROJETO", filtro.getProjeto() != null ? projetoService.load(filtro.getProjeto(), "projeto.nome").getNome() : null);
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.addParameter("PERIODO", SinedDateUtils.getPeriodoDeAte(filtro.getDtinicio(), filtro.getDtfim()));
		
		Resource resource = new Resource();
        resource.setContentType("application/pdf");
        resource.setFileName("dre_" + SinedUtil.datePatternForReport() + ".pdf");
        resource.setContents(Neo.getApplicationContext().getReportGenerator().toPdf(report));
		return new ResourceModelAndView(resource);
	}
	
	public GerarDREBean gerarDRE(GerarDREFiltro filtro, Configuracaodre configuracaodre){
		List<Configuracaodreitem> listaConfiguracaodreitem = configuracaodre.getListaConfiguracaodreitem();
		List<Configuracaodreimpostofinal> listaConfiguracaodreimpostofinal = configuracaodre.getListaConfiguracaodreimpostofinal();
		
		List<Configuracaodretipo> listaConfiguracaodretipoOperacionalBruto = new ArrayList<Configuracaodretipo>();
		List<Configuracaodretipo> listaConfiguracaodretipoOperacionalLiquido = new ArrayList<Configuracaodretipo>();
		List<Configuracaodretipo> listaConfiguracaodretipoNaoOperacional = new ArrayList<Configuracaodretipo>();
		
		for (Configuracaodreitem configuracaodreitem : listaConfiguracaodreitem) {
			if(configuracaodreitem != null && configuracaodreitem.getConfiguracaodretipo() != null){
				Configuracaodretipo configuracaodretipo = configuracaodreitem.getConfiguracaodretipo();
				Configuracaodreseparacao configuracaodreseparacao = configuracaodretipo.getConfiguracaodreseparacao();
				if(configuracaodreseparacao != null){
					switch (configuracaodreseparacao) {
						case ANTES_RESULTADO_BRUTO: 
							if(!listaConfiguracaodretipoOperacionalBruto.contains(configuracaodretipo)){
								listaConfiguracaodretipoOperacionalBruto.add(configuracaodretipo); 
							}
							break;
						case ANTES_RESULTADO_LIQUIDO: 
							if(!listaConfiguracaodretipoOperacionalLiquido.contains(configuracaodretipo)){
								listaConfiguracaodretipoOperacionalLiquido.add(configuracaodretipo); 
							}
							break;
						case ANTES_IMPOSTOS: 
							if(!listaConfiguracaodretipoNaoOperacional.contains(configuracaodretipo)){
								listaConfiguracaodretipoNaoOperacional.add(configuracaodretipo); 
							}
							break;
					}
				}
			}
		}
		
		Comparator<Configuracaodretipo> comparator = new Comparator<Configuracaodretipo>(){
			public int compare(Configuracaodretipo o1, Configuracaodretipo o2) {
				if(o1.getOrdem() != null && o2.getOrdem() != null){
					return o1.getOrdem().compareTo(o2.getOrdem());
				} else if(o1.getOrdem() != null){
					return o1.getOrdem().compareTo(Integer.MAX_VALUE);
				} else if(o2.getOrdem() != null){
					return new Integer(Integer.MAX_VALUE).compareTo(o2.getOrdem());
				}
				
				return 0;
			}
		};
		Collections.sort(listaConfiguracaodretipoOperacionalBruto, comparator);
		Collections.sort(listaConfiguracaodretipoOperacionalLiquido, comparator);
		Collections.sort(listaConfiguracaodretipoNaoOperacional, comparator);
		
		Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.crt");
		Date dtinicio = filtro.getDtinicio();
		Date dtfim = filtro.getDtfim();
		Centrocusto centrocusto = filtro.getCentrocusto();
		Projeto projeto = filtro.getProjeto();
		
		GerarDREBean bean = new GerarDREBean();
		if(StringUtils.isNotBlank(configuracaodre.getReceitaliquida_label())) 
			bean.setReceitaliquida_label(configuracaodre.getReceitaliquida_label());
		if(StringUtils.isNotBlank(configuracaodre.getResultadobruto_label())) 
			bean.setResultadobruto_label(configuracaodre.getResultadobruto_label());
		if(StringUtils.isNotBlank(configuracaodre.getResultadoliquidoantesimpostos_label())) 
			bean.setResultadoliquidoantesimpostos_label(configuracaodre.getResultadoliquidoantesimpostos_label());
		if(StringUtils.isNotBlank(configuracaodre.getResultadoliquido_label())) 
			bean.setResultadoliquido_label(configuracaodre.getResultadoliquido_label());
		
		for (Configuracaodretipo configuracaodretipo : listaConfiguracaodretipoOperacionalBruto) {
			GerarDREItemBean itemBean = new GerarDREItemBean();
			itemBean.setId(configuracaodretipo.getCdconfiguracaodretipo());
			itemBean.setOperacao(configuracaodretipo.getConfiguracaodreoperacao());
			itemBean.setDescricao((configuracaodretipo.getConfiguracaodreoperacao() != null && configuracaodretipo.getConfiguracaodreoperacao().equals(Configuracaodreoperacao.DEBITO) ? "(-) " : "") + configuracaodretipo.getDescricao());
			itemBean.setValor(new Money(movimentacaocontabilService.getSumValorForDRE(empresa, dtinicio, dtfim, configuracaodretipo, configuracaodre, centrocusto, projeto)));
			bean.getListaOperacionalBruto().add(itemBean);
		}
		for (Configuracaodretipo configuracaodretipo : listaConfiguracaodretipoOperacionalLiquido) {
			GerarDREItemBean itemBean = new GerarDREItemBean();
			itemBean.setId(configuracaodretipo.getCdconfiguracaodretipo());
			itemBean.setOperacao(configuracaodretipo.getConfiguracaodreoperacao());
			itemBean.setDescricao((configuracaodretipo.getConfiguracaodreoperacao() != null && configuracaodretipo.getConfiguracaodreoperacao().equals(Configuracaodreoperacao.DEBITO) ? "(-) " : "") + configuracaodretipo.getDescricao());
			itemBean.setValor(new Money(movimentacaocontabilService.getSumValorForDRE(empresa, dtinicio, dtfim, configuracaodretipo, configuracaodre, centrocusto, projeto)));
			bean.getListaOperacionalLiquido().add(itemBean);
		}
		for (Configuracaodretipo configuracaodretipo : listaConfiguracaodretipoNaoOperacional) {
			GerarDREItemBean itemBean = new GerarDREItemBean();
			itemBean.setId(configuracaodretipo.getCdconfiguracaodretipo());
			itemBean.setOperacao(configuracaodretipo.getConfiguracaodreoperacao());
			itemBean.setDescricao((configuracaodretipo.getConfiguracaodreoperacao() != null && configuracaodretipo.getConfiguracaodreoperacao().equals(Configuracaodreoperacao.DEBITO) ? "(-) " : "") + configuracaodretipo.getDescricao());
			itemBean.setValor(new Money(movimentacaocontabilService.getSumValorForDRE(empresa, dtinicio, dtfim, configuracaodretipo, configuracaodre, centrocusto, projeto)));
			bean.getListaNaoOperacional().add(itemBean);
		}
		
		Money receitaliquida = new Money();
		Money resultadobruto = new Money();
		Money resultadoliquidoantesimpostos = new Money();
		Money resultadoliquido = new Money();
		for (GerarDREItemBean it : bean.getListaOperacionalBruto()) {
			if(it.getOperacao().equals(Configuracaodreoperacao.CREDITO)){
				receitaliquida = receitaliquida.add(it.getValor());
			} else if(it.getOperacao().equals(Configuracaodreoperacao.DEBITO)){
				receitaliquida = receitaliquida.subtract(it.getValor());
			}
		}
		
		resultadobruto = resultadobruto.add(receitaliquida);
		for (GerarDREItemBean it : bean.getListaOperacionalLiquido()) {
			if(it.getOperacao().equals(Configuracaodreoperacao.CREDITO)){
				resultadobruto = resultadobruto.add(it.getValor());
			} else if(it.getOperacao().equals(Configuracaodreoperacao.DEBITO)){
				resultadobruto = resultadobruto.subtract(it.getValor());
			}
		}
		
		resultadoliquidoantesimpostos = resultadoliquidoantesimpostos.add(resultadobruto);
		for (GerarDREItemBean it : bean.getListaNaoOperacional()) {
			if(it.getOperacao().equals(Configuracaodreoperacao.CREDITO)){
				resultadoliquidoantesimpostos = resultadoliquidoantesimpostos.add(it.getValor());
			} else if(it.getOperacao().equals(Configuracaodreoperacao.DEBITO)){
				resultadoliquidoantesimpostos = resultadoliquidoantesimpostos.subtract(it.getValor());
			}
		}
		
		resultadoliquido = resultadoliquido.add(resultadoliquidoantesimpostos);
		for (Configuracaodreimpostofinal configuracaodreimpostofinal : listaConfiguracaodreimpostofinal) {
			Double percentual = configuracaodreimpostofinal.getPercentual() != null ? configuracaodreimpostofinal.getPercentual() : 0d;
			String formula = configuracaodreimpostofinal.getFormula();
			
			GerarDREImpostoBean impostoBean = new GerarDREImpostoBean();
			impostoBean.setDescricao("(-) " + configuracaodreimpostofinal.getDescricao());
			
			Money valor = new Money();
			
			if(StringUtils.isNotBlank(formula)){
				try {
					GerarDREFormulaImpostoBean formulaImpostoBean = new GerarDREFormulaImpostoBean();
					formulaImpostoBean.setCrt(empresa.getCrt() != null ? empresa.getCrt().name() : "");
					formulaImpostoBean.setDtfim(dtfim);
					formulaImpostoBean.setDtinicio(dtinicio);
					formulaImpostoBean.setDiasperiodo(SinedDateUtils.diferencaDias(dtinicio, dtfim, null, true));
					formulaImpostoBean.setReceitaliquida(receitaliquida.getValue().doubleValue());
					formulaImpostoBean.setResultadobruto(resultadobruto.getValue().doubleValue());
					formulaImpostoBean.setResultadoliquido(resultadoliquido.getValue().doubleValue());
					formulaImpostoBean.setResultadoliquidoantesimpostos(resultadoliquidoantesimpostos.getValue().doubleValue());
					
					ScriptEngineManager manager = new ScriptEngineManager();
					ScriptEngine engine = manager.getEngineByName("JavaScript");
					engine.put("dre", formulaImpostoBean);
					
					Object obj = engine.eval(formula);
	
					valor = new Money();
					if(obj != null){
						String resultadoStr = obj.toString();
						valor = new Money(resultadoStr);
					}
				} catch (ScriptException e) {
					throw new SinedException("Erro no processamento de f�rmula: " + e.getMessage(), e);
				}
			} else {
				valor = resultadoliquidoantesimpostos.multiply(new Money(percentual / 100d));
			}
			
			impostoBean.setPercentual(percentual);
			impostoBean.setValor(valor);
			bean.getListaImposto().add(impostoBean);
			
			resultadoliquido = resultadoliquido.subtract(valor);
		}

		bean.setReceitaliquida(receitaliquida);
		bean.setResultadobruto(resultadobruto);
		bean.setResultadoliquidoantesimpostos(resultadoliquidoantesimpostos);
		bean.setResultadoliquido(resultadoliquido);
		return bean;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @since 17/12/2019
	 * @author Rodrigo Freitas
	 */
	public List<GerarDREListagemBean> gerarDREListagem(GerarDREFiltro filtro) {
		return configuracaodreDAO.gerarDREListagem(filtro);
	}
	
}