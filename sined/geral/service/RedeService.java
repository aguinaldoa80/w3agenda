package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Redemascara;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.geral.dao.RedeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RedeService extends GenericService<Rede>{

	private RedeDAO redeDAO;
	private RedemascaraService redemascaraService;
	
	public void setRedeDAO(RedeDAO redeDAO) {
		this.redeDAO = redeDAO;
	}
	
	public void setRedemascaraService(RedemascaraService redemascaraService) {
		this.redemascaraService = redemascaraService;
	}
	
	/**
	 * Retorna uma lista de redes de acordo com a interface
	 * @param cliente
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Rede> findByInterface(Servidorinterface servidorinterface){
		return redeDAO.findByInterface(servidorinterface);
	}
	
	/**
	 * Retorna uma lista de redes de acordo com a interface e com o contrato material, quando existir
	**/
	public List<Rede> findByInterfaceAndContratoMaterial(Servidorinterface servidorinterface, Contratomaterial contratomaterial){
		return redeDAO.findByInterfaceAndContratoMaterial(servidorinterface, contratomaterial);
	}
	
	
	public String ipValido(String enderecoip, Redemascara mascara){
		
		String[] temp = enderecoip.split("\\.");
		mascara = redemascaraService.load(mascara);
		Integer identificador = mascara.getIdentificador();

		int REDE_A = Integer.parseInt(temp[0]);
		int REDE_B = Integer.parseInt(temp[1]);
		int REDE_C = Integer.parseInt(temp[2]);
		int REDE_D = Integer.parseInt(temp[3]);
						
		if (mascara.getCdredemascara() < 8) {								
			REDE_A = (REDE_A / identificador) * identificador;
			REDE_B = 0;
			REDE_C = 0;
			REDE_D = 0;			
		} else if (mascara.getCdredemascara() < 16) {
			REDE_B = (REDE_B / identificador) * identificador;
			REDE_C = 0;
			REDE_D = 0;						
		} else if (mascara.getCdredemascara() < 24) {
			REDE_C = (REDE_C / identificador) * identificador;
			REDE_D = 0;						
		} else {
			REDE_D = (REDE_D / identificador) * identificador;
		}
		return REDE_A + "." + REDE_B + "." + REDE_C + "." + REDE_D;
	}
}
