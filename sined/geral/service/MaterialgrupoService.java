package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialgrupocomissaovenda;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.MaterialgrupoDAO;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialgrupo.MaterialgrupoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialgrupo.MaterialGrupoW3producaoRESTModel;

public class MaterialgrupoService extends GenericService<Materialgrupo> {

	private MaterialgrupoDAO materialgrupoDAO;
	private ArquivoDAO arquivoDAO;
	private EmpresaService empresaService;
	private EnvioemailService envioemailService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setMaterialgrupoDAO(MaterialgrupoDAO materialgrupoDAO) {
		this.materialgrupoDAO = materialgrupoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	/* singleton */
	private static MaterialgrupoService instance;
	public static MaterialgrupoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialgrupoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *  
	 * @see br.com.linkcom.sined.geral.dao.MaterialgrupoDAO#findAllForFlex
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialgrupo> findAllForFlex(){
		return materialgrupoDAO.findAllForFlex();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.daoMaterialgrupoDAO.DAO#findByGrupo
	 * @param planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Materialgrupo> findByGrupo(Planejamento planejamento){
		return materialgrupoDAO.findByGrupo(planejamento);
	}
	
	/**
	 * Cria a string para a emiss�o da ordem de compra.
	 *
	 * @param listaMaterial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String makeStringObservacao(List<Ordemcompramaterial> listaMaterial) {
		if(listaMaterial == null || listaMaterial.size() == 0){
			return null;
		}
		
		if(listaMaterial.get(0) == null){
			return null;
		}
		
		Ordemcompramaterial om = listaMaterial.get(0);
		
		if(om.getMaterial() == null || om.getMaterial().getMaterialgrupo() == null || om.getMaterial().getMaterialgrupo().getArquivo() == null){
			return null;
		}
		
		Arquivo arquivo = null;
		try{
			arquivo = arquivoDAO.loadWithContents(om.getMaterial().getMaterialgrupo().getArquivo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return arquivo != null && arquivo.getContent() != null ? new String(arquivo.getContent()) : null;
	}
	
	/**
	 * M�todo que envia e-mail para usu�rio, quando o grupo de material estiver participando de uma solicita��o de compra, e est� estiver sendo autorizada
	 * 
	 * @param solicitacoes
	 * @author Tom�s Rabelo
	 */
	public void enviaEmailGrupoMaterial(List<Solicitacaocompra> solicitacoes) {
		Empresa empresa = empresaService.loadPrincipal();
		
		for (Solicitacaocompra solicitacaocompra : solicitacoes){ 
			String destinatario = solicitacaocompra.getMaterial().getMaterialgrupo().getEmail();
			
			if(destinatario != null && !destinatario.equals("")){
				String template = null;
				try{
					template = new TemplateManager("/WEB-INF/template/templateEnvioMaterialGrupoEmail.tpl")
						.assign("numero", solicitacaocompra.getIdentificador() != null ? solicitacaocompra.getIdentificador().toString() : solicitacaocompra.getDescricao())
						.assign("grupo", solicitacaocompra.getMaterial().getMaterialgrupo().getNome())
						.assign("empresa", empresa.getRazaosocialOuNome())
						.getTemplate();
				} catch (IOException e) {
					throw new SinedException("Erro no processamento do template.",e);
				}

				String assunto = "Autoriza��o solicita��o de compra.";

				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				Envioemail envioemail = envioemailService.registrarEnvio(
						empresa.getEmail(), 
						assunto, 
						template, 
						Envioemailtipo.AVISO_AUTORIZACAO_COMPRA,
						solicitacaocompra != null ? solicitacaocompra.getColaborador() : null,
						solicitacaocompra != null && solicitacaocompra.getColaborador() != null ? solicitacaocompra.getColaborador().getNome() : null,
						destinatario,
						email);
				try {
					email
						.setFrom(empresa.getEmail())
						.setSubject(assunto)
						.setTo(destinatario)
						.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinatario))
						.sendMessage();
				} catch (Exception e) {
					throw new SinedException("Erro ao enviar e-mail.",e);
				}
			}
		}
	}

	
	public List<Materialgrupo> findAtivos(){
		return materialgrupoDAO.findAtivos();
	}

	
	public List<Materialgrupo> findByUsuario(Usuario usuario) {
		return materialgrupoDAO.findByUsuario(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialgrupoDAO#loadTributacao(Materialgrupo materialgrupo, Empresa empresa)
	 *
	 * @param materialgrupo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialgrupo loadTributacao(Materialgrupo materialgrupo, Empresa empresa) {
		return materialgrupoDAO.loadTributacao(materialgrupo, empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialgrupoDAO#findByNomeForImportacao(String nome)
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2013
	 */
	public Materialgrupo findByNomeForImportacao(String nome) {
		return materialgrupoDAO.findByNomeForImportacao(nome);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialgrupoService#findForImportarproducao()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialgrupo> findForImportarproducao() {
		return materialgrupoDAO.findForImportarproducao();
	}
	
	/**
	 * M�todo que verifica se existe duplicidade nos itens do comissionamento do grupo de material
	 * obs: igualdade (cargo, comissionamento, documentotipo)
	 *
	 * @param materialgrupo
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existDuplicidadeCargoComissionamentoDocumentotipoFornecedor(Materialgrupo materialgrupo){
		Boolean duplicidade = false;
		
		if(materialgrupo != null && materialgrupo.getListaMaterialgrupocomissaovenda() != null && 
				materialgrupo.getListaMaterialgrupocomissaovenda().size() > 1){			
			int index = 0;
			int index2 = 0;
			for(Materialgrupocomissaovenda m1 : materialgrupo.getListaMaterialgrupocomissaovenda()){
				index2 = 0;
				for(Materialgrupocomissaovenda m2 : materialgrupo.getListaMaterialgrupocomissaovenda()){
					if(index != index2){
						boolean cargoIgual = (m2.getCargo() == null && m1.getCargo() == null) || 
							(m2.getCargo() != null && m1.getCargo() != null && m2.getCargo().equals(m1.getCargo()));
						boolean categoriaIgual = (m2.getCategoria() == null && m1.getCategoria() == null) || 
						(m2.getCategoria() != null && m2.getCategoria().getCdcategoria() != null && 
							m1.getCategoria() != null && m1.getCategoria().getCdcategoria() != null && m2.getCategoria().getCdcategoria().equals(m1.getCategoria().getCdcategoria()));
						boolean comissionamentoIgual = (m2.getComissionamento() == null && m1.getComissionamento() == null) || 
						(m2.getComissionamento() != null && m1.getComissionamento() != null && m2.getComissionamento().equals(m1.getComissionamento()));
						boolean documentotipoIgual = (m2.getDocumentotipo() == null && m1.getDocumentotipo() == null) || 
						(m2.getDocumentotipo() != null && m1.getDocumentotipo() != null && m2.getDocumentotipo().equals(m1.getDocumentotipo()));
						boolean pedidovendatipoIgual = (m2.getPedidovendatipo() == null && m1.getPedidovendatipo() == null) || 
						(m2.getPedidovendatipo() != null && m1.getPedidovendatipo() != null && m2.getPedidovendatipo().equals(m1.getPedidovendatipo()));
						boolean fornecedorIgual = (m2.getFornecedor() == null && m1.getFornecedor() == null) || 
						(m2.getFornecedor() != null && m1.getFornecedor() != null && m2.getFornecedor().equals(m1.getFornecedor()));
						
						duplicidade = cargoIgual && categoriaIgual && comissionamentoIgual && documentotipoIgual && pedidovendatipoIgual && fornecedorIgual;
					}
					if(duplicidade) break;
					index2++;
				}
				if(duplicidade) break;
				index++;
			}
		}
		
		return duplicidade;
	}
	
	public List<MaterialgrupoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialgrupoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialgrupoRESTModel> lista = new ArrayList<MaterialgrupoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialgrupo m : materialgrupoDAO.findForAndroid(whereIn))
				lista.add(new MaterialgrupoRESTModel(m));
		}
		
		return lista;
	}
	
	public List<MaterialGrupoW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de materialgrupo para a sincronizacao com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<MaterialGrupoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialGrupoW3producaoRESTModel> lista = new ArrayList<MaterialGrupoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialgrupo mg : materialgrupoDAO.findForW3Producao(whereIn))
				lista.add(new MaterialGrupoW3producaoRESTModel(mg));
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialgrupoDAO#findForSagefiscal(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Materialgrupo> findForSagefiscal(String whereIn) {
		return materialgrupoDAO.findForSagefiscal(whereIn);
	}
	
	public List<Materialgrupo> findForAnimal(){
		return materialgrupoDAO.findForAnimal();
	}
	
	public Materialgrupo findGerarBloco (String whereIn){
		return materialgrupoDAO.findGerarBloco(whereIn);
	}
	
	public Materialgrupo findforGerarBloco (Integer cdMaterialGrupo){
		return materialgrupoDAO.findforGerarBloco(cdMaterialGrupo);
	}
	
	public Materialgrupo findWithGradeEstoque (Materialgrupo materialgrupo){
		return materialgrupoDAO.findWithGradeEstoque(materialgrupo);
	}
	
	public boolean isGrupoGrade(Materialgrupo materialgrupo){
		if(Util.objects.isNotPersistent(materialgrupo)){
			return false;
		}
		return this.findWithGradeEstoque(materialgrupo).getGradeestoquetipo() != null;
	}
}
