package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.dao.ProvisaoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ConsultaSaldoEventoProvisionamentoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

public class ProvisaoService extends GenericService<Provisao>{

	private ProvisaoDAO provisaoDAO;
	private ColaboradorService colaboradorService;
	
	public void setProvisaoDAO(ProvisaoDAO provisaoDAO) {
		this.provisaoDAO = provisaoDAO;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	public void saveOrUpdateListNoUseTransaction(final List<Provisao> lista){
		for(Provisao provisao: lista){
			this.saveOrUpdateNoUseTransaction(provisao);
		}
	}
	
	public List<Provisao> findByColaboradorDespesaOrigem(Colaboradordespesa colaboradorDespesaOrigem){
		return provisaoDAO.findByColaboradorDespesaOrigem(colaboradorDespesaOrigem);
	}
	
	public void deleteProvisoes(String whereIn){
		provisaoDAO.deleteProvisoes(whereIn);
	}

	public Resource createRelatorioConsultaSaldoEventoProvisionamentoReportAnalitico(ConsultaSaldoEventoProvisionamentoReportFiltro filtro) throws Exception {
		List<Provisao> listaProvisao = findForSaldoEventoProvisao(filtro);
		return createRelatorioConsultaSaldoEventoProvisionamentoReport(filtro, listaProvisao, "analitico");
	}
	
	public Resource createRelatorioConsultaSaldoEventoProvisionamentoReportSintetico(ConsultaSaldoEventoProvisionamentoReportFiltro filtro) throws Exception {
		List<Provisao> listaProvisao = agruparProvisaoPorColaborador(findForSaldoEventoProvisao(filtro));
		return createRelatorioConsultaSaldoEventoProvisionamentoReport(filtro, listaProvisao, "sintetico");
	}
	
	public Resource createRelatorioConsultaSaldoEventoProvisionamentoReport(ConsultaSaldoEventoProvisionamentoReportFiltro filtro, List<Provisao> listaProvisao, String tipo) throws Exception {
		MergeReport mergeReport = new MergeReport("saldo_evento_provisionamento_" + tipo + "_" + SinedUtil.datePatternForReport() + ".pdf");
		Report report = null;
		
		if(SinedUtil.isListNotEmpty(listaProvisao)){
			Empresa empresa = null;
			List<Provisao> listaProvisaoPorEmpresa = null;
			Money total = new Money();
			
			for(Provisao provisao : listaProvisao){
				if(empresa == null || !empresa.equals(provisao.getEmpresa())){
					if(empresa != null && !empresa.equals(provisao.getEmpresa())){
						report.setDataSource(listaProvisaoPorEmpresa);
						report.addParameter("TOTAL", total);
						mergeReport.addReport(report);
						
						total = new Money();
					}
					
					report = criaReportParametroPadrao(filtro, provisao.getEmpresa(), tipo);
					empresa = provisao.getEmpresa();			
					listaProvisaoPorEmpresa = new ArrayList<Provisao>();
					listaProvisaoPorEmpresa.add(provisao);
				}else { 
					listaProvisaoPorEmpresa.add(provisao);
				}
				
				total = total.add(provisao.getValor());
			}
			
			report.addParameter("TOTAL", total);
			
			report.setDataSource(listaProvisaoPorEmpresa);
			mergeReport.addReport(report);
		} else {
			throw new SinedException("N�o existem dados para os filtros informados.");
		}
		
		return mergeReport.generateResource();
	}

	private List<Provisao> agruparProvisaoPorColaborador(List<Provisao> listaProvisao) {
		List<Provisao> listaAgrupada = new ArrayList<Provisao>();
		HashMap<Empresa, List<Provisao>> mapEmpresaProvisao = new HashMap<Empresa, List<Provisao>>();
		if(SinedUtil.isListNotEmpty(listaProvisao)){
			for(Provisao provisao : listaProvisao){
				if(provisao.getEmpresa() != null){
					if(provisao.getValor() == null){
						provisao.setValor(new Money());
					}
					if(mapEmpresaProvisao.get(provisao.getEmpresa()) == null){
						mapEmpresaProvisao.put(provisao.getEmpresa(), new ArrayList<Provisao>());
						mapEmpresaProvisao.get(provisao.getEmpresa()).add(provisao);
					}else {
						boolean encontrou = false;
						for(Provisao provisaoEmpresa : mapEmpresaProvisao.get(provisao.getEmpresa())){
							if(provisaoEmpresa.getColaborador() != null && 
									provisaoEmpresa.getValor() != null &&
									provisaoEmpresa.getValor().getValue().doubleValue() > 0 &&
									provisaoEmpresa.getColaborador().equals(provisao.getColaborador())){
								provisaoEmpresa.setValor(provisaoEmpresa.getValor().add(provisao.getValor()));
								encontrou = true;
							}
						}
						if(!encontrou){
							mapEmpresaProvisao.get(provisao.getEmpresa()).add(provisao);
						}
					}
				}
			}
			
			for(Empresa empresa : mapEmpresaProvisao.keySet()){
				listaAgrupada.addAll(mapEmpresaProvisao.get(empresa));
			}
		}
		return listaAgrupada;
	}
	private Report criaReportParametroPadrao(ConsultaSaldoEventoProvisionamentoReportFiltro filtro, Empresa empresa, String tipo) {
		Report report = new Report("/rh/consultaSaldoEventoProvisionamento_" + tipo);
		
		report.addParameter("LOGO", SinedUtil.getLogo(filtro.getEmpresa()));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("COLABORADOR", filtro.getColaborador() == null ? null: colaboradorService.loadWithoutWhereEmpresa(filtro.getColaborador()).getNome());
		report.addParameter("TITULO", "SALDO DE EVENTOS DE PROVISIONAMENTOS");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.addParameter("PERIODO", SinedDateUtils.getPeriodoDeAte(filtro.getDtinicio(), filtro.getDtfim()));
		
		return report;
	}

	private List<Provisao> findForSaldoEventoProvisao(ConsultaSaldoEventoProvisionamentoReportFiltro filtro) {
		return provisaoDAO.findForSaldoEventoProvisao(filtro);
	}
	
	public Money calcularValorTotalFechamentoFolha(Empresa empresa, String whereInEventoProvisionamento, Date dtInicio, Date dtFim) {
		return provisaoDAO.calcularValorTotalFechamentoFolha(empresa, whereInEventoProvisionamento, dtInicio, dtFim);
	}
	
	public List<Provisao> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro){
		return provisaoDAO.findForGerarLancamentoContabil(filtro);
	}
	
	public List<Provisao> findWithOrigem(String whereIn) {
		return provisaoDAO.findWithOrigem(whereIn);
	}
	public List<Provisao> findByIds(String whereIn) {
		return provisaoDAO.findByIds(whereIn);
	}
}
