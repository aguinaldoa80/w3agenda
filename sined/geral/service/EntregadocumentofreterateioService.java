package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.dao.EntregadocumentofreterateioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregadocumentofreterateioService extends GenericService<Entregadocumentofreterateio> {
	
	private EntregadocumentofreterateioDAO entregadocumentofreterateioDAO;
	
	public void setEntregadocumentofreterateioDAO(EntregadocumentofreterateioDAO entregadocumentofreterateioDAO) {
		this.entregadocumentofreterateioDAO = entregadocumentofreterateioDAO;
	}
	
	public List<Entregadocumentofreterateio> findByEntregadocumento(Entregadocumento entregadocumento){
		return entregadocumentofreterateioDAO.findByEntregadocumento(entregadocumento);
	}	
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregadocumentofreterateioDAO#findForCalcularCusto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 30/03/2016
	* @author Luiz Fernando
	*/
	public List<Entregadocumentofreterateio> findForCalcularCusto(String whereIn){
		return entregadocumentofreterateioDAO.findForCalcularCusto(whereIn);
	}
}
