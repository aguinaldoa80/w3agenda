package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupoemailService extends GenericService<Grupoemail>{

	@Override
	public void saveOrUpdate(Grupoemail bean) {
		this.ajustaListaGrupoemailColaborador(bean);
		super.saveOrUpdate(bean);
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Grupoemail bean) {
		this.ajustaListaGrupoemailColaborador(bean);
		super.saveOrUpdateNoUseTransaction(bean);
	}
	
	/**
	 * M�todo para remover os GrupoemailColaborador com colaboradores repetidos da listaGrupoemailColaborador.
	 * 
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	private void ajustaListaGrupoemailColaborador(Grupoemail bean){
		List<GrupoemailColaborador> listaGrupoemailColaborador = bean.getListaGrupoemailColaborador();
		if(SinedUtil.isListNotEmpty(listaGrupoemailColaborador)){
			List<GrupoemailColaborador> novaListaGrupoemailColaborador = new ArrayList<GrupoemailColaborador>();
			for (GrupoemailColaborador gec : listaGrupoemailColaborador) {
				if(!novaListaGrupoemailColaborador.contains(gec)){
					novaListaGrupoemailColaborador.add(gec);
				}
			}
			bean.setListaGrupoemailColaborador(novaListaGrupoemailColaborador);
		}
	}
}
