package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Beneficiovalor;
import br.com.linkcom.sined.geral.dao.BeneficiovalorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BeneficiovalorService extends GenericService<Beneficiovalor>{

	private BeneficiovalorDAO beneficiovalorDAO;
	private static BeneficiovalorService instance;
	
	public void setBeneficiovalorDAO(BeneficiovalorDAO beneficiovalorDAO) {
		this.beneficiovalorDAO = beneficiovalorDAO;
	}
	
	public static BeneficiovalorService getInstance() {
		if (instance==null){
			instance = Neo.getObject(BeneficiovalorService.class);
		}
		return instance;
	}
	
	public List<Beneficiovalor> findByCdsbeneficio(String whereIn){
		if (whereIn==null || whereIn.trim().equals("")){
			return new ArrayList<Beneficiovalor>();
		}
		return beneficiovalorDAO.findByCdsbeneficio(whereIn);
	}
}