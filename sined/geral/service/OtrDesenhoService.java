package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrDesenho;
import br.com.linkcom.sined.geral.dao.OtrDesenhoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrDesenhoService extends GenericService<OtrDesenho>{

	protected OtrDesenhoDAO otrDesenhoDao;

	public void setOtrDesenhoDao(OtrDesenhoDAO otrDesenhoDao) {
		this.otrDesenhoDao = otrDesenhoDao;
	}
	
	public List<OtrDesenho> findDesenho (){
		return otrDesenhoDao.findDesenho();
	}
}
