package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.BancoVO;
import br.com.linkcom.sined.geral.dao.BancoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.banco.BancoRESTModel;

public class BancoService extends GenericService<Banco> {

	private BancoDAO bancoDAO;
	
	public void setBancoDAO(BancoDAO bancoDAO) {
		this.bancoDAO = bancoDAO;
	}
	
	public BancoVO getBancoForRemessaConfiguravel(Banco banco){
		BancoVO bancoVO = new BancoVO();
		banco = this.load(banco);
		
		bancoVO.setNome(banco.getNome());
		bancoVO.setNumero(banco.getNumero().toString());
		
		return bancoVO;
	}
	
	/* singleton */
	private static BancoService instance;
	public static BancoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(BancoService.class);
		}
		return instance;
	}

	public Banco findByNumero(Integer numero) {
		return bancoDAO.findByNumero(numero);
	}
	
	public List<BancoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<BancoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<BancoRESTModel> lista = new ArrayList<BancoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Banco bean : bancoDAO.findForAndroid(whereIn))
				lista.add(new BancoRESTModel(bean));
		}
		
		return lista;
	}
	
}
