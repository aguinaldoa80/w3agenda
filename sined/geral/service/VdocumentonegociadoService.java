package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.dao.VdocumentonegociadoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VdocumentonegociadoService extends GenericService<Vdocumentonegociado> {

	private VdocumentonegociadoDAO vdocumentonegociadoDAO;
	
	public void setVdocumentonegociadoDAO(
			VdocumentonegociadoDAO vdocumentonegociadoDAO) {
		this.vdocumentonegociadoDAO = vdocumentonegociadoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VdocumentonegociadoDAO#findByDocumentos(String whereInDocumentos)
	 *
	 * @param whereInDocumentos
	 * @return
	 * @since 15/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vdocumentonegociado> findByDocumentos(String whereInDocumentos) {
		return vdocumentonegociadoDAO.findByDocumentos(whereInDocumentos);
	}
	
	/**
	 * M�todo que busca recursivamente documentos baixados em negocia��es. 
	 * Retorna true caso uma parcela de alguma das negocia��es esteja baixada, caso nenhuma esteja baixada retorna false.
	 * 
	 * @param whereInDocumentos
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean existeParcelasBaixadas(String whereInDocumentos){
		if(whereInDocumentos == null || whereInDocumentos.trim().isEmpty()){
			return false;
		}
		List<Vdocumentonegociado> parcelas = this.findByDocumentos(whereInDocumentos);
		List<Vdocumentonegociado> parcelasRenegociadas = new ListSet<Vdocumentonegociado>(Vdocumentonegociado.class);
		if(parcelas != null){
			for(Vdocumentonegociado vdocumentonegociado : parcelas){
				if(Documentoacao.NEGOCIADA.getCddocumentoacao().equals(vdocumentonegociado.getCddocumentoacaonegociado())){
					parcelasRenegociadas.add(vdocumentonegociado);
				} else if(Documentoacao.BAIXADA.getCddocumentoacao().equals(vdocumentonegociado.getCddocumentoacaonegociado())){
					return true;
				}
			}
		}
		if(!parcelasRenegociadas.isEmpty()){
			return existeParcelasBaixadas(SinedUtil.listAndConcatenate(parcelasRenegociadas, "parcelasRenegociadas", ","));
		}
		return false;
	}
	
	/**
	 * M�todo que monta o whereIn de documentos n�o baixados, inclu�ndo parcelas negociadas, a serem cancelados
	 * @param whereInDocumentos
	 * @param whereInParcelas
	 * @return
	 * @author Rafael Salvio
	 */
	public String montaWhereInParcelasNegociadas(String whereInDocumentos){
		return montaWhereInParcelasNegociadas(whereInDocumentos, new StringBuilder().append(whereInDocumentos));
	}
	
	/**
	 * M�todo que monta o whereIn de documentos n�o baixados, inclu�ndo parcelas negociadas, a serem cancelados
	 * @param whereInDocumentos
	 * @param whereInParcelas
	 * @return
	 * @author Rafael Salvio
	 */
	private String montaWhereInParcelasNegociadas(String whereInDocumentos, StringBuilder whereInParcelas){
		List<Vdocumentonegociado> parcelas = this.findByDocumentos(whereInDocumentos);
		List<Vdocumentonegociado> listaParcelasRenegociadas = new ListSet<Vdocumentonegociado>(Vdocumentonegociado.class);
		if(parcelas != null){
			for(Vdocumentonegociado parcela : parcelas){
				if(Documentoacao.NEGOCIADA.equals(parcela.getCddocumentoacaonegociado())){
					listaParcelasRenegociadas.add(parcela);
				}
			}
		}
			
		whereInParcelas.append("," + SinedUtil.listAndConcatenate(parcelas, "cddocumentonegociado", ","));
		if(!listaParcelasRenegociadas.isEmpty()){
			return montaWhereInParcelasNegociadas(SinedUtil.listAndConcatenate(listaParcelasRenegociadas, "cddocumentonegociado", ","), whereInParcelas);
		} else{			
			return whereInParcelas.toString();		
		}
	}
	
	public List<Vdocumentonegociado> findByDocumentosnegociados(String whereInDocumentosnegociados) {
		return vdocumentonegociadoDAO.findByDocumentosnegociados(whereInDocumentosnegociados);
	}
	
	public List<Vdocumentonegociado> findByDocumentosnegociadosRaiz(String whereInParcelas){
		return vdocumentonegociadoDAO.findByDocumentosnegociadosRaiz(whereInParcelas);
	}
}
