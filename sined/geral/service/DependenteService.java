package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Dependente;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DependenteService extends GenericService<Dependente> {	
	
//	/**
//	 * M�todo para salvar cada dependente da lista cadastrada 
//	 * 
//	 * @param listaColaboradordependente
//	 * @author Jo�o Paulo Zica
//	 */
//	public void saveDependente(Collection<Colaboradordependente> listaColaboradordependente){
//		if (listaColaboradordependente!=null){
//			for (Colaboradordependente colaboradordependente : listaColaboradordependente) {
//				Dependente dependente = colaboradordependente.getDependente();
//				saveOrUpdate(dependente);
//			}
//		}
//	}
	
	/* singleton */
	private static DependenteService instance;
	public static DependenteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DependenteService.class);
		}
		return instance;
	}
	
}
