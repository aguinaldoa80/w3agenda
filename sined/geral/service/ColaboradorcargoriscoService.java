package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargorisco;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorcargoriscoService extends GenericService<Colaboradorcargorisco> {	
	
	/* singleton */
	private static ColaboradorcargoriscoService instance;
	public static ColaboradorcargoriscoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoriscoService.class);
		}
		return instance;
	}
	
}
