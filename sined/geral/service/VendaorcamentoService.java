package br.com.linkcom.sined.geral.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.util.pdf.WKHTMLBuilder;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.ObjectUtils;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Ecommaterialidentificador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoItemHistorico;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupocomissaovenda;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Orcamentovalorcampoextra;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pedidovendavalorcampoextra;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.VendaOrcamentoFornecedorTicketMedio;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentohistorico;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterialseparacao;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.bean.Vendavalorcampoextra;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_VendamaterialTabelapreco;
import br.com.linkcom.sined.geral.bean.auxiliar.VendaFornecedorTicketMedioBean;
import br.com.linkcom.sined.geral.bean.enumeration.CampoComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaVendaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoiniciodata;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.VendaorcamentoDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaClienteRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaContaRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaEmpresaRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitItemRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitflexivelItemRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitflexivelRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaOutrosRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaPagamentoRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaRTF;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.CondicaoPagamentoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EnderecoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutoMestreGradeComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitflexivelComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitflexivelitemComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokititemComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ResumoMaterialunidadeconversao;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.TotaisComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaItemBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.ImpostoUtil;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.VelocityUtil;
import br.com.linkcom.sined.util.neo.ComprovanteTxtFileServlet;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

public class VendaorcamentoService extends GenericService<Vendaorcamento> {
	
	private VendaorcamentoDAO vendaorcamentoDAO;
	private VendaorcamentomaterialService vendaorcamentomaterialService;
	private ColaboradorService colaboradorService;
	private ClienteService clienteService;
	private PessoaService pessoaService;
	private MaterialService materialService;
	private EmpresaService empresaService;
	private VendaorcamentohistoricoService vendaorcamentohistoricoService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private UnidademedidaService unidademedidaService;
	private ParametrogeralService parametrogeralService;
	private VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private UsuarioService usuarioService;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService;
	private MaterialsimilarService materialsimilarService;
	private VendaService vendaService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private ContatoService contatoService;
	private VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService;
	private PrazopagamentoService prazopagamentoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private PedidovendaService pedidovendaService;
	private OrcamentovalorcampoextraService orcamentovalorcampoextraService;
	private VendaorcamentomaterialseparacaoService vendaorcamentomaterialseparacaoService;
	private MaterialrelacionadoService materialrelacionadoService;
	private ConfiguracaoecomService configuracaoecomService;
	private EcommaterialidentificadorService ecommaterialidentificadorService;
	private EmpresarepresentacaoService empresarepresentacaoService;
	private FornecedorService fornecedorService;
	private LocalarmazenagemService localarmazenagemService;
	private MaterialgrupocomissaovendaService materialgrupocomissaovendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private PedidovendatipoService pedidovendatipoService;
	private DocumentotipoService documentotipoService;
	private MaterialunidademedidaService materialunidademedidaService;
	private OportunidadeService oportunidadeService;
	private ContacrmService contacrmService;
	private ProjetoService projetoService;
	private ClientevendedorService clientevendedorService;
	private ContratoService contratoService;
	private PessoaContatoService pessoaContatoService;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService;
	private MaterialdevolucaoService materialdevolucaoService;
	private ProdutoService produtoService;
	
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setVendaorcamentohistoricoService(VendaorcamentohistoricoService vendaorcamentohistoricoService) {this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;}
	public void setVendaorcamentomaterialService(VendaorcamentomaterialService vendaorcamentomaterialService) {this.vendaorcamentomaterialService = vendaorcamentomaterialService;}
	public void setVendaorcamentoDAO(VendaorcamentoDAO vendaorcamentoDAO) {this.vendaorcamentoDAO = vendaorcamentoDAO;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setVendaorcamentoformapagamentoService(VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService) {this.vendaorcamentoformapagamentoService = vendaorcamentoformapagamentoService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setMaterialtabelaprecoitemService(MaterialtabelaprecoitemService materialtabelaprecoitemService) {this.materialtabelaprecoitemService = materialtabelaprecoitemService;}
	public void setMaterialsimilarService(MaterialsimilarService materialsimilarService) {this.materialsimilarService = materialsimilarService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setVendaorcamentomaterialmestreService(VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService) {this.vendaorcamentomaterialmestreService = vendaorcamentomaterialmestreService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setOrcamentovalorcampoextraService(OrcamentovalorcampoextraService orcamentovalorcampoextraService) {this.orcamentovalorcampoextraService = orcamentovalorcampoextraService;}
	public void setVendaorcamentomaterialseparacaoService(VendaorcamentomaterialseparacaoService vendaorcamentomaterialseparacaoService) {this.vendaorcamentomaterialseparacaoService = vendaorcamentomaterialseparacaoService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setConfiguracaoecomService(ConfiguracaoecomService configuracaoecomService) {this.configuracaoecomService = configuracaoecomService;}
	public void setEcommaterialidentificadorService(EcommaterialidentificadorService ecommaterialidentificadorService) {this.ecommaterialidentificadorService = ecommaterialidentificadorService;}
	public void setEmpresarepresentacaoService(EmpresarepresentacaoService empresarepresentacaoService) {this.empresarepresentacaoService = empresarepresentacaoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setMaterialgrupocomissaovendaService(MaterialgrupocomissaovendaService materialgrupocomissaovendaService) {this.materialgrupocomissaovendaService = materialgrupocomissaovendaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setContacrmService(ContacrmService contacrmService) {this.contacrmService = contacrmService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setMaterialTabelaPrecoItemHistoricoService(MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService) {this.materialTabelaPrecoItemHistoricoService = materialTabelaPrecoItemHistoricoService;}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {this.materialdevolucaoService = materialdevolucaoService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param id
	 * @return
	 * @author Ramon Brazil
	 */
	public Vendaorcamento loadVendaorcamento(Integer id){
		return vendaorcamentoDAO.loadVendaorcamento(id);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @since 29/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamento> findVendaorcamentoReport(String whereIn){
		return vendaorcamentoDAO.findVendaorcamentoReport(whereIn);
	}
	/**
	 * M�todo responsavel por gerar o comprovante dde venda
	 * @see br.com.linkcom.sined.geral.service.VendamaterialService#findByVenda(Venda)
	 * @see br.com.linkcom.sined.geral.service.DocumentoorigemService#getListaDocumentosDeVenda(Venda)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#load(Colaborador)
	 * @param filtro
	 * @return
	 * @author Ramon Brazil
	 */
	public Report gerarComprovante(VendaorcamentoFiltro filtro){		
		Vendaorcamento vendaorcamento = this.loadVendaorcamento(filtro.getCdvendaorcamento());
		return criaComprovanteVendaorcamentoReport(vendaorcamento);
	}
	
	/**
	 * M�todo que gera o comprovante dde v�rias vendas
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaorcamentoService#findVendaorcamentoReport(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.VendaService.criaComprovanteVendaReport(Venda venda)
	 * 
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarVariosComprovantes(VendaorcamentoFiltro filtro) throws Exception {
		MergeReport mergeReport = new MergeReport("comprovantes_vendasorcamento_"+SinedUtil.datePatternForReport()+".pdf");
		
		String whereIn = filtro.getWhereIn();
		
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		List<Vendaorcamento> listaVendaorcamento = this.findVendaorcamentoReport(whereIn);
		
		if(listaVendaorcamento != null && !listaVendaorcamento.isEmpty()){
			for(Vendaorcamento vendaorcamento : listaVendaorcamento){
				mergeReport.addReport(this.criaComprovanteVendaorcamentoReport(vendaorcamento));
			}
		}		
		return mergeReport.generateResource();
	}
	
	/**
	 * Gera os comprovantes de venda configur�veis para mais de um pedido de venda
	 * @param request
	 * @param comprovante
	 * @param whereIn
	 * @return
	 */
	public ModelAndView gerarComprovantesConfiguraveis(WebRequestContext request, String whereIn, ComprovanteConfiguravel comprovante) {
		List<Vendaorcamento> listaVendaorcamento = this.findVendaorcamentoReport(whereIn);
		StringBuilder comprovanteHTML = new StringBuilder();
		
		boolean emitirTXT = comprovante.getTxt() != null && comprovante.getTxt();
		
		for(int i=0; i<listaVendaorcamento.size(); i++ ){
			Vendaorcamento vendaorcamento = listaVendaorcamento.get(i);
			
			if(!emitirTXT){
				if(i != (listaVendaorcamento.size()-1) )
					comprovanteHTML.append("<div style='page-break-after: always;'>");
				else comprovanteHTML.append("<div>");
			}
			
			Map<String, Object> map = gerarMapaComprovanteVendaorcamentoVariavel(vendaorcamento);
			try {
				comprovanteHTML.append(VelocityUtil.renderTemplate(comprovante.getLayout(), map));
				if(!emitirTXT){
					comprovanteHTML.append("</div>");
				}
			} catch (Throwable e) {
				e.printStackTrace();
				throw new SinedException("Erro ao gerar o comprovante.");
			} 

		}
		
		String print = comprovanteHTML.toString()+"<script>javascript:window.print();</script>";
		if(emitirTXT){
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ComprovanteTxtFileServlet.add(request.getSession(), key, comprovanteHTML.toString());
			print = "<script>window.opener.location = '" + request.getServletRequest().getContextPath() + "/COMPROVANTETXT/" + key + "'; window.close();</script>";
		}
		
		request.getServletResponse().setContentType("text/html");
		try {
			request.getServletResponse().getWriter().print(print);
		} catch (IOException e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		}
	
		return null;
	}
	/**
	 * Gera o Comprovante configur�vel de uma venda
	 * @param request
	 * @param filtro
	 * @param templateContent
	 * @return
	 */
	public ModelAndView gerarComprovanteConfiguravel(WebRequestContext request, Integer cdvendaorcamento, ComprovanteConfiguravel comprovante) {
		Vendaorcamento vendaorcamento = this.loadVendaorcamento(cdvendaorcamento);
		StringBuilder comprovanteHTML = new StringBuilder();
		
		boolean emitirTXT = comprovante.getTxt() != null && comprovante.getTxt();
		
		try {
			comprovanteHTML.append(this.gerarComprovanteConfiguravelOnly(comprovante,vendaorcamento));
			if(!emitirTXT){
				comprovanteHTML.append("</div>");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		} 
		
		String print = comprovanteHTML.toString()+"<script>javascript:window.print();</script>";
		if(emitirTXT){
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ComprovanteTxtFileServlet.add(request.getSession(), key, comprovanteHTML.toString());
			print = "<script>window.open('" + request.getServletRequest().getContextPath() + "/COMPROVANTETXT/" + key + "');window.close();</script>";
		}
		
		request.getServletResponse().setContentType("text/html");
		try {
			request.getServletResponse().getWriter().print(print);
		} catch (IOException e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		}
	
		return null;
	}
	
	private String gerarComprovanteConfiguravelOnly(ComprovanteConfiguravel comprovante, Vendaorcamento vendaorcamento)throws Exception {
		return VelocityUtil.renderTemplate(comprovante.getLayout(), this.gerarMapaComprovanteVendaorcamentoVariavel(vendaorcamento));
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> gerarMapaComprovanteVendaorcamentoVariavel(Vendaorcamento vendaorcamento) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String logoNaoDisponivel = "'" + SinedUtil.getUrlWithContext() +  "/imagens/imgnaodisponivel.jpg'";
		List<Vendaorcamentomaterial> listaprodutos = vendaorcamentomaterialService.findByMaterial(vendaorcamento, "vendaorcamentomaterial.ordem, vendaorcamentomaterial.cdvendaorcamentomaterial, listaCaracteristica.cdmaterialcaracteristica");
		List<Vendaorcamentomaterialmestre> listaVendaorcamentomaterialmestre = vendaorcamentomaterialmestreService.findByVendaOrcamento(vendaorcamento);
		List<Vendaorcamentoformapagamento> listaVendaorcamentoFormaPagamento = vendaorcamentoformapagamentoService.findByVendaorcamento(vendaorcamento);
		
		
		vendaorcamento.setListavendaorcamentomaterial(listaprodutos);
//		this.ordenarMaterialproducao(vendaorcamento);
		
		vendaorcamento.setListaOrcamentovalorcampoextra(orcamentovalorcampoextraService.findByVendaorcamento(vendaorcamento));
		
		Colaborador colaborador = colaboradorService.loadWithoutPermissao(vendaorcamento.getColaborador(), "colaborador.cdpessoa, colaborador.nome, colaborador.email");
		colaborador.setListaTelefone(SinedUtil.listToSet(telefoneService.carregarListaTelefone(colaborador), Telefone.class));
		Usuario usuario = usuarioService.findUsuariocolaborador(colaborador.getCdpessoa());
		colaborador.setApelido(usuario !=null && usuario.getApelido()!=null ? usuario.getApelido() : "");
		
		Cliente cliente = vendaorcamento.getCliente() != null ? clienteService.carregarDadosCliente(vendaorcamento.getCliente()) : null;
		if(cliente != null){
			cliente.setNome(pessoaService.carregaPessoa(new Pessoa(cliente.getCdpessoa())).getNome());
		}
		
		Contato contato = null;
		if(vendaorcamento.getContato() != null){
			contato = contatoService.carregaContato(vendaorcamento.getContato()); 
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(CampoComprovanteConfiguravel.TITULO.getNome(), "OR�AMENTO");
		if(vendaorcamento.getVendaorcamentosituacao() != null && vendaorcamento.getVendaorcamentosituacao().getCdvendaorcamentosituacao() != null && vendaorcamento.getVendaorcamentosituacao().getDescricao() != null){
			map.put(CampoComprovanteConfiguravel.VENDA_SITUACAO.getNome(), vendaorcamento.getVendaorcamentosituacao().getCdvendaorcamentosituacao());
			map.put(CampoComprovanteConfiguravel.VENDA_SITUACAONOME.getNome(), vendaorcamento.getVendaorcamentosituacao().getDescricao());
		}
		map.put(CampoComprovanteConfiguravel.VENDEDOR_NOME.getNome(), colaborador.getNome());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_EMAIL.getNome(), colaborador.getEmail());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_CODIGO.getNome(), colaborador.getCdpessoa());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_NOMEEXIBICAO.getNome(), colaborador.getApelido()!=null ? colaborador.getApelido() : colaborador.getNome());
		
		String telefonesVendedor = colaborador.getTelefonesSemQuebraLinha();
		map.put(CampoComprovanteConfiguravel.VENDEDOR_TELEFONES.getNome(), telefonesVendedor == null ? "" : telefonesVendedor);
		
		map.put(CampoComprovanteConfiguravel.CONTA_NOME.getNome(), vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getNome() : "");
		map.put(CampoComprovanteConfiguravel.CONTA_CPF.getNome(), vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getCpf() : "");
		map.put(CampoComprovanteConfiguravel.CONTA_CNPJ.getNome(), vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getCnpj() : "");
		
		map.put(CampoComprovanteConfiguravel.CLIENTE_IDENTIFICADOR.getNome(), cliente != null ? (cliente.getIdentificador()==null ? "" : cliente.getIdentificador()) : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_NOME.getNome(), cliente != null ? cliente.getNome() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_RAZAOSOCIAL.getNome(), cliente != null ? cliente.getRazaosocial() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_CPF.getNome(), cliente != null ? cliente.getCpf() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_CNPJ.getNome(), cliente != null ? cliente.getCnpj() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_EMAIL.getNome(), cliente != null ? cliente.getEmail() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_ENDERECO.getNome(), cliente != null ? (cliente.getEndereco()==null ? "" : cliente.getEndereco().getLogradouroCompletoComCep()) : "");
		if(cliente != null && cliente.getEndereco() != null && cliente.getEndereco().getMunicipio() != null && cliente.getEndereco().getMunicipio().getUf() != null && cliente.getEndereco().getMunicipio().getUf().getSigla() != null){
			map.put(CampoComprovanteConfiguravel.CLIENTE_UF_SIGLA.getNome(), cliente.getEndereco().getMunicipio().getUf().getSigla());			
		}else {
			map.put(CampoComprovanteConfiguravel.CLIENTE_UF_SIGLA.getNome(), "");
		}
		map.put(CampoComprovanteConfiguravel.CLIENTE_INSCRICAOESTADUAL.getNome(), cliente != null ? (cliente.getInscricaoestadual() == null ? "" : cliente.getInscricaoestadual()) : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_OBSERVACAO.getNome(), cliente != null && cliente.getObservacao() != null ? cliente.getObservacao() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_RG.getNome(), cliente != null && cliente.getRg() != null ? cliente.getRg() : "");
		
		List<EnderecoComprovanteConfiguravel> listaEndereco = new ArrayList<EnderecoComprovanteConfiguravel>();
		if(cliente != null && cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){			
			for (Endereco endereco : cliente.getListaEndereco()){
				if(endereco.getLogradouroCompletoComCep()!=null && endereco.getEnderecotipo()!=null){
					EnderecoComprovanteConfiguravel ecc = new EnderecoComprovanteConfiguravel();
					ecc.setENDERECOCOMPLETO(endereco.getLogradouroCompletoComCep());
					ecc.setENDERECOTIPO(endereco.getEnderecotipo().getNome()!=null ? endereco.getEnderecotipo().getNome() : "");
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null){
						ecc.setENDERECOUFSIGLA(endereco.getMunicipio().getUf().getSigla());						
					}else {
						ecc.setENDERECOUFSIGLA("");	
					}
					listaEndereco.add(ecc);
				}
			}			
		}
		map.put(CampoComprovanteConfiguravel.ENDERECOCLIENTE_LISTA.getNome(),listaEndereco);
		
		Contato contatoresponsavel = null;
		if(cliente != null){
			contatoresponsavel = clienteService.getContatoresponsavelByCliente(cliente);
		}
		map.put(CampoComprovanteConfiguravel.CLIENTE_RESPONSAVEL.getNome(), contatoresponsavel != null && contatoresponsavel.getNome() != null ? contatoresponsavel.getNome() : "");
		
		String telefones = null;
		if(cliente != null){
			telefones = cliente.getTelefonesSemQuebraLinha();
		}
		map.put(CampoComprovanteConfiguravel.CLIENTE_TELEFONES.getNome(), telefones==null ? "" : telefones);
		
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATONOME.getNome(), contato == null ? "" : contato.getNome());
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATOEMAIL.getNome(), contato != null && contato.getEmailcontato() != null ? contato.getEmailcontato() : "");
		String telefonesContato = contato != null ? contato.getTelefonesSemQuebraLinha() : null;
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATOTELEFONES.getNome(), telefonesContato==null ? "" : telefonesContato);
		
		map.put(CampoComprovanteConfiguravel.VENDA_CODIGO.getNome(), vendaorcamento.getCdvendaorcamento());
		map.put(CampoComprovanteConfiguravel.VENDA_DATA.getNome(), vendaorcamento.getDtorcamento() != null ? sdf.format(vendaorcamento.getDtorcamento()) : null);
		map.put(CampoComprovanteConfiguravel.VENDA_DATA_HORA.getNome(), vendaorcamento.getDtorcamento() != null ? sdfHora.format(vendaorcamento.getDtorcamento()) : null);
		map.put(CampoComprovanteConfiguravel.VENDA_ENDERECO.getNome(), vendaorcamento.getEndereco() != null ? vendaorcamento.getEndereco().getLogradouroCompletoComCep() : "");
		map.put(CampoComprovanteConfiguravel.VENDA_IDENTIFICADOR.getNome(), vendaorcamento.getIdentificador() );
		map.put(CampoComprovanteConfiguravel.VENDA_IDENTIFICADOREXTERNO.getNome(), vendaorcamento.getIdentificadorexterno());
		map.put(CampoComprovanteConfiguravel.VENDA_PROJETO_NOME.getNome(), vendaorcamento.getProjeto() != null && vendaorcamento.getProjeto().getNome() != null ? vendaorcamento.getProjeto().getNome() : "");
		map.put(CampoComprovanteConfiguravel.VENDA_DATAHORAATUALIZACAO.getNome(), sdfHora.format(vendaorcamento.getDtaltera()));
		map.put(CampoComprovanteConfiguravel.VENDA_TIPO.getNome(), vendaorcamento.getPedidovendatipo()!=null && vendaorcamento.getPedidovendatipo().getDescricao() != null ? vendaorcamento.getPedidovendatipo().getDescricao() : "");
		
		Map<String, String> camposAdicionais = new HashMap<String, String>();
		if (vendaorcamento.getListaOrcamentovalorcampoextra() != null){
			for (Orcamentovalorcampoextra campoextra : vendaorcamento.getListaOrcamentovalorcampoextra()){
				camposAdicionais.put(campoextra.getCampoextrapedidovendatipo().getNome(), campoextra.getValor());
			}
		}
		map.put(CampoComprovanteConfiguravel.VENDA_CAMPOS_ADICIONAIS.getNome(), camposAdicionais);
		
		for (Vendaorcamentomaterial vendaorcamentomaterial : listaprodutos) {
            Money valorTotal = vendaService.getValortotal(vendaorcamentomaterial.getPreco(), vendaorcamentomaterial.getQuantidade(), vendaorcamentomaterial.getDesconto(),
                    vendaorcamentomaterial.getMultiplicador(), vendaorcamentomaterial.getOutrasdespesas(), vendaorcamentomaterial.getValorSeguro());
			vendaorcamentomaterial.setTotal(valorTotal);
		}
		
		BigDecimal pesoliquidototal = new BigDecimal(0);
		BigDecimal pesobrutototal = new BigDecimal(0);
		Double qtdetotalmetrocubico = 0.0;
		Double qtdemetrocubico = 0.0;
		Double fracao = 1.0;
		Double qtdeProduto = 0.0, qtdeServico = 0.0;
		Money valorTotalIpi = new Money();
		Money valorTotalIcms = new Money();
		Money valorTotalIcmsSt = new Money();
		Money valorTotalDesoneracaoIcms = new Money();
		Money valorTotalFcp = new Money();
		Money valorTotalFcpSt = new Money();
		Money valorTotalDifal = new Money();
//		Money totalmetrocubico = new Money();
		Material material;
		Money valorTotalProdutos = new Money();
		List<ProdutoComprovanteConfiguravel> listaProdutosmestre = new ArrayList<ProdutoComprovanteConfiguravel>();
		List<ProdutoComprovanteConfiguravel> listaProdutosComprovante = new ArrayList<ProdutoComprovanteConfiguravel>();
		List<TotaisComprovanteConfiguravel> listaTotaisGrupomaterialComprovante = new ArrayList<TotaisComprovanteConfiguravel>();
		List<ProdutokitComprovanteConfiguravel> listaProdutoskit = new ArrayList<ProdutokitComprovanteConfiguravel>();
		List<ProdutokitflexivelComprovanteConfiguravel> listaProdutoskitflexivel = new ArrayList<ProdutokitflexivelComprovanteConfiguravel>();
		List<TotaisComprovanteConfiguravel> listaTotaisGrupomaterialmestreComprovante = new ArrayList<TotaisComprovanteConfiguravel>();
		List<ProdutoMestreGradeComprovanteConfiguravel> listaProdutosMestreGrade = new ArrayList<ProdutoMestreGradeComprovanteConfiguravel>();
		
		for(Vendaorcamentomaterial vm : listaprodutos){
			ProdutoComprovanteConfiguravel pcc = new ProdutoComprovanteConfiguravel();
			if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
				pcc.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
			}
			if(vm.getMaterial() != null && vm.getMaterial().getPesobruto() != null){
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				if(vm.getMaterial().getPesobruto() != null){
					Double pesoBruto = vm.getMaterial().getPesobruto();
					if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
							vm.getMaterial().getUnidademedida() != null && 
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
					}
					pcc.setPESOBRUTO(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)).toString());
					pesobrutototal =  pesoliquidototal.add(new BigDecimal(pesoBruto * qtde));
				}
			}
			
			if(vm.getMaterial().getMaterialmestregrade() != null) {
				ProdutoMestreGradeComprovanteConfiguravel produtoMestreGrade = new ProdutoMestreGradeComprovanteConfiguravel();
				produtoMestreGrade.setCODIGO(vm.getMaterial().getMaterialmestregrade().getCdmaterial().toString());
				if(!listaProdutosMestreGrade.contains(produtoMestreGrade)){
					produtoMestreGrade.setDESCRICAO(vm.getMaterial().getMaterialmestregrade().getNome());
					produtoMestreGrade.setIDENTIFICADORMATERIALMESTRE(vm.getMaterial().getMaterialmestregrade().getIdentificacao());
					listaProdutosMestreGrade.add(produtoMestreGrade);
				}			
				pcc.setCDMATERIALMESTREGRADE(vm.getMaterial().getMaterialmestregrade().getCdmaterial().toString());
			}
			
			if(vm.getPesoVendaOuMaterial() != null){
				pcc.setPESOLIQUIDO(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())).toString());
				pesoliquidototal = pesoliquidototal.add(new BigDecimal(vm.getQuantidade() * vm.getPesoVendaOuMaterial()));
			}
			if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
				pcc.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
			}
			
			pcc.setCDMATERIAL(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCODIGO(vm.getMaterial().getIdentificacaoOuCdmaterial());
			pcc.setIDENTIFICADOR(vm.getMaterial().getIdentificacao());
			pcc.setDESCRICAO(vm.getMaterial().getNome());
			pcc.setCDMATERIALTIPO(vm.getMaterial().getMaterialtipo() != null ? vm.getMaterial().getMaterialtipo().getCdmaterialtipo() : null);
			pcc.setCDMATERIALCATEGORIA(vm.getMaterial().getMaterialcategoria() != null ? vm.getMaterial().getMaterialcategoria().getCdmaterialcategoria() : null);
			pcc.setUNIDADEMEDIDA(vm.getUnidademedida().getNome());
			pcc.setUNIDADEMEDIDASIMBOLO(vm.getUnidademedida().getSimbolo());
			pcc.setPRECO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
			pcc.setQUANTIDADE(vm.getQuantidade());
			pcc.setDESCONTO(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );
			pcc.setENTREGA(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
			pcc.setTOTAL(vm.getTotal().toString());
			pcc.setOBSERVACAO(vm.getObservacao());
			pcc.setNCM(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null ? vm.getMaterial().getNcmcapitulo().getCodeFormat() : "");
			pcc.setNCMCOMPLETO(vm.getMaterial()!=null && vm.getMaterial().getNcmcompleto()!=null ? vm.getMaterial().getNcmcompleto() : "");
			pcc.setNCMDESCRICAO(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null && vm.getMaterial().getNcmcapitulo().getDescricao()!=null ? vm.getMaterial().getNcmcapitulo().getDescricao() : "");
			pcc.setIDENTIFICADORMATERIALMESTRE(vm.getIdentificadorinterno() != null ? vm.getIdentificadorinterno() : "");
            pcc.setVALORIPI(vm.getValoripi() != null ? vm.getValoripi().getValue().doubleValue() : null);
            pcc.setVALORIPI(MoneyUtils.moneyToDouble(vm.getValoripi()));
            pcc.setIPI(vm.getIpi());
            pcc.setVALORICMS(MoneyUtils.moneyToDouble(vm.getValoricms()));
            pcc.setICMS(vm.getIcms());
            pcc.setVALORICMSST(MoneyUtils.moneyToDouble(vm.getValoricmsst()));
            pcc.setICMSST(vm.getIcmsst());
            pcc.setVALORFCP(MoneyUtils.moneyToDouble(vm.getValorfcp()));
            pcc.setFCP(vm.getFcp());
            pcc.setVALORFCPST(MoneyUtils.moneyToDouble(vm.getValorfcpst()));
            pcc.setFCPST(vm.getFcpst());
            pcc.setVALORDIFAL(MoneyUtils.moneyToDouble(vm.getValordifal()));
            pcc.setDIFAL(vm.getDifal());
			pcc.setOUTRASDESPESAS(MoneyUtils.moneyToDouble(vm.getOutrasdespesas()));
			pcc.setSEGURO(MoneyUtils.moneyToDouble(vm.getValorSeguro()));
			
			valorTotalIpi = valorTotalIpi.add(vm.getValoripi());
			valorTotalIcms = valorTotalIcms.add(vm.getValoricms());
			valorTotalIcmsSt = valorTotalIcmsSt.add(vm.getValoricmsst());
			valorTotalDesoneracaoIcms = valorTotalDesoneracaoIcms.add(vm.getValordesoneracaoicms());
			valorTotalFcp = valorTotalFcp.add(vm.getValorfcp());
			valorTotalFcpSt = valorTotalFcpSt.add(vm.getValorfcpst());
			valorTotalDifal = valorTotalDifal.add(vm.getValordifal());
			
			if(vm.getComprimentooriginal() != null){
				pcc.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
			}
			if(vm.getLargura() != null){
				pcc.setLARGURA(vm.getLargura().toString());
			}
			if(vm.getAltura() != null){
				pcc.setALTURA(vm.getAltura().toString());
			}
			if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
					!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
				pcc.setQUANTIDADEUNIDADEPRINCIPAL(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
			}
			if(vm.getLoteestoque() != null){
				pcc.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
			}
			if(vm.getMaterial().getMaterialgrupo() != null && vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null){
				pcc.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
			}
			
			qtdemetrocubico = 1.0;
			fracao = 1.0;
			
			
			if(vm.getUnidademedida() != null && vm.getUnidademedida().getSimbolo() != null && "M3".equalsIgnoreCase(vm.getUnidademedida().getSimbolo())){
				pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(vm.getQuantidade()),2));
				qtdetotalmetrocubico += vm.getQtdeMetrocunico(vm.getQuantidade());
				pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
//				totalmetrocubico = totalmetrocubico.add(new Money(vm.getQuantidade()*vm.getPreco()));
			}else {
				if(vm.getMaterial().getUnidademedida() != null && vm.getMaterial().getUnidademedida().getSimbolo() != null && 
						"M3".equalsIgnoreCase(vm.getMaterial().getUnidademedida().getSimbolo())){
					if(vm.getFatorconversao() != null && vm.getFatorconversao() > 0){
						qtdemetrocubico = vm.getQuantidade();
						pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(qtdemetrocubico),2));
						qtdetotalmetrocubico += vm.getQtdeMetrocunico(qtdemetrocubico);
						pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco()*vm.getFatorconversaoQtdereferencia())).format(vm.getPreco()*vm.getFatorconversaoQtdereferencia()));
//						totalmetrocubico = totalmetrocubico.add(new Money(qtdemetrocubico* (vm.getPreco()*vm.getFatorconversao())));
					}
				}else {
					material = materialService.findListaMaterialunidademedida(vm.getMaterial());
					if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
						for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
							if(materialunidademedida.getUnidademedida() != null && 
									"M3".equalsIgnoreCase(materialunidademedida.getUnidademedida().getSimbolo())){
								if(vm.getFatorconversao() != null && vm.getFatorconversao() > 0){
									fracao = vm.getFatorconversao();
								}else {
									fracao = unidademedidaService.getFatorconversao(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), materialunidademedida.getUnidademedida(), material, vm.getFatorconversao(), vm.getQtdereferencia());
								}
								if(fracao != null && fracao > 0){
									qtdemetrocubico = vm.getQuantidade() / fracao;
									pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(qtdemetrocubico),2));
									qtdetotalmetrocubico += vm.getQtdeMetrocunico(qtdemetrocubico);
									pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco()*vm.getFatorconversaoQtdereferencia())).format(vm.getPreco()*vm.getFatorconversaoQtdereferencia()));
//									totalmetrocubico = totalmetrocubico.add(new Money(qtdemetrocubico * (vm.getPreco()/fracao)));
								}
							}
						}
					}
				}
			}
			
			if(vm.getMaterial() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto()){
				qtdeProduto += vm.getQuantidade();
			}
			if(vm.getMaterial() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico()){
				qtdeServico += vm.getQuantidade();
			}
			Money total = new Money(SinedUtil.round(vm.getTotal().getValue(), 2));
			valorTotalProdutos = valorTotalProdutos.add(total);
			
			if(vm.getMaterial() != null && vm.getMaterial().getLocalizacaoestoque() != null && vm.getMaterial().getLocalizacaoestoque().getDescricao() != null){
				pcc.setLOCALIZACAOESTOQUE(vm.getMaterial().getLocalizacaoestoque().getDescricao());
			}
			
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getCdmaterial() != null){
				pcc.setCDMATERIALMESTRE(vm.getMaterialmestre().getCdmaterial().toString());
			}
			
			if (vm.getFornecedor()!=null && vm.getFornecedor().getNome()!=null){
				String nomefornecedor = vm.getFornecedor().getRazaosocial() != null && !vm.getFornecedor().getRazaosocial().equals("") ? vm.getFornecedor().getRazaosocial() : vm.getFornecedor().getNome();
				if(vm.getFornecedor().getCpfOuCnpj() != null){
					nomefornecedor += " - " +  vm.getFornecedor().getCpfOuCnpj();
				}
				
				pcc.setFORNECEDOR(nomefornecedor);
				pcc.setFORNECEDORRAZAOSOCIAL(vm.getFornecedor().getRazaosocial() != null ? vm.getFornecedor().getRazaosocial() : "");
				Endereco endereco = enderecoService.getEnderecoPrincipalFornecedor(vm.getFornecedor());
				List<Telefone> listaTelefone = telefoneService.findByPessoa(vm.getFornecedor());
				if(endereco != null){
					pcc.setFORNECEDORENDERECO(endereco.getLogradouroCompletoComBairro());
				}
				if(listaTelefone != null && !listaTelefone.isEmpty()){
					for(Telefone tel : listaTelefone){
						if(tel.getTelefonetipo().equals(Telefonetipo.PRINCIPAL)){
							pcc.setFORNECEDORTELEFONE(tel.getTelefone());
						}
					}
				}
			}
			
			listaProdutosComprovante.add(pcc);
			
			if((vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && 
					vm.getMaterialmestre().getVendapromocional()) || 
					(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional())){
				addMaterialKit(listaProdutoskit, vm);
			}else if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() != null && 
					vm.getMaterialmestre().getKitflexivel()){
				addMaterialKitflexivel(listaProdutoskitflexivel, vm, getItemMestre(listaVendaorcamentomaterialmestre, vm));
			}
			
			vendaService.adicionaGrupomaterial(vm.getMaterial().getMaterialgrupo(), listaTotaisGrupomaterialComprovante, total);
			if(vm.getMaterialmestre() != null){
				vendaService.adicionaGrupomaterialmestre(vm.getMaterialmestre().getMaterialgrupo(), listaTotaisGrupomaterialmestreComprovante, vm.getTotal());
			}
		}
		
		//NOVOS PRODUTOS MESTRES
		for(Vendaorcamentomaterialmestre vomm : listaVendaorcamentomaterialmestre) {			
			ProdutoComprovanteConfiguravel pcc = new ProdutoComprovanteConfiguravel();
			pcc.setCDMATERIAL(vomm.getMaterial()!=null && vomm.getMaterial().getCdmaterial() != null ? vomm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCODIGO(vomm.getMaterial()!=null && vomm.getMaterial().getCdmaterial()!=null ? vomm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCDMATERIALMESTRE(vomm.getMaterial()!=null && vomm.getMaterial().getCdmaterial()!=null ? vomm.getMaterial().getCdmaterial().toString() : "");
			pcc.setDESCRICAO(vomm.getMaterial()!=null && vomm.getMaterial().getNome()!=null ? vomm.getMaterial().getNome() : "");
			pcc.setCDMATERIALTIPO(vomm.getMaterial() != null && vomm.getMaterial().getMaterialtipo() != null ? vomm.getMaterial().getMaterialtipo().getCdmaterialtipo() : null);
			pcc.setCDMATERIALCATEGORIA(vomm.getMaterial() != null && vomm.getMaterial().getMaterialcategoria() != null ? vomm.getMaterial().getMaterialcategoria().getCdmaterialcategoria() : null);
			pcc.setALTURA(vomm.getAltura()!=null ? vomm.getAltura().toString() : "");
			pcc.setCOMPRIMENTO(vomm.getComprimento()!=null ? vomm.getComprimento().toString() : "");
			pcc.setLARGURA(vomm.getLargura()!=null ? vomm.getLargura().toString() : "");
			pcc.setQUANTIDADE(vomm.getQtde()!=null ? vomm.getQtde().doubleValue() : 0D);
			pcc.setCDMATERIALGRUPO(vomm.getMaterial()!=null && vomm.getMaterial().getMaterialgrupo() != null ? vomm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString() : "");
			pcc.setOBSERVACAO(vomm.getObservacao() != null ? vomm.getObservacao() : "");
			pcc.setIDENTIFICADORMATERIALMESTRE(vomm.getIdentificadorinterno() != null ? vomm.getIdentificadorinterno() : "");
			pcc.setVALORIPI(vomm.getValoripi() != null? vomm.getValoripi().getValue().doubleValue(): 0D);
			listaProdutosmestre.add(pcc);
		}
		
		map.put(CampoComprovanteConfiguravel.VENDA_VALORIPI.getNome(), valorTotalIpi.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORICMS.getNome(), valorTotalIcms.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORICMSST.getNome(), valorTotalIcmsSt.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORDESONERACAOICMS.getNome(), valorTotalDesoneracaoIcms.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORFCP.getNome(), valorTotalFcp.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORFCPST.getNome(), valorTotalFcpSt.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORDIFAL.getNome(), valorTotalDifal.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_PESOLIQUIDOTOTAL.getNome(), SinedUtil.roundByParametro(pesoliquidototal));
		map.put(CampoComprovanteConfiguravel.VENDA_PESOBRUTOTOTAL.getNome(), SinedUtil.roundByParametro(pesobrutototal.doubleValue()));
		map.put(CampoComprovanteConfiguravel.TOTAL_METROCUBICO.getNome(), new DecimalFormat("#.##").format(qtdetotalmetrocubico));
		map.put(CampoComprovanteConfiguravel.MATERIALGRUPO_LISTA.getNome(), listaTotaisGrupomaterialComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTO_VALORTOTAL.getNome(), valorTotalProdutos.toString());
		map.put(CampoComprovanteConfiguravel.PRODUTO_QTDETOTALPRODUTO.getNome(), qtdeProduto.toString());
		map.put(CampoComprovanteConfiguravel.PRODUTO_QTDETOTALSERVICO.getNome(), qtdeServico.toString());
		map.put(CampoComprovanteConfiguravel.VENDA_DATAENTREGA.getNome(), vendaorcamento.getDataentregaTrans() != null ? sdf.format(vendaorcamento.getDataentregaTrans()) : "");
		map.put(CampoComprovanteConfiguravel.VENDA_OBSERVACAOINTERNA.getNome(), vendaorcamento.getObservacaointerna() != null ? vendaorcamento.getObservacaointerna() : "");
		map.put(CampoComprovanteConfiguravel.MATERIALGRUPOMESTRE_LISTA.getNome(), listaTotaisGrupomaterialmestreComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTO_LISTA.getNome(), listaProdutosComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTOMESTRE_LISTA.getNome(), listaProdutosmestre);
		map.put(CampoComprovanteConfiguravel.PRODUTOKIT_LISTA.getNome(), listaProdutoskit);
		map.put(CampoComprovanteConfiguravel.PRODUTOKITFLEXIVEL_LISTA.getNome(), listaProdutoskitflexivel);
		map.put(CampoComprovanteConfiguravel.PRODUTOMESTREGRADE_LISTA.getNome(), listaProdutosMestreGrade);
		vendaService.addListaProdutoagrupadoForComprovante(listaProdutosComprovante, map);
		
		List<CondicaoPagamentoComprovanteConfiguravel> listaCondicaoPagamentoComprovante = new ArrayList<CondicaoPagamentoComprovanteConfiguravel>();
		for (Vendaorcamentoformapagamento vfp : listaVendaorcamentoFormaPagamento){
			CondicaoPagamentoComprovanteConfiguravel cpcc = new CondicaoPagamentoComprovanteConfiguravel();
			cpcc.setDESCRICAO(vfp.getPrazopagamento().getNome());
			cpcc.setQTDEPARCELA(vfp.getQtdeparcelas());
			if(vfp.getDtparcela() != null)
				cpcc.setDATA1PARCELA(sdf.format(vfp.getDtparcela()));
			cpcc.setVALORPARCELA(vfp.getValorparcela().toString());
			cpcc.setVALORTOTAL(vfp.getValortotal().toString());
			listaCondicaoPagamentoComprovante.add(cpcc);
		}
		map.put(CampoComprovanteConfiguravel.CONDICAOPAGAMENTO_LISTA.getNome(), listaCondicaoPagamentoComprovante);
		
		StringBuilder observacao = new StringBuilder();
		if(vendaorcamento.getObservacao() != null){
			observacao.append(vendaorcamento.getObservacao());
		}
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getObservacaovenda() != null && !"".equals(vendaorcamento.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(vendaorcamento.getEmpresa().getObservacaovenda());
		}
		String obsString = observacao.toString();
		if(obsString != null){
			obsString = obsString.replaceAll("\\n", "<br>");
		}
		map.put(CampoComprovanteConfiguravel.VENDA_OBSERVACAO.getNome(), obsString);
		map.put(CampoComprovanteConfiguravel.VENDA_DTVALIDADE.getNome(), vendaorcamento.getDtvalidade() == null ? "" : SinedDateUtils.toString(vendaorcamento.getDtvalidade()));
		map.put(CampoComprovanteConfiguravel.VENDA_VALORFRETE.getNome(), vendaorcamento.getValorfrete() == null ? new Money(0.0).toString() : vendaorcamento.getValorfrete().toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORDESCONTO.getNome(), vendaorcamento.getDesconto() == null ? new Money(0.0).toString() : vendaorcamento.getDesconto().toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALECOMPRA.getNome(), vendaorcamento.getValorusadovalecompra() == null ? new Money(0.0).toString() : vendaorcamento.getValorusadovalecompra().toString());
		map.put(CampoComprovanteConfiguravel.VENDA_LOCALARMAZENAGEM.getNome(), vendaorcamento.getLocalarmazenagem() != null && vendaorcamento.getLocalarmazenagem().getNome() != null ? vendaorcamento.getLocalarmazenagem().getNome() : "");
		
		Money valorFinal = valorTotalProdutos;
		if (vendaorcamento.getValorfrete() != null)
			valorFinal = valorFinal.add(vendaorcamento.getValorfrete());
		if (vendaorcamento.getDesconto() != null)
			valorFinal = valorFinal.subtract(vendaorcamento.getDesconto());
		if (vendaorcamento.getValorusadovalecompra() != null)
			valorFinal = valorFinal.subtract(vendaorcamento.getValorusadovalecompra());
		vendaorcamento.setValorfinal(valorFinal);

		map.put(CampoComprovanteConfiguravel.VENDA_VALORTOTAL.getNome(), vendaorcamento.getValorfinal() == null ? new Money(0.0).toString() : vendaorcamento.getValorfinal().toString());
		this.setTotalImpostos(vendaorcamento);
		map.put(CampoComprovanteConfiguravel.VENDA_VALORFINALMAISIPI.getNome(), vendaorcamento.getValorfinal().add(vendaorcamento.getTotalipi()).toString());
		map.put(CampoComprovanteConfiguravel.VENDA_VALORFINALCOMIMPOSTOS.getNome(), vendaorcamento.getValorfinal()
																								.add(vendaorcamento.getTotalipi())
																								.add(valorTotalIcms)
																								.add(valorTotalIcmsSt)
																								.subtract(valorTotalDesoneracaoIcms)
																								.add(valorTotalFcp)
																								.add(valorTotalFcpSt).toString());
		
		if(vendaorcamento.getFrete() != null){
			if(vendaorcamento.getFrete().equals(ResponsavelFrete.TERCEIROS) && vendaorcamento.getTerceiro() != null){
				map.put(CampoComprovanteConfiguravel.VENDA_DESCRICAOFRETE.getNome(), " (" + vendaorcamento.getFrete().getNome() + " - " + vendaorcamento.getTerceiro().getNome() + ")");
			} else {
				map.put(CampoComprovanteConfiguravel.VENDA_DESCRICAOFRETE.getNome(), " (" + vendaorcamento.getFrete().getNome() + ")");
			}
		} else {
			map.put(CampoComprovanteConfiguravel.VENDA_DESCRICAOFRETE.getNome(), "");
		}
		
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getCdpessoa() != null){
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_NOME.getNome(), vendaorcamento.getEmpresa().getRazaosocialOuNome() != null ?  vendaorcamento.getEmpresa().getRazaosocialOuNome() : null);
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_NOMEFANTASIA.getNome(), vendaorcamento.getEmpresa().getNomefantasia() != null ?  vendaorcamento.getEmpresa().getNomefantasia() : null);
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_CNPJ.getNome(), vendaorcamento.getEmpresa().getCnpj() != null ?  vendaorcamento.getEmpresa().getCnpj().toString() : null);
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_INSCRICAOESTADUAL.getNome(), vendaorcamento.getEmpresa().getInscricaoestadual());
			vendaorcamento.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(vendaorcamento.getEmpresa())));
			vendaorcamento.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.findByPessoa(vendaorcamento.getEmpresa())));
			Endereco endereco = vendaorcamento.getEmpresa().getEndereco();
			String telefonesEmpresa = vendaorcamento.getEmpresa().getTelefonesSemQuebraLinha();
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_ENDERECO.getNome(), endereco != null ? endereco.getLogradouroCompletoComBairro() : null);
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_TELEFONE.getNome(), telefonesEmpresa != null ? telefonesEmpresa : "");
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_SITE.getNome(), vendaorcamento.getEmpresa().getSite() != null && !vendaorcamento.getEmpresa().getSite().equals("") ? vendaorcamento.getEmpresa().getSite() : "");
			map.put(CampoComprovanteConfiguravel.VENDA_EMPRESA_EMAIL.getNome(), vendaorcamento.getEmpresa().getEmail() != null && !vendaorcamento.getEmpresa().getEmail().equals("") ? vendaorcamento.getEmpresa().getEmail() : "");
			
			if(vendaorcamento.getEmpresa().getLogomarca() != null){
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vendaorcamento.getEmpresa().getLogomarca().getCdarquivo());
				map.put(CampoComprovanteConfiguravel.VENDA_LOGO.getNome(), "'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vendaorcamento.getEmpresa().getLogomarca().getCdarquivo().toString()+"'");
			} else 
				map.put(CampoComprovanteConfiguravel.VENDA_LOGO.getNome(), logoNaoDisponivel);
		}
		
		Empresa empresa = empresaService.loadEmpresa(vendaorcamento.getEmpresa());
		
		if(empresa.getLogomarca() != null){
			DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), empresa.getLogomarca().getCdarquivo());
			map.put(CampoComprovanteConfiguravel.LOGO.getNome(), "'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+empresa.getLogomarca().getCdarquivo().toString()+"'");
		} else
			map.put(CampoComprovanteConfiguravel.LOGO.getNome(), logoNaoDisponivel);
		map.put(CampoComprovanteConfiguravel.EMPRESA.getNome(), empresa == null ? null : empresa.getRazaosocialOuNome());
		map.put(CampoComprovanteConfiguravel.USUARIO.getNome(), Util.strings.toStringDescription(Neo.getUser()));
		map.put(CampoComprovanteConfiguravel.DATA.getNome(), sdf.format(new Date(System.currentTimeMillis())) );
		map.put(CampoComprovanteConfiguravel.DATA_EXTENSO.getNome(), SinedDateUtils.dataExtenso(new Date(System.currentTimeMillis())) );
		map.put(CampoComprovanteConfiguravel.QUEBRA_LINHA_TXT.getNome(), "\r\n");
		map.put(CampoComprovanteConfiguravel.QUEBRA_LINHA_HTML.getNome(), "<br/>");
		
		return map;
	}
	
	private void addMaterialKit(List<ProdutokitComprovanteConfiguravel> listaProdutoskit, Vendaorcamentomaterial vm) {
		if(listaProdutoskit != null && vm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() && vm.getMaterialmestre().getVendapromocional()){
				boolean adicionar = true;
				for(ProdutokitComprovanteConfiguravel pkc : listaProdutoskit){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterialmestre())){
						adicionar = false;
						ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
						pkic.setDESCRICAO(vm.getMaterial().getNome());
						pkic.setPRECO(vm.getPreco());
						pkic.setQUANTIDADE(vm.getQuantidade());
						pkic.setTOTAL(vm.getTotalproduto().getValue().doubleValue());
						pkic.setMaterial(vm.getMaterial());
						
						if(pkc.getLISTAPRODUTOKITITEM() == null) 
							pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
						
						if(pkc.getTOTAL() == null) pkc.setTOTAL(pkic.getTOTAL());
						else pkc.setTOTAL(pkc.getTOTAL() + pkic.getTOTAL());
						
						pkc.getLISTAPRODUTOKITITEM().add(pkic);
						break;
					}
				}
				if(adicionar){
					ProdutokitComprovanteConfiguravel pkc = new ProdutokitComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(vm.getMaterialmestre().getNome());
					pkc.setMaterial(vm.getMaterialmestre());
					pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
					
					ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
					pkic.setDESCRICAO(vm.getMaterial().getNome());
					pkic.setPRECO(vm.getPreco());
					pkic.setQUANTIDADE(vm.getQuantidade());
					pkic.setTOTAL(vm.getTotalproduto().getValue().doubleValue());
					
					pkic.setMaterial(vm.getMaterial());
					pkc.setTOTAL(pkic.getTOTAL());
					
					pkc.getLISTAPRODUTOKITITEM().add(pkic);
					listaProdutoskit.add(pkc);
				}
			}else if(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional()){
				boolean adicionar = true;
				for(ProdutokitComprovanteConfiguravel pkc : listaProdutoskit){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterial())){
						adicionar = false;
						break;
					}
				}
				if(adicionar){
					ProdutokitComprovanteConfiguravel pkc = new ProdutokitComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterial().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(vm.getMaterial().getNome());
					pkc.setQUANTIDADE(vm.getQuantidade());
					pkc.setMaterial(vm.getMaterial());
					pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
					
					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vm.getMaterial().getCdmaterial());
					material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
					
					try{
						material.setProduto_altura(vm.getAltura());
						material.setProduto_largura(vm.getLargura());
						material.setQuantidade(vm.getQuantidade());
						materialrelacionadoService.calculaQuantidadeKit(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
						Double total = 0d;
						Double precoTotal = 0d;
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getQuantidade() != null && vm.getQuantidade() != null){
								ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
								pkic.setDESCRICAO(materialrelacionado.getMaterialpromocao().getNome());
								Double valorvendaItemKit = materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0d;
								Double preco = vm.getPreco() != null ? vm.getPreco() : 0d;
								Double valorvendaMaterial = material.getValorvenda() != null ? material.getValorvenda() : 0d;
								if(valorvendaMaterial == 0) valorvendaMaterial = 1d;
								pkic.setPRECO(preco * ((valorvendaItemKit * 100) / valorvendaMaterial) / 100);
								pkic.setQUANTIDADE(materialrelacionado.getQuantidade() * vm.getQuantidade());
								pkic.setTOTAL(pkic.getPRECO() * pkic.getQUANTIDADE());
								total += pkic.getTOTAL();
								pkic.setMaterial(vm.getMaterial());
								
								pkc.getLISTAPRODUTOKITITEM().add(pkic);
							}
						}
						pkc.setTOTAL(total);
						pkc.setPRECO(precoTotal);
					}
					listaProdutoskit.add(pkc);
				}
			}
		}
	}
	
	private Vendaorcamentomaterialmestre getItemMestre(List<Vendaorcamentomaterialmestre> lista, Vendaorcamentomaterial vendamaterial){
		if(SinedUtil.isListNotEmpty(lista) && vendamaterial != null && 
				org.apache.commons.lang.StringUtils.isNotEmpty(vendamaterial.getIdentificadorinterno())){
			for(Vendaorcamentomaterialmestre vendamaterialmestre : lista){
				if(org.apache.commons.lang.StringUtils.isNotEmpty(vendamaterialmestre.getIdentificadorinterno()) && 
						vendamaterialmestre.getIdentificadorinterno().equals(vendamaterial.getIdentificadorinterno())){
					return vendamaterialmestre;
				}
			}
		}
		return null;
	}
	
	private void addMaterialKitflexivel(List<ProdutokitflexivelComprovanteConfiguravel> listaProdutoskitflexivel, Vendaorcamentomaterial vm, Vendaorcamentomaterialmestre vmm) {
		if(listaProdutoskitflexivel != null && vm != null && vmm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() && vm.getMaterialmestre().getKitflexivel()){
				boolean adicionar = true;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				for(ProdutokitflexivelComprovanteConfiguravel pkc : listaProdutoskitflexivel){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterialmestre()) && 
							pkc.getIDENTIFICADORMATERIALMESTRE() != null && 
							vm.getIdentificadorinterno() != null &&
							pkc.getIDENTIFICADORMATERIALMESTRE().equals(vm.getIdentificadorinterno())){
						adicionar = false;
						ProdutokitflexivelitemComprovanteConfiguravel pkic = new ProdutokitflexivelitemComprovanteConfiguravel();
						pkic.setCODIGO(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
						pkic.setDESCRICAO(vm.getMaterial().getNome());
						pkic.setPRECO(vm.getPreco());
						pkic.setQUANTIDADE(vm.getQuantidade());
						pkic.setTOTALITEM(vm.getTotalproduto().getValue().doubleValue());
						
						if(vm.getMaterial().getMaterialgrupo() != null && 
								vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
							pkic.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
						
						pkic.setNCMCOMPLETO(vm.getMaterial().getNcmcompleto());
						
						if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
							DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
							pkic.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
						}
						
						pkic.setOBSERVACAO(vm.getObservacao());
						if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
							pkic.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
						}
						pkic.setPESOBRUTO(vm.getMaterial().getPesobruto());
						pkic.setPESOLIQUIDO(vm.getMaterial().getPesobruto());
						
						pkic.setENTREGA(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
						if(vm.getComprimentooriginal() != null){
							pkic.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							pkic.setLARGURA(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							pkic.setALTURA(vm.getAltura().toString());
						}
						if(vm.getLoteestoque() != null){
							pkic.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
						}
						
						pkic.setMaterial(vm.getMaterial());
						
						if(pkc.getLISTAPRODUTOKIFLEXIVELTITEM() == null) 
							pkc.setLISTAPRODUTOKIFLEXIVELTITEM(new ArrayList<ProdutokitflexivelitemComprovanteConfiguravel>());
						
//						if(pkc.getTOTALKIT() == null) pkc.setTOTALKIT(pkic.getTOTALITEM());
//						else pkc.setTOTALKIT(pkc.getTOTALKIT() + pkic.getTOTALITEM());
						
						pkc.getLISTAPRODUTOKIFLEXIVELTITEM().add(pkic);
						break;
					}
				}
				if(adicionar){
					ProdutokitflexivelComprovanteConfiguravel pkc = new ProdutokitflexivelComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(org.apache.commons.lang.StringUtils.isNotEmpty(vmm.getNomealternativo()) ? vmm.getNomealternativo() : vmm.getMaterial().getNome());
					pkc.setQUANTIDADE(vmm.getQtde());
					pkc.setPRECO(vmm.getPreco());
					pkc.setDESCONTO(vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : null);
					pkc.setTOTALKIT(vmm.getPreco()*vmm.getQtde() - (vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : 0d));
					pkc.setIDENTIFICADORMATERIALMESTRE(vmm.getIdentificadorinterno() != null ? vmm.getIdentificadorinterno() : "");
					pkc.setENTREGA(vmm.getDtprazoentrega()==null ? "" : sdf.format(vmm.getDtprazoentrega()));
					pkc.setUNIDADEMEDIDA(vmm.getUnidademedida()!=null && vmm.getUnidademedida().getNome()!=null ? vmm.getUnidademedida().getNome() : "");
					
					if(vm.getMaterialmestre().getMaterialgrupo() != null && 
							vm.getMaterialmestre().getMaterialgrupo().getCdmaterialgrupo() != null)
						pkc.setCDMATERIALGRUPO(vm.getMaterialmestre().getMaterialgrupo().getCdmaterialgrupo().toString());
					
					pkc.setNCMCOMPLETO(vmm.getMaterial().getNcmcompleto());
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getArquivo() != null && vm.getMaterialmestre().getArquivo().getCdarquivo() != null){
						DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterialmestre().getArquivo().getCdarquivo());
						pkc.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterialmestre().getArquivo().getCdarquivo().toString()+"'");
					}
					
					pkc.setOBSERVACAO(vm.getObservacao());
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						pkc.setCARACTERISTICA(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					pkc.setPESOBRUTO(vm.getMaterialmestre().getPesobruto());
					pkc.setPESOLIQUIDO(vm.getMaterialmestre().getPesobruto());
					
					pkc.setMaterial(vm.getMaterialmestre());
					pkc.setLISTAPRODUTOKIFLEXIVELTITEM(new ArrayList<ProdutokitflexivelitemComprovanteConfiguravel>());
					
					ProdutokitflexivelitemComprovanteConfiguravel pkic = new ProdutokitflexivelitemComprovanteConfiguravel();
					pkic.setCODIGO(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
					pkic.setDESCRICAO(vm.getMaterial().getNome());
					pkic.setPRECO(vm.getPreco());
					pkic.setQUANTIDADE(vm.getQuantidade());
					pkic.setTOTALITEM(vm.getTotalproduto().getValue().doubleValue());
					
					if(vm.getMaterial().getMaterialgrupo() != null && 
							vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						pkic.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					
					pkic.setNCMCOMPLETO(vm.getMaterial().getNcmcompleto());
					
					if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
						DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
						pkic.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
					}
					
					pkic.setOBSERVACAO(vm.getObservacao());
					if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
						pkic.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
					}
					pkic.setPESOBRUTO(vm.getMaterial().getPesobruto());
					pkic.setPESOLIQUIDO(vm.getMaterial().getPesobruto());
					
					pkic.setENTREGA(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
					if(vm.getComprimentooriginal() != null){
						pkic.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						pkic.setLARGURA(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						pkic.setALTURA(vm.getAltura().toString());
					}
					if(vm.getLoteestoque() != null){
						pkic.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
					}
					
					pkic.setMaterial(vm.getMaterial());
					
					pkc.getLISTAPRODUTOKIFLEXIVELTITEM().add(pkic);
					listaProdutoskitflexivel.add(pkc);
				}
			}
		}
	}
	
	/**
	 * M�todo que cria o Report do comprovante de venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Report criaComprovanteVendaorcamentoReport(Vendaorcamento vendaorcamento){	
		
		List<Vendaorcamentomaterial> listaprodutos = vendaorcamentomaterialService.findByMaterial(vendaorcamento, "vendaorcamentomaterial.ordem, vendaorcamentomaterial.cdvendaorcamentomaterial");
		List<Vendaorcamentoformapagamento> listaVendaorcamentoformapagamento = vendaorcamentoformapagamentoService.findByVendaorcamento(vendaorcamento);
		
		List<ResumoMaterialunidadeconversao> listaResumoMaterialunidadeconversao = materialService.preencheMaterialWithConversoesVendaorcamento(listaprodutos);
		
//		Colaborador colaborador = colaboradorService.load(vendaorcamento.getColaborador());
		Colaborador colaborador = vendaorcamento.getColaborador();
		Cliente cliente = vendaorcamento.getCliente() != null ? clienteService.carregarDadosCliente(vendaorcamento.getCliente()) : null;
		if(cliente != null){
			cliente.setNome(pessoaService.carregaPessoa(new Pessoa(cliente.getCdpessoa())).getNome());
		}
		Report report = new Report("/faturamento/comprovantevenda");
		vendaorcamento.setIdentificador(this.buscarIdentificadorVendaorcamento(vendaorcamento));
		report.addParameter("TITULO", "OR�AMENTO");
		if(listaVendaorcamentoformapagamento != null && !listaVendaorcamentoformapagamento.isEmpty()){
			report.addParameter("LISTAFORMAPAGAMENTO", listaVendaorcamentoformapagamento);
		}
		report.addParameter("VENDEDOREMAIL", colaborador.getEmail());
		report.addSubReport("SUB_VENDAMATERIAL", new Report("/faturamento/vendamaterial_sub"));
//		report.addSubReport("SUB_VENDAPAGAMENTO", new Report("/faturamento/vendapagamento_sub"));
		report.addSubReport("SUB_VENDAFORMAPAGAMENTO", new Report("/faturamento/vendaformapagamento_sub"));
		report.addParameter("CLIENTE", cliente != null ? (cliente.getRazaosocial() != null && !"".equals(cliente.getRazaosocial()) ? 
				cliente.getRazaosocial() : cliente.getNome()) : null);
		report.addParameter("CONTACRM", vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getNome() : null);
		report.addParameter("CPF", cliente != null ? cliente.getCpf() : (vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getCpf() : null));
		report.addParameter("CNPJ", cliente != null ? cliente.getCnpj() : (vendaorcamento.getContacrm() != null ? vendaorcamento.getContacrm().getCnpj() : null));
		
		String telefones = cliente != null ? cliente.getTelefonesSemQuebraLinha() : null;
		if(telefones != null){
			report.addParameter("CLIENTETELEFONES", telefones.replace("|", " "));
		}
		
		if(vendaorcamento.getFrete() != null){
			if(vendaorcamento.getFrete().equals(ResponsavelFrete.TERCEIROS) && vendaorcamento.getTerceiro() != null){
				report.addParameter("DESCRICAOFRETE", " (" + vendaorcamento.getFrete().getNome() + " - " + vendaorcamento.getTerceiro().getNome() + ")");
			} else {
				report.addParameter("DESCRICAOFRETE", " (" + vendaorcamento.getFrete().getNome() + ")");
			}
		} else {
			report.addParameter("DESCRICAOFRETE", "");
		}
		
		report.addParameter("VENDEDOR", colaborador.getNome());
		String identificador = vendaorcamento.getIdentificador() != null && !"".equals(vendaorcamento.getIdentificador()) ? vendaorcamento.getIdentificador() : null;
		report.addParameter("CODIGOVENDA", identificador != null ? identificador : vendaorcamento.getCdvendaorcamento().toString());
		report.addParameter("DATAVENDA", vendaorcamento.getDtorcamento());
		
		Endereco endereco = vendaorcamento.getEndereco();
		if(endereco == null && cliente != null){
			endereco = cliente.getEndereco();
		}
		report.addParameter("ENDERECO", endereco != null ? endereco.getLogradouroCompletoComCep() : "");
		
		Money totalvenda = new Money();
		Money valortotalprodutos = new Money();
		for (Vendaorcamentomaterial vendamaterial : listaprodutos) {
            vendamaterial.setTotal(vendaService.getValortotal(vendamaterial.getPreco(), vendamaterial.getQuantidade(), vendamaterial.getDesconto(),
                    vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
			totalvenda = totalvenda.add(vendamaterial.getTotal());
		}
		valortotalprodutos = new Money(totalvenda);
		if(vendaorcamento.getValorfrete() != null){
			totalvenda = totalvenda.add(vendaorcamento.getValorfrete());
		}
		
		Double totalpesobruto = 0.0;
		Double totalpesoliquido = 0.0;
		Double pesobruto = null;
		
		report.addParameter("TOTALPRODUTOS", valortotalprodutos);
		report.addParameter("TOTALVENDAMATERIALCOMDESCONTO", vendaorcamento.getDesconto() != null ? totalvenda.subtract(vendaorcamento.getDesconto()) : totalvenda);
		report.addParameter("TOTALVENDAMATERIALCOMDESCONTOFRETE", vendaorcamento.getDesconto() != null ? totalvenda.subtract(vendaorcamento.getDesconto()) : totalvenda);
		if(listaprodutos != null && !listaprodutos.isEmpty()){
			for(Vendaorcamentomaterial vendamaterial : listaprodutos){
				if(vendamaterial.getMaterial() != null){
					if(vendamaterial.getLoteestoque() != null && vendamaterial.getLoteestoque().getDescriptionAutocomplete() != null){
						vendamaterial.setMaterial(new Material(vendamaterial.getMaterial().getCdmaterial(), vendamaterial.getMaterial().getNome() + " - "  + vendamaterial.getLoteestoque().getDescriptionAutocomplete()));
					}
					if(vendamaterial.getObservacao() != null && !"".equals(vendamaterial.getObservacao())){
						vendamaterial.setMaterial(new Material(vendamaterial.getMaterial().getCdmaterial(), vendamaterial.getMaterial().getNome() + "\n"  + vendamaterial.getObservacao()));
					}
					
					if (vendamaterial.getMaterial().getPesobruto() != null) {
						pesobruto = vendamaterial.getMaterial().getPesobruto();
						if (vendamaterial.getMaterial().getUnidademedida() != null
								&& !vendamaterial.getMaterial().getUnidademedida().equals(vendamaterial.getUnidademedida())) {
							Double fracao = vendamaterial.getFatorconversaoQtdereferencia();
							if (fracao != null && fracao > 0) {
								pesobruto = pesobruto / fracao;
							}
						}
						totalpesobruto += vendamaterial.getQuantidade() * pesobruto;
					}
					if (vendamaterial.getPesoVendaOuMaterial() != null) {
						totalpesoliquido += vendamaterial.getQuantidade() * vendamaterial.getPesoVendaOuMaterial();
					}
				}
			}
		}
		
		report.addParameter("PESOBRUTOTOTAL", totalpesobruto == null
				|| totalpesobruto == 0 ? null : SinedUtil
				.roundByParametro(totalpesobruto));
		report.addParameter("PESOLIQUIDOTOTAL", totalpesoliquido == null
				|| totalpesoliquido == 0 ? null : SinedUtil
				.roundByParametro(totalpesoliquido));
		
		report.addParameter("LISTAPRODUTOS", listaprodutos);		
		StringBuilder observacao = new StringBuilder();
		if(vendaorcamento.getObservacao() != null){
			observacao.append(vendaorcamento.getObservacao());
		}
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getObservacaovenda() != null && !"".equals(vendaorcamento.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(vendaorcamento.getEmpresa().getObservacaovenda());
		}
		report.addParameter("OBSERVACAO", observacao.toString());
		report.addParameter("DTVALIDADE", vendaorcamento.getDtvalidade() == null ? null : SinedDateUtils.toString(vendaorcamento.getDtvalidade()));
		report.addParameter("VALORFRETE", vendaorcamento.getValorfrete() == null ? new Money(0.0) : vendaorcamento.getValorfrete());
		report.addParameter("VALORDESCONTO", vendaorcamento.getDesconto() == null ? new Money(0.0) : vendaorcamento.getDesconto());
		
		if(vendaorcamento.getFrete() != null){
			if(vendaorcamento.getFrete().equals(ResponsavelFrete.TERCEIROS) && vendaorcamento.getTerceiro() != null){
				report.addParameter("DESCRICAOFRETE", " (" + vendaorcamento.getFrete().getNome() + " - " + vendaorcamento.getTerceiro().getNome() + ")");
			} else {
				report.addParameter("DESCRICAOFRETE", " (" + vendaorcamento.getFrete().getNome() + ")");
			}
		} else {
			report.addParameter("DESCRICAOFRETE", "");
		}
		
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getCdpessoa() != null){
			report.addParameter("EMPRESAVENDA", vendaorcamento.getEmpresa().getRazaosocialOuNome() != null ?  vendaorcamento.getEmpresa().getRazaosocialOuNome() : null);
			report.addParameter("CNPJEMPRESA", vendaorcamento.getEmpresa().getCnpj() != null ?  vendaorcamento.getEmpresa().getCnpj().toString() : null);
			report.addParameter("INSCRICAOESTADUAL", vendaorcamento.getEmpresa().getInscricaoestadual());
			vendaorcamento.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(vendaorcamento.getEmpresa())));
			vendaorcamento.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(vendaorcamento.getEmpresa())));
			Endereco enderecoempresa = vendaorcamento.getEmpresa().getEndereco();
			Telefone telefone = vendaorcamento.getEmpresa().getTelefone();
			report.addParameter("ENDERECOEMPRESA", enderecoempresa != null ? enderecoempresa.getLogradouroCompletoComBairro() : null);
			report.addParameter("TELEFONEEMPRESA", telefone != null ? telefone.getTelefone() : null);
			report.addParameter("SITE", vendaorcamento.getEmpresa().getSite() != null && !vendaorcamento.getEmpresa().getSite().equals("") ? vendaorcamento.getEmpresa().getSite() : "");
			report.addParameter("EMAIL", vendaorcamento.getEmpresa().getEmail() != null && !vendaorcamento.getEmpresa().getEmail().equals("") ? vendaorcamento.getEmpresa().getEmail() : "");
			report.addParameter("LOGOVENDA", SinedUtil.getLogo(vendaorcamento.getEmpresa()));
		}
		
		Empresa empresa = empresaService.loadPrincipal();
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		if(listaResumoMaterialunidadeconversao != null && !listaResumoMaterialunidadeconversao.isEmpty()){
			report.addParameter("LISTARESUMOMATERIALUNIDADECONVERSAO", listaResumoMaterialunidadeconversao);
			report.addSubReport("SUB_RESUMOMATERIALUNIDADECONVERSAO", new Report("/faturamento/resumomaterialunidadeconversao"));
		}
		
		return report;
	}
	
	
	/**
	 * Metodo para retornar o total de uma venda
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public Money totalVendaorcamento(Vendaorcamento vendaorcamento){
		Money total = new Money(0);
		if (vendaorcamento.getListavendaorcamentomaterial() != null)
			for (Vendaorcamentomaterial vendamaterial : vendaorcamento.getListavendaorcamentomaterial()) 
                total = total.add(vendaService.getValortotal(vendamaterial.getPreco(), vendamaterial.getQuantidade(), vendamaterial.getDesconto(),
                        vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
		return total;
	}
	
	/**
	 * Atualiza os dados da venda alterados no crud
	 * @param venda
	 * @author Thiago Gona�ves
	 */
	public void updateVendaorcamento(Vendaorcamento vendaorcamento){
		vendaorcamentoDAO.updateVendaorcamento(vendaorcamento);
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setAcao("Altera��o");
		vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param vendaorcamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isVendaOrcamentoProducao(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.isVendaOrcamentoProducao(vendaorcamento);
	}
	
	/* singleton */
	private static VendaorcamentoService instance;
	public static VendaorcamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendaorcamentoService.class);
		}
		return instance;
	}

	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosVendasorcamento(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Integer total = getQtdeTotalVenda();
		return new DadosEstatisticosBean(DadosEstatisticosBean.VENDA, ultimoCadastro, total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalVenda() {
		return vendaorcamentoDAO.getQtdeTotalVenda();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return vendaorcamentoDAO.getDtUltimoCadastro();
	}
	
	public List<Vendaorcamento> findByCliente(Cliente cliente, Empresa empresa) {
		return vendaorcamentoDAO.findByCliente(cliente, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean haveVendaorcamentoSituacao(String whereIn, Boolean validateEndereco, Vendaorcamentosituacao... situacoes) {
		return vendaorcamentoDAO.haveVendaorcamentoSituacao(whereIn, validateEndereco, situacoes);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaorcamentoService#existOrcamentoNotcancelar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existOrcamentoNotcancelar(String whereIn) {
		return vendaorcamentoDAO.existOrcamentoNotcancelar(whereIn);
	}
	
	public boolean existOrcamentoSomenteEmAberto(String whereIn) {
		return vendaorcamentoDAO.existOrcamentoSomenteEmAberto(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vendaorcamento> findForCobranca(String whereIn) {
		return vendaorcamentoDAO.findForCobranca(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void updateSituacaoVendaorcamento(String whereIn, Vendaorcamentosituacao situacao) {
		vendaorcamentoDAO.updateSituacaoVendaorcamento(whereIn, situacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isVendaOrcamentoProducao(String whereIn) {
		return vendaorcamentoDAO.isVendaOrcamentoProducao(whereIn);
	}
	
	public Boolean validaVendaorcamento(WebRequestContext request, Vendaorcamento vendaorcamento, BindException errors){
		List<Vendaorcamentomaterial> listavendamaterial = vendaorcamento.getListavendaorcamentomaterial();
		Empresa empresa = empresaService.loadComContagerencialCentrocusto(vendaorcamento.getEmpresa());
		List<String> listError = new ArrayList<String>();
		Boolean vendasaldoproduto = Boolean.valueOf(parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO));
		Boolean vendasaldoporminmax = Boolean.valueOf(parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX));
		Materialtabelapreco materialtabelaprecoVenda = vendaorcamento.getMaterialtabelapreco();
		boolean considerarValorcustoMaterial = pedidovendatipoService.considerarValorcustoMaterial(vendaorcamento.getPedidovendatipo());
		Materialtabelaprecoitem materialtabelaprecoitem = null;
		Double qtdemultiplo;
		for (Vendaorcamentomaterial vendamaterial : listavendamaterial) {
			Material material = materialService.load(vendamaterial.getMaterial());
			vendamaterial.setMaterial(material);
			
			Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
			if(ObjectUtils.isNullOrNotInitialized(material.getUnidademedida())) material.setUnidademedida(unidademedida);
			
			if(!considerarValorcustoMaterial && (!vendasaldoproduto || !vendasaldoporminmax)){
				Double preco = vendamaterial.getPreco();
				Double precoComDesconto = preco;
				if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0){
					Double qtde = vendamaterial.getQuantidade() != null && vendamaterial.getQuantidade() > 0 ? 
							vendamaterial.getQuantidade() : 1d;
					Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() > 0 ? 
										vendamaterial.getMultiplicador() : 1d;	
					precoComDesconto = precoComDesconto - (vendamaterial.getDesconto().getValue().doubleValue()/ (qtde*multiplicador));
				}
				Double minimo = 0.0;
				Double maximo = 0.0;
				Double valorMaximoTabela = null;
				Double valorMinimoTabela = null;
				
				if(!vendamaterial.getUnidademedida().equals(unidademedida) && vendamaterial.getFatorconversao() != null){
					Materialunidademedida materialunidademedida = materialunidademedidaService.getMaterialunidademedida(materialService.loadMaterialunidademedida(material), vendamaterial.getUnidademedida());
					
					if(materialunidademedida != null && materialunidademedida.getValorminimo() != null){
						minimo = materialunidademedida.getValorminimo();
					}else if(material.getValorvendaminimo() != null)
						minimo = material.getValorvendaminimo() / vendamaterial.getFatorconversaoQtdereferencia();
					
					if(materialunidademedida != null && materialunidademedida.getValormaximo() != null){
						maximo = materialunidademedida.getValormaximo();
					}else if(material.getValorvendamaximo() != null)
						maximo = material.getValorvendamaximo() / vendamaterial.getFatorconversaoQtdereferencia();
				}else{
					minimo = material.getValorvendaminimo();
					maximo = material.getValorvendamaximo();
				}
				
				materialtabelaprecoitem = null;
				if(materialtabelaprecoVenda != null && materialtabelaprecoVenda.getCdmaterialtabelapreco() != null){
					materialtabelaprecoitem = materialtabelaprecoitemService.getItemTabelaPrecoAtual(vendamaterial.getMaterial(), vendamaterial.getUnidademedida(), materialtabelaprecoVenda);
					if(materialtabelaprecoitem != null){
						valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa(minimo);
						valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa(maximo);
						
						if(!vendamaterial.getUnidademedida().equals(unidademedida) && vendamaterial.getFatorconversao() != null){
							if(valorMinimoTabela != null)
								valorMinimoTabela = valorMinimoTabela / vendamaterial.getFatorconversaoQtdereferencia();
							if(valorMaximoTabela != null)							
								valorMaximoTabela = valorMaximoTabela / vendamaterial.getFatorconversaoQtdereferencia();
						}
					}
				}
				
				if(materialtabelaprecoitem == null){
					Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(vendamaterial.getMaterial(), vendaorcamento.getCliente(), vendaorcamento.getPrazopagamento(), vendaorcamento.getPedidovendatipo(), vendaorcamento.getEmpresa());
					if(materialtabelapreco == null){
						materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(vendamaterial.getMaterial(), vendaorcamento.getCliente(), null, null, vendaorcamento.getEmpresa(), vendamaterial.getUnidademedida());
						if(materialtabelaprecoitem != null){
							valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa(minimo);
							valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa(maximo);
							
							if(!vendamaterial.getUnidademedida().equals(unidademedida) && vendamaterial.getFatorconversao() != null){
								if(valorMinimoTabela != null)
									valorMinimoTabela = valorMinimoTabela / vendamaterial.getFatorconversaoQtdereferencia();
								if(valorMaximoTabela != null)							
									valorMaximoTabela = valorMaximoTabela / vendamaterial.getFatorconversaoQtdereferencia();
							}
						}
					}
				}
				if(valorMinimoTabela != null){
					minimo = valorMinimoTabela;
				}
				if(valorMaximoTabela != null){
					maximo = valorMaximoTabela;
				}
				
				Usuario usuarioLogado = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.salvarAbaixoMinimoPedido");
				Boolean venderAbaixoMinimo = usuarioLogado.getSalvarAbaixoMinimoPedido();
				if (minimo != null && minimo > 0.0){
					if (!(venderAbaixoMinimo != null ? venderAbaixoMinimo : Boolean.FALSE) && precoComDesconto < minimo){
						listError.add("Pre�o menor que o m�nimo permitido no cadastro do material " + material.getNome() + ".");
					}
				}
				if (maximo != null && maximo > 0){
					if (precoComDesconto > maximo){
						listError.add("Pre�o maior que o m�ximo permitido no cadastro do material " + material.getNome() + ".");
					}
				}
			}
			
			if(material.getConsiderarvendamultiplos() != null && material.getConsiderarvendamultiplos()){
				qtdemultiplo = materialService.validaMultiplo(vendamaterial.getQuantidade(), material.getQtdeunidade(), vendamaterial.getFatorconversao(), vendamaterial.getQtdereferencia());
				if(qtdemultiplo != null && qtdemultiplo > 0){
					listError.add("S� � permitido vender o material " + material.getNome() + " com a quantidade m�ltiplo de " + qtdemultiplo);
				}
			}
			
			Contagerencial  cg = vendamaterial.getMaterial().getContagerencialvenda() == null ?  empresa.getContagerencial() : vendamaterial.getMaterial().getContagerencialvenda();
			Centrocusto cc = vendamaterial.getMaterial().getCentrocustovenda() == null ?  empresa.getCentrocusto() : vendamaterial.getMaterial().getCentrocustovenda();
			
			if((cc == null || cg == null) && vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().getCdmaterial() != null){
				Material materialmestre = materialService.loadComContagerencialCentrocusto(vendamaterial.getMaterialmestre());
				if(materialmestre != null && materialmestre.getProducao() != null && materialmestre.getProducao()){
					if(cg == null && materialmestre.getContagerencialvenda() != null){
						cg = materialmestre.getContagerencialvenda();
					}
					
					if(cc == null && materialmestre.getCentrocustovenda() != null){
						cc = materialmestre.getCentrocustovenda();
					}
				}
			}
			
			if(cc == null){
				listError.add("Informar o centro de custo de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa o centro de custo de venda.");
			}
			
			if(cg == null){
				listError.add("Informar a conta gerencial de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa a conta gerencial de venda.");
			}
		}	
		
		if(TipoPessoaVendaEnum.CLIENTE.equals(vendaorcamento.getTipopessoa()) && (vendaorcamento.getCliente() == null || vendaorcamento.getCliente().getCdpessoa() == null)){
			listError.add("O cliente � obrigat�rio.");
		}
		if(TipoPessoaVendaEnum.CONTA.equals(vendaorcamento.getTipopessoa()) && (vendaorcamento.getContacrm() == null || vendaorcamento.getContacrm().getCdcontacrm() == null)){
			listError.add("A conta � obrigat�ria.");
		}
		
		Pedidovendatipo pedidovendatipo = null;
		if(vendaorcamento.getPedidovendatipo() != null && vendaorcamento.getPedidovendatipo().getCdpedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(vendaorcamento.getPedidovendatipo());
			
			if(Boolean.TRUE.equals(pedidovendatipo.getObrigarLoteOrcamento())){
				boolean retornaFalse = false;
				for (Vendaorcamentomaterial vom : vendaorcamento.getListavendaorcamentomaterial()) {
					if(!materialService.validateObrigarLote(vom, request)){
						retornaFalse = true;
					}		
				}
				if(retornaFalse){
					return false;
				}
			}
		}
		
		if(pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getValidarTicketMedioPorFornecedor())){
			if(request != null){
				if(!materialService.validateDadosForCalculoTicketMedio(materialService.translateVendaOrcamentoMaterialToMaterial(vendaorcamento.getListavendaorcamentomaterial()), request)){
					return false;
				}
			} else if(errors != null){
				if(!materialService.validateDadosForCalculoTicketMedio(materialService.translateVendaOrcamentoMaterialToMaterial(vendaorcamento.getListavendaorcamentomaterial()), errors)){
					return false;
				}
			}
			List<VendaFornecedorTicketMedioBean> lista = vendaService.calculaTicketMedioByFornecedor(vendaorcamento, vendaorcamento.getListavendaorcamentomaterial());
			if((SinedUtil.isListNotEmpty(lista))){
				if(request != null){
					if(!vendaService.validateTicketMedioPorFornecedor(lista, request)){
						return false;
					}
				}else if(errors != null){
					if(!vendaService.validateTicketMedioPorFornecedor(lista, errors)){
						return false;
					}
				}
				this.preencheListaVendaOrcamentoFornecedorTicketMedio(vendaorcamento, lista);
			}
		}	
		
		return true;
	}
	
	public Boolean validaListaVendamaterial(WebRequestContext request, Vendaorcamento vendaorcamento) {
		if(vendaorcamento.getListavendaorcamentomaterial() == null || vendaorcamento.getListavendaorcamentomaterial().isEmpty()){
			request.addError("Nenhum produto encontrado na venda.");
			return Boolean.FALSE;
		} else {		
			Boolean validoValorTabelaPreco = Boolean.TRUE;
			Materialtabelapreco materialtabelapreco = vendaorcamento.getMaterialtabelapreco();
			Double qtdemultiplo = null;
			for(Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
				Material material = materialService.load(vendaorcamentomaterial.getMaterial());
				Double valortabelapreco = null; 
				if(materialtabelapreco != null && materialtabelapreco.getCdmaterialtabelapreco() != null){
					Aux_VendamaterialTabelapreco auxVendamaterialTabelapreco = materialtabelaprecoService.getValorTabelaPrecoAtual(vendaorcamento.getMaterial(), vendaorcamentomaterial.getUnidademedida(), materialtabelapreco, vendaorcamentomaterial.getPreco());
					valortabelapreco = auxVendamaterialTabelapreco.getValorvendamaterial();
				}
				if(valortabelapreco == null){
					Aux_VendamaterialTabelapreco auxVendamaterialTabelapreco = materialtabelaprecoService.getValorTabelaPrecoAtual(vendaorcamentomaterial.getMaterial(), vendaorcamento.getCliente(), null, vendaorcamento.getPedidovendatipo(), false, vendaorcamentomaterial.getPreco(), vendaorcamento.getEmpresa(), vendaorcamentomaterial.getUnidademedida());
					valortabelapreco = auxVendamaterialTabelapreco.getValorvendamaterial();
				}
				if(valortabelapreco != null && vendaorcamentomaterial.getPreco() < valortabelapreco){
					request.addError("O valor do produto " + vendaorcamentomaterial.getMaterial().getNome() + " � menor que o pre�o de tabela.");
					validoValorTabelaPreco = Boolean.FALSE;
				}
				
				if(material.getConsiderarvendamultiplos() != null && material.getConsiderarvendamultiplos()){
					qtdemultiplo = materialService.validaMultiplo(vendaorcamentomaterial.getQuantidade(), material.getQtdeunidade(), vendaorcamentomaterial.getFatorconversao(), vendaorcamentomaterial.getQtdereferencia());
					if(qtdemultiplo != null && qtdemultiplo > 0){
						request.addError("S� � permitido vender o material " + material.getNome() + " com a quantidade m�ltiplo de " + qtdemultiplo);
						return Boolean.FALSE;
					}
				}
			}
			
			return validoValorTabelaPreco;
		}
	}
	
	public Double getQtdeDisponivelMaterialproducao(Material material, Vendaorcamento vendaorcamento){
		Double qtdedisponivel = null;
		if(material != null && material.getListaProducao() != null && !material.getListaProducao().isEmpty()){
			Venda vendaaux;
			Double entrada = 0.0;
			Double saida = 0.0;
						
			Double qtde = 0.0;
			for(Materialproducao materialproducao : material.getListaProducao()){
				if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() != null && materialproducao.getConsumo() != null){
					vendaaux = new Venda();
					vendaaux.setMaterial(materialproducao.getMaterial());
					if (vendaorcamento.getLocalarmazenagem() != null)
						vendaaux.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
					if(vendaorcamento.getEmpresa() != null)
						vendaaux.setEmpresa(vendaorcamento.getEmpresa());
					
					entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vendaaux.getMaterial(), vendaaux.getLocalarmazenagem(), vendaaux.getEmpresa(), vendaorcamento.getProjeto());
					if(entrada == null) entrada = 0.0;
					saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vendaaux.getMaterial(), vendaaux.getLocalarmazenagem(), vendaaux.getEmpresa(), vendaorcamento.getProjeto());
					if(saida == null) saida = 0.0;
					
					qtde = SinedUtil.getQtdeEntradaSubtractSaida(entrada,saida)/materialproducao.getConsumo(); 
					
					if(qtdedisponivel != null){
						if(qtde < qtdedisponivel){
							qtdedisponivel = qtde;
						}
					}else {
						qtdedisponivel = qtde;
					}
				}
			}
		}
		
		if(qtdedisponivel == null) 
			qtdedisponivel = 0.0;
		
		
		return new Double(qtdedisponivel.intValue());
	}
	
	/**
	 * Metodo que retorna uma entidade de vendas para a classe AprovarVendaProcess
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	public Vendaorcamento findVendaorcamento(Vendaorcamento _vendaorcamento){
		Vendaorcamento vendaorcamento = new Vendaorcamento();
		vendaorcamento = vendaorcamentoDAO.findVendaorcamento(_vendaorcamento);
		return vendaorcamento; 
	}
	
	/**
	 * Metodo que cria o relatorio de Geremnciar Venda
	 * 
	 * @return IReport
	 * @author Thiago Augusto
	 */
	public IReport gerarRelatorioListagem(VendaorcamentoFiltro filtro) {
		List<Vendaorcamento> lista = this.findForRelatorio(filtro);
		
		for (Vendaorcamento vendaorcamento : lista) {
			vendaorcamento.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(vendaorcamento));
		}
		
		Report report = new Report("/faturamento/gerenciarvendaorcamento");
		Report subReportVendaMaterial = new Report("/faturamento/vendaorcamentomaterial");
		if(lista != null && lista.size() > 0)
			report.setDataSource(lista);
		report.addSubReport("VENDAMATERIAL", subReportVendaMaterial);
		
		return report;
	}
	
	/**
	 * Metodo que busca a listagem que ser� apresentada no relat�rio
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	private List<Vendaorcamento> findForRelatorio(VendaorcamentoFiltro filtro) {
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		return vendaorcamentoDAO.findForRelatorio(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see	br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#findForCalculoTotalGeralVendas(VendaFiltro filtro)
	* 
	* @param filtro
	* @return
	* @since Oct 31, 2011
	* @author Luiz Fernando F Silva
	*/
	public VendaorcamentoFiltro findForCalculoTotalGeralVendas(VendaorcamentoFiltro filtro){
		return vendaorcamentoDAO.findForCalculoTotalGeralVendas(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaorcamentoService#findForCalculoSaldoVendas(VendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForCalculoSaldoVendas(VendaorcamentoFiltro filtro){
		return vendaorcamentoDAO.findForCalculoSaldoVendas(filtro);
	}
	
	/** M�todo que retorna ordenando da �ltima para a primeira data. 
	 * @author Thiago Augusto
	 * @param cliente
	 * @return
	 */
	public List<Vendaorcamento> findByClienteOrderData(Cliente cliente) {
		return vendaorcamentoDAO.findByClienteOrderData(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#buscaUltimaVendaorcamentoByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento buscaUltimaVendaorcamentoByCliente(Cliente cliente) {
		return vendaorcamentoDAO.buscaUltimaVendaorcamentoByCliente(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#findVendedorForVendaorcamento(Vendaorcamento vendaorcamento)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaborador findVendedorForVendaorcamento(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.findVendedorForVendaorcamento(vendaorcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.vendaorcamentoDAO.isVendaCancelada(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isVendaCancelada(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.isVendaorcamentoCancelada(vendaorcamento);
	}
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findVendaByCdVenda
	 * @param cdvenda
	 * @return
	 * @return Venda
	 * @author Thiago Augusto
	 * @date 21/05/2012
	 *
	 */
	public Vendaorcamento findVendaorcamentoByCdVendaorcamento(Integer cdvendaorcamento){
		return vendaorcamentoDAO.findVendaorcamentoByCdVendaorcamento(cdvendaorcamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#findForCsv(VendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamento> findForCsv(VendaorcamentoFiltro filtro) {
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		return vendaorcamentoDAO.findForCsv(filtro);
	}
	
	/**
	 * M�todo que concatena o alturaXlarguraXcomprimento
	 *
	 * @param vendamaterial
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public String getAlturaLarguraComprimentoConcatenados(Vendaorcamentomaterial vendamaterial, Material material){
		StringBuilder alturaLarguraComprimentoConcatenados = new StringBuilder();
//		Double fatorconversao = 1000.0;
		if(material != null && material.getCdmaterial() != null && vendamaterial != null){
//			Produto materialproduto = produtoService.carregaProduto(material);
//			if(materialproduto != null && materialproduto.getFatorconversao() != null && materialproduto.getFatorconversao() > 0){
//				fatorconversao = materialproduto.getFatorconversao();
//			}
			if(vendamaterial.getLargura() != null && vendamaterial.getLargura() > 0){
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(vendamaterial.getLargura()));
			}
			if(vendamaterial.getAltura() != null && vendamaterial.getAltura() > 0){
				if(!"".equals(alturaLarguraComprimentoConcatenados.toString()))
					alturaLarguraComprimentoConcatenados.append("x");
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(vendamaterial.getAltura()));
			}
			if(vendamaterial.getComprimentooriginal() != null && vendamaterial.getComprimentooriginal() > 0){
				if(!"".equals(alturaLarguraComprimentoConcatenados.toString()))
					alturaLarguraComprimentoConcatenados.append("x");
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(vendamaterial.getComprimentooriginal()));
			}
		}
		return alturaLarguraComprimentoConcatenados.toString();
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name buscarIdentificadorVenda
	 * @param venda
	 * @return
	 * @return Integer
	 * @author Thiago Augusto
	 * @date 13/07/2012
	 *
	 */
	public String buscarIdentificadorVendaorcamento(Vendaorcamento vendaorcamento){
		return vendaorcamentoDAO.buscarIdentificadorVendaorcamento(vendaorcamento);
	}
	
	public String verificaDebitoAcimaLimiteByCliente(String whereInDocumentotipo, Cliente cliente){
		boolean permitevendasemanalise = documentotipoService.getPermiteVendaSemAnaliseByWhereIn(whereInDocumentotipo);
		Money valortotaldebito = new Money();
		String msgdebitoacimalimitecliente = "";
		String paramLimite = parametrogeralService.getValorPorNome(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE);
		if(!permitevendasemanalise && paramLimite != null && "TRUE".equalsIgnoreCase(paramLimite)){
			if(cliente != null && cliente.getCdpessoa() != null){
				cliente = clienteService.carregarTaxapedidovendaCreditolimitecompraByCliente(cliente);
				if(cliente != null && cliente.getCreditolimitecompra() != null && 
						cliente.getCreditolimitecompra() >= 0){
					valortotaldebito = clienteService.getValorDebitoByCliente(cliente);
					if(valortotaldebito != null && cliente.getCreditolimitecompra() != null && valortotaldebito.getValue().doubleValue() > cliente.getCreditolimitecompra()){
						msgdebitoacimalimitecliente = "N�o � poss�vel Realizar a venda. O Cliente possui d�bito(s) acima do permitido.";
					}
				}
			}
		}
		return msgdebitoacimalimitecliente;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#loadWithCliente(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento loadWithCliente(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.loadWithCliente(vendaorcamento);
	}
	
	/**
	 * M�todo que gera o Resource do comprovante de venda
	 *
	 * @param venda
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 * @param comprovante 
	 */
	public Resource criaComprovantevendaorcamentoForEnviaremail(Vendaorcamento vendaorcamento, ComprovanteConfiguravel comprovante) throws Exception {
	
		String nomeArquivo = "comprovante_vendaorcamento_"+SinedUtil.datePatternForReport();
		
		if(comprovante != null && comprovante.getLayout() != null && (comprovante.getTxt() == null || !comprovante.getTxt())){
			String comprovanteHTML = this.gerarComprovanteConfiguravelOnly(comprovante, vendaorcamento);
			comprovanteHTML = SinedUtil.criarUrlsPublicas(comprovanteHTML);
			
			return new WKHTMLBuilder()
							.HTML(comprovanteHTML)
							.resource(nomeArquivo+ ".pdf");
			
		} else {
			MergeReport mergeReport = new MergeReport(nomeArquivo+".pdf");
			
			Report report = criaComprovanteVendaorcamentoReport(vendaorcamento);
			mergeReport.addReport(report);
			
			return mergeReport.generateResource();
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#findForEnviocomprovanteemail(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendaorcamento> findForEnviocomprovanteemail(String whereIn) {
		return vendaorcamentoDAO.findForEnviocomprovanteemail(whereIn);
	}
	
	/**
	 * M�todo que envia comprovante de venda para o fornecedor
	 *
	 * @param request
	 * @param venda
	 * @param empresa
	 * @param fornecedor
	 * @param report
	 * @return
	 * @author Luiz Fernando
	 * @param extensao 
	 * @param mimeType 
	 */
	public Boolean enviaComprovanteEmailRepresentante(WebRequestContext request, Vendaorcamento vendaorcamento, Empresa empresa, Fornecedor fornecedor, Resource report) {		
		if(empresa == null || report == null || fornecedor == null){
			request.addError("Fornecedor/Empresa n�o pode ser nulo.");
			return false;
		}
		
		String template = null;
		try{
			TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioEmailComprovantevenda.tpl");				
			template = templateAux
						.assign("msgcomprovantepedidovenda", "o or�amento")
						.assign("numero", vendaorcamento.getCdvendaorcamento().toString())
						.assign("data", new SimpleDateFormat("dd/MM/yyyy").format(vendaorcamento.getDtorcamento()))
						.assign("empresa", empresa.getRazaosocialOuNome())
						.getTemplate();
			
		} catch (IOException e) {
			request.addError(e.getMessage());
			request.addError("Falha na obten��o do template.");
			return false;
		}
		
		if(fornecedor.getEmail() == null || fornecedor.getEmail().equals("")){
			request.addError("Fornecedor " + fornecedor.getNome() + " sem e-mail cadastrado.");
			return false;
		}	
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(vendaorcamento.getColaborador() != null && vendaorcamento.getColaborador().getCdpessoa() != null){
			usuario = new Usuario(vendaorcamento.getColaborador().getCdpessoa());
		}
		usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.email");
		
		try {
			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			email
				.setFrom((usuario != null && usuario.getEmail() != null ? usuario.getEmail() : vendaorcamento.getEmpresa().getEmail()))
				.setSubject("Comprovante de or�amento")
				.setTo(fornecedor.getEmail())
				.attachFileUsingByteArray(report.getContents(), report.getFileName(), report.getContentType(), "comprovante_vendaorcamento_"+vendaorcamento.getCdvendaorcamento())
				.addHtmlText(template + usuarioService.addImagemassinaturaemailUsuario(usuario, email))
				.sendMessage();
			
			return true;
		} catch (Exception e) {
			request.addError(e.getMessage());
			request.addError("Falha no envio de e-mail do fornecedor " + fornecedor.getNome() + ".");
			return false;
		}
	}
	
	/**
	 * M�todo que verifica se existe fornecedor exclusivo no material
	 *
	 * @param listaVendamaterial
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMaterialFornecedorExclusivo(List<Vendaorcamentomaterial> listaVendaorcamentomaterial, Fornecedor fornecedor){
		Boolean exist = Boolean.FALSE;
		
		if(listaVendaorcamentomaterial != null && !listaVendaorcamentomaterial.isEmpty()){
			for(Vendaorcamentomaterial vendamaterial : listaVendaorcamentomaterial){
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getListaFornecedor() != null &&  
						!vendamaterial.getMaterial().getListaFornecedor().isEmpty()){
					for(Materialfornecedor materialfornecedor : vendamaterial.getMaterial().getListaFornecedor()){
						if(materialfornecedor.getFornecedor() != null && materialfornecedor.getFornecedor().equals(fornecedor)){
							return Boolean.TRUE;
						}
					}
				}
			}
		}
		return exist;
	}	
	
	/**
	 * M�todo que ajusta o valor de venda m�ximo e m�nimo ao carregar a venda
	 * obs: caso a unidade escolhida seja secund�ria
	 *
	 * @param venda
	 * @author Luiz Fernando
	 */
	public void ajustaSaldofinalValorvendaMaximoMinimo(Vendaorcamento vendaorcamento) {
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null && vendaorcamento.getListavendaorcamentomaterial() != null && 
				!vendaorcamento.getListavendaorcamentomaterial().isEmpty()){
			String vendaaldoporminmax = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX);
			Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
			List<Unidademedidaconversao> lista;
			Material material;
			Materialtabelaprecoitem materialtabelaprecoitem = null;
			Materialtabelapreco materialtabelaprecoVenda = vendaorcamento.getMaterialtabelapreco();
			
			for(Vendaorcamentomaterial vendamaterial : vendaorcamento.getListavendaorcamentomaterial()){
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null && 
						vendamaterial.getMaterial().getValorvendaminimo() != null && vendamaterial.getMaterial().getValorvendamaximo() != null){
					lista = unidademedidaconversaoService.conversoesByUnidademedida(vendamaterial.getUnidademedida());
					material = materialService.findListaMaterialunidademedida(vendamaterial.getMaterial());
					Boolean achou = Boolean.FALSE;
					Double valorMin = null;
					Double valorMax = null;
					Double valorVenda = null;
					if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
						for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
							if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
									materialunidademedida.getFracao() != null){
								Double fracao = materialunidademedida.getFracao();
								if(materialunidademedida.getUnidademedida().equals(vendamaterial.getUnidademedida())){
									if(vendamaterial.getMaterial().getValorvendaminimo() != null)
										valorMin = vendamaterial.getMaterial().getValorvendaminimo() / fracao;
									if(vendamaterial.getMaterial().getValorvendamaximo() != null)
										valorMax = vendamaterial.getMaterial().getValorvendamaximo() / fracao;
									if(vendamaterial.getMaterial().getValorvenda() != null)
										valorVenda = vendamaterial.getMaterial().getValorvenda() / fracao;
									achou = Boolean.TRUE;									
									break;
								}
							}
						}
					}
					
					if(!achou) {
						if(lista != null && lista.size() > 0){
							for (Unidademedidaconversao item : lista) {
								if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
									if(item.getUnidademedidarelacionada().equals(vendamaterial.getUnidademedida())){
										if(vendamaterial.getMaterial().getValorvendaminimo() != null)
											valorMin = vendamaterial.getMaterial().getValorvendamaximo() / item.getFracao();
										if(vendamaterial.getMaterial().getValorvendamaximo() != null)
											valorMax = vendamaterial.getMaterial().getValorvendamaximo() / item.getFracao();
										if(vendamaterial.getMaterial().getValorvenda() != null)
											valorVenda = vendamaterial.getMaterial().getValorvenda() / item.getFracao();
										break;
									}
								}
							}
						}
					}
					
					Double valorMaximoTabela = null;
					Double valorMinimoTabela = null;
					Double valorVendaTabela = null;
					
					materialtabelaprecoitem = null;
					if(materialtabelaprecoVenda != null && materialtabelaprecoVenda.getCdmaterialtabelapreco() != null){
						materialtabelaprecoitem = materialtabelaprecoitemService.getItemTabelaPrecoAtual(vendamaterial.getMaterial(), vendamaterial.getUnidademedida(), materialtabelaprecoVenda);
						if(materialtabelaprecoitem != null){
							valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
							valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
							valorVendaTabela = materialtabelaprecoitem.getValor();
						}
					}
					String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
					Integer numCasasDecimais = null; 
					if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
						String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
						if(aux != null && !aux.trim().isEmpty()){
							try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
						}
					}
					if(materialtabelaprecoitem == null){
						Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(vendamaterial.getMaterial(), vendaorcamento.getCliente(), vendaorcamento.getPrazopagamento(), null, vendaorcamento.getEmpresa());
						if(materialtabelapreco != null){
							if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getValorvenda() != null){
								valorVendaTabela = materialtabelapreco.getValorComDescontoAcrescimo(vendamaterial.getMaterial().getValorvenda(), numCasasDecimais);
							}
						}else {
							materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(vendamaterial.getMaterial(), vendaorcamento.getCliente(), null, null, vendaorcamento.getEmpresa(), vendamaterial.getUnidademedida());
							if(materialtabelaprecoitem != null){
								valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
								valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
								valorVendaTabela = materialtabelaprecoitem.getValor();
							}
						}
					}
					
					if(valorMinimoTabela != null){
						valorMin = valorMinimoTabela;
					}
					if(valorMaximoTabela != null){
						valorMax = valorMaximoTabela;
					}
					if(valorVendaTabela != null){
						valorVenda = valorVendaTabela;
					}
					
					if(valorMin != null){
						vendamaterial.getMaterial().setValorvendaminimo(valorMin);
					}
					if(valorMax != null){
						vendamaterial.getMaterial().setValorvendamaximo(valorMax);
					}
					if(valorVenda != null){
						vendamaterial.getMaterial().setValorvenda(valorVenda);
						vendamaterial.setValorvendamaterial(valorVenda);
					}
					
					if("TRUE".equalsIgnoreCase(vendaaldoporminmax)){
						this.calculaSaldoMaterial(vendamaterial, 
								vendamaterial.getPreco(), 
								vendamaterial.getMaterial().getValorvendaminimo(), 
								vendamaterial.getMaterial().getValorvendamaximo(), 
								vendamaterial.getQuantidade(), 
								vendamaterial.getDesconto(), 
								desconsiderardescontosaldoproduto);
					}else {
						this.calculaSaldoMaterialPorValorvenda(vendamaterial, 
								vendamaterial.getPreco(), 
								vendamaterial.getMaterial().getValorvenda(), 
								vendamaterial.getQuantidade(), 
								vendamaterial.getDesconto(),
								desconsiderardescontosaldoproduto);
					}
				}
			}
		}
	}
	
	public void calculaSaldoMaterial(Vendaorcamentomaterial vendaorcamentomaterial, Double preco, Double valorvendaminimo, Double valorvendamaximo, Double qtde, Money desconto, Boolean desconsiderardescontosaldoproduto) {
		Double saldo = 0.0;
		Double desc = 0.0;
		if(desconto != null) desc = desconto.getValue().doubleValue();
		
		if(vendaorcamentomaterial != null && preco != null && qtde != null){
			if(valorvendamaximo != null && preco > valorvendamaximo){
				if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
					saldo = (preco - valorvendamaximo) * qtde;
				}else {
					saldo = ((preco - (desc/qtde)) - valorvendamaximo) * qtde;
				}
			}else if(valorvendaminimo != null && preco < valorvendaminimo){
				if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
					saldo = (preco - valorvendaminimo) * qtde;
				}else {
					saldo = ((preco - (desc/qtde)) - valorvendaminimo) * qtde;
				}
			}
			vendaorcamentomaterial.setSaldo(saldo != null ? saldo : 0.0);
		}
	}
	
	public void calculaSaldoMaterialPorValorvenda(Vendaorcamentomaterial vendaorcamentomaterial, Double preco, Double valorvenda, Double qtde, Money desconto, Boolean desconsiderardescontosaldoproduto) {
		Double saldo = 0.0;
		Double desc = 0.0;
		if(desconto != null) desc = desconto.getValue().doubleValue();
		
		if(vendaorcamentomaterial != null && preco != null && qtde != null && valorvenda != null){
			if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
				saldo = (preco - valorvenda) * qtde;
			}else {
				saldo = ((preco - (desc/qtde)) - valorvenda) * qtde;
			}
			vendaorcamentomaterial.setSaldo(saldo != null ? saldo : 0.0);
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.vendaorcamentoDAO#loadWithMaterialtabelapreco(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento loadWithMaterialtabelapreco(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.loadWithMaterialtabelapreco(vendaorcamento);
	}
	
	/**
	 * Preeche uma lista de Vendamaterial a partir dos itens de um or�amento.
	 *
	 * @param listavendaorcamentomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/11/2012
	 */
	public List<Vendamaterial> preecheVendaMaterial(List<Vendaorcamentomaterial> listavendaorcamentomaterial, Empresa empresa) {
		HashMap<String, Money> mapFornecedorEmpresaComissao = new HashMap<String, Money>();
		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
		Vendamaterial vendamaterial;
		for (Vendaorcamentomaterial vendaorcamentomaterial : listavendaorcamentomaterial) {
			vendamaterial = new Vendamaterial();
			
			ImpostoUtil.copyDadosImposto(vendaorcamentomaterial, vendamaterial);
			
			vendamaterial.setOrdem(vendaorcamentomaterial.getOrdem());
			vendamaterial.setVendaorcamentomaterial(vendaorcamentomaterial);
			vendamaterial.setMaterial(vendaorcamentomaterial.getMaterial());
			vendamaterial.setMaterialmestre(vendaorcamentomaterial.getMaterialmestre());
			vendamaterial.setQuantidade(vendaorcamentomaterial.getQuantidade());
			vendamaterial.setMultiplicador(vendaorcamentomaterial.getMultiplicador() != null ? vendaorcamentomaterial.getMultiplicador() : 1.0);
			vendamaterial.setComprimento(vendaorcamentomaterial.getComprimento());
			vendamaterial.setComprimentooriginal(vendaorcamentomaterial.getComprimentooriginal());
			vendamaterial.setLargura(vendaorcamentomaterial.getLargura());
			vendamaterial.setAltura(vendaorcamentomaterial.getAltura());
			vendamaterial.setFatorconversao(vendaorcamentomaterial.getFatorconversao());
			vendamaterial.setPreco(vendaorcamentomaterial.getPreco());
			vendamaterial.setDesconto(vendaorcamentomaterial.getDesconto());
			vendamaterial.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto());
			vendamaterial.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
			vendamaterial.setPrazoentrega(vendaorcamentomaterial.getPrazoentrega());
			vendamaterial.setObservacao(vendaorcamentomaterial.getObservacao());
			vendamaterial.setLoteestoque(vendaorcamentomaterial.getLoteestoque());
			vendamaterial.setFornecedor(vendaorcamentomaterial.getFornecedor());
			vendamaterial.setPercentualcomissaoagencia(vendaorcamentomaterial.getPercentualcomissaoagencia());
			vendamaterial.setAliquotareaisipi(vendaorcamentomaterial.getAliquotareaisipi());
			vendamaterial.setTipocalculoipi(vendaorcamentomaterial.getTipocalculoipi());
			vendamaterial.setComissionamento(vendaorcamentomaterial.getComissionamento());
			vendamaterial.setPeso(vendaorcamentomaterial.getPesoVendaOuMaterial());
			vendamaterial.setValorvendamaterial(vendaorcamentomaterial.getValorvendamaterial());
			
			if(vendaorcamentomaterial.getFornecedor() != null){
				String fornecedorEmpresaId = vendaorcamentomaterial.getFornecedor().getCdpessoa() + "-" + empresa.getCdpessoa();
				if(mapFornecedorEmpresaComissao.get(fornecedorEmpresaId) == null){
					mapFornecedorEmpresaComissao.put(fornecedorEmpresaId, empresarepresentacaoService.getPercentualComissaoFornecedor(empresa, vendaorcamentomaterial.getFornecedor()));
				}
				vendamaterial.setPercentualrepresentacao(mapFornecedorEmpresaComissao.get(fornecedorEmpresaId));
			}
			vendamaterial.setValorcustomaterial(vendaorcamentomaterial.getMaterial().getValorcusto());
			vendamaterial.setQtdereferencia(vendaorcamentomaterial.getQtdereferencia() != null ? vendaorcamentomaterial.getQtdereferencia() : 1.0);
			vendamaterial.setSaldo(materialService.calcularSaldoMaterial( 
					vendaorcamentomaterial.getMaterial().getValorvendaminimo(), 
					vendaorcamentomaterial.getMaterial().getValorvendamaximo(),
					vendaorcamentomaterial.getPreco(),
					vendaorcamentomaterial.getQuantidade()));
			
			vendamaterial.setExistematerialsimilar(vendaorcamentomaterial.getExistematerialsimilar());
			vendamaterial.setIdentificadorinterno(vendaorcamentomaterial.getIdentificadorinterno());
			vendamaterial.setValorMinimo(vendaorcamentomaterial.getValorMinimo());
			
			if(vendamaterial.getMaterial() != null){
				vendamaterial.setIsMaterialmestregrade(materialService.existMaterialitemByMaterialgrademestre(vendamaterial.getMaterial()));
				if(vendamaterial.getIsMaterialmestregrade() == null || !vendamaterial.getIsMaterialmestregrade()) 
					vendamaterial.setIsControlegrade(materialService.isControleGrade(vendamaterial.getMaterial()));
			}
			
			listaVendamaterial.add(vendamaterial);
		}
		
		return listaVendamaterial;
	}
	
	public List<Vendamaterialmestre> preecheVendaMaterialmestre(List<Vendaorcamentomaterialmestre> listavendaorcamentomaterialmestre) {
		
		List<Vendamaterialmestre> listaVendamaterialmestre = new ArrayList<Vendamaterialmestre>();
		Vendamaterialmestre vendamaterialmestre;
		for (Vendaorcamentomaterialmestre vendaorcamentomaterialmestre : listavendaorcamentomaterialmestre) {
			vendamaterialmestre = new Vendamaterialmestre();
			vendamaterialmestre.setAltura(vendaorcamentomaterialmestre.getAltura());
			vendamaterialmestre.setComprimento(vendaorcamentomaterialmestre.getComprimento());
			vendamaterialmestre.setLargura(vendaorcamentomaterialmestre.getLargura());
			vendamaterialmestre.setMaterial(vendaorcamentomaterialmestre.getMaterial());
			vendamaterialmestre.setPeso(vendaorcamentomaterialmestre.getPeso());
			vendamaterialmestre.setQtde(vendaorcamentomaterialmestre.getQtde());
			vendamaterialmestre.setIdentificadorinterno(vendaorcamentomaterialmestre.getIdentificadorinterno());
			vendamaterialmestre.setPreco(vendaorcamentomaterialmestre.getPreco());
			vendamaterialmestre.setDtprazoentrega(vendaorcamentomaterialmestre.getDtprazoentrega());
			vendamaterialmestre.setExibiritenskitflexivel(vendaorcamentomaterialmestre.getExibiritenskitflexivel());
			vendamaterialmestre.setNomealternativo(vendaorcamentomaterialmestre.getNomealternativo());
			vendamaterialmestre.setQtdeanterior(vendaorcamentomaterialmestre.getQtde());
			vendamaterialmestre.setUnidademedida(vendaorcamentomaterialmestre.getMaterial().getUnidademedida());
			vendamaterialmestre.setDesconto(vendaorcamentomaterialmestre.getDesconto());
			
			vendamaterialmestre.setValoripi(vendaorcamentomaterialmestre.getValoripi());
			vendamaterialmestre.setIpi(vendaorcamentomaterialmestre.getIpi());
			vendamaterialmestre.setTipocobrancaipi(vendaorcamentomaterialmestre.getTipocobrancaipi());
			vendamaterialmestre.setAliquotaReaisIpi(vendaorcamentomaterialmestre.getAliquotaReaisIpi());
			vendamaterialmestre.setTipoCalculoIpi(vendaorcamentomaterialmestre.getTipoCalculoIpi());
			vendamaterialmestre.setGrupotributacao(vendaorcamentomaterialmestre.getGrupotributacao());
			
			ImpostoUtil.copyDadosImposto(vendaorcamentomaterialmestre, vendamaterialmestre);
			
			listaVendamaterialmestre.add(vendamaterialmestre);
		}
		
		return listaVendamaterialmestre;
	}
	
	public List<Vendavalorcampoextra> preecheVendavalorcampoextra(List<Orcamentovalorcampoextra> listaOrcamentovalorcampoextra) {
		List<Vendavalorcampoextra> lista = new ArrayList<Vendavalorcampoextra>();
		Vendavalorcampoextra vendavalorcampoextra;
		if(Hibernate.isInitialized(listaOrcamentovalorcampoextra) && SinedUtil.isListNotEmpty(listaOrcamentovalorcampoextra)){
			for (Orcamentovalorcampoextra bean : listaOrcamentovalorcampoextra) {
				vendavalorcampoextra = new Vendavalorcampoextra();
				vendavalorcampoextra.setCampoextrapedidovendatipo(bean.getCampoextrapedidovendatipo());
				vendavalorcampoextra.setValor(bean.getValor());
				lista.add(vendavalorcampoextra);
			}
		}
		
		return lista;
	}
	
	public List<Pedidovendavalorcampoextra> preechePedidoVendavalorcampoextra(List<Orcamentovalorcampoextra> listaOrcamentovalorcampoextra) {
		List<Pedidovendavalorcampoextra> lista = new ArrayList<Pedidovendavalorcampoextra>();
		Pedidovendavalorcampoextra pedidovendavalorcampoextra;
		if(Hibernate.isInitialized(listaOrcamentovalorcampoextra) && SinedUtil.isListNotEmpty(listaOrcamentovalorcampoextra)){
			for (Orcamentovalorcampoextra bean : listaOrcamentovalorcampoextra) {
				pedidovendavalorcampoextra = new Pedidovendavalorcampoextra();
				pedidovendavalorcampoextra.setCampoextrapedidovendatipo(bean.getCampoextrapedidovendatipo());
				pedidovendavalorcampoextra.setValor(bean.getValor());
				lista.add(pedidovendavalorcampoextra);
			}
		}
		
		return lista;
	}
	
	/**
	 * Preeche uma lista de Pedidovendamaterial a partir dos itens de um or�amento.
	 *
	 * @param listavendaorcamentomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/11/2012
	 */
	public List<Pedidovendamaterial> preechePedidoVendaMaterial(List<Vendaorcamentomaterial> listavendaorcamentomaterial, Empresa empresa) {
		
		HashMap<String, Money> mapFornecedorEmpresaComissao = new HashMap<String, Money>();
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		Pedidovendamaterial pedidovendamaterial;
		Double saldo = 0.0;
		String vendaaldoporminmax = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX);
		Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
		for (Vendaorcamentomaterial vendaorcamentomaterial : listavendaorcamentomaterial) {
			pedidovendamaterial = new Pedidovendamaterial();
			
			ImpostoUtil.copyDadosImposto(vendaorcamentomaterial, pedidovendamaterial);
			
			pedidovendamaterial.setOrdem(vendaorcamentomaterial.getOrdem());
			pedidovendamaterial.setVendaorcamentomaterial(vendaorcamentomaterial);
			pedidovendamaterial.setMaterial(vendaorcamentomaterial.getMaterial());
			pedidovendamaterial.setQuantidade(vendaorcamentomaterial.getQuantidade());
			pedidovendamaterial.setMultiplicador(vendaorcamentomaterial.getMultiplicador());
			pedidovendamaterial.setFatorconversao(vendaorcamentomaterial.getFatorconversao());
			pedidovendamaterial.setQtdereferencia(vendaorcamentomaterial.getQtdereferencia());
			pedidovendamaterial.setPreco(vendaorcamentomaterial.getPreco());
			pedidovendamaterial.setDesconto(vendaorcamentomaterial.getDesconto());
			pedidovendamaterial.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto());
			pedidovendamaterial.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
			pedidovendamaterial.setObservacao(vendaorcamentomaterial.getObservacao());
			pedidovendamaterial.setLoteestoque(vendaorcamentomaterial.getLoteestoque());
			pedidovendamaterial.setFornecedor(vendaorcamentomaterial.getFornecedor());
			pedidovendamaterial.setPercentualcomissaoagencia(vendaorcamentomaterial.getPercentualcomissaoagencia());
			pedidovendamaterial.setPeso(vendaorcamentomaterial.getPesoVendaOuMaterial());
			pedidovendamaterial.setValorMinimo(vendaorcamentomaterial.getValorMinimo());
			if(vendaorcamentomaterial.getFornecedor() != null && vendaorcamentomaterial.getFornecedor().getCdpessoa() != null && 
					empresa != null && empresa.getCdpessoa() != null){
				String fornecedorEmpresaId = vendaorcamentomaterial.getFornecedor().getCdpessoa() + "-" + empresa.getCdpessoa();
				if(mapFornecedorEmpresaComissao.get(fornecedorEmpresaId) == null){
					mapFornecedorEmpresaComissao.put(fornecedorEmpresaId, empresarepresentacaoService.getPercentualComissaoFornecedor(empresa, vendaorcamentomaterial.getFornecedor()));
				}
				pedidovendamaterial.setPercentualrepresentacao(mapFornecedorEmpresaComissao.get(fornecedorEmpresaId));
			}
			
			pedidovendamaterial.setValorcustomaterial(vendaorcamentomaterial.getMaterial().getValorcusto());
			pedidovendamaterial.setAltura(vendaorcamentomaterial.getAltura());
			pedidovendamaterial.setLargura(vendaorcamentomaterial.getLargura());
			pedidovendamaterial.setComprimento(vendaorcamentomaterial.getComprimento());
			pedidovendamaterial.setComprimentooriginal(vendaorcamentomaterial.getComprimentooriginal());
			pedidovendamaterial.setMaterialmestre(vendaorcamentomaterial.getMaterialmestre());
			pedidovendamaterial.setIdentificadorinterno(vendaorcamentomaterial.getIdentificadorinterno());
			pedidovendamaterial.setAliquotareaisipi(vendaorcamentomaterial.getAliquotareaisipi());
			pedidovendamaterial.setTipocalculoipi(vendaorcamentomaterial.getTipocalculoipi());
			pedidovendamaterial.setValorvendamaterial(vendaorcamentomaterial.getValorvendamaterial());
			
			if(pedidovendamaterial.getMaterial() != null)
				pedidovendamaterial.setIsMaterialmestregrade(materialService.existMaterialitemByMaterialgrademestre(pedidovendamaterial.getMaterial()));
			
			if("TRUE".equalsIgnoreCase(vendaaldoporminmax)){
				this.calculaSaldoMaterial(vendaorcamentomaterial, 
						vendaorcamentomaterial.getPreco(), 
						vendaorcamentomaterial.getMaterial().getValorvendaminimo(), 
						vendaorcamentomaterial.getMaterial().getValorvendamaximo(), 
						vendaorcamentomaterial.getQuantidade(), 
						vendaorcamentomaterial.getDesconto(),
						desconsiderardescontosaldoproduto);
			}else {
				this.calculaSaldoMaterialPorValorvenda(vendaorcamentomaterial, 
						vendaorcamentomaterial.getPreco(), 
						vendaorcamentomaterial.getMaterial().getValorvenda(), 
						vendaorcamentomaterial.getQuantidade(), 
						vendaorcamentomaterial.getDesconto(),
						desconsiderardescontosaldoproduto);
			}
			if(saldo != null){
				pedidovendamaterial.setSaldo(new Money(saldo));
			}
			if(vendaorcamentomaterial.getPrazoentrega() != null){
				pedidovendamaterial.setDtprazoentrega(new Date(vendaorcamentomaterial.getPrazoentrega().getTime()));
			}
			pedidovendamaterial.setFornecedor(vendaorcamentomaterial.getFornecedor());
			listaPedidovendamaterial.add(pedidovendamaterial);
		}
		
		return listaPedidovendamaterial;
	}
	
	public List<Pedidovendamaterialmestre> preechePedidovendaMaterialmestre(List<Vendaorcamentomaterialmestre> listavendaorcamentomaterialmestre) {
		
		List<Pedidovendamaterialmestre> listaVendamaterialmestre = new ArrayList<Pedidovendamaterialmestre>();
		Pedidovendamaterialmestre pedidovendamaterialmestre;
		for (Vendaorcamentomaterialmestre vendaorcamentomaterialmestre : listavendaorcamentomaterialmestre) {
			pedidovendamaterialmestre = new Pedidovendamaterialmestre();
			
			ImpostoUtil.copyDadosImposto(vendaorcamentomaterialmestre, pedidovendamaterialmestre);
			
			pedidovendamaterialmestre.setAltura(vendaorcamentomaterialmestre.getAltura());
			pedidovendamaterialmestre.setComprimento(vendaorcamentomaterialmestre.getComprimento());
			pedidovendamaterialmestre.setLargura(vendaorcamentomaterialmestre.getLargura());
			pedidovendamaterialmestre.setMaterial(vendaorcamentomaterialmestre.getMaterial());
			pedidovendamaterialmestre.setPeso(vendaorcamentomaterialmestre.getPeso());
			pedidovendamaterialmestre.setQtde(vendaorcamentomaterialmestre.getQtde());
			pedidovendamaterialmestre.setIdentificadorinterno(vendaorcamentomaterialmestre.getIdentificadorinterno());
			pedidovendamaterialmestre.setPreco(vendaorcamentomaterialmestre.getPreco());
			pedidovendamaterialmestre.setDtprazoentrega(vendaorcamentomaterialmestre.getDtprazoentrega());
			pedidovendamaterialmestre.setExibiritenskitflexivel(vendaorcamentomaterialmestre.getExibiritenskitflexivel());
			pedidovendamaterialmestre.setNomealternativo(vendaorcamentomaterialmestre.getNomealternativo());
			pedidovendamaterialmestre.setQtdeanterior(vendaorcamentomaterialmestre.getQtde());
			pedidovendamaterialmestre.setUnidademedida(vendaorcamentomaterialmestre.getMaterial().getUnidademedida());
			pedidovendamaterialmestre.setDesconto(vendaorcamentomaterialmestre.getDesconto());
			
			pedidovendamaterialmestre.setAliquotaReaisIpi(vendaorcamentomaterialmestre.getAliquotaReaisIpi());
			pedidovendamaterialmestre.setTipoCalculoIpi(vendaorcamentomaterialmestre.getTipoCalculoIpi());
			
			listaVendamaterialmestre.add(pedidovendamaterialmestre);
		}
		
		return listaVendamaterialmestre;
	}
	
	/**
	 * M�todo que verifica se o material tem materiais similares
	 *
	 * @param listavendaorcamentomaterial
	 * @author Luiz Fernando
	 */
	public void verificaTrocaMaterialsimilar(List<Vendaorcamentomaterial> listavendaorcamentomaterial) {
		if(listavendaorcamentomaterial != null && !listavendaorcamentomaterial.isEmpty()){
			for(Vendaorcamentomaterial vendaorcamentomaterial : listavendaorcamentomaterial){
				if(vendaorcamentomaterial.getMaterial() != null && vendaorcamentomaterial.getMaterial().getCdmaterial() != null){
					vendaorcamentomaterial.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(vendaorcamentomaterial.getMaterial()));
				}
			}
		}		
	}
	
	/**
	 * M�todo que copia o or�amento
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento criarCopia(Vendaorcamento origem) {
		origem = loadForEntrada(origem);
		
		if (origem.getColaborador()!=null && origem.getColaborador().getCdpessoa()!=null){
			origem.setColaborador(colaboradorService.load(origem.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
		}
		if (origem.getAgencia()!=null && origem.getAgencia().getCdpessoa()!=null){
			origem.setAgencia(fornecedorService.load(origem.getAgencia(), "fornecedor.cdpessoa, fornecedor.nome"));
		}
		if (origem.getCliente()!=null && origem.getCliente().getCdpessoa()!=null){
			origem.setCliente(clienteService.load(origem.getCliente(), "cliente.cdpessoa, cliente.nome"));
		}
		if (Util.objects.isPersistent(origem.getClienteindicacao())) {
			origem.setClienteindicacao(clienteService.load(origem.getClienteindicacao(), "cliente.cdpessoa, cliente.nome"));
		}
		if (origem.getContacrm()!=null && origem.getContacrm().getCdcontacrm()!=null){
			origem.setContacrm(contacrmService.load(origem.getContacrm(), "contacrm.cdcontacrm, contacrm.nome"));
		}
		if (origem.getLocalarmazenagem()!=null && origem.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
			origem.setLocalarmazenagem(localarmazenagemService.load(origem.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		}
		origem.setOportunidade(null);
		origem.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(origem));
		origem.setListavendaorcamentomaterialmestre(vendaorcamentomaterialmestreService.findByVendaOrcamento(origem));
		origem.setListaOrcamentovalorcampoextra(orcamentovalorcampoextraService.findByVendaorcamento(origem));
		this.ajustaSaldofinalValorvendaMaximoMinimo(origem);
		
		Vendaorcamento copia = new Vendaorcamento();
		copia = origem;
		copia.setCdvendaorcamento(null);
		copia.setCdvendaorcamento(null);
		copia.setDtorcamento(new Date(System.currentTimeMillis()));
		copia.setVendaorcamentosituacao(null);
		copia.setIdentificador(null);
		copia.setListavendaorcamentoformapagamento(null);
		copia.setEnderecoFaturamento(origem.getEnderecoFaturamento());
		if(copia.getProjeto() != null && copia.getProjeto().getCdprojeto() != null){
			Projeto projetoCopiado = projetoService.load(copia.getProjeto());
			if(Situacaoprojeto.CANCELADO.equals(projetoCopiado.getSituacao()) || Situacaoprojeto.CONCLUIDO.equals(projetoCopiado.getSituacao())){
				copia.setProjeto(null);
			}
		}
		
		Map<String, String> mapaIdentificador = new HashMap<String, String>();
		if(origem.getListavendaorcamentomaterialmestre() != null && !origem.getListavendaorcamentomaterialmestre().isEmpty()){
			Integer i = 0; 
			for(Vendaorcamentomaterialmestre item : origem.getListavendaorcamentomaterialmestre()){
				item.setCdvendaorcamentomaterialmestre(null);
				item.setVendaorcamento(copia);
				if(item.getIdentificadorinterno() != null && !item.getIdentificadorinterno().trim().isEmpty()){		
					String identificador = System.currentTimeMillis() + i.toString() + item.getMaterial().getCdmaterial().toString();
					mapaIdentificador.put(item.getIdentificadorinterno(), identificador);
					item.setIdentificadorinterno(identificador);
					i++;
				}
			}
			copia.setListavendaorcamentomaterialmestre(origem.getListavendaorcamentomaterialmestre());
		}
		if(origem.getListavendaorcamentomaterial() != null && !origem.getListavendaorcamentomaterial().isEmpty()){
			for(Vendaorcamentomaterial item : origem.getListavendaorcamentomaterial()){
				item.setCdvendaorcamentomaterial(null);
				item.setVendaorcamento(copia);
				item.setValorcustomaterial(item.getMaterial().getValorcusto());
				item.setComissionamento(null);
				if(item.getIdentificadorinterno() != null && !item.getIdentificadorinterno().trim().isEmpty()
						&& mapaIdentificador.containsKey(item.getIdentificadorinterno())){
					item.setIdentificadorinterno(mapaIdentificador.get(item.getIdentificadorinterno()));
				}
				
				item.setPercentualcomissaoagencia(null);
				if(origem.getAgencia() != null){
					Materialgrupocomissaovenda mgca = materialgrupocomissaovendaService.getComissaoAgencia(
							item.getMaterial(),
							item.getFornecedor(),
							origem.getAgencia(),
							null,
							origem.getPedidovendatipo()
									);
					if(mgca != null && mgca.getComissionamento() != null && mgca.getComissionamento().getPercentual() != null){
						item.setPercentualcomissaoagencia(mgca.getComissionamento().getPercentual());
					}
				}
			}
			copia.setListavendaorcamentomaterial(origem.getListavendaorcamentomaterial());
		}
		if(origem.getListaOrcamentovalorcampoextra() != null && !origem.getListaOrcamentovalorcampoextra().isEmpty()){
			for(Orcamentovalorcampoextra item : origem.getListaOrcamentovalorcampoextra()){
				item.setCdorcamentovalorcampoextra(null);
				item.setVendaorcamento(null);
			}
			copia.setListaOrcamentovalorcampoextra(origem.getListaOrcamentovalorcampoextra());
		}
		return copia;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#updateVendaorcamentoByCancelamentoOrigem(String whereInPedidovenda, String whereInVenda, String whereInContrato)
	 *
	 * @param whereInPedidovenda
	 * @author Luiz Fernando
	 */
	public void updateVendaorcamentoByCancelamentoOrigem(String whereInPedidovenda, String whereInVenda, String whereInContrato) {
		vendaorcamentoDAO.updateVendaorcamentoByCancelamentoOrigem(whereInPedidovenda, whereInVenda, whereInContrato);
	}
	
	public void updateVendaorcamentoByCancelamentoOrigemPedidovenda(String whereInPedidovenda) {
		this.updateVendaorcamentoByCancelamentoOrigem(whereInPedidovenda, null, null);
	}
	
	public void updateVendaorcamentoByCancelamentoOrigemVenda(String whereInVenda) {
		this.updateVendaorcamentoByCancelamentoOrigem(null, whereInVenda, null);
	}
	
	public void updateVendaorcamentoByCancelamentoOrigemContrato(String whereInContrato) {
		this.updateVendaorcamentoByCancelamentoOrigem(null, null, whereInContrato);
	}
	
	/**
	 * M�todo que retorna a lista de vendaorcamentomaterial com os valores de pre�o, valorminimo e valormaximo
	 * 
	 * @param vendaorcamento
	 * @return
	 */
	public List<Vendaorcamentomaterial> getListaVendaorcamentomaterialForCriarTabela(Vendaorcamento vendaorcamento) {
		List<Vendaorcamentomaterial> listaVendamaterial = new ArrayList<Vendaorcamentomaterial>();
		if(vendaorcamento != null && SinedUtil.isListNotEmpty(vendaorcamento.getListavendaorcamentomaterial())){
			Double valorvalecompra = vendaorcamento.getValorusadovalecompra() != null ? vendaorcamento.getValorusadovalecompra().getValue().doubleValue() : 0d;
			Double desconto = vendaorcamento.getDesconto() != null ? vendaorcamento.getDesconto().getValue().doubleValue() : 0d;
			Double valortotal = vendaorcamento.getTotalvendaSemArredondamento().getValue().doubleValue();
			
			valortotal += desconto + valorvalecompra;
			Vendaorcamentomaterial vendamaterial;
			for(Vendaorcamentomaterial vom : vendaorcamento.getListavendaorcamentomaterial()){
				vendamaterial = new Vendaorcamentomaterial();
				
				vendamaterial.setMaterial(new Material(vom.getMaterial().getCdmaterial()));
				vendamaterial.setUnidademedida(vom.getUnidademedida());
				vendamaterial.setPreco(vom.getPreco());
				vendamaterial.getMaterial().setValorvendaminimo(vom.getMaterial().getValorvendaminimo());
				vendamaterial.getMaterial().setValorvendamaximo(vom.getMaterial().getValorvendamaximo());
				vendamaterial.setDesconto(vom.getDesconto());
				vendamaterial.setPercentualdesconto(vom.getPercentualdesconto());
				vendamaterial.setCdvendaorcamentomaterial(vom.getCdvendaorcamentomaterial());
				
				
				vendaService.calcularMarkup(vendaorcamento, vom, false);
				
				listaVendamaterial.add(vendamaterial);
			}
		}
		return listaVendamaterial;
	}
	
	public List<Vendaorcamento> findForSolicitacaocompra(String whereIn){
		return vendaorcamentoDAO.findForSolicitacaocompra(whereIn);
	}
	
	public List<Vendaorcamento> findForOrdemcompra(String whereInCdvendaorcamento) {
		return vendaorcamentoDAO.findForOrdemcompra(whereInCdvendaorcamento);
	}
	
	public boolean haveVendaorcamentoDiferenteSituacao(String whereIn, Vendaorcamentosituacao... situacoes) {
		return vendaorcamentoDAO.haveVendaorcamentoDiferenteSituacao(whereIn, situacoes);
	}
	
	/**
	 * M�todo para ordenar os materiais de produ��o
	 *
	 * @param venda
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public void ordenarMaterialproducao(Vendaorcamento venda) {
		try {
			
			if(venda != null && venda.getListavendaorcamentomaterial() != null && !venda.getListavendaorcamentomaterial().isEmpty()){
				Map<Material, List<Vendaorcamentomaterial>> mapMaterialmestre = new HashMap<Material, List<Vendaorcamentomaterial>>();
				StringBuilder whereInCdmaterialmestre = new StringBuilder();
				List<Vendaorcamentomaterial> listaVendamaterialOrdenada = new ArrayList<Vendaorcamentomaterial>();
				List<Vendaorcamentomaterial> listaVendamaterialAvulso = new ArrayList<Vendaorcamentomaterial>();
				
				for(Vendaorcamentomaterial vendamaterial : venda.getListavendaorcamentomaterial()){
					if(vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().getProducao() != null && 
							vendamaterial.getMaterialmestre().getProducao()){
						if(mapMaterialmestre.get(vendamaterial.getMaterialmestre()) != null){
							List<Vendaorcamentomaterial> listaVendamaterial = mapMaterialmestre.get(vendamaterial.getMaterialmestre());
							listaVendamaterial.add(vendamaterial);
							mapMaterialmestre.put(vendamaterial.getMaterialmestre(), listaVendamaterial);
						}else {
							if(!whereInCdmaterialmestre.toString().equals(""))
								whereInCdmaterialmestre.append(",");
							whereInCdmaterialmestre.append(vendamaterial.getMaterialmestre().getCdmaterial());
							
							List<Vendaorcamentomaterial> listaVendamaterial = new ArrayList<Vendaorcamentomaterial>();
							listaVendamaterial.add(vendamaterial);
							mapMaterialmestre.put(vendamaterial.getMaterialmestre(), listaVendamaterial);
							
							
						}
					}else {
						listaVendamaterialAvulso.add(vendamaterial);
					}
				}
				
				if(!whereInCdmaterialmestre.toString().equals("")){
					List<Material> listaMaterial = materialService.findForOrdenarMaterialproducao(whereInCdmaterialmestre.toString());
					if(listaMaterial != null && !listaMaterial.isEmpty()){
						for(Material materialmestre : listaMaterial){
							List<Vendaorcamentomaterial> lista_aux = mapMaterialmestre.get(materialmestre);
							if(materialmestre.getListaProducao() != null && !materialmestre.getListaProducao().isEmpty() && 
									mapMaterialmestre.get(materialmestre) != null){
								for(Materialproducao materialproducao : materialmestre.getListaProducao()){
									if(materialproducao.getMaterial() != null){
										for(Vendaorcamentomaterial vendamaterial : lista_aux){
											if(materialproducao.getMaterial().equals(vendamaterial.getMaterial())){
												vendamaterial.setOrdemexibicao(materialproducao.getOrdemexibicao());
												break;
											}
										}
									}
								}
							}
							
							Collections.sort(lista_aux,new Comparator<Vendaorcamentomaterial>(){
								public int compare(Vendaorcamentomaterial vm1, Vendaorcamentomaterial vm2) {
									try {
										return vm1.getOrdemexibicao().compareTo(vm2.getOrdemexibicao());
									} catch (Exception e) {
										return 1;
									}
								}
							});
							
						}
						
						if(mapMaterialmestre.size() > 0){
							for(List<Vendaorcamentomaterial> lista : mapMaterialmestre.values()){
								listaVendamaterialOrdenada.addAll(lista);
							}
							
							listaVendamaterialOrdenada.addAll(listaVendamaterialAvulso);
							
							if(listaVendamaterialOrdenada.size() == venda.getListavendaorcamentomaterial().size()){
								venda.setListavendaorcamentomaterial(listaVendamaterialOrdenada);
							}
						}
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public AjaxRealizarVendaPagamentoBean pagamento(Vendaorcamento vendaorcamento, Date dtprazoentrega){
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(vendaorcamento.getPrazopagamento());
		
		Prazopagamento pg = prazopagamentoService.load(vendaorcamento.getPrazopagamento());
		Double juros = pg.getJuros();
		Tipoiniciodata tipoiniciodata = Tipoiniciodata.DATA_VENDA;
		if(pg.getTipoiniciodata() != null)
			tipoiniciodata = pg.getTipoiniciodata();
		
		Boolean isValorminimo = Boolean.TRUE;
		if(pg.getPrazomedio() != null && pg.getPrazomedio() && pg.getValorminimo() != null){
			if(vendaorcamento.getValor() < pg.getValorminimo().getValue().doubleValue())
				isValorminimo = Boolean.FALSE;			
		}
		
		AjaxRealizarVendaPagamentoBean bean = new AjaxRealizarVendaPagamentoBean();
		if(pg.getPrazomedio() != null && pg.getPrazomedio())
			bean.setParcela(vendaorcamento.getQtdeParcelas() != null ? vendaorcamento.getQtdeParcelas() : 1);
		else
			bean.setParcela(listaPagItem.size());
		
		AjaxRealizarVendaPagamentoItemBean item;
		Money valor;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date dtparcela;
		
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();

		Date dtvencimentoAnterior = vendaorcamento.getDtorcamento();
		if(Tipoiniciodata.DATA_ENTREGA.equals(tipoiniciodata)){
			if(dtprazoentrega != null){
				try {
					dtvencimentoAnterior = dtprazoentrega;
					if(pg.getDiasparainiciar() != null && pg.getDiasparainiciar() > 0){
						dtvencimentoAnterior = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), pg.getDiasparainiciar().intValue(), Calendar.DAY_OF_MONTH);
					}
				} catch (Exception e) {}
				
			}
		}
		
		if(pg.getPrazomedio() != null && pg.getPrazomedio()){
			Prazopagamentoitem it = listaPagItem.get(0);
			
			if(it.getDias() != null){
				if(bean.getParcela() % 2 != 0){
					Integer meio = (bean.getParcela()/2) + (bean.getParcela() % 2);
					Integer dias = it.getDias();
					Date dtParcelaMeio = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), dias, Calendar.DAY_OF_MONTH); 
					Integer intervaloParcealas = bean.getParcela(); 
					if(bean.getParcela() > 1)
						intervaloParcealas = SinedDateUtils.diferencaDias(dtParcelaMeio, dtvencimentoAnterior) / (bean.getParcela() / 2);
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(vendaorcamento.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(vendaorcamento.getValor()), bean.getParcela(), juros);
						}
						item.setValor(valor.toString());
						
						
						if( (meio-1) == i){
							dtparcela = dtParcelaMeio;
						}else if(i == 0){
							dtparcela = dtvencimentoAnterior;
						}else if(i < (meio - 1)){
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), -intervaloParcealas, Calendar.DAY_OF_MONTH);
						}else {
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						}
						
						item.setDtparcela(formatador.format(dtparcela));
						dtvencimentoAnterior = dtparcela;
						lista.add(item);
					}
				}else {					
					Integer intervaloParcealas = it.getDias() * 2 / bean.getParcela();
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(vendaorcamento.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(vendaorcamento.getValor()), bean.getParcela(), juros);
						}
						item.setValor(valor.toString());
						
						if(i == 0)
							dtparcela = dtvencimentoAnterior;
						else
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						
						item.setDtparcela(formatador.format(dtparcela));
						dtvencimentoAnterior = dtparcela;						
						lista.add(item);
					}
				}
			}
		}else{
			for (Prazopagamentoitem it : listaPagItem) {
				item = new AjaxRealizarVendaPagamentoItemBean();
				
				if(juros == null || juros.equals(0d)){			  
//					valor = new Money(vendaorcamento.getValor()).divide(new Money(bean.getParcela()));
					Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(vendaorcamento.getValor() / bean.getParcela()), 2).doubleValue();
					valor = new Money(valorTruncado); 
				}else{
					valor = calculaJuros(new Money(vendaorcamento.getValor()), bean.getParcela(), juros);
				}
				item.setValor(valor.toString());
				
				
				if(it.getDias() != null && it.getDias() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), it.getDias().intValue(), Calendar.DAY_OF_MONTH);
				} else if(it.getMeses() != null && it.getMeses() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), it.getMeses().intValue(), Calendar.MONTH);
				} else {
					dtparcela = dtvencimentoAnterior;
				}
				item.setDtparcela(formatador.format(dtparcela));
				
				lista.add(item);
			}
		}
		
		verificaDiferencaTotal(vendaorcamento, lista, bean.getParcela(), juros);
		
		bean.setLista(lista);
		bean.setIsValorminimo(isValorminimo);
		
		return bean;
	}
	
	/**
	 * M�todo para verificar e corrigir diferen�a no total de pagamentos na venda
	 *
	 * @see
	 *
	 * @param venda
	 * @param lista
	 * @param qtdParcelas
	 * @param juros
	 * @author Luiz Fernando
	 */
	private void verificaDiferencaTotal(Vendaorcamento vendaorcamento, List<AjaxRealizarVendaPagamentoItemBean> lista, Integer qtdParcelas, Double juros) {
		if(vendaorcamento != null && lista != null && lista.size() > 1){
			try {
				Double valorfinalpedidovenda = vendaorcamento.getValor();
				Money valortotalpagamentos = new Money(0.0);
				Double diferenca = 0.0;
				
				for(AjaxRealizarVendaPagamentoItemBean item : lista){
					valortotalpagamentos = valortotalpagamentos.add(new Money(item.getValor().replace(".", "").replace(",", ".")));
				}
				
				if(valorfinalpedidovenda != null && valortotalpagamentos != null && 
						valorfinalpedidovenda > valortotalpagamentos.getValue().doubleValue()){				
					if(valorfinalpedidovenda % qtdParcelas != 0){
						diferenca = valorfinalpedidovenda - valortotalpagamentos.getValue().doubleValue();
						
						AjaxRealizarVendaPagamentoItemBean item = lista.get(lista.size()-1);					
						item.setValor(new Money((new Double(item.getValor().replace(".", "").replace(",", ".")) + diferenca)).toString());
					}
				}else if((juros == null || juros == 0) && valortotalpagamentos.getValue().doubleValue() > valorfinalpedidovenda) {
					diferenca = valortotalpagamentos.getValue().doubleValue() - valorfinalpedidovenda;
					
					AjaxRealizarVendaPagamentoItemBean item = lista.get(lista.size()-1);					
					item.setValor(new Money((new Double(item.getValor().replace(",", ".")) - diferenca)).toString());
				}
			} catch (Exception e) {}		
		}
		
	}
	
	/**
	 * Recalcula o valor da parcela caso haja juros
	 * 
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	private Money calculaJuros(Money totalvenda, Integer parcela, Double juros){
		Double total = totalvenda.getValue().doubleValue();
		
		juros = juros/100.00; 
		Double valor = total*juros*Math.pow((1+juros),parcela)/(Math.pow((1+juros),parcela)-1);
		
		Money resultado = new Money(valor);
		return resultado;
	}
	
	public Date buscaPrimeiroDataEntrega(List<Vendaorcamentomaterial> lista) {
		Date dtprimeiraEntrega = null;
		if(lista != null && !lista.isEmpty()){
			for(Vendaorcamentomaterial vendaorcamentomaterial : lista){
				if(vendaorcamentomaterial.getPrazoentrega() != null){
					if(dtprimeiraEntrega == null){
						dtprimeiraEntrega = new Date(vendaorcamentomaterial.getPrazoentrega().getTime());
					}else {
						if(SinedDateUtils.beforeIgnoreHour(new Date(vendaorcamentomaterial.getPrazoentrega().getTime()), dtprimeiraEntrega)){
							dtprimeiraEntrega = new Date(vendaorcamentomaterial.getPrazoentrega().getTime());
						}
					}
				}
			}
		}
		return dtprimeiraEntrega;
	}
	
	public void calcularParcelasForSalvar(Vendaorcamento bean) {
		try {
			if(bean != null && bean.getListavendaorcamentoformapagamento() != null && !bean.getListavendaorcamentoformapagamento().isEmpty()){
				boolean consideraripiconta = false;
				Pedidovendatipo pedidovendatipo = bean.getPedidovendatipo();
				if(pedidovendatipo != null){
					pedidovendatipo = pedidovendatipoService.load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, pedidovendatipo.comodato, pedidovendatipo.consideraripiconta");
					consideraripiconta = pedidovendatipo.getConsideraripiconta() != null && pedidovendatipo.getConsideraripiconta(); 
				}
				for(Vendaorcamentoformapagamento vendaorcamentoformapagamento : bean.getListavendaorcamentoformapagamento()){
					if(vendaorcamentoformapagamento.getPrazopagamento() != null){
						Vendaorcamento vo = new Vendaorcamento();
						vo.setPrazopagamento(vendaorcamentoformapagamento.getPrazopagamento());
						if(consideraripiconta){
							vo.setValor(bean.getTotalvendaMaisImpostos().getValue().doubleValue());
						}else {
							vo.setValor(bean.getTotalvenda().getValue().doubleValue());
						}
						vo.setDtorcamento(bean.getDtorcamento());
						AjaxRealizarVendaPagamentoBean pagamentoBean = this.pagamento(vo, null);
						if(pagamentoBean != null){
							if(pagamentoBean.getLista() != null && !pagamentoBean.getLista().isEmpty()) {
								if(pagamentoBean.getLista().get(0).getValor() != null){
									vendaorcamentoformapagamento.setValorparcela(new Money(new Double(pagamentoBean.getLista().get(0).getValor().replace(".", "").replace(",", "."))));
								}
								if(pagamentoBean.getLista().get(0).getDtparcela() != null){
									vendaorcamentoformapagamento.setDtparcela(SinedDateUtils.stringToDate(pagamentoBean.getLista().get(0).getDtparcela()));
								}
							}
							
							Money total = new Money();
							for (AjaxRealizarVendaPagamentoItemBean itemBean : pagamentoBean.getLista()){
								if(itemBean.getValor() != null){
									total = total.add(new Money(new Double(itemBean.getValor().replace(".", "").replace(",", "."))));
								}
							}	
							vendaorcamentoformapagamento.setValortotal(total);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#loadForTrocarvendedor(Vendaorcamento vendaorcamento)
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public Vendaorcamento loadForTrocarvendedor(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.loadForTrocarvendedor(vendaorcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#updateColaboradorByVendaorcamento(Vendaorcamento vendaorcamento)
	 *
	 * @param vendaorcamento
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void updateColaboradorByVendaorcamento(Vendaorcamento vendaorcamento) {
		vendaorcamentoDAO.updateColaboradorByVendaorcamento(vendaorcamento);		
	}
	
	/**
	 * M�todo que processa a troca de respons�vel do or�amento
	 *
	 * @param vendaorcamentoWithColaboradorAtual
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void processaTrocaVendedor(Vendaorcamento vendaorcamentoWithColaboradorAtual) {
		if(vendaorcamentoWithColaboradorAtual != null && vendaorcamentoWithColaboradorAtual.getCdvendaorcamento() != null && vendaorcamentoWithColaboradorAtual.getColaborador() != null && 
				vendaorcamentoWithColaboradorAtual.getColaborador().getCdpessoa() != null){
			
			Vendaorcamento vWithColaboradorAnterior = loadForTrocarvendedor(vendaorcamentoWithColaboradorAtual);
			boolean isColaboradorDiferente = false;
			if(vWithColaboradorAnterior != null && vWithColaboradorAnterior.getColaborador() != null && 
					vWithColaboradorAnterior.getColaborador().getCdpessoa() != null &&
					!vWithColaboradorAnterior.getColaborador().equals(vendaorcamentoWithColaboradorAtual.getColaborador())){
				isColaboradorDiferente = true;
			}
			
			if(isColaboradorDiferente){
				updateColaboradorByVendaorcamento(vendaorcamentoWithColaboradorAtual);
				
				List<Venda> listaVenda = vendaService.findByOrcamento(vendaorcamentoWithColaboradorAtual);
				if(listaVenda != null && !listaVenda.isEmpty()){
					for(Venda venda : listaVenda){
						if(venda.getCdvenda() != null){
							venda.setColaborador(vendaorcamentoWithColaboradorAtual.getColaborador());
							vendaService.processaTrocaVendedor(venda);
						}
					}
				}
				
				List<Pedidovenda> listaPedidovenda = pedidovendaService.findByOrcamento(vendaorcamentoWithColaboradorAtual);
				if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
					for(Pedidovenda pedidovenda : listaPedidovenda){
						if(pedidovenda.getCdpedidovenda() != null){
							pedidovenda.setColaborador(vendaorcamentoWithColaboradorAtual.getColaborador());
							pedidovendaService.processaTrocaVendedor(pedidovenda);
						}
					}
				}
			}
		}		
	}
	
	@Override
	public void saveOrUpdate(final Vendaorcamento bean) {

		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				VendaorcamentoService.super.saveOrUpdateNoUseTransaction(bean);

				if (bean.getListavendaorcamentomaterial() != null){
					for (Vendaorcamentomaterial item : bean.getListavendaorcamentomaterial()){
						if (item.getListaVendaorcamentomaterialseparacao() != null){
							Iterator<Vendaorcamentomaterialseparacao> iterator = item.getListaVendaorcamentomaterialseparacao().iterator();
							while (iterator.hasNext()){
								Vendaorcamentomaterialseparacao itemSeparacao = iterator.next();
								if (itemSeparacao.getQuantidade() != null && itemSeparacao.getQuantidade() > 0){
									itemSeparacao.setVendaorcamentomaterial(item);
									vendaorcamentomaterialseparacaoService.saveOrUpdateNoUseTransaction(itemSeparacao);
								}else if (itemSeparacao.getCdvendaorcamentomaterialseparacao() != null){
									vendaorcamentomaterialseparacaoService.delete(itemSeparacao);
								}
							}
						}
					}
				}
				
				return status;
			}
		});
	}
	

	/**
	* M�todo com refer�ncia no DAO
	* 
	* @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#hasClienteComRestricoesEmAberto(Integer cdvendaorcamento)
	*
	* @param whereIn
	* @return
	* @author Lucas Costa
	*/
	public Boolean hasClienteComRestricoesEmAberto(Integer cdvendaorcamento) {
		return vendaorcamentoDAO.hasClienteComRestricoesEmAberto(cdvendaorcamento);
	}
	
	/**
	* M�todo que muda a situa��o do Or�amento para aprovado e grava no historico como Aprova��o para Contrato 
	* 
	*
	* @param vendaorcamento
	* @author Lucas Costa
	*/
	public void autorizarComoContrato (Vendaorcamento vendaorcamento, Contrato contrato){
		this.updateSituacaoVendaorcamento(vendaorcamento.getCdvendaorcamento().toString(), Vendaorcamentosituacao.AUTORIZADO);
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		vendaorcamentohistorico.setUsuarioaltera((Usuario)NeoWeb.getUser());
		vendaorcamentohistorico.setAcao("Autorizado");
		vendaorcamentohistorico.setObservacao("Aprova��o para Contrato <a href=\"javascript:visualizarContrato(" + contrato.getCdcontrato() + ")\">" + contrato.getCdcontrato() + "</a>");
		vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
	}
	
	protected boolean haveOrcamentoByIdentificadorEmpresa(String identificador, Empresa empresa, boolean verificarCancelado) {
		return vendaorcamentoDAO.haveOrcamentoByIdentificadorEmpresa(identificador, empresa, verificarCancelado);
	}
	
	/**
	 * 
	 * Gera um or�amento
	 * 
	 * @param CriarPedidovendaBean
	 * @since 14/08/2014
	 * @author Rafael Patr�cio
	 */
	public Integer criarOrcamentoWebService(CriarPedidovendaBean bean){
		Integer cdcolaborador = ParametrogeralService.getInstance().getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_COLABORADOR);
		Integer cdprojeto = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PROJETO);
		Integer cdlocalarmazenagem = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_LOCALARMAZENAGEM);
		Integer cdpedidovendatipo = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PEDIDOVENDATIPO);
		
		Pedidovendatipo pedidovendatipo = cdpedidovendatipo != null ? pedidovendatipoService.load(new Pedidovendatipo(cdpedidovendatipo)) : null;
		
		Colaborador colaborador = cdcolaborador != null ? new Colaborador(cdcolaborador) : null;
		Projeto projeto = cdprojeto != null ? new Projeto(cdprojeto) : null;
		Localarmazenagem localarmazenagem = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		
		if(bean.getColaborador_matricula() != null){
			Colaborador colaborador_aux = colaboradorService.findByMatricula(bean.getColaborador_matricula());
			if(colaborador_aux != null){
				colaborador = colaborador_aux;
			}
		}
		
		ResponsavelFrete frete = null;
		if(bean.getFrete_tipo() != null && 
				(bean.getFrete_tipo().equals(1) ||
				bean.getFrete_tipo().equals(2) ||
				bean.getFrete_tipo().equals(3) ||
				bean.getFrete_tipo().equals(4))){
			frete = new ResponsavelFrete(bean.getFrete_tipo());
		} else {
			frete = ResponsavelFrete.SEM_FRETE;
		}
		
		Empresa empresa = null;
		if(bean.getEmpresa_cnpj() != null){
			try {
				empresa = empresaService.carregaEmpresaByCnpj(new Cnpj(bean.getEmpresa_cnpj()));
			} catch (Exception e) {
				empresa = empresaService.loadPrincipal();
			}
		}
		
		if(empresa == null){
			empresa = empresaService.loadPrincipal();
		}
		
		Cliente cliente = null;
		if(bean.getCliente_cpfcnpj() != null && !bean.getCliente_cpfcnpj().trim().equals("")){
			try{
				String cnpjcpf = StringUtils.soNumero(bean.getCliente_cpfcnpj());
				
				if(cnpjcpf.length() == 11){
					cliente = clienteService.findByCpf(new Cpf(cnpjcpf));
				} else if(cnpjcpf.length() == 14){
					cliente = clienteService.findByCnpj(new Cnpj(cnpjcpf));
				}
			} catch (Exception e) {
				throw new SinedException("Erro na busca do cliente na base: " + e.getMessage());
			}
		}
		if(cliente == null){
			throw new SinedException("Cliente n�o encontrado na base.");
		}
		clientevendedorService.criarRelacaoClientevendedor(cliente, bean);
		
		List<Vendaorcamentomaterial> listaVendaorcamentomaterial = new ArrayList<Vendaorcamentomaterial>();
		List<CriarPedidovendaItemBean> pedidovendaItem = bean.getPedidovenda_item();
		
		Configuracaoecom configuracaoecom = configuracaoecomService.findPrincipalByEmpresa(empresa);
		
		for (CriarPedidovendaItemBean it : pedidovendaItem) {
			Material material = null;
			
			String identificadorMaterial = it.getMaterial_identificador();

			if (identificadorMaterial != null && !identificadorMaterial.trim().isEmpty()){
				if(configuracaoecom != null){
					Ecommaterialidentificador ecommaterialidentificador = ecommaterialidentificadorService.findByIdentificador(identificadorMaterial, configuracaoecom);
					if(ecommaterialidentificador != null){
						material = ecommaterialidentificador.getMaterial();
					}
				}
				
				if(material == null){
					material = materialService.findByIdentificacao(identificadorMaterial);
				}
				
			} else if (it.getMaterial_cdmaterial() != null && !it.getMaterial_cdmaterial().trim().isEmpty()) {
				material = materialService.loadByCodigoMaterial(Integer.valueOf(it.getMaterial_cdmaterial()));
				if(material == null){
					throw new SinedException("Material com c�digo '" + it.getMaterial_cdmaterial() + "' n�o encontrado na base.");
				}
			}
			
			if(material == null){
				throw new SinedException("Material com identificador '" + identificadorMaterial + "' n�o encontrado na base.");
			}

			Vendaorcamentomaterial vendaorcamentomaterial = new Vendaorcamentomaterial();
			vendaorcamentomaterial.setMaterial(material);
			vendaorcamentomaterial.setUnidademedida(material.getUnidademedida());
			vendaorcamentomaterial.setQuantidade(it.getQuantidade());
			
			if (it.getValor_unitario() != null)
				vendaorcamentomaterial.setPreco(it.getValor_unitario());
			else
				vendaorcamentomaterial.setPreco(material.getValorvenda());
			
			vendaorcamentomaterial.setDesconto(it.getDesconto());
			if(it.getObservacao() != null && !"".equals(it.getObservacao())){
				if(it.getObservacao().length() > 100){
					try {
						vendaorcamentomaterial.setObservacao(it.getObservacao().substring(0, 99));
					} catch (Exception e) {e.printStackTrace();} 
				}else {
					vendaorcamentomaterial.setObservacao(it.getObservacao());
				}
			}
			
            vendaorcamentomaterial.setTotal(vendaService.getValortotal(vendaorcamentomaterial.getPreco(), 
                    vendaorcamentomaterial.getQuantidade(),
                    vendaorcamentomaterial.getDesconto(), 
                    vendaorcamentomaterial.getMultiplicador(),
                    vendaorcamentomaterial.getOutrasdespesas(), vendaorcamentomaterial.getValorSeguro()));
			
			addItemVendaorcamentomaterialWebservice(listaVendaorcamentomaterial, vendaorcamentomaterial);
		}
		
		Vendaorcamento vendaorcamento = new Vendaorcamento();
		vendaorcamento.setCliente(cliente);
		vendaorcamento.setListavendaorcamentomaterial(listaVendaorcamentomaterial);
		vendaorcamento.setVendaorcamentosituacao(Vendaorcamentosituacao.EM_ABERTO);
		vendaorcamento.setDtorcamento(SinedDateUtils.currentDate());
		vendaorcamento.setEmpresa(empresa);
		vendaorcamento.setPedidovendatipo(pedidovendatipo);
		vendaorcamento.setDesconto(bean.getPedidovenda_desconto());
		vendaorcamento.setIdentificador(bean.getPedidovenda_identificador());
		vendaorcamento.setEndereco(bean.getCliente_cdendereco() != null ? new Endereco(bean.getCliente_cdendereco()) : cliente.getEndereco());
		vendaorcamento.setColaborador(colaborador);
		vendaorcamento.setObservacao(bean.getPedidovenda_observacao());
		vendaorcamento.setObservacaointerna(bean.getPedidovenda_observacaointerna());
		vendaorcamento.setFrete(frete);
		vendaorcamento.setValorfrete(bean.getFrete_valor());
		vendaorcamento.setProjeto(projeto);
		vendaorcamento.setLocalarmazenagem(localarmazenagem);
		
		
		this.saveOrUpdate(vendaorcamento);
		
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setAcao("Cria��o or�amento");
		
		vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
		
		if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.TABELA_VENDA_CLIENTE)) && vendaorcamento.getCliente() != null){
			List<Vendaorcamentomaterial> listaVendamaterialForCriarTabela = this.getListaVendaorcamentomaterialForCriarTabela(vendaorcamento);
			try {
				vendaorcamento.setListavendaorcamentomaterial(listaVendamaterialForCriarTabela);
				List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findByCliente(vendaorcamento.getCliente(),SinedDateUtils.castUtilDateForSqlDate(vendaorcamento.getDtorcamento()),null,null, materialtabelaprecoService.getTipocalculoByParametro());
				if(listaMaterialtabelapreco!=null && !listaMaterialtabelapreco.isEmpty())
					materialtabelaprecoService.atualizaTabelaprecoByCliente(vendaorcamento, listaMaterialtabelapreco);
				else { 
					materialtabelaprecoService.criaTabelaprecoByCliente(vendaorcamento, vendaorcamento.getCliente());
				}
			}  catch (DataIntegrityViolationException e) {
				e.printStackTrace();
				StringBuilder dataIntegrityViolationException = new StringBuilder();
				dataIntegrityViolationException.append("N�o foi poss�vel criar Tabela de pre�o para o Cliente").append("\n");
				if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_1")) {
					dataIntegrityViolationException.append("Existe uma tabela cadastrada para o Cliente.").append("\n");
				} else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
					dataIntegrityViolationException.append("Existe uma tabela cadastrada para o Tipo de Pedido de venda.").append("\n");
				}else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
					dataIntegrityViolationException.append("Existe uma tabela cadastrada para o Prazo de pagmento.").append("\n");
				} 
				throw new SinedException(dataIntegrityViolationException.toString());
			} catch (Exception e) {
				throw new SinedException("N�o foi poss�vel criar Tabela de pre�o para o Cliente");
			}
		}
		return vendaorcamento.getCdvendaorcamento();
	}
	
	/**
	* M�todo que adiciona os itens do kit na lista de or�amento, 
	* caso o material tenha a marca��o de exibir os itens, ou apenas insere o item na lista
	*
	* @param listaVendamaterial
	* @param vendamaterial
	* @since 24/04/2015
	* @author Luiz Fernando
	*/
	private void addItemVendaorcamentomaterialWebservice(List<Vendaorcamentomaterial> listaVendamaterial, Vendaorcamentomaterial vendamaterial) {
		if(vendamaterial != null && vendamaterial.getMaterial() != null && 
				vendamaterial.getQuantidade() != null && 
				vendamaterial.getQuantidade() > 0){
			if(listaVendamaterial == null)
				listaVendamaterial = new ArrayList<Vendaorcamentomaterial>();
			
			boolean adicionarItem = true;
			Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendamaterial.getMaterial().getCdmaterial());
			if(material != null && material.getVendapromocional() != null && material.getVendapromocional() && 
					material.getExibiritenskitvenda() != null && material.getExibiritenskitvenda()){
				if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
					Money totalKit = new Money();
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getValorvenda() != null && materialrelacionado.getQuantidade() != null && 
								materialrelacionado.getQuantidade() > 0){	
							totalKit = totalKit.add(new Money(materialrelacionado.getValorvenda().getValue().doubleValue() * materialrelacionado.getQuantidade() * vendamaterial.getQuantidade()));
						}
					}
					
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null && 
								materialrelacionado.getQuantidade() > 0){	
							Vendaorcamentomaterial vm = new Vendaorcamentomaterial();
							vm.setMaterial(materialrelacionado.getMaterialpromocao());
							vm.setUnidademedida(materialrelacionado.getMaterialpromocao().getUnidademedida());
							vm.setQuantidade(materialrelacionado.getQuantidade() * vendamaterial.getQuantidade());
							if(materialrelacionado.getValorvenda() != null){
								vm.setPreco(materialrelacionado.getValorvenda().getValue().doubleValue());
								if(totalKit != null && 
										totalKit.getValue().doubleValue() > 0 && 
										vendamaterial.getDesconto() != null && 
										vendamaterial.getDesconto().getValue().doubleValue() > 0){
									vm.setDesconto(new Money(vm.getPreco() * vm.getQuantidade() * 100d / totalKit.getValue().doubleValue() * vendamaterial.getDesconto().getValue().doubleValue() / 100d));
								}
							}else {
								vm.setPreco(0d);
								vm.setDesconto(new Money());
							}
							vm.setPrazoentrega(vendamaterial.getPrazoentrega());
							vm.setObservacao(vendamaterial.getObservacao());
							
                            vm.setTotal(vendaService.getValortotal(vm.getPreco(), 
                                    vm.getQuantidade(),
                                    vm.getDesconto(), 
                                    vm.getMultiplicador(), vm.getOutrasdespesas(), vm.getValorSeguro()));
							
							listaVendamaterial.add(vm);
							adicionarItem = false;
						}
					}
				}
			}
			if(adicionarItem){
				listaVendamaterial.add(vendamaterial);
			}
		}
	}
	
	public Boolean isIdentificadorCadastrado(Vendaorcamento vendaorcamento) {
		return vendaorcamentoDAO.isIdentificadorCadastrado(vendaorcamento);
	}
	
	public boolean validaKitFlexivel(WebRequestContext request,	Vendaorcamento pedidovenda) {
		boolean existeItemKitFlexivel = false;
		boolean existeKitFlexivel = false;
		if(pedidovenda != null && SinedUtil.isListNotEmpty(pedidovenda.getListavendaorcamentomaterialmestre())){
			for(Vendaorcamentomaterialmestre pvmm : pedidovenda.getListavendaorcamentomaterialmestre()){
				if(pvmm.getMaterial() != null && pvmm.getMaterial().getKitflexivel() != null && pvmm.getMaterial().getKitflexivel() &&
						org.apache.commons.lang.StringUtils.isNotEmpty(pvmm.getIdentificadorinterno())){
					existeKitFlexivel = true; 
					existeItemKitFlexivel = false;
					if(SinedUtil.isListNotEmpty(pedidovenda.getListavendaorcamentomaterial())){
						for(Vendaorcamentomaterial pvm : pedidovenda.getListavendaorcamentomaterial()){
							if(org.apache.commons.lang.StringUtils.isNotEmpty(pvm.getIdentificadorinterno()) &&
									pvm.getIdentificadorinterno().equals(pvmm.getIdentificadorinterno())){
								existeItemKitFlexivel = true;
								break;
							}
						}
					}
					if(existeKitFlexivel && !existeItemKitFlexivel){
						request.addError("Existe material de Kit Flex�vel sem itens inclusos para sua composi��o");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	* M�todo que calcula o valor aproximado de imposto do or�amento
	*
	* @param venda
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public void calcularValorAproximadoImposto(Vendaorcamento venda) {
		Money valoraproximadoimposto = null;
		
		if(SinedUtil.isListNotEmpty(venda.getListavendaorcamentomaterial())){
			List<Material> listaMaterial = materialService.findCalcularImposto(CollectionsUtil.listAndConcatenate(venda.getListavendaorcamentomaterial(), "material.cdmaterial", ","));
			
			for(Vendaorcamentomaterial vendamaterial : venda.getListavendaorcamentomaterial()){
				vendamaterial.setValorimposto(null);
				Material material = materialService.getMaterialByLista(vendamaterial.getMaterial(), listaMaterial);
				if(material != null){
					if(venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0){
						Money descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(venda.getListavendaorcamentomaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
						if(descontoproporcional != null){
							vendamaterial.setDesconto(descontoproporcional);
						}
					}
					
					Origemproduto origemproduto = material.getOrigemproduto();
					boolean servico = material.getServico() != null && material.getServico();
					
					Double percentualimposto = notafiscalprodutoService.getPercentualImposto(venda.getEmpresa(), 
						material.getPercentualimpostoecf(), 
						origemproduto,
						material.getNcmcompleto(),
						material.getExtipi(),
						material.getCodigonbs(),
						material.getCodlistaservico(),
						!servico,
						null);
					
					if(percentualimposto != null){
						Double valorImpostoItem = 0d;
						if(vendamaterial.getQuantidade() != null && vendamaterial.getPreco() != null){
							valorImpostoItem += vendamaterial.getQuantidade() * vendamaterial.getPreco();
						}
						if(vendamaterial.getDesconto() != null){
							valorImpostoItem -= vendamaterial.getDesconto().getValue().doubleValue();
						}
						
						if(valoraproximadoimposto == null) valoraproximadoimposto = new Money();
						vendamaterial.setValorimposto(new Money(valorImpostoItem * percentualimposto / 100));
						valoraproximadoimposto = valoraproximadoimposto.add(vendamaterial.getValorimposto()) ;
					}
				}
			}
		}
		venda.setValoraproximadoimposto(valoraproximadoimposto);
	}
	
	/**
	* M�todo que cria um or�amento de acordo com a oportunidade
	*
	* @param whereInOportunidade
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public Vendaorcamento criaOrcamentoByOportunidade(String whereInOportunidade, Integer cdvendaorcamento) {
		Vendaorcamento vendaorcamento = new Vendaorcamento();
		List<Oportunidade> lista = oportunidadeService.findForVenda(whereInOportunidade);
		if (SinedUtil.isListNotEmpty(lista)) {
			if(lista.size() > 1){
				throw new SinedException("N�o � permitido gerar or�amento de mais de uma oportunidade.");
			}
			
			Vendaorcamento ultimoVendaorcamento = findUltimoOrcamentoByOportunidade(lista.get(0), Vendaorcamentosituacao.EM_ABERTO);
			Vendaorcamento vendaorcamentoCopiar = cdvendaorcamento != null ? new Vendaorcamento(cdvendaorcamento) : null;
			Integer cdultimovendaorcamento = null;
			if(ultimoVendaorcamento == null){
				ultimoVendaorcamento = findUltimoOrcamentoByOportunidade(lista.get(0), Vendaorcamentosituacao.AUTORIZADO);
			}else {
				cdultimovendaorcamento = ultimoVendaorcamento.getCdvendaorcamento();
			}
			
			if(vendaorcamentoCopiar != null || ultimoVendaorcamento != null){
				vendaorcamento = criarCopia(vendaorcamentoCopiar != null ? vendaorcamentoCopiar : ultimoVendaorcamento);
				vendaorcamento.setCdultimovendaorcamento(cdultimovendaorcamento);
				vendaorcamento.setOportunidade(lista.get(0));
			}else {
				Cliente cliente = null;
				Contacrm contacrm = null;
				Empresa empresa = null;
				Contato contato = null;
				List<Vendaorcamentomaterial> listavendaorcamentomaterial = new ArrayList<Vendaorcamentomaterial>();
				Vendaorcamentomaterial vendaorcamentomaterial;
				for (Oportunidade oportunidade : lista) {
					if (cliente == null) cliente = oportunidade.getCliente();
					if (contacrm == null) contacrm = oportunidade.getContacrm();
					if (empresa == null) empresa = oportunidade.getEmpresa();
					if (contato == null) contato = oportunidade.getContato();
	
					vendaorcamento.setCliente(cliente);
					vendaorcamento.setContacrm(contacrm);
					vendaorcamento.setEmpresa(empresa);
	
					if (oportunidade.getListaOportunidadematerial() != null && !oportunidade.getListaOportunidadematerial().isEmpty()) {
						for (Oportunidadematerial oportunidadematerial : oportunidade.getListaOportunidadematerial()) {
							if (oportunidadematerial.getMaterial() != null) {
								vendaorcamentomaterial = new Vendaorcamentomaterial();
								vendaorcamentomaterial.setMaterial(oportunidadematerial.getMaterial());
								vendaorcamentomaterial.setUnidademedida(oportunidadematerial.getUnidademedida());
								vendaorcamentomaterial.setQuantidade(oportunidadematerial.getQuantidade() != null ? oportunidadematerial.getQuantidade() : 1d);
								vendaorcamentomaterial.setObservacao(oportunidadematerial.getObservacao());
								
								if (oportunidadematerial.getValorunitario() != null) {
									vendaorcamentomaterial.setPreco(oportunidadematerial.getValorunitario());
								} else if (oportunidadematerial.getMaterial().getValorvenda() != null) {
									vendaorcamentomaterial.setPreco(oportunidadematerial.getMaterial().getValorvenda());
								} else {
									vendaorcamentomaterial.setPreco(0d);
								}
								vendaorcamentomaterial.setMultiplicador(1d);
								vendaorcamentomaterial.setQtdereferencia(1d);
								vendaorcamentomaterial.setValorcustomaterial(vendaorcamentomaterial.getMaterial().getValorcusto());
	
								Materialtabelaprecoitem materialtabelaprecoitem = null;
	
								Double minimo = 0.0;
								Double maximo = 0.0;
								Double valorMaximoTabela = null;
								Double valorMinimoTabela = null;
	
								if (!vendaorcamentomaterial.getMaterial().getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) && vendaorcamentomaterial.getFatorconversao() != null && vendaorcamentomaterial.getFatorconversao() > 0) {
									if (vendaorcamentomaterial.getMaterial().getValorvendaminimo() != null)
										minimo = vendaorcamentomaterial.getMaterial().getValorvendaminimo() / vendaorcamentomaterial.getFatorconversaoQtdereferencia();
									if (vendaorcamentomaterial.getMaterial().getValorvendamaximo() != null)
										maximo = vendaorcamentomaterial.getMaterial().getValorvendamaximo() / vendaorcamentomaterial.getFatorconversaoQtdereferencia();
								} else {
									minimo = vendaorcamentomaterial.getMaterial().getValorvendaminimo();
									maximo = vendaorcamentomaterial.getMaterial().getValorvendamaximo();
								}
	
								materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(vendaorcamentomaterial.getMaterial(), 
																																	  cliente, 
																																	  null, 
																																	  null, 
																																	  vendaorcamento.getEmpresa(),
																																	  vendaorcamentomaterial.getUnidademedida());
								if (materialtabelaprecoitem != null) {
									if (vendaorcamentomaterial.getFatorconversao() != null && vendaorcamentomaterial.getFatorconversao() > 0) {
										if (materialtabelaprecoitem.getValorvendaminimo() != null)
											valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo() / vendaorcamentomaterial.getFatorconversaoQtdereferencia();
										if (materialtabelaprecoitem.getValorvendamaximo() != null)
											valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo() / vendaorcamentomaterial.getFatorconversaoQtdereferencia();
									} else {
										valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
										valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
									}
								}
	
								if (valorMinimoTabela != null) {
									minimo = valorMinimoTabela;
								}
								if (valorMaximoTabela != null) {
									maximo = valorMaximoTabela;
								}
								vendaorcamentomaterial.getMaterial().setValorvendaminimo(minimo);
								vendaorcamentomaterial.getMaterial().setValorvendamaximo(maximo);
	
								if (parametrogeralService.getBoolean(Parametrogeral.VENDASALDOPORMINMAX)) {
									this.calculaSaldoMaterial(vendaorcamentomaterial, 
											vendaorcamentomaterial.getPreco(), 
											vendaorcamentomaterial.getMaterial().getValorvendaminimo(),
											vendaorcamentomaterial.getMaterial().getValorvendamaximo(),
											vendaorcamentomaterial.getQuantidade(),
											vendaorcamentomaterial.getDesconto(),
											parametrogeralService.getBoolean(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
								} else {
									this.calculaSaldoMaterialPorValorvenda(
											vendaorcamentomaterial,
											vendaorcamentomaterial.getPreco(), 
											vendaorcamentomaterial.getMaterial().getValorvenda(),
											vendaorcamentomaterial.getQuantidade(),
											vendaorcamentomaterial.getDesconto(),
											parametrogeralService.getBoolean(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
								}
	
								listavendaorcamentomaterial.add(vendaorcamentomaterial);
							}
	
						}
					}
				}
	
				vendaorcamento.setListavendaorcamentomaterial(listavendaorcamentomaterial);
				vendaorcamento.setOportunidade(lista.get(0));
				if(contacrm != null){
					vendaorcamento.setTipopessoa(TipoPessoaVendaEnum.CONTA);
					vendaorcamento.setContacrm(contacrm);
				}else {
					vendaorcamento.setTipopessoa(TipoPessoaVendaEnum.CLIENTE);
					vendaorcamento.setCliente(cliente);
				}
				vendaorcamento.setContato(contato);
				vendaorcamento.setEmpresa(empresa != null ? empresa : empresaService.loadPrincipal());
				vendaorcamento.setColaborador(SinedUtil.getUsuarioComoColaborador());
				
				setOrdemItem(vendaorcamento);
			}
		}
		
		return vendaorcamento;
	}
	
	public void setOrdemItem(Vendaorcamento vendaorcamento) {
		if(vendaorcamento != null && SinedUtil.isListNotEmpty(vendaorcamento.getListavendaorcamentomaterial())){
			int ordem = 1;
			for(Vendaorcamentomaterial item : vendaorcamento.getListavendaorcamentomaterial()){
				item.setOrdem(ordem);
				ordem++;
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendaorcamentoDAO#findUltimoOrcamentoByOportunidade(Oportunidade oportunidade)
	*
	* @param oportunidade
	* @return
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public Vendaorcamento findUltimoOrcamentoByOportunidade(Oportunidade oportunidade, Vendaorcamentosituacao vendaorcamentosituacao) {
		return vendaorcamentoDAO.findUltimoOrcamentoByOportunidade(oportunidade, vendaorcamentosituacao);
	}
	
	public Money getValorDescontoVenda(List<Vendaorcamentomaterial> listavendaorcamentomaterial, Vendaorcamentomaterial vendamaterial, Money descontoTotal, Money valorUsadoValeCompra, boolean calcularDescontoUnitario) {
		Money valorDesconto = new Money(0);
		Money descontoTotalWithValeCompra = new Money();
		if(valorUsadoValeCompra != null && valorUsadoValeCompra.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(valorUsadoValeCompra);
		}
		if(descontoTotal != null && descontoTotal.getValue().doubleValue() > 0){
			descontoTotalWithValeCompra = descontoTotalWithValeCompra.add(descontoTotal);
		}
		
		if (descontoTotalWithValeCompra != null && descontoTotalWithValeCompra.getValue().doubleValue() > 0){
			
			Money totalMaterial = new Money(0);
			if(SinedUtil.isListNotEmpty(listavendaorcamentomaterial)){
				for (Vendaorcamentomaterial vm : listavendaorcamentomaterial) {
					Double qtde = vm.getQuantidade() == null ? 1 : vm.getQuantidade();
					Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
					totalMaterial = totalMaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
					if(vm.getDesconto() != null){
						totalMaterial = totalMaterial.subtract(vm.getDesconto());
					}
				}
			}
			
			Double multiplicador = vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() != 0 ? vendamaterial.getMultiplicador() : 1d;
			Double qtde = vendamaterial.getQuantidade() == null ? 1 : vendamaterial.getQuantidade();
			Money valorMaterial = new Money(vendamaterial.getPreco().doubleValue() *  qtde * multiplicador);
			
			if(vendamaterial.getDesconto() != null){
				valorMaterial = valorMaterial.subtract(vendamaterial.getDesconto());
			}
			
			//Calcula o valor total de desconto proporcional para o produto
			if (valorMaterial != null && valorMaterial.getValue().doubleValue() > 0 &&
				totalMaterial != null && totalMaterial.getValue().doubleValue() > 0){
				valorDesconto = descontoTotalWithValeCompra.multiply(valorMaterial.divide(totalMaterial));
			}		
			
			//Calcula o valor do desconto unit�rio
			if (calcularDescontoUnitario){
				valorDesconto = valorDesconto.divide(new Money(qtde*multiplicador));
			}
		}		
		
		return valorDesconto;
	}
	
	public VendaRTF makeComprovanteOrcamento(Vendaorcamento beanOrcamento){
		Vendaorcamento vendaorcamento = this.loadVendaorcamento(beanOrcamento.getCdvendaorcamento());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		VendaRTF bean = new VendaRTF();
		
		//Populando dados do cabe�alho do or�amento
		bean.setTipopedidovenda(vendaorcamento.getPedidovendatipo() != null? vendaorcamento.getPedidovendatipo().getDescricao(): "");
		bean.setResponsavel(vendaorcamento.getColaborador() != null? vendaorcamento.getColaborador().getNome(): "");
		bean.setDtvenda(sdf.format(vendaorcamento.getDtorcamento()));
		bean.setObservacoes(vendaorcamento.getObservacao());
		bean.setObservacoesinternas(vendaorcamento.getObservacaointerna());
		bean.setCodigo(vendaorcamento.getCdvendaorcamento().toString());
		
		//Populando dados da empresa
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getCdpessoa() != null){
			vendaorcamento.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(vendaorcamento.getEmpresa())));
			vendaorcamento.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.findByPessoa(vendaorcamento.getEmpresa())));
		}
		bean.setEmpresa(new VendaEmpresaRTF(vendaorcamento.getEmpresa()));
		
		Cliente cliente = vendaorcamento.getCliente() != null ? clienteService.carregarDadosCliente(vendaorcamento.getCliente()) : null;
		if(cliente != null){
			cliente.setNome(pessoaService.carregaPessoa(new Pessoa(cliente.getCdpessoa())).getNome());
			bean.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(cliente));
			cliente.setListaContato(pessoaContatoService.findByPessoa(cliente));
		}
		
		//Populando dados de conta crm
		bean.setConta(new VendaContaRTF(vendaorcamento.getContacrm()));
		
		//Populando dados do cliente
		bean.setCliente(new VendaClienteRTF(cliente));
		
		bean.setOutros(new VendaOutrosRTF(vendaorcamento));
		
		List<VendaMaterialKitRTF> listaProdutoskit = new ArrayList<VendaMaterialKitRTF>();
		List<VendaMaterialKitflexivelRTF> listaProdutoskitflexivel = new ArrayList<VendaMaterialKitflexivelRTF>();
		List<Vendaorcamentoformapagamento> listaVendaorcamentoFormaPagamento = vendaorcamentoformapagamentoService.findByVendaorcamento(vendaorcamento);
		List<Vendaorcamentomaterial> listaprodutos = vendaorcamentomaterialService.findByMaterial(vendaorcamento, "vendaorcamentomaterial.ordem, vendaorcamentomaterial.cdvendaorcamentomaterial, listaCaracteristica.cdmaterialcaracteristica");
		List<Vendaorcamentomaterialmestre> listaVendaorcamentomaterialmestre = vendaorcamentomaterialmestreService.findByVendaOrcamento(vendaorcamento);
		vendaorcamento.setListavendaorcamentomaterial(listaprodutos);
		
		vendaorcamento.setListaOrcamentovalorcampoextra(orcamentovalorcampoextraService.findByVendaorcamento(vendaorcamento));
		
		//Populando dados de pagamento
		for(Vendaorcamentoformapagamento formapagamento: listaVendaorcamentoFormaPagamento){
			bean.getListaPagamento().add(new VendaPagamentoRTF(formapagamento));
		}
		/*BigDecimal pesoliquidototal = new BigDecimal(0);
		BigDecimal pesobrutototal = new BigDecimal(0);
		Double qtdetotalmetrocubico = 0.0;
		Double qtdemetrocubico = 0.0;
		Double fracao = 1.0;*/
		for (Vendaorcamentomaterial vendaorcamentomaterial : listaprodutos) {
            Money valorTotal = vendaService.getValortotal(vendaorcamentomaterial.getPreco(), vendaorcamentomaterial.getQuantidade(), vendaorcamentomaterial.getDesconto(),
                    vendaorcamentomaterial.getMultiplicador(), vendaorcamentomaterial.getOutrasdespesas(), vendaorcamentomaterial.getValorSeguro());
			vendaorcamentomaterial.setTotal(valorTotal);
		}
		Double qtdeProduto = 0.0, qtdeServico = 0.0;
		Money valorTotalProdutos = new Money();
		
		//Populando itens do or�amento
		for(Vendaorcamentomaterial vm : listaprodutos){
			VendaMaterialRTF produto = new VendaMaterialRTF();
			if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
				try {
					File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterial().getArquivo()));
					BufferedImage image = ImageIO.read(input);
					
					File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterial().getArquivo()));
					ImageIO.write(image, "png", output);  
					
					produto.setImagem(new FileInputStream(output));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(vm.getMaterial() != null && vm.getMaterial().getPesobruto() != null){
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				if(vm.getMaterial().getPesobruto() != null){
					Double pesoBruto = vm.getMaterial().getPesobruto();
					if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
							vm.getMaterial().getUnidademedida() != null && 
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
					}
					produto.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)).toString());
					//pesobrutototal =  pesoliquidototal.add(new BigDecimal(pesoBruto * qtde));
				}
			}
			if(vm.getPesoVendaOuMaterial() != null){
				produto.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())).toString());
				//pesoliquidototal = pesoliquidototal.add(new BigDecimal(vm.getQuantidade() * vm.getPesoVendaOuMaterial()));
			}
			produto.setCdmaterial(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
			produto.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
			produto.setIdentificador(vm.getMaterial().getIdentificacao());
			produto.setDescricao(vm.getMaterial().getNome());
			produto.setUnidademedida(vm.getUnidademedida().getNome());
			produto.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
			produto.setQuantidade(vm.getQuantidade());
			if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
					!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
				produto.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
			}
			produto.setPreco(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
			produto.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
			produto.setCdvendamaterial(vm.getCdvendaorcamentomaterial().toString());
			produto.setTotal(vm.getTotal().toString());
			produto.setObservacao(vm.getObservacao());
			if(vm.getLoteestoque() != null){
				produto.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
			}
			produto.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );
			if(vm.getComprimentooriginal() != null){
				produto.setComprimento(vm.getComprimentooriginal().toString());
			}
			if(vm.getAltura() != null){
				produto.setAltura(vm.getAltura().toString());
			}
			if(vm.getLargura() != null){
				produto.setLargura(vm.getLargura().toString());
			}
			produto.setNcm(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null ? vm.getMaterial().getNcmcapitulo().getCodeFormat() : "");
			produto.setNcmcompleto(vm.getMaterial()!=null && vm.getMaterial().getNcmcompleto()!=null ? vm.getMaterial().getNcmcompleto() : "");
			produto.setNcmdescricao(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null && vm.getMaterial().getNcmcapitulo().getDescricao()!=null ? vm.getMaterial().getNcmcapitulo().getDescricao() : "");
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getCdmaterial() != null){
				produto.setCdmaterialmestre(vm.getMaterialmestre().getCdmaterial().toString());
			}
			produto.setIdentificadormaterialmestre(vm.getIdentificadorinterno() != null ? vm.getIdentificadorinterno() : "");
			if(vm.getMaterial()!=null && vm.getMaterial().getLocalizacaoestoque()!=null && vm.getMaterial().getLocalizacaoestoque().getDescricao() !=null){
				produto.setLocalizacaoestoque(vm.getMaterial().getLocalizacaoestoque().getDescricao());
			}
			if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
				produto.setCaracteristica(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
			}
			
			if(vm.getMaterial() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto()){
				qtdeProduto += vm.getQuantidade();
			}
			if(vm.getMaterial() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico()){
				qtdeServico += vm.getQuantidade();
			}
			Money total = new Money(SinedUtil.round(vm.getTotal().getValue(), 2));
			valorTotalProdutos = valorTotalProdutos.add(total);
			
			bean.getListaVendamaterial().add(produto);
			
			if((vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && 
					vm.getMaterialmestre().getVendapromocional()) || 
					(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional())){
				//Populando itens de kit
				addMaterialKitRTF(listaProdutoskit, vm);
			}else if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() != null && 
					vm.getMaterialmestre().getKitflexivel()){
				//Populando itens de kit flex�vel
				addMaterialKitflexivelRTF(listaProdutoskitflexivel, vm, getItemMestre(listaVendaorcamentomaterialmestre, vm));
			}else{
				bean.getListaVendamaterialavulso().add(produto);
			}
		}
		
		bean.setQtdetotalproduto(qtdeProduto);
		bean.setQtdetotalservico(qtdeServico);
		bean.setValortotal(valorTotalProdutos.toString());
		bean.getListaVendamaterialkit().addAll(listaProdutoskit);
		bean.getListaVendamaterialkitflexivel().addAll(listaProdutoskitflexivel);
		
		HashMap<String, String> camposAdicionais = new HashMap<String, String>();
		if (vendaorcamento.getListaOrcamentovalorcampoextra() != null){
			for (Orcamentovalorcampoextra campoextra : vendaorcamento.getListaOrcamentovalorcampoextra()){
				camposAdicionais.put(campoextra.getCampoextrapedidovendatipo().getNome(), campoextra.getValor());
			}
		}
		bean.setCamposadicionais(camposAdicionais);
		
		StringBuilder observacao = new StringBuilder();
		if(vendaorcamento.getObservacao() != null){
			observacao.append(vendaorcamento.getObservacao());
		}
		if(vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getObservacaovenda() != null && !"".equals(vendaorcamento.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(vendaorcamento.getEmpresa().getObservacaovenda());
		}
		bean.setDesconto(vendaorcamento.getDesconto() == null ? new Money(0.0).toString() : vendaorcamento.getDesconto().toString());
		
		Money valorFinal = valorTotalProdutos;
		if (vendaorcamento.getValorfrete() != null)
			valorFinal = valorFinal.add(vendaorcamento.getValorfrete());
		if (vendaorcamento.getDesconto() != null)
			valorFinal = valorFinal.subtract(vendaorcamento.getDesconto());
		if (vendaorcamento.getValorusadovalecompra() != null)
			valorFinal = valorFinal.subtract(vendaorcamento.getValorusadovalecompra());
		vendaorcamento.setValorfinal(valorFinal);
		bean.setValortotalprodutos(vendaorcamento.getValorfinal() == null ? new Money(0.0).toString() : vendaorcamento.getValorfinal().toString());
		return bean;
	}
	
	public Vendaorcamento findForComprovanteRTF(Vendaorcamento vendaorcamento){
		return vendaorcamentoDAO.findForComprovanteRTF(vendaorcamento);
	}
	
	private void addMaterialKitRTF(List<VendaMaterialKitRTF> listaProdutoskit, Vendaorcamentomaterial vm) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(listaProdutoskit != null && vm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && vm.getMaterialmestre().getVendapromocional()){
				boolean adicionar = true;
				for(VendaMaterialKitRTF kit : listaProdutoskit){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterialmestre())){
						adicionar = false;
						VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
						itemKit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
						itemKit.setDescricao(vm.getMaterial().getNome());
						itemKit.setUnidademedida(vm.getUnidademedida().getNome());
						itemKit.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
						itemKit.setPreco(vm.getPreco());
						itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
						itemKit.setQuantidade(vm.getQuantidade());
						itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
						itemKit.setObservacao(vm.getObservacao());
						if(vm.getLoteestoque() != null){
							itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
						}
						itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

						if(vm.getComprimentooriginal() != null){
							itemKit.setComprimento(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							itemKit.setLargura(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							itemKit.setAltura(vm.getAltura().toString());
						}
						itemKit.setMaterial(vm.getMaterial());
						
						if(kit.getTotal() == null) kit.setTotal(itemKit.getTotal());
						else kit.setTotal(kit.getTotal() + itemKit.getTotal());
						
						kit.getItens().add(itemKit);
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitRTF kit = new VendaMaterialKitRTF();
					kit.setCodigo(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					kit.setDescricao(vm.getMaterialmestre().getNome());
					kit.setMaterial(vm.getMaterialmestre());
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getPesobruto() != null){
						Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
						if(vm.getMaterialmestre().getPesobruto() != null){
							Double pesoBruto = vm.getMaterialmestre().getPesobruto();
							if(vm.getUnidademedida() != null && vm.getMaterialmestre() != null && 
									vm.getMaterialmestre().getUnidademedida() != null && 
									!vm.getUnidademedida().equals(vm.getMaterialmestre().getUnidademedida())){
								pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
							}
							kit.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)));
						}
					}
					if(vm.getPesoVendaOuMaterial() != null){
						kit.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())));
					}
					kit.setNcmcompleto(vm.getMaterialmestre().getNcmcompleto());
					if(vm.getMaterialmestre().getArquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterialmestre().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterialmestre().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					
					VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
					itemKit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
					itemKit.setDescricao(vm.getMaterial().getNome());
					itemKit.setUnidademedida(vm.getUnidademedida().getNome());
					itemKit.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
					itemKit.setPreco(vm.getPreco());
					itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
					itemKit.setQuantidade(vm.getQuantidade());
					itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
					itemKit.setObservacao(vm.getObservacao());
					if(vm.getLoteestoque() != null){
						itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
					}
					itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

					if(vm.getComprimentooriginal() != null){
						itemKit.setComprimento(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						itemKit.setLargura(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						itemKit.setAltura(vm.getAltura().toString());
					}
					itemKit.setMaterial(vm.getMaterial());
					kit.setTotal(itemKit.getTotal());
					
					kit.getItens().add(itemKit);
					listaProdutoskit.add(kit);
				}
			}else if(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional()){
				boolean adicionar = true;
				for(VendaMaterialKitRTF kit : listaProdutoskit){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterial())){
						adicionar = false;
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitRTF kit = new VendaMaterialKitRTF();
					kit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
					kit.setDescricao(vm.getMaterial().getNome());
					//kit.setQuantidade(vm.getQuantidade());
					kit.setMaterial(vm.getMaterial());
					if(vm.getMaterial().getPesobruto() != null){
						Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
						if(vm.getMaterial().getPesobruto() != null){
							Double pesoBruto = vm.getMaterial().getPesobruto();
							if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
									vm.getMaterial().getUnidademedida() != null && 
									!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
								pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
							}
							kit.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)));
						}
					}
					if(vm.getPesoVendaOuMaterial() != null){
						kit.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())));
					}
					kit.setNcmcompleto(vm.getMaterial().getNcmcompleto());
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
					}
					if(vm.getMaterial().getArquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterial().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterial().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vm.getMaterial().getCdmaterial());
					material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
					
					try{
						material.setProduto_altura(vm.getAltura());
						material.setProduto_largura(vm.getLargura());
						material.setQuantidade(vm.getQuantidade());
						materialrelacionadoService.calculaQuantidadeKit(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
						Double total = 0d;
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getQuantidade() != null && vm.getQuantidade() != null){
								Material materialItemDoKit = materialrelacionado.getMaterialpromocao();
								VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
								itemKit.setDescricao(materialItemDoKit.getNome());
								itemKit.setCodigo(materialItemDoKit.getIdentificacaoOuCdmaterial());
								Double valorvendaItemKit = materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0d;
								Double preco = vm.getPreco() != null ? vm.getPreco() : 0d;
								Double valorvendaMaterial = material.getValorvenda() != null ? material.getValorvenda() : 0d;
								if(valorvendaMaterial == 0) valorvendaMaterial = 1d;
								itemKit.setPreco(preco * ((valorvendaItemKit * 100) / valorvendaMaterial) / 100);
								itemKit.setQuantidade(materialrelacionado.getQuantidade() * vm.getQuantidade());
								itemKit.setTotal(itemKit.getPreco() * itemKit.getQuantidade());
								itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
								total += itemKit.getTotal();
								
								Unidademedida unidademedida = materialItemDoKit.getUnidademedida();

								itemKit.setUnidademedida(unidademedida.getNome());
								itemKit.setUnidademedidasimbolo(unidademedida.getSimbolo());
								
								itemKit.setObservacao(vm.getObservacao());
								if(vm.getLoteestoque() != null){
									itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
								}
								/*itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

								if(vm.getComprimentooriginal() != null){
									itemKit.setComprimento(vm.getComprimentooriginal().toString());
								}
								if(vm.getLargura() != null){
									itemKit.setLargura(vm.getLargura().toString());
								}
								if(vm.getAltura() != null){
									itemKit.setAltura(vm.getAltura().toString());
								}*/
								
								
								
								itemKit.setMaterial(vm.getMaterial());
								
								kit.getItens().add(itemKit);
							}
						}
						kit.setTotal(total);
					}
					listaProdutoskit.add(kit);
				}
			}
		}
	}
	
	private void addMaterialKitflexivelRTF(List<VendaMaterialKitflexivelRTF> listaProdutoskitflexivel, Vendaorcamentomaterial vm, Vendaorcamentomaterialmestre vmm) {
		if(listaProdutoskitflexivel != null && vm != null && vmm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() && vm.getMaterialmestre().getKitflexivel()){
				boolean adicionar = true;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				for(VendaMaterialKitflexivelRTF kit : listaProdutoskitflexivel){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterialmestre()) && 
							kit.getIdentificadormaterialmestre() != null && 
							vm.getIdentificadorinterno() != null &&
							kit.getIdentificadormaterialmestre().equals(vm.getIdentificadorinterno())){
						adicionar = false;
						VendaMaterialKitflexivelItemRTF itemKit = new VendaMaterialKitflexivelItemRTF();
						itemKit.setUnidademedida(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getNome(): "");
						itemKit.setUnidademedidasimbolo(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getSimbolo(): "");
						itemKit.setQuantidade(vm.getQuantidade());
						if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
								!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
							itemKit.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
						}
						itemKit.setPreco(vm.getPreco());
						itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
						itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
						itemKit.setObservacao(vm.getObservacao());
						itemKit.setDescricao(vm.getMaterial().getNome());
						if(vm.getLoteestoque() != null){
							itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
						}
						itemKit.setDesconto(vm.getDesconto() != null? vm.getDesconto().getValue().doubleValue(): null);
						if(vm.getComprimentooriginal() != null){
							itemKit.setComprimento(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							itemKit.setLargura(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							itemKit.setAltura(vm.getAltura().toString());
						}
						
						itemKit.setNcmcompleto(vm.getMaterial().getNcmcompleto());

						itemKit.setPesobruto(vm.getMaterial().getPesobruto());
						itemKit.setPesoliquido(vm.getMaterial().getPesobruto());
						
						itemKit.setMaterial(vm.getMaterial());
						
						kit.getItens().add(itemKit);
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitflexivelRTF kit = new VendaMaterialKitflexivelRTF();
					kit.setCodigo(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					kit.setDescricao(org.apache.commons.lang.StringUtils.isNotEmpty(vmm.getNomealternativo()) ? vmm.getNomealternativo() : vmm.getMaterial().getNome());
					kit.setPesobruto(vm.getMaterialmestre().getPesobruto());
					kit.setPesoliquido(vm.getMaterialmestre().getPesobruto());
					kit.setNcmcompleto(vmm.getMaterial().getNcmcompleto());
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					kit.setIdentificadormaterialmestre(vmm.getIdentificadorinterno() != null ? vmm.getIdentificadorinterno() : "");
					kit.setDesconto(vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : null);
					kit.setDtentrega(vmm.getDtprazoentrega()==null ? "" : sdf.format(vmm.getDtprazoentrega()));
					kit.setUnidademedida(vmm.getUnidademedida()!=null && vmm.getUnidademedida().getNome()!=null ? vmm.getUnidademedida().getNome() : "");
					kit.setQuantidade(vmm.getQtde());
					kit.setPreco(vmm.getPreco());
					
					kit.setTotal(vmm.getPreco()*vmm.getQtde() - (vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : 0d));
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getArquivo() != null && vm.getMaterialmestre().getArquivo().getCdarquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterialmestre().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterialmestre().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					kit.setMaterial(vm.getMaterialmestre());
					
					VendaMaterialKitflexivelItemRTF itemKit = new VendaMaterialKitflexivelItemRTF();
					itemKit.setDescricao(vm.getMaterial().getNome());
					itemKit.setPreco(vm.getPreco());
					itemKit.setQuantidade(vm.getQuantidade());
					if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						itemKit.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
					}
					itemKit.setUnidademedida(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getNome(): "");
					itemKit.setUnidademedidasimbolo(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getSimbolo(): "");
					itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
					
					itemKit.setNcmcompleto(vm.getMaterial().getNcmcompleto());
					
					itemKit.setObservacao(vm.getObservacao());
					itemKit.setPesobruto(vm.getMaterial().getPesobruto());
					itemKit.setPesoliquido(vm.getMaterial().getPesobruto());
					
					itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
					if(vm.getComprimentooriginal() != null){
						itemKit.setComprimento(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						itemKit.setLargura(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						itemKit.setAltura(vm.getAltura().toString());
					}
					if(vm.getLoteestoque() != null){
						itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
					}
					
					itemKit.setMaterial(vm.getMaterial());
					
					kit.getItens().add(itemKit);
					listaProdutoskitflexivel.add(kit);
				}
			}
		}		
	}
	
	public Prazopagamento getPrazopagamentoConfirmacaoVenda(Vendaorcamento vendaorcamento) {
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null){
			List<Vendaorcamentoformapagamento> lista = vendaorcamentoformapagamentoService.findByVendaorcamento(vendaorcamento);
			if(lista != null && lista.size() == 1){
				for(Vendaorcamentoformapagamento vendaorcamentoformapagamento : lista){
					return vendaorcamentoformapagamento.getPrazopagamento();
				}
			}
		}
		return null;
	}
	public void updateMotivoReprovacao(String ids,
			MotivoReprovacaoFaturamento motivo, String justificativareprovar) {
		vendaorcamentoDAO.updateMotivoReprovacao(ids, motivo,justificativareprovar);
	}
	public boolean isIdentificadorExternoRepetido(Vendaorcamento bean) {
		if(bean.getIdentificadorexterno() !=null){
			List<Vendaorcamento> isRepetido = vendaorcamentoDAO.identificadorExternoRepetido(bean.getCdvendaorcamento(),bean.getIdentificadorexterno());
			if(isRepetido != null && isRepetido.size()>0){
				return true;
			}
		}
		return false;
	}
	public boolean isAutorizaAprovado(Vendaorcamento bean) {
		if(bean.getCdvendaorcamento() != null){
			if (pedidovendaService.isOrigemOrcamento(bean) || vendaService.isOrigemOrcamento(bean)){
				return true;
			}
		}
		return false;
	}
	public boolean isAutorizaContrato(Vendaorcamento bean){
		if(contratoService.isOrigemOrcamento(bean)){
			return true;
		}
		return false;
	}
	
	public boolean verificarPermitirMaterialMestreVenda(Vendaorcamento vendaorcamento) {
		Vendaorcamento vendaOrcamentoFound = vendaorcamentoDAO.findCdMaterialById(vendaorcamento);
		
		if (vendaOrcamentoFound != null & vendaOrcamentoFound.getListavendaorcamentomaterial() != null) {
			for (Vendaorcamentomaterial m : vendaOrcamentoFound.getListavendaorcamentomaterial()) {
				if (m.getMaterial() != null && materialService.isMaterialMestre(m.getMaterial())) {
					return Boolean.TRUE;
				}
			}
		}
		
		return Boolean.FALSE;
	}
	
	public void cancelaAtualizacaoPreco(Vendaorcamentomaterial vendaOrcamentoMaterial){
		if(parametrogeralService.getBoolean(Parametrogeral.TABELA_VENDA_CLIENTE)){
			Materialtabelaprecoitem bean = materialtabelaprecoitemService.loadByVendaOrcamentoMaterial(vendaOrcamentoMaterial);
			if(bean != null){
				MaterialTabelaPrecoItemHistorico ultimoPreco = materialTabelaPrecoItemHistoricoService.loadUltimoHistoricoComVendaNaoCancelada(bean);
				if(ultimoPreco != null){
					if(Tipocalculo.PERCENTUAL.equals(bean.getMaterialtabelapreco().getTabelaprecotipo())){
						materialtabelaprecoitemService.updatePercentualDesconto(bean, ultimoPreco.getPercentualDesconto(), true);
					}else {
						materialtabelaprecoitemService.updatePreco(bean, ultimoPreco.getValor(), true);
					}
				}
			}
		}
	}
	
	public void setTotalImpostos(Vendaorcamento venda){
		if(venda != null){
			Money valorIpi = new Money(0);
			Money valorIcms = new Money(0);
			Money valorIcmsSt = new Money(0);
			Money valorFcp = new Money(0);
			Money valorFcpSt = new Money(0);
			Money valorDifal = new Money(0);
			Money valorDesoneracaoIcms = new Money(0);
			if(SinedUtil.isListNotEmpty(venda.getListavendaorcamentomaterial())){
				for (Vendaorcamentomaterial item : venda.getListavendaorcamentomaterial()) {
					valorIpi = valorIpi.add(item.getValoripi());
					valorIcms = valorIcms.add(item.getValoricms());
					valorIcmsSt = valorIcmsSt.add(item.getValoricmsst());
					valorFcp = valorFcp.add(item.getValorfcp());
					valorFcpSt = valorFcpSt.add(item.getValorfcpst());
					valorDifal = valorDifal.add(item.getValordifal());
					if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
						valorDesoneracaoIcms = valorDesoneracaoIcms.add(item.getValordesoneracaoicms());
					}
				}
			}
			if(SinedUtil.isListNotEmpty(venda.getListavendaorcamentomaterialmestre())){
				for (Vendaorcamentomaterialmestre item : venda.getListavendaorcamentomaterialmestre()) {
					valorIpi = valorIpi.add(item.getValoripi());
					valorIcms = valorIcms.add(item.getValoricms());
					valorIcmsSt = valorIcmsSt.add(item.getValoricmsst());
					valorFcp = valorFcp.add(item.getValorfcp());
					valorFcpSt = valorFcpSt.add(item.getValorfcpst());
					valorDifal = valorDifal.add(item.getValordifal());
					if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
						valorDesoneracaoIcms = valorDesoneracaoIcms.add(item.getValordesoneracaoicms());
					}
				}
			}
			venda.setTotalFcp(valorFcp);
			venda.setTotalFcpSt(valorFcpSt);
			venda.setTotalIcms(valorIcms);
			venda.setTotalIcmsSt(valorIcmsSt);
			venda.setTotalipi(valorIpi);
			venda.setTotalDifal(valorDifal);
			venda.setTotalDesoneracaoIcms(valorDesoneracaoIcms);
		}
	}
	
	/**
	 * Gera o relat�rio de custo x venda do or�amento de venda
	 *
	 * @param filtro
	 * @return
	 * @author Michael Rodrigues
	 */
	public IReport gerarRelatorioCustoorcamentovenda(EmitircustovendaFiltro filtro) {
		Report report = new Report("/faturamento/custoorcamentovenda");
		Report reportSub = new Report("/faturamento/sub_custoorcamentovenda");
		Report reportSubOrcamentoVendamaterial = new Report("/faturamento/sub_custoorcamentovendamaterial");
		reportSubOrcamentoVendamaterial.addParameter("calcularmargempor", filtro.getCalcularmargempor() != null ? filtro.getCalcularmargempor() : "Custo");

		reportSub.addSubReport("SUB_CUSTOORCAMENTOVENDAMATERIAL", reportSubOrcamentoVendamaterial);
		report.addSubReport("SUB_CUSTOORCAMENTOVENDA", reportSub);

		List<Vendaorcamento> lista = this.findForEmitircustoorcamentovenda(filtro);
		/*
		 * List<Materialdevolucao> listaMaterialdevolucao = null;
		 * 
		 * if (lista != null && lista.size() > 0) { listaMaterialdevolucao =
		 * materialdevolucaoService
		 * .getListaMaterialdevolucaoByVendaSemConversaoUnidadeprincipal
		 * (CollectionsUtil .listAndConcatenate(lista, "cdvenda", ",")); }
		 */
		if (lista == null || lista.size() == 0)
			throw new SinedException("N�o h� registros para este per�odo de datas. ");

		HashMap<Cliente, String> mapClienteVendedorPrincipal = new HashMap<Cliente, String>();
		StringBuilder whereInProduto = new StringBuilder();

		for (Vendaorcamento vendaorcamento : lista) {
			if (Boolean.TRUE.equals(filtro.getIsVendedorPrincipal())) {
				vendaorcamento.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(vendaorcamento.getCliente(), mapClienteVendedorPrincipal));
			}

			for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()) {
				if (vendaorcamento.getPedidovendatipo() != null && vendaorcamento.getPedidovendatipo().getBonificacao() != null && vendaorcamento.getPedidovendatipo().getBonificacao()) {
					vendaorcamentomaterial.setPreco(0d);
				}

				if (vendaorcamentomaterial.getMaterial() != null && vendaorcamentomaterial.getMaterial().getProduto() != null && vendaorcamentomaterial.getMaterial().getProduto()) {
					if (!"".equals(whereInProduto.toString()))
						whereInProduto.append(",");
					whereInProduto.append(vendaorcamentomaterial.getMaterial().getCdmaterial());
				}
				/*
				 * if (listaMaterialdevolucao != null &&
				 * listaMaterialdevolucao.size() > 0) { for (Materialdevolucao
				 * materialdevolucao : listaMaterialdevolucao) { boolean
				 * vendaIgual = materialdevolucao.getVenda() != null &&
				 * materialdevolucao.getVenda().getCdvenda() != null &&
				 * materialdevolucao.getVenda().getCdvenda()
				 * .equals(vendaorcamento.getCdvenda()); boolean
				 * vendamaterialIgual = materialdevolucao .getVendamaterial() !=
				 * null && materialdevolucao.getVendamaterial()
				 * .getCdvendamaterial() != null && materialdevolucao
				 * .getVendamaterial() .getCdvendamaterial()
				 * .equals(vendaorcamentomaterial .getCdvendamaterial());
				 * boolean materialIgual = materialdevolucao.getMaterial() !=
				 * null && materialdevolucao.getMaterial().equals(
				 * vendaorcamentomaterial.getMaterial()); boolean
				 * quantidadeIgual = materialdevolucao .getQtdevendida() != null
				 * && materialdevolucao.getQtdevendida().equals(
				 * vendaorcamentomaterial.getQuantidade());
				 * 
				 * if (vendaIgual && ((materialdevolucao.getVendamaterial() !=
				 * null && vendamaterialIgual) || (materialdevolucao
				 * .getVendamaterial() == null && materialIgual &&
				 * quantidadeIgual))) {
				 * vendaorcamentomaterial.setQtdejadevolvida(materialdevolucao
				 * .getQtdedevolvida());
				 * vendaorcamentomaterial.setQuantidade(vendaorcamentomaterial
				 * .getQuantidade() - materialdevolucao.getQtdedevolvida());
				 * 
				 * if (vendaorcamentomaterial.getQuantidade() <= 0d) {
				 * vendaorcamentomaterial.setQuantidade(0d);
				 * vendaorcamentomaterial.setDesconto(null); } } } }
				 */
			}
		}

		List<Produto> listaProdutos = null;
		if (whereInProduto != null && !"".equals(whereInProduto.toString())) {
			listaProdutos = produtoService.findForCustovenda(whereInProduto.toString());

			if (listaProdutos != null && !listaProdutos.isEmpty()) {
				for (Produto produto : listaProdutos) {
					for (Vendaorcamento vendaorcamento : lista) {
						for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()) {
							if (vendaorcamentomaterial.getMaterial() != null && vendaorcamentomaterial.getMaterial().getCdmaterial() != null
									&& vendaorcamentomaterial.getMaterial().getCdmaterial().equals(produto.getCdmaterial())) {
								vendaorcamentomaterial.setProdutotrans(produto);
							}
						}
					}
				}
			}
		}

		Double totalcusto = 0.0;
		Double totalvendamaterial = 0.0;
		Double totaldescontoaplicado = 0.0;
		Money totaldescontovenda = new Money();
		Money totaldescontovendaitem = new Money();
		Money totallucrobruto = new Money();
		Double totalvalorprodutos = 0.0;
		Double totalvalorprodutossemdesconto = 0d;
		Double totalvalorvendaprodutos = 0.0;
		Money margemmedia = new Money();
		Money valorvalecompraproporcional = new Money();
		Money descontoproporcionalsemvale = new Money();
		Money descontoproporcional;
		Boolean conversao;
		boolean considerarDesconto = filtro.getConsiderarDescontos() != null ? filtro.getConsiderarDescontos() : false;
		boolean considerarvalecompra = filtro.getConsiderarValeCompra() != null ? filtro.getConsiderarValeCompra() : false;
		List<Vendaorcamento> listaVendaAjustada = new ArrayList<Vendaorcamento>();

		for (Vendaorcamento vendaorcamento : lista) {
			List<Vendaorcamentomaterial> listaVendamaterialAjustada = new ArrayList<Vendaorcamentomaterial>();
			for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()) {
				if (vendaorcamentomaterial.getMaterial() != null && !vendaService.verificaFiltroCustovenda(filtro, vendaorcamentomaterial.getMaterial())) {
					continue;
				}

				listaVendamaterialAjustada.add(vendaorcamentomaterial);
				vendaorcamentomaterial.setCalcularmargempor(filtro.getCalcularmargempor());
				vendaorcamentomaterial.setConsiderarDesconto(considerarDesconto);
				vendaorcamentomaterial.setConsiderarValeCompra(considerarvalecompra);
				valorvalecompraproporcional = notafiscalprodutoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, null,
						vendaorcamento.getValorusadovalecompra(), false);
				descontoproporcionalsemvale = notafiscalprodutoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, vendaorcamento.getDesconto(),
						null, false);

				// Valor proporcional vale compra
				if (valorvalecompraproporcional != null) {
					vendaorcamentomaterial.setValorvalecomprapropocional(valorvalecompraproporcional.getValue().doubleValue());
				}

				// Valor proporcional do desconto sem vale compra
				if (descontoproporcionalsemvale != null) {
					vendaorcamentomaterial.setValordescontosemvalecompra(descontoproporcionalsemvale.getValue().doubleValue());
				}

				// Caso true, vale compra adicionado para os c�lculos, sen�o o
				// mesmo � retirado enviando null no lugar
				if (considerarvalecompra) {
					descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, vendaorcamento.getDesconto(),
							vendaorcamento.getValorusadovalecompra(), false);
				} else {
					descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, vendaorcamento.getDesconto(), null,
							false);
				}

				if (descontoproporcional != null) {
					vendaorcamentomaterial.setDescontoProporcional(descontoproporcional.getValue().doubleValue());
				}

				conversao = (vendaorcamentomaterial.getMaterial().getUnidademedida() != null && vendaorcamentomaterial.getUnidademedida() != null
						&& !vendaorcamentomaterial.getMaterial().getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) && vendaorcamentomaterial.getFatorconversao() != null);

				if (conversao) {
					if (vendaorcamentomaterial.getValorcustomaterial() == null) {
						vendaorcamentomaterial.setValorcustomaterial(vendaorcamentomaterial.getMaterial().getValorcusto());
					}

					if (vendaorcamentomaterial.getValorcustomaterial() != null) {
						vendaorcamentomaterial.setValorcustomaterial(SinedUtil.roundByParametro(
								(vendaorcamentomaterial.getValorcustomaterial() / vendaorcamentomaterial.getFatorconversaoQtdereferencia()), 2));
					}
				}

				conversao = conversao && (vendaorcamentomaterial.getUnidademedida().getCasasdecimaisestoque() != null);

				if (conversao && vendaorcamentomaterial.getMaterial().getValorvenda() != null) {
					vendaorcamentomaterial.setValorvendamaterial(SinedUtil.roundByParametro(
							vendaorcamentomaterial.getMaterial().getValorvenda() / vendaorcamentomaterial.getFatorconversaoQtdereferencia(), 2));
				}

				vendaorcamentomaterial.setConsiderarMultiplicadorCusto(!parametrogeralService.getBoolean("DESCONSIDERAR_MULTIPLICADOR_CUSTO"));
				Double valorcustoRelatorio = vendaorcamentomaterial.getValorcustoRelatorio();

				if (valorcustoRelatorio != null && !Double.isInfinite(valorcustoRelatorio) && vendaorcamentomaterial.getQuantidade() != null) {
					if (conversao) {
						totalcusto += SinedUtil.roundByParametro(valorcustoRelatorio * vendaorcamentomaterial.getQuantidade(), 2);
					} else {
						totalcusto += valorcustoRelatorio * vendaorcamentomaterial.getQuantidade();
					}
				}

				Money totalvalorvendaRelatorio = new Money();
				Double valorvendaRelatorio = vendaorcamentomaterial.getValorvendaRelatorio();

				if (valorvendaRelatorio != null && !Double.isInfinite(valorvendaRelatorio) && vendaorcamentomaterial.getQuantidadeRelatorio() != null) {
					if (conversao) {
						totalvalorvendaRelatorio = new Money(SinedUtil.roundByParametro(valorvendaRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio(), 2));
					} else {
						totalvalorvendaRelatorio = new Money(valorvendaRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio());
					}
				}

				Double valorprecoRelatorio = vendaorcamentomaterial.getPrecoRelatorio();

				if (valorprecoRelatorio != null && !Double.isInfinite(valorprecoRelatorio) && vendaorcamentomaterial.getQuantidadeRelatorio() != null) {
					if (conversao) {
						totalvendamaterial += SinedUtil.roundByParametro(valorprecoRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio(), 2);
					} else {
						totalvendamaterial += valorprecoRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio();
					}
				}

				if (vendaorcamentomaterial.getPercentualdescontocustovenda() != null) {
					totaldescontoaplicado += vendaorcamentomaterial.getPercentualdescontocustovenda();
				}

				if (vendaorcamentomaterial.getConsiderarDesconto() != null && vendaorcamentomaterial.getConsiderarDesconto() && vendaorcamentomaterial.getDescontoProporcional() != null) {
					totaldescontovenda = totaldescontovenda.add(new Money(vendaorcamentomaterial.getValordescontosemvalecompra()));
				}

				if (vendaorcamentomaterial.getConsiderarValeCompra() != null && vendaorcamentomaterial.getConsiderarValeCompra() && vendaorcamentomaterial.getValorvalecomprapropocional() != null) {
					totaldescontovenda = totaldescontovenda.add(new Money(vendaorcamentomaterial.getValorvalecomprapropocional()));
				}

				if (vendaorcamentomaterial.getValordescontocustovendaitem() != null) {
					totaldescontovendaitem = totaldescontovendaitem.add(new Money(vendaorcamentomaterial.getValordescontocustovendaitem()));
				}

				if (vendaorcamentomaterial.getPreco() != null && vendaorcamentomaterial.getQuantidade() != null) {
					vendaorcamentomaterial
							.setTotalprodutoReport(new Money(valorprecoRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio()).subtract(vendaorcamentomaterial.getDesconto() != null ? vendaorcamentomaterial
									.getDesconto() : new Money()));

					if (filtro.getConsiderarDescontos() != null && filtro.getConsiderarDescontos() && vendaorcamentomaterial.getDescontoProporcional() != null
							&& vendaorcamentomaterial.getDescontoProporcional() > 0) {
						vendaorcamentomaterial.setTotalprodutoReport(vendaorcamentomaterial.getTotalprodutoReport().subtract(new Money(vendaorcamentomaterial.getDescontoProporcional())));
					}

					if (filtro.getConsiderarValeCompra() != null && filtro.getConsiderarValeCompra() && vendaorcamentomaterial.getValorvalecomprapropocional() != null
							&& vendaorcamentomaterial.getValorvalecomprapropocional() > 0 && (filtro.getConsiderarDescontos() == null || !filtro.getConsiderarDescontos())) {
						vendaorcamentomaterial.setTotalprodutoReport(vendaorcamentomaterial.getTotalprodutoReport().subtract(new Money(vendaorcamentomaterial.getValorvalecomprapropocional())));
					}

					if (conversao) {
						vendaorcamentomaterial.setTotalprodutoReport(new Money(SinedUtil.roundByParametro(vendaorcamentomaterial.getTotalprodutoReport().getValue().doubleValue(), 2)));
					}

					vendaorcamentomaterial.setTotalprodutoDescontoReport(new Money(valorprecoRelatorio * vendaorcamentomaterial.getQuantidadeRelatorio()));

					if (conversao) {
						vendaorcamentomaterial.setTotalprodutoDescontoReport(new Money(SinedUtil.roundByParametro(vendaorcamentomaterial.getTotalprodutoDescontoReport().getValue().doubleValue(), 2)));
					}

					totalvalorprodutos += totalvalorvendaRelatorio.getValue().doubleValue();
					totalvalorprodutossemdesconto += vendaorcamentomaterial.getTotalprodutoDescontoReport().getValue().doubleValue();
					totalvalorvendaprodutos += vendaorcamentomaterial.getTotalprodutoReport().getValue().doubleValue();
				}
			}

			if (SinedUtil.isListNotEmpty(listaVendamaterialAjustada)) {
				vendaorcamento.setListavendaorcamentomaterial(listaVendamaterialAjustada);
				listaVendaAjustada.add(vendaorcamento);
			}
		}

		report.addParameter("LISTAORCAMENTOVENDA", listaVendaAjustada);
		report.addParameter("QTDEORCAMENTOVENDA", listaVendaAjustada.size());

		// Money totalProdutos = new Money(totalvalorprodutos);
		Money totalvendaProdutos = new Money(totalvalorprodutossemdesconto);
		totallucrobruto = new Money(totalvalorprodutossemdesconto).subtract(new Money(totalcusto));

		if (considerarDesconto) {
			totallucrobruto = totallucrobruto.subtract(totaldescontovenda);
		}

		if (totalvendaProdutos != null && totalvendaProdutos.getValue() != null && totalcusto != null) {
			Money valorTotalAux = totalvendaProdutos.subtract(new Money(totalcusto));

			if (valorTotalAux != null && valorTotalAux.getValue() != null && filtro.getConsiderarDescontos() != null && filtro.getConsiderarDescontos() && totaldescontovenda != null
					&& totaldescontovenda.getValue() != null) {
				valorTotalAux = valorTotalAux.subtract(totaldescontovenda);
			}

			if (valorTotalAux != null && valorTotalAux.getValue() != null) {
				if ("CUSTO".equalsIgnoreCase(filtro.getCalcularmargempor()) && totalcusto > 0.) {
					margemmedia = new Money(valorTotalAux.getValue().doubleValue() * 100 / totalcusto);
				} else if ("VENDAORCAMENTO".equalsIgnoreCase(filtro.getCalcularmargempor()) && totalvendamaterial > 0.) {
					margemmedia = new Money(valorTotalAux.getValue().doubleValue() * 100 / totalvendamaterial);
				}
			}
		}

		reportSubOrcamentoVendamaterial.addParameter("considerarvalecompra", considerarvalecompra);
		report.addParameter("totalcusto", new Money(totalcusto));
		report.addParameter("totaldescontoaplicado", totalvalorprodutossemdesconto != null && totalvalorprodutossemdesconto > 0 ? new Money(totaldescontovendaitem.getValue().doubleValue()
				/ totalvalorprodutossemdesconto * 100) : new Money());
		report.addParameter("totaldescontovenda", totaldescontovenda);
		report.addParameter("totallucrobruto", totallucrobruto);
		report.addParameter("totalvalorprodutos", new Money(totalvalorprodutos));
		report.addParameter("margemmedia", margemmedia);
		report.addParameter("totalvenda", new Money(totalvalorvendaprodutos));
		report.addParameter("exibirformulacalculo", filtro.getExibirformulacalculo() != null ? filtro.getExibirformulacalculo() : false);

		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForEmitircustoorcamentovenda(EmitircustovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Michael Rodrigues
	 */
	public List<Vendaorcamento> findForEmitircustoorcamentovenda(EmitircustovendaFiltro filtro) {
		return vendaorcamentoDAO.findForEmitircustoorcamentovenda(filtro);
	}
	
	private void preencheListaVendaOrcamentoFornecedorTicketMedio(Vendaorcamento venda, List<VendaFornecedorTicketMedioBean> lista){
		venda.setListaVendaOrcamentoFornecedorTicketMedio(new ArrayList<VendaOrcamentoFornecedorTicketMedio>());
		for(VendaFornecedorTicketMedioBean beanTicket: lista){
			venda.getListaVendaOrcamentoFornecedorTicketMedio().add(beanTicket.translateForVendaOrcamento(venda));
		}
	}
}