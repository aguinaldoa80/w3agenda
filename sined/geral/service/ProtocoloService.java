package br.com.linkcom.sined.geral.service;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Protocolo;
import br.com.linkcom.sined.geral.dao.ProtocoloDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProtocoloService extends GenericService<Protocolo>{
	
	private ProtocoloDAO protocoloDao;
	
 public void setProtocoloDao(ProtocoloDAO protocoloDao) {
		this.protocoloDao = protocoloDao;
	}

 
 	
 /**
	 * Atualiza a dada de leitura como a data atual
	 * @param bean
	 * @see  br.com.linkcom.sined.geral.dao.ProtocoloDAO#recebeProtocolo(Protocolo)
	 * @author C�ntia Nogueira
	 * 
	 */
	public void recebeProtocolo(Protocolo bean){
		protocoloDao.recebeProtocolo(bean);
	}

	/**
	 * Fun��o para a gera��o do relat�rio de eventos
	 * @param filtro
	 * @return
	 * @see  br.com.linkcom.sined.geral.dao.ProtocoloDAO#findForRelatorioEvento(EventoservicointernoFiltro)
	 * @author C�ntia Nogueira
	 */
	public List<Protocolo> findForRelatorioEvento(EventoservicointernoFiltro filtro){
		return protocoloDao.findForRelatorioEvento(filtro);
	}
}
