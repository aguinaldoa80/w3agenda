package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Etapa;
import br.com.linkcom.sined.geral.dao.EtapaDAO;

public class EtapaService extends GenericService<Etapa>{
	
	public Etapa loadByCdetapa(Integer cdcodigo){
		return EtapaDAO.getInstance().loadByCdetapa(cdcodigo);
	}
	public static EtapaService instance;
	
	public static EtapaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EtapaService.class);
		}
		return instance;
	}

}
