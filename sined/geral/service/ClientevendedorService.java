package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.ClientevendedorDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.clientevendedor.ClientevendedorRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.clienteVendedor.ClienteVendedorW3producaoRESTModel;

public class ClientevendedorService extends GenericService<Clientevendedor>{

	private ClientevendedorDAO clientevendedorDAO;
	private ParametrogeralService parametrogeralService;
	private ColaboradorService colaboradorService;
		
	public void setClientevendedorDAO(ClientevendedorDAO clientevendedorDAO) {
		this.clientevendedorDAO = clientevendedorDAO;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	/* singleton */
	private static ClientevendedorService instance;
	public static ClientevendedorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClientevendedorService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * buscar os Colaboadores que est�o como clientevendedor - Autocomplete
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ClientevendedorDAO#findClientevendedorAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaborador> findClientevendedorAutocomplete(String q){
		return clientevendedorDAO.findClientevendedorAutocomplete(q);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ClientevendedorDAO#findByCliente(Cliente cliente)
	 *
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public List<Clientevendedor> findByCliente(Cliente form) {
		return clientevendedorDAO.findByCliente(form);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClientevendedorDAO#findVendedorPrincipalByCliente(Cliente cliente)
	*
	* @param cliente
	* @return
	* @since 10/11/2016
	* @author Luiz Fernando
	*/
	public List<Clientevendedor> findVendedorPrincipalByCliente(Cliente cliente) {
		return clientevendedorDAO.findVendedorPrincipalByCliente(cliente);
	}
	
	/**
	 * Action que retorna um JsonModelAndView com o nome do vendedor principal
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/09/2014
	 */
	public ModelAndView actionVendedorprincipalVendaPedido(Cliente cliente) {
		Colaborador colaborador = getColaboradorVendedorPrincipal(cliente);
		String vendedorprincipal = colaborador != null && colaborador.getNome() != null ? colaborador.getNome() : "";
		Boolean preencherVendedorPrincipal = parametrogeralService.getBoolean(Parametrogeral.PREENCHER_VENDEDORPRINCIPAL_PEDIDODEVENDA);
		
		boolean isVendedorPrincipal = false;
		if (Util.objects.isPersistent(colaborador)) {
			isVendedorPrincipal = true;
		}
		
		colaborador = colaborador != null ? colaborador : SinedUtil.getUsuarioComoColaborador();
		String colaboradorNome = "";
		if (colaborador != null && colaborador.getNome() != null) {
			colaboradorNome = colaborador.getNome();
			colaborador.setNome(Util.strings.escape(colaborador.getNome()));
		}
		
		return new JsonModelAndView()
			.addObject("vendedorprincipal", vendedorprincipal)
			.addObject("colaborador", colaborador)
			.addObject("colaboradorNome", colaboradorNome)
			.addObject("isVendedorPrincipal", isVendedorPrincipal)
			.addObject("preencherVendedorPrincipal", preencherVendedorPrincipal);
	}

	
	/**
	* M�todo que retorna o nome do vendedor principal do cliente
	*
	* @param cliente
	* @return
	* @since 10/11/2016
	* @author Luiz Fernando
	 * @param mapClienteVendedorPrincipal 
	*/
	public String getNomeVendedorPrincipal(Cliente cliente) {
		return getNomeVendedorPrincipal(cliente, null);
	}
	
	public String getNomeVendedorPrincipal(Cliente cliente, HashMap<Cliente, String> mapClienteVendedorPrincipal) {
		Colaborador vendedorprincipal = null;
		if(mapClienteVendedorPrincipal != null && cliente != null && cliente.getCdpessoa() != null){
			if(mapClienteVendedorPrincipal.containsKey(cliente)){
				return mapClienteVendedorPrincipal.get(cliente);
			}
			
			vendedorprincipal = getVendedorPrincipal(cliente);
			mapClienteVendedorPrincipal.put(cliente, vendedorprincipal != null ? vendedorprincipal.getNome() : null);
		}else {
			vendedorprincipal = getVendedorPrincipal(cliente);
		}
		return vendedorprincipal != null && vendedorprincipal.getNome() != null ? vendedorprincipal.getNome() : "";
	}
	
	
	
	public Colaborador getColaboradorVendedorPrincipal(Cliente cliente, HashMap<Cliente, Colaborador> mapClienteVendedorPrincipal) {
		Colaborador vendedorprincipal = null;
		if(mapClienteVendedorPrincipal != null && cliente != null && cliente.getCdpessoa() != null){
			if(mapClienteVendedorPrincipal.containsKey(cliente)){
				return mapClienteVendedorPrincipal.get(cliente);
			}
			
			vendedorprincipal = getVendedorPrincipal(cliente);
			mapClienteVendedorPrincipal.put(cliente, vendedorprincipal);
		}else {
			vendedorprincipal = getVendedorPrincipal(cliente);
		}
		return vendedorprincipal;
	}
	
	public Colaborador getColaboradorVendedorPrincipal(Cliente cliente) {
		return getVendedorPrincipal(cliente);
	}
	
	/**
	* M�todo que retorna o vendedor principal do cliente
	*
	* @param cliente
	* @return
	* @since 10/11/2016
	* @author Luiz Fernando
	*/
	public Colaborador getVendedorPrincipal(Cliente cliente) {
		Colaborador vendedorprincipal = null;
		List<Clientevendedor> lista = this.findVendedorPrincipalByCliente(cliente);
		if(lista == null || lista.size() == 0){
			boolean obrigarVendedorPrincipal = parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_VENDEDORPRINCIPAL_VENDA);
			if(!obrigarVendedorPrincipal)
				lista = this.findByCliente(cliente);
		}
		if(lista != null && lista.size() > 0){
			Clientevendedor clientevendedor = lista.get(0);
			if(clientevendedor != null && clientevendedor.getColaborador() != null){
				vendedorprincipal = clientevendedor.getColaborador();
			}
		}
		return vendedorprincipal;
	}

	public List<ClientevendedorRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ClientevendedorRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClientevendedorRESTModel> lista = new ArrayList<ClientevendedorRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Clientevendedor c : clientevendedorDAO.findForAndroid(whereIn))
				lista.add(new ClientevendedorRESTModel(c));
		}
		
		return lista;
	}

	public List<Clientevendedor> findByClienteForAgenciaColaborador(Cliente cliente){
		return clientevendedorDAO.findByClienteForAgenciaColaborador(cliente);
	}
	
	public ModelAndView ajaxBuscaVendedorPrincipal(WebRequestContext request, Cliente cliente){
		List<Clientevendedor> listaClientevendedor = this.findVendedorPrincipalByCliente(cliente);
		ModelAndView retorno = new JsonModelAndView();
		if(!listaClientevendedor.isEmpty()){
			retorno.addObject("clientevendedor", listaClientevendedor.get(0));
		}
		return retorno;
	}
	public List<ClienteVendedorW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ClienteVendedorW3producaoRESTModel> lista = new ArrayList<ClienteVendedorW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Clientevendedor c: clientevendedorDAO.findForW3Producoa(whereIn, Boolean.TRUE) ){
					ClienteVendedorW3producaoRESTModel rest = new ClienteVendedorW3producaoRESTModel();
					rest.setCdCliente(c.getCliente().getCdpessoa());
					rest.setCdColaborador(c.getColaborador().getCdpessoa());
					rest.setNomeColaborador(c.getColaborador().getNome());
					lista.add(rest);
			}
		}
		return lista;
	}
	
	public Clientevendedor findByClienteAndColaborador(Cliente cliente, Colaborador vendedor) {
		return clientevendedorDAO.findByClienteAndColaborador(cliente, vendedor);
	}
	
	public void criarRelacaoClientevendedor(Cliente cliente, CriarPedidovendaBean criarPedidovendaBean){
		if(org.apache.commons.lang.StringUtils.isNotBlank(criarPedidovendaBean.getVendedor_cpf())){
			Colaborador vendedor = null;
			String cnpjcpf = br.com.linkcom.neo.util.StringUtils.soNumero(criarPedidovendaBean.getVendedor_cpf());
			if(cnpjcpf.length() == 11){
				try {
					vendedor = colaboradorService.findByCpf(new Cpf(cnpjcpf));
				} catch (Exception e) {
					System.out.println("Colaborador n�o encontrado na base com o CPF "+criarPedidovendaBean.getVendedor_cpf());
					e.printStackTrace();
				}
			}
			
			if(Util.objects.isPersistent(vendedor)){
				Clientevendedor clientevendedor = null;
				List<Clientevendedor> listaClientevendedor = this.findByCliente(cliente);
				if(SinedUtil.isListNotEmpty(listaClientevendedor)){
					clientevendedor = this.findByClienteAndColaborador(cliente, vendedor);
				}
				if(clientevendedor == null){
					clientevendedor = new Clientevendedor();
					
					clientevendedor.setCliente(cliente);
					clientevendedor.setColaborador(vendedor);
					clientevendedor.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
					clientevendedor.setPrincipal(SinedUtil.isListEmpty(listaClientevendedor));
					
					this.saveOrUpdate(clientevendedor);
				}
			}
		}
	}
}
