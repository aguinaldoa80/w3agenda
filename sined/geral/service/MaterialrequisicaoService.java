package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.MaterialrequisicaoDAO;

public class MaterialrequisicaoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Materialrequisicao> {

	private MaterialrequisicaoDAO materialrequisicaoDAO;
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setMaterialrequisicaoDAO(
			MaterialrequisicaoDAO materialrequisicaoDAO) {
		this.materialrequisicaoDAO = materialrequisicaoDAO;
	}
	
	/* singleton */
	private static MaterialrequisicaoService instance;
	public static MaterialrequisicaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialrequisicaoService.class);
		}
		return instance;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListaMaterialRequisicaoByRequisicao
	 * @param requisicao
	 * @return
	 * @return List<Materialrequisicao>
	 * @author Thiago Augusto
	 * @date 31/05/2012
	 *
	 */
	public List<Materialrequisicao> findListaMaterialRequisicaoByRequisicao(Requisicao requisicao){
		return materialrequisicaoDAO.findListaMaterialRequisicaoByRequisicao(requisicao);
	}
	
	public void updateFaturadoByAutorizacao(String whereIn, String whereInMaterialrequisicao){
		materialrequisicaoDAO.updateFaturadoByAutorizacao(whereIn, whereInMaterialrequisicao);
	}
	
	public void updateFaturadoByRequisicao(String whereIn){
		materialrequisicaoDAO.updateFaturadoByRequisicao(whereIn);
	}
	
	public void updateFaturado(String whereIn){
		materialrequisicaoDAO.updateFaturado(whereIn);
	}
	
	public void updatePercentualFaturado(Materialrequisicao materialrequisicao, Double percentual){
		materialrequisicaoDAO.updatePercentualFaturado(materialrequisicao, percentual);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialrequisicaoDAO#updateMaterialrequisicaoFaturado(String whereInNFSI)
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateMaterialrequisicaoFaturado(String whereIn) {
		materialrequisicaoDAO.updateMaterialrequisicaoFaturado(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialrequisicaoDAO#updateItemMaterialrequisicaoByItemNota(Integer cdnotafiscalservicoitem, Integer cdmaterialrequisicao)
	 *
	 * @param cdnotafiscalservicoitem
	 * @param cdmaterialrequisicao
	 * @author Luiz Fernando
	 */
	public void updateItemMaterialrequisicaoByItemNota(Integer cdnotafiscalservicoitem, Integer cdmaterialrequisicao) {
		materialrequisicaoDAO.updateItemMaterialrequisicaoByItemNota(cdnotafiscalservicoitem, cdmaterialrequisicao);
	}
	
	/**
	 * Compara��o por autoriza��o
	 *
	 * @param o1
	 * @param o2
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public int compareWithAutorizacao(Materialrequisicao o1, Materialrequisicao o2) {
		String autStr1 = o1.getAutorizacaoFaturamento();
		String autStr2 = o2.getAutorizacaoFaturamento();
		
		if(autStr1 == null && autStr2 == null) return 0;
		if(autStr1 == null) return 1;
		if(autStr2 == null) return -1;
		
		try {
			Integer aut1 = Integer.parseInt(autStr1);
			Integer aut2 = Integer.parseInt(autStr2);
			
			return aut1.compareTo(aut2);
		} catch (Exception e) {
			
		}
		
		return autStr1.compareTo(autStr2);
	}
	
	/**
	 * Compara��o por item
	 *
	 * @param o1
	 * @param o2
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public int compareWithItem(Materialrequisicao o1, Materialrequisicao o2) {
		String itemStr1 = o1.getItem();
		String itemStr2 = o2.getItem();
		
		if(itemStr1 == null && itemStr2 == null) return 0;
		if(itemStr1 == null) return 1;
		if(itemStr2 == null) return -1;
		
		try {
			Integer item1 = Integer.parseInt(itemStr1);
			Integer item2 = Integer.parseInt(itemStr2);
			
			return item1.compareTo(item2);
		} catch (Exception e) {
			
		}
		
		return itemStr1.compareTo(itemStr2);
	}
	
	/**
	 * Compara��o por material
	 *
	 * @param o1
	 * @param o2
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public int compareWithNomeMaterial(Materialrequisicao o1, Materialrequisicao o2) {
		Material material1 = o1.getMaterial();
		Material material2 = o2.getMaterial();
		
		if((material1 == null || material1.getNome() == null) && (material2 == null || material2.getNome() == null)) return 0;
		if(material1 == null || material1.getNome() == null) return 1;
		if(material2 == null || material2.getNome() == null) return -1;
		
		return material1.getNome().compareTo(material2.getNome());
	}

	public void saveArquivos(List<Materialrequisicao> listMaterial) {
		if(!listMaterial.isEmpty()){
			for (Materialrequisicao mat : listMaterial) {
				if(mat.getArquivo() !=null){
					arquivoDAO.saveFile(mat, "arquivo");
				}	
			}
		}	
	}
}
