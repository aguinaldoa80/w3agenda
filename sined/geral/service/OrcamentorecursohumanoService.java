package br.com.linkcom.sined.geral.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Dependenciacargo;
import br.com.linkcom.sined.geral.bean.Dependenciafaixa;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Periodoorcamentocargo;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.bean.Relacaocargo;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoHumanoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.GastoMDOItemReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.GastoMDOReportBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrcamentorecursohumanoService extends GenericService<Orcamentorecursohumano>{
	
	public final static String SESSION_FILTRO_RELATORIO_RH = "session_filtro_relatorio_gastomdo";
	
	private OrcamentorecursohumanoDAO orcamentorecursohumanoDAO;
	private BdiService bdiService;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private RelacaocargoService relacaocargoService;
	private ComposicaomaoobraService composicaomaoobraService;
	private ParametrogeralService parametrogeralService;
	
	public void setComposicaomaoobraService(
			ComposicaomaoobraService composicaomaoobraService) {
		this.composicaomaoobraService = composicaomaoobraService;
	}
	public void setRelacaocargoService(RelacaocargoService relacaocargoService) {
		this.relacaocargoService = relacaocargoService;
	}
	public void setPeriodoorcamentocargoService(
			PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	public void setOrcamentorecursohumanoDAO(OrcamentorecursohumanoDAO orcamentorecursohumanoDAO) {
		this.orcamentorecursohumanoDAO = orcamentorecursohumanoDAO;
	}
	public void setBdiService(BdiService bdiService) {
		this.bdiService = bdiService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * Busca os recursos humanos do or�amento a partir de um filtro
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO#findForListagemFlex(OrcamentoRecursoHumanoFiltro)
	 * 
	 * @param filtro
	 * @return List<Orcamentorecursohumano>
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Orcamentorecursohumano> findForListagemFlex(OrcamentoRecursoHumanoFiltro filtro) {
		return orcamentorecursohumanoDAO.findRecursoHumano(filtro);
	}
	
	/**
	 * Atualiza o custo do recurso humano passado como par�metro
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO#atualizaGastoRecursoHumanoForFlex(Orcamentorecursohumano)
	 *
	 * @param orcamentorecursohumano
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaGastoRecursoHumanoForFlex(Orcamentorecursohumano orcamentorecursohumano) {
		orcamentorecursohumanoDAO.atualizaGastoRecursoHumanoForFlex(orcamentorecursohumano);
	}
	
	/***
	 * Insere/atualiza/apaga os registros em Orcamentorecursohumano baseado na listaPeriodoOrcamentoCargo
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO#atualizaOrcamentoRecursoHumanoFlex(List, List)
	 * 
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargoApp
	 * @return 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoRecursoHumanoFlex(Orcamento orcamento, List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoApp) {
		if (orcamento == null || orcamento.getCdorcamento() == null || listaPeriodoOrcamentoCargoApp == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		Boolean horahomennototalefetivo = parametrogeralService.getBoolean(Parametrogeral.HORA_HOMEM_NO_TOTAL_EFETIVO);
		
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo = new ArrayList<Periodoorcamentocargo>(listaPeriodoOrcamentoCargoApp);
		
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForUpdate = new ArrayList<Orcamentorecursohumano>();
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForDelete = new ArrayList<Orcamentorecursohumano>();
		Orcamentorecursohumano orcamentoRecursoHumanoNovo;
		
		boolean existeCargo;
		int indexCargo;
		int i;		
		
		OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
		filtro.setOrcamento(orcamento);
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoBanco = findForListagemFlex(filtro);
		
		if (listaOrcamentoRecursoHumanoBanco != null) {
			for (Orcamentorecursohumano orcamentoRecursoHumanoBanco : listaOrcamentoRecursoHumanoBanco) {
				existeCargo = false;
				indexCargo  = -1;
				
				//Verifica se existe o cargo na lista que veio da aplica��o
				i = 0;
				for (Periodoorcamentocargo periodoOrcamentoCargoApp: listaPeriodoOrcamentoCargo) {
					if (periodoOrcamentoCargoApp.getCargo().equals(orcamentoRecursoHumanoBanco.getCargo())) {
						
						orcamentoRecursoHumanoBanco.setOrcamento(orcamento);
						if(horahomennototalefetivo != null && horahomennototalefetivo){
							orcamentoRecursoHumanoBanco.setTotalhoras(periodoOrcamentoCargoApp.getTotalHoraEfetiva());
						}else {
							orcamentoRecursoHumanoBanco.setTotalhoras(periodoOrcamentoCargoApp.getTotalhoracalculada());
						}
						listaOrcamentoRecursoHumanoForUpdate.add(orcamentoRecursoHumanoBanco);
						
						indexCargo  = i; 
						existeCargo = true;
						break;
					}
					i++;
				}
				
				if (existeCargo) {					
					//Remove o elemento da lista da aplica��o, j� que ele foi adicionado na lista para atualiza��o
					listaPeriodoOrcamentoCargo.remove(indexCargo);
				}
				else {
					//Adiciona o cargo na lista para exclus�o
					listaOrcamentoRecursoHumanoForDelete.add(orcamentoRecursoHumanoBanco);
				}
			}
		}
		
		//Adiciona os cargos que ainda existem na lista da aplica��o
		for (Periodoorcamentocargo periodoOrcamentoCargoApp : listaPeriodoOrcamentoCargo) {
			
			orcamentoRecursoHumanoNovo = new Orcamentorecursohumano();
			orcamentoRecursoHumanoNovo.setOrcamento(orcamento);
			orcamentoRecursoHumanoNovo.setCargo(periodoOrcamentoCargoApp.getCargo());
			orcamentoRecursoHumanoNovo.setTotalhoras(periodoOrcamentoCargoApp.getTotalHoraEfetiva());
			Money custoporhoraByCargo = composicaomaoobraService.getCustoporhoraByCargo(periodoOrcamentoCargoApp.getCargo());
			if(custoporhoraByCargo != null){
				orcamentoRecursoHumanoNovo.setCustohora(custoporhoraByCargo);
			} else {
				orcamentoRecursoHumanoNovo.setCustohora(periodoOrcamentoCargoApp.getCargo().getCustohora());
			}
			
			listaOrcamentoRecursoHumanoForUpdate.add(orcamentoRecursoHumanoNovo);
		}
		
		//Atualiza os valores no banco de dados
		orcamentorecursohumanoDAO.atualizaOrcamentoRecursoHumanoFlex(listaOrcamentoRecursoHumanoForUpdate, listaOrcamentoRecursoHumanoForDelete);		
	}
	
	public void atualizaOrcamentoRecursoHumanoSemHistogramaFlex(Orcamento orcamento) {
		
		Map<Cargo, Double> mapa = periodoorcamentocargoService.carregaListaCargoQuantidadeSemHistograma(orcamento);
		
		List<Relacaocargo> listaRelacaoCargo = relacaocargoService.findByOrcamento(orcamento);
		mapa = this.calculaRecursoHumanoDependenciaSemHistograma(orcamento, mapa, listaRelacaoCargo);
		
		OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
		filtro.setOrcamento(orcamento);
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoBanco = this.findForListagemFlex(filtro);
		
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoCalculado = this.preencheListaAPartirMapa(mapa);
		
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForUpdate = new ArrayList<Orcamentorecursohumano>();
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForDelete = new ArrayList<Orcamentorecursohumano>();
		Orcamentorecursohumano orcamentoRecursoHumanoNovo;
		
		boolean existeCargo;
		int indexCargo;
		int i;
		
		if (listaOrcamentoRecursoHumanoBanco != null) {
			for (Orcamentorecursohumano orcamentoRecursoHumanoBanco : listaOrcamentoRecursoHumanoBanco) {
				existeCargo = false;
				indexCargo  = -1;
				
				//Verifica se existe o cargo na lista que veio da aplica��o
				i = 0;
				for (Orcamentorecursohumano orcamentoRecursoHumanoCalculado: listaOrcamentoRecursoHumanoCalculado) {
					if (orcamentoRecursoHumanoCalculado.getCargo().equals(orcamentoRecursoHumanoBanco.getCargo())) {
						
						orcamentoRecursoHumanoBanco.setOrcamento(orcamento);
						orcamentoRecursoHumanoBanco.setTotalhoras(orcamentoRecursoHumanoCalculado.getTotalhoras());
						listaOrcamentoRecursoHumanoForUpdate.add(orcamentoRecursoHumanoBanco);
						
						indexCargo  = i; 
						existeCargo = true;
						break;
					}
					i++;
				}
				
				if (existeCargo) {					
					//Remove o elemento da lista da aplica��o, j� que ele foi adicionado na lista para atualiza��o
					listaOrcamentoRecursoHumanoCalculado.remove(indexCargo);
				}
				else {
					//Adiciona o cargo na lista para exclus�o
					listaOrcamentoRecursoHumanoForDelete.add(orcamentoRecursoHumanoBanco);
				}
			}
		}
		
		//Adiciona os cargos que ainda existem na lista da aplica��o
		for (Orcamentorecursohumano orcamentoRecursoHumanoCalculado: listaOrcamentoRecursoHumanoCalculado) {
			
			orcamentoRecursoHumanoNovo = new Orcamentorecursohumano();
			orcamentoRecursoHumanoNovo.setOrcamento(orcamento);
			orcamentoRecursoHumanoNovo.setCargo(orcamentoRecursoHumanoCalculado.getCargo());
			orcamentoRecursoHumanoNovo.setTotalhoras(orcamentoRecursoHumanoCalculado.getTotalhoras());
			Money custoporhoraByCargo = composicaomaoobraService.getCustoporhoraByCargo(orcamentoRecursoHumanoCalculado.getCargo());
			if(custoporhoraByCargo != null){
				orcamentoRecursoHumanoNovo.setCustohora(custoporhoraByCargo);
			} else {
				orcamentoRecursoHumanoNovo.setCustohora(orcamentoRecursoHumanoCalculado.getCargo().getCustohora());
			}
			listaOrcamentoRecursoHumanoForUpdate.add(orcamentoRecursoHumanoNovo);
		}
		
		//Atualiza os valores no banco de dados
		orcamentorecursohumanoDAO.atualizaOrcamentoRecursoHumanoFlex(listaOrcamentoRecursoHumanoForUpdate, listaOrcamentoRecursoHumanoForDelete);	
	}	
	
	private List<Orcamentorecursohumano> preencheListaAPartirMapa(Map<Cargo, Double> mapa) {
		List<Orcamentorecursohumano> lista = new ArrayList<Orcamentorecursohumano>();
		Orcamentorecursohumano orcamentorecursohumano;
		
		Set<Entry<Cargo, Double>> entrySet = mapa.entrySet();
		for (Entry<Cargo, Double> entry : entrySet) {
			orcamentorecursohumano = new Orcamentorecursohumano();
			Cargo cargo = entry.getKey();
			Double totalhoras = entry.getValue();
			
			orcamentorecursohumano.setCargo(cargo);
			orcamentorecursohumano.setTotalhoras(totalhoras);
			Money custoporhoraByCargo = composicaomaoobraService.getCustoporhoraByCargo(cargo);
			if(custoporhoraByCargo != null){
				orcamentorecursohumano.setCustohora(custoporhoraByCargo);
			} else {
				orcamentorecursohumano.setCustohora(cargo.getCustohora());
			}
			
			lista.add(orcamentorecursohumano);
		}
		
		return lista;
	}
	
	private Map<Cargo, Double> calculaRecursoHumanoDependenciaSemHistograma(Orcamento orcamento, Map<Cargo, Double> mapa, List<Relacaocargo> listaRelacaoCargo) {

		//1. Busca os cargos que n�o possuem depend�ncia
		mapa = this.calculaValoresRelacaoCargoSemDependencia(orcamento, mapa, listaRelacaoCargo);
		
		//2. Busca os cargos que possuem depend�ncia com cargos do tipo M�o-de-Obra Direta
		mapa = this.calculaValoresRelacaoCargoDependenciaMOD(orcamento, mapa, listaRelacaoCargo, true);		

		//3. Busca os cargos que possuem depend�ncia com alguns cargos
		mapa = this.calculaValoresRelacaoCargoDependenciaSelecaoCargo(orcamento, mapa, listaRelacaoCargo);
		
		//5. Busca os cargos que possuem depend�ncia com todos os cargos
		mapa = this.calculaValoresRelacaoCargoDependenciaMOD(orcamento, mapa, listaRelacaoCargo, false);
		
		return mapa;
	}
	
	private Map<Cargo, Double> calculaValoresRelacaoCargoDependenciaSelecaoCargo(Orcamento orcamento, Map<Cargo, Double> mapa, List<Relacaocargo> listaRelacaoCargo) {
		Double qtde;
		Double qtdeHDep, qtdeH;
		Cargo cargo;
		Set<Entry<Cargo, Double>> entrySet;
		List<Cargo> listaCargo;
		
		if (listaRelacaoCargo != null) {
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if (relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
					
					if(mapa.containsKey(relacaocargo.getCargo())){
						qtde = mapa.get(relacaocargo.getCargo());
					} else {
						qtde = 0d;
					}
					
					listaCargo = new ArrayList<Cargo>();
					if (relacaocargo.getListaDependenciacargo() != null) {
						for (Dependenciacargo dependenciaCargo : relacaocargo.getListaDependenciacargo()) {
							listaCargo.add(dependenciaCargo.getCargo());
						}
					}
					
					qtdeH = 0d;
					qtdeHDep = 0d;
					entrySet = mapa.entrySet();
					for (Entry<Cargo, Double> entry : entrySet) {
						cargo = entry.getKey();
						if(listaCargo.contains(cargo)){
							qtdeH += (entry.getValue() / cargo.getTotalhorasemana());
						}
					}
					
					if (relacaocargo.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
						qtdeHDep = qtdeH / relacaocargo.getQuantidade();
					} else {
						Double faixaDe, faixaAte;
						if(relacaocargo.getListaDependenciafaixa() != null){
							for (Dependenciafaixa dependenciaFaixa : relacaocargo.getListaDependenciafaixa()) {
								faixaDe  = dependenciaFaixa.getFaixade();
								faixaAte = dependenciaFaixa.getFaixaate() != null ? dependenciaFaixa.getFaixaate() : Double.MAX_VALUE;
	
								if (qtdeH >= faixaDe && qtdeH <= faixaAte) {
									qtdeHDep = dependenciaFaixa.getQuantidade();
									break;
								}
							}
						}
					}
					
					qtde += qtdeHDep * relacaocargo.getCargo().getTotalhorasemana();
					mapa.put(relacaocargo.getCargo(), qtde);
				}
			}
		}
		
		return mapa;
	}
	
	private Map<Cargo, Double> calculaValoresRelacaoCargoDependenciaMOD(Orcamento orcamento, Map<Cargo, Double> mapa, List<Relacaocargo> listaRelacaoCargo, boolean somenteMOD) {

		Double qtde;
		Double qtdeHDep, qtdeH;
		Cargo cargo;
		Set<Entry<Cargo, Double>> entrySet;
		if (listaRelacaoCargo != null) {
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if ((somenteMOD && relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.MOD)) ||
					(!somenteMOD && relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.TODOS_CARGOS))) {
					
					if(mapa.containsKey(relacaocargo.getCargo())){
						qtde = mapa.get(relacaocargo.getCargo());
					} else {
						qtde = 0d;
					}
					
					qtdeH = 0d;
					qtdeHDep = 0d;
					entrySet = mapa.entrySet();
					for (Entry<Cargo, Double> entry : entrySet) {
						cargo = entry.getKey();
						if(!somenteMOD || cargo.getTipocargo().equals(Tipocargo.MOD)){
							qtdeH += entry.getValue() / cargo.getTotalhorasemana();
						}
					}
					
					if (relacaocargo.getTiporelacaorecurso().equals(Tiporelacaorecurso.VALOR_UNICO)) {
						qtdeHDep = qtdeH / relacaocargo.getQuantidade();
					} else {
						Double faixaDe, faixaAte;
						if(relacaocargo.getListaDependenciafaixa() != null){
							for (Dependenciafaixa dependenciaFaixa : relacaocargo.getListaDependenciafaixa()) {
								faixaDe  = dependenciaFaixa.getFaixade();
								faixaAte = dependenciaFaixa.getFaixaate() != null ? dependenciaFaixa.getFaixaate() : Double.MAX_VALUE;
	
								if (qtdeH >= faixaDe && qtdeH <= faixaAte) {
									qtdeHDep = dependenciaFaixa.getQuantidade();
									break;
								}
							}
						}
					}
					
					qtde += qtdeHDep * relacaocargo.getCargo().getTotalhorasemana();
					mapa.put(relacaocargo.getCargo(), qtde);
				}
			}
		}
		
		return mapa;
	}
	
	private Map<Cargo, Double> calculaValoresRelacaoCargoSemDependencia(Orcamento orcamento, Map<Cargo, Double> mapa, List<Relacaocargo> listaRelacaoCargo) {
		
		Double qtde;
		if (listaRelacaoCargo != null) {
			for (Relacaocargo relacaocargo : listaRelacaoCargo) {
				if (relacaocargo.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
					if(mapa.containsKey(relacaocargo.getCargo())){
						qtde = mapa.get(relacaocargo.getCargo());
						qtde += relacaocargo.getQuantidade() * relacaocargo.getCargo().getTotalhorasemana();
					} else {
						qtde = relacaocargo.getQuantidade() * relacaocargo.getCargo().getTotalhorasemana();
					}
					mapa.put(relacaocargo.getCargo(), qtde);
				}
			}
		}
		
		return mapa;
	}
	
	/**
	 * Calcula o custo total da m�o-de-obra direta
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMDO(Orcamento, Tipocargo)
	 * 
	 * @param orcamento
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Double calculaCustoTotalMOD(Orcamento orcamento) {
		return calculaCustoTotalMDO(orcamento, Tipocargo.MOD);
	}
	
	/**
	 * Calcula o custo total da m�o-de-obra indireta
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMDO(Orcamento, Tipocargo)
	 * 
	 * @param orcamento
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Double calculaCustoTotalMOI(Orcamento orcamento) {
		return calculaCustoTotalMDO(orcamento, Tipocargo.MOI);
	}	
	
	/**
	 * Calcula o custo total da m�o-de-obra, direta ou indireta, dependendo do par�metro tipoCargo
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO#findRecursoHumano(OrcamentoRecursoHumanoFiltro)
	 * 
	 * @param orcamento
	 * @param tipoCargo
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Double[] calculaCustoTotalQtdeMDO(Orcamento orcamento, Tipocargo tipoCargo) {
		Double custoTotal = 0d;
		Double qtdeTotal = 0d;

		Double totalHoras;
		Double custoHora;
		Double totalFatorMdo;
		Double valorHHEncargoFator;
		Double valorTotalRecurso;
		
		OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
		filtro.setOrcamento(orcamento);
		filtro.setTipoCargo(tipoCargo);
		
		List<Orcamentorecursohumano> listaOrcamentoRH = this.findRecursoHumano(filtro);
		
		if (listaOrcamentoRH != null) {			
			for (Orcamentorecursohumano orcamentoRH : listaOrcamentoRH) {
				totalHoras = orcamentoRH.getTotalhoras() != null ? orcamentoRH.getTotalhoras() : 0d;
				custoHora = orcamentoRH.getCustohora() != null ? orcamentoRH.getCustohora().getValue().doubleValue() : 0d;				
				//totalHoraSemana = orcamentoRH.getCargo().getTotalhorasemana() != null ? orcamentoRH.getCargo().getTotalhorasemana() : 0;
				totalFatorMdo = orcamentoRH.getFatormdo() != null ? orcamentoRH.getFatormdo().getTotal() : 0d;
				
				//valorSalario = custoHora * totalHoraSemana * 4.28;
				valorHHEncargoFator = custoHora * totalFatorMdo;
				valorTotalRecurso = totalHoras * valorHHEncargoFator;
				
				custoTotal += valorTotalRecurso;
				qtdeTotal += totalHoras;
			}
		}		
		return new Double[]{custoTotal, qtdeTotal};
	}
	
	public Double calculaCustoTotalMDO(Orcamento orcamento, Tipocargo tipoCargo) {
		return calculaCustoTotalQtdeMDO(orcamento, tipoCargo)[0];
	}
	
	/**
	* M�todo que calcula o custo total por conta gerencial do cargo e/ou orcamento
	*
	* @param orcamento
	* @param tipoCargo
	* @return
	* @since 22/06/2015
	* @author Luiz Fernando
	*/
	public List<Projetodespesa> calculaCustoTotalQtdeMDOContagerencial(Orcamento orcamento, Tipocargo tipoCargo) {
		Double custoTotal = 0d;
		Double qtdeTotal = 0d;

		OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
		filtro.setOrcamento(orcamento);
		filtro.setTipoCargo(tipoCargo);
		
		List<Orcamentorecursohumano> listaOrcamentoRH = this.findRecursoHumano(filtro);
		List<Projetodespesa> listaProjetodespesa = new ArrayList<Projetodespesa>();
		
		HashMap<Contagerencial, Double[]> mapContagerencial = new HashMap<Contagerencial, Double[]>();
		if (listaOrcamentoRH != null) {	
			for (Orcamentorecursohumano orcamentoRH : listaOrcamentoRH) {
				Double custoTotalCG = 0d;
				Double qtdeTotalCG = 0d;
				
				Double totalHorasCG;
				Double custoHoraCG;
				Double totalFatorMdoCG;
				Double valorHHEncargoFatorCG;
				Double valorTotalRecursoCG;
					
				totalHorasCG = orcamentoRH.getTotalhoras() != null ? orcamentoRH.getTotalhoras() : 0d;
				custoHoraCG = orcamentoRH.getCustohora() != null ? orcamentoRH.getCustohora().getValue().doubleValue() : 0d;				
				//totalHoraSemana = orcamentoRH.getCargo().getTotalhorasemana() != null ? orcamentoRH.getCargo().getTotalhorasemana() : 0;
				totalFatorMdoCG = orcamentoRH.getFatormdo() != null ? orcamentoRH.getFatormdo().getTotal() : 0d;
				
				//valorSalario = custoHora * totalHoraSemana * 4.28;
				valorHHEncargoFatorCG = custoHoraCG * totalFatorMdoCG;
				valorTotalRecursoCG = totalHorasCG * valorHHEncargoFatorCG;
				
				custoTotalCG += valorTotalRecursoCG;
				qtdeTotalCG += totalHorasCG;
					
				if(orcamentoRH.getCargo() != null && orcamentoRH.getCargo().getContagerencial() != null){
					if(mapContagerencial.get(orcamentoRH.getCargo().getContagerencial()) != null){
						Double ct = mapContagerencial.get(orcamentoRH.getCargo().getContagerencial())[0];
						Double qt = mapContagerencial.get(orcamentoRH.getCargo().getContagerencial())[1];
						mapContagerencial.put(orcamentoRH.getCargo().getContagerencial(),  new Double[]{custoTotalCG+ct, qtdeTotalCG+qt});
					}else {
						mapContagerencial.put(orcamentoRH.getCargo().getContagerencial(),  new Double[]{custoTotalCG, qtdeTotalCG});
					}
				}else {
					custoTotal += valorTotalRecursoCG;
					qtdeTotal += totalHorasCG;
				}
			}
			
			if(mapContagerencial.size() > 0){
				for(Contagerencial cg : mapContagerencial.keySet()){
					Double resultados[] = mapContagerencial.get(cg);
					
					Money valortotal = new Money(resultados[0]);
					Double qtdetotal = resultados[1];
					
					Money valorhora = qtdetotal > 0 ? valortotal.divide(new Money(qtdetotal)) : new Money();
					
					listaProjetodespesa.add(new Projetodespesa(cg, valortotal, valorhora));
				}
			}
			
			Contagerencial cg = null;
			if(Tipocargo.MOD.equals(tipoCargo)){
				cg = orcamento.getContagerencialmod();
			}else if(Tipocargo.MOD.equals(tipoCargo)){
				cg = orcamento.getContagerencialmoi();
			}
			
			if(cg != null && custoTotal > 0){
				Money valortotal = new Money(custoTotal);
				Double qtdetotal = qtdeTotal;
				
				Money valorhora = qtdetotal > 0 ? valortotal.divide(new Money(qtdetotal)) : new Money();
				
				listaProjetodespesa.add(new Projetodespesa(cg, valortotal, valorhora));
			}
		}		
		return listaProjetodespesa;
	}
	
	public List<Orcamentorecursohumano> findRecursoHumano(OrcamentoRecursoHumanoFiltro filtro) {
		return orcamentorecursohumanoDAO.findRecursoHumano(filtro);
	}

	/**
	 * Coloca o filtro no escopo de sess�o, para a gera��o do relat�rio.
	 *
	 * @param filtro
	 * @author Rodrigo Alvarenga
	 */
	public void putSessionFiltroRelatorio(OrcamentoRecursoHumanoFiltro filtro){
		NeoWeb.getRequestContext().getSession().setAttribute(SESSION_FILTRO_RELATORIO_RH, filtro);
	}
	
	/**
	 * Gera o relat�rio do gasto com m�o-de-obra em PDF
	 *
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#findForListagemFlex(OrcamentoRecursoHumanoFiltro)
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaTotalGastoMDO(List)
	 * @see br.com.linkcom.sined.geral.service.BdiService#getByOrcamentoFlex(Orcamento)
	 *  
	 * @param orcamento
	 * @return IReport
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public IReport createReportGastoMDO(OrcamentoRecursoHumanoFiltro filtro) {
		
		if (filtro == null || filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		//Listas
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumano;		
		List<GastoMDOReportBean> listaGastoMDOReportBean = new ArrayList<GastoMDOReportBean>();
		List<GastoMDOItemReportBean> listaGastoMOD = new ArrayList<GastoMDOItemReportBean>();
		List<GastoMDOItemReportBean> listaGastoMOI = new ArrayList<GastoMDOItemReportBean>();		
		
		//Beans
		GastoMDOReportBean gastoMDOReportBean;
		GastoMDOItemReportBean gastoMDOItemReportBean;
		
		//Vari�veis
		DecimalFormat formatadorDecimal = new DecimalFormat("#,##0.00");
		Double totalHoraGeral = 0d;
		Double totalHoraMOD = 0d;
		Double totalHoraMOI = 0d;
		Money totalValorGeral = new Money(0);
		Money totalValorMOD = new Money(0);
		Money totalValorMOI = new Money(0);
		Money totalValorVendaGeral = new Money(0);
		Money totalValorVendaMOD = new Money(0);
		Money totalValorVendaMOI = new Money(0);
		
		Money somaGastoMDO;
		Money precoFinalBDI;
		Double fatorBDI;
		
		Double totalHora;
		Double totalHoraSemana;
		Money custoHoraSemEncargoSemFator;
		Money salario;
		Double fatorEncargo;
		Money custoHoraComEncargoComFator;
		Money custoTotal;
		Money custoHoraVenda;
		Money custoTotalVenda;
		
		listaOrcamentoRecursoHumano = findForListagemFlex(filtro);
		
		//Calculando os valores de venda
		somaGastoMDO = calculaTotalGastoMDO(listaOrcamentoRecursoHumano);
		precoFinalBDI = bdiService.getByOrcamentoFlex(filtro.getOrcamento()).getPrecoComFolga();		
		if (somaGastoMDO.getValue().intValue() != 0) {
			fatorBDI = precoFinalBDI.getValue().doubleValue() / somaGastoMDO.getValue().doubleValue();
		}
		else {
			fatorBDI = 0d;
		}		
		
		if (listaOrcamentoRecursoHumano != null) {
			for (Orcamentorecursohumano orcamentoRH : listaOrcamentoRecursoHumano) {
				
				totalHora = orcamentoRH.getTotalhoras() != null ? orcamentoRH.getTotalhoras() : 0d;
				custoHoraSemEncargoSemFator = orcamentoRH.getCustohora() != null ? orcamentoRH.getCustohora() : new Money(0);				
				totalHoraSemana = orcamentoRH.getCargo().getTotalhorasemana() != null ? orcamentoRH.getCargo().getTotalhorasemana() : 0d;
				fatorEncargo = orcamentoRH.getFatormdo() != null ? orcamentoRH.getFatormdo().getTotal() : 0d;
				salario = new Money(custoHoraSemEncargoSemFator.getValue().doubleValue() * totalHoraSemana * 4.28);
				custoHoraComEncargoComFator = new Money(custoHoraSemEncargoSemFator.getValue().doubleValue() * fatorEncargo);
				custoTotal = new Money(totalHora * custoHoraComEncargoComFator.getValue().doubleValue());
				custoHoraVenda = new Money(custoHoraComEncargoComFator.getValue().doubleValue() * fatorBDI);
				custoTotalVenda = new Money(custoHoraVenda.getValue().doubleValue() * totalHora);
				
				gastoMDOItemReportBean = new GastoMDOItemReportBean();
				gastoMDOItemReportBean.setNomeCargo(orcamentoRH.getCargo().getNome());
				gastoMDOItemReportBean.setTotalHora(totalHora.toString());
				gastoMDOItemReportBean.setCustoHoraSemEncargoSemFator("R$" + custoHoraSemEncargoSemFator.toString());
				gastoMDOItemReportBean.setSalario("R$" + salario.toString());
				gastoMDOItemReportBean.setFatorEncargo(formatadorDecimal.format(fatorEncargo));
				gastoMDOItemReportBean.setCustoHoraComEncargoComFator("R$" + custoHoraComEncargoComFator);
				gastoMDOItemReportBean.setCustoTotal("R$" + custoTotal);
				gastoMDOItemReportBean.setCustoHoraVenda("R$" + custoHoraVenda);
				gastoMDOItemReportBean.setCustoTotalVenda("R$" + custoTotalVenda);
				
				if (Tipocargo.MOD.equals(orcamentoRH.getCargo().getTipocargo())) {
					listaGastoMOD.add(gastoMDOItemReportBean);
					totalHoraMOD += totalHora;
					totalValorMOD = totalValorMOD.add(custoTotal);
					totalValorVendaMOD = totalValorVendaMOD.add(custoTotalVenda);
				}
				else if (Tipocargo.MOI.equals(orcamentoRH.getCargo().getTipocargo())) {
					listaGastoMOI.add(gastoMDOItemReportBean);
					totalHoraMOI += totalHora;
					totalValorMOI = totalValorMOI.add(custoTotal);
					totalValorVendaMOI = totalValorVendaMOI.add(custoTotalVenda);
				}				
				totalHoraGeral += totalHora;
				totalValorGeral = totalValorGeral.add(custoTotal);
				totalValorVendaGeral = totalValorVendaGeral.add(custoTotalVenda);
			}
		}
		
		gastoMDOReportBean = new GastoMDOReportBean();
		gastoMDOReportBean.setIncluirMOD(filtro.getTipoCargo() == null || filtro.getTipoCargo().equals(Tipocargo.MOD));
		gastoMDOReportBean.setIncluirMOI(filtro.getTipoCargo() == null || filtro.getTipoCargo().equals(Tipocargo.MOI));
		gastoMDOReportBean.setIncluirPrecoVenda(filtro.getIncluirPrecoVenda());
		gastoMDOReportBean.setListaMOD(listaGastoMOD);
		gastoMDOReportBean.setListaMOI(listaGastoMOI);
		gastoMDOReportBean.setTotalHoraGeral(totalHoraGeral.toString());
		gastoMDOReportBean.setTotalHoraMOD(totalHoraMOD.toString());
		gastoMDOReportBean.setTotalHoraMOI(totalHoraMOI.toString());
		gastoMDOReportBean.setTotalValorGeral("R$" + totalValorGeral.toString());
		gastoMDOReportBean.setTotalValorMOD("R$" + totalValorMOD.toString());
		gastoMDOReportBean.setTotalValorMOI("R$" + totalValorMOI.toString());
		gastoMDOReportBean.setTotalValorVendaGeral("R$" + totalValorVendaGeral.toString());
		gastoMDOReportBean.setTotalValorVendaMOD("R$" + totalValorVendaMOD.toString());
		gastoMDOReportBean.setTotalValorVendaMOI("R$" + totalValorVendaMOI.toString());
		listaGastoMDOReportBean.add(gastoMDOReportBean);
		
		Report report = new Report("/projeto/gastomdo");
		report.addSubReport("SUBREPORTGASTOMDO",new Report("/projeto/gastomdosub"));
		report.setDataSource(listaGastoMDOReportBean);
		return report;
	}
	
	/***
	 * Calcula o valor da soma dos gastos com m�o-de-obra de uma lista de recursos humanos de um or�amento
	 * 
	 * @param listaOrcamentoRH
	 * @return Money
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public Money calculaTotalGastoMDO(List<Orcamentorecursohumano> listaOrcamentoRH) {
		Double totalHora;
		Money custoHoraSemEncargoSemFator;
		Double fatorEncargo;
		Money custoHoraComEncargoComFator;
		Money custoTotal;
		Money somaGastoMDO = new Money(0);
		
		if (listaOrcamentoRH == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		for (Orcamentorecursohumano orcamentoRH : listaOrcamentoRH) {			
			totalHora = orcamentoRH.getTotalhoras() != null ? orcamentoRH.getTotalhoras() : 0d;
			custoHoraSemEncargoSemFator = orcamentoRH.getCustohora() != null ? orcamentoRH.getCustohora() : new Money(0);				
			fatorEncargo = orcamentoRH.getFatormdo() != null ? orcamentoRH.getFatormdo().getTotal() : 0d;
			custoHoraComEncargoComFator = new Money(custoHoraSemEncargoSemFator.getValue().doubleValue() * fatorEncargo);
			custoTotal = new Money(totalHora * custoHoraComEncargoComFator.getValue().doubleValue());
			somaGastoMDO = somaGastoMDO.add(custoTotal);
		}
		return somaGastoMDO;
	}	
	
	/***
	 * Calcula o total de horas de m�o-de-obra de um or�amento
	 * 
	 * @param orcamento
	 * @return Double
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public Double calculaTotalHorasMDO(Orcamento orcamento) {
		//Calcula o total de horas de m�o-de-obra do or�amento
		Double totalHoras = 0d;
		OrcamentoRecursoHumanoFiltro filtro = new OrcamentoRecursoHumanoFiltro();
		filtro.setOrcamento(orcamento);
		filtro.setTipoCargo(null);
		List<Orcamentorecursohumano> listaOrcamentoRecursoHumano = findForListagemFlex(filtro);
		if (listaOrcamentoRecursoHumano != null) {
			for (Orcamentorecursohumano orcamentoRH : listaOrcamentoRecursoHumano) {
				totalHoras += orcamentoRH.getTotalhoras() != null ? orcamentoRH.getTotalhoras() : 0;
			}
		}
		return totalHoras;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentorecursohumanoDAO#findByOrcamento
	 * @param orcamento
	 * @param mod
	 * @param moi
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentorecursohumano> findByOrcamento(Orcamento orcamento, Boolean mod, Boolean moi) {
		return orcamentorecursohumanoDAO.findByOrcamento(orcamento, mod, moi);
	}

	public List<Orcamentorecursohumano> findByCargoOrcamento(Orcamento orcamento) {
		return orcamentorecursohumanoDAO.findByCargoOrcamento(orcamento);
	}

	
}
