package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaItem;
import br.com.linkcom.sined.geral.dao.PropostaItemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PropostaItemService extends GenericService<PropostaItem> {

	private PropostaItemDAO propostaItemDAO;
	
	public void setPropostaItemDAO(PropostaItemDAO propostaItemDAO) {
		this.propostaItemDAO = propostaItemDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PropostaItemDAO#findByProposta(Proposta proposta)
	 * 
	 * @param proposta
	 * @return
	 * @since 24/01/2012
	 * @author Rodrigo Freitas
	 */
	public List<PropostaItem> findByProposta(Proposta proposta){
		return propostaItemDAO.findByProposta(proposta);
	}
	
	
}
