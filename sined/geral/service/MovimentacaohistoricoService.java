package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.MovimentacaohistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaohistoricoService extends GenericService<Movimentacaohistorico>{
	
	private MovimentacaohistoricoDAO movimentacaohistoricoDAO;
	
	public void setMovimentacaohistoricoDAO(MovimentacaohistoricoDAO movimentacaohistoricoDAO) {
		this.movimentacaohistoricoDAO = movimentacaohistoricoDAO;
	}
	
	private static MovimentacaohistoricoService instance;
	public static MovimentacaohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MovimentacaohistoricoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo para criar uma <b>Movimentacaohistorico</b> a partir de uma <b>Movimentacao</b>
	 * 
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacaohistorico geraHistoricoMovimentacao(Movimentacao movimentacao){
		Movimentacaohistorico mh = new Movimentacaohistorico();
		mh.setMovimentacao(movimentacao);
		mh.setMovimentacaoacao(movimentacao.getMovimentacaoacao());
		mh.setObservacao(movimentacao.getObservacao());
		if(StringUtils.isNotEmpty(movimentacao.getObservacaoConciliacao())){
			if(StringUtils.isNotEmpty(mh.getObservacao())){
				if(!mh.getObservacao().toUpperCase().contains(movimentacao.getObservacaoConciliacao().toUpperCase())){
					mh.setObservacao(mh.getObservacao()+". " +movimentacao.getObservacaoConciliacao());
				}
			}else {
				mh.setObservacao(movimentacao.getObservacaoConciliacao());
			}
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado != null){
			mh.setCdusuarioaltera(usuarioLogado.getCdpessoa());
		}
		
		mh.setDtaltera(new Timestamp(System.currentTimeMillis())); 
		return mh;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m historico de movimentacao financeira.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaohistoricoDAO#findByMovimentacao(Movimentacao)
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacaohistorico> findByMovimentacao(Movimentacao movimentacao){
		return movimentacaohistoricoDAO.findByMovimentacao(movimentacao);
	}
	
	/**
	 * M�todo para gerar hist�rico da movimenta��o de acordo com par�metros
	 *
	 * @param movimentacao
	 * @param observacao
	 * @param acao
	 * @return
	 * @author Luiz Fernando
	 */
	public Movimentacaohistorico geraHistoricoMovimentacao(Movimentacao movimentacao, String observacao, Boolean acao){
		Movimentacaohistorico mh = new Movimentacaohistorico();
		mh.setMovimentacao(movimentacao);
		if(acao != null && acao)
			mh.setMovimentacaoacao(movimentacao.getMovimentacaoacao());
		mh.setObservacao(observacao);
		mh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		mh.setDtaltera(new Timestamp(System.currentTimeMillis())); 
		return mh;
	}
}
