package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVendaPagamento;
import br.com.linkcom.sined.geral.dao.Ecom_PedidoVendaPagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class Ecom_PedidoVendaPagamentoService extends GenericService<Ecom_PedidoVendaPagamento>{

	private Ecom_PedidoVendaPagamentoDAO ecom_PedidoVendaPagamentoDAO;
	
	public void setEcom_PedidoVendaPagamentoDAO(Ecom_PedidoVendaPagamentoDAO ecom_PedidoVendaPagamentoDAO) {
		this.ecom_PedidoVendaPagamentoDAO = ecom_PedidoVendaPagamentoDAO;
	}
	
	public List<Ecom_PedidoVendaPagamento> findByPedidoVenda(Ecom_PedidoVenda pedidoVenda){
		return ecom_PedidoVendaPagamentoDAO.findByPedidoVenda(pedidoVenda);
	}
}
