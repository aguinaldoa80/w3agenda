package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoclasseService extends GenericService<Documentoclasse> {	
	
	/* singleton */
	private static DocumentoclasseService instance;
	public static DocumentoclasseService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentoclasseService.class);
		}
		return instance;
	}
	
}
