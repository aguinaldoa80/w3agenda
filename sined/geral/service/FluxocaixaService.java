package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.bean.Fluxocaixaorigem;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipolancamento;
import br.com.linkcom.sined.geral.dao.FluxocaixaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaMensalReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaMensalSubReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaOrigemBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class FluxocaixaService extends GenericService<Fluxocaixa> {

	private FluxocaixaDAO fluxocaixaDAO;
	private AgendamentoService agendamentoService;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private ContaService contaService;
	private PrazopagamentoService prazopagamentoService;
	private RateioitemService rateioitemService;
	private ContratoService contratoService;
	private ContagerencialService contagerencialService;
	private ColaboradordespesaService colaboradordespesaService;
	private PedidovendaService pedidovendaService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private CentrocustoService centrocustoService;
	private ControleorcamentoService controleOrcamentoService;
	
	public void setControleOrcamentoService(ControleorcamentoService controleOrcamentoService) {this.controleOrcamentoService = controleOrcamentoService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setFluxocaixaDAO(FluxocaixaDAO fluxocaixaDAO) {
		this.fluxocaixaDAO = fluxocaixaDAO;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {
		this.colaboradordespesaService = colaboradordespesaService;
	}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}

	/*
	 * In�cio dos m�todos de cria��o do relat�rio de Fluxo de caixa.
	 */

	/**
	 * M�todo utilizado para criar o relat�rio de Fluxo de caixa.
	 * 
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#calculaSaldoAtual(Contatipo,
	 *      java.sql.Date)
	 * @see br.com.linkcom.sined.geral.service.ContaService#calculaSaldoAtual(List,
	 *      java.sql.Date)
	 * @see br.com.linkcom.sined.geral.service.ContaService#findLimiteContas(Collection)
	 * @see br.com.linkcom.sined.geral.service.ContaService#findLimiteContatipo(Contatipo)
	 * @see #montaListaDadosRelatorio(FluxocaixaFiltroReport)
	 * @see #agrupaListaHeader(List, Money, boolean)
	 * @see #calculaCreditosDebitos(List, Report)
	 * @see #insereLinhaTotal(List)
	 * @param request
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public IReport createReportFluxocaixa(WebRequestContext request, FluxocaixaFiltroReport filtro) {

		preparaRelatorioFluxoCaixa(filtro);
		
		Report report;

		if (filtro.getMostrarDetalhes() != null && filtro.getMostrarDetalhes()) {
			report = new Report("/financeiro/fluxocaixa_analitico");
		} else {
			report = new Report("/financeiro/fluxocaixa");
		}

		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getPeriodoDe(), filtro.getPeriodoAte()));
		report.addParameter("periodoDe", filtro.getPeriodoDe());
		report.addParameter("periodoAte", filtro.getPeriodoAte());
		report.addParameter("dataAnterior", filtro.getDtInicioBase());

		if ((filtro.getMostrarDetalhes() == null || !filtro.getMostrarDetalhes()) && filtro.getMostrarLimiteContabancariaResumo() != null
						&& filtro.getMostrarLimiteContabancariaResumo()) {
			report.addParameter("LIMITECONTABANCARIA", filtro.getLimiteBancarias());
		}

		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		this.calculaCreditosDebitos(listaFluxo, report);

		Saldos saldos = new Saldos(filtro.getSaldoInicioBancarias(), filtro.getSaldoInicioCaixas(), filtro.getSaldoInicioCartoes());

		this.calculaSaldoInicialOutros(filtro, listaFluxo);
		this.ajustaSaldoAC(filtro, listaFluxo);
		this.somaSaldos(filtro, listaFluxo, saldos);

		report.addParameter("saldoAC", filtro.getSaldoAC());
		listaFluxo = this.agrupaListaHeader(listaFluxo, filtro.getSaldoAC(), Util.booleans.isTrue(filtro.getMostrarDetalhes()), filtro.getValoresAnteriores(), filtro, true, false);

		report.addParameter("saldoInicioBancarias", filtro.getSaldoInicioBancarias());
		report.addParameter("saldoInicioCaixas", filtro.getSaldoInicioCaixas());
		report.addParameter("saldoInicioCartoes", filtro.getSaldoInicioCartoes());
		report.addParameter("saldoInicioOutros", filtro.getSaldoInicioOutrosResumo());
		
		report.addParameter("saldoFimBancarias", saldos.getSaldoBancarias());
		report.addParameter("saldoFimCaixas", saldos.getSaldoCaixas());
		report.addParameter("saldoFimCartoes", saldos.getSaldoCartoes());
		report.addParameter("saldoFimOutros", saldos.getSaldoOutros());

		report.addParameter("somaInicial", filtro.getSaldoInicioBancarias().add(filtro.getSaldoInicioCaixas().add(filtro.getSaldoInicioCartoes())).add(filtro.getSaldoInicioOutrosResumo()));
		report.addParameter("somaFinal", saldos.somaSaldos());
		report.addParameter("saldoInicialInseridoManualmente", new Boolean(filtro.getSaldoACTela() != null));
		
		report.addParameter("observacoesFluxoCaixa", observacoesFluxoCaixa(filtro.getSaldoACTela() != null));

		listaFluxo = this.ajustaListaFluxoCaixaValoresAnteriores(filtro, listaFluxo);
		report.setDataSource(listaFluxo);
		
		this.insereLinhaTotal(listaFluxo);		

		return report;
	}
	
	private void calculaSaldoInicialOutros(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo) {
		if(filtro != null && filtro.getSaldoAC() != null && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() && 
				SinedUtil.isListNotEmpty(listaFluxo)){
			for (FluxocaixaBeanReport fc : listaFluxo) {
				if(fc.getData() != null && 
						SinedDateUtils.beforeOrEqualIgnoreHour((java.sql.Date) fc.getData(), (java.sql.Date) filtro.getDtInicioBase())) {
					boolean credito = Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao());
					
					if (fc.getConta() == null){
						if (credito) {
							filtro.setSaldoInicioOutros(filtro.getSaldoInicioOutros().add(fc.getCredito()));
							if((FluxocaixaBeanReport.AGENDAMENTO == fc.getTipo() && filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores()) || 
							   (FluxocaixaBeanReport.CONTRATO == fc.getTipo() && filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores()) ||
							   (FluxocaixaBeanReport.MOVIMENTACAO == fc.getTipo() && filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores()) ||
							   (FluxocaixaBeanReport.FLUXOCAIXA == fc.getTipo() && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores())){
								filtro.setSaldoInicioOutrosResumo(filtro.getSaldoInicioOutrosResumo().add(fc.getCredito()));
							}
						} else {
							filtro.setSaldoInicioOutros(filtro.getSaldoInicioOutros().subtract(fc.getDebito()));
							if((FluxocaixaBeanReport.AGENDAMENTO == fc.getTipo() && filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores()) || 
							   (FluxocaixaBeanReport.CONTRATO == fc.getTipo() && filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores()) ||
							   (FluxocaixaBeanReport.MOVIMENTACAO == fc.getTipo() && filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores()) ||
							   (FluxocaixaBeanReport.FLUXOCAIXA == fc.getTipo() && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores())){
								filtro.setSaldoInicioOutrosResumo(filtro.getSaldoInicioOutrosResumo().subtract(fc.getDebito()));
							}
						}
					}else {
						boolean caixa = Contatipo.TIPO_CAIXA.equals(fc.getConta().getContatipo());
						boolean bancaria = Contatipo.TIPO_CONTA_BANCARIA.equals(fc.getConta().getContatipo());
						boolean cartao = Contatipo.TIPO_CARTAO_CREDITO.equals(fc.getConta().getContatipo());
						
						if(!caixa && !bancaria && !cartao){
							if (credito) {
								filtro.setSaldoInicioOutros(filtro.getSaldoInicioOutros().subtract(fc.getCredito()));
								if((FluxocaixaBeanReport.AGENDAMENTO == fc.getTipo() && filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores()) || 
										   (FluxocaixaBeanReport.CONTRATO == fc.getTipo() && filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores()) ||
										   (FluxocaixaBeanReport.MOVIMENTACAO == fc.getTipo() && filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores()) ||
										   (FluxocaixaBeanReport.FLUXOCAIXA == fc.getTipo() && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores())){
											filtro.setSaldoInicioOutrosResumo(filtro.getSaldoInicioOutrosResumo().subtract(fc.getCredito()));
										}
							} else {
								filtro.setSaldoInicioOutros(filtro.getSaldoInicioOutros().add(fc.getDebito()));
								if((FluxocaixaBeanReport.AGENDAMENTO == fc.getTipo() && filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores()) || 
								   (FluxocaixaBeanReport.CONTRATO == fc.getTipo() && filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores()) ||
								   (FluxocaixaBeanReport.MOVIMENTACAO == fc.getTipo() && filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores()) ||
								   (FluxocaixaBeanReport.FLUXOCAIXA == fc.getTipo() && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores())){
									filtro.setSaldoInicioOutrosResumo(filtro.getSaldoInicioOutrosResumo().add(fc.getDebito()));
								}
							}
						}
					}
				}

			}
		}
		
	}

	/**
	* M�todo que remove as contas anteriores que n�o devem ser exibidas
	* (obs: mas o saldo deve ser considerado)
	*
	* @param filtro
	* @param listaFluxo
	* @return
	* @since 17/07/2014
	* @author Luiz Fernando
	*/
	private List<FluxocaixaBeanReport> ajustaListaFluxoCaixaValoresAnteriores(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo) {
		if(filtro != null && SinedUtil.isListNotEmpty(listaFluxo) && 
				( 
					(filtro.getLancAgendamentoAnteriores() == null || !filtro.getLancAgendamentoAnteriores()) ||
					(filtro.getLancMovimentacoesAnteriores() == null || !filtro.getLancMovimentacoesAnteriores()) ||
					(filtro.getLancContratoAnteriores() == null || !filtro.getLancContratoAnteriores())
				)){
			List<FluxocaixaBeanReport> listaFluxoAjustada = new ArrayList<FluxocaixaBeanReport>();
			for(FluxocaixaBeanReport bean : listaFluxo){
				if(removerItemListaFluxoCaixaByContaAnteriores(bean, filtro)){
					continue;
				}
				listaFluxoAjustada.add(bean);
			}
			return listaFluxoAjustada;
		}
		return listaFluxo;
	}
	
	/**
	* M�todo que verifica se � uma conta anterior e se n�o deve ser exibida no relat�rio,
	* mas � considerada no saldo anterior
	*
	* @param bean
	* @param filtro
	* @return
	* @since 17/07/2014
	* @author Luiz Fernando
	*/
	private boolean removerItemListaFluxoCaixaByContaAnteriores(FluxocaixaBeanReport bean, FluxocaixaFiltroReport filtro) {
		boolean remover = false;
		if(bean != null && filtro != null && filtro.getDtInicioBase() != null &&
				(bean.getData() != null && 
						SinedDateUtils.beforeIgnoreHour(new java.sql.Date(bean.getData().getTime()), filtro.getDtInicioBase()))  ||
				(bean.getDataAtrasada() != null && 
						SinedDateUtils.beforeIgnoreHour(new java.sql.Date(bean.getDataAtrasada().getTime()), filtro.getDtInicioBase()))
						){
			if(FluxocaixaBeanReport.AGENDAMENTO == bean.getTipo()){
				if((filtro.getLancAgendamentoAnteriores() == null || !filtro.getLancAgendamentoAnteriores()))
					remover = true;
			}else if(FluxocaixaBeanReport.MOVIMENTACAO == bean.getTipo()){
				if((filtro.getLancMovimentacoesAnteriores() == null || !filtro.getLancMovimentacoesAnteriores()))
					remover = true;
			}else if(FluxocaixaBeanReport.CONTRATO == bean.getTipo()){
				if((filtro.getLancContratoAnteriores() == null || !filtro.getLancContratoAnteriores()))
					remover = true;
			}else if(FluxocaixaBeanReport.CONTRATO == bean.getTipo()){
				if((filtro.getLancContratoAnteriores() == null || !filtro.getLancContratoAnteriores()))
					remover = true;
			}else if(FluxocaixaBeanReport.DOCUMENTO == bean.getTipo()){
				if((filtro.getLancContapagarAtraso() == null || !filtro.getLancContapagarAtraso()) &&
						(filtro.getLancContareceberAtraso() == null || !filtro.getLancContareceberAtraso()))
					remover = true;
			}
		}
		return remover;
	}
	
	@SuppressWarnings("unchecked")
	private void preparaRelatorioFluxoCaixa(FluxocaixaFiltroReport filtro) {

		Empresa[] empresas = new Empresa[] {};
		if (SinedUtil.isListNotEmpty(filtro.getListaEmpresa())) {
			Object[] objetos = CollectionsUtil.getListProperty(filtro.getListaEmpresa(), "empresa").toArray();
			empresas = new Empresa[objetos.length];
			for (int i = 0; i < objetos.length; i++) {
				empresas[i] = (Empresa) objetos[i];
			}
		} 
		if(empresas.length == 1){
			filtro.setEmpresa(empresas[0]);
		}
		filtro.setListaEmpresa(empresas);

		List<Conta> listConta = new ArrayList<Conta>();
		if (filtro.getConta() != null)
			listConta.add(filtro.getConta());
		if (filtro.getListaContabancaria() != null) {
			for (ItemDetalhe itemDetalhe : filtro.getListaContabancaria()) {
				listConta.add(itemDetalhe.getConta());
			}
		}
		filtro.setListaConta(listConta);

		String whereInNatureza = null;
//		if(SinedUtil.isListNotEmpty(filtro.getListanaturezaContaGerencial()))
//			whereInNatureza = NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial());

		java.sql.Date dtInicioBase = SinedDateUtils.incrementDate(filtro.getPeriodoDe(), -1, Calendar.DAY_OF_MONTH);
		filtro.setDtInicioBase(dtInicioBase);

		Money saldoInicioBancarias = new Money();
		Money saldoInicioCaixas = new Money();
		Money saldoInicioCartoes = new Money();

		Money limiteBancarias = new Money();
		Money limiteCartoes = new Money();
		//if (Util.booleans.isTrue(filtro.getShowMovimentacoes())) {
			// BLOCO RESPONS�VEL POR CALCULAR OS SALDOS DAS CONTAS DO QUADRO
			// RESUMO
			// SALDO INICIAL E FINAL DAS CONTAS BANC�RIAS
			if (SinedUtil.isListNotEmpty(filtro.getListaContabancaria()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
				List<Conta> listaContas = (List<Conta>) CollectionsUtil.getListProperty(filtro.getListaContabancaria(), "conta");
				saldoInicioBancarias = contaService.calculaSaldoAtual(listaContas, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
				limiteBancarias = contaService.findLimiteContas(listaContas, empresas);
			} else
				if (Util.booleans.isTrue(filtro.getRadContbanc()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
					saldoInicioBancarias = contaService.calculaSaldoAtual(Contatipo.TIPO_CONTA_BANCARIA, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
					limiteBancarias = contaService.findLimiteContatipo(Contatipo.TIPO_CONTA_BANCARIA, empresas);
				}
			// CONSIDERAR LIMITE DAS CONTAS BANC�RIAS COMO SALDO
			if (Util.booleans.isTrue(filtro.getConsLimiteContabancSaldo()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
				saldoInicioBancarias = saldoInicioBancarias.add(limiteBancarias);
			}

			// SALDO INICIAL E FINAL DAS CAIXAS
			if (SinedUtil.isListNotEmpty(filtro.getListaCaixa())) {
				saldoInicioCaixas = contaService.calculaSaldoAtual((List<Conta>) CollectionsUtil.getListProperty(filtro
						.getListaCaixa(), "conta"),
								dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
			} else
				if (Util.booleans.isTrue(filtro.getRadCaixa()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
					saldoInicioCaixas = contaService.calculaSaldoAtual(Contatipo.TIPO_CAIXA, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
				}

			// SALDO INICIAL DOS CART�ES DE CR�DITO
			if (SinedUtil.isListNotEmpty(filtro.getListaCartao()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
				List<Conta> listaContas = (List<Conta>) CollectionsUtil.getListProperty(filtro.getListaCartao(), "conta");
				saldoInicioCartoes = contaService.calculaSaldoAtual(listaContas, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
				limiteCartoes = contaService.findLimiteContas(listaContas);
			} else
				if (Util.booleans.isTrue(filtro.getRadCartao()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
					saldoInicioCartoes = contaService.calculaSaldoAtual(Contatipo.TIPO_CARTAO_CREDITO, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true, empresas);
					limiteCartoes = contaService.findLimiteContatipo(Contatipo.TIPO_CARTAO_CREDITO);
				}
			// CONSIDERAR LIMITE DOS CART�ES DE CR�DITO COMO SALDO
			if (Util.booleans.isTrue(filtro.getConsLimiteCartaoSaldo())) {
				saldoInicioCartoes = saldoInicioCartoes.add(limiteCartoes);
			}
		//}
		Money saldoCaixas = new Money(), saldoCartoes = new Money(), saldoContasbanc = new Money();

		// Saldo caixas
		if (filtro.getRadCaixa() != null && Util.booleans.isFalse(filtro.getRadCaixa()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
			// Op��o 'Escolher'
			saldoCaixas = contaService
							.calculaSaldoAtual((List<Conta>) CollectionsUtil.getListProperty(filtro.getListaCaixa(), "conta"), dtInicioBase, true, whereInNatureza, true);
		} else
			if (Util.booleans.isTrue(filtro.getRadCaixa())) {
				// Op��o 'Todos'
				saldoCaixas = new Money(saldoInicioCaixas);
			}

		// Saldo cartoes
		if (filtro.getRadCartao() != null && Util.booleans.isFalse(filtro.getRadCartao()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
			// Op��o 'Escolher'
			Collection<Conta> listaContas = (Collection<Conta>) CollectionsUtil.getListProperty(filtro.getListaCartao(), "conta");
			saldoCartoes = contaService.calculaSaldoAtual((List<Conta>) listaContas, dtInicioBase, true, whereInNatureza, true);

			// Considerar limite de cart�es de cr�dito como saldo
			if (Util.booleans.isTrue(filtro.getConsLimiteCartaoSaldo())) {
				saldoCartoes = saldoCartoes.add(contaService.findLimiteContas(listaContas));
			}

		} else
			if (Util.booleans.isTrue(filtro.getRadCartao())) {
				// Op��o 'Todos'
				saldoCartoes = new Money(saldoInicioCartoes);

				// Considerar limite de cart�es de cr�dito como saldo
				if (Util.booleans.isTrue(filtro.getConsLimiteCartaoSaldo())) {
					saldoCartoes = saldoCartoes.add(contaService.findLimiteContatipo(Contatipo.TIPO_CARTAO_CREDITO));
				}
			}

		// Saldo Contas bancarias
		if (filtro.getRadContbanc() != null && Util.booleans.isFalse(filtro.getRadContbanc()) /*&& Util.booleans.isTrue(filtro.getValoresAnteriores())*/) {
			// Op��o 'Escolher'
			Collection<Conta> listaContas = (Collection<Conta>) CollectionsUtil.getListProperty(filtro.getListaContabancaria(), "conta");
			saldoContasbanc = contaService.calculaSaldoAtual((List<Conta>) listaContas, dtInicioBase, filtro.getMostrarContasInativadas(), whereInNatureza, true);

			// Considerar limite de contas banc�rias como saldo
			if (Util.booleans.isTrue(filtro.getConsLimiteContabancSaldo())) {
				saldoContasbanc = saldoContasbanc.add(contaService.findLimiteContas(listaContas, empresas));
			}
		} else
			if (Util.booleans.isTrue(filtro.getRadContbanc())) {
				// Op��o 'Todos'
				saldoContasbanc = new Money(saldoInicioBancarias);

				// Considerar limite de contas banc�rias como saldo
				if (Util.booleans.isTrue(filtro.getConsLimiteContabancSaldo())) {
					saldoContasbanc = saldoContasbanc.add(contaService.findLimiteContatipo(Contatipo.TIPO_CONTA_BANCARIA, empresas));
				}
			}

		Money saldoAC = saldoCaixas.add(saldoCartoes.add(saldoContasbanc));
//		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			filtro.setSaldoInicioBancarias(saldoContasbanc);
			filtro.setSaldoInicioCaixas(saldoCaixas);
			filtro.setSaldoInicioCartoes(saldoCartoes);
//		}
		filtro.setSaldoAC(saldoAC);
	}
	
	/**
	 * M�todo quase id�ntico ao de cima. Difere�a que o de cima gera arquivo PDF
	 * e o esse CSV
	 * 
	 * @param filtro
	 * @return
	 */
	public Resource createReportFluxocaixaCSV(FluxocaixaFiltroReport filtro) {
		
		preparaRelatorioFluxoCaixa(filtro);

		filtro.setRadEvento(false);// Seta esse valor para aparecer sempre a
									// descri��o
		filtro.setMostrarDetalhes(true);
		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		// this.calculaCreditosDebitos(listaFluxo, report);

		Saldos saldos = new Saldos(filtro.getSaldoInicioBancarias(), filtro.getSaldoInicioCaixas(), filtro.getSaldoInicioCartoes());
		this.calculaSaldoInicialOutros(filtro, listaFluxo);
		this.ajustaSaldoAC(filtro, listaFluxo);
		this.somaSaldos(filtro, listaFluxo, saldos);
		listaFluxo = this.separaContagerencial(listaFluxo);
		listaFluxo = this.agrupaListaHeader(listaFluxo, filtro.getSaldoAC(), Util.booleans.isTrue(filtro.getMostrarDetalhes()), filtro.getValoresAnteriores(), filtro, false, false);
		listaFluxo = this.ajustaListaFluxoCaixaValoresAnteriores(filtro, listaFluxo);
		this.insereLinhaTotal(listaFluxo);
		String marcacaoSaldoInicial = filtro.getSaldoACTela() != null? "* ": "";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder csv;
		if (filtro.getShowMovimentacoes())
			csv = new StringBuilder("Data;Descri��o;Pessoa;Cr�ditos;D�bitos;Saldo D;"+marcacaoSaldoInicial+"Saldo AC;Conta;Documento;Conta Gerencial;Projeto(s);\n");
		else
			csv = new StringBuilder("Data;Descri��o;Pessoa;Cr�ditos;D�bitos;Saldo D;"+marcacaoSaldoInicial+"Saldo AC;Conta Gerencial;Projeto(s);\n");
		for (int i = 0; i < listaFluxo.size(); i++) {
			FluxocaixaBeanReport fluxocaixaBeanReport = listaFluxo.get(i);
			csv.append(SinedDateUtils.remoteDate().getTime() == fluxocaixaBeanReport.getData().getTime() ? ";" : format.format(fluxocaixaBeanReport.getData()) + ";")
				.append("\""+fluxocaixaBeanReport.getDescricao()+"\"").append(";")
				.append(fluxocaixaBeanReport.getPessoadescricao() != null ? fluxocaixaBeanReport.getPessoadescricao() : "").append(";")
				.append(fluxocaixaBeanReport.getCredito() == null ? "" : fluxocaixaBeanReport.getCredito()).append(";")
				.append(fluxocaixaBeanReport.getDebito() == null ? "" : fluxocaixaBeanReport.getDebito()).append(";")
				.append(fluxocaixaBeanReport.getSaldoD() == null ? "" : fluxocaixaBeanReport.getSaldoD()).append(";")
				.append((i != 0 ? fluxocaixaBeanReport.getSaldoAC() == null ? "" : fluxocaixaBeanReport.getSaldoAC() : filtro.getSaldoAC()) ).append(";");

			if (filtro.getShowMovimentacoes()) {
				csv.append(fluxocaixaBeanReport.getConta() == null || fluxocaixaBeanReport.getConta().getCdconta() == null ? "" : fluxocaixaBeanReport.getConta()).append(";");
				csv.append(fluxocaixaBeanReport.getDocumento() == null  ? "" : fluxocaixaBeanReport.getDocumento()).append(";");
			}

			csv.append(fluxocaixaBeanReport.getContagerencial() == null ? "" : fluxocaixaBeanReport.getContagerencial()).append(";");
			csv.append(fluxocaixaBeanReport.getProjeto() == null ? "" : fluxocaixaBeanReport.getProjeto()).append(";").append("\n");
		}
		
		csv.append("\n\n" + observacoesFluxoCaixa(filtro.getSaldoACTela() != null));

		Resource resource = new Resource("text/csv", "fluxocaixa_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public Resource createReportFluxocaixaCSVOrigem(FluxocaixaFiltroReport filtro) {
		
		if (filtro.getMostrarDetalhes() != null && filtro.getMostrarDetalhes()) {
			return createReportFluxocaixaCSVAnaliticoOrigem(filtro);
		} else {
			preparaRelatorioFluxoCaixa(filtro);
	
			List<FluxocaixaOrigemBeanReport> listaFluxo = montaListaDadosRelatorioOrigem(this.montaListaDadosRelatorio(filtro), filtro.getSaldoAC());
			String marcacaoSaldoInicial = filtro.getSaldoACTela() != null? "* ": "";
			StringBuilder csv = new StringBuilder("Data;CR;CHQ;CC;CT;AC;MNC;MCC;DPC;PV;CP;OC;AD;CF;DPD;MND;MCD;SALDO DIA;"+marcacaoSaldoInicial+"ACUMULADO\n");
			if(SinedUtil.isListNotEmpty(listaFluxo)){
				FluxocaixaOrigemBeanReport beanTotal = criaFluxocaixaOrigemBeanReportTotal(listaFluxo);
	
				for (int i = 0; i < listaFluxo.size(); i++) {
					inserirLinha(csv, listaFluxo.get(i));
					csv.append("\n");
				}
				inserirLinha(csv, beanTotal);
			}
			
			csv.append("\n\n" + observacoesFluxoCaixa(filtro.getSaldoACTela() != null));
			listaFluxo = null;
	
			Resource resource = new Resource("text/csv", "fluxocaixa_origem_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
			return resource;
		}
	}

	private Resource createReportFluxocaixaCSVAnaliticoOrigem(FluxocaixaFiltroReport filtro) {
		preparaRelatorioFluxoCaixa(filtro);
		
		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		ajustaListaFluxocaixaAnalitoOrigem(listaFluxo);

		Saldos saldos = new Saldos(filtro.getSaldoInicioBancarias(), filtro.getSaldoInicioCaixas(), filtro.getSaldoInicioCartoes());

		this.calculaSaldoInicialOutros(filtro, listaFluxo);
		this.ajustaSaldoAC(filtro, listaFluxo);
		this.somaSaldos(filtro, listaFluxo, saldos);

		listaFluxo = this.agrupaListaHeader(listaFluxo, filtro.getSaldoAC(), Util.booleans.isTrue(filtro.getMostrarDetalhes()), filtro.getValoresAnteriores(), filtro, true, true);


		listaFluxo = this.ajustaListaFluxoCaixaValoresAnteriores(filtro, listaFluxo);
		
		String marcacaoSaldoInicial = filtro.getSaldoACTela() != null? "* ": "";
		StringBuilder csv = new StringBuilder("Data;;PESSOA;HIST�RICO;CR�DITOS;D�BITOS;SALDO DIA;"+marcacaoSaldoInicial+"ACUMULADO\n");
		if(SinedUtil.isListNotEmpty(listaFluxo)){
			FluxocaixaBeanReport beanTotal = criaFluxocaixaAnalitOrigemBeanReportTotal(listaFluxo);

			for (int i = 0; i < listaFluxo.size(); i++) {
				inserirLinhaAnalitico(csv, listaFluxo.get(i));
				csv.append("\n");
			}
			inserirLinhaAnalitico(csv, beanTotal);
		}
		
		csv.append("\n\n" + observacoesFluxoCaixa(filtro.getSaldoACTela() != null));
		listaFluxo = null;
		
		Resource resource = new Resource("text/csv", "fluxocaixa_analitico_origem_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	private FluxocaixaBeanReport criaFluxocaixaAnalitOrigemBeanReportTotal(List<FluxocaixaBeanReport> listaFluxo) {
		Double total_credito = 0d;
		Double total_debito = 0d;
		
		Double total_saldo_dia = 0d;
		Money total_saldo_acumulado = new Money();
		
		if(SinedUtil.isListNotEmpty(listaFluxo)){
			total_saldo_acumulado = listaFluxo.get(listaFluxo.size()-1).getSaldoAC();
		
			for(FluxocaixaBeanReport bean : listaFluxo){
				if(!bean.isHeader() && !bean.isHeaderOrigem()){
					if(bean.getCredito() != null){
						total_credito += bean.getCredito().getValue().doubleValue();
					}
					if(bean.getDebito() != null){
						total_debito += bean.getDebito().getValue().doubleValue();
					}
				}
				
				if(bean.getSaldoD() != null) total_saldo_dia += bean.getSaldoD().getValue().doubleValue();
			}
		}
			
		FluxocaixaBeanReport beanTotal = new FluxocaixaBeanReport();
		beanTotal.setCredito(new Money(total_credito));
		beanTotal.setDebito(new Money(total_debito));
		
		beanTotal.setSaldoD(new Money(total_saldo_dia));
		beanTotal.setSaldoAC(new Money(total_saldo_acumulado));
		
		return beanTotal;
	}
	
	private void inserirLinhaAnalitico(StringBuilder csv, FluxocaixaBeanReport bean) {
		if(bean.isTotalizador()){
			csv.append("TOTAIS;");
			csv.append(";;;");
		}else {
			csv.append(bean.getData() == null || SinedDateUtils.remoteDate().getTime() == bean.getData().getTime() || (!bean.isHeader() && !bean.isHeaderOrigem()) ? 
					";" : SinedDateUtils.toString(bean.getData()) + ";");
			
			if(bean.isHeader() || bean.isHeaderOrigem()){
				csv.append("\""+bean.getDescricao()+"\"").append(";");
				csv.append(";");
				csv.append(";");
			}else {
				csv.append("\""+(bean.getIndicadorOrigem() != null ? bean.getIndicadorOrigem() : "")+"\"").append(";");
				csv.append("\""+(bean.getPessoadescricao() != null ? bean.getPessoadescricao() : "")+"\"").append(";");
				csv.append("\""+(bean.getDescricao() != null ? bean.getDescricao() : "")+"\"").append(";");
			}
		}
		
		csv.append("\""+(bean.getCredito() != null ? bean.getCredito() : "")+"\"").append(";");
		csv.append("\""+(bean.getDebito() != null ? bean.getDebito() : "")+"\"").append(";");
		
		csv.append("\""+(bean.getSaldoD() != null ? bean.getSaldoD() : "")+"\"").append(";");
		csv.append("\""+(bean.getSaldoAC() != null ? bean.getSaldoAC() : "")+"\"").append(";");
		
	}
	private void inserirLinha(StringBuilder csv, FluxocaixaOrigemBeanReport bean) {
		if(bean.isTotalizador()){
			csv.append("TOTAIS;");
		}else {
			csv.append(bean.getData() == null || SinedDateUtils.remoteDate().getTime() == bean.getData().getTime() ? ";" : SinedDateUtils.toString(bean.getData()) + ";");
		}
		
		csv.append("\""+bean.getVl_credito_cr()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_chq()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_cc()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_ct()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_ac()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_mnc()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_mcc()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_pv()+"\"").append(";");
		csv.append("\""+bean.getVl_credito_dpc()+"\"").append(";");
		
		csv.append("\""+bean.getVl_debito_cp()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_oc()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_ad()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_cf()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_mnd()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_mcd()+"\"").append(";");
		csv.append("\""+bean.getVl_debito_dpd()+"\"").append(";");
		
		csv.append("\""+bean.getVl_saldo_dia()+"\"").append(";");
		csv.append("\""+bean.getVl_saldo_acumulado()+"\"").append(";");
	}
	
	/**
	 * M�todo quase id�ntico ao de cima. Difere�a que faz o agrupamento mensal
	 * por conta gerencial
	 * 
	 * @param filtro
	 * @return
	 * @throws IOException 
	 */
	public Resource createReportFluxocaixaMensalCSV(FluxocaixaFiltroReport filtro) throws IOException {
		
		preparaRelatorioFluxoCaixa(filtro);
		boolean isOrcamento = Boolean.TRUE.equals(filtro.getConsControleOrcamentario());
		boolean isContaGerencial = false;
		if ("Mensal por conta gerencial (Formato CSV)".equals(filtro.getTipo().getNome())) {
			isContaGerencial = true;
		}
		filtro.setRadEvento(false);// Seta esse valor para aparecer sempre a descri��o
		filtro.setMostrarDetalhes(true);
		filtro.setAgrupamentoMensal(true);
		
		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		
		//Busca todo o controle de orcamento no filtro
		Controleorcamento controleOrcamento = filtro.getControleOrcamentario() != null ? controleOrcamentoService.buscarValorFluxoCaixa(filtro.getControleOrcamentario()) : null;

//		Money saldocontas = contaService.calculaSaldoAtual(filtro.getListConta(), filtro.getDtInicioBase(), filtro.getMostrarContasInativadas(), filtro.getEmpresas());
//		this.ajustaSaldoAC(filtro, listaFluxo);
		Money saldocontas = filtro.getSaldoAC() != null ? filtro.getSaldoAC() : new Money();
		saldocontas = this.somaSaldosCSV(filtro, listaFluxo, saldocontas, filtro.getDtInicioBase());

		List<FluxocaixaMensalReportBean> listaFluxoMensal = this.agrupaFluxoCaixaMensal(filtro, listaFluxo, controleOrcamento, isContaGerencial);
		listaFluxoMensal = this.totalizaSaldoContaGerencial(listaFluxoMensal, SinedDateUtils.mesesEntreInicioFim(filtro.getPeriodoDe(), filtro.getPeriodoAte()) + 1);			
		
		listaFluxoMensal = this.addTotalFluxoCaixaMensal(filtro, listaFluxoMensal, saldocontas, isContaGerencial);
		
		//se for relat�rio mensal por conta gerencial vai seguir esse processo, se for mensal por proje��o, muda o retorno no fim desse m�todo
		if(isContaGerencial){			
			
			StringBuilder csv;
			csv = new StringBuilder("Conta gerencial;");
			csv.append("Tipo opera��o;");
			
			if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
				csv.append("Valores Anteriores;");
			}
			for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : listaFluxoMensal.get(0).getListaMensal()) {
				csv.append(fluxocaixaMensalSubReportBean.getDescricao()).append(";");
				if(isOrcamento){
					csv.append("Or�amento " + fluxocaixaMensalSubReportBean.getDescricao()).append(";");
					csv.append("Diferen�a " + fluxocaixaMensalSubReportBean.getDescricao()).append(";");
				}
			}
			csv.append("\n");
			
			HashMap<String, Money> mapMesAnoValor = new HashMap<String, Money>();
			
			for (int i = 0; i < listaFluxoMensal.size(); i++) {
				Money totalOrcamento = new Money(0.0);
				Money diferencaTotalOrcamento = new Money(0.0);
				FluxocaixaMensalReportBean fluxocaixaMensalReportBean = listaFluxoMensal.get(i);
				
				boolean possuiLancamento = true;
				if(filtro.getMostrarContasgerenciaisSemLanc() == null || filtro.getMostrarContasgerenciaisSemLanc().equals(Boolean.FALSE)){
					if(fluxocaixaMensalReportBean.getValoresAnteriores() == null || fluxocaixaMensalReportBean.getValoresAnteriores().getValue().doubleValue() == 0 ||
							(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() &&
							(fluxocaixaMensalReportBean.getMostrarValorAnterior() == null || 
							!fluxocaixaMensalReportBean.getMostrarValorAnterior()))){
						possuiLancamento = false;
						for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaMensal()) {
							if (fluxocaixaMensalSubReportBean.getValor() != null && fluxocaixaMensalSubReportBean.getValor().getValue().doubleValue() != 0)
								possuiLancamento = true;
							if(possuiLancamento) break;
						}
						Integer count = 1;
						for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaTotalMensal()) {
							if (fluxocaixaMensalSubReportBean.getValor() != null && count < fluxocaixaMensalReportBean.getListaTotalMensal().size())
								possuiLancamento = true;
							if(possuiLancamento) break;
						}
					}
				}
				
				if(possuiLancamento){
					csv.append(fluxocaixaMensalReportBean.getIdentificador());
					if (fluxocaixaMensalReportBean.getNome() != null)
						csv.append(" - ").append(fluxocaixaMensalReportBean.getNome());
					csv.append(";");
					// TIPO OPERA��O
					if (fluxocaixaMensalReportBean.getTipooperacao() != null) {
						if (fluxocaixaMensalReportBean.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
							csv.append("D�BITO");
						} else {
							csv.append("CR�DITO");
						}
						csv.append(";");
					} else {
						csv.append(";");
					}
					if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
						csv.append(fluxocaixaMensalReportBean.getValoresAnteriores() != null && 
								((fluxocaixaMensalReportBean.getMostrarValorAnterior() != null && 
								fluxocaixaMensalReportBean.getMostrarValorAnterior()) ||
								i == listaFluxoMensal.size()-1)? 
										fluxocaixaMensalReportBean.getValoresAnteriores().toString() : "0,00");
						csv.append(";");
					}
					for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaMensal()) {
						if (fluxocaixaMensalSubReportBean.getValor() != null) {
							csv.append(fluxocaixaMensalSubReportBean.getValor().toString());
							if(isOrcamento && fluxocaixaMensalSubReportBean.getAno() != null && fluxocaixaMensalSubReportBean.getMes() != null){
								csv.append(";");
								Money valorOrcado = new Money();
								if("Total".equals(fluxocaixaMensalReportBean.getIdentificador())){
									valorOrcado = mapMesAnoValor.get(fluxocaixaMensalSubReportBean.getMesAno());
								}else {
									valorOrcado = buscarValorOrcado(filtro, fluxocaixaMensalReportBean.getNome(), fluxocaixaMensalSubReportBean, fluxocaixaMensalReportBean.getIdentificador(),null,null, controleOrcamento);
									Money valorTotalMesAno = mapMesAnoValor.get(fluxocaixaMensalSubReportBean.getMesAno());
									if(valorTotalMesAno == null) valorTotalMesAno = new Money();
									if(valorOrcado != null){
										if(Tipooperacao.TIPO_CREDITO.equals(fluxocaixaMensalReportBean.getTipooperacao())){
											valorTotalMesAno = valorTotalMesAno.add(valorOrcado);
										}else if(Tipooperacao.TIPO_DEBITO.equals(fluxocaixaMensalReportBean.getTipooperacao())){
											valorTotalMesAno = valorTotalMesAno.subtract(valorOrcado);
										}
									}
									mapMesAnoValor.put(fluxocaixaMensalSubReportBean.getMesAno(), valorTotalMesAno);
								}
								csv.append(valorOrcado != null ? valorOrcado.toString() : "").append(";");
								Money diferenca = valorOrcado != null ? new Money(valorOrcado.subtract(fluxocaixaMensalSubReportBean.getValor())) : fluxocaixaMensalSubReportBean.getValor();
								csv.append(diferenca.toString());
									
								totalOrcamento = totalOrcamento.add(valorOrcado);
								diferencaTotalOrcamento = diferencaTotalOrcamento.add(diferenca);
							}																			
							
						}
						if(i < listaFluxoMensal.size()-1){
							csv.append(";");
						}
						
					}
					
					if(isOrcamento && !"Acumulado".equalsIgnoreCase(fluxocaixaMensalReportBean.getIdentificador())){	
						csv.append(totalOrcamento.toString()).append(";");
						csv.append(diferencaTotalOrcamento.toString()).append(";");
					}
					
					Integer count = 1;
					for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaTotalMensal()) {
						if (fluxocaixaMensalSubReportBean.getValor() != null && count <= fluxocaixaMensalReportBean.getListaTotalMensal().size()){
							csv.append(fluxocaixaMensalSubReportBean.getValor().toString()).append(";");
							if(isOrcamento){
								csv.append(totalOrcamento.toString()).append(";");
								csv.append(diferencaTotalOrcamento.toString()).append(";");
							}
						}
						count++;
					}
					csv.append("\n");
				}
			}
			
			csv.append("\n\n" + observacoesFluxoCaixa(filtro.getSaldoACTela() != null));
			
			Resource resource = new Resource("text/csv", "fluxocaixamensal_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
			return resource;
		}
		return createReportFluxocaixaPorProjecaoExcel(filtro, listaFluxoMensal);
		} 

	public Resource createReportFluxocaixaPorProjecaoExcel(FluxocaixaFiltroReport filtro, List<FluxocaixaMensalReportBean> listaFluxoMensal) throws IOException {		
		
		SinedExcel wb = new SinedExcel();	
		
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 15);
        planilha.setColumnWidth((short) 0, ((short)(35*400)));
        
        HSSFRow headerRow = planilha.createRow((short) 0);
		planilha.addMergedRegion(new Region(0, (short) 0, 2, (short) ((short) listaFluxoMensal.get(1).getListaTotalMensal().size()+2)));
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Relat�rio de fluxo de caixa por proje��o");
		
		HSSFRow row = null;
		HSSFCell cell = null;  
		
		row = planilha.createRow(4);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Conta Gerencial");
		
		cell = row.createCell((short) 1);
		planilha.setColumnWidth((short)1, (short)1400);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("OP");
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			cell = row.createCell((short) 2);
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue("Valores Anteriores");
		}
		
		//meses e total mes cabe�alho
		for (int i = 0; i < listaFluxoMensal.get(0).getListaMensal().size(); i++) {
			FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean = listaFluxoMensal.get(0).getListaMensal().get(i);
			if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
				cell = row.createCell((short) ((short) i+3));				
			}else {
				cell = row.createCell((short) ((short) i+2));
			}
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue(fluxocaixaMensalSubReportBean.getDescricao());
		}

		for (int i = 0; i < listaFluxoMensal.size(); i++) {
			FluxocaixaMensalReportBean fluxocaixaMensalReportBean = listaFluxoMensal.get(i);
			
			boolean possuiLancamento = true;
			if(filtro.getMostrarContasgerenciaisSemLanc() == null || filtro.getMostrarContasgerenciaisSemLanc().equals(Boolean.FALSE)){
				if(fluxocaixaMensalReportBean.getValoresAnteriores() == null || fluxocaixaMensalReportBean.getValoresAnteriores().getValue().doubleValue() == 0 ||
						(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() &&
									(fluxocaixaMensalReportBean.getMostrarValorAnterior() == null || 
									!fluxocaixaMensalReportBean.getMostrarValorAnterior()))){
					possuiLancamento = false;
					for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaMensal()) {
						if (fluxocaixaMensalSubReportBean.getValor() != null && fluxocaixaMensalSubReportBean.getValor().getValue().doubleValue() != 0)
							possuiLancamento = true;
						if(possuiLancamento) break;
					}
					Integer count = 1;
					for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaTotalMensal()) {
						if (fluxocaixaMensalSubReportBean.getValor() != null && count < fluxocaixaMensalReportBean.getListaTotalMensal().size())
							possuiLancamento = true;
						if(possuiLancamento) break;
					}
				}
			}
			
			//pai (despesa receita NIVEL 1)
			if (possuiLancamento && fluxocaixaMensalReportBean.getNome() != null) {
				row = planilha.createRow(planilha.getLastRowNum() + 1);
				cell = row.createCell((short) 0);
				if (fluxocaixaMensalReportBean.getNivel() == 1){
					cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);					
				}else {
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
				}
				cell.setCellValue(fluxocaixaMensalReportBean.getIdentificador() + " - " + fluxocaixaMensalReportBean.getNome());
				
				//credito  debito
				if (fluxocaixaMensalReportBean.getTipooperacao() != null) {
					cell = row.createCell((short) 1);
					if (fluxocaixaMensalReportBean.getNivel() == 1){
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
					}else {
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					}
					if (fluxocaixaMensalReportBean.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
						cell.setCellValue("D");
					}else {
						cell.setCellValue("C");
					}
				}
				
				//valores anteriores
				if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
					cell = row.createCell((short) 2);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					if (fluxocaixaMensalReportBean.getValoresAnteriores() != null && 
							((fluxocaixaMensalReportBean.getMostrarValorAnterior() != null && 
							fluxocaixaMensalReportBean.getMostrarValorAnterior()) ||
							i == listaFluxoMensal.size()-1)) {
						cell.setCellValue(fluxocaixaMensalReportBean.getValoresAnteriores().toString());
					}else {
						cell.setCellValue("0,00");
					}
				}
				
				// valores mensais (linha)
				for (int j = 0; j < fluxocaixaMensalReportBean.getListaMensal().size(); j++){
					FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean = fluxocaixaMensalReportBean.getListaMensal().get(j);
					if (fluxocaixaMensalSubReportBean.getValor() != null) {
						if (filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
							cell = row.createCell((short) ((short) j+3));						
						} else {
							cell = row.createCell((short) ((short) j+2));
						}
						if (fluxocaixaMensalReportBean.getNivel() == 1 || "Total".equals(fluxocaixaMensalSubReportBean.getDescricao())){
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
						}else if (fluxocaixaMensalSubReportBean.getValorAlteradoOrcamento() != null && fluxocaixaMensalSubReportBean.getValorAlteradoOrcamento()){
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_YELLOW_MONEY);
						}else if (fluxocaixaMensalReportBean.getUltimoNivel() == null){
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_GREY);
						}else {
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);							
						}
						cell.setCellValue(fluxocaixaMensalSubReportBean.getValor().toString());
					}
				}						
			}		
			
			// 3 ultimas linhas
			if ("Total".equals(listaFluxoMensal.get(i).getIdentificador())){
				row = planilha.createRow(planilha.getLastRowNum() + 1);
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				cell.setCellValue("Total m�s");
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				
				for (int j = 0; j < fluxocaixaMensalReportBean.getListaMensal().size(); j++){
					FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean = fluxocaixaMensalReportBean.getListaMensal().get(j);
					if (fluxocaixaMensalSubReportBean.getValor() != null) {
						if (filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
							cell = row.createCell((short) ((short) j+3));						
						} else {
							cell = row.createCell((short) ((short) j+2));
						}
						if (fluxocaixaMensalSubReportBean.getValor().getValue().doubleValue() < 0){
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY_FONT_RED); 
						}else {
							cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);							
						}
						cell.setCellValue(fluxocaixaMensalSubReportBean.getValor().toString());
					}
				}				
			}
			if ("Saldo Anterior".equals(listaFluxoMensal.get(i).getIdentificador())){
				row = planilha.createRow(planilha.getLastRowNum() + 1);
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				cell.setCellValue("Saldo anterior");
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				
				int contador = 0;
				
				for (int j = 0; j < fluxocaixaMensalReportBean.getListaTotalMensal().size(); j++){
					FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean = fluxocaixaMensalReportBean.getListaTotalMensal().get(j);
					if (fluxocaixaMensalSubReportBean.getValor() != null) {
						if (filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
							cell = row.createCell((short) ((short) contador+3));						
						} else {
							cell = row.createCell((short) ((short) contador+2));
						}
						cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
						cell.setCellValue(fluxocaixaMensalSubReportBean.getValor().toString());
						contador ++;
					}
				}
				
			}
			if ("Acumulado".equals(listaFluxoMensal.get(i).getIdentificador())){
				row = planilha.createRow(planilha.getLastRowNum() + 1);
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				cell.setCellValue("Acumulado");
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT);
				
				int contador = 0;
				
				for (int j = 0; j < fluxocaixaMensalReportBean.getListaTotalMensal().size(); j++){
					FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean = fluxocaixaMensalReportBean.getListaTotalMensal().get(j);
					if (fluxocaixaMensalSubReportBean.getValor() != null) {
						if (filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
							cell = row.createCell((short) ((short) contador+3));						
						} else {
							cell = row.createCell((short) ((short) contador+2));
						}
						cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
						cell.setCellValue(fluxocaixaMensalSubReportBean.getValor().toString());
						contador++;
					}
				}
			}
		}
		
		row = planilha.createRow(planilha.getLastRowNum() + 2);
		cell = row.createCell((short) 0);
		cell.setCellValue("* Para c�lculo do saldo inicial � considerada todas as movimenta��es independente da natureza de conta gerencial informada no filtro.");
		
		row = planilha.createRow(planilha.getLastRowNum() + 1);
		cell = row.createCell((short) 0);
		cell.setCellValue("* O saldo final pode ariar de acordo com o filtro informado na natureza de conta gerencial");
		
		row = planilha.createRow(planilha.getLastRowNum() + 1);
		cell = row.createCell((short) 0);
		cell.setCellValue("* C�lulas sombreadas de amarelo s�o valores provenientes do controle or�ament�rio.");
		
		return wb.getWorkBookResource("acompanhamentotarefa.xls");			
	}
		
	private boolean validarListaParaValor(List<Controleorcamentoitem> listaItensOrcamento,String contaGerencia, Integer cdCentroCusto){
		if(listaItensOrcamento.size() == 1 && listaItensOrcamento.get(0).getContagerencial()!=null &&listaItensOrcamento.get(0).getContagerencial().getNome().equals(contaGerencia)){
			return true;
		} else if(listaItensOrcamento.size() == 1 && listaItensOrcamento.get(0).getCentrocusto()!=null &&listaItensOrcamento.get(0).getCentrocusto().getCdcentrocusto().equals(cdCentroCusto)){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	private Money buscarValorOrcado(FluxocaixaFiltroReport filtro, String contaGerencia,FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean,String identificador,Integer cdCentroCusto, Tipooperacao tipooperacao, Controleorcamento orcamento) {
		Date dataItem = new Date(fluxocaixaMensalSubReportBean.getAno() - 1900, fluxocaixaMensalSubReportBean.getMes() - 1, 01);
		Money valorRetorno = new Money(0.0);
		
		if(orcamento == null){
			return new Money(0.0);
		}
		
		Integer mesesPerido = orcamento.getQtnMesesExercicio();
		if(orcamento != null){					
			
			List<Controleorcamentoitem> listaItensOrcamento = getListaControleOrcamentoItem(filtro, contaGerencia, fluxocaixaMensalSubReportBean, identificador, cdCentroCusto, tipooperacao, orcamento);
			
			if(Tipolancamento.ANUAL.equals(orcamento.getTipolancamento())){
				boolean validado = validarListaParaValor(listaItensOrcamento,contaGerencia,cdCentroCusto);
				
				if(SinedUtil.isListNotEmpty(listaItensOrcamento)){
					if(validado){ 
						Controleorcamentoitem item = listaItensOrcamento.get(0);
						if(item.getMesano() == null){
							return item.getValor().divide(new Money(mesesPerido));
						}else{
							if(dataItem.equals(item.getMesano())){
								return item.getValor();
							}
						}
					}else{
						for (Controleorcamentoitem item : listaItensOrcamento) {
							if(item.getMesano() == null){
								valorRetorno = valorRetorno.add(item.getValor().divide(new Money(mesesPerido)));  //Money(12.00)));
							}else{
								if(dataItem.equals(item.getMesano())){
									return item.getValor();
								}
							}
						}
						return valorRetorno;
					}
				}
			} else if(Tipolancamento.MENSAL.equals(orcamento.getTipolancamento()) && SinedUtil.isListNotEmpty(listaItensOrcamento)){
				Money valorFinal = new Money(0.0);
				for (Controleorcamentoitem item : listaItensOrcamento) {
					if(dataItem.equals(item.getMesano())){
						valorFinal = valorFinal.add(item.getValor());
					}
				}
				return valorFinal;
			}
		}
		return valorRetorno;
	}
	
	
	private List<Controleorcamentoitem> getListaControleOrcamentoItem(FluxocaixaFiltroReport filtro, String contaGerencia, FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean, String identificador, Integer cdCentroCusto, Tipooperacao tipooperacao, Controleorcamento orcamento) {
		List<Controleorcamentoitem> listaControleOrcamentoItens = new ArrayList<Controleorcamentoitem>();	
		boolean identificadorIgual = false;
		boolean centroCustoIgual = false;
		boolean tipoOperacaoIgual = false;
		
		for (Controleorcamentoitem controleOrcamentoItem : orcamento.getListaControleorcamentoitem()) {
			identificadorIgual = false;
			centroCustoIgual = false;
			tipoOperacaoIgual = false;
			
			if(StringUtils.isBlank(identificador) || (controleOrcamentoItem.getContagerencial() != null 
													  && controleOrcamentoItem.getContagerencial().getVcontagerencial() != null 
													  && identificador.equals(controleOrcamentoItem.getContagerencial().getVcontagerencial().getIdentificador()))){
				identificadorIgual = true;
			}
			if (cdCentroCusto == null || (controleOrcamentoItem.getCentrocusto() != null 
													  && controleOrcamentoItem.getCentrocusto().getCdcentrocusto() != null
													  && cdCentroCusto.equals(controleOrcamentoItem.getCentrocusto().getCdcentrocusto())) || controleOrcamentoItem.getCentrocusto().getCdcentrocusto() == null){
				centroCustoIgual = true;				
			}	
			if (tipooperacao == null || (controleOrcamentoItem.getContagerencial() != null 
													  && controleOrcamentoItem.getContagerencial().getTipooperacao() != null
													  && tipooperacao.equals(controleOrcamentoItem.getContagerencial().getTipooperacao()))) {
				tipoOperacaoIgual = true;
			}
			if (identificadorIgual && centroCustoIgual && tipoOperacaoIgual) {
				listaControleOrcamentoItens.add(controleOrcamentoItem);
			}
		}	
		return listaControleOrcamentoItens;
	}
	
	
	private Money somaSaldosCSV(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo, Money saldocontas, java.sql.Date dtInicioBase) {
		boolean considerarContrato = filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores();
		boolean considerarAgendamento = filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores();
		boolean considerarDocumento = (filtro.getLancContapagarAtraso() != null && filtro.getLancContapagarAtraso()) ||
										(filtro.getLancContareceberAtraso() != null && filtro.getLancContareceberAtraso());
		
		if(saldocontas == null) saldocontas = new Money();
		if(SinedUtil.isListNotEmpty(listaFluxo) && dtInicioBase != null){
			for(FluxocaixaBeanReport beanReport : listaFluxo){
				if(((beanReport.getTipo() == FluxocaixaBeanReport.DOCUMENTO && considerarDocumento) ||
						(beanReport.getTipo() == FluxocaixaBeanReport.CONTRATO && considerarContrato) ||
						(beanReport.getTipo() == FluxocaixaBeanReport.AGENDAMENTO && considerarAgendamento)) &&
						beanReport.getData() != null && 
						SinedDateUtils.beforeIgnoreHour(new java.sql.Date(beanReport.getData().getTime()), dtInicioBase)){
					if(beanReport.getCredito() != null){
						saldocontas = saldocontas.add(beanReport.getCredito());
					}else if(beanReport.getDebito() != null){ 
						saldocontas = saldocontas.subtract(beanReport.getDebito());
					}
					
				}
			}
		}
		return saldocontas;
	}
	
	private List<FluxocaixaMensalReportBean> agrupaFluxoCaixaMensal(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo, Controleorcamento controleOrcamento, boolean isContaGerencial) {
		Map<Integer, FluxocaixaMensalReportBean> mapContaGerencial = new HashMap<Integer, FluxocaixaMensalReportBean>();
		
		List<Contagerencial> listaContagerencial = contagerencialService.findForFluxoCaixaMensal(filtro.getListanaturezaContaGerencial());
		for (Contagerencial cg : listaContagerencial) {
			mapContaGerencial.put(cg.getCdcontagerencial(), new FluxocaixaMensalReportBean(cg, filtro.getPeriodoDe(), filtro.getPeriodoAte()));
		}

		for (FluxocaixaBeanReport fc : listaFluxo) {
			if (fc.getListaRateioitem() != null && fc.getListaRateioitem().size() > 0) {
				for (Rateioitem ri : fc.getListaRateioitem()) {
					if (mapContaGerencial.containsKey(ri.getContagerencial().getCdcontagerencial())) {
						FluxocaixaMensalReportBean bean = mapContaGerencial.get(ri.getContagerencial().getCdcontagerencial());
						bean.setUltimoNivel(true);
						
						Integer mes = SinedDateUtils.mesesEntreInicio(filtro.getPeriodoDe(), new java.sql.Date(fc.getData().getTime()));
						if(fc.getData().before(filtro.getPeriodoDe())){
							Money valoresAnteriores = bean.getValoresAnteriores();
							if (valoresAnteriores == null)
								valoresAnteriores = new Money(0);
							
							if((fc.getTipo() == FluxocaixaBeanReport.MOVIMENTACAO && 
									filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores())|| 
									(fc.getTipo() == FluxocaixaBeanReport.AGENDAMENTO && 
									filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores())||
									(fc.getTipo() == FluxocaixaBeanReport.CONTRATO && 
									filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores())||
									(fc.getTipo() == FluxocaixaBeanReport.DOCUMENTO && (
									(filtro.getLancContapagarAtraso() != null && filtro.getLancContapagarAberto()) ||
									(filtro.getLancContareceberAtraso() != null && filtro.getLancContareceberAtraso())))){
								valoresAnteriores = valoresAnteriores.add(ri.getValor());
								bean.setValoresAnteriores(valoresAnteriores);
								bean.setMostrarValorAnterior(true);
							}
						}else {
							if (bean.getListaMensal().size() > mes) {
								Money valor = bean.getListaMensal().get(mes).getValor();
								if (valor == null)
									valor = new Money(0);
								valor = valor.add(ri.getValor());
								bean.getListaMensal().get(mes).setValor(valor);
								bean.setPossuiLancamento(Boolean.TRUE);
							}
						}
					}
				}
			}
		}
		//quando for relat�rio mensal por proje��o, compara o valor e o valor or�ado e seta o que for maior
		if (!isContaGerencial){		
			for (FluxocaixaBeanReport fc : listaFluxo) {
				if (fc.getListaRateioitem() != null && fc.getListaRateioitem().size() > 0) {
					for (Rateioitem ri : fc.getListaRateioitem()) {
						if (mapContaGerencial.containsKey(ri.getContagerencial().getCdcontagerencial())) {
							FluxocaixaMensalReportBean bean = mapContaGerencial.get(ri.getContagerencial().getCdcontagerencial());
							
							Integer mes = SinedDateUtils.mesesEntreInicio(filtro.getPeriodoDe(), new java.sql.Date(fc.getData().getTime()));
							if(!fc.getData().before(filtro.getPeriodoDe())){
								if (bean.getListaMensal().size() > mes) {
									Money valor = bean.getListaMensal().get(mes).getValor();
									if (valor == null)
										valor = new Money(0);
									
										Money valorOrcado = buscarValorOrcado(filtro, bean.getNome(), bean.getListaMensal().get(mes), bean.getIdentificador(), null, null, controleOrcamento);
										Date dataAtual = new Date();
									if (SinedDateUtils.afterOrEqualsIgnoreHour(new java.sql.Date(fc.getData().getTime()), new java.sql.Date(dataAtual.getTime())) && valorOrcado != null && valorOrcado.compareTo(valor) == -1) {
										bean.getListaMensal().get(mes).setValor(valorOrcado);
										bean.getListaMensal().get(mes).setValorAlteradoOrcamento(true);
									}
								}
							}
						}
					}
				}
			}
			
			Set<Integer> keys = new ListSet<Integer>(Integer.class, mapContaGerencial.keySet());
			for(Integer key : keys){
				FluxocaixaMensalReportBean bean = mapContaGerencial.get(key);
				for (FluxocaixaMensalSubReportBean fcm : bean.getListaMensal()) {
					if (fcm.getValor() == null){
						Money valorOrcado = buscarValorOrcado(filtro, bean.getNome(), fcm, bean.getIdentificador(), null, null, controleOrcamento);
						if (bean.getPossuiLancamento()){
							fcm.setValor(valorOrcado);
							if (fcm.getValor().getValue().doubleValue() != 0){
								fcm.setValorAlteradoOrcamento(true);							
							}						
						}
					}
				}
				
			}
		}
		
		//Remo��o de contas gerenciais sem lan�amento caso o filtro esteja assim definido
//		if(filtro.getMostrarContasgerenciaisSemLanc() == null || filtro.getMostrarContasgerenciaisSemLanc().equals(Boolean.FALSE)){
//			Set<Integer> keys = new ListSet<Integer>(Integer.class, mapContaGerencial.keySet());
//			for(Integer key : keys){
//				FluxocaixaMensalReportBean item = mapContaGerencial.get(key);
//				if(item.getPossuiLancamento() == null || Boolean.FALSE.equals(item.getPossuiLancamento())){
//					mapContaGerencial.remove(key);					
//				}
//			}
//		}
		
		List<FluxocaixaMensalReportBean> listaBean = new ArrayList<FluxocaixaMensalReportBean>();
		listaBean.addAll(mapContaGerencial.values());
		
		Collections.sort(listaBean, new Comparator<FluxocaixaMensalReportBean>(){
			@Override
			public int compare(FluxocaixaMensalReportBean o1, FluxocaixaMensalReportBean o2) {
				if(o2 == null || o2.getIdentificador() == null) return -1;
				if(o1 == null || o1.getIdentificador() == null) return -1;
				return o1.getIdentificador().compareTo(o2.getIdentificador());
			}
		});
		
		return listaBean;
	}

	private List<FluxocaixaMensalReportBean> agrupaFluxoCaixaMensalPorCentroCusto(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo) {
		
		Map<Integer, FluxocaixaMensalReportBean> mapBeanCredito = new HashMap<Integer, FluxocaixaMensalReportBean>();
		Map<Integer, FluxocaixaMensalReportBean> mapBeanDebito = new HashMap<Integer, FluxocaixaMensalReportBean>();
		
		
		List<Centrocusto> listaCentroCusto = new ArrayList<Centrocusto>();
		for (FluxocaixaBeanReport fc : listaFluxo) {
			if (fc.getListaRateioitem() != null && fc.getListaRateioitem().size() > 0) {
				for (Rateioitem ri : fc.getListaRateioitem()) {
					if (!listaCentroCusto.contains(ri.getCentrocusto())){
						listaCentroCusto.add(ri.getCentrocusto());
					}
				}
			}
		}
		List<Centrocusto> listaCentrocusto = centrocustoService.findAtivosFluxoCaixa(CollectionsUtil.listAndConcatenate(listaCentroCusto, "cdcentrocusto", ","));

		for (Centrocusto cc : listaCentrocusto) {
			FluxocaixaMensalReportBean fcc = new FluxocaixaMensalReportBean(cc.getNome(), filtro.getPeriodoDe(), filtro.getPeriodoAte());
			fcc.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			fcc.setCdcentrocusto(cc.getCdcentrocusto());
			mapBeanCredito.put(cc.getCdcentrocusto(), fcc);

			FluxocaixaMensalReportBean fcd = new FluxocaixaMensalReportBean(cc.getNome(), filtro.getPeriodoDe(), filtro.getPeriodoAte());
			fcd.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			fcd.setCdcentrocusto(cc.getCdcentrocusto());
			mapBeanDebito.put(cc.getCdcentrocusto(), fcd);
		}

		for (FluxocaixaBeanReport fc : listaFluxo) {
			if (fc.getListaRateioitem() != null && fc.getListaRateioitem().size() > 0) {
				for (Rateioitem ri : fc.getListaRateioitem()) {
					FluxocaixaMensalReportBean bean;
					if (fc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO))
						bean = mapBeanDebito.get(ri.getCentrocusto().getCdcentrocusto());
					else 
						bean = mapBeanCredito.get(ri.getCentrocusto().getCdcentrocusto());

					Integer mes = SinedDateUtils.mesesEntreInicio(filtro.getPeriodoDe(), new java.sql.Date(fc.getData().getTime()));
//					if (bean.getListaMensal().size() > mes) {
//						Money valor = bean.getListaMensal().get(mes).getValor();
//						if (valor == null)
//							valor = new Money(0);
//						valor = valor.add(ri.getValor());
//						bean.getListaMensal().get(mes).setValor(valor);
//					}
//					
//					if(fc.getData().before(filtro.getPeriodoDe())){
//						Money valoresAnteriores = bean.getValoresAnteriores();
//						if (valoresAnteriores == null)
//							valoresAnteriores = new Money(0);
//						valoresAnteriores = valoresAnteriores.add(ri.getValor());
//						bean.setValoresAnteriores(valoresAnteriores);
//					}
					if(fc.getData().before(filtro.getPeriodoDe())){
						Money valoresAnteriores = bean.getValoresAnteriores();
						if (valoresAnteriores == null)
							valoresAnteriores = new Money(0);
						valoresAnteriores = valoresAnteriores.add(ri.getValor());
						bean.setValoresAnteriores(valoresAnteriores);
						
						if((fc.getTipo() == FluxocaixaBeanReport.MOVIMENTACAO && 
								filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores())|| 
								(fc.getTipo() == FluxocaixaBeanReport.AGENDAMENTO && 
								filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores())||
								(fc.getTipo() == FluxocaixaBeanReport.CONTRATO && 
								filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores())||
								(fc.getTipo() == FluxocaixaBeanReport.DOCUMENTO && (
								(filtro.getLancContapagarAtraso() != null && filtro.getLancContapagarAberto()) ||
								(filtro.getLancContareceberAtraso() != null && filtro.getLancContareceberAtraso())))){
							bean.setMostrarValorAnterior(true);
						}
					}else {
						if (bean.getListaMensal().size() > mes) {
							Money valor = bean.getListaMensal().get(mes).getValor();
							if (valor == null)
								valor = new Money(0);
							valor = valor.add(ri.getValor());
							bean.getListaMensal().get(mes).setValor(valor);
							bean.setPossuiLancamento(Boolean.TRUE);
						}
					}
				}
			}
		}

		ArrayList<FluxocaixaMensalReportBean> list = new ArrayList<FluxocaixaMensalReportBean>();
		list.addAll(mapBeanDebito.values());
		list.addAll(mapBeanCredito.values());
		
		Collections.sort(list, new Comparator<FluxocaixaMensalReportBean>(){

			@Override
			public int compare(FluxocaixaMensalReportBean o1, FluxocaixaMensalReportBean o2) {

				int compareTipo = o1.getTipooperacao().getCdtipooperacao().compareTo(o2.getTipooperacao().getCdtipooperacao());
				if (compareTipo != 0)
					return compareTipo;
				else
					return o1.getIdentificador().compareTo(o2.getIdentificador());
			}
			
		});
		
		return list;
	}

	private List<FluxocaixaMensalReportBean> addTotalFluxoCaixaMensal(FluxocaixaFiltroReport filtro, List<FluxocaixaMensalReportBean> listaFluxo,
					Money saldocontas, boolean isContaGerencial) {
		FluxocaixaMensalReportBean fcTotal = new FluxocaixaMensalReportBean("Total", filtro.getPeriodoDe(), filtro.getPeriodoAte()); //total geral

		for (FluxocaixaMensalReportBean fc : listaFluxo) {
			if(fc.getNivel() == 1){
				Money valoresAnteriores = fcTotal.getValoresAnteriores();
				if (valoresAnteriores == null)
					valoresAnteriores = new Money(0);
				if(fc.getValoresAnteriores() != null){
					if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao()))
						valoresAnteriores = valoresAnteriores.add(fc.getValoresAnteriores());
					else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao()))
						valoresAnteriores = valoresAnteriores.subtract(fc.getValoresAnteriores());
				}
				fcTotal.setValoresAnteriores(valoresAnteriores);
			}
			if (fc.getNivel().intValue() > 1) {
				continue;
			}
	
			Integer index = 0;
			for (FluxocaixaMensalSubReportBean fcm : fc.getListaMensal()) {
				Money valor = fcTotal.getListaMensal().get(index).getValor();
				if (valor == null)
					valor = new Money(0);
				if (fcm.getValor() != null) {
					if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao()))
						valor = valor.add(fcm.getValor());
					else
						if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao()))
							valor = valor.subtract(fcm.getValor());
					fcTotal.getListaMensal().get(index).setValor(valor);
				}
				index++;
			}
		}
		listaFluxo.add(fcTotal);

		for (FluxocaixaMensalReportBean fc : listaFluxo) {
			Money total = new Money();
			boolean addValorAnterior = false;
			for (FluxocaixaMensalSubReportBean fcm : fc.getListaMensal()) {
				if (fcm.getValor() != null)
					total = total.add(fcm.getValor());
				if(!addValorAnterior){
					addValorAnterior = true;
					if(fc.getValoresAnteriores() != null){
						if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() &&
								fc.getValoresAnteriores() != null && (fc.getMostrarValorAnterior() != null && 
											fc.getMostrarValorAnterior())){
							total = total.add(fc.getValoresAnteriores());
						}
					}
				}
			}
			fc.getListaMensal().add(new FluxocaixaMensalSubReportBean("Total", total)); //coluna total
		}
		
		//relatorio mensal por projecao
		if(!isContaGerencial){
			if (saldocontas != null) {
				FluxocaixaMensalReportBean fcTotalGeral = null;
				String marcadoSaldoInicialIncluidoManualmente = filtro.getSaldoACTela() != null? "* ": "";
				
				FluxocaixaMensalReportBean fcSaldoAnterior = null;
				fcSaldoAnterior = new FluxocaixaMensalReportBean("Saldo Anterior", filtro.getPeriodoDe(), filtro.getPeriodoAte());
				
				fcTotalGeral = new FluxocaixaMensalReportBean(marcadoSaldoInicialIncluidoManualmente+"Acumulado", filtro.getPeriodoDe(), filtro.getPeriodoAte());
				
				fcTotalGeral.setValoresAnteriores(saldocontas);
				Money saldoAnterior = new Money();
				for (FluxocaixaMensalSubReportBean fc : fcTotal.getListaMensal()) {
					if (fc.getValor() != null) {
						saldoAnterior = saldocontas;
						saldocontas = saldocontas.add(fc.getValor());
					}
					
					fcSaldoAnterior.getListaTotalMensal().add(new FluxocaixaMensalSubReportBean("Saldo Anterior", saldoAnterior));
					fcTotalGeral.getListaTotalMensal().add(new FluxocaixaMensalSubReportBean("Acumulado", saldocontas));
				}
				listaFluxo.add(fcSaldoAnterior);
				listaFluxo.add(fcTotalGeral);
			}
		
		//conta gerencial
		}else {		
			if (saldocontas != null) {
				FluxocaixaMensalReportBean fcTotalGeral = null;
				String marcadoSaldoInicialIncluidoManualmente = filtro.getSaldoACTela() != null? "* ": "";
				fcTotalGeral = new FluxocaixaMensalReportBean(marcadoSaldoInicialIncluidoManualmente+"Acumulado", filtro.getPeriodoDe(), filtro.getPeriodoAte());
				
				fcTotalGeral.setValoresAnteriores(saldocontas);
				for (FluxocaixaMensalSubReportBean fc : fcTotal.getListaMensal()) {
					if (fc.getValor() != null) {
						saldocontas = saldocontas.add(fc.getValor());
					}
					fcTotalGeral.getListaTotalMensal().add(new FluxocaixaMensalSubReportBean("Acumulado", saldocontas));
				}
				listaFluxo.add(fcTotalGeral);
			}
		}

		return listaFluxo;
	}

	private List<FluxocaixaMensalReportBean> addTotalFluxoCaixaMensalPorCentroCusto(FluxocaixaFiltroReport filtro, List<FluxocaixaMensalReportBean> listaFluxo,
			Money saldocontas, Controleorcamento controleOrcamento) {
		
		Map<Integer, FluxocaixaMensalReportBean> mapTotais = new HashMap<Integer, FluxocaixaMensalReportBean>();
		FluxocaixaMensalReportBean fcTotalGeral = new FluxocaixaMensalReportBean("Total Geral", filtro.getPeriodoDe(), filtro.getPeriodoAte());

		Money totalGeralAnterior = new Money(0);
		
		// listaFluxo - LISTA DE FLUXO DE CAIXA POR CENTRO DE CUSTO E TIPO DE OPERA��O
		for (FluxocaixaMensalReportBean fc : listaFluxo) {
			
			// MAPA SALDO TOTAIS POR CENTRO DE CUSTO COM VALORES ANTERIORES
			FluxocaixaMensalReportBean fcTotal = mapTotais.get(fc.getCdcentrocusto());
			if (fcTotal == null){
				fcTotal = new FluxocaixaMensalReportBean("Saldo " + fc.getIdentificador(), filtro.getPeriodoDe(), filtro.getPeriodoAte());
				fcTotal.setValoresAnteriores(new Money(0));
				mapTotais.put(fc.getCdcentrocusto(), fcTotal);
			}
			
			Money valoresAnteriores = fcTotal.getValoresAnteriores();
			
			if(fc.getValoresAnteriores() != null){
				if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())){
					valoresAnteriores = valoresAnteriores.add(fc.getValoresAnteriores());
					totalGeralAnterior = totalGeralAnterior.add(fc.getValoresAnteriores());
				}else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
					valoresAnteriores = valoresAnteriores.subtract(fc.getValoresAnteriores());
					totalGeralAnterior = totalGeralAnterior.subtract(fc.getValoresAnteriores());
				}
			}
			fcTotal.setValoresAnteriores(valoresAnteriores);
			
			Integer index = 0;
			for (FluxocaixaMensalSubReportBean fcm : fc.getListaMensal()) {
				Money valor = fcTotal.getListaMensal().get(index).getValor();
				if (valor == null) valor = new Money(0);
				
				if (fcm.getValor() != null) {
					if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())){
						valor = valor.add(fcm.getValor());
					} else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
						valor = valor.subtract(fcm.getValor());
					}
					//SALDO TOTAL DO CENTRO DE CUSTO POR MES
					fcTotal.getListaMensal().get(index).setValor(valor);
				}
				if(Boolean.TRUE.equals(filtro.getConsControleOrcamentario())){
					if(fcm.getAno() != null && fcm.getMes() !=null){
						Money valorOrcadoTotal = fcTotal.getListaMensal().get(index).getValorOrcado();
						if (valorOrcadoTotal == null) valorOrcadoTotal = new Money(0);
						
						//BUSCAR VALOR ORCADO DO CENTRO DE CUSTO NO MES
						Money valorOrcado = buscarValorOrcado(filtro,null,fcm,null,fc.getCdcentrocusto(),fc.getTipooperacao(), controleOrcamento);
						if(valorOrcado == null) valorOrcado = new Money();
						if(fcm.getValorOrcado() == null) fcm.setValorOrcado(new Money());
						fcm.setValorOrcado(fcm.getValorOrcado().add(valorOrcado));
						
						if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())){
							valorOrcadoTotal = valorOrcadoTotal.add(valorOrcado);
						} else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
							valorOrcadoTotal = valorOrcadoTotal.subtract(valorOrcado);
						}
						
						//SALDO TOTAL DO CENTRO DE CUSTO POR MES
						fcTotal.getListaMensal().get(index).setValorOrcado(valorOrcadoTotal);
					}
				}

				//TOTAL GERAL POR MES
				Money valorGeral = fcTotalGeral.getListaMensal().get(index).getValor();
				
				if (valorGeral == null) valorGeral = new Money(0);
				
				if (fcm.getValor() != null) {
					if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())){
						valorGeral = valorGeral.add(fcm.getValor());
					} else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
						valorGeral = valorGeral.subtract(fcm.getValor());
					}
					fcTotalGeral.getListaMensal().get(index).setValor(valorGeral);
				}
				
				//TOTAL GERAL OR�ADO POR MES
				if(Boolean.TRUE.equals(filtro.getConsControleOrcamentario())){
					Money valorGeralOrcado = fcTotalGeral.getListaMensal().get(index).getValorOrcado();
					if (valorGeralOrcado == null) valorGeralOrcado = new Money(0);
					
					if (fcm.getValorOrcado() != null && fcm.getValorOrcado().getValue().doubleValue() > 0) {
						if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao()))
							valorGeralOrcado = valorGeralOrcado.add(fcm.getValorOrcado());
						else if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
							valorGeralOrcado = valorGeralOrcado.subtract(fcm.getValorOrcado());
						}
					}
					fcTotalGeral.getListaMensal().get(index).setValorOrcado(valorGeralOrcado);
				}

				index++;
			}
		}
		
		fcTotalGeral.setValoresAnteriores(totalGeralAnterior);

		List<FluxocaixaMensalReportBean> listaSaldoTotaisPorCentroCusto = new ArrayList<FluxocaixaMensalReportBean>(mapTotais.values());
		Collections.sort(listaSaldoTotaisPorCentroCusto, new Comparator<FluxocaixaMensalReportBean>(){
			@Override
			public int compare(FluxocaixaMensalReportBean o1, FluxocaixaMensalReportBean o2) {
				return o1.getIdentificador().compareTo(o2.getIdentificador());
			}
		});
		
		//ADICIONA A LISTA DE SALDO NA LISTA FINAL
		listaFluxo.addAll(listaSaldoTotaisPorCentroCusto);
		
		//ADICIONA O TOTAL GERAL NA LISTA FINAL
		listaFluxo.add(fcTotalGeral);

		for (FluxocaixaMensalReportBean fc : listaFluxo) {
			Money total = new Money();
			Money totalOrcado = new Money();
			
			boolean addValorAnterior = false;
			for (FluxocaixaMensalSubReportBean fcm : fc.getListaMensal()) {
				if (fcm.getValor() != null) total = total.add(fcm.getValor());
				if (fcm.getValorOrcado() != null) totalOrcado = totalOrcado.add(fcm.getValorOrcado());
				
				if(!addValorAnterior){
					addValorAnterior = true;
					if(fc.getValoresAnteriores() != null){
						if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() &&
								fc.getValoresAnteriores() != null && ((fc.getMostrarValorAnterior() != null && 
											fc.getMostrarValorAnterior()) || fc.getTipooperacao() == null)){
							total = total.add(fc.getValoresAnteriores());
						}
					}
				}
			}
			
			if(Boolean.TRUE.equals(filtro.getConsControleOrcamentario())){
				fc.getListaMensal().add(new FluxocaixaMensalSubReportBean("Total", total, totalOrcado));
			}else {
				fc.getListaMensal().add(new FluxocaixaMensalSubReportBean("Total", total));
			}
		}

		return listaFluxo;
	}
	
	private List<FluxocaixaMensalReportBean> totalizaSaldoContaGerencial(List<FluxocaixaMensalReportBean> listaFluxocaixaMensalReportBean, Integer qtdeMeses) {
//		Integer maiorNivel = Integer.MIN_VALUE;
//		for (FluxocaixaMensalReportBean fc : listaFluxocaixaMensalReportBean) {
//			if (fc.getNivel() > maiorNivel)
//				maiorNivel = fc.getNivel();
//		}
//
//		Integer nivel = maiorNivel;
//		Money total;
////		Money totalValoresAnteriores;
//		while (nivel > 1) {
//			for (int mes = 0; mes < qtdeMeses; mes++) {
//				total = new Money(0);
////				totalValoresAnteriores = new Money(0);
//				for (int i = listaFluxocaixaMensalReportBean.size() - 1; i > -1; i--) {
//					FluxocaixaMensalReportBean fc = listaFluxocaixaMensalReportBean.get(i);
//					Money valor = fc.getListaMensal().get(mes).getValor();
//					if (fc.getNivel().intValue() == nivel) {
//						if (valor != null){
//							total = total.add(valor);
////							if(mes == 0 && fc.getValoresAnteriores() != null){
////								totalValoresAnteriores = totalValoresAnteriores.add(fc.getValoresAnteriores());
////							}
//						}
//					} else
//						if (fc.getNivel().intValue() == (nivel - 1)) {
//							if (valor == null || (valor != null && valor.getValue().doubleValue() == 0)) {
//								fc.getListaMensal().get(mes).setValor(total);
//								total = new Money(0);
////								if(mes == 0){
////									fc.setValoresAnteriores(totalValoresAnteriores);
////									totalValoresAnteriores = new Money(0);
////								}
//							}
//						}
//				}
//			}
//			nivel--;
//		}
		
		for (int mes = 0; mes < qtdeMeses; mes++) {
			for(FluxocaixaMensalReportBean fc : listaFluxocaixaMensalReportBean){
				int nivel = fc.getNivel();
				FluxocaixaMensalSubReportBean fcSub = fc.getListaMensal().get(mes);
				for(FluxocaixaMensalReportBean fc2 : listaFluxocaixaMensalReportBean){
					if(fc2.getIdentificadorPai(nivel) != null && fc.getIdentificador() != null && fc2.getIdentificadorPai(nivel).equals(fc.getIdentificador()) &&
							!fc.getIdentificador().equals(fc2.getIdentificador())){
						FluxocaixaMensalSubReportBean fcSub2 = fc2.getListaMensal().get(mes);
						if(fcSub.getValor() == null) fcSub.setValor(new Money());
						if(fcSub2.getValor() == null) fcSub2.setValor(new Money());
						fcSub.setValor(fcSub.getValor().add(fcSub2.getValor()));
						
						if(fc2.getValoresAnteriores() != null && fc.getValoresAnteriores() != null && mes == 0){
							fc.setValoresAnteriores(fc.getValoresAnteriores().add(fc2.getValoresAnteriores()));
						}
					}
				}
			}
		}
		return listaFluxocaixaMensalReportBean;
	}
	
	/**
	 * M�todo para somar os saldos dos itens do fluxo de caixa para o quadro
	 * resumo do relat�rio.
	 * 
	 * @param listaFluxo
	 * @param saldos
	 * @author Fl�vio Tavares
	 * @param filtro 
	 */
	private void somaSaldos(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo, Saldos saldos) {
		for (FluxocaixaBeanReport fc : listaFluxo) {
			
			if(fc.getData() != null && 
					SinedDateUtils.beforeOrEqualIgnoreHour((java.sql.Date) fc.getData(), (java.sql.Date) filtro.getDtInicioBase())) {
				continue;
			}
			
			boolean credito = Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao());
			
			if (fc.getConta() == null){
				if (credito) {
					saldos.addSaldoOutros(fc.getCredito());
				} else {
					saldos.subtractSaldoOutros(fc.getDebito());
				}
				continue;
			}

			boolean caixa = Contatipo.TIPO_CAIXA.equals(fc.getConta().getContatipo());
			boolean bancaria = Contatipo.TIPO_CONTA_BANCARIA.equals(fc.getConta().getContatipo());
			boolean cartao = Contatipo.TIPO_CARTAO_CREDITO.equals(fc.getConta().getContatipo());

			if (caixa) {
				if (credito) {
					saldos.addSaldoCaixas(fc.getCredito());
				} else {
					saldos.subtractSaldoCaixas(fc.getDebito());
				}
			}
			if (bancaria) {
				if (credito) {
					saldos.addSaldoBancarias(fc.getCredito());
				} else {
					saldos.subtractSaldoBancarias(fc.getDebito());
				}
			}
			if (cartao) {
				if (credito) {
					saldos.addSaldoCartoes(fc.getCredito());
				} else {
					saldos.subtractSaldoCartoes(fc.getDebito());
				}
			}
			
			if(!caixa && !bancaria && !cartao){
				if (credito) {
					saldos.addSaldoOutros(fc.getCredito());
				} else {
					saldos.subtractSaldoOutros(fc.getDebito());
				}
			}
		}

	}

	/**
	 * M�todo respons�vel por calcular os creditos e d�bitos da lista do fluxo
	 * de caixa para o quadro Resumo do relat�rio.
	 * 
	 * @param listaFluxo
	 * @param report
	 * @author Fl�vio Tavares
	 */
	private void calculaCreditosDebitos(List<FluxocaixaBeanReport> listaFluxo, Report report) {

		Money creditoAberto = new Money(), debitoAberto = new Money(), creditoAtraso = new Money(), debitoAtraso = new Money();

		for (FluxocaixaBeanReport fc : listaFluxo) {
			switch (fc.getTipo()) {
				case FluxocaixaBeanReport.DOCUMENTO: // DOCUMENTO
					long diferenca = SinedDateUtils.calculaDiferencaDias(new java.sql.Date(fc.getData().getTime()), SinedDateUtils.currentDate());
					boolean atrasada = diferenca > 0;

					if (Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())) {
						if (atrasada) {
							creditoAtraso = creditoAtraso.add(fc.getCredito());
						} else {
							creditoAberto = creditoAberto.add(fc.getCredito());
						}
					}
					if (Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())) {
						if (atrasada) {
							debitoAtraso = debitoAtraso.add(fc.getDebito());
						} else {
							debitoAberto = debitoAberto.add(fc.getDebito());
						}
					}
					break;
				case FluxocaixaBeanReport.MOVIMENTACAO: 
					break;
				default:
					if(Tipooperacao.TIPO_CREDITO.equals(fc.getTipooperacao())){
						creditoAberto = creditoAberto.add(fc.getCredito()); 
					}
					if(Tipooperacao.TIPO_DEBITO.equals(fc.getTipooperacao())){
						debitoAberto = debitoAberto.add(fc.getDebito()); 
					}
					break;
			}
		}

		report.addParameter("creditoAberto", creditoAberto);
		report.addParameter("creditoAtraso", creditoAtraso);
		report.addParameter("debitoAberto", debitoAberto);
		report.addParameter("debitoAtraso", debitoAtraso);
	}

	/**
	 * M�todo respons�vel por separar os registros de fluxo por conta gerencial
	 * 
	 * @param listaFluxo
	 * @return
	 * @author Rafael Salvio
	 */
	private List<FluxocaixaBeanReport> separaContagerencial(List<FluxocaixaBeanReport> listaFluxo){
		List<FluxocaixaBeanReport> listaSeparada = new ArrayList<FluxocaixaBeanReport>();
		for (FluxocaixaBeanReport fluxo : listaFluxo) {
			if(fluxo.getListaRateioitem() != null && !fluxo.getListaRateioitem().isEmpty()){
				Map<Contagerencial, FluxocaixaBeanReport> mapaContagerencial = new HashMap<Contagerencial, FluxocaixaBeanReport>();
				for (Rateioitem rateioitem : fluxo.getListaRateioitem()) {
					FluxocaixaBeanReport aux = mapaContagerencial.get(rateioitem.getContagerencial());
					if(aux != null){
						if(Tipooperacao.TIPO_CREDITO.equals(aux.getTipooperacao())){
							aux.setCredito(aux.getCredito().add(rateioitem.getValor()));
						} else{
							aux.setDebito(aux.getDebito().add(rateioitem.getValor()));
						}
						aux.getListaRateioitem().add(rateioitem);
					} else{
						aux = new FluxocaixaBeanReport(fluxo, rateioitem);
					}
					mapaContagerencial.put(rateioitem.getContagerencial(), aux);
				}
				for (FluxocaixaBeanReport fluxoSeparado : mapaContagerencial.values()) {
					listaSeparada.add(fluxoSeparado);
				} 
			} else{
				listaSeparada.add(fluxo);
			}
		}
		return listaSeparada;
	}
	
	/**
	 * M�todo respons�vel por somar os valores de cr�dito e d�bito das linhas de
	 * cada detalhe do relat�rio.
	 * 
	 * @see #newFluxocaixa(FluxocaixaBeanReport)
	 * @param listaFluxo
	 *            - A lista deve estar ordenada por data.
	 * @param saldoAC
	 * @param exibeDetalhes
	 * @return
	 * @author Fl�vio Tavares
	 * @param valoresAnteriores
	 */
	private List<FluxocaixaBeanReport> agrupaListaHeader(List<FluxocaixaBeanReport> listaFluxo, Money saldoAC, boolean exibeDetalhes,
					Boolean valoresAnteriores, FluxocaixaFiltroReport filtro, boolean pdf, boolean origem) {

		// LISTA COM OS CABE�ALHOS E AS SOMAS DOS CR�DITOS/D�BITOS, E OS
		// DETALHES CASO SEJAM NECESS�RIOS
		List<FluxocaixaBeanReport> listaAgrupada = new ArrayList<FluxocaixaBeanReport>();

		Date data = null;
		String indicadorOrigem = null;
		
		FluxocaixaBeanReport header = new FluxocaixaBeanReport();
		Money creditos = new Money();
		Money debitos = new Money();
		Money saldod = new Money();
		Money saldoACMov = new Money();
		
		FluxocaixaBeanReport headerOrigem = new FluxocaixaBeanReport();
		Money creditosOrigem = new Money();
		Money debitosOrigem = new Money();
		
		for (FluxocaixaBeanReport fluxo : listaFluxo) {
			//caso seja removido, o valor do item est� inclu�do no saldo anterior
			if(removerItemListaFluxoCaixaByContaAnteriores(fluxo, filtro)){
				continue;
			}
			// MUDAN�A DE DATA
			if (!SinedDateUtils.equalsIgnoreHour((java.sql.Date) fluxo.getData(), (java.sql.Date) data)) {
				data = fluxo.getData();

				saldod = creditos.subtract(debitos);
				saldoAC = saldoAC.add(saldod);
				saldoACMov = saldoAC;

				header.setDebito(debitos);
				header.setCredito(creditos);
				header.setSaldoD(saldod);
				header.setSaldoAC(saldoAC);

				header = this.newFluxocaixa(fluxo);
				if (Util.booleans.isTrue(valoresAnteriores)
								|| (Util.booleans.isFalse(valoresAnteriores) && !header.getDescricao().toUpperCase().equals("CONTAS ANTERIORES")))
					listaAgrupada.add(header);

				creditos = new Money();
				debitos = new Money();
				
				if(origem){
					headerOrigem.setDebito(debitosOrigem);
					headerOrigem.setCredito(creditosOrigem);
					
					indicadorOrigem = fluxo.getIndicadorOrigem();
					headerOrigem = this.newFluxocaixaOrigem(fluxo);
					if(!headerOrigem.getDescricao().toUpperCase().equals("CONTAS ANTERIORES")){
						listaAgrupada.add(headerOrigem);
						creditosOrigem = new Money();
						debitosOrigem = new Money();
					}
				}
			}else if(origem){
				if (indicadorOrigem != null && !indicadorOrigem.equalsIgnoreCase(fluxo.getIndicadorOrigem())) {
					indicadorOrigem = fluxo.getIndicadorOrigem();

					headerOrigem.setDebito(debitosOrigem);
					headerOrigem.setCredito(creditosOrigem);

					headerOrigem = this.newFluxocaixaOrigem(fluxo);
					if (!headerOrigem.getDescricao().toUpperCase().equals("CONTAS ANTERIORES"))
						listaAgrupada.add(headerOrigem);

					creditosOrigem = new Money();
					debitosOrigem = new Money();
				}
			}
			
			boolean credito = fluxo.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO);
			if (fluxo.fluxoDeCartaocredito()) {
				creditos = creditos.add(fluxo.getCredito() != null ? fluxo.getCredito() : new Money(0));

				debitos = debitos.add(fluxo.getDebito() != null ? fluxo.getDebito() : new Money(0));

				if(origem){
					creditosOrigem = creditosOrigem.add(fluxo.getCredito() != null ? fluxo.getCredito() : new Money(0));
					debitosOrigem = debitosOrigem.add(fluxo.getDebito() != null ? fluxo.getDebito() : new Money(0));
				}
			} else {
				if (credito) {
					creditos = creditos.add(fluxo.getCredito());
				} else {
					debitos = debitos.add(fluxo.getDebito());
				}
				
				if(origem){
					if (credito) {
						creditosOrigem = creditosOrigem.add(fluxo.getCredito());
					} else {
						debitosOrigem = debitosOrigem.add(fluxo.getDebito());
					}
				}
			}

			// CONDI�AO PARA QUE A LINHA DO RELAT�RIO QUE REPRESENTA O DETALHE
			// SEJA OMITIDA SE N�O FOR PARA EXIBIR OS DETALHES
			fluxo.setShowDetail(exibeDetalhes);

			/*
			 * ADICIONA OS BEANS QUE REPRESENTAM O DETALHE DO RELAT�RIO
			 * DEPENDENDO DO PAR�METRO INFORMADO
			 */
			if (exibeDetalhes) {
				List<FluxocaixaBeanReport> lista = createListaAgrupadaPorProjeto(fluxo, pdf);
				for(FluxocaixaBeanReport fcBean : lista){
					saldoACMov = saldoACMov.add((fcBean.getCredito() != null ? fcBean.getCredito() : new Money())
									.subtract(fcBean.getDebito() != null ? fcBean.getDebito() : new Money()));
					fcBean.setSaldoAC(saldoACMov);
	
					listaAgrupada.add(fcBean);
				}
			}

		}

		saldod = creditos.subtract(debitos);
		saldoAC = saldoAC.add(saldod);

		header.setDebito(debitos);
		header.setCredito(creditos);
		header.setSaldoD(saldod);
		header.setSaldoAC(saldoAC);
		
		if(origem){
			headerOrigem.setDebito(debitosOrigem);
			headerOrigem.setCredito(creditosOrigem);
		}

		return listaAgrupada;
	}
	
	/**
	* M�todo que ajusta o saldo anterior
	* (obs: na fun��o do banco, o saldo considerado as movimenta��es sem origem. 
	*		Ao exibir as movimenta��es anteriores, o saldo estava duplicado. Este m�todo remove o item do saldo)
	*
	* @param filtro
	* @param listaFluxo
	* @since 17/07/2014
	* @author Luiz Fernando
	*/
	private void ajustaSaldoAC(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo) {
		if(filtro != null && filtro.getSaldoAC() != null && filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() && 
				SinedUtil.isListNotEmpty(listaFluxo) && 
				filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores()){
			Money diferencaBancarias = new Money();
			Money diferencaCaixas = new Money();
			Money diferencaCartoes = new Money();
			Money diferencaOutros = new Money();
			
			for (FluxocaixaBeanReport fluxo : listaFluxo) {
				boolean caixa = fluxo.getConta() != null && Contatipo.TIPO_CAIXA.equals(fluxo.getConta().getContatipo());
				boolean bancaria = fluxo.getConta() != null && Contatipo.TIPO_CONTA_BANCARIA.equals(fluxo.getConta().getContatipo());
				boolean cartao = fluxo.getConta() != null && Contatipo.TIPO_CARTAO_CREDITO.equals(fluxo.getConta().getContatipo());
				
				if(this.removerItemListaFluxoCaixaByContaAnteriores(fluxo, filtro)){
					if (!(fluxo.getTipo() == FluxocaixaBeanReport.MOVIMENTACAO /*&& 
							fluxo.getMovimentacaoSemOrigem() != null && fluxo.getMovimentacaoSemOrigem()*/)){
						
						if(Tipooperacao.TIPO_CREDITO.equals(fluxo.getTipooperacao())){
							if(fluxo.getCredito() != null){
								if(!caixa && !bancaria && !cartao){
									diferencaOutros = diferencaOutros.add(fluxo.getCredito());
								}else {
									if(caixa){
										diferencaCaixas = diferencaCaixas.add(fluxo.getCredito());
									}else if(bancaria){
										diferencaBancarias = diferencaBancarias.add(fluxo.getCredito());
									}else if(cartao){
										diferencaCartoes = diferencaCartoes.add(fluxo.getCredito());
									}
								}
							}
						}else if(Tipooperacao.TIPO_DEBITO.equals(fluxo.getTipooperacao())){
							if(fluxo.getDebito() != null){
								if(!caixa && !bancaria && !cartao){
									diferencaOutros = diferencaOutros.subtract(fluxo.getDebito());
								}else {
									if(caixa){
										diferencaCaixas = diferencaCaixas.subtract(fluxo.getDebito());
									}else if(bancaria){
										diferencaBancarias = diferencaBancarias.subtract(fluxo.getDebito());
									}else if(cartao){
										diferencaCartoes = diferencaCartoes.subtract(fluxo.getDebito());
									}
								}
							}
						}
					}
				}else if ((fluxo.getTipo() == FluxocaixaBeanReport.MOVIMENTACAO && 
						filtro.getLancMovimentacoesAnteriores() != null && filtro.getLancMovimentacoesAnteriores())|| 
						(fluxo.getTipo() == FluxocaixaBeanReport.AGENDAMENTO && 
						filtro.getLancAgendamentoAnteriores() != null && filtro.getLancAgendamentoAnteriores())||
						(fluxo.getTipo() == FluxocaixaBeanReport.CONTRATO && 
						filtro.getLancContratoAnteriores() != null && filtro.getLancContratoAnteriores())||
						(fluxo.getTipo() == FluxocaixaBeanReport.DOCUMENTO && (
						(filtro.getLancContapagarAtraso() != null && filtro.getLancContapagarAberto()) ||
						(filtro.getLancContareceberAtraso() != null && filtro.getLancContareceberAtraso())))){ 
					if(fluxo.getData() != null && 
						SinedDateUtils.beforeOrEqualIgnoreHour((java.sql.Date) fluxo.getData(), (java.sql.Date) filtro.getDtInicioBase()) /*&&
						fluxo.getMovimentacaoSemOrigem() != null && fluxo.getMovimentacaoSemOrigem()*/) {
						if(Tipooperacao.TIPO_CREDITO.equals(fluxo.getTipooperacao())){
							if(fluxo.getCredito() != null){
								if(!caixa && !bancaria && !cartao){
									diferencaOutros = diferencaOutros.add(fluxo.getCredito());
								}else {
									if(caixa){
										diferencaCaixas = diferencaCaixas.add(fluxo.getCredito());
									}else if(bancaria){
										diferencaBancarias = diferencaBancarias.add(fluxo.getCredito());
									}else if(cartao){
										diferencaCartoes = diferencaCartoes.add(fluxo.getCredito());
									}
								}
							}
						}else if(Tipooperacao.TIPO_DEBITO.equals(fluxo.getTipooperacao())){
							if(fluxo.getDebito() != null){
								if(!caixa && !bancaria && !cartao){
									diferencaOutros = diferencaOutros.subtract(fluxo.getDebito());
								}else {
									if(caixa){
										diferencaCaixas = diferencaCaixas.subtract(fluxo.getDebito());
									}else if(bancaria){
										diferencaBancarias = diferencaBancarias.subtract(fluxo.getDebito());
									}else if(cartao){
										diferencaCartoes = diferencaCartoes.subtract(fluxo.getDebito());
									}
								}
							}	
						}
					}
				}

			}
			filtro.setSaldoAC(filtro.getSaldoAC().subtract(diferencaCartoes).subtract(diferencaBancarias).subtract(diferencaCaixas).subtract(diferencaOutros));
		}
	}
	
	public List<FluxocaixaBeanReport> createListaAgrupadaPorProjeto(FluxocaixaBeanReport fluxo, boolean pdf){
		List<FluxocaixaBeanReport> lista = new ArrayList<FluxocaixaBeanReport>();
		if(pdf || fluxo.getListaRateioitem() == null || fluxo.getListaRateioitem().isEmpty()){
			lista.add(fluxo);
		} else {
			HashMap<Projeto, FluxocaixaBeanReport> mapProjeto = new HashMap<Projeto, FluxocaixaBeanReport>();
			FluxocaixaBeanReport fcProjetoNullo = null;
			for(Rateioitem ri : fluxo.getListaRateioitem()){
				if(ri.getProjeto() != null){
					FluxocaixaBeanReport fcProjeto = mapProjeto.get(ri.getProjeto());
					if(fcProjeto != null){
						if(Tipooperacao.TIPO_CREDITO.equals(fcProjeto.getTipooperacao())){
							if(fcProjeto.getCredito() == null) fcProjeto.setCredito(new Money());
							if(fluxo.getIsMedicao() != null && fluxo.getIsMedicao()){
								Money valor = fluxo.getCredito().multiply(new Money(ri.getPercentual() != null ? ri.getPercentual() : 0d)).divide(new Money(100d));
								fcProjeto.setCredito(fcProjeto.getCredito().add(valor));
							}else {
								fcProjeto.setCredito(fcProjeto.getCredito().add(ri.getValor()));
							}
						}else {
							if(fcProjeto.getDebito() == null) fcProjeto.setDebito(new Money());
							fcProjeto.setDebito(fcProjeto.getDebito().add(ri.getValor()));
						}
					}else {
						fcProjeto = new FluxocaixaBeanReport(fluxo, ri);
						if(Tipooperacao.TIPO_CREDITO.equals(fluxo.getTipooperacao())){
							if(fluxo.getIsMedicao() != null && fluxo.getIsMedicao()){
								Money valor = fluxo.getCredito().multiply(new Money(ri.getPercentual() != null ? ri.getPercentual() : 0d)).divide(new Money(100d));
								fcProjeto.setCredito(valor);
							}else {
								fcProjeto.setCredito(ri.getValor());
							}
						}else {
							fcProjeto.setDebito(ri.getValor());
						}
					}
					mapProjeto.put(ri.getProjeto(), fcProjeto);
				}else {
					if(fcProjetoNullo == null){
						fcProjetoNullo = new FluxocaixaBeanReport(fluxo, ri);
					} else {
						if(Tipooperacao.TIPO_CREDITO.equals(fcProjetoNullo.getTipooperacao())){
							if(fcProjetoNullo.getCredito() == null) fcProjetoNullo.setCredito(new Money());
							if(fluxo.getIsMedicao() != null && fluxo.getIsMedicao()){
								Money valor = fluxo.getCredito().multiply(new Money(ri.getPercentual() != null ? ri.getPercentual() : 0d)).divide(new Money(100d));
								fcProjetoNullo.setCredito(fcProjetoNullo.getCredito().add(valor));
							}else {
								fcProjetoNullo.setCredito(fcProjetoNullo.getCredito().add(ri.getValor()));
							}
						}else {
							if(fcProjetoNullo.getDebito() == null) fcProjetoNullo.setDebito(new Money());
							fcProjetoNullo.setDebito(fcProjetoNullo.getDebito().add(ri.getValor()));
						}
					}
				}
			}
			
			if(fcProjetoNullo != null){
				lista.add(fcProjetoNullo);
			}
			if(mapProjeto.size() > 0){
				lista.addAll(mapProjeto.values());
			}
		}
		return lista;
	}

	/**
	 * Cria um novo objeto para cabe�alho da linha do relat�rio.
	 * 
	 * @param fluxo
	 * @return
	 * @author Fl�vio Tavares
	 */
	private FluxocaixaBeanReport newFluxocaixa(FluxocaixaBeanReport fluxo) {
		FluxocaixaBeanReport fc = new FluxocaixaBeanReport();
		fc.setData(fluxo.getData());
		fc.setDescricao("RESUMO " + SinedDateUtils.toString(fluxo.getData()));
		if (SinedDateUtils.remoteDate().equals(fc.getData())) {
			fc.setDescricao("CONTAS ANTERIORES");
		}
		fc.setShowDetail(false);
		fc.setHeader(true);
		// fc.setPessoadescricao(fluxo.getPessoadescricao());
		return fc;
	}
	
	private FluxocaixaBeanReport newFluxocaixaOrigem(FluxocaixaBeanReport fluxo) {
		FluxocaixaBeanReport fc = new FluxocaixaBeanReport();
		fc.setData(fluxo.getData());
		fc.setDescricao(fluxo.getDescricaoIndicador());
		fc.setShowDetail(true);
		fc.setHeader(false);
		fc.setHeaderOrigem(true);
		return fc;
	}

	/**
	 * M�todo respons�vel por obter as listas de movimenta��o, contas a
	 * pagar/receber e agendamento e convert�-las em um bean gen�rico
	 * (Fluxocaixa) para exibir no relat�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#adicionaMovimentacaoFluxocaixa(List,
	 *      List)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#adicionaDocumentoFluxocaixa(List,
	 *      List)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#adicionaAgendamentoFluxocaixa(FluxocaixaFiltroReport,
	 *      List, List, java.sql.Date, java.sql.Date)
	 * @see #findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @see #adicionaFluxocaixaLista(FluxocaixaFiltroReport, List)
	 * @see Collections#sort(List)
	 * 
	 * @param filtro
	 * @return Lista de fluxo de caixa ordenada por data.
	 * @author Fl�vio Tavares
	 */
	private List<FluxocaixaBeanReport> montaListaDadosRelatorio(FluxocaixaFiltroReport filtro) {

		// PRINCIPAL LISTA DO RELAT�RIO, ONDE TODAS AS OUTRAS SER�O JUNTADAS.
		List<FluxocaixaBeanReport> listaFluxo = new ArrayList<FluxocaixaBeanReport>();

		// ADICIONA AS MOVIMENTA��ES NA LISTA
		List<Movimentacao> listaMovimentacao = movimentacaoService.findForReportFluxocaixa(filtro);
		movimentacaoService.adicionaMovimentacaoFluxocaixa(listaFluxo, listaMovimentacao, filtro.isFiltroOrigem());
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			// ADICIONA AS MOVIMENTA��ES ANTERIORES NA LISTA
			List<Movimentacao> listaMovimentacaoAnteriores = movimentacaoService.findAnterioresForReportFluxocaixa(filtro);
			movimentacaoService.adicionaMovimentacaoFluxocaixa(listaFluxo, listaMovimentacaoAnteriores, filtro.isFiltroOrigem());
			listaMovimentacaoAnteriores = null;
		}

		// ADICIONA AS MOVIMENTA��ES EM CART�ES DE CR�DITO NA LISTA
		List<Movimentacao> listaMovimentacaoCartao = movimentacaoService.findForFluxocaixa(filtro);
		movimentacaoService.adicionaMovimentacaoFluxocaixa(listaFluxo, listaMovimentacaoCartao, filtro.isFiltroOrigem());
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			//ADICIONA AS MOVIMENTA��ES EM CART�ES DE CR�DITO ANTERIORES NA LISTA
			List<Movimentacao> listaMovimentacaoCartaoAnteriores = movimentacaoService.findAnterioresForFluxocaixa(filtro);
			movimentacaoService.adicionaMovimentacaoFluxocaixa(listaFluxo, listaMovimentacaoCartaoAnteriores, filtro.isFiltroOrigem());
		}

		// ADICIONA AS CONTAS A PAGAR/REDCEBER ATRASADAS NA LISTA
		List<Documento> listaAnteriores = documentoService.findAnterioresForReportFluxocaixa(filtro);
		documentoService.adicionaDocumentoFluxocaixa(filtro, listaFluxo, listaAnteriores);

		// ADICIONA AS CONTAS A PAGAR/RECEBER NA LISTA
		List<Documento> listaDocumento = documentoService.findForReportFluxocaixa(filtro);
		documentoService.adicionaDocumentoFluxocaixa(filtro, listaFluxo, listaDocumento);

		
		// ADICIONA OS AGENDAMENTOS SIMULADOS NA LISTA
		List<Agendamento> listaAgendamento = agendamentoService.findForReportFluxocaixa(filtro);
		agendamentoService.adicionaAgendamentoFluxocaixa(filtro, listaFluxo, listaAgendamento, filtro.getPeriodoDe(), filtro.getPeriodoAte(), filtro.getValoresAnteriores());

		// ADICIONA OS CONTRATOS SIMULADOS NA LISTA
		List<Contrato> listaContrato = contratoService.findForReportFluxocaixa(filtro);
		contratoService.adicionaContratoFluxocaixa(filtro, listaFluxo, listaContrato, filtro.getPeriodoDe(), filtro.getPeriodoAte(), filtro
						.getRadEvento(), filtro.getValoresAnteriores());

		// ADICIONA AS DESPESAS COM COLABORADORES (DESPESA SEM CONTA A PAGAR)
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForReportFluxocaixa(filtro);
		colaboradordespesaService.adicionaColaboradordespesaFluxocaixa(filtro, listaFluxo, listaColaboradordespesa, filtro.getPeriodoDe(), filtro.getValoresAnteriores());
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			// ADICIONA AS DESPESAS ANTERIORES COM COLABORADORES (DESPESA SEM CONTA A PAGAR)
			List<Colaboradordespesa> listaColaboradordespesaAnteriores = colaboradordespesaService.findAnterioresForReportFluxocaixa(filtro);
			colaboradordespesaService.adicionaColaboradordespesaFluxocaixa(filtro, listaFluxo, listaColaboradordespesaAnteriores, filtro.getPeriodoDe(), filtro.getValoresAnteriores());
		}
		
		// ADICIONA OS PEDIDOS DE VENDA NA LISTA
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForReportFluxocaixa(filtro);
		pedidovendaService.adicionaPedidovendaFluxocaixa(filtro, listaFluxo, listaPedidovenda, filtro.getPeriodoDe(), filtro.getValoresAnteriores());
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			// ADICIONA OS PEDIDOS DE VENDA ANTERIORES NA LISTA
			List<Pedidovenda> listaPedidovendaAnteriores = pedidovendaService.findAnterioresForReportFluxocaixa(filtro);
			pedidovendaService.adicionaPedidovendaFluxocaixa(filtro, listaFluxo, listaPedidovendaAnteriores, filtro.getPeriodoDe(), filtro.getValoresAnteriores());
		}
					
		// ADICIONA OS FLUXOS DE CAIXA NA LISTA
		List<Fluxocaixa> listaFluxocaixa = this.findForReportFluxocaixaWithoutGroup(filtro);
		listaFluxo.addAll(this.adicionaFluxocaixaLista(filtro, listaFluxocaixa));
		// A LISTA � ORDENADA PARA SER AGRUPADA POR DATA NO RELAT�RIO
		Collections.sort(listaFluxo);

		return listaFluxo;
	}

	/**
	 * M�todo para inserir uma linha final no relat�rio informando o total de
	 * cr�ditos, d�bitos e saldo acumulado no per�odo.
	 * 
	 * @param listaFluxo
	 * @author Fl�vio Tavares
	 */
	private void insereLinhaTotal(List<FluxocaixaBeanReport> listaFluxo) {
		Money creditos = new Money();
		Money debitos = new Money();
		Money saldoD = new Money();
		for (FluxocaixaBeanReport fc : listaFluxo) {
			if (fc.isHeader()) {
				creditos = creditos.add(fc.getCredito());
				debitos = debitos.add(fc.getDebito());
				saldoD = saldoD.add(fc.getSaldoD());
			}
		}

		FluxocaixaBeanReport newFC = new FluxocaixaBeanReport();
		newFC.setDescricao("TOTAIS");
		newFC.setCredito(creditos);
		newFC.setDebito(debitos);
		newFC.setSaldoD(saldoD);
		newFC.setShowDetail(false);
		newFC.setData(SinedDateUtils.remoteDate());
		listaFluxo.add(newFC);
	}

	/**
	 * M�todo respons�vel por gerar as itera��es dos fluxos de caixa e
	 * convert�-las em uma lista de beans do relat�rio.
	 * 
	 * @see #geraIteracoesFluxocaixa(Fluxocaixa, java.sql.Date)
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport#FluxocaixaBeanReport(Fluxocaixa)
	 * @param filtro
	 * @param listaFluxocaixa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<FluxocaixaBeanReport> adicionaFluxocaixaLista(FluxocaixaFiltroReport filtro, List<Fluxocaixa> listaFluxocaixa) {
		/*
		 * Convers�o dos beans da lista para os do tipo do relat�rio
		 * (FluxocaixaBeanReport).
		 */
		List<FluxocaixaBeanReport> listaRetorno = new ArrayList<FluxocaixaBeanReport>(listaFluxocaixa.size());
		for (Fluxocaixa fc : listaFluxocaixa) {
			this.addHistoricoForFluxocaixaAnalitico(fc);
			FluxocaixaBeanReport fluxocaixaBeanReport = new FluxocaixaBeanReport(fc);
			if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores() && fluxocaixaBeanReport.getData().before(filtro.getPeriodoDe())){
				fluxocaixaBeanReport.setDataAtrasada(fluxocaixaBeanReport.getData());
				fluxocaixaBeanReport.setData(SinedDateUtils.remoteDate());
			}
			listaRetorno.add(fluxocaixaBeanReport);
		}

		return listaRetorno;
	}

	private void addHistoricoForFluxocaixaAnalitico(Fluxocaixa newFluxo) {
		if(newFluxo != null && newFluxo.getListaFluxocaixaorigem() != null && !newFluxo.getListaFluxocaixaorigem().isEmpty()){
			StringBuilder obs = new StringBuilder();
			for(Fluxocaixaorigem origem : newFluxo.getListaFluxocaixaorigem()){
				if(origem.getOrdemcompra() != null && origem.getOrdemcompra().getCdordemcompra() != null){
					if(!obs.toString().equals("")) obs.append(",");
					obs.append(origem.getOrdemcompra().getCdordemcompra());
				}
			}
			if(!obs.toString().equals("")){
				newFluxo.setDescricao((newFluxo.getDescricao() != null && !newFluxo.getDescricao().equals("") ? 
								newFluxo.getDescricao() + " - " : "Ordem de compra: ") + obs.toString());
			}
		}
	}

	/**
	 * Este m�todo � respons�vel por obter os registros que complementar�o o
	 * relat�rio de fluxo de caixa com dados de outros m�dulos do sistema. Faz
	 * refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FluxocaixaDAO#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Fluxocaixa> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return fluxocaixaDAO.findForReportFluxocaixa(filtro);
	}

	/**
	 * Semelhante ao m�todo findForReportFluxocaixa por�m sem o agrupamento e
	 * somatorio dos itens do rateio, m�todo utilizado para montar o agrupamento
	 * mensal por conta gerencial do fluxo de caixa Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FluxocaixaDAO#findForReportFluxocaixa(FluxocaixaFiltroReport)
	 * @param filtro
	 * @return
	 * @author Marden Silva
	 */
	public List<Fluxocaixa> findForReportFluxocaixaWithoutGroup(FluxocaixaFiltroReport filtro) {
		return fluxocaixaDAO.findForReportFluxocaixaWithoutGroup(filtro);
	}

	/**
	 * M�todo que cria os fluxos de caixa de uma Ordem de compra
	 * 
	 * @param ordemcompra
	 * @param fluxocaixaTipo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fluxocaixa> criaFluxoCaixasOrdemCompra(Ordemcompra ordemcompra, FluxocaixaTipo fluxocaixaTipo) {
		Prazopagamento prazopagamento = prazopagamentoService.loadForEntrada(ordemcompra.getPrazopagamento());
		Rateio rateio = new Rateio();
		Rateioitem rateioitemAux = null;
		List<Rateioitem> listaRateioitem = rateioitemService.findByRateio(ordemcompra.getRateio());
		List<Rateioitem> listaRateioitemAux = null;
		List<Fluxocaixa> listaFluxo = new ArrayList<Fluxocaixa>();

		Calendar ultimaData = Calendar.getInstance();
		long millis = ultimaData.getTimeInMillis();
		if(ordemcompra.getDtproximaentrega() != null){
			millis = ordemcompra.getDtproximaentrega().getTime();
		}else if(ordemcompra.getDtcriacao() != null){
			millis = ordemcompra.getDtcriacao().getTime();
		}
		
		List<Prazopagamentoitem> listaPrazoItem = prazopagamentoitemService.findByPrazo(ordemcompra.getPrazopagamento());
		if(listaPrazoItem != null && !listaPrazoItem.isEmpty()){
			for (Prazopagamentoitem item : listaPrazoItem) {
				ultimaData.setTimeInMillis(millis);
				
				ultimaData.add(Calendar.DAY_OF_MONTH, item.getDias() != null ? item.getDias() : 0);
				ultimaData.add(Calendar.MONTH, item.getMeses() != null ? item.getMeses() : 0);
	
				rateio = new Rateio();
				listaRateioitemAux = new ArrayList<Rateioitem>();
	
				if (listaRateioitem != null) {
					for (Rateioitem rateioitem : listaRateioitem) {
						rateioitemAux = new Rateioitem();
						rateioitemAux.setCentrocusto(rateioitem.getCentrocusto());
						rateioitemAux.setContagerencial(rateioitem.getContagerencial());
						rateioitemAux.setProjeto(rateioitem.getProjeto());
						rateioitemAux.setPercentual(rateioitem.getPercentual());
						rateioitemAux.setValor(new Money(rateioitem.getValor().divide(new Money(prazopagamento.getListaPagamentoItem().size()))));
						listaRateioitemAux.add(rateioitemAux);
					}
				}
				rateio.setListaRateioitem(listaRateioitemAux);
	
				Fluxocaixa fluxocaixa = new Fluxocaixa(fluxocaixaTipo, prazopagamento, rateio, Tipooperacao.TIPO_DEBITO, new java.sql.Date(ultimaData
								.getTimeInMillis()), new java.sql.Date(ultimaData.getTimeInMillis()), "Ordem de compra", new Money(ordemcompra
								.getValorTotalOrdemCompra()
								/ prazopagamento.getListaPagamentoItem().size()));
	
				Fluxocaixaorigem fluxocaixaorigem = new Fluxocaixaorigem(fluxocaixa, ordemcompra);
				fluxocaixa.setListaFluxocaixaorigem(new ListSet<Fluxocaixaorigem>(Fluxocaixaorigem.class));
				fluxocaixa.getListaFluxocaixaorigem().add(fluxocaixaorigem);
	
				listaFluxo.add(fluxocaixa);
			}
		}
		
		return listaFluxo;
	}
	
	public Resource createReportFluxocaixaMensalCSVPorCentroCusto(FluxocaixaFiltroReport filtro) {

		preparaRelatorioFluxoCaixa(filtro);
		boolean isOrcamento = Boolean.TRUE.equals(filtro.getConsControleOrcamentario());
		
		filtro.setRadEvento(false);// Seta esse valor para aparecer sempre a descri��o
		filtro.setMostrarDetalhes(true);
		filtro.setAgrupamentoMensal(true);
		
		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		
		//buscar orcamento
		Controleorcamento controleOrcamento = filtro.getControleOrcamentario() != null ? controleOrcamentoService.buscarValorFluxoCaixa(filtro.getControleOrcamentario()) : null;

		Money saldocontas = filtro.getSaldoAC() != null ? filtro.getSaldoAC() : new Money();
		saldocontas = this.somaSaldosCSV(filtro, listaFluxo, saldocontas, filtro.getDtInicioBase());
		
		List<FluxocaixaMensalReportBean> listaFluxoMensal = this.agrupaFluxoCaixaMensalPorCentroCusto(filtro, listaFluxo);
		listaFluxoMensal = this.addTotalFluxoCaixaMensalPorCentroCusto(filtro, listaFluxoMensal, saldocontas, controleOrcamento);

		StringBuilder csv;
		csv = new StringBuilder("Centro de custo;");
		csv.append("Tipo opera��o;");
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
			csv.append("Valores Anteriores;");
		}
		for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : listaFluxoMensal.get(0).getListaMensal()) {
			csv.append(fluxocaixaMensalSubReportBean.getDescricao()).append(";");
			if(isOrcamento){
				csv.append("Or�amento " + fluxocaixaMensalSubReportBean.getDescricao()).append(";");
				csv.append("Diferen�a " + fluxocaixaMensalSubReportBean.getDescricao()).append(";");
			}
		}
		csv.append("\n");
		
		for (int i = 0; i < listaFluxoMensal.size(); i++) {
			FluxocaixaMensalReportBean fluxocaixaMensalReportBean = listaFluxoMensal.get(i);
			Money totalOrcamento = new Money(0.0);
			Money diferencaTotalOrcamento = new Money(0.0);
			
			csv.append(fluxocaixaMensalReportBean.getIdentificador());
			if (fluxocaixaMensalReportBean.getNome() != null)
				csv.append(" - ").append(fluxocaixaMensalReportBean.getNome());
			csv.append(";");
			// TIPO OPERA��O
			if (fluxocaixaMensalReportBean.getTipooperacao() != null) {
				if (fluxocaixaMensalReportBean.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
					csv.append("D�BITO");
				} else {
					csv.append("CR�DITO");
				}
				csv.append(";");
			} else {
				csv.append(";");
			}
			if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
				csv.append(fluxocaixaMensalReportBean.getValoresAnteriores() != null && 
						((fluxocaixaMensalReportBean.getMostrarValorAnterior() != null && 
						fluxocaixaMensalReportBean.getMostrarValorAnterior()) ||
						(i == listaFluxoMensal.size()-1 || fluxocaixaMensalReportBean.getTipooperacao() == null))? 
						fluxocaixaMensalReportBean.getValoresAnteriores().toString() : "0,00");
				csv.append(";");
			}
			
			for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaMensal()) {
				if (fluxocaixaMensalSubReportBean.getValor() == null){
					fluxocaixaMensalSubReportBean.setValor(new Money());						
				}		
				csv.append(fluxocaixaMensalSubReportBean.getValor().toString());			
				
				if(isOrcamento){
					csv.append(";");
	
					Money valorOrcado = fluxocaixaMensalSubReportBean.getValorOrcado();
					if(valorOrcado == null) valorOrcado = new Money();
					csv.append(valorOrcado.toString()).append(";");
						
					Money diferenca = new Money(valorOrcado.subtract(fluxocaixaMensalSubReportBean.getValor()));
					csv.append(diferenca.toString());
						
					totalOrcamento = totalOrcamento.add(valorOrcado);
					diferencaTotalOrcamento = diferencaTotalOrcamento.add(diferenca);
					}
				csv.append(";");
			}
			
			for (FluxocaixaMensalSubReportBean fluxocaixaMensalSubReportBean : fluxocaixaMensalReportBean.getListaTotalMensal()) {
				if (fluxocaixaMensalSubReportBean.getValor() != null)
					csv.append(fluxocaixaMensalSubReportBean.getValor().toString()).append(";");
			}
			csv.append("\n");
		}

		Resource resource = new Resource("text/csv", "fluxocaixamensal_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public IReport createReportFluxocaixaAnaliticoOrigem(WebRequestContext request, FluxocaixaFiltroReport filtro) {
		preparaRelatorioFluxoCaixa(filtro);
		
		Report report = new Report("/financeiro/fluxocaixa_analitico_origem");

		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getPeriodoDe(), filtro.getPeriodoAte()));
		report.addParameter("periodoDe", filtro.getPeriodoDe());
		report.addParameter("periodoAte", filtro.getPeriodoAte());
		report.addParameter("dataAnterior", filtro.getDtInicioBase());

		if ((filtro.getMostrarDetalhes() == null || !filtro.getMostrarDetalhes()) && filtro.getMostrarLimiteContabancariaResumo() != null
						&& filtro.getMostrarLimiteContabancariaResumo()) {
			report.addParameter("LIMITECONTABANCARIA", filtro.getLimiteBancarias());
		}

		List<FluxocaixaBeanReport> listaFluxo = this.montaListaDadosRelatorio(filtro);
		ajustaListaFluxocaixaAnalitoOrigem(listaFluxo);
		this.calculaCreditosDebitos(listaFluxo, report);

		Saldos saldos = new Saldos(filtro.getSaldoInicioBancarias(), filtro.getSaldoInicioCaixas(), filtro.getSaldoInicioCartoes());

		this.calculaSaldoInicialOutros(filtro, listaFluxo);
		this.ajustaSaldoAC(filtro, listaFluxo);
		this.somaSaldos(filtro, listaFluxo, saldos);

		report.addParameter("saldoAC", filtro.getSaldoAC());
		listaFluxo = this.agrupaListaHeader(listaFluxo, filtro.getSaldoAC(), Util.booleans.isTrue(filtro.getMostrarDetalhes()), filtro.getValoresAnteriores(), filtro, true, true);

		report.addParameter("saldoInicioBancarias", filtro.getSaldoInicioBancarias());
		report.addParameter("saldoInicioCaixas", filtro.getSaldoInicioCaixas());
		report.addParameter("saldoInicioCartoes", filtro.getSaldoInicioCartoes());
		report.addParameter("saldoInicioOutros", filtro.getSaldoInicioOutrosResumo());
		
		report.addParameter("saldoFimBancarias", saldos.getSaldoBancarias());
		report.addParameter("saldoFimCaixas", saldos.getSaldoCaixas());
		report.addParameter("saldoFimCartoes", saldos.getSaldoCartoes());
		report.addParameter("saldoFimOutros", saldos.getSaldoOutros());

		report.addParameter("somaInicial", filtro.getSaldoInicioBancarias().add(filtro.getSaldoInicioCaixas().add(filtro.getSaldoInicioCartoes())).add(filtro.getSaldoInicioOutrosResumo()));
		report.addParameter("somaFinal", saldos.somaSaldos());
		report.addParameter("saldoInicialInseridoManualmente", new Boolean(filtro.getSaldoACTela() != null));
		
		report.addParameter("observacoesFluxoCaixa", observacoesFluxoCaixa(filtro.getSaldoACTela() != null));

		listaFluxo = this.ajustaListaFluxoCaixaValoresAnteriores(filtro, listaFluxo);
		
		report.setDataSource(listaFluxo);
		
		this.insereLinhaTotal(listaFluxo);		

		return report;
	}
	
	private void ajustaListaFluxocaixaAnalitoOrigem(List<FluxocaixaBeanReport> listaFluxo) {
		if(SinedUtil.isListNotEmpty(listaFluxo)){
			for(FluxocaixaBeanReport fluxocaixaBean : listaFluxo){
				if(Tipooperacao.TIPO_CREDITO.equals(fluxocaixaBean.getTipooperacao())){
					if(FluxocaixaBeanReport.A.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("AC");
					}else if(FluxocaixaBeanReport.C.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("CR");
					}else if(FluxocaixaBeanReport.M.equals(fluxocaixaBean.getIndicador())){
						if(Movimentacaoacao.CONCILIADA.equals(fluxocaixaBean.getMovimentacaoacao())){
							fluxocaixaBean.setIndicadorOrigem("MCC");
						}else if(Movimentacaoacao.NORMAL.equals(fluxocaixaBean.getMovimentacaoacao())){
							fluxocaixaBean.setIndicadorOrigem("MNC");
						}
					}else if(FluxocaixaBeanReport.PV.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("PV");
					}else if(FluxocaixaBeanReport.CT.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("CT");
					}else if(FluxocaixaBeanReport.DP.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("DPC");
					}
				}else if(Tipooperacao.TIPO_DEBITO.equals(fluxocaixaBean.getTipooperacao())){
					if(FluxocaixaBeanReport.A.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("AD");
					}else if(FluxocaixaBeanReport.C.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("CP");
					}else if(FluxocaixaBeanReport.M.equals(fluxocaixaBean.getIndicador())){
						if(Movimentacaoacao.CONCILIADA.equals(fluxocaixaBean.getMovimentacaoacao())){
							fluxocaixaBean.setIndicadorOrigem("MCD");
						}else if(Movimentacaoacao.NORMAL.equals(fluxocaixaBean.getMovimentacaoacao())){
							fluxocaixaBean.setIndicadorOrigem("MND");
						}
					}else if(FluxocaixaBeanReport.DP.equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("DPD");
					}else if("OC".equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("OC");
					}else if("CF".equals(fluxocaixaBean.getIndicador())){
						fluxocaixaBean.setIndicadorOrigem("CF");
					}
				}
			}
			
			Collections.sort(listaFluxo, new Comparator<FluxocaixaBeanReport>(){
				@Override
				public int compare(FluxocaixaBeanReport o1, FluxocaixaBeanReport o2) {
					int compare = o1.getData().compareTo(o2.getData());
					if(o1.getDataAtrasada() != null && o2.getDataAtrasada() != null){
						compare = o1.getDataAtrasada().compareTo(o2.getDataAtrasada());
					}
					if(compare == 0 && o2.getIndicadorOrigem() != null && o1.getIndicadorOrigem() != null){
						compare = o1.getIndicadorOrigem().compareTo(o2.getIndicadorOrigem());
					}
					if(compare == 0){
						compare = o1.getCompareValue().compareTo(o2.getCompareValue());
					}
					return compare;
				}
			});
		}
	}
	
	public IReport createReportFluxocaixaOrigem(WebRequestContext request, FluxocaixaFiltroReport filtro) {

		if (filtro.getMostrarDetalhes() != null && filtro.getMostrarDetalhes()) {
			return createReportFluxocaixaAnaliticoOrigem(request, filtro);
		} else {
			preparaRelatorioFluxoCaixa(filtro);
			
			Report report = new Report("/financeiro/fluxocaixa_origem");
	
			report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getPeriodoDe(), filtro.getPeriodoAte()));
			report.addParameter("periodoDe", filtro.getPeriodoDe());
			report.addParameter("periodoAte", filtro.getPeriodoAte());
			report.addParameter("dataAnterior", filtro.getDtInicioBase());
			report.addParameter("saldoAC", filtro.getSaldoAC());
			
			List<FluxocaixaBeanReport> listaFluxoCaixa = this.montaListaDadosRelatorio(filtro);
			this.ajustaSaldoAC(filtro, listaFluxoCaixa);
			
			List<FluxocaixaOrigemBeanReport> listaFluxo = montaListaDadosRelatorioOrigem(listaFluxoCaixa, filtro.getSaldoAC());
			
			report.setDataSource(listaFluxo);
			
			FluxocaixaOrigemBeanReport beanTotal = criaFluxocaixaOrigemBeanReportTotal(listaFluxo);
			report.addParameter("total_credito_cr", new Money(beanTotal.getVl_credito_cr()));
			report.addParameter("total_credito_chq",new Money(beanTotal.getVl_credito_chq()));
			report.addParameter("total_credito_cc", new Money(beanTotal.getVl_credito_cc()));
			report.addParameter("total_credito_ct", new Money(beanTotal.getVl_credito_ct()));
			report.addParameter("total_credito_ac", new Money(beanTotal.getVl_credito_ac()));
			report.addParameter("total_credito_mnc", new Money(beanTotal.getVl_credito_mnc()));
			report.addParameter("total_credito_mcc", new Money(beanTotal.getVl_credito_mcc()));
			report.addParameter("total_credito_pv", new Money(beanTotal.getVl_credito_pv()));
			report.addParameter("total_credito_dpc", new Money(beanTotal.getVl_credito_dpc()));
			
			report.addParameter("total_debito_cp", new Money(beanTotal.getVl_debito_cp()));
			report.addParameter("total_debito_oc", new Money(beanTotal.getVl_debito_oc()));
			report.addParameter("total_debito_ad", new Money(beanTotal.getVl_debito_ad()));
			report.addParameter("total_debito_cf", new Money(beanTotal.getVl_debito_cf()));
			report.addParameter("total_debito_mnd", new Money(beanTotal.getVl_debito_mnd()));
			report.addParameter("total_debito_mcd", new Money(beanTotal.getVl_debito_mcd()));
			report.addParameter("total_debito_dpd", new Money(beanTotal.getVl_debito_dpd()));
			
			report.addParameter("total_saldo_dia", new Money(beanTotal.getVl_saldo_dia()));
			report.addParameter("total_saldo_acumulado", new Money(beanTotal.getVl_saldo_acumulado()));

			return report;
		}
	}
	
	private FluxocaixaOrigemBeanReport criaFluxocaixaOrigemBeanReportTotal(List<FluxocaixaOrigemBeanReport> listaFluxo){
		Double total_credito_cr = 0d;
		Double total_credito_chq = 0d;
		Double total_credito_cc = 0d;
		Double total_credito_ct = 0d;
		Double total_credito_ac = 0d;
		Double total_credito_mnc = 0d;
		Double total_credito_mcc = 0d;
		Double total_credito_pv = 0d;
		Double total_credito_dpc = 0d;
		
		Double total_debito_cp = 0d;
		Double total_debito_oc = 0d;
		Double total_debito_ad = 0d;
		Double total_debito_cf = 0d;
		Double total_debito_mnd = 0d;
		Double total_debito_mcd = 0d;
		Double total_debito_dpd = 0d;
		
		Double total_saldo_dia = 0d;
		Money total_saldo_acumulado = new Money();
		
		if(SinedUtil.isListNotEmpty(listaFluxo)){
			total_saldo_acumulado = listaFluxo.get(listaFluxo.size()-1).getVl_saldo_acumulado();
		
			for(FluxocaixaOrigemBeanReport bean : listaFluxo){
				if(bean.getVl_credito_cr() != null) total_credito_cr += bean.getVl_credito_cr().getValue().doubleValue();
				if(bean.getVl_credito_chq() != null) total_credito_chq += bean.getVl_credito_chq().getValue().doubleValue();
				if(bean.getVl_credito_cc() != null) total_credito_cc += bean.getVl_credito_cc().getValue().doubleValue();
				if(bean.getVl_credito_ct() != null) total_credito_ct += bean.getVl_credito_ct().getValue().doubleValue();
				if(bean.getVl_credito_ac() != null) total_credito_ac += bean.getVl_credito_ac().getValue().doubleValue();
				if(bean.getVl_credito_mnc() != null) total_credito_mnc += bean.getVl_credito_mnc().getValue().doubleValue();
				if(bean.getVl_credito_mcc() != null) total_credito_mcc += bean.getVl_credito_mcc().getValue().doubleValue();
				if(bean.getVl_credito_pv() != null) total_credito_pv += bean.getVl_credito_pv().getValue().doubleValue();
				if(bean.getVl_credito_dpc() != null) total_credito_dpc += bean.getVl_credito_dpc().getValue().doubleValue();
				
				if(bean.getVl_debito_cp() != null) total_debito_cp += bean.getVl_debito_cp().getValue().doubleValue();
				if(bean.getVl_debito_oc() != null) total_debito_oc += bean.getVl_debito_oc().getValue().doubleValue();
				if(bean.getVl_debito_ad() != null) total_debito_ad += bean.getVl_debito_ad().getValue().doubleValue();
				if(bean.getVl_debito_cf() != null) total_debito_cf += bean.getVl_debito_cf().getValue().doubleValue();
				if(bean.getVl_debito_mnd() != null) total_debito_mnd += bean.getVl_debito_mnd().getValue().doubleValue();
				if(bean.getVl_debito_mcd() != null) total_debito_mcd += bean.getVl_debito_mcd().getValue().doubleValue();
				if(bean.getVl_debito_dpd() != null) total_debito_dpd += bean.getVl_debito_dpd().getValue().doubleValue();
				
				if(bean.getVl_saldo_dia() != null) total_saldo_dia += bean.getVl_saldo_dia().getValue().doubleValue();
			}
		}
			
		FluxocaixaOrigemBeanReport beanTotal = new FluxocaixaOrigemBeanReport();
		beanTotal.setVl_credito_cr(new Money(total_credito_cr));
		beanTotal.setVl_credito_chq(new Money(total_credito_chq));
		beanTotal.setVl_credito_cc(new Money(total_credito_cc));
		beanTotal.setVl_credito_ct(new Money(total_credito_ct));
		beanTotal.setVl_credito_ac(new Money(total_credito_ac));
		beanTotal.setVl_credito_mnc(new Money(total_credito_mnc));
		beanTotal.setVl_credito_mcc(new Money(total_credito_mcc));
		beanTotal.setVl_credito_pv(new Money(total_credito_pv));
		beanTotal.setVl_credito_dpc(new Money(total_credito_dpc));
		
		beanTotal.setVl_debito_cp(new Money(total_debito_cp));
		beanTotal.setVl_debito_oc(new Money(total_debito_oc));
		beanTotal.setVl_debito_ad(new Money(total_debito_ad));
		beanTotal.setVl_debito_cf(new Money(total_debito_cf));
		beanTotal.setVl_debito_mnd(new Money(total_debito_mnd));
		beanTotal.setVl_debito_mcd(new Money(total_debito_mcd));
		beanTotal.setVl_debito_dpd(new Money(total_debito_dpd));
		
		beanTotal.setVl_saldo_dia(new Money(total_saldo_dia));
		beanTotal.setVl_saldo_acumulado(new Money(total_saldo_acumulado));
		
		return beanTotal;
	}

	private List<FluxocaixaOrigemBeanReport> montaListaDadosRelatorioOrigem(List<FluxocaixaBeanReport> listaFluxocaixaBean, Money saldoAC) {
		List<FluxocaixaOrigemBeanReport> listaReport = new ArrayList<FluxocaixaOrigemBeanReport>();
		if(SinedUtil.isListNotEmpty(listaFluxocaixaBean)){
			Iterator<FluxocaixaBeanReport> iterator = listaFluxocaixaBean.iterator();
			Date data = null;
			FluxocaixaOrigemBeanReport bean = null;
			if(saldoAC == null) saldoAC = new Money();
			while (iterator.hasNext()) {
				FluxocaixaBeanReport fluxocaixaBean = iterator.next();
				if(data == null){
					data = fluxocaixaBean.getDataAtrasadaOuData();
				}
				if(data.equals(fluxocaixaBean.getDataAtrasadaOuData())){
					if(bean == null){
						bean = new FluxocaixaOrigemBeanReport();
						bean.setData(fluxocaixaBean.getData());
						bean.setDataAtrasada(fluxocaixaBean.getDataAtrasada());
						listaReport.add(bean);
					}
				}else {
					saldoAC = saldoAC.add(bean.getVl_saldo_dia());
					bean.setVl_saldo_acumulado(saldoAC);
					
					data = fluxocaixaBean.getDataAtrasadaOuData();
					
					bean = new FluxocaixaOrigemBeanReport();
					bean.setData(fluxocaixaBean.getData());
					bean.setDataAtrasada(fluxocaixaBean.getDataAtrasada());
					bean.setVl_saldo_acumulado(saldoAC);
					listaReport.add(bean);
				}
				
				addValor(fluxocaixaBean, bean);
				
				iterator.remove();
			}
			if(bean != null){
				saldoAC = saldoAC.add(bean.getVl_saldo_dia());
				bean.setVl_saldo_acumulado(saldoAC);
			}
		}
		return listaReport;
	}
	
	private void addValor(FluxocaixaBeanReport fluxocaixaBean, FluxocaixaOrigemBeanReport bean) {
		if(Tipooperacao.TIPO_CREDITO.equals(fluxocaixaBean.getTipooperacao())){
			if(FluxocaixaBeanReport.A.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_ac(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.C.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_cr(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.M.equals(fluxocaixaBean.getIndicador())){
				if(Movimentacaoacao.CONCILIADA.equals(fluxocaixaBean.getMovimentacaoacao())){
					bean.addVl_credito_mcc(fluxocaixaBean.getCredito());
				}else if(Movimentacaoacao.NORMAL.equals(fluxocaixaBean.getMovimentacaoacao())){
					bean.addVl_credito_mnc(fluxocaixaBean.getCredito());
				}
			}else if(FluxocaixaBeanReport.PV.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_pv(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.CT.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_ct(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.DP.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_dpc(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.CH.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_chq(fluxocaixaBean.getCredito());
			}else if(FluxocaixaBeanReport.CC.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_credito_cc(fluxocaixaBean.getCredito());
			}
		}else if(Tipooperacao.TIPO_DEBITO.equals(fluxocaixaBean.getTipooperacao())){
			if(FluxocaixaBeanReport.A.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_debito_ad(fluxocaixaBean.getDebito());
			}else if(FluxocaixaBeanReport.C.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_debito_cp(fluxocaixaBean.getDebito());
			}else if(FluxocaixaBeanReport.M.equals(fluxocaixaBean.getIndicador())){
				if(Movimentacaoacao.CONCILIADA.equals(fluxocaixaBean.getMovimentacaoacao())){
					bean.addVl_debito_mcd(fluxocaixaBean.getDebito());
				}else if(Movimentacaoacao.NORMAL.equals(fluxocaixaBean.getMovimentacaoacao())){
					bean.addVl_debito_mnd(fluxocaixaBean.getDebito());
				}
			}else if(FluxocaixaBeanReport.DP.equals(fluxocaixaBean.getIndicador())){
				bean.addVl_debito_dpd(fluxocaixaBean.getDebito());
			}else if("OC".equals(fluxocaixaBean.getIndicador())){
				bean.addVl_debito_oc(fluxocaixaBean.getDebito());
			}else if("CF".equals(fluxocaixaBean.getIndicador())){
				bean.addVl_debito_cf(fluxocaixaBean.getDebito());
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.FluxocaixaDAO#existeFluxocaixa(Ordemcompra ordemcompra)
	*
	* @param ordemcompra
	* @return
	* @since 13/01/2015
	* @author Luiz Fernando
	*/
	public boolean existeFluxocaixa(Ordemcompra ordemcompra) {
		return fluxocaixaDAO.existeFluxocaixa(ordemcompra);
	}
	
	public String observacoesFluxoCaixa(boolean saldoInicialInformadoManualmente){
		return (saldoInicialInformadoManualmente? "* Saldo inicial inserido manualmente\n":
												  "* Para c�lculo do saldo inicial � considerada todas as movimenta��es independente da natureza de conta gerencial informada no filtro.\n")+
			   "* O saldo final pode variar de acordo com o filtro informado na natureza de conta gerencial";
	}
}

/**
 * Classe respons�vel por controlar saldos de contas do relat�rio de fluxo de
 * caixa.
 * 
 * @author Fl�vio Tavares
 */
class Saldos {

	private Money saldoBancarias;
	private Money saldoCaixas;
	private Money saldoCartoes;
	private Money saldoOutros;

	public Saldos(Money saldoBancarias, Money saldoCaixas, Money saldoCartoes) {
		if (saldoBancarias == null || saldoCaixas == null || saldoCartoes == null) {
			throw new IllegalArgumentException("Nenhum saldo deve ser null.");
		}
		this.saldoBancarias = saldoBancarias;
		this.saldoCaixas = saldoCaixas;
		this.saldoCartoes = saldoCartoes;
		this.saldoOutros = new Money();
	}

	public Money getSaldoBancarias() {
		return saldoBancarias;
	}

	public Money getSaldoCaixas() {
		return saldoCaixas;
	}

	public Money getSaldoCartoes() {
		return saldoCartoes;
	}
	
	public Money getSaldoOutros() {
		return saldoOutros;
	}

	/**
	 * Retorna a soma de todos os saldos.
	 * 
	 * @return
	 */
	public Money somaSaldos() {
		return saldoCaixas.add(saldoBancarias.add(saldoCartoes.add(saldoOutros)));
	}

	/**
	 * Soma no saldo de caixas.
	 * 
	 * @param add
	 */
	public void addSaldoCaixas(Money add) {
		saldoCaixas = saldoCaixas.add(add);
	}

	/**
	 * Subtra��o no saldo de caixas.
	 * 
	 * @param add
	 */
	public void subtractSaldoCaixas(Money subtract) {
		saldoCaixas = saldoCaixas.subtract(subtract);
	}

	/**
	 * Soma no saldo de cart�es de cr�dito.
	 * 
	 * @param add
	 */
	public void addSaldoCartoes(Money add) {
		saldoCartoes = saldoCartoes.add(add);
	}

	/**
	 * Subtra��o no saldo de cart�es.
	 * 
	 * @param add
	 */
	public void subtractSaldoCartoes(Money subtract) {
		saldoCartoes = saldoCartoes.subtract(subtract);
	}

	/**
	 * Soma no saldo de contas banc�rias.
	 * 
	 * @param add
	 */
	public void addSaldoBancarias(Money add) {
		saldoBancarias = saldoBancarias.add(add);
	}

	/**
	 * Subtra��o no saldo de contas banc�rias.
	 * 
	 * @param add
	 */
	public void subtractSaldoBancarias(Money subtract) {
		saldoBancarias = saldoBancarias.subtract(subtract);
	}
	
	public void addSaldoOutros(Money add) {
		saldoOutros = saldoOutros.add(add);
	}
	
	public void subtractSaldoOutros(Money subtract) {
		saldoOutros = saldoOutros.subtract(subtract);
	}

}
