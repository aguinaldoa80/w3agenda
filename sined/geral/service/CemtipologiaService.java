package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.geral.dao.CemtipologiaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CemtipologiaService extends GenericService<Cemtipologia>{

	private CemtipologiaDAO cemtipologiaDAO;
	
	public void setCemtipologiaDAO(CemtipologiaDAO cemtipologiaDAO) {
		this.cemtipologiaDAO = cemtipologiaDAO;
	}
	
	public List<Cemtipologia> findByCemobra(Cemobra cemobra) {
		return cemtipologiaDAO.findByCemobra(cemobra);
	}
	

}
