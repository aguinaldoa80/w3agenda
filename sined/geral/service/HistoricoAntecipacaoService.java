package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.dao.HistoricoAntecipacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class HistoricoAntecipacaoService extends GenericService<HistoricoAntecipacao>{

	private HistoricoAntecipacaoDAO historicoAntecipacaoDAO;
	
	public void setHistoricoAntecipacaoDAO(HistoricoAntecipacaoDAO historicoAntecipacaoDAO) {
		this.historicoAntecipacaoDAO = historicoAntecipacaoDAO;
	}
	
	public List<HistoricoAntecipacao> findByHistoricoAntecipacao(String whereIn) {
		return historicoAntecipacaoDAO.findByHistoricoAntecipacao(whereIn);
	}

	public List<HistoricoAntecipacao> findByDocumentoReferencia(Documento documento) {
		return historicoAntecipacaoDAO.findByDocumentoReferencia(documento, false);
	}
	
	public List<HistoricoAntecipacao> findByDocumentoReferencia(Documento documento, Boolean isCompensacao) {
		return historicoAntecipacaoDAO.findByDocumentoReferencia(documento, isCompensacao);
	}
	
	public List<HistoricoAntecipacao> findByDocumento(Documento documento) {
		return historicoAntecipacaoDAO.findByDocumento(documento);
	}

	public Boolean existeAntecipacao(Documento documento) {
		if(documento == null || documento.getCddocumento() == null) return false;
		return historicoAntecipacaoDAO.existeAntecipacao(documento.getCddocumento().toString());
	}
	
	public Boolean existeAntecipacao(String whereIn) {
		return historicoAntecipacaoDAO.existeAntecipacao(whereIn);
	}
	
	public boolean isContaGeradaAPartirDeCompensacaoDeSaldo(Documento documento) {
		return historicoAntecipacaoDAO.isContaGeradaAPartirDeCompensacaoDeSaldo(documento);
				
	}
}