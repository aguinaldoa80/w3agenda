package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Diarioobramdo;
import br.com.linkcom.sined.geral.dao.DiarioobramdoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DiarioobramdoService extends GenericService<Diarioobramdo>{

	private DiarioobramdoDAO diarioobramdoDAO;
		
	public void setDiarioobramdoDAO(DiarioobramdoDAO diarioobramdoDAO) {
		this.diarioobramdoDAO = diarioobramdoDAO;	}
	

	/**
	* Faz referÍncia ao DAO
	* 
	* @see	br.com.linkcom.sined.geral.dao.DiarioobramdoDAO#findListDiarioobramdo(Integer cddiarioobra)
	*
	* @param cddiarioobra
	* @return
	* @since Jul 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Diarioobramdo> findListDiarioobramdo(Integer cddiarioobra) {		
		return diarioobramdoDAO.findListDiarioobramdo(cddiarioobra);
	}
}
