package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.geral.dao.EscalaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.util.Calendar;

public class EscalaService extends GenericService<Escala> {

	private EscalaDAO escalaDAO;
	
	public void setEscalaDAO(EscalaDAO escalaDAO) {
		this.escalaDAO = escalaDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escala findEscalaDoVeiculo(Veiculo veiculo){
		return escalaDAO.findEscalaHorarioDoVeiculo(veiculo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param pessoa
	 * @return
	 * @author Tom�s Rabelo
	 *//*
	public Escala findEscalaDoColaborador(Colaborador colaborador, Date dtinicio, Date dtfim) {
		return escalaDAO.findEscalaDoColaborador(colaborador, dtinicio, dtfim);
	}*/

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param escala
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escala carregaEscala(Escala escala) {
		return escalaDAO.carregaEscala(escala);
	}

	/**
	 * Prepara para gerar escala levando em considera��o se foi selecionado dias da semana ou n�o
	 * 
	 * @param bean
	 */
	public void prepareGerarEscala(Escala bean) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(bean.getHoraInicio());
		if(bean.getListaDiassemana() == null)
			bean.setListaescalahorario(new ArrayList<Escalahorario>());
		
		if(bean.getListaDiassemana() != null && !bean.getListaDiassemana().isEmpty()){
			for (Object dia : bean.getListaDiassemana()) {
				DiaSemana diaSemana = DiaSemana.getDiasemanaByString(dia.toString());
				calendar.setTime(bean.getHoraInicio());
				gerarEscala(bean, diaSemana, calendar);
			}
		}else{
			gerarEscala(bean, null, calendar);
		}
	}

	/**
	 * Gera a escala de hor�rio 
	 * 
	 * @param bean
	 * @param diaSemana
	 * @param calendar
	 */
	private void gerarEscala(Escala bean, DiaSemana diaSemana, Calendar calendar) {
		if(bean.getIntervalo() == null || bean.getIntervalo() < 10){
			bean.setIntervalo(10);
		}
		
		List<Escalahorario> listaEscala = new ListSet<Escalahorario>(Escalahorario.class);
		while (calendar.getTime().compareTo(bean.getHoraFim()) <= 0) {
			Escalahorario escalahorario = new Escalahorario();
			
			escalahorario.setHorainicio(new Hora(calendar.getTimeInMillis()));
			escalahorario.setDiasemana(diaSemana);
			
			calendar.add(Calendar.MINUTE, bean.getIntervalo());
			escalahorario.setHorafim(new Hora(calendar.getTimeInMillis()));
			
			if(calendar.getTime().compareTo(bean.getHoraFim()) > 0){
				break;
			}
			
			listaEscala.add(escalahorario);
		}
		bean.getListaescalahorario().addAll(listaEscala);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param pessoa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escala> findByColaborador(Colaborador colaborador) {
		return escalaDAO.findByColaborador(colaborador);
	}
	
	/**
	 * M�todo para preparar/gerar a escala no flex
	 *
	 * @param horainicio
	 * @param horafinal
	 * @param intervalo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Escalahorario> prepareGerarEscalaFlex(String horainicio, String horafinal, Integer intervalo) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( new Hora(horainicio));
				
		Escala bean = new Escala();
		bean.setIntervalo(intervalo);
		bean.setHoraInicio( new Hora(horainicio));
		bean.setHoraFim( new Hora(horafinal));
		bean.setListaescalahorario(new ArrayList<Escalahorario>());
		
		return gerarEscalaFlex(bean, null, calendar);		
	}
	
	/**
	 * M�todo que gera a escala horario no flex
	 *
	 * @param bean
	 * @param diaSemana
	 * @param calendar
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Escalahorario> gerarEscalaFlex(Escala bean, DiaSemana diaSemana, Calendar calendar) {
		if(bean.getIntervalo() == null || bean.getIntervalo() < 10){
			bean.setIntervalo(10);
		}
		
		List<Escalahorario> listaEscala = new ListSet<Escalahorario>(Escalahorario.class);
		while (calendar.getTime().compareTo(bean.getHoraFim()) < 0) {
			Escalahorario escalahorario = new Escalahorario();
			
			escalahorario.setHorainicio(new Hora(calendar.getTimeInMillis()));
			escalahorario.setDiasemana(diaSemana);
			
			calendar.add(Calendar.MINUTE, bean.getIntervalo());
			escalahorario.setHorafim(new Hora(calendar.getTimeInMillis()));
			
			escalahorario.setHorafimstring(escalahorario.getHorafim().toString());
			escalahorario.setHorainiciostring(escalahorario.getHorainicio().toString());
			
			if(calendar.getTime().compareTo(bean.getHoraFim()) > 0 && bean.getHoraFim().compareTo(escalahorario.getHorafim()) < 0){
				escalahorario.setHorafim(bean.getHoraFim());
				escalahorario.setHorafimstring(bean.getHoraFim().toString());
			}
			
			listaEscala.add(escalahorario);
		}
		return listaEscala;
	}
	
	public List<Escala> carregaListaEscala(String whereIn) {
		return escalaDAO.carregaListaEscala(whereIn);
	}
}
