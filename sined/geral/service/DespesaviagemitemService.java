package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.geral.bean.Despesaviagemtipo;
import br.com.linkcom.sined.geral.dao.DespesaviagemitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DespesaviagemitemService extends GenericService<Despesaviagemitem> {

	private DespesaviagemitemDAO despesaviagemitemDAO;
	
	public void setDespesaviagemitemDAO(
			DespesaviagemitemDAO despesaviagemitemDAO) {
		this.despesaviagemitemDAO = despesaviagemitemDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemitemDAO#updateValoresRealizadas
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateZerarValoresRealizadas(String whereIn) {
		despesaviagemitemDAO.updateZerarValoresRealizadas(whereIn);
	}

	public List<Despesaviagemitem> agrupaItensForRealizacao(List<Despesaviagemitem> listaDespesaviagemitem) {
		Map<Despesaviagemtipo, List<Despesaviagemitem>> map = new HashMap<Despesaviagemtipo, List<Despesaviagemitem>>();
		for (Despesaviagemitem despesaviagemitem : listaDespesaviagemitem) {
			if(despesaviagemitem != null && 
					despesaviagemitem.getDespesaviagemtipo() != null && 
					despesaviagemitem.getDespesaviagemtipo().getCddespesaviagemtipo() != null){
				if(map.containsKey(despesaviagemitem.getDespesaviagemtipo())){
					List<Despesaviagemitem> lista = map.get(despesaviagemitem.getDespesaviagemtipo());
					lista.add(despesaviagemitem);
					
					map.put(despesaviagemitem.getDespesaviagemtipo(), lista);
				} else {
					List<Despesaviagemitem> lista = new ArrayList<Despesaviagemitem>();
					lista.add(despesaviagemitem);
					
					map.put(despesaviagemitem.getDespesaviagemtipo(), lista);
				}
			}
		}
		
		List<Despesaviagemitem> listaDespesaviagemitemAgrupado = new ArrayList<Despesaviagemitem>();
		
		Set<Entry<Despesaviagemtipo, List<Despesaviagemitem>>> entrySet = map.entrySet();
		for (Entry<Despesaviagemtipo, List<Despesaviagemitem>> entry : entrySet) {
			Money valorprevisto = new Money();
			
			List<Despesaviagemitem> lista = entry.getValue();
			for (Despesaviagemitem despesaviagemitem : lista) {
				valorprevisto = valorprevisto.add(despesaviagemitem.getValorprevisto());
			}
			
			Despesaviagemitem it = new Despesaviagemitem();
			it.setDespesaviagemtipo(entry.getKey());
			it.setValorprevisto(valorprevisto);
			it.setValorrealizado(new Money());
			it.setWhereInItens(CollectionsUtil.listAndConcatenate(lista, "cddespesaviagemitem", ","));
			
			listaDespesaviagemitemAgrupado.add(it);
		}
		
		return listaDespesaviagemitemAgrupado;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemitemDAO#findForSaveRealizacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/11/2012
	 */
	public List<Despesaviagemitem> findForSaveRealizacao(String whereIn) {
		return despesaviagemitemDAO.findForSaveRealizacao(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DespesaviagemitemDAO#updateValorRealizado(Despesaviagemitem despesaviagemitem, Money valorrealizado)
	 *
	 * @param despesaviagemitem
	 * @param valorrealizado
	 * @author Rodrigo Freitas
	 * @since 03/12/2012
	 */
	public void updateValorRealizado(Despesaviagemitem despesaviagemitem, Money valorrealizado) {
		despesaviagemitemDAO.updateValorRealizado(despesaviagemitem, valorrealizado);
	}

}
