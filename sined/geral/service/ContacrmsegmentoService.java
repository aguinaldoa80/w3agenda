package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmsegmento;
import br.com.linkcom.sined.geral.dao.ContacrmsegmentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmsegmentoService extends GenericService<Contacrmsegmento>{
	
	protected ContacrmsegmentoDAO contacrmsegmentoDAO;
	
	public void setContacrmsegmentoDAO(ContacrmsegmentoDAO contacrmsegmentoDAO) {
		this.contacrmsegmentoDAO = contacrmsegmentoDAO;
	}
	
	
	
	public List<Contacrmsegmento> findByContacrm(Contacrm contacrm){
		return contacrmsegmentoDAO.findByContacrm(contacrm);
	}

}
