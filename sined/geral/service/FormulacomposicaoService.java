package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Formulacomposicao;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormulacomposicaoService extends GenericService<Formulacomposicao>{

	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FormulacomposicaoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	public List<Formulacomposicao> findAllForFlex(){
		return this.findAllForFlex(null);
	}	
	
	public List<Formulacomposicao> findAllForFlex(Orcamento orcamento){
		List<Formulacomposicao> lista = new ArrayList<Formulacomposicao>();
		if(orcamento == null || orcamento.getCalcularhistograma()){
			lista.add(Formulacomposicao.MEDIA);
			lista.add(Formulacomposicao.TOTAL);
			lista.add(Formulacomposicao.VALOR_MAXIMO);
			
			return lista;
		} else {
			lista.add(Formulacomposicao.TOTAL);
			
			return lista;
		}
	}
	
}
