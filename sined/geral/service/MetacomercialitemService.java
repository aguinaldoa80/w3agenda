package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.geral.bean.Metacomercialitem;
import br.com.linkcom.sined.geral.dao.MetacomercialitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MetacomercialitemService extends GenericService<Metacomercialitem> {

	private MetacomercialitemDAO metacomercialitemDAO;
	
	public void setMetacomercialitemDAO(MetacomercialitemDAO metacomercialitemDAO) {
		this.metacomercialitemDAO = metacomercialitemDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MetacomercialitemDAO#existsColaboradorPeriodo(Metacomercial metacomercial, Colaborador colaborador, Date dtinicio, Date dtfim)
	 *
	 * @param metacomercial
	 * @param colaborador
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsColaboradorPeriodo(Metacomercial metacomercial, Colaborador colaborador, Date dtinicio, Date dtfim) {
		return metacomercialitemDAO.existsColaboradorPeriodo(metacomercial, colaborador, dtinicio, dtfim);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MetacomercialitemService#existsCargoPeriodo(Metacomercial metacomercial, Cargo cargo, Date dtinicio, Date dtfim)
	 *
	 * @param metacomercial
	 * @param cargo
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsCargoPeriodo(Metacomercial metacomercial, Cargo cargo, Date dtinicio, Date dtfim) {
		return metacomercialitemDAO.existsCargoPeriodo(metacomercial, cargo, dtinicio, dtfim);
	}
	
	public List<Metacomercialitem> findForColaboradores(Metacomercial metacomercial){
		return metacomercialitemDAO.findForColaboradores(metacomercial);
	}

}
