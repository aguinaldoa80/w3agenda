package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import br.com.linkcom.lknfe.xml.nfe.ConsSitNFe;
import br.com.linkcom.lknfe.xml.nfse.abrasf.ConsultarNfseRpsEnvio;
import br.com.linkcom.lknfe.xml.nfse.abrasf.CpfCnpj;
import br.com.linkcom.lknfe.xml.nfse.abrasf.IdentificacaoRps;
import br.com.linkcom.lknfe.xml.nfse.abrasf.Prestador;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NumeroNfeNaoUtilizadoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MarcarArquivonfnotaBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivonfnotaService extends GenericService<Arquivonfnota> {

	private ArquivonfnotaDAO arquivonfnotaDAO;
	private EmpresaService empresaService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setArquivonfnotaDAO(ArquivonfnotaDAO arquivonfnotaDAO) {
		this.arquivonfnotaDAO = arquivonfnotaDAO;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByArquivonf(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @return
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByArquivonf(Arquivonf arquivonf) {
		return arquivonfnotaDAO.findByArquivonf(arquivonf);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateNumeroCodigo(Arquivonfnota arquivonfnota, String numeroNfse, String codigoVerificacao, Timestamp dataEmissao, Date dataCompetencia)
	 *
	 * @param arquivonfnota
	 * @param numeroNfse
	 * @param codigoVerificacao
	 * @param dataEmissao
	 * @param dataCompetencia
	 * @since 23/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumeroCodigo(Arquivonfnota arquivonfnota, String numeroNfse, String codigoVerificacao, Timestamp dataEmissao, Date dataCompetencia) {
		arquivonfnotaDAO.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao, dataEmissao, dataCompetencia);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateNumeroCodigo(Arquivonfnota arquivonfnota, String numeroNfse, String codigoVerificacao, Timestamp dataEmissao, Date dataCompetencia)
	 *
	 * @param arquivonfnota
	 * @param numeroNfse
	 * @param codigoVerificacao
	 * @since 29/05/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumeroCodigo(Arquivonfnota arquivonfnota, String numeroNfse, String codigoVerificacao) {
		arquivonfnotaDAO.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao, null, null);
	}
	
	public void updateNumero(Arquivonfnota arquivonfnota, String numeroNfse) {
		arquivonfnotaDAO.updateNumeroCodigo(arquivonfnota, numeroNfse, null, null, null);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateArquivoxmlcancelamento(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlcancelamento(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateArquivoxmlcancelamento(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findForCancelamento()
	 *
	 * @return
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findForCancelamento() {
		return arquivonfnotaDAO.findForCancelamento();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#marcarArquivonfnota(MarcarArquivonfnotaBean bean)
	 *
	 * @param bean
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarArquivonfnota(String whereIn, String flag) {
		return arquivonfnotaDAO.marcarArquivonfnota(whereIn, flag);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#desmarcarArquivonfnota(MarcarArquivonfnotaBean bean)
	 *
	 * @param bean
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void desmarcarArquivonfnota(Integer cdarquivonfnota, String flag) {
		arquivonfnotaDAO.desmarcarArquivonfnota(cdarquivonfnota, flag);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateArquivoretornocancelamento(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornocancelamento(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateArquivoretornocancelamento(arquivonfnota);
	}
	
	public void updateArquivoxmlretornoconsulta(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateArquivoxmlretornoconsulta(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#loadForCancelamento(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota loadForCancelamento(Arquivonfnota arquivonfnota) {
		return arquivonfnotaDAO.loadForCancelamento(arquivonfnota);
	}
	
	public Arquivonfnota loadForConsulta(Arquivonfnota arquivonfnota) {
		return arquivonfnotaDAO.loadForConsulta(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateCancelado(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateCancelado(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateCancelado(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * Para Nota de produto usar findByNotaProduto
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByNotaNotCancelada(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByNotaNotCancelada(Nota nota) {
		return arquivonfnotaDAO.findByNotaNotCancelada(nota, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByNotaNotCancelada(Nota nota, boolean dtcancelamentoNulo)
	*
	* @param nota
	* @return
	* @since 22/11/2016
	* @author Luiz Fernando
	*/
	public Arquivonfnota findByNotaCanceladaNaPrefeitura(Nota nota) {
		return arquivonfnotaDAO.findByNotaNotCancelada(nota, false);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByNotaNotCancelada(String whereIn)
	 *
	 * @param nota
	 * @return
	 * @since 26/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByNotaNotCancelada(String whereIn) {
		return arquivonfnotaDAO.findByNotaNotCancelada(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/07/2014
	 */
	public List<Arquivonfnota> findByNotaAndCancelada(String whereIn) {
		return arquivonfnotaDAO.findByNotaAndCancelada(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByNota(String whereIn) {
		return arquivonfnotaDAO.findByNota(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByChaveAcesso(Arquivonf arquivonf, String chNFe)
	 *
	 * @param arquivonf
	 * @param chNFeStr
	 * @return
	 * @since 22/03/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByChaveAcesso(Arquivonf arquivonf, String chNFe) {
		return arquivonfnotaDAO.findByChaveAcesso(arquivonf, chNFe);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateArquivonfnota(Arquivonfnota arquivonfnota, Timestamp dataRec, String nProtStr)
	 *
	 * @param arquivonfnota
	 * @param dataRec
	 * @param nProtStr
	 * @since 22/03/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivonfnota(Arquivonfnota arquivonfnota, Timestamp dataRec, String nProtStr) {
		arquivonfnotaDAO.updateArquivonfnota(arquivonfnota, dataRec, nProtStr);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateArquivoxml(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @since 26/03/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxml(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateArquivoxml(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findByNotaProduto(Notafiscalproduto nota)
	 *
	 * @param nf
	 * @return
	 * @since 09/04/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByNotaProduto(Notafiscalproduto nf) {
		return arquivonfnotaDAO.findByNotaProduto(nf);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/10/2012
	 */
	public Arquivonfnota findByNotaProdutoAndCancelada(Notafiscalproduto nf) {
		return arquivonfnotaDAO.findByNotaProdutoAndCancelada(nf);
	}
	
	public Arquivonfnota findByNotaServicoAndCancelada(NotaFiscalServico nfse) {
		return arquivonfnotaDAO.findByNotaServicoAndCancelada(nfse);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByNotaProduto(String whereIn) {
		return arquivonfnotaDAO.findByNotaProduto(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findForDownloadXml(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 26/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findForDownloadXml(String whereIn) {
		return arquivonfnotaDAO.findForDownloadXml(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findNovosCancelamentos(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findNovosCancelamentos(Empresa empresa) {
		return arquivonfnotaDAO.findNovosCancelamentos(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#findNovosCancelamentosPorEvento(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Arquivonfnota> findNovosCancelamentosPorEvento(Empresa empresa) {
		return arquivonfnotaDAO.findNovosCancelamentosPorEvento(empresa);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#removerCancelamento(Nota nota)
	 *
	 * @param nota
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public void removerCancelamento(Nota nota) {
		arquivonfnotaDAO.removerCancelamento(nota);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateCancelamentoPorEvento(Arquivonfnota arquivonfnota, Boolean cancelamentoPorEvento)
	 *
	 * @param arquivonfnota
	 * @param cancelamentoPorEvento
	 * @author Luiz Fernando
	 */
	public void updateCancelamentoPorEvento(Arquivonfnota arquivonfnota, Boolean cancelamentoPorEvento) {
		arquivonfnotaDAO.updateCancelamentoPorEvento(arquivonfnota, cancelamentoPorEvento);		
	}

	public void updateNumeroParacatu(Arquivonfnota arquivonfnota, String numeroNfse, Timestamp timestamp, String urlimpressaoparacatu) {
		arquivonfnotaDAO.updateNumeroParacatu(arquivonfnota, numeroNfse, timestamp, urlimpressaoparacatu);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaDAO#updateChaveacesso(Arquivonfnota arquivonfnota, String chNFeStr)
	 *
	 * @param arquivonfnota
	 * @param chNFeStr
	 * @author Rodrigo Freitas
	 * @since 02/01/2013
	 */
	public void updateChaveacesso(Arquivonfnota arquivonfnota, String chNFeStr) {
		arquivonfnotaDAO.updateChaveacesso(arquivonfnota, chNFeStr);
	}

	public Arquivonfnota findGeradaByNota(Nota nota) {
		return arquivonfnotaDAO.findGeradaByNota(nota);
	}

	public boolean haveNotaEmitida(String numeronfse, Configuracaonfe configuracaonfe, String cnpj) {
		Empresa empresa = empresaService.findByCnpj(new Cnpj(cnpj));
		return arquivonfnotaDAO.haveNotaEmitida(numeronfse, configuracaonfe, empresa);
	}
	
	public boolean haveNotaEmitida(String numeronfse, String codigoverificacao) {
		return arquivonfnotaDAO.haveNotaEmitida(numeronfse, codigoverificacao);
	}
	
	public boolean haveNotaEmitida(String numeronfse) {
		return arquivonfnotaDAO.haveNotaEmitida(numeronfse);
	}

	public List<Arquivonfnota> findArquivoNotaProcessadoByNota(String whereIn, NumeroNfeNaoUtilizadoFiltro filtro){
		return arquivonfnotaDAO.findArquivoNotaProcessadoByNota(whereIn, filtro);
	}

	public void updateTipocontigenciacancelamento(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateTipocontigenciacancelamento(arquivonfnota);
	}

	public void createArquivoXmlConsulta(Arquivonfnota arquivonfnota) {
		String xml = "";
		boolean oficial = arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().name().endsWith("PROD");
		
		ConsSitNFe consSitNFe = new ConsSitNFe();
		consSitNFe.setVersao("4.00");
		consSitNFe.setTpAmb(oficial);
		consSitNFe.setChNFe(arquivonfnota.getChaveacesso());
		
		xml = consSitNFe.toString();
		
		Arquivo arquivoXmlConsultaLote = new Arquivo(xml.getBytes(), "nfconsultanota_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		arquivonfnota.setArquivoxmlconsulta(arquivoXmlConsultaLote);
		
		arquivoDAO.saveFile(arquivonfnota, "arquivoxmlconsulta");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		this.updateArquivoxmlconsulta(arquivonfnota);
	}
	
	public void updateArquivoxmlconsulta(Arquivonfnota arquivonfnota) {
		arquivonfnotaDAO.updateArquivoxmlconsulta(arquivonfnota);
	}

	public List<Arquivonfnota> findArquivonfnotaConsulta(Empresa empresa) {
		return arquivonfnotaDAO.findArquivonfnotaConsulta(empresa);
	}

	public Arquivonfnota loadByCdarquivonfnota(Integer cdarquivonfnota) {
		return arquivonfnotaDAO.loadByCdarquivonfnota(cdarquivonfnota);
	}

	public Arquivonfnota loadUltimoWithArquivoNf(Notafiscalproduto nota) {
		return arquivonfnotaDAO.loadUltimoWithArquivoNf(nota);
	}

	public void createArquivoXmlConsultaBhiss(Arquivonfnota arquivonfnota) {
		Arquivonf arquivonf = arquivonfnota.getArquivonf();
		Configuracaonfe configuracaonfe = arquivonf.getConfiguracaonfe();
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		Nota nota = arquivonfnota.getNota();
		Empresa empresa = nota.getEmpresa();
		
		String cnpj = StringUtils.soNumero(empresa.getCnpj().getValue());
		String inscricaoMunicipal = configuracaonfe.getInscricaomunicipal();
		
		String xml = "";
		
		Prestador prestador = new Prestador(prefixowebservice.getPrefixo(), false, false, false);
		prestador.setCnpj(cnpj);
		prestador.setInscricaoMunicipal(inscricaoMunicipal);
		
		IdentificacaoRps identificacaoRps = new IdentificacaoRps(prefixowebservice.getPrefixo());
		identificacaoRps.setNumero(StringUtils.soNumero(nota.getNumero()));
		if(configuracaonfe.getSerienfse() != null && !configuracaonfe.getSerienfse().equals("")){
			identificacaoRps.setSerie(configuracaonfe.getSerienfse());
		} else {
			identificacaoRps.setSerie("A");
		}
		identificacaoRps.setTipo("1");
		
		ConsultarNfseRpsEnvio consultarNfseRpsEnvio = new ConsultarNfseRpsEnvio();
		consultarNfseRpsEnvio.setIdentificacaoRps(identificacaoRps);
		consultarNfseRpsEnvio.setPrestador(prestador);
		
		xml = consultarNfseRpsEnvio.toString();
		
		Arquivo arquivoXmlConsultaLote = new Arquivo(xml.getBytes(), "nfconsultanota_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		arquivonfnota.setArquivoxmlconsulta(arquivoXmlConsultaLote);
		
		arquivoDAO.saveFile(arquivonfnota, "arquivoxmlconsulta");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		this.updateArquivoxmlconsulta(arquivonfnota);
		
	}

	public void createArquivoXmlConsultaEEl2(Arquivonfnota arquivonfnota) {
		Nota nota = arquivonfnota.getNota();
		String xml = nota.getNumero();
		
		Arquivo arquivoXmlConsultaLote = new Arquivo(xml.getBytes(), "nfconsultanota_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
		arquivonfnota.setArquivoxmlconsulta(arquivoXmlConsultaLote);
		
		arquivoDAO.saveFile(arquivonfnota, "arquivoxmlconsulta");
		arquivoService.saveOrUpdate(arquivoXmlConsultaLote);
		
		this.updateArquivoxmlconsulta(arquivonfnota);
	}
	
}
