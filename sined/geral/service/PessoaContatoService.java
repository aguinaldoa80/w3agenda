package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.dao.PessoaContatoDAO;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoaContatoService extends GenericService<PessoaContato> {

	private PessoaContatoDAO pessoaContatoDAO;
	private ContatoService contatoService;
	
	public void setPessoaContatoDAO(PessoaContatoDAO pessoaContatoDAO) {
		this.pessoaContatoDAO = pessoaContatoDAO;
	}
	
	public List<PessoaContato> findByPessoa(Cliente cliente) {
		return pessoaContatoDAO.findByPessoa(cliente);
	}
	
	public List<PessoaContato> findByPessoa(Pessoa pessoa) {
		return pessoaContatoDAO.findByPessoa(pessoa);
	}
	
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}

	public void saveOrUpdateListaPessoaContato(Pessoa pessoa, List<PessoaContato> listaPessoaContato) {
		if(listaPessoaContato == null){
			return;
		}
		if(pessoa == null || pessoa.getCdpessoa() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser null.");
		}
		
		List<PessoaContato> listaAtual = this.findByPessoa(pessoa);
		List<PessoaContato> listaDelete = new ArrayList<PessoaContato>();
		
		List<PessoaContato> pessoaContatoList = new ArrayList<PessoaContato>();
			
		for (PessoaContato pessoaContatoAtual : listaAtual) {
			if(pessoaContatoAtual.getCdpessoacontato() != null && !listaPessoaContato.contains(pessoaContatoAtual)){
				listaDelete.add(pessoaContatoAtual);
			}
		}
		
		for (PessoaContato pessoaContatoDelete : listaDelete) {
			try {
				this.delete(pessoaContatoDelete);
			} catch (DataAccessException da) {
				throw new SinedException("Contato n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
			}
		}
		for (PessoaContato pessoaContato : listaPessoaContato) {
			pessoaContato.setPessoa(pessoa);
			
			pessoaContatoList.add(pessoaContato);
			
			Contato contato = pessoaContato.getContato();
			
			if(contato.getContatotipo() != null && contato.getContatotipo().getCdcontatotipo() == null) contato.setContatotipo(null);
			if(Hibernate.isInitialized(contato.getListaEndereco()) && SinedUtil.isListNotEmpty(contato.getListaEndereco())){
				for(Endereco endereco : contato.getListaEndereco()){
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() == null){
						endereco.setMunicipio(null);
					}
				}
			}
			try{
				contatoService.saveOrUpdateNoUseTransaction(contato);
				this.saveOrUpdateNoUseTransaction(pessoaContato);
			} catch (DataIntegrityViolationException e) {
				if(DatabaseError.isKeyPresent(e, "idx_pessoa_cpf")){
					throw new SinedException("CPF do contato j� est� cadastrado.");
				} else throw e;
			}
		}
	}

	public List<PessoaContato> findByPessoaContatotipo(Pessoa pessoa, Contatotipo contatoTipo) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			return new ArrayList<PessoaContato>();
		return pessoaContatoDAO.findByPessoaContatotipo(pessoa.getCdpessoa().toString(), contatoTipo);
	}

	public List<PessoaContato> findContatoByPessoaAndContato(Integer cdpessoa, Integer cdcontato) {
		return pessoaContatoDAO.findContatoByPessoaAndContato(cdpessoa, cdcontato);
	}
}
