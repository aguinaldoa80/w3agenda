package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariamaterial;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.dao.OrdemservicoveterinariaDAO;
import br.com.linkcom.sined.util.IteracaoVeterinariaDataHora;
import br.com.linkcom.sined.util.IteracaoVeterinariaParametro;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemservicoveterinariaService extends GenericService<Ordemservicoveterinaria> {
	
	private OrdemservicoveterinariaDAO ordemservicoveterinariaDAO;
	private static OrdemservicoveterinariaService instance;
	
	//Setters
	public void setOrdemservicoveterinariaDAO( OrdemservicoveterinariaDAO ordemservicoveterinariaDAO) { this.ordemservicoveterinariaDAO = ordemservicoveterinariaDAO;	}
	
	public static OrdemservicoveterinariaService getInstance() {
		if (instance==null){
			instance = Neo.getObject(OrdemservicoveterinariaService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que carrega a Lista de OrdemServicoVeterinaria para Faturamento ou Venda 
	 *
	 * @param whereIn OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public List<Ordemservicoveterinaria> carregaRequisicaoForFaturar(String whereIn){
		return ordemservicoveterinariaDAO.carregaRequisicaoForFaturar(whereIn);
	}

	/**
	 * M�todo que persiste uma Lista de OrdemServicoVeterinaria com o Status de Faturado 
	 *
	 * @param whereIn OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public void updateFaturado(String whereInOSV) {
		for (Ordemservicoveterinaria os : ordemservicoveterinariaDAO.carregaRequisicaoForFaturar(whereInOSV)) {
			updateFaturado(os);
		}
	}
	
	/**
	 * M�todo que persiste uma OrdemServicoVeterinaria com o Status de Faturado 
	 *
	 * @param OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public void updateFaturado(Ordemservicoveterinaria os){
		os.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.FATURADO));
		ordemservicoveterinariaDAO.updateStatus(os);
	}
	
	/**
	 * Este m�todo � igual nos projetos do W3erp e do W3erpVeterinaria. Modifica��es devem ser feitas nos dois projetos.
	 * 
	 * @param parametro
	 * @author Thiago Clemente
	 * 
	 */
	public IteracaoVeterinariaRetorno processaIteracao(IteracaoVeterinariaParametro parametro){
		Frequencia intervalo = parametro.getIntervalo();
		Integer aCada = parametro.getAcada();
		Date dtinicio = parametro.getDtinicio();
		Hora hrinicio = parametro.getHrinicio();
		Boolean prazotermino = parametro.getPrazotermino();
		Integer quantidadeiteracao = parametro.getQuantidadeiteracao();
		List<IteracaoVeterinariaDataHora> listaDataHoraAplicacao = parametro.getListaDataHoraAplicacao();
		
		if (intervalo==null 
				|| intervalo.getCdfrequencia().intValue()==Frequencia.UNICA.intValue()
				|| aCada==null 
				|| aCada.intValue()<1 
				|| dtinicio==null
				|| (intervalo.getCdfrequencia().intValue()==Frequencia.HORA.intValue() && hrinicio==null)
				|| (Boolean.TRUE.equals(prazotermino) && (quantidadeiteracao==null || quantidadeiteracao.intValue()<1))){
			return new IteracaoVeterinariaRetorno();
		}
		
		Date dtproximaiteracao = null;
		Hora hrproximaiteracao = null;
		Date dtfimprevisaoiteracao = null;
		Hora hrfimprevisaoiteracao = null;
		Timestamp dtfim = null;
		
		if (hrinicio==null){
			hrinicio = new Hora("00:00");
		}
		
		//Data atual
		Calendar calendarBase = Calendar.getInstance();
		calendarBase.set(Calendar.MINUTE, 0);
		calendarBase.set(Calendar.SECOND, 0);
		
		Calendar calendarInicio = Calendar.getInstance();
		calendarInicio.setTime(dtinicio);
		calendarInicio.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hrinicio.toString().substring(0, 2)));
		calendarInicio.set(Calendar.MINUTE, Integer.valueOf(hrinicio.toString().substring(3, 5)));
		calendarInicio.set(Calendar.SECOND, 0);
		
		long millisBase = calendarBase.getTimeInMillis();
		long millisInicio = calendarInicio.getTimeInMillis();
		
		//Se o in�cio � igual ou maior que a data atual
		if (millisInicio>=millisBase){
			dtproximaiteracao = new Date(millisInicio);
			hrproximaiteracao = SinedDateUtils.getHora(calendarInicio);
			if (quantidadeiteracao!=null){
				quantidadeiteracao--;
			}
		}else {
			Calendar calendarProximaIteracao = Calendar.getInstance();
			calendarProximaIteracao.setTimeInMillis(millisInicio);
			
			//A pr�xima itera��o s� pode ser ap�s a data atual
			while (calendarProximaIteracao.getTimeInMillis()<millisBase || listaDataHoraAplicacao!=null && listaDataHoraAplicacao.contains(new IteracaoVeterinariaDataHora(new Date(calendarProximaIteracao.getTimeInMillis()), SinedDateUtils.getHora(calendarProximaIteracao)))){
				SinedDateUtils.addCalendar(calendarProximaIteracao, intervalo, aCada);
			}
			
			dtproximaiteracao = new Date(calendarProximaIteracao.getTimeInMillis());
			hrproximaiteracao = SinedDateUtils.getHora(calendarProximaIteracao);
		}
		
		//Se tiver prazo para t�rmino devo calcular a data fim prevista
		if (Boolean.TRUE.equals(prazotermino)){
			Calendar calendarFimPrevisto = Calendar.getInstance();
			calendarFimPrevisto.setTimeInMillis(millisInicio);
			
			SinedDateUtils.addCalendar(calendarFimPrevisto, intervalo, (quantidadeiteracao * aCada));
			
			dtfimprevisaoiteracao = new Date(calendarFimPrevisto.getTimeInMillis());
			hrfimprevisaoiteracao = SinedDateUtils.getHora(calendarFimPrevisto);
			
			//Se a data atual for maior que a data fim prevista
			if (millisBase>calendarFimPrevisto.getTimeInMillis()){
				dtfim = SinedDateUtils.currentTimestamp();
				dtproximaiteracao = null;
				hrproximaiteracao = null;
			}
		}
		
		return new IteracaoVeterinariaRetorno(dtproximaiteracao, hrproximaiteracao, dtfimprevisaoiteracao, hrfimprevisaoiteracao, dtfim);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemservicoveterinariaDAO#updateItemFaturadoAposPedidoCancelado(String whereInPedidoCancelado)
	*
	* @param whereInPedidoCancelado
	* @since 17/08/2016
	* @author Luiz Fernando
	*/
	public void updateItemFaturadoAposPedidoCancelado(String whereInPedidoCancelado) {
		ordemservicoveterinariaDAO.updateItemFaturadoAposPedidoCancelado(whereInPedidoCancelado);
	}
	
	public void ajustaListaOrdemservicoveterinariamaterial(Ordemservicoveterinaria os) {
		if(os != null && SinedUtil.isListNotEmpty(os.getListaOrdemservicoveterinariamaterial())){
			List<Ordemservicoveterinariamaterial> listaItens = new ArrayList<Ordemservicoveterinariamaterial>();
			Ordemservicoveterinariamaterial beanNovo;
			for(Ordemservicoveterinariamaterial item : os.getListaOrdemservicoveterinariamaterial()){
				if(item.getMaterial() != null && item.getQtdeusada() != null && item.getQtdeusada() > 0d && item.getMaterial().getVendapromocional() != null && item.getMaterial().getVendapromocional() &&
						SinedUtil.isListNotEmpty(item.getMaterial().getListaMaterialrelacionado()) && Boolean.TRUE.equals(item.getMaterial().getExibiritenskitvenda())){
					for(Materialrelacionado materialrelacionado : item.getMaterial().getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null){
							beanNovo = new Ordemservicoveterinariamaterial();
							beanNovo.setMaterial(materialrelacionado.getMaterialpromocao());
							beanNovo.setMaterialmestre(item.getMaterial());
							beanNovo.setQtdeusada(item.getQtdeusada() * materialrelacionado.getQuantidade());
							if (materialrelacionado.getValorvenda() != null){
								beanNovo.setValortotal(new Money(materialrelacionado.getValorvenda().getValue().doubleValue() * beanNovo.getQtdeusada()));
							}
							beanNovo.setCdordemservicoveterinariamaterial(item.getCdordemservicoveterinariamaterial());
							beanNovo.setFaturado(item.getFaturado());
							beanNovo.setFaturar(item.getFaturar());
							listaItens.add(beanNovo);
						}
					}
				}else {
					listaItens.add(item);
				}
			}
			os.setListaOrdemservicoveterinariamaterial(listaItens);
		}
	}
	
	/*public static void main(String[] args) {
		try {
			DecimalFormat formatador2casas = new DecimalFormat("00");
			
			Calendar calendarInicio = Calendar.getInstance();
			calendarInicio.set(Calendar.DAY_OF_MONTH, 10);
			calendarInicio.set(Calendar.MONTH, 5);
			calendarInicio.set(Calendar.YEAR, 2016);
			calendarInicio.set(Calendar.HOUR_OF_DAY, 11);
			calendarInicio.set(Calendar.MINUTE, 0);
			
			Frequencia intervalo = new Frequencia(Frequencia.HORA);
			Integer acada = 1;
			Date dtinicio = new Date(calendarInicio.getTimeInMillis());
			Hora hrinicio = new Hora(formatador2casas.format(calendarInicio.get(Calendar.HOUR_OF_DAY)) + ":" + formatador2casas.format(calendarInicio.get(Calendar.MINUTE)));
			Boolean prazotermino = true;
			Integer quantidadeiteracao = 17;
			
			IteracaoVeterinariaParametro parametro = new IteracaoVeterinariaParametro(intervalo, acada, dtinicio, hrinicio, prazotermino, quantidadeiteracao); 
			
			IteracaoVeterinariaRetorno retorno = null;//processaIteracao(parametro);
			
			System.out.println("Data/Hora inicio: " + NeoFormater.getInstance().format(dtinicio) + " �s " + hrinicio);
			System.out.println("");
			System.out.println("Data pr�xima itera��o: " + (retorno.getDtproximaiteracao()!=null ? NeoFormater.getInstance().format(retorno.getDtproximaiteracao()) : ""));
			System.out.println("Hora pr�xima itera��o: " + (retorno.getHrproximaiteracao()!=null ? retorno.getHrproximaiteracao() : ""));
			System.out.println("");
			System.out.println("Data fim previsto: " + (retorno.getDtfimprevisaoiteracao()!=null ? NeoFormater.getInstance().format(retorno.getDtfimprevisaoiteracao()) : ""));
			System.out.println("Hora fim previsto: " + (retorno.getHrfimprevisaoiteracao()!=null ? retorno.getHrfimprevisaoiteracao() : ""));
			System.out.println("");
			System.out.println("Data fim: " + (retorno.getDtfim()!=null ? NeoFormater.getInstance().format(retorno.getDtfim()) : ""));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
}