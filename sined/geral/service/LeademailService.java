package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.dao.LeademailDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeademailService extends GenericService<Leademail>{
	
	protected LeademailDAO leademailDAO;
	
	public void setLeademailDAO(LeademailDAO leademailDAO) {
		this.leademailDAO = leademailDAO;
	}
	
	public String createListaEmail(Collection<Leademail> listaEmail, String emailLead, Boolean csv){
		StringBuilder sb = new StringBuilder();
		if(StringUtils.isNotEmpty(emailLead)){
			sb.append(emailLead);
			if(csv){
				sb.append(", ");
			} else {
				sb.append("<br>");
			}
		}
		
		for(Leademail e : listaEmail){
			if(e.getEmail() != null){
				sb.append(e.getEmail().toString());
				if(csv){
					sb.append(", ");
				} else {
					sb.append("<br>");
				}
			}
		}
		if(csv && sb.length() > 0) sb.delete(sb.length() - 2, sb.length());
		return sb.toString();
	}
	
	public List<Leademail> findByLead(Lead lead) {
		return leademailDAO.findByLead(lead);
	}

}
