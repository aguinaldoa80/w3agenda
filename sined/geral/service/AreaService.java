package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.dao.AreaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AreaService extends GenericService<Area> {

	private AreaDAO areaDAO;
	
	public void setAreaDAO(AreaDAO areaDAO) {
		this.areaDAO = areaDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.AreaDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Area> findAllForFlex(){
		return areaDAO.findAllForFlex();
	}
	public List<Area> verificaAreaProjeto(Area area){
		return areaDAO.verificaAreaProjeto(area);
	}
	
}
