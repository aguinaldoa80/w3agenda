package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.view.Vwrelatoriomargemprojeto;
import br.com.linkcom.sined.geral.bean.view.Vwrelatoriooverhead;
import br.com.linkcom.sined.geral.dao.VwrelatoriomargemprojetoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.MargemProjetoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwrelatoriomargemprojetoService extends GenericService<Vwrelatoriooverhead>{

	private VwrelatoriooverheadService vwrelatoriooverheadService;
	private VwrelatoriomargemprojetoDAO vwrelatoriomargemprojetoDAO;
	
	public void setVwrelatoriomargemprojetoDAO(VwrelatoriomargemprojetoDAO vwrelatoriomargemprojetoDAO) {this.vwrelatoriomargemprojetoDAO = vwrelatoriomargemprojetoDAO;}
	public void setVwrelatoriooverheadService(VwrelatoriooverheadService vwrelatoriooverheadService) {this.vwrelatoriooverheadService = vwrelatoriooverheadService;}
	
	/**
	 * Cria o relat�rio de Margem de projeto em Excel.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriooverheadService#criaListaData
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#juncaoGeral
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#findTodosProjetos
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#findByProjeto
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#montaLinhaDetalhe
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#montaLinhaOverheadProjeto
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#montaLinhaOverheadProjetoConformeProjeto
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#montaLinhaMargemBruta
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#montaLinhaMargemLiquida
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#makeAcumulada
	 * @see br.com.linkcom.sined.util.controller.SinedExcel#getAlgarismoColuna
	 * 
	 * @param filtro
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoMargemProjetoExcelFormatado(MargemProjetoFiltro filtro) throws IOException {
		
		List<Date> listaData = vwrelatoriooverheadService.criaListaData(filtro.getDtref1(), filtro.getDtref2());
		
		List<Vwrelatoriomargemprojeto> listaGeralReceita = this.juncaoGeral(this.findTodosProjetos(filtro.getDtref1(), filtro.getDtref2(), Tipooperacao.TIPO_CREDITO));
		List<Vwrelatoriomargemprojeto> listaGeralDespesa = this.juncaoGeral(this.findTodosProjetos(filtro.getDtref1(), filtro.getDtref2(), Tipooperacao.TIPO_DEBITO));
		
		List<Vwrelatoriomargemprojeto> listaProjetoReceita = this.findByProjeto(filtro.getProjeto(), filtro.getDtref1(), filtro.getDtref2(), Tipooperacao.TIPO_CREDITO);
		List<Vwrelatoriomargemprojeto> listaProjetoDespesa = this.findByProjeto(filtro.getProjeto(), filtro.getDtref1(), filtro.getDtref2(), Tipooperacao.TIPO_DEBITO);
		
		SinedExcel wb = new SinedExcel();

		HSSFRow row;
		HSSFCell cell;
		
		HSSFSheet p = wb.createSheet("Planilha");
        p.setDefaultColumnWidth((short) 25);
        p.setDisplayGridlines(false);
        
        
        // T�TULO DO RELAT�RIO
        row = p.createRow((short) 0);
		p.addMergedRegion(new Region(0, (short) 0, 2, (short) listaData.size()));
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cell.setCellValue("Margem L�quida de Projeto");
		
		// CABE�ALHO DA TABELA
		row = p.createRow((short) 4);
		row.setHeightInPoints((float)30);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Per�odo");
		
		// DEFININDO O TAMANHO DA PRIMEIRA COLUNA
		p.setColumnWidth((short)0, (short)(290*35));
		
		SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			cell.setCellValue(format.format(listaData.get(i)));	
		}
		
		this.montaLinhaDetalhe(listaData, listaGeralReceita, p, 5, "RECEITA GERAL");
		this.montaLinhaDetalhe(listaData, listaGeralDespesa, p, 6, "OVERHEAD GERAL");
		this.montaLinhaDetalhe(listaData, listaProjetoReceita, p, 7, "RECEITA PROJETO");
		this.montaLinhaDetalhe(listaData, listaProjetoDespesa, p, 8, "DESPESAS PROJETO");
		
		this.montaLinhaOverheadProjeto(listaData, p);
		this.montaLinhaOverheadProjetoConformeProjeto(listaData, p);
		this.montaLinhaMargemBruta(listaData, p);
		this.montaLinhaMargemLiquida(listaData, p);
		
		String ultimaColuna = SinedExcel.getAlgarismoColuna(listaData.size());
		
		this.makeAcumulada(p, 15, "(SUM(B8:" + ultimaColuna + "8)-SUM(B9:" + ultimaColuna + "9))/SUM(B8:" + ultimaColuna + "8)", "MARGEM BRUTA ACUMULADA", 20);
		this.makeAcumulada(p, 16, "(SUM(B8:" + ultimaColuna + "8)-(SUM(B9:" + ultimaColuna + "9)+SUM(B10:" + ultimaColuna + "10)))/SUM(B8:" + ultimaColuna + "8)", "MARGEM L�QUIDA ACUMULADA", 20);
		this.makeAcumulada(p, 17, "(SUM(B8:" + ultimaColuna + "8)-(SUM(B9:" + ultimaColuna + "9)+SUM(B11:" + ultimaColuna + "11)))/SUM(B8:" + ultimaColuna + "8)", "MARGEM LIQUIDA ACUMULADA C/ OVERHEAD PREVISTO EM PROPOSTA 11%", 30);
		
		return wb.getWorkBookResource("margem_projeto_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * Monta a linha das partes abaixo no relat�rio.
	 *
	 * @param p
	 * @param linha
	 * @param formula
	 * @param titulo
	 * @param rowHeight
	 * @author Rodrigo Freitas
	 */
	private void makeAcumulada(HSSFSheet p, int linha, String formula, String titulo, int rowHeight) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) linha);
		row.setHeightInPoints((float)rowHeight);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(titulo);
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		cell.setCellFormula(formula);
	}
	
	/**
	 * Cria a linha da margem l�quida
	 *
	 * @param listaData
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void montaLinhaMargemLiquida(List<Date> listaData, HSSFSheet p) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) 12);
		row.setHeightInPoints((float)20);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("MARGEM L�QUIDA MENSAL");
		
		String colunaStr;
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			colunaStr = SinedExcel.getAlgarismoColuna(i+1);
			cell.setCellFormula("(" + colunaStr + "8-(" + colunaStr + "9+" + colunaStr + "10))/" + colunaStr + "8");
		}
	}
	
	/**
	 * Cria a linha da margem bruta
	 *
	 * @param listaData
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void montaLinhaMargemBruta(List<Date> listaData, HSSFSheet p) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) 11);
		row.setHeightInPoints((float)20);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("MARGEM BRUTA MENSAL");
		
		String colunaStr;
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
			colunaStr = SinedExcel.getAlgarismoColuna(i+1);
			cell.setCellFormula("(" + colunaStr + "8-" + colunaStr + "9)/" + colunaStr + "8");
		}
	}
	
	/**
	 * Cria a linha do overhead do projeto conforme a proposta.
	 *
	 * @param listaData
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void montaLinhaOverheadProjetoConformeProjeto(List<Date> listaData, HSSFSheet p) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) 10);
		row.setHeightInPoints((float)30);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("OVERHEAD PROJETO CONFORME PREVISTO EM PROPOSTA 11%");
		
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			String colunaStr = SinedExcel.getAlgarismoColuna(i+1);
			cell.setCellFormula("(((11/100)*" + colunaStr + "6)*" + colunaStr + "8)/" + colunaStr + "6");
		}
	}
	
	/**
	 * Cria a linha do overhead do projeto
	 *
	 * @param listaData
	 * @param p
	 * @author Rodrigo Freitas
	 */
	private void montaLinhaOverheadProjeto(List<Date> listaData, HSSFSheet p) {
		HSSFRow row;
		HSSFCell cell;
		row = p.createRow((short) 9);
		row.setHeightInPoints((float)20);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue("OVERHEAD PROJETO");
		
		String colunaStr;
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			colunaStr = SinedExcel.getAlgarismoColuna(i+1);
			cell.setCellFormula("(" + colunaStr + "7*" + colunaStr + "8)/" + colunaStr + "6");
		}
	}
	
	/**
	 * Cria a linha do detalhe do datagrid do relat�rio.
	 *
	 * @param listaData
	 * @param lista
	 * @param p
	 * @param linha
	 * @param titulo
	 * @author Rodrigo Freitas
	 */
	private void montaLinhaDetalhe(List<Date> listaData, List<Vwrelatoriomargemprojeto> lista, HSSFSheet p, int linha, String titulo) {
		HSSFRow row;
		HSSFCell cell;
		
		row = p.createRow((short) linha);
		row.setHeightInPoints((float)20);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(titulo);
		
		int idx;
		for (int i = 0; i < listaData.size(); i++) {	
			cell = row.createCell((short)(i+1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
			idx = lista.indexOf(new Vwrelatoriomargemprojeto(listaData.get(i)));
			if(idx != -1){
				cell.setCellValue(lista.get(idx).getValor().getValue().doubleValue());
			} else {
				cell.setCellValue(0);	
			}
		}
	}
	
	/**
	 * Faz o agrupamento dos registros com a mesma data.
	 *
	 * @param listaGeral
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Vwrelatoriomargemprojeto> juncaoGeral(List<Vwrelatoriomargemprojeto> listaGeral) {
		List<Vwrelatoriomargemprojeto> listaDefinitiva = new ArrayList<Vwrelatoriomargemprojeto>();
		Boolean achou;
		for (Vwrelatoriomargemprojeto bean : listaGeral) {
			achou = false;
			for (Vwrelatoriomargemprojeto bean2 : listaDefinitiva) {
				if(SinedDateUtils.equalsIgnoreHour(bean2.getDtref(), bean.getDtref())){
					bean2.setValor(bean2.getValor().add(bean.getValor()));
					achou = true;
					break;
				}
			}
			if(!achou){
				bean.setProjeto(null);
				listaDefinitiva.add(bean);
			}
		}
		
		return listaDefinitiva;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VwrelatoriomargemprojetoDAO#findByProjeto
	 * 
	 * @param projeto
	 * @param dtref1
	 * @param dtref2
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Vwrelatoriomargemprojeto> findByProjeto(Projeto projeto, Date dtref1, Date dtref2, Tipooperacao tipo) {
		return vwrelatoriomargemprojetoDAO.findByProjeto(projeto, dtref1, dtref2, tipo);
	}

	/**
	 * Busca os resgitros do findByProjeto com o projeto nulo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService#findByProjeto
	 *
	 * @param dtref1
	 * @param dtref2
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Vwrelatoriomargemprojeto> findTodosProjetos(Date dtref1, Date dtref2, Tipooperacao tipo) {
		return this.findByProjeto(null, dtref1, dtref2, tipo);
	}

	
}
