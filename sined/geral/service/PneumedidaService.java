package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.dao.PneumedidaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.pneumedida.PneumedidaRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumedida.PneuMedidaW3producaoRESTModel;

public class PneumedidaService extends GenericService<Pneumedida> {

	private PneumedidaDAO pneumedidaDAO;
	private PneuSegmentoService pneuSegmentoService;

	public void setPneumedidaDAO(PneumedidaDAO pneumedidaDAO) {
		this.pneumedidaDAO = pneumedidaDAO;
	}
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {
		this.pneuSegmentoService = pneuSegmentoService;
	}

	/* singleton */
	private static PneumedidaService instance;
	public static PneumedidaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneumedidaService.class);
		}
		return instance;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumedidaDAO#findAutocomplete(String q)
	*
	* @param q
	* @param otr
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumedida> findAutocomplete(String q, PneuSegmento pneuSegmento){
		return pneumedidaDAO.findAutocomplete(q, pneuSegmentoService.isOtr(pneuSegmento));
	}
	
	public List<Pneumedida> findAutocomplete(String q){
		String cdPneuSegmento = null;
		try {
			cdPneuSegmento = NeoWeb.getRequestContext().getParameter("cdpneuSegmento");
		} catch (Exception e) {}
		
		if(StringUtils.isNotBlank(cdPneuSegmento)){
			return this.findAutocomplete(q, new PneuSegmento(Integer.parseInt(cdPneuSegmento)));
		}
		return pneumedidaDAO.findAutocomplete(q, false);
	}
	
	public List<PneuMedidaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de pneumedida a ser sincronizada com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PneuMedidaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PneuMedidaW3producaoRESTModel> lista = new ArrayList<PneuMedidaW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumedida pm : pneumedidaDAO.findForW3Producao(whereIn))
				lista.add(new PneuMedidaW3producaoRESTModel(pm));
		}
		
		return lista;
	}
	
	/**
	 * Carrega pelo nome, se n�o existir insere o registro.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumedida loadOrInsertByNome(String nome){
		Pneumedida pneumedida = this.loadByNome(nome);
		if(pneumedida == null){
			pneumedida = new Pneumedida();
			pneumedida.setNome(nome);
			this.saveOrUpdate(pneumedida);
		}
		return pneumedida;
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.PneumedidaService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumedidaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PneumedidaDAO.findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<PneumedidaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PneumedidaRESTModel> lista = new ArrayList<PneumedidaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pneumedida bean : pneumedidaDAO.findForAndroid(whereIn))
				lista.add(new PneumedidaRESTModel(bean));
		}
		
		return lista;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumedida loadByNome(String nome) {
		return pneumedidaDAO.loadByNome(nome);
	}
	
	public List<Pneumedida> loadByOtr(){
		return pneumedidaDAO.loadByOtr();
	}
	
	public List<Pneumedida> findByPneuSegmento(PneuSegmento pneuSegmento) {
		if(!Util.objects.isPersistent(pneuSegmento)){
			return new ArrayList<Pneumedida>();
		}
		return pneumedidaDAO.findByPneuSegmento(pneuSegmentoService.isOtr(pneuSegmento));
	}
	
	public boolean isOtr(Pneumedida pneumedida){
		Pneumedida bean = this.load(pneumedida, "pneumedida.cdpneumedida, pneumedida.otr");
		if(bean == null){
			return false;
		}
		return Boolean.TRUE.equals(bean.getOtr());
	}
}