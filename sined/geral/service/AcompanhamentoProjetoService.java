package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.keyvalue.MultiKey;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;

/***
 * Classe respons�vel pelas regras de gera��o do relat�rio de acompanhamento do projeto
 * 
 * @author Rodrigo Alvarenga
 *
 */
public class AcompanhamentoProjetoService {
	
	/***
	 * Verifica se uma determinada data � dia �til ou n�o, baseado em uma lista de feriados 
	 * e se s�bado e domingo s�o dias �teis ou n�o.
	 * 
	 * @param calCorrente
	 * @param listaFeriado
	 * @param sabadoUtil
	 * @param domingoUtil
	 * @return verdadeiro ou falso
	 * @throws SinedException - Caso algum dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public static Boolean isDiaUtil(Calendar calCorrente, List<Date> listaFeriado, Boolean sabadoUtil, Boolean domingoUtil) {
		
		if (listaFeriado == null || sabadoUtil == null || domingoUtil == null) {
			throw new SinedException("Par�metros inv�lidos. Os campos listaFeriado, sabadoUtil e domingoUtil n�o podem ser nulos.");
		}
		
		//Verifica se dtCorrente � dia �til. 
		//Primeiro verifica se o dia n�o � feriado
		if (!listaFeriado.contains(calCorrente.getTime()) &&
			//Depois verifica se � para considerar os s�bados como dias �teis. Caso n�o seja, verifica se o dia n�o � um s�bado
			(sabadoUtil || calCorrente.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) &&
			//Depois verifica se � para considerar os domingos como dias �teis. Caso n�o seja, verifica se o dia n�o � um domingo
			(domingoUtil || calCorrente.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)) {
			//Somente se passar por todos os filtros acima, a data corrente � um dia �til para a tarefa
			return true;
		}
		return false;
	}
	
	/**
	 * Monta um mapa de cada tarefa e uma lista de dias em que a mesma est� planejada para ser executada.
	 * Para isso exclui os feriados, os s�bados (caso o par�metro sabadoUtil seja falso) 
	 * e os domingos (caso o par�metro domingoUtil seja falso)
	 *
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#isDiaUtil(Calendar, List, Boolean, Boolean)
	 * 
	 * @param listaTarefa
	 * @param listaFeriado
	 * @param sabadoUtil
	 * @param domingoUtil
	 * 
	 * @return Map<Tarefa,List<Date>>
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public static Map<Tarefa,List<Date>> montaMapaDiasTarefa(List<Tarefa> listaTarefa, List<Date> listaFeriado, Boolean sabadoUtil, Boolean domingoUtil) {
		
		if (listaTarefa == null || listaFeriado == null || sabadoUtil == null || domingoUtil == null) {
			throw new SinedException("Par�metros inv�lidos. Os campos listaTarefa, listaFeriado, sabadoUtil e domingoUtil n�o podem ser nulos.");
		}
		
		Map<Tarefa,List<Date>> mapaDiaTarefa = new HashMap<Tarefa,List<Date>>();
		
		List<Date> listaDiaTarefa;		
		Calendar calCorrente = Calendar.getInstance();
		int totalDiasTarefa;
		
		for (Tarefa tarefa : listaTarefa) {
			
			listaDiaTarefa = new ArrayList<Date>();
			totalDiasTarefa = 0;
			
			if (tarefa.getDuracao() == null) {
				throw new SinedException("N�o pode haver tarefa com dura��o nula no c�lculo dos dias das tarefas.");
			}
			
			calCorrente.setTime(tarefa.getDtinicio());
			
			while (totalDiasTarefa < tarefa.getDuracao()) {				
				//Verifica se dtCorrente � dia �til.
				if (isDiaUtil(calCorrente, listaFeriado, sabadoUtil, domingoUtil)) {
					listaDiaTarefa.add(new Date(calCorrente.getTime().getTime()));
					totalDiasTarefa++;
				}
				calCorrente.add(Calendar.DAY_OF_MONTH, 1);
			}
			mapaDiaTarefa.put(tarefa,listaDiaTarefa);
		}
		return mapaDiaTarefa;
	}
	
	/**
	 * Monta um mapa com a quantidade produzida de cada tarefa em cada dia.
	 *
	 * @param listaTarefa
	 * @param listaProducaoDiaria
	 * @return mapa da quantidade produzida por tarefa e dia
	 * @throws SinedException - caso o par�metro seja nulo 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public static Map<MultiKey,Double> montaMapaProducaoDiaria(List<Producaodiaria> listaProducaoDiaria) {
		
		if (listaProducaoDiaria == null) {
			throw new SinedException("Par�metros inv�lidos. O campo listaProducaoDiaria n�o pode ser nulo.");			
		}
		
		Map<MultiKey,Double> mapaProducaoDiaria = new HashMap<MultiKey, Double>();		
		Double qtdeProduzida;
		MultiKey key;
			
		for (Producaodiaria producaodiaria : listaProducaoDiaria) {				
				key = new MultiKey(new Object[]{producaodiaria.getTarefa().getCdtarefa(),producaodiaria.getDtproducao()});
				qtdeProduzida = mapaProducaoDiaria.get(key);
				
				if (qtdeProduzida == null) {
					qtdeProduzida = 0d;
				}
				qtdeProduzida += producaodiaria.getQtde();
				mapaProducaoDiaria.put(key, qtdeProduzida);					
		}
		return mapaProducaoDiaria;
	}
	
	/**
	 * Monta um mapa com o custo do projeto em cada dia.
	 *
	 * @param listaMovimentacao
	 * @return mapa do custo por dia
	 * @throws SinedException - caso o par�metro seja nulo 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public static Map<Date,Money> montaMapaCustoDiario(List<Movimentacao> listaMovimentacao) {
		
		if (listaMovimentacao == null) {
			throw new SinedException("Par�metros inv�lidos. O campo listaMovimentacao n�o pode ser nulo.");			
		}
		
		Map<Date,Money> mapaCustoDiario = new HashMap<Date,Money>();		
		Money custoItem;
		Money custoDia;
		Date dtOperacao;
			
		for (Movimentacao movimentacao : listaMovimentacao) {
			dtOperacao = movimentacao.getDtbanco() != null ? movimentacao.getDtbanco() : movimentacao.getDtmovimentacao();
			
			if (movimentacao.getRateio() != null && movimentacao.getRateio().getListaRateioitem() != null) {
				for (Rateioitem rateioitem : movimentacao.getRateio().getListaRateioitem()) {
					custoItem = rateioitem.getValor() != null ? rateioitem.getValor() : new Money(0);
					
					custoDia = mapaCustoDiario.get(dtOperacao);
					if (custoDia == null) {
						custoDia = new Money(0);
					}
					custoDia = custoDia.add(custoItem);
					mapaCustoDiario.put(dtOperacao, custoDia);
				}
			}
		}
		return mapaCustoDiario;
	}	
	
	/**
	 * Monta uma lista de acompanhamento do projeto para ser utilizada no relat�rio de Acompanhamento de Projeto
	 * 
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#montaMapaDiasTarefa(List, List, Boolean, Boolean)
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#montaMapaProducaoDiaria(List)
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#montaMapaCustoDiario(List)
	 * @see br.com.linkcom.sined.util.SinedDateUtils#calculaDiferencaDias(Date, Date)
	 * @see br.com.linkcom.sined.geral.service.AcompanhamentoProjetoService#isDiaUtil(Calendar, List, Boolean, Boolean)
	 * 
	 * @param planejamento
	 * @param listaTarefa
	 * @param listaProducaoDiaria
	 * @param listaMovimentacao
	 * @param listaFeriado
	 * @param sabadoUtil
	 * @param domingoUtil
	 * 
	 * @return lista de AcompanhamentoProjeto
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public static List<AcompanhamentoProjeto> montaListaAcompanhamentoProjeto(Planejamento planejamento, List<Tarefa> listaTarefa, List<Producaodiaria> listaProducaoDiaria, List<Movimentacao> listaMovimentacao, List<Date> listaFeriado, Boolean sabadoUtil, Boolean domingoUtil) {
		
		if (planejamento == null || planejamento.getCdplanejamento() == null || listaTarefa == null || 
			listaProducaoDiaria == null || listaMovimentacao == null || listaFeriado == null || sabadoUtil == null || domingoUtil == null) {
			throw new SinedException("Par�metros inv�lidos. Os campos planejamento, listaTarefa, listaProducaoDiaria, listaMovimentacao, listaFeriado, sabadoUtil e domingoUtil n�o podem ser nulos.");
		}
		
		List<AcompanhamentoProjeto> listaAcompanhamentoProjeto = new ArrayList<AcompanhamentoProjeto>();		
		AcompanhamentoProjeto acompanhamentoProjeto = null;
		
		//Vari�veis do Planejamento
		Double percPlanTarefa = null;
		Double percPlanTarefaAcu = 0d;
		//Monta um mapa com cada tarefa e seus dias planejados para execu��o 
		Map<Tarefa, List<Date>> mapaDiasTarefa = montaMapaDiasTarefa(listaTarefa, listaFeriado, sabadoUtil, domingoUtil);
		List<Date> listaDiaTarefa;		
		
		//Vari�veis do Andamento
		Double percAndTarefa = null;
		Double percAndTarefaAcu = 0d;
		Map<MultiKey, Double> mapaProducaoDiaria = montaMapaProducaoDiaria(listaProducaoDiaria);
		MultiKey key;
		Double valorProducaoDiario;		
		Double fatorAndTarefa;		
		
		//Vari�veis do Custo
		Money custoDiario;
		Money custoDiarioAcu = new Money(0);
		Double percCustoAcu = 0d;
		//Monta um mapa com o custo di�rio do projeto 
		Map<Date,Money> mapaCustoDiario = montaMapaCustoDiario(listaMovimentacao);		
		
		//Busca a menor e a maior data para exibir os dados.
		//Nem sempre a maior data ser� o fim do planejamento, pois podem existir tarefas executadas fora do prazo.
		Date dtInicio = planejamento.getDtinicio();
		Date dtFim = planejamento.getDtfim();
		for (Producaodiaria producaodiaria : listaProducaoDiaria) {
			if (producaodiaria.getDtproducao().compareTo(dtFim) > 0) {
				dtFim = producaodiaria.getDtproducao();
			}
		}
		Calendar calCorrente = Calendar.getInstance();
		calCorrente.setTime(dtInicio);
		
		//Calcula o n�mero total de dias para exibi��o dos dados
		Long totalDias = SinedDateUtils.calculaDiferencaDias(dtInicio,dtFim);		
		Boolean diaUtil;
		
		//Para cada dia
		for (int i = 0; i <= totalDias; i++) {
			
			//Verifica se dtCorrente � dia �til.
			diaUtil = isDiaUtil(calCorrente, listaFeriado, sabadoUtil, domingoUtil);
			
			if (diaUtil) {
				for (Tarefa tarefa : listaTarefa) {

					/****************************************/
					/** C�lculo do Planejamento da Tarefa **/
					/**************************************/
					
					//Verifica se a dura��o da tarefa engloba a data corrente
					listaDiaTarefa = mapaDiasTarefa.get(tarefa);

					//Verifica se a data corrente faz parte do planejamento da tarefa					
					if (listaDiaTarefa != null && listaDiaTarefa.contains(calCorrente.getTime())) {
						//Obt�m-se o percentual planejado da tarefa no dia
						percPlanTarefa = 100.00 * tarefa.getPeso() / tarefa.getDuracao();
						
						//Acumula-se o valor dos percentuais planejados de todas as tarefas no dia
						percPlanTarefaAcu += percPlanTarefa;						
					}
					
					/*************************************/
					/** C�lculo do Andamento da Tarefa **/
					/***********************************/
					
					key = new MultiKey(tarefa.getCdtarefa(), calCorrente.getTime());
					valorProducaoDiario = mapaProducaoDiaria.get(key);
					if (valorProducaoDiario == null) {
						valorProducaoDiario = 0d;
					}
					
					//Calcula o fator de conclus�o da tarefa (0 => 0% ; 1 => 100%)
					if (tarefa.getQtde() == null || tarefa.getQtde() == 0) {
						fatorAndTarefa = 0d;
					}
					else {
						fatorAndTarefa = valorProducaoDiario / tarefa.getQtde();
					}
					if (fatorAndTarefa > 1.0) {
						fatorAndTarefa = 1.0;
					}
					
					percAndTarefa = 100.00 * fatorAndTarefa * tarefa.getPeso();
					percAndTarefaAcu += percAndTarefa;
					
				}
				
				/*********************************/
				/** C�lculo do Custo da Tarefa **/
				/*******************************/					
				custoDiario = mapaCustoDiario.get(calCorrente.getTime());
				
				if (custoDiario == null) {
					custoDiario = new Money(0);
				}
				custoDiarioAcu = custoDiarioAcu.add(custoDiario);
				
				if (planejamento.getCusto().getValue().intValue() == 0) {
					percCustoAcu = 0d;
				}
				else {
					percCustoAcu = custoDiarioAcu.getValue().doubleValue() * 100.00 / planejamento.getCusto().getValue().doubleValue();
				}
			}
			
			//Adiciona os valores no bean do Relat�rio
			acompanhamentoProjeto = new AcompanhamentoProjeto();
			acompanhamentoProjeto.setData(new Date(calCorrente.getTime().getTime()));
			acompanhamentoProjeto.setPlanejamento(percPlanTarefaAcu);
			acompanhamentoProjeto.setAndamento(percAndTarefaAcu);
			acompanhamentoProjeto.setCusto(percCustoAcu);			
			acompanhamentoProjeto.setDiaUtil(diaUtil);
			listaAcompanhamentoProjeto.add(acompanhamentoProjeto);
			
			calCorrente.add(Calendar.DAY_OF_MONTH, 1);
		}
		return listaAcompanhamentoProjeto;
	}	
}
