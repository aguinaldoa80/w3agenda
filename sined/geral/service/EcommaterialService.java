package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterial;
import br.com.linkcom.sined.geral.dao.EcommaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EcommaterialService extends GenericService<Ecommaterial>{
	
	private EcommaterialDAO ecommaterialDAO;
	
	public void setEcommaterialDAO(EcommaterialDAO ecommaterialDAO) {
		this.ecommaterialDAO = ecommaterialDAO;
	}
	
	/* singleton */
	private static EcommaterialService instance;
	public static EcommaterialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EcommaterialService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#haveDadosSincronizacao()
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public boolean haveDadosSincronizacao() {
		return ecommaterialDAO.haveDadosSincronizacao();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#primeiroFilaSincronizacao()
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public Ecommaterial primeiroFilaSincronizacao(Timestamp currentTimestamp) {
		return ecommaterialDAO.primeiroFilaSincronizacao(currentTimestamp);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#loadEcommaterial(Ecommaterial ecommaterial)
	*
	* @param ecommaterial
	* @return
	* @since 23/04/2015
	* @author Luiz Fernando
	*/
	public Ecommaterial loadEcommaterial(Ecommaterial ecommaterial) {
		return ecommaterialDAO.loadEcommaterial(ecommaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#updateSincronizado(Ecommaterial ecommaterial)
	 *
	 * @param ecommaterial
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateSincronizado(Ecommaterial ecommaterial) {
		ecommaterialDAO.updateSincronizado(ecommaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#updateFinalFila(Ecommaterial ecommaterial)
	 *
	 * @param ecommaterial
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateFinalFila(Ecommaterial ecommaterial) {
		ecommaterialDAO.updateFinalFila(ecommaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialDAO#updateIdentificador(Ecommaterial ecommaterial, String identificador)
	 *
	 * @param ecommaterial
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @param configuracaoecom 
	 * @since 18/07/2013
	 */
	public void updateIdentificador(Ecommaterial ecommaterial, String identificador, Configuracaoecom configuracaoecom) {
		ecommaterialDAO.updateIdentificador(ecommaterial, identificador, configuracaoecom);
	}
	
}
