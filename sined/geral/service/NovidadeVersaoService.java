package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.NovidadeVersao;
import br.com.linkcom.sined.geral.dao.NovidadeVersaoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaNovidadeVersaoBean;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.NovidadeVersaoRetorno;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NovidadeVersaoService extends GenericService<NovidadeVersao>{
	
	private NovidadeVersaoDAO novidadeVersaoDAO;
	
	public void setNovidadeVersaoDAO(NovidadeVersaoDAO novidadeVersaoDAO) {
		this.novidadeVersaoDAO = novidadeVersaoDAO;
	}

	/* singleton */
	private static NovidadeVersaoService instance;
	public static NovidadeVersaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(NovidadeVersaoService.class);
		}
		return instance;
	}
	
	public NovidadeVersao findByNovidadeVersao(ConsultaNovidadeVersaoBean bean){
		return novidadeVersaoDAO.findByNovidadeVersao(bean); 
	}

	public NovidadeVersaoRetorno existeNovidadeVersao() {
		
		Object obj = NeoWeb.getRequestContext().getSession().getAttribute(SinedUtil.EXISTE_NOVIDADE_VERSAO);
		Object obj2 = NeoWeb.getRequestContext().getSession().getAttribute(SinedUtil.EXISTE_NOVIDADE_VERSAO_PERIODO);
		
		if(obj == null && obj2 == null){
			NovidadeVersaoRetorno novidadeVersaoRetorno = ControleAcessoUtil.util.doNovidadeVersao(NeoWeb.getRequestContext().getServletRequest(), true, true, null);
			
			if((novidadeVersaoRetorno != null && novidadeVersaoRetorno.getVersao() != null && !novidadeVersaoRetorno.getUltimaVersao()) || (SinedUtil.getUsuarioLogado().getCdpessoa().equals(1))){
				if(novidadeVersaoRetorno == null){
					novidadeVersaoRetorno = new NovidadeVersaoRetorno();
				}
				novidadeVersaoRetorno.setExisteNovidade(true);
				novidadeVersaoRetorno.setExisteNovidadePeriodo(true);
			}else {
			//	novidadeVersaoRetorno = ControleAcessoUtil.util.doNovidadeVersao(NeoWeb.getRequestContext().getServletRequest(), true, false, null);
				if(novidadeVersaoRetorno != null && novidadeVersaoRetorno.getVersao() != null){
					novidadeVersaoRetorno.setExisteNovidade(false);
					novidadeVersaoRetorno.setExisteNovidadePeriodo(false);
				}
			}
			
			return novidadeVersaoRetorno;	
		}else {
			NovidadeVersaoRetorno novidadeVersaoRetorno = new NovidadeVersaoRetorno();
			novidadeVersaoRetorno.setExisteNovidade((Boolean) obj);
			novidadeVersaoRetorno.setExisteNovidadePeriodo((Boolean) obj2);
			
			return novidadeVersaoRetorno;
		}
		
	}

	public List<NovidadeVersao> findVersoesComNovidade(ConsultaNovidadeVersaoBean bean) {
		return novidadeVersaoDAO.findVersoesComNovidade(bean);
	}

	public NovidadeVersao findUltimaNovidadeVersao(ConsultaNovidadeVersaoBean bean) {
		return novidadeVersaoDAO.findUltimaNovidadeVersao(bean);
	}
	
	public NovidadeVersao findCdNovidadeVersao(ConsultaNovidadeVersaoBean bean) {
		return novidadeVersaoDAO.findCdNovidadeVersao(bean);
	}
	
}
