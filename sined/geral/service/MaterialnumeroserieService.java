package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.dao.MaterialnumeroserieDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialnumeroserieService extends GenericService<Materialnumeroserie> {

	private MaterialnumeroserieDAO materialnumeroserieDAO;
	
	public void setMaterialnumeroserieDAO(MaterialnumeroserieDAO materialnumeroserieDAO) {this.materialnumeroserieDAO = materialnumeroserieDAO;}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialnumeroserieDAO#findListaMaterialnumeroserieByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialnumeroserie> findListaMaterialnumeroserieByMaterial(Material material) {
		return materialnumeroserieDAO.findListaMaterialnumeroserieByMaterial(material);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialnumeroserieDAO#findListaMaterialnumeroserieByMaterial(Material material)
	 *
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialnumeroserie> loadWithNumeroserie(Integer codigo) {
		return materialnumeroserieDAO.loadWithNumeroserie(codigo);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialnumeroserieDAO#salvaEscolhaNumeroserie(Integer cdnota, Integer cdmaterial, String whereIn)
	 *
	 * @param cdnota
	 * @param cdmaterial
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void salvaEscolhaNumeroserie(Integer cdnota, Integer cdmaterial, String whereIn) {
		materialnumeroserieDAO.salvaEscolhaNumeroserie(cdnota, cdmaterial, whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialnumeroserieDAO#getQtdeJaRegistrada(Integer cdmaterial, Integer cdnota)
	 *
	 * @param cdmaterial
	 * @param cdnota
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getQtdeJaRegistrada(Integer cdmaterial, Integer cdnota) {
		return materialnumeroserieDAO.getQtdeJaRegistrada(cdmaterial, cdnota);
	}
	
	public List<Materialnumeroserie> findByCdsmaterial(String whereIn) {
		return materialnumeroserieDAO.findByCdsmaterial(whereIn);
	}
}