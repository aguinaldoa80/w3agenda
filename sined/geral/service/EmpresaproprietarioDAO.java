package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;

public class EmpresaproprietarioDAO extends GenericDAO<Empresaproprietario>{

	public List<Empresaproprietario> findProprietariosFazendaAutoComplete(String nome) {
		return query().
				autocomplete()
				.select("empresaproprietario.cdempresaproprietario, colaborador.cdpessoa, colaborador.nome, empresa.cdpessoa")
				.join("empresaproprietario.colaborador colaborador")
				.join("empresaproprietario.empresa empresa")
				.where("empresa.propriedadeRural = true")
				.whereLikeIgnoreAll("colaborador.nome", nome)
				.list();
	}

	public Empresaproprietario findColaboradorPrincipal(Integer cdpessoa) {
		return query()
				.select("colaborador.cdpessoa, colaborador.nome, empresa.nomefantasia")
				.join("empresaproprietario.colaborador colaborador")
				.join("empresaproprietario.empresa empresa")
				.where("empresa.cdpessoa = ?", cdpessoa)
				.where("empresaproprietario.principal = true")
				.unique();
	}

	public List<Empresaproprietario> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return query()
				.select("empresaproprietario.cdempresaproprietario, colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.email, " +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, " +
						"municipio.cdmunicipio, municipio.cdibge, uf.cduf, uf.sigla, endereco.cep, listaTelefone.telefone, telefonetipo.cdtelefonetipo, " +
						"colaborador.crccontabilista, colaboradorcontabilista.razaosocial, colaboradorcontabilista.cpf, " +
						"colaboradorcontabilista.cnpj, colaboradorcontabilista.email, listaTelefoneContabilista.telefone, telefonetipoContabilista.cdtelefonetipo")
				.join("empresaproprietario.colaborador colaborador")
				.join("colaborador.endereco endereco")
				.join("endereco.municipio municipio")
				.join("municipio.uf uf")
				.leftOuterJoin("colaborador.listaTelefone listaTelefone")
				.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
				.leftOuterJoin("colaborador.colaboradorcontabilista colaboradorcontabilista")
				.leftOuterJoin("colaboradorcontabilista.listaTelefone listaTelefoneContabilista")
				.leftOuterJoin("listaTelefoneContabilista.telefonetipo telefonetipoContabilista")
				.join("empresaproprietario.empresa empresa")
				.where("colaborador.cdpessoa= ?", filtro.getColaborador().getCdpessoa())
				.where("empresaproprietario.principal = true")
				.list();
	}

}
