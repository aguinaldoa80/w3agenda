package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Riscotrabalho;
import br.com.linkcom.sined.geral.dao.RiscotrabalhoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RiscotrabalhoService extends GenericService<Riscotrabalho> {	
	
	private RiscotrabalhoDAO riscotrabalhoDAO;
	
	public void setRiscotrabalhoDAO(RiscotrabalhoDAO riscotrabalhoDAO) {
		this.riscotrabalhoDAO = riscotrabalhoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Riscotrabalho> getRiscosByCargo(Cargo cargo) {
		return riscotrabalhoDAO.getRiscosByCargo(cargo);
	}
	
}
