package br.com.linkcom.sined.geral.service;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoautori;
import br.com.linkcom.sined.geral.dao.DocumentoautoriDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoautoriService extends GenericService<Documentoautori>{


	DocumentoautoriDAO documentoautorizacaoDAO;
	
	public void setDocumentoautorizacaoDAO(
			DocumentoautoriDAO documentoautorizacaoDAO) {
		this.documentoautorizacaoDAO = documentoautorizacaoDAO;
	}
	
	/**
	 * Prepara a autoriza��o da conta a pagar.
	 * 
	 * @param documentohistorico
	 * @param documento
	 * @return 
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public Documentoautori gerarDocumentoAutorizacao(Money valor, String obs, Documento documento) {
		
		Documentoautori documentoautorizacao = new Documentoautori();
		
		documentoautorizacao.setData(new Date(System.currentTimeMillis()));
		documentoautorizacao.setDocumento(documento);
		documentoautorizacao.setObservacao(obs);
		documentoautorizacao.setValor(valor);
		
		return documentoautorizacao;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoautoriDAO#deleteAutorizacao
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void deleteAutorizacao(Documento documento) {
		documentoautorizacaoDAO.deleteAutorizacao(documento);	
	}

	public boolean hasAutorizacao(Documento documento) {
		return documentoautorizacaoDAO.hasAutorizacao(documento);	
	}
	
}
