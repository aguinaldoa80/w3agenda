package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.dao.ResponsavelFreteDAO;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.responsavelfrete.ResponsavelFreteRESTModel;

public class ResponsavelFreteService extends GenericService<ResponsavelFrete> {

	private ResponsavelFreteDAO responsavelFreteDAO;
	
	public void setResponsavelFreteDAO(ResponsavelFreteDAO responsavelFreteDAO) {
		this.responsavelFreteDAO = responsavelFreteDAO;
	}
	
	/* singleton */
	private static ResponsavelFreteService instance;
	public static ResponsavelFreteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ResponsavelFreteService.class);
		}
		return instance;
	}
	
	public List<ResponsavelFreteRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ResponsavelFreteRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ResponsavelFreteRESTModel> lista = new ArrayList<ResponsavelFreteRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(ResponsavelFrete bean : responsavelFreteDAO.findForAndroid(whereIn))
				lista.add(new ResponsavelFreteRESTModel(bean));
		}
		
		return lista;
	}
	
	public List<GenericBean> findForWSVenda(){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findAll());
	}
}
