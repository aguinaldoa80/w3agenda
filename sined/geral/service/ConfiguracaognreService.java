package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ConfiguracaognreDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaognreService extends GenericService<Configuracaognre> {
	
	private ConfiguracaognreDAO configuracaognreDAO;

	public void setConfiguracaognreDAO(ConfiguracaognreDAO configuracaognreDAO) {
		this.configuracaognreDAO = configuracaognreDAO;
	}
	
	public boolean hasDuplicateByEmpresaAndCodigo(Configuracaognre configuracaognre) {
		return configuracaognreDAO.hasDuplicateByEmpresaAndCodigo(configuracaognre);
	}

	public List<Configuracaognre> findByEmpresa(Empresa empresa) {
		return configuracaognreDAO.findByEmpresa(empresa);
	}
	
}
