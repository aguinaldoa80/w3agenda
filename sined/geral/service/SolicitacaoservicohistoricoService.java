package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicohistorico;
import br.com.linkcom.sined.geral.dao.SolicitacaoservicohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaoservicohistoricoService extends GenericService<Solicitacaoservicohistorico> {

	private SolicitacaoservicohistoricoDAO solicitacaoservicohistoricoDAO;
	
	public void setSolicitacaoservicohistoricoDAO(
			SolicitacaoservicohistoricoDAO solicitacaoservicohistoricoDAO) {
		this.solicitacaoservicohistoricoDAO = solicitacaoservicohistoricoDAO;
	}
	
	public List<Solicitacaoservicohistorico> findBySolicitacaoservico(Solicitacaoservico form) {
		return solicitacaoservicohistoricoDAO.findBySolicitacaoservico(form);
	}

	
}
