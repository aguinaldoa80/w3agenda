package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.geral.dao.ProducaoordemmaterialmateriaprimaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemmaterialmateriaprimaService extends GenericService<Producaoordemmaterialmateriaprima> {
	
	private ProducaoordemmaterialmateriaprimaDAO producaoordemmaterialmateriaprimaDAO;

	public void setProducaoordemmaterialmateriaprimaDAO(ProducaoordemmaterialmateriaprimaDAO producaoordemmaterialmateriaprimaDAO) {
		this.producaoordemmaterialmateriaprimaDAO = producaoordemmaterialmateriaprimaDAO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialmateriaprimaDAO#findByProducaoordemmaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterialmateriaprima> findByProducaoordemmaterial(String whereIn){
		return producaoordemmaterialmateriaprimaDAO.findByProducaoordemmaterial(whereIn);
	}
	
}
