package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.ValecompraDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.valecompra.ValecompraRESTModel;

public class ValecompraService extends GenericService<Valecompra> {
	
	private ParametrogeralService parametrogeralService;
	private ContareceberService contareceberService;
	private ValecompraDAO valecompraDAO;
	private GarantiareformaitemService garantiareformaitemService;
//	private DocumentoService documentoService;
	
	public void setValecompraDAO(ValecompraDAO valecompraDAO) {
		this.valecompraDAO = valecompraDAO;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setGarantiareformaitemService(
			GarantiareformaitemService garantiareformaitemService) {
		this.garantiareformaitemService = garantiareformaitemService;
	}
//	public void setDocumentoService(DocumentoService documentoService) {
//		this.documentoService = documentoService;
//	}
	
	/* singleton */
	private static ValecompraService instance;
	public static ValecompraService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ValecompraService.class);
		}
		return instance;
	}
	
	/**
	 * Cria vale compra no processo de processamento de arquivo de retorno
	 *
	 * @param bean
	 * @param listaDocumentoValecompra
	 * @param listaMov
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public void createValecompraArquivoRetorno(ArquivoConfiguravelRetornoBean bean, List<Documento> listaDocumentoValecompra, List<Movimentacao> listaMov) {
		this.createValecompraArquivoRetorno(null, bean, listaDocumentoValecompra, listaMov);
	}
	
	/**
	 * Cria vale compra no processo de processamento de arquivo de retorno
	 *
	 * @param request
	 * @param bean
	 * @param listaDocumento
	 * @param listaMov
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	private void createValecompraArquivoRetorno(WebRequestContext request, ArquivoConfiguravelRetornoBean bean, List<Documento> listaDocumento, List<Movimentacao> listaMov) {
		try {
			String gerarvalecompra = parametrogeralService.getValorPorNome(Parametrogeral.GERAR_VALECOMPRA);
			if("TRUE".equalsIgnoreCase(gerarvalecompra) && listaDocumento != null && !listaDocumento.isEmpty()){
				if(listaMov != null && !listaMov.isEmpty()){
					Double valortotalmovimentacao = 0.0;
					for(Movimentacao m : listaMov){
						if(m.getValor() != null){
							valortotalmovimentacao += m.getValor().getValue().doubleValue();
						}
					}
					
					this.createValecompraByDocumento(request, bean, listaDocumento, SinedUtil.round(valortotalmovimentacao, 2), null, null, null, true);
				}
			}
		} catch (Exception e) {
			request.addError("Erro na cria��o do vale compra.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Cria vale compra a partir da lista de documentos e o valor passado por par�metro
	 *
	 * @param request
	 * @param listaDocumento
	 * @param valor
	 * @author Rodrigo Freitas
	 * @param dtpagamento 
	 * @param listaDocumentoForValecompra 
	 * @since 26/09/2014
	 */
	public void createValecompraByDocumento(WebRequestContext request, List<Documento> listaDocumento, Double valor, Double valorrestante, Date dtpagamento, List<Documento> listaDocumentoForValecompra) {
		this.createValecompraByDocumento(request, null, listaDocumento, valor, valorrestante, dtpagamento, listaDocumentoForValecompra, false);
	}
	
	/**
	 * Cria vale compra a partir da lista de documentos e o valor passado por par�metro
	 *
	 * @param request
	 * @param bean
	 * @param listaDocumento
	 * @param valor
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	private void createValecompraByDocumento(WebRequestContext request, ArquivoConfiguravelRetornoBean bean, List<Documento> listaDocumento, Double valor, Double valorrestante, Date dtpagamento, List<Documento> listaDocumentoForValecompra, boolean fromArquivoRetorno) {
		List<Valecompra> listaValecompra = contareceberService.criaValecompraByCliente(listaDocumento, valor, valorrestante, dtpagamento, listaDocumentoForValecompra, fromArquivoRetorno);
		if(listaValecompra != null && !listaValecompra.isEmpty()){
			for(Valecompra valecompra : listaValecompra){
				try {
					this.saveOrUpdate(valecompra);
					if(bean == null && valecompra.getValor() != null &&
						valecompra.getCliente() != null && valecompra.getCliente().getNome() != null){
						request.addMessage("Foi gerado um vale-compra no valor de "+valecompra.getValor().toString()+" para o cliente "+valecompra.getCliente().getNome()+".");	
					}
				} catch (Exception e) {
					String mensagem = "Erro ao criar vale compra do cliente " + valecompra.getCliente().getNome();
					if(bean != null){
						bean.addErro(mensagem);
					} else if(request != null){
						request.addError(mensagem);
					}
				}
			}
		}
	}
	
	/**
	 * Retorna o saldo do vale compra do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public Money getSaldoByCliente(Cliente cliente) {
		return this.getSaldoByCliente(cliente, null);
	}
	
	/**
	 * Retorna o saldo do vale compra do cliente
	 *
	 * @param cliente
	 * @param whereInExcludeValecompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public Money getSaldoByCliente(Cliente cliente, String whereInExcludeValecompra) {
		List<Valecompra> listaValecompraAntigo = this.findByCliente(cliente);
		List<Valecompra> listaValecompraNovo = new ArrayList<Valecompra>();
		
		if(whereInExcludeValecompra != null && !"".equals(whereInExcludeValecompra)){
			String[] ids = whereInExcludeValecompra.split(",");
			List<String> listaIds = Arrays.asList(ids);
			for (Valecompra valecompra : listaValecompraAntigo) {
				if(valecompra.getCdvalecompra() != null && !listaIds.contains(valecompra.getCdvalecompra().toString())){
					listaValecompraNovo.add(valecompra);
				}
			}
		} else {
			listaValecompraNovo.addAll(listaValecompraAntigo);
		}
		
		return this.getSaldoByCliente(listaValecompraNovo);
	}
	
	/**
	 * Retorna o saldo do vale compra
	 *
	 * @param listaValecompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public Money getSaldoByCliente(List<Valecompra> listaValecompra) {
		Money saldo = new Money();
		
		for (Valecompra valecompra : listaValecompra) {
			if(valecompra.getTipooperacao() != null){
				if(valecompra.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
					saldo = saldo.subtract(valecompra.getValor());
				} else if(valecompra.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
					saldo = saldo.add(valecompra.getValor());
				}
			}
		}
		return saldo;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public List<Valecompra> findByCliente(Cliente cliente) {
		return valecompraDAO.findByCliente(cliente);
	}	
	
	/**
	 * Action no pedido de venda, venda e or�amento que busca o saldo de vale compra do cliente
	 *
	 * @param cliente
	 * @param whereInValecompra 
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public ModelAndView actionSaldovalecompraVendaPedidoOrcamento(Cliente cliente, String whereInValecompra) {
		JsonModelAndView json = new JsonModelAndView();
		if(cliente != null && cliente.getCdpessoa() != null){
			json.addObject("saldoValecompra", this.getSaldoByCliente(cliente, whereInValecompra));
		}
		return json;
	}
	
	public List<ValecompraRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ValecompraRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ValecompraRESTModel> lista = new ArrayList<ValecompraRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Valecompra bean : valecompraDAO.findForAndroid(whereIn))
				lista.add(new ValecompraRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * Action no pedido de venda, venda e or�amento que busca o saldo de vale compra do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public ModelAndView actionSaldovalecompraVendaPedidoOrcamento(Pedidovenda pedidovenda, Cliente cliente) {
		String whereInValecompra = null;
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			Valecompra valecompra = findByPedidovenda(pedidovenda);
			if(valecompra != null && valecompra.getCdvalecompra() != null){
				whereInValecompra = valecompra.getCdvalecompra().toString();
			}
		}
		return this.actionSaldovalecompraVendaPedidoOrcamento(cliente, whereInValecompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param valecompra
	 * @param valor
	 * @author Rafael Salvio
	 */
	public void updateSaldo(Valecompra valecompra, Money valor){
		valecompraDAO.updateSaldo(valecompra, valor);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Valecompra> findCreditoByCliente(Cliente cliente) {
		return valecompraDAO.findCreditoByCliente(cliente);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ValecompraDAO#findByPedidovenda(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public Valecompra findByPedidovenda(Pedidovenda pedidovenda) {
		List<Valecompra> lista = valecompraDAO.findByPedidovenda(pedidovenda);
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	public Valecompra findByPedidovenda(Pedidovenda pedidovenda, Tipooperacao tipooperacao) {
		List<Valecompra> lista = valecompraDAO.findByPedidovenda(pedidovenda, tipooperacao);
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	public List<Valecompra> findAllByPedidovenda(Pedidovenda pedidovenda) {
		return valecompraDAO.findByPedidovenda(pedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ValecompraDAO#updateSaldoByValecompra(Valecompra valecompra, Money valor)
	*
	* @param valecompra
	* @param valor
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public void updateSaldoByValecompra(Valecompra valecompra, Money valor){
		valecompraDAO.updateSaldoByValecompra(valecompra, valor);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ValecompraDAO#findByVenda(Venda venda, Tipooperacao tipooperacao)
	*
	* @param venda
	* @param tipooperacao
	* @return
	* @since 15/01/2016
	* @author Luiz Fernando
	*/
	public Valecompra findByVenda(Venda venda, Tipooperacao tipooperacao) {
		List<Valecompra> lista = valecompraDAO.findByVenda(venda, tipooperacao);
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ValecompraDAO#findSaldoValecompra(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<GenericBean> findSaldoValecompra(String whereIn) {
		return valecompraDAO.findSaldoValecompra(whereIn);
	}
	
	public boolean estornarContaReceber(boolean validar, String whereInCddocumento){
		/*if (whereInCddocumento==null || whereInCddocumento.trim().equals("")){
			return true;
		}
		
		Map<Integer, Money> mapaSaldoCliente = new HashMap<Integer, Money>();
		Map<Integer, Money> mapaValorValecompra = new HashMap<Integer, Money>();
		Map<Integer, Set<Integer>> mapaListaCddocumento = new HashMap<Integer, Set<Integer>>();
		
		for (Documento documento: documentoService.carregaDocumentos(whereInCddocumento)){
			if (documento.getPessoa()!=null && documento.getListaValecompraorigem()!=null && !documento.getListaValecompraorigem().isEmpty()){
				Integer cdpessoa = documento.getPessoa().getCdpessoa();
				
				if (!mapaSaldoCliente.containsKey(cdpessoa)){
					List<GenericBean> listaSaldo = valecompraDAO.findSaldoValecompra(cdpessoa.toString());
					Money saldo = !listaSaldo.isEmpty() ? new Money((Double) listaSaldo.get(0).getValue()) : new Money();
					mapaSaldoCliente.put(cdpessoa, saldo);
				}
				
				for (Valecompraorigem valecompraorigem: documento.getListaValecompraorigem()){
					Valecompra valecompra = valecompraorigem.getValecompra();
					
					if (valecompra!=null && valecompra.getValor()!=null && Tipooperacao.TIPO_CREDITO.equals(valecompra.getTipooperacao())){
						mapaValorValecompra.put(cdpessoa, SinedUtil.zeroIfNull(mapaValorValecompra.get(cdpessoa)).add(valecompra.getValor()));
						
						Set<Integer> listaDocumentoAux = mapaListaCddocumento.get(cdpessoa);
						if (listaDocumentoAux==null){
							listaDocumentoAux = new HashSet<Integer>();
						}
						listaDocumentoAux.add(documento.getCddocumento());
						mapaListaCddocumento.put(cdpessoa, listaDocumentoAux);
					}
				}
			}
		}
		
		if (validar){
			for (Integer cdpessoa: mapaValorValecompra.keySet()){
				Money saldo = mapaSaldoCliente.get(cdpessoa);
				if (saldo.toLong()<mapaValorValecompra.get(cdpessoa).toLong()){
					return false;
				}
			}
			
			return true;
		}else {
			for (Integer cdpessoa: mapaValorValecompra.keySet()){
				Valecompra valecompra = new Valecompra();
				valecompra.setCliente(new Cliente(cdpessoa));
				valecompra.setIdentificacao("Referente ao estorno de baixa da(s) conta(s) " + CollectionsUtil.concatenate(mapaListaCddocumento.get(cdpessoa), ", "));
				valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				valecompra.setValor(mapaValorValecompra.get(cdpessoa));
				valecompra.setData(SinedDateUtils.currentDate());
				saveOrUpdate(valecompra);
			}
			
			return true;
		}*/
		
		return true; //TEMPORARIAMENTE AT� A AN�LISE REVISAR
	}
	
	public Valecompra criaValecompraByGarantia(Garantiareformaitem garantiareformaitem) throws SinedException{
		Valecompraorigem valecompraorigem = new Valecompraorigem();
		valecompraorigem.setGarantiareforma(garantiareformaitem.getGarantiareforma());
		Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
		listaValecompraorigem.add(valecompraorigem);
		garantiareformaitemService.calculaValorGarantia(garantiareformaitem, null, null);
		
		Valecompra valecompra = new Valecompra();
		valecompra.setCliente(garantiareformaitem.getGarantiareforma().getCliente());
		valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		valecompra.setValor(garantiareformaitem.getValorCreditoGarantia());
		valecompra.setData(SinedDateUtils.currentDate());
		valecompra.setIdentificacao("Cr�dito gerado a partir de garantia de reforma");
		valecompra.setListaValecompraorigem(listaValecompraorigem);
		
		return valecompra;
	}
	
	public List<Valecompra> findByGarantiareforma(Garantiareforma garantiareforma, Tipooperacao tipooperacao) {
		return valecompraDAO.findByGarantiareforma(garantiareforma, tipooperacao);
	}
	
	public void saveValeCompra(List<Valecompra> lista){
		for(Valecompra bean: lista){
			this.saveOrUpdate(bean);
		}
	}
	
}