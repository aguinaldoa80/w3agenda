package br.com.linkcom.sined.geral.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioitem;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.CalendarioDAO;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class CalendarioService extends GenericService<Calendario> {

	private CalendarioDAO calendarioDAO;
	private EnderecoService enderecoService;
	
	public void setCalendarioDAO(CalendarioDAO calendarioDAO) {
		this.calendarioDAO = calendarioDAO;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	/* singleton */
	private static CalendarioService instance;
	public static CalendarioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CalendarioService.class);
		}
		return instance;
	}
	
	/**
	 * Retorna um calend�rio e sua lista de feriados de um determinado projeto
	 *
	 * @see br.com.linkcom.sined.geral.dao.CalendarioDAO#findByProjeto(Projeto)
	 *
	 * @param projeto
	 * @return calendario ou nulo, caso nenhum n�o exista nenhum calendario associado ao projeto
	 * @throws SinedException - caso exista mais de um calend�rio associado ao projeto.
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Calendario getByProjeto(Projeto projeto) {
		List<Calendario> listaCalendario = calendarioDAO.findByProjeto(projeto);
		if (listaCalendario != null && !listaCalendario.isEmpty()) {
			if (listaCalendario.size() > 1) {
				throw new SinedException("Existe mais de um calend�rio associado ao projeto.");
			}
			return listaCalendario.get(0);
		}
		return null;
	}
	
	/**
	 * Retorna um calend�rio e sua lista de feriados de um determinado or�amento
	 *
	 * @see 
	 *
	 * @param projeto
	 * @return calendario ou nulo, caso nenhum n�o exista nenhum calendario associado ao or�amento
	 * @throws SinedException - caso exista mais de um calend�rio associado ao or�amento.
	 * 
	 * @author Rodrigo Freitas
	 */	
	public Calendario getByOrcamento(Orcamento orcamento) {
		List<Calendario> listaCalendario = calendarioDAO.findByOrcamento(orcamento);
		if (listaCalendario != null && !listaCalendario.isEmpty()) {
			if (listaCalendario.size() > 1) {
				throw new SinedException("Existe mais de um calend�rio associado ao or�amento.");
			}
			return listaCalendario.get(0);
		}
		return null;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CalendarioDAO#findWithLista
	 * @param calendario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Calendario findWithLista(Projeto projeto) {
		return calendarioDAO.findWithLista(projeto);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CalendarioDAO#loadWithLista
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Calendario> loadWithLista(String whereIn,String orderBy, boolean asc) {
		return calendarioDAO.loadWithLista(whereIn, orderBy, asc);
	}	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param data
	 * @return
	 * @author Taidson
	 * @since 08/11/2010
	 */
	public Boolean isFeriado(Date data, Uf uf, Municipio municipio){
		return calendarioDAO.isFeriado(data, uf, municipio, false);
	}
	
	public Boolean isFeriado(Date data, Uf uf, Municipio municipio, boolean consideraFimSemana){
		return calendarioDAO.isFeriado(data, uf, municipio, consideraFimSemana);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Busca calendario geral 
	* 
	* @see	br.com.linkcom.sined.geral.dao.CalendarioDAO#findCalendarioGeral()
	*
	* @return
	* @since Aug 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Calendario> findCalendarioGeral(Uf uf, Municipio municipio){
		return calendarioDAO.findCalendarioGeral(uf, municipio);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param uf
	 * @param municipio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/02/2015
	 */
	public List<Calendario> findCalendarioGeralWithoutWhereEmpresaProjeto(Uf uf, Municipio municipio){
		return calendarioDAO.findCalendarioGeralWithoutWhereEmpresaProjeto(uf, municipio);
	}
	
	/**
	 * M�todo que retorna o calend�rio geral cadastrado no sistema.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/11/2012
	 */
	public List<Calendario> getCalendarioGeral(boolean inContextNeo, Uf uf, Municipio municipio){
		List<Calendario> listaCalendario = null;
		
		if(inContextNeo){
			listaCalendario = this.findCalendarioGeral(uf, municipio);
		} else {
			listaCalendario = this.findCalendarioGeralWithoutWhereEmpresaProjeto(uf, municipio);
		}
		
		return listaCalendario;
	}
	
	public List<Calendario> getCalendarioGeral(Uf uf, Municipio municipio){
		return this.getCalendarioGeral(true, uf, municipio);
	}
	
	public Calendario getCalendarioUnificado(Uf uf, Municipio municipio){
		return this.getCalendarioUnificado(false, uf, municipio);
	}
	
	public Calendario getCalendarioUnificado(boolean inContextNeo, Uf uf, Municipio municipio){
		Calendario calendarioAgrupado = null;
		List<Calendario> lista = this.getCalendarioGeral(uf, municipio);
		if(SinedUtil.isListNotEmpty(lista)){
			if(lista.size() == 1){
				calendarioAgrupado = lista.get(0);
			}else {
				for(Calendario c : lista){
					calendarioAgrupado = agrupaDiaUtilEFeriado(calendarioAgrupado, c);
				}
			}
		}
		return calendarioAgrupado;
	}
	
	private Calendario agrupaDiaUtilEFeriado(Calendario calendarioAgrupado, Calendario calendario) {
		if(calendarioAgrupado == null){
			calendarioAgrupado = calendario;
		}else if(calendario != null){
			if(calendario.getSabadoutil() != null && calendario.getSabadoutil()){
				calendarioAgrupado.setSabadoutil(calendario.getSabadoutil());
			}
			if(calendario.getDomingoutil() != null && calendario.getDomingoutil()){
				calendarioAgrupado.setDomingoutil(calendario.getDomingoutil());
			}
			
			if(SinedUtil.isListNotEmpty(calendario.getListaCalendarioitem())){
				if(SinedUtil.isListNotEmpty(calendarioAgrupado.getListaCalendarioitem())){
					for(Calendarioitem calendarioitem : calendario.getListaCalendarioitem()){
						if(calendarioitem.getDtferiado() != null){
							boolean existeFeriado = false;
							for(Calendarioitem calendarioitemAgrupado : calendarioAgrupado.getListaCalendarioitem()){
								if(calendarioitemAgrupado.getDtferiado() != null && 
										calendarioitemAgrupado.getDtferiado().equals(calendarioitem.getDtferiado())){
									existeFeriado = true;
									break;
								}
							}
							if(!existeFeriado){
								calendarioAgrupado.getListaCalendarioitem().add(calendarioitem);
							}
						}
					}
				}else {
					calendarioAgrupado.setListaCalendarioitem(new ListSet<Calendarioitem>(Calendarioitem.class));
					calendarioAgrupado.getListaCalendarioitem().addAll(calendario.getListaCalendarioitem());
				}
			}
		}
		return calendarioAgrupado;
	}
	
	/**
	 * M�todo que retorna a quantidade de dias uteis entre datas. Considerando o calendario, caso tenha cadastrado.
	 *
	 * @param dtinicio
	 * @param dtfim
	 * @param calendario
	 * @return
	 * @author Luiz Fernando
	 * @since 13/11/2013
	 */
	public Integer getQtdeDiasUteis(Date dtinicio, Date dtfim, Calendario calendario) {
		Integer qtdeDiasuteis = 0;
		int dias = SinedDateUtils.diferencaDias(dtfim, dtinicio);
		
		Calendar dtLimiteAux = Calendar.getInstance();
		dtLimiteAux.setTimeInMillis(dtinicio.getTime());
		
		if(calendario != null){
			for (int i = 0; i <= dias; i++) {
				if(SinedDateUtils.diaValidoCalendar(new java.sql.Date(dtLimiteAux.getTimeInMillis()), calendario)){
					qtdeDiasuteis++;
				}
				dtLimiteAux.add(Calendar.DAY_OF_MONTH, 1);
			}
		} else {
			for (int i = 0; i <= dias; i++) {
				if(SinedDateUtils.diaValidoWithoutCalendar(new java.sql.Date(dtLimiteAux.getTimeInMillis()), false)){
					qtdeDiasuteis++;
				}
				dtLimiteAux.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		
		return qtdeDiasuteis;
	}
	
	public java.sql.Date proximoDiaUtil(Endereco endereco, Date dataApartir){
		Uf uf = null;
		java.sql.Date data = new java.sql.Date(dataApartir.getTime());
		Municipio mun = null;
		if(endereco != null){
			mun = endereco.getMunicipio();
			if(mun != null){
				uf = mun.getUf();
			}
		}
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(data);
		while(this.isFeriado(data, uf, mun, true)){
			data = SinedDateUtils.addDiasData(data, 1);
		}
		
		return data;
	}
	
	public void ajaxVerificaFeriado(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		String cdpessoa = request.getParameter("cdempresa");
		Endereco enderecoempresa = null;
		Municipio mun = null;
		Uf uf = null;
		if(Util.strings.isNotEmpty(cdpessoa)){
			enderecoempresa = enderecoService.getEnderecoPrincipalPessoa(new Pessoa(Integer.parseInt(cdpessoa)));
			if(enderecoempresa != null){
				mun = enderecoempresa.getMunicipio();
				if(mun != null){
					uf = mun.getUf();
				}
			}
		}
		Date data = request.getParameter("data") != null? DateUtils.stringToDate(request.getParameter("data")): null;
		Boolean isFeriado = this.isFeriado(data, uf, mun, true);
		view.println("var isFeriado = " + isFeriado.toString());
	}
	
	public void ajaxProximoDiaUtil(WebRequestContext request){
		String strData = request.getParameter("data");
		String cdpessoa = request.getParameter("cdempresa");
		Endereco enderecoempresa = null;
		if(Util.strings.isNotEmpty(cdpessoa)){
			enderecoempresa = enderecoService.getEnderecoPrincipalPessoa(new Pessoa(Integer.parseInt(cdpessoa)));
		}
		//Date data = DateUtils.stringToDate(strData);
		//java.sql.Date dataAux = new java.sql.Date(data.getTime());
		//data = DateUtils.incrementaData(dataAux, new Integer(1));
		Date data = this.proximoDiaUtil(enderecoempresa, DateUtils.stringToDate(strData));
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		view.println("var dataUtil = '" + new SimpleDateFormat("dd/MM/yyyy").format(data)+"'");
	}

	@Override
	protected ListagemResult<Calendario> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Calendario> listagemResult = calendarioDAO.findForExportacao(filtro);
		List<Calendario> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcalendario", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}
		
		return listagemResult;
	}
}
