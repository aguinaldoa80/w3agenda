package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.dao.ItemlistaservicoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ItemlistaservicoService extends GenericService<Itemlistaservico> {

	private ItemlistaservicoDAO itemlistaservicoDAO;
	
	public void setItemlistaservicoDAO(ItemlistaservicoDAO itemlistaservicoDAO) {
		this.itemlistaservicoDAO = itemlistaservicoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ItemlistaservicoDAO#loadByCodigo(String codigo)
	 * 
	 * @param codigo
	 * @return
	 * @since 30/03/2012
	 * @author Rodrigo Freitas
	 */
	public Itemlistaservico loadByCodigo(String codigo) {
		return itemlistaservicoDAO.loadByCodigo(codigo);
	}

}
