package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoadap;
import br.com.linkcom.sined.geral.dao.PessoadapDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoadapService extends GenericService<Pessoadap> {

	/* singleton */
	private static PessoadapService instance;
	public static PessoadapService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PessoadapService.class);
		}
		return instance;
	}
	
	private  PessoadapDAO pessoadapDAO;
	
	public void setPessoadapDAO(PessoadapDAO pessoadapDAO) {
		this.pessoadapDAO = pessoadapDAO;
	}
	

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/11/2016
	 */
	public List<Pessoadap> findByPessoa(Pessoa pessoa){
		return pessoadapDAO.findByPessoa(pessoa);
	}
	

}
