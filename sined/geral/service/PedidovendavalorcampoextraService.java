package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendavalorcampoextra;
import br.com.linkcom.sined.geral.dao.PedidovendavalorcampoextraDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaValorCampoExtraWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendavalorcampoextraService  extends GenericService<Pedidovendavalorcampoextra>{
	
	private PedidovendavalorcampoextraDAO pedidovendavalorcampoextraDAO;
	
	public void setPedidovendavalorcampoextraDAO(PedidovendavalorcampoextraDAO pedidovendavalorcampoextraDAO) {
		this.pedidovendavalorcampoextraDAO = pedidovendavalorcampoextraDAO;
	}

	/**
	 * Busca todos os campos extras de um determinado {@link Pedidovenda}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Pedidovendavalorcampoextra> findByPedidoVenda(Pedidovenda bean) {
		return pedidovendavalorcampoextraDAO.findByPedidoVenda(bean);
	}
	
	public List<PedidoVendaValorCampoExtraWSBean> toWSList(List<Pedidovendavalorcampoextra> listaPedidoVendaValorCampoExtra){
		List<PedidoVendaValorCampoExtraWSBean> lista = new ArrayList<PedidoVendaValorCampoExtraWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidoVendaValorCampoExtra)){
			for(Pedidovendavalorcampoextra pvce: listaPedidoVendaValorCampoExtra){
				PedidoVendaValorCampoExtraWSBean bean = new PedidoVendaValorCampoExtraWSBean();
				
				bean.setCampoextrapedidovendatipo(ObjectUtils.translateEntityToGenericBean(pvce.getCampoextrapedidovendatipo()));
				bean.setCdpedidovendavalorcampoextra(pvce.getCdpedidovendavalorcampoextra());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvce.getPedidovenda()));
				bean.setValor(pvce.getValor());
				
				lista.add(bean);
			}
		}
		return lista;
	}

}
