package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ConfiguracaotefDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaotefService extends GenericService<Configuracaotef>{
	
	private ConfiguracaotefDAO configuracaotefDAO;
	
	public void setConfiguracaotefDAO(ConfiguracaotefDAO configuracaotefDAO) {
		this.configuracaotefDAO = configuracaotefDAO;
	}

	public Configuracaotef loadByEmpresa(Empresa empresa) {
		return configuracaotefDAO.loadByEmpresa(empresa);
	}

}
