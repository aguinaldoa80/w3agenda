package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.dao.VendapagamentoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaPagamentoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendapagamentoService extends GenericService<Vendapagamento> {

	private VendapagamentoDAO vendapagamentoDAO;
	private DocumentotipoService documentotipoService;
	
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setVendapagamentoDAO(VendapagamentoDAO vendapagamentoDAO) {
		this.vendapagamentoDAO = vendapagamentoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param venda
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vendapagamento> findVendaPagamentoByVenda(Venda venda) {
		return vendapagamentoDAO.findVendaPagamentoByVenda(venda);
	}

	public void deleteAllFromVenda(Venda venda) {
		vendapagamentoDAO.deleteAllFromVenda(venda);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 17/02/2012
	 * @param bean
	 */
	public void updateValorVendaPagamento(Vendapagamento bean){
		vendapagamentoDAO.updateValorVendaPagamento(bean);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#findForChequeByDocumento(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendapagamento findForChequeByDocumento(Documento documento) {
		return vendapagamentoDAO.findForChequeByDocumento(documento);
	}

	/**
	 * Pega o valor total das taxas de uma lista de Vendapagamento.
	 *
	 * @param listavendapagamento
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public Money getTotalTaxaVenda(List<Vendapagamento> listavendapagamento) {
		Money taxavenda = new Money();
		if(listavendapagamento != null && listavendapagamento.size() > 0){
			for (Vendapagamento vendapagamento : listavendapagamento) {
				if(vendapagamento.getDocumentotipo() != null && vendapagamento.getDocumentotipo().getCddocumentotipo() != null){
					Documentotipo documentotipo = documentotipoService.load(vendapagamento.getDocumentotipo());
					if(documentotipo.getTaxavenda() != null && documentotipo.getTaxavenda() > 0){
						Money valortotal = vendapagamento.getValororiginal();
						Money taxa = new Money(documentotipo.getTaxavenda());
						if(documentotipo.getPercentual() != null && documentotipo.getPercentual()){
							taxavenda = taxavenda.add(valortotal.multiply(taxa).divide(new Money(100d)));
						}else {
							taxavenda = taxavenda.add(valortotal.add(taxa));
						}
					}
				}
			}
		}
		return taxavenda;
	}	
	

	/**
	 * Retorna o cdvenda da venda contida na vendapagamento retornada ou null caso esta n�o exista.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael salvio
	 * @since 16/04/2012
	 */
	public Integer verificaContaVendaFaturada(Integer cdconta) {
		
		Vendapagamento vendapagamento = vendapagamentoDAO.verificaContaVendaFaturada(cdconta);
		if(vendapagamento != null && vendapagamento.getVenda() != null){
			return vendapagamento.getVenda().getCdvenda();
		}
		return null;
	}
	

	/**
	 * Retorna o cdvenda da venda contida na vendapagamento retornada ou null caso esta n�o exista.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael salvio
	 * @since 19/04/2012
	 */
	public Integer findByConta(Integer cdconta){

		Vendapagamento vendapagamento = vendapagamentoDAO.findByConta(cdconta);
		if(vendapagamento != null && vendapagamento.getVenda() != null){
			return vendapagamento.getVenda().getCdvenda();
		}
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#findByCheque(Cheque cheque)
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendapagamento> findByCheque(Cheque cheque){
		return vendapagamentoDAO.findByCheque(cheque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#updateReferenciaCheque(String whereIn)
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 * @since 02/04/2014
	 */
	public void updateReferenciaCheque(String whereIn) {
		vendapagamentoDAO.updateReferenciaCheque(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#findForSubstituicaoByCheque(String whereInCheque, Documentoacao... documentoacao)
	*
	* @param whereInChequeAntigo
	* @param documentoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Vendapagamento> findForSubstituicaoByCheque(String whereInChequeAntigo, Documentoacao... documentoacao) {
		return vendapagamentoDAO.findForSubstituicaoByCheque(whereInChequeAntigo, documentoacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#updateVendapagamentoRemoveCheque(String whereInVendapagamento)
	*
	* @param whereInVendapagamento
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateVendapagamentoRemoveCheque(String whereInVendapagamento) {
		vendapagamentoDAO.updateVendapagamentoRemoveCheque(whereInVendapagamento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendapagamentoDAO#updateVendapagamentoChequeSubstituto(String whereInVendapagamento, Cheque chequeSubstituto)
	*
	* @param whereInVendapagamento
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateVendapagamentoChequeSubstituto(String whereInVendapagamento, Cheque chequeSubstituto) {
		vendapagamentoDAO.updateVendapagamentoChequeSubstituto(whereInVendapagamento, chequeSubstituto);
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.VendapagamentoService#existeDocumento(Venda venda)
	*
	* @param venda
	* @return
	* @since 20/05/2016
	* @author Luiz Fernando
	*/
	public boolean existeDocumento(Venda venda) {
		return vendapagamentoDAO.existeDocumento(venda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/12/2016
	 */
	public Integer countParcelaVendaByNota(Nota nf) {
		return vendapagamentoDAO.countParcelaVendaByNota(nf);
	}
	
	public void updateValororiginal(String whereInCdvendapagamento, Money valororiginal){
		vendapagamentoDAO.updateValororiginal(whereInCdvendapagamento, valororiginal);
	}
	
	public void updateDocumento(Vendapagamento vendapagamento) {
		vendapagamentoDAO.updateDocumento(vendapagamento);
	}
	
	public List<Vendapagamento> buscarPorVendaDoClienteSemContaReceber(Cliente cliente){
		return vendapagamentoDAO.buscarPorVendaDoClienteSemContaReceber(cliente);
	}
	
	public List<VendaPagamentoWSBean> toWSList(List<Vendapagamento> listaVendaPagamento){
		List<VendaPagamentoWSBean> lista = new ArrayList<VendaPagamentoWSBean>();
		if(SinedUtil.isListNotEmpty(listaVendaPagamento)){
			for(Vendapagamento vp: listaVendaPagamento){
				VendaPagamentoWSBean bean = new VendaPagamentoWSBean();
				bean.setAgencia(vp.getAgencia());
				bean.setBanco(vp.getBanco());
				bean.setCdvendapagamento(vp.getCdvendapagamento());
				bean.setCheque(ObjectUtils.translateEntityToGenericBean(vp.getAgencia()));
				bean.setConta(vp.getConta());
				bean.setCpfcnpj(vp.getCpfcnpj());
				bean.setDataparcela(new java.util.Date(vp.getDataparcela().getTime()));
				bean.setDocumento(ObjectUtils.translateEntityToGenericBean(vp.getDocumento()));
				bean.setDocumentoantecipacao(ObjectUtils.translateEntityToGenericBean(vp.getDocumentoantecipacao()));
				bean.setDocumentotipo(ObjectUtils.translateEntityToGenericBean(vp.getDocumentotipo()));
				bean.setEmitente(vp.getEmitente());
				bean.setNumero(vp.getNumero());
				bean.setPrazomedio(vp.getPrazomedio());
				bean.setValorjuros(vp.getValorjuros());
				bean.setValororiginal(vp.getValororiginal());
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vp.getVenda()));
				
				lista.add(bean);
			}
		}
		return lista;
	}
	
	public void updateInfoCartao(Vendapagamento vendapagamento, String autorizacao, String bandeira, String adquirente) {
		vendapagamentoDAO.updateInfoCartao(vendapagamento, autorizacao, bandeira, adquirente);
	}
	
	public List<Vendapagamento> findByVendaForIntegracaoTEF(String whereInVenda) {
		return vendapagamentoDAO.findByVendaForIntegracaoTEF(whereInVenda);
	}
}