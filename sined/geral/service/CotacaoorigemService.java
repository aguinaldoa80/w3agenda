package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.dao.CotacaoorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CotacaoorigemService extends GenericService<Cotacaoorigem> {

	private CotacaoorigemDAO cotacaoorigemDAO;
	
	public void setCotacaoorigemDAO(CotacaoorigemDAO cotacaoorigemDAO) {
		this.cotacaoorigemDAO = cotacaoorigemDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoorigemDAO#findByCotacao
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaoorigem> findByCotacao(Cotacao cotacao){
		return cotacaoorigemDAO.findByCotacao(cotacao);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaoorigem> findSolicitacoesByCotacao(Cotacao cotacao) {
		return cotacaoorigemDAO.findSolicitacoesByCotacao(cotacao);
	}
	
	/**
	 * Carrega a lista de COtacaoorigem a partir de cotac�es.
	 * 
	 * @param whereIn
	 * @return
	 * @author Thiago Clemente
	 * 
	 */
	public List<Cotacaoorigem> findByCotacao(String whereIn){
		return cotacaoorigemDAO.findByCotacao(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereInCdSolicitacao
	 * @param cdcotacao
	 */
	public void deleteCotacaoOrigem(String whereInCdSolicitacao,Integer cdcotacao) {
		cotacaoorigemDAO.deleteCotacaoOrigem(whereInCdSolicitacao, cdcotacao);
	}
}
