package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipodependente;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipodependenteService extends GenericService<Tipodependente> {	
	
	/* singleton */
	private static TipodependenteService instance;
	public static TipodependenteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipodependenteService.class);
		}
		return instance;
	}
	
}
