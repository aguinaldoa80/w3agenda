package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.dao.LocalarmazenagemempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.localarmazenagemempresa.LocalarmazenagemempresaRESTModel;

public class LocalarmazenagemempresaService extends GenericService<Localarmazenagemempresa> {

	private LocalarmazenagemempresaDAO localarmazenagemempresaDAO;

	public void setLocalarmazenagemempresaDAO(
			LocalarmazenagemempresaDAO localarmazenagemempresaDAO) {
		this.localarmazenagemempresaDAO = localarmazenagemempresaDAO;
	}
	
	/* singleton */
	private static LocalarmazenagemempresaService instance;
	public static LocalarmazenagemempresaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LocalarmazenagemempresaService.class);
		}
		return instance;
	}
	
	public List<Localarmazenagemempresa> findByLocalarmazenagem(Localarmazenagem form) {
		return localarmazenagemempresaDAO.findByLocalarmazenagem(form);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.LocalarmazenagemempresaService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<LocalarmazenagemempresaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LocalarmazenagemempresaDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<LocalarmazenagemempresaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<LocalarmazenagemempresaRESTModel> lista = new ArrayList<LocalarmazenagemempresaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Localarmazenagemempresa bean : localarmazenagemempresaDAO.findForAndroid(whereIn))
				lista.add(new LocalarmazenagemempresaRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * M�todo que carrega o localarmazenagem e empresa a partir de um cnpj para ajuste de estoque realizado pelo WMS
	 * 
	 * @param cnpj
	 * @return
	 * @author Rafael Salvio
	 */
	public Localarmazenagemempresa findForAjusteEstoqueWMS(Cnpj cnpj){
		return localarmazenagemempresaDAO.findForAjusteEstoqueWMS(cnpj);
	}
}