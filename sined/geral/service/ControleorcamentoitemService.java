package br.com.linkcom.sined.geral.service;

import java.util.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.dao.ControleorcamentoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ControleorcamentoitemService extends GenericService<Controleorcamentoitem>{

	private ControleorcamentoitemDAO controleorcamentoitemDAO;

	public void setControleorcamentoitemDAO(
			ControleorcamentoitemDAO controleorcamentoitemDAO) {
		this.controleorcamentoitemDAO = controleorcamentoitemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see	 br.com.linkcom.sined.geral.service.ControleorcamentoitemService#findByContagerencialWhereIn
	 *
	 * @param whereInContagerencial
	 * @param whereInCentrocusto 
	 * @param exercicio
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamentoitem> findByContagerencialWhereIn(String whereInContagerencial, String whereInCentrocusto, Date dtExercicio, Integer cdempresa){
		return controleorcamentoitemDAO.findByContagerencialWhereIn(whereInContagerencial, whereInCentrocusto, dtExercicio, cdempresa);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca o valor or�ado da conta gerencial
	 * 
	 * @see	br.com.linkcom.sined.geral.dao.ControleorcamentoitemDAO#findForValorOr�adoByContagerencial(Contagerencial contagerencial, Empresa empresa, Integer exercicio)
	 *
	 * @param contagerencial
	 * @param empresa
	 * @param exercicio
	 * @return
	 * @author Luiz Fernando
	 */
	public Controleorcamentoitem findForValorOr�adoByContagerencial(Contagerencial contagerencial, Centrocusto centrocusto, Empresa empresa, Date dtExercicio, Tipooperacao tipooperacao, Projeto projeto) {
		return controleorcamentoitemDAO.findForValorOr�adoByContagerencial(contagerencial, centrocusto, empresa, dtExercicio, tipooperacao, projeto);
	}
	
	public List<Controleorcamentoitem> findByControleOrcamento(Controleorcamento controleorcamento) {
		return controleorcamentoitemDAO.findByControleOrcamento(controleorcamento);
	}

	public Boolean verificaContaGerencialMensal(Controleorcamentoitem bean) {
		return controleorcamentoitemDAO.verificaContaGerencialMensal(bean);
	}

	public Boolean verificaContaGerencialAnual(Controleorcamentoitem bean) {
		return controleorcamentoitemDAO.verificaContaGerencialAnual(bean);
	}
}
