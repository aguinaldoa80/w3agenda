package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Clienterelacao;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.dao.ClienterelacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ClienterelacaoService extends GenericService<Clienterelacao>{
	
	private ClienterelacaoDAO clienterelacaoDAO;
	
	public void setClienterelacaoDAO(ClienterelacaoDAO clienterelacaoDAO) {
		this.clienterelacaoDAO = clienterelacaoDAO;
	}
	
	public List<Clienterelacao> findByColaborador(Colaborador colaborador){
		return clienterelacaoDAO.findByColaborador(colaborador); 
	}
//	
//	public void delete(Colaborador bean, List<Clienterelacao> id){
//		clienterelacaoDAO.delete(bean, id);
//	}
//	
//	public void saveOrUpdate(List<Clienterelacao> listaClienterelacao){
//		clienterelacaoDAO.saveOrUpdate(listaClienterelacao);
//	}

}
