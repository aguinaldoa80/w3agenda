package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.dao.ProspeccaoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProspeccaoService extends GenericService<Prospeccao> {

	private InteracaoitemService interacaoitemService;
	private ProspeccaoDAO prospeccaoDAO;
	private InteracaoService interacaoService;
	private ContatoService contatoService;
	
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}
	public void setInteracaoitemService(
			InteracaoitemService interacaoitemService) {
		this.interacaoitemService = interacaoitemService;
	}
	public void setProspeccaoDAO(ProspeccaoDAO prospeccaoDAO) {
		this.prospeccaoDAO = prospeccaoDAO;
	}
	
	
	@Override
	public void delete(Prospeccao bean) {
		bean = loadWithInteracao(bean);
		Interacao interacao = bean.getInteracao();
		super.delete(bean);
		if (interacao != null && interacao.getCdinteracao() != null) {
			List<Interacaoitem> listaItem = interacaoitemService.findByInteracao(interacao);
			for (Interacaoitem interacaoitem : listaItem) {
				interacaoitemService.delete(interacaoitem);
			}
			interacaoService.delete(interacao);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProspeccaoDAO#loadWithInteracao
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Prospeccao loadWithInteracao(Prospeccao prospeccao){
		return prospeccaoDAO.loadWithInteracao(prospeccao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProspeccaoDAO#updateInteracao
	 * @param prospeccao
	 * @author Rodrigo Freitas
	 */
	public void updateInteracao(Prospeccao prospeccao){
		prospeccaoDAO.updateInteracao(prospeccao);
	}


	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProspeccaoService#loadWithListaProposta
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Prospeccao> loadWithListaProposta(String whereIn) {
		return prospeccaoDAO.loadWithListaProposta(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProspeccaoDAO#loadForNew
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Prospeccao loadForNew(Prospeccao prospeccao) {
		return prospeccaoDAO.loadForNew(prospeccao);
	}
	
	public List<Contato> retornarListaContato(Cliente cliente){
		return contatoService.findByPessoaCombo(cliente);
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		return new JsonModelAndView().addObject("lista", contatoService.findByPessoaCombo(cliente));
	}

	public void processarListagem(ListagemResult<Prospeccao> listagemResult) {
		List<Prospeccao> list = listagemResult.list();

		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdprospeccao", ",");
		list.removeAll(list);
		list.addAll(loadWithListaProposta(whereIn));
	}
	
	@Override
	protected ListagemResult<Prospeccao> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Prospeccao> listagemResult = super.findForExportacao(filtro);
		processarListagem(listagemResult);
		
		return listagemResult;
	}
	
	
	public Boolean verificaProspeccaoPorContato(Contato contato){
		return prospeccaoDAO.verificaProspeccaoPorContato(contato);
	}
}
