package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Bdi;
import br.com.linkcom.sined.geral.bean.BdiComposicaoOrcamentoForm;
import br.com.linkcom.sined.geral.bean.BdiForm;
import br.com.linkcom.sined.geral.bean.Bdicalculotipo;
import br.com.linkcom.sined.geral.bean.Bdicomposicaoorcamento;
import br.com.linkcom.sined.geral.bean.Bdiitem;
import br.com.linkcom.sined.geral.bean.Bditributo;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.dao.BdiDAO;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiItemForm;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiTributoForm;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.BdiReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BdiService extends GenericService<Bdi>{
	
	private BdiDAO bdiDAO;
	private ComposicaoorcamentoService composicaoorcamentoService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private OrcamentoService orcamentoService;
	private TarefaorcamentoService tarefaorcamentoService;
	private CustomaoobraitemService customaoobraitemService;
	
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}
	public void setBdiDAO(BdiDAO bdiDAO) {
		this.bdiDAO = bdiDAO;
	}
	public void setComposicaoorcamentoService(ComposicaoorcamentoService composicaoorcamentoService) {
		this.composicaoorcamentoService = composicaoorcamentoService;
	}
	public void setOrcamentorecursohumanoService(OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	public void setCustomaoobraitemService(CustomaoobraitemService customaoobraitemService) {
		this.customaoobraitemService = customaoobraitemService;
	}
	
	public void deleteBdi(BdiForm bean) {
		if(bean != null && bean.getCdBdi() != null){
			this.delete(new Bdi(bean.getCdBdi()));
		}
	}
	
	/***
	 * Retorna o BDI com seus itens e composi��es de um determinado or�amento
	 * Caso ainda n�o exista o BDI, ser� criado um com os valores padr�es do or�amento e j� ser� salvo no banco de dados
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BdiDAO#getByOrcamento(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.BdiService#montaBdi(Orcamento, Bdi)
	 * 
	 * @param orcamento
	 * @return BdiForm
	 * @throws SinedException - caso o par�metro or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public BdiForm getByOrcamentoFlex(Orcamento orcamento) {
		Bdi bdi = bdiDAO.getByOrcamento(orcamento);
		
		//Converte o objeto vindo do banco para o objeto utilizado na aplica��o
		BdiForm bdiForm = montaBdi(orcamento, bdi);
		
		if (bdi == null) {
			saveBdiFlex(bdiForm);
			bdi = bdiDAO.getByOrcamento(orcamento);
			bdiForm = montaBdi(orcamento, bdi);
		}
		
		return bdiForm;
	}
	
	/***
	 * Retorna o BDI com seus itens e composi��es de um determinado or�amento
	 * Caso ainda n�o exista o BDI, ser� criado um com os valores padr�es do or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertBdiToBdiForm(Bdi)
	 * @see br.com.linkcom.sined.geral.service.BdiService#atualizaCustoArvoreBdiComposicao(List)
	 * @see br.com.linkcom.sined.geral.service.BdiService#atualizaTaxaArvoreBdiItem(List)
	 * @see br.com.linkcom.sined.geral.service.BdiService#calculaValoresBdi(BdiForm)
	 * 
	 * @param orcamento
	 * @param bdi
	 * @return BdiForm
	 * @throws SinedException - caso o par�metro or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public BdiForm montaBdi(Orcamento orcamento, Bdi bdi) {
		//Converte o objeto vindo do banco para o objeto utilizado na aplica��o
		BdiForm bdiForm = convertBdiToBdiForm(orcamento, bdi);
		
		//Atualiza os valores dos nodos pai da �rvore de composi��o do BDI
		atualizaCustoArvoreBdiComposicao(bdiForm.getListaBdiComposicaoOrcamentoForm());
		
		//Atualiza os valores dos nodos pai da �rvore de itens do BDI
		atualizaTaxaArvoreBdiItem(bdiForm.getListaBdiItemForm());
		
		//Atualiza os valores dos nodos pai da �rvore de tributos do BDI
		atualizaTaxaArvoreBdiTributo(bdiForm.getListaBdiTributoForm());
		
		//Calcula os valores referentes ao BDI
		bdiForm = calculaValoresBdi(bdiForm);
		
		return bdiForm;		
	}
	
	/***
	 * Converte um objeto do tipo Bdi para um objeto do tipo BdiForm
	 * Caso o par�metro bdi seja nulo, ser� criado um objeto do tipo BdiForm com os valores padr�es do or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#populateBdiForm(Orcamento, BdiForm, Bdi)
	 * 
	 * @param bdi
	 * @param orcamento
	 * @return BdiForm
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public BdiForm convertBdiToBdiForm(Orcamento orcamento, Bdi bdi) {
		BdiForm bdiForm = new BdiForm();
		bdiForm.setOrcamento(orcamento);
		bdiForm.setCdBdi(bdi != null ? bdi.getCdbdi() : null);
		bdiForm.setFolga(bdi != null ? bdi.getFolga() : null);
		bdiForm = populateBdiForm(orcamento, bdiForm, bdi);
		
		return bdiForm;
	}
	
	/***
	 * Converte um objeto do tipo Bdi para um objeto do tipo BdiForm
	 * Caso o par�metro bdi seja nulo, ser� criado um objeto do tipo BdiForm com os valores padr�es do or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#findByOrcamento(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.ComposicaoorcamentoService#calculaCustoTotalComposicao(Composicaoorcamento)
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMOD(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaCustoTotalMOI(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertListBdiItemToListBdiItemForm(List)
	 * @see br.com.linkcom.sined.geral.service.BdiService#montaArvoreBdiItem(List, BdiItemForm)
	 * 
	 * @param orcamento
	 * @param bdiForm
	 * @param bdi
	 * @return BdiForm
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private BdiForm populateBdiForm(Orcamento orcamento, BdiForm bdiForm, Bdi bdi) {
		
		BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoForm;
		BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoFormFilho;
		BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoFormNeto;		
		List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoForm;
		List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoFormFilho;
		List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoFormNeto;
		List<BdiItemForm> listaBdiItemForm;
		List<BdiTributoForm> listaBdiTributoForm;

		listaBdiComposicaoOrcamentoForm = new ArrayList<BdiComposicaoOrcamentoForm>();
		listaBdiComposicaoOrcamentoFormFilho = new ArrayList<BdiComposicaoOrcamentoForm>();
		listaBdiItemForm = new ArrayList<BdiItemForm>();
		listaBdiTributoForm = new ArrayList<BdiTributoForm>();
		Map<Composicaoorcamento,Bdicomposicaoorcamento> mapaComposicaoOrcamento = new HashMap<Composicaoorcamento, Bdicomposicaoorcamento>();
		Bdicomposicaoorcamento bdiComposicaoOrcamentoMapa;
		Boolean incluiMod = true;
		Boolean incluiMoi = true;	
		
		Double valoroutrastarefas = null;
		Double valorrecursogeraldireto = null;
		Double valoracrescimo = null;
		Double valoritenscustomo = null;
		Boolean incluioutrastarefas = true;
		Boolean incluiacrescimo = true;
		Boolean incluirecursogeraldireto = true;
		Boolean incluiitenscustomo = true;
		
		if (bdi != null) {
			if (bdi.getListaBdicomposicaoorcamento() != null) {
				for (Bdicomposicaoorcamento bdiComposicaoOrcamento : bdi.getListaBdicomposicaoorcamento()) {
					mapaComposicaoOrcamento.put(bdiComposicaoOrcamento.getComposicaoorcamento(), bdiComposicaoOrcamento);
				}
			}
			incluiMod = bdi.getIncluimod();
			incluiMoi = bdi.getIncluimoi();
			
			valoroutrastarefas = bdi.getValoroutrastarefas();
			valorrecursogeraldireto = bdi.getValorrecursogeraldireto();
			valoracrescimo = bdi.getValoracrescimo();
			valoritenscustomo = bdi.getValoritenscustomo();
			incluiacrescimo = bdi.getIncluiacrescimo() != null ? bdi.getIncluiacrescimo() : true;
			incluioutrastarefas = bdi.getIncluioutrastarefas() != null ? bdi.getIncluioutrastarefas() : true;
			incluirecursogeraldireto = bdi.getIncluirecursogeraldireto() != null ? bdi.getIncluirecursogeraldireto() : true;
			incluiitenscustomo = bdi.getIncluiitenscustomo() != null ? bdi.getIncluiitenscustomo() : true;
		}
		
		//Insere o nodo Raiz da �rvore (Or�amento)			
		bdiComposicaoOrcamentoForm = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoForm.setNome("Or�amento");
		bdiComposicaoOrcamentoForm.setMarcado(true);			
		
		//Insere os Recursos Gerais do BDI (Composi��o do Or�amento)
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("Composi��o indireta");
		bdiComposicaoOrcamentoFormFilho.setMarcado(true);
		
		//Busca as composi��es do or�amento
		List<Composicaoorcamento> listaComposicaoOrcamento = composicaoorcamentoService.findByOrcamento(orcamento);
		if (listaComposicaoOrcamento != null) {
			listaBdiComposicaoOrcamentoFormNeto = new ArrayList<BdiComposicaoOrcamentoForm>();
			for (Composicaoorcamento composicaoOrcamento : listaComposicaoOrcamento) {
				
				bdiComposicaoOrcamentoFormNeto = new BdiComposicaoOrcamentoForm();
				bdiComposicaoOrcamentoFormNeto.setComposicaoOrcamento(composicaoOrcamento);
				bdiComposicaoOrcamentoFormNeto.setNome(composicaoOrcamento.getNome());
				bdiComposicaoOrcamentoFormNeto.setValor(composicaoorcamentoService.calculaCustoTotalComposicao(composicaoOrcamento));
				
				bdiComposicaoOrcamentoMapa = mapaComposicaoOrcamento.get(composicaoOrcamento);
				
				//Se a composi��o j� existir no BDI, busca o valor do campo "inclui".
				if (bdiComposicaoOrcamentoMapa != null) {
					bdiComposicaoOrcamentoFormNeto.setMarcado(bdiComposicaoOrcamentoMapa.getInclui());
				}
				else {
					bdiComposicaoOrcamentoFormNeto.setMarcado(true);					
				}
				listaBdiComposicaoOrcamentoFormNeto.add(bdiComposicaoOrcamentoFormNeto);
			}
			bdiComposicaoOrcamentoFormFilho.setListaBdiComposicaoOrcamentoFormFilho(listaBdiComposicaoOrcamentoFormNeto);				
		}
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		//Insere os Recursos Humanos do BDI (MOD e MOI)
		listaBdiComposicaoOrcamentoFormNeto = new ArrayList<BdiComposicaoOrcamentoForm>();			
		
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("M�o-de-obra");
		bdiComposicaoOrcamentoFormFilho.setMarcado(incluiMod || incluiMoi);
		
		//MOD
		bdiComposicaoOrcamentoFormNeto = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormNeto.setMarcado(incluiMod);
		bdiComposicaoOrcamentoFormNeto.setNome("MOD");
		bdiComposicaoOrcamentoFormNeto.setValor(orcamentorecursohumanoService.calculaCustoTotalMOD(orcamento));			
		listaBdiComposicaoOrcamentoFormNeto.add(bdiComposicaoOrcamentoFormNeto);
		
		//MOI
		bdiComposicaoOrcamentoFormNeto = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormNeto.setMarcado(incluiMoi);
		bdiComposicaoOrcamentoFormNeto.setNome("MOI");
		bdiComposicaoOrcamentoFormNeto.setValor(orcamentorecursohumanoService.calculaCustoTotalMOI(orcamento));			
		listaBdiComposicaoOrcamentoFormNeto.add(bdiComposicaoOrcamentoFormNeto);
		
		bdiComposicaoOrcamentoFormFilho.setListaBdiComposicaoOrcamentoFormFilho(listaBdiComposicaoOrcamentoFormNeto);			
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("Itens de custo com MO");
		bdiComposicaoOrcamentoFormFilho.setMarcado(incluiitenscustomo);
		if(valoritenscustomo == null || valoritenscustomo == 0){
			valoritenscustomo = customaoobraitemService.getTotalcustovenda(orcamento);
		}
		bdiComposicaoOrcamentoFormFilho.setValor(valoritenscustomo);
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("Recursos Gerais Diretos");
		bdiComposicaoOrcamentoFormFilho.setMarcado(incluirecursogeraldireto);
		if(valorrecursogeraldireto == null || valorrecursogeraldireto == 0){
			valorrecursogeraldireto = tarefaorcamentoService.getTotalRecursosGeraisDireto(orcamento);
		}
		bdiComposicaoOrcamentoFormFilho.setValor(valorrecursogeraldireto);
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("Outras Tarefas");
		bdiComposicaoOrcamentoFormFilho.setMarcado(incluioutrastarefas);
		if(valoroutrastarefas == null || valoroutrastarefas == 0){
			valoroutrastarefas = tarefaorcamentoService.getTotalOutrasTarefas(orcamento);
		}
		bdiComposicaoOrcamentoFormFilho.setValor(valoroutrastarefas);
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		bdiComposicaoOrcamentoFormFilho = new BdiComposicaoOrcamentoForm();
		bdiComposicaoOrcamentoFormFilho.setNome("Acr�scimo em Tarefas");
		bdiComposicaoOrcamentoFormFilho.setMarcado(incluiacrescimo);
		if(valoracrescimo == null || valoracrescimo == 0){
			valoracrescimo = tarefaorcamentoService.getTotalAcrescimo(orcamento);
		}
		bdiComposicaoOrcamentoFormFilho.setValor(valoracrescimo);
		listaBdiComposicaoOrcamentoFormFilho.add(bdiComposicaoOrcamentoFormFilho);
		
		
		bdiComposicaoOrcamentoForm.setListaBdiComposicaoOrcamentoFormFilho(listaBdiComposicaoOrcamentoFormFilho);
		listaBdiComposicaoOrcamentoForm.add(bdiComposicaoOrcamentoForm);
		
		bdiForm.setListaBdiComposicaoOrcamentoForm(listaBdiComposicaoOrcamentoForm);
		
		//Monta a �rvore com os itens do BDI
		listaBdiItemForm = convertListBdiItemToListBdiItemForm(bdi != null ? bdi.getListaBdiitem() : null);
		montaArvoreBdiItem(listaBdiItemForm, listaBdiItemForm.get(0));
		bdiForm.setListaBdiItemForm(listaBdiItemForm);		
		
		//Monta a �rvore com os tributos do BDI
		listaBdiTributoForm = convertListBdiTributoToListBdiTributoForm(bdi != null ? bdi.getListaBditributo() : null);
		montaArvoreBdiTributo(listaBdiTributoForm, listaBdiTributoForm.get(0));
		bdiForm.setListaBdiTributoForm(listaBdiTributoForm);		
		
		return bdiForm;
	}
	
	public void montaArvoreBdiItem(List<BdiItemForm> listaBdiItemFormEmLinha, BdiItemForm bdiItemFormRaiz) {
		if (listaBdiItemFormEmLinha == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		BdiItemForm bdiItemFormFilho;
		
		//Monta os itens filhos
		for (int i = listaBdiItemFormEmLinha.size() - 1 ; i >= 0 ; i--) {
			bdiItemFormFilho = listaBdiItemFormEmLinha.get(i);
			//Se filha tem algum pai...
			if (bdiItemFormFilho.getBdiItemPai() != null) {
				//Procura quem � o pai
				for (BdiItemForm bdiItemFormPai : listaBdiItemFormEmLinha) {
					if (bdiItemFormPai.equals(bdiItemFormFilho.getBdiItemPai())) {
						if (bdiItemFormPai.getListaBdiItemFormFilho() == null) {
							bdiItemFormPai.setListaBdiItemFormFilho(new ArrayList<BdiItemForm>());
						}
						bdiItemFormPai.getListaBdiItemFormFilho().add(bdiItemFormFilho);
						bdiItemFormFilho.setBdiItemPai(bdiItemFormPai);
						break;
					}
				}
			}
			if (bdiItemFormFilho.equals(bdiItemFormRaiz)) {
				bdiItemFormFilho.setBdiItemPai(null);
			}
		}
		
		//Elimina os itens deixando apenas o item raiz
		for (int i = listaBdiItemFormEmLinha.size() - 1 ; i >= 0 ; i--) {
			bdiItemFormFilho = listaBdiItemFormEmLinha.get(i);
			if (bdiItemFormFilho.getBdiItemPai() != null) {
				listaBdiItemFormEmLinha.remove(i);
			}
		}
	}
	
	/***
	 * Converte uma lista de objetos do tipo BdiItem para uma lista de objetos do tipo BdiItemForm
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertBdiItemToBdiItemForm(Bdiitem, BdiItemForm)
	 * 
	 * @param listaBdiItem
	 * @param bdiItemPai
	 * @param listaBdiItem
	 * @return List<BdiItemForm>
	 * @throws SinedException - caso um dos par�metros bdi ou listaBdiItemForm seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<BdiItemForm> convertListBdiItemToListBdiItemForm(List<Bdiitem> listaBdiItem) {

		BdiItemForm bdiItemFormRaiz;
		BdiItemForm bdiItemFormFilho;
		BdiItemForm bdiItemFormPai;		
		List<BdiItemForm> listaBdiItemForm = new ArrayList<BdiItemForm>();
		
		bdiItemFormRaiz = new BdiItemForm();
		bdiItemFormRaiz.setCdBdiItem(-1);
		bdiItemFormRaiz.setNome("Bdi");
		bdiItemFormRaiz.setValor(0d);
		bdiItemFormRaiz.setBdiItemPai(null);
		listaBdiItemForm.add(bdiItemFormRaiz);

		//Converte os objetos
		if (listaBdiItem != null) {
			for (Bdiitem bdiItem : listaBdiItem) {
				if (bdiItem.getBdiitempai() == null) {
					bdiItemFormFilho = convertBdiItemToBdiItemForm(bdiItem, bdiItemFormRaiz);
				}
				else {
					//Converte o pai somente para ser utilizado no filho.
					//Por isso, n�o se preocupa com o pai do pai...
					bdiItemFormPai = convertBdiItemToBdiItemForm(bdiItem.getBdiitempai(), null);
					
					//Converte o filho
					bdiItemFormFilho = convertBdiItemToBdiItemForm(bdiItem, bdiItemFormPai);
				}
				listaBdiItemForm.add(bdiItemFormFilho);
			}
		}
		return listaBdiItemForm;
	}
	
	/***
	 * Converte um objeto do tipo BdiItemForm para um objeto do tipo BdiItem
	 * 
	 * @param bdiItem
	 * @param bdiItemFormPai
	 * @return BdiItemForm
	 * @throws SinedException - caso o par�metro bdiItem seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private BdiItemForm convertBdiItemToBdiItemForm(Bdiitem bdiItem, BdiItemForm bdiItemFormPai) {
		BdiItemForm bdiItemForm = new BdiItemForm();
		
		if (bdiItem == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		bdiItemForm.setCdBdiItem(bdiItem.getCdbdiitem());
		bdiItemForm.setNome(bdiItem.getNome());
		bdiItemForm.setValor(bdiItem.getValor());
		bdiItemForm.setContagerencial(bdiItem.getContagerencial());
		bdiItemForm.setBdiItemPai(bdiItemFormPai);
		
		return bdiItemForm;
	}	
	
	/***
	 * Atualiza os valores dos nodos pai da �rvore de composi��o do BDI
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#calculaValorBdiComposicao(BdiComposicaoOrcamentoForm)
	 * 
	 * @param listaBdiComposicaoOrcamentoForm
	 * @return List<BdiComposicaoOrcamentoForm>
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<BdiComposicaoOrcamentoForm> atualizaCustoArvoreBdiComposicao(List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoForm) {
		if (listaBdiComposicaoOrcamentoForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		//Busca o elemento inicial da lista (Or�amento)
		BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoForm = listaBdiComposicaoOrcamentoForm.get(0);
		bdiComposicaoOrcamentoForm.setValor(calculaValorBdiComposicao(bdiComposicaoOrcamentoForm));
		
		return listaBdiComposicaoOrcamentoForm;
	}
	
	/***
	 * M�todo recursivo que retorna o valor do custo de um nodo pai baseado no somat�rio dos custos dos nodos filhos
	 * 
	 * @param bdiComposicaoOrcamentoForm
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Double calculaValorBdiComposicao(BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoForm) {
		Double valor = 0d;
		Boolean statusFilhos = false;
	
		if (bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho() != null && !bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho().isEmpty()) {
		
			for (BdiComposicaoOrcamentoForm filho : bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho()) {
				valor += calculaValorBdiComposicao(filho);
				statusFilhos = statusFilhos || filho.getMarcado();
			}
			if (statusFilhos) {
				bdiComposicaoOrcamentoForm.setValor(valor);
				bdiComposicaoOrcamentoForm.setMarcado(true);
			}
			else {
				bdiComposicaoOrcamentoForm.setValor(0d);
				bdiComposicaoOrcamentoForm.setMarcado(false);
			}
		}
		else {
			if (bdiComposicaoOrcamentoForm.getMarcado() == true) {
				valor = bdiComposicaoOrcamentoForm.getValor() != null ? bdiComposicaoOrcamentoForm.getValor() : 0d;
			}
			else {
				valor = 0d;
			}	
		}
		return valor;		
	}
	
	/***
	 * Atualiza os valores dos nodos pai da �rvore de itens do BDI
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#calculaValorBdiItem(BdiItemForm)
	 * 
	 * @param listaBdiItemForm
	 * @return List<BdiComposicaoOrcamentoForm>
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<BdiItemForm> atualizaTaxaArvoreBdiItem(List<BdiItemForm> listaBdiItemForm) {
		if (listaBdiItemForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		//Busca o elemento inicial da lista (Taxas)
		BdiItemForm bdiItemForm = listaBdiItemForm.get(0);
		bdiItemForm.setValor(calculaValorBdiItem(bdiItemForm));
		
		return listaBdiItemForm;
	}
	
	/***
	 * M�todo recursivo que retorna o valor da taxa de um nodo pai baseado no somat�rio das taxas dos nodos filhos
	 * 
	 * @param bdiItemForm
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Double calculaValorBdiItem(BdiItemForm bdiItemForm) {
		Double valor = 0d;
		
		if (bdiItemForm.getListaBdiItemFormFilho() != null && !bdiItemForm.getListaBdiItemFormFilho().isEmpty()) {
			for (BdiItemForm filho : bdiItemForm.getListaBdiItemFormFilho()) {
				valor += calculaValorBdiItem(filho);			
			}
			bdiItemForm.setValor(valor);
		}
		else {
			valor = bdiItemForm.getValor();
		}
		return valor;		
	}	
	
	/***
	 * Calcula os valores referentes ao BDI
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#calculaValorBdiComposicao(BdiComposicaoOrcamentoForm)
	 * 
	 * @param bdiForm
	 * @return BdiForm
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public BdiForm calculaValoresBdi(BdiForm bdiForm) {
		Money custoTotal = new Money(0);
		Double totalTaxas = 0d;
		Double totalTributos = 0d;
		Money precoTotalComBdi = new Money(0);
		Double bdiGeralComTributos = 0d;
		Double folga = 0d;
		Money precoComFolga = new Money(0);
		Double bdiGeral = 0d;
		
		if (bdiForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		//Calcula o custo total
		if (bdiForm.getListaBdiComposicaoOrcamentoForm() != null && !bdiForm.getListaBdiComposicaoOrcamentoForm().isEmpty()) {
			BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoForm = bdiForm.getListaBdiComposicaoOrcamentoForm().get(0);
			if (bdiComposicaoOrcamentoForm.getValor() == null) {
				bdiComposicaoOrcamentoForm.setValor(calculaValorBdiComposicao(bdiComposicaoOrcamentoForm));
			}
			custoTotal = new Money(bdiComposicaoOrcamentoForm.getValor());
		}		
		
		//Calcula o total de taxas
		if (bdiForm.getListaBdiItemForm() != null) {
			for (BdiItemForm bdiItemForm : bdiForm.getListaBdiItemForm()) {
				totalTaxas += bdiItemForm.getValor();
			}
		}
		
		//Calcula o total de tributos
		if (bdiForm.getListaBdiTributoForm() != null) {
			for (BdiTributoForm bdiTributoForm : bdiForm.getListaBdiTributoForm()) {
				totalTributos += bdiTributoForm.getValor();
			}
		}
		
		Bdicalculotipo bdicalculotipo = Bdicalculotipo.PERCENTUAL_SIMPLES;
		if(bdiForm.getOrcamento().getBdicalculotipo() != null){
			bdicalculotipo = bdiForm.getOrcamento().getBdicalculotipo();
		}
		
		//se tiver marcado calcularhistograma o calculo � diferente, sen�o o calculo � simples
		if(bdicalculotipo.equals(Bdicalculotipo.PERCENTUAL_INVERTIDO)){
			//Calcula o pre�o total com taxas		
			precoTotalComBdi = new Money(custoTotal.getValue().doubleValue() / (1 - (totalTaxas / 100)));
			
			//Calcula o BDI Geral com tributos
			if(totalTributos != null && totalTributos > 0){
				bdiGeralComTributos = (precoTotalComBdi.getValue().doubleValue() / (1 - (totalTributos / 100)));
			}else {
				bdiGeralComTributos = ((precoTotalComBdi.getValue().doubleValue() / custoTotal.getValue().doubleValue()) - 1) * 100;
			}
	
			//Calcula o pre�o com folga		
			folga = bdiForm.getFolga() != null ? bdiForm.getFolga() : 0d;
			precoComFolga = new Money(precoTotalComBdi.getValue().doubleValue() / (1 - (folga / 100)));
			
			//Calcula o BDI Geral
			bdiGeral = ((precoComFolga.getValue().doubleValue() / custoTotal.getValue().doubleValue()) - 1) * 100;
		} else {
			//Calcula o pre�o total com taxas		
			precoTotalComBdi = new Money(custoTotal.getValue().doubleValue() * (1 + (totalTaxas / 100)));
			
			//Calcula o pre�o com folga		
			folga = bdiForm.getFolga() != null ? bdiForm.getFolga() : 0d;
			precoComFolga = new Money(precoTotalComBdi.getValue().doubleValue() * (1 + (folga / 100)));			
		}
		
		if (custoTotal.getValue().doubleValue() == 0) {
			bdiGeral = 0d;
		}
		
		bdiForm.setPrecoCusto(custoTotal);
		bdiForm.setTotalTaxas(totalTaxas);
		bdiForm.setTotalTributos(totalTributos);
		bdiForm.setPrecoTotalComTaxas(precoTotalComBdi);
		bdiForm.setBdiGeralComImpostos(bdiGeralComTributos);
		bdiForm.setFolga(folga);
		bdiForm.setPrecoComFolga(precoComFolga);
		bdiForm.setBdiGeral(bdiGeral);
		
		return bdiForm;
	}
	
	/***
	 * Converte um objeto do tipo BdiForm para um objeto do tipo Bdi e salva no banco de dados
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertBdiFormToBdi(BdiForm)
	 * @see br.com.linkcom.sined.geral.service.BdiService#saveOrUpdate(Bdi)
	 * 
	 * @param bdiForm
	 * @return
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveBdiFlex(BdiForm bdiForm) {
		Bdi bdi = convertBdiFormToBdi(bdiForm);
		saveOrUpdate(bdi);
	}
	
	/***
	 * Converte um objeto do tipo BdiForm para um objeto do tipo Bdi
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertListBdiItemFormToListBdiItem(Bdi, List, Bdiitem, List)
	 * 
	 * @param bdiForm
	 * @return Bdi
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Bdi convertBdiFormToBdi(BdiForm bdiForm) {
		Bdicomposicaoorcamento bdiComposicaoOrcamento;
		List<Bdicomposicaoorcamento> listaBdiComposicaoOrcamento = new ArrayList<Bdicomposicaoorcamento>();
		List<Bdiitem> listaBdiItem = new ArrayList<Bdiitem>();
		List<Bditributo> listaBditributo = new ArrayList<Bditributo>();
		
		Bdi bdi = new Bdi();
		
		bdi.setCdbdi(bdiForm.getCdBdi() != null && bdiForm.getCdBdi().intValue() != 0 ? bdiForm.getCdBdi() : null);
		bdi.setFolga(bdiForm.getFolga());
		bdi.setOrcamento(bdiForm.getOrcamento());
		
		//Itens do BDI
		listaBdiItem = convertListBdiItemFormToListBdiItem(bdi, bdiForm.getListaBdiItemForm().get(0).getListaBdiItemFormFilho(), null, null);
		bdi.setListaBdiitem(listaBdiItem);
		
		//Tributos do BDI
		listaBditributo = convertListBdiTributoFormToListBdiTributo(bdi, bdiForm.getListaBdiTributoForm().get(0).getListaBdiTributoFormFilho(), null, null);
		bdi.setListaBditributo(listaBditributo);
		
		//Composi��o do BDI
		List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoFormFilho = bdiForm.getListaBdiComposicaoOrcamentoForm().get(0).getListaBdiComposicaoOrcamentoFormFilho();
		
		for (BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoFormFilho : listaBdiComposicaoOrcamentoFormFilho) {
			if (bdiComposicaoOrcamentoFormFilho.getNome().equals("Composi��o")) {
				for (BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoFormNeto : bdiComposicaoOrcamentoFormFilho.getListaBdiComposicaoOrcamentoFormFilho()) {
					bdiComposicaoOrcamento = new Bdicomposicaoorcamento();
					bdiComposicaoOrcamento.setCdbdicomposicaoorcamento(bdiComposicaoOrcamentoFormNeto.getCdBdiComposicaoOrcamento() != null && bdiComposicaoOrcamentoFormNeto.getCdBdiComposicaoOrcamento().intValue() != 0 ? bdiComposicaoOrcamentoFormNeto.getCdBdiComposicaoOrcamento() : null);
					bdiComposicaoOrcamento.setBdi(bdi);
					bdiComposicaoOrcamento.setComposicaoorcamento(bdiComposicaoOrcamentoFormNeto.getComposicaoOrcamento());
					bdiComposicaoOrcamento.setInclui(bdiComposicaoOrcamentoFormNeto.getMarcado());
					listaBdiComposicaoOrcamento.add(bdiComposicaoOrcamento);
				}				
			}			
			else if (bdiComposicaoOrcamentoFormFilho.getNome().equals("M�o-de-obra")) {
				for (BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoFormNeto : bdiComposicaoOrcamentoFormFilho.getListaBdiComposicaoOrcamentoFormFilho()) {
					if (bdiComposicaoOrcamentoFormNeto.getNome().equals("MOD")) {
						bdi.setIncluimod(bdiComposicaoOrcamentoFormNeto.getMarcado());
					}
					else if (bdiComposicaoOrcamentoFormNeto.getNome().equals("MOI")) {
						bdi.setIncluimoi(bdiComposicaoOrcamentoFormNeto.getMarcado());
					}
				}
			}
			else if (bdiComposicaoOrcamentoFormFilho.getNome().equals("Recursos Gerais Diretos")) {
				bdi.setValorrecursogeraldireto(bdiComposicaoOrcamentoFormFilho.getValor());
				bdi.setIncluirecursogeraldireto(bdiComposicaoOrcamentoFormFilho.getMarcado());
			}
			else if (bdiComposicaoOrcamentoFormFilho.getNome().equals("Outras Tarefas")) {
				bdi.setValoroutrastarefas(bdiComposicaoOrcamentoFormFilho.getValor());
				bdi.setIncluioutrastarefas(bdiComposicaoOrcamentoFormFilho.getMarcado());
			}
			else if (bdiComposicaoOrcamentoFormFilho.getNome().equals("Acr�scimo em Tarefas")) {
				bdi.setValoracrescimo(bdiComposicaoOrcamentoFormFilho.getValor());
				bdi.setIncluiacrescimo(bdiComposicaoOrcamentoFormFilho.getMarcado());
			}
			else if (bdiComposicaoOrcamentoFormFilho.getNome().equals("Itens de custo com MO")) {
				bdi.setValoritenscustomo(bdiComposicaoOrcamentoFormFilho.getValor());
				bdi.setIncluiitenscustomo(bdiComposicaoOrcamentoFormFilho.getMarcado());
			}
		}
		bdi.setListaBdicomposicaoorcamento(listaBdiComposicaoOrcamento);

		return bdi;
	}
	
	/***
	 * Converte uma lista de objetos do tipo BdiItemForm para uma lista de objetos do tipo BdiItem
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#convertBdiItemFormToBdiItem(BdiItemForm, Bdi, Bdiitem)
	 * 
	 * @param listaBdiItemForm
	 * @param bdiItemPai
	 * @param listaBdiItem
	 * @return List<Bdiitem>
	 * @throws SinedException - caso um dos par�metros bdi ou listaBdiItemForm seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Bdiitem> convertListBdiItemFormToListBdiItem(Bdi bdi, List<BdiItemForm> listaBdiItemForm, Bdiitem bdiItemPai, List<Bdiitem> listaBdiItem) {
		if (bdi == null || listaBdiItemForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		if (listaBdiItem == null) {
			listaBdiItem = new ArrayList<Bdiitem>();
		}
		
		Bdiitem bdiItem;
		
		for (BdiItemForm bdiItemForm : listaBdiItemForm) {
			bdiItem = convertBdiItemFormToBdiItem(bdi, bdiItemForm, bdiItemPai);
			listaBdiItem.add(bdiItem);
			
			if (bdiItemForm.getListaBdiItemFormFilho() != null && !bdiItemForm.getListaBdiItemFormFilho().isEmpty()) {
				listaBdiItem = convertListBdiItemFormToListBdiItem(bdi, bdiItemForm.getListaBdiItemFormFilho(), bdiItem, listaBdiItem);
			}			
		}
		
		return listaBdiItem;
	}
	
	/***
	 * Converte um objeto do tipo BdiItemForm para um objeto do tipo BdiItem
	 * 
	 * @param bdiItemForm
	 * @param bdi
	 * @param bdiItemPai
	 * @return Bdiitem
	 * @throws SinedException - caso um dos par�metros bdiItemForm ou bdi seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	private Bdiitem convertBdiItemFormToBdiItem(Bdi bdi, BdiItemForm bdiItemForm, Bdiitem bdiItemPai) {
		Bdiitem bdiItem = new Bdiitem();
		
		if (bdiItemForm == null || bdi == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		bdiItem.setCdbdiitem(bdiItemForm.getCdBdiItem().intValue() != 0 ? bdiItemForm.getCdBdiItem() : null);
		bdiItem.setBdi(bdi);
		bdiItem.setNome(bdiItemForm.getNome());
		bdiItem.setBdiitempai(bdiItemPai);
		bdiItem.setValor(bdiItemForm.getValor());
		bdiItem.setContagerencial(bdiItemForm.getContagerencial());
		
		return bdiItem;
	}
	
	/**
	 * Gera o relat�rio do BDI em PDF
	 *
	 * @see br.com.linkcom.sined.geral.service.BdiService#getByOrcamentoFlex(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.BdiService#getListaBdiItemReportBean(List, List, int)
	 * 
	 * @param orcamento
	 * @return IReport
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public IReport createReportBdi(Orcamento orcamento) {
		DecimalFormat formatadorDecimal = new DecimalFormat("#,##0.00");
		formatadorDecimal.setRoundingMode(RoundingMode.HALF_UP);
		
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		orcamento = orcamentoService.loadForEntrada(orcamento);
		List<BdiReportBean> listaBdiReportBean = new ArrayList<BdiReportBean>();
		BdiForm bdiForm = getByOrcamentoFlex(orcamento);
		
		listaBdiReportBean = getListaBdiItemReportBean(bdiForm.getListaBdiItemForm(), listaBdiReportBean, 1);
		
		Report report = new Report("/projeto/bdi");
		report.addParameter("CUSTOTOTAL", "R$" + formatadorDecimal.format(bdiForm.getPrecoCusto().getValue().doubleValue()));
		report.addParameter("PRECOTOTALCOMTAXAS", "R$" + formatadorDecimal.format(bdiForm.getPrecoTotalComTaxas().getValue().doubleValue()));
		report.addParameter("BDIGERALCOMIMPOSTOS", formatadorDecimal.format(bdiForm.getBdiGeralComImpostos()) + "%");
		report.addParameter("FOLGA", formatadorDecimal.format(bdiForm.getFolga()) + "%");
		report.addParameter("PRECOCOMFOLGA", "R$" + formatadorDecimal.format(bdiForm.getPrecoComFolga().getValue().doubleValue()));
		report.addParameter("BDIGERAL", formatadorDecimal.format(bdiForm.getBdiGeral()) + "%");
		report.setDataSource(listaBdiReportBean);
		return report;
	}
	
	/**
	* M�todo que gerar o CSV do BDI
	*
	* @param orcamento
	* @return
	* @since 05/08/2014
	* @author Luiz Fernando
	*/
	public Resource createReportResumoGeralExcelOrcamento(Orcamento orcamento) throws IOException {
		DecimalFormat formatadorDecimal = new DecimalFormat("#,##0.00");
		formatadorDecimal.setRoundingMode(RoundingMode.HALF_UP);
		
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		orcamento = orcamentoService.loadForEntrada(orcamento);	
		
		Double totalHorasMDO = orcamentorecursohumanoService.calculaTotalHorasMDO(orcamento);
		
		//Beans
		BdiForm bdiForm;

		//Listas
		List<BdiReportBean> listaBdiReportBean = new ArrayList<BdiReportBean>();
		
		bdiForm = getByOrcamentoFlex(orcamento);
		
		listaBdiReportBean = getListaBdiComposicaoReportBean(bdiForm.getListaBdiComposicaoOrcamentoForm(), listaBdiReportBean, 1, "", totalHorasMDO);
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 10);
        planilha.setColumnWidth((short) 0, ((short)(35*200)));
        planilha.setColumnWidth((short) 1, ((short)(35*300)));
        planilha.setColumnWidth((short) 2, ((short)(35*200)));
        planilha.setColumnWidth((short) 3, ((short)(35*200)));
        
        HSSFRow headerRow = planilha.createRow((short) 0);
        headerRow.setHeight((short) 1000);
		planilha.addMergedRegion(new Region(0, (short) 0, 0, (short) 3));
		
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Resumo Geral do Or�amento");
		
		HSSFRow row = null;
		HSSFCell cell = null;
		short border =  (short) 1;
		
		//Estilo do detalhe
		HSSFCellStyle detalheStyle = wb.createCellStyle();
		detalheStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		detalheStyle.setBorderTop(border);
		detalheStyle.setBorderBottom(border);
		detalheStyle.setBorderLeft(border);
		detalheStyle.setBorderRight(border);
		detalheStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		detalheStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		detalheStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		detalheStyle.setWrapText(true);
		
		HSSFFont font_detalhe_bold = wb.createFont();
		font_detalhe_bold.setFontHeightInPoints((short) 10);
		font_detalhe_bold.setFontName("Arial");
		font_detalhe_bold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		
		HSSFCellStyle detalheStyleNegrito = wb.createCellStyle();
		detalheStyleNegrito.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		detalheStyleNegrito.setBorderTop(border);
		detalheStyleNegrito.setBorderBottom(border);
		detalheStyleNegrito.setBorderLeft(border);
		detalheStyleNegrito.setBorderRight(border);
		detalheStyleNegrito.setFillForegroundColor(HSSFColor.WHITE.index);
		detalheStyleNegrito.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		detalheStyleNegrito.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		detalheStyleNegrito.setWrapText(true);
		detalheStyleNegrito.setFont(font_detalhe_bold);
		
		//--------------
		
		row = planilha.createRow(planilha.getLastRowNum()+1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(detalheStyleNegrito);
		cell.setCellValue("Cliente");
		cell = row.createCell((short) 1);
		cell.setCellStyle(detalheStyle);
		cell.setCellValue(orcamento.getCliente() != null  ? orcamento.getCliente().getNome() : "");
		
		row = planilha.createRow(planilha.getLastRowNum()+1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(detalheStyleNegrito);
		cell.setCellValue("Descri��o");
		cell = row.createCell((short) 1);
		cell.setCellStyle(detalheStyle);
		cell.setCellValue(orcamento.getNome());
		
		row = planilha.createRow(planilha.getLastRowNum()+1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(detalheStyleNegrito);
		cell.setCellValue("Local");
		cell = row.createCell((short) 1);
		cell.setCellStyle(detalheStyle);
		cell.setCellValue(orcamento.getMunicipio() != null ? orcamento.getMunicipio().getNome() + " / " + orcamento.getMunicipio().getUf().getSigla() : "");
		
		row = planilha.createRow(planilha.getLastRowNum()+1);
		cell = row.createCell((short) 0);
		cell.setCellStyle(detalheStyleNegrito);
		cell.setCellValue("Data");
		cell = row.createCell((short) 1);
		cell.setCellStyle(detalheStyle);
		cell.setCellValue(SinedDateUtils.toString(orcamento.getData()));
		
		row = planilha.createRow(planilha.getLastRowNum()+2);
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Item");
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Descri��o");
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Custo Total");
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Custo / H");
		
		if(SinedUtil.isListNotEmpty(listaBdiReportBean)){
			for(BdiReportBean bdiReportBean : listaBdiReportBean){
				row = planilha.createRow(planilha.getLastRowNum()+1);
				
				cell = row.createCell((short) 0);
				cell.setCellValue(bdiReportBean.getNumero());
				setStyleDetalheResumoCSV(cell, detalheStyle, detalheStyleNegrito, bdiReportBean.getNegrito());
				
				cell = row.createCell((short) 1);
				cell.setCellValue(bdiReportBean.getItem());
				setStyleDetalheResumoCSV(cell, detalheStyle, detalheStyleNegrito, bdiReportBean.getNegrito());
				
				cell = row.createCell((short) 2);
				cell.setCellValue(bdiReportBean.getValor());
				setStyleDetalheResumoCSV(cell, detalheStyle, detalheStyleNegrito, bdiReportBean.getNegrito());
				
				cell = row.createCell((short) 3);
				cell.setCellValue(bdiReportBean.getValorHora());
				setStyleDetalheResumoCSV(cell, detalheStyle, detalheStyleNegrito, bdiReportBean.getNegrito());
			}
		}
		
		return wb.getWorkBookResource("resumoorcamento_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	private void setStyleDetalheResumoCSV(HSSFCell cell, HSSFCellStyle detalheStyle, HSSFCellStyle detalheStyleNegrito, Boolean negrito) {
		if(negrito != null && negrito){
			cell.setCellStyle(detalheStyleNegrito);
		}else {
			cell.setCellStyle(detalheStyle);
		}
	}
	/**
	 * M�todo recursivo que transforma uma hierarquia de objetos em uma lista plana com o a hierarquia representada pelo deslocamento dos nomes
	 * Ex: Item 1 -> Item 2 -> Item 3
	 * Resultado: 
	 * Item 1
	 *   Item 2
	 *     Item 3 
	 *
	 * @param listaBdiItemForm
	 * @param listaBdiReportBean
	 * @param nivel
	 * @return List<BdiItemReportBean>
	 * @throws SinedException - caso o par�metro listaBdiReportBean seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<BdiReportBean> getListaBdiItemReportBean(List<BdiItemForm> listaBdiItemForm, List<BdiReportBean> listaBdiReportBean, int nivel) {
		if (listaBdiReportBean == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		DecimalFormat formatadorDecimal = new DecimalFormat("##0.00");		
		BdiReportBean bdiReportBean;
		String offSet;
		
		if (listaBdiItemForm != null) {
			for (BdiItemForm bdiItemForm : listaBdiItemForm) {
				bdiReportBean = new BdiReportBean();
				offSet = "";
				for (int i = 1; i < nivel; i++) {
					offSet += StringUtils.stringCheia(""," ",3 * i, false);
				}
				bdiReportBean.setItem(offSet + bdiItemForm.getNome());
				bdiReportBean.setValor(formatadorDecimal.format(bdiItemForm.getValor()) + "%");
				
				if (bdiItemForm.getListaBdiItemFormFilho() != null && !bdiItemForm.getListaBdiItemFormFilho().isEmpty()) {
					bdiReportBean.setNegrito(true);
					listaBdiReportBean.add(bdiReportBean);
					listaBdiReportBean = getListaBdiItemReportBean(bdiItemForm.getListaBdiItemFormFilho(), listaBdiReportBean, nivel + 1);
				}
				else {
					bdiReportBean.setNegrito(false);
					listaBdiReportBean.add(bdiReportBean);
				}
			}
		}
		return listaBdiReportBean;
	}
	
	/**
	 * Gera o relat�rio do Resumo Geral do Or�amento em PDF
	 *
	 * @see br.com.linkcom.sined.geral.service.BdiService#getByOrcamentoFlex(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.OrcamentoService#loadForEntrada(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService#calculaTotalHorasMDO(Orcamento)
	 * @see br.com.linkcom.sined.geral.service.BdiService#getListaBdiComposicaoReportBean(List, List, int)
	 * 
	 * @param orcamento
	 * @return IReport
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public IReport createReportResumoGeralOrcamento(Orcamento orcamento) {

		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		orcamento = orcamentoService.loadForEntrada(orcamento);	
		
		Double totalHorasMDO = orcamentorecursohumanoService.calculaTotalHorasMDO(orcamento);
		
		//Beans
		BdiForm bdiForm;

		//Listas
		List<BdiReportBean> listaBdiReportBean = new ArrayList<BdiReportBean>();
		
		bdiForm = getByOrcamentoFlex(orcamento);
		
		listaBdiReportBean = getListaBdiComposicaoReportBean(bdiForm.getListaBdiComposicaoOrcamentoForm(), listaBdiReportBean, 1, "", totalHorasMDO);
		
		Report report = new Report("/projeto/resumoorcamento");
		report.addParameter("NOMECLIENTE", orcamento.getCliente() != null  ? orcamento.getCliente().getNome() : "");
		report.addParameter("NOMEORCAMENTO", orcamento.getNome());
		report.addParameter("LOCALORCAMENTO", orcamento.getMunicipio() != null ? orcamento.getMunicipio().getNome() + " / " + orcamento.getMunicipio().getUf().getSigla() : "");
		report.addParameter("DATAORCAMENTO", SinedDateUtils.toString(orcamento.getData()));
		report.setDataSource(listaBdiReportBean);
		return report;
	}
	
	/**
	 * M�todo recursivo que transforma uma hierarquia de objetos em uma lista plana com o a hierarquia representada pelo deslocamento dos nomes
	 * Ex: Item 1 -> Item 2 -> Item 3
	 * Resultado: 
	 * Item 1
	 *   Item 2
	 *     Item 3 
	 *
	 * @param listaBdiComposicaoOrcamentoForm
	 * @param listaBdiReportBean
	 * @param nivel
	 * @param numItemPrefix
	 * @param totalHorasMDO
	 * @return List<BdiItemReportBean>
	 * @throws SinedException - caso o par�metro listaBdiReportBean seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<BdiReportBean> getListaBdiComposicaoReportBean(List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoForm, List<BdiReportBean> listaBdiReportBean, int nivel, String numItemPrefix, Double totalHorasMDO) {
		if (listaBdiReportBean == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		DecimalFormat formatadorDecimal = new DecimalFormat("#,##0.00");
		formatadorDecimal.setRoundingMode(RoundingMode.HALF_UP);
		
		BdiReportBean bdiReportBean;
		String offSet;
		int numeroItem = 1;
		
		if (listaBdiComposicaoOrcamentoForm != null) {
			for (BdiComposicaoOrcamentoForm bdiComposicaoOrcamentoForm : listaBdiComposicaoOrcamentoForm) {
				if (bdiComposicaoOrcamentoForm.getMarcado()) {
					bdiReportBean = new BdiReportBean();
					offSet = "";
					for (int i = 1; i < nivel; i++) {
						offSet += StringUtils.stringCheia(""," ",3 * i, false);
					}
					if(bdiComposicaoOrcamentoForm.getValor() == null) bdiComposicaoOrcamentoForm.setValor(0d);
					bdiReportBean.setNumero(numItemPrefix + String.valueOf(numeroItem));					
					bdiReportBean.setItem(offSet + bdiComposicaoOrcamentoForm.getNome());
					bdiReportBean.setValor("R$" + formatadorDecimal.format(bdiComposicaoOrcamentoForm.getValor()));
					bdiReportBean.setValorHora("R$" + formatadorDecimal.format((totalHorasMDO != 0 ? bdiComposicaoOrcamentoForm.getValor() / totalHorasMDO : 0)));
					
					if (bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho() != null && !bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho().isEmpty()) {
						bdiReportBean.setNegrito(true);
						listaBdiReportBean.add(bdiReportBean);
						listaBdiReportBean = getListaBdiComposicaoReportBean(bdiComposicaoOrcamentoForm.getListaBdiComposicaoOrcamentoFormFilho(), listaBdiReportBean, nivel + 1, bdiReportBean.getNumero() + ".", totalHorasMDO);
					}
					else {
						bdiReportBean.setNegrito(false);
						listaBdiReportBean.add(bdiReportBean);
					}
					numeroItem++;
				}
			}
		}
		return listaBdiReportBean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BdiDAO#findByOrcamento(Orcamento orcamento)
	 *
	 * @param orcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Bdi findByOrcamento(Orcamento orcamento) {
		return bdiDAO.findByOrcamento(orcamento);
	}
	
	/**
	* M�todo convert a lista de bean de bdiTributo em BdiTributoForm
	*
	* @param listaBdiTributo
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public List<BdiTributoForm> convertListBdiTributoToListBdiTributoForm(List<Bditributo> listaBdiTributo) {

		BdiTributoForm bdiTributoFormRaiz;
		BdiTributoForm bdiTributoFormFilho;
		BdiTributoForm bdiTributoFormPai;		
		List<BdiTributoForm> listaBdiTributoForm = new ArrayList<BdiTributoForm>();
		
		bdiTributoFormRaiz = new BdiTributoForm();
		bdiTributoFormRaiz.setCdBdiTributo(-1);
		bdiTributoFormRaiz.setNome("Tributo");
		bdiTributoFormRaiz.setValor(0d);
		bdiTributoFormRaiz.setBdiTributoPai(null);
		listaBdiTributoForm.add(bdiTributoFormRaiz);

		//Converte os objetos
		if (listaBdiTributo != null) {
			for (Bditributo bdiTributo : listaBdiTributo) {
				if (bdiTributo.getBditributopai() == null) {
					bdiTributoFormFilho = convertBdiTributoToBdiTributoForm(bdiTributo, bdiTributoFormRaiz);
				}
				else {
					//Converte o pai somente para ser utilizado no filho.
					//Por isso, n�o se preocupa com o pai do pai...
					bdiTributoFormPai = convertBdiTributoToBdiTributoForm(bdiTributo.getBditributopai(), null);
					
					//Converte o filho
					bdiTributoFormFilho = convertBdiTributoToBdiTributoForm(bdiTributo, bdiTributoFormPai);
				}
				listaBdiTributoForm.add(bdiTributoFormFilho);
			}
		}
		return listaBdiTributoForm;
	}
	
	/**
	* M�todo convert o bean de bean de bdiTributo em BdiTributoForm
	*
	* @param bdiTributo
	* @param bdiTributoFormPai
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	private BdiTributoForm convertBdiTributoToBdiTributoForm(Bditributo bdiTributo, BdiTributoForm bdiTributoFormPai) {
		BdiTributoForm bdiTributoForm = new BdiTributoForm();
		
		if (bdiTributo == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		bdiTributoForm.setCdBdiTributo(bdiTributo.getCdbditributo());
		bdiTributoForm.setNome(bdiTributo.getNome());
		bdiTributoForm.setValor(bdiTributo.getValor());
		bdiTributoForm.setContagerencial(bdiTributo.getContagerencial());
		bdiTributoForm.setBdiTributoPai(bdiTributoFormPai);
		
		return bdiTributoForm;
	}
	
	/**
	* M�todo que converte o bean de bdiTributoForm em bdiTributo
	*
	* @param bdi
	* @param bdiTributoForm
	* @param bdiTributoPai
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	private Bditributo convertBdiTributoFormToBdiTributo(Bdi bdi, BdiTributoForm bdiTributoForm, Bditributo bdiTributoPai) {
		Bditributo bdiTributo = new Bditributo();
		
		if (bdiTributoForm == null || bdi == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		bdiTributo.setCdbditributo(bdiTributoForm.getCdBdiTributo().intValue() != 0 ? bdiTributoForm.getCdBdiTributo() : null);
		bdiTributo.setBdi(bdi);
		bdiTributo.setNome(bdiTributoForm.getNome());
		bdiTributo.setBditributopai(bdiTributoPai);
		bdiTributo.setValor(bdiTributoForm.getValor());
		bdiTributo.setContagerencial(bdiTributoForm.getContagerencial());
		
		return bdiTributo;
	}
	
	/**
	* M�todo que monta a arvore de bdiTributo
	*
	* @param listaBdiTributoFormEmLinha
	* @param bdiTributoFormRaiz
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public void montaArvoreBdiTributo(List<BdiTributoForm> listaBdiTributoFormEmLinha, BdiTributoForm bdiTributoFormRaiz) {
		if (listaBdiTributoFormEmLinha == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		BdiTributoForm bdiTributoFormFilho;
		
		//Monta os itens filhos
		for (int i = listaBdiTributoFormEmLinha.size() - 1 ; i >= 0 ; i--) {
			bdiTributoFormFilho = listaBdiTributoFormEmLinha.get(i);
			//Se filha tem algum pai...
			if (bdiTributoFormFilho.getBdiTributoPai() != null) {
				//Procura quem � o pai
				for (BdiTributoForm bdiTributoFormPai : listaBdiTributoFormEmLinha) {
					if (bdiTributoFormPai.equals(bdiTributoFormFilho.getBdiTributoPai())) {
						if (bdiTributoFormPai.getListaBdiTributoFormFilho() == null) {
							bdiTributoFormPai.setListaBdiTributoFormFilho(new ArrayList<BdiTributoForm>());
						}
						bdiTributoFormPai.getListaBdiTributoFormFilho().add(bdiTributoFormFilho);
						bdiTributoFormFilho.setBdiTributoPai(bdiTributoFormPai);
						break;
					}
				}
			}
			if (bdiTributoFormFilho.equals(bdiTributoFormRaiz)) {
				bdiTributoFormFilho.setBdiTributoPai(null);
			}
		}
		
		//Elimina os itens deixando apenas o Tributo raiz
		for (int i = listaBdiTributoFormEmLinha.size() - 1 ; i >= 0 ; i--) {
			bdiTributoFormFilho = listaBdiTributoFormEmLinha.get(i);
			if (bdiTributoFormFilho.getBdiTributoPai() != null) {
				listaBdiTributoFormEmLinha.remove(i);
			}
		}
	}
	
	/**
	* M�todo que converte a lista de bdiTributoForm em bdiTributo
	*
	* @param bdi
	* @param listaBdiTributoForm
	* @param bdiTributoPai
	* @param listaBdiTributo
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public List<Bditributo> convertListBdiTributoFormToListBdiTributo(Bdi bdi, List<BdiTributoForm> listaBdiTributoForm, Bditributo bdiTributoPai, List<Bditributo> listaBdiTributo) {
		if (bdi == null || listaBdiTributoForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		if (listaBdiTributo == null) {
			listaBdiTributo = new ArrayList<Bditributo>();
		}
		
		Bditributo bdiTributo;
		
		for (BdiTributoForm bdiTributoForm : listaBdiTributoForm) {
			bdiTributo = convertBdiTributoFormToBdiTributo(bdi, bdiTributoForm, bdiTributoPai);
			listaBdiTributo.add(bdiTributo);
			
			if (bdiTributoForm.getListaBdiTributoFormFilho() != null && !bdiTributoForm.getListaBdiTributoFormFilho().isEmpty()) {
				listaBdiTributo = convertListBdiTributoFormToListBdiTributo(bdi, bdiTributoForm.getListaBdiTributoFormFilho(), bdiTributo, listaBdiTributo);
			}			
		}
		
		return listaBdiTributo;
	}
	
	/**
	* M�todo que calcula o valor dos tributos
	*
	* @param bdiTributoForm
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	private Double calculaValorBdiTributo(BdiTributoForm bdiTributoForm) {
		Double valor = 0d;
		
		if (bdiTributoForm.getListaBdiTributoFormFilho() != null && !bdiTributoForm.getListaBdiTributoFormFilho().isEmpty()) {
			for (BdiTributoForm filho : bdiTributoForm.getListaBdiTributoFormFilho()) {
				valor += calculaValorBdiTributo(filho);			
			}
			bdiTributoForm.setValor(valor);
		}
		else {
			valor = bdiTributoForm.getValor();
		}
		return valor;		
	}
	
	/**
	* M�todo que atualiza as taxas da arvore de tributos
	*
	* @param listaBdiTributoForm
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public List<BdiTributoForm> atualizaTaxaArvoreBdiTributo(List<BdiTributoForm> listaBdiTributoForm) {
		if (listaBdiTributoForm == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		//Busca o elemento inicial da lista (Tributos)
		BdiTributoForm bdiTributoForm = listaBdiTributoForm.get(0);
		bdiTributoForm.setValor(calculaValorBdiTributo(bdiTributoForm));
		
		return listaBdiTributoForm;
	}
	
	/**
	* M�todo que retorna a lista de Bean de tributo para o relat�rio
	*
	* @param listaBdiTributoForm
	* @param listaBdiReportBean
	* @param nivel
	* @return
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public List<BdiReportBean> getListaBdiTributoReportBean(List<BdiTributoForm> listaBdiTributoForm, List<BdiReportBean> listaBdiReportBean, int nivel) {
		if (listaBdiReportBean == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		
		DecimalFormat formatadorDecimal = new DecimalFormat("##0.00");		
		BdiReportBean bdiReportBean;
		String offSet;
		
		if (listaBdiTributoForm != null) {
			for (BdiTributoForm bdiTributoForm : listaBdiTributoForm) {
				bdiReportBean = new BdiReportBean();
				offSet = "";
				for (int i = 1; i < nivel; i++) {
					offSet += StringUtils.stringCheia(""," ",3 * i, false);
				}
				bdiReportBean.setItem(offSet + bdiTributoForm.getNome());
				bdiReportBean.setValor(formatadorDecimal.format(bdiTributoForm.getValor()) + "%");
				
				if (bdiTributoForm.getListaBdiTributoFormFilho() != null && !bdiTributoForm.getListaBdiTributoFormFilho().isEmpty()) {
					bdiReportBean.setNegrito(true);
					listaBdiReportBean.add(bdiReportBean);
					listaBdiReportBean = getListaBdiTributoReportBean(bdiTributoForm.getListaBdiTributoFormFilho(), listaBdiReportBean, nivel + 1);
				}
				else {
					bdiReportBean.setNegrito(false);
					listaBdiReportBean.add(bdiReportBean);
				}
			}
		}
		return listaBdiReportBean;
	}
}
