package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentonotacontrato;
import br.com.linkcom.sined.geral.dao.VwdocumentonotacontratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwdocumentonotacontratoService extends GenericService<Vwdocumentonotacontrato>{
	
	private VwdocumentonotacontratoDAO vwdocumentonotacontratoDAO;
	
	public void setVwdocumentonotacontratoDAO(VwdocumentonotacontratoDAO vwdocumentonotacontratoDAO) {
		this.vwdocumentonotacontratoDAO = vwdocumentonotacontratoDAO;
	}
	
	public List<Vwdocumentonotacontrato> listaDocumentonotacontrato (Contrato contrato){
		return vwdocumentonotacontratoDAO.listaDocumentonotacontrato(contrato);
	}

	
}
