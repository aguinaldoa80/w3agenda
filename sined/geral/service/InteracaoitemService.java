package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.dao.InteracaoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InteracaoitemService extends GenericService<Interacaoitem>{
	
	private InteracaoitemDAO interacaoitemDAO;
	
	public void setInteracaoitemDAO(InteracaoitemDAO interacaoitemDAO) {
		this.interacaoitemDAO = interacaoitemDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InteracaoitemDAO#findByInteracao
	 * @param interacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Interacaoitem> findByInteracao(Interacao interacao){
		return interacaoitemDAO.findByInteracao(interacao);
	}
	
}
