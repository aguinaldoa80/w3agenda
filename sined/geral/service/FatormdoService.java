package br.com.linkcom.sined.geral.service;

import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.sined.geral.bean.Fatormdo;
import br.com.linkcom.sined.geral.bean.Fatormdoitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.dao.FatormdoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.FatorMdoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FatormdoService extends GenericService<Fatormdo>{
	
	private FatormdoDAO fatormdoDAO;
	
	public void setFatormdoDAO(FatormdoDAO fatormdoDAO) {
		this.fatormdoDAO = fatormdoDAO;
	}
	
	/**
	 * Carrega a lista de fatores de m�o-de-obra de um determinado or�amento.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FatormdoDAO#findByOrcamentoForFlex(br.com.linkcom.sined.geral.bean.Orcamento)
	 *
	 * @param orcamento
	 * @return List<Fatormdo>
	 * @throws SinedException - caso o par�metro or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Fatormdo> findByOrcamentoForFlex(Orcamento orcamento){
		return fatormdoDAO.findByOrcamentoForFlex(orcamento);
	}
	
	/* 
	 * Deixar sobrescrito mesmo sem nenhuma implementa��o para ser usado
	 * na parte em flex da aplica��o.
	 */
	@Override
	public Fatormdo loadForEntrada(Fatormdo bean) {
		return super.loadForEntrada(bean);
	}
	
	@Override
	public void saveOrUpdate(Fatormdo bean) {
		if(bean != null && bean.getCdfatormdo() != null && bean.getCdfatormdo().intValue() == 0) {
			bean.setCdfatormdo(null);
		}
		if (bean.getListaFatormdoitem() != null) {
			for (Fatormdoitem fatormdoitem : bean.getListaFatormdoitem()) {
				if (fatormdoitem != null && fatormdoitem.getCdfatormdoitem().intValue() == 0) {
					fatormdoitem.setCdfatormdoitem(null);
				}
			}
		}
		super.saveOrUpdate(bean);
	}

	/**
	 * Busca os fatores de m�o-de-obra a partir de um filtro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FatormdoDAO#findForListagemFlex(FatorMdoFiltro)
	 * 
	 * @param filtro
	 * @return List<Fatormdo>
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Fatormdo> findForListagemFlex(FatorMdoFiltro filtro) {
		return fatormdoDAO.findForListagemFlex(filtro);
	}
	
	/**
	 * Deleta o fator de m�o-de-obra.
	 *
	 * @param fatormdo
	 * @author Rodrigo Alvarenga
	 */
	public void deleteFlex(Fatormdo fatormdo){
		if (fatormdo == null || fatormdo.getCdfatormdo() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		delete(fatormdo);
	}
	
	/**
	 * Calcula o valor do fator, baseado na f�rmula e na nos valores dos itens.
	 *
	 * @param formula
	 * @param listaFatorMdoItem
	 * @return Double
	 * @throws SinedException - caso ocorra algum erro na interpreta��o da f�rmula
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Double calculaValorTotal(String formula, List<Fatormdoitem> listaFatorMdoItem) {
		Object resultado = null;
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		
		//Passa tudo pra mai�sculo
		formula = formula.toUpperCase();
		
		//Retira os pontos da f�rmula
		formula = formula.replaceAll("\\.","");
		
		//Troca as v�rgulas por pontos
		formula = formula.replaceAll(",",".");

		if (listaFatorMdoItem != null) {
			for (Fatormdoitem fatorMdoItem : listaFatorMdoItem) {
				engine.put(fatorMdoItem.getSigla(), fatorMdoItem.getValor());
			}
		}		
		
		try {
			resultado = engine.eval(formula);
		} 
		catch (ScriptException e) {
			throw new SinedException(e.getMessage());
		}
		return (Double) resultado;
	}	
}
