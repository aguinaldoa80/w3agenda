package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ContacrmhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmhistoricoService extends GenericService<Contacrmhistorico>{

	private ContacrmhistoricoDAO contacrmhistoricoDAO;
	
	public void setContacrmhistoricoDAO(
			ContacrmhistoricoDAO contacrmhistoricoDAO) {
		this.contacrmhistoricoDAO = contacrmhistoricoDAO;
	}
	
	public List<Contacrmhistorico> findByContacrm(Contacrm contacrm) {
		return contacrmhistoricoDAO.findByContacrm(contacrm, null, null);
	}
	
	public List<Contacrmhistorico> findByContacrm(Contacrm contacrm, Atividadetipo atividadetipo, Empresa empresa) {
		return contacrmhistoricoDAO.findByContacrm(contacrm, atividadetipo, empresa);
	}
	
	public List<Contacrmhistorico> findByContacrm(Contacrm contacrm, Atividadetipo atividadetipo) {
		return contacrmhistoricoDAO.findByContacrm(contacrm, atividadetipo, null);
	}

	public boolean haveHistorico(Contacrm c, Timestamp dtenvio, String obs) {
		return contacrmhistoricoDAO.haveHistorico(c, dtenvio, obs);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Carrega a lista de hist�rico da contacrm ordenada por data
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContacrmhistoricoDAO#carregaListaContacrmhistorico(Contacrm contacrm)
	*
	* @param contacrm
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contacrmhistorico> carregaListaContacrmhistorico(Contacrm contacrm) {
		return contacrmhistoricoDAO.carregaListaContacrmhistorico(contacrm);
	}

}
