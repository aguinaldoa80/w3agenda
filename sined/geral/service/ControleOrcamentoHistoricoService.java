package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.ControleOrcamentoHistorico;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.dao.ControleOrcamentoHistoricoDAO;

public class ControleOrcamentoHistoricoService extends GenericService<ControleOrcamentoHistorico> {

	public ControleOrcamentoHistoricoDAO controleOrcamentoHistoricoDAO;
	
	public void setControleOrcamentoHistoricoDAO(
			ControleOrcamentoHistoricoDAO controleOrcamentoHistoricoDAO) {
		this.controleOrcamentoHistoricoDAO = controleOrcamentoHistoricoDAO;
	}

	public List<ControleOrcamentoHistorico> findByControleOrcamento(Controleorcamento controleorcamento) {
		return controleOrcamentoHistoricoDAO.findByControleOrcamento(controleorcamento);
	}
	
}
