package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcomissao;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobccomissionamento;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ResumoComissionamento;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentocomissaoService extends GenericService<Documentocomissao>{
	
	private DocumentocomissaoDAO documentocomissaoDAO;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaService notaService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	private ContratoService contratoService;
	private ContratodescontoService contratodescontoService;
	private DocumentoService documentoService;
	
	public void setContratodescontoService(
			ContratodescontoService contratodescontoService) {
		this.contratodescontoService = contratodescontoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setDocumentocomissaoDAO(DocumentocomissaoDAO documentocomissaoDAO) {
		this.documentocomissaoDAO = documentocomissaoDAO;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public List<Documentocomissao> findByCliente(Cliente cliente){
		return documentocomissaoDAO.findByCliente(cliente);
	}
	public List<Documentocomissao> findByPagamento(String whereIn){
		return documentocomissaoDAO.findByPagamento(whereIn);
	}
	public void baixarComissao(Documentocomissao documentocomissao, Colaboradorcomissao colaboradorcomissao){
		documentocomissaoDAO.baixarComissao(documentocomissao, colaboradorcomissao);
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {
		this.vdocumentonegociadoService = vdocumentonegociadoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findForFaturamento
	 *
	 * @param c
	 * @param colaborador
	 * @param comissionamento
	 * @param vendedor
	 * @return
	 * @author Rodrigo Freitas
	 * @param vendedor2 
	 * @param documento 
	 */
	public Integer findForFaturamento(Contrato contrato, Pessoa colaborador, Comissionamento comissionamento, boolean vendedor) {
		return documentocomissaoDAO.findForFaturamento(contrato, colaborador, comissionamento, vendedor);
	}

	/**
	 * M�todo que faz ajustes na lista para listagem
	 * 
	 * @param list
	 * @author Tom�s Rabelo
	 */
	public void ajustaDadosDasNotas(List<Documentocomissao> list) {
		List<Nota> listaNotas = new ArrayList<Nota>();
		for (Documentocomissao item : list) {
			if(item.getNota() != null && item.getNota().getCdNota() != null){
				listaNotas.add(item.getNota());
			}else if(item.getDocumento() != null && item.getDocumento().getCddocumento() != null && item.getContrato() != null && item.getContrato().getCdcontrato() != null){
				List<Documento> listaDocNegociado = documentoService.findForDocumentoNegociadoWithNota( item.getDocumento(),  item.getContrato());
				if(listaDocNegociado != null && listaDocNegociado.size() > 0){
					for(Documento d : listaDocNegociado){
						if(d.getListaNotaDocumento() != null && d.getListaNotaDocumento().size() > 0){
							item.setNota(d.getListaNotaDocumento().get(0).getNota());
							break;
						}
					}
				}
			}
		}
		
		if(listaNotas != null && !listaNotas.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaNotas, "cdNota", ",");
			List<Notafiscalproduto> listaNotasFiscaisProdutos = notafiscalprodutoService.findByWhereIn(whereIn);
			List<NotaFiscalServico> listaNotasFiscaisServicos = notaFiscalServicoService.findByWhereIn(whereIn);
			
			if(listaNotasFiscaisServicos != null && !listaNotasFiscaisServicos.isEmpty()){
				for (NotaFiscalServico notaFiscalServico : listaNotasFiscaisServicos) {
					for (Documentocomissao documentocomissao : list){ 
						if(documentocomissao.getNota() != null && documentocomissao.getNota().getCdNota() != null && notaFiscalServico.equals(documentocomissao.getNota())){
							documentocomissao.setNota(notaFiscalServico);
							//break;
						}
					}
				}
			}

			if(listaNotasFiscaisProdutos != null && !listaNotasFiscaisProdutos.isEmpty()){
				for (Notafiscalproduto notafiscalproduto : listaNotasFiscaisProdutos) {
					for (Documentocomissao documentocomissao : list){ 
						if(documentocomissao.getNota() != null && documentocomissao.getNota().getCdNota() != null && notafiscalproduto.equals(documentocomissao.getNota())){
							documentocomissao.setNota(notafiscalproduto);
							//break;
						}
					}
				}
			}
			
		}
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca documentocomiss�o do contrato
	* 
	* @see	br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO.findByPagamento(Contrato contrato)
	*
	* @param contrato
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public Documentocomissao findByPagamento(Contrato contrato){
		return documentocomissaoDAO.findByPagamento(contrato);
	}
	
	/**
	*  M�todo com refer�ncia no DAO
	*  Busca os documentocomissao gerados pelo faturamento do contrato
	*
	* @param contrato
	* @param nota
	* @return
	* @since Oct 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Documentocomissao> findComissaocontrato(Contrato contrato, Nota nota){
		return documentocomissaoDAO.findComissaocontrato(contrato, nota);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* Atualiza o documentocomissao com o n�mero do documento gerado pelo GERAR RECEITA AUTOM�TICA da NFS
	*
	* @param documentocomissao
	* @param documento
	* @since Oct 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaDocumentocomissao(Documentocomissao documentocomissao,Documento documento){
		documentocomissaoDAO.atualizaDocumentocomissao(documentocomissao, documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findByPagamentoVenda(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findByPagamentoVenda(String whereIn){
		return documentocomissaoDAO.findByPagamentoVenda(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findForTotalDocumentocomissao(DocumentocomissaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissao(DocumentocomissaoFiltro filtro) {
		return documentocomissaoDAO.findForTotalDocumentocomissao(filtro);
	}
	
	public Money findForTotaldocumentoDocumentocomissao(DocumentocomissaoFiltro filtro) {
		return documentocomissaoDAO.findForTotaldocumentoDocumentocomissao(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#getTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada, String tipo)
	 *
	 * @param colaborador
	 * @param dataGerada
	 * @param tipo
	 * @return
	 * @author Luiz Fernando
	 * @param empresa 
	 */
	public Money getTotalComissaoColaboradorMes(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2, String tipo) {
		return documentocomissaoDAO.getTotalComissaoColaboradorMes(empresa, colaborador, dtreferencia1, dtreferencia2, tipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#deleteDocumentocomissaoByVendaCancelada(Venda venda)
	 *
	 * @param venda
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissaoByVendaCancelada(Venda venda) {
		documentocomissaoDAO.deleteDocumentocomissaoByVendaCancelada(venda);
		
	}
	
	public void deleteDocumentocomissaoByPedidovendaCancelado(Pedidovenda pedidovenda) {
		documentocomissaoDAO.deleteDocumentocomissaoByPedidovendaCancelado(pedidovenda);
		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documentocomissao> loadWithLista(String whereIn){
		return documentocomissaoDAO.loadWithLista(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findDocumentocomissao(Documento documento, Nota nota, Pessoa pessoa, Comissionamento comissionamento, Contrato contrato, Boolean vendedor)
	 *
	 * @param documento
	 * @param nota
	 * @param pessoa
	 * @param comissionamento
	 * @param contrato
	 * @param vendedor
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documentocomissao> findDocumentocomissao(Documento documento, Nota nota, Pessoa pessoa, 
			Comissionamento comissionamento, Contrato contrato, Boolean vendedor) {
		return documentocomissaoDAO.findDocumentocomissao(documento, nota, pessoa, comissionamento, contrato, vendedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findDocumentocomissaoByDocumentonegociado(Documento documento, Nota nota, Pessoa pessoa, Comissionamento comissionamento, Contrato contrato, Boolean vendedor)
	 *
	 * @param documento
	 * @param nota
	 * @param pessoa
	 * @param comissionamento
	 * @param contrato
	 * @param vendedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findDocumentocomissaoByDocumentonegociado(Documento documento, Nota nota, Pessoa pessoa, 
			Comissionamento comissionamento, Contrato contrato, Boolean vendedor) {
		return documentocomissaoDAO.findDocumentocomissaoByDocumentonegociado(documento, nota, pessoa, comissionamento, contrato, vendedor);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#updateValorComissao(Documentocomissao documentocomissao, Money valorcomissao)
	 *
	 * @param documentocomissao
	 * @param valorcomissao
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateValorComissao(Documentocomissao documentocomissao, Money valorcomissao) {
		documentocomissaoDAO.updateValorComissao(documentocomissao, valorcomissao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findForExcluircomissao(Contrato contrato, String whereNotInExclusao)
	 *
	 * @param contrato
	 * @param whereNotInExclusao
	 * @return
	 * @author Luiz Fernando
	 * @param documento 
	 * @param nota 
	 */
	public List<Documentocomissao> findForExcluircomissao(Contrato contrato, Nota nota, Documento documento, String whereNotInExclusao, String whereNotInExclusaoVendedor) {
		return documentocomissaoDAO.findForExcluircomissao(contrato, nota, documento, whereNotInExclusao, whereNotInExclusaoVendedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#deleteDocumentocomissao(Documentocomissao documentocomissao)
	 *
	 * @param documentocomissao
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissao(Documentocomissao documentocomissao) {
		documentocomissaoDAO.deleteDocumentocomissao(documentocomissao);
	}
	
	public void deleteDocumentocomissaoByDocumento(Documento doc) {
		documentocomissaoDAO.deleteDocumentocomissaoByDocumento(doc);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#atualizaDocumentocomissao(Venda venda, Pedidovenda pedidovenda)
	 *
	 * @param venda
	 * @param pedidovenda
	 * @author Luiz Fernando
	 */
	public void atualizaDocumentocomissao(Venda venda, Pedidovenda pedidovenda) {
		documentocomissaoDAO.atualizaDocumentocomissao(venda, pedidovenda);
	}
	
	/**
	 * M�todo que gera o CSV do comissionamento de contrato
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Resource gerarRelarorioCSVListagem(DocumentocomissaoFiltro filtro) {
		
		Documentocomissaotipoperiodo tipoperiodo = filtro.getDocumentocomissaotipoperiodo();
		
		filtro.setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo.PAGAMENTO);
		List<Documentocomissao> listaPagamento = this.findForRelarorioCSVListagem(filtro);
		this.ajustaDadosListagem(listaPagamento);
		
		filtro.setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo.EMISSAO);
		List<Documentocomissao> listaEmissao = this.findForRelarorioCSVListagem(filtro);
		this.ajustaDadosListagem(listaEmissao);
		
		filtro.setDocumentocomissaotipoperiodo(tipoperiodo);
		
		HashMap<Integer, String> mapPessoa = new HashMap<Integer, String>();
		HashMap<Integer, Money> mapPessoaTotalcomsisaoEmitidos = new HashMap<Integer, Money>();
		HashMap<Integer, Money> mapPessoaTotalcomsisaoRecebidos = new HashMap<Integer, Money>();

		for (Documentocomissao documentocomissao : listaEmissao) {
			if(documentocomissao.getPessoa() != null && documentocomissao.getPessoa().getCdpessoa() != null){
				mapPessoa.put(documentocomissao.getPessoa().getCdpessoa(), documentocomissao.getPessoa().getNome());
				if(documentocomissao.getValorcomissao() != null){
					if(mapPessoaTotalcomsisaoEmitidos.get(documentocomissao.getPessoa().getCdpessoa()) != null){
						mapPessoaTotalcomsisaoEmitidos.put(documentocomissao.getPessoa().getCdpessoa(), mapPessoaTotalcomsisaoEmitidos.get(documentocomissao.getPessoa().getCdpessoa()).add(documentocomissao.getValorcomissao()));
					}else {
						mapPessoaTotalcomsisaoEmitidos.put(documentocomissao.getPessoa().getCdpessoa(), documentocomissao.getValorcomissao());
					}
				}
			}
		}
		
		for (Documentocomissao documentocomissao : listaPagamento) {
			if(documentocomissao.getPessoa() != null && documentocomissao.getPessoa().getCdpessoa() != null){
				mapPessoa.put(documentocomissao.getPessoa().getCdpessoa(), documentocomissao.getPessoa().getNome());
				if(documentocomissao.getValorcomissao() != null){
					if(mapPessoaTotalcomsisaoRecebidos.get(documentocomissao.getPessoa().getCdpessoa()) != null){
						mapPessoaTotalcomsisaoRecebidos.put(documentocomissao.getPessoa().getCdpessoa(), mapPessoaTotalcomsisaoRecebidos.get(documentocomissao.getPessoa().getCdpessoa()).add(documentocomissao.getValorcomissao()));
					}else {
						mapPessoaTotalcomsisaoRecebidos.put(documentocomissao.getPessoa().getCdpessoa(), documentocomissao.getValorcomissao());
					}
				}
			}
		}
		
		StringBuilder csv = new StringBuilder();
		csv.append("Colaborador;Total de comissionamento de documentos recebidos;Total de comissionamento de documentos emitidos;\n");
		for(Integer cdpessoa : mapPessoa.keySet()){
			csv.append(mapPessoa.get(cdpessoa)).append(";");
			csv.append(mapPessoaTotalcomsisaoRecebidos.get(cdpessoa) != null ? mapPessoaTotalcomsisaoRecebidos.get(cdpessoa) : "0,00").append(";");
			csv.append(mapPessoaTotalcomsisaoEmitidos.get(cdpessoa) != null ? mapPessoaTotalcomsisaoEmitidos.get(cdpessoa) : "0,00").append(";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "comissionamento_contrato" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findForRelarorioCSVListagem(DocumentocomissaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRelarorioCSVListagem(DocumentocomissaoFiltro filtro) {
		return documentocomissaoDAO.findForRelarorioCSVListagem(filtro);
	}
	
	@SuppressWarnings("unchecked")
	public void ajustaDadosListagem(List<Documentocomissao> list){
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cddocumentocomissao", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(this.loadWithLista(whereIn));
		}	
		
		this.ajustaDadosDasNotas(list);
		
		if(list.size() > 0){
			List<Nota> notas = (List<Nota>)CollectionsUtil.getListProperty(list, "nota");
			for (Iterator<Nota> iterator2 = notas.iterator(); iterator2.hasNext();) {
				Nota nota = iterator2.next();
				if(nota == null){
					iterator2.remove();
				}
			}
			notas = notaService.carregarInfoParaCalcularTotal(notas);
			
			List<Documento> listaAux = (List<Documento>)CollectionsUtil.getListProperty(list, "documento");
			List<Vdocumentonegociado> listaNegociados = null;
			List<Documento> listaDoc = new ArrayList<Documento>();
			if(listaAux != null){
				for (int i = 0; i < listaAux.size(); i++) {
					if (listaAux.get(i) != null)
						listaDoc.add(listaAux.get(i));
				}
				String whereInDocumentos = CollectionsUtil.listAndConcatenate(listaDoc, "cddocumento", ",");
				if(whereInDocumentos != null && !"".equals(whereInDocumentos))
					listaNegociados = vdocumentonegociadoService.findByDocumentos(whereInDocumentos);
			}
			
			for (Documentocomissao documentocomissao : list) {
				if(documentocomissao.getNota() != null){
					for (Nota nota : notas) {
						if(nota.getCdNota().equals(documentocomissao.getNota().getCdNota())){
							documentocomissao.setNota(nota);
							break;
						}
					}
				}
				
				if(documentocomissao.getDocumento() != null && listaNegociados != null) {
					List<Documento> documentosNegociados = new ArrayList<Documento>();
					for (Vdocumentonegociado v : listaNegociados) {
						if(v.getCddocumento().equals(documentocomissao.getDocumento().getCddocumento())){
							documentosNegociados.add(new Documento(v.getCddocumentonegociado(), 
																	v.getCddocumentoacaonegociado(), 
																	v.getDtvencimentonegociado(), 
																	v.getDtemissaonegociado()));
						}
					}
					documentocomissao.setDocumentosNegociados(documentosNegociados);
				}
			}
		}
	}
	
	/**
	 * Gera o relat�rio de vis�o Colaborador.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2012
	 */
	public IReport gerarRelatorioComissionamentoColaborador(DocumentocomissaoFiltro filtro) {
		
		List<Documentocomissao> lista = this.findForRelatorioComissionamentoColaborador(filtro);
		this.ajustaDadosListagem(lista);
		
		for (Documentocomissao documentocomissao : lista) {
			StringBuilder sbDocumentoNota = new StringBuilder();
			sbDocumentoNota.append("Conta a receber: ").append(documentocomissao.getDocumento().getCddocumento()).append("\n");
			if(documentocomissao.getDocumentosNegociados() != null && documentocomissao.getDocumentosNegociados().size() > 0){
				sbDocumentoNota.append("Negocia��o: ").append(CollectionsUtil.listAndConcatenate(documentocomissao.getDocumentosNegociados(), "cddocumento", ", ")).append("\n");
			}
			if(documentocomissao.getNota() != null && documentocomissao.getNota().getCdNota() != null){
				sbDocumentoNota.append("Nota: ").append(documentocomissao.getNota().getNumero());
			}
			documentocomissao.setDocumentoNota(sbDocumentoNota.toString());
			
			if(documentocomissao.getColaboradorcomissao() != null && 
									documentocomissao.getColaboradorcomissao().getDocumento() != null && 
									documentocomissao.getColaboradorcomissao().getDocumento().getValor() != null &&
									documentocomissao.getColaboradorcomissao().getDocumento().getDocumentoacao() != null &&
									documentocomissao.getColaboradorcomissao().getDocumento().getDocumentoacao().equals(Documentoacao.CANCELADA)){
				documentocomissao.setValorrepassadoreport(documentocomissao.getColaboradorcomissao().getDocumento().getValor());
			}
		}
		
		Report report = new Report("/rh/comissionamentoColaborador");
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaoDAO#findForRelatorioComissionamentoColaborador(DocumentocomissaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2012
	 */
	private List<Documentocomissao> findForRelatorioComissionamentoColaborador(DocumentocomissaoFiltro filtro) {
		return documentocomissaoDAO.findForRelatorioComissionamentoColaborador(filtro);
	}
	
	/**
	 * Gera o relat�rio de comissionamento da vis�o Contrato.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2012
	 */
	public IReport gerarRelatorioComissionamentoContrato(DocumentocomissaoFiltro filtro) {
		List<Contrato> lista = contratoService.findForRelatorioComissionamentoContrato(filtro);
		List<Contrato> listaNova = new ArrayList<Contrato>();
		List<ResumoComissionamento> listaResumocomissao = new ArrayList<ResumoComissionamento>();
		HashMap<Pessoa, Money> mapColaboradorValorrepassado = new HashMap<Pessoa, Money>();
		
		List<Documentocomissao> listaDc = this.findForRelatorioComissionamentoColaborador(filtro);
		this.ajustaDadosListagem(listaDc);
		
		for (Documentocomissao documentocomissao : listaDc) {
			if(documentocomissao.getColaboradorcomissao() != null && 
									documentocomissao.getColaboradorcomissao().getDocumento() != null && 
									documentocomissao.getColaboradorcomissao().getDocumento().getValor() != null &&
									documentocomissao.getColaboradorcomissao().getDocumento().getDocumentoacao() != null &&
									!documentocomissao.getColaboradorcomissao().getDocumento().getDocumentoacao().equals(Documentoacao.CANCELADA)){
				if(mapColaboradorValorrepassado.get(documentocomissao.getPessoa()) != null){
					mapColaboradorValorrepassado.put(documentocomissao.getPessoa(), 
							mapColaboradorValorrepassado.get(documentocomissao.getPessoa()).add(documentocomissao.getColaboradorcomissao().getDocumento().getValor())
					);
				}else {
					mapColaboradorValorrepassado.put(documentocomissao.getPessoa(),documentocomissao.getColaboradorcomissao().getDocumento().getValor());
				}
			}
		}
		
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdcontrato", ",");
			List<Contratodesconto> listaContratodesconto = contratodescontoService.findByContratoComissinamento(whereIn); 
		
			for (Contrato contrato : lista) {
				
				List<Contratodesconto> listaDesconto = new ArrayList<Contratodesconto>();
				for (Contratodesconto contratodesconto : listaContratodesconto) {
					if(contratodesconto.getContrato() != null && contrato.equals(contratodesconto.getContrato())){
						listaDesconto.add(contratodesconto);
					}
				}
				contrato.setListaContratodesconto(listaDesconto);
				
				if(contrato.getListaContratocolaborador() == null){
					contrato.setListaContratocolaborador(new ArrayList<Contratocolaborador>());
				}
				
				if(contrato.getComissionamento() != null && contrato.getVendedor() != null){
					Contratocolaborador contratocolaborador = new Contratocolaborador();
					contratocolaborador.setComissionamento(contrato.getComissionamento());
					contratocolaborador.setOrdemcomissao(0);
					contratocolaborador.setColaborador(new Colaborador(contrato.getVendedor().getCdpessoa(), contrato.getVendedor().getNome()));
					contratocolaborador.setIsVendedor(Boolean.TRUE);
					
					contrato.getListaContratocolaborador().add(contratocolaborador);
				}
				
				List<Contratocolaborador> listaNovaColaborador = new ArrayList<Contratocolaborador>();
				
				for (Contratocolaborador cc : contrato.getListaContratocolaborador()) {
					if(cc.getComissionamento() != null){
						if(cc.getOrdemcomissao() == null) cc.setOrdemcomissao(0);
						cc.setIsVendedor(Boolean.FALSE);
						listaNovaColaborador.add(cc);
					}
				}
				
				contrato.setListaContratocolaborador(listaNovaColaborador);
				
				Collections.sort(contrato.getListaContratocolaborador(), new Comparator<Contratocolaborador>(){
					public int compare(Contratocolaborador o1, Contratocolaborador o2) {
						if(!o1.getOrdemcomissao().equals(o2.getOrdemcomissao())){
							return o1.getOrdemcomissao().compareTo(o2.getOrdemcomissao());
						} else {
							return o1.getColaborador().getNome().compareTo(o2.getColaborador().getNome());
						}
					}
				});
				
				if(contrato.getListaContratocolaborador() != null && contrato.getListaContratocolaborador().size() > 0){
					listaNova.add(contrato);
				}
			}
			
			for (Contrato contrato : listaNova) {
				Money valorbruto = contrato.getValor();
				Money valorliquido = contrato.getValorContrato();
				
				List<Contratocolaborador> listaContratocolaborador = contrato.getListaContratocolaborador();
				for (Contratocolaborador contratocolaborador : listaContratocolaborador) {
					Comissionamento comissionamento = contratocolaborador.getComissionamento();
					
					contratocolaborador.setPercentual(comissionamento.getPercentual());
					contratocolaborador.setValor(comissionamento.getValor());
					
					Money valor = new Money();
					if(comissionamento.getTipobccomissionamento() != null){
						if(comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
							valor = valorbruto;
						} else {
							valor = valorliquido;
						}
					}
					
					if(comissionamento.getValor() == null){
						Money valorcomissao = contratoService.calculaValorComissao(contrato, valor, contratocolaborador.getColaborador(), contratocolaborador.getIsVendedor(), listaContratocolaborador);
						BigDecimal vlrcomissao = SinedUtil.roundFloor(valorcomissao.getValue(), 2);
						contratocolaborador.setValor(vlrcomissao.doubleValue());
					} else if(comissionamento.getPercentual() == null && comissionamento.getValor() != null){
						Double percentual = (comissionamento.getValor() * 100d) / valor.getValue().doubleValue();
						contratocolaborador.setPercentual(percentual);
					}
				}
			}
		}
		
		if(mapColaboradorValorrepassado.size() > 0){
			for(Pessoa pessoa : mapColaboradorValorrepassado.keySet()){
				listaResumocomissao.add(new ResumoComissionamento(pessoa, mapColaboradorValorrepassado.get(pessoa)));
			}
		}
		
		Report report = new Report("/rh/comissionamentoContrato");
		Report subreport = new Report("/rh/comissionamentoContrato_subreport");
		Report subReportResumocomissionamento = new Report("/rh/resumocomissionamento");
		
		report.addSubReport("LISTACOLABORADOR_SUBREPORT", subreport);
		report.setDataSource(listaNova);
		
		report.addParameter("LISTARESUMOCOMISSAO", listaResumocomissao);
		report.addSubReport("SUB_RESUMOCOMISSAO", subReportResumocomissionamento);
		return report;
	}
	
	public List<Documentocomissao> findForGerarArquivoSEFIP(Colaborador colaborador, Date dtinicio, Date dtfim) {
		return documentocomissaoDAO.findForGerarArquivoSEFIP(colaborador, dtinicio, dtfim);
	}
	
	/**
	 * M�todo que gera um documento comissao e uma entrada em comissaocolaborador 
	 * de acordo com o colaborador fornecido via faturamento por WebService.
	 * 
	 * @param documento
	 * @param colaborador
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public void gerarComissaoFaturamentoWebService(final Documento documento, final Colaborador colaborador) throws Exception{
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Documentocomissao documentocomissao = new Documentocomissao();
				documentocomissao.setPessoa(colaborador);
				documentocomissao.setDocumento(documento);
				documentocomissao.setContrato(documento.getContrato());
				documentocomissao.setValorcomissao(documento.getValor());		
				saveOrUpdate(documentocomissao);
				
				return status;
			}
		});
	}
	
	public Boolean podeGerarComissao(Contrato contrato, Nota nota, Documento documento, Pessoa vendedor, Integer qtde, boolean verificarExistencia, Documentocomissao documentocomissaoNovo) {
		boolean podeGerar = qtde == null;
		if(contrato != null && vendedor != null && qtde != null){
			List<Documentocomissao> listaDocumentocomissao = findForCalcularQtdeGeradaByContratoPessoa(contrato, vendedor);
			if(listaDocumentocomissao == null || listaDocumentocomissao.isEmpty()){
				podeGerar = true;
			}else {
				if(verificarExistencia){
					listaDocumentocomissao = ajustaListaDocumentocomissao(listaDocumentocomissao, documentocomissaoNovo, qtde);
					podeGerar = true;
					int count = 0;
					for(Documentocomissao dc : listaDocumentocomissao){
						if(dc.getDocumento() != null && dc.getDocumento().getCddocumento() != null &&
								documento != null && documento.getCddocumento() != null && documento.getDtemissao() != null && 
								dc.getDocumento().getDtemissao() != null){
							if((SinedDateUtils.afterOrEqualsIgnoreHour(documento.getDtemissao(), dc.getDocumento().getDtemissao()) &&
								count >= qtde.intValue()) || 
								(SinedDateUtils.beforeOrEqualIgnoreHour(documento.getDtemissao(), dc.getDocumento().getDtemissao()) &&
								count >= qtde.intValue())){
									if(count > qtde || documento.getCddocumento().compareTo(dc.getDocumento().getCddocumento()) != -1){
										podeGerar = false;
									}
							}
						}else if(dc.getNota() != null && dc.getNota().getCdNota() != null && nota != null && 
								nota.getCdNota() != null && dc.getNota().getDtEmissao() != null){
								
							if((SinedDateUtils.afterOrEqualsIgnoreHour(nota.getDtEmissao(), dc.getNota().getDtEmissao()) &&
								count >= qtde.intValue()) || 
								(SinedDateUtils.beforeOrEqualIgnoreHour(nota.getDtEmissao(), dc.getNota().getDtEmissao()) &&
								count >= qtde.intValue())){
									if(count > qtde || nota.getCdNota().compareTo(dc.getNota().getCdNota()) != -1){
										podeGerar = false;
									}
							}
						}
						if(count >= qtde.intValue() || !podeGerar){
							break;
						}
						count++;
					}
				}else if(listaDocumentocomissao.size() < qtde){
					podeGerar = true;
				}
			}
		}
		return podeGerar;
	}
	
	private List<Documentocomissao> ajustaListaDocumentocomissao(List<Documentocomissao> listaDocumentocomissao, Documentocomissao documentocomissaoNovo, Integer qtde) {
		List<Documentocomissao> listaDocumentocomissaoAjustada = new ArrayList<Documentocomissao>();
		if(documentocomissaoNovo != null && listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
			boolean adicionarForValidacao = true;
			int indexRemover = 0;
			for(Documentocomissao dc : listaDocumentocomissao){
				if(dc.getDocumento() != null && dc.getDocumento().getCddocumento() != null &&
						documentocomissaoNovo.getDocumento() != null && documentocomissaoNovo.getDocumento().getCddocumento() != null && 
						dc.getDocumento().getCddocumento().equals(documentocomissaoNovo.getDocumento().getCddocumento())){
					adicionarForValidacao = false;
					break;
				}else if(dc.getNota() != null && dc.getNota().getCdNota() != null &&
						documentocomissaoNovo.getNota() != null && 
						documentocomissaoNovo.getNota().getCdNota() != null && 
						dc.getNota().getCdNota().equals(documentocomissaoNovo.getNota().getCdNota())){
					adicionarForValidacao = false;
					break;
				}
				indexRemover++;
			}
			if(adicionarForValidacao){
				listaDocumentocomissao.add(documentocomissaoNovo);
				Collections.sort(listaDocumentocomissao, new Comparator<Documentocomissao>(){
					public int compare(Documentocomissao dc1, Documentocomissao dc2){
						int compare = -1;
						if(dc1.getDocumento() != null && dc1.getDocumento().getDtemissao() != null && 
								dc2.getDocumento() != null && dc2.getDocumento().getDtemissao() != null){
							if(SinedDateUtils.afterIgnoreHour(dc1.getDocumento().getDtemissao(), dc2.getDocumento().getDtemissao())){
								compare = 1;
							}else if(SinedDateUtils.beforeIgnoreHour(dc1.getDocumento().getDtemissao(), dc2.getDocumento().getDtemissao())){
								compare = -1;
							}else {
								try {
									compare = dc2.getDocumento().getCddocumento().compareTo(dc1.getDocumento().getCddocumento());
								} catch (Exception e) {
									compare = 0;
								}
							}
						}else if(dc1.getNota() != null && dc1.getNota().getDtEmissao() != null && 
								dc2.getNota() != null && dc2.getNota().getDtEmissao() != null){
							if(SinedDateUtils.afterIgnoreHour(dc1.getNota().getDtEmissao(), dc2.getNota().getDtEmissao())){
								compare = 1;
							}else if(SinedDateUtils.beforeIgnoreHour(dc1.getNota().getDtEmissao(), dc2.getNota().getDtEmissao())){
								compare = -1;
							}else {
								try {
									compare = dc2.getNota().getCdNota().compareTo(dc1.getNota().getCdNota());
								} catch (Exception e) {
									compare = 0;
								}
							}
						}else {
							compare = 0;
						}
						return compare;
					}
				});
			}else if(qtde != null && indexRemover > qtde.intValue()){
				listaDocumentocomissao.remove(indexRemover);
			}
			listaDocumentocomissaoAjustada = listaDocumentocomissao;
		}
		return listaDocumentocomissaoAjustada;		
	}
	
	public List<Documentocomissao> findForCalcularQtdeGeradaByContratoPessoa(Contrato contrato, Pessoa vendedor) {
		return documentocomissaoDAO.findForCalcularQtdeGeradaByContratoPessoa(contrato, vendedor);
	}
	
	public List<Documentocomissao> buscarComissionamentoPorFaixas(DocumentocomissaoFiltro filtro) {
		return documentocomissaoDAO.buscarComissionamentoPorFaixas(filtro);
	}
}