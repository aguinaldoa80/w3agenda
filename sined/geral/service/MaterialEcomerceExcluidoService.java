package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.MaterialEcomerceExcluido;
import br.com.linkcom.sined.geral.dao.MaterialEcomerceExcluidoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialEcomerceExcluidoService extends GenericService<MaterialEcomerceExcluido>{

	private MaterialEcomerceExcluidoDAO materialEcomerceExcluidoDAO;
	
	public void setMaterialEcomerceExcluidoDAO(MaterialEcomerceExcluidoDAO materialEcomerceExcluidoDAO) {
		this.materialEcomerceExcluidoDAO = materialEcomerceExcluidoDAO;
	}
	
	public MaterialEcomerceExcluido loadByIdEcommerce(Integer idEcommerce){
		return materialEcomerceExcluidoDAO.loadByIdEcommerce(idEcommerce);
	}
}
