package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendaitemadicional;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.ProducaoagendaitemadicionalDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ProducaoAgendaItemAdicionalWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoItemAdicionalProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoItemAdicionalProducaoW3producaoRESTWSBean;

public class ProducaoagendaitemadicionalService extends GenericService<Producaoagendaitemadicional> {
	
	private ProducaoagendaitemadicionalDAO producaoagendaitemadicionalDAO;
	private PneuService pneuService;
	private ProducaoagendaService producaoagendaService;
	private VendaService vendaService;
	private VendamaterialService vendamaterialService;
	private ParametrogeralService parametrogeralService;
	
	public void setProducaoagendaitemadicionalDAO(ProducaoagendaitemadicionalDAO producaoagendaitemadicionalDAO) {
		this.producaoagendaitemadicionalDAO = producaoagendaitemadicionalDAO;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	/* singleton */
	private static ProducaoagendaitemadicionalService instance;
	public static ProducaoagendaitemadicionalService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProducaoagendaitemadicionalService.class);
		}
		return instance;
	}
	
	public RetornoItemAdicionalProducaoW3producaoRESTModel retornoProducaoItemAdicionalForW3producao(RetornoItemAdicionalProducaoW3producaoRESTWSBean bean){
		RetornoItemAdicionalProducaoW3producaoRESTModel model = new RetornoItemAdicionalProducaoW3producaoRESTModel();
		
		try {
			Producaoagenda producaoagenda = new Producaoagenda(bean.getCdproducaoagenda());
			
			Producaoagendaitemadicional producaoagendaitemadicional = new Producaoagendaitemadicional();
			producaoagendaitemadicional.setProducaoagenda(producaoagenda);
			producaoagendaitemadicional.setMaterial(new Material(bean.getCdmaterial()));
			producaoagendaitemadicional.setQuantidade(bean.getQuantidade());
			producaoagendaitemadicional.setPneu(new Pneu(bean.getCdpneu()));
			
			saveOrUpdate(producaoagendaitemadicional);
			
			model.setSincronizado(Boolean.TRUE);
			
			producaoagenda = producaoagendaService.loadProducaoagenda(producaoagenda);
			Integer cdlocalarmazenagem = parametrogeralService.getIntegerNullable(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA);
			Localarmazenagem localarmazenagem_padrao = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
			Localarmazenagem localarmazenagem = producaoagenda.getEmpresa() != null ? producaoagenda.getEmpresa().getLocalarmazenagemmateriaprima() : null;
			
			producaoagendaService.doBaixaEstoqueItemAdicional(producaoagenda, producaoagendaitemadicional, localarmazenagem_padrao, localarmazenagem);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return model;
	}
	
	public List<Producaoagendaitemadicional> findByProducaoagenda(Producaoagenda producaoagenda) {
		return producaoagendaitemadicionalDAO.findByProducaoagenda(producaoagenda);
	}
	
	public List<Producaoagendaitemadicional> getItensAdicionaisByPedidovenda(Pedidovenda pedidovenda) {
		List<Producaoagendaitemadicional> listaProducaoagendaitemadicional = new ArrayList<Producaoagendaitemadicional>();
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByPedidovenda(pedidovenda);
		
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			Pneu pneu = pneuService.getPneuProducaoagenda(producaoagenda);
			if(producaoagenda.getListaProducaoagendaitemadicional() != null){
				for(Producaoagendaitemadicional producaoagendaitemadicional : producaoagenda.getListaProducaoagendaitemadicional()){
					if(producaoagendaitemadicional.getPneu() == null){
						producaoagendaitemadicional.setPneu(pneu);
					}
					listaProducaoagendaitemadicional.add(producaoagendaitemadicional);
				}
			}
		}		

		if(SinedUtil.isListNotEmpty(listaProducaoagendaitemadicional)){
			for(Producaoagendaitemadicional producaoagendaitemadicional : listaProducaoagendaitemadicional){
				vendaService.preencheValorVendaComTabelaPreco(
						producaoagendaitemadicional.getMaterial(), 
						null, 
						pedidovenda.getPedidovendatipo(), 
						pedidovenda.getCliente(), 
						pedidovenda.getEmpresa(), 
						producaoagendaitemadicional.getMaterial().getUnidademedida(), 
						pedidovenda.getPrazopagamento(), 
						null);
				
				if(producaoagendaitemadicional.getValorvenda() == null) producaoagendaitemadicional.setValorvenda(0d);
			}
			
			List<Producaoagendaitemadicional> listaAjustada = new ArrayList<Producaoagendaitemadicional>();
			List<Vendamaterial> listaVendamaterial  = vendamaterialService.findByItensAdicionais(null, CollectionsUtil.listAndConcatenate(listaProducaoagendaitemadicional, "cdproducaoagendaitemadicional", ","));
			if(SinedUtil.isListNotEmpty(listaVendamaterial)){
				for(Producaoagendaitemadicional producaoagendaitemadicional : listaProducaoagendaitemadicional){
					for(Vendamaterial vendamaterial : listaVendamaterial){
						if(vendamaterial.getCdproducaoagendaitemadicional() != null && vendamaterial.getQuantidade() != null &&
								producaoagendaitemadicional.getQuantidade() != null &&
								vendamaterial.getCdproducaoagendaitemadicional().equals(producaoagendaitemadicional.getCdproducaoagendaitemadicional())){
							producaoagendaitemadicional.setQuantidade(producaoagendaitemadicional.getQuantidade()-vendamaterial.getQuantidade());
						}
					}
					if(producaoagendaitemadicional.getQuantidade() != null && producaoagendaitemadicional.getQuantidade() > 0){
						listaAjustada.add(producaoagendaitemadicional);
					}
				}
				listaProducaoagendaitemadicional = listaAjustada;
			}
			
		}
		return listaProducaoagendaitemadicional;
	}
	
	public List<ProducaoAgendaItemAdicionalWSBean> toWSList(List<Producaoagendaitemadicional> lista){
		List<ProducaoAgendaItemAdicionalWSBean> retorno = new ArrayList<ProducaoAgendaItemAdicionalWSBean>();
		if(Hibernate.isInitialized(lista) && SinedUtil.isListNotEmpty(lista)){
			for(Producaoagendaitemadicional item: lista){
				ProducaoAgendaItemAdicionalWSBean bean = new ProducaoAgendaItemAdicionalWSBean();
				bean.setCdproducaoagendaitemadicional(item.getCdproducaoagendaitemadicional());
				bean.setQuantidade(item.getQuantidade());
				bean.setQuantidadeDisponivel(item.getQuantidadeDisponivel());
				bean.setValortotal(item.getValortotal());
				bean.setValorvenda(item.getValorvenda());
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(item.getMaterial()));
				bean.setPneu(ObjectUtils.translateEntityToGenericBean(item.getPneu()));
				bean.setProducaoagenda(ObjectUtils.translateEntityToGenericBean(item.getProducaoagenda()));

				retorno.add(bean);
			}
		}
		return retorno;
	}
}