package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.dao.AjustefiscalDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AjustefiscalService extends GenericService<Ajustefiscal> {

	private AjustefiscalDAO ajustefiscalDAO;
	
	public void setAjustefiscalDAO(AjustefiscalDAO ajustefiscalDAO) {
		this.ajustefiscalDAO = ajustefiscalDAO;
	}

	public List<Ajustefiscal> findByEntregadocumento(Entregadocumento entregadocumento) {
		return ajustefiscalDAO.findByEntregadocumento(entregadocumento);
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscal(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			return null;
		return ajustefiscalDAO.buscarItemComAjusteFiscal(entregadocumento.getCdentregadocumento().toString());
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscal(String whereInEntregadocumento) {
		return ajustefiscalDAO.buscarItemComAjusteFiscal(whereInEntregadocumento);
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscalNota(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null)
			return null;
		return ajustefiscalDAO.buscarItemComAjusteFiscalNota(notafiscalproduto.getCdNota().toString());
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscalNota(String whereInNota) {
		return ajustefiscalDAO.buscarItemComAjusteFiscalNota(whereInNota);
	}
}
