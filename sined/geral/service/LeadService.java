	package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.CampoExtraLead;
import br.com.linkcom.sined.geral.bean.CampoExtraSegmento;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.auxiliar.TelefoneLeadBean;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.LeadDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarLeadBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadService extends GenericService<Lead>{
	
	private LeadDAO leadDAO;
	private LeadtelefoneService leadtelefoneService;
	private LeademailService leademailService;
	private ContacrmService contacrmService;
	private LeadhistoricoService leadhistoricoService;
	private LeadsegmentoService leadsegmentoService;
	private LeadqualificacaoService leadqualificacaoService;
	private LeadsituacaoService leadsituacaoService;
	private ColaboradorService colaboradorService;
	private TelefonetipoService telefonetipoService;
	private ClienteService clienteService;
	private SegmentoService segmentoService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setLeadDAO(LeadDAO leadDAO) {
		this.leadDAO = leadDAO;
	}
	public void setLeadtelefoneService(LeadtelefoneService leadtelefoneService) {
		this.leadtelefoneService = leadtelefoneService;
	}
	public void setLeademailService(LeademailService leademailService) {
		this.leademailService = leademailService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setLeadsegmentoService(LeadsegmentoService leadsegmentoService) {
		this.leadsegmentoService = leadsegmentoService;
	}
	public void setLeadqualificacaoService(
			LeadqualificacaoService leadqualificacaoService) {
		this.leadqualificacaoService = leadqualificacaoService;
	}
	public void setLeadsituacaoService(LeadsituacaoService leadsituacaoService) {
		this.leadsituacaoService = leadsituacaoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setTelefonetipoService(TelefonetipoService telefonetipoService) {
		this.telefonetipoService = telefonetipoService;
	}
	public void setSegmentoService(SegmentoService segmentoService) {
		this.segmentoService = segmentoService;
	}
	
	public List<Lead> findForReport(LeadFiltro filtro){
		return leadDAO.findForReport(filtro);
	}
	
	
	public IReport createRelatorioLead(LeadFiltro leadFiltro){
		
		Report report = new Report("/crm/leads");
		List<Lead> lista = this.findForReport(leadFiltro);
		
		
		for(Lead lead : lista){
			String montaEmail = "";
			if(lead.getListLeadtelefone()!= null){
				String strFone = "";
				for(Leadtelefone tel : lead.getListLeadtelefone()){
					strFone = strFone + tel.getTelefone();
					if(tel.getTelefonetipo() != null && tel.getTelefonetipo().getNome() != null){
						strFone = strFone + " (" + tel.getTelefonetipo().getNome() + ")\n" ;
					}
				}
				if(!strFone.equals("")){
					strFone= strFone.substring(0, (strFone.length()-1));
				}
				lead.setTelefones(strFone);
				
			}
			
			if(lead.getListleademail() != null){
				for(Leademail leademail : lead.getListleademail()){
					montaEmail = montaEmail + leademail.getEmail() + "\n";
				}
				lead.setEmails(montaEmail);
			}
			
			if(lead.getEmpresa() != null && !"".equals(lead.getEmpresa())){
				lead.setNomeempresa(lead.getEmpresa());
			}
			
		}
		
		report.setDataSource(lista);
		return report;
	}
	
	public Lead findForContacrm(Integer whereIn){
		return leadDAO.findForContacrm(whereIn);
	}
	
	public List<Lead> findLead(String whereIn) {
		return leadDAO.findLead(whereIn);
	}
	
	public Lead carregaLead(String whereIn) {
		return leadDAO.carregaLead(whereIn);
	}
	
	public List<Lead> findLeadsAutocomplete(String q) {
		return leadDAO.findLeadsAutocomplete(q);
	}
	
	public List<Lead> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return leadDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Lead> findLeadForBuscaGeral(String busca) {
		return leadDAO.findLeadForBuscaGeral(busca);
	}
	
	public List<Lead> findByEmail(String email) {
		return leadDAO.findByEmail(email);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Busca Lead ordenado pela data de leadhistorico
	* 
	* @see	br.com.linkcom.sined.geral.dao.LeadDAO#carregaLeadOrderByDataLeadhistorico(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Aug 22, 2011
	* @author Luiz Fernando F Silva
	*/
	public Lead carregaLeadOrderByDataLeadhistorico(String whereIn) {
		return leadDAO.carregaLeadOrderByDataLeadhistorico(whereIn);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadSituacao(Lead lead){
		leadDAO.updateLeadSituacao(lead);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadQualificacao(Lead lead){
		leadDAO.updateLeadQualificacao(lead);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 25/01/2012
	 * @param lead
	 */
	public void updateLeadProximoPasso(Lead lead){
		leadDAO.updateLeadProximoPasso(lead);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 25/01/2012
	 * @param lead
	 */
	public void updateLeadDtRetorno(Lead lead){
		leadDAO.updateLeadDtRetorno(lead);
	}
	
	public List<Lead> verificaLeadsConvertidos(List<Lead> listaResult){
		String whereIn = CollectionsUtil.listAndConcatenate(listaResult, "cdlead", ",");
		
		List<Contacrm> listContacrm = contacrmService.findContacrmByLeads(whereIn);
		List<Cliente> listCliente = clienteService.findByLead(whereIn);
		
		for(Lead lead : listaResult){
			for(Contacrm contacrm : listContacrm){
				if(lead.getCdlead().equals(contacrm.getLead().getCdlead())){
					lead.setConvertido(true);
				}
			}
			
			for (Cliente cliente : listCliente) {
				if(cliente.getListaLead() != null && cliente.getListaLead().size() > 0){
					for (Lead lead2 : cliente.getListaLead()) {
						if(lead.getCdlead().equals(lead2.getCdlead())){
							lead.setConvertido(true);
						}
					}
				}
			}
		}
		return listaResult;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name updateClienteLead
	 * @param lead
	 * @return void
	 * @author Thiago Augusto
	 * @date 12/06/2012
	 *
	 */
	public void updateClienteLead(Lead lead){
		leadDAO.updateClienteLead(lead);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LeadDAO#getTotaiAtividadetipoBylead(Lead lead)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Leadhistorico> getTotaisAtividadetipoByLead(String whereIn){
		return leadDAO.getTotaiAtividadetipoBylead(whereIn);
	}
	
	/**
	 * Altera o respos�vel pelo lead
	 * @param id
	 * @param responsavel
	 */
	public void updateResponsavelLeads(String whereInCdlead, Colaborador responsavel) {
		leadDAO.updateResponsavelLeads(whereInCdlead, responsavel);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<Lead> findAllForDesassociarCampanha(String whereIn) throws Exception{
		return leadDAO.findAllForDesassociarCampanha(whereIn);
	}
	
	/**
	 * 
	 * @param empresa
	 * @author Thiago Clemente
	 */
	public List<Lead> findForBuscarEmpresa(String empresa) {
		return leadDAO.findForBuscarEmpresa(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LeadDAO#findForCSV(LeadFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Lead> findForCSV(LeadFiltro filtro) {
		return leadDAO.findForCSV(filtro);
	}
	
	/**
	 *  M�todo que adiciona informa��es para a listagem de lead
	 *  
	 * @param lead
	 * @param filtro
	 * @param csv
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void addInfListagem(Lead lead, LeadFiltro filtro, Boolean csv) {
		if(lead != null && filtro != null){
			Date dataAtual = new Date(System.currentTimeMillis());
			
			if(csv == null || !csv){
				lead.setListLeadtelefone(SinedUtil.listToSet(leadtelefoneService.findByLead(lead), Leadtelefone.class));
				lead.setListleademail(SinedUtil.listToSet(leademailService.findByLead(lead), Leademail.class));
			}
			
			if(lead.getListLeadtelefone()!= null){
				String strFone = "";
				for(Leadtelefone tel : lead.getListLeadtelefone()){
					strFone = strFone + tel.getTelefone();
					if(tel.getTelefonetipo() != null && tel.getTelefonetipo().getNome() != null){
						strFone = strFone + " (" + tel.getTelefonetipo().getNome() + ")<BR>" ;
					}
				}
				if(!strFone.equals("")){
					strFone= strFone.substring(0, (strFone.length()-4));
				}
				lead.setListaTelefones(strFone);
				
			}
			
			if(StringUtils.isNotBlank(lead.getEmail()) && lead.getListleademail() == null){
				lead.setListleademail(new ListSet<Leademail>(Leademail.class));
			}
	
			if(lead.getListleademail()!= null){
				lead.setListaEmails(leademailService.createListaEmail(lead.getListleademail(), lead.getEmail(), csv));
			}
			
			if(lead.getListLeadcampanha() != null){
				lead.setListaCampanhas(CollectionsUtil.listAndConcatenate(lead.getListLeadcampanha(), "campanha.nome", ", "));
			}
			
			if(lead.getCdlead() != null)
				lead.setListLeadhistoricoTotais(this.getTotaisAtividadetipoByLead(lead.getCdlead().toString()));
			
			if(csv == null || !csv){
				lead.setListLeadhistorico(leadhistoricoService.findByLead(lead));
			}
				
			if(lead.getListLeadhistorico() != null && !lead.getListLeadhistorico().isEmpty()){
				if(lead.getListLeadhistorico().get(lead.getListLeadhistorico().size()-1).getDtaltera() != null){
					int tamanhoLista = lead.getListLeadhistorico().size();
					
					if(lead.getConvertido() != null && lead.getConvertido() && lead.getDtconversao() != null) {
						
						if(tamanhoLista > 1) {
							Date dtUltimaConversao = lead.getDtconversao();
							Date dtPrimeiroHistorico = new Date(lead.getListLeadhistorico().get(tamanhoLista - 1).getDtaltera().getTime());

							lead.setCiclo(SinedDateUtils.diferencaDias(new java.util.Date(dtUltimaConversao.getTime()), new java.util.Date(dtPrimeiroHistorico.getTime())));
						} else {
							lead.setCiclo(0);
						}
						
					} else {
						lead.setCiclo(SinedDateUtils.diferencaDias(dataAtual, lead.getListLeadhistorico().get(tamanhoLista - 1).getDtaltera()));
					}
				}
			}
			
			if (filtro.getDtiniciohistorico()!=null || filtro.getDtfimhistorico()!=null){
				lead.setQtdehistorico(leadhistoricoService.getQtdeInteracoes(lead, filtro.getDtiniciohistorico(), filtro.getDtfimhistorico()).intValue());
			}
			
			if(csv == null || !csv){
				lead.setListLeadsegmento(leadsegmentoService.findByLead(lead));
			}
		}
	}
	
	public Lead findLeadForOportunidade (String whereIn){
		return leadDAO.findLeadForOportunidade(whereIn);
	}
	
	public Lead criarLeadWebService(CriarLeadBean beanWS){
		Lead bean = new Lead();
		if(beanWS.getCdresponsavel() == null){
			throw new SinedException("Id do colaborador respons�vel do lead deve ser informado.");
		}
		if(beanWS.getCdleadsituacao() == null){
			throw new SinedException("Id da situa��o do lead deve ser informado.");
		}
		if(beanWS.getCdleadqualificacao() == null){
			throw new SinedException("Id da qualifica��o do lead deve ser informado.");
		}
		if(beanWS.getContato() == null){
			throw new SinedException("Contato do lead deve ser informado.");
		}
		if(beanWS.getTelefone() == null){
			throw new SinedException("Ao menos um telefone deve ser informado.");
		}
		
		Colaborador responsavel = colaboradorService.load(new Colaborador(beanWS.getCdresponsavel()));
		if(responsavel == null){
			throw new SinedException("Id do colaborador respons�vel do lead n�o cadastrado.");
		}
		
		Leadsituacao leadsituacao = new Leadsituacao();
		leadsituacao.setCdleadsituacao(beanWS.getCdleadsituacao());
		if(leadsituacaoService.load(leadsituacao) == null){
			throw new SinedException("Id de situa��o do lead n�o cadastrado.");
		}
		Leadqualificacao leadqualificacao = new Leadqualificacao();
		leadqualificacao.setCdleadqualificacao(beanWS.getCdleadqualificacao());
		if(leadqualificacaoService.load(leadqualificacao) == null){
			throw new SinedException("Id de qualifica��o do lead n�o cadastrado.");
		}
		bean.setResponsavel(responsavel);
		bean.setLeadsituacao(leadsituacao);
		bean.setLeadqualificacao(leadqualificacao);
		bean.setNome(beanWS.getContato());
		bean.setEmpresa(beanWS.getEmpresa());
		bean.setEmail(beanWS.getEmail());
		bean.setObservacao(beanWS.getObservacao());
		bean.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
		for(TelefoneLeadBean telefone: beanWS.getTelefone()){
			if(telefone.getTelefone() == null){
				throw new SinedException("O n�mero do telefone deve ser informado.");
			}
			if(telefone.getCdtelefonetipo() == null){
				throw new SinedException("O tipo de telefone deve ser informado.");
			}
			Telefonetipo telefonetipo = new Telefonetipo(telefone.getCdtelefonetipo());
			telefonetipo = telefonetipoService.load(telefonetipo);
			if(telefonetipo == null){
				throw new SinedException("Tipo de telefone inv�lido. O tipo de telefone deve estar entre os valores 1, 2, 3, 4, 5 ou 6.");
			}
			Leadtelefone leadtelefone = new Leadtelefone();
			leadtelefone.setLead(bean);
			leadtelefone.setTelefone(telefone.getTelefone());
			leadtelefone.setTelefonetipo(telefonetipo);
			bean.getListLeadtelefone().add(leadtelefone);
		}
		saveOrUpdate(bean);
		String observacao = "Criado via WebService."+ (beanWS.getObservacao() != null? "\n"+beanWS.getObservacao(): "");
		Leadhistorico leadhistorico = new Leadhistorico();
		leadhistorico.setLead(bean);
		leadhistorico.setObservacao(observacao);
		leadhistoricoService.saveOrUpdate(leadhistorico);
		return bean;
	}
	
	public List<Lead> findWithResponsavel(String whereIn){
		return leadDAO.findWithResponsavel(whereIn);
	}
	
	public boolean usuarioPodeEditarLead(String whereInLead){
		if(SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado())){
			for(Lead lead: this.findWithResponsavel(whereInLead)){
				if(!SinedUtil.getUsuarioLogado().equals(lead.getResponsavel())){
					return false;
				}
			}
		}
		return true;
	}

	public Lead loadWithResponsavel(Integer cdlead){
		return this.findWithResponsavel(cdlead.toString()).get(0);
	}
	
	public Set<CampoExtraLead> carregaCamposExtraLead(List<Leadsegmento> leadSegmentoListFound) {
		Segmento segmento = null;
		Lead lead = null;
		CampoExtraLead campoExtraLead = null;
		Set<CampoExtraLead> campoExtraLeadList = new ListSet<CampoExtraLead>(CampoExtraLead.class);
		
		for (Leadsegmento ls : leadSegmentoListFound) {
			if (ls.getSegmento() != null) {
				segmento = segmentoService.loadFetch(ls.getSegmento(), "campoExtraSegmentoList");
				
				if (segmento.getCampoExtraSegmentoList() != null && segmento.getCampoExtraSegmentoList().size() > 0) {
					for (CampoExtraSegmento s : segmento.getCampoExtraSegmentoList()) {
						campoExtraLead = new CampoExtraLead();
						campoExtraLead.setCampoExtraSegmento(s);
						
						lead = this.loadFetch(ls.getLead(), "campoExtraLeadList");
						if (lead.getCampoExtraLeadList() != null && lead.getCampoExtraLeadList().size() > 0) {
							for (CampoExtraLead l : lead.getCampoExtraLeadList()) {
								if (s.getCdcampoextrasegmento().equals(l.getCampoExtraSegmento().getCdcampoextrasegmento())) {
									campoExtraLead.setCdcampoextralead(l.getCdcampoextralead());
									campoExtraLead.setValor(l.getValor());
									break;
								} else {
									campoExtraLead.setValor(s.getConteudoPadrao() != null ? s.getConteudoPadrao() : "");
								}
							}
						} else {
							campoExtraLead.setValor(s.getConteudoPadrao() != null ? s.getConteudoPadrao() : "");
						}
						
						campoExtraLeadList.add(campoExtraLead);
					}
				}
			}
		}
		
		return campoExtraLeadList;
	}
	
	public Resource gerarRelatorioCSVListagem(LeadFiltro filtro){
		
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Lead> listaLead = this.findForCSV(filtro);
		
		if(listaLead != null && !listaLead.isEmpty()){
			for(Lead lead : listaLead){
				this.addInfListagem(lead, filtro, true);			
			}
		}
		
		StringBuilder csv = new StringBuilder();
		csv.append("\"Nome\";");
		csv.append("\"Empresa\";");
		csv.append("\"Campanhas\";");
		csv.append("\"Respons�vel\";");
		csv.append("\"Situa��o\";");
		csv.append("\"Qualifica��o\";");
		csv.append("\"E-mails\";");
		csv.append("\"Telefones\";");
		csv.append("\"Data de Retorno\";");
		csv.append("\"Pr�ximo Passo\";");
		csv.append("\"Ciclo\";");
		csv.append("\"Totais por Tipo de Atividade\";");
		csv.append("\"�lt. Contato\";");
		csv.append("\"Int.\";");
		csv.append("\"Segmento\";");
		
		for (Lead lead: listaLead){
			
			StringBuilder csvAux = new StringBuilder();
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getNome()) + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getEmpresa()) + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getListaCampanhas()) + "\";");
			csvAux.append("\"" + (lead.getResponsavel()!=null ? lead.getResponsavel().getNome() : "") + "\";");
			csvAux.append("\"" + (lead.getLeadsituacao()!=null ? lead.getLeadsituacao().getNome() : "") + "\";");
			csvAux.append("\"" + (lead.getLeadqualificacao()!=null ? lead.getLeadqualificacao().getNome() : "") + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getListaEmails()) + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getListaTelefones()) + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getDtRetornoString()) + "\";");
			csvAux.append("\"" + Util.strings.emptyIfNull(lead.getProximopasso()) + "\";");
			csvAux.append("\"" + (lead.getCiclo()!=null ? lead.getCiclo() : "") + "\";");
			csvAux.append("\"" + (lead.getTotaisatividadetipo()!=null ? lead.getTotaisatividadetipo() : "") + "\";");
			csvAux.append("\"" + (lead.getDtultimohistorico()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(lead.getDtultimohistorico()) : "") + "\";");
			csvAux.append("\"" + (lead.getQtdehistorico()!=null ? lead.getQtdehistorico() : "") + "\";");
			if(lead.getListLeadsegmento() != null)
				csvAux.append("\"" + CollectionsUtil.listAndConcatenate(lead.getListLeadsegmento(), "segmento.nome", " / "));
			csvAux.append("\";");
			
			String csvAuxString = csvAux.toString();
			csvAuxString = csvAuxString.replace("<br>", "");
			csvAuxString = csvAuxString.replaceAll("\n", "");
			
			csv.append("\n");
			csv.append(csvAuxString);
		}		
		
		Resource resource = new Resource("text/csv", "lead_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		
		return resource;
	}

	public void updateDataConversaoLead(Lead lead){
		this.leadDAO.updateDataConversaoLead(lead);
	}
}