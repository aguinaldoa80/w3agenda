package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.dao.OrdemcompraentregaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcompraentregaService extends GenericService<Ordemcompraentrega>{

	private OrdemcompraentregaDAO ordemcompraentregaDAO;
	
	public void setOrdemcompraentregaDAO(OrdemcompraentregaDAO ordemcompraentregaDAO) {
		this.ordemcompraentregaDAO = ordemcompraentregaDAO;
	}

	public void salvaOrdemcompraentrega(List<Ordemcompra> listaOrdemcompra, Entrega entrega){	
		for (Ordemcompra ordemcompra : listaOrdemcompra) {
				Ordemcompraentrega ordemcompraentrega = new Ordemcompraentrega();
				ordemcompraentrega.setOrdemcompra(ordemcompra);
				ordemcompraentrega.setEntrega(entrega);
				this.saveOrUpdateNoUseTransaction(ordemcompraentrega);
		}
	}

	public List<Ordemcompraentrega> findByEntrega(Entrega entrega) {
		return ordemcompraentregaDAO.findByEntrega(entrega);
	}

	public List<Ordemcompraentrega> findByOrdemcompra(Ordemcompra ordemcompra) {
		return ordemcompraentregaDAO.findByOrdemcompra(ordemcompra);
	}
}
