package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.dao.PneuSegmentoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PneuSegmentoService extends GenericService<PneuSegmento>{
	protected PneuSegmentoDAO pneuSegmentoDao;
	
	private static PneuSegmentoService instance;
	
	public static PneuSegmentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PneuSegmentoService.class);
		}
		return instance;
	}

	public void setPneuSegmentoDao(PneuSegmentoDAO pneuSegmentoDao) {
		this.pneuSegmentoDao = pneuSegmentoDao;
	}
	
	public Boolean validaPrincipalAtivo (PneuSegmento pneuSegmento){
		return pneuSegmentoDao.validaPrincipalAtivo(pneuSegmento);
	}
	
	public List<PneuSegmento> findPneuSegmento(){
		return pneuSegmentoDao.findPneuSegmento();
	}
	
	public PneuSegmento findPneuSegmentoCd(String whereIn){
		return pneuSegmentoDao.findPneuSegmentoCd(whereIn);
	}
	
	public List<PneuSegmento> findAutocomplete(String q) {
		return pneuSegmentoDao.findAutocomplete(q);
	}
	
	public boolean isOtr(PneuSegmento pneuSegmento){
		PneuSegmento bean = this.load(pneuSegmento, "pneuSegmento.cdPneuSegmento, pneuSegmento.otr");
		if(bean == null){
			throw new SinedException("Segmento de pneu n�o pode ser nulo");
		}
		return Boolean.TRUE.equals(bean.getOtr());
	}
	
	public PneuSegmento loadForHabilitarCampos(PneuSegmento pneuSegmento) {
		return pneuSegmentoDao.loadForHabilitarCampos(pneuSegmento);
	}
	
	public PneuSegmento loadPrincipal() {
		return pneuSegmentoDao.loadPrincipal();
	}
}
