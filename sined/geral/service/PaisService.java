package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.dao.PaisDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PaisService extends GenericService<Pais>{
	
	private PaisDAO paisDAO;
	
	public void setPaisDAO(PaisDAO paisDAO) {this.paisDAO = paisDAO;}

	public boolean isInternacional(Pais pais1, Pais pais2){
		int cdpais1 = pais1==null ? Pais.BRASIL.getCdpais() : pais1.getCdpais();  
		int cdpais2 = pais2==null ? Pais.BRASIL.getCdpais() : pais2.getCdpais();  
		return cdpais1!=cdpais2;
	}

	public Pais findByNome(String nome) {
		return paisDAO.findByNome(nome);
	}
}
