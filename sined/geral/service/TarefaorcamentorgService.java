package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorg;
import br.com.linkcom.sined.geral.dao.TarefaorcamentorgDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefaorcamentorgService extends GenericService<Tarefaorcamentorg> {
	
	private TarefaorcamentorgDAO tarefaorcamentorgDAO;
	
	public void setTarefaorcamentorgDAO(TarefaorcamentorgDAO tarefaorcamentorgDAO) {
		this.tarefaorcamentorgDAO = tarefaorcamentorgDAO;
	}
	
	/**
	 * Busca os recursos gerais das tarefas de um determinado or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaorcamentorgDAO#findByOrcamentoFlex(Orcamento)
	 *
	 * @param orcamento
 	 * @return List<Tarefaorcamentorg>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */	
	public List<Tarefaorcamentorg> findByOrcamentoFlex(Orcamento orcamento) {
		return tarefaorcamentorgDAO.findByOrcamentoFlex(orcamento);
	}

	public List<Tarefaorcamentorg> findByOrcamento(Orcamento orcamento) {
		return tarefaorcamentorgDAO.findByOrcamento(orcamento);
	}
}
